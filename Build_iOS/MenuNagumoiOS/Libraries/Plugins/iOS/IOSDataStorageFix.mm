#import "IOSDataStorageFix.h"


@implementation IOSDataStorageFix
static IOSDataStorageFix *_sharedInstance;

+ (id)sharedInstance {
    
    if (_sharedInstance == nil)  {
        _sharedInstance = [[self alloc] init];
    }
    
    return _sharedInstance;
}

- (BOOL) _MTV_IOS_RemoveFromBackup:(NSString *) filePathString
{
    NSURL* URL= [NSURL fileURLWithPath: filePathString];
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
 
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }else{
        NSLog(@"Succes excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}

extern "C" { 
    bool _MTV_RemoveFromBackup(char* filePathString){

        [[IOSDataStorageFix sharedInstance] _MTV_IOS_RemoveFromBackup: [ISNDataConvertor charToNSString:filePathString ]];
    }
}
@end