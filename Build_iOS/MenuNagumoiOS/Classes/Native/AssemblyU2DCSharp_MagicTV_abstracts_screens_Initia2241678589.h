﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MagicTV.abstracts.screens.InitialSplashScreen
struct InitialSplashScreen_t2241678589;

#include "AssemblyU2DCSharp_MagicTV_abstracts_screens_LoadSc4059313920.h"
#include "AssemblyU2DCSharp_MagicTV_globals_Perspective644553802.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.abstracts.screens.InitialSplashScreen
struct  InitialSplashScreen_t2241678589  : public LoadScreenAbstract_t4059313920
{
public:
	// MagicTV.globals.Perspective MagicTV.abstracts.screens.InitialSplashScreen::_currentPerspective
	int32_t ____currentPerspective_8;
	// System.Int32 MagicTV.abstracts.screens.InitialSplashScreen::_timeoutToHide
	int32_t ____timeoutToHide_10;

public:
	inline static int32_t get_offset_of__currentPerspective_8() { return static_cast<int32_t>(offsetof(InitialSplashScreen_t2241678589, ____currentPerspective_8)); }
	inline int32_t get__currentPerspective_8() const { return ____currentPerspective_8; }
	inline int32_t* get_address_of__currentPerspective_8() { return &____currentPerspective_8; }
	inline void set__currentPerspective_8(int32_t value)
	{
		____currentPerspective_8 = value;
	}

	inline static int32_t get_offset_of__timeoutToHide_10() { return static_cast<int32_t>(offsetof(InitialSplashScreen_t2241678589, ____timeoutToHide_10)); }
	inline int32_t get__timeoutToHide_10() const { return ____timeoutToHide_10; }
	inline int32_t* get_address_of__timeoutToHide_10() { return &____timeoutToHide_10; }
	inline void set__timeoutToHide_10(int32_t value)
	{
		____timeoutToHide_10 = value;
	}
};

struct InitialSplashScreen_t2241678589_StaticFields
{
public:
	// MagicTV.abstracts.screens.InitialSplashScreen MagicTV.abstracts.screens.InitialSplashScreen::Instance
	InitialSplashScreen_t2241678589 * ___Instance_9;

public:
	inline static int32_t get_offset_of_Instance_9() { return static_cast<int32_t>(offsetof(InitialSplashScreen_t2241678589_StaticFields, ___Instance_9)); }
	inline InitialSplashScreen_t2241678589 * get_Instance_9() const { return ___Instance_9; }
	inline InitialSplashScreen_t2241678589 ** get_address_of_Instance_9() { return &___Instance_9; }
	inline void set_Instance_9(InitialSplashScreen_t2241678589 * value)
	{
		___Instance_9 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
