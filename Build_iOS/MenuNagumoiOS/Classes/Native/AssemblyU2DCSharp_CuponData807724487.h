﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CuponData
struct  CuponData_t807724487  : public Il2CppObject
{
public:
	// System.Int32 CuponData::valor
	int32_t ___valor_0;
	// System.Int32 CuponData::pts
	int32_t ___pts_1;
	// System.Int32 CuponData::promocao
	int32_t ___promocao_2;
	// System.String CuponData::id
	String_t* ___id_3;
	// System.String CuponData::dataExpira
	String_t* ___dataExpira_4;
	// System.Boolean CuponData::used
	bool ___used_5;

public:
	inline static int32_t get_offset_of_valor_0() { return static_cast<int32_t>(offsetof(CuponData_t807724487, ___valor_0)); }
	inline int32_t get_valor_0() const { return ___valor_0; }
	inline int32_t* get_address_of_valor_0() { return &___valor_0; }
	inline void set_valor_0(int32_t value)
	{
		___valor_0 = value;
	}

	inline static int32_t get_offset_of_pts_1() { return static_cast<int32_t>(offsetof(CuponData_t807724487, ___pts_1)); }
	inline int32_t get_pts_1() const { return ___pts_1; }
	inline int32_t* get_address_of_pts_1() { return &___pts_1; }
	inline void set_pts_1(int32_t value)
	{
		___pts_1 = value;
	}

	inline static int32_t get_offset_of_promocao_2() { return static_cast<int32_t>(offsetof(CuponData_t807724487, ___promocao_2)); }
	inline int32_t get_promocao_2() const { return ___promocao_2; }
	inline int32_t* get_address_of_promocao_2() { return &___promocao_2; }
	inline void set_promocao_2(int32_t value)
	{
		___promocao_2 = value;
	}

	inline static int32_t get_offset_of_id_3() { return static_cast<int32_t>(offsetof(CuponData_t807724487, ___id_3)); }
	inline String_t* get_id_3() const { return ___id_3; }
	inline String_t** get_address_of_id_3() { return &___id_3; }
	inline void set_id_3(String_t* value)
	{
		___id_3 = value;
		Il2CppCodeGenWriteBarrier(&___id_3, value);
	}

	inline static int32_t get_offset_of_dataExpira_4() { return static_cast<int32_t>(offsetof(CuponData_t807724487, ___dataExpira_4)); }
	inline String_t* get_dataExpira_4() const { return ___dataExpira_4; }
	inline String_t** get_address_of_dataExpira_4() { return &___dataExpira_4; }
	inline void set_dataExpira_4(String_t* value)
	{
		___dataExpira_4 = value;
		Il2CppCodeGenWriteBarrier(&___dataExpira_4, value);
	}

	inline static int32_t get_offset_of_used_5() { return static_cast<int32_t>(offsetof(CuponData_t807724487, ___used_5)); }
	inline bool get_used_5() const { return ___used_5; }
	inline bool* get_address_of_used_5() { return &___used_5; }
	inline void set_used_5(bool value)
	{
		___used_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
