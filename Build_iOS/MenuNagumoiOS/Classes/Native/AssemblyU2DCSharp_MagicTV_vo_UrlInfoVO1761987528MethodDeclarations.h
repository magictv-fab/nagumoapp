﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.vo.UrlInfoVO
struct UrlInfoVO_t1761987528;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.vo.UrlInfoVO::.ctor()
extern "C"  void UrlInfoVO__ctor_m2072431291 (UrlInfoVO_t1761987528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.vo.UrlInfoVO::ToString()
extern "C"  String_t* UrlInfoVO_ToString_m1511497912 (UrlInfoVO_t1761987528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.UrlInfoVO MagicTV.vo.UrlInfoVO::Clone()
extern "C"  UrlInfoVO_t1761987528 * UrlInfoVO_Clone_m1069712825 (UrlInfoVO_t1761987528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
