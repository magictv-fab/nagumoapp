﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.PDF417.Internal.EC.ModulusGF
struct ModulusGF_t2738856732;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.EC.ModulusPoly
struct  ModulusPoly_t3133325097  : public Il2CppObject
{
public:
	// ZXing.PDF417.Internal.EC.ModulusGF ZXing.PDF417.Internal.EC.ModulusPoly::field
	ModulusGF_t2738856732 * ___field_0;
	// System.Int32[] ZXing.PDF417.Internal.EC.ModulusPoly::coefficients
	Int32U5BU5D_t3230847821* ___coefficients_1;

public:
	inline static int32_t get_offset_of_field_0() { return static_cast<int32_t>(offsetof(ModulusPoly_t3133325097, ___field_0)); }
	inline ModulusGF_t2738856732 * get_field_0() const { return ___field_0; }
	inline ModulusGF_t2738856732 ** get_address_of_field_0() { return &___field_0; }
	inline void set_field_0(ModulusGF_t2738856732 * value)
	{
		___field_0 = value;
		Il2CppCodeGenWriteBarrier(&___field_0, value);
	}

	inline static int32_t get_offset_of_coefficients_1() { return static_cast<int32_t>(offsetof(ModulusPoly_t3133325097, ___coefficients_1)); }
	inline Int32U5BU5D_t3230847821* get_coefficients_1() const { return ___coefficients_1; }
	inline Int32U5BU5D_t3230847821** get_address_of_coefficients_1() { return &___coefficients_1; }
	inline void set_coefficients_1(Int32U5BU5D_t3230847821* value)
	{
		___coefficients_1 = value;
		Il2CppCodeGenWriteBarrier(&___coefficients_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
