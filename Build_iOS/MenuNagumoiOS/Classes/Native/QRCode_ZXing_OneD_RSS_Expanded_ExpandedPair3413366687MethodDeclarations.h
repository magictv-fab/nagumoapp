﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.RSS.Expanded.ExpandedPair
struct ExpandedPair_t3413366687;
// ZXing.OneD.RSS.DataCharacter
struct DataCharacter_t770728801;
// ZXing.OneD.RSS.FinderPattern
struct FinderPattern_t3366792524;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_OneD_RSS_DataCharacter770728801.h"
#include "QRCode_ZXing_OneD_RSS_FinderPattern3366792524.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void ZXing.OneD.RSS.Expanded.ExpandedPair::set_MayBeLast(System.Boolean)
extern "C"  void ExpandedPair_set_MayBeLast_m1546656587 (ExpandedPair_t3413366687 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.OneD.RSS.DataCharacter ZXing.OneD.RSS.Expanded.ExpandedPair::get_LeftChar()
extern "C"  DataCharacter_t770728801 * ExpandedPair_get_LeftChar_m2319284949 (ExpandedPair_t3413366687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.ExpandedPair::set_LeftChar(ZXing.OneD.RSS.DataCharacter)
extern "C"  void ExpandedPair_set_LeftChar_m2173118668 (ExpandedPair_t3413366687 * __this, DataCharacter_t770728801 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.OneD.RSS.DataCharacter ZXing.OneD.RSS.Expanded.ExpandedPair::get_RightChar()
extern "C"  DataCharacter_t770728801 * ExpandedPair_get_RightChar_m2733912540 (ExpandedPair_t3413366687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.ExpandedPair::set_RightChar(ZXing.OneD.RSS.DataCharacter)
extern "C"  void ExpandedPair_set_RightChar_m1956333931 (ExpandedPair_t3413366687 * __this, DataCharacter_t770728801 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.OneD.RSS.FinderPattern ZXing.OneD.RSS.Expanded.ExpandedPair::get_FinderPattern()
extern "C"  FinderPattern_t3366792524 * ExpandedPair_get_FinderPattern_m2904727167 (ExpandedPair_t3413366687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.ExpandedPair::set_FinderPattern(ZXing.OneD.RSS.FinderPattern)
extern "C"  void ExpandedPair_set_FinderPattern_m2616770904 (ExpandedPair_t3413366687 * __this, FinderPattern_t3366792524 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.ExpandedPair::.ctor(ZXing.OneD.RSS.DataCharacter,ZXing.OneD.RSS.DataCharacter,ZXing.OneD.RSS.FinderPattern,System.Boolean)
extern "C"  void ExpandedPair__ctor_m350131120 (ExpandedPair_t3413366687 * __this, DataCharacter_t770728801 * ___leftChar0, DataCharacter_t770728801 * ___rightChar1, FinderPattern_t3366792524 * ___finderPattern2, bool ___mayBeLast3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.RSS.Expanded.ExpandedPair::get_MustBeLast()
extern "C"  bool ExpandedPair_get_MustBeLast_m962515566 (ExpandedPair_t3413366687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.OneD.RSS.Expanded.ExpandedPair::ToString()
extern "C"  String_t* ExpandedPair_ToString_m3692205278 (ExpandedPair_t3413366687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.RSS.Expanded.ExpandedPair::Equals(System.Object)
extern "C"  bool ExpandedPair_Equals_m1901776658 (ExpandedPair_t3413366687 * __this, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.RSS.Expanded.ExpandedPair::EqualsOrNull(System.Object,System.Object)
extern "C"  bool ExpandedPair_EqualsOrNull_m3525606710 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o10, Il2CppObject * ___o21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.RSS.Expanded.ExpandedPair::GetHashCode()
extern "C"  int32_t ExpandedPair_GetHashCode_m301970742 (ExpandedPair_t3413366687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.RSS.Expanded.ExpandedPair::hashNotNull(System.Object)
extern "C"  int32_t ExpandedPair_hashNotNull_m35171875 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
