﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.MasterServerClearHostList
struct MasterServerClearHostList_t3350951132;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.MasterServerClearHostList::.ctor()
extern "C"  void MasterServerClearHostList__ctor_m3245766618 (MasterServerClearHostList_t3350951132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerClearHostList::OnEnter()
extern "C"  void MasterServerClearHostList_OnEnter_m427191601 (MasterServerClearHostList_t3350951132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
