﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWW
struct WWW_t3134621005;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// UnityEngine.Sprite
struct Sprite_t3199167241;
// System.Object
struct Il2CppObject;
// OfertasManagerCRM
struct OfertasManagerCRM_t3100875987;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OfertasManagerCRM/<ILoadImgs>c__Iterator6
struct  U3CILoadImgsU3Ec__Iterator6_t3375676004  : public Il2CppObject
{
public:
	// System.Int32 OfertasManagerCRM/<ILoadImgs>c__Iterator6::index
	int32_t ___index_0;
	// System.Int32 OfertasManagerCRM/<ILoadImgs>c__Iterator6::<i>__0
	int32_t ___U3CiU3E__0_1;
	// UnityEngine.WWW OfertasManagerCRM/<ILoadImgs>c__Iterator6::<www>__1
	WWW_t3134621005 * ___U3CwwwU3E__1_2;
	// UnityEngine.Texture2D OfertasManagerCRM/<ILoadImgs>c__Iterator6::<texture>__2
	Texture2D_t3884108195 * ___U3CtextureU3E__2_3;
	// UnityEngine.Sprite OfertasManagerCRM/<ILoadImgs>c__Iterator6::<spt>__3
	Sprite_t3199167241 * ___U3CsptU3E__3_4;
	// System.Int32 OfertasManagerCRM/<ILoadImgs>c__Iterator6::<i>__4
	int32_t ___U3CiU3E__4_5;
	// UnityEngine.WWW OfertasManagerCRM/<ILoadImgs>c__Iterator6::<wwwDePor>__5
	WWW_t3134621005 * ___U3CwwwDePorU3E__5_6;
	// UnityEngine.Texture2D OfertasManagerCRM/<ILoadImgs>c__Iterator6::<texture>__6
	Texture2D_t3884108195 * ___U3CtextureU3E__6_7;
	// UnityEngine.Sprite OfertasManagerCRM/<ILoadImgs>c__Iterator6::<spt>__7
	Sprite_t3199167241 * ___U3CsptU3E__7_8;
	// System.Int32 OfertasManagerCRM/<ILoadImgs>c__Iterator6::$PC
	int32_t ___U24PC_9;
	// System.Object OfertasManagerCRM/<ILoadImgs>c__Iterator6::$current
	Il2CppObject * ___U24current_10;
	// System.Int32 OfertasManagerCRM/<ILoadImgs>c__Iterator6::<$>index
	int32_t ___U3CU24U3Eindex_11;
	// OfertasManagerCRM OfertasManagerCRM/<ILoadImgs>c__Iterator6::<>f__this
	OfertasManagerCRM_t3100875987 * ___U3CU3Ef__this_12;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator6_t3375676004, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__0_1() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator6_t3375676004, ___U3CiU3E__0_1)); }
	inline int32_t get_U3CiU3E__0_1() const { return ___U3CiU3E__0_1; }
	inline int32_t* get_address_of_U3CiU3E__0_1() { return &___U3CiU3E__0_1; }
	inline void set_U3CiU3E__0_1(int32_t value)
	{
		___U3CiU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CwwwU3E__1_2() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator6_t3375676004, ___U3CwwwU3E__1_2)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__1_2() const { return ___U3CwwwU3E__1_2; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__1_2() { return &___U3CwwwU3E__1_2; }
	inline void set_U3CwwwU3E__1_2(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U3CtextureU3E__2_3() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator6_t3375676004, ___U3CtextureU3E__2_3)); }
	inline Texture2D_t3884108195 * get_U3CtextureU3E__2_3() const { return ___U3CtextureU3E__2_3; }
	inline Texture2D_t3884108195 ** get_address_of_U3CtextureU3E__2_3() { return &___U3CtextureU3E__2_3; }
	inline void set_U3CtextureU3E__2_3(Texture2D_t3884108195 * value)
	{
		___U3CtextureU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtextureU3E__2_3, value);
	}

	inline static int32_t get_offset_of_U3CsptU3E__3_4() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator6_t3375676004, ___U3CsptU3E__3_4)); }
	inline Sprite_t3199167241 * get_U3CsptU3E__3_4() const { return ___U3CsptU3E__3_4; }
	inline Sprite_t3199167241 ** get_address_of_U3CsptU3E__3_4() { return &___U3CsptU3E__3_4; }
	inline void set_U3CsptU3E__3_4(Sprite_t3199167241 * value)
	{
		___U3CsptU3E__3_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsptU3E__3_4, value);
	}

	inline static int32_t get_offset_of_U3CiU3E__4_5() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator6_t3375676004, ___U3CiU3E__4_5)); }
	inline int32_t get_U3CiU3E__4_5() const { return ___U3CiU3E__4_5; }
	inline int32_t* get_address_of_U3CiU3E__4_5() { return &___U3CiU3E__4_5; }
	inline void set_U3CiU3E__4_5(int32_t value)
	{
		___U3CiU3E__4_5 = value;
	}

	inline static int32_t get_offset_of_U3CwwwDePorU3E__5_6() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator6_t3375676004, ___U3CwwwDePorU3E__5_6)); }
	inline WWW_t3134621005 * get_U3CwwwDePorU3E__5_6() const { return ___U3CwwwDePorU3E__5_6; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwDePorU3E__5_6() { return &___U3CwwwDePorU3E__5_6; }
	inline void set_U3CwwwDePorU3E__5_6(WWW_t3134621005 * value)
	{
		___U3CwwwDePorU3E__5_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwDePorU3E__5_6, value);
	}

	inline static int32_t get_offset_of_U3CtextureU3E__6_7() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator6_t3375676004, ___U3CtextureU3E__6_7)); }
	inline Texture2D_t3884108195 * get_U3CtextureU3E__6_7() const { return ___U3CtextureU3E__6_7; }
	inline Texture2D_t3884108195 ** get_address_of_U3CtextureU3E__6_7() { return &___U3CtextureU3E__6_7; }
	inline void set_U3CtextureU3E__6_7(Texture2D_t3884108195 * value)
	{
		___U3CtextureU3E__6_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtextureU3E__6_7, value);
	}

	inline static int32_t get_offset_of_U3CsptU3E__7_8() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator6_t3375676004, ___U3CsptU3E__7_8)); }
	inline Sprite_t3199167241 * get_U3CsptU3E__7_8() const { return ___U3CsptU3E__7_8; }
	inline Sprite_t3199167241 ** get_address_of_U3CsptU3E__7_8() { return &___U3CsptU3E__7_8; }
	inline void set_U3CsptU3E__7_8(Sprite_t3199167241 * value)
	{
		___U3CsptU3E__7_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsptU3E__7_8, value);
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator6_t3375676004, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator6_t3375676004, ___U24current_10)); }
	inline Il2CppObject * get_U24current_10() const { return ___U24current_10; }
	inline Il2CppObject ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(Il2CppObject * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_10, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eindex_11() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator6_t3375676004, ___U3CU24U3Eindex_11)); }
	inline int32_t get_U3CU24U3Eindex_11() const { return ___U3CU24U3Eindex_11; }
	inline int32_t* get_address_of_U3CU24U3Eindex_11() { return &___U3CU24U3Eindex_11; }
	inline void set_U3CU24U3Eindex_11(int32_t value)
	{
		___U3CU24U3Eindex_11 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_12() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator6_t3375676004, ___U3CU3Ef__this_12)); }
	inline OfertasManagerCRM_t3100875987 * get_U3CU3Ef__this_12() const { return ___U3CU3Ef__this_12; }
	inline OfertasManagerCRM_t3100875987 ** get_address_of_U3CU3Ef__this_12() { return &___U3CU3Ef__this_12; }
	inline void set_U3CU3Ef__this_12(OfertasManagerCRM_t3100875987 * value)
	{
		___U3CU3Ef__this_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
