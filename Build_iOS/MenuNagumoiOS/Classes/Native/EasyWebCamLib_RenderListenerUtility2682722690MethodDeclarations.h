﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Action
struct Action_t3771233898;
// System.Action`1<System.Boolean>
struct Action_1_t872614854;
// RenderListenerUtility
struct RenderListenerUtility_t2682722690;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3771233898.h"

// System.Void RenderListenerUtility::add_onQuit(System.Action)
extern "C"  void RenderListenerUtility_add_onQuit_m1222160124 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RenderListenerUtility::remove_onQuit(System.Action)
extern "C"  void RenderListenerUtility_remove_onQuit_m2761031175 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RenderListenerUtility::add_onPause(System.Action`1<System.Boolean>)
extern "C"  void RenderListenerUtility_add_onPause_m2565202413 (Il2CppObject * __this /* static, unused */, Action_1_t872614854 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RenderListenerUtility::remove_onPause(System.Action`1<System.Boolean>)
extern "C"  void RenderListenerUtility_remove_onPause_m766011650 (Il2CppObject * __this /* static, unused */, Action_1_t872614854 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RenderListenerUtility::.cctor()
extern "C"  void RenderListenerUtility__cctor_m720909886 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RenderListenerUtility::Start()
extern "C"  void RenderListenerUtility_Start_m1895438415 (RenderListenerUtility_t2682722690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RenderListenerUtility::Update()
extern "C"  void RenderListenerUtility_Update_m2929868190 (RenderListenerUtility_t2682722690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RenderListenerUtility::OnApplicationPause(System.Boolean)
extern "C"  void RenderListenerUtility_OnApplicationPause_m1365855825 (RenderListenerUtility_t2682722690 * __this, bool ___paused0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RenderListenerUtility::OnApplicationQuit()
extern "C"  void RenderListenerUtility_OnApplicationQuit_m1995206221 (RenderListenerUtility_t2682722690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RenderListenerUtility::.ctor()
extern "C"  void RenderListenerUtility__ctor_m2948300623 (RenderListenerUtility_t2682722690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
