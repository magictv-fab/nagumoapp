﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Camera
struct Camera_t2727095145;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_ScreenOrientation1849668026.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraPlaneController
struct  CameraPlaneController_t4203072787  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Camera CameraPlaneController::_targetCam
	Camera_t2727095145 * ____targetCam_2;
	// UnityEngine.ScreenOrientation CameraPlaneController::orientation
	int32_t ___orientation_3;
	// System.Single CameraPlaneController::height
	float ___height_4;
	// System.Single CameraPlaneController::width
	float ___width_5;
	// System.Single CameraPlaneController::screenRatio
	float ___screenRatio_6;
	// UnityEngine.Vector3 CameraPlaneController::and_pRot
	Vector3_t4282066566  ___and_pRot_7;
	// UnityEngine.Vector3 CameraPlaneController::and_puRot
	Vector3_t4282066566  ___and_puRot_8;
	// UnityEngine.Vector3 CameraPlaneController::and_lRot
	Vector3_t4282066566  ___and_lRot_9;
	// UnityEngine.Vector3 CameraPlaneController::and_lrRot
	Vector3_t4282066566  ___and_lrRot_10;

public:
	inline static int32_t get_offset_of__targetCam_2() { return static_cast<int32_t>(offsetof(CameraPlaneController_t4203072787, ____targetCam_2)); }
	inline Camera_t2727095145 * get__targetCam_2() const { return ____targetCam_2; }
	inline Camera_t2727095145 ** get_address_of__targetCam_2() { return &____targetCam_2; }
	inline void set__targetCam_2(Camera_t2727095145 * value)
	{
		____targetCam_2 = value;
		Il2CppCodeGenWriteBarrier(&____targetCam_2, value);
	}

	inline static int32_t get_offset_of_orientation_3() { return static_cast<int32_t>(offsetof(CameraPlaneController_t4203072787, ___orientation_3)); }
	inline int32_t get_orientation_3() const { return ___orientation_3; }
	inline int32_t* get_address_of_orientation_3() { return &___orientation_3; }
	inline void set_orientation_3(int32_t value)
	{
		___orientation_3 = value;
	}

	inline static int32_t get_offset_of_height_4() { return static_cast<int32_t>(offsetof(CameraPlaneController_t4203072787, ___height_4)); }
	inline float get_height_4() const { return ___height_4; }
	inline float* get_address_of_height_4() { return &___height_4; }
	inline void set_height_4(float value)
	{
		___height_4 = value;
	}

	inline static int32_t get_offset_of_width_5() { return static_cast<int32_t>(offsetof(CameraPlaneController_t4203072787, ___width_5)); }
	inline float get_width_5() const { return ___width_5; }
	inline float* get_address_of_width_5() { return &___width_5; }
	inline void set_width_5(float value)
	{
		___width_5 = value;
	}

	inline static int32_t get_offset_of_screenRatio_6() { return static_cast<int32_t>(offsetof(CameraPlaneController_t4203072787, ___screenRatio_6)); }
	inline float get_screenRatio_6() const { return ___screenRatio_6; }
	inline float* get_address_of_screenRatio_6() { return &___screenRatio_6; }
	inline void set_screenRatio_6(float value)
	{
		___screenRatio_6 = value;
	}

	inline static int32_t get_offset_of_and_pRot_7() { return static_cast<int32_t>(offsetof(CameraPlaneController_t4203072787, ___and_pRot_7)); }
	inline Vector3_t4282066566  get_and_pRot_7() const { return ___and_pRot_7; }
	inline Vector3_t4282066566 * get_address_of_and_pRot_7() { return &___and_pRot_7; }
	inline void set_and_pRot_7(Vector3_t4282066566  value)
	{
		___and_pRot_7 = value;
	}

	inline static int32_t get_offset_of_and_puRot_8() { return static_cast<int32_t>(offsetof(CameraPlaneController_t4203072787, ___and_puRot_8)); }
	inline Vector3_t4282066566  get_and_puRot_8() const { return ___and_puRot_8; }
	inline Vector3_t4282066566 * get_address_of_and_puRot_8() { return &___and_puRot_8; }
	inline void set_and_puRot_8(Vector3_t4282066566  value)
	{
		___and_puRot_8 = value;
	}

	inline static int32_t get_offset_of_and_lRot_9() { return static_cast<int32_t>(offsetof(CameraPlaneController_t4203072787, ___and_lRot_9)); }
	inline Vector3_t4282066566  get_and_lRot_9() const { return ___and_lRot_9; }
	inline Vector3_t4282066566 * get_address_of_and_lRot_9() { return &___and_lRot_9; }
	inline void set_and_lRot_9(Vector3_t4282066566  value)
	{
		___and_lRot_9 = value;
	}

	inline static int32_t get_offset_of_and_lrRot_10() { return static_cast<int32_t>(offsetof(CameraPlaneController_t4203072787, ___and_lrRot_10)); }
	inline Vector3_t4282066566  get_and_lrRot_10() const { return ___and_lrRot_10; }
	inline Vector3_t4282066566 * get_address_of_and_lrRot_10() { return &___and_lrRot_10; }
	inline void set_and_lrRot_10(Vector3_t4282066566  value)
	{
		___and_lrRot_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
