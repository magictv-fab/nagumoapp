﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JsonProperty
struct JsonProperty_t2248022781;

#include "codegen/il2cpp-codegen.h"

// System.Void JsonProperty::.ctor()
extern "C"  void JsonProperty__ctor_m3248219198 (JsonProperty_t2248022781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
