﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutEndArea
struct GUILayoutEndArea_t2673584043;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutEndArea::.ctor()
extern "C"  void GUILayoutEndArea__ctor_m2765597467 (GUILayoutEndArea_t2673584043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutEndArea::Reset()
extern "C"  void GUILayoutEndArea_Reset_m412030408 (GUILayoutEndArea_t2673584043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutEndArea::OnGUI()
extern "C"  void GUILayoutEndArea_OnGUI_m2260996117 (GUILayoutEndArea_t2673584043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
