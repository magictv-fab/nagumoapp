﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.Common.ReedSolomon.GenericGF
struct GenericGF_t2563420960;
// System.Collections.Generic.IList`1<ZXing.Common.ReedSolomon.GenericGFPoly>
struct IList_1_t3450517423;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.ReedSolomon.ReedSolomonEncoder
struct  ReedSolomonEncoder_t1017191363  : public Il2CppObject
{
public:
	// ZXing.Common.ReedSolomon.GenericGF ZXing.Common.ReedSolomon.ReedSolomonEncoder::field
	GenericGF_t2563420960 * ___field_0;
	// System.Collections.Generic.IList`1<ZXing.Common.ReedSolomon.GenericGFPoly> ZXing.Common.ReedSolomon.ReedSolomonEncoder::cachedGenerators
	Il2CppObject* ___cachedGenerators_1;

public:
	inline static int32_t get_offset_of_field_0() { return static_cast<int32_t>(offsetof(ReedSolomonEncoder_t1017191363, ___field_0)); }
	inline GenericGF_t2563420960 * get_field_0() const { return ___field_0; }
	inline GenericGF_t2563420960 ** get_address_of_field_0() { return &___field_0; }
	inline void set_field_0(GenericGF_t2563420960 * value)
	{
		___field_0 = value;
		Il2CppCodeGenWriteBarrier(&___field_0, value);
	}

	inline static int32_t get_offset_of_cachedGenerators_1() { return static_cast<int32_t>(offsetof(ReedSolomonEncoder_t1017191363, ___cachedGenerators_1)); }
	inline Il2CppObject* get_cachedGenerators_1() const { return ___cachedGenerators_1; }
	inline Il2CppObject** get_address_of_cachedGenerators_1() { return &___cachedGenerators_1; }
	inline void set_cachedGenerators_1(Il2CppObject* value)
	{
		___cachedGenerators_1 = value;
		Il2CppCodeGenWriteBarrier(&___cachedGenerators_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
