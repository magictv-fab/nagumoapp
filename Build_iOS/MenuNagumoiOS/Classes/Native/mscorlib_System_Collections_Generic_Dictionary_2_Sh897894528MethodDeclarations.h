﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.DecodeHintType,System.Object>
struct ShimEnumerator_t897894528;
// System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>
struct Dictionary_2_t1182116501;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.DecodeHintType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1459855393_gshared (ShimEnumerator_t897894528 * __this, Dictionary_2_t1182116501 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m1459855393(__this, ___host0, method) ((  void (*) (ShimEnumerator_t897894528 *, Dictionary_2_t1182116501 *, const MethodInfo*))ShimEnumerator__ctor_m1459855393_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.DecodeHintType,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m337221284_gshared (ShimEnumerator_t897894528 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m337221284(__this, method) ((  bool (*) (ShimEnumerator_t897894528 *, const MethodInfo*))ShimEnumerator_MoveNext_m337221284_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.DecodeHintType,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m2931820550_gshared (ShimEnumerator_t897894528 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m2931820550(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t897894528 *, const MethodInfo*))ShimEnumerator_get_Entry_m2931820550_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.DecodeHintType,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3472880069_gshared (ShimEnumerator_t897894528 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m3472880069(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t897894528 *, const MethodInfo*))ShimEnumerator_get_Key_m3472880069_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.DecodeHintType,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1296593687_gshared (ShimEnumerator_t897894528 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m1296593687(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t897894528 *, const MethodInfo*))ShimEnumerator_get_Value_m1296593687_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.DecodeHintType,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m1910708191_gshared (ShimEnumerator_t897894528 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m1910708191(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t897894528 *, const MethodInfo*))ShimEnumerator_get_Current_m1910708191_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.DecodeHintType,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m3297876211_gshared (ShimEnumerator_t897894528 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m3297876211(__this, method) ((  void (*) (ShimEnumerator_t897894528 *, const MethodInfo*))ShimEnumerator_Reset_m3297876211_gshared)(__this, method)
