﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GUIGridSizer
struct GUIGridSizer_t3080214772;
// UnityEngine.GUIContent[]
struct GUIContentU5BU5D_t3588725815;
// UnityEngine.GUIStyle
struct GUIStyle_t2990928826;
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t2977405297;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GUIStyle2990928826.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"

// System.Void UnityEngine.GUIGridSizer::.ctor(UnityEngine.GUIContent[],System.Int32,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  void GUIGridSizer__ctor_m2437688367 (GUIGridSizer_t3080214772 * __this, GUIContentU5BU5D_t3588725815* ___contents0, int32_t ___xCount1, GUIStyle_t2990928826 * ___buttonStyle2, GUILayoutOptionU5BU5D_t2977405297* ___options3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.GUIGridSizer::GetRect(UnityEngine.GUIContent[],System.Int32,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  Rect_t4241904616  GUIGridSizer_GetRect_m3633838259 (Il2CppObject * __this /* static, unused */, GUIContentU5BU5D_t3588725815* ___contents0, int32_t ___xCount1, GUIStyle_t2990928826 * ___style2, GUILayoutOptionU5BU5D_t2977405297* ___options3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.GUIGridSizer::get_rows()
extern "C"  int32_t GUIGridSizer_get_rows_m185089833 (GUIGridSizer_t3080214772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
