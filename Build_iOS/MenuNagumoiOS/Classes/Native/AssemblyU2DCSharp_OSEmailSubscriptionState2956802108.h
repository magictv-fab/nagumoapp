﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSEmailSubscriptionState
struct  OSEmailSubscriptionState_t2956802108  : public Il2CppObject
{
public:
	// System.String OSEmailSubscriptionState::emailUserId
	String_t* ___emailUserId_0;
	// System.String OSEmailSubscriptionState::emailAddress
	String_t* ___emailAddress_1;
	// System.Boolean OSEmailSubscriptionState::subscribed
	bool ___subscribed_2;

public:
	inline static int32_t get_offset_of_emailUserId_0() { return static_cast<int32_t>(offsetof(OSEmailSubscriptionState_t2956802108, ___emailUserId_0)); }
	inline String_t* get_emailUserId_0() const { return ___emailUserId_0; }
	inline String_t** get_address_of_emailUserId_0() { return &___emailUserId_0; }
	inline void set_emailUserId_0(String_t* value)
	{
		___emailUserId_0 = value;
		Il2CppCodeGenWriteBarrier(&___emailUserId_0, value);
	}

	inline static int32_t get_offset_of_emailAddress_1() { return static_cast<int32_t>(offsetof(OSEmailSubscriptionState_t2956802108, ___emailAddress_1)); }
	inline String_t* get_emailAddress_1() const { return ___emailAddress_1; }
	inline String_t** get_address_of_emailAddress_1() { return &___emailAddress_1; }
	inline void set_emailAddress_1(String_t* value)
	{
		___emailAddress_1 = value;
		Il2CppCodeGenWriteBarrier(&___emailAddress_1, value);
	}

	inline static int32_t get_offset_of_subscribed_2() { return static_cast<int32_t>(offsetof(OSEmailSubscriptionState_t2956802108, ___subscribed_2)); }
	inline bool get_subscribed_2() const { return ___subscribed_2; }
	inline bool* get_address_of_subscribed_2() { return &___subscribed_2; }
	inline void set_subscribed_2(bool value)
	{
		___subscribed_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
