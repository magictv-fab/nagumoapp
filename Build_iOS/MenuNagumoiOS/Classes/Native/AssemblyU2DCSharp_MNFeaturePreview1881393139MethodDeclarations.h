﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MNFeaturePreview
struct MNFeaturePreview_t1881393139;

#include "codegen/il2cpp-codegen.h"

// System.Void MNFeaturePreview::.ctor()
extern "C"  void MNFeaturePreview__ctor_m3895959560 (MNFeaturePreview_t1881393139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNFeaturePreview::InitStyles()
extern "C"  void MNFeaturePreview_InitStyles_m28046734 (MNFeaturePreview_t1881393139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNFeaturePreview::Start()
extern "C"  void MNFeaturePreview_Start_m2843097352 (MNFeaturePreview_t1881393139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNFeaturePreview::UpdateToStartPos()
extern "C"  void MNFeaturePreview_UpdateToStartPos_m2000291346 (MNFeaturePreview_t1881393139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
