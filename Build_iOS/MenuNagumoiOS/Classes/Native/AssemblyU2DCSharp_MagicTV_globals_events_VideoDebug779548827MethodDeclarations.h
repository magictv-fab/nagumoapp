﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.globals.events.VideoDebugEvents/OnFloatEventHandler
struct OnFloatEventHandler_t1243657927;
// MagicTV.globals.events.VideoDebugEvents/OnStringEventHandler
struct OnStringEventHandler_t1157926502;
// System.String
struct String_t;
// MagicTV.globals.events.VideoDebugEvents/OnMetaEventHandler
struct OnMetaEventHandler_t4203203482;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_globals_events_VideoDebu1243657927.h"
#include "AssemblyU2DCSharp_MagicTV_globals_events_VideoDebu1157926502.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_MagicTV_globals_events_VideoDebu4203203482.h"

// System.Void MagicTV.globals.events.VideoDebugEvents::AddDebugFloat1EventHandler(MagicTV.globals.events.VideoDebugEvents/OnFloatEventHandler)
extern "C"  void VideoDebugEvents_AddDebugFloat1EventHandler_m271227922 (Il2CppObject * __this /* static, unused */, OnFloatEventHandler_t1243657927 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.VideoDebugEvents::RemoveDebugFloat1EventHandler(MagicTV.globals.events.VideoDebugEvents/OnFloatEventHandler)
extern "C"  void VideoDebugEvents_RemoveDebugFloat1EventHandler_m2860353879 (Il2CppObject * __this /* static, unused */, OnFloatEventHandler_t1243657927 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.VideoDebugEvents::RaiseDebugFloat1(System.Single)
extern "C"  void VideoDebugEvents_RaiseDebugFloat1_m3165355821 (Il2CppObject * __this /* static, unused */, float ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.VideoDebugEvents::AddChangeDebugShader(MagicTV.globals.events.VideoDebugEvents/OnStringEventHandler)
extern "C"  void VideoDebugEvents_AddChangeDebugShader_m3914959875 (Il2CppObject * __this /* static, unused */, OnStringEventHandler_t1157926502 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.VideoDebugEvents::RemoveChangeDebugShader(MagicTV.globals.events.VideoDebugEvents/OnStringEventHandler)
extern "C"  void VideoDebugEvents_RemoveChangeDebugShader_m3112085406 (Il2CppObject * __this /* static, unused */, OnStringEventHandler_t1157926502 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.VideoDebugEvents::RaiseDebugShader(System.String)
extern "C"  void VideoDebugEvents_RaiseDebugShader_m1868618740 (Il2CppObject * __this /* static, unused */, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.VideoDebugEvents::AddSaveEventHandler(MagicTV.globals.events.VideoDebugEvents/OnMetaEventHandler)
extern "C"  void VideoDebugEvents_AddSaveEventHandler_m180193844 (Il2CppObject * __this /* static, unused */, OnMetaEventHandler_t4203203482 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.VideoDebugEvents::RemoveSaveEventHandler(MagicTV.globals.events.VideoDebugEvents/OnMetaEventHandler)
extern "C"  void VideoDebugEvents_RemoveSaveEventHandler_m3636497529 (Il2CppObject * __this /* static, unused */, OnMetaEventHandler_t4203203482 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.VideoDebugEvents::RaiseSave(System.String,System.String,System.Single,System.Single)
extern "C"  void VideoDebugEvents_RaiseSave_m4005683071 (Il2CppObject * __this /* static, unused */, String_t* ___bundleID0, String_t* ___materialName1, float ___float12, float ___float23, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.VideoDebugEvents::AddDebugFloat2EventHandler(MagicTV.globals.events.VideoDebugEvents/OnFloatEventHandler)
extern "C"  void VideoDebugEvents_AddDebugFloat2EventHandler_m714839345 (Il2CppObject * __this /* static, unused */, OnFloatEventHandler_t1243657927 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.VideoDebugEvents::RemoveDebugFloat2EventHandler(MagicTV.globals.events.VideoDebugEvents/OnFloatEventHandler)
extern "C"  void VideoDebugEvents_RemoveDebugFloat2EventHandler_m3303965302 (Il2CppObject * __this /* static, unused */, OnFloatEventHandler_t1243657927 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.VideoDebugEvents::RaiseDebugFloat2(System.Single)
extern "C"  void VideoDebugEvents_RaiseDebugFloat2_m2654821644 (Il2CppObject * __this /* static, unused */, float ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.VideoDebugEvents::AddDebugFloat3EventHandler(MagicTV.globals.events.VideoDebugEvents/OnFloatEventHandler)
extern "C"  void VideoDebugEvents_AddDebugFloat3EventHandler_m1158450768 (Il2CppObject * __this /* static, unused */, OnFloatEventHandler_t1243657927 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.VideoDebugEvents::RemoveDebugFloat3EventHandler(MagicTV.globals.events.VideoDebugEvents/OnFloatEventHandler)
extern "C"  void VideoDebugEvents_RemoveDebugFloat3EventHandler_m3747576725 (Il2CppObject * __this /* static, unused */, OnFloatEventHandler_t1243657927 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.VideoDebugEvents::RaiseDebugFloat3(System.Single)
extern "C"  void VideoDebugEvents_RaiseDebugFloat3_m2144287467 (Il2CppObject * __this /* static, unused */, float ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.VideoDebugEvents::AddDebugFloat4EventHandler(MagicTV.globals.events.VideoDebugEvents/OnFloatEventHandler)
extern "C"  void VideoDebugEvents_AddDebugFloat4EventHandler_m1602062191 (Il2CppObject * __this /* static, unused */, OnFloatEventHandler_t1243657927 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.VideoDebugEvents::RemoveDebugFloat4EventHandler(MagicTV.globals.events.VideoDebugEvents/OnFloatEventHandler)
extern "C"  void VideoDebugEvents_RemoveDebugFloat4EventHandler_m4191188148 (Il2CppObject * __this /* static, unused */, OnFloatEventHandler_t1243657927 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.VideoDebugEvents::RaiseDebugFloat4(System.Single)
extern "C"  void VideoDebugEvents_RaiseDebugFloat4_m1633753290 (Il2CppObject * __this /* static, unused */, float ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.VideoDebugEvents::AddChangeDebugFloat1EventHandler(MagicTV.globals.events.VideoDebugEvents/OnFloatEventHandler)
extern "C"  void VideoDebugEvents_AddChangeDebugFloat1EventHandler_m3461182434 (Il2CppObject * __this /* static, unused */, OnFloatEventHandler_t1243657927 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.VideoDebugEvents::RemoveChangeDebugFloat1EventHandler(MagicTV.globals.events.VideoDebugEvents/OnFloatEventHandler)
extern "C"  void VideoDebugEvents_RemoveChangeDebugFloat1EventHandler_m1242077543 (Il2CppObject * __this /* static, unused */, OnFloatEventHandler_t1243657927 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.VideoDebugEvents::RaiseChangeDebugFloat1(System.Single)
extern "C"  void VideoDebugEvents_RaiseChangeDebugFloat1_m1488221885 (Il2CppObject * __this /* static, unused */, float ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.VideoDebugEvents::AddChangeDebugFloat2EventHandler(MagicTV.globals.events.VideoDebugEvents/OnFloatEventHandler)
extern "C"  void VideoDebugEvents_AddChangeDebugFloat2EventHandler_m3904793857 (Il2CppObject * __this /* static, unused */, OnFloatEventHandler_t1243657927 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.VideoDebugEvents::RemoveChangeDebugFloat2EventHandler(MagicTV.globals.events.VideoDebugEvents/OnFloatEventHandler)
extern "C"  void VideoDebugEvents_RemoveChangeDebugFloat2EventHandler_m1685688966 (Il2CppObject * __this /* static, unused */, OnFloatEventHandler_t1243657927 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.VideoDebugEvents::RaiseChangeDebugFloat2(System.Single)
extern "C"  void VideoDebugEvents_RaiseChangeDebugFloat2_m977687708 (Il2CppObject * __this /* static, unused */, float ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.VideoDebugEvents::AddChangeDebugFloat3EventHandler(MagicTV.globals.events.VideoDebugEvents/OnFloatEventHandler)
extern "C"  void VideoDebugEvents_AddChangeDebugFloat3EventHandler_m53437984 (Il2CppObject * __this /* static, unused */, OnFloatEventHandler_t1243657927 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.VideoDebugEvents::RemoveChangeDebugFloat3EventHandler(MagicTV.globals.events.VideoDebugEvents/OnFloatEventHandler)
extern "C"  void VideoDebugEvents_RemoveChangeDebugFloat3EventHandler_m2129300389 (Il2CppObject * __this /* static, unused */, OnFloatEventHandler_t1243657927 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.VideoDebugEvents::RaiseChangeDebugFloat3(System.Single)
extern "C"  void VideoDebugEvents_RaiseChangeDebugFloat3_m467153531 (Il2CppObject * __this /* static, unused */, float ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.VideoDebugEvents::AddChangeDebugFloat4EventHandler(MagicTV.globals.events.VideoDebugEvents/OnFloatEventHandler)
extern "C"  void VideoDebugEvents_AddChangeDebugFloat4EventHandler_m497049407 (Il2CppObject * __this /* static, unused */, OnFloatEventHandler_t1243657927 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.VideoDebugEvents::RemoveChangeDebugFloat4EventHandler(MagicTV.globals.events.VideoDebugEvents/OnFloatEventHandler)
extern "C"  void VideoDebugEvents_RemoveChangeDebugFloat4EventHandler_m2572911812 (Il2CppObject * __this /* static, unused */, OnFloatEventHandler_t1243657927 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.VideoDebugEvents::RaiseChangeDebugFloat4(System.Single)
extern "C"  void VideoDebugEvents_RaiseChangeDebugFloat4_m4251586650 (Il2CppObject * __this /* static, unused */, float ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
