﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetLayer
struct  SetLayer_t1144868535  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetLayer::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// System.Int32 HutongGames.PlayMaker.Actions.SetLayer::layer
	int32_t ___layer_10;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(SetLayer_t1144868535, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_layer_10() { return static_cast<int32_t>(offsetof(SetLayer_t1144868535, ___layer_10)); }
	inline int32_t get_layer_10() const { return ___layer_10; }
	inline int32_t* get_address_of_layer_10() { return &___layer_10; }
	inline void set_layer_10(int32_t value)
	{
		___layer_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
