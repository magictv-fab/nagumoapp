﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Cloth
struct Cloth_t4194460560;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnableClothController
struct  EnableClothController_t4016542003  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Cloth EnableClothController::cloth
	Cloth_t4194460560 * ___cloth_2;

public:
	inline static int32_t get_offset_of_cloth_2() { return static_cast<int32_t>(offsetof(EnableClothController_t4016542003, ___cloth_2)); }
	inline Cloth_t4194460560 * get_cloth_2() const { return ___cloth_2; }
	inline Cloth_t4194460560 ** get_address_of_cloth_2() { return &___cloth_2; }
	inline void set_cloth_2(Cloth_t4194460560 * value)
	{
		___cloth_2 = value;
		Il2CppCodeGenWriteBarrier(&___cloth_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
