﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GravityConfig
struct GravityConfig_t4085554960;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void GravityConfig::.ctor()
extern "C"  void GravityConfig__ctor_m2307896411 (GravityConfig_t4085554960 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GravityConfig::getUniqueName()
extern "C"  String_t* GravityConfig_getUniqueName_m2962698734 (GravityConfig_t4085554960 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GravityConfig::configByString(System.String)
extern "C"  void GravityConfig_configByString_m4256846927 (GravityConfig_t4085554960 * __this, String_t* ___metadata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GravityConfig::Reset()
extern "C"  void GravityConfig_Reset_m4249296648 (GravityConfig_t4085554960 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject GravityConfig::getGameObjectReference()
extern "C"  GameObject_t3674682005 * GravityConfig_getGameObjectReference_m991696728 (GravityConfig_t4085554960 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
