﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.transform.filters.SlotFilterPlugin
struct SlotFilterPlugin_t3610247053;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

// System.Void ARM.transform.filters.SlotFilterPlugin::.ctor()
extern "C"  void SlotFilterPlugin__ctor_m4117802443 (SlotFilterPlugin_t3610247053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.SlotFilterPlugin::Start()
extern "C"  void SlotFilterPlugin_Start_m3064940235 (SlotFilterPlugin_t3610247053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.transform.filters.SlotFilterPlugin::_doFilter(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  SlotFilterPlugin__doFilter_m3971686843 (SlotFilterPlugin_t3610247053 * __this, Vector3_t4282066566  ___last0, Vector3_t4282066566  ___nextValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion ARM.transform.filters.SlotFilterPlugin::_doFilter(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  SlotFilterPlugin__doFilter_m2264807619 (SlotFilterPlugin_t3610247053 * __this, Quaternion_t1553702882  ___last0, Quaternion_t1553702882  ___nextValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.SlotFilterPlugin::LateUpdate()
extern "C"  void SlotFilterPlugin_LateUpdate_m4197483368 (SlotFilterPlugin_t3610247053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
