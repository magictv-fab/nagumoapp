﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TBEasyWebCam.CallBack.EasyWebCamStartedDelegate
struct EasyWebCamStartedDelegate_t3886828347;
// TBEasyWebCam.CallBack.EasyWebCamUpdateDelegate
struct EasyWebCamUpdateDelegate_t1731309161;
// TBEasyWebCam.CallBack.EasyWebCamStopedDelegate
struct EasyWebCamStopedDelegate_t2264447425;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TBEasyWebCam.EasyWebCamBase
struct  EasyWebCamBase_t3084723642  : public Il2CppObject
{
public:

public:
};

struct EasyWebCamBase_t3084723642_StaticFields
{
public:
	// TBEasyWebCam.CallBack.EasyWebCamStartedDelegate TBEasyWebCam.EasyWebCamBase::onEasyWebCamStart
	EasyWebCamStartedDelegate_t3886828347 * ___onEasyWebCamStart_0;
	// TBEasyWebCam.CallBack.EasyWebCamUpdateDelegate TBEasyWebCam.EasyWebCamBase::OnEasyWebCamUpdate
	EasyWebCamUpdateDelegate_t1731309161 * ___OnEasyWebCamUpdate_1;
	// TBEasyWebCam.CallBack.EasyWebCamStopedDelegate TBEasyWebCam.EasyWebCamBase::OnEasyWebCamStoped
	EasyWebCamStopedDelegate_t2264447425 * ___OnEasyWebCamStoped_2;
	// System.Boolean TBEasyWebCam.EasyWebCamBase::isRunning
	bool ___isRunning_3;

public:
	inline static int32_t get_offset_of_onEasyWebCamStart_0() { return static_cast<int32_t>(offsetof(EasyWebCamBase_t3084723642_StaticFields, ___onEasyWebCamStart_0)); }
	inline EasyWebCamStartedDelegate_t3886828347 * get_onEasyWebCamStart_0() const { return ___onEasyWebCamStart_0; }
	inline EasyWebCamStartedDelegate_t3886828347 ** get_address_of_onEasyWebCamStart_0() { return &___onEasyWebCamStart_0; }
	inline void set_onEasyWebCamStart_0(EasyWebCamStartedDelegate_t3886828347 * value)
	{
		___onEasyWebCamStart_0 = value;
		Il2CppCodeGenWriteBarrier(&___onEasyWebCamStart_0, value);
	}

	inline static int32_t get_offset_of_OnEasyWebCamUpdate_1() { return static_cast<int32_t>(offsetof(EasyWebCamBase_t3084723642_StaticFields, ___OnEasyWebCamUpdate_1)); }
	inline EasyWebCamUpdateDelegate_t1731309161 * get_OnEasyWebCamUpdate_1() const { return ___OnEasyWebCamUpdate_1; }
	inline EasyWebCamUpdateDelegate_t1731309161 ** get_address_of_OnEasyWebCamUpdate_1() { return &___OnEasyWebCamUpdate_1; }
	inline void set_OnEasyWebCamUpdate_1(EasyWebCamUpdateDelegate_t1731309161 * value)
	{
		___OnEasyWebCamUpdate_1 = value;
		Il2CppCodeGenWriteBarrier(&___OnEasyWebCamUpdate_1, value);
	}

	inline static int32_t get_offset_of_OnEasyWebCamStoped_2() { return static_cast<int32_t>(offsetof(EasyWebCamBase_t3084723642_StaticFields, ___OnEasyWebCamStoped_2)); }
	inline EasyWebCamStopedDelegate_t2264447425 * get_OnEasyWebCamStoped_2() const { return ___OnEasyWebCamStoped_2; }
	inline EasyWebCamStopedDelegate_t2264447425 ** get_address_of_OnEasyWebCamStoped_2() { return &___OnEasyWebCamStoped_2; }
	inline void set_OnEasyWebCamStoped_2(EasyWebCamStopedDelegate_t2264447425 * value)
	{
		___OnEasyWebCamStoped_2 = value;
		Il2CppCodeGenWriteBarrier(&___OnEasyWebCamStoped_2, value);
	}

	inline static int32_t get_offset_of_isRunning_3() { return static_cast<int32_t>(offsetof(EasyWebCamBase_t3084723642_StaticFields, ___isRunning_3)); }
	inline bool get_isRunning_3() const { return ___isRunning_3; }
	inline bool* get_address_of_isRunning_3() { return &___isRunning_3; }
	inline void set_isRunning_3(bool value)
	{
		___isRunning_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
