﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAnimatorSpeed
struct SetAnimatorSpeed_t3423972358;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAnimatorSpeed::.ctor()
extern "C"  void SetAnimatorSpeed__ctor_m624778528 (SetAnimatorSpeed_t3423972358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorSpeed::Reset()
extern "C"  void SetAnimatorSpeed_Reset_m2566178765 (SetAnimatorSpeed_t3423972358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorSpeed::OnEnter()
extern "C"  void SetAnimatorSpeed_OnEnter_m2803439863 (SetAnimatorSpeed_t3423972358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorSpeed::OnUpdate()
extern "C"  void SetAnimatorSpeed_OnUpdate_m140849196 (SetAnimatorSpeed_t3423972358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorSpeed::DoPlaybackSpeed()
extern "C"  void SetAnimatorSpeed_DoPlaybackSpeed_m1675068895 (SetAnimatorSpeed_t3423972358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
