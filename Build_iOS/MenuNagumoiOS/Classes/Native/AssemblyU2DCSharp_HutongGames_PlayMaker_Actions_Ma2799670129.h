﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.MasterServerGetHostData
struct  MasterServerGetHostData_t2799670129  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.MasterServerGetHostData::hostIndex
	FsmInt_t1596138449 * ___hostIndex_9;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.MasterServerGetHostData::useNat
	FsmBool_t1075959796 * ___useNat_10;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.MasterServerGetHostData::gameType
	FsmString_t952858651 * ___gameType_11;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.MasterServerGetHostData::gameName
	FsmString_t952858651 * ___gameName_12;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.MasterServerGetHostData::connectedPlayers
	FsmInt_t1596138449 * ___connectedPlayers_13;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.MasterServerGetHostData::playerLimit
	FsmInt_t1596138449 * ___playerLimit_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.MasterServerGetHostData::ipAddress
	FsmString_t952858651 * ___ipAddress_15;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.MasterServerGetHostData::port
	FsmInt_t1596138449 * ___port_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.MasterServerGetHostData::passwordProtected
	FsmBool_t1075959796 * ___passwordProtected_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.MasterServerGetHostData::comment
	FsmString_t952858651 * ___comment_18;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.MasterServerGetHostData::guid
	FsmString_t952858651 * ___guid_19;

public:
	inline static int32_t get_offset_of_hostIndex_9() { return static_cast<int32_t>(offsetof(MasterServerGetHostData_t2799670129, ___hostIndex_9)); }
	inline FsmInt_t1596138449 * get_hostIndex_9() const { return ___hostIndex_9; }
	inline FsmInt_t1596138449 ** get_address_of_hostIndex_9() { return &___hostIndex_9; }
	inline void set_hostIndex_9(FsmInt_t1596138449 * value)
	{
		___hostIndex_9 = value;
		Il2CppCodeGenWriteBarrier(&___hostIndex_9, value);
	}

	inline static int32_t get_offset_of_useNat_10() { return static_cast<int32_t>(offsetof(MasterServerGetHostData_t2799670129, ___useNat_10)); }
	inline FsmBool_t1075959796 * get_useNat_10() const { return ___useNat_10; }
	inline FsmBool_t1075959796 ** get_address_of_useNat_10() { return &___useNat_10; }
	inline void set_useNat_10(FsmBool_t1075959796 * value)
	{
		___useNat_10 = value;
		Il2CppCodeGenWriteBarrier(&___useNat_10, value);
	}

	inline static int32_t get_offset_of_gameType_11() { return static_cast<int32_t>(offsetof(MasterServerGetHostData_t2799670129, ___gameType_11)); }
	inline FsmString_t952858651 * get_gameType_11() const { return ___gameType_11; }
	inline FsmString_t952858651 ** get_address_of_gameType_11() { return &___gameType_11; }
	inline void set_gameType_11(FsmString_t952858651 * value)
	{
		___gameType_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameType_11, value);
	}

	inline static int32_t get_offset_of_gameName_12() { return static_cast<int32_t>(offsetof(MasterServerGetHostData_t2799670129, ___gameName_12)); }
	inline FsmString_t952858651 * get_gameName_12() const { return ___gameName_12; }
	inline FsmString_t952858651 ** get_address_of_gameName_12() { return &___gameName_12; }
	inline void set_gameName_12(FsmString_t952858651 * value)
	{
		___gameName_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameName_12, value);
	}

	inline static int32_t get_offset_of_connectedPlayers_13() { return static_cast<int32_t>(offsetof(MasterServerGetHostData_t2799670129, ___connectedPlayers_13)); }
	inline FsmInt_t1596138449 * get_connectedPlayers_13() const { return ___connectedPlayers_13; }
	inline FsmInt_t1596138449 ** get_address_of_connectedPlayers_13() { return &___connectedPlayers_13; }
	inline void set_connectedPlayers_13(FsmInt_t1596138449 * value)
	{
		___connectedPlayers_13 = value;
		Il2CppCodeGenWriteBarrier(&___connectedPlayers_13, value);
	}

	inline static int32_t get_offset_of_playerLimit_14() { return static_cast<int32_t>(offsetof(MasterServerGetHostData_t2799670129, ___playerLimit_14)); }
	inline FsmInt_t1596138449 * get_playerLimit_14() const { return ___playerLimit_14; }
	inline FsmInt_t1596138449 ** get_address_of_playerLimit_14() { return &___playerLimit_14; }
	inline void set_playerLimit_14(FsmInt_t1596138449 * value)
	{
		___playerLimit_14 = value;
		Il2CppCodeGenWriteBarrier(&___playerLimit_14, value);
	}

	inline static int32_t get_offset_of_ipAddress_15() { return static_cast<int32_t>(offsetof(MasterServerGetHostData_t2799670129, ___ipAddress_15)); }
	inline FsmString_t952858651 * get_ipAddress_15() const { return ___ipAddress_15; }
	inline FsmString_t952858651 ** get_address_of_ipAddress_15() { return &___ipAddress_15; }
	inline void set_ipAddress_15(FsmString_t952858651 * value)
	{
		___ipAddress_15 = value;
		Il2CppCodeGenWriteBarrier(&___ipAddress_15, value);
	}

	inline static int32_t get_offset_of_port_16() { return static_cast<int32_t>(offsetof(MasterServerGetHostData_t2799670129, ___port_16)); }
	inline FsmInt_t1596138449 * get_port_16() const { return ___port_16; }
	inline FsmInt_t1596138449 ** get_address_of_port_16() { return &___port_16; }
	inline void set_port_16(FsmInt_t1596138449 * value)
	{
		___port_16 = value;
		Il2CppCodeGenWriteBarrier(&___port_16, value);
	}

	inline static int32_t get_offset_of_passwordProtected_17() { return static_cast<int32_t>(offsetof(MasterServerGetHostData_t2799670129, ___passwordProtected_17)); }
	inline FsmBool_t1075959796 * get_passwordProtected_17() const { return ___passwordProtected_17; }
	inline FsmBool_t1075959796 ** get_address_of_passwordProtected_17() { return &___passwordProtected_17; }
	inline void set_passwordProtected_17(FsmBool_t1075959796 * value)
	{
		___passwordProtected_17 = value;
		Il2CppCodeGenWriteBarrier(&___passwordProtected_17, value);
	}

	inline static int32_t get_offset_of_comment_18() { return static_cast<int32_t>(offsetof(MasterServerGetHostData_t2799670129, ___comment_18)); }
	inline FsmString_t952858651 * get_comment_18() const { return ___comment_18; }
	inline FsmString_t952858651 ** get_address_of_comment_18() { return &___comment_18; }
	inline void set_comment_18(FsmString_t952858651 * value)
	{
		___comment_18 = value;
		Il2CppCodeGenWriteBarrier(&___comment_18, value);
	}

	inline static int32_t get_offset_of_guid_19() { return static_cast<int32_t>(offsetof(MasterServerGetHostData_t2799670129, ___guid_19)); }
	inline FsmString_t952858651 * get_guid_19() const { return ___guid_19; }
	inline FsmString_t952858651 ** get_address_of_guid_19() { return &___guid_19; }
	inline void set_guid_19(FsmString_t952858651 * value)
	{
		___guid_19 = value;
		Il2CppCodeGenWriteBarrier(&___guid_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
