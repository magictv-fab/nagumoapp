﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.IntCompare
struct IntCompare_t990531422;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.IntCompare::.ctor()
extern "C"  void IntCompare__ctor_m1865543368 (IntCompare_t990531422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntCompare::Reset()
extern "C"  void IntCompare_Reset_m3806943605 (IntCompare_t990531422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntCompare::OnEnter()
extern "C"  void IntCompare_OnEnter_m1177542815 (IntCompare_t990531422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntCompare::OnUpdate()
extern "C"  void IntCompare_OnUpdate_m1277648260 (IntCompare_t990531422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntCompare::DoIntCompare()
extern "C"  void IntCompare_DoIntCompare_m3168313117 (IntCompare_t990531422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Actions.IntCompare::ErrorCheck()
extern "C"  String_t* IntCompare_ErrorCheck_m2986004223 (IntCompare_t990531422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
