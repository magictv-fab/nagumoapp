﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.EAN13Reader
struct EAN13Reader_t2319538857;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "QRCode_ZXing_BarcodeFormat4201805817.h"

// System.Void ZXing.OneD.EAN13Reader::.ctor()
extern "C"  void EAN13Reader__ctor_m2175664138 (EAN13Reader_t2319538857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.EAN13Reader::decodeMiddle(ZXing.Common.BitArray,System.Int32[],System.Text.StringBuilder)
extern "C"  int32_t EAN13Reader_decodeMiddle_m4022977559 (EAN13Reader_t2319538857 * __this, BitArray_t4163851164 * ___row0, Int32U5BU5D_t3230847821* ___startRange1, StringBuilder_t243639308 * ___resultString2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.BarcodeFormat ZXing.OneD.EAN13Reader::get_BarcodeFormat()
extern "C"  int32_t EAN13Reader_get_BarcodeFormat_m1933739152 (EAN13Reader_t2319538857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.EAN13Reader::determineFirstDigit(System.Text.StringBuilder,System.Int32)
extern "C"  bool EAN13Reader_determineFirstDigit_m1872499921 (Il2CppObject * __this /* static, unused */, StringBuilder_t243639308 * ___resultString0, int32_t ___lgPatternFound1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.EAN13Reader::.cctor()
extern "C"  void EAN13Reader__cctor_m2538982627 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
