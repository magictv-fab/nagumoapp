﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t1820556512;
// System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair>
struct List_1_t486584943;
// System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedRow>
struct List_1_t2931017303;

#include "QRCode_ZXing_OneD_RSS_AbstractRSSReader1625773429.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.RSSExpandedReader
struct  RSSExpandedReader_t2976724276  : public AbstractRSSReader_t1625773429
{
public:
	// System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair> ZXing.OneD.RSS.Expanded.RSSExpandedReader::pairs
	List_1_t486584943 * ___pairs_16;
	// System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedRow> ZXing.OneD.RSS.Expanded.RSSExpandedReader::rows
	List_1_t2931017303 * ___rows_17;
	// System.Int32[] ZXing.OneD.RSS.Expanded.RSSExpandedReader::startEnd
	Int32U5BU5D_t3230847821* ___startEnd_18;
	// System.Boolean ZXing.OneD.RSS.Expanded.RSSExpandedReader::startFromEven
	bool ___startFromEven_19;

public:
	inline static int32_t get_offset_of_pairs_16() { return static_cast<int32_t>(offsetof(RSSExpandedReader_t2976724276, ___pairs_16)); }
	inline List_1_t486584943 * get_pairs_16() const { return ___pairs_16; }
	inline List_1_t486584943 ** get_address_of_pairs_16() { return &___pairs_16; }
	inline void set_pairs_16(List_1_t486584943 * value)
	{
		___pairs_16 = value;
		Il2CppCodeGenWriteBarrier(&___pairs_16, value);
	}

	inline static int32_t get_offset_of_rows_17() { return static_cast<int32_t>(offsetof(RSSExpandedReader_t2976724276, ___rows_17)); }
	inline List_1_t2931017303 * get_rows_17() const { return ___rows_17; }
	inline List_1_t2931017303 ** get_address_of_rows_17() { return &___rows_17; }
	inline void set_rows_17(List_1_t2931017303 * value)
	{
		___rows_17 = value;
		Il2CppCodeGenWriteBarrier(&___rows_17, value);
	}

	inline static int32_t get_offset_of_startEnd_18() { return static_cast<int32_t>(offsetof(RSSExpandedReader_t2976724276, ___startEnd_18)); }
	inline Int32U5BU5D_t3230847821* get_startEnd_18() const { return ___startEnd_18; }
	inline Int32U5BU5D_t3230847821** get_address_of_startEnd_18() { return &___startEnd_18; }
	inline void set_startEnd_18(Int32U5BU5D_t3230847821* value)
	{
		___startEnd_18 = value;
		Il2CppCodeGenWriteBarrier(&___startEnd_18, value);
	}

	inline static int32_t get_offset_of_startFromEven_19() { return static_cast<int32_t>(offsetof(RSSExpandedReader_t2976724276, ___startFromEven_19)); }
	inline bool get_startFromEven_19() const { return ___startFromEven_19; }
	inline bool* get_address_of_startFromEven_19() { return &___startFromEven_19; }
	inline void set_startFromEven_19(bool value)
	{
		___startFromEven_19 = value;
	}
};

struct RSSExpandedReader_t2976724276_StaticFields
{
public:
	// System.Int32[] ZXing.OneD.RSS.Expanded.RSSExpandedReader::SYMBOL_WIDEST
	Int32U5BU5D_t3230847821* ___SYMBOL_WIDEST_10;
	// System.Int32[] ZXing.OneD.RSS.Expanded.RSSExpandedReader::EVEN_TOTAL_SUBSET
	Int32U5BU5D_t3230847821* ___EVEN_TOTAL_SUBSET_11;
	// System.Int32[] ZXing.OneD.RSS.Expanded.RSSExpandedReader::GSUM
	Int32U5BU5D_t3230847821* ___GSUM_12;
	// System.Int32[][] ZXing.OneD.RSS.Expanded.RSSExpandedReader::FINDER_PATTERNS
	Int32U5BU5DU5BU5D_t1820556512* ___FINDER_PATTERNS_13;
	// System.Int32[][] ZXing.OneD.RSS.Expanded.RSSExpandedReader::WEIGHTS
	Int32U5BU5DU5BU5D_t1820556512* ___WEIGHTS_14;
	// System.Int32[][] ZXing.OneD.RSS.Expanded.RSSExpandedReader::FINDER_PATTERN_SEQUENCES
	Int32U5BU5DU5BU5D_t1820556512* ___FINDER_PATTERN_SEQUENCES_15;

public:
	inline static int32_t get_offset_of_SYMBOL_WIDEST_10() { return static_cast<int32_t>(offsetof(RSSExpandedReader_t2976724276_StaticFields, ___SYMBOL_WIDEST_10)); }
	inline Int32U5BU5D_t3230847821* get_SYMBOL_WIDEST_10() const { return ___SYMBOL_WIDEST_10; }
	inline Int32U5BU5D_t3230847821** get_address_of_SYMBOL_WIDEST_10() { return &___SYMBOL_WIDEST_10; }
	inline void set_SYMBOL_WIDEST_10(Int32U5BU5D_t3230847821* value)
	{
		___SYMBOL_WIDEST_10 = value;
		Il2CppCodeGenWriteBarrier(&___SYMBOL_WIDEST_10, value);
	}

	inline static int32_t get_offset_of_EVEN_TOTAL_SUBSET_11() { return static_cast<int32_t>(offsetof(RSSExpandedReader_t2976724276_StaticFields, ___EVEN_TOTAL_SUBSET_11)); }
	inline Int32U5BU5D_t3230847821* get_EVEN_TOTAL_SUBSET_11() const { return ___EVEN_TOTAL_SUBSET_11; }
	inline Int32U5BU5D_t3230847821** get_address_of_EVEN_TOTAL_SUBSET_11() { return &___EVEN_TOTAL_SUBSET_11; }
	inline void set_EVEN_TOTAL_SUBSET_11(Int32U5BU5D_t3230847821* value)
	{
		___EVEN_TOTAL_SUBSET_11 = value;
		Il2CppCodeGenWriteBarrier(&___EVEN_TOTAL_SUBSET_11, value);
	}

	inline static int32_t get_offset_of_GSUM_12() { return static_cast<int32_t>(offsetof(RSSExpandedReader_t2976724276_StaticFields, ___GSUM_12)); }
	inline Int32U5BU5D_t3230847821* get_GSUM_12() const { return ___GSUM_12; }
	inline Int32U5BU5D_t3230847821** get_address_of_GSUM_12() { return &___GSUM_12; }
	inline void set_GSUM_12(Int32U5BU5D_t3230847821* value)
	{
		___GSUM_12 = value;
		Il2CppCodeGenWriteBarrier(&___GSUM_12, value);
	}

	inline static int32_t get_offset_of_FINDER_PATTERNS_13() { return static_cast<int32_t>(offsetof(RSSExpandedReader_t2976724276_StaticFields, ___FINDER_PATTERNS_13)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_FINDER_PATTERNS_13() const { return ___FINDER_PATTERNS_13; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_FINDER_PATTERNS_13() { return &___FINDER_PATTERNS_13; }
	inline void set_FINDER_PATTERNS_13(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___FINDER_PATTERNS_13 = value;
		Il2CppCodeGenWriteBarrier(&___FINDER_PATTERNS_13, value);
	}

	inline static int32_t get_offset_of_WEIGHTS_14() { return static_cast<int32_t>(offsetof(RSSExpandedReader_t2976724276_StaticFields, ___WEIGHTS_14)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_WEIGHTS_14() const { return ___WEIGHTS_14; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_WEIGHTS_14() { return &___WEIGHTS_14; }
	inline void set_WEIGHTS_14(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___WEIGHTS_14 = value;
		Il2CppCodeGenWriteBarrier(&___WEIGHTS_14, value);
	}

	inline static int32_t get_offset_of_FINDER_PATTERN_SEQUENCES_15() { return static_cast<int32_t>(offsetof(RSSExpandedReader_t2976724276_StaticFields, ___FINDER_PATTERN_SEQUENCES_15)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_FINDER_PATTERN_SEQUENCES_15() const { return ___FINDER_PATTERN_SEQUENCES_15; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_FINDER_PATTERN_SEQUENCES_15() { return &___FINDER_PATTERN_SEQUENCES_15; }
	inline void set_FINDER_PATTERN_SEQUENCES_15(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___FINDER_PATTERN_SEQUENCES_15 = value;
		Il2CppCodeGenWriteBarrier(&___FINDER_PATTERN_SEQUENCES_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
