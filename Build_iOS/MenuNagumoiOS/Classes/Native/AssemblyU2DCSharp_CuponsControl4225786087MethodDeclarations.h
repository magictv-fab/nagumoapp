﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CuponsControl
struct CuponsControl_t4225786087;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void CuponsControl::.ctor()
extern "C"  void CuponsControl__ctor_m1066079332 (CuponsControl_t4225786087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CuponsControl::Start()
extern "C"  Il2CppObject * CuponsControl_Start_m1161217452 (CuponsControl_t4225786087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CuponsControl::UpdateDt()
extern "C"  Il2CppObject * CuponsControl_UpdateDt_m3492389009 (CuponsControl_t4225786087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CuponsControl::Base64Encode(System.String)
extern "C"  String_t* CuponsControl_Base64Encode_m612403488 (Il2CppObject * __this /* static, unused */, String_t* ___plainText0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
