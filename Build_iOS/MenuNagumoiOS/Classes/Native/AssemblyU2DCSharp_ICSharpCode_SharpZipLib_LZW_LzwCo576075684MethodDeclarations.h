﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.LZW.LzwConstants
struct LzwConstants_t576075684;

#include "codegen/il2cpp-codegen.h"

// System.Void ICSharpCode.SharpZipLib.LZW.LzwConstants::.ctor()
extern "C"  void LzwConstants__ctor_m611581151 (LzwConstants_t576075684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
