﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t3134621005;
// CEP
struct CEP_t66606;
// System.Object
struct Il2CppObject;
// Cadastro
struct Cadastro_t3948724313;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cadastro/<IBuscaCEPAPI>c__Iterator48
struct  U3CIBuscaCEPAPIU3Ec__Iterator48_t2377846270  : public Il2CppObject
{
public:
	// System.String Cadastro/<IBuscaCEPAPI>c__Iterator48::cep
	String_t* ___cep_0;
	// UnityEngine.WWW Cadastro/<IBuscaCEPAPI>c__Iterator48::<wwwCep>__0
	WWW_t3134621005 * ___U3CwwwCepU3E__0_1;
	// CEP Cadastro/<IBuscaCEPAPI>c__Iterator48::<cepAPI>__1
	CEP_t66606 * ___U3CcepAPIU3E__1_2;
	// System.Int32 Cadastro/<IBuscaCEPAPI>c__Iterator48::$PC
	int32_t ___U24PC_3;
	// System.Object Cadastro/<IBuscaCEPAPI>c__Iterator48::$current
	Il2CppObject * ___U24current_4;
	// System.String Cadastro/<IBuscaCEPAPI>c__Iterator48::<$>cep
	String_t* ___U3CU24U3Ecep_5;
	// Cadastro Cadastro/<IBuscaCEPAPI>c__Iterator48::<>f__this
	Cadastro_t3948724313 * ___U3CU3Ef__this_6;

public:
	inline static int32_t get_offset_of_cep_0() { return static_cast<int32_t>(offsetof(U3CIBuscaCEPAPIU3Ec__Iterator48_t2377846270, ___cep_0)); }
	inline String_t* get_cep_0() const { return ___cep_0; }
	inline String_t** get_address_of_cep_0() { return &___cep_0; }
	inline void set_cep_0(String_t* value)
	{
		___cep_0 = value;
		Il2CppCodeGenWriteBarrier(&___cep_0, value);
	}

	inline static int32_t get_offset_of_U3CwwwCepU3E__0_1() { return static_cast<int32_t>(offsetof(U3CIBuscaCEPAPIU3Ec__Iterator48_t2377846270, ___U3CwwwCepU3E__0_1)); }
	inline WWW_t3134621005 * get_U3CwwwCepU3E__0_1() const { return ___U3CwwwCepU3E__0_1; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwCepU3E__0_1() { return &___U3CwwwCepU3E__0_1; }
	inline void set_U3CwwwCepU3E__0_1(WWW_t3134621005 * value)
	{
		___U3CwwwCepU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwCepU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CcepAPIU3E__1_2() { return static_cast<int32_t>(offsetof(U3CIBuscaCEPAPIU3Ec__Iterator48_t2377846270, ___U3CcepAPIU3E__1_2)); }
	inline CEP_t66606 * get_U3CcepAPIU3E__1_2() const { return ___U3CcepAPIU3E__1_2; }
	inline CEP_t66606 ** get_address_of_U3CcepAPIU3E__1_2() { return &___U3CcepAPIU3E__1_2; }
	inline void set_U3CcepAPIU3E__1_2(CEP_t66606 * value)
	{
		___U3CcepAPIU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcepAPIU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CIBuscaCEPAPIU3Ec__Iterator48_t2377846270, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CIBuscaCEPAPIU3Ec__Iterator48_t2377846270, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ecep_5() { return static_cast<int32_t>(offsetof(U3CIBuscaCEPAPIU3Ec__Iterator48_t2377846270, ___U3CU24U3Ecep_5)); }
	inline String_t* get_U3CU24U3Ecep_5() const { return ___U3CU24U3Ecep_5; }
	inline String_t** get_address_of_U3CU24U3Ecep_5() { return &___U3CU24U3Ecep_5; }
	inline void set_U3CU24U3Ecep_5(String_t* value)
	{
		___U3CU24U3Ecep_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ecep_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_6() { return static_cast<int32_t>(offsetof(U3CIBuscaCEPAPIU3Ec__Iterator48_t2377846270, ___U3CU3Ef__this_6)); }
	inline Cadastro_t3948724313 * get_U3CU3Ef__this_6() const { return ___U3CU3Ef__this_6; }
	inline Cadastro_t3948724313 ** get_address_of_U3CU3Ef__this_6() { return &___U3CU3Ef__this_6; }
	inline void set_U3CU3Ef__this_6(Cadastro_t3948724313 * value)
	{
		___U3CU3Ef__this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
