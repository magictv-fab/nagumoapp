﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.CanvasGroup
struct CanvasGroup_t3702418109;
// UnityEngine.UI.Image
struct Image_t538875265;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DemoScript
struct  DemoScript_t545284270  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean DemoScript::hideGUI
	bool ___hideGUI_2;
	// UnityEngine.Texture2D DemoScript::texture
	Texture2D_t3884108195 * ___texture_3;
	// UnityEngine.UI.Text DemoScript::console
	Text_t9039225 * ___console_4;
	// UnityEngine.CanvasGroup DemoScript::ui
	CanvasGroup_t3702418109 * ___ui_5;
	// UnityEngine.UI.Image DemoScript::screenshot
	Image_t538875265 * ___screenshot_6;

public:
	inline static int32_t get_offset_of_hideGUI_2() { return static_cast<int32_t>(offsetof(DemoScript_t545284270, ___hideGUI_2)); }
	inline bool get_hideGUI_2() const { return ___hideGUI_2; }
	inline bool* get_address_of_hideGUI_2() { return &___hideGUI_2; }
	inline void set_hideGUI_2(bool value)
	{
		___hideGUI_2 = value;
	}

	inline static int32_t get_offset_of_texture_3() { return static_cast<int32_t>(offsetof(DemoScript_t545284270, ___texture_3)); }
	inline Texture2D_t3884108195 * get_texture_3() const { return ___texture_3; }
	inline Texture2D_t3884108195 ** get_address_of_texture_3() { return &___texture_3; }
	inline void set_texture_3(Texture2D_t3884108195 * value)
	{
		___texture_3 = value;
		Il2CppCodeGenWriteBarrier(&___texture_3, value);
	}

	inline static int32_t get_offset_of_console_4() { return static_cast<int32_t>(offsetof(DemoScript_t545284270, ___console_4)); }
	inline Text_t9039225 * get_console_4() const { return ___console_4; }
	inline Text_t9039225 ** get_address_of_console_4() { return &___console_4; }
	inline void set_console_4(Text_t9039225 * value)
	{
		___console_4 = value;
		Il2CppCodeGenWriteBarrier(&___console_4, value);
	}

	inline static int32_t get_offset_of_ui_5() { return static_cast<int32_t>(offsetof(DemoScript_t545284270, ___ui_5)); }
	inline CanvasGroup_t3702418109 * get_ui_5() const { return ___ui_5; }
	inline CanvasGroup_t3702418109 ** get_address_of_ui_5() { return &___ui_5; }
	inline void set_ui_5(CanvasGroup_t3702418109 * value)
	{
		___ui_5 = value;
		Il2CppCodeGenWriteBarrier(&___ui_5, value);
	}

	inline static int32_t get_offset_of_screenshot_6() { return static_cast<int32_t>(offsetof(DemoScript_t545284270, ___screenshot_6)); }
	inline Image_t538875265 * get_screenshot_6() const { return ___screenshot_6; }
	inline Image_t538875265 ** get_address_of_screenshot_6() { return &___screenshot_6; }
	inline void set_screenshot_6(Image_t538875265 * value)
	{
		___screenshot_6 = value;
		Il2CppCodeGenWriteBarrier(&___screenshot_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
