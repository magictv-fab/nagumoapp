﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;

#include "QRCode_ZXing_Common_GlobalHistogramBinarizer1656947045.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.HybridBinarizer
struct  HybridBinarizer_t3517076856  : public GlobalHistogramBinarizer_t1656947045
{
public:
	// ZXing.Common.BitMatrix ZXing.Common.HybridBinarizer::matrix
	BitMatrix_t1058711404 * ___matrix_4;

public:
	inline static int32_t get_offset_of_matrix_4() { return static_cast<int32_t>(offsetof(HybridBinarizer_t3517076856, ___matrix_4)); }
	inline BitMatrix_t1058711404 * get_matrix_4() const { return ___matrix_4; }
	inline BitMatrix_t1058711404 ** get_address_of_matrix_4() { return &___matrix_4; }
	inline void set_matrix_4(BitMatrix_t1058711404 * value)
	{
		___matrix_4 = value;
		Il2CppCodeGenWriteBarrier(&___matrix_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
