﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.CreateObject
struct CreateObject_t687322499;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.CreateObject::.ctor()
extern "C"  void CreateObject__ctor_m251100227 (CreateObject_t687322499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CreateObject::Reset()
extern "C"  void CreateObject_Reset_m2192500464 (CreateObject_t687322499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CreateObject::OnEnter()
extern "C"  void CreateObject_OnEnter_m180878170 (CreateObject_t687322499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
