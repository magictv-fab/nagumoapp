﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.display.RotateObject/OnRotateComplete
struct OnRotateComplete_t2396770826;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.display.RotateObject
struct  RotateObject_t3491987816  : public MonoBehaviour_t667441552
{
public:
	// ARM.display.RotateObject/OnRotateComplete ARM.display.RotateObject::_onRotateComplete
	OnRotateComplete_t2396770826 * ____onRotateComplete_2;
	// System.Single ARM.display.RotateObject::speedy
	float ___speedy_3;
	// UnityEngine.Vector3 ARM.display.RotateObject::_currentRotation
	Vector3_t4282066566  ____currentRotation_4;

public:
	inline static int32_t get_offset_of__onRotateComplete_2() { return static_cast<int32_t>(offsetof(RotateObject_t3491987816, ____onRotateComplete_2)); }
	inline OnRotateComplete_t2396770826 * get__onRotateComplete_2() const { return ____onRotateComplete_2; }
	inline OnRotateComplete_t2396770826 ** get_address_of__onRotateComplete_2() { return &____onRotateComplete_2; }
	inline void set__onRotateComplete_2(OnRotateComplete_t2396770826 * value)
	{
		____onRotateComplete_2 = value;
		Il2CppCodeGenWriteBarrier(&____onRotateComplete_2, value);
	}

	inline static int32_t get_offset_of_speedy_3() { return static_cast<int32_t>(offsetof(RotateObject_t3491987816, ___speedy_3)); }
	inline float get_speedy_3() const { return ___speedy_3; }
	inline float* get_address_of_speedy_3() { return &___speedy_3; }
	inline void set_speedy_3(float value)
	{
		___speedy_3 = value;
	}

	inline static int32_t get_offset_of__currentRotation_4() { return static_cast<int32_t>(offsetof(RotateObject_t3491987816, ____currentRotation_4)); }
	inline Vector3_t4282066566  get__currentRotation_4() const { return ____currentRotation_4; }
	inline Vector3_t4282066566 * get_address_of__currentRotation_4() { return &____currentRotation_4; }
	inline void set__currentRotation_4(Vector3_t4282066566  value)
	{
		____currentRotation_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
