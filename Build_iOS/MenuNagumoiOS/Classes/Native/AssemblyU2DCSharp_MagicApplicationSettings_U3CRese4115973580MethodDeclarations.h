﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicApplicationSettings/<ResetHard>c__AnonStorey95
struct U3CResetHardU3Ec__AnonStorey95_t4115973580;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MagicApplicationSettings/<ResetHard>c__AnonStorey95::.ctor()
extern "C"  void U3CResetHardU3Ec__AnonStorey95__ctor_m1689744159 (U3CResetHardU3Ec__AnonStorey95_t4115973580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicApplicationSettings/<ResetHard>c__AnonStorey95::<>m__22(System.String)
extern "C"  bool U3CResetHardU3Ec__AnonStorey95_U3CU3Em__22_m1781020774 (U3CResetHardU3Ec__AnonStorey95_t4115973580 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
