﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21845333178MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3689347932(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2599727649 *, int32_t, OnChangeToAnStateEventHandler_t630243546 *, const MethodInfo*))KeyValuePair_2__ctor_m726640746_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::get_Key()
#define KeyValuePair_2_get_Key_m1972457612(__this, method) ((  int32_t (*) (KeyValuePair_2_t2599727649 *, const MethodInfo*))KeyValuePair_2_get_Key_m1096840254_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3210196301(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2599727649 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m2352912255_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::get_Value()
#define KeyValuePair_2_get_Value_m2415666224(__this, method) ((  OnChangeToAnStateEventHandler_t630243546 * (*) (KeyValuePair_2_t2599727649 *, const MethodInfo*))KeyValuePair_2_get_Value_m3223841534_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m722311885(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2599727649 *, OnChangeToAnStateEventHandler_t630243546 *, const MethodInfo*))KeyValuePair_2_set_Value_m2289308671_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::ToString()
#define KeyValuePair_2_ToString_m3263444443(__this, method) ((  String_t* (*) (KeyValuePair_2_t2599727649 *, const MethodInfo*))KeyValuePair_2_ToString_m469421891_gshared)(__this, method)
