﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t3871136040;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// UnityEngine.Animator
struct Animator_t2776330603;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "UnityEngine_UnityEngine_AvatarTarget2373143374.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AnimatorMatchTarget
struct  AnimatorMatchTarget_t2687678941  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AnimatorMatchTarget::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// UnityEngine.AvatarTarget HutongGames.PlayMaker.Actions.AnimatorMatchTarget::bodyPart
	int32_t ___bodyPart_10;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.AnimatorMatchTarget::target
	FsmGameObject_t1697147867 * ___target_11;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.AnimatorMatchTarget::targetPosition
	FsmVector3_t533912882 * ___targetPosition_12;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.AnimatorMatchTarget::targetRotation
	FsmQuaternion_t3871136040 * ___targetRotation_13;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.AnimatorMatchTarget::positionWeight
	FsmVector3_t533912882 * ___positionWeight_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AnimatorMatchTarget::rotationWeight
	FsmFloat_t2134102846 * ___rotationWeight_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AnimatorMatchTarget::startNormalizedTime
	FsmFloat_t2134102846 * ___startNormalizedTime_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AnimatorMatchTarget::targetNormalizedTime
	FsmFloat_t2134102846 * ___targetNormalizedTime_17;
	// System.Boolean HutongGames.PlayMaker.Actions.AnimatorMatchTarget::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.AnimatorMatchTarget::_animator
	Animator_t2776330603 * ____animator_19;
	// UnityEngine.Transform HutongGames.PlayMaker.Actions.AnimatorMatchTarget::_transform
	Transform_t1659122786 * ____transform_20;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(AnimatorMatchTarget_t2687678941, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_bodyPart_10() { return static_cast<int32_t>(offsetof(AnimatorMatchTarget_t2687678941, ___bodyPart_10)); }
	inline int32_t get_bodyPart_10() const { return ___bodyPart_10; }
	inline int32_t* get_address_of_bodyPart_10() { return &___bodyPart_10; }
	inline void set_bodyPart_10(int32_t value)
	{
		___bodyPart_10 = value;
	}

	inline static int32_t get_offset_of_target_11() { return static_cast<int32_t>(offsetof(AnimatorMatchTarget_t2687678941, ___target_11)); }
	inline FsmGameObject_t1697147867 * get_target_11() const { return ___target_11; }
	inline FsmGameObject_t1697147867 ** get_address_of_target_11() { return &___target_11; }
	inline void set_target_11(FsmGameObject_t1697147867 * value)
	{
		___target_11 = value;
		Il2CppCodeGenWriteBarrier(&___target_11, value);
	}

	inline static int32_t get_offset_of_targetPosition_12() { return static_cast<int32_t>(offsetof(AnimatorMatchTarget_t2687678941, ___targetPosition_12)); }
	inline FsmVector3_t533912882 * get_targetPosition_12() const { return ___targetPosition_12; }
	inline FsmVector3_t533912882 ** get_address_of_targetPosition_12() { return &___targetPosition_12; }
	inline void set_targetPosition_12(FsmVector3_t533912882 * value)
	{
		___targetPosition_12 = value;
		Il2CppCodeGenWriteBarrier(&___targetPosition_12, value);
	}

	inline static int32_t get_offset_of_targetRotation_13() { return static_cast<int32_t>(offsetof(AnimatorMatchTarget_t2687678941, ___targetRotation_13)); }
	inline FsmQuaternion_t3871136040 * get_targetRotation_13() const { return ___targetRotation_13; }
	inline FsmQuaternion_t3871136040 ** get_address_of_targetRotation_13() { return &___targetRotation_13; }
	inline void set_targetRotation_13(FsmQuaternion_t3871136040 * value)
	{
		___targetRotation_13 = value;
		Il2CppCodeGenWriteBarrier(&___targetRotation_13, value);
	}

	inline static int32_t get_offset_of_positionWeight_14() { return static_cast<int32_t>(offsetof(AnimatorMatchTarget_t2687678941, ___positionWeight_14)); }
	inline FsmVector3_t533912882 * get_positionWeight_14() const { return ___positionWeight_14; }
	inline FsmVector3_t533912882 ** get_address_of_positionWeight_14() { return &___positionWeight_14; }
	inline void set_positionWeight_14(FsmVector3_t533912882 * value)
	{
		___positionWeight_14 = value;
		Il2CppCodeGenWriteBarrier(&___positionWeight_14, value);
	}

	inline static int32_t get_offset_of_rotationWeight_15() { return static_cast<int32_t>(offsetof(AnimatorMatchTarget_t2687678941, ___rotationWeight_15)); }
	inline FsmFloat_t2134102846 * get_rotationWeight_15() const { return ___rotationWeight_15; }
	inline FsmFloat_t2134102846 ** get_address_of_rotationWeight_15() { return &___rotationWeight_15; }
	inline void set_rotationWeight_15(FsmFloat_t2134102846 * value)
	{
		___rotationWeight_15 = value;
		Il2CppCodeGenWriteBarrier(&___rotationWeight_15, value);
	}

	inline static int32_t get_offset_of_startNormalizedTime_16() { return static_cast<int32_t>(offsetof(AnimatorMatchTarget_t2687678941, ___startNormalizedTime_16)); }
	inline FsmFloat_t2134102846 * get_startNormalizedTime_16() const { return ___startNormalizedTime_16; }
	inline FsmFloat_t2134102846 ** get_address_of_startNormalizedTime_16() { return &___startNormalizedTime_16; }
	inline void set_startNormalizedTime_16(FsmFloat_t2134102846 * value)
	{
		___startNormalizedTime_16 = value;
		Il2CppCodeGenWriteBarrier(&___startNormalizedTime_16, value);
	}

	inline static int32_t get_offset_of_targetNormalizedTime_17() { return static_cast<int32_t>(offsetof(AnimatorMatchTarget_t2687678941, ___targetNormalizedTime_17)); }
	inline FsmFloat_t2134102846 * get_targetNormalizedTime_17() const { return ___targetNormalizedTime_17; }
	inline FsmFloat_t2134102846 ** get_address_of_targetNormalizedTime_17() { return &___targetNormalizedTime_17; }
	inline void set_targetNormalizedTime_17(FsmFloat_t2134102846 * value)
	{
		___targetNormalizedTime_17 = value;
		Il2CppCodeGenWriteBarrier(&___targetNormalizedTime_17, value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(AnimatorMatchTarget_t2687678941, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of__animator_19() { return static_cast<int32_t>(offsetof(AnimatorMatchTarget_t2687678941, ____animator_19)); }
	inline Animator_t2776330603 * get__animator_19() const { return ____animator_19; }
	inline Animator_t2776330603 ** get_address_of__animator_19() { return &____animator_19; }
	inline void set__animator_19(Animator_t2776330603 * value)
	{
		____animator_19 = value;
		Il2CppCodeGenWriteBarrier(&____animator_19, value);
	}

	inline static int32_t get_offset_of__transform_20() { return static_cast<int32_t>(offsetof(AnimatorMatchTarget_t2687678941, ____transform_20)); }
	inline Transform_t1659122786 * get__transform_20() const { return ____transform_20; }
	inline Transform_t1659122786 ** get_address_of__transform_20() { return &____transform_20; }
	inline void set__transform_20(Transform_t1659122786 * value)
	{
		____transform_20 = value;
		Il2CppCodeGenWriteBarrier(&____transform_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
