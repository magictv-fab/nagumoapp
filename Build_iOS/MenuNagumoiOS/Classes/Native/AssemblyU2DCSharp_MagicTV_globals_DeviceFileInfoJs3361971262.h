﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.globals.DeviceFileInfoJson/<filterUrlFromId>c__AnonStorey9D
struct  U3CfilterUrlFromIdU3Ec__AnonStorey9D_t3361971262  : public Il2CppObject
{
public:
	// System.String MagicTV.globals.DeviceFileInfoJson/<filterUrlFromId>c__AnonStorey9D::id
	String_t* ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CfilterUrlFromIdU3Ec__AnonStorey9D_t3361971262, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier(&___id_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
