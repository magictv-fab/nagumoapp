﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.globals.events.ScreenEvents
struct ScreenEvents_t2706497391;
// System.String
struct String_t;
// MagicTV.globals.events.ScreenEvents/OnVoidEventHandler
struct OnVoidEventHandler_t2512456021;
// MagicTV.globals.events.ScreenEvents/OnStringEventHandler
struct OnStringEventHandler_t2329146386;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_MagicTV_globals_events_ScreenEve2512456021.h"
#include "AssemblyU2DCSharp_MagicTV_globals_events_ScreenEve2329146386.h"

// System.Void MagicTV.globals.events.ScreenEvents::.ctor()
extern "C"  void ScreenEvents__ctor_m2905149166 (ScreenEvents_t2706497391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ScreenEvents::SendAlert(System.String,System.String,System.String,MagicTV.globals.events.ScreenEvents/OnVoidEventHandler)
extern "C"  void ScreenEvents_SendAlert_m1561784727 (ScreenEvents_t2706497391 * __this, String_t* ___message0, String_t* ___title1, String_t* ___buttonLabel2, OnVoidEventHandler_t2512456021 * ___onCloseEventHandler3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ScreenEvents::OnAlertClose()
extern "C"  void ScreenEvents_OnAlertClose_m1452113425 (ScreenEvents_t2706497391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ScreenEvents::showCurrentCaption()
extern "C"  void ScreenEvents_showCurrentCaption_m124462016 (ScreenEvents_t2706497391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ScreenEvents::SetCaption(System.String)
extern "C"  void ScreenEvents_SetCaption_m2991265352 (ScreenEvents_t2706497391 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ScreenEvents::ResetCaption()
extern "C"  void ScreenEvents_ResetCaption_m3284985709 (ScreenEvents_t2706497391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ScreenEvents::ConfigCaption(System.Int32,System.Boolean)
extern "C"  void ScreenEvents_ConfigCaption_m4105725884 (ScreenEvents_t2706497391 * __this, int32_t ___size0, bool ___autoSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ScreenEvents::ResetConfigCaption()
extern "C"  void ScreenEvents_ResetConfigCaption_m550498763 (ScreenEvents_t2706497391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ScreenEvents::RaiseShowHelpPage()
extern "C"  void ScreenEvents_RaiseShowHelpPage_m3921494757 (ScreenEvents_t2706497391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ScreenEvents::AddShowPageCalledEventHandler(MagicTV.globals.events.ScreenEvents/OnVoidEventHandler)
extern "C"  void ScreenEvents_AddShowPageCalledEventHandler_m862387025 (ScreenEvents_t2706497391 * __this, OnVoidEventHandler_t2512456021 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ScreenEvents::RemoveShowPageCalledEventHandler(MagicTV.globals.events.ScreenEvents/OnVoidEventHandler)
extern "C"  void ScreenEvents_RemoveShowPageCalledEventHandler_m513272062 (ScreenEvents_t2706497391 * __this, OnVoidEventHandler_t2512456021 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ScreenEvents::Awake()
extern "C"  void ScreenEvents_Awake_m3142754385 (ScreenEvents_t2706497391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ScreenEvents::AddPrintScreenCompletedEventHandler(MagicTV.globals.events.ScreenEvents/OnStringEventHandler)
extern "C"  void ScreenEvents_AddPrintScreenCompletedEventHandler_m1837129789 (ScreenEvents_t2706497391 * __this, OnStringEventHandler_t2329146386 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ScreenEvents::RemovePrintScreenCompletedEventHandler(MagicTV.globals.events.ScreenEvents/OnStringEventHandler)
extern "C"  void ScreenEvents_RemovePrintScreenCompletedEventHandler_m3048427754 (ScreenEvents_t2706497391 * __this, OnStringEventHandler_t2329146386 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ScreenEvents::RaisePrintScreenCompleted(System.String)
extern "C"  void ScreenEvents_RaisePrintScreenCompleted_m1474795352 (ScreenEvents_t2706497391 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ScreenEvents::RaiseShowCloseButton()
extern "C"  void ScreenEvents_RaiseShowCloseButton_m1077637335 (ScreenEvents_t2706497391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ScreenEvents::RaiseHideCloseButton()
extern "C"  void ScreenEvents_RaiseHideCloseButton_m3967485618 (ScreenEvents_t2706497391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ScreenEvents::AddPrintScreenCalledEventHandler(MagicTV.globals.events.ScreenEvents/OnVoidEventHandler)
extern "C"  void ScreenEvents_AddPrintScreenCalledEventHandler_m748094790 (ScreenEvents_t2706497391 * __this, OnVoidEventHandler_t2512456021 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ScreenEvents::RemovePrintScreenCalledEventHandler(MagicTV.globals.events.ScreenEvents/OnVoidEventHandler)
extern "C"  void ScreenEvents_RemovePrintScreenCalledEventHandler_m2675022969 (ScreenEvents_t2706497391 * __this, OnVoidEventHandler_t2512456021 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ScreenEvents::RaisePrintScreenCalled()
extern "C"  void ScreenEvents_RaisePrintScreenCalled_m68701856 (ScreenEvents_t2706497391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ScreenEvents::AddSetPINEventHandler(MagicTV.globals.events.ScreenEvents/OnStringEventHandler)
extern "C"  void ScreenEvents_AddSetPINEventHandler_m372099454 (ScreenEvents_t2706497391 * __this, OnStringEventHandler_t2329146386 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ScreenEvents::RemoveSetPINEventHandler(MagicTV.globals.events.ScreenEvents/OnStringEventHandler)
extern "C"  void ScreenEvents_RemoveSetPINEventHandler_m1029291243 (ScreenEvents_t2706497391 * __this, OnStringEventHandler_t2329146386 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ScreenEvents::RaiseSetPINEventHandler(System.String)
extern "C"  void ScreenEvents_RaiseSetPINEventHandler_m863570311 (ScreenEvents_t2706497391 * __this, String_t* ___pin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ScreenEvents::AddGoBackEventHandler(MagicTV.globals.events.ScreenEvents/OnVoidEventHandler)
extern "C"  void ScreenEvents_AddGoBackEventHandler_m2155346039 (ScreenEvents_t2706497391 * __this, OnVoidEventHandler_t2512456021 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ScreenEvents::RemoveGoBackEventHandler(MagicTV.globals.events.ScreenEvents/OnVoidEventHandler)
extern "C"  void ScreenEvents_RemoveGoBackEventHandler_m1458823972 (ScreenEvents_t2706497391 * __this, OnVoidEventHandler_t2512456021 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ScreenEvents::RaiseGoBackCalled()
extern "C"  void ScreenEvents_RaiseGoBackCalled_m1681470948 (ScreenEvents_t2706497391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.globals.events.ScreenEvents MagicTV.globals.events.ScreenEvents::GetInstance()
extern "C"  ScreenEvents_t2706497391 * ScreenEvents_GetInstance_m3030442669 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
