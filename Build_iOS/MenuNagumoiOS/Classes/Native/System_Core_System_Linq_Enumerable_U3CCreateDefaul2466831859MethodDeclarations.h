﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Enumerable/<CreateDefaultIfEmptyIterator>c__Iterator2`1<System.Object>
struct U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_t2466831859;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Linq.Enumerable/<CreateDefaultIfEmptyIterator>c__Iterator2`1<System.Object>::.ctor()
extern "C"  void U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1__ctor_m3831142946_gshared (U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_t2466831859 * __this, const MethodInfo* method);
#define U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1__ctor_m3831142946(__this, method) ((  void (*) (U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_t2466831859 *, const MethodInfo*))U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1__ctor_m3831142946_gshared)(__this, method)
// TSource System.Linq.Enumerable/<CreateDefaultIfEmptyIterator>c__Iterator2`1<System.Object>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  Il2CppObject * U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m2440793963_gshared (U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_t2466831859 * __this, const MethodInfo* method);
#define U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m2440793963(__this, method) ((  Il2CppObject * (*) (U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_t2466831859 *, const MethodInfo*))U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m2440793963_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateDefaultIfEmptyIterator>c__Iterator2`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_System_Collections_IEnumerator_get_Current_m2625862990_gshared (U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_t2466831859 * __this, const MethodInfo* method);
#define U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_System_Collections_IEnumerator_get_Current_m2625862990(__this, method) ((  Il2CppObject * (*) (U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_t2466831859 *, const MethodInfo*))U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_System_Collections_IEnumerator_get_Current_m2625862990_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateDefaultIfEmptyIterator>c__Iterator2`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_System_Collections_IEnumerable_GetEnumerator_m2123007599_gshared (U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_t2466831859 * __this, const MethodInfo* method);
#define U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_System_Collections_IEnumerable_GetEnumerator_m2123007599(__this, method) ((  Il2CppObject * (*) (U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_t2466831859 *, const MethodInfo*))U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_System_Collections_IEnumerable_GetEnumerator_m2123007599_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateDefaultIfEmptyIterator>c__Iterator2`1<System.Object>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m3651343486_gshared (U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_t2466831859 * __this, const MethodInfo* method);
#define U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m3651343486(__this, method) ((  Il2CppObject* (*) (U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_t2466831859 *, const MethodInfo*))U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m3651343486_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateDefaultIfEmptyIterator>c__Iterator2`1<System.Object>::MoveNext()
extern "C"  bool U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_MoveNext_m2306535258_gshared (U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_t2466831859 * __this, const MethodInfo* method);
#define U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_MoveNext_m2306535258(__this, method) ((  bool (*) (U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_t2466831859 *, const MethodInfo*))U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_MoveNext_m2306535258_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateDefaultIfEmptyIterator>c__Iterator2`1<System.Object>::Dispose()
extern "C"  void U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_Dispose_m839681503_gshared (U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_t2466831859 * __this, const MethodInfo* method);
#define U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_Dispose_m839681503(__this, method) ((  void (*) (U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_t2466831859 *, const MethodInfo*))U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_Dispose_m839681503_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateDefaultIfEmptyIterator>c__Iterator2`1<System.Object>::Reset()
extern "C"  void U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_Reset_m1477575887_gshared (U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_t2466831859 * __this, const MethodInfo* method);
#define U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_Reset_m1477575887(__this, method) ((  void (*) (U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_t2466831859 *, const MethodInfo*))U3CCreateDefaultIfEmptyIteratorU3Ec__Iterator2_1_Reset_m1477575887_gshared)(__this, method)
