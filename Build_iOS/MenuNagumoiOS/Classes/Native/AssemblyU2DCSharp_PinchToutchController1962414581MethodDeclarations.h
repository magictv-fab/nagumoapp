﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PinchToutchController
struct PinchToutchController_t1962414581;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void PinchToutchController::.ctor()
extern "C"  void PinchToutchController__ctor_m2419804950 (PinchToutchController_t1962414581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PinchToutchController::Start()
extern "C"  void PinchToutchController_Start_m1366942742 (PinchToutchController_t1962414581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PinchToutchController::LateUpdate()
extern "C"  void PinchToutchController_LateUpdate_m4135288061 (PinchToutchController_t1962414581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PinchToutchController::Update()
extern "C"  void PinchToutchController_Update_m3726371511 (PinchToutchController_t1962414581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PinchToutchController::HasCameraToFollow()
extern "C"  bool PinchToutchController_HasCameraToFollow_m3406347315 (PinchToutchController_t1962414581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PinchToutchController::CameraFollow()
extern "C"  void PinchToutchController_CameraFollow_m3326685220 (PinchToutchController_t1962414581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single PinchToutchController::GetPercentage(System.Single,System.Single)
extern "C"  float PinchToutchController_GetPercentage_m3889583040 (PinchToutchController_t1962414581 * __this, float ___wholeNumber0, float ___currentNumber1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single PinchToutchController::Angle(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  float PinchToutchController_Angle_m1500013755 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___pos10, Vector2_t4282066565  ___pos21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PinchToutchController::CheckScaleTolerance()
extern "C"  void PinchToutchController_CheckScaleTolerance_m716382527 (PinchToutchController_t1962414581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PinchToutchController::Rotate()
extern "C"  void PinchToutchController_Rotate_m2363470537 (PinchToutchController_t1962414581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PinchToutchController::OnScroll(System.Single)
extern "C"  void PinchToutchController_OnScroll_m240764017 (PinchToutchController_t1962414581 * __this, float ___scrollValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PinchToutchController::Scale()
extern "C"  void PinchToutchController_Scale_m880054014 (PinchToutchController_t1962414581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PinchToutchController::Move()
extern "C"  void PinchToutchController_Move_m1115239167 (PinchToutchController_t1962414581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 PinchToutchController::GetLastTouch()
extern "C"  Vector2_t4282066565  PinchToutchController_GetLastTouch_m224666822 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PinchToutchController::OnMouseDown()
extern "C"  void PinchToutchController_OnMouseDown_m2816975804 (PinchToutchController_t1962414581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PinchToutchController::OnMouseUp()
extern "C"  void PinchToutchController_OnMouseUp_m1496171957 (PinchToutchController_t1962414581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PinchToutchController::OnSelect()
extern "C"  void PinchToutchController_OnSelect_m3299127785 (PinchToutchController_t1962414581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
