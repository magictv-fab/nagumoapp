﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PedidoData
struct PedidoData_t3443010735;

#include "codegen/il2cpp-codegen.h"

// System.Void PedidoData::.ctor()
extern "C"  void PedidoData__ctor_m3409526988 (PedidoData_t3443010735 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
