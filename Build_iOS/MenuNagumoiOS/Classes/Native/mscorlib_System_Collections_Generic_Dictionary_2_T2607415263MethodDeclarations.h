﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.StateMachine,System.Object,MagicTV.globals.StateMachine>
struct Transform_1_t2607415263;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_MagicTV_globals_StateMachine367995870.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.StateMachine,System.Object,MagicTV.globals.StateMachine>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m742614827_gshared (Transform_1_t2607415263 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m742614827(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t2607415263 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m742614827_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.StateMachine,System.Object,MagicTV.globals.StateMachine>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m2115507185_gshared (Transform_1_t2607415263 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m2115507185(__this, ___key0, ___value1, method) ((  int32_t (*) (Transform_1_t2607415263 *, int32_t, Il2CppObject *, const MethodInfo*))Transform_1_Invoke_m2115507185_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.StateMachine,System.Object,MagicTV.globals.StateMachine>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m4021086672_gshared (Transform_1_t2607415263 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m4021086672(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t2607415263 *, int32_t, Il2CppObject *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m4021086672_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<MagicTV.globals.StateMachine,System.Object,MagicTV.globals.StateMachine>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m3138739193_gshared (Transform_1_t2607415263 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m3138739193(__this, ___result0, method) ((  int32_t (*) (Transform_1_t2607415263 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m3138739193_gshared)(__this, ___result0, method)
