﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TabloideData[]
struct TabloideDataU5BU5D_t684230357;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TabloidesData
struct  TabloidesData_t2107312651  : public Il2CppObject
{
public:
	// TabloideData[] TabloidesData::tabloides
	TabloideDataU5BU5D_t684230357* ___tabloides_0;

public:
	inline static int32_t get_offset_of_tabloides_0() { return static_cast<int32_t>(offsetof(TabloidesData_t2107312651, ___tabloides_0)); }
	inline TabloideDataU5BU5D_t684230357* get_tabloides_0() const { return ___tabloides_0; }
	inline TabloideDataU5BU5D_t684230357** get_address_of_tabloides_0() { return &___tabloides_0; }
	inline void set_tabloides_0(TabloideDataU5BU5D_t684230357* value)
	{
		___tabloides_0 = value;
		Il2CppCodeGenWriteBarrier(&___tabloides_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
