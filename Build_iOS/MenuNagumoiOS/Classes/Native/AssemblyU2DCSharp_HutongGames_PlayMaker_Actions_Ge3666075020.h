﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// PlayMakerAnimatorMoveProxy
struct PlayMakerAnimatorMoveProxy_t4175490694;
// UnityEngine.Animator
struct Animator_t2776330603;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition
struct  GetAnimatorIsLayerInTransition_t3666075020  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::layerIndex
	FsmInt_t1596138449 * ___layerIndex_10;
	// System.Boolean HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::everyFrame
	bool ___everyFrame_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::isInTransition
	FsmBool_t1075959796 * ___isInTransition_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::isInTransitionEvent
	FsmEvent_t2133468028 * ___isInTransitionEvent_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::isNotInTransitionEvent
	FsmEvent_t2133468028 * ___isNotInTransitionEvent_14;
	// PlayMakerAnimatorMoveProxy HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::_animatorProxy
	PlayMakerAnimatorMoveProxy_t4175490694 * ____animatorProxy_15;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::_animator
	Animator_t2776330603 * ____animator_16;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(GetAnimatorIsLayerInTransition_t3666075020, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_layerIndex_10() { return static_cast<int32_t>(offsetof(GetAnimatorIsLayerInTransition_t3666075020, ___layerIndex_10)); }
	inline FsmInt_t1596138449 * get_layerIndex_10() const { return ___layerIndex_10; }
	inline FsmInt_t1596138449 ** get_address_of_layerIndex_10() { return &___layerIndex_10; }
	inline void set_layerIndex_10(FsmInt_t1596138449 * value)
	{
		___layerIndex_10 = value;
		Il2CppCodeGenWriteBarrier(&___layerIndex_10, value);
	}

	inline static int32_t get_offset_of_everyFrame_11() { return static_cast<int32_t>(offsetof(GetAnimatorIsLayerInTransition_t3666075020, ___everyFrame_11)); }
	inline bool get_everyFrame_11() const { return ___everyFrame_11; }
	inline bool* get_address_of_everyFrame_11() { return &___everyFrame_11; }
	inline void set_everyFrame_11(bool value)
	{
		___everyFrame_11 = value;
	}

	inline static int32_t get_offset_of_isInTransition_12() { return static_cast<int32_t>(offsetof(GetAnimatorIsLayerInTransition_t3666075020, ___isInTransition_12)); }
	inline FsmBool_t1075959796 * get_isInTransition_12() const { return ___isInTransition_12; }
	inline FsmBool_t1075959796 ** get_address_of_isInTransition_12() { return &___isInTransition_12; }
	inline void set_isInTransition_12(FsmBool_t1075959796 * value)
	{
		___isInTransition_12 = value;
		Il2CppCodeGenWriteBarrier(&___isInTransition_12, value);
	}

	inline static int32_t get_offset_of_isInTransitionEvent_13() { return static_cast<int32_t>(offsetof(GetAnimatorIsLayerInTransition_t3666075020, ___isInTransitionEvent_13)); }
	inline FsmEvent_t2133468028 * get_isInTransitionEvent_13() const { return ___isInTransitionEvent_13; }
	inline FsmEvent_t2133468028 ** get_address_of_isInTransitionEvent_13() { return &___isInTransitionEvent_13; }
	inline void set_isInTransitionEvent_13(FsmEvent_t2133468028 * value)
	{
		___isInTransitionEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___isInTransitionEvent_13, value);
	}

	inline static int32_t get_offset_of_isNotInTransitionEvent_14() { return static_cast<int32_t>(offsetof(GetAnimatorIsLayerInTransition_t3666075020, ___isNotInTransitionEvent_14)); }
	inline FsmEvent_t2133468028 * get_isNotInTransitionEvent_14() const { return ___isNotInTransitionEvent_14; }
	inline FsmEvent_t2133468028 ** get_address_of_isNotInTransitionEvent_14() { return &___isNotInTransitionEvent_14; }
	inline void set_isNotInTransitionEvent_14(FsmEvent_t2133468028 * value)
	{
		___isNotInTransitionEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___isNotInTransitionEvent_14, value);
	}

	inline static int32_t get_offset_of__animatorProxy_15() { return static_cast<int32_t>(offsetof(GetAnimatorIsLayerInTransition_t3666075020, ____animatorProxy_15)); }
	inline PlayMakerAnimatorMoveProxy_t4175490694 * get__animatorProxy_15() const { return ____animatorProxy_15; }
	inline PlayMakerAnimatorMoveProxy_t4175490694 ** get_address_of__animatorProxy_15() { return &____animatorProxy_15; }
	inline void set__animatorProxy_15(PlayMakerAnimatorMoveProxy_t4175490694 * value)
	{
		____animatorProxy_15 = value;
		Il2CppCodeGenWriteBarrier(&____animatorProxy_15, value);
	}

	inline static int32_t get_offset_of__animator_16() { return static_cast<int32_t>(offsetof(GetAnimatorIsLayerInTransition_t3666075020, ____animator_16)); }
	inline Animator_t2776330603 * get__animator_16() const { return ____animator_16; }
	inline Animator_t2776330603 ** get_address_of__animator_16() { return &____animator_16; }
	inline void set__animator_16(Animator_t2776330603 * value)
	{
		____animator_16 = value;
		Il2CppCodeGenWriteBarrier(&____animator_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
