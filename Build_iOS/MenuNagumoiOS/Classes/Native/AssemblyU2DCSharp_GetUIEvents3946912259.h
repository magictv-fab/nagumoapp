﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Events.UnityEvent
struct UnityEvent_t1266085011;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetUIEvents
struct  GetUIEvents_t3946912259  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Events.UnityEvent GetUIEvents::onPointerClick
	UnityEvent_t1266085011 * ___onPointerClick_2;

public:
	inline static int32_t get_offset_of_onPointerClick_2() { return static_cast<int32_t>(offsetof(GetUIEvents_t3946912259, ___onPointerClick_2)); }
	inline UnityEvent_t1266085011 * get_onPointerClick_2() const { return ___onPointerClick_2; }
	inline UnityEvent_t1266085011 ** get_address_of_onPointerClick_2() { return &___onPointerClick_2; }
	inline void set_onPointerClick_2(UnityEvent_t1266085011 * value)
	{
		___onPointerClick_2 = value;
		Il2CppCodeGenWriteBarrier(&___onPointerClick_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
