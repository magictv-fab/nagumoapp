﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetFsmMaterial
struct GetFsmMaterial_t3631711289;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetFsmMaterial::.ctor()
extern "C"  void GetFsmMaterial__ctor_m4155126861 (GetFsmMaterial_t3631711289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmMaterial::Reset()
extern "C"  void GetFsmMaterial_Reset_m1801559802 (GetFsmMaterial_t3631711289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmMaterial::OnEnter()
extern "C"  void GetFsmMaterial_OnEnter_m2444024036 (GetFsmMaterial_t3631711289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmMaterial::OnUpdate()
extern "C"  void GetFsmMaterial_OnUpdate_m1883860447 (GetFsmMaterial_t3631711289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmMaterial::DoGetFsmVariable()
extern "C"  void GetFsmMaterial_DoGetFsmVariable_m2543975976 (GetFsmMaterial_t3631711289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
