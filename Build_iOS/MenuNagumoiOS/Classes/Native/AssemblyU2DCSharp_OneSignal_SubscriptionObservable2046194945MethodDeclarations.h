﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OneSignal/SubscriptionObservable
struct SubscriptionObservable_t2046194945;
// System.Object
struct Il2CppObject;
// OSSubscriptionStateChanges
struct OSSubscriptionStateChanges_t52612275;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_OSSubscriptionStateChanges52612275.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void OneSignal/SubscriptionObservable::.ctor(System.Object,System.IntPtr)
extern "C"  void SubscriptionObservable__ctor_m367568168 (SubscriptionObservable_t2046194945 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal/SubscriptionObservable::Invoke(OSSubscriptionStateChanges)
extern "C"  void SubscriptionObservable_Invoke_m3124580335 (SubscriptionObservable_t2046194945 * __this, OSSubscriptionStateChanges_t52612275 * ___stateChanges0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult OneSignal/SubscriptionObservable::BeginInvoke(OSSubscriptionStateChanges,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SubscriptionObservable_BeginInvoke_m2600727536 (SubscriptionObservable_t2046194945 * __this, OSSubscriptionStateChanges_t52612275 * ___stateChanges0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal/SubscriptionObservable::EndInvoke(System.IAsyncResult)
extern "C"  void SubscriptionObservable_EndInvoke_m2272062264 (SubscriptionObservable_t2046194945 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
