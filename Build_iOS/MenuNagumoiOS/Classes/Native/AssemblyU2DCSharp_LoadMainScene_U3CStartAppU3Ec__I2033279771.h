﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AsyncOperation
struct AsyncOperation_t3699081103;
// System.Object
struct Il2CppObject;
// LoadMainScene
struct LoadMainScene_t1678736365;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadMainScene/<StartApp>c__Iterator3B
struct  U3CStartAppU3Ec__Iterator3B_t2033279771  : public Il2CppObject
{
public:
	// UnityEngine.AsyncOperation LoadMainScene/<StartApp>c__Iterator3B::<async>__0
	AsyncOperation_t3699081103 * ___U3CasyncU3E__0_0;
	// System.Int32 LoadMainScene/<StartApp>c__Iterator3B::$PC
	int32_t ___U24PC_1;
	// System.Object LoadMainScene/<StartApp>c__Iterator3B::$current
	Il2CppObject * ___U24current_2;
	// LoadMainScene LoadMainScene/<StartApp>c__Iterator3B::<>f__this
	LoadMainScene_t1678736365 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_U3CasyncU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartAppU3Ec__Iterator3B_t2033279771, ___U3CasyncU3E__0_0)); }
	inline AsyncOperation_t3699081103 * get_U3CasyncU3E__0_0() const { return ___U3CasyncU3E__0_0; }
	inline AsyncOperation_t3699081103 ** get_address_of_U3CasyncU3E__0_0() { return &___U3CasyncU3E__0_0; }
	inline void set_U3CasyncU3E__0_0(AsyncOperation_t3699081103 * value)
	{
		___U3CasyncU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CasyncU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CStartAppU3Ec__Iterator3B_t2033279771, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CStartAppU3Ec__Iterator3B_t2033279771, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3CStartAppU3Ec__Iterator3B_t2033279771, ___U3CU3Ef__this_3)); }
	inline LoadMainScene_t1678736365 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline LoadMainScene_t1678736365 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(LoadMainScene_t1678736365 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
