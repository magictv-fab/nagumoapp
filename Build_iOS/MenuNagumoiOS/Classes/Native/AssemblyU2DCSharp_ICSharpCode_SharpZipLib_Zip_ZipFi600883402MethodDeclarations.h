﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.ZipFile/UncompressedStream
struct UncompressedStream_t600883402;
// System.IO.Stream
struct Stream_t1561764144;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "mscorlib_System_IO_SeekOrigin4120335598.h"

// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/UncompressedStream::.ctor(System.IO.Stream)
extern "C"  void UncompressedStream__ctor_m2769968040 (UncompressedStream_t600883402 * __this, Stream_t1561764144 * ___baseStream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/UncompressedStream::Close()
extern "C"  void UncompressedStream_Close_m1014627367 (UncompressedStream_t600883402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile/UncompressedStream::get_CanRead()
extern "C"  bool UncompressedStream_get_CanRead_m347555288 (UncompressedStream_t600883402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/UncompressedStream::Flush()
extern "C"  void UncompressedStream_Flush_m3682682419 (UncompressedStream_t600883402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile/UncompressedStream::get_CanWrite()
extern "C"  bool UncompressedStream_get_CanWrite_m2706932863 (UncompressedStream_t600883402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile/UncompressedStream::get_CanSeek()
extern "C"  bool UncompressedStream_get_CanSeek_m376310330 (UncompressedStream_t600883402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile/UncompressedStream::get_Length()
extern "C"  int64_t UncompressedStream_get_Length_m1150033105 (UncompressedStream_t600883402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile/UncompressedStream::get_Position()
extern "C"  int64_t UncompressedStream_get_Position_m1596720084 (UncompressedStream_t600883402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/UncompressedStream::set_Position(System.Int64)
extern "C"  void UncompressedStream_set_Position_m4140170315 (UncompressedStream_t600883402 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipFile/UncompressedStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t UncompressedStream_Read_m4049760850 (UncompressedStream_t600883402 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile/UncompressedStream::Seek(System.Int64,System.IO.SeekOrigin)
extern "C"  int64_t UncompressedStream_Seek_m2924524659 (UncompressedStream_t600883402 * __this, int64_t ___offset0, int32_t ___origin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/UncompressedStream::SetLength(System.Int64)
extern "C"  void UncompressedStream_SetLength_m706610025 (UncompressedStream_t600883402 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/UncompressedStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C"  void UncompressedStream_Write_m2816870363 (UncompressedStream_t600883402 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
