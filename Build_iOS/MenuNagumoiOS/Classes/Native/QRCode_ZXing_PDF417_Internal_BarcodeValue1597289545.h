﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IDictionary`2<System.Int32,System.Int32>
struct IDictionary_2_t728975084;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.BarcodeValue
struct  BarcodeValue_t1597289545  : public Il2CppObject
{
public:
	// System.Collections.Generic.IDictionary`2<System.Int32,System.Int32> ZXing.PDF417.Internal.BarcodeValue::values
	Il2CppObject* ___values_0;

public:
	inline static int32_t get_offset_of_values_0() { return static_cast<int32_t>(offsetof(BarcodeValue_t1597289545, ___values_0)); }
	inline Il2CppObject* get_values_0() const { return ___values_0; }
	inline Il2CppObject** get_address_of_values_0() { return &___values_0; }
	inline void set_values_0(Il2CppObject* value)
	{
		___values_0 = value;
		Il2CppCodeGenWriteBarrier(&___values_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
