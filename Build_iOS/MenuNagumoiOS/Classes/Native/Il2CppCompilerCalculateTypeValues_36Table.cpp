﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_LZW_LzwCo576075684.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_LZW_LzwE2479978148.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_LZW_LzwI2878829311.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_SharpZip2520014981.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Tar_Inva3009170913.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Tar_TarA1409635571.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Tar_TarB2823691623.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Tar_TarEn762185187.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Tar_TarE3143306048.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Tar_TarH2980539316.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Tar_TarI1386636699.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Tar_TarI3457066855.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Tar_TarO1551862984.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Comp2454063301.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Comp2584813498.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Comp1587604368.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Comp3684739431.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Comp3769756376.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Comp1054057453.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Comp1829109954.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Comp1975778921.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Comp1952417709.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Comp1141403250.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Comp3572745737.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Compr537202536.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Compr441930877.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Compr928059325.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Compr617095569.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Comp2348681196.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_FastZ381075024.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Fast2142290519.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Fast1147420323.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Fast2877285688.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Windo500910988.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_UseZ3006992774.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Comp3174865049.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Encryp93834846.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Gener857591220.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipCo422568284.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_HostS860450688.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipE3141689087.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipE2027130115.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipE2336318763.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipE3036340399.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipE2326470748.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_RawT2798815536.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Exte1879413977.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Extend90301089.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_NTTa1676567466.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipEx591052775.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Keys1890758614.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Test3010800723.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Test3198385319.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Test2697759954.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_File4232823638.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipF2937401711.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipF3895517839.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipF2599129714.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipF3266384358.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipF3527085946.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipF3473946306.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipFi163542597.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipFi600883402.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipF2132028793.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipF3775663731.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Stat3533306910.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Dyna3440508339.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Base1865109560.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Disk2562884100.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Memo3679306472.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Desc3973589607.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Entr4202945134.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipH2122119617.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipI3995932855.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipI2108073026.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipN1757759150.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipO2362341588.h"
#include "AssemblyU2DCSharp_ARMVideoPlaybackBehaviour1131738883.h"
#include "AssemblyU2DCSharp_ARMVideoPlaybackBehaviour_OnVoid2058934555.h"
#include "AssemblyU2DCSharp_ARMVVPChromakeyGUIConfig1243100968.h"
#include "AssemblyU2DCSharp_ARMVuforiaInitializationErrorHand811017192.h"
#include "AssemblyU2DCSharp_ARM_camera_abstracts_TrackEventD3123780554.h"
#include "AssemblyU2DCSharp_ARM_camera_abstracts_TrackEventD1498492137.h"
#include "AssemblyU2DCSharp_ARM_camera_ra_VuforiaTrackEvent3472797067.h"
#include "AssemblyU2DCSharp_Analytics310950758.h"
#include "AssemblyU2DCSharp_GoogleUniversalAnalytics2200773556.h"
#include "AssemblyU2DCSharp_GoogleUniversalAnalytics_HitType1313315314.h"
#include "AssemblyU2DCSharp_GoogleUniversalAnalytics_NetAcce4130471992.h"
#include "AssemblyU2DCSharp_GoogleUniversalAnalytics_Delegat2694597681.h"
#include "AssemblyU2DCSharp_GoogleUniversalAnalytics_U3CnetA1845284094.h"
#include "AssemblyU2DCSharp_GoogleUniversalAnalytics_U3CdoWW3302283590.h"
#include "AssemblyU2DCSharp_LegacySystem_IO_File3963205794.h"
#include "AssemblyU2DCSharp_AnalyticsExampleMagicTvGUI215054320.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3600 = { sizeof (LzwConstants_t576075684), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3600[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3601 = { sizeof (LzwException_t2479978148), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3602 = { sizeof (LzwInputStream_t2878829311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3602[27] = 
{
	0,
	0,
	0,
	LzwInputStream_t2878829311::get_offset_of_baseInputStream_5(),
	LzwInputStream_t2878829311::get_offset_of_isStreamOwner_6(),
	LzwInputStream_t2878829311::get_offset_of_isClosed_7(),
	LzwInputStream_t2878829311::get_offset_of_one_8(),
	LzwInputStream_t2878829311::get_offset_of_headerParsed_9(),
	LzwInputStream_t2878829311::get_offset_of_tabPrefix_10(),
	LzwInputStream_t2878829311::get_offset_of_tabSuffix_11(),
	LzwInputStream_t2878829311::get_offset_of_zeros_12(),
	LzwInputStream_t2878829311::get_offset_of_stack_13(),
	LzwInputStream_t2878829311::get_offset_of_blockMode_14(),
	LzwInputStream_t2878829311::get_offset_of_nBits_15(),
	LzwInputStream_t2878829311::get_offset_of_maxBits_16(),
	LzwInputStream_t2878829311::get_offset_of_maxMaxCode_17(),
	LzwInputStream_t2878829311::get_offset_of_maxCode_18(),
	LzwInputStream_t2878829311::get_offset_of_bitMask_19(),
	LzwInputStream_t2878829311::get_offset_of_oldCode_20(),
	LzwInputStream_t2878829311::get_offset_of_finChar_21(),
	LzwInputStream_t2878829311::get_offset_of_stackP_22(),
	LzwInputStream_t2878829311::get_offset_of_freeEnt_23(),
	LzwInputStream_t2878829311::get_offset_of_data_24(),
	LzwInputStream_t2878829311::get_offset_of_bitPos_25(),
	LzwInputStream_t2878829311::get_offset_of_end_26(),
	LzwInputStream_t2878829311::get_offset_of_got_27(),
	LzwInputStream_t2878829311::get_offset_of_eof_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3603 = { sizeof (SharpZipBaseException_t2520014981), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3604 = { sizeof (InvalidHeaderException_t3009170913), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3605 = { sizeof (TarArchive_t1409635571), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3605[13] = 
{
	TarArchive_t1409635571::get_offset_of_keepOldFiles_0(),
	TarArchive_t1409635571::get_offset_of_asciiTranslate_1(),
	TarArchive_t1409635571::get_offset_of_userId_2(),
	TarArchive_t1409635571::get_offset_of_userName_3(),
	TarArchive_t1409635571::get_offset_of_groupId_4(),
	TarArchive_t1409635571::get_offset_of_groupName_5(),
	TarArchive_t1409635571::get_offset_of_rootPath_6(),
	TarArchive_t1409635571::get_offset_of_pathPrefix_7(),
	TarArchive_t1409635571::get_offset_of_applyUserInfoOverrides_8(),
	TarArchive_t1409635571::get_offset_of_tarIn_9(),
	TarArchive_t1409635571::get_offset_of_tarOut_10(),
	TarArchive_t1409635571::get_offset_of_isDisposed_11(),
	TarArchive_t1409635571::get_offset_of_ProgressMessageEvent_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3606 = { sizeof (TarBuffer_t2823691623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3606[11] = 
{
	0,
	0,
	0,
	TarBuffer_t2823691623::get_offset_of_inputStream_3(),
	TarBuffer_t2823691623::get_offset_of_outputStream_4(),
	TarBuffer_t2823691623::get_offset_of_recordBuffer_5(),
	TarBuffer_t2823691623::get_offset_of_currentBlockIndex_6(),
	TarBuffer_t2823691623::get_offset_of_currentRecordIndex_7(),
	TarBuffer_t2823691623::get_offset_of_recordSize_8(),
	TarBuffer_t2823691623::get_offset_of_blockFactor_9(),
	TarBuffer_t2823691623::get_offset_of_isStreamOwner__10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3607 = { sizeof (TarEntry_t762185187), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3607[2] = 
{
	TarEntry_t762185187::get_offset_of_file_0(),
	TarEntry_t762185187::get_offset_of_header_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3608 = { sizeof (TarException_t3143306048), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3609 = { sizeof (TarHeader_t2980539316), -1, sizeof(TarHeader_t2980539316_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3609[62] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	TarHeader_t2980539316_StaticFields::get_offset_of_dateTime1970_37(),
	TarHeader_t2980539316::get_offset_of_name_38(),
	TarHeader_t2980539316::get_offset_of_mode_39(),
	TarHeader_t2980539316::get_offset_of_userId_40(),
	TarHeader_t2980539316::get_offset_of_groupId_41(),
	TarHeader_t2980539316::get_offset_of_size_42(),
	TarHeader_t2980539316::get_offset_of_modTime_43(),
	TarHeader_t2980539316::get_offset_of_checksum_44(),
	TarHeader_t2980539316::get_offset_of_isChecksumValid_45(),
	TarHeader_t2980539316::get_offset_of_typeFlag_46(),
	TarHeader_t2980539316::get_offset_of_linkName_47(),
	TarHeader_t2980539316::get_offset_of_magic_48(),
	TarHeader_t2980539316::get_offset_of_version_49(),
	TarHeader_t2980539316::get_offset_of_userName_50(),
	TarHeader_t2980539316::get_offset_of_groupName_51(),
	TarHeader_t2980539316::get_offset_of_devMajor_52(),
	TarHeader_t2980539316::get_offset_of_devMinor_53(),
	TarHeader_t2980539316_StaticFields::get_offset_of_userIdAsSet_54(),
	TarHeader_t2980539316_StaticFields::get_offset_of_groupIdAsSet_55(),
	TarHeader_t2980539316_StaticFields::get_offset_of_userNameAsSet_56(),
	TarHeader_t2980539316_StaticFields::get_offset_of_groupNameAsSet_57(),
	TarHeader_t2980539316_StaticFields::get_offset_of_defaultUserId_58(),
	TarHeader_t2980539316_StaticFields::get_offset_of_defaultGroupId_59(),
	TarHeader_t2980539316_StaticFields::get_offset_of_defaultGroupName_60(),
	TarHeader_t2980539316_StaticFields::get_offset_of_defaultUser_61(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3610 = { sizeof (TarInputStream_t1386636699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3610[8] = 
{
	TarInputStream_t1386636699::get_offset_of_hasHitEOF_2(),
	TarInputStream_t1386636699::get_offset_of_entrySize_3(),
	TarInputStream_t1386636699::get_offset_of_entryOffset_4(),
	TarInputStream_t1386636699::get_offset_of_readBuffer_5(),
	TarInputStream_t1386636699::get_offset_of_tarBuffer_6(),
	TarInputStream_t1386636699::get_offset_of_currentEntry_7(),
	TarInputStream_t1386636699::get_offset_of_entryFactory_8(),
	TarInputStream_t1386636699::get_offset_of_inputStream_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3611 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3612 = { sizeof (EntryFactoryAdapter_t3457066855), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3613 = { sizeof (TarOutputStream_t1551862984), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3613[8] = 
{
	TarOutputStream_t1551862984::get_offset_of_currBytes_2(),
	TarOutputStream_t1551862984::get_offset_of_assemblyBufferLength_3(),
	TarOutputStream_t1551862984::get_offset_of_isClosed_4(),
	TarOutputStream_t1551862984::get_offset_of_currSize_5(),
	TarOutputStream_t1551862984::get_offset_of_blockBuffer_6(),
	TarOutputStream_t1551862984::get_offset_of_assemblyBuffer_7(),
	TarOutputStream_t1551862984::get_offset_of_buffer_8(),
	TarOutputStream_t1551862984::get_offset_of_outputStream_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3614 = { sizeof (Deflater_t2454063301), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3614[21] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	Deflater_t2454063301::get_offset_of_level_15(),
	Deflater_t2454063301::get_offset_of_noZlibHeaderOrFooter_16(),
	Deflater_t2454063301::get_offset_of_state_17(),
	Deflater_t2454063301::get_offset_of_totalOut_18(),
	Deflater_t2454063301::get_offset_of_pending_19(),
	Deflater_t2454063301::get_offset_of_engine_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3615 = { sizeof (DeflaterConstants_t2584813498), -1, sizeof(DeflaterConstants_t2584813498_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3615[27] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	DeflaterConstants_t2584813498_StaticFields::get_offset_of_MAX_BLOCK_SIZE_21(),
	DeflaterConstants_t2584813498_StaticFields::get_offset_of_GOOD_LENGTH_22(),
	DeflaterConstants_t2584813498_StaticFields::get_offset_of_MAX_LAZY_23(),
	DeflaterConstants_t2584813498_StaticFields::get_offset_of_NICE_LENGTH_24(),
	DeflaterConstants_t2584813498_StaticFields::get_offset_of_MAX_CHAIN_25(),
	DeflaterConstants_t2584813498_StaticFields::get_offset_of_COMPR_FUNC_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3616 = { sizeof (DeflateStrategy_t1587604368)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3616[4] = 
{
	DeflateStrategy_t1587604368::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3617 = { sizeof (DeflaterEngine_t3684739431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3617[24] = 
{
	0,
	DeflaterEngine_t3684739431::get_offset_of_ins_h_28(),
	DeflaterEngine_t3684739431::get_offset_of_head_29(),
	DeflaterEngine_t3684739431::get_offset_of_prev_30(),
	DeflaterEngine_t3684739431::get_offset_of_matchStart_31(),
	DeflaterEngine_t3684739431::get_offset_of_matchLen_32(),
	DeflaterEngine_t3684739431::get_offset_of_prevAvailable_33(),
	DeflaterEngine_t3684739431::get_offset_of_blockStart_34(),
	DeflaterEngine_t3684739431::get_offset_of_strstart_35(),
	DeflaterEngine_t3684739431::get_offset_of_lookahead_36(),
	DeflaterEngine_t3684739431::get_offset_of_window_37(),
	DeflaterEngine_t3684739431::get_offset_of_strategy_38(),
	DeflaterEngine_t3684739431::get_offset_of_max_chain_39(),
	DeflaterEngine_t3684739431::get_offset_of_max_lazy_40(),
	DeflaterEngine_t3684739431::get_offset_of_niceLength_41(),
	DeflaterEngine_t3684739431::get_offset_of_goodLength_42(),
	DeflaterEngine_t3684739431::get_offset_of_compressionFunction_43(),
	DeflaterEngine_t3684739431::get_offset_of_inputBuf_44(),
	DeflaterEngine_t3684739431::get_offset_of_totalIn_45(),
	DeflaterEngine_t3684739431::get_offset_of_inputOff_46(),
	DeflaterEngine_t3684739431::get_offset_of_inputEnd_47(),
	DeflaterEngine_t3684739431::get_offset_of_pending_48(),
	DeflaterEngine_t3684739431::get_offset_of_huffman_49(),
	DeflaterEngine_t3684739431::get_offset_of_adler_50(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3618 = { sizeof (DeflaterHuffman_t3769756376), -1, sizeof(DeflaterHuffman_t3769756376_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3618[22] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	DeflaterHuffman_t3769756376_StaticFields::get_offset_of_BL_ORDER_8(),
	DeflaterHuffman_t3769756376_StaticFields::get_offset_of_bit4Reverse_9(),
	DeflaterHuffman_t3769756376_StaticFields::get_offset_of_staticLCodes_10(),
	DeflaterHuffman_t3769756376_StaticFields::get_offset_of_staticLLength_11(),
	DeflaterHuffman_t3769756376_StaticFields::get_offset_of_staticDCodes_12(),
	DeflaterHuffman_t3769756376_StaticFields::get_offset_of_staticDLength_13(),
	DeflaterHuffman_t3769756376::get_offset_of_pending_14(),
	DeflaterHuffman_t3769756376::get_offset_of_literalTree_15(),
	DeflaterHuffman_t3769756376::get_offset_of_distTree_16(),
	DeflaterHuffman_t3769756376::get_offset_of_blTree_17(),
	DeflaterHuffman_t3769756376::get_offset_of_d_buf_18(),
	DeflaterHuffman_t3769756376::get_offset_of_l_buf_19(),
	DeflaterHuffman_t3769756376::get_offset_of_last_lit_20(),
	DeflaterHuffman_t3769756376::get_offset_of_extra_bits_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3619 = { sizeof (Tree_t1054057453), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3619[8] = 
{
	Tree_t1054057453::get_offset_of_freqs_0(),
	Tree_t1054057453::get_offset_of_length_1(),
	Tree_t1054057453::get_offset_of_minNumCodes_2(),
	Tree_t1054057453::get_offset_of_numCodes_3(),
	Tree_t1054057453::get_offset_of_codes_4(),
	Tree_t1054057453::get_offset_of_bl_counts_5(),
	Tree_t1054057453::get_offset_of_maxLength_6(),
	Tree_t1054057453::get_offset_of_dh_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3620 = { sizeof (DeflaterPending_t1829109954), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3621 = { sizeof (Inflater_t1975778921), -1, sizeof(Inflater_t1975778921_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3621[33] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	Inflater_t1975778921_StaticFields::get_offset_of_CPLENS_13(),
	Inflater_t1975778921_StaticFields::get_offset_of_CPLEXT_14(),
	Inflater_t1975778921_StaticFields::get_offset_of_CPDIST_15(),
	Inflater_t1975778921_StaticFields::get_offset_of_CPDEXT_16(),
	Inflater_t1975778921::get_offset_of_mode_17(),
	Inflater_t1975778921::get_offset_of_readAdler_18(),
	Inflater_t1975778921::get_offset_of_neededBits_19(),
	Inflater_t1975778921::get_offset_of_repLength_20(),
	Inflater_t1975778921::get_offset_of_repDist_21(),
	Inflater_t1975778921::get_offset_of_uncomprLen_22(),
	Inflater_t1975778921::get_offset_of_isLastBlock_23(),
	Inflater_t1975778921::get_offset_of_totalOut_24(),
	Inflater_t1975778921::get_offset_of_totalIn_25(),
	Inflater_t1975778921::get_offset_of_noHeader_26(),
	Inflater_t1975778921::get_offset_of_input_27(),
	Inflater_t1975778921::get_offset_of_outputWindow_28(),
	Inflater_t1975778921::get_offset_of_dynHeader_29(),
	Inflater_t1975778921::get_offset_of_litlenTree_30(),
	Inflater_t1975778921::get_offset_of_distTree_31(),
	Inflater_t1975778921::get_offset_of_adler_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3622 = { sizeof (InflaterDynHeader_t1952417709), -1, sizeof(InflaterDynHeader_t1952417709_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3622[20] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	InflaterDynHeader_t1952417709_StaticFields::get_offset_of_repMin_6(),
	InflaterDynHeader_t1952417709_StaticFields::get_offset_of_repBits_7(),
	InflaterDynHeader_t1952417709_StaticFields::get_offset_of_BL_ORDER_8(),
	InflaterDynHeader_t1952417709::get_offset_of_blLens_9(),
	InflaterDynHeader_t1952417709::get_offset_of_litdistLens_10(),
	InflaterDynHeader_t1952417709::get_offset_of_blTree_11(),
	InflaterDynHeader_t1952417709::get_offset_of_mode_12(),
	InflaterDynHeader_t1952417709::get_offset_of_lnum_13(),
	InflaterDynHeader_t1952417709::get_offset_of_dnum_14(),
	InflaterDynHeader_t1952417709::get_offset_of_blnum_15(),
	InflaterDynHeader_t1952417709::get_offset_of_num_16(),
	InflaterDynHeader_t1952417709::get_offset_of_repSymbol_17(),
	InflaterDynHeader_t1952417709::get_offset_of_lastLen_18(),
	InflaterDynHeader_t1952417709::get_offset_of_ptr_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3623 = { sizeof (InflaterHuffmanTree_t1141403250), -1, sizeof(InflaterHuffmanTree_t1141403250_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3623[4] = 
{
	0,
	InflaterHuffmanTree_t1141403250::get_offset_of_tree_1(),
	InflaterHuffmanTree_t1141403250_StaticFields::get_offset_of_defLitLenTree_2(),
	InflaterHuffmanTree_t1141403250_StaticFields::get_offset_of_defDistTree_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3624 = { sizeof (PendingBuffer_t3572745737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3624[5] = 
{
	PendingBuffer_t3572745737::get_offset_of_buffer__0(),
	PendingBuffer_t3572745737::get_offset_of_start_1(),
	PendingBuffer_t3572745737::get_offset_of_end_2(),
	PendingBuffer_t3572745737::get_offset_of_bits_3(),
	PendingBuffer_t3572745737::get_offset_of_bitCount_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3625 = { sizeof (DeflaterOutputStream_t537202536), -1, sizeof(DeflaterOutputStream_t537202536_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3625[9] = 
{
	DeflaterOutputStream_t537202536::get_offset_of_password_2(),
	DeflaterOutputStream_t537202536::get_offset_of_cryptoTransform__3(),
	DeflaterOutputStream_t537202536::get_offset_of_AESAuthCode_4(),
	DeflaterOutputStream_t537202536::get_offset_of_buffer__5(),
	DeflaterOutputStream_t537202536::get_offset_of_deflater__6(),
	DeflaterOutputStream_t537202536::get_offset_of_baseOutputStream__7(),
	DeflaterOutputStream_t537202536::get_offset_of_isClosed__8(),
	DeflaterOutputStream_t537202536::get_offset_of_isStreamOwner__9(),
	DeflaterOutputStream_t537202536_StaticFields::get_offset_of__aesRnd_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3626 = { sizeof (InflaterInputBuffer_t441930877), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3626[8] = 
{
	InflaterInputBuffer_t441930877::get_offset_of_rawLength_0(),
	InflaterInputBuffer_t441930877::get_offset_of_rawData_1(),
	InflaterInputBuffer_t441930877::get_offset_of_clearTextLength_2(),
	InflaterInputBuffer_t441930877::get_offset_of_clearText_3(),
	InflaterInputBuffer_t441930877::get_offset_of_internalClearText_4(),
	InflaterInputBuffer_t441930877::get_offset_of_available_5(),
	InflaterInputBuffer_t441930877::get_offset_of_cryptoTransform_6(),
	InflaterInputBuffer_t441930877::get_offset_of_inputStream_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3627 = { sizeof (InflaterInputStream_t928059325), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3627[6] = 
{
	InflaterInputStream_t928059325::get_offset_of_inf_2(),
	InflaterInputStream_t928059325::get_offset_of_inputBuffer_3(),
	InflaterInputStream_t928059325::get_offset_of_baseInputStream_4(),
	InflaterInputStream_t928059325::get_offset_of_csize_5(),
	InflaterInputStream_t928059325::get_offset_of_isClosed_6(),
	InflaterInputStream_t928059325::get_offset_of_isStreamOwner_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3628 = { sizeof (OutputWindow_t617095569), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3628[5] = 
{
	0,
	0,
	OutputWindow_t617095569::get_offset_of_window_2(),
	OutputWindow_t617095569::get_offset_of_windowEnd_3(),
	OutputWindow_t617095569::get_offset_of_windowFilled_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3629 = { sizeof (StreamManipulator_t2348681196), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3629[5] = 
{
	StreamManipulator_t2348681196::get_offset_of_window__0(),
	StreamManipulator_t2348681196::get_offset_of_windowStart__1(),
	StreamManipulator_t2348681196::get_offset_of_windowEnd__2(),
	StreamManipulator_t2348681196::get_offset_of_buffer__3(),
	StreamManipulator_t2348681196::get_offset_of_bitsInBuffer__4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3630 = { sizeof (FastZipEvents_t381075024), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3630[7] = 
{
	FastZipEvents_t381075024::get_offset_of_ProcessDirectory_0(),
	FastZipEvents_t381075024::get_offset_of_ProcessFile_1(),
	FastZipEvents_t381075024::get_offset_of_Progress_2(),
	FastZipEvents_t381075024::get_offset_of_CompletedFile_3(),
	FastZipEvents_t381075024::get_offset_of_DirectoryFailure_4(),
	FastZipEvents_t381075024::get_offset_of_FileFailure_5(),
	FastZipEvents_t381075024::get_offset_of_progressInterval__6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3631 = { sizeof (FastZip_t2142290519), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3631[17] = 
{
	FastZip_t2142290519::get_offset_of_continueRunning__0(),
	FastZip_t2142290519::get_offset_of_buffer__1(),
	FastZip_t2142290519::get_offset_of_outputStream__2(),
	FastZip_t2142290519::get_offset_of_zipFile__3(),
	FastZip_t2142290519::get_offset_of_sourceDirectory__4(),
	FastZip_t2142290519::get_offset_of_fileFilter__5(),
	FastZip_t2142290519::get_offset_of_directoryFilter__6(),
	FastZip_t2142290519::get_offset_of_overwrite__7(),
	FastZip_t2142290519::get_offset_of_confirmDelegate__8(),
	FastZip_t2142290519::get_offset_of_restoreDateTimeOnExtract__9(),
	FastZip_t2142290519::get_offset_of_restoreAttributesOnExtract__10(),
	FastZip_t2142290519::get_offset_of_createEmptyDirectories__11(),
	FastZip_t2142290519::get_offset_of_events__12(),
	FastZip_t2142290519::get_offset_of_entryFactory__13(),
	FastZip_t2142290519::get_offset_of_extractNameTransform__14(),
	FastZip_t2142290519::get_offset_of_useZip64__15(),
	FastZip_t2142290519::get_offset_of_password__16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3632 = { sizeof (Overwrite_t1147420323)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3632[4] = 
{
	Overwrite_t1147420323::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3633 = { sizeof (ConfirmOverwriteDelegate_t2877285688), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3634 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3635 = { sizeof (WindowsNameTransform_t500910988), -1, sizeof(WindowsNameTransform_t500910988_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3635[5] = 
{
	0,
	WindowsNameTransform_t500910988::get_offset_of__baseDirectory_1(),
	WindowsNameTransform_t500910988::get_offset_of__trimIncomingPaths_2(),
	WindowsNameTransform_t500910988::get_offset_of__replacementChar_3(),
	WindowsNameTransform_t500910988_StaticFields::get_offset_of_InvalidEntryChars_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3636 = { sizeof (UseZip64_t3006992774)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3636[4] = 
{
	UseZip64_t3006992774::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3637 = { sizeof (CompressionMethod_t3174865049)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3637[6] = 
{
	CompressionMethod_t3174865049::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3638 = { sizeof (EncryptionAlgorithm_t93834846)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3638[15] = 
{
	EncryptionAlgorithm_t93834846::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3639 = { sizeof (GeneralBitFlags_t857591220)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3639[16] = 
{
	GeneralBitFlags_t857591220::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3640 = { sizeof (ZipConstants_t422568284), -1, sizeof(ZipConstants_t422568284_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3640[36] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	ZipConstants_t422568284_StaticFields::get_offset_of_defaultCodePage_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3641 = { sizeof (HostSystemID_t860450688)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3641[22] = 
{
	HostSystemID_t860450688::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3642 = { sizeof (ZipEntry_t3141689087), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3642[19] = 
{
	ZipEntry_t3141689087::get_offset_of_known_0(),
	ZipEntry_t3141689087::get_offset_of_externalFileAttributes_1(),
	ZipEntry_t3141689087::get_offset_of_versionMadeBy_2(),
	ZipEntry_t3141689087::get_offset_of_name_3(),
	ZipEntry_t3141689087::get_offset_of_size_4(),
	ZipEntry_t3141689087::get_offset_of_compressedSize_5(),
	ZipEntry_t3141689087::get_offset_of_versionToExtract_6(),
	ZipEntry_t3141689087::get_offset_of_crc_7(),
	ZipEntry_t3141689087::get_offset_of_dosTime_8(),
	ZipEntry_t3141689087::get_offset_of_method_9(),
	ZipEntry_t3141689087::get_offset_of_extra_10(),
	ZipEntry_t3141689087::get_offset_of_comment_11(),
	ZipEntry_t3141689087::get_offset_of_flags_12(),
	ZipEntry_t3141689087::get_offset_of_zipFileIndex_13(),
	ZipEntry_t3141689087::get_offset_of_offset_14(),
	ZipEntry_t3141689087::get_offset_of_forceZip64__15(),
	ZipEntry_t3141689087::get_offset_of_cryptoCheckValue__16(),
	ZipEntry_t3141689087::get_offset_of__aesVer_17(),
	ZipEntry_t3141689087::get_offset_of__aesEncryptionStrength_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3643 = { sizeof (Known_t2027130115)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3643[7] = 
{
	Known_t2027130115::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3644 = { sizeof (ZipEntryFactory_t2336318763), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3644[6] = 
{
	ZipEntryFactory_t2336318763::get_offset_of_nameTransform__0(),
	ZipEntryFactory_t2336318763::get_offset_of_fixedDateTime__1(),
	ZipEntryFactory_t2336318763::get_offset_of_timeSetting__2(),
	ZipEntryFactory_t2336318763::get_offset_of_isUnicodeText__3(),
	ZipEntryFactory_t2336318763::get_offset_of_getAttributes__4(),
	ZipEntryFactory_t2336318763::get_offset_of_setAttributes__5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3645 = { sizeof (TimeSetting_t3036340399)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3645[8] = 
{
	TimeSetting_t3036340399::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3646 = { sizeof (ZipException_t2326470748), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3647 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3648 = { sizeof (RawTaggedData_t2798815536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3648[2] = 
{
	RawTaggedData_t2798815536::get_offset_of__tag_0(),
	RawTaggedData_t2798815536::get_offset_of__data_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3649 = { sizeof (ExtendedUnixData_t1879413977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3649[4] = 
{
	ExtendedUnixData_t1879413977::get_offset_of__flags_0(),
	ExtendedUnixData_t1879413977::get_offset_of__modificationTime_1(),
	ExtendedUnixData_t1879413977::get_offset_of__lastAccessTime_2(),
	ExtendedUnixData_t1879413977::get_offset_of__createTime_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3650 = { sizeof (Flags_t90301089)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3650[4] = 
{
	Flags_t90301089::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3651 = { sizeof (NTTaggedData_t1676567466), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3651[3] = 
{
	NTTaggedData_t1676567466::get_offset_of__lastAccessTime_0(),
	NTTaggedData_t1676567466::get_offset_of__lastModificationTime_1(),
	NTTaggedData_t1676567466::get_offset_of__createTime_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3652 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3653 = { sizeof (ZipExtraData_t591052775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3653[5] = 
{
	ZipExtraData_t591052775::get_offset_of__index_0(),
	ZipExtraData_t591052775::get_offset_of__readValueStart_1(),
	ZipExtraData_t591052775::get_offset_of__readValueLength_2(),
	ZipExtraData_t591052775::get_offset_of__newEntry_3(),
	ZipExtraData_t591052775::get_offset_of__data_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3654 = { sizeof (KeysRequiredEventArgs_t1890758614), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3654[2] = 
{
	KeysRequiredEventArgs_t1890758614::get_offset_of_fileName_1(),
	KeysRequiredEventArgs_t1890758614::get_offset_of_key_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3655 = { sizeof (TestStrategy_t3010800723)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3655[3] = 
{
	TestStrategy_t3010800723::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3656 = { sizeof (TestOperation_t3198385319)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3656[7] = 
{
	TestOperation_t3198385319::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3657 = { sizeof (TestStatus_t2697759954), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3657[6] = 
{
	TestStatus_t2697759954::get_offset_of_file__0(),
	TestStatus_t2697759954::get_offset_of_entry__1(),
	TestStatus_t2697759954::get_offset_of_entryValid__2(),
	TestStatus_t2697759954::get_offset_of_errorCount__3(),
	TestStatus_t2697759954::get_offset_of_bytesTested__4(),
	TestStatus_t2697759954::get_offset_of_operation__5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3658 = { sizeof (FileUpdateMode_t4232823638)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3658[3] = 
{
	FileUpdateMode_t4232823638::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3659 = { sizeof (ZipFile_t2937401711), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3659[24] = 
{
	0,
	ZipFile_t2937401711::get_offset_of_KeysRequired_1(),
	ZipFile_t2937401711::get_offset_of_isDisposed__2(),
	ZipFile_t2937401711::get_offset_of_name__3(),
	ZipFile_t2937401711::get_offset_of_comment__4(),
	ZipFile_t2937401711::get_offset_of_rawPassword__5(),
	ZipFile_t2937401711::get_offset_of_baseStream__6(),
	ZipFile_t2937401711::get_offset_of_isStreamOwner_7(),
	ZipFile_t2937401711::get_offset_of_offsetOfFirstEntry_8(),
	ZipFile_t2937401711::get_offset_of_entries__9(),
	ZipFile_t2937401711::get_offset_of_key_10(),
	ZipFile_t2937401711::get_offset_of_isNewArchive__11(),
	ZipFile_t2937401711::get_offset_of_useZip64__12(),
	ZipFile_t2937401711::get_offset_of_updates__13(),
	ZipFile_t2937401711::get_offset_of_updateCount__14(),
	ZipFile_t2937401711::get_offset_of_updateIndex__15(),
	ZipFile_t2937401711::get_offset_of_archiveStorage__16(),
	ZipFile_t2937401711::get_offset_of_updateDataSource__17(),
	ZipFile_t2937401711::get_offset_of_contentsEdited__18(),
	ZipFile_t2937401711::get_offset_of_bufferSize__19(),
	ZipFile_t2937401711::get_offset_of_copyBuffer__20(),
	ZipFile_t2937401711::get_offset_of_newComment__21(),
	ZipFile_t2937401711::get_offset_of_commentEdited__22(),
	ZipFile_t2937401711::get_offset_of_updateEntryFactory__23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3660 = { sizeof (HeaderTest_t3895517839)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3660[3] = 
{
	HeaderTest_t3895517839::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3661 = { sizeof (UpdateCommand_t2599129714)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3661[4] = 
{
	UpdateCommand_t2599129714::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3662 = { sizeof (UpdateComparer_t3266384358), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3663 = { sizeof (ZipUpdate_t3527085946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3663[8] = 
{
	ZipUpdate_t3527085946::get_offset_of_entry__0(),
	ZipUpdate_t3527085946::get_offset_of_outEntry__1(),
	ZipUpdate_t3527085946::get_offset_of_command__2(),
	ZipUpdate_t3527085946::get_offset_of_dataSource__3(),
	ZipUpdate_t3527085946::get_offset_of_filename__4(),
	ZipUpdate_t3527085946::get_offset_of_sizePatchOffset__5(),
	ZipUpdate_t3527085946::get_offset_of_crcPatchOffset__6(),
	ZipUpdate_t3527085946::get_offset_of__offsetBasedSize_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3664 = { sizeof (ZipString_t3473946306), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3664[3] = 
{
	ZipString_t3473946306::get_offset_of_comment__0(),
	ZipString_t3473946306::get_offset_of_rawComment__1(),
	ZipString_t3473946306::get_offset_of_isSourceString__2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3665 = { sizeof (ZipEntryEnumerator_t163542597), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3665[2] = 
{
	ZipEntryEnumerator_t163542597::get_offset_of_array_0(),
	ZipEntryEnumerator_t163542597::get_offset_of_index_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3666 = { sizeof (UncompressedStream_t600883402), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3666[1] = 
{
	UncompressedStream_t600883402::get_offset_of_baseStream__2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3667 = { sizeof (PartialInputStream_t2132028793), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3667[6] = 
{
	PartialInputStream_t2132028793::get_offset_of_zipFile__2(),
	PartialInputStream_t2132028793::get_offset_of_baseStream__3(),
	PartialInputStream_t2132028793::get_offset_of_start__4(),
	PartialInputStream_t2132028793::get_offset_of_length__5(),
	PartialInputStream_t2132028793::get_offset_of_readPos__6(),
	PartialInputStream_t2132028793::get_offset_of_end__7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3668 = { sizeof (KeysRequiredEventHandler_t3775663731), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3669 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3670 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3671 = { sizeof (StaticDiskDataSource_t3533306910), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3671[1] = 
{
	StaticDiskDataSource_t3533306910::get_offset_of_fileName__0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3672 = { sizeof (DynamicDiskDataSource_t3440508339), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3673 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3674 = { sizeof (BaseArchiveStorage_t1865109560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3674[1] = 
{
	BaseArchiveStorage_t1865109560::get_offset_of_updateMode__0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3675 = { sizeof (DiskArchiveStorage_t2562884100), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3675[3] = 
{
	DiskArchiveStorage_t2562884100::get_offset_of_temporaryStream__1(),
	DiskArchiveStorage_t2562884100::get_offset_of_fileName__2(),
	DiskArchiveStorage_t2562884100::get_offset_of_temporaryName__3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3676 = { sizeof (MemoryArchiveStorage_t3679306472), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3676[2] = 
{
	MemoryArchiveStorage_t3679306472::get_offset_of_temporaryStream__1(),
	MemoryArchiveStorage_t3679306472::get_offset_of_finalStream__2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3677 = { sizeof (DescriptorData_t3973589607), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3677[3] = 
{
	DescriptorData_t3973589607::get_offset_of_size_0(),
	DescriptorData_t3973589607::get_offset_of_compressedSize_1(),
	DescriptorData_t3973589607::get_offset_of_crc_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3678 = { sizeof (EntryPatchData_t4202945134), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3678[2] = 
{
	EntryPatchData_t4202945134::get_offset_of_sizePatchOffset__0(),
	EntryPatchData_t4202945134::get_offset_of_crcPatchOffset__1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3679 = { sizeof (ZipHelperStream_t2122119617), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3679[2] = 
{
	ZipHelperStream_t2122119617::get_offset_of_isOwner__2(),
	ZipHelperStream_t2122119617::get_offset_of_stream__3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3680 = { sizeof (ZipInputStream_t3995932855), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3680[7] = 
{
	ZipInputStream_t3995932855::get_offset_of_internalReader_8(),
	ZipInputStream_t3995932855::get_offset_of_crc_9(),
	ZipInputStream_t3995932855::get_offset_of_entry_10(),
	ZipInputStream_t3995932855::get_offset_of_size_11(),
	ZipInputStream_t3995932855::get_offset_of_method_12(),
	ZipInputStream_t3995932855::get_offset_of_flags_13(),
	ZipInputStream_t3995932855::get_offset_of_password_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3681 = { sizeof (ReadDataHandler_t2108073026), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3682 = { sizeof (ZipNameTransform_t1757759150), -1, sizeof(ZipNameTransform_t1757759150_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3682[3] = 
{
	ZipNameTransform_t1757759150::get_offset_of_trimPrefix__0(),
	ZipNameTransform_t1757759150_StaticFields::get_offset_of_InvalidEntryChars_1(),
	ZipNameTransform_t1757759150_StaticFields::get_offset_of_InvalidEntryCharsRelaxed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3683 = { sizeof (ZipOutputStream_t2362341588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3683[12] = 
{
	ZipOutputStream_t2362341588::get_offset_of_entries_11(),
	ZipOutputStream_t2362341588::get_offset_of_crc_12(),
	ZipOutputStream_t2362341588::get_offset_of_curEntry_13(),
	ZipOutputStream_t2362341588::get_offset_of_defaultCompressionLevel_14(),
	ZipOutputStream_t2362341588::get_offset_of_curMethod_15(),
	ZipOutputStream_t2362341588::get_offset_of_size_16(),
	ZipOutputStream_t2362341588::get_offset_of_offset_17(),
	ZipOutputStream_t2362341588::get_offset_of_zipComment_18(),
	ZipOutputStream_t2362341588::get_offset_of_patchEntryHeader_19(),
	ZipOutputStream_t2362341588::get_offset_of_crcPatchPos_20(),
	ZipOutputStream_t2362341588::get_offset_of_sizePatchPos_21(),
	ZipOutputStream_t2362341588::get_offset_of_useZip64__22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3684 = { sizeof (ARMVideoPlaybackBehaviour_t1131738883), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3684[19] = 
{
	ARMVideoPlaybackBehaviour_t1131738883::get_offset_of_buttonGame_6(),
	ARMVideoPlaybackBehaviour_t1131738883::get_offset_of_m_path_7(),
	ARMVideoPlaybackBehaviour_t1131738883::get_offset_of_loop_8(),
	ARMVideoPlaybackBehaviour_t1131738883::get_offset_of_ARMCurrentState_9(),
	ARMVideoPlaybackBehaviour_t1131738883::get_offset_of_mVideoPlayer_10(),
	ARMVideoPlaybackBehaviour_t1131738883::get_offset_of_mIsInternalComponentInited_11(),
	ARMVideoPlaybackBehaviour_t1131738883::get_offset_of_mIsPrepared_12(),
	ARMVideoPlaybackBehaviour_t1131738883::get_offset_of_mVideoTexture_13(),
	ARMVideoPlaybackBehaviour_t1131738883::get_offset_of_firstPlay_14(),
	ARMVideoPlaybackBehaviour_t1131738883::get_offset_of_mKeyframeTexture_15(),
	ARMVideoPlaybackBehaviour_t1131738883::get_offset_of_mMediaType_16(),
	ARMVideoPlaybackBehaviour_t1131738883::get_offset_of_mCurrentState_17(),
	ARMVideoPlaybackBehaviour_t1131738883::get_offset_of_mSeekPosition_18(),
	ARMVideoPlaybackBehaviour_t1131738883::get_offset_of_mPlayPosition_19(),
	ARMVideoPlaybackBehaviour_t1131738883::get_offset_of_isPlayableOnTexture_20(),
	ARMVideoPlaybackBehaviour_t1131738883::get_offset_of_debug_status_21(),
	ARMVideoPlaybackBehaviour_t1131738883::get_offset_of_initCalled_22(),
	ARMVideoPlaybackBehaviour_t1131738883::get_offset_of_onYouCanShowMe_23(),
	ARMVideoPlaybackBehaviour_t1131738883::get_offset_of__onFinishedEventTrigger_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3685 = { sizeof (OnVoidEvent_t2058934555), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3686 = { sizeof (ARMVVPChromakeyGUIConfig_t1243100968), -1, sizeof(ARMVVPChromakeyGUIConfig_t1243100968_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3686[21] = 
{
	ARMVVPChromakeyGUIConfig_t1243100968::get_offset_of_sensibilityChanger_2(),
	ARMVVPChromakeyGUIConfig_t1243100968::get_offset_of_recorteChanger_3(),
	ARMVVPChromakeyGUIConfig_t1243100968::get_offset_of_smoothChanger_4(),
	ARMVVPChromakeyGUIConfig_t1243100968::get_offset_of_smoothSensibilityChanger_5(),
	ARMVVPChromakeyGUIConfig_t1243100968::get_offset_of_sensibility_6(),
	ARMVVPChromakeyGUIConfig_t1243100968::get_offset_of_recorte_7(),
	ARMVVPChromakeyGUIConfig_t1243100968::get_offset_of_smoothSensibility_8(),
	ARMVVPChromakeyGUIConfig_t1243100968::get_offset_of_smooth_9(),
	ARMVVPChromakeyGUIConfig_t1243100968::get_offset_of_shaderChoiced_10(),
	ARMVVPChromakeyGUIConfig_t1243100968::get_offset_of_GUIMaterialName_11(),
	ARMVVPChromakeyGUIConfig_t1243100968::get_offset_of_GUISensibility_12(),
	ARMVVPChromakeyGUIConfig_t1243100968::get_offset_of_GUIRecorte_13(),
	ARMVVPChromakeyGUIConfig_t1243100968::get_offset_of_GUIFloatValSmoothInput_14(),
	ARMVVPChromakeyGUIConfig_t1243100968::get_offset_of_GUISmoothLabel_15(),
	ARMVVPChromakeyGUIConfig_t1243100968::get_offset_of_GUIFloatValSmoothSensibilityInput_16(),
	ARMVVPChromakeyGUIConfig_t1243100968::get_offset_of_GUISmoothSensibilityLabel_17(),
	ARMVVPChromakeyGUIConfig_t1243100968::get_offset_of_GUIFloatValSensi_18(),
	ARMVVPChromakeyGUIConfig_t1243100968::get_offset_of_GUIFloatrecorte_19(),
	ARMVVPChromakeyGUIConfig_t1243100968::get_offset_of_GUISaveString_20(),
	ARMVVPChromakeyGUIConfig_t1243100968::get_offset_of_bundleID_21(),
	ARMVVPChromakeyGUIConfig_t1243100968_StaticFields::get_offset_of_Instance_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3687 = { sizeof (ARMVuforiaInitializationErrorHandler_t811017192), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3687[6] = 
{
	0,
	ARMVuforiaInitializationErrorHandler_t811017192::get_offset_of_DisableList_3(),
	ARMVuforiaInitializationErrorHandler_t811017192::get_offset_of_EnaableList_4(),
	ARMVuforiaInitializationErrorHandler_t811017192::get_offset_of_mErrorText_5(),
	ARMVuforiaInitializationErrorHandler_t811017192::get_offset_of_mErrorOccurred_6(),
	ARMVuforiaInitializationErrorHandler_t811017192::get_offset_of__restart_when_return_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3688 = { sizeof (TrackEventDispatcherAbstract_t3123780554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3688[2] = 
{
	TrackEventDispatcherAbstract_t3123780554::get_offset_of_onTrackIn_2(),
	TrackEventDispatcherAbstract_t3123780554::get_offset_of_onTrackOut_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3689 = { sizeof (OnTrack_t1498492137), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3690 = { sizeof (VuforiaTrackEvent_t3472797067), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3690[1] = 
{
	VuforiaTrackEvent_t3472797067::get_offset_of_mTrackableBehaviour_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3691 = { sizeof (Analytics_t310950758), -1, sizeof(Analytics_t310950758_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3691[12] = 
{
	0,
	Analytics_t310950758::get_offset_of_trackingID_3(),
	Analytics_t310950758::get_offset_of_appName_4(),
	Analytics_t310950758::get_offset_of_appVersion_5(),
	Analytics_t310950758::get_offset_of_newLevelAnalyticsEventPrefix_6(),
	Analytics_t310950758::get_offset_of_useHTTPS_7(),
	Analytics_t310950758::get_offset_of_useOfflineCache_8(),
	Analytics_t310950758_StaticFields::get_offset_of_gua_9(),
	Analytics_t310950758_StaticFields::get_offset_of_instance_10(),
	Analytics_t310950758::get_offset_of_sceneName_11(),
	Analytics_t310950758::get_offset_of_offlineCacheFileName_12(),
	Analytics_t310950758_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3692 = { sizeof (GoogleUniversalAnalytics_t2200773556), -1, sizeof(GoogleUniversalAnalytics_t2200773556_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3692[37] = 
{
	GoogleUniversalAnalytics_t2200773556_StaticFields::get_offset_of_hitTypeNames_0(),
	GoogleUniversalAnalytics_t2200773556_StaticFields::get_offset_of_httpCollectUrl_1(),
	GoogleUniversalAnalytics_t2200773556_StaticFields::get_offset_of_httpsCollectUrl_2(),
	GoogleUniversalAnalytics_t2200773556_StaticFields::get_offset_of_guaVersionData_3(),
	GoogleUniversalAnalytics_t2200773556::get_offset_of_defaultHitData_4(),
	GoogleUniversalAnalytics_t2200773556::get_offset_of_sb_5(),
	GoogleUniversalAnalytics_t2200773556::get_offset_of_hitType_6(),
	GoogleUniversalAnalytics_t2200773556::get_offset_of_escapeString_7(),
	GoogleUniversalAnalytics_t2200773556_StaticFields::get_offset_of_instance_8(),
	GoogleUniversalAnalytics_t2200773556::get_offset_of_useHTTPS_9(),
	GoogleUniversalAnalytics_t2200773556::get_offset_of_helperBehaviour_10(),
	GoogleUniversalAnalytics_t2200773556::get_offset_of_trackingID_11(),
	GoogleUniversalAnalytics_t2200773556::get_offset_of_clientID_12(),
	GoogleUniversalAnalytics_t2200773556::get_offset_of_userID_13(),
	GoogleUniversalAnalytics_t2200773556::get_offset_of_userIP_14(),
	GoogleUniversalAnalytics_t2200773556::get_offset_of_userAgent_15(),
	GoogleUniversalAnalytics_t2200773556::get_offset_of_appName_16(),
	GoogleUniversalAnalytics_t2200773556::get_offset_of_appVersion_17(),
	GoogleUniversalAnalytics_t2200773556::get_offset_of_appID_18(),
	GoogleUniversalAnalytics_t2200773556::get_offset_of_analyticsDisabled__19(),
	GoogleUniversalAnalytics_t2200773556::get_offset_of_useOfflineCache_20(),
	GoogleUniversalAnalytics_t2200773556::get_offset_of_offlineCacheFilePath_21(),
	GoogleUniversalAnalytics_t2200773556::get_offset_of_offlineCacheReader_22(),
	GoogleUniversalAnalytics_t2200773556::get_offset_of_offlineCacheWriter_23(),
	GoogleUniversalAnalytics_t2200773556::get_offset_of_offlineQueueLength_24(),
	GoogleUniversalAnalytics_t2200773556::get_offset_of_offlineQueueSentHits_25(),
	GoogleUniversalAnalytics_t2200773556::get_offset_of_netAccessStatus_26(),
	GoogleUniversalAnalytics_t2200773556::get_offset_of_defaultReachabilityCheckPeriod_27(),
	GoogleUniversalAnalytics_t2200773556::get_offset_of_hitBeingBuiltRetryTime_28(),
	GoogleUniversalAnalytics_t2200773556::get_offset_of_netVerificationErrorRetryTime_29(),
	GoogleUniversalAnalytics_t2200773556::get_offset_of_netVerificationMismatchRetryTime_30(),
	GoogleUniversalAnalytics_t2200773556::get_offset_of_cachedHitSendThrottleTime_31(),
	GoogleUniversalAnalytics_t2200773556::get_offset_of_customHeaders_32(),
	GoogleUniversalAnalytics_t2200773556::get_offset_of_epoch_33(),
	GoogleUniversalAnalytics_t2200773556_StaticFields::get_offset_of_offlineQueueLengthPrefKey_34(),
	GoogleUniversalAnalytics_t2200773556_StaticFields::get_offset_of_offlineQueueSentHitsPrefKey_35(),
	GoogleUniversalAnalytics_t2200773556_StaticFields::get_offset_of_tabRowSplitter_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3693 = { sizeof (HitType_t1313315314)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3693[10] = 
{
	HitType_t1313315314::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3694 = { sizeof (NetAccessStatus_t4130471992)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3694[6] = 
{
	NetAccessStatus_t4130471992::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3695 = { sizeof (Delegate_escapeString_t2694597681), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3696 = { sizeof (U3CnetActivityU3Ec__Iterator33_t1845284094), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3696[7] = 
{
	U3CnetActivityU3Ec__Iterator33_t1845284094::get_offset_of_U3CprevReachabilityU3E__0_0(),
	U3CnetActivityU3Ec__Iterator33_t1845284094::get_offset_of_U3CnetworkReachabilityU3E__1_1(),
	U3CnetActivityU3Ec__Iterator33_t1845284094::get_offset_of_U3CwwwU3E__2_2(),
	U3CnetActivityU3Ec__Iterator33_t1845284094::get_offset_of_U3CresultU3E__3_3(),
	U3CnetActivityU3Ec__Iterator33_t1845284094::get_offset_of_U24PC_4(),
	U3CnetActivityU3Ec__Iterator33_t1845284094::get_offset_of_U24current_5(),
	U3CnetActivityU3Ec__Iterator33_t1845284094::get_offset_of_U3CU3Ef__this_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3697 = { sizeof (U3CdoWWWRequestAndCheckResultU3Ec__Iterator34_t3302283590), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3697[8] = 
{
	U3CdoWWWRequestAndCheckResultU3Ec__Iterator34_t3302283590::get_offset_of_postDataString_0(),
	U3CdoWWWRequestAndCheckResultU3Ec__Iterator34_t3302283590::get_offset_of_U3CwwwU3E__0_1(),
	U3CdoWWWRequestAndCheckResultU3Ec__Iterator34_t3302283590::get_offset_of_U3CsendFailed_saveToOfflineU3E__1_2(),
	U3CdoWWWRequestAndCheckResultU3Ec__Iterator34_t3302283590::get_offset_of_U3CresultU3E__2_3(),
	U3CdoWWWRequestAndCheckResultU3Ec__Iterator34_t3302283590::get_offset_of_U24PC_4(),
	U3CdoWWWRequestAndCheckResultU3Ec__Iterator34_t3302283590::get_offset_of_U24current_5(),
	U3CdoWWWRequestAndCheckResultU3Ec__Iterator34_t3302283590::get_offset_of_U3CU24U3EpostDataString_6(),
	U3CdoWWWRequestAndCheckResultU3Ec__Iterator34_t3302283590::get_offset_of_U3CU3Ef__this_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3698 = { sizeof (File_t3963205794), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3699 = { sizeof (AnalyticsExampleMagicTvGUI_t215054320), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3699[4] = 
{
	AnalyticsExampleMagicTvGUI_t215054320::get_offset_of__Analytics_2(),
	AnalyticsExampleMagicTvGUI_t215054320::get_offset_of_CurrentPageName_3(),
	AnalyticsExampleMagicTvGUI_t215054320::get_offset_of_trackingID_4(),
	AnalyticsExampleMagicTvGUI_t215054320::get_offset_of_appName_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
