﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetNextChild
struct GetNextChild_t464321883;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void HutongGames.PlayMaker.Actions.GetNextChild::.ctor()
extern "C"  void GetNextChild__ctor_m3560540011 (GetNextChild_t464321883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextChild::Reset()
extern "C"  void GetNextChild_Reset_m1206972952 (GetNextChild_t464321883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextChild::OnEnter()
extern "C"  void GetNextChild_OnEnter_m2276711554 (GetNextChild_t464321883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextChild::DoGetNextChild(UnityEngine.GameObject)
extern "C"  void GetNextChild_DoGetNextChild_m3232394895 (GetNextChild_t464321883 * __this, GameObject_t3674682005 * ___parent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
