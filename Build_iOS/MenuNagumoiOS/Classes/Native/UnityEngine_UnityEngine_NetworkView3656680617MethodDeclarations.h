﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.NetworkView
struct NetworkView_t3656680617;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_NetworkViewID3400394436.h"
#include "UnityEngine_UnityEngine_NetworkPlayer3231273765.h"

// System.Void UnityEngine.NetworkView::Internal_GetViewID(UnityEngine.NetworkViewID&)
extern "C"  void NetworkView_Internal_GetViewID_m3457400931 (NetworkView_t3656680617 * __this, NetworkViewID_t3400394436 * ___viewID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.NetworkViewID UnityEngine.NetworkView::get_viewID()
extern "C"  NetworkViewID_t3400394436  NetworkView_get_viewID_m148368939 (NetworkView_t3656680617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NetworkView::get_isMine()
extern "C"  bool NetworkView_get_isMine_m4033327326 (NetworkView_t3656680617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.NetworkPlayer UnityEngine.NetworkView::get_owner()
extern "C"  NetworkPlayer_t3231273765  NetworkView_get_owner_m1924541257 (NetworkView_t3656680617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.NetworkView UnityEngine.NetworkView::Find(UnityEngine.NetworkViewID)
extern "C"  NetworkView_t3656680617 * NetworkView_Find_m2926682619 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436  ___viewID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.NetworkView UnityEngine.NetworkView::INTERNAL_CALL_Find(UnityEngine.NetworkViewID&)
extern "C"  NetworkView_t3656680617 * NetworkView_INTERNAL_CALL_Find_m752431216 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436 * ___viewID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
