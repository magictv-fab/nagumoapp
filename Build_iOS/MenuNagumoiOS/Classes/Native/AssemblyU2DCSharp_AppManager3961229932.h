﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// ISampleAppUIEventHandler
struct ISampleAppUIEventHandler_t3468374354;
// SplashScreenView
struct SplashScreenView_t1797787416;
// AboutScreenView
struct AboutScreenView_t204522622;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_AppManager_ViewType4151742434.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AppManager
struct  AppManager_t3961229932  : public MonoBehaviour_t667441552
{
public:
	// System.String AppManager::TitleForAboutPage
	String_t* ___TitleForAboutPage_2;
	// ISampleAppUIEventHandler AppManager::m_UIEventHandler
	ISampleAppUIEventHandler_t3468374354 * ___m_UIEventHandler_3;
	// SplashScreenView AppManager::mSplashView
	SplashScreenView_t1797787416 * ___mSplashView_5;
	// AboutScreenView AppManager::mAboutView
	AboutScreenView_t204522622 * ___mAboutView_6;
	// System.Single AppManager::mSecondsVisible
	float ___mSecondsVisible_7;

public:
	inline static int32_t get_offset_of_TitleForAboutPage_2() { return static_cast<int32_t>(offsetof(AppManager_t3961229932, ___TitleForAboutPage_2)); }
	inline String_t* get_TitleForAboutPage_2() const { return ___TitleForAboutPage_2; }
	inline String_t** get_address_of_TitleForAboutPage_2() { return &___TitleForAboutPage_2; }
	inline void set_TitleForAboutPage_2(String_t* value)
	{
		___TitleForAboutPage_2 = value;
		Il2CppCodeGenWriteBarrier(&___TitleForAboutPage_2, value);
	}

	inline static int32_t get_offset_of_m_UIEventHandler_3() { return static_cast<int32_t>(offsetof(AppManager_t3961229932, ___m_UIEventHandler_3)); }
	inline ISampleAppUIEventHandler_t3468374354 * get_m_UIEventHandler_3() const { return ___m_UIEventHandler_3; }
	inline ISampleAppUIEventHandler_t3468374354 ** get_address_of_m_UIEventHandler_3() { return &___m_UIEventHandler_3; }
	inline void set_m_UIEventHandler_3(ISampleAppUIEventHandler_t3468374354 * value)
	{
		___m_UIEventHandler_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_UIEventHandler_3, value);
	}

	inline static int32_t get_offset_of_mSplashView_5() { return static_cast<int32_t>(offsetof(AppManager_t3961229932, ___mSplashView_5)); }
	inline SplashScreenView_t1797787416 * get_mSplashView_5() const { return ___mSplashView_5; }
	inline SplashScreenView_t1797787416 ** get_address_of_mSplashView_5() { return &___mSplashView_5; }
	inline void set_mSplashView_5(SplashScreenView_t1797787416 * value)
	{
		___mSplashView_5 = value;
		Il2CppCodeGenWriteBarrier(&___mSplashView_5, value);
	}

	inline static int32_t get_offset_of_mAboutView_6() { return static_cast<int32_t>(offsetof(AppManager_t3961229932, ___mAboutView_6)); }
	inline AboutScreenView_t204522622 * get_mAboutView_6() const { return ___mAboutView_6; }
	inline AboutScreenView_t204522622 ** get_address_of_mAboutView_6() { return &___mAboutView_6; }
	inline void set_mAboutView_6(AboutScreenView_t204522622 * value)
	{
		___mAboutView_6 = value;
		Il2CppCodeGenWriteBarrier(&___mAboutView_6, value);
	}

	inline static int32_t get_offset_of_mSecondsVisible_7() { return static_cast<int32_t>(offsetof(AppManager_t3961229932, ___mSecondsVisible_7)); }
	inline float get_mSecondsVisible_7() const { return ___mSecondsVisible_7; }
	inline float* get_address_of_mSecondsVisible_7() { return &___mSecondsVisible_7; }
	inline void set_mSecondsVisible_7(float value)
	{
		___mSecondsVisible_7 = value;
	}
};

struct AppManager_t3961229932_StaticFields
{
public:
	// AppManager/ViewType AppManager::mActiveViewType
	int32_t ___mActiveViewType_4;

public:
	inline static int32_t get_offset_of_mActiveViewType_4() { return static_cast<int32_t>(offsetof(AppManager_t3961229932_StaticFields, ___mActiveViewType_4)); }
	inline int32_t get_mActiveViewType_4() const { return ___mActiveViewType_4; }
	inline int32_t* get_address_of_mActiveViewType_4() { return &___mActiveViewType_4; }
	inline void set_mActiveViewType_4(int32_t value)
	{
		___mActiveViewType_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
