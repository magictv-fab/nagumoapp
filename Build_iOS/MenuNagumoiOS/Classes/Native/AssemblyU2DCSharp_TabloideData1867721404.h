﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TabloideData
struct  TabloideData_t1867721404  : public Il2CppObject
{
public:
	// System.Single TabloideData::valor
	float ___valor_0;
	// System.String TabloideData::imagem
	String_t* ___imagem_1;
	// System.String TabloideData::texto
	String_t* ___texto_2;
	// System.String TabloideData::link
	String_t* ___link_3;
	// System.Int32 TabloideData::id_tabloide
	int32_t ___id_tabloide_4;

public:
	inline static int32_t get_offset_of_valor_0() { return static_cast<int32_t>(offsetof(TabloideData_t1867721404, ___valor_0)); }
	inline float get_valor_0() const { return ___valor_0; }
	inline float* get_address_of_valor_0() { return &___valor_0; }
	inline void set_valor_0(float value)
	{
		___valor_0 = value;
	}

	inline static int32_t get_offset_of_imagem_1() { return static_cast<int32_t>(offsetof(TabloideData_t1867721404, ___imagem_1)); }
	inline String_t* get_imagem_1() const { return ___imagem_1; }
	inline String_t** get_address_of_imagem_1() { return &___imagem_1; }
	inline void set_imagem_1(String_t* value)
	{
		___imagem_1 = value;
		Il2CppCodeGenWriteBarrier(&___imagem_1, value);
	}

	inline static int32_t get_offset_of_texto_2() { return static_cast<int32_t>(offsetof(TabloideData_t1867721404, ___texto_2)); }
	inline String_t* get_texto_2() const { return ___texto_2; }
	inline String_t** get_address_of_texto_2() { return &___texto_2; }
	inline void set_texto_2(String_t* value)
	{
		___texto_2 = value;
		Il2CppCodeGenWriteBarrier(&___texto_2, value);
	}

	inline static int32_t get_offset_of_link_3() { return static_cast<int32_t>(offsetof(TabloideData_t1867721404, ___link_3)); }
	inline String_t* get_link_3() const { return ___link_3; }
	inline String_t** get_address_of_link_3() { return &___link_3; }
	inline void set_link_3(String_t* value)
	{
		___link_3 = value;
		Il2CppCodeGenWriteBarrier(&___link_3, value);
	}

	inline static int32_t get_offset_of_id_tabloide_4() { return static_cast<int32_t>(offsetof(TabloideData_t1867721404, ___id_tabloide_4)); }
	inline int32_t get_id_tabloide_4() const { return ___id_tabloide_4; }
	inline int32_t* get_address_of_id_tabloide_4() { return &___id_tabloide_4; }
	inline void set_id_tabloide_4(int32_t value)
	{
		___id_tabloide_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
