﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// UnityEngine.AudioClip
struct AudioClip_t794140988;
// System.Collections.Generic.List`1<System.Int64>
struct List_1_t2522024147;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Area730.Notifications.NotificationInstance
struct  NotificationInstance_t3873938600  : public Il2CppObject
{
public:
	// System.String Area730.Notifications.NotificationInstance::name
	String_t* ___name_0;
	// UnityEngine.Texture2D Area730.Notifications.NotificationInstance::smallIcon
	Texture2D_t3884108195 * ___smallIcon_1;
	// UnityEngine.Texture2D Area730.Notifications.NotificationInstance::largeIcon
	Texture2D_t3884108195 * ___largeIcon_2;
	// System.String Area730.Notifications.NotificationInstance::title
	String_t* ___title_3;
	// System.String Area730.Notifications.NotificationInstance::body
	String_t* ___body_4;
	// System.String Area730.Notifications.NotificationInstance::ticker
	String_t* ___ticker_5;
	// System.Boolean Area730.Notifications.NotificationInstance::autoCancel
	bool ___autoCancel_6;
	// System.Boolean Area730.Notifications.NotificationInstance::isRepeating
	bool ___isRepeating_7;
	// System.Int32 Area730.Notifications.NotificationInstance::intervalHours
	int32_t ___intervalHours_8;
	// System.Int32 Area730.Notifications.NotificationInstance::intervalMinutes
	int32_t ___intervalMinutes_9;
	// System.Int32 Area730.Notifications.NotificationInstance::intervalSeconds
	int32_t ___intervalSeconds_10;
	// System.Boolean Area730.Notifications.NotificationInstance::alertOnce
	bool ___alertOnce_11;
	// System.Int32 Area730.Notifications.NotificationInstance::number
	int32_t ___number_12;
	// System.Int32 Area730.Notifications.NotificationInstance::delayHours
	int32_t ___delayHours_13;
	// System.Int32 Area730.Notifications.NotificationInstance::delayMinutes
	int32_t ___delayMinutes_14;
	// System.Int32 Area730.Notifications.NotificationInstance::delaySeconds
	int32_t ___delaySeconds_15;
	// System.Boolean Area730.Notifications.NotificationInstance::defaultSound
	bool ___defaultSound_16;
	// System.Boolean Area730.Notifications.NotificationInstance::defaultVibrate
	bool ___defaultVibrate_17;
	// UnityEngine.AudioClip Area730.Notifications.NotificationInstance::soundFile
	AudioClip_t794140988 * ___soundFile_18;
	// System.Collections.Generic.List`1<System.Int64> Area730.Notifications.NotificationInstance::vibroPattern
	List_1_t2522024147 * ___vibroPattern_19;
	// System.String Area730.Notifications.NotificationInstance::group
	String_t* ___group_20;
	// System.String Area730.Notifications.NotificationInstance::sortKey
	String_t* ___sortKey_21;
	// System.Boolean Area730.Notifications.NotificationInstance::hasColor
	bool ___hasColor_22;
	// UnityEngine.Color Area730.Notifications.NotificationInstance::color
	Color_t4194546905  ___color_23;
	// System.Int32 Area730.Notifications.NotificationInstance::id
	int32_t ___id_24;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(NotificationInstance_t3873938600, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_smallIcon_1() { return static_cast<int32_t>(offsetof(NotificationInstance_t3873938600, ___smallIcon_1)); }
	inline Texture2D_t3884108195 * get_smallIcon_1() const { return ___smallIcon_1; }
	inline Texture2D_t3884108195 ** get_address_of_smallIcon_1() { return &___smallIcon_1; }
	inline void set_smallIcon_1(Texture2D_t3884108195 * value)
	{
		___smallIcon_1 = value;
		Il2CppCodeGenWriteBarrier(&___smallIcon_1, value);
	}

	inline static int32_t get_offset_of_largeIcon_2() { return static_cast<int32_t>(offsetof(NotificationInstance_t3873938600, ___largeIcon_2)); }
	inline Texture2D_t3884108195 * get_largeIcon_2() const { return ___largeIcon_2; }
	inline Texture2D_t3884108195 ** get_address_of_largeIcon_2() { return &___largeIcon_2; }
	inline void set_largeIcon_2(Texture2D_t3884108195 * value)
	{
		___largeIcon_2 = value;
		Il2CppCodeGenWriteBarrier(&___largeIcon_2, value);
	}

	inline static int32_t get_offset_of_title_3() { return static_cast<int32_t>(offsetof(NotificationInstance_t3873938600, ___title_3)); }
	inline String_t* get_title_3() const { return ___title_3; }
	inline String_t** get_address_of_title_3() { return &___title_3; }
	inline void set_title_3(String_t* value)
	{
		___title_3 = value;
		Il2CppCodeGenWriteBarrier(&___title_3, value);
	}

	inline static int32_t get_offset_of_body_4() { return static_cast<int32_t>(offsetof(NotificationInstance_t3873938600, ___body_4)); }
	inline String_t* get_body_4() const { return ___body_4; }
	inline String_t** get_address_of_body_4() { return &___body_4; }
	inline void set_body_4(String_t* value)
	{
		___body_4 = value;
		Il2CppCodeGenWriteBarrier(&___body_4, value);
	}

	inline static int32_t get_offset_of_ticker_5() { return static_cast<int32_t>(offsetof(NotificationInstance_t3873938600, ___ticker_5)); }
	inline String_t* get_ticker_5() const { return ___ticker_5; }
	inline String_t** get_address_of_ticker_5() { return &___ticker_5; }
	inline void set_ticker_5(String_t* value)
	{
		___ticker_5 = value;
		Il2CppCodeGenWriteBarrier(&___ticker_5, value);
	}

	inline static int32_t get_offset_of_autoCancel_6() { return static_cast<int32_t>(offsetof(NotificationInstance_t3873938600, ___autoCancel_6)); }
	inline bool get_autoCancel_6() const { return ___autoCancel_6; }
	inline bool* get_address_of_autoCancel_6() { return &___autoCancel_6; }
	inline void set_autoCancel_6(bool value)
	{
		___autoCancel_6 = value;
	}

	inline static int32_t get_offset_of_isRepeating_7() { return static_cast<int32_t>(offsetof(NotificationInstance_t3873938600, ___isRepeating_7)); }
	inline bool get_isRepeating_7() const { return ___isRepeating_7; }
	inline bool* get_address_of_isRepeating_7() { return &___isRepeating_7; }
	inline void set_isRepeating_7(bool value)
	{
		___isRepeating_7 = value;
	}

	inline static int32_t get_offset_of_intervalHours_8() { return static_cast<int32_t>(offsetof(NotificationInstance_t3873938600, ___intervalHours_8)); }
	inline int32_t get_intervalHours_8() const { return ___intervalHours_8; }
	inline int32_t* get_address_of_intervalHours_8() { return &___intervalHours_8; }
	inline void set_intervalHours_8(int32_t value)
	{
		___intervalHours_8 = value;
	}

	inline static int32_t get_offset_of_intervalMinutes_9() { return static_cast<int32_t>(offsetof(NotificationInstance_t3873938600, ___intervalMinutes_9)); }
	inline int32_t get_intervalMinutes_9() const { return ___intervalMinutes_9; }
	inline int32_t* get_address_of_intervalMinutes_9() { return &___intervalMinutes_9; }
	inline void set_intervalMinutes_9(int32_t value)
	{
		___intervalMinutes_9 = value;
	}

	inline static int32_t get_offset_of_intervalSeconds_10() { return static_cast<int32_t>(offsetof(NotificationInstance_t3873938600, ___intervalSeconds_10)); }
	inline int32_t get_intervalSeconds_10() const { return ___intervalSeconds_10; }
	inline int32_t* get_address_of_intervalSeconds_10() { return &___intervalSeconds_10; }
	inline void set_intervalSeconds_10(int32_t value)
	{
		___intervalSeconds_10 = value;
	}

	inline static int32_t get_offset_of_alertOnce_11() { return static_cast<int32_t>(offsetof(NotificationInstance_t3873938600, ___alertOnce_11)); }
	inline bool get_alertOnce_11() const { return ___alertOnce_11; }
	inline bool* get_address_of_alertOnce_11() { return &___alertOnce_11; }
	inline void set_alertOnce_11(bool value)
	{
		___alertOnce_11 = value;
	}

	inline static int32_t get_offset_of_number_12() { return static_cast<int32_t>(offsetof(NotificationInstance_t3873938600, ___number_12)); }
	inline int32_t get_number_12() const { return ___number_12; }
	inline int32_t* get_address_of_number_12() { return &___number_12; }
	inline void set_number_12(int32_t value)
	{
		___number_12 = value;
	}

	inline static int32_t get_offset_of_delayHours_13() { return static_cast<int32_t>(offsetof(NotificationInstance_t3873938600, ___delayHours_13)); }
	inline int32_t get_delayHours_13() const { return ___delayHours_13; }
	inline int32_t* get_address_of_delayHours_13() { return &___delayHours_13; }
	inline void set_delayHours_13(int32_t value)
	{
		___delayHours_13 = value;
	}

	inline static int32_t get_offset_of_delayMinutes_14() { return static_cast<int32_t>(offsetof(NotificationInstance_t3873938600, ___delayMinutes_14)); }
	inline int32_t get_delayMinutes_14() const { return ___delayMinutes_14; }
	inline int32_t* get_address_of_delayMinutes_14() { return &___delayMinutes_14; }
	inline void set_delayMinutes_14(int32_t value)
	{
		___delayMinutes_14 = value;
	}

	inline static int32_t get_offset_of_delaySeconds_15() { return static_cast<int32_t>(offsetof(NotificationInstance_t3873938600, ___delaySeconds_15)); }
	inline int32_t get_delaySeconds_15() const { return ___delaySeconds_15; }
	inline int32_t* get_address_of_delaySeconds_15() { return &___delaySeconds_15; }
	inline void set_delaySeconds_15(int32_t value)
	{
		___delaySeconds_15 = value;
	}

	inline static int32_t get_offset_of_defaultSound_16() { return static_cast<int32_t>(offsetof(NotificationInstance_t3873938600, ___defaultSound_16)); }
	inline bool get_defaultSound_16() const { return ___defaultSound_16; }
	inline bool* get_address_of_defaultSound_16() { return &___defaultSound_16; }
	inline void set_defaultSound_16(bool value)
	{
		___defaultSound_16 = value;
	}

	inline static int32_t get_offset_of_defaultVibrate_17() { return static_cast<int32_t>(offsetof(NotificationInstance_t3873938600, ___defaultVibrate_17)); }
	inline bool get_defaultVibrate_17() const { return ___defaultVibrate_17; }
	inline bool* get_address_of_defaultVibrate_17() { return &___defaultVibrate_17; }
	inline void set_defaultVibrate_17(bool value)
	{
		___defaultVibrate_17 = value;
	}

	inline static int32_t get_offset_of_soundFile_18() { return static_cast<int32_t>(offsetof(NotificationInstance_t3873938600, ___soundFile_18)); }
	inline AudioClip_t794140988 * get_soundFile_18() const { return ___soundFile_18; }
	inline AudioClip_t794140988 ** get_address_of_soundFile_18() { return &___soundFile_18; }
	inline void set_soundFile_18(AudioClip_t794140988 * value)
	{
		___soundFile_18 = value;
		Il2CppCodeGenWriteBarrier(&___soundFile_18, value);
	}

	inline static int32_t get_offset_of_vibroPattern_19() { return static_cast<int32_t>(offsetof(NotificationInstance_t3873938600, ___vibroPattern_19)); }
	inline List_1_t2522024147 * get_vibroPattern_19() const { return ___vibroPattern_19; }
	inline List_1_t2522024147 ** get_address_of_vibroPattern_19() { return &___vibroPattern_19; }
	inline void set_vibroPattern_19(List_1_t2522024147 * value)
	{
		___vibroPattern_19 = value;
		Il2CppCodeGenWriteBarrier(&___vibroPattern_19, value);
	}

	inline static int32_t get_offset_of_group_20() { return static_cast<int32_t>(offsetof(NotificationInstance_t3873938600, ___group_20)); }
	inline String_t* get_group_20() const { return ___group_20; }
	inline String_t** get_address_of_group_20() { return &___group_20; }
	inline void set_group_20(String_t* value)
	{
		___group_20 = value;
		Il2CppCodeGenWriteBarrier(&___group_20, value);
	}

	inline static int32_t get_offset_of_sortKey_21() { return static_cast<int32_t>(offsetof(NotificationInstance_t3873938600, ___sortKey_21)); }
	inline String_t* get_sortKey_21() const { return ___sortKey_21; }
	inline String_t** get_address_of_sortKey_21() { return &___sortKey_21; }
	inline void set_sortKey_21(String_t* value)
	{
		___sortKey_21 = value;
		Il2CppCodeGenWriteBarrier(&___sortKey_21, value);
	}

	inline static int32_t get_offset_of_hasColor_22() { return static_cast<int32_t>(offsetof(NotificationInstance_t3873938600, ___hasColor_22)); }
	inline bool get_hasColor_22() const { return ___hasColor_22; }
	inline bool* get_address_of_hasColor_22() { return &___hasColor_22; }
	inline void set_hasColor_22(bool value)
	{
		___hasColor_22 = value;
	}

	inline static int32_t get_offset_of_color_23() { return static_cast<int32_t>(offsetof(NotificationInstance_t3873938600, ___color_23)); }
	inline Color_t4194546905  get_color_23() const { return ___color_23; }
	inline Color_t4194546905 * get_address_of_color_23() { return &___color_23; }
	inline void set_color_23(Color_t4194546905  value)
	{
		___color_23 = value;
	}

	inline static int32_t get_offset_of_id_24() { return static_cast<int32_t>(offsetof(NotificationInstance_t3873938600, ___id_24)); }
	inline int32_t get_id_24() const { return ___id_24; }
	inline int32_t* get_address_of_id_24() { return &___id_24; }
	inline void set_id_24(int32_t value)
	{
		___id_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
