﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmAnimationCurve
struct FsmAnimationCurve_t2685995989;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.FsmAnimationCurve::.ctor()
extern "C"  void FsmAnimationCurve__ctor_m1684122154 (FsmAnimationCurve_t2685995989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
