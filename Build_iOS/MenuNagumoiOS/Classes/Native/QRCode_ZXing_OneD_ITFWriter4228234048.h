﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "QRCode_ZXing_OneD_OneDimensionalCodeWriter4068326409.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.ITFWriter
struct  ITFWriter_t4228234048  : public OneDimensionalCodeWriter_t4068326409
{
public:

public:
};

struct ITFWriter_t4228234048_StaticFields
{
public:
	// System.Int32[] ZXing.OneD.ITFWriter::START_PATTERN
	Int32U5BU5D_t3230847821* ___START_PATTERN_0;
	// System.Int32[] ZXing.OneD.ITFWriter::END_PATTERN
	Int32U5BU5D_t3230847821* ___END_PATTERN_1;

public:
	inline static int32_t get_offset_of_START_PATTERN_0() { return static_cast<int32_t>(offsetof(ITFWriter_t4228234048_StaticFields, ___START_PATTERN_0)); }
	inline Int32U5BU5D_t3230847821* get_START_PATTERN_0() const { return ___START_PATTERN_0; }
	inline Int32U5BU5D_t3230847821** get_address_of_START_PATTERN_0() { return &___START_PATTERN_0; }
	inline void set_START_PATTERN_0(Int32U5BU5D_t3230847821* value)
	{
		___START_PATTERN_0 = value;
		Il2CppCodeGenWriteBarrier(&___START_PATTERN_0, value);
	}

	inline static int32_t get_offset_of_END_PATTERN_1() { return static_cast<int32_t>(offsetof(ITFWriter_t4228234048_StaticFields, ___END_PATTERN_1)); }
	inline Int32U5BU5D_t3230847821* get_END_PATTERN_1() const { return ___END_PATTERN_1; }
	inline Int32U5BU5D_t3230847821** get_address_of_END_PATTERN_1() { return &___END_PATTERN_1; }
	inline void set_END_PATTERN_1(Int32U5BU5D_t3230847821* value)
	{
		___END_PATTERN_1 = value;
		Il2CppCodeGenWriteBarrier(&___END_PATTERN_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
