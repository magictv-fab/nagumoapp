﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<MagicTV.download.BundleDownloadItem>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3478246507(__this, ___l0, method) ((  void (*) (Enumerator_t2103796691 *, List_1_t2084123921 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<MagicTV.download.BundleDownloadItem>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4097763591(__this, method) ((  void (*) (Enumerator_t2103796691 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<MagicTV.download.BundleDownloadItem>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3260221107(__this, method) ((  Il2CppObject * (*) (Enumerator_t2103796691 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<MagicTV.download.BundleDownloadItem>::Dispose()
#define Enumerator_Dispose_m2998791440(__this, method) ((  void (*) (Enumerator_t2103796691 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<MagicTV.download.BundleDownloadItem>::VerifyState()
#define Enumerator_VerifyState_m2182390857(__this, method) ((  void (*) (Enumerator_t2103796691 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<MagicTV.download.BundleDownloadItem>::MoveNext()
#define Enumerator_MoveNext_m1759983347(__this, method) ((  bool (*) (Enumerator_t2103796691 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<MagicTV.download.BundleDownloadItem>::get_Current()
#define Enumerator_get_Current_m688628480(__this, method) ((  BundleDownloadItem_t715938369 * (*) (Enumerator_t2103796691 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
