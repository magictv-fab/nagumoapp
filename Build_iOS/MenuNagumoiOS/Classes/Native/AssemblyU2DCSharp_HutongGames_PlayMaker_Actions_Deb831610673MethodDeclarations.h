﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DebugFsmVariable
struct DebugFsmVariable_t831610673;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DebugFsmVariable::.ctor()
extern "C"  void DebugFsmVariable__ctor_m479267413 (DebugFsmVariable_t831610673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DebugFsmVariable::Reset()
extern "C"  void DebugFsmVariable_Reset_m2420667650 (DebugFsmVariable_t831610673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DebugFsmVariable::OnEnter()
extern "C"  void DebugFsmVariable_OnEnter_m406211820 (DebugFsmVariable_t831610673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
