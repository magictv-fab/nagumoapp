﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>
struct Dictionary_2_t2510884092;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3125820146.h"
#include "AssemblyU2DCSharp_MagicTV_globals_Perspective644553802.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<MagicTV.globals.Perspective,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1003905940_gshared (Enumerator_t3125820146 * __this, Dictionary_2_t2510884092 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m1003905940(__this, ___host0, method) ((  void (*) (Enumerator_t3125820146 *, Dictionary_2_t2510884092 *, const MethodInfo*))Enumerator__ctor_m1003905940_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<MagicTV.globals.Perspective,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1581710093_gshared (Enumerator_t3125820146 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1581710093(__this, method) ((  Il2CppObject * (*) (Enumerator_t3125820146 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1581710093_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<MagicTV.globals.Perspective,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2019399841_gshared (Enumerator_t3125820146 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2019399841(__this, method) ((  void (*) (Enumerator_t3125820146 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2019399841_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<MagicTV.globals.Perspective,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2723477302_gshared (Enumerator_t3125820146 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2723477302(__this, method) ((  void (*) (Enumerator_t3125820146 *, const MethodInfo*))Enumerator_Dispose_m2723477302_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<MagicTV.globals.Perspective,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3215032333_gshared (Enumerator_t3125820146 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3215032333(__this, method) ((  bool (*) (Enumerator_t3125820146 *, const MethodInfo*))Enumerator_MoveNext_m3215032333_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<MagicTV.globals.Perspective,System.Object>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2728643495_gshared (Enumerator_t3125820146 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2728643495(__this, method) ((  int32_t (*) (Enumerator_t3125820146 *, const MethodInfo*))Enumerator_get_Current_m2728643495_gshared)(__this, method)
