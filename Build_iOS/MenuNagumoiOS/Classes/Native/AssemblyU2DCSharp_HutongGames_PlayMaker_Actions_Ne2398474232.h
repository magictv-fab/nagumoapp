﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "UnityEngine_UnityEngine_NetworkPlayer3231273765.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkGetLastPing
struct  NetworkGetLastPing_t2398474232  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.NetworkGetLastPing::playerIndex
	FsmInt_t1596138449 * ___playerIndex_9;
	// System.Boolean HutongGames.PlayMaker.Actions.NetworkGetLastPing::cachePlayerReference
	bool ___cachePlayerReference_10;
	// System.Boolean HutongGames.PlayMaker.Actions.NetworkGetLastPing::everyFrame
	bool ___everyFrame_11;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.NetworkGetLastPing::lastPing
	FsmInt_t1596138449 * ___lastPing_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetLastPing::PlayerNotFoundEvent
	FsmEvent_t2133468028 * ___PlayerNotFoundEvent_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetLastPing::PlayerFoundEvent
	FsmEvent_t2133468028 * ___PlayerFoundEvent_14;
	// UnityEngine.NetworkPlayer HutongGames.PlayMaker.Actions.NetworkGetLastPing::_player
	NetworkPlayer_t3231273765  ____player_15;

public:
	inline static int32_t get_offset_of_playerIndex_9() { return static_cast<int32_t>(offsetof(NetworkGetLastPing_t2398474232, ___playerIndex_9)); }
	inline FsmInt_t1596138449 * get_playerIndex_9() const { return ___playerIndex_9; }
	inline FsmInt_t1596138449 ** get_address_of_playerIndex_9() { return &___playerIndex_9; }
	inline void set_playerIndex_9(FsmInt_t1596138449 * value)
	{
		___playerIndex_9 = value;
		Il2CppCodeGenWriteBarrier(&___playerIndex_9, value);
	}

	inline static int32_t get_offset_of_cachePlayerReference_10() { return static_cast<int32_t>(offsetof(NetworkGetLastPing_t2398474232, ___cachePlayerReference_10)); }
	inline bool get_cachePlayerReference_10() const { return ___cachePlayerReference_10; }
	inline bool* get_address_of_cachePlayerReference_10() { return &___cachePlayerReference_10; }
	inline void set_cachePlayerReference_10(bool value)
	{
		___cachePlayerReference_10 = value;
	}

	inline static int32_t get_offset_of_everyFrame_11() { return static_cast<int32_t>(offsetof(NetworkGetLastPing_t2398474232, ___everyFrame_11)); }
	inline bool get_everyFrame_11() const { return ___everyFrame_11; }
	inline bool* get_address_of_everyFrame_11() { return &___everyFrame_11; }
	inline void set_everyFrame_11(bool value)
	{
		___everyFrame_11 = value;
	}

	inline static int32_t get_offset_of_lastPing_12() { return static_cast<int32_t>(offsetof(NetworkGetLastPing_t2398474232, ___lastPing_12)); }
	inline FsmInt_t1596138449 * get_lastPing_12() const { return ___lastPing_12; }
	inline FsmInt_t1596138449 ** get_address_of_lastPing_12() { return &___lastPing_12; }
	inline void set_lastPing_12(FsmInt_t1596138449 * value)
	{
		___lastPing_12 = value;
		Il2CppCodeGenWriteBarrier(&___lastPing_12, value);
	}

	inline static int32_t get_offset_of_PlayerNotFoundEvent_13() { return static_cast<int32_t>(offsetof(NetworkGetLastPing_t2398474232, ___PlayerNotFoundEvent_13)); }
	inline FsmEvent_t2133468028 * get_PlayerNotFoundEvent_13() const { return ___PlayerNotFoundEvent_13; }
	inline FsmEvent_t2133468028 ** get_address_of_PlayerNotFoundEvent_13() { return &___PlayerNotFoundEvent_13; }
	inline void set_PlayerNotFoundEvent_13(FsmEvent_t2133468028 * value)
	{
		___PlayerNotFoundEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___PlayerNotFoundEvent_13, value);
	}

	inline static int32_t get_offset_of_PlayerFoundEvent_14() { return static_cast<int32_t>(offsetof(NetworkGetLastPing_t2398474232, ___PlayerFoundEvent_14)); }
	inline FsmEvent_t2133468028 * get_PlayerFoundEvent_14() const { return ___PlayerFoundEvent_14; }
	inline FsmEvent_t2133468028 ** get_address_of_PlayerFoundEvent_14() { return &___PlayerFoundEvent_14; }
	inline void set_PlayerFoundEvent_14(FsmEvent_t2133468028 * value)
	{
		___PlayerFoundEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___PlayerFoundEvent_14, value);
	}

	inline static int32_t get_offset_of__player_15() { return static_cast<int32_t>(offsetof(NetworkGetLastPing_t2398474232, ____player_15)); }
	inline NetworkPlayer_t3231273765  get__player_15() const { return ____player_15; }
	inline NetworkPlayer_t3231273765 * get_address_of__player_15() { return &____player_15; }
	inline void set__player_15(NetworkPlayer_t3231273765  value)
	{
		____player_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
