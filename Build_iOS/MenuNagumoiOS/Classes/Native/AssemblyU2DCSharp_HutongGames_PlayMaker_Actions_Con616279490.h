﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmMaterial
struct FsmMaterial_t924399665;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t821476169;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ConvertMaterialToObject
struct  ConvertMaterialToObject_t616279490  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Actions.ConvertMaterialToObject::materialVariable
	FsmMaterial_t924399665 * ___materialVariable_9;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.ConvertMaterialToObject::objectVariable
	FsmObject_t821476169 * ___objectVariable_10;
	// System.Boolean HutongGames.PlayMaker.Actions.ConvertMaterialToObject::everyFrame
	bool ___everyFrame_11;

public:
	inline static int32_t get_offset_of_materialVariable_9() { return static_cast<int32_t>(offsetof(ConvertMaterialToObject_t616279490, ___materialVariable_9)); }
	inline FsmMaterial_t924399665 * get_materialVariable_9() const { return ___materialVariable_9; }
	inline FsmMaterial_t924399665 ** get_address_of_materialVariable_9() { return &___materialVariable_9; }
	inline void set_materialVariable_9(FsmMaterial_t924399665 * value)
	{
		___materialVariable_9 = value;
		Il2CppCodeGenWriteBarrier(&___materialVariable_9, value);
	}

	inline static int32_t get_offset_of_objectVariable_10() { return static_cast<int32_t>(offsetof(ConvertMaterialToObject_t616279490, ___objectVariable_10)); }
	inline FsmObject_t821476169 * get_objectVariable_10() const { return ___objectVariable_10; }
	inline FsmObject_t821476169 ** get_address_of_objectVariable_10() { return &___objectVariable_10; }
	inline void set_objectVariable_10(FsmObject_t821476169 * value)
	{
		___objectVariable_10 = value;
		Il2CppCodeGenWriteBarrier(&___objectVariable_10, value);
	}

	inline static int32_t get_offset_of_everyFrame_11() { return static_cast<int32_t>(offsetof(ConvertMaterialToObject_t616279490, ___everyFrame_11)); }
	inline bool get_everyFrame_11() const { return ___everyFrame_11; }
	inline bool* get_address_of_everyFrame_11() { return &___everyFrame_11; }
	inline void set_everyFrame_11(bool value)
	{
		___everyFrame_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
