﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.BroadcastEvent
struct  BroadcastEvent_t3531411777  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.BroadcastEvent::broadcastEvent
	FsmString_t952858651 * ___broadcastEvent_9;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.BroadcastEvent::gameObject
	FsmGameObject_t1697147867 * ___gameObject_10;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.BroadcastEvent::sendToChildren
	FsmBool_t1075959796 * ___sendToChildren_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.BroadcastEvent::excludeSelf
	FsmBool_t1075959796 * ___excludeSelf_12;

public:
	inline static int32_t get_offset_of_broadcastEvent_9() { return static_cast<int32_t>(offsetof(BroadcastEvent_t3531411777, ___broadcastEvent_9)); }
	inline FsmString_t952858651 * get_broadcastEvent_9() const { return ___broadcastEvent_9; }
	inline FsmString_t952858651 ** get_address_of_broadcastEvent_9() { return &___broadcastEvent_9; }
	inline void set_broadcastEvent_9(FsmString_t952858651 * value)
	{
		___broadcastEvent_9 = value;
		Il2CppCodeGenWriteBarrier(&___broadcastEvent_9, value);
	}

	inline static int32_t get_offset_of_gameObject_10() { return static_cast<int32_t>(offsetof(BroadcastEvent_t3531411777, ___gameObject_10)); }
	inline FsmGameObject_t1697147867 * get_gameObject_10() const { return ___gameObject_10; }
	inline FsmGameObject_t1697147867 ** get_address_of_gameObject_10() { return &___gameObject_10; }
	inline void set_gameObject_10(FsmGameObject_t1697147867 * value)
	{
		___gameObject_10 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_10, value);
	}

	inline static int32_t get_offset_of_sendToChildren_11() { return static_cast<int32_t>(offsetof(BroadcastEvent_t3531411777, ___sendToChildren_11)); }
	inline FsmBool_t1075959796 * get_sendToChildren_11() const { return ___sendToChildren_11; }
	inline FsmBool_t1075959796 ** get_address_of_sendToChildren_11() { return &___sendToChildren_11; }
	inline void set_sendToChildren_11(FsmBool_t1075959796 * value)
	{
		___sendToChildren_11 = value;
		Il2CppCodeGenWriteBarrier(&___sendToChildren_11, value);
	}

	inline static int32_t get_offset_of_excludeSelf_12() { return static_cast<int32_t>(offsetof(BroadcastEvent_t3531411777, ___excludeSelf_12)); }
	inline FsmBool_t1075959796 * get_excludeSelf_12() const { return ___excludeSelf_12; }
	inline FsmBool_t1075959796 ** get_address_of_excludeSelf_12() { return &___excludeSelf_12; }
	inline void set_excludeSelf_12(FsmBool_t1075959796 * value)
	{
		___excludeSelf_12 = value;
		Il2CppCodeGenWriteBarrier(&___excludeSelf_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
