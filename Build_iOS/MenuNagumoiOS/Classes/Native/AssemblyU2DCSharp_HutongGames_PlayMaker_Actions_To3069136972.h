﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// UnityEngine.GUIElement
struct GUIElement_t3775428101;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_To3978919677.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.TouchGUIEvent
struct  TouchGUIEvent_t3069136972  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.TouchGUIEvent::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.TouchGUIEvent::fingerId
	FsmInt_t1596138449 * ___fingerId_10;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.TouchGUIEvent::touchBegan
	FsmEvent_t2133468028 * ___touchBegan_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.TouchGUIEvent::touchMoved
	FsmEvent_t2133468028 * ___touchMoved_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.TouchGUIEvent::touchStationary
	FsmEvent_t2133468028 * ___touchStationary_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.TouchGUIEvent::touchEnded
	FsmEvent_t2133468028 * ___touchEnded_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.TouchGUIEvent::touchCanceled
	FsmEvent_t2133468028 * ___touchCanceled_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.TouchGUIEvent::notTouching
	FsmEvent_t2133468028 * ___notTouching_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.TouchGUIEvent::storeFingerId
	FsmInt_t1596138449 * ___storeFingerId_17;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.TouchGUIEvent::storeHitPoint
	FsmVector3_t533912882 * ___storeHitPoint_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.TouchGUIEvent::normalizeHitPoint
	FsmBool_t1075959796 * ___normalizeHitPoint_19;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.TouchGUIEvent::storeOffset
	FsmVector3_t533912882 * ___storeOffset_20;
	// HutongGames.PlayMaker.Actions.TouchGUIEvent/OffsetOptions HutongGames.PlayMaker.Actions.TouchGUIEvent::relativeTo
	int32_t ___relativeTo_21;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.TouchGUIEvent::normalizeOffset
	FsmBool_t1075959796 * ___normalizeOffset_22;
	// System.Boolean HutongGames.PlayMaker.Actions.TouchGUIEvent::everyFrame
	bool ___everyFrame_23;
	// UnityEngine.Vector3 HutongGames.PlayMaker.Actions.TouchGUIEvent::touchStartPos
	Vector3_t4282066566  ___touchStartPos_24;
	// UnityEngine.GUIElement HutongGames.PlayMaker.Actions.TouchGUIEvent::guiElement
	GUIElement_t3775428101 * ___guiElement_25;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(TouchGUIEvent_t3069136972, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_fingerId_10() { return static_cast<int32_t>(offsetof(TouchGUIEvent_t3069136972, ___fingerId_10)); }
	inline FsmInt_t1596138449 * get_fingerId_10() const { return ___fingerId_10; }
	inline FsmInt_t1596138449 ** get_address_of_fingerId_10() { return &___fingerId_10; }
	inline void set_fingerId_10(FsmInt_t1596138449 * value)
	{
		___fingerId_10 = value;
		Il2CppCodeGenWriteBarrier(&___fingerId_10, value);
	}

	inline static int32_t get_offset_of_touchBegan_11() { return static_cast<int32_t>(offsetof(TouchGUIEvent_t3069136972, ___touchBegan_11)); }
	inline FsmEvent_t2133468028 * get_touchBegan_11() const { return ___touchBegan_11; }
	inline FsmEvent_t2133468028 ** get_address_of_touchBegan_11() { return &___touchBegan_11; }
	inline void set_touchBegan_11(FsmEvent_t2133468028 * value)
	{
		___touchBegan_11 = value;
		Il2CppCodeGenWriteBarrier(&___touchBegan_11, value);
	}

	inline static int32_t get_offset_of_touchMoved_12() { return static_cast<int32_t>(offsetof(TouchGUIEvent_t3069136972, ___touchMoved_12)); }
	inline FsmEvent_t2133468028 * get_touchMoved_12() const { return ___touchMoved_12; }
	inline FsmEvent_t2133468028 ** get_address_of_touchMoved_12() { return &___touchMoved_12; }
	inline void set_touchMoved_12(FsmEvent_t2133468028 * value)
	{
		___touchMoved_12 = value;
		Il2CppCodeGenWriteBarrier(&___touchMoved_12, value);
	}

	inline static int32_t get_offset_of_touchStationary_13() { return static_cast<int32_t>(offsetof(TouchGUIEvent_t3069136972, ___touchStationary_13)); }
	inline FsmEvent_t2133468028 * get_touchStationary_13() const { return ___touchStationary_13; }
	inline FsmEvent_t2133468028 ** get_address_of_touchStationary_13() { return &___touchStationary_13; }
	inline void set_touchStationary_13(FsmEvent_t2133468028 * value)
	{
		___touchStationary_13 = value;
		Il2CppCodeGenWriteBarrier(&___touchStationary_13, value);
	}

	inline static int32_t get_offset_of_touchEnded_14() { return static_cast<int32_t>(offsetof(TouchGUIEvent_t3069136972, ___touchEnded_14)); }
	inline FsmEvent_t2133468028 * get_touchEnded_14() const { return ___touchEnded_14; }
	inline FsmEvent_t2133468028 ** get_address_of_touchEnded_14() { return &___touchEnded_14; }
	inline void set_touchEnded_14(FsmEvent_t2133468028 * value)
	{
		___touchEnded_14 = value;
		Il2CppCodeGenWriteBarrier(&___touchEnded_14, value);
	}

	inline static int32_t get_offset_of_touchCanceled_15() { return static_cast<int32_t>(offsetof(TouchGUIEvent_t3069136972, ___touchCanceled_15)); }
	inline FsmEvent_t2133468028 * get_touchCanceled_15() const { return ___touchCanceled_15; }
	inline FsmEvent_t2133468028 ** get_address_of_touchCanceled_15() { return &___touchCanceled_15; }
	inline void set_touchCanceled_15(FsmEvent_t2133468028 * value)
	{
		___touchCanceled_15 = value;
		Il2CppCodeGenWriteBarrier(&___touchCanceled_15, value);
	}

	inline static int32_t get_offset_of_notTouching_16() { return static_cast<int32_t>(offsetof(TouchGUIEvent_t3069136972, ___notTouching_16)); }
	inline FsmEvent_t2133468028 * get_notTouching_16() const { return ___notTouching_16; }
	inline FsmEvent_t2133468028 ** get_address_of_notTouching_16() { return &___notTouching_16; }
	inline void set_notTouching_16(FsmEvent_t2133468028 * value)
	{
		___notTouching_16 = value;
		Il2CppCodeGenWriteBarrier(&___notTouching_16, value);
	}

	inline static int32_t get_offset_of_storeFingerId_17() { return static_cast<int32_t>(offsetof(TouchGUIEvent_t3069136972, ___storeFingerId_17)); }
	inline FsmInt_t1596138449 * get_storeFingerId_17() const { return ___storeFingerId_17; }
	inline FsmInt_t1596138449 ** get_address_of_storeFingerId_17() { return &___storeFingerId_17; }
	inline void set_storeFingerId_17(FsmInt_t1596138449 * value)
	{
		___storeFingerId_17 = value;
		Il2CppCodeGenWriteBarrier(&___storeFingerId_17, value);
	}

	inline static int32_t get_offset_of_storeHitPoint_18() { return static_cast<int32_t>(offsetof(TouchGUIEvent_t3069136972, ___storeHitPoint_18)); }
	inline FsmVector3_t533912882 * get_storeHitPoint_18() const { return ___storeHitPoint_18; }
	inline FsmVector3_t533912882 ** get_address_of_storeHitPoint_18() { return &___storeHitPoint_18; }
	inline void set_storeHitPoint_18(FsmVector3_t533912882 * value)
	{
		___storeHitPoint_18 = value;
		Il2CppCodeGenWriteBarrier(&___storeHitPoint_18, value);
	}

	inline static int32_t get_offset_of_normalizeHitPoint_19() { return static_cast<int32_t>(offsetof(TouchGUIEvent_t3069136972, ___normalizeHitPoint_19)); }
	inline FsmBool_t1075959796 * get_normalizeHitPoint_19() const { return ___normalizeHitPoint_19; }
	inline FsmBool_t1075959796 ** get_address_of_normalizeHitPoint_19() { return &___normalizeHitPoint_19; }
	inline void set_normalizeHitPoint_19(FsmBool_t1075959796 * value)
	{
		___normalizeHitPoint_19 = value;
		Il2CppCodeGenWriteBarrier(&___normalizeHitPoint_19, value);
	}

	inline static int32_t get_offset_of_storeOffset_20() { return static_cast<int32_t>(offsetof(TouchGUIEvent_t3069136972, ___storeOffset_20)); }
	inline FsmVector3_t533912882 * get_storeOffset_20() const { return ___storeOffset_20; }
	inline FsmVector3_t533912882 ** get_address_of_storeOffset_20() { return &___storeOffset_20; }
	inline void set_storeOffset_20(FsmVector3_t533912882 * value)
	{
		___storeOffset_20 = value;
		Il2CppCodeGenWriteBarrier(&___storeOffset_20, value);
	}

	inline static int32_t get_offset_of_relativeTo_21() { return static_cast<int32_t>(offsetof(TouchGUIEvent_t3069136972, ___relativeTo_21)); }
	inline int32_t get_relativeTo_21() const { return ___relativeTo_21; }
	inline int32_t* get_address_of_relativeTo_21() { return &___relativeTo_21; }
	inline void set_relativeTo_21(int32_t value)
	{
		___relativeTo_21 = value;
	}

	inline static int32_t get_offset_of_normalizeOffset_22() { return static_cast<int32_t>(offsetof(TouchGUIEvent_t3069136972, ___normalizeOffset_22)); }
	inline FsmBool_t1075959796 * get_normalizeOffset_22() const { return ___normalizeOffset_22; }
	inline FsmBool_t1075959796 ** get_address_of_normalizeOffset_22() { return &___normalizeOffset_22; }
	inline void set_normalizeOffset_22(FsmBool_t1075959796 * value)
	{
		___normalizeOffset_22 = value;
		Il2CppCodeGenWriteBarrier(&___normalizeOffset_22, value);
	}

	inline static int32_t get_offset_of_everyFrame_23() { return static_cast<int32_t>(offsetof(TouchGUIEvent_t3069136972, ___everyFrame_23)); }
	inline bool get_everyFrame_23() const { return ___everyFrame_23; }
	inline bool* get_address_of_everyFrame_23() { return &___everyFrame_23; }
	inline void set_everyFrame_23(bool value)
	{
		___everyFrame_23 = value;
	}

	inline static int32_t get_offset_of_touchStartPos_24() { return static_cast<int32_t>(offsetof(TouchGUIEvent_t3069136972, ___touchStartPos_24)); }
	inline Vector3_t4282066566  get_touchStartPos_24() const { return ___touchStartPos_24; }
	inline Vector3_t4282066566 * get_address_of_touchStartPos_24() { return &___touchStartPos_24; }
	inline void set_touchStartPos_24(Vector3_t4282066566  value)
	{
		___touchStartPos_24 = value;
	}

	inline static int32_t get_offset_of_guiElement_25() { return static_cast<int32_t>(offsetof(TouchGUIEvent_t3069136972, ___guiElement_25)); }
	inline GUIElement_t3775428101 * get_guiElement_25() const { return ___guiElement_25; }
	inline GUIElement_t3775428101 ** get_address_of_guiElement_25() { return &___guiElement_25; }
	inline void set_guiElement_25(GUIElement_t3775428101 * value)
	{
		___guiElement_25 = value;
		Il2CppCodeGenWriteBarrier(&___guiElement_25, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
