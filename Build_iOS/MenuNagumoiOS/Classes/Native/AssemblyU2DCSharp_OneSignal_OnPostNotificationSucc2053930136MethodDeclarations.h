﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OneSignal/OnPostNotificationSuccess
struct OnPostNotificationSuccess_t2053930136;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t696267445;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void OneSignal/OnPostNotificationSuccess::.ctor(System.Object,System.IntPtr)
extern "C"  void OnPostNotificationSuccess__ctor_m211164783 (OnPostNotificationSuccess_t2053930136 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal/OnPostNotificationSuccess::Invoke(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void OnPostNotificationSuccess_Invoke_m1968748008 (OnPostNotificationSuccess_t2053930136 * __this, Dictionary_2_t696267445 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult OneSignal/OnPostNotificationSuccess::BeginInvoke(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnPostNotificationSuccess_BeginInvoke_m3276943101 (OnPostNotificationSuccess_t2053930136 * __this, Dictionary_2_t696267445 * ___response0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal/OnPostNotificationSuccess::EndInvoke(System.IAsyncResult)
extern "C"  void OnPostNotificationSuccess_EndInvoke_m132128767 (OnPostNotificationSuccess_t2053930136 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
