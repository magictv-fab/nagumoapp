﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ARM_abstracts_ToggleOnOffAbstract832893812.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.events.TrackInToggleOnOff
struct  TrackInToggleOnOff_t288618104  : public ToggleOnOffAbstract_t832893812
{
public:
	// System.Boolean ARM.events.TrackInToggleOnOff::ResetOnTrackin
	bool ___ResetOnTrackin_7;

public:
	inline static int32_t get_offset_of_ResetOnTrackin_7() { return static_cast<int32_t>(offsetof(TrackInToggleOnOff_t288618104, ___ResetOnTrackin_7)); }
	inline bool get_ResetOnTrackin_7() const { return ___ResetOnTrackin_7; }
	inline bool* get_address_of_ResetOnTrackin_7() { return &___ResetOnTrackin_7; }
	inline void set_ResetOnTrackin_7(bool value)
	{
		___ResetOnTrackin_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
