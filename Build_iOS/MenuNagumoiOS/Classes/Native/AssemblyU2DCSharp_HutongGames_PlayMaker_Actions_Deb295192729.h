﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "PlayMaker_HutongGames_PlayMaker_LogLevel284580066.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DebugLog
struct  DebugLog_t295192729  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.LogLevel HutongGames.PlayMaker.Actions.DebugLog::logLevel
	int32_t ___logLevel_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.DebugLog::text
	FsmString_t952858651 * ___text_10;

public:
	inline static int32_t get_offset_of_logLevel_9() { return static_cast<int32_t>(offsetof(DebugLog_t295192729, ___logLevel_9)); }
	inline int32_t get_logLevel_9() const { return ___logLevel_9; }
	inline int32_t* get_address_of_logLevel_9() { return &___logLevel_9; }
	inline void set_logLevel_9(int32_t value)
	{
		___logLevel_9 = value;
	}

	inline static int32_t get_offset_of_text_10() { return static_cast<int32_t>(offsetof(DebugLog_t295192729, ___text_10)); }
	inline FsmString_t952858651 * get_text_10() const { return ___text_10; }
	inline FsmString_t952858651 ** get_address_of_text_10() { return &___text_10; }
	inline void set_text_10(FsmString_t952858651 * value)
	{
		___text_10 = value;
		Il2CppCodeGenWriteBarrier(&___text_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
