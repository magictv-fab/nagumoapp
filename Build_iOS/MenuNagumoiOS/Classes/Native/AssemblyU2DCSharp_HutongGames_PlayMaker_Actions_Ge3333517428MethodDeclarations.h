﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight
struct GetAnimatorLayerWeight_t3333517428;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight::.ctor()
extern "C"  void GetAnimatorLayerWeight__ctor_m2928099954 (GetAnimatorLayerWeight_t3333517428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight::Reset()
extern "C"  void GetAnimatorLayerWeight_Reset_m574532895 (GetAnimatorLayerWeight_t3333517428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight::OnEnter()
extern "C"  void GetAnimatorLayerWeight_OnEnter_m92205513 (GetAnimatorLayerWeight_t3333517428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight::OnAnimatorMoveEvent()
extern "C"  void GetAnimatorLayerWeight_OnAnimatorMoveEvent_m2306613907 (GetAnimatorLayerWeight_t3333517428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight::OnUpdate()
extern "C"  void GetAnimatorLayerWeight_OnUpdate_m1991930266 (GetAnimatorLayerWeight_t3333517428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight::GetLayerWeight()
extern "C"  void GetAnimatorLayerWeight_GetLayerWeight_m1458072709 (GetAnimatorLayerWeight_t3333517428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight::OnExit()
extern "C"  void GetAnimatorLayerWeight_OnExit_m2090103983 (GetAnimatorLayerWeight_t3333517428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
