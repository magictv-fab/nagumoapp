﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.InputField
struct InputField_t609046876;
// UnityEngine.UI.Toggle
struct Toggle_t110812896;
// UnityEngine.UI.Dropdown
struct Dropdown_t4201779933;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.UI.Image
struct Image_t538875265;
// JsonCadastro
struct JsonCadastro_t2830276961;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "mscorlib_System_DateTime4283661327.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cadastro
struct  Cadastro_t3948724313  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.InputField Cadastro::nomeInput
	InputField_t609046876 * ___nomeInput_2;
	// UnityEngine.UI.InputField Cadastro::sobreNomeInput
	InputField_t609046876 * ___sobreNomeInput_3;
	// UnityEngine.UI.InputField Cadastro::cpfInput
	InputField_t609046876 * ___cpfInput_4;
	// UnityEngine.UI.InputField Cadastro::emailInput
	InputField_t609046876 * ___emailInput_5;
	// UnityEngine.UI.InputField Cadastro::reemailInput
	InputField_t609046876 * ___reemailInput_6;
	// UnityEngine.UI.InputField Cadastro::telefoneInput
	InputField_t609046876 * ___telefoneInput_7;
	// UnityEngine.UI.InputField Cadastro::idadeInput
	InputField_t609046876 * ___idadeInput_8;
	// UnityEngine.UI.InputField Cadastro::senhaInput
	InputField_t609046876 * ___senhaInput_9;
	// UnityEngine.UI.InputField Cadastro::resenhaInput
	InputField_t609046876 * ___resenhaInput_10;
	// UnityEngine.UI.InputField Cadastro::novaSenhaInput
	InputField_t609046876 * ___novaSenhaInput_11;
	// UnityEngine.UI.InputField Cadastro::cepInput
	InputField_t609046876 * ___cepInput_12;
	// UnityEngine.UI.InputField Cadastro::ruaInput
	InputField_t609046876 * ___ruaInput_13;
	// UnityEngine.UI.InputField Cadastro::bairroInput
	InputField_t609046876 * ___bairroInput_14;
	// UnityEngine.UI.InputField Cadastro::numeroInput
	InputField_t609046876 * ___numeroInput_15;
	// UnityEngine.UI.InputField Cadastro::complementoInput
	InputField_t609046876 * ___complementoInput_16;
	// UnityEngine.UI.InputField Cadastro::cidadeInput
	InputField_t609046876 * ___cidadeInput_17;
	// UnityEngine.UI.InputField Cadastro::estadoInput
	InputField_t609046876 * ___estadoInput_18;
	// UnityEngine.UI.InputField Cadastro::dddInput
	InputField_t609046876 * ___dddInput_19;
	// UnityEngine.UI.Toggle Cadastro::politicaPriv
	Toggle_t110812896 * ___politicaPriv_20;
	// UnityEngine.UI.Toggle Cadastro::politicaFeidl
	Toggle_t110812896 * ___politicaFeidl_21;
	// UnityEngine.UI.Dropdown Cadastro::sexoDd
	Dropdown_t4201779933 * ___sexoDd_22;
	// UnityEngine.UI.Dropdown Cadastro::estadoDd
	Dropdown_t4201779933 * ___estadoDd_23;
	// UnityEngine.GameObject Cadastro::popup
	GameObject_t3674682005 * ___popup_24;
	// UnityEngine.GameObject Cadastro::loadingObj
	GameObject_t3674682005 * ___loadingObj_25;
	// UnityEngine.UI.Image Cadastro::foto
	Image_t538875265 * ___foto_26;
	// JsonCadastro Cadastro::jsonCadastro
	JsonCadastro_t2830276961 * ___jsonCadastro_27;
	// System.Boolean Cadastro::onConnection
	bool ___onConnection_28;
	// System.Boolean Cadastro::back
	bool ___back_29;
	// System.String Cadastro::lastedChar
	String_t* ___lastedChar_30;
	// System.DateTime Cadastro::tmpDateTime
	DateTime_t4283661327  ___tmpDateTime_31;
	// System.String Cadastro::levelName
	String_t* ___levelName_32;

public:
	inline static int32_t get_offset_of_nomeInput_2() { return static_cast<int32_t>(offsetof(Cadastro_t3948724313, ___nomeInput_2)); }
	inline InputField_t609046876 * get_nomeInput_2() const { return ___nomeInput_2; }
	inline InputField_t609046876 ** get_address_of_nomeInput_2() { return &___nomeInput_2; }
	inline void set_nomeInput_2(InputField_t609046876 * value)
	{
		___nomeInput_2 = value;
		Il2CppCodeGenWriteBarrier(&___nomeInput_2, value);
	}

	inline static int32_t get_offset_of_sobreNomeInput_3() { return static_cast<int32_t>(offsetof(Cadastro_t3948724313, ___sobreNomeInput_3)); }
	inline InputField_t609046876 * get_sobreNomeInput_3() const { return ___sobreNomeInput_3; }
	inline InputField_t609046876 ** get_address_of_sobreNomeInput_3() { return &___sobreNomeInput_3; }
	inline void set_sobreNomeInput_3(InputField_t609046876 * value)
	{
		___sobreNomeInput_3 = value;
		Il2CppCodeGenWriteBarrier(&___sobreNomeInput_3, value);
	}

	inline static int32_t get_offset_of_cpfInput_4() { return static_cast<int32_t>(offsetof(Cadastro_t3948724313, ___cpfInput_4)); }
	inline InputField_t609046876 * get_cpfInput_4() const { return ___cpfInput_4; }
	inline InputField_t609046876 ** get_address_of_cpfInput_4() { return &___cpfInput_4; }
	inline void set_cpfInput_4(InputField_t609046876 * value)
	{
		___cpfInput_4 = value;
		Il2CppCodeGenWriteBarrier(&___cpfInput_4, value);
	}

	inline static int32_t get_offset_of_emailInput_5() { return static_cast<int32_t>(offsetof(Cadastro_t3948724313, ___emailInput_5)); }
	inline InputField_t609046876 * get_emailInput_5() const { return ___emailInput_5; }
	inline InputField_t609046876 ** get_address_of_emailInput_5() { return &___emailInput_5; }
	inline void set_emailInput_5(InputField_t609046876 * value)
	{
		___emailInput_5 = value;
		Il2CppCodeGenWriteBarrier(&___emailInput_5, value);
	}

	inline static int32_t get_offset_of_reemailInput_6() { return static_cast<int32_t>(offsetof(Cadastro_t3948724313, ___reemailInput_6)); }
	inline InputField_t609046876 * get_reemailInput_6() const { return ___reemailInput_6; }
	inline InputField_t609046876 ** get_address_of_reemailInput_6() { return &___reemailInput_6; }
	inline void set_reemailInput_6(InputField_t609046876 * value)
	{
		___reemailInput_6 = value;
		Il2CppCodeGenWriteBarrier(&___reemailInput_6, value);
	}

	inline static int32_t get_offset_of_telefoneInput_7() { return static_cast<int32_t>(offsetof(Cadastro_t3948724313, ___telefoneInput_7)); }
	inline InputField_t609046876 * get_telefoneInput_7() const { return ___telefoneInput_7; }
	inline InputField_t609046876 ** get_address_of_telefoneInput_7() { return &___telefoneInput_7; }
	inline void set_telefoneInput_7(InputField_t609046876 * value)
	{
		___telefoneInput_7 = value;
		Il2CppCodeGenWriteBarrier(&___telefoneInput_7, value);
	}

	inline static int32_t get_offset_of_idadeInput_8() { return static_cast<int32_t>(offsetof(Cadastro_t3948724313, ___idadeInput_8)); }
	inline InputField_t609046876 * get_idadeInput_8() const { return ___idadeInput_8; }
	inline InputField_t609046876 ** get_address_of_idadeInput_8() { return &___idadeInput_8; }
	inline void set_idadeInput_8(InputField_t609046876 * value)
	{
		___idadeInput_8 = value;
		Il2CppCodeGenWriteBarrier(&___idadeInput_8, value);
	}

	inline static int32_t get_offset_of_senhaInput_9() { return static_cast<int32_t>(offsetof(Cadastro_t3948724313, ___senhaInput_9)); }
	inline InputField_t609046876 * get_senhaInput_9() const { return ___senhaInput_9; }
	inline InputField_t609046876 ** get_address_of_senhaInput_9() { return &___senhaInput_9; }
	inline void set_senhaInput_9(InputField_t609046876 * value)
	{
		___senhaInput_9 = value;
		Il2CppCodeGenWriteBarrier(&___senhaInput_9, value);
	}

	inline static int32_t get_offset_of_resenhaInput_10() { return static_cast<int32_t>(offsetof(Cadastro_t3948724313, ___resenhaInput_10)); }
	inline InputField_t609046876 * get_resenhaInput_10() const { return ___resenhaInput_10; }
	inline InputField_t609046876 ** get_address_of_resenhaInput_10() { return &___resenhaInput_10; }
	inline void set_resenhaInput_10(InputField_t609046876 * value)
	{
		___resenhaInput_10 = value;
		Il2CppCodeGenWriteBarrier(&___resenhaInput_10, value);
	}

	inline static int32_t get_offset_of_novaSenhaInput_11() { return static_cast<int32_t>(offsetof(Cadastro_t3948724313, ___novaSenhaInput_11)); }
	inline InputField_t609046876 * get_novaSenhaInput_11() const { return ___novaSenhaInput_11; }
	inline InputField_t609046876 ** get_address_of_novaSenhaInput_11() { return &___novaSenhaInput_11; }
	inline void set_novaSenhaInput_11(InputField_t609046876 * value)
	{
		___novaSenhaInput_11 = value;
		Il2CppCodeGenWriteBarrier(&___novaSenhaInput_11, value);
	}

	inline static int32_t get_offset_of_cepInput_12() { return static_cast<int32_t>(offsetof(Cadastro_t3948724313, ___cepInput_12)); }
	inline InputField_t609046876 * get_cepInput_12() const { return ___cepInput_12; }
	inline InputField_t609046876 ** get_address_of_cepInput_12() { return &___cepInput_12; }
	inline void set_cepInput_12(InputField_t609046876 * value)
	{
		___cepInput_12 = value;
		Il2CppCodeGenWriteBarrier(&___cepInput_12, value);
	}

	inline static int32_t get_offset_of_ruaInput_13() { return static_cast<int32_t>(offsetof(Cadastro_t3948724313, ___ruaInput_13)); }
	inline InputField_t609046876 * get_ruaInput_13() const { return ___ruaInput_13; }
	inline InputField_t609046876 ** get_address_of_ruaInput_13() { return &___ruaInput_13; }
	inline void set_ruaInput_13(InputField_t609046876 * value)
	{
		___ruaInput_13 = value;
		Il2CppCodeGenWriteBarrier(&___ruaInput_13, value);
	}

	inline static int32_t get_offset_of_bairroInput_14() { return static_cast<int32_t>(offsetof(Cadastro_t3948724313, ___bairroInput_14)); }
	inline InputField_t609046876 * get_bairroInput_14() const { return ___bairroInput_14; }
	inline InputField_t609046876 ** get_address_of_bairroInput_14() { return &___bairroInput_14; }
	inline void set_bairroInput_14(InputField_t609046876 * value)
	{
		___bairroInput_14 = value;
		Il2CppCodeGenWriteBarrier(&___bairroInput_14, value);
	}

	inline static int32_t get_offset_of_numeroInput_15() { return static_cast<int32_t>(offsetof(Cadastro_t3948724313, ___numeroInput_15)); }
	inline InputField_t609046876 * get_numeroInput_15() const { return ___numeroInput_15; }
	inline InputField_t609046876 ** get_address_of_numeroInput_15() { return &___numeroInput_15; }
	inline void set_numeroInput_15(InputField_t609046876 * value)
	{
		___numeroInput_15 = value;
		Il2CppCodeGenWriteBarrier(&___numeroInput_15, value);
	}

	inline static int32_t get_offset_of_complementoInput_16() { return static_cast<int32_t>(offsetof(Cadastro_t3948724313, ___complementoInput_16)); }
	inline InputField_t609046876 * get_complementoInput_16() const { return ___complementoInput_16; }
	inline InputField_t609046876 ** get_address_of_complementoInput_16() { return &___complementoInput_16; }
	inline void set_complementoInput_16(InputField_t609046876 * value)
	{
		___complementoInput_16 = value;
		Il2CppCodeGenWriteBarrier(&___complementoInput_16, value);
	}

	inline static int32_t get_offset_of_cidadeInput_17() { return static_cast<int32_t>(offsetof(Cadastro_t3948724313, ___cidadeInput_17)); }
	inline InputField_t609046876 * get_cidadeInput_17() const { return ___cidadeInput_17; }
	inline InputField_t609046876 ** get_address_of_cidadeInput_17() { return &___cidadeInput_17; }
	inline void set_cidadeInput_17(InputField_t609046876 * value)
	{
		___cidadeInput_17 = value;
		Il2CppCodeGenWriteBarrier(&___cidadeInput_17, value);
	}

	inline static int32_t get_offset_of_estadoInput_18() { return static_cast<int32_t>(offsetof(Cadastro_t3948724313, ___estadoInput_18)); }
	inline InputField_t609046876 * get_estadoInput_18() const { return ___estadoInput_18; }
	inline InputField_t609046876 ** get_address_of_estadoInput_18() { return &___estadoInput_18; }
	inline void set_estadoInput_18(InputField_t609046876 * value)
	{
		___estadoInput_18 = value;
		Il2CppCodeGenWriteBarrier(&___estadoInput_18, value);
	}

	inline static int32_t get_offset_of_dddInput_19() { return static_cast<int32_t>(offsetof(Cadastro_t3948724313, ___dddInput_19)); }
	inline InputField_t609046876 * get_dddInput_19() const { return ___dddInput_19; }
	inline InputField_t609046876 ** get_address_of_dddInput_19() { return &___dddInput_19; }
	inline void set_dddInput_19(InputField_t609046876 * value)
	{
		___dddInput_19 = value;
		Il2CppCodeGenWriteBarrier(&___dddInput_19, value);
	}

	inline static int32_t get_offset_of_politicaPriv_20() { return static_cast<int32_t>(offsetof(Cadastro_t3948724313, ___politicaPriv_20)); }
	inline Toggle_t110812896 * get_politicaPriv_20() const { return ___politicaPriv_20; }
	inline Toggle_t110812896 ** get_address_of_politicaPriv_20() { return &___politicaPriv_20; }
	inline void set_politicaPriv_20(Toggle_t110812896 * value)
	{
		___politicaPriv_20 = value;
		Il2CppCodeGenWriteBarrier(&___politicaPriv_20, value);
	}

	inline static int32_t get_offset_of_politicaFeidl_21() { return static_cast<int32_t>(offsetof(Cadastro_t3948724313, ___politicaFeidl_21)); }
	inline Toggle_t110812896 * get_politicaFeidl_21() const { return ___politicaFeidl_21; }
	inline Toggle_t110812896 ** get_address_of_politicaFeidl_21() { return &___politicaFeidl_21; }
	inline void set_politicaFeidl_21(Toggle_t110812896 * value)
	{
		___politicaFeidl_21 = value;
		Il2CppCodeGenWriteBarrier(&___politicaFeidl_21, value);
	}

	inline static int32_t get_offset_of_sexoDd_22() { return static_cast<int32_t>(offsetof(Cadastro_t3948724313, ___sexoDd_22)); }
	inline Dropdown_t4201779933 * get_sexoDd_22() const { return ___sexoDd_22; }
	inline Dropdown_t4201779933 ** get_address_of_sexoDd_22() { return &___sexoDd_22; }
	inline void set_sexoDd_22(Dropdown_t4201779933 * value)
	{
		___sexoDd_22 = value;
		Il2CppCodeGenWriteBarrier(&___sexoDd_22, value);
	}

	inline static int32_t get_offset_of_estadoDd_23() { return static_cast<int32_t>(offsetof(Cadastro_t3948724313, ___estadoDd_23)); }
	inline Dropdown_t4201779933 * get_estadoDd_23() const { return ___estadoDd_23; }
	inline Dropdown_t4201779933 ** get_address_of_estadoDd_23() { return &___estadoDd_23; }
	inline void set_estadoDd_23(Dropdown_t4201779933 * value)
	{
		___estadoDd_23 = value;
		Il2CppCodeGenWriteBarrier(&___estadoDd_23, value);
	}

	inline static int32_t get_offset_of_popup_24() { return static_cast<int32_t>(offsetof(Cadastro_t3948724313, ___popup_24)); }
	inline GameObject_t3674682005 * get_popup_24() const { return ___popup_24; }
	inline GameObject_t3674682005 ** get_address_of_popup_24() { return &___popup_24; }
	inline void set_popup_24(GameObject_t3674682005 * value)
	{
		___popup_24 = value;
		Il2CppCodeGenWriteBarrier(&___popup_24, value);
	}

	inline static int32_t get_offset_of_loadingObj_25() { return static_cast<int32_t>(offsetof(Cadastro_t3948724313, ___loadingObj_25)); }
	inline GameObject_t3674682005 * get_loadingObj_25() const { return ___loadingObj_25; }
	inline GameObject_t3674682005 ** get_address_of_loadingObj_25() { return &___loadingObj_25; }
	inline void set_loadingObj_25(GameObject_t3674682005 * value)
	{
		___loadingObj_25 = value;
		Il2CppCodeGenWriteBarrier(&___loadingObj_25, value);
	}

	inline static int32_t get_offset_of_foto_26() { return static_cast<int32_t>(offsetof(Cadastro_t3948724313, ___foto_26)); }
	inline Image_t538875265 * get_foto_26() const { return ___foto_26; }
	inline Image_t538875265 ** get_address_of_foto_26() { return &___foto_26; }
	inline void set_foto_26(Image_t538875265 * value)
	{
		___foto_26 = value;
		Il2CppCodeGenWriteBarrier(&___foto_26, value);
	}

	inline static int32_t get_offset_of_jsonCadastro_27() { return static_cast<int32_t>(offsetof(Cadastro_t3948724313, ___jsonCadastro_27)); }
	inline JsonCadastro_t2830276961 * get_jsonCadastro_27() const { return ___jsonCadastro_27; }
	inline JsonCadastro_t2830276961 ** get_address_of_jsonCadastro_27() { return &___jsonCadastro_27; }
	inline void set_jsonCadastro_27(JsonCadastro_t2830276961 * value)
	{
		___jsonCadastro_27 = value;
		Il2CppCodeGenWriteBarrier(&___jsonCadastro_27, value);
	}

	inline static int32_t get_offset_of_onConnection_28() { return static_cast<int32_t>(offsetof(Cadastro_t3948724313, ___onConnection_28)); }
	inline bool get_onConnection_28() const { return ___onConnection_28; }
	inline bool* get_address_of_onConnection_28() { return &___onConnection_28; }
	inline void set_onConnection_28(bool value)
	{
		___onConnection_28 = value;
	}

	inline static int32_t get_offset_of_back_29() { return static_cast<int32_t>(offsetof(Cadastro_t3948724313, ___back_29)); }
	inline bool get_back_29() const { return ___back_29; }
	inline bool* get_address_of_back_29() { return &___back_29; }
	inline void set_back_29(bool value)
	{
		___back_29 = value;
	}

	inline static int32_t get_offset_of_lastedChar_30() { return static_cast<int32_t>(offsetof(Cadastro_t3948724313, ___lastedChar_30)); }
	inline String_t* get_lastedChar_30() const { return ___lastedChar_30; }
	inline String_t** get_address_of_lastedChar_30() { return &___lastedChar_30; }
	inline void set_lastedChar_30(String_t* value)
	{
		___lastedChar_30 = value;
		Il2CppCodeGenWriteBarrier(&___lastedChar_30, value);
	}

	inline static int32_t get_offset_of_tmpDateTime_31() { return static_cast<int32_t>(offsetof(Cadastro_t3948724313, ___tmpDateTime_31)); }
	inline DateTime_t4283661327  get_tmpDateTime_31() const { return ___tmpDateTime_31; }
	inline DateTime_t4283661327 * get_address_of_tmpDateTime_31() { return &___tmpDateTime_31; }
	inline void set_tmpDateTime_31(DateTime_t4283661327  value)
	{
		___tmpDateTime_31 = value;
	}

	inline static int32_t get_offset_of_levelName_32() { return static_cast<int32_t>(offsetof(Cadastro_t3948724313, ___levelName_32)); }
	inline String_t* get_levelName_32() const { return ___levelName_32; }
	inline String_t** get_address_of_levelName_32() { return &___levelName_32; }
	inline void set_levelName_32(String_t* value)
	{
		___levelName_32 = value;
		Il2CppCodeGenWriteBarrier(&___levelName_32, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
