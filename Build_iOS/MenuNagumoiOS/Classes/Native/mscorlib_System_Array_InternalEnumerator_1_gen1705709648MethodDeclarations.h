﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1705709648.h"
#include "mscorlib_System_Array1146569071.h"
#include "QRCode_ZXing_ResultMetadataType2923366972.h"

// System.Void System.Array/InternalEnumerator`1<ZXing.ResultMetadataType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1298500929_gshared (InternalEnumerator_1_t1705709648 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1298500929(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1705709648 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1298500929_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<ZXing.ResultMetadataType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1173550783_gshared (InternalEnumerator_1_t1705709648 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1173550783(__this, method) ((  void (*) (InternalEnumerator_1_t1705709648 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1173550783_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<ZXing.ResultMetadataType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1904340139_gshared (InternalEnumerator_1_t1705709648 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1904340139(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1705709648 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1904340139_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<ZXing.ResultMetadataType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2452980824_gshared (InternalEnumerator_1_t1705709648 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2452980824(__this, method) ((  void (*) (InternalEnumerator_1_t1705709648 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2452980824_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<ZXing.ResultMetadataType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4033760043_gshared (InternalEnumerator_1_t1705709648 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m4033760043(__this, method) ((  bool (*) (InternalEnumerator_1_t1705709648 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m4033760043_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<ZXing.ResultMetadataType>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m3244421000_gshared (InternalEnumerator_1_t1705709648 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3244421000(__this, method) ((  int32_t (*) (InternalEnumerator_1_t1705709648 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3244421000_gshared)(__this, method)
