﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen71225793.h"
#include "mscorlib_System_String7231557.h"

// System.Nullable`1<UnityEngine.Vector3> PipeToVector3::Parse(System.String)
extern "C"  Nullable_1_t71225793  PipeToVector3_Parse_m1799787456 (Il2CppObject * __this /* static, unused */, String_t* ___string_with_pipe0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
