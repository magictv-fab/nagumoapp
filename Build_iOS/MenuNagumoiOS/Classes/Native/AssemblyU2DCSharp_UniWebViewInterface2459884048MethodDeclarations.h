﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniWebViewInterface
struct UniWebViewInterface_t2459884048;
// UniWebViewInterface/UnitySendMessageDelegate
struct UnitySendMessageDelegate_t2158631822;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UniWebViewInterface_UnitySendMes2158631822.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"

// System.Void UniWebViewInterface::.ctor()
extern "C"  void UniWebViewInterface__ctor_m4029385563 (UniWebViewInterface_t2459884048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::.cctor()
extern "C"  void UniWebViewInterface__cctor_m4169771954 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_connectMessageSender(UniWebViewInterface/UnitySendMessageDelegate)
extern "C"  void UniWebViewInterface_uv_connectMessageSender_m2498285563 (Il2CppObject * __this /* static, unused */, UnitySendMessageDelegate_t2158631822 * ___sendMessageDelegate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::ConnectMessageSender()
extern "C"  void UniWebViewInterface_ConnectMessageSender_m193500763 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::SendMessage(System.IntPtr,System.IntPtr,System.IntPtr)
extern "C"  void UniWebViewInterface_SendMessage_m1406601688 (Il2CppObject * __this /* static, unused */, IntPtr_t ___namePtr0, IntPtr_t ___methodPtr1, IntPtr_t ___parameterPtr2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_UniWebViewInterface_SendMessage_m1406601688(intptr_t ___objectName0, intptr_t ___methodName1, intptr_t ___parameter2);
// System.Void UniWebViewInterface::uv_setLogLevel(System.Int32)
extern "C"  void UniWebViewInterface_uv_setLogLevel_m1486420414 (Il2CppObject * __this /* static, unused */, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::SetLogLevel(System.Int32)
extern "C"  void UniWebViewInterface_SetLogLevel_m3645025260 (Il2CppObject * __this /* static, unused */, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_init(System.String,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void UniWebViewInterface_uv_init_m41530011 (Il2CppObject * __this /* static, unused */, String_t* ___name0, int32_t ___x1, int32_t ___y2, int32_t ___width3, int32_t ___height4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::Init(System.String,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void UniWebViewInterface_Init_m3454977097 (Il2CppObject * __this /* static, unused */, String_t* ___name0, int32_t ___x1, int32_t ___y2, int32_t ___width3, int32_t ___height4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_destroy(System.String)
extern "C"  void UniWebViewInterface_uv_destroy_m554776925 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::Destroy(System.String)
extern "C"  void UniWebViewInterface_Destroy_m1711957487 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_load(System.String,System.String,System.Boolean)
extern "C"  void UniWebViewInterface_uv_load_m1380556060 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___url1, bool ___skipEncoding2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::Load(System.String,System.String,System.Boolean)
extern "C"  void UniWebViewInterface_Load_m2166314798 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___url1, bool ___skipEncoding2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_loadHTMLString(System.String,System.String,System.String,System.Boolean)
extern "C"  void UniWebViewInterface_uv_loadHTMLString_m3716453564 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___html1, String_t* ___baseUrl2, bool ___skipEncoding3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::LoadHTMLString(System.String,System.String,System.String,System.Boolean)
extern "C"  void UniWebViewInterface_LoadHTMLString_m2159130830 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___html1, String_t* ___baseUrl2, bool ___skipEncoding3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_reload(System.String)
extern "C"  void UniWebViewInterface_uv_reload_m4231789874 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::Reload(System.String)
extern "C"  void UniWebViewInterface_Reload_m805434976 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_stop(System.String)
extern "C"  void UniWebViewInterface_uv_stop_m2961130025 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::Stop(System.String)
extern "C"  void UniWebViewInterface_Stop_m2519576279 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UniWebViewInterface::uv_getUrl(System.String)
extern "C"  String_t* UniWebViewInterface_uv_getUrl_m3431667535 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UniWebViewInterface::GetUrl(System.String)
extern "C"  String_t* UniWebViewInterface_GetUrl_m2110229539 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_setFrame(System.String,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void UniWebViewInterface_uv_setFrame_m1350448800 (Il2CppObject * __this /* static, unused */, String_t* ___name0, int32_t ___x1, int32_t ___y2, int32_t ___width3, int32_t ___height4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::SetFrame(System.String,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void UniWebViewInterface_SetFrame_m1385611598 (Il2CppObject * __this /* static, unused */, String_t* ___name0, int32_t ___x1, int32_t ___y2, int32_t ___width3, int32_t ___height4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_setPosition(System.String,System.Int32,System.Int32)
extern "C"  void UniWebViewInterface_uv_setPosition_m3969203436 (Il2CppObject * __this /* static, unused */, String_t* ___name0, int32_t ___x1, int32_t ___y2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::SetPosition(System.String,System.Int32,System.Int32)
extern "C"  void UniWebViewInterface_SetPosition_m3875249662 (Il2CppObject * __this /* static, unused */, String_t* ___name0, int32_t ___x1, int32_t ___y2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_setSize(System.String,System.Int32,System.Int32)
extern "C"  void UniWebViewInterface_uv_setSize_m1516684052 (Il2CppObject * __this /* static, unused */, String_t* ___name0, int32_t ___width1, int32_t ___height2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::SetSize(System.String,System.Int32,System.Int32)
extern "C"  void UniWebViewInterface_SetSize_m2302442790 (Il2CppObject * __this /* static, unused */, String_t* ___name0, int32_t ___width1, int32_t ___height2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniWebViewInterface::uv_show(System.String,System.Boolean,System.Int32,System.Single,System.String)
extern "C"  bool UniWebViewInterface_uv_show_m2615331957 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___fade1, int32_t ___edge2, float ___duration3, String_t* ___identifier4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniWebViewInterface::Show(System.String,System.Boolean,System.Int32,System.Single,System.String)
extern "C"  bool UniWebViewInterface_Show_m3361161483 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___fade1, int32_t ___edge2, float ___duration3, String_t* ___identifier4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniWebViewInterface::uv_hide(System.String,System.Boolean,System.Int32,System.Single,System.String)
extern "C"  bool UniWebViewInterface_uv_hide_m3339995792 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___fade1, int32_t ___edge2, float ___duration3, String_t* ___identifier4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniWebViewInterface::Hide(System.String,System.Boolean,System.Int32,System.Single,System.String)
extern "C"  bool UniWebViewInterface_Hide_m4085825318 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___fade1, int32_t ___edge2, float ___duration3, String_t* ___identifier4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniWebViewInterface::uv_animateTo(System.String,System.Int32,System.Int32,System.Int32,System.Int32,System.Single,System.Single,System.String)
extern "C"  bool UniWebViewInterface_uv_animateTo_m265169749 (Il2CppObject * __this /* static, unused */, String_t* ___name0, int32_t ___x1, int32_t ___y2, int32_t ___width3, int32_t ___height4, float ___duration5, float ___delay6, String_t* ___identifier7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniWebViewInterface::AnimateTo(System.String,System.Int32,System.Int32,System.Int32,System.Int32,System.Single,System.Single,System.String)
extern "C"  bool UniWebViewInterface_AnimateTo_m2032071935 (Il2CppObject * __this /* static, unused */, String_t* ___name0, int32_t ___x1, int32_t ___y2, int32_t ___width3, int32_t ___height4, float ___duration5, float ___delay6, String_t* ___identifier7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_addJavaScript(System.String,System.String,System.String)
extern "C"  void UniWebViewInterface_uv_addJavaScript_m2241976641 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___jsString1, String_t* ___identifier2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::AddJavaScript(System.String,System.String,System.String)
extern "C"  void UniWebViewInterface_AddJavaScript_m887972179 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___jsString1, String_t* ___identifier2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_evaluateJavaScript(System.String,System.String,System.String)
extern "C"  void UniWebViewInterface_uv_evaluateJavaScript_m1411928861 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___jsString1, String_t* ___identifier2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::EvaluateJavaScript(System.String,System.String,System.String)
extern "C"  void UniWebViewInterface_EvaluateJavaScript_m2911438411 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___jsString1, String_t* ___identifier2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_addUrlScheme(System.String,System.String)
extern "C"  void UniWebViewInterface_uv_addUrlScheme_m585774580 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___scheme1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::AddUrlScheme(System.String,System.String)
extern "C"  void UniWebViewInterface_AddUrlScheme_m3430272034 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___scheme1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_removeUrlScheme(System.String,System.String)
extern "C"  void UniWebViewInterface_uv_removeUrlScheme_m432867523 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___scheme1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::RemoveUrlScheme(System.String,System.String)
extern "C"  void UniWebViewInterface_RemoveUrlScheme_m1151769557 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___scheme1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_addSslExceptionDomain(System.String,System.String)
extern "C"  void UniWebViewInterface_uv_addSslExceptionDomain_m491734603 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___domain1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::AddSslExceptionDomain(System.String,System.String)
extern "C"  void UniWebViewInterface_AddSslExceptionDomain_m3986605021 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___domain1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_removeSslExceptionDomain(System.String,System.String)
extern "C"  void UniWebViewInterface_uv_removeSslExceptionDomain_m2329619548 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___domain1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::RemoveSslExceptionDomain(System.String,System.String)
extern "C"  void UniWebViewInterface_RemoveSslExceptionDomain_m3712019850 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___domain1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_setHeaderField(System.String,System.String,System.String)
extern "C"  void UniWebViewInterface_uv_setHeaderField_m217308504 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___key1, String_t* ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::SetHeaderField(System.String,System.String,System.String)
extern "C"  void UniWebViewInterface_SetHeaderField_m1192843142 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___key1, String_t* ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_setUserAgent(System.String,System.String)
extern "C"  void UniWebViewInterface_uv_setUserAgent_m3397267631 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___userAgent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::SetUserAgent(System.String,System.String)
extern "C"  void UniWebViewInterface_SetUserAgent_m1946797789 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___userAgent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UniWebViewInterface::uv_getUserAgent(System.String)
extern "C"  String_t* UniWebViewInterface_uv_getUserAgent_m4189902244 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UniWebViewInterface::GetUserAgent(System.String)
extern "C"  String_t* UniWebViewInterface_GetUserAgent_m4017838968 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_setAllowAutoPlay(System.Boolean)
extern "C"  void UniWebViewInterface_uv_setAllowAutoPlay_m4294925528 (Il2CppObject * __this /* static, unused */, bool ___flag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::SetAllowAutoPlay(System.Boolean)
extern "C"  void UniWebViewInterface_SetAllowAutoPlay_m1524595434 (Il2CppObject * __this /* static, unused */, bool ___flag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_setAllowInlinePlay(System.Boolean)
extern "C"  void UniWebViewInterface_uv_setAllowInlinePlay_m88855938 (Il2CppObject * __this /* static, unused */, bool ___flag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::SetAllowInlinePlay(System.Boolean)
extern "C"  void UniWebViewInterface_SetAllowInlinePlay_m681359124 (Il2CppObject * __this /* static, unused */, bool ___flag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_setAllowJavaScriptOpenWindow(System.Boolean)
extern "C"  void UniWebViewInterface_uv_setAllowJavaScriptOpenWindow_m2240890908 (Il2CppObject * __this /* static, unused */, bool ___flag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::SetAllowJavaScriptOpenWindow(System.Boolean)
extern "C"  void UniWebViewInterface_SetAllowJavaScriptOpenWindow_m2959792942 (Il2CppObject * __this /* static, unused */, bool ___flag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_setJavaScriptEnabled(System.Boolean)
extern "C"  void UniWebViewInterface_uv_setJavaScriptEnabled_m3212340192 (Il2CppObject * __this /* static, unused */, bool ___flag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::SetJavaScriptEnabled(System.Boolean)
extern "C"  void UniWebViewInterface_SetJavaScriptEnabled_m1377251570 (Il2CppObject * __this /* static, unused */, bool ___flag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_cleanCache(System.String)
extern "C"  void UniWebViewInterface_uv_cleanCache_m2270025490 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::CleanCache(System.String)
extern "C"  void UniWebViewInterface_CleanCache_m133663040 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_clearCookies()
extern "C"  void UniWebViewInterface_uv_clearCookies_m1446702841 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::ClearCookies()
extern "C"  void UniWebViewInterface_ClearCookies_m3538603403 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_setCookie(System.String,System.String,System.Boolean)
extern "C"  void UniWebViewInterface_uv_setCookie_m2707533936 (Il2CppObject * __this /* static, unused */, String_t* ___url0, String_t* ___cookie1, bool ___skipEncoding2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::SetCookie(System.String,System.String,System.Boolean)
extern "C"  void UniWebViewInterface_SetCookie_m4089934238 (Il2CppObject * __this /* static, unused */, String_t* ___url0, String_t* ___cookie1, bool ___skipEncoding2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UniWebViewInterface::uv_getCookie(System.String,System.String,System.Boolean)
extern "C"  String_t* UniWebViewInterface_uv_getCookie_m3144158497 (Il2CppObject * __this /* static, unused */, String_t* ___url0, String_t* ___key1, bool ___skipEncoding2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UniWebViewInterface::GetCookie(System.String,System.String,System.Boolean)
extern "C"  String_t* UniWebViewInterface_GetCookie_m1954296309 (Il2CppObject * __this /* static, unused */, String_t* ___url0, String_t* ___key1, bool ___skipEncoding2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_clearHttpAuthUsernamePasswordHost(System.String,System.String)
extern "C"  void UniWebViewInterface_uv_clearHttpAuthUsernamePasswordHost_m3854146205 (Il2CppObject * __this /* static, unused */, String_t* ___host0, String_t* ___realm1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::ClearHttpAuthUsernamePassword(System.String,System.String)
extern "C"  void UniWebViewInterface_ClearHttpAuthUsernamePassword_m1860632311 (Il2CppObject * __this /* static, unused */, String_t* ___host0, String_t* ___realm1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_setBackgroundColor(System.String,System.Single,System.Single,System.Single,System.Single)
extern "C"  void UniWebViewInterface_uv_setBackgroundColor_m2709692812 (Il2CppObject * __this /* static, unused */, String_t* ___name0, float ___r1, float ___g2, float ___b3, float ___a4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::SetBackgroundColor(System.String,System.Single,System.Single,System.Single,System.Single)
extern "C"  void UniWebViewInterface_SetBackgroundColor_m1946411962 (Il2CppObject * __this /* static, unused */, String_t* ___name0, float ___r1, float ___g2, float ___b3, float ___a4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_setWebViewAlpha(System.String,System.Single)
extern "C"  void UniWebViewInterface_uv_setWebViewAlpha_m3953328213 (Il2CppObject * __this /* static, unused */, String_t* ___name0, float ___alpha1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::SetWebViewAlpha(System.String,System.Single)
extern "C"  void UniWebViewInterface_SetWebViewAlpha_m377262951 (Il2CppObject * __this /* static, unused */, String_t* ___name0, float ___alpha1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UniWebViewInterface::uv_getWebViewAlpha(System.String)
extern "C"  float UniWebViewInterface_uv_getWebViewAlpha_m3022502344 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UniWebViewInterface::GetWebViewAlpha(System.String)
extern "C"  float UniWebViewInterface_GetWebViewAlpha_m3815018562 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_setShowSpinnerWhileLoading(System.String,System.Boolean)
extern "C"  void UniWebViewInterface_uv_setShowSpinnerWhileLoading_m761606903 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___show1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::SetShowSpinnerWhileLoading(System.String,System.Boolean)
extern "C"  void UniWebViewInterface_SetShowSpinnerWhileLoading_m3702569737 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___show1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_setSpinnerText(System.String,System.String)
extern "C"  void UniWebViewInterface_uv_setSpinnerText_m3256835139 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___text1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::SetSpinnerText(System.String,System.String)
extern "C"  void UniWebViewInterface_SetSpinnerText_m924720881 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___text1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniWebViewInterface::uv_canGoBack(System.String)
extern "C"  bool UniWebViewInterface_uv_canGoBack_m2163466764 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniWebViewInterface::CanGoBack(System.String)
extern "C"  bool UniWebViewInterface_CanGoBack_m2589876534 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniWebViewInterface::uv_canGoForward(System.String)
extern "C"  bool UniWebViewInterface_uv_canGoForward_m2192161482 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniWebViewInterface::CanGoForward(System.String)
extern "C"  bool UniWebViewInterface_CanGoForward_m852357984 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_goBack(System.String)
extern "C"  void UniWebViewInterface_uv_goBack_m3428726300 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::GoBack(System.String)
extern "C"  void UniWebViewInterface_GoBack_m2371402 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_goForward(System.String)
extern "C"  void UniWebViewInterface_uv_goForward_m2906008762 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::GoForward(System.String)
extern "C"  void UniWebViewInterface_GoForward_m2559999180 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_setOpenLinksInExternalBrowser(System.String,System.Boolean)
extern "C"  void UniWebViewInterface_uv_setOpenLinksInExternalBrowser_m564568177 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___flag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::SetOpenLinksInExternalBrowser(System.String,System.Boolean)
extern "C"  void UniWebViewInterface_SetOpenLinksInExternalBrowser_m1750484767 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___flag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_setHorizontalScrollBarEnabled(System.String,System.Boolean)
extern "C"  void UniWebViewInterface_uv_setHorizontalScrollBarEnabled_m967413607 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___enabled1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::SetHorizontalScrollBarEnabled(System.String,System.Boolean)
extern "C"  void UniWebViewInterface_SetHorizontalScrollBarEnabled_m2153330197 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___enabled1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_setVerticalScrollBarEnabled(System.String,System.Boolean)
extern "C"  void UniWebViewInterface_uv_setVerticalScrollBarEnabled_m710659641 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___enabled1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::SetVerticalScrollBarEnabled(System.String,System.Boolean)
extern "C"  void UniWebViewInterface_SetVerticalScrollBarEnabled_m1686194279 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___enabled1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_setBouncesEnabled(System.String,System.Boolean)
extern "C"  void UniWebViewInterface_uv_setBouncesEnabled_m656815710 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___enabled1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::SetBouncesEnabled(System.String,System.Boolean)
extern "C"  void UniWebViewInterface_SetBouncesEnabled_m2760372748 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___enabled1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_setZoomEnabled(System.String,System.Boolean)
extern "C"  void UniWebViewInterface_uv_setZoomEnabled_m3737913182 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___enabled1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::SetZoomEnabled(System.String,System.Boolean)
extern "C"  void UniWebViewInterface_SetZoomEnabled_m161847920 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___enabled1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_setShowToolbar(System.String,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern "C"  void UniWebViewInterface_uv_setShowToolbar_m1882464943 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___show1, bool ___animated2, bool ___onTop3, bool ___adjustInset4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::SetShowToolbar(System.String,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern "C"  void UniWebViewInterface_SetShowToolbar_m1119184093 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___show1, bool ___animated2, bool ___onTop3, bool ___adjustInset4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_setToolbarDoneButtonText(System.String,System.String)
extern "C"  void UniWebViewInterface_uv_setToolbarDoneButtonText_m28263149 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___text1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::SetToolbarDoneButtonText(System.String,System.String)
extern "C"  void UniWebViewInterface_SetToolbarDoneButtonText_m1410663451 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___text1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_setWindowUserResizeEnabled(System.String,System.Boolean)
extern "C"  void UniWebViewInterface_uv_setWindowUserResizeEnabled_m2383557186 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___enabled1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::SetWindowUserResizeEnabled(System.String,System.Boolean)
extern "C"  void UniWebViewInterface_SetWindowUserResizeEnabled_m1029552724 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___enabled1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_setWebContentsDebuggingEnabled(System.Boolean)
extern "C"  void UniWebViewInterface_uv_setWebContentsDebuggingEnabled_m393193229 (Il2CppObject * __this /* static, unused */, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::SetWebContentsDebuggingEnabled(System.Boolean)
extern "C"  void UniWebViewInterface_SetWebContentsDebuggingEnabled_m4063280543 (Il2CppObject * __this /* static, unused */, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::uv_print(System.String)
extern "C"  void UniWebViewInterface_uv_print_m2062995402 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::Print(System.String)
extern "C"  void UniWebViewInterface_Print_m1259731164 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface::CheckPlatform()
extern "C"  void UniWebViewInterface_CheckPlatform_m3560843092 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
