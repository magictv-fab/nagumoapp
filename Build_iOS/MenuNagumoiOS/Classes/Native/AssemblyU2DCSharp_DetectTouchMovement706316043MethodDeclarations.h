﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DetectTouchMovement
struct DetectTouchMovement_t706316043;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void DetectTouchMovement::.ctor()
extern "C"  void DetectTouchMovement__ctor_m4260645056 (DetectTouchMovement_t706316043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DetectTouchMovement::Calculate()
extern "C"  void DetectTouchMovement_Calculate_m3366435140 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DetectTouchMovement::Angle(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  float DetectTouchMovement_Angle_m4257208529 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___pos10, Vector2_t4282066565  ___pos21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
