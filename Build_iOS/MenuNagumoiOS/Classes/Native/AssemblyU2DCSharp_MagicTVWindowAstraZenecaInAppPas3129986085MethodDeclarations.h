﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTVWindowAstraZenecaInAppPassword
struct MagicTVWindowAstraZenecaInAppPassword_t3129986085;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MagicTVWindowAstraZenecaInAppPassword::.ctor()
extern "C"  void MagicTVWindowAstraZenecaInAppPassword__ctor_m3247126246 (MagicTVWindowAstraZenecaInAppPassword_t3129986085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowAstraZenecaInAppPassword::PasswordChange(System.String)
extern "C"  void MagicTVWindowAstraZenecaInAppPassword_PasswordChange_m924356825 (MagicTVWindowAstraZenecaInAppPassword_t3129986085 * __this, String_t* ___password0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowAstraZenecaInAppPassword::LoginClick()
extern "C"  void MagicTVWindowAstraZenecaInAppPassword_LoginClick_m2562984893 (MagicTVWindowAstraZenecaInAppPassword_t3129986085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
