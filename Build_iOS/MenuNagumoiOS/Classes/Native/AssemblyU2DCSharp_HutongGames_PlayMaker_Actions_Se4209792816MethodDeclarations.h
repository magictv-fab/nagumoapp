﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetGUITexture
struct SetGUITexture_t4209792816;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetGUITexture::.ctor()
extern "C"  void SetGUITexture__ctor_m2750025734 (SetGUITexture_t4209792816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUITexture::Reset()
extern "C"  void SetGUITexture_Reset_m396458675 (SetGUITexture_t4209792816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUITexture::OnEnter()
extern "C"  void SetGUITexture_OnEnter_m761571933 (SetGUITexture_t4209792816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
