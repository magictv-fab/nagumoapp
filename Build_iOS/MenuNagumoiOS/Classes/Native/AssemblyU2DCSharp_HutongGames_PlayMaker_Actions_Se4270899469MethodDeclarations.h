﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetLightIntensity
struct SetLightIntensity_t4270899469;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetLightIntensity::.ctor()
extern "C"  void SetLightIntensity__ctor_m375306953 (SetLightIntensity_t4270899469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightIntensity::Reset()
extern "C"  void SetLightIntensity_Reset_m2316707190 (SetLightIntensity_t4270899469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightIntensity::OnEnter()
extern "C"  void SetLightIntensity_OnEnter_m3579424864 (SetLightIntensity_t4270899469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightIntensity::OnUpdate()
extern "C"  void SetLightIntensity_OnUpdate_m2721547747 (SetLightIntensity_t4270899469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightIntensity::DoSetLightIntensity()
extern "C"  void SetLightIntensity_DoSetLightIntensity_m2178421723 (SetLightIntensity_t4270899469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
