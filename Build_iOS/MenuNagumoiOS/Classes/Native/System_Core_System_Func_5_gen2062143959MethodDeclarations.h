﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Func`5<System.Object,System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat,System.Object>
struct Func_5_t2062143959;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "QRCode_ZXing_RGBLuminanceSource_BitmapFormat3665922245.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void System.Func`5<System.Object,System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_5__ctor_m1898550480_gshared (Func_5_t2062143959 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Func_5__ctor_m1898550480(__this, ___object0, ___method1, method) ((  void (*) (Func_5_t2062143959 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_5__ctor_m1898550480_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`5<System.Object,System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat,System.Object>::Invoke(T1,T2,T3,T4)
extern "C"  Il2CppObject * Func_5_Invoke_m1781865976_gshared (Func_5_t2062143959 * __this, Il2CppObject * ___arg10, int32_t ___arg21, int32_t ___arg32, int32_t ___arg43, const MethodInfo* method);
#define Func_5_Invoke_m1781865976(__this, ___arg10, ___arg21, ___arg32, ___arg43, method) ((  Il2CppObject * (*) (Func_5_t2062143959 *, Il2CppObject *, int32_t, int32_t, int32_t, const MethodInfo*))Func_5_Invoke_m1781865976_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, method)
// System.IAsyncResult System.Func`5<System.Object,System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat,System.Object>::BeginInvoke(T1,T2,T3,T4,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_5_BeginInvoke_m2558481243_gshared (Func_5_t2062143959 * __this, Il2CppObject * ___arg10, int32_t ___arg21, int32_t ___arg32, int32_t ___arg43, AsyncCallback_t1369114871 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method);
#define Func_5_BeginInvoke_m2558481243(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___callback4, ___object5, method) ((  Il2CppObject * (*) (Func_5_t2062143959 *, Il2CppObject *, int32_t, int32_t, int32_t, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_5_BeginInvoke_m2558481243_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___callback4, ___object5, method)
// TResult System.Func`5<System.Object,System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_5_EndInvoke_m1829609986_gshared (Func_5_t2062143959 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Func_5_EndInvoke_m1829609986(__this, ___result0, method) ((  Il2CppObject * (*) (Func_5_t2062143959 *, Il2CppObject *, const MethodInfo*))Func_5_EndInvoke_m1829609986_gshared)(__this, ___result0, method)
