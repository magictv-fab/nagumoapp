﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iTw410382178.h"
#include "AssemblyU2DCSharp_iTween_LoopType1485160459.h"
#include "UnityEngine_UnityEngine_Space4209342076.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT3260241011.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.iTweenPunchPosition
struct  iTweenPunchPosition_t2465480643  : public iTweenFsmAction_t410382178
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.iTweenPunchPosition::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.iTweenPunchPosition::id
	FsmString_t952858651 * ___id_18;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.iTweenPunchPosition::vector
	FsmVector3_t533912882 * ___vector_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.iTweenPunchPosition::time
	FsmFloat_t2134102846 * ___time_20;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.iTweenPunchPosition::delay
	FsmFloat_t2134102846 * ___delay_21;
	// iTween/LoopType HutongGames.PlayMaker.Actions.iTweenPunchPosition::loopType
	int32_t ___loopType_22;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.iTweenPunchPosition::space
	int32_t ___space_23;
	// HutongGames.PlayMaker.Actions.iTweenFsmAction/AxisRestriction HutongGames.PlayMaker.Actions.iTweenPunchPosition::axis
	int32_t ___axis_24;

public:
	inline static int32_t get_offset_of_gameObject_17() { return static_cast<int32_t>(offsetof(iTweenPunchPosition_t2465480643, ___gameObject_17)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_17() const { return ___gameObject_17; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_17() { return &___gameObject_17; }
	inline void set_gameObject_17(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_17 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_17, value);
	}

	inline static int32_t get_offset_of_id_18() { return static_cast<int32_t>(offsetof(iTweenPunchPosition_t2465480643, ___id_18)); }
	inline FsmString_t952858651 * get_id_18() const { return ___id_18; }
	inline FsmString_t952858651 ** get_address_of_id_18() { return &___id_18; }
	inline void set_id_18(FsmString_t952858651 * value)
	{
		___id_18 = value;
		Il2CppCodeGenWriteBarrier(&___id_18, value);
	}

	inline static int32_t get_offset_of_vector_19() { return static_cast<int32_t>(offsetof(iTweenPunchPosition_t2465480643, ___vector_19)); }
	inline FsmVector3_t533912882 * get_vector_19() const { return ___vector_19; }
	inline FsmVector3_t533912882 ** get_address_of_vector_19() { return &___vector_19; }
	inline void set_vector_19(FsmVector3_t533912882 * value)
	{
		___vector_19 = value;
		Il2CppCodeGenWriteBarrier(&___vector_19, value);
	}

	inline static int32_t get_offset_of_time_20() { return static_cast<int32_t>(offsetof(iTweenPunchPosition_t2465480643, ___time_20)); }
	inline FsmFloat_t2134102846 * get_time_20() const { return ___time_20; }
	inline FsmFloat_t2134102846 ** get_address_of_time_20() { return &___time_20; }
	inline void set_time_20(FsmFloat_t2134102846 * value)
	{
		___time_20 = value;
		Il2CppCodeGenWriteBarrier(&___time_20, value);
	}

	inline static int32_t get_offset_of_delay_21() { return static_cast<int32_t>(offsetof(iTweenPunchPosition_t2465480643, ___delay_21)); }
	inline FsmFloat_t2134102846 * get_delay_21() const { return ___delay_21; }
	inline FsmFloat_t2134102846 ** get_address_of_delay_21() { return &___delay_21; }
	inline void set_delay_21(FsmFloat_t2134102846 * value)
	{
		___delay_21 = value;
		Il2CppCodeGenWriteBarrier(&___delay_21, value);
	}

	inline static int32_t get_offset_of_loopType_22() { return static_cast<int32_t>(offsetof(iTweenPunchPosition_t2465480643, ___loopType_22)); }
	inline int32_t get_loopType_22() const { return ___loopType_22; }
	inline int32_t* get_address_of_loopType_22() { return &___loopType_22; }
	inline void set_loopType_22(int32_t value)
	{
		___loopType_22 = value;
	}

	inline static int32_t get_offset_of_space_23() { return static_cast<int32_t>(offsetof(iTweenPunchPosition_t2465480643, ___space_23)); }
	inline int32_t get_space_23() const { return ___space_23; }
	inline int32_t* get_address_of_space_23() { return &___space_23; }
	inline void set_space_23(int32_t value)
	{
		___space_23 = value;
	}

	inline static int32_t get_offset_of_axis_24() { return static_cast<int32_t>(offsetof(iTweenPunchPosition_t2465480643, ___axis_24)); }
	inline int32_t get_axis_24() const { return ___axis_24; }
	inline int32_t* get_address_of_axis_24() { return &___axis_24; }
	inline void set_axis_24(int32_t value)
	{
		___axis_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
