﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.QrCode.Internal.DataMask/DataMask001
struct DataMask001_t1996578301;

#include "codegen/il2cpp-codegen.h"

// System.Boolean ZXing.QrCode.Internal.DataMask/DataMask001::isMasked(System.Int32,System.Int32)
extern "C"  bool DataMask001_isMasked_m2107176183 (DataMask001_t1996578301 * __this, int32_t ___i0, int32_t ___j1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.DataMask/DataMask001::.ctor()
extern "C"  void DataMask001__ctor_m2889712606 (DataMask001_t1996578301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
