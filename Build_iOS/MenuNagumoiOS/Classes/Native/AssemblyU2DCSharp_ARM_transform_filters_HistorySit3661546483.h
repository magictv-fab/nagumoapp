﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.transform.filters.abstracts.histories.HistoryPositionFilterAbstract
struct HistoryPositionFilterAbstract_t673389937;
// ARM.transform.filters.abstracts.histories.HistoryAngleFilterAbstract
struct HistoryAngleFilterAbstract_t4059889395;

#include "AssemblyU2DCSharp_ARM_transform_filters_abstracts_4158746314.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.transform.filters.HistorySituationCamera
struct  HistorySituationCamera_t3661546483  : public TransformPositionAngleFilterAbstract_t4158746314
{
public:
	// ARM.transform.filters.abstracts.histories.HistoryPositionFilterAbstract ARM.transform.filters.HistorySituationCamera::positionFilter
	HistoryPositionFilterAbstract_t673389937 * ___positionFilter_15;
	// ARM.transform.filters.abstracts.histories.HistoryAngleFilterAbstract ARM.transform.filters.HistorySituationCamera::angleFilter
	HistoryAngleFilterAbstract_t4059889395 * ___angleFilter_16;
	// System.Boolean ARM.transform.filters.HistorySituationCamera::_hasPositionFilter
	bool ____hasPositionFilter_17;
	// System.Boolean ARM.transform.filters.HistorySituationCamera::_hasAngleFilter
	bool ____hasAngleFilter_18;
	// System.Boolean ARM.transform.filters.HistorySituationCamera::_isActive
	bool ____isActive_19;

public:
	inline static int32_t get_offset_of_positionFilter_15() { return static_cast<int32_t>(offsetof(HistorySituationCamera_t3661546483, ___positionFilter_15)); }
	inline HistoryPositionFilterAbstract_t673389937 * get_positionFilter_15() const { return ___positionFilter_15; }
	inline HistoryPositionFilterAbstract_t673389937 ** get_address_of_positionFilter_15() { return &___positionFilter_15; }
	inline void set_positionFilter_15(HistoryPositionFilterAbstract_t673389937 * value)
	{
		___positionFilter_15 = value;
		Il2CppCodeGenWriteBarrier(&___positionFilter_15, value);
	}

	inline static int32_t get_offset_of_angleFilter_16() { return static_cast<int32_t>(offsetof(HistorySituationCamera_t3661546483, ___angleFilter_16)); }
	inline HistoryAngleFilterAbstract_t4059889395 * get_angleFilter_16() const { return ___angleFilter_16; }
	inline HistoryAngleFilterAbstract_t4059889395 ** get_address_of_angleFilter_16() { return &___angleFilter_16; }
	inline void set_angleFilter_16(HistoryAngleFilterAbstract_t4059889395 * value)
	{
		___angleFilter_16 = value;
		Il2CppCodeGenWriteBarrier(&___angleFilter_16, value);
	}

	inline static int32_t get_offset_of__hasPositionFilter_17() { return static_cast<int32_t>(offsetof(HistorySituationCamera_t3661546483, ____hasPositionFilter_17)); }
	inline bool get__hasPositionFilter_17() const { return ____hasPositionFilter_17; }
	inline bool* get_address_of__hasPositionFilter_17() { return &____hasPositionFilter_17; }
	inline void set__hasPositionFilter_17(bool value)
	{
		____hasPositionFilter_17 = value;
	}

	inline static int32_t get_offset_of__hasAngleFilter_18() { return static_cast<int32_t>(offsetof(HistorySituationCamera_t3661546483, ____hasAngleFilter_18)); }
	inline bool get__hasAngleFilter_18() const { return ____hasAngleFilter_18; }
	inline bool* get_address_of__hasAngleFilter_18() { return &____hasAngleFilter_18; }
	inline void set__hasAngleFilter_18(bool value)
	{
		____hasAngleFilter_18 = value;
	}

	inline static int32_t get_offset_of__isActive_19() { return static_cast<int32_t>(offsetof(HistorySituationCamera_t3661546483, ____isActive_19)); }
	inline bool get__isActive_19() const { return ____isActive_19; }
	inline bool* get_address_of__isActive_19() { return &____isActive_19; }
	inline void set__isActive_19(bool value)
	{
		____isActive_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
