﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Button
struct Button_t3896396478;
// UnityEngine.UI.Text
struct Text_t9039225;
// System.Action
struct Action_t3771233898;

#include "AssemblyU2DCSharp_GenericWindowControllerScript248075822.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTVWindowSplashDownloadProgressControllerScript
struct  MagicTVWindowSplashDownloadProgressControllerScript_t3918082434  : public GenericWindowControllerScript_t248075822
{
public:
	// UnityEngine.UI.Button MagicTVWindowSplashDownloadProgressControllerScript::ButtonCancelDownload
	Button_t3896396478 * ___ButtonCancelDownload_4;
	// UnityEngine.UI.Text MagicTVWindowSplashDownloadProgressControllerScript::progressText
	Text_t9039225 * ___progressText_5;
	// System.Action MagicTVWindowSplashDownloadProgressControllerScript::onClickCancelDownload
	Action_t3771233898 * ___onClickCancelDownload_6;
	// System.Single MagicTVWindowSplashDownloadProgressControllerScript::_progress
	float ____progress_7;

public:
	inline static int32_t get_offset_of_ButtonCancelDownload_4() { return static_cast<int32_t>(offsetof(MagicTVWindowSplashDownloadProgressControllerScript_t3918082434, ___ButtonCancelDownload_4)); }
	inline Button_t3896396478 * get_ButtonCancelDownload_4() const { return ___ButtonCancelDownload_4; }
	inline Button_t3896396478 ** get_address_of_ButtonCancelDownload_4() { return &___ButtonCancelDownload_4; }
	inline void set_ButtonCancelDownload_4(Button_t3896396478 * value)
	{
		___ButtonCancelDownload_4 = value;
		Il2CppCodeGenWriteBarrier(&___ButtonCancelDownload_4, value);
	}

	inline static int32_t get_offset_of_progressText_5() { return static_cast<int32_t>(offsetof(MagicTVWindowSplashDownloadProgressControllerScript_t3918082434, ___progressText_5)); }
	inline Text_t9039225 * get_progressText_5() const { return ___progressText_5; }
	inline Text_t9039225 ** get_address_of_progressText_5() { return &___progressText_5; }
	inline void set_progressText_5(Text_t9039225 * value)
	{
		___progressText_5 = value;
		Il2CppCodeGenWriteBarrier(&___progressText_5, value);
	}

	inline static int32_t get_offset_of_onClickCancelDownload_6() { return static_cast<int32_t>(offsetof(MagicTVWindowSplashDownloadProgressControllerScript_t3918082434, ___onClickCancelDownload_6)); }
	inline Action_t3771233898 * get_onClickCancelDownload_6() const { return ___onClickCancelDownload_6; }
	inline Action_t3771233898 ** get_address_of_onClickCancelDownload_6() { return &___onClickCancelDownload_6; }
	inline void set_onClickCancelDownload_6(Action_t3771233898 * value)
	{
		___onClickCancelDownload_6 = value;
		Il2CppCodeGenWriteBarrier(&___onClickCancelDownload_6, value);
	}

	inline static int32_t get_offset_of__progress_7() { return static_cast<int32_t>(offsetof(MagicTVWindowSplashDownloadProgressControllerScript_t3918082434, ____progress_7)); }
	inline float get__progress_7() const { return ____progress_7; }
	inline float* get_address_of__progress_7() { return &____progress_7; }
	inline void set__progress_7(float value)
	{
		____progress_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
