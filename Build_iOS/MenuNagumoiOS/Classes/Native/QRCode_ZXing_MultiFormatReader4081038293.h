﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>
struct IDictionary_2_t759989846;
// System.Collections.Generic.IList`1<ZXing.Reader>
struct IList_1_t1009850332;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.MultiFormatReader
struct  MultiFormatReader_t4081038293  : public Il2CppObject
{
public:
	// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object> ZXing.MultiFormatReader::hints
	Il2CppObject* ___hints_0;
	// System.Collections.Generic.IList`1<ZXing.Reader> ZXing.MultiFormatReader::readers
	Il2CppObject* ___readers_1;

public:
	inline static int32_t get_offset_of_hints_0() { return static_cast<int32_t>(offsetof(MultiFormatReader_t4081038293, ___hints_0)); }
	inline Il2CppObject* get_hints_0() const { return ___hints_0; }
	inline Il2CppObject** get_address_of_hints_0() { return &___hints_0; }
	inline void set_hints_0(Il2CppObject* value)
	{
		___hints_0 = value;
		Il2CppCodeGenWriteBarrier(&___hints_0, value);
	}

	inline static int32_t get_offset_of_readers_1() { return static_cast<int32_t>(offsetof(MultiFormatReader_t4081038293, ___readers_1)); }
	inline Il2CppObject* get_readers_1() const { return ___readers_1; }
	inline Il2CppObject** get_address_of_readers_1() { return &___readers_1; }
	inline void set_readers_1(Il2CppObject* value)
	{
		___readers_1 = value;
		Il2CppCodeGenWriteBarrier(&___readers_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
