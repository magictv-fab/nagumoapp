﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.abstracts.ToggleOnOffAbstract
struct ToggleOnOffAbstract_t832893812;

#include "AssemblyU2DCSharp_ARM_abstracts_ToggleOnOffAbstract832893812.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.events.InvertToggleEvent
struct  InvertToggleEvent_t625222166  : public ToggleOnOffAbstract_t832893812
{
public:
	// ARM.abstracts.ToggleOnOffAbstract ARM.events.InvertToggleEvent::toggle
	ToggleOnOffAbstract_t832893812 * ___toggle_7;

public:
	inline static int32_t get_offset_of_toggle_7() { return static_cast<int32_t>(offsetof(InvertToggleEvent_t625222166, ___toggle_7)); }
	inline ToggleOnOffAbstract_t832893812 * get_toggle_7() const { return ___toggle_7; }
	inline ToggleOnOffAbstract_t832893812 ** get_address_of_toggle_7() { return &___toggle_7; }
	inline void set_toggle_7(ToggleOnOffAbstract_t832893812 * value)
	{
		___toggle_7 = value;
		Il2CppCodeGenWriteBarrier(&___toggle_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
