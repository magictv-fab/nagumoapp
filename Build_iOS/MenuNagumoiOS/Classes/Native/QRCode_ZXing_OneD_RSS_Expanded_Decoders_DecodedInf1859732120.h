﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "QRCode_ZXing_OneD_RSS_Expanded_Decoders_DecodedObje746289471.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation
struct  DecodedInformation_t1859732120  : public DecodedObject_t746289471
{
public:
	// System.String ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation::newString
	String_t* ___newString_1;
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation::remainingValue
	int32_t ___remainingValue_2;
	// System.Boolean ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation::remaining
	bool ___remaining_3;

public:
	inline static int32_t get_offset_of_newString_1() { return static_cast<int32_t>(offsetof(DecodedInformation_t1859732120, ___newString_1)); }
	inline String_t* get_newString_1() const { return ___newString_1; }
	inline String_t** get_address_of_newString_1() { return &___newString_1; }
	inline void set_newString_1(String_t* value)
	{
		___newString_1 = value;
		Il2CppCodeGenWriteBarrier(&___newString_1, value);
	}

	inline static int32_t get_offset_of_remainingValue_2() { return static_cast<int32_t>(offsetof(DecodedInformation_t1859732120, ___remainingValue_2)); }
	inline int32_t get_remainingValue_2() const { return ___remainingValue_2; }
	inline int32_t* get_address_of_remainingValue_2() { return &___remainingValue_2; }
	inline void set_remainingValue_2(int32_t value)
	{
		___remainingValue_2 = value;
	}

	inline static int32_t get_offset_of_remaining_3() { return static_cast<int32_t>(offsetof(DecodedInformation_t1859732120, ___remaining_3)); }
	inline bool get_remaining_3() const { return ___remaining_3; }
	inline bool* get_address_of_remaining_3() { return &___remaining_3; }
	inline void set_remaining_3(bool value)
	{
		___remaining_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
