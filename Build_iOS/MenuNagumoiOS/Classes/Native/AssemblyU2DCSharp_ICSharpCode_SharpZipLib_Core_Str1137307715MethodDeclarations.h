﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Core.StreamUtils
struct StreamUtils_t1137307715;
// System.IO.Stream
struct Stream_t1561764144;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// ICSharpCode.SharpZipLib.Core.ProgressHandler
struct ProgressHandler_t2724445839;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Core_Pro2724445839.h"
#include "mscorlib_System_TimeSpan413522987.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"

// System.Void ICSharpCode.SharpZipLib.Core.StreamUtils::.ctor()
extern "C"  void StreamUtils__ctor_m2770208136 (StreamUtils_t1137307715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.StreamUtils::ReadFully(System.IO.Stream,System.Byte[])
extern "C"  void StreamUtils_ReadFully_m3286690004 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___stream0, ByteU5BU5D_t4260760469* ___buffer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.StreamUtils::ReadFully(System.IO.Stream,System.Byte[],System.Int32,System.Int32)
extern "C"  void StreamUtils_ReadFully_m3806895412 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___stream0, ByteU5BU5D_t4260760469* ___buffer1, int32_t ___offset2, int32_t ___count3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.StreamUtils::Copy(System.IO.Stream,System.IO.Stream,System.Byte[])
extern "C"  void StreamUtils_Copy_m3207715544 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___source0, Stream_t1561764144 * ___destination1, ByteU5BU5D_t4260760469* ___buffer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.StreamUtils::Copy(System.IO.Stream,System.IO.Stream,System.Byte[],ICSharpCode.SharpZipLib.Core.ProgressHandler,System.TimeSpan,System.Object,System.String)
extern "C"  void StreamUtils_Copy_m2351178671 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___source0, Stream_t1561764144 * ___destination1, ByteU5BU5D_t4260760469* ___buffer2, ProgressHandler_t2724445839 * ___progressHandler3, TimeSpan_t413522987  ___updateInterval4, Il2CppObject * ___sender5, String_t* ___name6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.StreamUtils::Copy(System.IO.Stream,System.IO.Stream,System.Byte[],ICSharpCode.SharpZipLib.Core.ProgressHandler,System.TimeSpan,System.Object,System.String,System.Int64)
extern "C"  void StreamUtils_Copy_m4138514697 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___source0, Stream_t1561764144 * ___destination1, ByteU5BU5D_t4260760469* ___buffer2, ProgressHandler_t2724445839 * ___progressHandler3, TimeSpan_t413522987  ___updateInterval4, Il2CppObject * ___sender5, String_t* ___name6, int64_t ___fixedTarget7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
