﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkCloseConnection
struct  NetworkCloseConnection_t686438608  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.NetworkCloseConnection::connectionIndex
	FsmInt_t1596138449 * ___connectionIndex_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.NetworkCloseConnection::connectionGUID
	FsmString_t952858651 * ___connectionGUID_10;
	// System.Boolean HutongGames.PlayMaker.Actions.NetworkCloseConnection::sendDisconnectionNotification
	bool ___sendDisconnectionNotification_11;

public:
	inline static int32_t get_offset_of_connectionIndex_9() { return static_cast<int32_t>(offsetof(NetworkCloseConnection_t686438608, ___connectionIndex_9)); }
	inline FsmInt_t1596138449 * get_connectionIndex_9() const { return ___connectionIndex_9; }
	inline FsmInt_t1596138449 ** get_address_of_connectionIndex_9() { return &___connectionIndex_9; }
	inline void set_connectionIndex_9(FsmInt_t1596138449 * value)
	{
		___connectionIndex_9 = value;
		Il2CppCodeGenWriteBarrier(&___connectionIndex_9, value);
	}

	inline static int32_t get_offset_of_connectionGUID_10() { return static_cast<int32_t>(offsetof(NetworkCloseConnection_t686438608, ___connectionGUID_10)); }
	inline FsmString_t952858651 * get_connectionGUID_10() const { return ___connectionGUID_10; }
	inline FsmString_t952858651 ** get_address_of_connectionGUID_10() { return &___connectionGUID_10; }
	inline void set_connectionGUID_10(FsmString_t952858651 * value)
	{
		___connectionGUID_10 = value;
		Il2CppCodeGenWriteBarrier(&___connectionGUID_10, value);
	}

	inline static int32_t get_offset_of_sendDisconnectionNotification_11() { return static_cast<int32_t>(offsetof(NetworkCloseConnection_t686438608, ___sendDisconnectionNotification_11)); }
	inline bool get_sendDisconnectionNotification_11() const { return ___sendDisconnectionNotification_11; }
	inline bool* get_address_of_sendDisconnectionNotification_11() { return &___sendDisconnectionNotification_11; }
	inline void set_sendDisconnectionNotification_11(bool value)
	{
		___sendDisconnectionNotification_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
