﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MagicTV.abstracts.comm.ServerCommunicationAbstract/OnUpdateRequestCompleteEventHandler
struct OnUpdateRequestCompleteEventHandler_t239303760;
// MagicTV.abstracts.comm.ServerCommunicationAbstract/OnBool
struct OnBool_t3660731593;
// MagicTV.abstracts.comm.ServerCommunicationAbstract/OnUpdateRequestErrorEventHandler
struct OnUpdateRequestErrorEventHandler_t1521989041;

#include "AssemblyU2DCSharp_ARM_abstracts_components_General3900398046.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.abstracts.comm.ServerCommunicationAbstract
struct  ServerCommunicationAbstract_t3244961639  : public GeneralComponentsAbstract_t3900398046
{
public:
	// MagicTV.abstracts.comm.ServerCommunicationAbstract/OnUpdateRequestCompleteEventHandler MagicTV.abstracts.comm.ServerCommunicationAbstract::onUpdateRequestComplete
	OnUpdateRequestCompleteEventHandler_t239303760 * ___onUpdateRequestComplete_4;
	// MagicTV.abstracts.comm.ServerCommunicationAbstract/OnBool MagicTV.abstracts.comm.ServerCommunicationAbstract::onTokenRequestComplete
	OnBool_t3660731593 * ___onTokenRequestComplete_5;
	// MagicTV.abstracts.comm.ServerCommunicationAbstract/OnUpdateRequestErrorEventHandler MagicTV.abstracts.comm.ServerCommunicationAbstract::onUpdateRequestError
	OnUpdateRequestErrorEventHandler_t1521989041 * ___onUpdateRequestError_6;

public:
	inline static int32_t get_offset_of_onUpdateRequestComplete_4() { return static_cast<int32_t>(offsetof(ServerCommunicationAbstract_t3244961639, ___onUpdateRequestComplete_4)); }
	inline OnUpdateRequestCompleteEventHandler_t239303760 * get_onUpdateRequestComplete_4() const { return ___onUpdateRequestComplete_4; }
	inline OnUpdateRequestCompleteEventHandler_t239303760 ** get_address_of_onUpdateRequestComplete_4() { return &___onUpdateRequestComplete_4; }
	inline void set_onUpdateRequestComplete_4(OnUpdateRequestCompleteEventHandler_t239303760 * value)
	{
		___onUpdateRequestComplete_4 = value;
		Il2CppCodeGenWriteBarrier(&___onUpdateRequestComplete_4, value);
	}

	inline static int32_t get_offset_of_onTokenRequestComplete_5() { return static_cast<int32_t>(offsetof(ServerCommunicationAbstract_t3244961639, ___onTokenRequestComplete_5)); }
	inline OnBool_t3660731593 * get_onTokenRequestComplete_5() const { return ___onTokenRequestComplete_5; }
	inline OnBool_t3660731593 ** get_address_of_onTokenRequestComplete_5() { return &___onTokenRequestComplete_5; }
	inline void set_onTokenRequestComplete_5(OnBool_t3660731593 * value)
	{
		___onTokenRequestComplete_5 = value;
		Il2CppCodeGenWriteBarrier(&___onTokenRequestComplete_5, value);
	}

	inline static int32_t get_offset_of_onUpdateRequestError_6() { return static_cast<int32_t>(offsetof(ServerCommunicationAbstract_t3244961639, ___onUpdateRequestError_6)); }
	inline OnUpdateRequestErrorEventHandler_t1521989041 * get_onUpdateRequestError_6() const { return ___onUpdateRequestError_6; }
	inline OnUpdateRequestErrorEventHandler_t1521989041 ** get_address_of_onUpdateRequestError_6() { return &___onUpdateRequestError_6; }
	inline void set_onUpdateRequestError_6(OnUpdateRequestErrorEventHandler_t1521989041 * value)
	{
		___onUpdateRequestError_6 = value;
		Il2CppCodeGenWriteBarrier(&___onUpdateRequestError_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
