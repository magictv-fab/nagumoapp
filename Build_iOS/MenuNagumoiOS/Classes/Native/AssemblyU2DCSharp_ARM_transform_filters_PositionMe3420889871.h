﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.utils.PoolVector3
struct PoolVector3_t628120362;

#include "AssemblyU2DCSharp_ARM_transform_filters_abstracts_h673389937.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.transform.filters.PositionMedianFilter
struct  PositionMedianFilter_t3420889871  : public HistoryPositionFilterAbstract_t673389937
{
public:
	// System.Int32 ARM.transform.filters.PositionMedianFilter::maxSizeCache
	int32_t ___maxSizeCache_15;
	// ARM.utils.PoolVector3 ARM.transform.filters.PositionMedianFilter::_historyPosition
	PoolVector3_t628120362 * ____historyPosition_16;
	// System.Boolean ARM.transform.filters.PositionMedianFilter::_hasItem
	bool ____hasItem_17;
	// System.Boolean ARM.transform.filters.PositionMedianFilter::_loaded
	bool ____loaded_18;
	// System.Boolean ARM.transform.filters.PositionMedianFilter::isFull
	bool ___isFull_19;
	// System.Boolean ARM.transform.filters.PositionMedianFilter::_calcInProgress
	bool ____calcInProgress_20;
	// System.Boolean ARM.transform.filters.PositionMedianFilter::_lastMeanValueHasValue
	bool ____lastMeanValueHasValue_21;
	// UnityEngine.Vector3 ARM.transform.filters.PositionMedianFilter::__lastMeanValue
	Vector3_t4282066566  _____lastMeanValue_22;
	// System.Boolean ARM.transform.filters.PositionMedianFilter::_saving
	bool ____saving_23;

public:
	inline static int32_t get_offset_of_maxSizeCache_15() { return static_cast<int32_t>(offsetof(PositionMedianFilter_t3420889871, ___maxSizeCache_15)); }
	inline int32_t get_maxSizeCache_15() const { return ___maxSizeCache_15; }
	inline int32_t* get_address_of_maxSizeCache_15() { return &___maxSizeCache_15; }
	inline void set_maxSizeCache_15(int32_t value)
	{
		___maxSizeCache_15 = value;
	}

	inline static int32_t get_offset_of__historyPosition_16() { return static_cast<int32_t>(offsetof(PositionMedianFilter_t3420889871, ____historyPosition_16)); }
	inline PoolVector3_t628120362 * get__historyPosition_16() const { return ____historyPosition_16; }
	inline PoolVector3_t628120362 ** get_address_of__historyPosition_16() { return &____historyPosition_16; }
	inline void set__historyPosition_16(PoolVector3_t628120362 * value)
	{
		____historyPosition_16 = value;
		Il2CppCodeGenWriteBarrier(&____historyPosition_16, value);
	}

	inline static int32_t get_offset_of__hasItem_17() { return static_cast<int32_t>(offsetof(PositionMedianFilter_t3420889871, ____hasItem_17)); }
	inline bool get__hasItem_17() const { return ____hasItem_17; }
	inline bool* get_address_of__hasItem_17() { return &____hasItem_17; }
	inline void set__hasItem_17(bool value)
	{
		____hasItem_17 = value;
	}

	inline static int32_t get_offset_of__loaded_18() { return static_cast<int32_t>(offsetof(PositionMedianFilter_t3420889871, ____loaded_18)); }
	inline bool get__loaded_18() const { return ____loaded_18; }
	inline bool* get_address_of__loaded_18() { return &____loaded_18; }
	inline void set__loaded_18(bool value)
	{
		____loaded_18 = value;
	}

	inline static int32_t get_offset_of_isFull_19() { return static_cast<int32_t>(offsetof(PositionMedianFilter_t3420889871, ___isFull_19)); }
	inline bool get_isFull_19() const { return ___isFull_19; }
	inline bool* get_address_of_isFull_19() { return &___isFull_19; }
	inline void set_isFull_19(bool value)
	{
		___isFull_19 = value;
	}

	inline static int32_t get_offset_of__calcInProgress_20() { return static_cast<int32_t>(offsetof(PositionMedianFilter_t3420889871, ____calcInProgress_20)); }
	inline bool get__calcInProgress_20() const { return ____calcInProgress_20; }
	inline bool* get_address_of__calcInProgress_20() { return &____calcInProgress_20; }
	inline void set__calcInProgress_20(bool value)
	{
		____calcInProgress_20 = value;
	}

	inline static int32_t get_offset_of__lastMeanValueHasValue_21() { return static_cast<int32_t>(offsetof(PositionMedianFilter_t3420889871, ____lastMeanValueHasValue_21)); }
	inline bool get__lastMeanValueHasValue_21() const { return ____lastMeanValueHasValue_21; }
	inline bool* get_address_of__lastMeanValueHasValue_21() { return &____lastMeanValueHasValue_21; }
	inline void set__lastMeanValueHasValue_21(bool value)
	{
		____lastMeanValueHasValue_21 = value;
	}

	inline static int32_t get_offset_of___lastMeanValue_22() { return static_cast<int32_t>(offsetof(PositionMedianFilter_t3420889871, _____lastMeanValue_22)); }
	inline Vector3_t4282066566  get___lastMeanValue_22() const { return _____lastMeanValue_22; }
	inline Vector3_t4282066566 * get_address_of___lastMeanValue_22() { return &_____lastMeanValue_22; }
	inline void set___lastMeanValue_22(Vector3_t4282066566  value)
	{
		_____lastMeanValue_22 = value;
	}

	inline static int32_t get_offset_of__saving_23() { return static_cast<int32_t>(offsetof(PositionMedianFilter_t3420889871, ____saving_23)); }
	inline bool get__saving_23() const { return ____saving_23; }
	inline bool* get_address_of__saving_23() { return &____saving_23; }
	inline void set__saving_23(bool value)
	{
		____saving_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
