﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.utils.VectorCompare
struct VectorCompare_t4238853496;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void ARM.utils.VectorCompare::.ctor()
extern "C"  void VectorCompare__ctor_m3887467512 (VectorCompare_t4238853496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARM.utils.VectorCompare::Compare(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  int32_t VectorCompare_Compare_m2501144771 (VectorCompare_t4238853496 * __this, Vector3_t4282066566  ___x0, Vector3_t4282066566  ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
