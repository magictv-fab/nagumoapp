﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.events.GenericParameterVO
struct GenericParameterVO_t1159220759;

#include "codegen/il2cpp-codegen.h"

// System.Void ARM.events.GenericParameterVO::.ctor()
extern "C"  void GenericParameterVO__ctor_m1627514237 (GenericParameterVO_t1159220759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
