﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetVector3XYZ
struct  SetVector3XYZ_t3385117689  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SetVector3XYZ::vector3Variable
	FsmVector3_t533912882 * ___vector3Variable_9;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SetVector3XYZ::vector3Value
	FsmVector3_t533912882 * ___vector3Value_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetVector3XYZ::x
	FsmFloat_t2134102846 * ___x_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetVector3XYZ::y
	FsmFloat_t2134102846 * ___y_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetVector3XYZ::z
	FsmFloat_t2134102846 * ___z_13;
	// System.Boolean HutongGames.PlayMaker.Actions.SetVector3XYZ::everyFrame
	bool ___everyFrame_14;

public:
	inline static int32_t get_offset_of_vector3Variable_9() { return static_cast<int32_t>(offsetof(SetVector3XYZ_t3385117689, ___vector3Variable_9)); }
	inline FsmVector3_t533912882 * get_vector3Variable_9() const { return ___vector3Variable_9; }
	inline FsmVector3_t533912882 ** get_address_of_vector3Variable_9() { return &___vector3Variable_9; }
	inline void set_vector3Variable_9(FsmVector3_t533912882 * value)
	{
		___vector3Variable_9 = value;
		Il2CppCodeGenWriteBarrier(&___vector3Variable_9, value);
	}

	inline static int32_t get_offset_of_vector3Value_10() { return static_cast<int32_t>(offsetof(SetVector3XYZ_t3385117689, ___vector3Value_10)); }
	inline FsmVector3_t533912882 * get_vector3Value_10() const { return ___vector3Value_10; }
	inline FsmVector3_t533912882 ** get_address_of_vector3Value_10() { return &___vector3Value_10; }
	inline void set_vector3Value_10(FsmVector3_t533912882 * value)
	{
		___vector3Value_10 = value;
		Il2CppCodeGenWriteBarrier(&___vector3Value_10, value);
	}

	inline static int32_t get_offset_of_x_11() { return static_cast<int32_t>(offsetof(SetVector3XYZ_t3385117689, ___x_11)); }
	inline FsmFloat_t2134102846 * get_x_11() const { return ___x_11; }
	inline FsmFloat_t2134102846 ** get_address_of_x_11() { return &___x_11; }
	inline void set_x_11(FsmFloat_t2134102846 * value)
	{
		___x_11 = value;
		Il2CppCodeGenWriteBarrier(&___x_11, value);
	}

	inline static int32_t get_offset_of_y_12() { return static_cast<int32_t>(offsetof(SetVector3XYZ_t3385117689, ___y_12)); }
	inline FsmFloat_t2134102846 * get_y_12() const { return ___y_12; }
	inline FsmFloat_t2134102846 ** get_address_of_y_12() { return &___y_12; }
	inline void set_y_12(FsmFloat_t2134102846 * value)
	{
		___y_12 = value;
		Il2CppCodeGenWriteBarrier(&___y_12, value);
	}

	inline static int32_t get_offset_of_z_13() { return static_cast<int32_t>(offsetof(SetVector3XYZ_t3385117689, ___z_13)); }
	inline FsmFloat_t2134102846 * get_z_13() const { return ___z_13; }
	inline FsmFloat_t2134102846 ** get_address_of_z_13() { return &___z_13; }
	inline void set_z_13(FsmFloat_t2134102846 * value)
	{
		___z_13 = value;
		Il2CppCodeGenWriteBarrier(&___z_13, value);
	}

	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(SetVector3XYZ_t3385117689, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
