﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetStringValue
struct  SetStringValue_t3445720326  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetStringValue::stringVariable
	FsmString_t952858651 * ___stringVariable_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetStringValue::stringValue
	FsmString_t952858651 * ___stringValue_10;
	// System.Boolean HutongGames.PlayMaker.Actions.SetStringValue::everyFrame
	bool ___everyFrame_11;

public:
	inline static int32_t get_offset_of_stringVariable_9() { return static_cast<int32_t>(offsetof(SetStringValue_t3445720326, ___stringVariable_9)); }
	inline FsmString_t952858651 * get_stringVariable_9() const { return ___stringVariable_9; }
	inline FsmString_t952858651 ** get_address_of_stringVariable_9() { return &___stringVariable_9; }
	inline void set_stringVariable_9(FsmString_t952858651 * value)
	{
		___stringVariable_9 = value;
		Il2CppCodeGenWriteBarrier(&___stringVariable_9, value);
	}

	inline static int32_t get_offset_of_stringValue_10() { return static_cast<int32_t>(offsetof(SetStringValue_t3445720326, ___stringValue_10)); }
	inline FsmString_t952858651 * get_stringValue_10() const { return ___stringValue_10; }
	inline FsmString_t952858651 ** get_address_of_stringValue_10() { return &___stringValue_10; }
	inline void set_stringValue_10(FsmString_t952858651 * value)
	{
		___stringValue_10 = value;
		Il2CppCodeGenWriteBarrier(&___stringValue_10, value);
	}

	inline static int32_t get_offset_of_everyFrame_11() { return static_cast<int32_t>(offsetof(SetStringValue_t3445720326, ___everyFrame_11)); }
	inline bool get_everyFrame_11() const { return ___everyFrame_11; }
	inline bool* get_address_of_everyFrame_11() { return &___everyFrame_11; }
	inline void set_everyFrame_11(bool value)
	{
		___everyFrame_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
