﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WebCamTexture
struct WebCamTexture_t1290350902;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeviceCamera
struct  DeviceCamera_t3055454523  : public Il2CppObject
{
public:
	// UnityEngine.WebCamTexture DeviceCamera::webcamera
	WebCamTexture_t1290350902 * ___webcamera_0;
	// System.Boolean DeviceCamera::isRunning
	bool ___isRunning_1;
	// System.Boolean DeviceCamera::isUseEasyWebCam
	bool ___isUseEasyWebCam_2;

public:
	inline static int32_t get_offset_of_webcamera_0() { return static_cast<int32_t>(offsetof(DeviceCamera_t3055454523, ___webcamera_0)); }
	inline WebCamTexture_t1290350902 * get_webcamera_0() const { return ___webcamera_0; }
	inline WebCamTexture_t1290350902 ** get_address_of_webcamera_0() { return &___webcamera_0; }
	inline void set_webcamera_0(WebCamTexture_t1290350902 * value)
	{
		___webcamera_0 = value;
		Il2CppCodeGenWriteBarrier(&___webcamera_0, value);
	}

	inline static int32_t get_offset_of_isRunning_1() { return static_cast<int32_t>(offsetof(DeviceCamera_t3055454523, ___isRunning_1)); }
	inline bool get_isRunning_1() const { return ___isRunning_1; }
	inline bool* get_address_of_isRunning_1() { return &___isRunning_1; }
	inline void set_isRunning_1(bool value)
	{
		___isRunning_1 = value;
	}

	inline static int32_t get_offset_of_isUseEasyWebCam_2() { return static_cast<int32_t>(offsetof(DeviceCamera_t3055454523, ___isUseEasyWebCam_2)); }
	inline bool get_isUseEasyWebCam_2() const { return ___isUseEasyWebCam_2; }
	inline bool* get_address_of_isUseEasyWebCam_2() { return &___isUseEasyWebCam_2; }
	inline void set_isUseEasyWebCam_2(bool value)
	{
		___isUseEasyWebCam_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
