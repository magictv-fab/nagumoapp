﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTVWindowInAppGUIControllerScript
struct MagicTVWindowInAppGUIControllerScript_t4145218373;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MagicTVWindowInAppGUIControllerScript::.ctor()
extern "C"  void MagicTVWindowInAppGUIControllerScript__ctor_m954467270 (MagicTVWindowInAppGUIControllerScript_t4145218373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowInAppGUIControllerScript::CameraClick()
extern "C"  void MagicTVWindowInAppGUIControllerScript_CameraClick_m3182110535 (MagicTVWindowInAppGUIControllerScript_t4145218373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MagicTVWindowInAppGUIControllerScript::SaveScreenShot()
extern "C"  Il2CppObject * MagicTVWindowInAppGUIControllerScript_SaveScreenShot_m1790235481 (MagicTVWindowInAppGUIControllerScript_t4145218373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowInAppGUIControllerScript::HelpClick()
extern "C"  void MagicTVWindowInAppGUIControllerScript_HelpClick_m3943993451 (MagicTVWindowInAppGUIControllerScript_t4145218373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowInAppGUIControllerScript::InstructionsClick()
extern "C"  void MagicTVWindowInAppGUIControllerScript_InstructionsClick_m2841013895 (MagicTVWindowInAppGUIControllerScript_t4145218373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowInAppGUIControllerScript::HowToPopUpClick()
extern "C"  void MagicTVWindowInAppGUIControllerScript_HowToPopUpClick_m2227581131 (MagicTVWindowInAppGUIControllerScript_t4145218373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowInAppGUIControllerScript::AboutClick()
extern "C"  void MagicTVWindowInAppGUIControllerScript_AboutClick_m3016812089 (MagicTVWindowInAppGUIControllerScript_t4145218373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowInAppGUIControllerScript::PrintScreenDone()
extern "C"  void MagicTVWindowInAppGUIControllerScript_PrintScreenDone_m3475868415 (MagicTVWindowInAppGUIControllerScript_t4145218373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowInAppGUIControllerScript::UpdateClick()
extern "C"  void MagicTVWindowInAppGUIControllerScript_UpdateClick_m2972176675 (MagicTVWindowInAppGUIControllerScript_t4145218373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowInAppGUIControllerScript::UpdateStart()
extern "C"  void MagicTVWindowInAppGUIControllerScript_UpdateStart_m214466941 (MagicTVWindowInAppGUIControllerScript_t4145218373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowInAppGUIControllerScript::UpdateDone()
extern "C"  void MagicTVWindowInAppGUIControllerScript_UpdateDone_m543067177 (MagicTVWindowInAppGUIControllerScript_t4145218373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowInAppGUIControllerScript::printScreenDoneLater()
extern "C"  void MagicTVWindowInAppGUIControllerScript_printScreenDoneLater_m2081802511 (MagicTVWindowInAppGUIControllerScript_t4145218373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowInAppGUIControllerScript::ShowOnlyWaterMark(System.Boolean)
extern "C"  void MagicTVWindowInAppGUIControllerScript_ShowOnlyWaterMark_m701539094 (MagicTVWindowInAppGUIControllerScript_t4145218373 * __this, bool ___onlyWaterMark0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowInAppGUIControllerScript::LoadFaceTrackerScene()
extern "C"  void MagicTVWindowInAppGUIControllerScript_LoadFaceTrackerScene_m555083061 (MagicTVWindowInAppGUIControllerScript_t4145218373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowInAppGUIControllerScript::Show()
extern "C"  void MagicTVWindowInAppGUIControllerScript_Show_m2757109883 (MagicTVWindowInAppGUIControllerScript_t4145218373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowInAppGUIControllerScript::screenshotComplete(System.String)
extern "C"  void MagicTVWindowInAppGUIControllerScript_screenshotComplete_m383792965 (MagicTVWindowInAppGUIControllerScript_t4145218373 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowInAppGUIControllerScript::OnClearPress()
extern "C"  void MagicTVWindowInAppGUIControllerScript_OnClearPress_m1791809459 (MagicTVWindowInAppGUIControllerScript_t4145218373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowInAppGUIControllerScript::OnClearUp()
extern "C"  void MagicTVWindowInAppGUIControllerScript_OnClearUp_m699862029 (MagicTVWindowInAppGUIControllerScript_t4145218373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowInAppGUIControllerScript::checkClearCache()
extern "C"  void MagicTVWindowInAppGUIControllerScript_checkClearCache_m440225377 (MagicTVWindowInAppGUIControllerScript_t4145218373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowInAppGUIControllerScript::ResetClick()
extern "C"  void MagicTVWindowInAppGUIControllerScript_ResetClick_m2183777847 (MagicTVWindowInAppGUIControllerScript_t4145218373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowInAppGUIControllerScript::ClearCacheClick()
extern "C"  void MagicTVWindowInAppGUIControllerScript_ClearCacheClick_m3645078967 (MagicTVWindowInAppGUIControllerScript_t4145218373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowInAppGUIControllerScript::BackClick()
extern "C"  void MagicTVWindowInAppGUIControllerScript_BackClick_m1589957637 (MagicTVWindowInAppGUIControllerScript_t4145218373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
