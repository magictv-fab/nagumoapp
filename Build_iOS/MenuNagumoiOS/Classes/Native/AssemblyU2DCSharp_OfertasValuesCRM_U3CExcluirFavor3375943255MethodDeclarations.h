﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertasValuesCRM/<ExcluirFavorito>c__Iterator14
struct U3CExcluirFavoritoU3Ec__Iterator14_t3375943255;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OfertasValuesCRM/<ExcluirFavorito>c__Iterator14::.ctor()
extern "C"  void U3CExcluirFavoritoU3Ec__Iterator14__ctor_m485919476 (U3CExcluirFavoritoU3Ec__Iterator14_t3375943255 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasValuesCRM/<ExcluirFavorito>c__Iterator14::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CExcluirFavoritoU3Ec__Iterator14_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3614319326 (U3CExcluirFavoritoU3Ec__Iterator14_t3375943255 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasValuesCRM/<ExcluirFavorito>c__Iterator14::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CExcluirFavoritoU3Ec__Iterator14_System_Collections_IEnumerator_get_Current_m5087858 (U3CExcluirFavoritoU3Ec__Iterator14_t3375943255 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OfertasValuesCRM/<ExcluirFavorito>c__Iterator14::MoveNext()
extern "C"  bool U3CExcluirFavoritoU3Ec__Iterator14_MoveNext_m1993418368 (U3CExcluirFavoritoU3Ec__Iterator14_t3375943255 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasValuesCRM/<ExcluirFavorito>c__Iterator14::Dispose()
extern "C"  void U3CExcluirFavoritoU3Ec__Iterator14_Dispose_m3010431537 (U3CExcluirFavoritoU3Ec__Iterator14_t3375943255 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasValuesCRM/<ExcluirFavorito>c__Iterator14::Reset()
extern "C"  void U3CExcluirFavoritoU3Ec__Iterator14_Reset_m2427319713 (U3CExcluirFavoritoU3Ec__Iterator14_t3375943255 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
