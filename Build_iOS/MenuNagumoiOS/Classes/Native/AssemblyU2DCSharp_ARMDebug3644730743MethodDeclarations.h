﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"

// System.Void ARMDebug::.cctor()
extern "C"  void ARMDebug__cctor_m945022249 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMDebug::Log(System.Object)
extern "C"  void ARMDebug_Log_m562781358 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMDebug::LogWarning(System.Object)
extern "C"  void ARMDebug_LogWarning_m2178420828 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMDebug::LogError(System.Object)
extern "C"  void ARMDebug_LogError_m872732080 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ARMDebug::getCurrentMiliseconds()
extern "C"  int64_t ARMDebug_getCurrentMiliseconds_m3106970838 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMDebug::ResetCount()
extern "C"  void ARMDebug_ResetCount_m371206016 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMDebug::LogTime(System.String)
extern "C"  void ARMDebug_LogTime_m3546703375 (Il2CppObject * __this /* static, unused */, String_t* ___logText0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
