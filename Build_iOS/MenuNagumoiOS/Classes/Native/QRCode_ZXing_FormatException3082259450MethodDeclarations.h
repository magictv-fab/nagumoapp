﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.FormatException
struct FormatException_t3082259450;

#include "codegen/il2cpp-codegen.h"

// System.Void ZXing.FormatException::.ctor()
extern "C"  void FormatException__ctor_m3326016669 (FormatException_t3082259450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.FormatException ZXing.FormatException::get_Instance()
extern "C"  FormatException_t3082259450 * FormatException_get_Instance_m4115085920 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
