﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23222658402MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<ZXing.ResultPoint,System.Int32>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2967265341(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3309166264 *, ResultPoint_t1538592853 *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m2730552978_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<ZXing.ResultPoint,System.Int32>::get_Key()
#define KeyValuePair_2_get_Key_m1594977680(__this, method) ((  ResultPoint_t1538592853 * (*) (KeyValuePair_2_t3309166264 *, const MethodInfo*))KeyValuePair_2_get_Key_m4285571350_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<ZXing.ResultPoint,System.Int32>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3041691596(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3309166264 *, ResultPoint_t1538592853 *, const MethodInfo*))KeyValuePair_2_set_Key_m1188304983_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<ZXing.ResultPoint,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m2168798977(__this, method) ((  int32_t (*) (KeyValuePair_2_t3309166264 *, const MethodInfo*))KeyValuePair_2_get_Value_m2690735574_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<ZXing.ResultPoint,System.Int32>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2638681548(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3309166264 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m137193687_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<ZXing.ResultPoint,System.Int32>::ToString()
#define KeyValuePair_2_ToString_m3644300694(__this, method) ((  String_t* (*) (KeyValuePair_2_t3309166264 *, const MethodInfo*))KeyValuePair_2_ToString_m2052282219_gshared)(__this, method)
