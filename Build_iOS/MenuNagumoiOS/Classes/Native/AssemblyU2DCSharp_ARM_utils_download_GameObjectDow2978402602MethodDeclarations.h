﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.utils.download.GameObjectDownloader
struct GameObjectDownloader_t2978402602;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.WWW
struct WWW_t3134621005;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ARM.utils.download.GameObjectDownloader::.ctor()
extern "C"  void GameObjectDownloader__ctor_m1642452366 (GameObjectDownloader_t2978402602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.download.GameObjectDownloader::prepare()
extern "C"  void GameObjectDownloader_prepare_m3070706515 (GameObjectDownloader_t2978402602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.download.GameObjectDownloader::Load(System.String,System.Int32)
extern "C"  void GameObjectDownloader_Load_m590301041 (GameObjectDownloader_t2978402602 * __this, String_t* ___url0, int32_t ___attempts1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ARM.utils.download.GameObjectDownloader::doLoad()
extern "C"  Il2CppObject * GameObjectDownloader_doLoad_m2915328799 (GameObjectDownloader_t2978402602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.download.GameObjectDownloader::Dispose()
extern "C"  void GameObjectDownloader_Dispose_m2042009163 (GameObjectDownloader_t2978402602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.download.GameObjectDownloader::OnDestroy()
extern "C"  void GameObjectDownloader_OnDestroy_m949367687 (GameObjectDownloader_t2978402602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARM.utils.download.GameObjectDownloader::IsComplete()
extern "C"  bool GameObjectDownloader_IsComplete_m3912318917 (GameObjectDownloader_t2978402602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWW ARM.utils.download.GameObjectDownloader::GetRequest()
extern "C"  WWW_t3134621005 * GameObjectDownloader_GetRequest_m1251856156 (GameObjectDownloader_t2978402602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.download.GameObjectDownloader::errorCheck()
extern "C"  void GameObjectDownloader_errorCheck_m3981245942 (GameObjectDownloader_t2978402602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.download.GameObjectDownloader::onComplete()
extern "C"  void GameObjectDownloader_onComplete_m1495037006 (GameObjectDownloader_t2978402602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.download.GameObjectDownloader::Update()
extern "C"  void GameObjectDownloader_Update_m1103277887 (GameObjectDownloader_t2978402602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
