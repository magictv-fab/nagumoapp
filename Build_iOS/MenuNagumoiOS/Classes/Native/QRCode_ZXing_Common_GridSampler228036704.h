﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.Common.GridSampler
struct GridSampler_t228036704;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.GridSampler
struct  GridSampler_t228036704  : public Il2CppObject
{
public:

public:
};

struct GridSampler_t228036704_StaticFields
{
public:
	// ZXing.Common.GridSampler ZXing.Common.GridSampler::gridSampler
	GridSampler_t228036704 * ___gridSampler_0;

public:
	inline static int32_t get_offset_of_gridSampler_0() { return static_cast<int32_t>(offsetof(GridSampler_t228036704_StaticFields, ___gridSampler_0)); }
	inline GridSampler_t228036704 * get_gridSampler_0() const { return ___gridSampler_0; }
	inline GridSampler_t228036704 ** get_address_of_gridSampler_0() { return &___gridSampler_0; }
	inline void set_gridSampler_0(GridSampler_t228036704 * value)
	{
		___gridSampler_0 = value;
		Il2CppCodeGenWriteBarrier(&___gridSampler_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
