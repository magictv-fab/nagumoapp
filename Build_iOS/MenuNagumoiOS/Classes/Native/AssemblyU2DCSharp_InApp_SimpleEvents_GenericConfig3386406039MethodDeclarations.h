﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InApp.SimpleEvents.GenericConfig
struct GenericConfig_t3386406039;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void InApp.SimpleEvents.GenericConfig::.ctor()
extern "C"  void GenericConfig__ctor_m2895577171 (GenericConfig_t3386406039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.SimpleEvents.GenericConfig::Start()
extern "C"  void GenericConfig_Start_m1842714963 (GenericConfig_t3386406039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.SimpleEvents.GenericConfig::configByString(System.String)
extern "C"  void GenericConfig_configByString_m2037634631 (GenericConfig_t3386406039 * __this, String_t* ___metadado0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.SimpleEvents.GenericConfig::Reset()
extern "C"  void GenericConfig_Reset_m542010112 (GenericConfig_t3386406039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
