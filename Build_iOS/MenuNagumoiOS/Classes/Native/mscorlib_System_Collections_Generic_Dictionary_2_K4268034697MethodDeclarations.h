﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Key89880820MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.Aztec.Internal.Decoder/Table,System.String[]>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m264486239(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t4268034697 *, Dictionary_2_t2641275246 *, const MethodInfo*))KeyCollection__ctor_m4050008939_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.Aztec.Internal.Decoder/Table,System.String[]>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m13270167(__this, ___item0, method) ((  void (*) (KeyCollection_t4268034697 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m565995275_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.Aztec.Internal.Decoder/Table,System.String[]>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3856980942(__this, method) ((  void (*) (KeyCollection_t4268034697 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m594989890_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.Aztec.Internal.Decoder/Table,System.String[]>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3600910743(__this, ___item0, method) ((  bool (*) (KeyCollection_t4268034697 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2185592867_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.Aztec.Internal.Decoder/Table,System.String[]>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m4196496572(__this, ___item0, method) ((  bool (*) (KeyCollection_t4268034697 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3341393480_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.Aztec.Internal.Decoder/Table,System.String[]>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2630213152(__this, method) ((  Il2CppObject* (*) (KeyCollection_t4268034697 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1777894484_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.Aztec.Internal.Decoder/Table,System.String[]>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m4098194240(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t4268034697 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3741050036_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.Aztec.Internal.Decoder/Table,System.String[]>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m615709647(__this, method) ((  Il2CppObject * (*) (KeyCollection_t4268034697 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1777573059_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.Aztec.Internal.Decoder/Table,System.String[]>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1612642104(__this, method) ((  bool (*) (KeyCollection_t4268034697 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2996796100_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.Aztec.Internal.Decoder/Table,System.String[]>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1662658026(__this, method) ((  bool (*) (KeyCollection_t4268034697 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4256393334_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.Aztec.Internal.Decoder/Table,System.String[]>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3045052316(__this, method) ((  Il2CppObject * (*) (KeyCollection_t4268034697 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1335749608_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.Aztec.Internal.Decoder/Table,System.String[]>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m1288069140(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t4268034697 *, TableU5BU5D_t1820856876*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3666087456_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.Aztec.Internal.Decoder/Table,System.String[]>::GetEnumerator()
#define KeyCollection_GetEnumerator_m427171169(__this, method) ((  Enumerator_t3256211300  (*) (KeyCollection_t4268034697 *, const MethodInfo*))KeyCollection_GetEnumerator_m596646573_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.Aztec.Internal.Decoder/Table,System.String[]>::get_Count()
#define KeyCollection_get_Count_m1228161956(__this, method) ((  int32_t (*) (KeyCollection_t4268034697 *, const MethodInfo*))KeyCollection_get_Count_m1517854896_gshared)(__this, method)
