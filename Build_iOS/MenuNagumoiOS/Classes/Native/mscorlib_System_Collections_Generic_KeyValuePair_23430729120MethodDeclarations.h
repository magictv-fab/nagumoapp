﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23830311593MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.NetworkReachability,System.Action>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m512154488(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3430729120 *, int32_t, Action_t3771233898 *, const MethodInfo*))KeyValuePair_2__ctor_m3523372559_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.NetworkReachability,System.Action>::get_Key()
#define KeyValuePair_2_get_Key_m1455878384(__this, method) ((  int32_t (*) (KeyValuePair_2_t3430729120 *, const MethodInfo*))KeyValuePair_2_get_Key_m2409877625_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.NetworkReachability,System.Action>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2212180913(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3430729120 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m1018898106_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.NetworkReachability,System.Action>::get_Value()
#define KeyValuePair_2_get_Value_m879975700(__this, method) ((  Action_t3771233898 * (*) (KeyValuePair_2_t3430729120 *, const MethodInfo*))KeyValuePair_2_get_Value_m2845212253_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.NetworkReachability,System.Action>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3944957745(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3430729120 *, Action_t3771233898 *, const MethodInfo*))KeyValuePair_2_set_Value_m2102431162_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.NetworkReachability,System.Action>::ToString()
#define KeyValuePair_2_ToString_m2753919543(__this, method) ((  String_t* (*) (KeyValuePair_2_t3430729120 *, const MethodInfo*))KeyValuePair_2_ToString_m2263124942_gshared)(__this, method)
