﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ARM_transform_filters_abstracts_4158746314.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.transform.filters.CameraZoomFilterButton
struct  CameraZoomFilterButton_t1406660070  : public TransformPositionAngleFilterAbstract_t4158746314
{
public:
	// UnityEngine.Vector3 ARM.transform.filters.CameraZoomFilterButton::positionToZoom
	Vector3_t4282066566  ___positionToZoom_15;
	// System.Single ARM.transform.filters.CameraZoomFilterButton::percentZoom
	float ___percentZoom_16;
	// UnityEngine.Vector3 ARM.transform.filters.CameraZoomFilterButton::DebugResultVector
	Vector3_t4282066566  ___DebugResultVector_17;

public:
	inline static int32_t get_offset_of_positionToZoom_15() { return static_cast<int32_t>(offsetof(CameraZoomFilterButton_t1406660070, ___positionToZoom_15)); }
	inline Vector3_t4282066566  get_positionToZoom_15() const { return ___positionToZoom_15; }
	inline Vector3_t4282066566 * get_address_of_positionToZoom_15() { return &___positionToZoom_15; }
	inline void set_positionToZoom_15(Vector3_t4282066566  value)
	{
		___positionToZoom_15 = value;
	}

	inline static int32_t get_offset_of_percentZoom_16() { return static_cast<int32_t>(offsetof(CameraZoomFilterButton_t1406660070, ___percentZoom_16)); }
	inline float get_percentZoom_16() const { return ___percentZoom_16; }
	inline float* get_address_of_percentZoom_16() { return &___percentZoom_16; }
	inline void set_percentZoom_16(float value)
	{
		___percentZoom_16 = value;
	}

	inline static int32_t get_offset_of_DebugResultVector_17() { return static_cast<int32_t>(offsetof(CameraZoomFilterButton_t1406660070, ___DebugResultVector_17)); }
	inline Vector3_t4282066566  get_DebugResultVector_17() const { return ___DebugResultVector_17; }
	inline Vector3_t4282066566 * get_address_of_DebugResultVector_17() { return &___DebugResultVector_17; }
	inline void set_DebugResultVector_17(Vector3_t4282066566  value)
	{
		___DebugResultVector_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
