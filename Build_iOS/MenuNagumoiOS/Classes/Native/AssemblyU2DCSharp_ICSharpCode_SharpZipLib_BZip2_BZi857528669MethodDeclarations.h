﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IO.Stream
struct Stream_t1561764144;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream1561764144.h"

// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2::Decompress(System.IO.Stream,System.IO.Stream,System.Boolean)
extern "C"  void BZip2_Decompress_m3951001434 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___inStream0, Stream_t1561764144 * ___outStream1, bool ___isStreamOwner2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.BZip2.BZip2::Compress(System.IO.Stream,System.IO.Stream,System.Boolean,System.Int32)
extern "C"  void BZip2_Compress_m1393767326 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___inStream0, Stream_t1561764144 * ___outStream1, bool ___isStreamOwner2, int32_t ___level3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
