﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PedidoValues
struct PedidoValues_t2123607271;

#include "codegen/il2cpp-codegen.h"

// System.Void PedidoValues::.ctor()
extern "C"  void PedidoValues__ctor_m2228935060 (PedidoValues_t2123607271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PedidoValues::Start()
extern "C"  void PedidoValues_Start_m1176072852 (PedidoValues_t2123607271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PedidoValues::Update()
extern "C"  void PedidoValues_Update_m2104372217 (PedidoValues_t2123607271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PedidoValues::BtnExcluir()
extern "C"  void PedidoValues_BtnExcluir_m1157464310 (PedidoValues_t2123607271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
