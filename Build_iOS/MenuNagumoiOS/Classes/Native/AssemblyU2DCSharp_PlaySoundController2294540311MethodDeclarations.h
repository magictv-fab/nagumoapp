﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlaySoundController
struct PlaySoundController_t2294540311;

#include "codegen/il2cpp-codegen.h"

// System.Void PlaySoundController::.ctor()
extern "C"  void PlaySoundController__ctor_m3319289140 (PlaySoundController_t2294540311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlaySoundController::PlaySound()
extern "C"  void PlaySoundController_PlaySound_m1255683405 (PlaySoundController_t2294540311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
