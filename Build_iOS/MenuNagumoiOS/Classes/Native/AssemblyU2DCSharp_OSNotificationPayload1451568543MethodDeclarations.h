﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OSNotificationPayload
struct OSNotificationPayload_t1451568543;

#include "codegen/il2cpp-codegen.h"

// System.Void OSNotificationPayload::.ctor()
extern "C"  void OSNotificationPayload__ctor_m352431788 (OSNotificationPayload_t1451568543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
