﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2400260867.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23617918191.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ZXing.EncodeHintType,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2428608609_gshared (InternalEnumerator_1_t2400260867 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2428608609(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2400260867 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2428608609_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ZXing.EncodeHintType,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m607834015_gshared (InternalEnumerator_1_t2400260867 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m607834015(__this, method) ((  void (*) (InternalEnumerator_1_t2400260867 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m607834015_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ZXing.EncodeHintType,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m982409237_gshared (InternalEnumerator_1_t2400260867 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m982409237(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2400260867 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m982409237_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ZXing.EncodeHintType,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2262368120_gshared (InternalEnumerator_1_t2400260867 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2262368120(__this, method) ((  void (*) (InternalEnumerator_1_t2400260867 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2262368120_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ZXing.EncodeHintType,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1464342863_gshared (InternalEnumerator_1_t2400260867 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1464342863(__this, method) ((  bool (*) (InternalEnumerator_1_t2400260867 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1464342863_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ZXing.EncodeHintType,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t3617918191  InternalEnumerator_1_get_Current_m3926715402_gshared (InternalEnumerator_1_t2400260867 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3926715402(__this, method) ((  KeyValuePair_2_t3617918191  (*) (InternalEnumerator_1_t2400260867 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3926715402_gshared)(__this, method)
