﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.ResultMetadataType,System.Object>
struct ValueCollection_t1835634707;
// System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>
struct Dictionary_2_t3135028994;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1066862402.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.ResultMetadataType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m3185583768_gshared (ValueCollection_t1835634707 * __this, Dictionary_2_t3135028994 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m3185583768(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t1835634707 *, Dictionary_2_t3135028994 *, const MethodInfo*))ValueCollection__ctor_m3185583768_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.ResultMetadataType,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1773331098_gshared (ValueCollection_t1835634707 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1773331098(__this, ___item0, method) ((  void (*) (ValueCollection_t1835634707 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1773331098_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.ResultMetadataType,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3185978467_gshared (ValueCollection_t1835634707 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3185978467(__this, method) ((  void (*) (ValueCollection_t1835634707 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3185978467_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.ResultMetadataType,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3812842128_gshared (ValueCollection_t1835634707 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3812842128(__this, ___item0, method) ((  bool (*) (ValueCollection_t1835634707 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3812842128_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.ResultMetadataType,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m693229365_gshared (ValueCollection_t1835634707 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m693229365(__this, ___item0, method) ((  bool (*) (ValueCollection_t1835634707 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m693229365_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.ResultMetadataType,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4045824675_gshared (ValueCollection_t1835634707 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4045824675(__this, method) ((  Il2CppObject* (*) (ValueCollection_t1835634707 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4045824675_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.ResultMetadataType,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m1170664871_gshared (ValueCollection_t1835634707 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m1170664871(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1835634707 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1170664871_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.ResultMetadataType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1732793974_gshared (ValueCollection_t1835634707 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1732793974(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1835634707 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1732793974_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.ResultMetadataType,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2088017987_gshared (ValueCollection_t1835634707 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2088017987(__this, method) ((  bool (*) (ValueCollection_t1835634707 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2088017987_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.ResultMetadataType,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2060382883_gshared (ValueCollection_t1835634707 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2060382883(__this, method) ((  bool (*) (ValueCollection_t1835634707 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2060382883_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.ResultMetadataType,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m1242283733_gshared (ValueCollection_t1835634707 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1242283733(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1835634707 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1242283733_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.ResultMetadataType,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m1981285599_gshared (ValueCollection_t1835634707 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m1981285599(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1835634707 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1981285599_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.ResultMetadataType,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1066862402  ValueCollection_GetEnumerator_m2716111624_gshared (ValueCollection_t1835634707 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m2716111624(__this, method) ((  Enumerator_t1066862402  (*) (ValueCollection_t1835634707 *, const MethodInfo*))ValueCollection_GetEnumerator_m2716111624_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.ResultMetadataType,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m3135133149_gshared (ValueCollection_t1835634707 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m3135133149(__this, method) ((  int32_t (*) (ValueCollection_t1835634707 *, const MethodInfo*))ValueCollection_get_Count_m3135133149_gshared)(__this, method)
