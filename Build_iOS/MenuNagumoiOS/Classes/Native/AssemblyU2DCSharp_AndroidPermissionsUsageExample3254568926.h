﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Action`1<System.String>
struct Action_1_t403047693;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AndroidPermissionsUsageExample
struct  AndroidPermissionsUsageExample_t3254568926  : public MonoBehaviour_t667441552
{
public:

public:
};

struct AndroidPermissionsUsageExample_t3254568926_StaticFields
{
public:
	// System.Action`1<System.String> AndroidPermissionsUsageExample::<>f__am$cache0
	Action_1_t403047693 * ___U3CU3Ef__amU24cache0_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_10() { return static_cast<int32_t>(offsetof(AndroidPermissionsUsageExample_t3254568926_StaticFields, ___U3CU3Ef__amU24cache0_10)); }
	inline Action_1_t403047693 * get_U3CU3Ef__amU24cache0_10() const { return ___U3CU3Ef__amU24cache0_10; }
	inline Action_1_t403047693 ** get_address_of_U3CU3Ef__amU24cache0_10() { return &___U3CU3Ef__amU24cache0_10; }
	inline void set_U3CU3Ef__amU24cache0_10(Action_1_t403047693 * value)
	{
		___U3CU3Ef__amU24cache0_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
