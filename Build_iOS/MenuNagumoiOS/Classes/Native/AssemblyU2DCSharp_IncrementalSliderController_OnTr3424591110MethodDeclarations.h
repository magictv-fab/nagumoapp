﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IncrementalSliderController/OnTransformAxisEventHandler
struct OnTransformAxisEventHandler_t3424591110;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void IncrementalSliderController/OnTransformAxisEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnTransformAxisEventHandler__ctor_m134915037 (OnTransformAxisEventHandler_t3424591110 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IncrementalSliderController/OnTransformAxisEventHandler::Invoke(System.Single)
extern "C"  void OnTransformAxisEventHandler_Invoke_m2290225300 (OnTransformAxisEventHandler_t3424591110 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult IncrementalSliderController/OnTransformAxisEventHandler::BeginInvoke(System.Single,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnTransformAxisEventHandler_BeginInvoke_m3382688943 (OnTransformAxisEventHandler_t3424591110 * __this, float ___value0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IncrementalSliderController/OnTransformAxisEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnTransformAxisEventHandler_EndInvoke_m693931117 (OnTransformAxisEventHandler_t3424591110 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
