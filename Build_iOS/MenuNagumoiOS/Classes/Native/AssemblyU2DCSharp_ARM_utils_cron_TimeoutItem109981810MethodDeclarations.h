﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.utils.cron.TimeoutItem
struct TimeoutItem_t109981810;

#include "codegen/il2cpp-codegen.h"

// System.Void ARM.utils.cron.TimeoutItem::.ctor()
extern "C"  void TimeoutItem__ctor_m1421644760 (TimeoutItem_t109981810 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.cron.TimeoutItem::Play()
extern "C"  void TimeoutItem_Play_m3105213600 (TimeoutItem_t109981810 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.cron.TimeoutItem::Update()
extern "C"  void TimeoutItem_Update_m2848176693 (TimeoutItem_t109981810 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.cron.TimeoutItem::Stop()
extern "C"  void TimeoutItem_Stop_m3198897646 (TimeoutItem_t109981810 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
