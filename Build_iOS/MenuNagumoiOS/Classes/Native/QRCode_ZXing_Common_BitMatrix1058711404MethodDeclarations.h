﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"

// System.Int32 ZXing.Common.BitMatrix::get_Width()
extern "C"  int32_t BitMatrix_get_Width_m942387987 (BitMatrix_t1058711404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Common.BitMatrix::get_Height()
extern "C"  int32_t BitMatrix_get_Height_m3948038172 (BitMatrix_t1058711404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.BitMatrix::.ctor(System.Int32)
extern "C"  void BitMatrix__ctor_m2078294391 (BitMatrix_t1058711404 * __this, int32_t ___dimension0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.BitMatrix::.ctor(System.Int32,System.Int32)
extern "C"  void BitMatrix__ctor_m227357888 (BitMatrix_t1058711404 * __this, int32_t ___width0, int32_t ___height1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.BitMatrix::.ctor(System.Int32,System.Int32,System.Int32,System.Int32[])
extern "C"  void BitMatrix__ctor_m1031100734 (BitMatrix_t1058711404 * __this, int32_t ___width0, int32_t ___height1, int32_t ___rowSize2, Int32U5BU5D_t3230847821* ___bits3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Common.BitMatrix::get_Item(System.Int32,System.Int32)
extern "C"  bool BitMatrix_get_Item_m631366584 (BitMatrix_t1058711404 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.BitMatrix::set_Item(System.Int32,System.Int32,System.Boolean)
extern "C"  void BitMatrix_set_Item_m2036772677 (BitMatrix_t1058711404 * __this, int32_t ___x0, int32_t ___y1, bool ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.BitMatrix::flip(System.Int32,System.Int32)
extern "C"  void BitMatrix_flip_m4258400539 (BitMatrix_t1058711404 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.BitMatrix::clear()
extern "C"  void BitMatrix_clear_m3405986161 (BitMatrix_t1058711404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.BitMatrix::setRegion(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void BitMatrix_setRegion_m3650894188 (BitMatrix_t1058711404 * __this, int32_t ___left0, int32_t ___top1, int32_t ___width2, int32_t ___height3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.BitArray ZXing.Common.BitMatrix::getRow(System.Int32,ZXing.Common.BitArray)
extern "C"  BitArray_t4163851164 * BitMatrix_getRow_m1231093608 (BitMatrix_t1058711404 * __this, int32_t ___y0, BitArray_t4163851164 * ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.BitMatrix::setRow(System.Int32,ZXing.Common.BitArray)
extern "C"  void BitMatrix_setRow_m1930822094 (BitMatrix_t1058711404 * __this, int32_t ___y0, BitArray_t4163851164 * ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.BitMatrix::rotate180()
extern "C"  void BitMatrix_rotate180_m3551225106 (BitMatrix_t1058711404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.Common.BitMatrix::getEnclosingRectangle()
extern "C"  Int32U5BU5D_t3230847821* BitMatrix_getEnclosingRectangle_m1556478673 (BitMatrix_t1058711404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.Common.BitMatrix::getTopLeftOnBit()
extern "C"  Int32U5BU5D_t3230847821* BitMatrix_getTopLeftOnBit_m1151847168 (BitMatrix_t1058711404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.Common.BitMatrix::getBottomRightOnBit()
extern "C"  Int32U5BU5D_t3230847821* BitMatrix_getBottomRightOnBit_m2492165931 (BitMatrix_t1058711404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Common.BitMatrix::Equals(System.Object)
extern "C"  bool BitMatrix_Equals_m495553827 (BitMatrix_t1058711404 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Common.BitMatrix::GetHashCode()
extern "C"  int32_t BitMatrix_GetHashCode_m1929617095 (BitMatrix_t1058711404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.Common.BitMatrix::ToString()
extern "C"  String_t* BitMatrix_ToString_m2413454765 (BitMatrix_t1058711404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.Common.BitMatrix::ToString(System.String,System.String,System.String)
extern "C"  String_t* BitMatrix_ToString_m2288402541 (BitMatrix_t1058711404 * __this, String_t* ___setString0, String_t* ___unsetString1, String_t* ___lineSeparator2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ZXing.Common.BitMatrix::Clone()
extern "C"  Il2CppObject * BitMatrix_Clone_m4118583564 (BitMatrix_t1058711404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
