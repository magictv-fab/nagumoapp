﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<ZXing.Result>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1884012762(__this, ___l0, method) ((  void (*) (Enumerator_t3998581541 *, List_1_t3978908771 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ZXing.Result>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1349563192(__this, method) ((  void (*) (Enumerator_t3998581541 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<ZXing.Result>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2954114734(__this, method) ((  Il2CppObject * (*) (Enumerator_t3998581541 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ZXing.Result>::Dispose()
#define Enumerator_Dispose_m3206194495(__this, method) ((  void (*) (Enumerator_t3998581541 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ZXing.Result>::VerifyState()
#define Enumerator_VerifyState_m602647800(__this, method) ((  void (*) (Enumerator_t3998581541 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<ZXing.Result>::MoveNext()
#define Enumerator_MoveNext_m438149608(__this, method) ((  bool (*) (Enumerator_t3998581541 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<ZXing.Result>::get_Current()
#define Enumerator_get_Current_m932862225(__this, method) ((  Result_t2610723219 * (*) (Enumerator_t3998581541 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
