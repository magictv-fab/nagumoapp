﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.abstracts.screens.LoadScreenAbstract
struct LoadScreenAbstract_t4059313920;
// MagicTV.abstracts.screens.LoadScreenAbstract/OnLoadAnswerEventHandler
struct OnLoadAnswerEventHandler_t3624955211;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_abstracts_screens_LoadSc3624955211.h"

// System.Void MagicTV.abstracts.screens.LoadScreenAbstract::.ctor()
extern "C"  void LoadScreenAbstract__ctor_m1472527396 (LoadScreenAbstract_t4059313920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.LoadScreenAbstract::AddLoadCanceledEventHandler(MagicTV.abstracts.screens.LoadScreenAbstract/OnLoadAnswerEventHandler)
extern "C"  void LoadScreenAbstract_AddLoadCanceledEventHandler_m3675620661 (LoadScreenAbstract_t4059313920 * __this, OnLoadAnswerEventHandler_t3624955211 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.LoadScreenAbstract::RemoveLoadCanceledEventHandler(MagicTV.abstracts.screens.LoadScreenAbstract/OnLoadAnswerEventHandler)
extern "C"  void LoadScreenAbstract_RemoveLoadCanceledEventHandler_m3300229716 (LoadScreenAbstract_t4059313920 * __this, OnLoadAnswerEventHandler_t3624955211 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.LoadScreenAbstract::AddLoadConfirmedEventHandler(MagicTV.abstracts.screens.LoadScreenAbstract/OnLoadAnswerEventHandler)
extern "C"  void LoadScreenAbstract_AddLoadConfirmedEventHandler_m1521864895 (LoadScreenAbstract_t4059313920 * __this, OnLoadAnswerEventHandler_t3624955211 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.LoadScreenAbstract::RemoveLoadConfirmedEventHandler(MagicTV.abstracts.screens.LoadScreenAbstract/OnLoadAnswerEventHandler)
extern "C"  void LoadScreenAbstract_RemoveLoadConfirmedEventHandler_m2769647488 (LoadScreenAbstract_t4059313920 * __this, OnLoadAnswerEventHandler_t3624955211 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.LoadScreenAbstract::RaiseLoadCanceled()
extern "C"  void LoadScreenAbstract_RaiseLoadCanceled_m777366189 (LoadScreenAbstract_t4059313920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.LoadScreenAbstract::RaiseLoadConfirmed()
extern "C"  void LoadScreenAbstract_RaiseLoadConfirmed_m240087981 (LoadScreenAbstract_t4059313920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
