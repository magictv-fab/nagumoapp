﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Common.DecoderResult
struct DecoderResult_t3752650303;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// ZXing.QrCode.Internal.Version
struct Version_t1953509534;
// ZXing.QrCode.Internal.ErrorCorrectionLevel
struct ErrorCorrectionLevel_t1225927610;
// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>
struct IDictionary_2_t759989846;
// ZXing.Common.BitSource
struct BitSource_t1243445190;
// System.Text.StringBuilder
struct StringBuilder_t243639308;
// ZXing.Common.CharacterSetECI
struct CharacterSetECI_t2542265168;
// System.Collections.Generic.IList`1<System.Byte[]>
struct IList_1_t2660440376;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_QrCode_Internal_Version1953509534.h"
#include "QRCode_ZXing_QrCode_Internal_ErrorCorrectionLevel1225927610.h"
#include "QRCode_ZXing_Common_BitSource1243445190.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "QRCode_ZXing_Common_CharacterSetECI2542265168.h"

// ZXing.Common.DecoderResult ZXing.QrCode.Internal.DecodedBitStreamParser::decode(System.Byte[],ZXing.QrCode.Internal.Version,ZXing.QrCode.Internal.ErrorCorrectionLevel,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  DecoderResult_t3752650303 * DecodedBitStreamParser_decode_m4034619803 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, Version_t1953509534 * ___version1, ErrorCorrectionLevel_t1225927610 * ___ecLevel2, Il2CppObject* ___hints3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.QrCode.Internal.DecodedBitStreamParser::decodeHanziSegment(ZXing.Common.BitSource,System.Text.StringBuilder,System.Int32)
extern "C"  bool DecodedBitStreamParser_decodeHanziSegment_m3955162359 (Il2CppObject * __this /* static, unused */, BitSource_t1243445190 * ___bits0, StringBuilder_t243639308 * ___result1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.QrCode.Internal.DecodedBitStreamParser::decodeKanjiSegment(ZXing.Common.BitSource,System.Text.StringBuilder,System.Int32)
extern "C"  bool DecodedBitStreamParser_decodeKanjiSegment_m1646145354 (Il2CppObject * __this /* static, unused */, BitSource_t1243445190 * ___bits0, StringBuilder_t243639308 * ___result1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.QrCode.Internal.DecodedBitStreamParser::decodeByteSegment(ZXing.Common.BitSource,System.Text.StringBuilder,System.Int32,ZXing.Common.CharacterSetECI,System.Collections.Generic.IList`1<System.Byte[]>,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  bool DecodedBitStreamParser_decodeByteSegment_m3642958765 (Il2CppObject * __this /* static, unused */, BitSource_t1243445190 * ___bits0, StringBuilder_t243639308 * ___result1, int32_t ___count2, CharacterSetECI_t2542265168 * ___currentCharacterSetECI3, Il2CppObject* ___byteSegments4, Il2CppObject* ___hints5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char ZXing.QrCode.Internal.DecodedBitStreamParser::toAlphaNumericChar(System.Int32)
extern "C"  Il2CppChar DecodedBitStreamParser_toAlphaNumericChar_m4209382985 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.QrCode.Internal.DecodedBitStreamParser::decodeAlphanumericSegment(ZXing.Common.BitSource,System.Text.StringBuilder,System.Int32,System.Boolean)
extern "C"  bool DecodedBitStreamParser_decodeAlphanumericSegment_m1154408837 (Il2CppObject * __this /* static, unused */, BitSource_t1243445190 * ___bits0, StringBuilder_t243639308 * ___result1, int32_t ___count2, bool ___fc1InEffect3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.QrCode.Internal.DecodedBitStreamParser::decodeNumericSegment(ZXing.Common.BitSource,System.Text.StringBuilder,System.Int32)
extern "C"  bool DecodedBitStreamParser_decodeNumericSegment_m1902253952 (Il2CppObject * __this /* static, unused */, BitSource_t1243445190 * ___bits0, StringBuilder_t243639308 * ___result1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.QrCode.Internal.DecodedBitStreamParser::parseECIValue(ZXing.Common.BitSource)
extern "C"  int32_t DecodedBitStreamParser_parseECIValue_m2549184708 (Il2CppObject * __this /* static, unused */, BitSource_t1243445190 * ___bits0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.DecodedBitStreamParser::.cctor()
extern "C"  void DecodedBitStreamParser__cctor_m3171766883 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
