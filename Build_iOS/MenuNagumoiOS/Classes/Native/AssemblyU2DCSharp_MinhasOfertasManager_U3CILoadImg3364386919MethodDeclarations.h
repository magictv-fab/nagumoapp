﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MinhasOfertasManager/<ILoadImgs>c__Iterator1F
struct U3CILoadImgsU3Ec__Iterator1F_t3364386919;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MinhasOfertasManager/<ILoadImgs>c__Iterator1F::.ctor()
extern "C"  void U3CILoadImgsU3Ec__Iterator1F__ctor_m65522916 (U3CILoadImgsU3Ec__Iterator1F_t3364386919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MinhasOfertasManager/<ILoadImgs>c__Iterator1F::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CILoadImgsU3Ec__Iterator1F_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m128751022 (U3CILoadImgsU3Ec__Iterator1F_t3364386919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MinhasOfertasManager/<ILoadImgs>c__Iterator1F::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CILoadImgsU3Ec__Iterator1F_System_Collections_IEnumerator_get_Current_m2968237378 (U3CILoadImgsU3Ec__Iterator1F_t3364386919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MinhasOfertasManager/<ILoadImgs>c__Iterator1F::MoveNext()
extern "C"  bool U3CILoadImgsU3Ec__Iterator1F_MoveNext_m3698270608 (U3CILoadImgsU3Ec__Iterator1F_t3364386919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MinhasOfertasManager/<ILoadImgs>c__Iterator1F::Dispose()
extern "C"  void U3CILoadImgsU3Ec__Iterator1F_Dispose_m2736263201 (U3CILoadImgsU3Ec__Iterator1F_t3364386919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MinhasOfertasManager/<ILoadImgs>c__Iterator1F::Reset()
extern "C"  void U3CILoadImgsU3Ec__Iterator1F_Reset_m2006923153 (U3CILoadImgsU3Ec__Iterator1F_t3364386919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
