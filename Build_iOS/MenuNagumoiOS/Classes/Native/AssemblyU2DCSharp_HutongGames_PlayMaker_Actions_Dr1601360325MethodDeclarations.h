﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DrawTexture
struct DrawTexture_t1601360325;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DrawTexture::.ctor()
extern "C"  void DrawTexture__ctor_m2580760337 (DrawTexture_t1601360325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DrawTexture::Reset()
extern "C"  void DrawTexture_Reset_m227193278 (DrawTexture_t1601360325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DrawTexture::OnGUI()
extern "C"  void DrawTexture_OnGUI_m2076158987 (DrawTexture_t1601360325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
