﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkGetTime
struct  NetworkGetTime_t1303179293  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.NetworkGetTime::time
	FsmFloat_t2134102846 * ___time_9;

public:
	inline static int32_t get_offset_of_time_9() { return static_cast<int32_t>(offsetof(NetworkGetTime_t1303179293, ___time_9)); }
	inline FsmFloat_t2134102846 * get_time_9() const { return ___time_9; }
	inline FsmFloat_t2134102846 ** get_address_of_time_9() { return &___time_9; }
	inline void set_time_9(FsmFloat_t2134102846 * value)
	{
		___time_9 = value;
		Il2CppCodeGenWriteBarrier(&___time_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
