﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AutoSwapScroll/<Start>c__Iterator8A
struct U3CStartU3Ec__Iterator8A_t3846047746;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AutoSwapScroll/<Start>c__Iterator8A::.ctor()
extern "C"  void U3CStartU3Ec__Iterator8A__ctor_m3696827817 (U3CStartU3Ec__Iterator8A_t3846047746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AutoSwapScroll/<Start>c__Iterator8A::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator8A_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2251546569 (U3CStartU3Ec__Iterator8A_t3846047746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AutoSwapScroll/<Start>c__Iterator8A::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator8A_System_Collections_IEnumerator_get_Current_m1128830301 (U3CStartU3Ec__Iterator8A_t3846047746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AutoSwapScroll/<Start>c__Iterator8A::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator8A_MoveNext_m949579179 (U3CStartU3Ec__Iterator8A_t3846047746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AutoSwapScroll/<Start>c__Iterator8A::Dispose()
extern "C"  void U3CStartU3Ec__Iterator8A_Dispose_m611861414 (U3CStartU3Ec__Iterator8A_t3846047746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AutoSwapScroll/<Start>c__Iterator8A::Reset()
extern "C"  void U3CStartU3Ec__Iterator8A_Reset_m1343260758 (U3CStartU3Ec__Iterator8A_t3846047746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
