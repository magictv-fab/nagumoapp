﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OSEmailSubscriptionStateChanges
struct OSEmailSubscriptionStateChanges_t721307367;

#include "codegen/il2cpp-codegen.h"

// System.Void OSEmailSubscriptionStateChanges::.ctor()
extern "C"  void OSEmailSubscriptionStateChanges__ctor_m3419571300 (OSEmailSubscriptionStateChanges_t721307367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
