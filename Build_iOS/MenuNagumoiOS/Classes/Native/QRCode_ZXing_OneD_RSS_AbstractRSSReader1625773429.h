﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Single[]
struct SingleU5BU5D_t2316563989;

#include "QRCode_ZXing_OneD_OneDReader3436042911.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.AbstractRSSReader
struct  AbstractRSSReader_t1625773429  : public OneDReader_t3436042911
{
public:
	// System.Int32[] ZXing.OneD.RSS.AbstractRSSReader::decodeFinderCounters
	Int32U5BU5D_t3230847821* ___decodeFinderCounters_4;
	// System.Int32[] ZXing.OneD.RSS.AbstractRSSReader::dataCharacterCounters
	Int32U5BU5D_t3230847821* ___dataCharacterCounters_5;
	// System.Single[] ZXing.OneD.RSS.AbstractRSSReader::oddRoundingErrors
	SingleU5BU5D_t2316563989* ___oddRoundingErrors_6;
	// System.Single[] ZXing.OneD.RSS.AbstractRSSReader::evenRoundingErrors
	SingleU5BU5D_t2316563989* ___evenRoundingErrors_7;
	// System.Int32[] ZXing.OneD.RSS.AbstractRSSReader::oddCounts
	Int32U5BU5D_t3230847821* ___oddCounts_8;
	// System.Int32[] ZXing.OneD.RSS.AbstractRSSReader::evenCounts
	Int32U5BU5D_t3230847821* ___evenCounts_9;

public:
	inline static int32_t get_offset_of_decodeFinderCounters_4() { return static_cast<int32_t>(offsetof(AbstractRSSReader_t1625773429, ___decodeFinderCounters_4)); }
	inline Int32U5BU5D_t3230847821* get_decodeFinderCounters_4() const { return ___decodeFinderCounters_4; }
	inline Int32U5BU5D_t3230847821** get_address_of_decodeFinderCounters_4() { return &___decodeFinderCounters_4; }
	inline void set_decodeFinderCounters_4(Int32U5BU5D_t3230847821* value)
	{
		___decodeFinderCounters_4 = value;
		Il2CppCodeGenWriteBarrier(&___decodeFinderCounters_4, value);
	}

	inline static int32_t get_offset_of_dataCharacterCounters_5() { return static_cast<int32_t>(offsetof(AbstractRSSReader_t1625773429, ___dataCharacterCounters_5)); }
	inline Int32U5BU5D_t3230847821* get_dataCharacterCounters_5() const { return ___dataCharacterCounters_5; }
	inline Int32U5BU5D_t3230847821** get_address_of_dataCharacterCounters_5() { return &___dataCharacterCounters_5; }
	inline void set_dataCharacterCounters_5(Int32U5BU5D_t3230847821* value)
	{
		___dataCharacterCounters_5 = value;
		Il2CppCodeGenWriteBarrier(&___dataCharacterCounters_5, value);
	}

	inline static int32_t get_offset_of_oddRoundingErrors_6() { return static_cast<int32_t>(offsetof(AbstractRSSReader_t1625773429, ___oddRoundingErrors_6)); }
	inline SingleU5BU5D_t2316563989* get_oddRoundingErrors_6() const { return ___oddRoundingErrors_6; }
	inline SingleU5BU5D_t2316563989** get_address_of_oddRoundingErrors_6() { return &___oddRoundingErrors_6; }
	inline void set_oddRoundingErrors_6(SingleU5BU5D_t2316563989* value)
	{
		___oddRoundingErrors_6 = value;
		Il2CppCodeGenWriteBarrier(&___oddRoundingErrors_6, value);
	}

	inline static int32_t get_offset_of_evenRoundingErrors_7() { return static_cast<int32_t>(offsetof(AbstractRSSReader_t1625773429, ___evenRoundingErrors_7)); }
	inline SingleU5BU5D_t2316563989* get_evenRoundingErrors_7() const { return ___evenRoundingErrors_7; }
	inline SingleU5BU5D_t2316563989** get_address_of_evenRoundingErrors_7() { return &___evenRoundingErrors_7; }
	inline void set_evenRoundingErrors_7(SingleU5BU5D_t2316563989* value)
	{
		___evenRoundingErrors_7 = value;
		Il2CppCodeGenWriteBarrier(&___evenRoundingErrors_7, value);
	}

	inline static int32_t get_offset_of_oddCounts_8() { return static_cast<int32_t>(offsetof(AbstractRSSReader_t1625773429, ___oddCounts_8)); }
	inline Int32U5BU5D_t3230847821* get_oddCounts_8() const { return ___oddCounts_8; }
	inline Int32U5BU5D_t3230847821** get_address_of_oddCounts_8() { return &___oddCounts_8; }
	inline void set_oddCounts_8(Int32U5BU5D_t3230847821* value)
	{
		___oddCounts_8 = value;
		Il2CppCodeGenWriteBarrier(&___oddCounts_8, value);
	}

	inline static int32_t get_offset_of_evenCounts_9() { return static_cast<int32_t>(offsetof(AbstractRSSReader_t1625773429, ___evenCounts_9)); }
	inline Int32U5BU5D_t3230847821* get_evenCounts_9() const { return ___evenCounts_9; }
	inline Int32U5BU5D_t3230847821** get_address_of_evenCounts_9() { return &___evenCounts_9; }
	inline void set_evenCounts_9(Int32U5BU5D_t3230847821* value)
	{
		___evenCounts_9 = value;
		Il2CppCodeGenWriteBarrier(&___evenCounts_9, value);
	}
};

struct AbstractRSSReader_t1625773429_StaticFields
{
public:
	// System.Int32 ZXing.OneD.RSS.AbstractRSSReader::MAX_AVG_VARIANCE
	int32_t ___MAX_AVG_VARIANCE_2;
	// System.Int32 ZXing.OneD.RSS.AbstractRSSReader::MAX_INDIVIDUAL_VARIANCE
	int32_t ___MAX_INDIVIDUAL_VARIANCE_3;

public:
	inline static int32_t get_offset_of_MAX_AVG_VARIANCE_2() { return static_cast<int32_t>(offsetof(AbstractRSSReader_t1625773429_StaticFields, ___MAX_AVG_VARIANCE_2)); }
	inline int32_t get_MAX_AVG_VARIANCE_2() const { return ___MAX_AVG_VARIANCE_2; }
	inline int32_t* get_address_of_MAX_AVG_VARIANCE_2() { return &___MAX_AVG_VARIANCE_2; }
	inline void set_MAX_AVG_VARIANCE_2(int32_t value)
	{
		___MAX_AVG_VARIANCE_2 = value;
	}

	inline static int32_t get_offset_of_MAX_INDIVIDUAL_VARIANCE_3() { return static_cast<int32_t>(offsetof(AbstractRSSReader_t1625773429_StaticFields, ___MAX_INDIVIDUAL_VARIANCE_3)); }
	inline int32_t get_MAX_INDIVIDUAL_VARIANCE_3() const { return ___MAX_INDIVIDUAL_VARIANCE_3; }
	inline int32_t* get_address_of_MAX_INDIVIDUAL_VARIANCE_3() { return &___MAX_INDIVIDUAL_VARIANCE_3; }
	inline void set_MAX_INDIVIDUAL_VARIANCE_3(int32_t value)
	{
		___MAX_INDIVIDUAL_VARIANCE_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
