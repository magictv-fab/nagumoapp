﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PublishManager/<IFavoritar>c__Iterator74
struct U3CIFavoritarU3Ec__Iterator74_t983049004;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PublishManager/<IFavoritar>c__Iterator74::.ctor()
extern "C"  void U3CIFavoritarU3Ec__Iterator74__ctor_m3051425007 (U3CIFavoritarU3Ec__Iterator74_t983049004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PublishManager/<IFavoritar>c__Iterator74::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CIFavoritarU3Ec__Iterator74_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3982032717 (U3CIFavoritarU3Ec__Iterator74_t983049004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PublishManager/<IFavoritar>c__Iterator74::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CIFavoritarU3Ec__Iterator74_System_Collections_IEnumerator_get_Current_m990199009 (U3CIFavoritarU3Ec__Iterator74_t983049004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PublishManager/<IFavoritar>c__Iterator74::MoveNext()
extern "C"  bool U3CIFavoritarU3Ec__Iterator74_MoveNext_m684493453 (U3CIFavoritarU3Ec__Iterator74_t983049004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PublishManager/<IFavoritar>c__Iterator74::Dispose()
extern "C"  void U3CIFavoritarU3Ec__Iterator74_Dispose_m3150018924 (U3CIFavoritarU3Ec__Iterator74_t983049004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PublishManager/<IFavoritar>c__Iterator74::Reset()
extern "C"  void U3CIFavoritarU3Ec__Iterator74_Reset_m697857948 (U3CIFavoritarU3Ec__Iterator74_t983049004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
