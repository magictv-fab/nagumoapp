﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.ZipFile/ZipString
struct ZipString_t3473946306;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipF3473946306.h"

// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/ZipString::.ctor(System.String)
extern "C"  void ZipString__ctor_m246652089 (ZipString_t3473946306 * __this, String_t* ___comment0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/ZipString::.ctor(System.Byte[])
extern "C"  void ZipString__ctor_m2483813760 (ZipString_t3473946306 * __this, ByteU5BU5D_t4260760469* ___rawString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile/ZipString::get_IsSourceString()
extern "C"  bool ZipString_get_IsSourceString_m2434507046 (ZipString_t3473946306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipFile/ZipString::get_RawLength()
extern "C"  int32_t ZipString_get_RawLength_m3840015322 (ZipString_t3473946306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ICSharpCode.SharpZipLib.Zip.ZipFile/ZipString::get_RawComment()
extern "C"  ByteU5BU5D_t4260760469* ZipString_get_RawComment_m1727923249 (ZipString_t3473946306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/ZipString::Reset()
extern "C"  void ZipString_Reset_m822636438 (ZipString_t3473946306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/ZipString::MakeTextAvailable()
extern "C"  void ZipString_MakeTextAvailable_m3028466261 (ZipString_t3473946306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/ZipString::MakeBytesAvailable()
extern "C"  void ZipString_MakeBytesAvailable_m2765236967 (ZipString_t3473946306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Zip.ZipFile/ZipString::op_Implicit(ICSharpCode.SharpZipLib.Zip.ZipFile/ZipString)
extern "C"  String_t* ZipString_op_Implicit_m2567809315 (Il2CppObject * __this /* static, unused */, ZipString_t3473946306 * ___zipString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
