﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetMaterial
struct SetMaterial_t1870547511;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetMaterial::.ctor()
extern "C"  void SetMaterial__ctor_m2615357151 (SetMaterial_t1870547511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMaterial::Reset()
extern "C"  void SetMaterial_Reset_m261790092 (SetMaterial_t1870547511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMaterial::OnEnter()
extern "C"  void SetMaterial_OnEnter_m194082550 (SetMaterial_t1870547511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMaterial::DoSetMaterial()
extern "C"  void SetMaterial_DoSetMaterial_m1620619931 (SetMaterial_t1870547511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
