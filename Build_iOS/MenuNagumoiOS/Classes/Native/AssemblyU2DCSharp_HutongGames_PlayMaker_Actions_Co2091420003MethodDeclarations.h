﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ColorRamp
struct ColorRamp_t2091420003;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ColorRamp::.ctor()
extern "C"  void ColorRamp__ctor_m865426227 (ColorRamp_t2091420003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ColorRamp::Reset()
extern "C"  void ColorRamp_Reset_m2806826464 (ColorRamp_t2091420003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ColorRamp::OnEnter()
extern "C"  void ColorRamp_OnEnter_m2137644618 (ColorRamp_t2091420003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ColorRamp::OnUpdate()
extern "C"  void ColorRamp_OnUpdate_m976033081 (ColorRamp_t2091420003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ColorRamp::DoColorRamp()
extern "C"  void ColorRamp_DoColorRamp_m696926107 (ColorRamp_t2091420003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Actions.ColorRamp::ErrorCheck()
extern "C"  String_t* ColorRamp_ErrorCheck_m1039163726 (ColorRamp_t2091420003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
