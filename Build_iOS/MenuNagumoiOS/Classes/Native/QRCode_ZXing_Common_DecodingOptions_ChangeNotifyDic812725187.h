﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>
struct IDictionary_2_t759989846;
// System.Action`2<System.Object,System.EventArgs>
struct Action_2_t2663079113;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>
struct  ChangeNotifyDictionary_2_t812725187  : public Il2CppObject
{
public:
	// System.Collections.Generic.IDictionary`2<TKey,TValue> ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::values
	Il2CppObject* ___values_0;
	// System.Action`2<System.Object,System.EventArgs> ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::ValueChanged
	Action_2_t2663079113 * ___ValueChanged_1;

public:
	inline static int32_t get_offset_of_values_0() { return static_cast<int32_t>(offsetof(ChangeNotifyDictionary_2_t812725187, ___values_0)); }
	inline Il2CppObject* get_values_0() const { return ___values_0; }
	inline Il2CppObject** get_address_of_values_0() { return &___values_0; }
	inline void set_values_0(Il2CppObject* value)
	{
		___values_0 = value;
		Il2CppCodeGenWriteBarrier(&___values_0, value);
	}

	inline static int32_t get_offset_of_ValueChanged_1() { return static_cast<int32_t>(offsetof(ChangeNotifyDictionary_2_t812725187, ___ValueChanged_1)); }
	inline Action_2_t2663079113 * get_ValueChanged_1() const { return ___ValueChanged_1; }
	inline Action_2_t2663079113 ** get_address_of_ValueChanged_1() { return &___ValueChanged_1; }
	inline void set_ValueChanged_1(Action_2_t2663079113 * value)
	{
		___ValueChanged_1 = value;
		Il2CppCodeGenWriteBarrier(&___ValueChanged_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
