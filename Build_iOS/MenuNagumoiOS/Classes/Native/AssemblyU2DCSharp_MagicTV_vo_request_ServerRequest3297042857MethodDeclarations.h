﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.vo.request.ServerRequestDeviceVO
struct ServerRequestDeviceVO_t3297042857;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.vo.request.ServerRequestDeviceVO::.ctor()
extern "C"  void ServerRequestDeviceVO__ctor_m3547512149 (ServerRequestDeviceVO_t3297042857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
