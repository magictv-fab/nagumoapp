﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.RectTransform
struct RectTransform_t972643934;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonAlterarSenha
struct  ButtonAlterarSenha_t3991155776  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject ButtonAlterarSenha::seta
	GameObject_t3674682005 * ___seta_2;
	// UnityEngine.GameObject ButtonAlterarSenha::groupSenhas
	GameObject_t3674682005 * ___groupSenhas_3;
	// UnityEngine.RectTransform ButtonAlterarSenha::rectTransform
	RectTransform_t972643934 * ___rectTransform_4;
	// System.Boolean ButtonAlterarSenha::openClose
	bool ___openClose_5;
	// System.Int32 ButtonAlterarSenha::rectCloseY
	int32_t ___rectCloseY_6;
	// System.Int32 ButtonAlterarSenha::rectOpenY
	int32_t ___rectOpenY_7;

public:
	inline static int32_t get_offset_of_seta_2() { return static_cast<int32_t>(offsetof(ButtonAlterarSenha_t3991155776, ___seta_2)); }
	inline GameObject_t3674682005 * get_seta_2() const { return ___seta_2; }
	inline GameObject_t3674682005 ** get_address_of_seta_2() { return &___seta_2; }
	inline void set_seta_2(GameObject_t3674682005 * value)
	{
		___seta_2 = value;
		Il2CppCodeGenWriteBarrier(&___seta_2, value);
	}

	inline static int32_t get_offset_of_groupSenhas_3() { return static_cast<int32_t>(offsetof(ButtonAlterarSenha_t3991155776, ___groupSenhas_3)); }
	inline GameObject_t3674682005 * get_groupSenhas_3() const { return ___groupSenhas_3; }
	inline GameObject_t3674682005 ** get_address_of_groupSenhas_3() { return &___groupSenhas_3; }
	inline void set_groupSenhas_3(GameObject_t3674682005 * value)
	{
		___groupSenhas_3 = value;
		Il2CppCodeGenWriteBarrier(&___groupSenhas_3, value);
	}

	inline static int32_t get_offset_of_rectTransform_4() { return static_cast<int32_t>(offsetof(ButtonAlterarSenha_t3991155776, ___rectTransform_4)); }
	inline RectTransform_t972643934 * get_rectTransform_4() const { return ___rectTransform_4; }
	inline RectTransform_t972643934 ** get_address_of_rectTransform_4() { return &___rectTransform_4; }
	inline void set_rectTransform_4(RectTransform_t972643934 * value)
	{
		___rectTransform_4 = value;
		Il2CppCodeGenWriteBarrier(&___rectTransform_4, value);
	}

	inline static int32_t get_offset_of_openClose_5() { return static_cast<int32_t>(offsetof(ButtonAlterarSenha_t3991155776, ___openClose_5)); }
	inline bool get_openClose_5() const { return ___openClose_5; }
	inline bool* get_address_of_openClose_5() { return &___openClose_5; }
	inline void set_openClose_5(bool value)
	{
		___openClose_5 = value;
	}

	inline static int32_t get_offset_of_rectCloseY_6() { return static_cast<int32_t>(offsetof(ButtonAlterarSenha_t3991155776, ___rectCloseY_6)); }
	inline int32_t get_rectCloseY_6() const { return ___rectCloseY_6; }
	inline int32_t* get_address_of_rectCloseY_6() { return &___rectCloseY_6; }
	inline void set_rectCloseY_6(int32_t value)
	{
		___rectCloseY_6 = value;
	}

	inline static int32_t get_offset_of_rectOpenY_7() { return static_cast<int32_t>(offsetof(ButtonAlterarSenha_t3991155776, ___rectOpenY_7)); }
	inline int32_t get_rectOpenY_7() const { return ___rectOpenY_7; }
	inline int32_t* get_address_of_rectOpenY_7() { return &___rectOpenY_7; }
	inline void set_rectOpenY_7(int32_t value)
	{
		___rectOpenY_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
