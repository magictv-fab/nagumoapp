﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Extensions.HorizontalScrollXSnap
struct HorizontalScrollXSnap_t885073351;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1848751023;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1848751023.h"

// System.Void UnityEngine.UI.Extensions.HorizontalScrollXSnap::.ctor()
extern "C"  void HorizontalScrollXSnap__ctor_m1986490513 (HorizontalScrollXSnap_t885073351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.HorizontalScrollXSnap::Start()
extern "C"  void HorizontalScrollXSnap_Start_m933628305 (HorizontalScrollXSnap_t885073351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.HorizontalScrollXSnap::Update()
extern "C"  void HorizontalScrollXSnap_Update_m3178525852 (HorizontalScrollXSnap_t885073351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.HorizontalScrollXSnap::NextScreen()
extern "C"  void HorizontalScrollXSnap_NextScreen_m2109936274 (HorizontalScrollXSnap_t885073351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.HorizontalScrollXSnap::PreviousScreen()
extern "C"  void HorizontalScrollXSnap_PreviousScreen_m3781993878 (HorizontalScrollXSnap_t885073351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.HorizontalScrollXSnap::NextScreenCommand()
extern "C"  void HorizontalScrollXSnap_NextScreenCommand_m1959533243 (HorizontalScrollXSnap_t885073351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.HorizontalScrollXSnap::PrevScreenCommand()
extern "C"  void HorizontalScrollXSnap_PrevScreenCommand_m3524369275 (HorizontalScrollXSnap_t885073351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.UI.Extensions.HorizontalScrollXSnap::FindClosestFrom(UnityEngine.Vector3,System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C"  Vector3_t4282066566  HorizontalScrollXSnap_FindClosestFrom_m3202820061 (HorizontalScrollXSnap_t885073351 * __this, Vector3_t4282066566  ___start0, List_1_t1355284822 * ___positions1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.UI.Extensions.HorizontalScrollXSnap::CurrentScreen()
extern "C"  int32_t HorizontalScrollXSnap_CurrentScreen_m2476926914 (HorizontalScrollXSnap_t885073351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.HorizontalScrollXSnap::ChangeBulletsInfo(System.Int32)
extern "C"  void HorizontalScrollXSnap_ChangeBulletsInfo_m3128927087 (HorizontalScrollXSnap_t885073351 * __this, int32_t ___currentScreen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.HorizontalScrollXSnap::DistributePages()
extern "C"  void HorizontalScrollXSnap_DistributePages_m2879825362 (HorizontalScrollXSnap_t885073351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.HorizontalScrollXSnap::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void HorizontalScrollXSnap_OnBeginDrag_m361591313 (HorizontalScrollXSnap_t885073351 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.HorizontalScrollXSnap::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void HorizontalScrollXSnap_OnEndDrag_m3073527775 (HorizontalScrollXSnap_t885073351 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.HorizontalScrollXSnap::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void HorizontalScrollXSnap_OnDrag_m2618385208 (HorizontalScrollXSnap_t885073351 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.HorizontalScrollXSnap::<Start>m__5()
extern "C"  void HorizontalScrollXSnap_U3CStartU3Em__5_m2661938063 (HorizontalScrollXSnap_t885073351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.HorizontalScrollXSnap::<Start>m__6()
extern "C"  void HorizontalScrollXSnap_U3CStartU3Em__6_m2661939024 (HorizontalScrollXSnap_t885073351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
