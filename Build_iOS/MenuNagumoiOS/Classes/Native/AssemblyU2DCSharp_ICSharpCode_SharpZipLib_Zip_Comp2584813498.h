﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.Compression.DeflaterConstants
struct  DeflaterConstants_t2584813498  : public Il2CppObject
{
public:

public:
};

struct DeflaterConstants_t2584813498_StaticFields
{
public:
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterConstants::MAX_BLOCK_SIZE
	int32_t ___MAX_BLOCK_SIZE_21;
	// System.Int32[] ICSharpCode.SharpZipLib.Zip.Compression.DeflaterConstants::GOOD_LENGTH
	Int32U5BU5D_t3230847821* ___GOOD_LENGTH_22;
	// System.Int32[] ICSharpCode.SharpZipLib.Zip.Compression.DeflaterConstants::MAX_LAZY
	Int32U5BU5D_t3230847821* ___MAX_LAZY_23;
	// System.Int32[] ICSharpCode.SharpZipLib.Zip.Compression.DeflaterConstants::NICE_LENGTH
	Int32U5BU5D_t3230847821* ___NICE_LENGTH_24;
	// System.Int32[] ICSharpCode.SharpZipLib.Zip.Compression.DeflaterConstants::MAX_CHAIN
	Int32U5BU5D_t3230847821* ___MAX_CHAIN_25;
	// System.Int32[] ICSharpCode.SharpZipLib.Zip.Compression.DeflaterConstants::COMPR_FUNC
	Int32U5BU5D_t3230847821* ___COMPR_FUNC_26;

public:
	inline static int32_t get_offset_of_MAX_BLOCK_SIZE_21() { return static_cast<int32_t>(offsetof(DeflaterConstants_t2584813498_StaticFields, ___MAX_BLOCK_SIZE_21)); }
	inline int32_t get_MAX_BLOCK_SIZE_21() const { return ___MAX_BLOCK_SIZE_21; }
	inline int32_t* get_address_of_MAX_BLOCK_SIZE_21() { return &___MAX_BLOCK_SIZE_21; }
	inline void set_MAX_BLOCK_SIZE_21(int32_t value)
	{
		___MAX_BLOCK_SIZE_21 = value;
	}

	inline static int32_t get_offset_of_GOOD_LENGTH_22() { return static_cast<int32_t>(offsetof(DeflaterConstants_t2584813498_StaticFields, ___GOOD_LENGTH_22)); }
	inline Int32U5BU5D_t3230847821* get_GOOD_LENGTH_22() const { return ___GOOD_LENGTH_22; }
	inline Int32U5BU5D_t3230847821** get_address_of_GOOD_LENGTH_22() { return &___GOOD_LENGTH_22; }
	inline void set_GOOD_LENGTH_22(Int32U5BU5D_t3230847821* value)
	{
		___GOOD_LENGTH_22 = value;
		Il2CppCodeGenWriteBarrier(&___GOOD_LENGTH_22, value);
	}

	inline static int32_t get_offset_of_MAX_LAZY_23() { return static_cast<int32_t>(offsetof(DeflaterConstants_t2584813498_StaticFields, ___MAX_LAZY_23)); }
	inline Int32U5BU5D_t3230847821* get_MAX_LAZY_23() const { return ___MAX_LAZY_23; }
	inline Int32U5BU5D_t3230847821** get_address_of_MAX_LAZY_23() { return &___MAX_LAZY_23; }
	inline void set_MAX_LAZY_23(Int32U5BU5D_t3230847821* value)
	{
		___MAX_LAZY_23 = value;
		Il2CppCodeGenWriteBarrier(&___MAX_LAZY_23, value);
	}

	inline static int32_t get_offset_of_NICE_LENGTH_24() { return static_cast<int32_t>(offsetof(DeflaterConstants_t2584813498_StaticFields, ___NICE_LENGTH_24)); }
	inline Int32U5BU5D_t3230847821* get_NICE_LENGTH_24() const { return ___NICE_LENGTH_24; }
	inline Int32U5BU5D_t3230847821** get_address_of_NICE_LENGTH_24() { return &___NICE_LENGTH_24; }
	inline void set_NICE_LENGTH_24(Int32U5BU5D_t3230847821* value)
	{
		___NICE_LENGTH_24 = value;
		Il2CppCodeGenWriteBarrier(&___NICE_LENGTH_24, value);
	}

	inline static int32_t get_offset_of_MAX_CHAIN_25() { return static_cast<int32_t>(offsetof(DeflaterConstants_t2584813498_StaticFields, ___MAX_CHAIN_25)); }
	inline Int32U5BU5D_t3230847821* get_MAX_CHAIN_25() const { return ___MAX_CHAIN_25; }
	inline Int32U5BU5D_t3230847821** get_address_of_MAX_CHAIN_25() { return &___MAX_CHAIN_25; }
	inline void set_MAX_CHAIN_25(Int32U5BU5D_t3230847821* value)
	{
		___MAX_CHAIN_25 = value;
		Il2CppCodeGenWriteBarrier(&___MAX_CHAIN_25, value);
	}

	inline static int32_t get_offset_of_COMPR_FUNC_26() { return static_cast<int32_t>(offsetof(DeflaterConstants_t2584813498_StaticFields, ___COMPR_FUNC_26)); }
	inline Int32U5BU5D_t3230847821* get_COMPR_FUNC_26() const { return ___COMPR_FUNC_26; }
	inline Int32U5BU5D_t3230847821** get_address_of_COMPR_FUNC_26() { return &___COMPR_FUNC_26; }
	inline void set_COMPR_FUNC_26(Int32U5BU5D_t3230847821* value)
	{
		___COMPR_FUNC_26 = value;
		Il2CppCodeGenWriteBarrier(&___COMPR_FUNC_26, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
