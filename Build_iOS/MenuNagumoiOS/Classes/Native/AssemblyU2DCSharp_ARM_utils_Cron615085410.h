﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// ARM.utils.CronWatcher
struct CronWatcher_t1902937828;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.utils.Cron
struct  Cron_t615085410  : public Il2CppObject
{
public:

public:
};

struct Cron_t615085410_StaticFields
{
public:
	// ARM.utils.CronWatcher ARM.utils.Cron::_CronWatcherReference
	CronWatcher_t1902937828 * ____CronWatcherReference_1;

public:
	inline static int32_t get_offset_of__CronWatcherReference_1() { return static_cast<int32_t>(offsetof(Cron_t615085410_StaticFields, ____CronWatcherReference_1)); }
	inline CronWatcher_t1902937828 * get__CronWatcherReference_1() const { return ____CronWatcherReference_1; }
	inline CronWatcher_t1902937828 ** get_address_of__CronWatcherReference_1() { return &____CronWatcherReference_1; }
	inline void set__CronWatcherReference_1(CronWatcher_t1902937828 * value)
	{
		____CronWatcherReference_1 = value;
		Il2CppCodeGenWriteBarrier(&____CronWatcherReference_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
