﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "AssemblyU2DCSharp_ARM_transform_filters_abstracts_4158746314.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoveRotateFilter
struct  MoveRotateFilter_t1484818244  : public TransformPositionAngleFilterAbstract_t4158746314
{
public:
	// UnityEngine.GameObject MoveRotateFilter::gameObjectToMove
	GameObject_t3674682005 * ___gameObjectToMove_15;
	// System.Boolean MoveRotateFilter::readGlobalPosition
	bool ___readGlobalPosition_16;
	// System.Boolean MoveRotateFilter::writeGlobalPosition
	bool ___writeGlobalPosition_17;
	// System.Boolean MoveRotateFilter::movePosition
	bool ___movePosition_18;
	// System.Boolean MoveRotateFilter::moveAngle
	bool ___moveAngle_19;
	// System.Boolean MoveRotateFilter::_changeOnLateUpdate
	bool ____changeOnLateUpdate_20;
	// UnityEngine.Vector3 MoveRotateFilter::forceToMovePosition
	Vector3_t4282066566  ___forceToMovePosition_21;
	// UnityEngine.Vector3 MoveRotateFilter::forceToRotate
	Vector3_t4282066566  ___forceToRotate_22;
	// System.Boolean MoveRotateFilter::TransformThisObjectToo
	bool ___TransformThisObjectToo_23;
	// UnityEngine.Quaternion MoveRotateFilter::rotationToSend
	Quaternion_t1553702882  ___rotationToSend_24;
	// UnityEngine.Vector3 MoveRotateFilter::positionToSend
	Vector3_t4282066566  ___positionToSend_25;
	// UnityEngine.Vector3 MoveRotateFilter::debugCurrent
	Vector3_t4282066566  ___debugCurrent_26;
	// UnityEngine.Vector3 MoveRotateFilter::debugNext
	Vector3_t4282066566  ___debugNext_27;

public:
	inline static int32_t get_offset_of_gameObjectToMove_15() { return static_cast<int32_t>(offsetof(MoveRotateFilter_t1484818244, ___gameObjectToMove_15)); }
	inline GameObject_t3674682005 * get_gameObjectToMove_15() const { return ___gameObjectToMove_15; }
	inline GameObject_t3674682005 ** get_address_of_gameObjectToMove_15() { return &___gameObjectToMove_15; }
	inline void set_gameObjectToMove_15(GameObject_t3674682005 * value)
	{
		___gameObjectToMove_15 = value;
		Il2CppCodeGenWriteBarrier(&___gameObjectToMove_15, value);
	}

	inline static int32_t get_offset_of_readGlobalPosition_16() { return static_cast<int32_t>(offsetof(MoveRotateFilter_t1484818244, ___readGlobalPosition_16)); }
	inline bool get_readGlobalPosition_16() const { return ___readGlobalPosition_16; }
	inline bool* get_address_of_readGlobalPosition_16() { return &___readGlobalPosition_16; }
	inline void set_readGlobalPosition_16(bool value)
	{
		___readGlobalPosition_16 = value;
	}

	inline static int32_t get_offset_of_writeGlobalPosition_17() { return static_cast<int32_t>(offsetof(MoveRotateFilter_t1484818244, ___writeGlobalPosition_17)); }
	inline bool get_writeGlobalPosition_17() const { return ___writeGlobalPosition_17; }
	inline bool* get_address_of_writeGlobalPosition_17() { return &___writeGlobalPosition_17; }
	inline void set_writeGlobalPosition_17(bool value)
	{
		___writeGlobalPosition_17 = value;
	}

	inline static int32_t get_offset_of_movePosition_18() { return static_cast<int32_t>(offsetof(MoveRotateFilter_t1484818244, ___movePosition_18)); }
	inline bool get_movePosition_18() const { return ___movePosition_18; }
	inline bool* get_address_of_movePosition_18() { return &___movePosition_18; }
	inline void set_movePosition_18(bool value)
	{
		___movePosition_18 = value;
	}

	inline static int32_t get_offset_of_moveAngle_19() { return static_cast<int32_t>(offsetof(MoveRotateFilter_t1484818244, ___moveAngle_19)); }
	inline bool get_moveAngle_19() const { return ___moveAngle_19; }
	inline bool* get_address_of_moveAngle_19() { return &___moveAngle_19; }
	inline void set_moveAngle_19(bool value)
	{
		___moveAngle_19 = value;
	}

	inline static int32_t get_offset_of__changeOnLateUpdate_20() { return static_cast<int32_t>(offsetof(MoveRotateFilter_t1484818244, ____changeOnLateUpdate_20)); }
	inline bool get__changeOnLateUpdate_20() const { return ____changeOnLateUpdate_20; }
	inline bool* get_address_of__changeOnLateUpdate_20() { return &____changeOnLateUpdate_20; }
	inline void set__changeOnLateUpdate_20(bool value)
	{
		____changeOnLateUpdate_20 = value;
	}

	inline static int32_t get_offset_of_forceToMovePosition_21() { return static_cast<int32_t>(offsetof(MoveRotateFilter_t1484818244, ___forceToMovePosition_21)); }
	inline Vector3_t4282066566  get_forceToMovePosition_21() const { return ___forceToMovePosition_21; }
	inline Vector3_t4282066566 * get_address_of_forceToMovePosition_21() { return &___forceToMovePosition_21; }
	inline void set_forceToMovePosition_21(Vector3_t4282066566  value)
	{
		___forceToMovePosition_21 = value;
	}

	inline static int32_t get_offset_of_forceToRotate_22() { return static_cast<int32_t>(offsetof(MoveRotateFilter_t1484818244, ___forceToRotate_22)); }
	inline Vector3_t4282066566  get_forceToRotate_22() const { return ___forceToRotate_22; }
	inline Vector3_t4282066566 * get_address_of_forceToRotate_22() { return &___forceToRotate_22; }
	inline void set_forceToRotate_22(Vector3_t4282066566  value)
	{
		___forceToRotate_22 = value;
	}

	inline static int32_t get_offset_of_TransformThisObjectToo_23() { return static_cast<int32_t>(offsetof(MoveRotateFilter_t1484818244, ___TransformThisObjectToo_23)); }
	inline bool get_TransformThisObjectToo_23() const { return ___TransformThisObjectToo_23; }
	inline bool* get_address_of_TransformThisObjectToo_23() { return &___TransformThisObjectToo_23; }
	inline void set_TransformThisObjectToo_23(bool value)
	{
		___TransformThisObjectToo_23 = value;
	}

	inline static int32_t get_offset_of_rotationToSend_24() { return static_cast<int32_t>(offsetof(MoveRotateFilter_t1484818244, ___rotationToSend_24)); }
	inline Quaternion_t1553702882  get_rotationToSend_24() const { return ___rotationToSend_24; }
	inline Quaternion_t1553702882 * get_address_of_rotationToSend_24() { return &___rotationToSend_24; }
	inline void set_rotationToSend_24(Quaternion_t1553702882  value)
	{
		___rotationToSend_24 = value;
	}

	inline static int32_t get_offset_of_positionToSend_25() { return static_cast<int32_t>(offsetof(MoveRotateFilter_t1484818244, ___positionToSend_25)); }
	inline Vector3_t4282066566  get_positionToSend_25() const { return ___positionToSend_25; }
	inline Vector3_t4282066566 * get_address_of_positionToSend_25() { return &___positionToSend_25; }
	inline void set_positionToSend_25(Vector3_t4282066566  value)
	{
		___positionToSend_25 = value;
	}

	inline static int32_t get_offset_of_debugCurrent_26() { return static_cast<int32_t>(offsetof(MoveRotateFilter_t1484818244, ___debugCurrent_26)); }
	inline Vector3_t4282066566  get_debugCurrent_26() const { return ___debugCurrent_26; }
	inline Vector3_t4282066566 * get_address_of_debugCurrent_26() { return &___debugCurrent_26; }
	inline void set_debugCurrent_26(Vector3_t4282066566  value)
	{
		___debugCurrent_26 = value;
	}

	inline static int32_t get_offset_of_debugNext_27() { return static_cast<int32_t>(offsetof(MoveRotateFilter_t1484818244, ___debugNext_27)); }
	inline Vector3_t4282066566  get_debugNext_27() const { return ___debugNext_27; }
	inline Vector3_t4282066566 * get_address_of_debugNext_27() { return &___debugNext_27; }
	inline void set_debugNext_27(Vector3_t4282066566  value)
	{
		___debugNext_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
