﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SampleAppUIRect
struct  SampleAppUIRect_t2784570703  : public Il2CppObject
{
public:
	// System.Single SampleAppUIRect::mX
	float ___mX_0;
	// System.Single SampleAppUIRect::mY
	float ___mY_1;
	// System.Single SampleAppUIRect::mWidth
	float ___mWidth_2;
	// System.Single SampleAppUIRect::mHeight
	float ___mHeight_3;

public:
	inline static int32_t get_offset_of_mX_0() { return static_cast<int32_t>(offsetof(SampleAppUIRect_t2784570703, ___mX_0)); }
	inline float get_mX_0() const { return ___mX_0; }
	inline float* get_address_of_mX_0() { return &___mX_0; }
	inline void set_mX_0(float value)
	{
		___mX_0 = value;
	}

	inline static int32_t get_offset_of_mY_1() { return static_cast<int32_t>(offsetof(SampleAppUIRect_t2784570703, ___mY_1)); }
	inline float get_mY_1() const { return ___mY_1; }
	inline float* get_address_of_mY_1() { return &___mY_1; }
	inline void set_mY_1(float value)
	{
		___mY_1 = value;
	}

	inline static int32_t get_offset_of_mWidth_2() { return static_cast<int32_t>(offsetof(SampleAppUIRect_t2784570703, ___mWidth_2)); }
	inline float get_mWidth_2() const { return ___mWidth_2; }
	inline float* get_address_of_mWidth_2() { return &___mWidth_2; }
	inline void set_mWidth_2(float value)
	{
		___mWidth_2 = value;
	}

	inline static int32_t get_offset_of_mHeight_3() { return static_cast<int32_t>(offsetof(SampleAppUIRect_t2784570703, ___mHeight_3)); }
	inline float get_mHeight_3() const { return ___mHeight_3; }
	inline float* get_address_of_mHeight_3() { return &___mHeight_3; }
	inline void set_mHeight_3(float value)
	{
		___mHeight_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
