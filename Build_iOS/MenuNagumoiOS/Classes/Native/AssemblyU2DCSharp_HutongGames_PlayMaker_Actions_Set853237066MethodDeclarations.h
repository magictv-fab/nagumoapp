﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetEventData
struct SetEventData_t853237066;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetEventData::.ctor()
extern "C"  void SetEventData__ctor_m1112974428 (SetEventData_t853237066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetEventData::Reset()
extern "C"  void SetEventData_Reset_m3054374665 (SetEventData_t853237066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetEventData::OnEnter()
extern "C"  void SetEventData_OnEnter_m3808264499 (SetEventData_t853237066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
