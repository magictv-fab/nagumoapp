﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Extensions.TileSizeFitter
struct TileSizeFitter_t255948707;
// UnityEngine.RectTransform
struct RectTransform_t972643934;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void UnityEngine.UI.Extensions.TileSizeFitter::.ctor()
extern "C"  void TileSizeFitter__ctor_m754953573 (TileSizeFitter_t255948707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.UI.Extensions.TileSizeFitter::get_Border()
extern "C"  Vector2_t4282066565  TileSizeFitter_get_Border_m1793673167 (TileSizeFitter_t255948707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.TileSizeFitter::set_Border(UnityEngine.Vector2)
extern "C"  void TileSizeFitter_set_Border_m937036178 (TileSizeFitter_t255948707 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.UI.Extensions.TileSizeFitter::get_TileSize()
extern "C"  Vector2_t4282066565  TileSizeFitter_get_TileSize_m2949985842 (TileSizeFitter_t255948707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.TileSizeFitter::set_TileSize(UnityEngine.Vector2)
extern "C"  void TileSizeFitter_set_TileSize_m3285113999 (TileSizeFitter_t255948707 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform UnityEngine.UI.Extensions.TileSizeFitter::get_rectTransform()
extern "C"  RectTransform_t972643934 * TileSizeFitter_get_rectTransform_m4179950464 (TileSizeFitter_t255948707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.TileSizeFitter::OnEnable()
extern "C"  void TileSizeFitter_OnEnable_m149742593 (TileSizeFitter_t255948707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.TileSizeFitter::OnDisable()
extern "C"  void TileSizeFitter_OnDisable_m787990348 (TileSizeFitter_t255948707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.TileSizeFitter::OnRectTransformDimensionsChange()
extern "C"  void TileSizeFitter_OnRectTransformDimensionsChange_m651285705 (TileSizeFitter_t255948707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.TileSizeFitter::UpdateRect()
extern "C"  void TileSizeFitter_UpdateRect_m248067020 (TileSizeFitter_t255948707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.UI.Extensions.TileSizeFitter::GetParentSize()
extern "C"  Vector2_t4282066565  TileSizeFitter_GetParentSize_m108467977 (TileSizeFitter_t255948707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.TileSizeFitter::SetLayoutHorizontal()
extern "C"  void TileSizeFitter_SetLayoutHorizontal_m3883146867 (TileSizeFitter_t255948707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.TileSizeFitter::SetLayoutVertical()
extern "C"  void TileSizeFitter_SetLayoutVertical_m3988433093 (TileSizeFitter_t255948707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.TileSizeFitter::SetDirty()
extern "C"  void TileSizeFitter_SetDirty_m382234831 (TileSizeFitter_t255948707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
