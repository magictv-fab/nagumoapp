﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.transform.filters.HistorySituationCamera
struct HistorySituationCamera_t3661546483;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

// System.Void ARM.transform.filters.HistorySituationCamera::.ctor()
extern "C"  void HistorySituationCamera__ctor_m4157213221 (HistorySituationCamera_t3661546483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.HistorySituationCamera::Start()
extern "C"  void HistorySituationCamera_Start_m3104351013 (HistorySituationCamera_t3661546483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.HistorySituationCamera::Update()
extern "C"  void HistorySituationCamera_Update_m1751453064 (HistorySituationCamera_t3661546483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.HistorySituationCamera::_Off()
extern "C"  void HistorySituationCamera__Off_m2626412271 (HistorySituationCamera_t3661546483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.HistorySituationCamera::resetFilters()
extern "C"  void HistorySituationCamera_resetFilters_m3242999051 (HistorySituationCamera_t3661546483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.transform.filters.HistorySituationCamera::_doFilter(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  HistorySituationCamera__doFilter_m1324035489 (HistorySituationCamera_t3661546483 * __this, Vector3_t4282066566  ___last0, Vector3_t4282066566  ___nextValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion ARM.transform.filters.HistorySituationCamera::_doFilter(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  HistorySituationCamera__doFilter_m1993956777 (HistorySituationCamera_t3661546483 * __this, Quaternion_t1553702882  ___last0, Quaternion_t1553702882  ___nextValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
