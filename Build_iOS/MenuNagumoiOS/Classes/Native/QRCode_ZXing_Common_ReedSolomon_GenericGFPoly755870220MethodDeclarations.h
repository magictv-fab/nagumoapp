﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Common.ReedSolomon.GenericGFPoly
struct GenericGFPoly_t755870220;
// ZXing.Common.ReedSolomon.GenericGF
struct GenericGF_t2563420960;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// ZXing.Common.ReedSolomon.GenericGFPoly[]
struct GenericGFPolyU5BU5D_t2416185413;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_ReedSolomon_GenericGF2563420960.h"
#include "QRCode_ZXing_Common_ReedSolomon_GenericGFPoly755870220.h"

// System.Void ZXing.Common.ReedSolomon.GenericGFPoly::.ctor(ZXing.Common.ReedSolomon.GenericGF,System.Int32[])
extern "C"  void GenericGFPoly__ctor_m2846127002 (GenericGFPoly_t755870220 * __this, GenericGF_t2563420960 * ___field0, Int32U5BU5D_t3230847821* ___coefficients1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.Common.ReedSolomon.GenericGFPoly::get_Coefficients()
extern "C"  Int32U5BU5D_t3230847821* GenericGFPoly_get_Coefficients_m896110258 (GenericGFPoly_t755870220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Common.ReedSolomon.GenericGFPoly::get_Degree()
extern "C"  int32_t GenericGFPoly_get_Degree_m4145294850 (GenericGFPoly_t755870220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Common.ReedSolomon.GenericGFPoly::get_isZero()
extern "C"  bool GenericGFPoly_get_isZero_m1291059758 (GenericGFPoly_t755870220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Common.ReedSolomon.GenericGFPoly::getCoefficient(System.Int32)
extern "C"  int32_t GenericGFPoly_getCoefficient_m1139606749 (GenericGFPoly_t755870220 * __this, int32_t ___degree0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Common.ReedSolomon.GenericGFPoly::evaluateAt(System.Int32)
extern "C"  int32_t GenericGFPoly_evaluateAt_m2724420298 (GenericGFPoly_t755870220 * __this, int32_t ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.ReedSolomon.GenericGFPoly ZXing.Common.ReedSolomon.GenericGFPoly::addOrSubtract(ZXing.Common.ReedSolomon.GenericGFPoly)
extern "C"  GenericGFPoly_t755870220 * GenericGFPoly_addOrSubtract_m2229827198 (GenericGFPoly_t755870220 * __this, GenericGFPoly_t755870220 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.ReedSolomon.GenericGFPoly ZXing.Common.ReedSolomon.GenericGFPoly::multiply(ZXing.Common.ReedSolomon.GenericGFPoly)
extern "C"  GenericGFPoly_t755870220 * GenericGFPoly_multiply_m2769137140 (GenericGFPoly_t755870220 * __this, GenericGFPoly_t755870220 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.ReedSolomon.GenericGFPoly ZXing.Common.ReedSolomon.GenericGFPoly::multiply(System.Int32)
extern "C"  GenericGFPoly_t755870220 * GenericGFPoly_multiply_m1600218427 (GenericGFPoly_t755870220 * __this, int32_t ___scalar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.ReedSolomon.GenericGFPoly ZXing.Common.ReedSolomon.GenericGFPoly::multiplyByMonomial(System.Int32,System.Int32)
extern "C"  GenericGFPoly_t755870220 * GenericGFPoly_multiplyByMonomial_m1620372571 (GenericGFPoly_t755870220 * __this, int32_t ___degree0, int32_t ___coefficient1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.ReedSolomon.GenericGFPoly[] ZXing.Common.ReedSolomon.GenericGFPoly::divide(ZXing.Common.ReedSolomon.GenericGFPoly)
extern "C"  GenericGFPolyU5BU5D_t2416185413* GenericGFPoly_divide_m4198391591 (GenericGFPoly_t755870220 * __this, GenericGFPoly_t755870220 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.Common.ReedSolomon.GenericGFPoly::ToString()
extern "C"  String_t* GenericGFPoly_ToString_m1791026542 (GenericGFPoly_t755870220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
