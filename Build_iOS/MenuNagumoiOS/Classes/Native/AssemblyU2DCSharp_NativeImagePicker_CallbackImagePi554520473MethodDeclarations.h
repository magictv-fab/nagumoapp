﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NativeImagePicker/CallbackImagePicked
struct CallbackImagePicked_t554520473;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void NativeImagePicker/CallbackImagePicked::.ctor(System.Object,System.IntPtr)
extern "C"  void CallbackImagePicked__ctor_m311729904 (CallbackImagePicked_t554520473 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeImagePicker/CallbackImagePicked::Invoke(System.String)
extern "C"  void CallbackImagePicked_Invoke_m1729966072 (CallbackImagePicked_t554520473 * __this, String_t* ___filePath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult NativeImagePicker/CallbackImagePicked::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CallbackImagePicked_BeginInvoke_m4259295749 (CallbackImagePicked_t554520473 * __this, String_t* ___filePath0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeImagePicker/CallbackImagePicked::EndInvoke(System.IAsyncResult)
extern "C"  void CallbackImagePicked_EndInvoke_m1617758464 (CallbackImagePicked_t554520473 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
