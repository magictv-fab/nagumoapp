﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SmoothFollowAction
struct  SmoothFollowAction_t1760465533  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SmoothFollowAction::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.SmoothFollowAction::targetObject
	FsmGameObject_t1697147867 * ___targetObject_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SmoothFollowAction::distance
	FsmFloat_t2134102846 * ___distance_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SmoothFollowAction::height
	FsmFloat_t2134102846 * ___height_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SmoothFollowAction::heightDamping
	FsmFloat_t2134102846 * ___heightDamping_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SmoothFollowAction::rotationDamping
	FsmFloat_t2134102846 * ___rotationDamping_14;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.SmoothFollowAction::cachedObect
	GameObject_t3674682005 * ___cachedObect_15;
	// UnityEngine.Transform HutongGames.PlayMaker.Actions.SmoothFollowAction::myTransform
	Transform_t1659122786 * ___myTransform_16;
	// UnityEngine.Transform HutongGames.PlayMaker.Actions.SmoothFollowAction::targetTransform
	Transform_t1659122786 * ___targetTransform_17;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(SmoothFollowAction_t1760465533, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_targetObject_10() { return static_cast<int32_t>(offsetof(SmoothFollowAction_t1760465533, ___targetObject_10)); }
	inline FsmGameObject_t1697147867 * get_targetObject_10() const { return ___targetObject_10; }
	inline FsmGameObject_t1697147867 ** get_address_of_targetObject_10() { return &___targetObject_10; }
	inline void set_targetObject_10(FsmGameObject_t1697147867 * value)
	{
		___targetObject_10 = value;
		Il2CppCodeGenWriteBarrier(&___targetObject_10, value);
	}

	inline static int32_t get_offset_of_distance_11() { return static_cast<int32_t>(offsetof(SmoothFollowAction_t1760465533, ___distance_11)); }
	inline FsmFloat_t2134102846 * get_distance_11() const { return ___distance_11; }
	inline FsmFloat_t2134102846 ** get_address_of_distance_11() { return &___distance_11; }
	inline void set_distance_11(FsmFloat_t2134102846 * value)
	{
		___distance_11 = value;
		Il2CppCodeGenWriteBarrier(&___distance_11, value);
	}

	inline static int32_t get_offset_of_height_12() { return static_cast<int32_t>(offsetof(SmoothFollowAction_t1760465533, ___height_12)); }
	inline FsmFloat_t2134102846 * get_height_12() const { return ___height_12; }
	inline FsmFloat_t2134102846 ** get_address_of_height_12() { return &___height_12; }
	inline void set_height_12(FsmFloat_t2134102846 * value)
	{
		___height_12 = value;
		Il2CppCodeGenWriteBarrier(&___height_12, value);
	}

	inline static int32_t get_offset_of_heightDamping_13() { return static_cast<int32_t>(offsetof(SmoothFollowAction_t1760465533, ___heightDamping_13)); }
	inline FsmFloat_t2134102846 * get_heightDamping_13() const { return ___heightDamping_13; }
	inline FsmFloat_t2134102846 ** get_address_of_heightDamping_13() { return &___heightDamping_13; }
	inline void set_heightDamping_13(FsmFloat_t2134102846 * value)
	{
		___heightDamping_13 = value;
		Il2CppCodeGenWriteBarrier(&___heightDamping_13, value);
	}

	inline static int32_t get_offset_of_rotationDamping_14() { return static_cast<int32_t>(offsetof(SmoothFollowAction_t1760465533, ___rotationDamping_14)); }
	inline FsmFloat_t2134102846 * get_rotationDamping_14() const { return ___rotationDamping_14; }
	inline FsmFloat_t2134102846 ** get_address_of_rotationDamping_14() { return &___rotationDamping_14; }
	inline void set_rotationDamping_14(FsmFloat_t2134102846 * value)
	{
		___rotationDamping_14 = value;
		Il2CppCodeGenWriteBarrier(&___rotationDamping_14, value);
	}

	inline static int32_t get_offset_of_cachedObect_15() { return static_cast<int32_t>(offsetof(SmoothFollowAction_t1760465533, ___cachedObect_15)); }
	inline GameObject_t3674682005 * get_cachedObect_15() const { return ___cachedObect_15; }
	inline GameObject_t3674682005 ** get_address_of_cachedObect_15() { return &___cachedObect_15; }
	inline void set_cachedObect_15(GameObject_t3674682005 * value)
	{
		___cachedObect_15 = value;
		Il2CppCodeGenWriteBarrier(&___cachedObect_15, value);
	}

	inline static int32_t get_offset_of_myTransform_16() { return static_cast<int32_t>(offsetof(SmoothFollowAction_t1760465533, ___myTransform_16)); }
	inline Transform_t1659122786 * get_myTransform_16() const { return ___myTransform_16; }
	inline Transform_t1659122786 ** get_address_of_myTransform_16() { return &___myTransform_16; }
	inline void set_myTransform_16(Transform_t1659122786 * value)
	{
		___myTransform_16 = value;
		Il2CppCodeGenWriteBarrier(&___myTransform_16, value);
	}

	inline static int32_t get_offset_of_targetTransform_17() { return static_cast<int32_t>(offsetof(SmoothFollowAction_t1760465533, ___targetTransform_17)); }
	inline Transform_t1659122786 * get_targetTransform_17() const { return ___targetTransform_17; }
	inline Transform_t1659122786 ** get_address_of_targetTransform_17() { return &___targetTransform_17; }
	inline void set_targetTransform_17(Transform_t1659122786 * value)
	{
		___targetTransform_17 = value;
		Il2CppCodeGenWriteBarrier(&___targetTransform_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
