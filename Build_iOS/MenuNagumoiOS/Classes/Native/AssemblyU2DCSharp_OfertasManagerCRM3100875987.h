﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OfertasCRMDataServer
struct OfertasCRMDataServer_t217840361;
// OfertasManagerCRM
struct OfertasManagerCRM_t3100875987;
// UnityEngine.UI.Text
struct Text_t9039225;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Collections.Generic.List`1<OfertasValuesCRM>
struct List_1_t4225135114;
// System.Collections.Generic.List`1<UnityEngine.Texture2D>
struct List_1_t957326451;
// System.Collections.Generic.List`1<UnityEngine.Sprite>
struct List_1_t272385497;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;
// CRM_ScrollViewsManager
struct CRM_ScrollViewsManager_t2923429133;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t3606982749;
// UnityEngine.UI.Extensions.HorizontalScrollSnap
struct HorizontalScrollSnap_t2651831999;
// Header_toggle_buttons
struct Header_toggle_buttons_t797597032;
// System.String
struct String_t;
// UnityEngine.RectTransform
struct RectTransform_t972643934;
// System.Globalization.CultureInfo
struct CultureInfo_t1065375142;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OfertasManagerCRM
struct  OfertasManagerCRM_t3100875987  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.Text OfertasManagerCRM::nomeDoCliente
	Text_t9039225 * ___nomeDoCliente_5;
	// System.Collections.Generic.List`1<OfertasValuesCRM> OfertasManagerCRM::ofertaValuesLst
	List_1_t4225135114 * ___ofertaValuesLst_22;
	// System.Collections.Generic.List`1<OfertasValuesCRM> OfertasManagerCRM::ofertaDEPORValuesLst
	List_1_t4225135114 * ___ofertaDEPORValuesLst_23;
	// UnityEngine.GameObject OfertasManagerCRM::loadingObj
	GameObject_t3674682005 * ___loadingObj_28;
	// UnityEngine.GameObject OfertasManagerCRM::popup
	GameObject_t3674682005 * ___popup_29;
	// UnityEngine.GameObject OfertasManagerCRM::itemPrefabPreco2
	GameObject_t3674682005 * ___itemPrefabPreco2_30;
	// UnityEngine.GameObject OfertasManagerCRM::containerItensPreco2
	GameObject_t3674682005 * ___containerItensPreco2_31;
	// UnityEngine.GameObject OfertasManagerCRM::itemPrefab_DE_POR
	GameObject_t3674682005 * ___itemPrefab_DE_POR_33;
	// UnityEngine.GameObject OfertasManagerCRM::containerItens_DE_POR
	GameObject_t3674682005 * ___containerItens_DE_POR_34;
	// CRM_ScrollViewsManager OfertasManagerCRM::_CRM_ScrollViewManager
	CRM_ScrollViewsManager_t2923429133 * ____CRM_ScrollViewManager_35;
	// UnityEngine.GameObject OfertasManagerCRM::favoritouPrimeiraVezObj
	GameObject_t3674682005 * ___favoritouPrimeiraVezObj_36;
	// UnityEngine.GameObject OfertasManagerCRM::popupSemConexao
	GameObject_t3674682005 * ___popupSemConexao_37;
	// UnityEngine.UI.ScrollRect OfertasManagerCRM::scrollRect
	ScrollRect_t3606982749 * ___scrollRect_38;
	// System.Int32 OfertasManagerCRM::ofertaPrecoCounter
	int32_t ___ofertaPrecoCounter_39;
	// System.Int32 OfertasManagerCRM::ofertaDePorCounter
	int32_t ___ofertaDePorCounter_40;
	// UnityEngine.GameObject OfertasManagerCRM::selectedItem
	GameObject_t3674682005 * ___selectedItem_42;
	// UnityEngine.UI.Extensions.HorizontalScrollSnap OfertasManagerCRM::horizontalScrollSnap
	HorizontalScrollSnap_t2651831999 * ___horizontalScrollSnap_43;
	// UnityEngine.UI.Extensions.HorizontalScrollSnap OfertasManagerCRM::horizontalScrollSnapHEADER
	HorizontalScrollSnap_t2651831999 * ___horizontalScrollSnapHEADER_44;
	// Header_toggle_buttons OfertasManagerCRM::togglesbuttonscript
	Header_toggle_buttons_t797597032 * ___togglesbuttonscript_45;
	// UnityEngine.RectTransform OfertasManagerCRM::rectContainer
	RectTransform_t972643934 * ___rectContainer_49;
	// System.Boolean OfertasManagerCRM::onRectUpdate
	bool ___onRectUpdate_50;
	// System.Globalization.CultureInfo OfertasManagerCRM::myCulture
	CultureInfo_t1065375142 * ___myCulture_52;

public:
	inline static int32_t get_offset_of_nomeDoCliente_5() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987, ___nomeDoCliente_5)); }
	inline Text_t9039225 * get_nomeDoCliente_5() const { return ___nomeDoCliente_5; }
	inline Text_t9039225 ** get_address_of_nomeDoCliente_5() { return &___nomeDoCliente_5; }
	inline void set_nomeDoCliente_5(Text_t9039225 * value)
	{
		___nomeDoCliente_5 = value;
		Il2CppCodeGenWriteBarrier(&___nomeDoCliente_5, value);
	}

	inline static int32_t get_offset_of_ofertaValuesLst_22() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987, ___ofertaValuesLst_22)); }
	inline List_1_t4225135114 * get_ofertaValuesLst_22() const { return ___ofertaValuesLst_22; }
	inline List_1_t4225135114 ** get_address_of_ofertaValuesLst_22() { return &___ofertaValuesLst_22; }
	inline void set_ofertaValuesLst_22(List_1_t4225135114 * value)
	{
		___ofertaValuesLst_22 = value;
		Il2CppCodeGenWriteBarrier(&___ofertaValuesLst_22, value);
	}

	inline static int32_t get_offset_of_ofertaDEPORValuesLst_23() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987, ___ofertaDEPORValuesLst_23)); }
	inline List_1_t4225135114 * get_ofertaDEPORValuesLst_23() const { return ___ofertaDEPORValuesLst_23; }
	inline List_1_t4225135114 ** get_address_of_ofertaDEPORValuesLst_23() { return &___ofertaDEPORValuesLst_23; }
	inline void set_ofertaDEPORValuesLst_23(List_1_t4225135114 * value)
	{
		___ofertaDEPORValuesLst_23 = value;
		Il2CppCodeGenWriteBarrier(&___ofertaDEPORValuesLst_23, value);
	}

	inline static int32_t get_offset_of_loadingObj_28() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987, ___loadingObj_28)); }
	inline GameObject_t3674682005 * get_loadingObj_28() const { return ___loadingObj_28; }
	inline GameObject_t3674682005 ** get_address_of_loadingObj_28() { return &___loadingObj_28; }
	inline void set_loadingObj_28(GameObject_t3674682005 * value)
	{
		___loadingObj_28 = value;
		Il2CppCodeGenWriteBarrier(&___loadingObj_28, value);
	}

	inline static int32_t get_offset_of_popup_29() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987, ___popup_29)); }
	inline GameObject_t3674682005 * get_popup_29() const { return ___popup_29; }
	inline GameObject_t3674682005 ** get_address_of_popup_29() { return &___popup_29; }
	inline void set_popup_29(GameObject_t3674682005 * value)
	{
		___popup_29 = value;
		Il2CppCodeGenWriteBarrier(&___popup_29, value);
	}

	inline static int32_t get_offset_of_itemPrefabPreco2_30() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987, ___itemPrefabPreco2_30)); }
	inline GameObject_t3674682005 * get_itemPrefabPreco2_30() const { return ___itemPrefabPreco2_30; }
	inline GameObject_t3674682005 ** get_address_of_itemPrefabPreco2_30() { return &___itemPrefabPreco2_30; }
	inline void set_itemPrefabPreco2_30(GameObject_t3674682005 * value)
	{
		___itemPrefabPreco2_30 = value;
		Il2CppCodeGenWriteBarrier(&___itemPrefabPreco2_30, value);
	}

	inline static int32_t get_offset_of_containerItensPreco2_31() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987, ___containerItensPreco2_31)); }
	inline GameObject_t3674682005 * get_containerItensPreco2_31() const { return ___containerItensPreco2_31; }
	inline GameObject_t3674682005 ** get_address_of_containerItensPreco2_31() { return &___containerItensPreco2_31; }
	inline void set_containerItensPreco2_31(GameObject_t3674682005 * value)
	{
		___containerItensPreco2_31 = value;
		Il2CppCodeGenWriteBarrier(&___containerItensPreco2_31, value);
	}

	inline static int32_t get_offset_of_itemPrefab_DE_POR_33() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987, ___itemPrefab_DE_POR_33)); }
	inline GameObject_t3674682005 * get_itemPrefab_DE_POR_33() const { return ___itemPrefab_DE_POR_33; }
	inline GameObject_t3674682005 ** get_address_of_itemPrefab_DE_POR_33() { return &___itemPrefab_DE_POR_33; }
	inline void set_itemPrefab_DE_POR_33(GameObject_t3674682005 * value)
	{
		___itemPrefab_DE_POR_33 = value;
		Il2CppCodeGenWriteBarrier(&___itemPrefab_DE_POR_33, value);
	}

	inline static int32_t get_offset_of_containerItens_DE_POR_34() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987, ___containerItens_DE_POR_34)); }
	inline GameObject_t3674682005 * get_containerItens_DE_POR_34() const { return ___containerItens_DE_POR_34; }
	inline GameObject_t3674682005 ** get_address_of_containerItens_DE_POR_34() { return &___containerItens_DE_POR_34; }
	inline void set_containerItens_DE_POR_34(GameObject_t3674682005 * value)
	{
		___containerItens_DE_POR_34 = value;
		Il2CppCodeGenWriteBarrier(&___containerItens_DE_POR_34, value);
	}

	inline static int32_t get_offset_of__CRM_ScrollViewManager_35() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987, ____CRM_ScrollViewManager_35)); }
	inline CRM_ScrollViewsManager_t2923429133 * get__CRM_ScrollViewManager_35() const { return ____CRM_ScrollViewManager_35; }
	inline CRM_ScrollViewsManager_t2923429133 ** get_address_of__CRM_ScrollViewManager_35() { return &____CRM_ScrollViewManager_35; }
	inline void set__CRM_ScrollViewManager_35(CRM_ScrollViewsManager_t2923429133 * value)
	{
		____CRM_ScrollViewManager_35 = value;
		Il2CppCodeGenWriteBarrier(&____CRM_ScrollViewManager_35, value);
	}

	inline static int32_t get_offset_of_favoritouPrimeiraVezObj_36() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987, ___favoritouPrimeiraVezObj_36)); }
	inline GameObject_t3674682005 * get_favoritouPrimeiraVezObj_36() const { return ___favoritouPrimeiraVezObj_36; }
	inline GameObject_t3674682005 ** get_address_of_favoritouPrimeiraVezObj_36() { return &___favoritouPrimeiraVezObj_36; }
	inline void set_favoritouPrimeiraVezObj_36(GameObject_t3674682005 * value)
	{
		___favoritouPrimeiraVezObj_36 = value;
		Il2CppCodeGenWriteBarrier(&___favoritouPrimeiraVezObj_36, value);
	}

	inline static int32_t get_offset_of_popupSemConexao_37() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987, ___popupSemConexao_37)); }
	inline GameObject_t3674682005 * get_popupSemConexao_37() const { return ___popupSemConexao_37; }
	inline GameObject_t3674682005 ** get_address_of_popupSemConexao_37() { return &___popupSemConexao_37; }
	inline void set_popupSemConexao_37(GameObject_t3674682005 * value)
	{
		___popupSemConexao_37 = value;
		Il2CppCodeGenWriteBarrier(&___popupSemConexao_37, value);
	}

	inline static int32_t get_offset_of_scrollRect_38() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987, ___scrollRect_38)); }
	inline ScrollRect_t3606982749 * get_scrollRect_38() const { return ___scrollRect_38; }
	inline ScrollRect_t3606982749 ** get_address_of_scrollRect_38() { return &___scrollRect_38; }
	inline void set_scrollRect_38(ScrollRect_t3606982749 * value)
	{
		___scrollRect_38 = value;
		Il2CppCodeGenWriteBarrier(&___scrollRect_38, value);
	}

	inline static int32_t get_offset_of_ofertaPrecoCounter_39() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987, ___ofertaPrecoCounter_39)); }
	inline int32_t get_ofertaPrecoCounter_39() const { return ___ofertaPrecoCounter_39; }
	inline int32_t* get_address_of_ofertaPrecoCounter_39() { return &___ofertaPrecoCounter_39; }
	inline void set_ofertaPrecoCounter_39(int32_t value)
	{
		___ofertaPrecoCounter_39 = value;
	}

	inline static int32_t get_offset_of_ofertaDePorCounter_40() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987, ___ofertaDePorCounter_40)); }
	inline int32_t get_ofertaDePorCounter_40() const { return ___ofertaDePorCounter_40; }
	inline int32_t* get_address_of_ofertaDePorCounter_40() { return &___ofertaDePorCounter_40; }
	inline void set_ofertaDePorCounter_40(int32_t value)
	{
		___ofertaDePorCounter_40 = value;
	}

	inline static int32_t get_offset_of_selectedItem_42() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987, ___selectedItem_42)); }
	inline GameObject_t3674682005 * get_selectedItem_42() const { return ___selectedItem_42; }
	inline GameObject_t3674682005 ** get_address_of_selectedItem_42() { return &___selectedItem_42; }
	inline void set_selectedItem_42(GameObject_t3674682005 * value)
	{
		___selectedItem_42 = value;
		Il2CppCodeGenWriteBarrier(&___selectedItem_42, value);
	}

	inline static int32_t get_offset_of_horizontalScrollSnap_43() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987, ___horizontalScrollSnap_43)); }
	inline HorizontalScrollSnap_t2651831999 * get_horizontalScrollSnap_43() const { return ___horizontalScrollSnap_43; }
	inline HorizontalScrollSnap_t2651831999 ** get_address_of_horizontalScrollSnap_43() { return &___horizontalScrollSnap_43; }
	inline void set_horizontalScrollSnap_43(HorizontalScrollSnap_t2651831999 * value)
	{
		___horizontalScrollSnap_43 = value;
		Il2CppCodeGenWriteBarrier(&___horizontalScrollSnap_43, value);
	}

	inline static int32_t get_offset_of_horizontalScrollSnapHEADER_44() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987, ___horizontalScrollSnapHEADER_44)); }
	inline HorizontalScrollSnap_t2651831999 * get_horizontalScrollSnapHEADER_44() const { return ___horizontalScrollSnapHEADER_44; }
	inline HorizontalScrollSnap_t2651831999 ** get_address_of_horizontalScrollSnapHEADER_44() { return &___horizontalScrollSnapHEADER_44; }
	inline void set_horizontalScrollSnapHEADER_44(HorizontalScrollSnap_t2651831999 * value)
	{
		___horizontalScrollSnapHEADER_44 = value;
		Il2CppCodeGenWriteBarrier(&___horizontalScrollSnapHEADER_44, value);
	}

	inline static int32_t get_offset_of_togglesbuttonscript_45() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987, ___togglesbuttonscript_45)); }
	inline Header_toggle_buttons_t797597032 * get_togglesbuttonscript_45() const { return ___togglesbuttonscript_45; }
	inline Header_toggle_buttons_t797597032 ** get_address_of_togglesbuttonscript_45() { return &___togglesbuttonscript_45; }
	inline void set_togglesbuttonscript_45(Header_toggle_buttons_t797597032 * value)
	{
		___togglesbuttonscript_45 = value;
		Il2CppCodeGenWriteBarrier(&___togglesbuttonscript_45, value);
	}

	inline static int32_t get_offset_of_rectContainer_49() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987, ___rectContainer_49)); }
	inline RectTransform_t972643934 * get_rectContainer_49() const { return ___rectContainer_49; }
	inline RectTransform_t972643934 ** get_address_of_rectContainer_49() { return &___rectContainer_49; }
	inline void set_rectContainer_49(RectTransform_t972643934 * value)
	{
		___rectContainer_49 = value;
		Il2CppCodeGenWriteBarrier(&___rectContainer_49, value);
	}

	inline static int32_t get_offset_of_onRectUpdate_50() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987, ___onRectUpdate_50)); }
	inline bool get_onRectUpdate_50() const { return ___onRectUpdate_50; }
	inline bool* get_address_of_onRectUpdate_50() { return &___onRectUpdate_50; }
	inline void set_onRectUpdate_50(bool value)
	{
		___onRectUpdate_50 = value;
	}

	inline static int32_t get_offset_of_myCulture_52() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987, ___myCulture_52)); }
	inline CultureInfo_t1065375142 * get_myCulture_52() const { return ___myCulture_52; }
	inline CultureInfo_t1065375142 ** get_address_of_myCulture_52() { return &___myCulture_52; }
	inline void set_myCulture_52(CultureInfo_t1065375142 * value)
	{
		___myCulture_52 = value;
		Il2CppCodeGenWriteBarrier(&___myCulture_52, value);
	}
};

struct OfertasManagerCRM_t3100875987_StaticFields
{
public:
	// OfertasCRMDataServer OfertasManagerCRM::ofertasDataServer
	OfertasCRMDataServer_t217840361 * ___ofertasDataServer_3;
	// OfertasManagerCRM OfertasManagerCRM::instance
	OfertasManagerCRM_t3100875987 * ___instance_4;
	// System.Collections.Generic.List`1<System.String> OfertasManagerCRM::titleLst
	List_1_t1375417109 * ___titleLst_6;
	// System.Collections.Generic.List`1<System.String> OfertasManagerCRM::valueLst
	List_1_t1375417109 * ___valueLst_7;
	// System.Collections.Generic.List`1<System.String> OfertasManagerCRM::infoLst
	List_1_t1375417109 * ___infoLst_8;
	// System.Collections.Generic.List`1<System.String> OfertasManagerCRM::idLst
	List_1_t1375417109 * ___idLst_9;
	// System.Collections.Generic.List`1<System.String> OfertasManagerCRM::idcrmLst
	List_1_t1375417109 * ___idcrmLst_10;
	// System.Collections.Generic.List`1<System.String> OfertasManagerCRM::validadeLst
	List_1_t1375417109 * ___validadeLst_11;
	// System.Collections.Generic.List`1<System.String> OfertasManagerCRM::linkLst
	List_1_t1375417109 * ___linkLst_12;
	// System.Collections.Generic.List`1<System.String> OfertasManagerCRM::titleDEPOR
	List_1_t1375417109 * ___titleDEPOR_13;
	// System.Collections.Generic.List`1<System.String> OfertasManagerCRM::valueDEPORLst
	List_1_t1375417109 * ___valueDEPORLst_14;
	// System.Collections.Generic.List`1<System.String> OfertasManagerCRM::valueDEPOR_DELst
	List_1_t1375417109 * ___valueDEPOR_DELst_15;
	// System.Collections.Generic.List`1<System.String> OfertasManagerCRM::valueDEPORDE_PORLst
	List_1_t1375417109 * ___valueDEPORDE_PORLst_16;
	// System.Collections.Generic.List`1<System.String> OfertasManagerCRM::infoDEPORLst
	List_1_t1375417109 * ___infoDEPORLst_17;
	// System.Collections.Generic.List`1<System.String> OfertasManagerCRM::idDEPORLst
	List_1_t1375417109 * ___idDEPORLst_18;
	// System.Collections.Generic.List`1<System.String> OfertasManagerCRM::idDEPORcrmLst
	List_1_t1375417109 * ___idDEPORcrmLst_19;
	// System.Collections.Generic.List`1<System.String> OfertasManagerCRM::validadeDEPORLst
	List_1_t1375417109 * ___validadeDEPORLst_20;
	// System.Collections.Generic.List`1<System.String> OfertasManagerCRM::linkDEPORLst
	List_1_t1375417109 * ___linkDEPORLst_21;
	// System.Collections.Generic.List`1<UnityEngine.Texture2D> OfertasManagerCRM::textureLst
	List_1_t957326451 * ___textureLst_24;
	// System.Collections.Generic.List`1<UnityEngine.Sprite> OfertasManagerCRM::imgLst
	List_1_t272385497 * ___imgLst_25;
	// System.Collections.Generic.List`1<UnityEngine.Texture2D> OfertasManagerCRM::textureDEPORLst
	List_1_t957326451 * ___textureDEPORLst_26;
	// System.Collections.Generic.List`1<UnityEngine.Sprite> OfertasManagerCRM::imgDEPORLst
	List_1_t272385497 * ___imgDEPORLst_27;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> OfertasManagerCRM::itensList
	List_1_t747900261 * ___itensList_32;
	// UnityEngine.GameObject OfertasManagerCRM::currentItem
	GameObject_t3674682005 * ___currentItem_41;
	// System.String OfertasManagerCRM::currentJsonTxt
	String_t* ___currentJsonTxt_46;
	// System.Int32 OfertasManagerCRM::loadedItensCount
	int32_t ___loadedItensCount_47;
	// System.Int32 OfertasManagerCRM::loadedItensDEPORCount
	int32_t ___loadedItensDEPORCount_48;
	// System.Int32 OfertasManagerCRM::loadedImgsCount
	int32_t ___loadedImgsCount_51;

public:
	inline static int32_t get_offset_of_ofertasDataServer_3() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987_StaticFields, ___ofertasDataServer_3)); }
	inline OfertasCRMDataServer_t217840361 * get_ofertasDataServer_3() const { return ___ofertasDataServer_3; }
	inline OfertasCRMDataServer_t217840361 ** get_address_of_ofertasDataServer_3() { return &___ofertasDataServer_3; }
	inline void set_ofertasDataServer_3(OfertasCRMDataServer_t217840361 * value)
	{
		___ofertasDataServer_3 = value;
		Il2CppCodeGenWriteBarrier(&___ofertasDataServer_3, value);
	}

	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987_StaticFields, ___instance_4)); }
	inline OfertasManagerCRM_t3100875987 * get_instance_4() const { return ___instance_4; }
	inline OfertasManagerCRM_t3100875987 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(OfertasManagerCRM_t3100875987 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier(&___instance_4, value);
	}

	inline static int32_t get_offset_of_titleLst_6() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987_StaticFields, ___titleLst_6)); }
	inline List_1_t1375417109 * get_titleLst_6() const { return ___titleLst_6; }
	inline List_1_t1375417109 ** get_address_of_titleLst_6() { return &___titleLst_6; }
	inline void set_titleLst_6(List_1_t1375417109 * value)
	{
		___titleLst_6 = value;
		Il2CppCodeGenWriteBarrier(&___titleLst_6, value);
	}

	inline static int32_t get_offset_of_valueLst_7() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987_StaticFields, ___valueLst_7)); }
	inline List_1_t1375417109 * get_valueLst_7() const { return ___valueLst_7; }
	inline List_1_t1375417109 ** get_address_of_valueLst_7() { return &___valueLst_7; }
	inline void set_valueLst_7(List_1_t1375417109 * value)
	{
		___valueLst_7 = value;
		Il2CppCodeGenWriteBarrier(&___valueLst_7, value);
	}

	inline static int32_t get_offset_of_infoLst_8() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987_StaticFields, ___infoLst_8)); }
	inline List_1_t1375417109 * get_infoLst_8() const { return ___infoLst_8; }
	inline List_1_t1375417109 ** get_address_of_infoLst_8() { return &___infoLst_8; }
	inline void set_infoLst_8(List_1_t1375417109 * value)
	{
		___infoLst_8 = value;
		Il2CppCodeGenWriteBarrier(&___infoLst_8, value);
	}

	inline static int32_t get_offset_of_idLst_9() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987_StaticFields, ___idLst_9)); }
	inline List_1_t1375417109 * get_idLst_9() const { return ___idLst_9; }
	inline List_1_t1375417109 ** get_address_of_idLst_9() { return &___idLst_9; }
	inline void set_idLst_9(List_1_t1375417109 * value)
	{
		___idLst_9 = value;
		Il2CppCodeGenWriteBarrier(&___idLst_9, value);
	}

	inline static int32_t get_offset_of_idcrmLst_10() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987_StaticFields, ___idcrmLst_10)); }
	inline List_1_t1375417109 * get_idcrmLst_10() const { return ___idcrmLst_10; }
	inline List_1_t1375417109 ** get_address_of_idcrmLst_10() { return &___idcrmLst_10; }
	inline void set_idcrmLst_10(List_1_t1375417109 * value)
	{
		___idcrmLst_10 = value;
		Il2CppCodeGenWriteBarrier(&___idcrmLst_10, value);
	}

	inline static int32_t get_offset_of_validadeLst_11() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987_StaticFields, ___validadeLst_11)); }
	inline List_1_t1375417109 * get_validadeLst_11() const { return ___validadeLst_11; }
	inline List_1_t1375417109 ** get_address_of_validadeLst_11() { return &___validadeLst_11; }
	inline void set_validadeLst_11(List_1_t1375417109 * value)
	{
		___validadeLst_11 = value;
		Il2CppCodeGenWriteBarrier(&___validadeLst_11, value);
	}

	inline static int32_t get_offset_of_linkLst_12() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987_StaticFields, ___linkLst_12)); }
	inline List_1_t1375417109 * get_linkLst_12() const { return ___linkLst_12; }
	inline List_1_t1375417109 ** get_address_of_linkLst_12() { return &___linkLst_12; }
	inline void set_linkLst_12(List_1_t1375417109 * value)
	{
		___linkLst_12 = value;
		Il2CppCodeGenWriteBarrier(&___linkLst_12, value);
	}

	inline static int32_t get_offset_of_titleDEPOR_13() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987_StaticFields, ___titleDEPOR_13)); }
	inline List_1_t1375417109 * get_titleDEPOR_13() const { return ___titleDEPOR_13; }
	inline List_1_t1375417109 ** get_address_of_titleDEPOR_13() { return &___titleDEPOR_13; }
	inline void set_titleDEPOR_13(List_1_t1375417109 * value)
	{
		___titleDEPOR_13 = value;
		Il2CppCodeGenWriteBarrier(&___titleDEPOR_13, value);
	}

	inline static int32_t get_offset_of_valueDEPORLst_14() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987_StaticFields, ___valueDEPORLst_14)); }
	inline List_1_t1375417109 * get_valueDEPORLst_14() const { return ___valueDEPORLst_14; }
	inline List_1_t1375417109 ** get_address_of_valueDEPORLst_14() { return &___valueDEPORLst_14; }
	inline void set_valueDEPORLst_14(List_1_t1375417109 * value)
	{
		___valueDEPORLst_14 = value;
		Il2CppCodeGenWriteBarrier(&___valueDEPORLst_14, value);
	}

	inline static int32_t get_offset_of_valueDEPOR_DELst_15() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987_StaticFields, ___valueDEPOR_DELst_15)); }
	inline List_1_t1375417109 * get_valueDEPOR_DELst_15() const { return ___valueDEPOR_DELst_15; }
	inline List_1_t1375417109 ** get_address_of_valueDEPOR_DELst_15() { return &___valueDEPOR_DELst_15; }
	inline void set_valueDEPOR_DELst_15(List_1_t1375417109 * value)
	{
		___valueDEPOR_DELst_15 = value;
		Il2CppCodeGenWriteBarrier(&___valueDEPOR_DELst_15, value);
	}

	inline static int32_t get_offset_of_valueDEPORDE_PORLst_16() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987_StaticFields, ___valueDEPORDE_PORLst_16)); }
	inline List_1_t1375417109 * get_valueDEPORDE_PORLst_16() const { return ___valueDEPORDE_PORLst_16; }
	inline List_1_t1375417109 ** get_address_of_valueDEPORDE_PORLst_16() { return &___valueDEPORDE_PORLst_16; }
	inline void set_valueDEPORDE_PORLst_16(List_1_t1375417109 * value)
	{
		___valueDEPORDE_PORLst_16 = value;
		Il2CppCodeGenWriteBarrier(&___valueDEPORDE_PORLst_16, value);
	}

	inline static int32_t get_offset_of_infoDEPORLst_17() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987_StaticFields, ___infoDEPORLst_17)); }
	inline List_1_t1375417109 * get_infoDEPORLst_17() const { return ___infoDEPORLst_17; }
	inline List_1_t1375417109 ** get_address_of_infoDEPORLst_17() { return &___infoDEPORLst_17; }
	inline void set_infoDEPORLst_17(List_1_t1375417109 * value)
	{
		___infoDEPORLst_17 = value;
		Il2CppCodeGenWriteBarrier(&___infoDEPORLst_17, value);
	}

	inline static int32_t get_offset_of_idDEPORLst_18() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987_StaticFields, ___idDEPORLst_18)); }
	inline List_1_t1375417109 * get_idDEPORLst_18() const { return ___idDEPORLst_18; }
	inline List_1_t1375417109 ** get_address_of_idDEPORLst_18() { return &___idDEPORLst_18; }
	inline void set_idDEPORLst_18(List_1_t1375417109 * value)
	{
		___idDEPORLst_18 = value;
		Il2CppCodeGenWriteBarrier(&___idDEPORLst_18, value);
	}

	inline static int32_t get_offset_of_idDEPORcrmLst_19() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987_StaticFields, ___idDEPORcrmLst_19)); }
	inline List_1_t1375417109 * get_idDEPORcrmLst_19() const { return ___idDEPORcrmLst_19; }
	inline List_1_t1375417109 ** get_address_of_idDEPORcrmLst_19() { return &___idDEPORcrmLst_19; }
	inline void set_idDEPORcrmLst_19(List_1_t1375417109 * value)
	{
		___idDEPORcrmLst_19 = value;
		Il2CppCodeGenWriteBarrier(&___idDEPORcrmLst_19, value);
	}

	inline static int32_t get_offset_of_validadeDEPORLst_20() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987_StaticFields, ___validadeDEPORLst_20)); }
	inline List_1_t1375417109 * get_validadeDEPORLst_20() const { return ___validadeDEPORLst_20; }
	inline List_1_t1375417109 ** get_address_of_validadeDEPORLst_20() { return &___validadeDEPORLst_20; }
	inline void set_validadeDEPORLst_20(List_1_t1375417109 * value)
	{
		___validadeDEPORLst_20 = value;
		Il2CppCodeGenWriteBarrier(&___validadeDEPORLst_20, value);
	}

	inline static int32_t get_offset_of_linkDEPORLst_21() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987_StaticFields, ___linkDEPORLst_21)); }
	inline List_1_t1375417109 * get_linkDEPORLst_21() const { return ___linkDEPORLst_21; }
	inline List_1_t1375417109 ** get_address_of_linkDEPORLst_21() { return &___linkDEPORLst_21; }
	inline void set_linkDEPORLst_21(List_1_t1375417109 * value)
	{
		___linkDEPORLst_21 = value;
		Il2CppCodeGenWriteBarrier(&___linkDEPORLst_21, value);
	}

	inline static int32_t get_offset_of_textureLst_24() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987_StaticFields, ___textureLst_24)); }
	inline List_1_t957326451 * get_textureLst_24() const { return ___textureLst_24; }
	inline List_1_t957326451 ** get_address_of_textureLst_24() { return &___textureLst_24; }
	inline void set_textureLst_24(List_1_t957326451 * value)
	{
		___textureLst_24 = value;
		Il2CppCodeGenWriteBarrier(&___textureLst_24, value);
	}

	inline static int32_t get_offset_of_imgLst_25() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987_StaticFields, ___imgLst_25)); }
	inline List_1_t272385497 * get_imgLst_25() const { return ___imgLst_25; }
	inline List_1_t272385497 ** get_address_of_imgLst_25() { return &___imgLst_25; }
	inline void set_imgLst_25(List_1_t272385497 * value)
	{
		___imgLst_25 = value;
		Il2CppCodeGenWriteBarrier(&___imgLst_25, value);
	}

	inline static int32_t get_offset_of_textureDEPORLst_26() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987_StaticFields, ___textureDEPORLst_26)); }
	inline List_1_t957326451 * get_textureDEPORLst_26() const { return ___textureDEPORLst_26; }
	inline List_1_t957326451 ** get_address_of_textureDEPORLst_26() { return &___textureDEPORLst_26; }
	inline void set_textureDEPORLst_26(List_1_t957326451 * value)
	{
		___textureDEPORLst_26 = value;
		Il2CppCodeGenWriteBarrier(&___textureDEPORLst_26, value);
	}

	inline static int32_t get_offset_of_imgDEPORLst_27() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987_StaticFields, ___imgDEPORLst_27)); }
	inline List_1_t272385497 * get_imgDEPORLst_27() const { return ___imgDEPORLst_27; }
	inline List_1_t272385497 ** get_address_of_imgDEPORLst_27() { return &___imgDEPORLst_27; }
	inline void set_imgDEPORLst_27(List_1_t272385497 * value)
	{
		___imgDEPORLst_27 = value;
		Il2CppCodeGenWriteBarrier(&___imgDEPORLst_27, value);
	}

	inline static int32_t get_offset_of_itensList_32() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987_StaticFields, ___itensList_32)); }
	inline List_1_t747900261 * get_itensList_32() const { return ___itensList_32; }
	inline List_1_t747900261 ** get_address_of_itensList_32() { return &___itensList_32; }
	inline void set_itensList_32(List_1_t747900261 * value)
	{
		___itensList_32 = value;
		Il2CppCodeGenWriteBarrier(&___itensList_32, value);
	}

	inline static int32_t get_offset_of_currentItem_41() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987_StaticFields, ___currentItem_41)); }
	inline GameObject_t3674682005 * get_currentItem_41() const { return ___currentItem_41; }
	inline GameObject_t3674682005 ** get_address_of_currentItem_41() { return &___currentItem_41; }
	inline void set_currentItem_41(GameObject_t3674682005 * value)
	{
		___currentItem_41 = value;
		Il2CppCodeGenWriteBarrier(&___currentItem_41, value);
	}

	inline static int32_t get_offset_of_currentJsonTxt_46() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987_StaticFields, ___currentJsonTxt_46)); }
	inline String_t* get_currentJsonTxt_46() const { return ___currentJsonTxt_46; }
	inline String_t** get_address_of_currentJsonTxt_46() { return &___currentJsonTxt_46; }
	inline void set_currentJsonTxt_46(String_t* value)
	{
		___currentJsonTxt_46 = value;
		Il2CppCodeGenWriteBarrier(&___currentJsonTxt_46, value);
	}

	inline static int32_t get_offset_of_loadedItensCount_47() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987_StaticFields, ___loadedItensCount_47)); }
	inline int32_t get_loadedItensCount_47() const { return ___loadedItensCount_47; }
	inline int32_t* get_address_of_loadedItensCount_47() { return &___loadedItensCount_47; }
	inline void set_loadedItensCount_47(int32_t value)
	{
		___loadedItensCount_47 = value;
	}

	inline static int32_t get_offset_of_loadedItensDEPORCount_48() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987_StaticFields, ___loadedItensDEPORCount_48)); }
	inline int32_t get_loadedItensDEPORCount_48() const { return ___loadedItensDEPORCount_48; }
	inline int32_t* get_address_of_loadedItensDEPORCount_48() { return &___loadedItensDEPORCount_48; }
	inline void set_loadedItensDEPORCount_48(int32_t value)
	{
		___loadedItensDEPORCount_48 = value;
	}

	inline static int32_t get_offset_of_loadedImgsCount_51() { return static_cast<int32_t>(offsetof(OfertasManagerCRM_t3100875987_StaticFields, ___loadedImgsCount_51)); }
	inline int32_t get_loadedImgsCount_51() const { return ___loadedImgsCount_51; }
	inline int32_t* get_address_of_loadedImgsCount_51() { return &___loadedImgsCount_51; }
	inline void set_loadedImgsCount_51(int32_t value)
	{
		___loadedImgsCount_51 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
