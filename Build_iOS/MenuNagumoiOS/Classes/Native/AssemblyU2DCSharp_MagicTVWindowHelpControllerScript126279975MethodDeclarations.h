﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTVWindowHelpControllerScript
struct MagicTVWindowHelpControllerScript_t126279975;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTVWindowHelpControllerScript::.ctor()
extern "C"  void MagicTVWindowHelpControllerScript__ctor_m1976784932 (MagicTVWindowHelpControllerScript_t126279975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowHelpControllerScript::PrintClick()
extern "C"  void MagicTVWindowHelpControllerScript_PrintClick_m635029883 (MagicTVWindowHelpControllerScript_t126279975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowHelpControllerScript::YouTubeClick()
extern "C"  void MagicTVWindowHelpControllerScript_YouTubeClick_m3213036645 (MagicTVWindowHelpControllerScript_t126279975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
