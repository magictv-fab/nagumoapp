﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.events.VoidEventDispatcher/VoidEvent
struct VoidEvent_t1756309665;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ARM.events.VoidEventDispatcher/VoidEvent::.ctor(System.Object,System.IntPtr)
extern "C"  void VoidEvent__ctor_m1087021256 (VoidEvent_t1756309665 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.events.VoidEventDispatcher/VoidEvent::Invoke()
extern "C"  void VoidEvent_Invoke_m2242295586 (VoidEvent_t1756309665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ARM.events.VoidEventDispatcher/VoidEvent::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * VoidEvent_BeginInvoke_m1041854369 (VoidEvent_t1756309665 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.events.VoidEventDispatcher/VoidEvent::EndInvoke(System.IAsyncResult)
extern "C"  void VoidEvent_EndInvoke_m3034429144 (VoidEvent_t1756309665 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
