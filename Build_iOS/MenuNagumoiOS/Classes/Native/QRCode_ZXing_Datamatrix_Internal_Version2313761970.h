﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.Datamatrix.Internal.Version[]
struct VersionU5BU5D_t2074916935;
// ZXing.Datamatrix.Internal.Version/ECBlocks
struct ECBlocks_t3710297555;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Datamatrix.Internal.Version
struct  Version_t2313761970  : public Il2CppObject
{
public:
	// System.Int32 ZXing.Datamatrix.Internal.Version::versionNumber
	int32_t ___versionNumber_1;
	// System.Int32 ZXing.Datamatrix.Internal.Version::symbolSizeRows
	int32_t ___symbolSizeRows_2;
	// System.Int32 ZXing.Datamatrix.Internal.Version::symbolSizeColumns
	int32_t ___symbolSizeColumns_3;
	// System.Int32 ZXing.Datamatrix.Internal.Version::dataRegionSizeRows
	int32_t ___dataRegionSizeRows_4;
	// System.Int32 ZXing.Datamatrix.Internal.Version::dataRegionSizeColumns
	int32_t ___dataRegionSizeColumns_5;
	// ZXing.Datamatrix.Internal.Version/ECBlocks ZXing.Datamatrix.Internal.Version::ecBlocks
	ECBlocks_t3710297555 * ___ecBlocks_6;
	// System.Int32 ZXing.Datamatrix.Internal.Version::totalCodewords
	int32_t ___totalCodewords_7;

public:
	inline static int32_t get_offset_of_versionNumber_1() { return static_cast<int32_t>(offsetof(Version_t2313761970, ___versionNumber_1)); }
	inline int32_t get_versionNumber_1() const { return ___versionNumber_1; }
	inline int32_t* get_address_of_versionNumber_1() { return &___versionNumber_1; }
	inline void set_versionNumber_1(int32_t value)
	{
		___versionNumber_1 = value;
	}

	inline static int32_t get_offset_of_symbolSizeRows_2() { return static_cast<int32_t>(offsetof(Version_t2313761970, ___symbolSizeRows_2)); }
	inline int32_t get_symbolSizeRows_2() const { return ___symbolSizeRows_2; }
	inline int32_t* get_address_of_symbolSizeRows_2() { return &___symbolSizeRows_2; }
	inline void set_symbolSizeRows_2(int32_t value)
	{
		___symbolSizeRows_2 = value;
	}

	inline static int32_t get_offset_of_symbolSizeColumns_3() { return static_cast<int32_t>(offsetof(Version_t2313761970, ___symbolSizeColumns_3)); }
	inline int32_t get_symbolSizeColumns_3() const { return ___symbolSizeColumns_3; }
	inline int32_t* get_address_of_symbolSizeColumns_3() { return &___symbolSizeColumns_3; }
	inline void set_symbolSizeColumns_3(int32_t value)
	{
		___symbolSizeColumns_3 = value;
	}

	inline static int32_t get_offset_of_dataRegionSizeRows_4() { return static_cast<int32_t>(offsetof(Version_t2313761970, ___dataRegionSizeRows_4)); }
	inline int32_t get_dataRegionSizeRows_4() const { return ___dataRegionSizeRows_4; }
	inline int32_t* get_address_of_dataRegionSizeRows_4() { return &___dataRegionSizeRows_4; }
	inline void set_dataRegionSizeRows_4(int32_t value)
	{
		___dataRegionSizeRows_4 = value;
	}

	inline static int32_t get_offset_of_dataRegionSizeColumns_5() { return static_cast<int32_t>(offsetof(Version_t2313761970, ___dataRegionSizeColumns_5)); }
	inline int32_t get_dataRegionSizeColumns_5() const { return ___dataRegionSizeColumns_5; }
	inline int32_t* get_address_of_dataRegionSizeColumns_5() { return &___dataRegionSizeColumns_5; }
	inline void set_dataRegionSizeColumns_5(int32_t value)
	{
		___dataRegionSizeColumns_5 = value;
	}

	inline static int32_t get_offset_of_ecBlocks_6() { return static_cast<int32_t>(offsetof(Version_t2313761970, ___ecBlocks_6)); }
	inline ECBlocks_t3710297555 * get_ecBlocks_6() const { return ___ecBlocks_6; }
	inline ECBlocks_t3710297555 ** get_address_of_ecBlocks_6() { return &___ecBlocks_6; }
	inline void set_ecBlocks_6(ECBlocks_t3710297555 * value)
	{
		___ecBlocks_6 = value;
		Il2CppCodeGenWriteBarrier(&___ecBlocks_6, value);
	}

	inline static int32_t get_offset_of_totalCodewords_7() { return static_cast<int32_t>(offsetof(Version_t2313761970, ___totalCodewords_7)); }
	inline int32_t get_totalCodewords_7() const { return ___totalCodewords_7; }
	inline int32_t* get_address_of_totalCodewords_7() { return &___totalCodewords_7; }
	inline void set_totalCodewords_7(int32_t value)
	{
		___totalCodewords_7 = value;
	}
};

struct Version_t2313761970_StaticFields
{
public:
	// ZXing.Datamatrix.Internal.Version[] ZXing.Datamatrix.Internal.Version::VERSIONS
	VersionU5BU5D_t2074916935* ___VERSIONS_0;

public:
	inline static int32_t get_offset_of_VERSIONS_0() { return static_cast<int32_t>(offsetof(Version_t2313761970_StaticFields, ___VERSIONS_0)); }
	inline VersionU5BU5D_t2074916935* get_VERSIONS_0() const { return ___VERSIONS_0; }
	inline VersionU5BU5D_t2074916935** get_address_of_VERSIONS_0() { return &___VERSIONS_0; }
	inline void set_VERSIONS_0(VersionU5BU5D_t2074916935* value)
	{
		___VERSIONS_0 = value;
		Il2CppCodeGenWriteBarrier(&___VERSIONS_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
