﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Common.ReedSolomon.ReedSolomonEncoder
struct ReedSolomonEncoder_t1017191363;
// ZXing.Common.ReedSolomon.GenericGF
struct GenericGF_t2563420960;
// ZXing.Common.ReedSolomon.GenericGFPoly
struct GenericGFPoly_t755870220;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_ReedSolomon_GenericGF2563420960.h"

// System.Void ZXing.Common.ReedSolomon.ReedSolomonEncoder::.ctor(ZXing.Common.ReedSolomon.GenericGF)
extern "C"  void ReedSolomonEncoder__ctor_m2636388532 (ReedSolomonEncoder_t1017191363 * __this, GenericGF_t2563420960 * ___field0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.ReedSolomon.GenericGFPoly ZXing.Common.ReedSolomon.ReedSolomonEncoder::buildGenerator(System.Int32)
extern "C"  GenericGFPoly_t755870220 * ReedSolomonEncoder_buildGenerator_m1827133141 (ReedSolomonEncoder_t1017191363 * __this, int32_t ___degree0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.ReedSolomon.ReedSolomonEncoder::encode(System.Int32[],System.Int32)
extern "C"  void ReedSolomonEncoder_encode_m4066871180 (ReedSolomonEncoder_t1017191363 * __this, Int32U5BU5D_t3230847821* ___toEncode0, int32_t ___ecBytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
