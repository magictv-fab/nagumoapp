﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen2887177558MethodDeclarations.h"

// System.Void System.Comparison`1<HutongGames.PlayMaker.NamedVariable>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m3354586408(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t1928131426 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m487232819_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<HutongGames.PlayMaker.NamedVariable>::Invoke(T,T)
#define Comparison_1_Invoke_m2910531096(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t1928131426 *, NamedVariable_t3211770239 *, NamedVariable_t3211770239 *, const MethodInfo*))Comparison_1_Invoke_m1888033133_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<HutongGames.PlayMaker.NamedVariable>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m1047932305(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t1928131426 *, NamedVariable_t3211770239 *, NamedVariable_t3211770239 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m3177996774_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<HutongGames.PlayMaker.NamedVariable>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m1853709140(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t1928131426 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m651541983_gshared)(__this, ___result0, method)
