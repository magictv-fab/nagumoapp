﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23830311593.h"
#include "UnityEngine_UnityEngine_NetworkReachability612403035.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.NetworkReachability,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3523372559_gshared (KeyValuePair_2_t3830311593 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m3523372559(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3830311593 *, int32_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m3523372559_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.NetworkReachability,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m2409877625_gshared (KeyValuePair_2_t3830311593 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m2409877625(__this, method) ((  int32_t (*) (KeyValuePair_2_t3830311593 *, const MethodInfo*))KeyValuePair_2_get_Key_m2409877625_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.NetworkReachability,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1018898106_gshared (KeyValuePair_2_t3830311593 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m1018898106(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3830311593 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m1018898106_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.NetworkReachability,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m2845212253_gshared (KeyValuePair_2_t3830311593 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m2845212253(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t3830311593 *, const MethodInfo*))KeyValuePair_2_get_Value_m2845212253_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.NetworkReachability,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2102431162_gshared (KeyValuePair_2_t3830311593 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m2102431162(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3830311593 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m2102431162_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.NetworkReachability,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2263124942_gshared (KeyValuePair_2_t3830311593 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m2263124942(__this, method) ((  String_t* (*) (KeyValuePair_2_t3830311593 *, const MethodInfo*))KeyValuePair_2_ToString_m2263124942_gshared)(__this, method)
