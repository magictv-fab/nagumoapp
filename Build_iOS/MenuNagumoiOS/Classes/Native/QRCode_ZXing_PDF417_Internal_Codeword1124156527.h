﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.Codeword
struct  Codeword_t1124156527  : public Il2CppObject
{
public:
	// System.Int32 ZXing.PDF417.Internal.Codeword::<StartX>k__BackingField
	int32_t ___U3CStartXU3Ek__BackingField_1;
	// System.Int32 ZXing.PDF417.Internal.Codeword::<EndX>k__BackingField
	int32_t ___U3CEndXU3Ek__BackingField_2;
	// System.Int32 ZXing.PDF417.Internal.Codeword::<Bucket>k__BackingField
	int32_t ___U3CBucketU3Ek__BackingField_3;
	// System.Int32 ZXing.PDF417.Internal.Codeword::<Value>k__BackingField
	int32_t ___U3CValueU3Ek__BackingField_4;
	// System.Int32 ZXing.PDF417.Internal.Codeword::<RowNumber>k__BackingField
	int32_t ___U3CRowNumberU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CStartXU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Codeword_t1124156527, ___U3CStartXU3Ek__BackingField_1)); }
	inline int32_t get_U3CStartXU3Ek__BackingField_1() const { return ___U3CStartXU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CStartXU3Ek__BackingField_1() { return &___U3CStartXU3Ek__BackingField_1; }
	inline void set_U3CStartXU3Ek__BackingField_1(int32_t value)
	{
		___U3CStartXU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CEndXU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Codeword_t1124156527, ___U3CEndXU3Ek__BackingField_2)); }
	inline int32_t get_U3CEndXU3Ek__BackingField_2() const { return ___U3CEndXU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CEndXU3Ek__BackingField_2() { return &___U3CEndXU3Ek__BackingField_2; }
	inline void set_U3CEndXU3Ek__BackingField_2(int32_t value)
	{
		___U3CEndXU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CBucketU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Codeword_t1124156527, ___U3CBucketU3Ek__BackingField_3)); }
	inline int32_t get_U3CBucketU3Ek__BackingField_3() const { return ___U3CBucketU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CBucketU3Ek__BackingField_3() { return &___U3CBucketU3Ek__BackingField_3; }
	inline void set_U3CBucketU3Ek__BackingField_3(int32_t value)
	{
		___U3CBucketU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Codeword_t1124156527, ___U3CValueU3Ek__BackingField_4)); }
	inline int32_t get_U3CValueU3Ek__BackingField_4() const { return ___U3CValueU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CValueU3Ek__BackingField_4() { return &___U3CValueU3Ek__BackingField_4; }
	inline void set_U3CValueU3Ek__BackingField_4(int32_t value)
	{
		___U3CValueU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CRowNumberU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Codeword_t1124156527, ___U3CRowNumberU3Ek__BackingField_5)); }
	inline int32_t get_U3CRowNumberU3Ek__BackingField_5() const { return ___U3CRowNumberU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CRowNumberU3Ek__BackingField_5() { return &___U3CRowNumberU3Ek__BackingField_5; }
	inline void set_U3CRowNumberU3Ek__BackingField_5(int32_t value)
	{
		___U3CRowNumberU3Ek__BackingField_5 = value;
	}
};

struct Codeword_t1124156527_StaticFields
{
public:
	// System.Int32 ZXing.PDF417.Internal.Codeword::BARCODE_ROW_UNKNOWN
	int32_t ___BARCODE_ROW_UNKNOWN_0;

public:
	inline static int32_t get_offset_of_BARCODE_ROW_UNKNOWN_0() { return static_cast<int32_t>(offsetof(Codeword_t1124156527_StaticFields, ___BARCODE_ROW_UNKNOWN_0)); }
	inline int32_t get_BARCODE_ROW_UNKNOWN_0() const { return ___BARCODE_ROW_UNKNOWN_0; }
	inline int32_t* get_address_of_BARCODE_ROW_UNKNOWN_0() { return &___BARCODE_ROW_UNKNOWN_0; }
	inline void set_BARCODE_ROW_UNKNOWN_0(int32_t value)
	{
		___BARCODE_ROW_UNKNOWN_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
