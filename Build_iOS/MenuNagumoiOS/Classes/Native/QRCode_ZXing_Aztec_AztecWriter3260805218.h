﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Text.Encoding
struct Encoding_t2012439129;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Aztec.AztecWriter
struct  AztecWriter_t3260805218  : public Il2CppObject
{
public:

public:
};

struct AztecWriter_t3260805218_StaticFields
{
public:
	// System.Text.Encoding ZXing.Aztec.AztecWriter::DEFAULT_CHARSET
	Encoding_t2012439129 * ___DEFAULT_CHARSET_0;

public:
	inline static int32_t get_offset_of_DEFAULT_CHARSET_0() { return static_cast<int32_t>(offsetof(AztecWriter_t3260805218_StaticFields, ___DEFAULT_CHARSET_0)); }
	inline Encoding_t2012439129 * get_DEFAULT_CHARSET_0() const { return ___DEFAULT_CHARSET_0; }
	inline Encoding_t2012439129 ** get_address_of_DEFAULT_CHARSET_0() { return &___DEFAULT_CHARSET_0; }
	inline void set_DEFAULT_CHARSET_0(Encoding_t2012439129 * value)
	{
		___DEFAULT_CHARSET_0 = value;
		Il2CppCodeGenWriteBarrier(&___DEFAULT_CHARSET_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
