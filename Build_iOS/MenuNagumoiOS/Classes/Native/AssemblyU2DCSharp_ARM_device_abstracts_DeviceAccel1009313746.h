﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract/OnVoidEventHandler
struct OnVoidEventHandler_t1904900265;
// ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract/OnMoveHandler
struct OnMoveHandler_t662891892;

#include "AssemblyU2DCSharp_ARM_events_ChangeEventAbstract369667058.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract
struct  DeviceAccelerometerEventDispetcherAbstract_t1009313746  : public ChangeEventAbstract_t369667058
{
public:
	// System.Single ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::toleranceChangePercent
	float ___toleranceChangePercent_3;
	// UnityEngine.Vector3 ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::_lastVectorValue
	Vector3_t4282066566  ____lastVectorValue_4;
	// System.Boolean ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::_lastVectorHasValue
	bool ____lastVectorHasValue_5;
	// System.Single ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::DebugPercentChange
	float ___DebugPercentChange_6;
	// ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract/OnVoidEventHandler ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::onShakeInit
	OnVoidEventHandler_t1904900265 * ___onShakeInit_7;
	// ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract/OnVoidEventHandler ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::onShakeEnd
	OnVoidEventHandler_t1904900265 * ___onShakeEnd_8;
	// ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract/OnVoidEventHandler ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::onMoveInit
	OnVoidEventHandler_t1904900265 * ___onMoveInit_9;
	// ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract/OnVoidEventHandler ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::onMoveEnd
	OnVoidEventHandler_t1904900265 * ___onMoveEnd_10;
	// ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract/OnMoveHandler ARM.device.abstracts.DeviceAccelerometerEventDispetcherAbstract::onMove
	OnMoveHandler_t662891892 * ___onMove_11;

public:
	inline static int32_t get_offset_of_toleranceChangePercent_3() { return static_cast<int32_t>(offsetof(DeviceAccelerometerEventDispetcherAbstract_t1009313746, ___toleranceChangePercent_3)); }
	inline float get_toleranceChangePercent_3() const { return ___toleranceChangePercent_3; }
	inline float* get_address_of_toleranceChangePercent_3() { return &___toleranceChangePercent_3; }
	inline void set_toleranceChangePercent_3(float value)
	{
		___toleranceChangePercent_3 = value;
	}

	inline static int32_t get_offset_of__lastVectorValue_4() { return static_cast<int32_t>(offsetof(DeviceAccelerometerEventDispetcherAbstract_t1009313746, ____lastVectorValue_4)); }
	inline Vector3_t4282066566  get__lastVectorValue_4() const { return ____lastVectorValue_4; }
	inline Vector3_t4282066566 * get_address_of__lastVectorValue_4() { return &____lastVectorValue_4; }
	inline void set__lastVectorValue_4(Vector3_t4282066566  value)
	{
		____lastVectorValue_4 = value;
	}

	inline static int32_t get_offset_of__lastVectorHasValue_5() { return static_cast<int32_t>(offsetof(DeviceAccelerometerEventDispetcherAbstract_t1009313746, ____lastVectorHasValue_5)); }
	inline bool get__lastVectorHasValue_5() const { return ____lastVectorHasValue_5; }
	inline bool* get_address_of__lastVectorHasValue_5() { return &____lastVectorHasValue_5; }
	inline void set__lastVectorHasValue_5(bool value)
	{
		____lastVectorHasValue_5 = value;
	}

	inline static int32_t get_offset_of_DebugPercentChange_6() { return static_cast<int32_t>(offsetof(DeviceAccelerometerEventDispetcherAbstract_t1009313746, ___DebugPercentChange_6)); }
	inline float get_DebugPercentChange_6() const { return ___DebugPercentChange_6; }
	inline float* get_address_of_DebugPercentChange_6() { return &___DebugPercentChange_6; }
	inline void set_DebugPercentChange_6(float value)
	{
		___DebugPercentChange_6 = value;
	}

	inline static int32_t get_offset_of_onShakeInit_7() { return static_cast<int32_t>(offsetof(DeviceAccelerometerEventDispetcherAbstract_t1009313746, ___onShakeInit_7)); }
	inline OnVoidEventHandler_t1904900265 * get_onShakeInit_7() const { return ___onShakeInit_7; }
	inline OnVoidEventHandler_t1904900265 ** get_address_of_onShakeInit_7() { return &___onShakeInit_7; }
	inline void set_onShakeInit_7(OnVoidEventHandler_t1904900265 * value)
	{
		___onShakeInit_7 = value;
		Il2CppCodeGenWriteBarrier(&___onShakeInit_7, value);
	}

	inline static int32_t get_offset_of_onShakeEnd_8() { return static_cast<int32_t>(offsetof(DeviceAccelerometerEventDispetcherAbstract_t1009313746, ___onShakeEnd_8)); }
	inline OnVoidEventHandler_t1904900265 * get_onShakeEnd_8() const { return ___onShakeEnd_8; }
	inline OnVoidEventHandler_t1904900265 ** get_address_of_onShakeEnd_8() { return &___onShakeEnd_8; }
	inline void set_onShakeEnd_8(OnVoidEventHandler_t1904900265 * value)
	{
		___onShakeEnd_8 = value;
		Il2CppCodeGenWriteBarrier(&___onShakeEnd_8, value);
	}

	inline static int32_t get_offset_of_onMoveInit_9() { return static_cast<int32_t>(offsetof(DeviceAccelerometerEventDispetcherAbstract_t1009313746, ___onMoveInit_9)); }
	inline OnVoidEventHandler_t1904900265 * get_onMoveInit_9() const { return ___onMoveInit_9; }
	inline OnVoidEventHandler_t1904900265 ** get_address_of_onMoveInit_9() { return &___onMoveInit_9; }
	inline void set_onMoveInit_9(OnVoidEventHandler_t1904900265 * value)
	{
		___onMoveInit_9 = value;
		Il2CppCodeGenWriteBarrier(&___onMoveInit_9, value);
	}

	inline static int32_t get_offset_of_onMoveEnd_10() { return static_cast<int32_t>(offsetof(DeviceAccelerometerEventDispetcherAbstract_t1009313746, ___onMoveEnd_10)); }
	inline OnVoidEventHandler_t1904900265 * get_onMoveEnd_10() const { return ___onMoveEnd_10; }
	inline OnVoidEventHandler_t1904900265 ** get_address_of_onMoveEnd_10() { return &___onMoveEnd_10; }
	inline void set_onMoveEnd_10(OnVoidEventHandler_t1904900265 * value)
	{
		___onMoveEnd_10 = value;
		Il2CppCodeGenWriteBarrier(&___onMoveEnd_10, value);
	}

	inline static int32_t get_offset_of_onMove_11() { return static_cast<int32_t>(offsetof(DeviceAccelerometerEventDispetcherAbstract_t1009313746, ___onMove_11)); }
	inline OnMoveHandler_t662891892 * get_onMove_11() const { return ___onMove_11; }
	inline OnMoveHandler_t662891892 ** get_address_of_onMove_11() { return &___onMove_11; }
	inline void set_onMove_11(OnMoveHandler_t662891892 * value)
	{
		___onMove_11 = value;
		Il2CppCodeGenWriteBarrier(&___onMove_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
