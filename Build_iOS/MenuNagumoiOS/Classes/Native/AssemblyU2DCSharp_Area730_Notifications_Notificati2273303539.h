﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Int64[]
struct Int64U5BU5D_t2174042770;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Area730.Notifications.Notification
struct  Notification_t2273303539  : public Il2CppObject
{
public:
	// System.Int32 Area730.Notifications.Notification::_id
	int32_t ____id_0;
	// System.String Area730.Notifications.Notification::_smallIcon
	String_t* ____smallIcon_1;
	// System.String Area730.Notifications.Notification::_largeIcon
	String_t* ____largeIcon_2;
	// System.Int32 Area730.Notifications.Notification::_defaults
	int32_t ____defaults_3;
	// System.Boolean Area730.Notifications.Notification::_autoCancel
	bool ____autoCancel_4;
	// System.String Area730.Notifications.Notification::_sound
	String_t* ____sound_5;
	// System.String Area730.Notifications.Notification::_ticker
	String_t* ____ticker_6;
	// System.String Area730.Notifications.Notification::_title
	String_t* ____title_7;
	// System.String Area730.Notifications.Notification::_body
	String_t* ____body_8;
	// System.Int64[] Area730.Notifications.Notification::_vibratePattern
	Int64U5BU5D_t2174042770* ____vibratePattern_9;
	// System.Int64 Area730.Notifications.Notification::_when
	int64_t ____when_10;
	// System.Int64 Area730.Notifications.Notification::_delay
	int64_t ____delay_11;
	// System.Boolean Area730.Notifications.Notification::_isRepeating
	bool ____isRepeating_12;
	// System.Int64 Area730.Notifications.Notification::_interval
	int64_t ____interval_13;
	// System.Int32 Area730.Notifications.Notification::_number
	int32_t ____number_14;
	// System.Boolean Area730.Notifications.Notification::_alertOnce
	bool ____alertOnce_15;
	// System.String Area730.Notifications.Notification::_color
	String_t* ____color_16;
	// System.String Area730.Notifications.Notification::_group
	String_t* ____group_17;
	// System.String Area730.Notifications.Notification::_sortKey
	String_t* ____sortKey_18;

public:
	inline static int32_t get_offset_of__id_0() { return static_cast<int32_t>(offsetof(Notification_t2273303539, ____id_0)); }
	inline int32_t get__id_0() const { return ____id_0; }
	inline int32_t* get_address_of__id_0() { return &____id_0; }
	inline void set__id_0(int32_t value)
	{
		____id_0 = value;
	}

	inline static int32_t get_offset_of__smallIcon_1() { return static_cast<int32_t>(offsetof(Notification_t2273303539, ____smallIcon_1)); }
	inline String_t* get__smallIcon_1() const { return ____smallIcon_1; }
	inline String_t** get_address_of__smallIcon_1() { return &____smallIcon_1; }
	inline void set__smallIcon_1(String_t* value)
	{
		____smallIcon_1 = value;
		Il2CppCodeGenWriteBarrier(&____smallIcon_1, value);
	}

	inline static int32_t get_offset_of__largeIcon_2() { return static_cast<int32_t>(offsetof(Notification_t2273303539, ____largeIcon_2)); }
	inline String_t* get__largeIcon_2() const { return ____largeIcon_2; }
	inline String_t** get_address_of__largeIcon_2() { return &____largeIcon_2; }
	inline void set__largeIcon_2(String_t* value)
	{
		____largeIcon_2 = value;
		Il2CppCodeGenWriteBarrier(&____largeIcon_2, value);
	}

	inline static int32_t get_offset_of__defaults_3() { return static_cast<int32_t>(offsetof(Notification_t2273303539, ____defaults_3)); }
	inline int32_t get__defaults_3() const { return ____defaults_3; }
	inline int32_t* get_address_of__defaults_3() { return &____defaults_3; }
	inline void set__defaults_3(int32_t value)
	{
		____defaults_3 = value;
	}

	inline static int32_t get_offset_of__autoCancel_4() { return static_cast<int32_t>(offsetof(Notification_t2273303539, ____autoCancel_4)); }
	inline bool get__autoCancel_4() const { return ____autoCancel_4; }
	inline bool* get_address_of__autoCancel_4() { return &____autoCancel_4; }
	inline void set__autoCancel_4(bool value)
	{
		____autoCancel_4 = value;
	}

	inline static int32_t get_offset_of__sound_5() { return static_cast<int32_t>(offsetof(Notification_t2273303539, ____sound_5)); }
	inline String_t* get__sound_5() const { return ____sound_5; }
	inline String_t** get_address_of__sound_5() { return &____sound_5; }
	inline void set__sound_5(String_t* value)
	{
		____sound_5 = value;
		Il2CppCodeGenWriteBarrier(&____sound_5, value);
	}

	inline static int32_t get_offset_of__ticker_6() { return static_cast<int32_t>(offsetof(Notification_t2273303539, ____ticker_6)); }
	inline String_t* get__ticker_6() const { return ____ticker_6; }
	inline String_t** get_address_of__ticker_6() { return &____ticker_6; }
	inline void set__ticker_6(String_t* value)
	{
		____ticker_6 = value;
		Il2CppCodeGenWriteBarrier(&____ticker_6, value);
	}

	inline static int32_t get_offset_of__title_7() { return static_cast<int32_t>(offsetof(Notification_t2273303539, ____title_7)); }
	inline String_t* get__title_7() const { return ____title_7; }
	inline String_t** get_address_of__title_7() { return &____title_7; }
	inline void set__title_7(String_t* value)
	{
		____title_7 = value;
		Il2CppCodeGenWriteBarrier(&____title_7, value);
	}

	inline static int32_t get_offset_of__body_8() { return static_cast<int32_t>(offsetof(Notification_t2273303539, ____body_8)); }
	inline String_t* get__body_8() const { return ____body_8; }
	inline String_t** get_address_of__body_8() { return &____body_8; }
	inline void set__body_8(String_t* value)
	{
		____body_8 = value;
		Il2CppCodeGenWriteBarrier(&____body_8, value);
	}

	inline static int32_t get_offset_of__vibratePattern_9() { return static_cast<int32_t>(offsetof(Notification_t2273303539, ____vibratePattern_9)); }
	inline Int64U5BU5D_t2174042770* get__vibratePattern_9() const { return ____vibratePattern_9; }
	inline Int64U5BU5D_t2174042770** get_address_of__vibratePattern_9() { return &____vibratePattern_9; }
	inline void set__vibratePattern_9(Int64U5BU5D_t2174042770* value)
	{
		____vibratePattern_9 = value;
		Il2CppCodeGenWriteBarrier(&____vibratePattern_9, value);
	}

	inline static int32_t get_offset_of__when_10() { return static_cast<int32_t>(offsetof(Notification_t2273303539, ____when_10)); }
	inline int64_t get__when_10() const { return ____when_10; }
	inline int64_t* get_address_of__when_10() { return &____when_10; }
	inline void set__when_10(int64_t value)
	{
		____when_10 = value;
	}

	inline static int32_t get_offset_of__delay_11() { return static_cast<int32_t>(offsetof(Notification_t2273303539, ____delay_11)); }
	inline int64_t get__delay_11() const { return ____delay_11; }
	inline int64_t* get_address_of__delay_11() { return &____delay_11; }
	inline void set__delay_11(int64_t value)
	{
		____delay_11 = value;
	}

	inline static int32_t get_offset_of__isRepeating_12() { return static_cast<int32_t>(offsetof(Notification_t2273303539, ____isRepeating_12)); }
	inline bool get__isRepeating_12() const { return ____isRepeating_12; }
	inline bool* get_address_of__isRepeating_12() { return &____isRepeating_12; }
	inline void set__isRepeating_12(bool value)
	{
		____isRepeating_12 = value;
	}

	inline static int32_t get_offset_of__interval_13() { return static_cast<int32_t>(offsetof(Notification_t2273303539, ____interval_13)); }
	inline int64_t get__interval_13() const { return ____interval_13; }
	inline int64_t* get_address_of__interval_13() { return &____interval_13; }
	inline void set__interval_13(int64_t value)
	{
		____interval_13 = value;
	}

	inline static int32_t get_offset_of__number_14() { return static_cast<int32_t>(offsetof(Notification_t2273303539, ____number_14)); }
	inline int32_t get__number_14() const { return ____number_14; }
	inline int32_t* get_address_of__number_14() { return &____number_14; }
	inline void set__number_14(int32_t value)
	{
		____number_14 = value;
	}

	inline static int32_t get_offset_of__alertOnce_15() { return static_cast<int32_t>(offsetof(Notification_t2273303539, ____alertOnce_15)); }
	inline bool get__alertOnce_15() const { return ____alertOnce_15; }
	inline bool* get_address_of__alertOnce_15() { return &____alertOnce_15; }
	inline void set__alertOnce_15(bool value)
	{
		____alertOnce_15 = value;
	}

	inline static int32_t get_offset_of__color_16() { return static_cast<int32_t>(offsetof(Notification_t2273303539, ____color_16)); }
	inline String_t* get__color_16() const { return ____color_16; }
	inline String_t** get_address_of__color_16() { return &____color_16; }
	inline void set__color_16(String_t* value)
	{
		____color_16 = value;
		Il2CppCodeGenWriteBarrier(&____color_16, value);
	}

	inline static int32_t get_offset_of__group_17() { return static_cast<int32_t>(offsetof(Notification_t2273303539, ____group_17)); }
	inline String_t* get__group_17() const { return ____group_17; }
	inline String_t** get_address_of__group_17() { return &____group_17; }
	inline void set__group_17(String_t* value)
	{
		____group_17 = value;
		Il2CppCodeGenWriteBarrier(&____group_17, value);
	}

	inline static int32_t get_offset_of__sortKey_18() { return static_cast<int32_t>(offsetof(Notification_t2273303539, ____sortKey_18)); }
	inline String_t* get__sortKey_18() const { return ____sortKey_18; }
	inline String_t** get_address_of__sortKey_18() { return &____sortKey_18; }
	inline void set__sortKey_18(String_t* value)
	{
		____sortKey_18 = value;
		Il2CppCodeGenWriteBarrier(&____sortKey_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
