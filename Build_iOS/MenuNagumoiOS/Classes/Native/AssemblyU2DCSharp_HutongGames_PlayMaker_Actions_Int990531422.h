﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.IntCompare
struct  IntCompare_t990531422  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.IntCompare::integer1
	FsmInt_t1596138449 * ___integer1_9;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.IntCompare::integer2
	FsmInt_t1596138449 * ___integer2_10;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.IntCompare::equal
	FsmEvent_t2133468028 * ___equal_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.IntCompare::lessThan
	FsmEvent_t2133468028 * ___lessThan_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.IntCompare::greaterThan
	FsmEvent_t2133468028 * ___greaterThan_13;
	// System.Boolean HutongGames.PlayMaker.Actions.IntCompare::everyFrame
	bool ___everyFrame_14;

public:
	inline static int32_t get_offset_of_integer1_9() { return static_cast<int32_t>(offsetof(IntCompare_t990531422, ___integer1_9)); }
	inline FsmInt_t1596138449 * get_integer1_9() const { return ___integer1_9; }
	inline FsmInt_t1596138449 ** get_address_of_integer1_9() { return &___integer1_9; }
	inline void set_integer1_9(FsmInt_t1596138449 * value)
	{
		___integer1_9 = value;
		Il2CppCodeGenWriteBarrier(&___integer1_9, value);
	}

	inline static int32_t get_offset_of_integer2_10() { return static_cast<int32_t>(offsetof(IntCompare_t990531422, ___integer2_10)); }
	inline FsmInt_t1596138449 * get_integer2_10() const { return ___integer2_10; }
	inline FsmInt_t1596138449 ** get_address_of_integer2_10() { return &___integer2_10; }
	inline void set_integer2_10(FsmInt_t1596138449 * value)
	{
		___integer2_10 = value;
		Il2CppCodeGenWriteBarrier(&___integer2_10, value);
	}

	inline static int32_t get_offset_of_equal_11() { return static_cast<int32_t>(offsetof(IntCompare_t990531422, ___equal_11)); }
	inline FsmEvent_t2133468028 * get_equal_11() const { return ___equal_11; }
	inline FsmEvent_t2133468028 ** get_address_of_equal_11() { return &___equal_11; }
	inline void set_equal_11(FsmEvent_t2133468028 * value)
	{
		___equal_11 = value;
		Il2CppCodeGenWriteBarrier(&___equal_11, value);
	}

	inline static int32_t get_offset_of_lessThan_12() { return static_cast<int32_t>(offsetof(IntCompare_t990531422, ___lessThan_12)); }
	inline FsmEvent_t2133468028 * get_lessThan_12() const { return ___lessThan_12; }
	inline FsmEvent_t2133468028 ** get_address_of_lessThan_12() { return &___lessThan_12; }
	inline void set_lessThan_12(FsmEvent_t2133468028 * value)
	{
		___lessThan_12 = value;
		Il2CppCodeGenWriteBarrier(&___lessThan_12, value);
	}

	inline static int32_t get_offset_of_greaterThan_13() { return static_cast<int32_t>(offsetof(IntCompare_t990531422, ___greaterThan_13)); }
	inline FsmEvent_t2133468028 * get_greaterThan_13() const { return ___greaterThan_13; }
	inline FsmEvent_t2133468028 ** get_address_of_greaterThan_13() { return &___greaterThan_13; }
	inline void set_greaterThan_13(FsmEvent_t2133468028 * value)
	{
		___greaterThan_13 = value;
		Il2CppCodeGenWriteBarrier(&___greaterThan_13, value);
	}

	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(IntCompare_t990531422, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
