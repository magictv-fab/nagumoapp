﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.NetworkReachability,System.Object>
struct ValueCollection_t2632136600;
// System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>
struct Dictionary_2_t3931530887;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1863364295.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.NetworkReachability,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m1605897482_gshared (ValueCollection_t2632136600 * __this, Dictionary_2_t3931530887 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m1605897482(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2632136600 *, Dictionary_2_t3931530887 *, const MethodInfo*))ValueCollection__ctor_m1605897482_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.NetworkReachability,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1860920808_gshared (ValueCollection_t2632136600 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1860920808(__this, ___item0, method) ((  void (*) (ValueCollection_t2632136600 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1860920808_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.NetworkReachability,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2945484465_gshared (ValueCollection_t2632136600 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2945484465(__this, method) ((  void (*) (ValueCollection_t2632136600 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2945484465_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.NetworkReachability,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m4108960702_gshared (ValueCollection_t2632136600 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m4108960702(__this, ___item0, method) ((  bool (*) (ValueCollection_t2632136600 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m4108960702_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.NetworkReachability,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3392975843_gshared (ValueCollection_t2632136600 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3392975843(__this, ___item0, method) ((  bool (*) (ValueCollection_t2632136600 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3392975843_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.NetworkReachability,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4124050175_gshared (ValueCollection_t2632136600 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4124050175(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2632136600 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4124050175_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.NetworkReachability,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m1870085749_gshared (ValueCollection_t2632136600 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m1870085749(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2632136600 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1870085749_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.NetworkReachability,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3766397488_gshared (ValueCollection_t2632136600 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3766397488(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2632136600 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3766397488_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.NetworkReachability,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2384136561_gshared (ValueCollection_t2632136600 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2384136561(__this, method) ((  bool (*) (ValueCollection_t2632136600 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2384136561_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.NetworkReachability,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2169974993_gshared (ValueCollection_t2632136600 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2169974993(__this, method) ((  bool (*) (ValueCollection_t2632136600 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2169974993_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.NetworkReachability,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m2970768317_gshared (ValueCollection_t2632136600 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2970768317(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2632136600 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2970768317_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.NetworkReachability,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m3141820113_gshared (ValueCollection_t2632136600 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m3141820113(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2632136600 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m3141820113_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.NetworkReachability,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1863364295  ValueCollection_GetEnumerator_m28159028_gshared (ValueCollection_t2632136600 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m28159028(__this, method) ((  Enumerator_t1863364295  (*) (ValueCollection_t2632136600 *, const MethodInfo*))ValueCollection_GetEnumerator_m28159028_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.NetworkReachability,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m3414445207_gshared (ValueCollection_t2632136600 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m3414445207(__this, method) ((  int32_t (*) (ValueCollection_t2632136600 *, const MethodInfo*))ValueCollection_get_Count_m3414445207_gshared)(__this, method)
