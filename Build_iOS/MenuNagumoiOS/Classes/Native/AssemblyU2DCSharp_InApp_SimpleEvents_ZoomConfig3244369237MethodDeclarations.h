﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InApp.SimpleEvents.ZoomConfig
struct ZoomConfig_t3244369237;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void InApp.SimpleEvents.ZoomConfig::.ctor()
extern "C"  void ZoomConfig__ctor_m353099813 (ZoomConfig_t3244369237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// InApp.SimpleEvents.ZoomConfig InApp.SimpleEvents.ZoomConfig::GetInstance()
extern "C"  ZoomConfig_t3244369237 * ZoomConfig_GetInstance_m1303087899 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.SimpleEvents.ZoomConfig::Start()
extern "C"  void ZoomConfig_Start_m3595204901 (ZoomConfig_t3244369237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.SimpleEvents.ZoomConfig::configByString(System.String)
extern "C"  void ZoomConfig_configByString_m1679240345 (ZoomConfig_t3244369237 * __this, String_t* ___config0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.SimpleEvents.ZoomConfig::Reset()
extern "C"  void ZoomConfig_Reset_m2294500050 (ZoomConfig_t3244369237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
