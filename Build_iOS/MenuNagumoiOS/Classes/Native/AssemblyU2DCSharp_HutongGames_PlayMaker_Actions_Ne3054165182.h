﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// UnityEngine.NetworkView
struct NetworkView_t3656680617;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkViewIsMine
struct  NetworkViewIsMine_t3054165182  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.NetworkViewIsMine::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.NetworkViewIsMine::isMine
	FsmBool_t1075959796 * ___isMine_10;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkViewIsMine::isMineEvent
	FsmEvent_t2133468028 * ___isMineEvent_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkViewIsMine::isNotMineEvent
	FsmEvent_t2133468028 * ___isNotMineEvent_12;
	// UnityEngine.NetworkView HutongGames.PlayMaker.Actions.NetworkViewIsMine::_networkView
	NetworkView_t3656680617 * ____networkView_13;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(NetworkViewIsMine_t3054165182, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_isMine_10() { return static_cast<int32_t>(offsetof(NetworkViewIsMine_t3054165182, ___isMine_10)); }
	inline FsmBool_t1075959796 * get_isMine_10() const { return ___isMine_10; }
	inline FsmBool_t1075959796 ** get_address_of_isMine_10() { return &___isMine_10; }
	inline void set_isMine_10(FsmBool_t1075959796 * value)
	{
		___isMine_10 = value;
		Il2CppCodeGenWriteBarrier(&___isMine_10, value);
	}

	inline static int32_t get_offset_of_isMineEvent_11() { return static_cast<int32_t>(offsetof(NetworkViewIsMine_t3054165182, ___isMineEvent_11)); }
	inline FsmEvent_t2133468028 * get_isMineEvent_11() const { return ___isMineEvent_11; }
	inline FsmEvent_t2133468028 ** get_address_of_isMineEvent_11() { return &___isMineEvent_11; }
	inline void set_isMineEvent_11(FsmEvent_t2133468028 * value)
	{
		___isMineEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___isMineEvent_11, value);
	}

	inline static int32_t get_offset_of_isNotMineEvent_12() { return static_cast<int32_t>(offsetof(NetworkViewIsMine_t3054165182, ___isNotMineEvent_12)); }
	inline FsmEvent_t2133468028 * get_isNotMineEvent_12() const { return ___isNotMineEvent_12; }
	inline FsmEvent_t2133468028 ** get_address_of_isNotMineEvent_12() { return &___isNotMineEvent_12; }
	inline void set_isNotMineEvent_12(FsmEvent_t2133468028 * value)
	{
		___isNotMineEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___isNotMineEvent_12, value);
	}

	inline static int32_t get_offset_of__networkView_13() { return static_cast<int32_t>(offsetof(NetworkViewIsMine_t3054165182, ____networkView_13)); }
	inline NetworkView_t3656680617 * get__networkView_13() const { return ____networkView_13; }
	inline NetworkView_t3656680617 ** get_address_of__networkView_13() { return &____networkView_13; }
	inline void set__networkView_13(NetworkView_t3656680617 * value)
	{
		____networkView_13 = value;
		Il2CppCodeGenWriteBarrier(&____networkView_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
