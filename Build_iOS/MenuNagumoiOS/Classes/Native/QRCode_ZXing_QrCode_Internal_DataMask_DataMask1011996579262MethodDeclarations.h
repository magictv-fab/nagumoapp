﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.QrCode.Internal.DataMask/DataMask101
struct DataMask101_t1996579262;

#include "codegen/il2cpp-codegen.h"

// System.Boolean ZXing.QrCode.Internal.DataMask/DataMask101::isMasked(System.Int32,System.Int32)
extern "C"  bool DataMask101_isMasked_m3568756182 (DataMask101_t1996579262 * __this, int32_t ___i0, int32_t ___j1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.DataMask/DataMask101::.ctor()
extern "C"  void DataMask101__ctor_m3018795325 (DataMask101_t1996579262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
