﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackgroundMovement
struct  BackgroundMovement_t1630496797  : public MonoBehaviour_t667441552
{
public:
	// System.Single BackgroundMovement::acelerometerY
	float ___acelerometerY_2;
	// System.Single BackgroundMovement::cornerX
	float ___cornerX_3;

public:
	inline static int32_t get_offset_of_acelerometerY_2() { return static_cast<int32_t>(offsetof(BackgroundMovement_t1630496797, ___acelerometerY_2)); }
	inline float get_acelerometerY_2() const { return ___acelerometerY_2; }
	inline float* get_address_of_acelerometerY_2() { return &___acelerometerY_2; }
	inline void set_acelerometerY_2(float value)
	{
		___acelerometerY_2 = value;
	}

	inline static int32_t get_offset_of_cornerX_3() { return static_cast<int32_t>(offsetof(BackgroundMovement_t1630496797, ___cornerX_3)); }
	inline float get_cornerX_3() const { return ___cornerX_3; }
	inline float* get_address_of_cornerX_3() { return &___cornerX_3; }
	inline void set_cornerX_3(float value)
	{
		___cornerX_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
