﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen4293064463MethodDeclarations.h"

// System.Void System.Action`2<System.Object,System.EventArgs>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m1085972986(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t2663079113 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m4171654682_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<System.Object,System.EventArgs>::Invoke(T1,T2)
#define Action_2_Invoke_m2366895921(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t2663079113 *, Il2CppObject *, EventArgs_t2540831021 *, const MethodInfo*))Action_2_Invoke_m2160582097_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<System.Object,System.EventArgs>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m1223167048(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t2663079113 *, Il2CppObject *, EventArgs_t2540831021 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m3413657584_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<System.Object,System.EventArgs>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m1982252378(__this, ___result0, method) ((  void (*) (Action_2_t2663079113 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m3926193450_gshared)(__this, ___result0, method)
