﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AmIVisible
struct AmIVisible_t1175015477;

#include "codegen/il2cpp-codegen.h"

// System.Void AmIVisible::.ctor()
extern "C"  void AmIVisible__ctor_m2523968518 (AmIVisible_t1175015477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AmIVisible::OnBecameVisible()
extern "C"  void AmIVisible_OnBecameVisible_m3729045566 (AmIVisible_t1175015477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AmIVisible::OnBecameInvisible()
extern "C"  void AmIVisible_OnBecameInvisible_m3501719033 (AmIVisible_t1175015477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AmIVisible::RaiseChangeVisible(System.Boolean)
extern "C"  void AmIVisible_RaiseChangeVisible_m838147275 (AmIVisible_t1175015477 * __this, bool ___visible0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
