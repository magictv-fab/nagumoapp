﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// nativeShareAndroidScript/<ShareScreenshot>c__Iterator25
struct U3CShareScreenshotU3Ec__Iterator25_t3076450652;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void nativeShareAndroidScript/<ShareScreenshot>c__Iterator25::.ctor()
extern "C"  void U3CShareScreenshotU3Ec__Iterator25__ctor_m561290639 (U3CShareScreenshotU3Ec__Iterator25_t3076450652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object nativeShareAndroidScript/<ShareScreenshot>c__Iterator25::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CShareScreenshotU3Ec__Iterator25_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1855610915 (U3CShareScreenshotU3Ec__Iterator25_t3076450652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object nativeShareAndroidScript/<ShareScreenshot>c__Iterator25::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CShareScreenshotU3Ec__Iterator25_System_Collections_IEnumerator_get_Current_m3277785527 (U3CShareScreenshotU3Ec__Iterator25_t3076450652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean nativeShareAndroidScript/<ShareScreenshot>c__Iterator25::MoveNext()
extern "C"  bool U3CShareScreenshotU3Ec__Iterator25_MoveNext_m2671820293 (U3CShareScreenshotU3Ec__Iterator25_t3076450652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void nativeShareAndroidScript/<ShareScreenshot>c__Iterator25::Dispose()
extern "C"  void U3CShareScreenshotU3Ec__Iterator25_Dispose_m2427675148 (U3CShareScreenshotU3Ec__Iterator25_t3076450652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void nativeShareAndroidScript/<ShareScreenshot>c__Iterator25::Reset()
extern "C"  void U3CShareScreenshotU3Ec__Iterator25_Reset_m2502690876 (U3CShareScreenshotU3Ec__Iterator25_t3076450652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean nativeShareAndroidScript/<ShareScreenshot>c__Iterator25::<>m__7()
extern "C"  bool U3CShareScreenshotU3Ec__Iterator25_U3CU3Em__7_m3585388749 (U3CShareScreenshotU3Ec__Iterator25_t3076450652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
