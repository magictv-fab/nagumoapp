﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.LoadLevelNum
struct  LoadLevelNum_t2730020496  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.LoadLevelNum::levelIndex
	FsmInt_t1596138449 * ___levelIndex_9;
	// System.Boolean HutongGames.PlayMaker.Actions.LoadLevelNum::additive
	bool ___additive_10;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.LoadLevelNum::loadedEvent
	FsmEvent_t2133468028 * ___loadedEvent_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.LoadLevelNum::dontDestroyOnLoad
	FsmBool_t1075959796 * ___dontDestroyOnLoad_12;

public:
	inline static int32_t get_offset_of_levelIndex_9() { return static_cast<int32_t>(offsetof(LoadLevelNum_t2730020496, ___levelIndex_9)); }
	inline FsmInt_t1596138449 * get_levelIndex_9() const { return ___levelIndex_9; }
	inline FsmInt_t1596138449 ** get_address_of_levelIndex_9() { return &___levelIndex_9; }
	inline void set_levelIndex_9(FsmInt_t1596138449 * value)
	{
		___levelIndex_9 = value;
		Il2CppCodeGenWriteBarrier(&___levelIndex_9, value);
	}

	inline static int32_t get_offset_of_additive_10() { return static_cast<int32_t>(offsetof(LoadLevelNum_t2730020496, ___additive_10)); }
	inline bool get_additive_10() const { return ___additive_10; }
	inline bool* get_address_of_additive_10() { return &___additive_10; }
	inline void set_additive_10(bool value)
	{
		___additive_10 = value;
	}

	inline static int32_t get_offset_of_loadedEvent_11() { return static_cast<int32_t>(offsetof(LoadLevelNum_t2730020496, ___loadedEvent_11)); }
	inline FsmEvent_t2133468028 * get_loadedEvent_11() const { return ___loadedEvent_11; }
	inline FsmEvent_t2133468028 ** get_address_of_loadedEvent_11() { return &___loadedEvent_11; }
	inline void set_loadedEvent_11(FsmEvent_t2133468028 * value)
	{
		___loadedEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___loadedEvent_11, value);
	}

	inline static int32_t get_offset_of_dontDestroyOnLoad_12() { return static_cast<int32_t>(offsetof(LoadLevelNum_t2730020496, ___dontDestroyOnLoad_12)); }
	inline FsmBool_t1075959796 * get_dontDestroyOnLoad_12() const { return ___dontDestroyOnLoad_12; }
	inline FsmBool_t1075959796 ** get_address_of_dontDestroyOnLoad_12() { return &___dontDestroyOnLoad_12; }
	inline void set_dontDestroyOnLoad_12(FsmBool_t1075959796 * value)
	{
		___dontDestroyOnLoad_12 = value;
		Il2CppCodeGenWriteBarrier(&___dontDestroyOnLoad_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
