﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>
struct Dictionary_2_t1098039674;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1712975728.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Char,ZXing.Aztec.Internal.Decoder/Table>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2639398213_gshared (Enumerator_t1712975728 * __this, Dictionary_2_t1098039674 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m2639398213(__this, ___host0, method) ((  void (*) (Enumerator_t1712975728 *, Dictionary_2_t1098039674 *, const MethodInfo*))Enumerator__ctor_m2639398213_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m280506886_gshared (Enumerator_t1712975728 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m280506886(__this, method) ((  Il2CppObject * (*) (Enumerator_t1712975728 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m280506886_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2330012240_gshared (Enumerator_t1712975728 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2330012240(__this, method) ((  void (*) (Enumerator_t1712975728 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2330012240_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Char,ZXing.Aztec.Internal.Decoder/Table>::Dispose()
extern "C"  void Enumerator_Dispose_m5199655_gshared (Enumerator_t1712975728 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m5199655(__this, method) ((  void (*) (Enumerator_t1712975728 *, const MethodInfo*))Enumerator_Dispose_m5199655_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Char,ZXing.Aztec.Internal.Decoder/Table>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m211597184_gshared (Enumerator_t1712975728 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m211597184(__this, method) ((  bool (*) (Enumerator_t1712975728 *, const MethodInfo*))Enumerator_MoveNext_m211597184_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Char,ZXing.Aztec.Internal.Decoder/Table>::get_Current()
extern "C"  Il2CppChar Enumerator_get_Current_m3369431704_gshared (Enumerator_t1712975728 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3369431704(__this, method) ((  Il2CppChar (*) (Enumerator_t1712975728 *, const MethodInfo*))Enumerator_get_Current_m3369431704_gshared)(__this, method)
