﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ZipConstants
struct  ZipConstants_t422568284  : public Il2CppObject
{
public:

public:
};

struct ZipConstants_t422568284_StaticFields
{
public:
	// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipConstants::defaultCodePage
	int32_t ___defaultCodePage_35;

public:
	inline static int32_t get_offset_of_defaultCodePage_35() { return static_cast<int32_t>(offsetof(ZipConstants_t422568284_StaticFields, ___defaultCodePage_35)); }
	inline int32_t get_defaultCodePage_35() const { return ___defaultCodePage_35; }
	inline int32_t* get_address_of_defaultCodePage_35() { return &___defaultCodePage_35; }
	inline void set_defaultCodePage_35(int32_t value)
	{
		___defaultCodePage_35 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
