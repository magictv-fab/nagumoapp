﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Common.DecoderResult
struct DecoderResult_t3752650303;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// ZXing.Common.BitSource
struct BitSource_t1243445190;
// System.Text.StringBuilder
struct StringBuilder_t243639308;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Collections.Generic.IList`1<System.Byte[]>
struct IList_1_t2660440376;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitSource1243445190.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "QRCode_ZXing_Datamatrix_Internal_DecodedBitStreamP1613312036.h"

// ZXing.Common.DecoderResult ZXing.Datamatrix.Internal.DecodedBitStreamParser::decode(System.Byte[])
extern "C"  DecoderResult_t3752650303 * DecodedBitStreamParser_decode_m3198505103 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Datamatrix.Internal.DecodedBitStreamParser::decodeAsciiSegment(ZXing.Common.BitSource,System.Text.StringBuilder,System.Text.StringBuilder,ZXing.Datamatrix.Internal.DecodedBitStreamParser/Mode&)
extern "C"  bool DecodedBitStreamParser_decodeAsciiSegment_m233218736 (Il2CppObject * __this /* static, unused */, BitSource_t1243445190 * ___bits0, StringBuilder_t243639308 * ___result1, StringBuilder_t243639308 * ___resultTrailer2, int32_t* ___mode3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Datamatrix.Internal.DecodedBitStreamParser::decodeC40Segment(ZXing.Common.BitSource,System.Text.StringBuilder)
extern "C"  bool DecodedBitStreamParser_decodeC40Segment_m2797015074 (Il2CppObject * __this /* static, unused */, BitSource_t1243445190 * ___bits0, StringBuilder_t243639308 * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Datamatrix.Internal.DecodedBitStreamParser::decodeTextSegment(ZXing.Common.BitSource,System.Text.StringBuilder)
extern "C"  bool DecodedBitStreamParser_decodeTextSegment_m2404702884 (Il2CppObject * __this /* static, unused */, BitSource_t1243445190 * ___bits0, StringBuilder_t243639308 * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Datamatrix.Internal.DecodedBitStreamParser::decodeAnsiX12Segment(ZXing.Common.BitSource,System.Text.StringBuilder)
extern "C"  bool DecodedBitStreamParser_decodeAnsiX12Segment_m999576523 (Il2CppObject * __this /* static, unused */, BitSource_t1243445190 * ___bits0, StringBuilder_t243639308 * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Internal.DecodedBitStreamParser::parseTwoBytes(System.Int32,System.Int32,System.Int32[])
extern "C"  void DecodedBitStreamParser_parseTwoBytes_m2799405388 (Il2CppObject * __this /* static, unused */, int32_t ___firstByte0, int32_t ___secondByte1, Int32U5BU5D_t3230847821* ___result2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Datamatrix.Internal.DecodedBitStreamParser::decodeEdifactSegment(ZXing.Common.BitSource,System.Text.StringBuilder)
extern "C"  bool DecodedBitStreamParser_decodeEdifactSegment_m2255492363 (Il2CppObject * __this /* static, unused */, BitSource_t1243445190 * ___bits0, StringBuilder_t243639308 * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Datamatrix.Internal.DecodedBitStreamParser::decodeBase256Segment(ZXing.Common.BitSource,System.Text.StringBuilder,System.Collections.Generic.IList`1<System.Byte[]>)
extern "C"  bool DecodedBitStreamParser_decodeBase256Segment_m507336643 (Il2CppObject * __this /* static, unused */, BitSource_t1243445190 * ___bits0, StringBuilder_t243639308 * ___result1, Il2CppObject* ___byteSegments2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Internal.DecodedBitStreamParser::unrandomize255State(System.Int32,System.Int32)
extern "C"  int32_t DecodedBitStreamParser_unrandomize255State_m2954196530 (Il2CppObject * __this /* static, unused */, int32_t ___randomizedBase256Codeword0, int32_t ___base256CodewordPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Internal.DecodedBitStreamParser::.cctor()
extern "C"  void DecodedBitStreamParser__cctor_m2317432160 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
