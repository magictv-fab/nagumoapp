﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Permissions
struct Permissions_t2801572964;

#include "codegen/il2cpp-codegen.h"

// System.Void Permissions::.ctor()
extern "C"  void Permissions__ctor_m4115566471 (Permissions_t2801572964 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Permissions::Start()
extern "C"  void Permissions_Start_m3062704263 (Permissions_t2801572964 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Permissions::Update()
extern "C"  void Permissions_Update_m460403814 (Permissions_t2801572964 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Permissions::Permission()
extern "C"  void Permissions_Permission_m2447493612 (Permissions_t2801572964 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
