﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "PlayMaker_HutongGames_PlayMaker_LogLevel284580066.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DebugInt
struct  DebugInt_t295189828  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.LogLevel HutongGames.PlayMaker.Actions.DebugInt::logLevel
	int32_t ___logLevel_9;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.DebugInt::intVariable
	FsmInt_t1596138449 * ___intVariable_10;

public:
	inline static int32_t get_offset_of_logLevel_9() { return static_cast<int32_t>(offsetof(DebugInt_t295189828, ___logLevel_9)); }
	inline int32_t get_logLevel_9() const { return ___logLevel_9; }
	inline int32_t* get_address_of_logLevel_9() { return &___logLevel_9; }
	inline void set_logLevel_9(int32_t value)
	{
		___logLevel_9 = value;
	}

	inline static int32_t get_offset_of_intVariable_10() { return static_cast<int32_t>(offsetof(DebugInt_t295189828, ___intVariable_10)); }
	inline FsmInt_t1596138449 * get_intVariable_10() const { return ___intVariable_10; }
	inline FsmInt_t1596138449 ** get_address_of_intVariable_10() { return &___intVariable_10; }
	inline void set_intVariable_10(FsmInt_t1596138449 * value)
	{
		___intVariable_10 = value;
		Il2CppCodeGenWriteBarrier(&___intVariable_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
