﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>
struct Dictionary_2_t3931530887;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke251499645.h"
#include "UnityEngine_UnityEngine_NetworkReachability612403035.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.NetworkReachability,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3093485539_gshared (Enumerator_t251499645 * __this, Dictionary_2_t3931530887 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m3093485539(__this, ___host0, method) ((  void (*) (Enumerator_t251499645 *, Dictionary_2_t3931530887 *, const MethodInfo*))Enumerator__ctor_m3093485539_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.NetworkReachability,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1827587358_gshared (Enumerator_t251499645 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1827587358(__this, method) ((  Il2CppObject * (*) (Enumerator_t251499645 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1827587358_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.NetworkReachability,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2855284978_gshared (Enumerator_t251499645 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2855284978(__this, method) ((  void (*) (Enumerator_t251499645 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2855284978_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.NetworkReachability,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3878956357_gshared (Enumerator_t251499645 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3878956357(__this, method) ((  void (*) (Enumerator_t251499645 *, const MethodInfo*))Enumerator_Dispose_m3878956357_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.NetworkReachability,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1173585118_gshared (Enumerator_t251499645 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1173585118(__this, method) ((  bool (*) (Enumerator_t251499645 *, const MethodInfo*))Enumerator_MoveNext_m1173585118_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.NetworkReachability,System.Object>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m3262202422_gshared (Enumerator_t251499645 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3262202422(__this, method) ((  int32_t (*) (Enumerator_t251499645 *, const MethodInfo*))Enumerator_get_Current_m3262202422_gshared)(__this, method)
