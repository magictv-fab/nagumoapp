﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.RSS.Expanded.Decoders.AI013x0xDecoder
struct AI013x0xDecoder_t3995208024;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"

// System.Void ZXing.OneD.RSS.Expanded.Decoders.AI013x0xDecoder::.ctor(ZXing.Common.BitArray)
extern "C"  void AI013x0xDecoder__ctor_m525459028 (AI013x0xDecoder_t3995208024 * __this, BitArray_t4163851164 * ___information0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.OneD.RSS.Expanded.Decoders.AI013x0xDecoder::parseInformation()
extern "C"  String_t* AI013x0xDecoder_parseInformation_m2285645095 (AI013x0xDecoder_t3995208024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.Decoders.AI013x0xDecoder::.cctor()
extern "C"  void AI013x0xDecoder__cctor_m4290493972 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
