﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FindGameObject
struct  FindGameObject_t2514584402  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.FindGameObject::objectName
	FsmString_t952858651 * ___objectName_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.FindGameObject::withTag
	FsmString_t952858651 * ___withTag_10;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.FindGameObject::store
	FsmGameObject_t1697147867 * ___store_11;

public:
	inline static int32_t get_offset_of_objectName_9() { return static_cast<int32_t>(offsetof(FindGameObject_t2514584402, ___objectName_9)); }
	inline FsmString_t952858651 * get_objectName_9() const { return ___objectName_9; }
	inline FsmString_t952858651 ** get_address_of_objectName_9() { return &___objectName_9; }
	inline void set_objectName_9(FsmString_t952858651 * value)
	{
		___objectName_9 = value;
		Il2CppCodeGenWriteBarrier(&___objectName_9, value);
	}

	inline static int32_t get_offset_of_withTag_10() { return static_cast<int32_t>(offsetof(FindGameObject_t2514584402, ___withTag_10)); }
	inline FsmString_t952858651 * get_withTag_10() const { return ___withTag_10; }
	inline FsmString_t952858651 ** get_address_of_withTag_10() { return &___withTag_10; }
	inline void set_withTag_10(FsmString_t952858651 * value)
	{
		___withTag_10 = value;
		Il2CppCodeGenWriteBarrier(&___withTag_10, value);
	}

	inline static int32_t get_offset_of_store_11() { return static_cast<int32_t>(offsetof(FindGameObject_t2514584402, ___store_11)); }
	inline FsmGameObject_t1697147867 * get_store_11() const { return ___store_11; }
	inline FsmGameObject_t1697147867 ** get_address_of_store_11() { return &___store_11; }
	inline void set_store_11(FsmGameObject_t1697147867 * value)
	{
		___store_11 = value;
		Il2CppCodeGenWriteBarrier(&___store_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
