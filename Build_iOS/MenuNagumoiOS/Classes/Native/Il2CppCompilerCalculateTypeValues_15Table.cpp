﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Configuration_System_Configuration_Provider3487699904.h"
#include "System_Configuration_System_Configuration_Provider4139595341.h"
#include "System_Configuration_System_Configuration_ClientCo3765224944.h"
#include "System_Configuration_System_Configuration_ConfigNa4110742328.h"
#include "System_Configuration_System_Configuration_ConfigIn1047989972.h"
#include "System_Configuration_System_Configuration_Configura710589292.h"
#include "System_Configuration_System_Configuration_Configur3008879018.h"
#include "System_Configuration_System_Configuration_Configura956581384.h"
#include "System_Configuration_System_Configuration_Configur3599302988.h"
#include "System_Configuration_System_Configuration_Configur3842833450.h"
#include "System_Configuration_System_Configuration_ElementMa792723588.h"
#include "System_Configuration_System_Configuration_Configur1978119848.h"
#include "System_Configuration_System_Configuration_Configura180565334.h"
#include "System_Configuration_System_Configuration_Configur3906163842.h"
#include "System_Configuration_System_Configuration_Configur1727501650.h"
#include "System_Configuration_System_Configuration_Configura355685230.h"
#include "System_Configuration_System_Configuration_Configur1608233025.h"
#include "System_Configuration_System_Configuration_Configur1970371071.h"
#include "System_Configuration_System_Configuration_Configur1616530321.h"
#include "System_Configuration_System_Configuration_Configura677452661.h"
#include "System_Configuration_System_Configuration_Configur2045963483.h"
#include "System_Configuration_System_Configuration_Configur3009015393.h"
#include "System_Configuration_System_Configuration_Configur1568293941.h"
#include "System_Configuration_System_Configuration_Configurat55611423.h"
#include "System_Configuration_System_Configuration_Configur2366956343.h"
#include "System_Configuration_System_Configuration_Configur1929345356.h"
#include "System_Configuration_System_Configuration_Configur3180944403.h"
#include "System_Configuration_System_Configuration_Configur1911555921.h"
#include "System_Configuration_System_Configuration_Configur1787025236.h"
#include "System_Configuration_System_Configuration_Configur2272995238.h"
#include "System_Configuration_System_Configuration_Configur4020418340.h"
#include "System_Configuration_System_Configuration_Configura664448871.h"
#include "System_Configuration_System_Configuration_Configur3525642550.h"
#include "System_Configuration_System_Configuration_Configur4042579377.h"
#include "System_Configuration_ConfigXmlTextReader1241202565.h"
#include "System_Configuration_System_Configuration_DefaultS1277146536.h"
#include "System_Configuration_System_Configuration_DefaultV3634404501.h"
#include "System_Configuration_System_Configuration_ElementIn891176276.h"
#include "System_Configuration_System_Configuration_ExeConfi2966372562.h"
#include "System_Configuration_System_Configuration_IgnoreSe1685294889.h"
#include "System_Configuration_System_Configuration_Internal3847365013.h"
#include "System_Configuration_System_Configuration_Internal1184887678.h"
#include "System_Configuration_System_Configuration_Internal3052580727.h"
#include "System_Configuration_System_Configuration_ExeConfi1105095376.h"
#include "System_Configuration_System_Configuration_Internal3052878513.h"
#include "System_Configuration_System_Configuration_Property2703580109.h"
#include "System_Configuration_System_Configuration_Property2683512459.h"
#include "System_Configuration_System_Configuration_Property1915765386.h"
#include "System_Configuration_System_Configuration_Property3488709272.h"
#include "System_Configuration_System_Configuration_Protecte2119798060.h"
#include "System_Configuration_System_Configuration_Protecte1000417341.h"
#include "System_Configuration_System_Configuration_Protecte4281214715.h"
#include "System_Configuration_System_Configuration_Protecte1314853459.h"
#include "System_Configuration_System_Configuration_Provider1407873976.h"
#include "System_Configuration_System_Configuration_Provider2591528118.h"
#include "System_Configuration_System_Configuration_SectionI3830512105.h"
#include "System_Configuration_System_Configuration_SectionG2963986668.h"
#include "System_Configuration_System_Configuration_ConfigIn2269248722.h"
#include "System_Configuration_System_Configuration_SectionI3122661835.h"
#include "System_Configuration_System_MonoTODOAttribute2091695241.h"
#include "System_Configuration_System_MonoInternalNoteAttribu254854272.h"
#include "System_Security_U3CModuleU3E86524790.h"
#include "System_U3CModuleU3E86524790.h"
#include "System_System_Runtime_InteropServices_DefaultParame708093789.h"
#include "System_Locale2281372282.h"
#include "System_System_MonoTODOAttribute2091695241.h"
#include "System_System_MonoLimitationAttribute1719169173.h"
#include "System_System_Collections_Specialized_HybridDictio3032146074.h"
#include "System_System_Collections_Specialized_ListDictiona2682732540.h"
#include "System_System_Collections_Specialized_ListDictiona2044266038.h"
#include "System_System_Collections_Specialized_ListDictiona3271093914.h"
#include "System_System_Collections_Specialized_ListDictiona2450441588.h"
#include "System_System_Collections_Specialized_ListDictiona3666873205.h"
#include "System_System_Collections_Specialized_NameObjectCo1023199937.h"
#include "System_System_Collections_Specialized_NameObjectCo1625138937.h"
#include "System_System_Collections_Specialized_NameObjectCo4030006590.h"
#include "System_System_Collections_Specialized_NameObjectCo1246329035.h"
#include "System_System_Collections_Specialized_NameValueCol2791941106.h"
#include "System_System_Collections_Specialized_StringCollec3266201207.h"
#include "System_System_Collections_Specialized_StringDictio1159596143.h"
#include "System_System_Collections_Specialized_StringEnumer4086853533.h"
#include "System_System_ComponentModel_ArrayConverter1568537747.h"
#include "System_System_ComponentModel_ArrayConverter_ArrayP1292166168.h"
#include "System_System_ComponentModel_AttributeCollection100867136.h"
#include "System_System_ComponentModel_BaseNumberConverter3737780012.h"
#include "System_System_ComponentModel_BooleanConverter2934406884.h"
#include "System_System_ComponentModel_BrowsableAttribute686446803.h"
#include "System_System_ComponentModel_ByteConverter4141866270.h"
#include "System_System_ComponentModel_CategoryAttribute2980835428.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1500 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1501 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1502 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1503 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1504 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1505 = { sizeof (ProviderBase_t3487699904), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1505[3] = 
{
	ProviderBase_t3487699904::get_offset_of_alreadyInitialized_0(),
	ProviderBase_t3487699904::get_offset_of__description_1(),
	ProviderBase_t3487699904::get_offset_of__name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1506 = { sizeof (ProviderCollection_t4139595341), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1506[3] = 
{
	ProviderCollection_t4139595341::get_offset_of_lookup_0(),
	ProviderCollection_t4139595341::get_offset_of_readOnly_1(),
	ProviderCollection_t4139595341::get_offset_of_values_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1507 = { sizeof (ClientConfigurationSystem_t3765224944), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1507[1] = 
{
	ClientConfigurationSystem_t3765224944::get_offset_of_cfg_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1508 = { sizeof (ConfigNameValueCollection_t4110742328), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1508[1] = 
{
	ConfigNameValueCollection_t4110742328::get_offset_of_modified_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1509 = { sizeof (ConfigInfo_t1047989972), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1509[6] = 
{
	ConfigInfo_t1047989972::get_offset_of_Name_0(),
	ConfigInfo_t1047989972::get_offset_of_TypeName_1(),
	ConfigInfo_t1047989972::get_offset_of_Type_2(),
	ConfigInfo_t1047989972::get_offset_of_streamName_3(),
	ConfigInfo_t1047989972::get_offset_of_Parent_4(),
	ConfigInfo_t1047989972::get_offset_of_ConfigHost_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1510 = { sizeof (Configuration_t710589292), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1510[12] = 
{
	Configuration_t710589292::get_offset_of_parent_0(),
	Configuration_t710589292::get_offset_of_elementData_1(),
	Configuration_t710589292::get_offset_of_streamName_2(),
	Configuration_t710589292::get_offset_of_rootSectionGroup_3(),
	Configuration_t710589292::get_offset_of_locations_4(),
	Configuration_t710589292::get_offset_of_rootGroup_5(),
	Configuration_t710589292::get_offset_of_system_6(),
	Configuration_t710589292::get_offset_of_hasFile_7(),
	Configuration_t710589292::get_offset_of_rootNamespace_8(),
	Configuration_t710589292::get_offset_of_configPath_9(),
	Configuration_t710589292::get_offset_of_locationConfigPath_10(),
	Configuration_t710589292::get_offset_of_locationSubPath_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1511 = { sizeof (ConfigurationAllowDefinition_t3008879018)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1511[5] = 
{
	ConfigurationAllowDefinition_t3008879018::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1512 = { sizeof (ConfigurationAllowExeDefinition_t956581384)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1512[5] = 
{
	ConfigurationAllowExeDefinition_t956581384::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1513 = { sizeof (ConfigurationCollectionAttribute_t3599302988), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1513[5] = 
{
	ConfigurationCollectionAttribute_t3599302988::get_offset_of_addItemName_0(),
	ConfigurationCollectionAttribute_t3599302988::get_offset_of_clearItemsName_1(),
	ConfigurationCollectionAttribute_t3599302988::get_offset_of_removeItemName_2(),
	ConfigurationCollectionAttribute_t3599302988::get_offset_of_collectionType_3(),
	ConfigurationCollectionAttribute_t3599302988::get_offset_of_itemType_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1514 = { sizeof (ConfigurationElement_t3842833450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1514[13] = 
{
	ConfigurationElement_t3842833450::get_offset_of_rawXml_0(),
	ConfigurationElement_t3842833450::get_offset_of_modified_1(),
	ConfigurationElement_t3842833450::get_offset_of_map_2(),
	ConfigurationElement_t3842833450::get_offset_of_keyProps_3(),
	ConfigurationElement_t3842833450::get_offset_of_defaultCollection_4(),
	ConfigurationElement_t3842833450::get_offset_of_readOnly_5(),
	ConfigurationElement_t3842833450::get_offset_of_elementInfo_6(),
	ConfigurationElement_t3842833450::get_offset_of__configuration_7(),
	ConfigurationElement_t3842833450::get_offset_of_lockAllAttributesExcept_8(),
	ConfigurationElement_t3842833450::get_offset_of_lockAllElementsExcept_9(),
	ConfigurationElement_t3842833450::get_offset_of_lockAttributes_10(),
	ConfigurationElement_t3842833450::get_offset_of_lockElements_11(),
	ConfigurationElement_t3842833450::get_offset_of_lockItem_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1515 = { sizeof (ElementMap_t792723588), -1, sizeof(ElementMap_t792723588_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1515[3] = 
{
	ElementMap_t792723588_StaticFields::get_offset_of_elementMaps_0(),
	ElementMap_t792723588::get_offset_of_properties_1(),
	ElementMap_t792723588::get_offset_of_collectionAttribute_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1516 = { sizeof (ConfigurationElementCollection_t1978119848), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1516[10] = 
{
	ConfigurationElementCollection_t1978119848::get_offset_of_list_13(),
	ConfigurationElementCollection_t1978119848::get_offset_of_removed_14(),
	ConfigurationElementCollection_t1978119848::get_offset_of_inherited_15(),
	ConfigurationElementCollection_t1978119848::get_offset_of_emitClear_16(),
	ConfigurationElementCollection_t1978119848::get_offset_of_modified_17(),
	ConfigurationElementCollection_t1978119848::get_offset_of_comparer_18(),
	ConfigurationElementCollection_t1978119848::get_offset_of_inheritedLimitIndex_19(),
	ConfigurationElementCollection_t1978119848::get_offset_of_addElementName_20(),
	ConfigurationElementCollection_t1978119848::get_offset_of_clearElementName_21(),
	ConfigurationElementCollection_t1978119848::get_offset_of_removeElementName_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1517 = { sizeof (ConfigurationRemoveElement_t180565334), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1517[3] = 
{
	ConfigurationRemoveElement_t180565334::get_offset_of_properties_13(),
	ConfigurationRemoveElement_t180565334::get_offset_of__origElement_14(),
	ConfigurationRemoveElement_t180565334::get_offset_of__origCollection_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1518 = { sizeof (ConfigurationElementCollectionType_t3906163842)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1518[5] = 
{
	ConfigurationElementCollectionType_t3906163842::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1519 = { sizeof (ConfigurationErrorsException_t1727501650), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1519[2] = 
{
	ConfigurationErrorsException_t1727501650::get_offset_of_filename_13(),
	ConfigurationErrorsException_t1727501650::get_offset_of_line_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1520 = { sizeof (ConfigurationFileMap_t355685230), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1520[1] = 
{
	ConfigurationFileMap_t355685230::get_offset_of_machineConfigFilename_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1521 = { sizeof (ConfigurationLocation_t1608233025), -1, sizeof(ConfigurationLocation_t1608233025_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1521[7] = 
{
	ConfigurationLocation_t1608233025_StaticFields::get_offset_of_pathTrimChars_0(),
	ConfigurationLocation_t1608233025::get_offset_of_path_1(),
	ConfigurationLocation_t1608233025::get_offset_of_configuration_2(),
	ConfigurationLocation_t1608233025::get_offset_of_parent_3(),
	ConfigurationLocation_t1608233025::get_offset_of_xmlContent_4(),
	ConfigurationLocation_t1608233025::get_offset_of_parentResolved_5(),
	ConfigurationLocation_t1608233025::get_offset_of_allowOverride_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1522 = { sizeof (ConfigurationLocationCollection_t1970371071), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1523 = { sizeof (ConfigurationLockType_t1616530321)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1523[4] = 
{
	ConfigurationLockType_t1616530321::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1524 = { sizeof (ConfigurationLockCollection_t677452661), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1524[6] = 
{
	ConfigurationLockCollection_t677452661::get_offset_of_names_0(),
	ConfigurationLockCollection_t677452661::get_offset_of_element_1(),
	ConfigurationLockCollection_t677452661::get_offset_of_lockType_2(),
	ConfigurationLockCollection_t677452661::get_offset_of_is_modified_3(),
	ConfigurationLockCollection_t677452661::get_offset_of_valid_name_hash_4(),
	ConfigurationLockCollection_t677452661::get_offset_of_valid_names_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1525 = { sizeof (ConfigurationManager_t2045963483), -1, sizeof(ConfigurationManager_t2045963483_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1525[3] = 
{
	ConfigurationManager_t2045963483_StaticFields::get_offset_of_configFactory_0(),
	ConfigurationManager_t2045963483_StaticFields::get_offset_of_configSystem_1(),
	ConfigurationManager_t2045963483_StaticFields::get_offset_of_lockobj_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1526 = { sizeof (ConfigurationProperty_t3009015393), -1, sizeof(ConfigurationProperty_t3009015393_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1526[9] = 
{
	ConfigurationProperty_t3009015393_StaticFields::get_offset_of_NoDefaultValue_0(),
	ConfigurationProperty_t3009015393::get_offset_of_name_1(),
	ConfigurationProperty_t3009015393::get_offset_of_type_2(),
	ConfigurationProperty_t3009015393::get_offset_of_default_value_3(),
	ConfigurationProperty_t3009015393::get_offset_of_converter_4(),
	ConfigurationProperty_t3009015393::get_offset_of_validation_5(),
	ConfigurationProperty_t3009015393::get_offset_of_flags_6(),
	ConfigurationProperty_t3009015393::get_offset_of_description_7(),
	ConfigurationProperty_t3009015393::get_offset_of_collectionAttribute_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1527 = { sizeof (ConfigurationPropertyAttribute_t1568293941), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1527[3] = 
{
	ConfigurationPropertyAttribute_t1568293941::get_offset_of_name_0(),
	ConfigurationPropertyAttribute_t1568293941::get_offset_of_default_value_1(),
	ConfigurationPropertyAttribute_t1568293941::get_offset_of_flags_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1528 = { sizeof (ConfigurationPropertyCollection_t55611423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1528[1] = 
{
	ConfigurationPropertyCollection_t55611423::get_offset_of_collection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1529 = { sizeof (ConfigurationPropertyOptions_t2366956343)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1529[5] = 
{
	ConfigurationPropertyOptions_t2366956343::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1530 = { sizeof (ConfigurationSaveMode_t1929345356)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1530[4] = 
{
	ConfigurationSaveMode_t1929345356::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1531 = { sizeof (ConfigurationSection_t3180944403), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1531[4] = 
{
	ConfigurationSection_t3180944403::get_offset_of_sectionInformation_13(),
	ConfigurationSection_t3180944403::get_offset_of_section_handler_14(),
	ConfigurationSection_t3180944403::get_offset_of_externalDataXml_15(),
	ConfigurationSection_t3180944403::get_offset_of__configContext_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1532 = { sizeof (ConfigurationSectionCollection_t1911555921), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1532[2] = 
{
	ConfigurationSectionCollection_t1911555921::get_offset_of_group_10(),
	ConfigurationSectionCollection_t1911555921::get_offset_of_config_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1533 = { sizeof (U3CGetEnumeratorU3Ec__Iterator0_t1787025236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1533[5] = 
{
	U3CGetEnumeratorU3Ec__Iterator0_t1787025236::get_offset_of_U3CU24s_32U3E__0_0(),
	U3CGetEnumeratorU3Ec__Iterator0_t1787025236::get_offset_of_U3CkeyU3E__1_1(),
	U3CGetEnumeratorU3Ec__Iterator0_t1787025236::get_offset_of_U24PC_2(),
	U3CGetEnumeratorU3Ec__Iterator0_t1787025236::get_offset_of_U24current_3(),
	U3CGetEnumeratorU3Ec__Iterator0_t1787025236::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1534 = { sizeof (ConfigurationSectionGroup_t2272995238), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1534[5] = 
{
	ConfigurationSectionGroup_t2272995238::get_offset_of_sections_0(),
	ConfigurationSectionGroup_t2272995238::get_offset_of_groups_1(),
	ConfigurationSectionGroup_t2272995238::get_offset_of_config_2(),
	ConfigurationSectionGroup_t2272995238::get_offset_of_group_3(),
	ConfigurationSectionGroup_t2272995238::get_offset_of_initialized_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1535 = { sizeof (ConfigurationSectionGroupCollection_t4020418340), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1535[2] = 
{
	ConfigurationSectionGroupCollection_t4020418340::get_offset_of_group_10(),
	ConfigurationSectionGroupCollection_t4020418340::get_offset_of_config_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1536 = { sizeof (ConfigurationUserLevel_t664448871)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1536[4] = 
{
	ConfigurationUserLevel_t664448871::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1537 = { sizeof (ConfigurationValidatorAttribute_t3525642550), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1537[2] = 
{
	ConfigurationValidatorAttribute_t3525642550::get_offset_of_validatorType_0(),
	ConfigurationValidatorAttribute_t3525642550::get_offset_of_instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1538 = { sizeof (ConfigurationValidatorBase_t4042579377), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1539 = { sizeof (ConfigXmlTextReader_t1241202565), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1539[1] = 
{
	ConfigXmlTextReader_t1241202565::get_offset_of_fileName_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1540 = { sizeof (DefaultSection_t1277146536), -1, sizeof(DefaultSection_t1277146536_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1540[1] = 
{
	DefaultSection_t1277146536_StaticFields::get_offset_of_properties_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1541 = { sizeof (DefaultValidator_t3634404501), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1542 = { sizeof (ElementInformation_t891176276), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1542[3] = 
{
	ElementInformation_t891176276::get_offset_of_propertyInfo_0(),
	ElementInformation_t891176276::get_offset_of_owner_1(),
	ElementInformation_t891176276::get_offset_of_properties_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1543 = { sizeof (ExeConfigurationFileMap_t2966372562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1543[3] = 
{
	ExeConfigurationFileMap_t2966372562::get_offset_of_exeConfigFilename_1(),
	ExeConfigurationFileMap_t2966372562::get_offset_of_localUserConfigFilename_2(),
	ExeConfigurationFileMap_t2966372562::get_offset_of_roamingUserConfigFilename_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1544 = { sizeof (IgnoreSection_t1685294889), -1, sizeof(IgnoreSection_t1685294889_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1544[2] = 
{
	IgnoreSection_t1685294889::get_offset_of_xml_17(),
	IgnoreSection_t1685294889_StaticFields::get_offset_of_properties_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1545 = { sizeof (InternalConfigurationFactory_t3847365013), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1546 = { sizeof (InternalConfigurationSystem_t1184887678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1546[3] = 
{
	InternalConfigurationSystem_t1184887678::get_offset_of_host_0(),
	InternalConfigurationSystem_t1184887678::get_offset_of_root_1(),
	InternalConfigurationSystem_t1184887678::get_offset_of_hostInitParams_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1547 = { sizeof (InternalConfigurationHost_t3052580727), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1548 = { sizeof (ExeConfigurationHost_t1105095376), -1, sizeof(ExeConfigurationHost_t1105095376_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1548[3] = 
{
	ExeConfigurationHost_t1105095376::get_offset_of_map_0(),
	ExeConfigurationHost_t1105095376::get_offset_of_level_1(),
	ExeConfigurationHost_t1105095376_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1549 = { sizeof (InternalConfigurationRoot_t3052878513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1549[2] = 
{
	InternalConfigurationRoot_t3052878513::get_offset_of_host_0(),
	InternalConfigurationRoot_t3052878513::get_offset_of_isDesignTime_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1550 = { sizeof (PropertyInformation_t2703580109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1550[5] = 
{
	PropertyInformation_t2703580109::get_offset_of_isModified_0(),
	PropertyInformation_t2703580109::get_offset_of_val_1(),
	PropertyInformation_t2703580109::get_offset_of_origin_2(),
	PropertyInformation_t2703580109::get_offset_of_owner_3(),
	PropertyInformation_t2703580109::get_offset_of_property_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1551 = { sizeof (PropertyInformationCollection_t2683512459), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1552 = { sizeof (PropertyInformationEnumerator_t1915765386), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1552[2] = 
{
	PropertyInformationEnumerator_t1915765386::get_offset_of_collection_0(),
	PropertyInformationEnumerator_t1915765386::get_offset_of_position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1553 = { sizeof (PropertyValueOrigin_t3488709272)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1553[4] = 
{
	PropertyValueOrigin_t3488709272::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1554 = { sizeof (ProtectedConfiguration_t2119798060), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1555 = { sizeof (ProtectedConfigurationProvider_t1000417341), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1556 = { sizeof (ProtectedConfigurationProviderCollection_t4281214715), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1557 = { sizeof (ProtectedConfigurationSection_t1314853459), -1, sizeof(ProtectedConfigurationSection_t1314853459_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1557[4] = 
{
	ProtectedConfigurationSection_t1314853459_StaticFields::get_offset_of_defaultProviderProp_17(),
	ProtectedConfigurationSection_t1314853459_StaticFields::get_offset_of_providersProp_18(),
	ProtectedConfigurationSection_t1314853459_StaticFields::get_offset_of_properties_19(),
	ProtectedConfigurationSection_t1314853459::get_offset_of_providers_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1558 = { sizeof (ProviderSettings_t1407873976), -1, sizeof(ProviderSettings_t1407873976_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1558[4] = 
{
	ProviderSettings_t1407873976::get_offset_of_parameters_13(),
	ProviderSettings_t1407873976_StaticFields::get_offset_of_nameProp_14(),
	ProviderSettings_t1407873976_StaticFields::get_offset_of_typeProp_15(),
	ProviderSettings_t1407873976_StaticFields::get_offset_of_properties_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1559 = { sizeof (ProviderSettingsCollection_t2591528118), -1, sizeof(ProviderSettingsCollection_t2591528118_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1559[1] = 
{
	ProviderSettingsCollection_t2591528118_StaticFields::get_offset_of_props_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1560 = { sizeof (SectionInfo_t3830512105), -1, sizeof(SectionInfo_t3830512105_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1560[6] = 
{
	SectionInfo_t3830512105::get_offset_of_allowLocation_6(),
	SectionInfo_t3830512105::get_offset_of_requirePermission_7(),
	SectionInfo_t3830512105::get_offset_of_restartOnExternalChanges_8(),
	SectionInfo_t3830512105::get_offset_of_allowDefinition_9(),
	SectionInfo_t3830512105::get_offset_of_allowExeDefinition_10(),
	SectionInfo_t3830512105_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1561 = { sizeof (SectionGroupInfo_t2963986668), -1, sizeof(SectionGroupInfo_t2963986668_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1561[3] = 
{
	SectionGroupInfo_t2963986668::get_offset_of_sections_6(),
	SectionGroupInfo_t2963986668::get_offset_of_groups_7(),
	SectionGroupInfo_t2963986668_StaticFields::get_offset_of_emptyList_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1562 = { sizeof (ConfigInfoCollection_t2269248722), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1563 = { sizeof (SectionInformation_t3122661835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1563[13] = 
{
	SectionInformation_t3122661835::get_offset_of_parent_0(),
	SectionInformation_t3122661835::get_offset_of_allow_definition_1(),
	SectionInformation_t3122661835::get_offset_of_allow_exe_definition_2(),
	SectionInformation_t3122661835::get_offset_of_allow_location_3(),
	SectionInformation_t3122661835::get_offset_of_allow_override_4(),
	SectionInformation_t3122661835::get_offset_of_inherit_on_child_apps_5(),
	SectionInformation_t3122661835::get_offset_of_restart_on_external_changes_6(),
	SectionInformation_t3122661835::get_offset_of_require_permission_7(),
	SectionInformation_t3122661835::get_offset_of_config_source_8(),
	SectionInformation_t3122661835::get_offset_of_name_9(),
	SectionInformation_t3122661835::get_offset_of_raw_xml_10(),
	SectionInformation_t3122661835::get_offset_of_protection_provider_11(),
	SectionInformation_t3122661835::get_offset_of_U3CConfigFilePathU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1564 = { sizeof (MonoTODOAttribute_t2091695243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1564[1] = 
{
	MonoTODOAttribute_t2091695243::get_offset_of_comment_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1565 = { sizeof (MonoInternalNoteAttribute_t254854272), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1566 = { sizeof (U3CModuleU3E_t86524794), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1567 = { sizeof (U3CModuleU3E_t86524795), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1568 = { sizeof (DefaultParameterValueAttribute_t708093789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1568[1] = 
{
	DefaultParameterValueAttribute_t708093789::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1569 = { sizeof (Locale_t2281372284), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1570 = { sizeof (MonoTODOAttribute_t2091695244), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1570[1] = 
{
	MonoTODOAttribute_t2091695244::get_offset_of_comment_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1571 = { sizeof (MonoLimitationAttribute_t1719169174), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1572 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1572[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1573 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1573[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1574 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1574[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1575 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1575[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1576 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1576[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1577 = { sizeof (HybridDictionary_t3032146074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1577[3] = 
{
	HybridDictionary_t3032146074::get_offset_of_caseInsensitive_0(),
	HybridDictionary_t3032146074::get_offset_of_hashtable_1(),
	HybridDictionary_t3032146074::get_offset_of_list_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1578 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1579 = { sizeof (ListDictionary_t2682732540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1579[4] = 
{
	ListDictionary_t2682732540::get_offset_of_count_0(),
	ListDictionary_t2682732540::get_offset_of_version_1(),
	ListDictionary_t2682732540::get_offset_of_head_2(),
	ListDictionary_t2682732540::get_offset_of_comparer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1580 = { sizeof (DictionaryNode_t2044266038), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1580[3] = 
{
	DictionaryNode_t2044266038::get_offset_of_key_0(),
	DictionaryNode_t2044266038::get_offset_of_value_1(),
	DictionaryNode_t2044266038::get_offset_of_next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1581 = { sizeof (DictionaryNodeEnumerator_t3271093914), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1581[4] = 
{
	DictionaryNodeEnumerator_t3271093914::get_offset_of_dict_0(),
	DictionaryNodeEnumerator_t3271093914::get_offset_of_isAtStart_1(),
	DictionaryNodeEnumerator_t3271093914::get_offset_of_current_2(),
	DictionaryNodeEnumerator_t3271093914::get_offset_of_version_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1582 = { sizeof (DictionaryNodeCollection_t2450441588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1582[2] = 
{
	DictionaryNodeCollection_t2450441588::get_offset_of_dict_0(),
	DictionaryNodeCollection_t2450441588::get_offset_of_isKeyList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1583 = { sizeof (DictionaryNodeCollectionEnumerator_t3666873205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1583[2] = 
{
	DictionaryNodeCollectionEnumerator_t3666873205::get_offset_of_inner_0(),
	DictionaryNodeCollectionEnumerator_t3666873205::get_offset_of_isKeyList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1584 = { sizeof (NameObjectCollectionBase_t1023199937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1584[10] = 
{
	NameObjectCollectionBase_t1023199937::get_offset_of_m_ItemsContainer_0(),
	NameObjectCollectionBase_t1023199937::get_offset_of_m_NullKeyItem_1(),
	NameObjectCollectionBase_t1023199937::get_offset_of_m_ItemsArray_2(),
	NameObjectCollectionBase_t1023199937::get_offset_of_m_hashprovider_3(),
	NameObjectCollectionBase_t1023199937::get_offset_of_m_comparer_4(),
	NameObjectCollectionBase_t1023199937::get_offset_of_m_defCapacity_5(),
	NameObjectCollectionBase_t1023199937::get_offset_of_m_readonly_6(),
	NameObjectCollectionBase_t1023199937::get_offset_of_infoCopy_7(),
	NameObjectCollectionBase_t1023199937::get_offset_of_keyscoll_8(),
	NameObjectCollectionBase_t1023199937::get_offset_of_equality_comparer_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1585 = { sizeof (_Item_t1625138937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1585[2] = 
{
	_Item_t1625138937::get_offset_of_key_0(),
	_Item_t1625138937::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1586 = { sizeof (_KeysEnumerator_t4030006590), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1586[2] = 
{
	_KeysEnumerator_t4030006590::get_offset_of_m_collection_0(),
	_KeysEnumerator_t4030006590::get_offset_of_m_position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1587 = { sizeof (KeysCollection_t1246329035), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1587[1] = 
{
	KeysCollection_t1246329035::get_offset_of_m_collection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1588 = { sizeof (NameValueCollection_t2791941106), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1588[2] = 
{
	NameValueCollection_t2791941106::get_offset_of_cachedAllKeys_10(),
	NameValueCollection_t2791941106::get_offset_of_cachedAll_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1589 = { sizeof (StringCollection_t3266201207), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1589[1] = 
{
	StringCollection_t3266201207::get_offset_of_data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1590 = { sizeof (StringDictionary_t1159596143), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1590[1] = 
{
	StringDictionary_t1159596143::get_offset_of_contents_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1591 = { sizeof (StringEnumerator_t4086853533), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1591[1] = 
{
	StringEnumerator_t4086853533::get_offset_of_enumerable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1592 = { sizeof (ArrayConverter_t1568537747), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1593 = { sizeof (ArrayPropertyDescriptor_t1292166168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1593[2] = 
{
	ArrayPropertyDescriptor_t1292166168::get_offset_of_index_3(),
	ArrayPropertyDescriptor_t1292166168::get_offset_of_array_type_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1594 = { sizeof (AttributeCollection_t100867136), -1, sizeof(AttributeCollection_t100867136_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1594[2] = 
{
	AttributeCollection_t100867136::get_offset_of_attrList_0(),
	AttributeCollection_t100867136_StaticFields::get_offset_of_Empty_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1595 = { sizeof (BaseNumberConverter_t3737780012), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1595[1] = 
{
	BaseNumberConverter_t3737780012::get_offset_of_InnerType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1596 = { sizeof (BooleanConverter_t2934406884), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1597 = { sizeof (BrowsableAttribute_t686446803), -1, sizeof(BrowsableAttribute_t686446803_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1597[4] = 
{
	BrowsableAttribute_t686446803::get_offset_of_browsable_0(),
	BrowsableAttribute_t686446803_StaticFields::get_offset_of_Default_1(),
	BrowsableAttribute_t686446803_StaticFields::get_offset_of_No_2(),
	BrowsableAttribute_t686446803_StaticFields::get_offset_of_Yes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1598 = { sizeof (ByteConverter_t4141866270), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1599 = { sizeof (CategoryAttribute_t2980835428), -1, sizeof(CategoryAttribute_t2980835428_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1599[4] = 
{
	CategoryAttribute_t2980835428::get_offset_of_category_0(),
	CategoryAttribute_t2980835428::get_offset_of_IsLocalized_1(),
	CategoryAttribute_t2980835428_StaticFields::get_offset_of_def_2(),
	CategoryAttribute_t2980835428_StaticFields::get_offset_of_lockobj_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
