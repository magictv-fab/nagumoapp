﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AcougueData
struct AcougueData_t537282265;

#include "codegen/il2cpp-codegen.h"

// System.Void AcougueData::.ctor()
extern "C"  void AcougueData__ctor_m18143410 (AcougueData_t537282265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
