﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChangeObj
struct  ChangeObj_t3459212039  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject ChangeObj::go1
	GameObject_t3674682005 * ___go1_2;
	// UnityEngine.GameObject ChangeObj::go2
	GameObject_t3674682005 * ___go2_3;
	// UnityEngine.Vector3 ChangeObj::pos1
	Vector3_t4282066566  ___pos1_4;
	// UnityEngine.Vector3 ChangeObj::pos2
	Vector3_t4282066566  ___pos2_5;
	// UnityEngine.Vector3 ChangeObj::rot1
	Vector3_t4282066566  ___rot1_6;
	// UnityEngine.Vector3 ChangeObj::rot2
	Vector3_t4282066566  ___rot2_7;
	// UnityEngine.Vector3 ChangeObj::size1
	Vector3_t4282066566  ___size1_8;
	// UnityEngine.Vector3 ChangeObj::size2
	Vector3_t4282066566  ___size2_9;
	// System.Boolean ChangeObj::change
	bool ___change_10;

public:
	inline static int32_t get_offset_of_go1_2() { return static_cast<int32_t>(offsetof(ChangeObj_t3459212039, ___go1_2)); }
	inline GameObject_t3674682005 * get_go1_2() const { return ___go1_2; }
	inline GameObject_t3674682005 ** get_address_of_go1_2() { return &___go1_2; }
	inline void set_go1_2(GameObject_t3674682005 * value)
	{
		___go1_2 = value;
		Il2CppCodeGenWriteBarrier(&___go1_2, value);
	}

	inline static int32_t get_offset_of_go2_3() { return static_cast<int32_t>(offsetof(ChangeObj_t3459212039, ___go2_3)); }
	inline GameObject_t3674682005 * get_go2_3() const { return ___go2_3; }
	inline GameObject_t3674682005 ** get_address_of_go2_3() { return &___go2_3; }
	inline void set_go2_3(GameObject_t3674682005 * value)
	{
		___go2_3 = value;
		Il2CppCodeGenWriteBarrier(&___go2_3, value);
	}

	inline static int32_t get_offset_of_pos1_4() { return static_cast<int32_t>(offsetof(ChangeObj_t3459212039, ___pos1_4)); }
	inline Vector3_t4282066566  get_pos1_4() const { return ___pos1_4; }
	inline Vector3_t4282066566 * get_address_of_pos1_4() { return &___pos1_4; }
	inline void set_pos1_4(Vector3_t4282066566  value)
	{
		___pos1_4 = value;
	}

	inline static int32_t get_offset_of_pos2_5() { return static_cast<int32_t>(offsetof(ChangeObj_t3459212039, ___pos2_5)); }
	inline Vector3_t4282066566  get_pos2_5() const { return ___pos2_5; }
	inline Vector3_t4282066566 * get_address_of_pos2_5() { return &___pos2_5; }
	inline void set_pos2_5(Vector3_t4282066566  value)
	{
		___pos2_5 = value;
	}

	inline static int32_t get_offset_of_rot1_6() { return static_cast<int32_t>(offsetof(ChangeObj_t3459212039, ___rot1_6)); }
	inline Vector3_t4282066566  get_rot1_6() const { return ___rot1_6; }
	inline Vector3_t4282066566 * get_address_of_rot1_6() { return &___rot1_6; }
	inline void set_rot1_6(Vector3_t4282066566  value)
	{
		___rot1_6 = value;
	}

	inline static int32_t get_offset_of_rot2_7() { return static_cast<int32_t>(offsetof(ChangeObj_t3459212039, ___rot2_7)); }
	inline Vector3_t4282066566  get_rot2_7() const { return ___rot2_7; }
	inline Vector3_t4282066566 * get_address_of_rot2_7() { return &___rot2_7; }
	inline void set_rot2_7(Vector3_t4282066566  value)
	{
		___rot2_7 = value;
	}

	inline static int32_t get_offset_of_size1_8() { return static_cast<int32_t>(offsetof(ChangeObj_t3459212039, ___size1_8)); }
	inline Vector3_t4282066566  get_size1_8() const { return ___size1_8; }
	inline Vector3_t4282066566 * get_address_of_size1_8() { return &___size1_8; }
	inline void set_size1_8(Vector3_t4282066566  value)
	{
		___size1_8 = value;
	}

	inline static int32_t get_offset_of_size2_9() { return static_cast<int32_t>(offsetof(ChangeObj_t3459212039, ___size2_9)); }
	inline Vector3_t4282066566  get_size2_9() const { return ___size2_9; }
	inline Vector3_t4282066566 * get_address_of_size2_9() { return &___size2_9; }
	inline void set_size2_9(Vector3_t4282066566  value)
	{
		___size2_9 = value;
	}

	inline static int32_t get_offset_of_change_10() { return static_cast<int32_t>(offsetof(ChangeObj_t3459212039, ___change_10)); }
	inline bool get_change_10() const { return ___change_10; }
	inline bool* get_address_of_change_10() { return &___change_10; }
	inline void set_change_10(bool value)
	{
		___change_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
