﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.device.AccelerometerEventDispacher
struct AccelerometerEventDispacher_t1386858712;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t1365137228;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ARM.device.AccelerometerEventDispacher::.ctor()
extern "C"  void AccelerometerEventDispacher__ctor_m2013069147 (AccelerometerEventDispacher_t1386858712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.AccelerometerEventDispacher::Start()
extern "C"  void AccelerometerEventDispacher_Start_m960206939 (AccelerometerEventDispacher_t1386858712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.AccelerometerEventDispacher::LateUpdate()
extern "C"  void AccelerometerEventDispacher_LateUpdate_m1567140824 (AccelerometerEventDispacher_t1386858712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.AccelerometerEventDispacher::onTime()
extern "C"  void AccelerometerEventDispacher_onTime_m4139965109 (AccelerometerEventDispacher_t1386858712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.AccelerometerEventDispacher::OnGUI()
extern "C"  void AccelerometerEventDispacher_OnGUI_m1508467797 (AccelerometerEventDispacher_t1386858712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ARM.device.AccelerometerEventDispacher::getUniqueName()
extern "C"  String_t* AccelerometerEventDispacher_getUniqueName_m659569800 (AccelerometerEventDispacher_t1386858712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.AccelerometerEventDispacher::turnOn()
extern "C"  void AccelerometerEventDispacher_turnOn_m2749280421 (AccelerometerEventDispacher_t1386858712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.AccelerometerEventDispacher::turnOff()
extern "C"  void AccelerometerEventDispacher_turnOff_m3623135691 (AccelerometerEventDispacher_t1386858712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.AccelerometerEventDispacher::ResetOnOff()
extern "C"  void AccelerometerEventDispacher_ResetOnOff_m3179226954 (AccelerometerEventDispacher_t1386858712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARM.device.AccelerometerEventDispacher::isActive()
extern "C"  bool AccelerometerEventDispacher_isActive_m3341995245 (AccelerometerEventDispacher_t1386858712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject ARM.device.AccelerometerEventDispacher::getGameObjectReference()
extern "C"  GameObject_t3674682005 * AccelerometerEventDispacher_getGameObjectReference_m3448187746 (AccelerometerEventDispacher_t1386858712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.AccelerometerEventDispacher::configByString(System.String)
extern "C"  void AccelerometerEventDispacher_configByString_m877485903 (AccelerometerEventDispacher_t1386858712 * __this, String_t* ___metadata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.AccelerometerEventDispacher::configByFloatList(System.Collections.Generic.List`1<System.Single>)
extern "C"  void AccelerometerEventDispacher_configByFloatList_m2381005029 (AccelerometerEventDispacher_t1386858712 * __this, List_1_t1365137228 * ___config0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.AccelerometerEventDispacher::Reset()
extern "C"  void AccelerometerEventDispacher_Reset_m3954469384 (AccelerometerEventDispacher_t1386858712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
