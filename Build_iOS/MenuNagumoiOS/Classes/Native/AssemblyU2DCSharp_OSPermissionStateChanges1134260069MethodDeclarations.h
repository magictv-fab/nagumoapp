﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OSPermissionStateChanges
struct OSPermissionStateChanges_t1134260069;

#include "codegen/il2cpp-codegen.h"

// System.Void OSPermissionStateChanges::.ctor()
extern "C"  void OSPermissionStateChanges__ctor_m1426890966 (OSPermissionStateChanges_t1134260069 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
