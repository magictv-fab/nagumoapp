﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Correios.Net.Address
struct  Address_t2296282618  : public Il2CppObject
{
public:
	// System.String Correios.Net.Address::_city
	String_t* ____city_0;
	// System.String Correios.Net.Address::_district
	String_t* ____district_1;
	// System.String Correios.Net.Address::_state
	String_t* ____state_2;
	// System.String Correios.Net.Address::_street
	String_t* ____street_3;
	// System.String Correios.Net.Address::_zip
	String_t* ____zip_4;
	// System.Boolean Correios.Net.Address::<UniqueZip>k__BackingField
	bool ___U3CUniqueZipU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of__city_0() { return static_cast<int32_t>(offsetof(Address_t2296282618, ____city_0)); }
	inline String_t* get__city_0() const { return ____city_0; }
	inline String_t** get_address_of__city_0() { return &____city_0; }
	inline void set__city_0(String_t* value)
	{
		____city_0 = value;
		Il2CppCodeGenWriteBarrier(&____city_0, value);
	}

	inline static int32_t get_offset_of__district_1() { return static_cast<int32_t>(offsetof(Address_t2296282618, ____district_1)); }
	inline String_t* get__district_1() const { return ____district_1; }
	inline String_t** get_address_of__district_1() { return &____district_1; }
	inline void set__district_1(String_t* value)
	{
		____district_1 = value;
		Il2CppCodeGenWriteBarrier(&____district_1, value);
	}

	inline static int32_t get_offset_of__state_2() { return static_cast<int32_t>(offsetof(Address_t2296282618, ____state_2)); }
	inline String_t* get__state_2() const { return ____state_2; }
	inline String_t** get_address_of__state_2() { return &____state_2; }
	inline void set__state_2(String_t* value)
	{
		____state_2 = value;
		Il2CppCodeGenWriteBarrier(&____state_2, value);
	}

	inline static int32_t get_offset_of__street_3() { return static_cast<int32_t>(offsetof(Address_t2296282618, ____street_3)); }
	inline String_t* get__street_3() const { return ____street_3; }
	inline String_t** get_address_of__street_3() { return &____street_3; }
	inline void set__street_3(String_t* value)
	{
		____street_3 = value;
		Il2CppCodeGenWriteBarrier(&____street_3, value);
	}

	inline static int32_t get_offset_of__zip_4() { return static_cast<int32_t>(offsetof(Address_t2296282618, ____zip_4)); }
	inline String_t* get__zip_4() const { return ____zip_4; }
	inline String_t** get_address_of__zip_4() { return &____zip_4; }
	inline void set__zip_4(String_t* value)
	{
		____zip_4 = value;
		Il2CppCodeGenWriteBarrier(&____zip_4, value);
	}

	inline static int32_t get_offset_of_U3CUniqueZipU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Address_t2296282618, ___U3CUniqueZipU3Ek__BackingField_5)); }
	inline bool get_U3CUniqueZipU3Ek__BackingField_5() const { return ___U3CUniqueZipU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CUniqueZipU3Ek__BackingField_5() { return &___U3CUniqueZipU3Ek__BackingField_5; }
	inline void set_U3CUniqueZipU3Ek__BackingField_5(bool value)
	{
		___U3CUniqueZipU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
