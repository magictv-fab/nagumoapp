﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Animator
struct Animator_t2776330603;
// System.Object
struct Il2CppObject;
// PanelManager
struct PanelManager_t2965005609;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PanelManager/<DisablePanelDeleyed>c__Iterator6D
struct  U3CDisablePanelDeleyedU3Ec__Iterator6D_t1466402221  : public Il2CppObject
{
public:
	// System.Boolean PanelManager/<DisablePanelDeleyed>c__Iterator6D::<closedStateReached>__0
	bool ___U3CclosedStateReachedU3E__0_0;
	// System.Boolean PanelManager/<DisablePanelDeleyed>c__Iterator6D::<wantToClose>__1
	bool ___U3CwantToCloseU3E__1_1;
	// UnityEngine.Animator PanelManager/<DisablePanelDeleyed>c__Iterator6D::anim
	Animator_t2776330603 * ___anim_2;
	// System.Int32 PanelManager/<DisablePanelDeleyed>c__Iterator6D::$PC
	int32_t ___U24PC_3;
	// System.Object PanelManager/<DisablePanelDeleyed>c__Iterator6D::$current
	Il2CppObject * ___U24current_4;
	// UnityEngine.Animator PanelManager/<DisablePanelDeleyed>c__Iterator6D::<$>anim
	Animator_t2776330603 * ___U3CU24U3Eanim_5;
	// PanelManager PanelManager/<DisablePanelDeleyed>c__Iterator6D::<>f__this
	PanelManager_t2965005609 * ___U3CU3Ef__this_6;

public:
	inline static int32_t get_offset_of_U3CclosedStateReachedU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDisablePanelDeleyedU3Ec__Iterator6D_t1466402221, ___U3CclosedStateReachedU3E__0_0)); }
	inline bool get_U3CclosedStateReachedU3E__0_0() const { return ___U3CclosedStateReachedU3E__0_0; }
	inline bool* get_address_of_U3CclosedStateReachedU3E__0_0() { return &___U3CclosedStateReachedU3E__0_0; }
	inline void set_U3CclosedStateReachedU3E__0_0(bool value)
	{
		___U3CclosedStateReachedU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CwantToCloseU3E__1_1() { return static_cast<int32_t>(offsetof(U3CDisablePanelDeleyedU3Ec__Iterator6D_t1466402221, ___U3CwantToCloseU3E__1_1)); }
	inline bool get_U3CwantToCloseU3E__1_1() const { return ___U3CwantToCloseU3E__1_1; }
	inline bool* get_address_of_U3CwantToCloseU3E__1_1() { return &___U3CwantToCloseU3E__1_1; }
	inline void set_U3CwantToCloseU3E__1_1(bool value)
	{
		___U3CwantToCloseU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_anim_2() { return static_cast<int32_t>(offsetof(U3CDisablePanelDeleyedU3Ec__Iterator6D_t1466402221, ___anim_2)); }
	inline Animator_t2776330603 * get_anim_2() const { return ___anim_2; }
	inline Animator_t2776330603 ** get_address_of_anim_2() { return &___anim_2; }
	inline void set_anim_2(Animator_t2776330603 * value)
	{
		___anim_2 = value;
		Il2CppCodeGenWriteBarrier(&___anim_2, value);
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDisablePanelDeleyedU3Ec__Iterator6D_t1466402221, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CDisablePanelDeleyedU3Ec__Iterator6D_t1466402221, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eanim_5() { return static_cast<int32_t>(offsetof(U3CDisablePanelDeleyedU3Ec__Iterator6D_t1466402221, ___U3CU24U3Eanim_5)); }
	inline Animator_t2776330603 * get_U3CU24U3Eanim_5() const { return ___U3CU24U3Eanim_5; }
	inline Animator_t2776330603 ** get_address_of_U3CU24U3Eanim_5() { return &___U3CU24U3Eanim_5; }
	inline void set_U3CU24U3Eanim_5(Animator_t2776330603 * value)
	{
		___U3CU24U3Eanim_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eanim_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_6() { return static_cast<int32_t>(offsetof(U3CDisablePanelDeleyedU3Ec__Iterator6D_t1466402221, ___U3CU3Ef__this_6)); }
	inline PanelManager_t2965005609 * get_U3CU3Ef__this_6() const { return ___U3CU3Ef__this_6; }
	inline PanelManager_t2965005609 ** get_address_of_U3CU3Ef__this_6() { return &___U3CU3Ef__this_6; }
	inline void set_U3CU3Ef__this_6(PanelManager_t2965005609 * value)
	{
		___U3CU3Ef__this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
