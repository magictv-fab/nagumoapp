﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PlayMakerFSM
struct PlayMakerFSM_t3799847376;
// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetFsmState
struct  GetFsmState_t1222578997  : public FsmStateAction_t2366529033
{
public:
	// PlayMakerFSM HutongGames.PlayMaker.Actions.GetFsmState::fsmComponent
	PlayMakerFSM_t3799847376 * ___fsmComponent_9;
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetFsmState::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_10;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmState::fsmName
	FsmString_t952858651 * ___fsmName_11;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmState::storeResult
	FsmString_t952858651 * ___storeResult_12;
	// System.Boolean HutongGames.PlayMaker.Actions.GetFsmState::everyFrame
	bool ___everyFrame_13;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.GetFsmState::fsm
	PlayMakerFSM_t3799847376 * ___fsm_14;

public:
	inline static int32_t get_offset_of_fsmComponent_9() { return static_cast<int32_t>(offsetof(GetFsmState_t1222578997, ___fsmComponent_9)); }
	inline PlayMakerFSM_t3799847376 * get_fsmComponent_9() const { return ___fsmComponent_9; }
	inline PlayMakerFSM_t3799847376 ** get_address_of_fsmComponent_9() { return &___fsmComponent_9; }
	inline void set_fsmComponent_9(PlayMakerFSM_t3799847376 * value)
	{
		___fsmComponent_9 = value;
		Il2CppCodeGenWriteBarrier(&___fsmComponent_9, value);
	}

	inline static int32_t get_offset_of_gameObject_10() { return static_cast<int32_t>(offsetof(GetFsmState_t1222578997, ___gameObject_10)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_10() const { return ___gameObject_10; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_10() { return &___gameObject_10; }
	inline void set_gameObject_10(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_10 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_10, value);
	}

	inline static int32_t get_offset_of_fsmName_11() { return static_cast<int32_t>(offsetof(GetFsmState_t1222578997, ___fsmName_11)); }
	inline FsmString_t952858651 * get_fsmName_11() const { return ___fsmName_11; }
	inline FsmString_t952858651 ** get_address_of_fsmName_11() { return &___fsmName_11; }
	inline void set_fsmName_11(FsmString_t952858651 * value)
	{
		___fsmName_11 = value;
		Il2CppCodeGenWriteBarrier(&___fsmName_11, value);
	}

	inline static int32_t get_offset_of_storeResult_12() { return static_cast<int32_t>(offsetof(GetFsmState_t1222578997, ___storeResult_12)); }
	inline FsmString_t952858651 * get_storeResult_12() const { return ___storeResult_12; }
	inline FsmString_t952858651 ** get_address_of_storeResult_12() { return &___storeResult_12; }
	inline void set_storeResult_12(FsmString_t952858651 * value)
	{
		___storeResult_12 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_12, value);
	}

	inline static int32_t get_offset_of_everyFrame_13() { return static_cast<int32_t>(offsetof(GetFsmState_t1222578997, ___everyFrame_13)); }
	inline bool get_everyFrame_13() const { return ___everyFrame_13; }
	inline bool* get_address_of_everyFrame_13() { return &___everyFrame_13; }
	inline void set_everyFrame_13(bool value)
	{
		___everyFrame_13 = value;
	}

	inline static int32_t get_offset_of_fsm_14() { return static_cast<int32_t>(offsetof(GetFsmState_t1222578997, ___fsm_14)); }
	inline PlayMakerFSM_t3799847376 * get_fsm_14() const { return ___fsm_14; }
	inline PlayMakerFSM_t3799847376 ** get_address_of_fsm_14() { return &___fsm_14; }
	inline void set_fsm_14(PlayMakerFSM_t3799847376 * value)
	{
		___fsm_14 = value;
		Il2CppCodeGenWriteBarrier(&___fsm_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
