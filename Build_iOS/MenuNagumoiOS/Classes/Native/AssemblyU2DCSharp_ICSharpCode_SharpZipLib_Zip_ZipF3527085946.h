﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ICSharpCode.SharpZipLib.Zip.ZipEntry
struct ZipEntry_t3141689087;
// ICSharpCode.SharpZipLib.Zip.IStaticDataSource
struct IStaticDataSource_t3265679022;
// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipF2599129714.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate
struct  ZipUpdate_t3527085946  : public Il2CppObject
{
public:
	// ICSharpCode.SharpZipLib.Zip.ZipEntry ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate::entry_
	ZipEntry_t3141689087 * ___entry__0;
	// ICSharpCode.SharpZipLib.Zip.ZipEntry ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate::outEntry_
	ZipEntry_t3141689087 * ___outEntry__1;
	// ICSharpCode.SharpZipLib.Zip.ZipFile/UpdateCommand ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate::command_
	int32_t ___command__2;
	// ICSharpCode.SharpZipLib.Zip.IStaticDataSource ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate::dataSource_
	Il2CppObject * ___dataSource__3;
	// System.String ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate::filename_
	String_t* ___filename__4;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate::sizePatchOffset_
	int64_t ___sizePatchOffset__5;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate::crcPatchOffset_
	int64_t ___crcPatchOffset__6;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate::_offsetBasedSize
	int64_t ____offsetBasedSize_7;

public:
	inline static int32_t get_offset_of_entry__0() { return static_cast<int32_t>(offsetof(ZipUpdate_t3527085946, ___entry__0)); }
	inline ZipEntry_t3141689087 * get_entry__0() const { return ___entry__0; }
	inline ZipEntry_t3141689087 ** get_address_of_entry__0() { return &___entry__0; }
	inline void set_entry__0(ZipEntry_t3141689087 * value)
	{
		___entry__0 = value;
		Il2CppCodeGenWriteBarrier(&___entry__0, value);
	}

	inline static int32_t get_offset_of_outEntry__1() { return static_cast<int32_t>(offsetof(ZipUpdate_t3527085946, ___outEntry__1)); }
	inline ZipEntry_t3141689087 * get_outEntry__1() const { return ___outEntry__1; }
	inline ZipEntry_t3141689087 ** get_address_of_outEntry__1() { return &___outEntry__1; }
	inline void set_outEntry__1(ZipEntry_t3141689087 * value)
	{
		___outEntry__1 = value;
		Il2CppCodeGenWriteBarrier(&___outEntry__1, value);
	}

	inline static int32_t get_offset_of_command__2() { return static_cast<int32_t>(offsetof(ZipUpdate_t3527085946, ___command__2)); }
	inline int32_t get_command__2() const { return ___command__2; }
	inline int32_t* get_address_of_command__2() { return &___command__2; }
	inline void set_command__2(int32_t value)
	{
		___command__2 = value;
	}

	inline static int32_t get_offset_of_dataSource__3() { return static_cast<int32_t>(offsetof(ZipUpdate_t3527085946, ___dataSource__3)); }
	inline Il2CppObject * get_dataSource__3() const { return ___dataSource__3; }
	inline Il2CppObject ** get_address_of_dataSource__3() { return &___dataSource__3; }
	inline void set_dataSource__3(Il2CppObject * value)
	{
		___dataSource__3 = value;
		Il2CppCodeGenWriteBarrier(&___dataSource__3, value);
	}

	inline static int32_t get_offset_of_filename__4() { return static_cast<int32_t>(offsetof(ZipUpdate_t3527085946, ___filename__4)); }
	inline String_t* get_filename__4() const { return ___filename__4; }
	inline String_t** get_address_of_filename__4() { return &___filename__4; }
	inline void set_filename__4(String_t* value)
	{
		___filename__4 = value;
		Il2CppCodeGenWriteBarrier(&___filename__4, value);
	}

	inline static int32_t get_offset_of_sizePatchOffset__5() { return static_cast<int32_t>(offsetof(ZipUpdate_t3527085946, ___sizePatchOffset__5)); }
	inline int64_t get_sizePatchOffset__5() const { return ___sizePatchOffset__5; }
	inline int64_t* get_address_of_sizePatchOffset__5() { return &___sizePatchOffset__5; }
	inline void set_sizePatchOffset__5(int64_t value)
	{
		___sizePatchOffset__5 = value;
	}

	inline static int32_t get_offset_of_crcPatchOffset__6() { return static_cast<int32_t>(offsetof(ZipUpdate_t3527085946, ___crcPatchOffset__6)); }
	inline int64_t get_crcPatchOffset__6() const { return ___crcPatchOffset__6; }
	inline int64_t* get_address_of_crcPatchOffset__6() { return &___crcPatchOffset__6; }
	inline void set_crcPatchOffset__6(int64_t value)
	{
		___crcPatchOffset__6 = value;
	}

	inline static int32_t get_offset_of__offsetBasedSize_7() { return static_cast<int32_t>(offsetof(ZipUpdate_t3527085946, ____offsetBasedSize_7)); }
	inline int64_t get__offsetBasedSize_7() const { return ____offsetBasedSize_7; }
	inline int64_t* get_address_of__offsetBasedSize_7() { return &____offsetBasedSize_7; }
	inline void set__offsetBasedSize_7(int64_t value)
	{
		____offsetBasedSize_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
