﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniWebView/<GetHTMLContent>c__AnonStoreyA7
struct U3CGetHTMLContentU3Ec__AnonStoreyA7_t1968010125;
// UniWebViewNativeResultPayload
struct UniWebViewNativeResultPayload_t996691953;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UniWebViewNativeResultPayload996691953.h"

// System.Void UniWebView/<GetHTMLContent>c__AnonStoreyA7::.ctor()
extern "C"  void U3CGetHTMLContentU3Ec__AnonStoreyA7__ctor_m3680269230 (U3CGetHTMLContentU3Ec__AnonStoreyA7_t1968010125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView/<GetHTMLContent>c__AnonStoreyA7::<>m__89(UniWebViewNativeResultPayload)
extern "C"  void U3CGetHTMLContentU3Ec__AnonStoreyA7_U3CU3Em__89_m599930665 (U3CGetHTMLContentU3Ec__AnonStoreyA7_t1968010125 * __this, UniWebViewNativeResultPayload_t996691953 * ___payload0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
