﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Single[][]
struct SingleU5BU5DU5BU5D_t1565445624;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.PDF417CodewordDecoder
struct  PDF417CodewordDecoder_t1208976947  : public Il2CppObject
{
public:

public:
};

struct PDF417CodewordDecoder_t1208976947_StaticFields
{
public:
	// System.Single[][] ZXing.PDF417.Internal.PDF417CodewordDecoder::RATIOS_TABLE
	SingleU5BU5DU5BU5D_t1565445624* ___RATIOS_TABLE_0;

public:
	inline static int32_t get_offset_of_RATIOS_TABLE_0() { return static_cast<int32_t>(offsetof(PDF417CodewordDecoder_t1208976947_StaticFields, ___RATIOS_TABLE_0)); }
	inline SingleU5BU5DU5BU5D_t1565445624* get_RATIOS_TABLE_0() const { return ___RATIOS_TABLE_0; }
	inline SingleU5BU5DU5BU5D_t1565445624** get_address_of_RATIOS_TABLE_0() { return &___RATIOS_TABLE_0; }
	inline void set_RATIOS_TABLE_0(SingleU5BU5DU5BU5D_t1565445624* value)
	{
		___RATIOS_TABLE_0 = value;
		Il2CppCodeGenWriteBarrier(&___RATIOS_TABLE_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
