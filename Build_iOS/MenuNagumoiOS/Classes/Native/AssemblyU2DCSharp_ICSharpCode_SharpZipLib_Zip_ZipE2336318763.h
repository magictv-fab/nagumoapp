﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ICSharpCode.SharpZipLib.Core.INameTransform
struct INameTransform_t2173030;

#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipE3036340399.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ZipEntryFactory
struct  ZipEntryFactory_t2336318763  : public Il2CppObject
{
public:
	// ICSharpCode.SharpZipLib.Core.INameTransform ICSharpCode.SharpZipLib.Zip.ZipEntryFactory::nameTransform_
	Il2CppObject * ___nameTransform__0;
	// System.DateTime ICSharpCode.SharpZipLib.Zip.ZipEntryFactory::fixedDateTime_
	DateTime_t4283661327  ___fixedDateTime__1;
	// ICSharpCode.SharpZipLib.Zip.ZipEntryFactory/TimeSetting ICSharpCode.SharpZipLib.Zip.ZipEntryFactory::timeSetting_
	int32_t ___timeSetting__2;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntryFactory::isUnicodeText_
	bool ___isUnicodeText__3;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntryFactory::getAttributes_
	int32_t ___getAttributes__4;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntryFactory::setAttributes_
	int32_t ___setAttributes__5;

public:
	inline static int32_t get_offset_of_nameTransform__0() { return static_cast<int32_t>(offsetof(ZipEntryFactory_t2336318763, ___nameTransform__0)); }
	inline Il2CppObject * get_nameTransform__0() const { return ___nameTransform__0; }
	inline Il2CppObject ** get_address_of_nameTransform__0() { return &___nameTransform__0; }
	inline void set_nameTransform__0(Il2CppObject * value)
	{
		___nameTransform__0 = value;
		Il2CppCodeGenWriteBarrier(&___nameTransform__0, value);
	}

	inline static int32_t get_offset_of_fixedDateTime__1() { return static_cast<int32_t>(offsetof(ZipEntryFactory_t2336318763, ___fixedDateTime__1)); }
	inline DateTime_t4283661327  get_fixedDateTime__1() const { return ___fixedDateTime__1; }
	inline DateTime_t4283661327 * get_address_of_fixedDateTime__1() { return &___fixedDateTime__1; }
	inline void set_fixedDateTime__1(DateTime_t4283661327  value)
	{
		___fixedDateTime__1 = value;
	}

	inline static int32_t get_offset_of_timeSetting__2() { return static_cast<int32_t>(offsetof(ZipEntryFactory_t2336318763, ___timeSetting__2)); }
	inline int32_t get_timeSetting__2() const { return ___timeSetting__2; }
	inline int32_t* get_address_of_timeSetting__2() { return &___timeSetting__2; }
	inline void set_timeSetting__2(int32_t value)
	{
		___timeSetting__2 = value;
	}

	inline static int32_t get_offset_of_isUnicodeText__3() { return static_cast<int32_t>(offsetof(ZipEntryFactory_t2336318763, ___isUnicodeText__3)); }
	inline bool get_isUnicodeText__3() const { return ___isUnicodeText__3; }
	inline bool* get_address_of_isUnicodeText__3() { return &___isUnicodeText__3; }
	inline void set_isUnicodeText__3(bool value)
	{
		___isUnicodeText__3 = value;
	}

	inline static int32_t get_offset_of_getAttributes__4() { return static_cast<int32_t>(offsetof(ZipEntryFactory_t2336318763, ___getAttributes__4)); }
	inline int32_t get_getAttributes__4() const { return ___getAttributes__4; }
	inline int32_t* get_address_of_getAttributes__4() { return &___getAttributes__4; }
	inline void set_getAttributes__4(int32_t value)
	{
		___getAttributes__4 = value;
	}

	inline static int32_t get_offset_of_setAttributes__5() { return static_cast<int32_t>(offsetof(ZipEntryFactory_t2336318763, ___setAttributes__5)); }
	inline int32_t get_setAttributes__5() const { return ___setAttributes__5; }
	inline int32_t* get_address_of_setAttributes__5() { return &___setAttributes__5; }
	inline void set_setAttributes__5(int32_t value)
	{
		___setAttributes__5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
