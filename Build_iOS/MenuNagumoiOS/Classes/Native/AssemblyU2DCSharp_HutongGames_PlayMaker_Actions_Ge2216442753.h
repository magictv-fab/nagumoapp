﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmRect
struct FsmRect_t1076426478;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetRectFields
struct  GetRectFields_t2216442753  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.GetRectFields::rectVariable
	FsmRect_t1076426478 * ___rectVariable_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetRectFields::storeX
	FsmFloat_t2134102846 * ___storeX_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetRectFields::storeY
	FsmFloat_t2134102846 * ___storeY_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetRectFields::storeWidth
	FsmFloat_t2134102846 * ___storeWidth_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetRectFields::storeHeight
	FsmFloat_t2134102846 * ___storeHeight_13;
	// System.Boolean HutongGames.PlayMaker.Actions.GetRectFields::everyFrame
	bool ___everyFrame_14;

public:
	inline static int32_t get_offset_of_rectVariable_9() { return static_cast<int32_t>(offsetof(GetRectFields_t2216442753, ___rectVariable_9)); }
	inline FsmRect_t1076426478 * get_rectVariable_9() const { return ___rectVariable_9; }
	inline FsmRect_t1076426478 ** get_address_of_rectVariable_9() { return &___rectVariable_9; }
	inline void set_rectVariable_9(FsmRect_t1076426478 * value)
	{
		___rectVariable_9 = value;
		Il2CppCodeGenWriteBarrier(&___rectVariable_9, value);
	}

	inline static int32_t get_offset_of_storeX_10() { return static_cast<int32_t>(offsetof(GetRectFields_t2216442753, ___storeX_10)); }
	inline FsmFloat_t2134102846 * get_storeX_10() const { return ___storeX_10; }
	inline FsmFloat_t2134102846 ** get_address_of_storeX_10() { return &___storeX_10; }
	inline void set_storeX_10(FsmFloat_t2134102846 * value)
	{
		___storeX_10 = value;
		Il2CppCodeGenWriteBarrier(&___storeX_10, value);
	}

	inline static int32_t get_offset_of_storeY_11() { return static_cast<int32_t>(offsetof(GetRectFields_t2216442753, ___storeY_11)); }
	inline FsmFloat_t2134102846 * get_storeY_11() const { return ___storeY_11; }
	inline FsmFloat_t2134102846 ** get_address_of_storeY_11() { return &___storeY_11; }
	inline void set_storeY_11(FsmFloat_t2134102846 * value)
	{
		___storeY_11 = value;
		Il2CppCodeGenWriteBarrier(&___storeY_11, value);
	}

	inline static int32_t get_offset_of_storeWidth_12() { return static_cast<int32_t>(offsetof(GetRectFields_t2216442753, ___storeWidth_12)); }
	inline FsmFloat_t2134102846 * get_storeWidth_12() const { return ___storeWidth_12; }
	inline FsmFloat_t2134102846 ** get_address_of_storeWidth_12() { return &___storeWidth_12; }
	inline void set_storeWidth_12(FsmFloat_t2134102846 * value)
	{
		___storeWidth_12 = value;
		Il2CppCodeGenWriteBarrier(&___storeWidth_12, value);
	}

	inline static int32_t get_offset_of_storeHeight_13() { return static_cast<int32_t>(offsetof(GetRectFields_t2216442753, ___storeHeight_13)); }
	inline FsmFloat_t2134102846 * get_storeHeight_13() const { return ___storeHeight_13; }
	inline FsmFloat_t2134102846 ** get_address_of_storeHeight_13() { return &___storeHeight_13; }
	inline void set_storeHeight_13(FsmFloat_t2134102846 * value)
	{
		___storeHeight_13 = value;
		Il2CppCodeGenWriteBarrier(&___storeHeight_13, value);
	}

	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(GetRectFields_t2216442753, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
