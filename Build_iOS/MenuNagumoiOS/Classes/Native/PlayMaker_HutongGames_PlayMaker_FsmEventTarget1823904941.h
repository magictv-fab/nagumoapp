﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// PlayMakerFSM
struct PlayMakerFSM_t3799847376;

#include "mscorlib_System_Object4170816371.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventTarget_Eve3998278035.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmEventTarget
struct  FsmEventTarget_t1823904941  : public Il2CppObject
{
public:
	// HutongGames.PlayMaker.FsmEventTarget/EventTarget HutongGames.PlayMaker.FsmEventTarget::target
	int32_t ___target_0;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.FsmEventTarget::excludeSelf
	FsmBool_t1075959796 * ___excludeSelf_1;
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.FsmEventTarget::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_2;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.FsmEventTarget::fsmName
	FsmString_t952858651 * ___fsmName_3;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.FsmEventTarget::sendToChildren
	FsmBool_t1075959796 * ___sendToChildren_4;
	// PlayMakerFSM HutongGames.PlayMaker.FsmEventTarget::fsmComponent
	PlayMakerFSM_t3799847376 * ___fsmComponent_5;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(FsmEventTarget_t1823904941, ___target_0)); }
	inline int32_t get_target_0() const { return ___target_0; }
	inline int32_t* get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(int32_t value)
	{
		___target_0 = value;
	}

	inline static int32_t get_offset_of_excludeSelf_1() { return static_cast<int32_t>(offsetof(FsmEventTarget_t1823904941, ___excludeSelf_1)); }
	inline FsmBool_t1075959796 * get_excludeSelf_1() const { return ___excludeSelf_1; }
	inline FsmBool_t1075959796 ** get_address_of_excludeSelf_1() { return &___excludeSelf_1; }
	inline void set_excludeSelf_1(FsmBool_t1075959796 * value)
	{
		___excludeSelf_1 = value;
		Il2CppCodeGenWriteBarrier(&___excludeSelf_1, value);
	}

	inline static int32_t get_offset_of_gameObject_2() { return static_cast<int32_t>(offsetof(FsmEventTarget_t1823904941, ___gameObject_2)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_2() const { return ___gameObject_2; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_2() { return &___gameObject_2; }
	inline void set_gameObject_2(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_2 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_2, value);
	}

	inline static int32_t get_offset_of_fsmName_3() { return static_cast<int32_t>(offsetof(FsmEventTarget_t1823904941, ___fsmName_3)); }
	inline FsmString_t952858651 * get_fsmName_3() const { return ___fsmName_3; }
	inline FsmString_t952858651 ** get_address_of_fsmName_3() { return &___fsmName_3; }
	inline void set_fsmName_3(FsmString_t952858651 * value)
	{
		___fsmName_3 = value;
		Il2CppCodeGenWriteBarrier(&___fsmName_3, value);
	}

	inline static int32_t get_offset_of_sendToChildren_4() { return static_cast<int32_t>(offsetof(FsmEventTarget_t1823904941, ___sendToChildren_4)); }
	inline FsmBool_t1075959796 * get_sendToChildren_4() const { return ___sendToChildren_4; }
	inline FsmBool_t1075959796 ** get_address_of_sendToChildren_4() { return &___sendToChildren_4; }
	inline void set_sendToChildren_4(FsmBool_t1075959796 * value)
	{
		___sendToChildren_4 = value;
		Il2CppCodeGenWriteBarrier(&___sendToChildren_4, value);
	}

	inline static int32_t get_offset_of_fsmComponent_5() { return static_cast<int32_t>(offsetof(FsmEventTarget_t1823904941, ___fsmComponent_5)); }
	inline PlayMakerFSM_t3799847376 * get_fsmComponent_5() const { return ___fsmComponent_5; }
	inline PlayMakerFSM_t3799847376 ** get_address_of_fsmComponent_5() { return &___fsmComponent_5; }
	inline void set_fsmComponent_5(PlayMakerFSM_t3799847376 * value)
	{
		___fsmComponent_5 = value;
		Il2CppCodeGenWriteBarrier(&___fsmComponent_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
