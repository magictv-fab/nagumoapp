﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.transform.filters.CameraZoomFilterButton
struct CameraZoomFilterButton_t1406660070;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

// System.Void ARM.transform.filters.CameraZoomFilterButton::.ctor()
extern "C"  void CameraZoomFilterButton__ctor_m3743768978 (CameraZoomFilterButton_t1406660070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.CameraZoomFilterButton::Start()
extern "C"  void CameraZoomFilterButton_Start_m2690906770 (CameraZoomFilterButton_t1406660070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.transform.filters.CameraZoomFilterButton::_doFilter(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  CameraZoomFilterButton__doFilter_m3542635540 (CameraZoomFilterButton_t1406660070 * __this, Vector3_t4282066566  ___last0, Vector3_t4282066566  ___nextValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.CameraZoomFilterButton::OnGUI()
extern "C"  void CameraZoomFilterButton_OnGUI_m3239167628 (CameraZoomFilterButton_t1406660070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.transform.filters.CameraZoomFilterButton::filtroComPlugin(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  CameraZoomFilterButton_filtroComPlugin_m2520387671 (CameraZoomFilterButton_t1406660070 * __this, Vector3_t4282066566  ___nextValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion ARM.transform.filters.CameraZoomFilterButton::_doFilter(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  CameraZoomFilterButton__doFilter_m160342492 (CameraZoomFilterButton_t1406660070 * __this, Quaternion_t1553702882  ___last0, Quaternion_t1553702882  ___nextValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
