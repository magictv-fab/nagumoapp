﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTVWindowPromptControllerScript
struct MagicTVWindowPromptControllerScript_t1472770442;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MagicTVWindowPromptControllerScript::.ctor()
extern "C"  void MagicTVWindowPromptControllerScript__ctor_m3888164129 (MagicTVWindowPromptControllerScript_t1472770442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowPromptControllerScript::ClickOK()
extern "C"  void MagicTVWindowPromptControllerScript_ClickOK_m1881308963 (MagicTVWindowPromptControllerScript_t1472770442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowPromptControllerScript::SetInputPromptText(System.String)
extern "C"  void MagicTVWindowPromptControllerScript_SetInputPromptText_m2304931558 (MagicTVWindowPromptControllerScript_t1472770442 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTVWindowPromptControllerScript::GetPromptString()
extern "C"  String_t* MagicTVWindowPromptControllerScript_GetPromptString_m3111421165 (MagicTVWindowPromptControllerScript_t1472770442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowPromptControllerScript::RaiseClickOk()
extern "C"  void MagicTVWindowPromptControllerScript_RaiseClickOk_m562533371 (MagicTVWindowPromptControllerScript_t1472770442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
