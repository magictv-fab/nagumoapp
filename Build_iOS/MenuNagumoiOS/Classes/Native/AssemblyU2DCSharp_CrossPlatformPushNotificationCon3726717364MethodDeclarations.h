﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CrossPlatformPushNotificationController
struct CrossPlatformPushNotificationController_t3726717364;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t696267445;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void CrossPlatformPushNotificationController::.ctor()
extern "C"  void CrossPlatformPushNotificationController__ctor_m1927088183 (CrossPlatformPushNotificationController_t3726717364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CrossPlatformPushNotificationController::Start()
extern "C"  void CrossPlatformPushNotificationController_Start_m874225975 (CrossPlatformPushNotificationController_t3726717364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CrossPlatformPushNotificationController::HandleNotification(System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Boolean)
extern "C"  void CrossPlatformPushNotificationController_HandleNotification_m878618330 (Il2CppObject * __this /* static, unused */, String_t* ___message0, Dictionary_2_t696267445 * ___additionalData1, bool ___isActive2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
