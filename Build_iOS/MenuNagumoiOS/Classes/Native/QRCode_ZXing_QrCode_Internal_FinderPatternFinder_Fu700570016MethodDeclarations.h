﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.QrCode.Internal.FinderPatternFinder/FurthestFromAverageComparator
struct FurthestFromAverageComparator_t700570016;
// ZXing.QrCode.Internal.FinderPattern
struct FinderPattern_t4119758992;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_QrCode_Internal_FinderPattern4119758992.h"

// System.Void ZXing.QrCode.Internal.FinderPatternFinder/FurthestFromAverageComparator::.ctor(System.Single)
extern "C"  void FurthestFromAverageComparator__ctor_m3932425824 (FurthestFromAverageComparator_t700570016 * __this, float ___f0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.QrCode.Internal.FinderPatternFinder/FurthestFromAverageComparator::Compare(ZXing.QrCode.Internal.FinderPattern,ZXing.QrCode.Internal.FinderPattern)
extern "C"  int32_t FurthestFromAverageComparator_Compare_m2145063236 (FurthestFromAverageComparator_t700570016 * __this, FinderPattern_t4119758992 * ___x0, FinderPattern_t4119758992 * ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
