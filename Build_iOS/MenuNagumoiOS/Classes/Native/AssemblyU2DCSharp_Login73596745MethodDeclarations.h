﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Login
struct Login_t73596745;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void Login::.ctor()
extern "C"  void Login__ctor_m3262311490 (Login_t73596745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login::.cctor()
extern "C"  void Login__cctor_m1865312171 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login::Awake()
extern "C"  void Login_Awake_m3499916709 (Login_t73596745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login::Start()
extern "C"  void Login_Start_m2209449282 (Login_t73596745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login::OnApplicationQuit()
extern "C"  void Login_OnApplicationQuit_m3695249088 (Login_t73596745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login::Update()
extern "C"  void Login_Update_m4074270475 (Login_t73596745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login::ButtonCupons()
extern "C"  void Login_ButtonCupons_m3608882954 (Login_t73596745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login::ButtonContas()
extern "C"  void Login_ButtonContas_m2525800184 (Login_t73596745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login::BtnEsqueceuSenha()
extern "C"  void Login_BtnEsqueceuSenha_m1217448475 (Login_t73596745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login::LoginButon()
extern "C"  void Login_LoginButon_m1702442105 (Login_t73596745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login::EsqueceuSenhaButon()
extern "C"  void Login_EsqueceuSenhaButon_m2842741581 (Login_t73596745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Login::ISendJsonEsqueceuSenha()
extern "C"  Il2CppObject * Login_ISendJsonEsqueceuSenha_m1003561654 (Login_t73596745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Login::ILogin()
extern "C"  Il2CppObject * Login_ILogin_m1269638586 (Login_t73596745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Login::ILoadImgProfile(System.String)
extern "C"  Il2CppObject * Login_ILoadImgProfile_m3516741573 (Login_t73596745 * __this, String_t* ___photoName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Login::IEsqueceuSenha()
extern "C"  Il2CppObject * Login_IEsqueceuSenha_m3496860006 (Login_t73596745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login::PopUp(System.String)
extern "C"  void Login_PopUp_m789277526 (Login_t73596745 * __this, String_t* ___txt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login::ButtonLoadGame()
extern "C"  void Login_ButtonLoadGame_m4203045964 (Login_t73596745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login::Close()
extern "C"  void Login_Close_m678203736 (Login_t73596745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Login::GoLevel()
extern "C"  Il2CppObject * Login_GoLevel_m4144505956 (Login_t73596745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Login::Md5Sum(System.String)
extern "C"  String_t* Login_Md5Sum_m1046279638 (Login_t73596745 * __this, String_t* ___strToEncrypt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Login::Base64Encode(System.String)
extern "C"  String_t* Login_Base64Encode_m2655377662 (Il2CppObject * __this /* static, unused */, String_t* ___plainText0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
