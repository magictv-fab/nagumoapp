﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.in_apps.controllers.ARM3DSimpleController
struct ARM3DSimpleController_t1586749877;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.in_apps.controllers.ARM3DSimpleController::.ctor()
extern "C"  void ARM3DSimpleController__ctor_m2871699176 (ARM3DSimpleController_t1586749877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.controllers.ARM3DSimpleController::Init()
extern "C"  void ARM3DSimpleController_Init_m1983834732 (ARM3DSimpleController_t1586749877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.controllers.ARM3DSimpleController::Play()
extern "C"  void ARM3DSimpleController_Play_m2182158224 (ARM3DSimpleController_t1586749877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.controllers.ARM3DSimpleController::Pause()
extern "C"  void ARM3DSimpleController_Pause_m2925825148 (ARM3DSimpleController_t1586749877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
