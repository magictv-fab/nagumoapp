﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BackgroudCam
struct BackgroudCam_t4120779363;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void BackgroudCam::.ctor()
extern "C"  void BackgroudCam__ctor_m4056799128 (BackgroudCam_t4120779363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BackgroudCam::Start()
extern "C"  Il2CppObject * BackgroudCam_Start_m2348232528 (BackgroudCam_t4120779363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BackgroudCam::openCamera()
extern "C"  Il2CppObject * BackgroudCam_openCamera_m1972954339 (BackgroudCam_t4120779363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BackgroudCam::BackButton()
extern "C"  void BackgroudCam_BackButton_m3881831205 (BackgroudCam_t4120779363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BackgroudCam::Close()
extern "C"  void BackgroudCam_Close_m1472691374 (BackgroudCam_t4120779363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BackgroudCam::OnDisable()
extern "C"  void BackgroudCam_OnDisable_m795844607 (BackgroudCam_t4120779363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BackgroudCam::OnDestroy()
extern "C"  void BackgroudCam_OnDestroy_m2973376657 (BackgroudCam_t4120779363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BackgroudCam::GoLevel()
extern "C"  Il2CppObject * BackgroudCam_GoLevel_m57971882 (BackgroudCam_t4120779363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BackgroudCam::fakeScreenShot()
extern "C"  void BackgroudCam_fakeScreenShot_m3039684807 (BackgroudCam_t4120779363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
