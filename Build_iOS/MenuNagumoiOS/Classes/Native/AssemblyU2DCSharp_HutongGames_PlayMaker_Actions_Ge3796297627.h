﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmVar[]
struct FsmVarU5BU5D_t3498949300;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// PlayMakerFSM
struct PlayMakerFSM_t3799847376;
// HutongGames.PlayMaker.INamedVariable[]
struct INamedVariableU5BU5D_t2748317531;
// HutongGames.PlayMaker.NamedVariable[]
struct NamedVariableU5BU5D_t2180779430;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetFsmVariables
struct  GetFsmVariables_t3796297627  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetFsmVariables::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmVariables::fsmName
	FsmString_t952858651 * ___fsmName_10;
	// HutongGames.PlayMaker.FsmVar[] HutongGames.PlayMaker.Actions.GetFsmVariables::getVariables
	FsmVarU5BU5D_t3498949300* ___getVariables_11;
	// System.Boolean HutongGames.PlayMaker.Actions.GetFsmVariables::everyFrame
	bool ___everyFrame_12;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.GetFsmVariables::cachedGO
	GameObject_t3674682005 * ___cachedGO_13;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.GetFsmVariables::sourceFsm
	PlayMakerFSM_t3799847376 * ___sourceFsm_14;
	// HutongGames.PlayMaker.INamedVariable[] HutongGames.PlayMaker.Actions.GetFsmVariables::sourceVariables
	INamedVariableU5BU5D_t2748317531* ___sourceVariables_15;
	// HutongGames.PlayMaker.NamedVariable[] HutongGames.PlayMaker.Actions.GetFsmVariables::targetVariables
	NamedVariableU5BU5D_t2180779430* ___targetVariables_16;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(GetFsmVariables_t3796297627, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_fsmName_10() { return static_cast<int32_t>(offsetof(GetFsmVariables_t3796297627, ___fsmName_10)); }
	inline FsmString_t952858651 * get_fsmName_10() const { return ___fsmName_10; }
	inline FsmString_t952858651 ** get_address_of_fsmName_10() { return &___fsmName_10; }
	inline void set_fsmName_10(FsmString_t952858651 * value)
	{
		___fsmName_10 = value;
		Il2CppCodeGenWriteBarrier(&___fsmName_10, value);
	}

	inline static int32_t get_offset_of_getVariables_11() { return static_cast<int32_t>(offsetof(GetFsmVariables_t3796297627, ___getVariables_11)); }
	inline FsmVarU5BU5D_t3498949300* get_getVariables_11() const { return ___getVariables_11; }
	inline FsmVarU5BU5D_t3498949300** get_address_of_getVariables_11() { return &___getVariables_11; }
	inline void set_getVariables_11(FsmVarU5BU5D_t3498949300* value)
	{
		___getVariables_11 = value;
		Il2CppCodeGenWriteBarrier(&___getVariables_11, value);
	}

	inline static int32_t get_offset_of_everyFrame_12() { return static_cast<int32_t>(offsetof(GetFsmVariables_t3796297627, ___everyFrame_12)); }
	inline bool get_everyFrame_12() const { return ___everyFrame_12; }
	inline bool* get_address_of_everyFrame_12() { return &___everyFrame_12; }
	inline void set_everyFrame_12(bool value)
	{
		___everyFrame_12 = value;
	}

	inline static int32_t get_offset_of_cachedGO_13() { return static_cast<int32_t>(offsetof(GetFsmVariables_t3796297627, ___cachedGO_13)); }
	inline GameObject_t3674682005 * get_cachedGO_13() const { return ___cachedGO_13; }
	inline GameObject_t3674682005 ** get_address_of_cachedGO_13() { return &___cachedGO_13; }
	inline void set_cachedGO_13(GameObject_t3674682005 * value)
	{
		___cachedGO_13 = value;
		Il2CppCodeGenWriteBarrier(&___cachedGO_13, value);
	}

	inline static int32_t get_offset_of_sourceFsm_14() { return static_cast<int32_t>(offsetof(GetFsmVariables_t3796297627, ___sourceFsm_14)); }
	inline PlayMakerFSM_t3799847376 * get_sourceFsm_14() const { return ___sourceFsm_14; }
	inline PlayMakerFSM_t3799847376 ** get_address_of_sourceFsm_14() { return &___sourceFsm_14; }
	inline void set_sourceFsm_14(PlayMakerFSM_t3799847376 * value)
	{
		___sourceFsm_14 = value;
		Il2CppCodeGenWriteBarrier(&___sourceFsm_14, value);
	}

	inline static int32_t get_offset_of_sourceVariables_15() { return static_cast<int32_t>(offsetof(GetFsmVariables_t3796297627, ___sourceVariables_15)); }
	inline INamedVariableU5BU5D_t2748317531* get_sourceVariables_15() const { return ___sourceVariables_15; }
	inline INamedVariableU5BU5D_t2748317531** get_address_of_sourceVariables_15() { return &___sourceVariables_15; }
	inline void set_sourceVariables_15(INamedVariableU5BU5D_t2748317531* value)
	{
		___sourceVariables_15 = value;
		Il2CppCodeGenWriteBarrier(&___sourceVariables_15, value);
	}

	inline static int32_t get_offset_of_targetVariables_16() { return static_cast<int32_t>(offsetof(GetFsmVariables_t3796297627, ___targetVariables_16)); }
	inline NamedVariableU5BU5D_t2180779430* get_targetVariables_16() const { return ___targetVariables_16; }
	inline NamedVariableU5BU5D_t2180779430** get_address_of_targetVariables_16() { return &___targetVariables_16; }
	inline void set_targetVariables_16(NamedVariableU5BU5D_t2180779430* value)
	{
		___targetVariables_16 = value;
		Il2CppCodeGenWriteBarrier(&___targetVariables_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
