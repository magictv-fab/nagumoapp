﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// PlayMakerAnimatorMoveProxy
struct PlayMakerAnimatorMoveProxy_t4175490694;
// UnityEngine.Animator
struct Animator_t2776330603;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight
struct  GetAnimatorGravityWeight_t3107738545  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// System.Boolean HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight::everyFrame
	bool ___everyFrame_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight::gravityWeight
	FsmFloat_t2134102846 * ___gravityWeight_11;
	// PlayMakerAnimatorMoveProxy HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight::_animatorProxy
	PlayMakerAnimatorMoveProxy_t4175490694 * ____animatorProxy_12;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight::_animator
	Animator_t2776330603 * ____animator_13;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(GetAnimatorGravityWeight_t3107738545, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_everyFrame_10() { return static_cast<int32_t>(offsetof(GetAnimatorGravityWeight_t3107738545, ___everyFrame_10)); }
	inline bool get_everyFrame_10() const { return ___everyFrame_10; }
	inline bool* get_address_of_everyFrame_10() { return &___everyFrame_10; }
	inline void set_everyFrame_10(bool value)
	{
		___everyFrame_10 = value;
	}

	inline static int32_t get_offset_of_gravityWeight_11() { return static_cast<int32_t>(offsetof(GetAnimatorGravityWeight_t3107738545, ___gravityWeight_11)); }
	inline FsmFloat_t2134102846 * get_gravityWeight_11() const { return ___gravityWeight_11; }
	inline FsmFloat_t2134102846 ** get_address_of_gravityWeight_11() { return &___gravityWeight_11; }
	inline void set_gravityWeight_11(FsmFloat_t2134102846 * value)
	{
		___gravityWeight_11 = value;
		Il2CppCodeGenWriteBarrier(&___gravityWeight_11, value);
	}

	inline static int32_t get_offset_of__animatorProxy_12() { return static_cast<int32_t>(offsetof(GetAnimatorGravityWeight_t3107738545, ____animatorProxy_12)); }
	inline PlayMakerAnimatorMoveProxy_t4175490694 * get__animatorProxy_12() const { return ____animatorProxy_12; }
	inline PlayMakerAnimatorMoveProxy_t4175490694 ** get_address_of__animatorProxy_12() { return &____animatorProxy_12; }
	inline void set__animatorProxy_12(PlayMakerAnimatorMoveProxy_t4175490694 * value)
	{
		____animatorProxy_12 = value;
		Il2CppCodeGenWriteBarrier(&____animatorProxy_12, value);
	}

	inline static int32_t get_offset_of__animator_13() { return static_cast<int32_t>(offsetof(GetAnimatorGravityWeight_t3107738545, ____animator_13)); }
	inline Animator_t2776330603 * get__animator_13() const { return ____animator_13; }
	inline Animator_t2776330603 ** get_address_of__animator_13() { return &____animator_13; }
	inline void set__animator_13(Animator_t2776330603 * value)
	{
		____animator_13 = value;
		Il2CppCodeGenWriteBarrier(&____animator_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
