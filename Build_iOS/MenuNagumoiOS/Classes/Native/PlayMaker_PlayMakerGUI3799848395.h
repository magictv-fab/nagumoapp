﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<PlayMakerFSM>
struct List_1_t873065632;
// HutongGames.PlayMaker.Fsm
struct Fsm_t1527112426;
// UnityEngine.GUIContent
struct GUIContent_t2094828418;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// PlayMakerGUI
struct PlayMakerGUI_t3799848395;
// UnityEngine.GUISkin
struct GUISkin_t3371348110;
// UnityEngine.GUIStyle
struct GUIStyle_t2990928826;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// UnityEngine.Texture
struct Texture_t2526458961;
// System.Comparison`1<PlayMakerFSM>
struct Comparison_1_t2516208563;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerGUI
struct  PlayMakerGUI_t3799848395  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean PlayMakerGUI::previewOnGUI
	bool ___previewOnGUI_6;
	// System.Boolean PlayMakerGUI::enableGUILayout
	bool ___enableGUILayout_7;
	// System.Boolean PlayMakerGUI::drawStateLabels
	bool ___drawStateLabels_8;
	// System.Boolean PlayMakerGUI::GUITextureStateLabels
	bool ___GUITextureStateLabels_9;
	// System.Boolean PlayMakerGUI::GUITextStateLabels
	bool ___GUITextStateLabels_10;
	// System.Boolean PlayMakerGUI::filterLabelsWithDistance
	bool ___filterLabelsWithDistance_11;
	// System.Single PlayMakerGUI::maxLabelDistance
	float ___maxLabelDistance_12;
	// System.Boolean PlayMakerGUI::controlMouseCursor
	bool ___controlMouseCursor_13;

public:
	inline static int32_t get_offset_of_previewOnGUI_6() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t3799848395, ___previewOnGUI_6)); }
	inline bool get_previewOnGUI_6() const { return ___previewOnGUI_6; }
	inline bool* get_address_of_previewOnGUI_6() { return &___previewOnGUI_6; }
	inline void set_previewOnGUI_6(bool value)
	{
		___previewOnGUI_6 = value;
	}

	inline static int32_t get_offset_of_enableGUILayout_7() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t3799848395, ___enableGUILayout_7)); }
	inline bool get_enableGUILayout_7() const { return ___enableGUILayout_7; }
	inline bool* get_address_of_enableGUILayout_7() { return &___enableGUILayout_7; }
	inline void set_enableGUILayout_7(bool value)
	{
		___enableGUILayout_7 = value;
	}

	inline static int32_t get_offset_of_drawStateLabels_8() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t3799848395, ___drawStateLabels_8)); }
	inline bool get_drawStateLabels_8() const { return ___drawStateLabels_8; }
	inline bool* get_address_of_drawStateLabels_8() { return &___drawStateLabels_8; }
	inline void set_drawStateLabels_8(bool value)
	{
		___drawStateLabels_8 = value;
	}

	inline static int32_t get_offset_of_GUITextureStateLabels_9() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t3799848395, ___GUITextureStateLabels_9)); }
	inline bool get_GUITextureStateLabels_9() const { return ___GUITextureStateLabels_9; }
	inline bool* get_address_of_GUITextureStateLabels_9() { return &___GUITextureStateLabels_9; }
	inline void set_GUITextureStateLabels_9(bool value)
	{
		___GUITextureStateLabels_9 = value;
	}

	inline static int32_t get_offset_of_GUITextStateLabels_10() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t3799848395, ___GUITextStateLabels_10)); }
	inline bool get_GUITextStateLabels_10() const { return ___GUITextStateLabels_10; }
	inline bool* get_address_of_GUITextStateLabels_10() { return &___GUITextStateLabels_10; }
	inline void set_GUITextStateLabels_10(bool value)
	{
		___GUITextStateLabels_10 = value;
	}

	inline static int32_t get_offset_of_filterLabelsWithDistance_11() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t3799848395, ___filterLabelsWithDistance_11)); }
	inline bool get_filterLabelsWithDistance_11() const { return ___filterLabelsWithDistance_11; }
	inline bool* get_address_of_filterLabelsWithDistance_11() { return &___filterLabelsWithDistance_11; }
	inline void set_filterLabelsWithDistance_11(bool value)
	{
		___filterLabelsWithDistance_11 = value;
	}

	inline static int32_t get_offset_of_maxLabelDistance_12() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t3799848395, ___maxLabelDistance_12)); }
	inline float get_maxLabelDistance_12() const { return ___maxLabelDistance_12; }
	inline float* get_address_of_maxLabelDistance_12() { return &___maxLabelDistance_12; }
	inline void set_maxLabelDistance_12(float value)
	{
		___maxLabelDistance_12 = value;
	}

	inline static int32_t get_offset_of_controlMouseCursor_13() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t3799848395, ___controlMouseCursor_13)); }
	inline bool get_controlMouseCursor_13() const { return ___controlMouseCursor_13; }
	inline bool* get_address_of_controlMouseCursor_13() { return &___controlMouseCursor_13; }
	inline void set_controlMouseCursor_13(bool value)
	{
		___controlMouseCursor_13 = value;
	}
};

struct PlayMakerGUI_t3799848395_StaticFields
{
public:
	// System.Collections.Generic.List`1<PlayMakerFSM> PlayMakerGUI::fsmList
	List_1_t873065632 * ___fsmList_3;
	// HutongGames.PlayMaker.Fsm PlayMakerGUI::SelectedFSM
	Fsm_t1527112426 * ___SelectedFSM_4;
	// UnityEngine.GUIContent PlayMakerGUI::labelContent
	GUIContent_t2094828418 * ___labelContent_5;
	// System.Collections.Generic.List`1<PlayMakerFSM> PlayMakerGUI::SortedFsmList
	List_1_t873065632 * ___SortedFsmList_14;
	// UnityEngine.GameObject PlayMakerGUI::labelGameObject
	GameObject_t3674682005 * ___labelGameObject_15;
	// System.Single PlayMakerGUI::fsmLabelIndex
	float ___fsmLabelIndex_16;
	// PlayMakerGUI PlayMakerGUI::instance
	PlayMakerGUI_t3799848395 * ___instance_17;
	// UnityEngine.GUISkin PlayMakerGUI::guiSkin
	GUISkin_t3371348110 * ___guiSkin_18;
	// UnityEngine.Color PlayMakerGUI::guiColor
	Color_t4194546905  ___guiColor_19;
	// UnityEngine.Color PlayMakerGUI::guiBackgroundColor
	Color_t4194546905  ___guiBackgroundColor_20;
	// UnityEngine.Color PlayMakerGUI::guiContentColor
	Color_t4194546905  ___guiContentColor_21;
	// UnityEngine.Matrix4x4 PlayMakerGUI::guiMatrix
	Matrix4x4_t1651859333  ___guiMatrix_22;
	// UnityEngine.GUIStyle PlayMakerGUI::stateLabelStyle
	GUIStyle_t2990928826 * ___stateLabelStyle_23;
	// UnityEngine.Texture2D PlayMakerGUI::stateLabelBackground
	Texture2D_t3884108195 * ___stateLabelBackground_24;
	// UnityEngine.Texture PlayMakerGUI::<MouseCursor>k__BackingField
	Texture_t2526458961 * ___U3CMouseCursorU3Ek__BackingField_25;
	// System.Boolean PlayMakerGUI::<LockCursor>k__BackingField
	bool ___U3CLockCursorU3Ek__BackingField_26;
	// System.Boolean PlayMakerGUI::<HideCursor>k__BackingField
	bool ___U3CHideCursorU3Ek__BackingField_27;
	// System.Comparison`1<PlayMakerFSM> PlayMakerGUI::CS$<>9__CachedAnonymousMethodDelegate2
	Comparison_1_t2516208563 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_28;

public:
	inline static int32_t get_offset_of_fsmList_3() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t3799848395_StaticFields, ___fsmList_3)); }
	inline List_1_t873065632 * get_fsmList_3() const { return ___fsmList_3; }
	inline List_1_t873065632 ** get_address_of_fsmList_3() { return &___fsmList_3; }
	inline void set_fsmList_3(List_1_t873065632 * value)
	{
		___fsmList_3 = value;
		Il2CppCodeGenWriteBarrier(&___fsmList_3, value);
	}

	inline static int32_t get_offset_of_SelectedFSM_4() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t3799848395_StaticFields, ___SelectedFSM_4)); }
	inline Fsm_t1527112426 * get_SelectedFSM_4() const { return ___SelectedFSM_4; }
	inline Fsm_t1527112426 ** get_address_of_SelectedFSM_4() { return &___SelectedFSM_4; }
	inline void set_SelectedFSM_4(Fsm_t1527112426 * value)
	{
		___SelectedFSM_4 = value;
		Il2CppCodeGenWriteBarrier(&___SelectedFSM_4, value);
	}

	inline static int32_t get_offset_of_labelContent_5() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t3799848395_StaticFields, ___labelContent_5)); }
	inline GUIContent_t2094828418 * get_labelContent_5() const { return ___labelContent_5; }
	inline GUIContent_t2094828418 ** get_address_of_labelContent_5() { return &___labelContent_5; }
	inline void set_labelContent_5(GUIContent_t2094828418 * value)
	{
		___labelContent_5 = value;
		Il2CppCodeGenWriteBarrier(&___labelContent_5, value);
	}

	inline static int32_t get_offset_of_SortedFsmList_14() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t3799848395_StaticFields, ___SortedFsmList_14)); }
	inline List_1_t873065632 * get_SortedFsmList_14() const { return ___SortedFsmList_14; }
	inline List_1_t873065632 ** get_address_of_SortedFsmList_14() { return &___SortedFsmList_14; }
	inline void set_SortedFsmList_14(List_1_t873065632 * value)
	{
		___SortedFsmList_14 = value;
		Il2CppCodeGenWriteBarrier(&___SortedFsmList_14, value);
	}

	inline static int32_t get_offset_of_labelGameObject_15() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t3799848395_StaticFields, ___labelGameObject_15)); }
	inline GameObject_t3674682005 * get_labelGameObject_15() const { return ___labelGameObject_15; }
	inline GameObject_t3674682005 ** get_address_of_labelGameObject_15() { return &___labelGameObject_15; }
	inline void set_labelGameObject_15(GameObject_t3674682005 * value)
	{
		___labelGameObject_15 = value;
		Il2CppCodeGenWriteBarrier(&___labelGameObject_15, value);
	}

	inline static int32_t get_offset_of_fsmLabelIndex_16() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t3799848395_StaticFields, ___fsmLabelIndex_16)); }
	inline float get_fsmLabelIndex_16() const { return ___fsmLabelIndex_16; }
	inline float* get_address_of_fsmLabelIndex_16() { return &___fsmLabelIndex_16; }
	inline void set_fsmLabelIndex_16(float value)
	{
		___fsmLabelIndex_16 = value;
	}

	inline static int32_t get_offset_of_instance_17() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t3799848395_StaticFields, ___instance_17)); }
	inline PlayMakerGUI_t3799848395 * get_instance_17() const { return ___instance_17; }
	inline PlayMakerGUI_t3799848395 ** get_address_of_instance_17() { return &___instance_17; }
	inline void set_instance_17(PlayMakerGUI_t3799848395 * value)
	{
		___instance_17 = value;
		Il2CppCodeGenWriteBarrier(&___instance_17, value);
	}

	inline static int32_t get_offset_of_guiSkin_18() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t3799848395_StaticFields, ___guiSkin_18)); }
	inline GUISkin_t3371348110 * get_guiSkin_18() const { return ___guiSkin_18; }
	inline GUISkin_t3371348110 ** get_address_of_guiSkin_18() { return &___guiSkin_18; }
	inline void set_guiSkin_18(GUISkin_t3371348110 * value)
	{
		___guiSkin_18 = value;
		Il2CppCodeGenWriteBarrier(&___guiSkin_18, value);
	}

	inline static int32_t get_offset_of_guiColor_19() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t3799848395_StaticFields, ___guiColor_19)); }
	inline Color_t4194546905  get_guiColor_19() const { return ___guiColor_19; }
	inline Color_t4194546905 * get_address_of_guiColor_19() { return &___guiColor_19; }
	inline void set_guiColor_19(Color_t4194546905  value)
	{
		___guiColor_19 = value;
	}

	inline static int32_t get_offset_of_guiBackgroundColor_20() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t3799848395_StaticFields, ___guiBackgroundColor_20)); }
	inline Color_t4194546905  get_guiBackgroundColor_20() const { return ___guiBackgroundColor_20; }
	inline Color_t4194546905 * get_address_of_guiBackgroundColor_20() { return &___guiBackgroundColor_20; }
	inline void set_guiBackgroundColor_20(Color_t4194546905  value)
	{
		___guiBackgroundColor_20 = value;
	}

	inline static int32_t get_offset_of_guiContentColor_21() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t3799848395_StaticFields, ___guiContentColor_21)); }
	inline Color_t4194546905  get_guiContentColor_21() const { return ___guiContentColor_21; }
	inline Color_t4194546905 * get_address_of_guiContentColor_21() { return &___guiContentColor_21; }
	inline void set_guiContentColor_21(Color_t4194546905  value)
	{
		___guiContentColor_21 = value;
	}

	inline static int32_t get_offset_of_guiMatrix_22() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t3799848395_StaticFields, ___guiMatrix_22)); }
	inline Matrix4x4_t1651859333  get_guiMatrix_22() const { return ___guiMatrix_22; }
	inline Matrix4x4_t1651859333 * get_address_of_guiMatrix_22() { return &___guiMatrix_22; }
	inline void set_guiMatrix_22(Matrix4x4_t1651859333  value)
	{
		___guiMatrix_22 = value;
	}

	inline static int32_t get_offset_of_stateLabelStyle_23() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t3799848395_StaticFields, ___stateLabelStyle_23)); }
	inline GUIStyle_t2990928826 * get_stateLabelStyle_23() const { return ___stateLabelStyle_23; }
	inline GUIStyle_t2990928826 ** get_address_of_stateLabelStyle_23() { return &___stateLabelStyle_23; }
	inline void set_stateLabelStyle_23(GUIStyle_t2990928826 * value)
	{
		___stateLabelStyle_23 = value;
		Il2CppCodeGenWriteBarrier(&___stateLabelStyle_23, value);
	}

	inline static int32_t get_offset_of_stateLabelBackground_24() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t3799848395_StaticFields, ___stateLabelBackground_24)); }
	inline Texture2D_t3884108195 * get_stateLabelBackground_24() const { return ___stateLabelBackground_24; }
	inline Texture2D_t3884108195 ** get_address_of_stateLabelBackground_24() { return &___stateLabelBackground_24; }
	inline void set_stateLabelBackground_24(Texture2D_t3884108195 * value)
	{
		___stateLabelBackground_24 = value;
		Il2CppCodeGenWriteBarrier(&___stateLabelBackground_24, value);
	}

	inline static int32_t get_offset_of_U3CMouseCursorU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t3799848395_StaticFields, ___U3CMouseCursorU3Ek__BackingField_25)); }
	inline Texture_t2526458961 * get_U3CMouseCursorU3Ek__BackingField_25() const { return ___U3CMouseCursorU3Ek__BackingField_25; }
	inline Texture_t2526458961 ** get_address_of_U3CMouseCursorU3Ek__BackingField_25() { return &___U3CMouseCursorU3Ek__BackingField_25; }
	inline void set_U3CMouseCursorU3Ek__BackingField_25(Texture_t2526458961 * value)
	{
		___U3CMouseCursorU3Ek__BackingField_25 = value;
		Il2CppCodeGenWriteBarrier(&___U3CMouseCursorU3Ek__BackingField_25, value);
	}

	inline static int32_t get_offset_of_U3CLockCursorU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t3799848395_StaticFields, ___U3CLockCursorU3Ek__BackingField_26)); }
	inline bool get_U3CLockCursorU3Ek__BackingField_26() const { return ___U3CLockCursorU3Ek__BackingField_26; }
	inline bool* get_address_of_U3CLockCursorU3Ek__BackingField_26() { return &___U3CLockCursorU3Ek__BackingField_26; }
	inline void set_U3CLockCursorU3Ek__BackingField_26(bool value)
	{
		___U3CLockCursorU3Ek__BackingField_26 = value;
	}

	inline static int32_t get_offset_of_U3CHideCursorU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t3799848395_StaticFields, ___U3CHideCursorU3Ek__BackingField_27)); }
	inline bool get_U3CHideCursorU3Ek__BackingField_27() const { return ___U3CHideCursorU3Ek__BackingField_27; }
	inline bool* get_address_of_U3CHideCursorU3Ek__BackingField_27() { return &___U3CHideCursorU3Ek__BackingField_27; }
	inline void set_U3CHideCursorU3Ek__BackingField_27(bool value)
	{
		___U3CHideCursorU3Ek__BackingField_27 = value;
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_28() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t3799848395_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_28)); }
	inline Comparison_1_t2516208563 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_28() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_28; }
	inline Comparison_1_t2516208563 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_28() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_28; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_28(Comparison_1_t2516208563 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_28 = value;
		Il2CppCodeGenWriteBarrier(&___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_28, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
