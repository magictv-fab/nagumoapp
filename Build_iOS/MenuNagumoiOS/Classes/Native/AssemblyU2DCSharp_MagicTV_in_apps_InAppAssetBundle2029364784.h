﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PlayMakerFSM
struct PlayMakerFSM_t3799847376;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AssetBundle>
struct Dictionary_2_t2891378058;
// ARM.utils.request.ARMSingleRequest
struct ARMSingleRequest_t410726329;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// ARM.components.GroupInAppAbstractComponentsManager
struct GroupInAppAbstractComponentsManager_t739334794;
// MagicTV.abstracts.InAppAbstract[]
struct InAppAbstractU5BU5D_t412350073;

#include "AssemblyU2DCSharp_InApp_InAppActorAbstract1901198337.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.in_apps.InAppAssetBundlePresentation
struct  InAppAssetBundlePresentation_t2029364784  : public InAppActorAbstract_t1901198337
{
public:
	// PlayMakerFSM MagicTV.in_apps.InAppAssetBundlePresentation::_FSM
	PlayMakerFSM_t3799847376 * ____FSM_21;
	// ARM.utils.request.ARMSingleRequest MagicTV.in_apps.InAppAssetBundlePresentation::_requestModel3D
	ARMSingleRequest_t410726329 * ____requestModel3D_23;
	// System.String MagicTV.in_apps.InAppAssetBundlePresentation::_localFileSystemAssetPath
	String_t* ____localFileSystemAssetPath_24;
	// System.String MagicTV.in_apps.InAppAssetBundlePresentation::_externalLink
	String_t* ____externalLink_25;
	// System.Int32 MagicTV.in_apps.InAppAssetBundlePresentation::_delayToOpenLink
	int32_t ____delayToOpenLink_26;
	// System.Boolean MagicTV.in_apps.InAppAssetBundlePresentation::_doPrepareAlreadyCalled
	bool ____doPrepareAlreadyCalled_27;
	// UnityEngine.GameObject MagicTV.in_apps.InAppAssetBundlePresentation::_loadedModel3d
	GameObject_t3674682005 * ____loadedModel3d_28;
	// System.Boolean MagicTV.in_apps.InAppAssetBundlePresentation::_FSMIsReady
	bool ____FSMIsReady_29;
	// System.Boolean MagicTV.in_apps.InAppAssetBundlePresentation::_hasInternalApps
	bool ____hasInternalApps_30;
	// System.Boolean MagicTV.in_apps.InAppAssetBundlePresentation::_InternalAppsAreReady
	bool ____InternalAppsAreReady_31;
	// System.Collections.Generic.List`1<System.Int32> MagicTV.in_apps.InAppAssetBundlePresentation::_clickGOInstanceID
	List_1_t2522024052 * ____clickGOInstanceID_32;
	// System.Boolean MagicTV.in_apps.InAppAssetBundlePresentation::_hasClick
	bool ____hasClick_33;
	// System.Boolean MagicTV.in_apps.InAppAssetBundlePresentation::_clickLocked
	bool ____clickLocked_34;
	// ARM.components.GroupInAppAbstractComponentsManager MagicTV.in_apps.InAppAssetBundlePresentation::subInternalInApps
	GroupInAppAbstractComponentsManager_t739334794 * ___subInternalInApps_35;
	// MagicTV.abstracts.InAppAbstract[] MagicTV.in_apps.InAppAssetBundlePresentation::DebubInternalInApps
	InAppAbstractU5BU5D_t412350073* ___DebubInternalInApps_36;

public:
	inline static int32_t get_offset_of__FSM_21() { return static_cast<int32_t>(offsetof(InAppAssetBundlePresentation_t2029364784, ____FSM_21)); }
	inline PlayMakerFSM_t3799847376 * get__FSM_21() const { return ____FSM_21; }
	inline PlayMakerFSM_t3799847376 ** get_address_of__FSM_21() { return &____FSM_21; }
	inline void set__FSM_21(PlayMakerFSM_t3799847376 * value)
	{
		____FSM_21 = value;
		Il2CppCodeGenWriteBarrier(&____FSM_21, value);
	}

	inline static int32_t get_offset_of__requestModel3D_23() { return static_cast<int32_t>(offsetof(InAppAssetBundlePresentation_t2029364784, ____requestModel3D_23)); }
	inline ARMSingleRequest_t410726329 * get__requestModel3D_23() const { return ____requestModel3D_23; }
	inline ARMSingleRequest_t410726329 ** get_address_of__requestModel3D_23() { return &____requestModel3D_23; }
	inline void set__requestModel3D_23(ARMSingleRequest_t410726329 * value)
	{
		____requestModel3D_23 = value;
		Il2CppCodeGenWriteBarrier(&____requestModel3D_23, value);
	}

	inline static int32_t get_offset_of__localFileSystemAssetPath_24() { return static_cast<int32_t>(offsetof(InAppAssetBundlePresentation_t2029364784, ____localFileSystemAssetPath_24)); }
	inline String_t* get__localFileSystemAssetPath_24() const { return ____localFileSystemAssetPath_24; }
	inline String_t** get_address_of__localFileSystemAssetPath_24() { return &____localFileSystemAssetPath_24; }
	inline void set__localFileSystemAssetPath_24(String_t* value)
	{
		____localFileSystemAssetPath_24 = value;
		Il2CppCodeGenWriteBarrier(&____localFileSystemAssetPath_24, value);
	}

	inline static int32_t get_offset_of__externalLink_25() { return static_cast<int32_t>(offsetof(InAppAssetBundlePresentation_t2029364784, ____externalLink_25)); }
	inline String_t* get__externalLink_25() const { return ____externalLink_25; }
	inline String_t** get_address_of__externalLink_25() { return &____externalLink_25; }
	inline void set__externalLink_25(String_t* value)
	{
		____externalLink_25 = value;
		Il2CppCodeGenWriteBarrier(&____externalLink_25, value);
	}

	inline static int32_t get_offset_of__delayToOpenLink_26() { return static_cast<int32_t>(offsetof(InAppAssetBundlePresentation_t2029364784, ____delayToOpenLink_26)); }
	inline int32_t get__delayToOpenLink_26() const { return ____delayToOpenLink_26; }
	inline int32_t* get_address_of__delayToOpenLink_26() { return &____delayToOpenLink_26; }
	inline void set__delayToOpenLink_26(int32_t value)
	{
		____delayToOpenLink_26 = value;
	}

	inline static int32_t get_offset_of__doPrepareAlreadyCalled_27() { return static_cast<int32_t>(offsetof(InAppAssetBundlePresentation_t2029364784, ____doPrepareAlreadyCalled_27)); }
	inline bool get__doPrepareAlreadyCalled_27() const { return ____doPrepareAlreadyCalled_27; }
	inline bool* get_address_of__doPrepareAlreadyCalled_27() { return &____doPrepareAlreadyCalled_27; }
	inline void set__doPrepareAlreadyCalled_27(bool value)
	{
		____doPrepareAlreadyCalled_27 = value;
	}

	inline static int32_t get_offset_of__loadedModel3d_28() { return static_cast<int32_t>(offsetof(InAppAssetBundlePresentation_t2029364784, ____loadedModel3d_28)); }
	inline GameObject_t3674682005 * get__loadedModel3d_28() const { return ____loadedModel3d_28; }
	inline GameObject_t3674682005 ** get_address_of__loadedModel3d_28() { return &____loadedModel3d_28; }
	inline void set__loadedModel3d_28(GameObject_t3674682005 * value)
	{
		____loadedModel3d_28 = value;
		Il2CppCodeGenWriteBarrier(&____loadedModel3d_28, value);
	}

	inline static int32_t get_offset_of__FSMIsReady_29() { return static_cast<int32_t>(offsetof(InAppAssetBundlePresentation_t2029364784, ____FSMIsReady_29)); }
	inline bool get__FSMIsReady_29() const { return ____FSMIsReady_29; }
	inline bool* get_address_of__FSMIsReady_29() { return &____FSMIsReady_29; }
	inline void set__FSMIsReady_29(bool value)
	{
		____FSMIsReady_29 = value;
	}

	inline static int32_t get_offset_of__hasInternalApps_30() { return static_cast<int32_t>(offsetof(InAppAssetBundlePresentation_t2029364784, ____hasInternalApps_30)); }
	inline bool get__hasInternalApps_30() const { return ____hasInternalApps_30; }
	inline bool* get_address_of__hasInternalApps_30() { return &____hasInternalApps_30; }
	inline void set__hasInternalApps_30(bool value)
	{
		____hasInternalApps_30 = value;
	}

	inline static int32_t get_offset_of__InternalAppsAreReady_31() { return static_cast<int32_t>(offsetof(InAppAssetBundlePresentation_t2029364784, ____InternalAppsAreReady_31)); }
	inline bool get__InternalAppsAreReady_31() const { return ____InternalAppsAreReady_31; }
	inline bool* get_address_of__InternalAppsAreReady_31() { return &____InternalAppsAreReady_31; }
	inline void set__InternalAppsAreReady_31(bool value)
	{
		____InternalAppsAreReady_31 = value;
	}

	inline static int32_t get_offset_of__clickGOInstanceID_32() { return static_cast<int32_t>(offsetof(InAppAssetBundlePresentation_t2029364784, ____clickGOInstanceID_32)); }
	inline List_1_t2522024052 * get__clickGOInstanceID_32() const { return ____clickGOInstanceID_32; }
	inline List_1_t2522024052 ** get_address_of__clickGOInstanceID_32() { return &____clickGOInstanceID_32; }
	inline void set__clickGOInstanceID_32(List_1_t2522024052 * value)
	{
		____clickGOInstanceID_32 = value;
		Il2CppCodeGenWriteBarrier(&____clickGOInstanceID_32, value);
	}

	inline static int32_t get_offset_of__hasClick_33() { return static_cast<int32_t>(offsetof(InAppAssetBundlePresentation_t2029364784, ____hasClick_33)); }
	inline bool get__hasClick_33() const { return ____hasClick_33; }
	inline bool* get_address_of__hasClick_33() { return &____hasClick_33; }
	inline void set__hasClick_33(bool value)
	{
		____hasClick_33 = value;
	}

	inline static int32_t get_offset_of__clickLocked_34() { return static_cast<int32_t>(offsetof(InAppAssetBundlePresentation_t2029364784, ____clickLocked_34)); }
	inline bool get__clickLocked_34() const { return ____clickLocked_34; }
	inline bool* get_address_of__clickLocked_34() { return &____clickLocked_34; }
	inline void set__clickLocked_34(bool value)
	{
		____clickLocked_34 = value;
	}

	inline static int32_t get_offset_of_subInternalInApps_35() { return static_cast<int32_t>(offsetof(InAppAssetBundlePresentation_t2029364784, ___subInternalInApps_35)); }
	inline GroupInAppAbstractComponentsManager_t739334794 * get_subInternalInApps_35() const { return ___subInternalInApps_35; }
	inline GroupInAppAbstractComponentsManager_t739334794 ** get_address_of_subInternalInApps_35() { return &___subInternalInApps_35; }
	inline void set_subInternalInApps_35(GroupInAppAbstractComponentsManager_t739334794 * value)
	{
		___subInternalInApps_35 = value;
		Il2CppCodeGenWriteBarrier(&___subInternalInApps_35, value);
	}

	inline static int32_t get_offset_of_DebubInternalInApps_36() { return static_cast<int32_t>(offsetof(InAppAssetBundlePresentation_t2029364784, ___DebubInternalInApps_36)); }
	inline InAppAbstractU5BU5D_t412350073* get_DebubInternalInApps_36() const { return ___DebubInternalInApps_36; }
	inline InAppAbstractU5BU5D_t412350073** get_address_of_DebubInternalInApps_36() { return &___DebubInternalInApps_36; }
	inline void set_DebubInternalInApps_36(InAppAbstractU5BU5D_t412350073* value)
	{
		___DebubInternalInApps_36 = value;
		Il2CppCodeGenWriteBarrier(&___DebubInternalInApps_36, value);
	}
};

struct InAppAssetBundlePresentation_t2029364784_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AssetBundle> MagicTV.in_apps.InAppAssetBundlePresentation::_AssetBundleLib
	Dictionary_2_t2891378058 * ____AssetBundleLib_22;

public:
	inline static int32_t get_offset_of__AssetBundleLib_22() { return static_cast<int32_t>(offsetof(InAppAssetBundlePresentation_t2029364784_StaticFields, ____AssetBundleLib_22)); }
	inline Dictionary_2_t2891378058 * get__AssetBundleLib_22() const { return ____AssetBundleLib_22; }
	inline Dictionary_2_t2891378058 ** get_address_of__AssetBundleLib_22() { return &____AssetBundleLib_22; }
	inline void set__AssetBundleLib_22(Dictionary_2_t2891378058 * value)
	{
		____AssetBundleLib_22 = value;
		Il2CppCodeGenWriteBarrier(&____AssetBundleLib_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
