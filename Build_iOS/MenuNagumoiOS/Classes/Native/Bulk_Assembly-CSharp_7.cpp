﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// HutongGames.PlayMaker.Actions.GUILayoutBeginArea
struct GUILayoutBeginArea_t3761865337;
// HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject
struct GUILayoutBeginAreaFollowObject_t439899113;
// HutongGames.PlayMaker.Actions.GUILayoutBeginCentered
struct GUILayoutBeginCentered_t363266336;
// HutongGames.PlayMaker.Actions.GUILayoutBeginHorizontal
struct GUILayoutBeginHorizontal_t2139859280;
// HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView
struct GUILayoutBeginScrollView_t818578494;
// HutongGames.PlayMaker.Actions.GUILayoutBeginVertical
struct GUILayoutBeginVertical_t3526512866;
// HutongGames.PlayMaker.Actions.GUILayoutBox
struct GUILayoutBox_t740407662;
// HutongGames.PlayMaker.Actions.GUILayoutButton
struct GUILayoutButton_t2660680933;
// HutongGames.PlayMaker.Actions.GUILayoutConfirmPasswordField
struct GUILayoutConfirmPasswordField_t1819635698;
// HutongGames.PlayMaker.Actions.GUILayoutEmailField
struct GUILayoutEmailField_t215605361;
// HutongGames.PlayMaker.Actions.GUILayoutEndArea
struct GUILayoutEndArea_t2673584043;
// HutongGames.PlayMaker.Actions.GUILayoutEndCentered
struct GUILayoutEndCentered_t2146385234;
// HutongGames.PlayMaker.Actions.GUILayoutEndHorizontal
struct GUILayoutEndHorizontal_t2025169154;
// HutongGames.PlayMaker.Actions.GUILayoutEndScrollView
struct GUILayoutEndScrollView_t703888368;
// HutongGames.PlayMaker.Actions.GUILayoutEndVertical
struct GUILayoutEndVertical_t1014664468;
// HutongGames.PlayMaker.Actions.GUILayoutFlexibleSpace
struct GUILayoutFlexibleSpace_t1009047966;
// HutongGames.PlayMaker.Actions.GUILayoutFloatField
struct GUILayoutFloatField_t2175025585;
// HutongGames.PlayMaker.Actions.GUILayoutFloatLabel
struct GUILayoutFloatLabel_t2180325291;
// HutongGames.PlayMaker.Actions.GUILayoutHorizontalSlider
struct GUILayoutHorizontalSlider_t3080788440;
// HutongGames.PlayMaker.Actions.GUILayoutIntField
struct GUILayoutIntField_t3766456830;
// HutongGames.PlayMaker.Actions.GUILayoutIntLabel
struct GUILayoutIntLabel_t3771756536;
// HutongGames.PlayMaker.Actions.GUILayoutLabel
struct GUILayoutLabel_t3579067543;
// HutongGames.PlayMaker.Actions.GUILayoutPasswordField
struct GUILayoutPasswordField_t886514882;
// HutongGames.PlayMaker.Actions.GUILayoutRepeatButton
struct GUILayoutRepeatButton_t3980277856;
// HutongGames.PlayMaker.Actions.GUILayoutSpace
struct GUILayoutSpace_t3585978025;
// HutongGames.PlayMaker.Actions.GUILayoutTextField
struct GUILayoutTextField_t3774275952;
// HutongGames.PlayMaker.Actions.GUILayoutTextLabel
struct GUILayoutTextLabel_t3779575658;
// HutongGames.PlayMaker.Actions.GUILayoutToggle
struct GUILayoutToggle_t3170064647;
// HutongGames.PlayMaker.Actions.GUILayoutToolbar
struct GUILayoutToolbar_t3141290782;
// UnityEngine.GUIContent[]
struct GUIContentU5BU5D_t3588725815;
// System.String
struct String_t;
// HutongGames.PlayMaker.Actions.GUILayoutVerticalSlider
struct GUILayoutVerticalSlider_t1513124266;
// HutongGames.PlayMaker.Actions.GUITooltip
struct GUITooltip_t3898253232;
// HutongGames.PlayMaker.Actions.GUIVerticalSlider
struct GUIVerticalSlider_t4042007136;
// HutongGames.PlayMaker.Actions.HasComponent
struct HasComponent_t1091840427;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// HutongGames.PlayMaker.Actions.IntAdd
struct IntAdd_t3052069402;
// HutongGames.PlayMaker.Actions.IntChanged
struct IntChanged_t778990893;
// HutongGames.PlayMaker.Actions.IntClamp
struct IntClamp_t286226100;
// HutongGames.PlayMaker.Actions.IntCompare
struct IntCompare_t990531422;
// HutongGames.PlayMaker.Actions.IntOperator
struct IntOperator_t91950145;
// HutongGames.PlayMaker.Actions.IntSwitch
struct IntSwitch_t102584145;
// HutongGames.PlayMaker.Actions.InverseTransformDirection
struct InverseTransformDirection_t4174732817;
// HutongGames.PlayMaker.Actions.InverseTransformPoint
struct InverseTransformPoint_t1066006434;
// HutongGames.PlayMaker.Actions.InvokeMethod
struct InvokeMethod_t2919758241;
// HutongGames.PlayMaker.Actions.IsKinematic
struct IsKinematic_t784342153;
// HutongGames.PlayMaker.Actions.IsSleeping
struct IsSleeping_t599267933;
// HutongGames.PlayMaker.Actions.iTweenFsmAction
struct iTweenFsmAction_t410382178;
// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// iTweenFSMEvents
struct iTweenFSMEvents_t871409943;
// System.Object
struct Il2CppObject;
// HutongGames.PlayMaker.Actions.iTweenLookFrom
struct iTweenLookFrom_t3433969491;
// HutongGames.PlayMaker.Actions.iTweenLookTo
struct iTweenLookTo_t440825252;
// HutongGames.PlayMaker.Actions.iTweenLookUpdate
struct iTweenLookUpdate_t2645160178;
// HutongGames.PlayMaker.Actions.iTweenMoveAdd
struct iTweenMoveAdd_t1025491740;
// HutongGames.PlayMaker.Actions.iTweenMoveBy
struct iTweenMoveBy_t469656626;
// HutongGames.PlayMaker.Actions.iTweenMoveFrom
struct iTweenMoveFrom_t1076675461;
// HutongGames.PlayMaker.Actions.iTweenMoveTo
struct iTweenMoveTo_t469657174;
// HutongGames.PlayMaker.Actions.iTweenMoveUpdate
struct iTweenMoveUpdate_t733362340;
// HutongGames.PlayMaker.Actions.iTweenPause
struct iTweenPause_t1146816066;
// HutongGames.PlayMaker.Actions.iTweenPunchPosition
struct iTweenPunchPosition_t2465480643;
// HutongGames.PlayMaker.Actions.iTweenPunchRotation
struct iTweenPunchRotation_t1677375000;
// HutongGames.PlayMaker.Actions.iTweenPunchScale
struct iTweenPunchScale_t4163517798;
// HutongGames.PlayMaker.Actions.iTweenResume
struct iTweenResume_t603494487;
// HutongGames.PlayMaker.Actions.iTweenRotateAdd
struct iTweenRotateAdd_t368396562;
// HutongGames.PlayMaker.Actions.iTweenRotateBy
struct iTweenRotateBy_t1141196668;
// HutongGames.PlayMaker.Actions.iTweenRotateFrom
struct iTweenRotateFrom_t2181561423;
// HutongGames.PlayMaker.Actions.iTweenRotateTo
struct iTweenRotateTo_t1141197216;
// HutongGames.PlayMaker.Actions.iTweenRotateUpdate
struct iTweenRotateUpdate_t1671849710;
// HutongGames.PlayMaker.Actions.iTweenScaleAdd
struct iTweenScaleAdd_t289621857;
// HutongGames.PlayMaker.Actions.iTweenScaleBy
struct iTweenScaleBy_t1692844877;
// HutongGames.PlayMaker.Actions.iTweenScaleFrom
struct iTweenScaleFrom_t4034512864;
// HutongGames.PlayMaker.Actions.iTweenScaleTo
struct iTweenScaleTo_t1692845425;
// HutongGames.PlayMaker.Actions.iTweenScaleUpdate
struct iTweenScaleUpdate_t4241723967;
// HutongGames.PlayMaker.Actions.iTweenShakePosition
struct iTweenShakePosition_t1348015515;
// HutongGames.PlayMaker.Actions.iTweenShakeRotation
struct iTweenShakeRotation_t559909872;
// HutongGames.PlayMaker.Actions.iTweenShakeScale
struct iTweenShakeScale_t2838558350;
// HutongGames.PlayMaker.Actions.iTweenStop
struct iTweenStop_t3798813676;
// HutongGames.PlayMaker.Actions.KillDelayedEvents
struct KillDelayedEvents_t3079223979;
// HutongGames.PlayMaker.Actions.LoadLevel
struct LoadLevel_t1482164620;
// HutongGames.PlayMaker.Actions.LoadLevelNum
struct LoadLevelNum_t2730020496;
// HutongGames.PlayMaker.Actions.LookAt
struct LookAt_t3138770714;
// HutongGames.PlayMaker.Actions.MasterServerClearHostList
struct MasterServerClearHostList_t3350951132;
// HutongGames.PlayMaker.Actions.MasterServerGetHostCount
struct MasterServerGetHostCount_t240962846;
// HutongGames.PlayMaker.Actions.MasterServerGetHostData
struct MasterServerGetHostData_t2799670129;
// HutongGames.PlayMaker.Actions.MasterServerGetNextHostData
struct MasterServerGetNextHostData_t2113983876;
// HutongGames.PlayMaker.Actions.MasterServerGetProperties
struct MasterServerGetProperties_t3166189106;
// HutongGames.PlayMaker.Actions.MasterServerRegisterHost
struct MasterServerRegisterHost_t3580704376;
// HutongGames.PlayMaker.Actions.MasterServerRequestHostList
struct MasterServerRequestHostList_t4119465886;
// HutongGames.PlayMaker.Actions.MasterServerSetProperties
struct MasterServerSetProperties_t3940320318;
// HutongGames.PlayMaker.Actions.MasterServerUnregisterHost
struct MasterServerUnregisterHost_t3653261073;
// HutongGames.PlayMaker.Actions.MouseLook
struct MouseLook_t696688690;
// UnityEngine.Rigidbody
struct Rigidbody_t3346577219;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.Actions.MouseLook2
struct MouseLook2_t3768519990;
// HutongGames.PlayMaker.Actions.MousePick
struct MousePick_t696801716;
// HutongGames.PlayMaker.Actions.MousePickEvent
struct MousePickEvent_t909730492;
// HutongGames.PlayMaker.Actions.MoveTowards
struct MoveTowards_t2875353433;
// HutongGames.PlayMaker.Actions.NavMeshAgentAnimatorSynchronizer
struct NavMeshAgentAnimatorSynchronizer_t2695016998;
// UnityEngine.NavMeshAgent
struct NavMeshAgent_t588466745;
// UnityEngine.Animator
struct Animator_t2776330603;
// PlayMakerAnimatorMoveProxy
struct PlayMakerAnimatorMoveProxy_t4175490694;
// HutongGames.PlayMaker.Actions.NetworkCloseConnection
struct NetworkCloseConnection_t686438608;
// HutongGames.PlayMaker.Actions.NetworkConnect
struct NetworkConnect_t2329652676;
// HutongGames.PlayMaker.Actions.NetworkDestroy
struct NetworkDestroy_t2935674068;
// HutongGames.PlayMaker.Actions.NetworkDisconnect
struct NetworkDisconnect_t1440046520;
// HutongGames.PlayMaker.Actions.NetworkGetConnectedPlayerProperties
struct NetworkGetConnectedPlayerProperties_t4024683587;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3761865337.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3761865337MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmFloat2134102846MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmBool1075959796MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmString952858651MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmRect1076426478.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmFloat2134102846.h"
#include "mscorlib_System_Single4291918972.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmBool1075959796.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmString952858651.h"
#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3211770239MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmRect1076426478MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen3187157168MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyle2990928826MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayout3864601915MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Rect4241904616MethodDeclarations.h"
#include "mscorlib_System_Int321153838500.h"
#include "UnityEngine_UnityEngine_GUIContent2094828418.h"
#include "UnityEngine_UnityEngine_GUIContent2094828418MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyle2990928826.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GUI439899113.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GUI439899113MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmGameObject1697147867.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmGameObject1697147867MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3071478659MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera2727095145MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform1659122786MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3501516275MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector24282066565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GUI363266336.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GUI363266336MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_GUILayoutOption331591504.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2139859280.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2139859280MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2615417833MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2615417833.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTexture3073272573.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTexture3073272573MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GUI818578494.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GUI818578494MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector2533912881.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector2533912881MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3526512866.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3526512866MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GUI740407662.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GUI740407662MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2660680933.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2660680933MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent2133468028.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm1527112426MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm1527112426.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU1819635698.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU1819635698MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmInt1596138449MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmInt1596138449.h"
#include "UnityEngine_UnityEngine_GUI3134605553MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIUtility1028319349MethodDeclarations.h"
#include "mscorlib_System_Char2862622538.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GUI215605361.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GUI215605361MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2673584043.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2673584043MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2146385234.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2146385234MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2025169154.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2025169154MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GUI703888368.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GUI703888368MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU1014664468.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU1014664468MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU1009047966.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU1009047966MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2175025585.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2175025585MethodDeclarations.h"
#include "mscorlib_System_Single4291918972MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2180325291.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2180325291MethodDeclarations.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3080788440.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3080788440MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3766456830.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3766456830MethodDeclarations.h"
#include "mscorlib_System_Int321153838500MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3771756536.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3771756536MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3579067543.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3579067543MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GUI886514882.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GUI886514882MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3980277856.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3980277856MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3585978025.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3585978025MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3774275952.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3774275952MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3779575658.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3779575658MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3170064647.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3170064647MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3141290782.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3141290782MethodDeclarations.h"
#include "PlayMaker_ArrayTypes.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU1513124266.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU1513124266MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3898253232.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3898253232MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU4042007136.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU4042007136MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3055477407MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3055477407.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ha1091840427.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ha1091840427MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3501516275.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmOwnerDefault251897112.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmOwnerDefault251897112MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_OwnerDefaultOption1934292325.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_In3052069402.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_In3052069402MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Int778990893.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Int778990893MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Int286226100.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Int286226100MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf4203372500MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Int990531422.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Int990531422MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent2133468028MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_IntO91950145.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_IntO91950145MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_In1437297806.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_In1437297806MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Int102584145.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Int102584145MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_In4174732817.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_In4174732817MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector3533912882.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector3533912882MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_In1066006434.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_In1066006434MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_In2919758241.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_In2919758241MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_IsK784342153.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_IsK784342153MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co2322045418MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody3346577219MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody3346577219.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_IsS599267933.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_IsS599267933MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iTw410382178.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iTw410382178MethodDeclarations.h"
#include "AssemblyU2DCSharp_iTweenFSMEvents871409943.h"
#include "AssemblyU2DCSharp_iTweenFSMEvents871409943MethodDeclarations.h"
#include "AssemblyU2DCSharp_iTween3087282050MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT3260241011.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT3260241011MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT3433969491.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT3433969491MethodDeclarations.h"
#include "AssemblyU2DCSharp_iTween_EaseType2734598229.h"
#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3211770239.h"
#include "AssemblyU2DCSharp_iTween_LoopType1485160459.h"
#include "UnityEngine_UnityEngine_Vector34282066566MethodDeclarations.h"
#include "mscorlib_System_Type2863145774MethodDeclarations.h"
#include "mscorlib_System_Enum2862688501MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_RuntimeTypeHandle2669177232.h"
#include "mscorlib_System_Collections_Hashtable1407064410.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iTw440825252.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iTw440825252MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT2645160178.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT2645160178MethodDeclarations.h"
#include "mscorlib_System_Collections_Hashtable1407064410MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT1025491740.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT1025491740MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Space4209342076.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iTw469656626.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iTw469656626MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT1076675461.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT1076675461MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iTw469657174.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iTw469657174MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color4194546905MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iTw733362340.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iTw733362340MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT1146816066.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT1146816066MethodDeclarations.h"
#include "AssemblyU2DCSharp_iTweenFSMType470630072.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT2465480643.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT2465480643MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT1677375000.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT1677375000MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT4163517798.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT4163517798MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iTw603494487.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iTw603494487MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iTw368396562.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iTw368396562MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT1141196668.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT1141196668MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT2181561423.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT2181561423MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT1141197216.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT1141197216MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT1671849710.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT1671849710MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iTw289621857.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iTw289621857MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT1692844877.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT1692844877MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT4034512864.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT4034512864MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT1692845425.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT1692845425MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT4241723967.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT4241723967MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT1348015515.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT1348015515MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iTw559909872.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iTw559909872MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT2838558350.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT2838558350MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT3798813676.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT3798813676MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ki3079223979.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ki3079223979MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Lo1482164620.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Lo1482164620MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application2856536070MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug4195163081MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AsyncOperation3699081103.h"
#include "UnityEngine_UnityEngine_AsyncOperation3699081103MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Lo2730020496.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Lo2730020496MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Lo3138770714.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Lo3138770714MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmColor2131419205MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmColor2131419205.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ma3350951132.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ma3350951132MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MasterServer2117519177MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Mas240962846.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Mas240962846MethodDeclarations.h"
#include "UnityEngine_UnityEngine_HostData3270478838.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ma2799670129.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ma2799670129MethodDeclarations.h"
#include "UnityEngine_UnityEngine_HostData3270478838MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ma2113983876.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ma2113983876MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ma3166189106.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ma3166189106MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ma3580704376.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ma3580704376MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ma4119465886.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ma4119465886MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ma3940320318.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ma3940320318MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ma3653261073.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ma3653261073MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Mou696688690.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Mou696688690MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Mo3994311403.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input4200062272MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Mo3994311403MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Mo3768519990.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Mo3768519990MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Mo1951151207.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Mo1951151207MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Mou696801716.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Mou696801716MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionHelpers1898294297MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Mou909730492.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Mou909730492MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionHelpers1898294297.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Mo2875353433.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Mo2875353433MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time4241968337MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Na2695016998.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Na2695016998MethodDeclarations.h"
#include "System_Core_System_Action3771233898MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayMakerAnimatorMoveProxy4175490694MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NavMeshAgent588466745.h"
#include "UnityEngine_UnityEngine_Animator2776330603.h"
#include "AssemblyU2DCSharp_PlayMakerAnimatorMoveProxy4175490694.h"
#include "System_Core_System_Action3771233898.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "UnityEngine_UnityEngine_Animator2776330603MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NavMeshAgent588466745MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Net686438608.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Net686438608MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Network1492793700MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkPlayer3231273765.h"
#include "UnityEngine_UnityEngine_NetworkPlayer3231273765MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ne2329652676.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ne2329652676MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkConnectionError1049203712.h"
#include "mscorlib_System_Enum2862688501.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ne2935674068.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ne2935674068MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co2632148816MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkView3656680617MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkView3656680617.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ne1440046520.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ne1440046520MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ne4024683587.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ne4024683587MethodDeclarations.h"

// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m955200616_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m955200616(__this, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m955200616_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<iTweenFSMEvents>()
#define GameObject_AddComponent_TisiTweenFSMEvents_t871409943_m3828280117(__this, method) ((  iTweenFSMEvents_t871409943 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m955200616_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2925956997_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2925956997(__this, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2925956997_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Rigidbody>()
#define GameObject_GetComponent_TisRigidbody_t3346577219_m1334743405(__this, method) ((  Rigidbody_t3346577219 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2925956997_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.NavMeshAgent>()
#define GameObject_GetComponent_TisNavMeshAgent_t588466745_m2114887537(__this, method) ((  NavMeshAgent_t588466745 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2925956997_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animator>()
#define GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(__this, method) ((  Animator_t2776330603 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2925956997_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<PlayMakerAnimatorMoveProxy>()
#define GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623(__this, method) ((  PlayMakerAnimatorMoveProxy_t4175490694 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2925956997_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginArea::.ctor()
extern "C"  void GUILayoutBeginArea__ctor_m3361698317 (GUILayoutBeginArea_t3761865337 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginArea::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutBeginArea_Reset_m1008131258_MetadataUsageId;
extern "C"  void GUILayoutBeginArea_Reset_m1008131258 (GUILayoutBeginArea_t3761865337 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutBeginArea_Reset_m1008131258_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_screenRect_9((FsmRect_t1076426478 *)NULL);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_left_10(L_0);
		FsmFloat_t2134102846 * L_1 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_top_11(L_1);
		FsmFloat_t2134102846 * L_2 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_width_12(L_2);
		FsmFloat_t2134102846 * L_3 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_height_13(L_3);
		FsmBool_t1075959796 * L_4 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_normalized_14(L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_6 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		__this->set_style_15(L_6);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginArea::OnGUI()
extern Il2CppClass* Rect_t4241904616_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIContent_t2094828418_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t2990928826_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutBeginArea_OnGUI_m2857096967_MetadataUsageId;
extern "C"  void GUILayoutBeginArea_OnGUI_m2857096967 (GUILayoutBeginArea_t3761865337 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutBeginArea_OnGUI_m2857096967_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	GUILayoutBeginArea_t3761865337 * G_B2_0 = NULL;
	GUILayoutBeginArea_t3761865337 * G_B1_0 = NULL;
	Rect_t4241904616  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	GUILayoutBeginArea_t3761865337 * G_B3_1 = NULL;
	{
		FsmRect_t1076426478 * L_0 = __this->get_screenRect_9();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if (L_1)
		{
			G_B2_0 = __this;
			goto IL_0021;
		}
	}
	{
		FsmRect_t1076426478 * L_2 = __this->get_screenRect_9();
		NullCheck(L_2);
		Rect_t4241904616  L_3 = FsmRect_get_Value_m1002500317(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B1_0;
		goto IL_002a;
	}

IL_0021:
	{
		Initobj (Rect_t4241904616_il2cpp_TypeInfo_var, (&V_0));
		Rect_t4241904616  L_4 = V_0;
		G_B3_0 = L_4;
		G_B3_1 = G_B2_0;
	}

IL_002a:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_rect_16(G_B3_0);
		FsmFloat_t2134102846 * L_5 = __this->get_left_10();
		NullCheck(L_5);
		bool L_6 = NamedVariable_get_IsNone_m281035543(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0055;
		}
	}
	{
		Rect_t4241904616 * L_7 = __this->get_address_of_rect_16();
		FsmFloat_t2134102846 * L_8 = __this->get_left_10();
		NullCheck(L_8);
		float L_9 = FsmFloat_get_Value_m4137923823(L_8, /*hidden argument*/NULL);
		Rect_set_x_m577970569(L_7, L_9, /*hidden argument*/NULL);
	}

IL_0055:
	{
		FsmFloat_t2134102846 * L_10 = __this->get_top_11();
		NullCheck(L_10);
		bool L_11 = NamedVariable_get_IsNone_m281035543(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_007b;
		}
	}
	{
		Rect_t4241904616 * L_12 = __this->get_address_of_rect_16();
		FsmFloat_t2134102846 * L_13 = __this->get_top_11();
		NullCheck(L_13);
		float L_14 = FsmFloat_get_Value_m4137923823(L_13, /*hidden argument*/NULL);
		Rect_set_y_m67436392(L_12, L_14, /*hidden argument*/NULL);
	}

IL_007b:
	{
		FsmFloat_t2134102846 * L_15 = __this->get_width_12();
		NullCheck(L_15);
		bool L_16 = NamedVariable_get_IsNone_m281035543(L_15, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_00a1;
		}
	}
	{
		Rect_t4241904616 * L_17 = __this->get_address_of_rect_16();
		FsmFloat_t2134102846 * L_18 = __this->get_width_12();
		NullCheck(L_18);
		float L_19 = FsmFloat_get_Value_m4137923823(L_18, /*hidden argument*/NULL);
		Rect_set_width_m3771513595(L_17, L_19, /*hidden argument*/NULL);
	}

IL_00a1:
	{
		FsmFloat_t2134102846 * L_20 = __this->get_height_13();
		NullCheck(L_20);
		bool L_21 = NamedVariable_get_IsNone_m281035543(L_20, /*hidden argument*/NULL);
		if (L_21)
		{
			goto IL_00c7;
		}
	}
	{
		Rect_t4241904616 * L_22 = __this->get_address_of_rect_16();
		FsmFloat_t2134102846 * L_23 = __this->get_height_13();
		NullCheck(L_23);
		float L_24 = FsmFloat_get_Value_m4137923823(L_23, /*hidden argument*/NULL);
		Rect_set_height_m3398820332(L_22, L_24, /*hidden argument*/NULL);
	}

IL_00c7:
	{
		FsmBool_t1075959796 * L_25 = __this->get_normalized_14();
		NullCheck(L_25);
		bool L_26 = FsmBool_get_Value_m3101329097(L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_0137;
		}
	}
	{
		Rect_t4241904616 * L_27 = __this->get_address_of_rect_16();
		Rect_t4241904616 * L_28 = L_27;
		float L_29 = Rect_get_x_m982385354(L_28, /*hidden argument*/NULL);
		int32_t L_30 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_x_m577970569(L_28, ((float)((float)L_29*(float)(((float)((float)L_30))))), /*hidden argument*/NULL);
		Rect_t4241904616 * L_31 = __this->get_address_of_rect_16();
		Rect_t4241904616 * L_32 = L_31;
		float L_33 = Rect_get_width_m2824209432(L_32, /*hidden argument*/NULL);
		int32_t L_34 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_width_m3771513595(L_32, ((float)((float)L_33*(float)(((float)((float)L_34))))), /*hidden argument*/NULL);
		Rect_t4241904616 * L_35 = __this->get_address_of_rect_16();
		Rect_t4241904616 * L_36 = L_35;
		float L_37 = Rect_get_y_m982386315(L_36, /*hidden argument*/NULL);
		int32_t L_38 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_y_m67436392(L_36, ((float)((float)L_37*(float)(((float)((float)L_38))))), /*hidden argument*/NULL);
		Rect_t4241904616 * L_39 = __this->get_address_of_rect_16();
		Rect_t4241904616 * L_40 = L_39;
		float L_41 = Rect_get_height_m2154960823(L_40, /*hidden argument*/NULL);
		int32_t L_42 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_height_m3398820332(L_40, ((float)((float)L_41*(float)(((float)((float)L_42))))), /*hidden argument*/NULL);
	}

IL_0137:
	{
		Rect_t4241904616  L_43 = __this->get_rect_16();
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t2094828418_il2cpp_TypeInfo_var);
		GUIContent_t2094828418 * L_44 = ((GUIContent_t2094828418_StaticFields*)GUIContent_t2094828418_il2cpp_TypeInfo_var->static_fields)->get_none_6();
		FsmString_t952858651 * L_45 = __this->get_style_15();
		NullCheck(L_45);
		String_t* L_46 = FsmString_get_Value_m872383149(L_45, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t2990928826_il2cpp_TypeInfo_var);
		GUIStyle_t2990928826 * L_47 = GUIStyle_op_Implicit_m169215436(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		GUILayout_BeginArea_m2566106901(NULL /*static, unused*/, L_43, L_44, L_47, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject::.ctor()
extern "C"  void GUILayoutBeginAreaFollowObject__ctor_m2004408989 (GUILayoutBeginAreaFollowObject_t439899113 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutBeginAreaFollowObject_Reset_m3945809226_MetadataUsageId;
extern "C"  void GUILayoutBeginAreaFollowObject_Reset_m3945809226 (GUILayoutBeginAreaFollowObject_t439899113 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutBeginAreaFollowObject_Reset_m3945809226_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_9((FsmGameObject_t1697147867 *)NULL);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_offsetLeft_10(L_0);
		FsmFloat_t2134102846 * L_1 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_offsetTop_11(L_1);
		FsmFloat_t2134102846 * L_2 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_width_12(L_2);
		FsmFloat_t2134102846 * L_3 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_height_13(L_3);
		FsmBool_t1075959796 * L_4 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_normalized_14(L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_6 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		__this->set_style_15(L_6);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject::OnGUI()
extern "C"  void GUILayoutBeginAreaFollowObject_OnGUI_m1499807639 (GUILayoutBeginAreaFollowObject_t439899113 * __this, const MethodInfo* method)
{
	GameObject_t3674682005 * V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t4282066565  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	Rect_t4241904616  V_6;
	memset(&V_6, 0, sizeof(V_6));
	float G_B7_0 = 0.0f;
	float G_B6_0 = 0.0f;
	float G_B8_0 = 0.0f;
	float G_B8_1 = 0.0f;
	float G_B10_0 = 0.0f;
	float G_B9_0 = 0.0f;
	float G_B11_0 = 0.0f;
	float G_B11_1 = 0.0f;
	{
		FsmGameObject_t1697147867 * L_0 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_1 = FsmGameObject_get_Value_m673294275(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t3674682005 * L_2 = V_0;
		bool L_3 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_2, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0028;
		}
	}
	{
		Camera_t2727095145 * L_4 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}

IL_0028:
	{
		GUILayoutBeginAreaFollowObject_DummyBeginArea_m2118469909(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}

IL_002e:
	{
		GameObject_t3674682005 * L_6 = V_0;
		NullCheck(L_6);
		Transform_t1659122786 * L_7 = GameObject_get_transform_m1278640159(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t4282066566  L_8 = Transform_get_position_m2211398607(L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		Camera_t2727095145 * L_9 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_t1659122786 * L_10 = Component_get_transform_m4257140443(L_9, /*hidden argument*/NULL);
		Vector3_t4282066566  L_11 = V_1;
		NullCheck(L_10);
		Vector3_t4282066566  L_12 = Transform_InverseTransformPoint_m1626812000(L_10, L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		float L_13 = (&V_2)->get_z_3();
		if ((!(((float)L_13) < ((float)(0.0f)))))
		{
			goto IL_0062;
		}
	}
	{
		GUILayoutBeginAreaFollowObject_DummyBeginArea_m2118469909(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}

IL_0062:
	{
		Camera_t2727095145 * L_14 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_15 = V_1;
		NullCheck(L_14);
		Vector3_t4282066566  L_16 = Camera_WorldToScreenPoint_m2400233676(L_14, L_15, /*hidden argument*/NULL);
		Vector2_t4282066565  L_17 = Vector2_op_Implicit_m4083860659(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		V_3 = L_17;
		float L_18 = (&V_3)->get_x_1();
		FsmBool_t1075959796 * L_19 = __this->get_normalized_14();
		NullCheck(L_19);
		bool L_20 = FsmBool_get_Value_m3101329097(L_19, /*hidden argument*/NULL);
		G_B6_0 = L_18;
		if (!L_20)
		{
			G_B7_0 = L_18;
			goto IL_00a1;
		}
	}
	{
		FsmFloat_t2134102846 * L_21 = __this->get_offsetLeft_10();
		NullCheck(L_21);
		float L_22 = FsmFloat_get_Value_m4137923823(L_21, /*hidden argument*/NULL);
		int32_t L_23 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B8_0 = ((float)((float)L_22*(float)(((float)((float)L_23)))));
		G_B8_1 = G_B6_0;
		goto IL_00ac;
	}

IL_00a1:
	{
		FsmFloat_t2134102846 * L_24 = __this->get_offsetLeft_10();
		NullCheck(L_24);
		float L_25 = FsmFloat_get_Value_m4137923823(L_24, /*hidden argument*/NULL);
		G_B8_0 = L_25;
		G_B8_1 = G_B7_0;
	}

IL_00ac:
	{
		V_4 = ((float)((float)G_B8_1+(float)G_B8_0));
		float L_26 = (&V_3)->get_y_2();
		FsmBool_t1075959796 * L_27 = __this->get_normalized_14();
		NullCheck(L_27);
		bool L_28 = FsmBool_get_Value_m3101329097(L_27, /*hidden argument*/NULL);
		G_B9_0 = L_26;
		if (!L_28)
		{
			G_B10_0 = L_26;
			goto IL_00dd;
		}
	}
	{
		FsmFloat_t2134102846 * L_29 = __this->get_offsetTop_11();
		NullCheck(L_29);
		float L_30 = FsmFloat_get_Value_m4137923823(L_29, /*hidden argument*/NULL);
		int32_t L_31 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B11_0 = ((float)((float)L_30*(float)(((float)((float)L_31)))));
		G_B11_1 = G_B9_0;
		goto IL_00e8;
	}

IL_00dd:
	{
		FsmFloat_t2134102846 * L_32 = __this->get_offsetTop_11();
		NullCheck(L_32);
		float L_33 = FsmFloat_get_Value_m4137923823(L_32, /*hidden argument*/NULL);
		G_B11_0 = L_33;
		G_B11_1 = G_B10_0;
	}

IL_00e8:
	{
		V_5 = ((float)((float)G_B11_1+(float)G_B11_0));
		float L_34 = V_4;
		float L_35 = V_5;
		FsmFloat_t2134102846 * L_36 = __this->get_width_12();
		NullCheck(L_36);
		float L_37 = FsmFloat_get_Value_m4137923823(L_36, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_38 = __this->get_height_13();
		NullCheck(L_38);
		float L_39 = FsmFloat_get_Value_m4137923823(L_38, /*hidden argument*/NULL);
		Rect__ctor_m3291325233((&V_6), L_34, L_35, L_37, L_39, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_40 = __this->get_normalized_14();
		NullCheck(L_40);
		bool L_41 = FsmBool_get_Value_m3101329097(L_40, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_0144;
		}
	}
	{
		Rect_t4241904616 * L_42 = (&V_6);
		float L_43 = Rect_get_width_m2824209432(L_42, /*hidden argument*/NULL);
		int32_t L_44 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_width_m3771513595(L_42, ((float)((float)L_43*(float)(((float)((float)L_44))))), /*hidden argument*/NULL);
		Rect_t4241904616 * L_45 = (&V_6);
		float L_46 = Rect_get_height_m2154960823(L_45, /*hidden argument*/NULL);
		int32_t L_47 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_height_m3398820332(L_45, ((float)((float)L_46*(float)(((float)((float)L_47))))), /*hidden argument*/NULL);
	}

IL_0144:
	{
		int32_t L_48 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_49 = Rect_get_y_m982386315((&V_6), /*hidden argument*/NULL);
		Rect_set_y_m67436392((&V_6), ((float)((float)(((float)((float)L_48)))-(float)L_49)), /*hidden argument*/NULL);
		Rect_t4241904616  L_50 = V_6;
		FsmString_t952858651 * L_51 = __this->get_style_15();
		NullCheck(L_51);
		String_t* L_52 = FsmString_get_Value_m872383149(L_51, /*hidden argument*/NULL);
		GUILayout_BeginArea_m2598177129(NULL /*static, unused*/, L_50, L_52, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject::DummyBeginArea()
extern Il2CppClass* Rect_t4241904616_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutBeginAreaFollowObject_DummyBeginArea_m2118469909_MetadataUsageId;
extern "C"  void GUILayoutBeginAreaFollowObject_DummyBeginArea_m2118469909 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutBeginAreaFollowObject_DummyBeginArea_m2118469909_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Rect_t4241904616_il2cpp_TypeInfo_var, (&V_0));
		Rect_t4241904616  L_0 = V_0;
		GUILayout_BeginArea_m4120101869(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginCentered::.ctor()
extern "C"  void GUILayoutBeginCentered__ctor_m3559633734 (GUILayoutBeginCentered_t363266336 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginCentered::Reset()
extern "C"  void GUILayoutBeginCentered_Reset_m1206066675 (GUILayoutBeginCentered_t363266336 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginCentered::OnGUI()
extern Il2CppClass* GUILayoutOptionU5BU5D_t2977405297_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutBeginCentered_OnGUI_m3055032384_MetadataUsageId;
extern "C"  void GUILayoutBeginCentered_OnGUI_m3055032384 (GUILayoutBeginCentered_t363266336 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutBeginCentered_OnGUI_m3055032384_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUILayout_BeginVertical_m2155819644(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2977405297*)SZArrayNew(GUILayoutOptionU5BU5D_t2977405297_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m3316706189(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m722450062(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2977405297*)SZArrayNew(GUILayoutOptionU5BU5D_t2977405297_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m3316706189(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginVertical_m2155819644(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2977405297*)SZArrayNew(GUILayoutOptionU5BU5D_t2977405297_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginHorizontal::.ctor()
extern "C"  void GUILayoutBeginHorizontal__ctor_m2092243734 (GUILayoutBeginHorizontal_t2139859280 * __this, const MethodInfo* method)
{
	{
		GUILayoutAction__ctor_m2599973229(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginHorizontal::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutBeginHorizontal_Reset_m4033643971_MetadataUsageId;
extern "C"  void GUILayoutBeginHorizontal_Reset_m4033643971 (GUILayoutBeginHorizontal_t2139859280 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutBeginHorizontal_Reset_m4033643971_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUILayoutAction_Reset_m246406170(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_text_12(L_1);
		__this->set_image_11((FsmTexture_t3073272573 *)NULL);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_3 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_tooltip_13(L_3);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_5 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		__this->set_style_14(L_5);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginHorizontal::OnGUI()
extern Il2CppClass* GUIContent_t2094828418_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t2990928826_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutBeginHorizontal_OnGUI_m1587642384_MetadataUsageId;
extern "C"  void GUILayoutBeginHorizontal_OnGUI_m1587642384 (GUILayoutBeginHorizontal_t2139859280 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutBeginHorizontal_OnGUI_m1587642384_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmString_t952858651 * L_0 = __this->get_text_12();
		NullCheck(L_0);
		String_t* L_1 = FsmString_get_Value_m872383149(L_0, /*hidden argument*/NULL);
		FsmTexture_t3073272573 * L_2 = __this->get_image_11();
		NullCheck(L_2);
		Texture_t2526458961 * L_3 = FsmTexture_get_Value_m3156202285(L_2, /*hidden argument*/NULL);
		FsmString_t952858651 * L_4 = __this->get_tooltip_13();
		NullCheck(L_4);
		String_t* L_5 = FsmString_get_Value_m872383149(L_4, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_6 = (GUIContent_t2094828418 *)il2cpp_codegen_object_new(GUIContent_t2094828418_il2cpp_TypeInfo_var);
		GUIContent__ctor_m905567255(L_6, L_1, L_3, L_5, /*hidden argument*/NULL);
		FsmString_t952858651 * L_7 = __this->get_style_14();
		NullCheck(L_7);
		String_t* L_8 = FsmString_get_Value_m872383149(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t2990928826_il2cpp_TypeInfo_var);
		GUIStyle_t2990928826 * L_9 = GUIStyle_op_Implicit_m169215436(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2977405297* L_10 = GUILayoutAction_get_LayoutOptions_m1855901188(__this, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m2423643318(NULL /*static, unused*/, L_6, L_9, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView::.ctor()
extern "C"  void GUILayoutBeginScrollView__ctor_m4157778920 (GUILayoutBeginScrollView_t818578494 * __this, const MethodInfo* method)
{
	{
		GUILayoutAction__ctor_m2599973229(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView::Reset()
extern "C"  void GUILayoutBeginScrollView_Reset_m1804211861 (GUILayoutBeginScrollView_t818578494 * __this, const MethodInfo* method)
{
	{
		GUILayoutAction_Reset_m246406170(__this, /*hidden argument*/NULL);
		__this->set_scrollPosition_11((FsmVector2_t533912881 *)NULL);
		__this->set_horizontalScrollbar_12((FsmBool_t1075959796 *)NULL);
		__this->set_verticalScrollbar_13((FsmBool_t1075959796 *)NULL);
		__this->set_useCustomStyle_14((FsmBool_t1075959796 *)NULL);
		__this->set_horizontalStyle_15((FsmString_t952858651 *)NULL);
		__this->set_verticalStyle_16((FsmString_t952858651 *)NULL);
		__this->set_backgroundStyle_17((FsmString_t952858651 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView::OnGUI()
extern Il2CppClass* GUIStyle_t2990928826_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutBeginScrollView_OnGUI_m3653177570_MetadataUsageId;
extern "C"  void GUILayoutBeginScrollView_OnGUI_m3653177570 (GUILayoutBeginScrollView_t818578494 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutBeginScrollView_OnGUI_m3653177570_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmBool_t1075959796 * L_0 = __this->get_useCustomStyle_14();
		NullCheck(L_0);
		bool L_1 = FsmBool_get_Value_m3101329097(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_007c;
		}
	}
	{
		FsmVector2_t533912881 * L_2 = __this->get_scrollPosition_11();
		FsmVector2_t533912881 * L_3 = __this->get_scrollPosition_11();
		NullCheck(L_3);
		Vector2_t4282066565  L_4 = FsmVector2_get_Value_m1313754285(L_3, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_5 = __this->get_horizontalScrollbar_12();
		NullCheck(L_5);
		bool L_6 = FsmBool_get_Value_m3101329097(L_5, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_7 = __this->get_verticalScrollbar_13();
		NullCheck(L_7);
		bool L_8 = FsmBool_get_Value_m3101329097(L_7, /*hidden argument*/NULL);
		FsmString_t952858651 * L_9 = __this->get_horizontalStyle_15();
		NullCheck(L_9);
		String_t* L_10 = FsmString_get_Value_m872383149(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t2990928826_il2cpp_TypeInfo_var);
		GUIStyle_t2990928826 * L_11 = GUIStyle_op_Implicit_m169215436(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		FsmString_t952858651 * L_12 = __this->get_verticalStyle_16();
		NullCheck(L_12);
		String_t* L_13 = FsmString_get_Value_m872383149(L_12, /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_14 = GUIStyle_op_Implicit_m169215436(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		FsmString_t952858651 * L_15 = __this->get_backgroundStyle_17();
		NullCheck(L_15);
		String_t* L_16 = FsmString_get_Value_m872383149(L_15, /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_17 = GUIStyle_op_Implicit_m169215436(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2977405297* L_18 = GUILayoutAction_get_LayoutOptions_m1855901188(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_19 = GUILayout_BeginScrollView_m3507871150(NULL /*static, unused*/, L_4, L_6, L_8, L_11, L_14, L_17, L_18, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmVector2_set_Value_m2900659718(L_2, L_19, /*hidden argument*/NULL);
		goto IL_00b3;
	}

IL_007c:
	{
		FsmVector2_t533912881 * L_20 = __this->get_scrollPosition_11();
		FsmVector2_t533912881 * L_21 = __this->get_scrollPosition_11();
		NullCheck(L_21);
		Vector2_t4282066565  L_22 = FsmVector2_get_Value_m1313754285(L_21, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_23 = __this->get_horizontalScrollbar_12();
		NullCheck(L_23);
		bool L_24 = FsmBool_get_Value_m3101329097(L_23, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_25 = __this->get_verticalScrollbar_13();
		NullCheck(L_25);
		bool L_26 = FsmBool_get_Value_m3101329097(L_25, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2977405297* L_27 = GUILayoutAction_get_LayoutOptions_m1855901188(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_28 = GUILayout_BeginScrollView_m426646201(NULL /*static, unused*/, L_22, L_24, L_26, L_27, /*hidden argument*/NULL);
		NullCheck(L_20);
		FsmVector2_set_Value_m2900659718(L_20, L_28, /*hidden argument*/NULL);
	}

IL_00b3:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginVertical::.ctor()
extern "C"  void GUILayoutBeginVertical__ctor_m3448831428 (GUILayoutBeginVertical_t3526512866 * __this, const MethodInfo* method)
{
	{
		GUILayoutAction__ctor_m2599973229(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginVertical::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutBeginVertical_Reset_m1095264369_MetadataUsageId;
extern "C"  void GUILayoutBeginVertical_Reset_m1095264369 (GUILayoutBeginVertical_t3526512866 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutBeginVertical_Reset_m1095264369_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUILayoutAction_Reset_m246406170(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_text_12(L_1);
		__this->set_image_11((FsmTexture_t3073272573 *)NULL);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_3 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_tooltip_13(L_3);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_5 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		__this->set_style_14(L_5);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginVertical::OnGUI()
extern Il2CppClass* GUIContent_t2094828418_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t2990928826_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutBeginVertical_OnGUI_m2944230078_MetadataUsageId;
extern "C"  void GUILayoutBeginVertical_OnGUI_m2944230078 (GUILayoutBeginVertical_t3526512866 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutBeginVertical_OnGUI_m2944230078_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmString_t952858651 * L_0 = __this->get_text_12();
		NullCheck(L_0);
		String_t* L_1 = FsmString_get_Value_m872383149(L_0, /*hidden argument*/NULL);
		FsmTexture_t3073272573 * L_2 = __this->get_image_11();
		NullCheck(L_2);
		Texture_t2526458961 * L_3 = FsmTexture_get_Value_m3156202285(L_2, /*hidden argument*/NULL);
		FsmString_t952858651 * L_4 = __this->get_tooltip_13();
		NullCheck(L_4);
		String_t* L_5 = FsmString_get_Value_m872383149(L_4, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_6 = (GUIContent_t2094828418 *)il2cpp_codegen_object_new(GUIContent_t2094828418_il2cpp_TypeInfo_var);
		GUIContent__ctor_m905567255(L_6, L_1, L_3, L_5, /*hidden argument*/NULL);
		FsmString_t952858651 * L_7 = __this->get_style_14();
		NullCheck(L_7);
		String_t* L_8 = FsmString_get_Value_m872383149(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t2990928826_il2cpp_TypeInfo_var);
		GUIStyle_t2990928826 * L_9 = GUIStyle_op_Implicit_m169215436(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2977405297* L_10 = GUILayoutAction_get_LayoutOptions_m1855901188(__this, /*hidden argument*/NULL);
		GUILayout_BeginVertical_m3125652388(NULL /*static, unused*/, L_6, L_9, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBox::.ctor()
extern "C"  void GUILayoutBox__ctor_m382447800 (GUILayoutBox_t740407662 * __this, const MethodInfo* method)
{
	{
		GUILayoutAction__ctor_m2599973229(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBox::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutBox_Reset_m2323848037_MetadataUsageId;
extern "C"  void GUILayoutBox_Reset_m2323848037 (GUILayoutBox_t740407662 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutBox_Reset_m2323848037_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUILayoutAction_Reset_m246406170(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_text_12(L_1);
		__this->set_image_11((FsmTexture_t3073272573 *)NULL);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_3 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_tooltip_13(L_3);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_5 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		__this->set_style_14(L_5);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBox::OnGUI()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIContent_t2094828418_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t2990928826_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutBox_OnGUI_m4172813746_MetadataUsageId;
extern "C"  void GUILayoutBox_OnGUI_m4172813746 (GUILayoutBox_t740407662 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutBox_OnGUI_m4172813746_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmString_t952858651 * L_0 = __this->get_style_14();
		NullCheck(L_0);
		String_t* L_1 = FsmString_get_Value_m872383149(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004b;
		}
	}
	{
		FsmString_t952858651 * L_3 = __this->get_text_12();
		NullCheck(L_3);
		String_t* L_4 = FsmString_get_Value_m872383149(L_3, /*hidden argument*/NULL);
		FsmTexture_t3073272573 * L_5 = __this->get_image_11();
		NullCheck(L_5);
		Texture_t2526458961 * L_6 = FsmTexture_get_Value_m3156202285(L_5, /*hidden argument*/NULL);
		FsmString_t952858651 * L_7 = __this->get_tooltip_13();
		NullCheck(L_7);
		String_t* L_8 = FsmString_get_Value_m872383149(L_7, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_9 = (GUIContent_t2094828418 *)il2cpp_codegen_object_new(GUIContent_t2094828418_il2cpp_TypeInfo_var);
		GUIContent__ctor_m905567255(L_9, L_4, L_6, L_8, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2977405297* L_10 = GUILayoutAction_get_LayoutOptions_m1855901188(__this, /*hidden argument*/NULL);
		GUILayout_Box_m3611689199(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		goto IL_008c;
	}

IL_004b:
	{
		FsmString_t952858651 * L_11 = __this->get_text_12();
		NullCheck(L_11);
		String_t* L_12 = FsmString_get_Value_m872383149(L_11, /*hidden argument*/NULL);
		FsmTexture_t3073272573 * L_13 = __this->get_image_11();
		NullCheck(L_13);
		Texture_t2526458961 * L_14 = FsmTexture_get_Value_m3156202285(L_13, /*hidden argument*/NULL);
		FsmString_t952858651 * L_15 = __this->get_tooltip_13();
		NullCheck(L_15);
		String_t* L_16 = FsmString_get_Value_m872383149(L_15, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_17 = (GUIContent_t2094828418 *)il2cpp_codegen_object_new(GUIContent_t2094828418_il2cpp_TypeInfo_var);
		GUIContent__ctor_m905567255(L_17, L_12, L_14, L_16, /*hidden argument*/NULL);
		FsmString_t952858651 * L_18 = __this->get_style_14();
		NullCheck(L_18);
		String_t* L_19 = FsmString_get_Value_m872383149(L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t2990928826_il2cpp_TypeInfo_var);
		GUIStyle_t2990928826 * L_20 = GUIStyle_op_Implicit_m169215436(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2977405297* L_21 = GUILayoutAction_get_LayoutOptions_m1855901188(__this, /*hidden argument*/NULL);
		GUILayout_Box_m1649373272(NULL /*static, unused*/, L_17, L_20, L_21, /*hidden argument*/NULL);
	}

IL_008c:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutButton::.ctor()
extern "C"  void GUILayoutButton__ctor_m722346993 (GUILayoutButton_t2660680933 * __this, const MethodInfo* method)
{
	{
		GUILayoutAction__ctor_m2599973229(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutButton::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutButton_Reset_m2663747230_MetadataUsageId;
extern "C"  void GUILayoutButton_Reset_m2663747230 (GUILayoutButton_t2660680933 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutButton_Reset_m2663747230_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUILayoutAction_Reset_m246406170(__this, /*hidden argument*/NULL);
		__this->set_sendEvent_11((FsmEvent_t2133468028 *)NULL);
		__this->set_storeButtonState_12((FsmBool_t1075959796 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_text_14(L_1);
		__this->set_image_13((FsmTexture_t3073272573 *)NULL);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_3 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_tooltip_15(L_3);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_5 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		__this->set_style_16(L_5);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutButton::OnGUI()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIContent_t2094828418_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t2990928826_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutButton_OnGUI_m217745643_MetadataUsageId;
extern "C"  void GUILayoutButton_OnGUI_m217745643 (GUILayoutButton_t2660680933 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutButton_OnGUI_m217745643_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		FsmString_t952858651 * L_0 = __this->get_style_16();
		NullCheck(L_0);
		String_t* L_1 = FsmString_get_Value_m872383149(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004c;
		}
	}
	{
		FsmString_t952858651 * L_3 = __this->get_text_14();
		NullCheck(L_3);
		String_t* L_4 = FsmString_get_Value_m872383149(L_3, /*hidden argument*/NULL);
		FsmTexture_t3073272573 * L_5 = __this->get_image_13();
		NullCheck(L_5);
		Texture_t2526458961 * L_6 = FsmTexture_get_Value_m3156202285(L_5, /*hidden argument*/NULL);
		FsmString_t952858651 * L_7 = __this->get_tooltip_15();
		NullCheck(L_7);
		String_t* L_8 = FsmString_get_Value_m872383149(L_7, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_9 = (GUIContent_t2094828418 *)il2cpp_codegen_object_new(GUIContent_t2094828418_il2cpp_TypeInfo_var);
		GUIContent__ctor_m905567255(L_9, L_4, L_6, L_8, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2977405297* L_10 = GUILayoutAction_get_LayoutOptions_m1855901188(__this, /*hidden argument*/NULL);
		bool L_11 = GUILayout_Button_m1054745774(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		goto IL_008e;
	}

IL_004c:
	{
		FsmString_t952858651 * L_12 = __this->get_text_14();
		NullCheck(L_12);
		String_t* L_13 = FsmString_get_Value_m872383149(L_12, /*hidden argument*/NULL);
		FsmTexture_t3073272573 * L_14 = __this->get_image_13();
		NullCheck(L_14);
		Texture_t2526458961 * L_15 = FsmTexture_get_Value_m3156202285(L_14, /*hidden argument*/NULL);
		FsmString_t952858651 * L_16 = __this->get_tooltip_15();
		NullCheck(L_16);
		String_t* L_17 = FsmString_get_Value_m872383149(L_16, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_18 = (GUIContent_t2094828418 *)il2cpp_codegen_object_new(GUIContent_t2094828418_il2cpp_TypeInfo_var);
		GUIContent__ctor_m905567255(L_18, L_13, L_15, L_17, /*hidden argument*/NULL);
		FsmString_t952858651 * L_19 = __this->get_style_16();
		NullCheck(L_19);
		String_t* L_20 = FsmString_get_Value_m872383149(L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t2990928826_il2cpp_TypeInfo_var);
		GUIStyle_t2990928826 * L_21 = GUIStyle_op_Implicit_m169215436(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2977405297* L_22 = GUILayoutAction_get_LayoutOptions_m1855901188(__this, /*hidden argument*/NULL);
		bool L_23 = GUILayout_Button_m3489753593(NULL /*static, unused*/, L_18, L_21, L_22, /*hidden argument*/NULL);
		V_0 = L_23;
	}

IL_008e:
	{
		bool L_24 = V_0;
		if (!L_24)
		{
			goto IL_00a5;
		}
	}
	{
		Fsm_t1527112426 * L_25 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_26 = __this->get_sendEvent_11();
		NullCheck(L_25);
		Fsm_Event_m625948263(L_25, L_26, /*hidden argument*/NULL);
	}

IL_00a5:
	{
		FsmBool_t1075959796 * L_27 = __this->get_storeButtonState_12();
		if (!L_27)
		{
			goto IL_00bc;
		}
	}
	{
		FsmBool_t1075959796 * L_28 = __this->get_storeButtonState_12();
		bool L_29 = V_0;
		NullCheck(L_28);
		FsmBool_set_Value_m1126216340(L_28, L_29, /*hidden argument*/NULL);
	}

IL_00bc:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutConfirmPasswordField::.ctor()
extern "C"  void GUILayoutConfirmPasswordField__ctor_m198238596 (GUILayoutConfirmPasswordField_t1819635698 * __this, const MethodInfo* method)
{
	{
		GUILayoutAction__ctor_m2599973229(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutConfirmPasswordField::Reset()
extern Il2CppCodeGenString* _stringLiteral942981037;
extern Il2CppCodeGenString* _stringLiteral42;
extern const uint32_t GUILayoutConfirmPasswordField_Reset_m2139638833_MetadataUsageId;
extern "C"  void GUILayoutConfirmPasswordField_Reset_m2139638833 (GUILayoutConfirmPasswordField_t1819635698 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutConfirmPasswordField_Reset_m2139638833_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_text_11((FsmString_t952858651 *)NULL);
		FsmInt_t1596138449 * L_0 = FsmInt_op_Implicit_m1006909518(NULL /*static, unused*/, ((int32_t)25), /*hidden argument*/NULL);
		__this->set_maxLength_12(L_0);
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, _stringLiteral942981037, /*hidden argument*/NULL);
		__this->set_style_13(L_1);
		FsmString_t952858651 * L_2 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, _stringLiteral42, /*hidden argument*/NULL);
		__this->set_mask_15(L_2);
		__this->set_changedEvent_14((FsmEvent_t2133468028 *)NULL);
		FsmBool_t1075959796 * L_3 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_confirm_16(L_3);
		__this->set_password_17((FsmString_t952858651 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutConfirmPasswordField::OnGUI()
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t2990928826_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIUtility_t1028319349_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutConfirmPasswordField_OnGUI_m3988604542_MetadataUsageId;
extern "C"  void GUILayoutConfirmPasswordField_OnGUI_m3988604542 (GUILayoutConfirmPasswordField_t1819635698 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutConfirmPasswordField_OnGUI_m3988604542_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		bool L_0 = GUI_get_changed_m1591686125(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		GUI_set_changed_m727947722(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		FsmString_t952858651 * L_1 = __this->get_text_11();
		FsmString_t952858651 * L_2 = __this->get_text_11();
		NullCheck(L_2);
		String_t* L_3 = FsmString_get_Value_m872383149(L_2, /*hidden argument*/NULL);
		FsmString_t952858651 * L_4 = __this->get_mask_15();
		NullCheck(L_4);
		String_t* L_5 = FsmString_get_Value_m872383149(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Il2CppChar L_6 = String_get_Chars_m3015341861(L_5, 0, /*hidden argument*/NULL);
		FsmString_t952858651 * L_7 = __this->get_style_13();
		NullCheck(L_7);
		String_t* L_8 = FsmString_get_Value_m872383149(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t2990928826_il2cpp_TypeInfo_var);
		GUIStyle_t2990928826 * L_9 = GUIStyle_op_Implicit_m169215436(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2977405297* L_10 = GUILayoutAction_get_LayoutOptions_m1855901188(__this, /*hidden argument*/NULL);
		String_t* L_11 = GUILayout_PasswordField_m487636731(NULL /*static, unused*/, L_3, L_6, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_1);
		FsmString_set_Value_m829393196(L_1, L_11, /*hidden argument*/NULL);
		bool L_12 = GUI_get_changed_m1591686125(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0073;
		}
	}
	{
		Fsm_t1527112426 * L_13 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_14 = __this->get_changedEvent_14();
		NullCheck(L_13);
		Fsm_Event_m625948263(L_13, L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		GUIUtility_ExitGUI_m2798995767(NULL /*static, unused*/, /*hidden argument*/NULL);
		goto IL_0079;
	}

IL_0073:
	{
		bool L_15 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_changed_m727947722(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
	}

IL_0079:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutEmailField::.ctor()
extern "C"  void GUILayoutEmailField__ctor_m2284622309 (GUILayoutEmailField_t215605361 * __this, const MethodInfo* method)
{
	{
		GUILayoutAction__ctor_m2599973229(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutEmailField::Reset()
extern Il2CppCodeGenString* _stringLiteral942981037;
extern const uint32_t GUILayoutEmailField_Reset_m4226022546_MetadataUsageId;
extern "C"  void GUILayoutEmailField_Reset_m4226022546 (GUILayoutEmailField_t215605361 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutEmailField_Reset_m4226022546_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_text_11((FsmString_t952858651 *)NULL);
		FsmInt_t1596138449 * L_0 = FsmInt_op_Implicit_m1006909518(NULL /*static, unused*/, ((int32_t)25), /*hidden argument*/NULL);
		__this->set_maxLength_12(L_0);
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, _stringLiteral942981037, /*hidden argument*/NULL);
		__this->set_style_13(L_1);
		FsmBool_t1075959796 * L_2 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_valid_15(L_2);
		__this->set_changedEvent_14((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutEmailField::OnGUI()
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t2990928826_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIUtility_t1028319349_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutEmailField_OnGUI_m1780020959_MetadataUsageId;
extern "C"  void GUILayoutEmailField_OnGUI_m1780020959 (GUILayoutEmailField_t215605361 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutEmailField_OnGUI_m1780020959_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		bool L_0 = GUI_get_changed_m1591686125(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		GUI_set_changed_m727947722(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		FsmString_t952858651 * L_1 = __this->get_text_11();
		FsmString_t952858651 * L_2 = __this->get_text_11();
		NullCheck(L_2);
		String_t* L_3 = FsmString_get_Value_m872383149(L_2, /*hidden argument*/NULL);
		FsmString_t952858651 * L_4 = __this->get_style_13();
		NullCheck(L_4);
		String_t* L_5 = FsmString_get_Value_m872383149(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t2990928826_il2cpp_TypeInfo_var);
		GUIStyle_t2990928826 * L_6 = GUIStyle_op_Implicit_m169215436(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2977405297* L_7 = GUILayoutAction_get_LayoutOptions_m1855901188(__this, /*hidden argument*/NULL);
		String_t* L_8 = GUILayout_TextField_m2367652128(NULL /*static, unused*/, L_3, L_6, L_7, /*hidden argument*/NULL);
		NullCheck(L_1);
		FsmString_set_Value_m829393196(L_1, L_8, /*hidden argument*/NULL);
		bool L_9 = GUI_get_changed_m1591686125(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0062;
		}
	}
	{
		Fsm_t1527112426 * L_10 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_11 = __this->get_changedEvent_14();
		NullCheck(L_10);
		Fsm_Event_m625948263(L_10, L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		GUIUtility_ExitGUI_m2798995767(NULL /*static, unused*/, /*hidden argument*/NULL);
		goto IL_0068;
	}

IL_0062:
	{
		bool L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_changed_m727947722(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
	}

IL_0068:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutEndArea::.ctor()
extern "C"  void GUILayoutEndArea__ctor_m2765597467 (GUILayoutEndArea_t2673584043 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutEndArea::Reset()
extern "C"  void GUILayoutEndArea_Reset_m412030408 (GUILayoutEndArea_t2673584043 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutEndArea::OnGUI()
extern "C"  void GUILayoutEndArea_OnGUI_m2260996117 (GUILayoutEndArea_t2673584043 * __this, const MethodInfo* method)
{
	{
		GUILayout_EndArea_m755663130(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutEndCentered::.ctor()
extern "C"  void GUILayoutEndCentered__ctor_m3634672980 (GUILayoutEndCentered_t2146385234 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutEndCentered::Reset()
extern "C"  void GUILayoutEndCentered_Reset_m1281105921 (GUILayoutEndCentered_t2146385234 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutEndCentered::OnGUI()
extern "C"  void GUILayoutEndCentered_OnGUI_m3130071630 (GUILayoutEndCentered_t2146385234 * __this, const MethodInfo* method)
{
	{
		GUILayout_EndVertical_m3685530563(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m3316706189(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m556624369(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m3316706189(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndVertical_m3685530563(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutEndHorizontal::.ctor()
extern "C"  void GUILayoutEndHorizontal__ctor_m1190515108 (GUILayoutEndHorizontal_t2025169154 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutEndHorizontal::Reset()
extern "C"  void GUILayoutEndHorizontal_Reset_m3131915345 (GUILayoutEndHorizontal_t2025169154 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutEndHorizontal::OnGUI()
extern "C"  void GUILayoutEndHorizontal_OnGUI_m685913758 (GUILayoutEndHorizontal_t2025169154 * __this, const MethodInfo* method)
{
	{
		GUILayout_EndHorizontal_m556624369(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutEndScrollView::.ctor()
extern "C"  void GUILayoutEndScrollView__ctor_m3256050294 (GUILayoutEndScrollView_t703888368 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutEndScrollView::OnGUI()
extern "C"  void GUILayoutEndScrollView_OnGUI_m2751448944 (GUILayoutEndScrollView_t703888368 * __this, const MethodInfo* method)
{
	{
		GUILayout_EndScrollView_m2116108639(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutEndVertical::.ctor()
extern "C"  void GUILayoutEndVertical__ctor_m3523870674 (GUILayoutEndVertical_t1014664468 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutEndVertical::Reset()
extern "C"  void GUILayoutEndVertical_Reset_m1170303615 (GUILayoutEndVertical_t1014664468 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutEndVertical::OnGUI()
extern "C"  void GUILayoutEndVertical_OnGUI_m3019269324 (GUILayoutEndVertical_t1014664468 * __this, const MethodInfo* method)
{
	{
		GUILayout_EndVertical_m3685530563(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutFlexibleSpace::.ctor()
extern "C"  void GUILayoutFlexibleSpace__ctor_m3722872968 (GUILayoutFlexibleSpace_t1009047966 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutFlexibleSpace::Reset()
extern "C"  void GUILayoutFlexibleSpace_Reset_m1369305909 (GUILayoutFlexibleSpace_t1009047966 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutFlexibleSpace::OnGUI()
extern "C"  void GUILayoutFlexibleSpace_OnGUI_m3218271618 (GUILayoutFlexibleSpace_t1009047966 * __this, const MethodInfo* method)
{
	{
		GUILayout_FlexibleSpace_m3316706189(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutFloatField::.ctor()
extern "C"  void GUILayoutFloatField__ctor_m3168508069 (GUILayoutFloatField_t2175025585 * __this, const MethodInfo* method)
{
	{
		GUILayoutAction__ctor_m2599973229(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutFloatField::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutFloatField_Reset_m814941010_MetadataUsageId;
extern "C"  void GUILayoutFloatField_Reset_m814941010 (GUILayoutFloatField_t2175025585 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutFloatField_Reset_m814941010_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUILayoutAction_Reset_m246406170(__this, /*hidden argument*/NULL);
		__this->set_floatVariable_11((FsmFloat_t2134102846 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_style_12(L_1);
		__this->set_changedEvent_13((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutFloatField::OnGUI()
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t2990928826_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIUtility_t1028319349_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutFloatField_OnGUI_m2663906719_MetadataUsageId;
extern "C"  void GUILayoutFloatField_OnGUI_m2663906719 (GUILayoutFloatField_t2175025585 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutFloatField_OnGUI_m2663906719_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		bool L_0 = GUI_get_changed_m1591686125(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		GUI_set_changed_m727947722(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		FsmString_t952858651 * L_1 = __this->get_style_12();
		NullCheck(L_1);
		String_t* L_2 = FsmString_get_Value_m872383149(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0064;
		}
	}
	{
		FsmFloat_t2134102846 * L_4 = __this->get_floatVariable_11();
		FsmFloat_t2134102846 * L_5 = __this->get_floatVariable_11();
		NullCheck(L_5);
		float L_6 = FsmFloat_get_Value_m4137923823(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		String_t* L_7 = Single_ToString_m5736032((&V_1), /*hidden argument*/NULL);
		FsmString_t952858651 * L_8 = __this->get_style_12();
		NullCheck(L_8);
		String_t* L_9 = FsmString_get_Value_m872383149(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t2990928826_il2cpp_TypeInfo_var);
		GUIStyle_t2990928826 * L_10 = GUIStyle_op_Implicit_m169215436(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2977405297* L_11 = GUILayoutAction_get_LayoutOptions_m1855901188(__this, /*hidden argument*/NULL);
		String_t* L_12 = GUILayout_TextField_m2367652128(NULL /*static, unused*/, L_7, L_10, L_11, /*hidden argument*/NULL);
		float L_13 = Single_Parse_m3022284664(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		NullCheck(L_4);
		FsmFloat_set_Value_m1568963140(L_4, L_13, /*hidden argument*/NULL);
		goto IL_0092;
	}

IL_0064:
	{
		FsmFloat_t2134102846 * L_14 = __this->get_floatVariable_11();
		FsmFloat_t2134102846 * L_15 = __this->get_floatVariable_11();
		NullCheck(L_15);
		float L_16 = FsmFloat_get_Value_m4137923823(L_15, /*hidden argument*/NULL);
		V_2 = L_16;
		String_t* L_17 = Single_ToString_m5736032((&V_2), /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2977405297* L_18 = GUILayoutAction_get_LayoutOptions_m1855901188(__this, /*hidden argument*/NULL);
		String_t* L_19 = GUILayout_TextField_m3370910503(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		float L_20 = Single_Parse_m3022284664(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		NullCheck(L_14);
		FsmFloat_set_Value_m1568963140(L_14, L_20, /*hidden argument*/NULL);
	}

IL_0092:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		bool L_21 = GUI_get_changed_m1591686125(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00b7;
		}
	}
	{
		Fsm_t1527112426 * L_22 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_23 = __this->get_changedEvent_13();
		NullCheck(L_22);
		Fsm_Event_m625948263(L_22, L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		GUIUtility_ExitGUI_m2798995767(NULL /*static, unused*/, /*hidden argument*/NULL);
		goto IL_00bd;
	}

IL_00b7:
	{
		bool L_24 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_changed_m727947722(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
	}

IL_00bd:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutFloatLabel::.ctor()
extern "C"  void GUILayoutFloatLabel__ctor_m216781803 (GUILayoutFloatLabel_t2180325291 * __this, const MethodInfo* method)
{
	{
		GUILayoutAction__ctor_m2599973229(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutFloatLabel::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutFloatLabel_Reset_m2158182040_MetadataUsageId;
extern "C"  void GUILayoutFloatLabel_Reset_m2158182040 (GUILayoutFloatLabel_t2180325291 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutFloatLabel_Reset_m2158182040_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUILayoutAction_Reset_m246406170(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_prefix_11(L_1);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_3 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_style_13(L_3);
		__this->set_floatVariable_12((FsmFloat_t2134102846 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutFloatLabel::OnGUI()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIContent_t2094828418_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t2990928826_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutFloatLabel_OnGUI_m4007147749_MetadataUsageId;
extern "C"  void GUILayoutFloatLabel_OnGUI_m4007147749 (GUILayoutFloatLabel_t2180325291 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutFloatLabel_OnGUI_m4007147749_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmString_t952858651 * L_0 = __this->get_style_13();
		NullCheck(L_0);
		String_t* L_1 = FsmString_get_Value_m872383149(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004a;
		}
	}
	{
		FsmString_t952858651 * L_3 = __this->get_prefix_11();
		NullCheck(L_3);
		String_t* L_4 = FsmString_get_Value_m872383149(L_3, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_5 = __this->get_floatVariable_12();
		NullCheck(L_5);
		float L_6 = FsmFloat_get_Value_m4137923823(L_5, /*hidden argument*/NULL);
		float L_7 = L_6;
		Il2CppObject * L_8 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m389863537(NULL /*static, unused*/, L_4, L_8, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_10 = (GUIContent_t2094828418 *)il2cpp_codegen_object_new(GUIContent_t2094828418_il2cpp_TypeInfo_var);
		GUIContent__ctor_m174155123(L_10, L_9, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2977405297* L_11 = GUILayoutAction_get_LayoutOptions_m1855901188(__this, /*hidden argument*/NULL);
		GUILayout_Label_m3931056408(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		goto IL_008a;
	}

IL_004a:
	{
		FsmString_t952858651 * L_12 = __this->get_prefix_11();
		NullCheck(L_12);
		String_t* L_13 = FsmString_get_Value_m872383149(L_12, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_14 = __this->get_floatVariable_12();
		NullCheck(L_14);
		float L_15 = FsmFloat_get_Value_m4137923823(L_14, /*hidden argument*/NULL);
		float L_16 = L_15;
		Il2CppObject * L_17 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_16);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m389863537(NULL /*static, unused*/, L_13, L_17, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_19 = (GUIContent_t2094828418 *)il2cpp_codegen_object_new(GUIContent_t2094828418_il2cpp_TypeInfo_var);
		GUIContent__ctor_m174155123(L_19, L_18, /*hidden argument*/NULL);
		FsmString_t952858651 * L_20 = __this->get_style_13();
		NullCheck(L_20);
		String_t* L_21 = FsmString_get_Value_m872383149(L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t2990928826_il2cpp_TypeInfo_var);
		GUIStyle_t2990928826 * L_22 = GUIStyle_op_Implicit_m169215436(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2977405297* L_23 = GUILayoutAction_get_LayoutOptions_m1855901188(__this, /*hidden argument*/NULL);
		GUILayout_Label_m796535759(NULL /*static, unused*/, L_19, L_22, L_23, /*hidden argument*/NULL);
	}

IL_008a:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutHorizontalSlider::.ctor()
extern "C"  void GUILayoutHorizontalSlider__ctor_m1744088670 (GUILayoutHorizontalSlider_t3080788440 * __this, const MethodInfo* method)
{
	{
		GUILayoutAction__ctor_m2599973229(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutHorizontalSlider::Reset()
extern "C"  void GUILayoutHorizontalSlider_Reset_m3685488907 (GUILayoutHorizontalSlider_t3080788440 * __this, const MethodInfo* method)
{
	{
		GUILayoutAction_Reset_m246406170(__this, /*hidden argument*/NULL);
		__this->set_floatVariable_11((FsmFloat_t2134102846 *)NULL);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_leftValue_12(L_0);
		FsmFloat_t2134102846 * L_1 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (100.0f), /*hidden argument*/NULL);
		__this->set_rightValue_13(L_1);
		__this->set_changedEvent_14((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutHorizontalSlider::OnGUI()
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIUtility_t1028319349_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutHorizontalSlider_OnGUI_m1239487320_MetadataUsageId;
extern "C"  void GUILayoutHorizontalSlider_OnGUI_m1239487320 (GUILayoutHorizontalSlider_t3080788440 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutHorizontalSlider_OnGUI_m1239487320_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		bool L_0 = GUI_get_changed_m1591686125(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		GUI_set_changed_m727947722(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_1 = __this->get_floatVariable_11();
		if (!L_1)
		{
			goto IL_004e;
		}
	}
	{
		FsmFloat_t2134102846 * L_2 = __this->get_floatVariable_11();
		FsmFloat_t2134102846 * L_3 = __this->get_floatVariable_11();
		NullCheck(L_3);
		float L_4 = FsmFloat_get_Value_m4137923823(L_3, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_5 = __this->get_leftValue_12();
		NullCheck(L_5);
		float L_6 = FsmFloat_get_Value_m4137923823(L_5, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_7 = __this->get_rightValue_13();
		NullCheck(L_7);
		float L_8 = FsmFloat_get_Value_m4137923823(L_7, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2977405297* L_9 = GUILayoutAction_get_LayoutOptions_m1855901188(__this, /*hidden argument*/NULL);
		float L_10 = GUILayout_HorizontalSlider_m2607521747(NULL /*static, unused*/, L_4, L_6, L_8, L_9, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmFloat_set_Value_m1568963140(L_2, L_10, /*hidden argument*/NULL);
	}

IL_004e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		bool L_11 = GUI_get_changed_m1591686125(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0073;
		}
	}
	{
		Fsm_t1527112426 * L_12 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_13 = __this->get_changedEvent_14();
		NullCheck(L_12);
		Fsm_Event_m625948263(L_12, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		GUIUtility_ExitGUI_m2798995767(NULL /*static, unused*/, /*hidden argument*/NULL);
		goto IL_0079;
	}

IL_0073:
	{
		bool L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_changed_m727947722(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
	}

IL_0079:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutIntField::.ctor()
extern "C"  void GUILayoutIntField__ctor_m624781560 (GUILayoutIntField_t3766456830 * __this, const MethodInfo* method)
{
	{
		GUILayoutAction__ctor_m2599973229(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutIntField::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutIntField_Reset_m2566181797_MetadataUsageId;
extern "C"  void GUILayoutIntField_Reset_m2566181797 (GUILayoutIntField_t3766456830 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutIntField_Reset_m2566181797_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUILayoutAction_Reset_m246406170(__this, /*hidden argument*/NULL);
		__this->set_intVariable_11((FsmInt_t1596138449 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_style_12(L_1);
		__this->set_changedEvent_13((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutIntField::OnGUI()
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t2990928826_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIUtility_t1028319349_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutIntField_OnGUI_m120180210_MetadataUsageId;
extern "C"  void GUILayoutIntField_OnGUI_m120180210 (GUILayoutIntField_t3766456830 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutIntField_OnGUI_m120180210_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		bool L_0 = GUI_get_changed_m1591686125(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		GUI_set_changed_m727947722(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		FsmString_t952858651 * L_1 = __this->get_style_12();
		NullCheck(L_1);
		String_t* L_2 = FsmString_get_Value_m872383149(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0064;
		}
	}
	{
		FsmInt_t1596138449 * L_4 = __this->get_intVariable_11();
		FsmInt_t1596138449 * L_5 = __this->get_intVariable_11();
		NullCheck(L_5);
		int32_t L_6 = FsmInt_get_Value_m27059446(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		String_t* L_7 = Int32_ToString_m1286526384((&V_1), /*hidden argument*/NULL);
		FsmString_t952858651 * L_8 = __this->get_style_12();
		NullCheck(L_8);
		String_t* L_9 = FsmString_get_Value_m872383149(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t2990928826_il2cpp_TypeInfo_var);
		GUIStyle_t2990928826 * L_10 = GUIStyle_op_Implicit_m169215436(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2977405297* L_11 = GUILayoutAction_get_LayoutOptions_m1855901188(__this, /*hidden argument*/NULL);
		String_t* L_12 = GUILayout_TextField_m2367652128(NULL /*static, unused*/, L_7, L_10, L_11, /*hidden argument*/NULL);
		int32_t L_13 = Int32_Parse_m3837759498(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		NullCheck(L_4);
		FsmInt_set_Value_m2087583461(L_4, L_13, /*hidden argument*/NULL);
		goto IL_0092;
	}

IL_0064:
	{
		FsmInt_t1596138449 * L_14 = __this->get_intVariable_11();
		FsmInt_t1596138449 * L_15 = __this->get_intVariable_11();
		NullCheck(L_15);
		int32_t L_16 = FsmInt_get_Value_m27059446(L_15, /*hidden argument*/NULL);
		V_2 = L_16;
		String_t* L_17 = Int32_ToString_m1286526384((&V_2), /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2977405297* L_18 = GUILayoutAction_get_LayoutOptions_m1855901188(__this, /*hidden argument*/NULL);
		String_t* L_19 = GUILayout_TextField_m3370910503(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		int32_t L_20 = Int32_Parse_m3837759498(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		NullCheck(L_14);
		FsmInt_set_Value_m2087583461(L_14, L_20, /*hidden argument*/NULL);
	}

IL_0092:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		bool L_21 = GUI_get_changed_m1591686125(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00b7;
		}
	}
	{
		Fsm_t1527112426 * L_22 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_23 = __this->get_changedEvent_13();
		NullCheck(L_22);
		Fsm_Event_m625948263(L_22, L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		GUIUtility_ExitGUI_m2798995767(NULL /*static, unused*/, /*hidden argument*/NULL);
		goto IL_00bd;
	}

IL_00b7:
	{
		bool L_24 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_changed_m727947722(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
	}

IL_00bd:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutIntLabel::.ctor()
extern "C"  void GUILayoutIntLabel__ctor_m1968022590 (GUILayoutIntLabel_t3771756536 * __this, const MethodInfo* method)
{
	{
		GUILayoutAction__ctor_m2599973229(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutIntLabel::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutIntLabel_Reset_m3909422827_MetadataUsageId;
extern "C"  void GUILayoutIntLabel_Reset_m3909422827 (GUILayoutIntLabel_t3771756536 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutIntLabel_Reset_m3909422827_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUILayoutAction_Reset_m246406170(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_prefix_11(L_1);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_3 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_style_13(L_3);
		__this->set_intVariable_12((FsmInt_t1596138449 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutIntLabel::OnGUI()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIContent_t2094828418_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t2990928826_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutIntLabel_OnGUI_m1463421240_MetadataUsageId;
extern "C"  void GUILayoutIntLabel_OnGUI_m1463421240 (GUILayoutIntLabel_t3771756536 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutIntLabel_OnGUI_m1463421240_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmString_t952858651 * L_0 = __this->get_style_13();
		NullCheck(L_0);
		String_t* L_1 = FsmString_get_Value_m872383149(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004a;
		}
	}
	{
		FsmString_t952858651 * L_3 = __this->get_prefix_11();
		NullCheck(L_3);
		String_t* L_4 = FsmString_get_Value_m872383149(L_3, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_5 = __this->get_intVariable_12();
		NullCheck(L_5);
		int32_t L_6 = FsmInt_get_Value_m27059446(L_5, /*hidden argument*/NULL);
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m389863537(NULL /*static, unused*/, L_4, L_8, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_10 = (GUIContent_t2094828418 *)il2cpp_codegen_object_new(GUIContent_t2094828418_il2cpp_TypeInfo_var);
		GUIContent__ctor_m174155123(L_10, L_9, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2977405297* L_11 = GUILayoutAction_get_LayoutOptions_m1855901188(__this, /*hidden argument*/NULL);
		GUILayout_Label_m3931056408(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		goto IL_008a;
	}

IL_004a:
	{
		FsmString_t952858651 * L_12 = __this->get_prefix_11();
		NullCheck(L_12);
		String_t* L_13 = FsmString_get_Value_m872383149(L_12, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_14 = __this->get_intVariable_12();
		NullCheck(L_14);
		int32_t L_15 = FsmInt_get_Value_m27059446(L_14, /*hidden argument*/NULL);
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_16);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m389863537(NULL /*static, unused*/, L_13, L_17, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_19 = (GUIContent_t2094828418 *)il2cpp_codegen_object_new(GUIContent_t2094828418_il2cpp_TypeInfo_var);
		GUIContent__ctor_m174155123(L_19, L_18, /*hidden argument*/NULL);
		FsmString_t952858651 * L_20 = __this->get_style_13();
		NullCheck(L_20);
		String_t* L_21 = FsmString_get_Value_m872383149(L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t2990928826_il2cpp_TypeInfo_var);
		GUIStyle_t2990928826 * L_22 = GUIStyle_op_Implicit_m169215436(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2977405297* L_23 = GUILayoutAction_get_LayoutOptions_m1855901188(__this, /*hidden argument*/NULL);
		GUILayout_Label_m796535759(NULL /*static, unused*/, L_19, L_22, L_23, /*hidden argument*/NULL);
	}

IL_008a:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutLabel::.ctor()
extern "C"  void GUILayoutLabel__ctor_m781452719 (GUILayoutLabel_t3579067543 * __this, const MethodInfo* method)
{
	{
		GUILayoutAction__ctor_m2599973229(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutLabel::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutLabel_Reset_m2722852956_MetadataUsageId;
extern "C"  void GUILayoutLabel_Reset_m2722852956 (GUILayoutLabel_t3579067543 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutLabel_Reset_m2722852956_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUILayoutAction_Reset_m246406170(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_text_12(L_1);
		__this->set_image_11((FsmTexture_t3073272573 *)NULL);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_3 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_tooltip_13(L_3);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_5 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		__this->set_style_14(L_5);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutLabel::OnGUI()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIContent_t2094828418_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t2990928826_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutLabel_OnGUI_m276851369_MetadataUsageId;
extern "C"  void GUILayoutLabel_OnGUI_m276851369 (GUILayoutLabel_t3579067543 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutLabel_OnGUI_m276851369_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmString_t952858651 * L_0 = __this->get_style_14();
		NullCheck(L_0);
		String_t* L_1 = FsmString_get_Value_m872383149(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004b;
		}
	}
	{
		FsmString_t952858651 * L_3 = __this->get_text_12();
		NullCheck(L_3);
		String_t* L_4 = FsmString_get_Value_m872383149(L_3, /*hidden argument*/NULL);
		FsmTexture_t3073272573 * L_5 = __this->get_image_11();
		NullCheck(L_5);
		Texture_t2526458961 * L_6 = FsmTexture_get_Value_m3156202285(L_5, /*hidden argument*/NULL);
		FsmString_t952858651 * L_7 = __this->get_tooltip_13();
		NullCheck(L_7);
		String_t* L_8 = FsmString_get_Value_m872383149(L_7, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_9 = (GUIContent_t2094828418 *)il2cpp_codegen_object_new(GUIContent_t2094828418_il2cpp_TypeInfo_var);
		GUIContent__ctor_m905567255(L_9, L_4, L_6, L_8, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2977405297* L_10 = GUILayoutAction_get_LayoutOptions_m1855901188(__this, /*hidden argument*/NULL);
		GUILayout_Label_m3931056408(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		goto IL_008c;
	}

IL_004b:
	{
		FsmString_t952858651 * L_11 = __this->get_text_12();
		NullCheck(L_11);
		String_t* L_12 = FsmString_get_Value_m872383149(L_11, /*hidden argument*/NULL);
		FsmTexture_t3073272573 * L_13 = __this->get_image_11();
		NullCheck(L_13);
		Texture_t2526458961 * L_14 = FsmTexture_get_Value_m3156202285(L_13, /*hidden argument*/NULL);
		FsmString_t952858651 * L_15 = __this->get_tooltip_13();
		NullCheck(L_15);
		String_t* L_16 = FsmString_get_Value_m872383149(L_15, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_17 = (GUIContent_t2094828418 *)il2cpp_codegen_object_new(GUIContent_t2094828418_il2cpp_TypeInfo_var);
		GUIContent__ctor_m905567255(L_17, L_12, L_14, L_16, /*hidden argument*/NULL);
		FsmString_t952858651 * L_18 = __this->get_style_14();
		NullCheck(L_18);
		String_t* L_19 = FsmString_get_Value_m872383149(L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t2990928826_il2cpp_TypeInfo_var);
		GUIStyle_t2990928826 * L_20 = GUIStyle_op_Implicit_m169215436(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2977405297* L_21 = GUILayoutAction_get_LayoutOptions_m1855901188(__this, /*hidden argument*/NULL);
		GUILayout_Label_m796535759(NULL /*static, unused*/, L_17, L_20, L_21, /*hidden argument*/NULL);
	}

IL_008c:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutPasswordField::.ctor()
extern "C"  void GUILayoutPasswordField__ctor_m1810662884 (GUILayoutPasswordField_t886514882 * __this, const MethodInfo* method)
{
	{
		GUILayoutAction__ctor_m2599973229(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutPasswordField::Reset()
extern Il2CppCodeGenString* _stringLiteral942981037;
extern Il2CppCodeGenString* _stringLiteral42;
extern const uint32_t GUILayoutPasswordField_Reset_m3752063121_MetadataUsageId;
extern "C"  void GUILayoutPasswordField_Reset_m3752063121 (GUILayoutPasswordField_t886514882 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutPasswordField_Reset_m3752063121_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_text_11((FsmString_t952858651 *)NULL);
		FsmInt_t1596138449 * L_0 = FsmInt_op_Implicit_m1006909518(NULL /*static, unused*/, ((int32_t)25), /*hidden argument*/NULL);
		__this->set_maxLength_12(L_0);
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, _stringLiteral942981037, /*hidden argument*/NULL);
		__this->set_style_13(L_1);
		FsmString_t952858651 * L_2 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, _stringLiteral42, /*hidden argument*/NULL);
		__this->set_mask_15(L_2);
		__this->set_changedEvent_14((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutPasswordField::OnGUI()
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t2990928826_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIUtility_t1028319349_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutPasswordField_OnGUI_m1306061534_MetadataUsageId;
extern "C"  void GUILayoutPasswordField_OnGUI_m1306061534 (GUILayoutPasswordField_t886514882 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutPasswordField_OnGUI_m1306061534_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		bool L_0 = GUI_get_changed_m1591686125(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		GUI_set_changed_m727947722(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		FsmString_t952858651 * L_1 = __this->get_text_11();
		FsmString_t952858651 * L_2 = __this->get_text_11();
		NullCheck(L_2);
		String_t* L_3 = FsmString_get_Value_m872383149(L_2, /*hidden argument*/NULL);
		FsmString_t952858651 * L_4 = __this->get_mask_15();
		NullCheck(L_4);
		String_t* L_5 = FsmString_get_Value_m872383149(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Il2CppChar L_6 = String_get_Chars_m3015341861(L_5, 0, /*hidden argument*/NULL);
		FsmString_t952858651 * L_7 = __this->get_style_13();
		NullCheck(L_7);
		String_t* L_8 = FsmString_get_Value_m872383149(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t2990928826_il2cpp_TypeInfo_var);
		GUIStyle_t2990928826 * L_9 = GUIStyle_op_Implicit_m169215436(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2977405297* L_10 = GUILayoutAction_get_LayoutOptions_m1855901188(__this, /*hidden argument*/NULL);
		String_t* L_11 = GUILayout_PasswordField_m487636731(NULL /*static, unused*/, L_3, L_6, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_1);
		FsmString_set_Value_m829393196(L_1, L_11, /*hidden argument*/NULL);
		bool L_12 = GUI_get_changed_m1591686125(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0073;
		}
	}
	{
		Fsm_t1527112426 * L_13 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_14 = __this->get_changedEvent_14();
		NullCheck(L_13);
		Fsm_Event_m625948263(L_13, L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		GUIUtility_ExitGUI_m2798995767(NULL /*static, unused*/, /*hidden argument*/NULL);
		goto IL_0079;
	}

IL_0073:
	{
		bool L_15 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_changed_m727947722(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
	}

IL_0079:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutRepeatButton::.ctor()
extern "C"  void GUILayoutRepeatButton__ctor_m897391830 (GUILayoutRepeatButton_t3980277856 * __this, const MethodInfo* method)
{
	{
		GUILayoutAction__ctor_m2599973229(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutRepeatButton::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutRepeatButton_Reset_m2838792067_MetadataUsageId;
extern "C"  void GUILayoutRepeatButton_Reset_m2838792067 (GUILayoutRepeatButton_t3980277856 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutRepeatButton_Reset_m2838792067_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUILayoutAction_Reset_m246406170(__this, /*hidden argument*/NULL);
		__this->set_sendEvent_11((FsmEvent_t2133468028 *)NULL);
		__this->set_storeButtonState_12((FsmBool_t1075959796 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_text_14(L_1);
		__this->set_image_13((FsmTexture_t3073272573 *)NULL);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_3 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_tooltip_15(L_3);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_5 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		__this->set_style_16(L_5);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutRepeatButton::OnGUI()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIContent_t2094828418_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t2990928826_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutRepeatButton_OnGUI_m392790480_MetadataUsageId;
extern "C"  void GUILayoutRepeatButton_OnGUI_m392790480 (GUILayoutRepeatButton_t3980277856 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutRepeatButton_OnGUI_m392790480_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		FsmString_t952858651 * L_0 = __this->get_style_16();
		NullCheck(L_0);
		String_t* L_1 = FsmString_get_Value_m872383149(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004c;
		}
	}
	{
		FsmString_t952858651 * L_3 = __this->get_text_14();
		NullCheck(L_3);
		String_t* L_4 = FsmString_get_Value_m872383149(L_3, /*hidden argument*/NULL);
		FsmTexture_t3073272573 * L_5 = __this->get_image_13();
		NullCheck(L_5);
		Texture_t2526458961 * L_6 = FsmTexture_get_Value_m3156202285(L_5, /*hidden argument*/NULL);
		FsmString_t952858651 * L_7 = __this->get_tooltip_15();
		NullCheck(L_7);
		String_t* L_8 = FsmString_get_Value_m872383149(L_7, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_9 = (GUIContent_t2094828418 *)il2cpp_codegen_object_new(GUIContent_t2094828418_il2cpp_TypeInfo_var);
		GUIContent__ctor_m905567255(L_9, L_4, L_6, L_8, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2977405297* L_10 = GUILayoutAction_get_LayoutOptions_m1855901188(__this, /*hidden argument*/NULL);
		bool L_11 = GUILayout_RepeatButton_m32698665(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		goto IL_008e;
	}

IL_004c:
	{
		FsmString_t952858651 * L_12 = __this->get_text_14();
		NullCheck(L_12);
		String_t* L_13 = FsmString_get_Value_m872383149(L_12, /*hidden argument*/NULL);
		FsmTexture_t3073272573 * L_14 = __this->get_image_13();
		NullCheck(L_14);
		Texture_t2526458961 * L_15 = FsmTexture_get_Value_m3156202285(L_14, /*hidden argument*/NULL);
		FsmString_t952858651 * L_16 = __this->get_tooltip_15();
		NullCheck(L_16);
		String_t* L_17 = FsmString_get_Value_m872383149(L_16, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_18 = (GUIContent_t2094828418 *)il2cpp_codegen_object_new(GUIContent_t2094828418_il2cpp_TypeInfo_var);
		GUIContent__ctor_m905567255(L_18, L_13, L_15, L_17, /*hidden argument*/NULL);
		FsmString_t952858651 * L_19 = __this->get_style_16();
		NullCheck(L_19);
		String_t* L_20 = FsmString_get_Value_m872383149(L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t2990928826_il2cpp_TypeInfo_var);
		GUIStyle_t2990928826 * L_21 = GUIStyle_op_Implicit_m169215436(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2977405297* L_22 = GUILayoutAction_get_LayoutOptions_m1855901188(__this, /*hidden argument*/NULL);
		bool L_23 = GUILayout_RepeatButton_m3026503774(NULL /*static, unused*/, L_18, L_21, L_22, /*hidden argument*/NULL);
		V_0 = L_23;
	}

IL_008e:
	{
		bool L_24 = V_0;
		if (!L_24)
		{
			goto IL_00a5;
		}
	}
	{
		Fsm_t1527112426 * L_25 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_26 = __this->get_sendEvent_11();
		NullCheck(L_25);
		Fsm_Event_m625948263(L_25, L_26, /*hidden argument*/NULL);
	}

IL_00a5:
	{
		FsmBool_t1075959796 * L_27 = __this->get_storeButtonState_12();
		bool L_28 = V_0;
		NullCheck(L_27);
		FsmBool_set_Value_m1126216340(L_27, L_28, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutSpace::.ctor()
extern "C"  void GUILayoutSpace__ctor_m1976879069 (GUILayoutSpace_t3585978025 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutSpace::Reset()
extern "C"  void GUILayoutSpace_Reset_m3918279306 (GUILayoutSpace_t3585978025 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (10.0f), /*hidden argument*/NULL);
		__this->set_space_9(L_0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutSpace::OnGUI()
extern "C"  void GUILayoutSpace_OnGUI_m1472277719 (GUILayoutSpace_t3585978025 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = __this->get_space_9();
		NullCheck(L_0);
		float L_1 = FsmFloat_get_Value_m4137923823(L_0, /*hidden argument*/NULL);
		GUILayout_Space_m559926739(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutTextField::.ctor()
extern "C"  void GUILayoutTextField__ctor_m4138203382 (GUILayoutTextField_t3774275952 * __this, const MethodInfo* method)
{
	{
		GUILayoutAction__ctor_m2599973229(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutTextField::Reset()
extern Il2CppCodeGenString* _stringLiteral942981037;
extern const uint32_t GUILayoutTextField_Reset_m1784636323_MetadataUsageId;
extern "C"  void GUILayoutTextField_Reset_m1784636323 (GUILayoutTextField_t3774275952 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutTextField_Reset_m1784636323_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUILayoutAction_Reset_m246406170(__this, /*hidden argument*/NULL);
		__this->set_text_11((FsmString_t952858651 *)NULL);
		FsmInt_t1596138449 * L_0 = FsmInt_op_Implicit_m1006909518(NULL /*static, unused*/, ((int32_t)25), /*hidden argument*/NULL);
		__this->set_maxLength_12(L_0);
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, _stringLiteral942981037, /*hidden argument*/NULL);
		__this->set_style_13(L_1);
		__this->set_changedEvent_14((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutTextField::OnGUI()
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t2990928826_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIUtility_t1028319349_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutTextField_OnGUI_m3633602032_MetadataUsageId;
extern "C"  void GUILayoutTextField_OnGUI_m3633602032 (GUILayoutTextField_t3774275952 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutTextField_OnGUI_m3633602032_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		bool L_0 = GUI_get_changed_m1591686125(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		GUI_set_changed_m727947722(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		FsmString_t952858651 * L_1 = __this->get_text_11();
		FsmString_t952858651 * L_2 = __this->get_text_11();
		NullCheck(L_2);
		String_t* L_3 = FsmString_get_Value_m872383149(L_2, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_4 = __this->get_maxLength_12();
		NullCheck(L_4);
		int32_t L_5 = FsmInt_get_Value_m27059446(L_4, /*hidden argument*/NULL);
		FsmString_t952858651 * L_6 = __this->get_style_13();
		NullCheck(L_6);
		String_t* L_7 = FsmString_get_Value_m872383149(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t2990928826_il2cpp_TypeInfo_var);
		GUIStyle_t2990928826 * L_8 = GUIStyle_op_Implicit_m169215436(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2977405297* L_9 = GUILayoutAction_get_LayoutOptions_m1855901188(__this, /*hidden argument*/NULL);
		String_t* L_10 = GUILayout_TextField_m3429491049(NULL /*static, unused*/, L_3, L_5, L_8, L_9, /*hidden argument*/NULL);
		NullCheck(L_1);
		FsmString_set_Value_m829393196(L_1, L_10, /*hidden argument*/NULL);
		bool L_11 = GUI_get_changed_m1591686125(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_006d;
		}
	}
	{
		Fsm_t1527112426 * L_12 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_13 = __this->get_changedEvent_14();
		NullCheck(L_12);
		Fsm_Event_m625948263(L_12, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		GUIUtility_ExitGUI_m2798995767(NULL /*static, unused*/, /*hidden argument*/NULL);
		goto IL_0073;
	}

IL_006d:
	{
		bool L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_changed_m727947722(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
	}

IL_0073:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutTextLabel::.ctor()
extern "C"  void GUILayoutTextLabel__ctor_m1186477116 (GUILayoutTextLabel_t3779575658 * __this, const MethodInfo* method)
{
	{
		GUILayoutAction__ctor_m2599973229(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutTextLabel::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutTextLabel_Reset_m3127877353_MetadataUsageId;
extern "C"  void GUILayoutTextLabel_Reset_m3127877353 (GUILayoutTextLabel_t3779575658 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutTextLabel_Reset_m3127877353_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUILayoutAction_Reset_m246406170(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_text_11(L_1);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_3 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_style_12(L_3);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutTextLabel::OnGUI()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIContent_t2094828418_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t2990928826_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutTextLabel_OnGUI_m681875766_MetadataUsageId;
extern "C"  void GUILayoutTextLabel_OnGUI_m681875766 (GUILayoutTextLabel_t3779575658 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutTextLabel_OnGUI_m681875766_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmString_t952858651 * L_0 = __this->get_style_12();
		NullCheck(L_0);
		String_t* L_1 = FsmString_get_Value_m872383149(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0035;
		}
	}
	{
		FsmString_t952858651 * L_3 = __this->get_text_11();
		NullCheck(L_3);
		String_t* L_4 = FsmString_get_Value_m872383149(L_3, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_5 = (GUIContent_t2094828418 *)il2cpp_codegen_object_new(GUIContent_t2094828418_il2cpp_TypeInfo_var);
		GUIContent__ctor_m174155123(L_5, L_4, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2977405297* L_6 = GUILayoutAction_get_LayoutOptions_m1855901188(__this, /*hidden argument*/NULL);
		GUILayout_Label_m3931056408(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		goto IL_0060;
	}

IL_0035:
	{
		FsmString_t952858651 * L_7 = __this->get_text_11();
		NullCheck(L_7);
		String_t* L_8 = FsmString_get_Value_m872383149(L_7, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_9 = (GUIContent_t2094828418 *)il2cpp_codegen_object_new(GUIContent_t2094828418_il2cpp_TypeInfo_var);
		GUIContent__ctor_m174155123(L_9, L_8, /*hidden argument*/NULL);
		FsmString_t952858651 * L_10 = __this->get_style_12();
		NullCheck(L_10);
		String_t* L_11 = FsmString_get_Value_m872383149(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t2990928826_il2cpp_TypeInfo_var);
		GUIStyle_t2990928826 * L_12 = GUIStyle_op_Implicit_m169215436(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2977405297* L_13 = GUILayoutAction_get_LayoutOptions_m1855901188(__this, /*hidden argument*/NULL);
		GUILayout_Label_m796535759(NULL /*static, unused*/, L_9, L_12, L_13, /*hidden argument*/NULL);
	}

IL_0060:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutToggle::.ctor()
extern "C"  void GUILayoutToggle__ctor_m1532565007 (GUILayoutToggle_t3170064647 * __this, const MethodInfo* method)
{
	{
		GUILayoutAction__ctor_m2599973229(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutToggle::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2510530420;
extern const uint32_t GUILayoutToggle_Reset_m3473965244_MetadataUsageId;
extern "C"  void GUILayoutToggle_Reset_m3473965244 (GUILayoutToggle_t3170064647 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutToggle_Reset_m3473965244_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUILayoutAction_Reset_m246406170(__this, /*hidden argument*/NULL);
		__this->set_storeButtonState_11((FsmBool_t1075959796 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_text_13(L_1);
		__this->set_image_12((FsmTexture_t3073272573 *)NULL);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_3 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_tooltip_14(L_3);
		FsmString_t952858651 * L_4 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, _stringLiteral2510530420, /*hidden argument*/NULL);
		__this->set_style_15(L_4);
		__this->set_changedEvent_16((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutToggle::OnGUI()
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIContent_t2094828418_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t2990928826_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIUtility_t1028319349_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutToggle_OnGUI_m1027963657_MetadataUsageId;
extern "C"  void GUILayoutToggle_OnGUI_m1027963657 (GUILayoutToggle_t3170064647 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutToggle_OnGUI_m1027963657_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		bool L_0 = GUI_get_changed_m1591686125(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		GUI_set_changed_m727947722(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_1 = __this->get_storeButtonState_11();
		FsmBool_t1075959796 * L_2 = __this->get_storeButtonState_11();
		NullCheck(L_2);
		bool L_3 = FsmBool_get_Value_m3101329097(L_2, /*hidden argument*/NULL);
		FsmString_t952858651 * L_4 = __this->get_text_13();
		NullCheck(L_4);
		String_t* L_5 = FsmString_get_Value_m872383149(L_4, /*hidden argument*/NULL);
		FsmTexture_t3073272573 * L_6 = __this->get_image_12();
		NullCheck(L_6);
		Texture_t2526458961 * L_7 = FsmTexture_get_Value_m3156202285(L_6, /*hidden argument*/NULL);
		FsmString_t952858651 * L_8 = __this->get_tooltip_14();
		NullCheck(L_8);
		String_t* L_9 = FsmString_get_Value_m872383149(L_8, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_10 = (GUIContent_t2094828418 *)il2cpp_codegen_object_new(GUIContent_t2094828418_il2cpp_TypeInfo_var);
		GUIContent__ctor_m905567255(L_10, L_5, L_7, L_9, /*hidden argument*/NULL);
		FsmString_t952858651 * L_11 = __this->get_style_15();
		NullCheck(L_11);
		String_t* L_12 = FsmString_get_Value_m872383149(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t2990928826_il2cpp_TypeInfo_var);
		GUIStyle_t2990928826 * L_13 = GUIStyle_op_Implicit_m169215436(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2977405297* L_14 = GUILayoutAction_get_LayoutOptions_m1855901188(__this, /*hidden argument*/NULL);
		bool L_15 = GUILayout_Toggle_m4174309060(NULL /*static, unused*/, L_3, L_10, L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_1);
		FsmBool_set_Value_m1126216340(L_1, L_15, /*hidden argument*/NULL);
		bool L_16 = GUI_get_changed_m1591686125(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0088;
		}
	}
	{
		Fsm_t1527112426 * L_17 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_18 = __this->get_changedEvent_16();
		NullCheck(L_17);
		Fsm_Event_m625948263(L_17, L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		GUIUtility_ExitGUI_m2798995767(NULL /*static, unused*/, /*hidden argument*/NULL);
		goto IL_008e;
	}

IL_0088:
	{
		bool L_19 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_changed_m727947722(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
	}

IL_008e:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutToolbar::.ctor()
extern "C"  void GUILayoutToolbar__ctor_m1694251272 (GUILayoutToolbar_t3141290782 * __this, const MethodInfo* method)
{
	{
		GUILayoutAction__ctor_m2599973229(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIContent[] HutongGames.PlayMaker.Actions.GUILayoutToolbar::get_Contents()
extern Il2CppClass* GUIContentU5BU5D_t3588725815_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIContent_t2094828418_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutToolbar_get_Contents_m1153165093_MetadataUsageId;
extern "C"  GUIContentU5BU5D_t3588725815* GUILayoutToolbar_get_Contents_m1153165093 (GUILayoutToolbar_t3141290782 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutToolbar_get_Contents_m1153165093_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		GUIContentU5BU5D_t3588725815* L_0 = __this->get_contents_18();
		if (L_0)
		{
			goto IL_00e3;
		}
	}
	{
		FsmInt_t1596138449 * L_1 = __this->get_numButtons_11();
		NullCheck(L_1);
		int32_t L_2 = FsmInt_get_Value_m27059446(L_1, /*hidden argument*/NULL);
		__this->set_contents_18(((GUIContentU5BU5D_t3588725815*)SZArrayNew(GUIContentU5BU5D_t3588725815_il2cpp_TypeInfo_var, (uint32_t)L_2)));
		V_0 = 0;
		goto IL_0039;
	}

IL_0028:
	{
		GUIContentU5BU5D_t3588725815* L_3 = __this->get_contents_18();
		int32_t L_4 = V_0;
		GUIContent_t2094828418 * L_5 = (GUIContent_t2094828418 *)il2cpp_codegen_object_new(GUIContent_t2094828418_il2cpp_TypeInfo_var);
		GUIContent__ctor_m923375087(L_5, /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		ArrayElementTypeCheck (L_3, L_5);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (GUIContent_t2094828418 *)L_5);
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0039:
	{
		int32_t L_7 = V_0;
		FsmInt_t1596138449 * L_8 = __this->get_numButtons_11();
		NullCheck(L_8);
		int32_t L_9 = FsmInt_get_Value_m27059446(L_8, /*hidden argument*/NULL);
		if ((((int32_t)L_7) < ((int32_t)L_9)))
		{
			goto IL_0028;
		}
	}
	{
		V_1 = 0;
		goto IL_006f;
	}

IL_0051:
	{
		GUIContentU5BU5D_t3588725815* L_10 = __this->get_contents_18();
		int32_t L_11 = V_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		GUIContent_t2094828418 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		FsmTextureU5BU5D_t997957744* L_14 = __this->get_imagesArray_14();
		int32_t L_15 = V_1;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		FsmTexture_t3073272573 * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		NullCheck(L_17);
		Texture_t2526458961 * L_18 = FsmTexture_get_Value_m3156202285(L_17, /*hidden argument*/NULL);
		NullCheck(L_13);
		GUIContent_set_image_m3694827683(L_13, L_18, /*hidden argument*/NULL);
		int32_t L_19 = V_1;
		V_1 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_006f:
	{
		int32_t L_20 = V_1;
		FsmTextureU5BU5D_t997957744* L_21 = __this->get_imagesArray_14();
		NullCheck(L_21);
		if ((((int32_t)L_20) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_21)->max_length)))))))
		{
			goto IL_0051;
		}
	}
	{
		V_2 = 0;
		goto IL_00a2;
	}

IL_0084:
	{
		GUIContentU5BU5D_t3588725815* L_22 = __this->get_contents_18();
		int32_t L_23 = V_2;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = L_23;
		GUIContent_t2094828418 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		FsmStringU5BU5D_t2523845914* L_26 = __this->get_textsArray_15();
		int32_t L_27 = V_2;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		int32_t L_28 = L_27;
		FsmString_t952858651 * L_29 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		NullCheck(L_29);
		String_t* L_30 = FsmString_get_Value_m872383149(L_29, /*hidden argument*/NULL);
		NullCheck(L_25);
		GUIContent_set_text_m1575840163(L_25, L_30, /*hidden argument*/NULL);
		int32_t L_31 = V_2;
		V_2 = ((int32_t)((int32_t)L_31+(int32_t)1));
	}

IL_00a2:
	{
		int32_t L_32 = V_2;
		FsmStringU5BU5D_t2523845914* L_33 = __this->get_textsArray_15();
		NullCheck(L_33);
		if ((((int32_t)L_32) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_33)->max_length)))))))
		{
			goto IL_0084;
		}
	}
	{
		V_3 = 0;
		goto IL_00d5;
	}

IL_00b7:
	{
		GUIContentU5BU5D_t3588725815* L_34 = __this->get_contents_18();
		int32_t L_35 = V_3;
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, L_35);
		int32_t L_36 = L_35;
		GUIContent_t2094828418 * L_37 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_36));
		FsmStringU5BU5D_t2523845914* L_38 = __this->get_tooltipsArray_16();
		int32_t L_39 = V_3;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, L_39);
		int32_t L_40 = L_39;
		FsmString_t952858651 * L_41 = (L_38)->GetAt(static_cast<il2cpp_array_size_t>(L_40));
		NullCheck(L_41);
		String_t* L_42 = FsmString_get_Value_m872383149(L_41, /*hidden argument*/NULL);
		NullCheck(L_37);
		GUIContent_set_tooltip_m3547225103(L_37, L_42, /*hidden argument*/NULL);
		int32_t L_43 = V_3;
		V_3 = ((int32_t)((int32_t)L_43+(int32_t)1));
	}

IL_00d5:
	{
		int32_t L_44 = V_3;
		FsmStringU5BU5D_t2523845914* L_45 = __this->get_tooltipsArray_16();
		NullCheck(L_45);
		if ((((int32_t)L_44) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_45)->max_length)))))))
		{
			goto IL_00b7;
		}
	}

IL_00e3:
	{
		GUIContentU5BU5D_t3588725815* L_46 = __this->get_contents_18();
		return L_46;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutToolbar::Reset()
extern Il2CppClass* FsmEventU5BU5D_t2862142229_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmTextureU5BU5D_t997957744_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmStringU5BU5D_t2523845914_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2001146706;
extern const uint32_t GUILayoutToolbar_Reset_m3635651509_MetadataUsageId;
extern "C"  void GUILayoutToolbar_Reset_m3635651509 (GUILayoutToolbar_t3141290782 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutToolbar_Reset_m3635651509_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUILayoutAction_Reset_m246406170(__this, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_0 = FsmInt_op_Implicit_m1006909518(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		__this->set_numButtons_11(L_0);
		__this->set_selectedButton_12((FsmInt_t1596138449 *)NULL);
		__this->set_buttonEventsArray_13(((FsmEventU5BU5D_t2862142229*)SZArrayNew(FsmEventU5BU5D_t2862142229_il2cpp_TypeInfo_var, (uint32_t)0)));
		__this->set_imagesArray_14(((FsmTextureU5BU5D_t997957744*)SZArrayNew(FsmTextureU5BU5D_t997957744_il2cpp_TypeInfo_var, (uint32_t)0)));
		__this->set_tooltipsArray_16(((FsmStringU5BU5D_t2523845914*)SZArrayNew(FsmStringU5BU5D_t2523845914_il2cpp_TypeInfo_var, (uint32_t)0)));
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, _stringLiteral2001146706, /*hidden argument*/NULL);
		__this->set_style_17(L_1);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutToolbar::OnEnter()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutToolbar_OnEnter_m4069563103_MetadataUsageId;
extern "C"  void GUILayoutToolbar_OnEnter_m4069563103 (GUILayoutToolbar_t3141290782 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutToolbar_OnEnter_m4069563103_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = VirtFuncInvoker0< String_t* >::Invoke(45 /* System.String HutongGames.PlayMaker.Actions.GUILayoutToolbar::ErrorCheck() */, __this);
		V_0 = L_0;
		String_t* L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001f;
		}
	}
	{
		String_t* L_3 = V_0;
		FsmStateAction_LogError_m3478223492(__this, L_3, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutToolbar::OnGUI()
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t2990928826_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIUtility_t1028319349_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutToolbar_OnGUI_m1189649922_MetadataUsageId;
extern "C"  void GUILayoutToolbar_OnGUI_m1189649922 (GUILayoutToolbar_t3141290782 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutToolbar_OnGUI_m1189649922_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		bool L_0 = GUI_get_changed_m1591686125(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		GUI_set_changed_m727947722(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_1 = __this->get_selectedButton_12();
		FsmInt_t1596138449 * L_2 = __this->get_selectedButton_12();
		NullCheck(L_2);
		int32_t L_3 = FsmInt_get_Value_m27059446(L_2, /*hidden argument*/NULL);
		GUIContentU5BU5D_t3588725815* L_4 = GUILayoutToolbar_get_Contents_m1153165093(__this, /*hidden argument*/NULL);
		FsmString_t952858651 * L_5 = __this->get_style_17();
		NullCheck(L_5);
		String_t* L_6 = FsmString_get_Value_m872383149(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t2990928826_il2cpp_TypeInfo_var);
		GUIStyle_t2990928826 * L_7 = GUIStyle_op_Implicit_m169215436(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2977405297* L_8 = GUILayoutAction_get_LayoutOptions_m1855901188(__this, /*hidden argument*/NULL);
		int32_t L_9 = GUILayout_Toolbar_m1916473871(NULL /*static, unused*/, L_3, L_4, L_7, L_8, /*hidden argument*/NULL);
		NullCheck(L_1);
		FsmInt_set_Value_m2087583461(L_1, L_9, /*hidden argument*/NULL);
		bool L_10 = GUI_get_changed_m1591686125(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_008c;
		}
	}
	{
		FsmInt_t1596138449 * L_11 = __this->get_selectedButton_12();
		NullCheck(L_11);
		int32_t L_12 = FsmInt_get_Value_m27059446(L_11, /*hidden argument*/NULL);
		FsmEventU5BU5D_t2862142229* L_13 = __this->get_buttonEventsArray_13();
		NullCheck(L_13);
		if ((((int32_t)L_12) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length)))))))
		{
			goto IL_0087;
		}
	}
	{
		Fsm_t1527112426 * L_14 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEventU5BU5D_t2862142229* L_15 = __this->get_buttonEventsArray_13();
		FsmInt_t1596138449 * L_16 = __this->get_selectedButton_12();
		NullCheck(L_16);
		int32_t L_17 = FsmInt_get_Value_m27059446(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_17);
		int32_t L_18 = L_17;
		FsmEvent_t2133468028 * L_19 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		NullCheck(L_14);
		Fsm_Event_m625948263(L_14, L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		GUIUtility_ExitGUI_m2798995767(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0087:
	{
		goto IL_0092;
	}

IL_008c:
	{
		bool L_20 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_changed_m727947722(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
	}

IL_0092:
	{
		return;
	}
}
// System.String HutongGames.PlayMaker.Actions.GUILayoutToolbar::ErrorCheck()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1901741343;
extern Il2CppCodeGenString* _stringLiteral2894577841;
extern Il2CppCodeGenString* _stringLiteral2583251399;
extern const uint32_t GUILayoutToolbar_ErrorCheck_m1656665727_MetadataUsageId;
extern "C"  String_t* GUILayoutToolbar_ErrorCheck_m1656665727 (GUILayoutToolbar_t3141290782 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutToolbar_ErrorCheck_m1656665727_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		FsmTextureU5BU5D_t997957744* L_1 = __this->get_imagesArray_14();
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0038;
		}
	}
	{
		FsmTextureU5BU5D_t997957744* L_2 = __this->get_imagesArray_14();
		NullCheck(L_2);
		FsmInt_t1596138449 * L_3 = __this->get_numButtons_11();
		NullCheck(L_3);
		int32_t L_4 = FsmInt_get_Value_m27059446(L_3, /*hidden argument*/NULL);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))) == ((int32_t)L_4)))
		{
			goto IL_0038;
		}
	}
	{
		String_t* L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m138640077(NULL /*static, unused*/, L_5, _stringLiteral1901741343, /*hidden argument*/NULL);
		V_0 = L_6;
	}

IL_0038:
	{
		FsmStringU5BU5D_t2523845914* L_7 = __this->get_textsArray_15();
		NullCheck(L_7);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_006a;
		}
	}
	{
		FsmStringU5BU5D_t2523845914* L_8 = __this->get_textsArray_15();
		NullCheck(L_8);
		FsmInt_t1596138449 * L_9 = __this->get_numButtons_11();
		NullCheck(L_9);
		int32_t L_10 = FsmInt_get_Value_m27059446(L_9, /*hidden argument*/NULL);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length))))) == ((int32_t)L_10)))
		{
			goto IL_006a;
		}
	}
	{
		String_t* L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m138640077(NULL /*static, unused*/, L_11, _stringLiteral2894577841, /*hidden argument*/NULL);
		V_0 = L_12;
	}

IL_006a:
	{
		FsmStringU5BU5D_t2523845914* L_13 = __this->get_tooltipsArray_16();
		NullCheck(L_13);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_009c;
		}
	}
	{
		FsmStringU5BU5D_t2523845914* L_14 = __this->get_tooltipsArray_16();
		NullCheck(L_14);
		FsmInt_t1596138449 * L_15 = __this->get_numButtons_11();
		NullCheck(L_15);
		int32_t L_16 = FsmInt_get_Value_m27059446(L_15, /*hidden argument*/NULL);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length))))) == ((int32_t)L_16)))
		{
			goto IL_009c;
		}
	}
	{
		String_t* L_17 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m138640077(NULL /*static, unused*/, L_17, _stringLiteral2583251399, /*hidden argument*/NULL);
		V_0 = L_18;
	}

IL_009c:
	{
		String_t* L_19 = V_0;
		return L_19;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutVerticalSlider::.ctor()
extern "C"  void GUILayoutVerticalSlider__ctor_m2221317324 (GUILayoutVerticalSlider_t1513124266 * __this, const MethodInfo* method)
{
	{
		GUILayoutAction__ctor_m2599973229(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutVerticalSlider::Reset()
extern "C"  void GUILayoutVerticalSlider_Reset_m4162717561 (GUILayoutVerticalSlider_t1513124266 * __this, const MethodInfo* method)
{
	{
		GUILayoutAction_Reset_m246406170(__this, /*hidden argument*/NULL);
		__this->set_floatVariable_11((FsmFloat_t2134102846 *)NULL);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (100.0f), /*hidden argument*/NULL);
		__this->set_topValue_12(L_0);
		FsmFloat_t2134102846 * L_1 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_bottomValue_13(L_1);
		__this->set_changedEvent_14((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutVerticalSlider::OnGUI()
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIUtility_t1028319349_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutVerticalSlider_OnGUI_m1716715974_MetadataUsageId;
extern "C"  void GUILayoutVerticalSlider_OnGUI_m1716715974 (GUILayoutVerticalSlider_t1513124266 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutVerticalSlider_OnGUI_m1716715974_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		bool L_0 = GUI_get_changed_m1591686125(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		GUI_set_changed_m727947722(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_1 = __this->get_floatVariable_11();
		if (!L_1)
		{
			goto IL_004e;
		}
	}
	{
		FsmFloat_t2134102846 * L_2 = __this->get_floatVariable_11();
		FsmFloat_t2134102846 * L_3 = __this->get_floatVariable_11();
		NullCheck(L_3);
		float L_4 = FsmFloat_get_Value_m4137923823(L_3, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_5 = __this->get_topValue_12();
		NullCheck(L_5);
		float L_6 = FsmFloat_get_Value_m4137923823(L_5, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_7 = __this->get_bottomValue_13();
		NullCheck(L_7);
		float L_8 = FsmFloat_get_Value_m4137923823(L_7, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2977405297* L_9 = GUILayoutAction_get_LayoutOptions_m1855901188(__this, /*hidden argument*/NULL);
		float L_10 = GUILayout_VerticalSlider_m339552641(NULL /*static, unused*/, L_4, L_6, L_8, L_9, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmFloat_set_Value_m1568963140(L_2, L_10, /*hidden argument*/NULL);
	}

IL_004e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		bool L_11 = GUI_get_changed_m1591686125(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0073;
		}
	}
	{
		Fsm_t1527112426 * L_12 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_13 = __this->get_changedEvent_14();
		NullCheck(L_12);
		Fsm_Event_m625948263(L_12, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		GUIUtility_ExitGUI_m2798995767(NULL /*static, unused*/, /*hidden argument*/NULL);
		goto IL_0079;
	}

IL_0073:
	{
		bool L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_changed_m727947722(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
	}

IL_0079:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUITooltip::.ctor()
extern "C"  void GUITooltip__ctor_m4005681846 (GUITooltip_t3898253232 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUITooltip::Reset()
extern "C"  void GUITooltip_Reset_m1652114787 (GUITooltip_t3898253232 * __this, const MethodInfo* method)
{
	{
		__this->set_storeTooltip_9((FsmString_t952858651 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUITooltip::OnGUI()
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern const uint32_t GUITooltip_OnGUI_m3501080496_MetadataUsageId;
extern "C"  void GUITooltip_OnGUI_m3501080496 (GUITooltip_t3898253232 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUITooltip_OnGUI_m3501080496_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmString_t952858651 * L_0 = __this->get_storeTooltip_9();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		String_t* L_1 = GUI_get_tooltip_m2215748153(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmString_set_Value_m829393196(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUIVerticalSlider::.ctor()
extern "C"  void GUIVerticalSlider__ctor_m2718967510 (GUIVerticalSlider_t4042007136 * __this, const MethodInfo* method)
{
	{
		GUIAction__ctor_m141141367(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUIVerticalSlider::Reset()
extern Il2CppCodeGenString* _stringLiteral454225879;
extern Il2CppCodeGenString* _stringLiteral1272975455;
extern const uint32_t GUIVerticalSlider_Reset_m365400451_MetadataUsageId;
extern "C"  void GUIVerticalSlider_Reset_m365400451 (GUIVerticalSlider_t4042007136 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUIVerticalSlider_Reset_m365400451_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIAction_Reset_m2082541604(__this, /*hidden argument*/NULL);
		__this->set_floatVariable_16((FsmFloat_t2134102846 *)NULL);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (100.0f), /*hidden argument*/NULL);
		__this->set_topValue_17(L_0);
		FsmFloat_t2134102846 * L_1 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_bottomValue_18(L_1);
		FsmString_t952858651 * L_2 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, _stringLiteral454225879, /*hidden argument*/NULL);
		__this->set_sliderStyle_19(L_2);
		FsmString_t952858651 * L_3 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, _stringLiteral1272975455, /*hidden argument*/NULL);
		__this->set_thumbStyle_20(L_3);
		FsmFloat_t2134102846 * L_4 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.1f), /*hidden argument*/NULL);
		((GUIAction_t3055477407 *)__this)->set_width_12(L_4);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUIVerticalSlider::OnGUI()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t2990928826_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral454225879;
extern Il2CppCodeGenString* _stringLiteral1272975455;
extern const uint32_t GUIVerticalSlider_OnGUI_m2214366160_MetadataUsageId;
extern "C"  void GUIVerticalSlider_OnGUI_m2214366160 (GUIVerticalSlider_t4042007136 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUIVerticalSlider_OnGUI_m2214366160_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float G_B3_0 = 0.0f;
	float G_B3_1 = 0.0f;
	float G_B3_2 = 0.0f;
	Rect_t4241904616  G_B3_3;
	memset(&G_B3_3, 0, sizeof(G_B3_3));
	FsmFloat_t2134102846 * G_B3_4 = NULL;
	float G_B2_0 = 0.0f;
	float G_B2_1 = 0.0f;
	float G_B2_2 = 0.0f;
	Rect_t4241904616  G_B2_3;
	memset(&G_B2_3, 0, sizeof(G_B2_3));
	FsmFloat_t2134102846 * G_B2_4 = NULL;
	String_t* G_B4_0 = NULL;
	float G_B4_1 = 0.0f;
	float G_B4_2 = 0.0f;
	float G_B4_3 = 0.0f;
	Rect_t4241904616  G_B4_4;
	memset(&G_B4_4, 0, sizeof(G_B4_4));
	FsmFloat_t2134102846 * G_B4_5 = NULL;
	GUIStyle_t2990928826 * G_B6_0 = NULL;
	float G_B6_1 = 0.0f;
	float G_B6_2 = 0.0f;
	float G_B6_3 = 0.0f;
	Rect_t4241904616  G_B6_4;
	memset(&G_B6_4, 0, sizeof(G_B6_4));
	FsmFloat_t2134102846 * G_B6_5 = NULL;
	GUIStyle_t2990928826 * G_B5_0 = NULL;
	float G_B5_1 = 0.0f;
	float G_B5_2 = 0.0f;
	float G_B5_3 = 0.0f;
	Rect_t4241904616  G_B5_4;
	memset(&G_B5_4, 0, sizeof(G_B5_4));
	FsmFloat_t2134102846 * G_B5_5 = NULL;
	String_t* G_B7_0 = NULL;
	GUIStyle_t2990928826 * G_B7_1 = NULL;
	float G_B7_2 = 0.0f;
	float G_B7_3 = 0.0f;
	float G_B7_4 = 0.0f;
	Rect_t4241904616  G_B7_5;
	memset(&G_B7_5, 0, sizeof(G_B7_5));
	FsmFloat_t2134102846 * G_B7_6 = NULL;
	{
		GUIAction_OnGUI_m3931507313(__this, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_0 = __this->get_floatVariable_16();
		if (!L_0)
		{
			goto IL_00b0;
		}
	}
	{
		FsmFloat_t2134102846 * L_1 = __this->get_floatVariable_16();
		Rect_t4241904616  L_2 = ((GUIAction_t3055477407 *)__this)->get_rect_15();
		FsmFloat_t2134102846 * L_3 = __this->get_floatVariable_16();
		NullCheck(L_3);
		float L_4 = FsmFloat_get_Value_m4137923823(L_3, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_5 = __this->get_topValue_17();
		NullCheck(L_5);
		float L_6 = FsmFloat_get_Value_m4137923823(L_5, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_7 = __this->get_bottomValue_18();
		NullCheck(L_7);
		float L_8 = FsmFloat_get_Value_m4137923823(L_7, /*hidden argument*/NULL);
		FsmString_t952858651 * L_9 = __this->get_sliderStyle_19();
		NullCheck(L_9);
		String_t* L_10 = FsmString_get_Value_m872383149(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_12 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		G_B2_0 = L_8;
		G_B2_1 = L_6;
		G_B2_2 = L_4;
		G_B2_3 = L_2;
		G_B2_4 = L_1;
		if (!L_12)
		{
			G_B3_0 = L_8;
			G_B3_1 = L_6;
			G_B3_2 = L_4;
			G_B3_3 = L_2;
			G_B3_4 = L_1;
			goto IL_0068;
		}
	}
	{
		FsmString_t952858651 * L_13 = __this->get_sliderStyle_19();
		NullCheck(L_13);
		String_t* L_14 = FsmString_get_Value_m872383149(L_13, /*hidden argument*/NULL);
		G_B4_0 = L_14;
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		G_B4_3 = G_B2_2;
		G_B4_4 = G_B2_3;
		G_B4_5 = G_B2_4;
		goto IL_006d;
	}

IL_0068:
	{
		G_B4_0 = _stringLiteral454225879;
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
		G_B4_3 = G_B3_2;
		G_B4_4 = G_B3_3;
		G_B4_5 = G_B3_4;
	}

IL_006d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t2990928826_il2cpp_TypeInfo_var);
		GUIStyle_t2990928826 * L_15 = GUIStyle_op_Implicit_m169215436(NULL /*static, unused*/, G_B4_0, /*hidden argument*/NULL);
		FsmString_t952858651 * L_16 = __this->get_thumbStyle_20();
		NullCheck(L_16);
		String_t* L_17 = FsmString_get_Value_m872383149(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_19 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		G_B5_0 = L_15;
		G_B5_1 = G_B4_1;
		G_B5_2 = G_B4_2;
		G_B5_3 = G_B4_3;
		G_B5_4 = G_B4_4;
		G_B5_5 = G_B4_5;
		if (!L_19)
		{
			G_B6_0 = L_15;
			G_B6_1 = G_B4_1;
			G_B6_2 = G_B4_2;
			G_B6_3 = G_B4_3;
			G_B6_4 = G_B4_4;
			G_B6_5 = G_B4_5;
			goto IL_009c;
		}
	}
	{
		FsmString_t952858651 * L_20 = __this->get_thumbStyle_20();
		NullCheck(L_20);
		String_t* L_21 = FsmString_get_Value_m872383149(L_20, /*hidden argument*/NULL);
		G_B7_0 = L_21;
		G_B7_1 = G_B5_0;
		G_B7_2 = G_B5_1;
		G_B7_3 = G_B5_2;
		G_B7_4 = G_B5_3;
		G_B7_5 = G_B5_4;
		G_B7_6 = G_B5_5;
		goto IL_00a1;
	}

IL_009c:
	{
		G_B7_0 = _stringLiteral1272975455;
		G_B7_1 = G_B6_0;
		G_B7_2 = G_B6_1;
		G_B7_3 = G_B6_2;
		G_B7_4 = G_B6_3;
		G_B7_5 = G_B6_4;
		G_B7_6 = G_B6_5;
	}

IL_00a1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t2990928826_il2cpp_TypeInfo_var);
		GUIStyle_t2990928826 * L_22 = GUIStyle_op_Implicit_m169215436(NULL /*static, unused*/, G_B7_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		float L_23 = GUI_VerticalSlider_m2968703877(NULL /*static, unused*/, G_B7_5, G_B7_4, G_B7_3, G_B7_2, G_B7_1, L_22, /*hidden argument*/NULL);
		NullCheck(G_B7_6);
		FsmFloat_set_Value_m1568963140(G_B7_6, L_23, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.HasComponent::.ctor()
extern "C"  void HasComponent__ctor_m3373741339 (HasComponent_t1091840427 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.HasComponent::Reset()
extern "C"  void HasComponent_Reset_m1020174280 (HasComponent_t1091840427 * __this, const MethodInfo* method)
{
	{
		__this->set_aComponent_16((Component_t3501516275 *)NULL);
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_trueEvent_12((FsmEvent_t2133468028 *)NULL);
		__this->set_falseEvent_13((FsmEvent_t2133468028 *)NULL);
		__this->set_component_10((FsmString_t952858651 *)NULL);
		__this->set_store_14((FsmBool_t1075959796 *)NULL);
		__this->set_everyFrame_15((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.HasComponent::OnEnter()
extern "C"  void HasComponent_OnEnter_m3151814194 (HasComponent_t1091840427 * __this, const MethodInfo* method)
{
	HasComponent_t1091840427 * G_B2_0 = NULL;
	HasComponent_t1091840427 * G_B1_0 = NULL;
	GameObject_t3674682005 * G_B3_0 = NULL;
	HasComponent_t1091840427 * G_B3_1 = NULL;
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_9();
		NullCheck(L_0);
		int32_t L_1 = FsmOwnerDefault_get_OwnerOption_m3357910390(L_0, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if (L_1)
		{
			G_B2_0 = __this;
			goto IL_001c;
		}
	}
	{
		GameObject_t3674682005 * L_2 = FsmStateAction_get_Owner_m1855633209(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_002c;
	}

IL_001c:
	{
		FsmOwnerDefault_t251897112 * L_3 = __this->get_gameObject_9();
		NullCheck(L_3);
		FsmGameObject_t1697147867 * L_4 = FsmOwnerDefault_get_GameObject_m3249227945(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_t3674682005 * L_5 = FsmGameObject_get_Value_m673294275(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
	}

IL_002c:
	{
		NullCheck(G_B3_1);
		HasComponent_DoHasComponent_m1765689135(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		bool L_6 = __this->get_everyFrame_15();
		if (L_6)
		{
			goto IL_0042;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.HasComponent::OnUpdate()
extern "C"  void HasComponent_OnUpdate_m2350518865 (HasComponent_t1091840427 * __this, const MethodInfo* method)
{
	HasComponent_t1091840427 * G_B2_0 = NULL;
	HasComponent_t1091840427 * G_B1_0 = NULL;
	GameObject_t3674682005 * G_B3_0 = NULL;
	HasComponent_t1091840427 * G_B3_1 = NULL;
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_9();
		NullCheck(L_0);
		int32_t L_1 = FsmOwnerDefault_get_OwnerOption_m3357910390(L_0, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if (L_1)
		{
			G_B2_0 = __this;
			goto IL_001c;
		}
	}
	{
		GameObject_t3674682005 * L_2 = FsmStateAction_get_Owner_m1855633209(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_002c;
	}

IL_001c:
	{
		FsmOwnerDefault_t251897112 * L_3 = __this->get_gameObject_9();
		NullCheck(L_3);
		FsmGameObject_t1697147867 * L_4 = FsmOwnerDefault_get_GameObject_m3249227945(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_t3674682005 * L_5 = FsmGameObject_get_Value_m673294275(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
	}

IL_002c:
	{
		NullCheck(G_B3_1);
		HasComponent_DoHasComponent_m1765689135(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.HasComponent::OnExit()
extern "C"  void HasComponent_OnExit_m3020085030 (HasComponent_t1091840427 * __this, const MethodInfo* method)
{
	{
		FsmBool_t1075959796 * L_0 = __this->get_removeOnExit_11();
		NullCheck(L_0);
		bool L_1 = FsmBool_get_Value_m3101329097(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		Component_t3501516275 * L_2 = __this->get_aComponent_16();
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		Component_t3501516275 * L_4 = __this->get_aComponent_16();
		Object_Destroy_m176400816(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_002c:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.HasComponent::DoHasComponent(UnityEngine.GameObject)
extern "C"  void HasComponent_DoHasComponent_m1765689135 (HasComponent_t1091840427 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method)
{
	Fsm_t1527112426 * G_B2_0 = NULL;
	Fsm_t1527112426 * G_B1_0 = NULL;
	FsmEvent_t2133468028 * G_B3_0 = NULL;
	Fsm_t1527112426 * G_B3_1 = NULL;
	{
		GameObject_t3674682005 * L_0 = ___go0;
		FsmString_t952858651 * L_1 = __this->get_component_10();
		NullCheck(L_1);
		String_t* L_2 = FsmString_get_Value_m872383149(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t3501516275 * L_3 = GameObject_GetComponent_m2525409030(L_0, L_2, /*hidden argument*/NULL);
		__this->set_aComponent_16(L_3);
		Fsm_t1527112426 * L_4 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		Component_t3501516275 * L_5 = __this->get_aComponent_16();
		bool L_6 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_5, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		G_B1_0 = L_4;
		if (!L_6)
		{
			G_B2_0 = L_4;
			goto IL_0039;
		}
	}
	{
		FsmEvent_t2133468028 * L_7 = __this->get_trueEvent_12();
		G_B3_0 = L_7;
		G_B3_1 = G_B1_0;
		goto IL_003f;
	}

IL_0039:
	{
		FsmEvent_t2133468028 * L_8 = __this->get_falseEvent_13();
		G_B3_0 = L_8;
		G_B3_1 = G_B2_0;
	}

IL_003f:
	{
		NullCheck(G_B3_1);
		Fsm_Event_m625948263(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IntAdd::.ctor()
extern "C"  void IntAdd__ctor_m3750984588 (IntAdd_t3052069402 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IntAdd::Reset()
extern "C"  void IntAdd_Reset_m1397417529 (IntAdd_t3052069402 * __this, const MethodInfo* method)
{
	{
		__this->set_intVariable_9((FsmInt_t1596138449 *)NULL);
		__this->set_add_10((FsmInt_t1596138449 *)NULL);
		__this->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IntAdd::OnEnter()
extern "C"  void IntAdd_OnEnter_m610356323 (IntAdd_t3052069402 * __this, const MethodInfo* method)
{
	{
		FsmInt_t1596138449 * L_0 = __this->get_intVariable_9();
		FsmInt_t1596138449 * L_1 = L_0;
		NullCheck(L_1);
		int32_t L_2 = FsmInt_get_Value_m27059446(L_1, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_3 = __this->get_add_10();
		NullCheck(L_3);
		int32_t L_4 = FsmInt_get_Value_m27059446(L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		FsmInt_set_Value_m2087583461(L_1, ((int32_t)((int32_t)L_2+(int32_t)L_4)), /*hidden argument*/NULL);
		bool L_5 = __this->get_everyFrame_11();
		if (L_5)
		{
			goto IL_002e;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_002e:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IntAdd::OnUpdate()
extern "C"  void IntAdd_OnUpdate_m874736192 (IntAdd_t3052069402 * __this, const MethodInfo* method)
{
	{
		FsmInt_t1596138449 * L_0 = __this->get_intVariable_9();
		FsmInt_t1596138449 * L_1 = L_0;
		NullCheck(L_1);
		int32_t L_2 = FsmInt_get_Value_m27059446(L_1, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_3 = __this->get_add_10();
		NullCheck(L_3);
		int32_t L_4 = FsmInt_get_Value_m27059446(L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		FsmInt_set_Value_m2087583461(L_1, ((int32_t)((int32_t)L_2+(int32_t)L_4)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IntChanged::.ctor()
extern "C"  void IntChanged__ctor_m822731225 (IntChanged_t778990893 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IntChanged::Reset()
extern "C"  void IntChanged_Reset_m2764131462 (IntChanged_t778990893 * __this, const MethodInfo* method)
{
	{
		__this->set_intVariable_9((FsmInt_t1596138449 *)NULL);
		__this->set_changedEvent_10((FsmEvent_t2133468028 *)NULL);
		__this->set_storeResult_11((FsmBool_t1075959796 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IntChanged::OnEnter()
extern "C"  void IntChanged_OnEnter_m4057420656 (IntChanged_t778990893 * __this, const MethodInfo* method)
{
	{
		FsmInt_t1596138449 * L_0 = __this->get_intVariable_9();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0017:
	{
		FsmInt_t1596138449 * L_2 = __this->get_intVariable_9();
		NullCheck(L_2);
		int32_t L_3 = FsmInt_get_Value_m27059446(L_2, /*hidden argument*/NULL);
		__this->set_previousValue_12(L_3);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IntChanged::OnUpdate()
extern "C"  void IntChanged_OnUpdate_m359548115 (IntChanged_t778990893 * __this, const MethodInfo* method)
{
	{
		FsmBool_t1075959796 * L_0 = __this->get_storeResult_11();
		NullCheck(L_0);
		FsmBool_set_Value_m1126216340(L_0, (bool)0, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_1 = __this->get_intVariable_9();
		NullCheck(L_1);
		int32_t L_2 = FsmInt_get_Value_m27059446(L_1, /*hidden argument*/NULL);
		int32_t L_3 = __this->get_previousValue_12();
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_0050;
		}
	}
	{
		FsmInt_t1596138449 * L_4 = __this->get_intVariable_9();
		NullCheck(L_4);
		int32_t L_5 = FsmInt_get_Value_m27059446(L_4, /*hidden argument*/NULL);
		__this->set_previousValue_12(L_5);
		FsmBool_t1075959796 * L_6 = __this->get_storeResult_11();
		NullCheck(L_6);
		FsmBool_set_Value_m1126216340(L_6, (bool)1, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_7 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_8 = __this->get_changedEvent_10();
		NullCheck(L_7);
		Fsm_Event_m625948263(L_7, L_8, /*hidden argument*/NULL);
	}

IL_0050:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IntClamp::.ctor()
extern "C"  void IntClamp__ctor_m1600343602 (IntClamp_t286226100 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IntClamp::Reset()
extern "C"  void IntClamp_Reset_m3541743839 (IntClamp_t286226100 * __this, const MethodInfo* method)
{
	{
		__this->set_intVariable_9((FsmInt_t1596138449 *)NULL);
		__this->set_minValue_10((FsmInt_t1596138449 *)NULL);
		__this->set_maxValue_11((FsmInt_t1596138449 *)NULL);
		__this->set_everyFrame_12((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IntClamp::OnEnter()
extern "C"  void IntClamp_OnEnter_m4018605449 (IntClamp_t286226100 * __this, const MethodInfo* method)
{
	{
		IntClamp_DoClamp_m2319002240(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_12();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IntClamp::OnUpdate()
extern "C"  void IntClamp_OnUpdate_m3451243994 (IntClamp_t286226100 * __this, const MethodInfo* method)
{
	{
		IntClamp_DoClamp_m2319002240(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IntClamp::DoClamp()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t IntClamp_DoClamp_m2319002240_MetadataUsageId;
extern "C"  void IntClamp_DoClamp_m2319002240 (IntClamp_t286226100 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IntClamp_DoClamp_m2319002240_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmInt_t1596138449 * L_0 = __this->get_intVariable_9();
		FsmInt_t1596138449 * L_1 = __this->get_intVariable_9();
		NullCheck(L_1);
		int32_t L_2 = FsmInt_get_Value_m27059446(L_1, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_3 = __this->get_minValue_10();
		NullCheck(L_3);
		int32_t L_4 = FsmInt_get_Value_m27059446(L_3, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_5 = __this->get_maxValue_11();
		NullCheck(L_5);
		int32_t L_6 = FsmInt_get_Value_m27059446(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_7 = Mathf_Clamp_m510460741(NULL /*static, unused*/, L_2, L_4, L_6, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmInt_set_Value_m2087583461(L_0, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IntCompare::.ctor()
extern "C"  void IntCompare__ctor_m1865543368 (IntCompare_t990531422 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IntCompare::Reset()
extern "C"  void IntCompare_Reset_m3806943605 (IntCompare_t990531422 * __this, const MethodInfo* method)
{
	{
		FsmInt_t1596138449 * L_0 = FsmInt_op_Implicit_m1006909518(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		__this->set_integer1_9(L_0);
		FsmInt_t1596138449 * L_1 = FsmInt_op_Implicit_m1006909518(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		__this->set_integer2_10(L_1);
		__this->set_equal_11((FsmEvent_t2133468028 *)NULL);
		__this->set_lessThan_12((FsmEvent_t2133468028 *)NULL);
		__this->set_greaterThan_13((FsmEvent_t2133468028 *)NULL);
		__this->set_everyFrame_14((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IntCompare::OnEnter()
extern "C"  void IntCompare_OnEnter_m1177542815 (IntCompare_t990531422 * __this, const MethodInfo* method)
{
	{
		IntCompare_DoIntCompare_m3168313117(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_14();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IntCompare::OnUpdate()
extern "C"  void IntCompare_OnUpdate_m1277648260 (IntCompare_t990531422 * __this, const MethodInfo* method)
{
	{
		IntCompare_DoIntCompare_m3168313117(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IntCompare::DoIntCompare()
extern "C"  void IntCompare_DoIntCompare_m3168313117 (IntCompare_t990531422 * __this, const MethodInfo* method)
{
	{
		FsmInt_t1596138449 * L_0 = __this->get_integer1_9();
		NullCheck(L_0);
		int32_t L_1 = FsmInt_get_Value_m27059446(L_0, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_2 = __this->get_integer2_10();
		NullCheck(L_2);
		int32_t L_3 = FsmInt_get_Value_m27059446(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_3))))
		{
			goto IL_002d;
		}
	}
	{
		Fsm_t1527112426 * L_4 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_5 = __this->get_equal_11();
		NullCheck(L_4);
		Fsm_Event_m625948263(L_4, L_5, /*hidden argument*/NULL);
		return;
	}

IL_002d:
	{
		FsmInt_t1596138449 * L_6 = __this->get_integer1_9();
		NullCheck(L_6);
		int32_t L_7 = FsmInt_get_Value_m27059446(L_6, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_8 = __this->get_integer2_10();
		NullCheck(L_8);
		int32_t L_9 = FsmInt_get_Value_m27059446(L_8, /*hidden argument*/NULL);
		if ((((int32_t)L_7) >= ((int32_t)L_9)))
		{
			goto IL_005a;
		}
	}
	{
		Fsm_t1527112426 * L_10 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_11 = __this->get_lessThan_12();
		NullCheck(L_10);
		Fsm_Event_m625948263(L_10, L_11, /*hidden argument*/NULL);
		return;
	}

IL_005a:
	{
		FsmInt_t1596138449 * L_12 = __this->get_integer1_9();
		NullCheck(L_12);
		int32_t L_13 = FsmInt_get_Value_m27059446(L_12, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_14 = __this->get_integer2_10();
		NullCheck(L_14);
		int32_t L_15 = FsmInt_get_Value_m27059446(L_14, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)L_15)))
		{
			goto IL_0086;
		}
	}
	{
		Fsm_t1527112426 * L_16 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_17 = __this->get_greaterThan_13();
		NullCheck(L_16);
		Fsm_Event_m625948263(L_16, L_17, /*hidden argument*/NULL);
	}

IL_0086:
	{
		return;
	}
}
// System.String HutongGames.PlayMaker.Actions.IntCompare::ErrorCheck()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1226149032;
extern const uint32_t IntCompare_ErrorCheck_m2986004223_MetadataUsageId;
extern "C"  String_t* IntCompare_ErrorCheck_m2986004223 (IntCompare_t990531422 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IntCompare_ErrorCheck_m2986004223_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmEvent_t2133468028 * L_0 = __this->get_equal_11();
		bool L_1 = FsmEvent_IsNullOrEmpty_m3021350928(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0036;
		}
	}
	{
		FsmEvent_t2133468028 * L_2 = __this->get_lessThan_12();
		bool L_3 = FsmEvent_IsNullOrEmpty_m3021350928(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0036;
		}
	}
	{
		FsmEvent_t2133468028 * L_4 = __this->get_greaterThan_13();
		bool L_5 = FsmEvent_IsNullOrEmpty_m3021350928(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0036;
		}
	}
	{
		return _stringLiteral1226149032;
	}

IL_0036:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_6;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IntOperator::.ctor()
extern "C"  void IntOperator__ctor_m872355861 (IntOperator_t91950145 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IntOperator::Reset()
extern "C"  void IntOperator_Reset_m2813756098 (IntOperator_t91950145 * __this, const MethodInfo* method)
{
	{
		__this->set_integer1_9((FsmInt_t1596138449 *)NULL);
		__this->set_integer2_10((FsmInt_t1596138449 *)NULL);
		__this->set_operation_11(0);
		__this->set_storeResult_12((FsmInt_t1596138449 *)NULL);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IntOperator::OnEnter()
extern "C"  void IntOperator_OnEnter_m207088300 (IntOperator_t91950145 * __this, const MethodInfo* method)
{
	{
		IntOperator_DoIntOperator_m1191144795(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IntOperator::OnUpdate()
extern "C"  void IntOperator_OnUpdate_m1258329367 (IntOperator_t91950145 * __this, const MethodInfo* method)
{
	{
		IntOperator_DoIntOperator_m1191144795(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IntOperator::DoIntOperator()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t IntOperator_DoIntOperator_m1191144795_MetadataUsageId;
extern "C"  void IntOperator_DoIntOperator_m1191144795 (IntOperator_t91950145 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IntOperator_DoIntOperator_m1191144795_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		FsmInt_t1596138449 * L_0 = __this->get_integer1_9();
		NullCheck(L_0);
		int32_t L_1 = FsmInt_get_Value_m27059446(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		FsmInt_t1596138449 * L_2 = __this->get_integer2_10();
		NullCheck(L_2);
		int32_t L_3 = FsmInt_get_Value_m27059446(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = __this->get_operation_11();
		V_2 = L_4;
		int32_t L_5 = V_2;
		if (L_5 == 0)
		{
			goto IL_0042;
		}
		if (L_5 == 1)
		{
			goto IL_0055;
		}
		if (L_5 == 2)
		{
			goto IL_0068;
		}
		if (L_5 == 3)
		{
			goto IL_007b;
		}
		if (L_5 == 4)
		{
			goto IL_008e;
		}
		if (L_5 == 5)
		{
			goto IL_00a5;
		}
	}
	{
		goto IL_00bc;
	}

IL_0042:
	{
		FsmInt_t1596138449 * L_6 = __this->get_storeResult_12();
		int32_t L_7 = V_0;
		int32_t L_8 = V_1;
		NullCheck(L_6);
		FsmInt_set_Value_m2087583461(L_6, ((int32_t)((int32_t)L_7+(int32_t)L_8)), /*hidden argument*/NULL);
		goto IL_00bc;
	}

IL_0055:
	{
		FsmInt_t1596138449 * L_9 = __this->get_storeResult_12();
		int32_t L_10 = V_0;
		int32_t L_11 = V_1;
		NullCheck(L_9);
		FsmInt_set_Value_m2087583461(L_9, ((int32_t)((int32_t)L_10-(int32_t)L_11)), /*hidden argument*/NULL);
		goto IL_00bc;
	}

IL_0068:
	{
		FsmInt_t1596138449 * L_12 = __this->get_storeResult_12();
		int32_t L_13 = V_0;
		int32_t L_14 = V_1;
		NullCheck(L_12);
		FsmInt_set_Value_m2087583461(L_12, ((int32_t)((int32_t)L_13*(int32_t)L_14)), /*hidden argument*/NULL);
		goto IL_00bc;
	}

IL_007b:
	{
		FsmInt_t1596138449 * L_15 = __this->get_storeResult_12();
		int32_t L_16 = V_0;
		int32_t L_17 = V_1;
		NullCheck(L_15);
		FsmInt_set_Value_m2087583461(L_15, ((int32_t)((int32_t)L_16/(int32_t)L_17)), /*hidden argument*/NULL);
		goto IL_00bc;
	}

IL_008e:
	{
		FsmInt_t1596138449 * L_18 = __this->get_storeResult_12();
		int32_t L_19 = V_0;
		int32_t L_20 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_21 = Mathf_Min_m2413438171(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		NullCheck(L_18);
		FsmInt_set_Value_m2087583461(L_18, L_21, /*hidden argument*/NULL);
		goto IL_00bc;
	}

IL_00a5:
	{
		FsmInt_t1596138449 * L_22 = __this->get_storeResult_12();
		int32_t L_23 = V_0;
		int32_t L_24 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_25 = Mathf_Max_m2911193737(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		FsmInt_set_Value_m2087583461(L_22, L_25, /*hidden argument*/NULL);
		goto IL_00bc;
	}

IL_00bc:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IntSwitch::.ctor()
extern "C"  void IntSwitch__ctor_m2901762309 (IntSwitch_t102584145 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IntSwitch::Reset()
extern Il2CppClass* FsmIntU5BU5D_t1976821196_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmEventU5BU5D_t2862142229_il2cpp_TypeInfo_var;
extern const uint32_t IntSwitch_Reset_m548195250_MetadataUsageId;
extern "C"  void IntSwitch_Reset_m548195250 (IntSwitch_t102584145 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IntSwitch_Reset_m548195250_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_intVariable_9((FsmInt_t1596138449 *)NULL);
		__this->set_compareTo_10(((FsmIntU5BU5D_t1976821196*)SZArrayNew(FsmIntU5BU5D_t1976821196_il2cpp_TypeInfo_var, (uint32_t)1)));
		__this->set_sendEvent_11(((FsmEventU5BU5D_t2862142229*)SZArrayNew(FsmEventU5BU5D_t2862142229_il2cpp_TypeInfo_var, (uint32_t)1)));
		__this->set_everyFrame_12((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IntSwitch::OnEnter()
extern "C"  void IntSwitch_OnEnter_m551532444 (IntSwitch_t102584145 * __this, const MethodInfo* method)
{
	{
		IntSwitch_DoIntSwitch_m4266271323(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_12();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IntSwitch::OnUpdate()
extern "C"  void IntSwitch_OnUpdate_m3346163239 (IntSwitch_t102584145 * __this, const MethodInfo* method)
{
	{
		IntSwitch_DoIntSwitch_m4266271323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IntSwitch::DoIntSwitch()
extern "C"  void IntSwitch_DoIntSwitch_m4266271323 (IntSwitch_t102584145 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmInt_t1596138449 * L_0 = __this->get_intVariable_9();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		V_0 = 0;
		goto IL_004d;
	}

IL_0018:
	{
		FsmInt_t1596138449 * L_2 = __this->get_intVariable_9();
		NullCheck(L_2);
		int32_t L_3 = FsmInt_get_Value_m27059446(L_2, /*hidden argument*/NULL);
		FsmIntU5BU5D_t1976821196* L_4 = __this->get_compareTo_10();
		int32_t L_5 = V_0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		FsmInt_t1596138449 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_7);
		int32_t L_8 = FsmInt_get_Value_m27059446(L_7, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)L_8))))
		{
			goto IL_0049;
		}
	}
	{
		Fsm_t1527112426 * L_9 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEventU5BU5D_t2862142229* L_10 = __this->get_sendEvent_11();
		int32_t L_11 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		FsmEvent_t2133468028 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck(L_9);
		Fsm_Event_m625948263(L_9, L_13, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		int32_t L_14 = V_0;
		V_0 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_004d:
	{
		int32_t L_15 = V_0;
		FsmIntU5BU5D_t1976821196* L_16 = __this->get_compareTo_10();
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length)))))))
		{
			goto IL_0018;
		}
	}
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.InverseTransformDirection::.ctor()
extern "C"  void InverseTransformDirection__ctor_m665262661 (InverseTransformDirection_t4174732817 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.InverseTransformDirection::Reset()
extern "C"  void InverseTransformDirection_Reset_m2606662898 (InverseTransformDirection_t4174732817 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_worldDirection_10((FsmVector3_t533912882 *)NULL);
		__this->set_storeResult_11((FsmVector3_t533912882 *)NULL);
		__this->set_everyFrame_12((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.InverseTransformDirection::OnEnter()
extern "C"  void InverseTransformDirection_OnEnter_m3053986012 (InverseTransformDirection_t4174732817 * __this, const MethodInfo* method)
{
	{
		InverseTransformDirection_DoInverseTransformDirection_m347529819(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_12();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.InverseTransformDirection::OnUpdate()
extern "C"  void InverseTransformDirection_OnUpdate_m3612812519 (InverseTransformDirection_t4174732817 * __this, const MethodInfo* method)
{
	{
		InverseTransformDirection_DoInverseTransformDirection_m347529819(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.InverseTransformDirection::DoInverseTransformDirection()
extern "C"  void InverseTransformDirection_DoInverseTransformDirection_m347529819 (InverseTransformDirection_t4174732817 * __this, const MethodInfo* method)
{
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		FsmVector3_t533912882 * L_5 = __this->get_storeResult_11();
		GameObject_t3674682005 * L_6 = V_0;
		NullCheck(L_6);
		Transform_t1659122786 * L_7 = GameObject_get_transform_m1278640159(L_6, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_8 = __this->get_worldDirection_10();
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = FsmVector3_get_Value_m2779135117(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t4282066566  L_10 = Transform_InverseTransformDirection_m416562129(L_7, L_9, /*hidden argument*/NULL);
		NullCheck(L_5);
		FsmVector3_set_Value_m716982822(L_5, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.InverseTransformPoint::.ctor()
extern "C"  void InverseTransformPoint__ctor_m233269204 (InverseTransformPoint_t1066006434 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.InverseTransformPoint::Reset()
extern "C"  void InverseTransformPoint_Reset_m2174669441 (InverseTransformPoint_t1066006434 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_worldPosition_10((FsmVector3_t533912882 *)NULL);
		__this->set_storeResult_11((FsmVector3_t533912882 *)NULL);
		__this->set_everyFrame_12((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.InverseTransformPoint::OnEnter()
extern "C"  void InverseTransformPoint_OnEnter_m225134251 (InverseTransformPoint_t1066006434 * __this, const MethodInfo* method)
{
	{
		InverseTransformPoint_DoInverseTransformPoint_m2473106811(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_12();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.InverseTransformPoint::OnUpdate()
extern "C"  void InverseTransformPoint_OnUpdate_m1817753848 (InverseTransformPoint_t1066006434 * __this, const MethodInfo* method)
{
	{
		InverseTransformPoint_DoInverseTransformPoint_m2473106811(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.InverseTransformPoint::DoInverseTransformPoint()
extern "C"  void InverseTransformPoint_DoInverseTransformPoint_m2473106811 (InverseTransformPoint_t1066006434 * __this, const MethodInfo* method)
{
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		FsmVector3_t533912882 * L_5 = __this->get_storeResult_11();
		GameObject_t3674682005 * L_6 = V_0;
		NullCheck(L_6);
		Transform_t1659122786 * L_7 = GameObject_get_transform_m1278640159(L_6, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_8 = __this->get_worldPosition_10();
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = FsmVector3_get_Value_m2779135117(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t4282066566  L_10 = Transform_InverseTransformPoint_m1626812000(L_7, L_9, /*hidden argument*/NULL);
		NullCheck(L_5);
		FsmVector3_set_Value_m716982822(L_5, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.InvokeMethod::.ctor()
extern "C"  void InvokeMethod__ctor_m1586428389 (InvokeMethod_t2919758241 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.InvokeMethod::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokeMethod_Reset_m3527828626_MetadataUsageId;
extern "C"  void InvokeMethod_Reset_m3527828626 (InvokeMethod_t2919758241 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokeMethod_Reset_m3527828626_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_behaviour_10((FsmString_t952858651 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_methodName_11(L_1);
		__this->set_delay_12((FsmFloat_t2134102846 *)NULL);
		FsmBool_t1075959796 * L_2 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_repeating_13(L_2);
		FsmFloat_t2134102846 * L_3 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_repeatDelay_14(L_3);
		FsmBool_t1075959796 * L_4 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_cancelOnExit_15(L_4);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.InvokeMethod::OnEnter()
extern "C"  void InvokeMethod_OnEnter_m3530987644 (InvokeMethod_t2919758241 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		InvokeMethod_DoInvokeMethod_m3833673499(__this, L_2, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.InvokeMethod::DoInvokeMethod(UnityEngine.GameObject)
extern Il2CppClass* MonoBehaviour_t667441552_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2193137791;
extern Il2CppCodeGenString* _stringLiteral2683139273;
extern const uint32_t InvokeMethod_DoInvokeMethod_m3833673499_MetadataUsageId;
extern "C"  void InvokeMethod_DoInvokeMethod_m3833673499 (InvokeMethod_t2919758241 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokeMethod_DoInvokeMethod_m3833673499_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = ___go0;
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		GameObject_t3674682005 * L_2 = ___go0;
		FsmString_t952858651 * L_3 = __this->get_behaviour_10();
		NullCheck(L_3);
		String_t* L_4 = FsmString_get_Value_m872383149(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		Component_t3501516275 * L_5 = GameObject_GetComponent_m2525409030(L_2, L_4, /*hidden argument*/NULL);
		__this->set_component_16(((MonoBehaviour_t667441552 *)IsInstClass(L_5, MonoBehaviour_t667441552_il2cpp_TypeInfo_var)));
		MonoBehaviour_t667441552 * L_6 = __this->get_component_16();
		bool L_7 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_6, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0061;
		}
	}
	{
		GameObject_t3674682005 * L_8 = ___go0;
		NullCheck(L_8);
		String_t* L_9 = Object_get_name_m3709440845(L_8, /*hidden argument*/NULL);
		FsmString_t952858651 * L_10 = __this->get_behaviour_10();
		NullCheck(L_10);
		String_t* L_11 = FsmString_get_Value_m872383149(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m2933632197(NULL /*static, unused*/, _stringLiteral2193137791, L_9, _stringLiteral2683139273, L_11, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_12, /*hidden argument*/NULL);
		return;
	}

IL_0061:
	{
		FsmBool_t1075959796 * L_13 = __this->get_repeating_13();
		NullCheck(L_13);
		bool L_14 = FsmBool_get_Value_m3101329097(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00a2;
		}
	}
	{
		MonoBehaviour_t667441552 * L_15 = __this->get_component_16();
		FsmString_t952858651 * L_16 = __this->get_methodName_11();
		NullCheck(L_16);
		String_t* L_17 = FsmString_get_Value_m872383149(L_16, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_18 = __this->get_delay_12();
		NullCheck(L_18);
		float L_19 = FsmFloat_get_Value_m4137923823(L_18, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_20 = __this->get_repeatDelay_14();
		NullCheck(L_20);
		float L_21 = FsmFloat_get_Value_m4137923823(L_20, /*hidden argument*/NULL);
		NullCheck(L_15);
		MonoBehaviour_InvokeRepeating_m1115468640(L_15, L_17, L_19, L_21, /*hidden argument*/NULL);
		goto IL_00c3;
	}

IL_00a2:
	{
		MonoBehaviour_t667441552 * L_22 = __this->get_component_16();
		FsmString_t952858651 * L_23 = __this->get_methodName_11();
		NullCheck(L_23);
		String_t* L_24 = FsmString_get_Value_m872383149(L_23, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_25 = __this->get_delay_12();
		NullCheck(L_25);
		float L_26 = FsmFloat_get_Value_m4137923823(L_25, /*hidden argument*/NULL);
		NullCheck(L_22);
		MonoBehaviour_Invoke_m2825545578(L_22, L_24, L_26, /*hidden argument*/NULL);
	}

IL_00c3:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.InvokeMethod::OnExit()
extern "C"  void InvokeMethod_OnExit_m3447958428 (InvokeMethod_t2919758241 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour_t667441552 * L_0 = __this->get_component_16();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FsmBool_t1075959796 * L_2 = __this->get_cancelOnExit_15();
		NullCheck(L_2);
		bool L_3 = FsmBool_get_Value_m3101329097(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0038;
		}
	}
	{
		MonoBehaviour_t667441552 * L_4 = __this->get_component_16();
		FsmString_t952858651 * L_5 = __this->get_methodName_11();
		NullCheck(L_5);
		String_t* L_6 = FsmString_get_Value_m872383149(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		MonoBehaviour_CancelInvoke_m2461959659(L_4, L_6, /*hidden argument*/NULL);
	}

IL_0038:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IsKinematic::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m3497509022_MethodInfo_var;
extern const uint32_t IsKinematic__ctor_m4094941389_MetadataUsageId;
extern "C"  void IsKinematic__ctor_m4094941389 (IsKinematic_t784342153 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IsKinematic__ctor_m4094941389_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m3497509022(__this, /*hidden argument*/ComponentAction_1__ctor_m3497509022_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IsKinematic::Reset()
extern "C"  void IsKinematic_Reset_m1741374330 (IsKinematic_t784342153 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_trueEvent_12((FsmEvent_t2133468028 *)NULL);
		__this->set_falseEvent_13((FsmEvent_t2133468028 *)NULL);
		__this->set_store_14((FsmBool_t1075959796 *)NULL);
		__this->set_everyFrame_15((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IsKinematic::OnEnter()
extern "C"  void IsKinematic_OnEnter_m440360292 (IsKinematic_t784342153 * __this, const MethodInfo* method)
{
	{
		IsKinematic_DoIsKinematic_m2210796635(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IsKinematic::OnUpdate()
extern "C"  void IsKinematic_OnUpdate_m4194793823 (IsKinematic_t784342153 * __this, const MethodInfo* method)
{
	{
		IsKinematic_DoIsKinematic_m2210796635(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IsKinematic::DoIsKinematic()
extern const MethodInfo* ComponentAction_1_UpdateCache_m864821753_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_rigidbody_m1866302531_MethodInfo_var;
extern const uint32_t IsKinematic_DoIsKinematic_m2210796635_MetadataUsageId;
extern "C"  void IsKinematic_DoIsKinematic_m2210796635 (IsKinematic_t784342153 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IsKinematic_DoIsKinematic_m2210796635_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	bool V_1 = false;
	Fsm_t1527112426 * G_B3_0 = NULL;
	Fsm_t1527112426 * G_B2_0 = NULL;
	FsmEvent_t2133468028 * G_B4_0 = NULL;
	Fsm_t1527112426 * G_B4_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m864821753(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m864821753_MethodInfo_var);
		if (!L_4)
		{
			goto IL_0058;
		}
	}
	{
		Rigidbody_t3346577219 * L_5 = ComponentAction_1_get_rigidbody_m1866302531(__this, /*hidden argument*/ComponentAction_1_get_rigidbody_m1866302531_MethodInfo_var);
		NullCheck(L_5);
		bool L_6 = Rigidbody_get_isKinematic_m3963857442(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		FsmBool_t1075959796 * L_7 = __this->get_store_14();
		bool L_8 = V_1;
		NullCheck(L_7);
		FsmBool_set_Value_m1126216340(L_7, L_8, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_9 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		bool L_10 = V_1;
		G_B2_0 = L_9;
		if (!L_10)
		{
			G_B3_0 = L_9;
			goto IL_004d;
		}
	}
	{
		FsmEvent_t2133468028 * L_11 = __this->get_trueEvent_12();
		G_B4_0 = L_11;
		G_B4_1 = G_B2_0;
		goto IL_0053;
	}

IL_004d:
	{
		FsmEvent_t2133468028 * L_12 = __this->get_falseEvent_13();
		G_B4_0 = L_12;
		G_B4_1 = G_B3_0;
	}

IL_0053:
	{
		NullCheck(G_B4_1);
		Fsm_Event_m625948263(G_B4_1, G_B4_0, /*hidden argument*/NULL);
	}

IL_0058:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IsSleeping::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m3497509022_MethodInfo_var;
extern const uint32_t IsSleeping__ctor_m1099895465_MetadataUsageId;
extern "C"  void IsSleeping__ctor_m1099895465 (IsSleeping_t599267933 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IsSleeping__ctor_m1099895465_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m3497509022(__this, /*hidden argument*/ComponentAction_1__ctor_m3497509022_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IsSleeping::Reset()
extern "C"  void IsSleeping_Reset_m3041295702 (IsSleeping_t599267933 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_trueEvent_12((FsmEvent_t2133468028 *)NULL);
		__this->set_falseEvent_13((FsmEvent_t2133468028 *)NULL);
		__this->set_store_14((FsmBool_t1075959796 *)NULL);
		__this->set_everyFrame_15((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IsSleeping::OnEnter()
extern "C"  void IsSleeping_OnEnter_m4124282944 (IsSleeping_t599267933 * __this, const MethodInfo* method)
{
	{
		IsSleeping_DoIsSleeping_m2186122075(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IsSleeping::OnUpdate()
extern "C"  void IsSleeping_OnUpdate_m2432279043 (IsSleeping_t599267933 * __this, const MethodInfo* method)
{
	{
		IsSleeping_DoIsSleeping_m2186122075(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.IsSleeping::DoIsSleeping()
extern const MethodInfo* ComponentAction_1_UpdateCache_m864821753_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_rigidbody_m1866302531_MethodInfo_var;
extern const uint32_t IsSleeping_DoIsSleeping_m2186122075_MetadataUsageId;
extern "C"  void IsSleeping_DoIsSleeping_m2186122075 (IsSleeping_t599267933 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IsSleeping_DoIsSleeping_m2186122075_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	bool V_1 = false;
	Fsm_t1527112426 * G_B3_0 = NULL;
	Fsm_t1527112426 * G_B2_0 = NULL;
	FsmEvent_t2133468028 * G_B4_0 = NULL;
	Fsm_t1527112426 * G_B4_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m864821753(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m864821753_MethodInfo_var);
		if (!L_4)
		{
			goto IL_0058;
		}
	}
	{
		Rigidbody_t3346577219 * L_5 = ComponentAction_1_get_rigidbody_m1866302531(__this, /*hidden argument*/ComponentAction_1_get_rigidbody_m1866302531_MethodInfo_var);
		NullCheck(L_5);
		bool L_6 = Rigidbody_IsSleeping_m435617895(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		FsmBool_t1075959796 * L_7 = __this->get_store_14();
		bool L_8 = V_1;
		NullCheck(L_7);
		FsmBool_set_Value_m1126216340(L_7, L_8, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_9 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		bool L_10 = V_1;
		G_B2_0 = L_9;
		if (!L_10)
		{
			G_B3_0 = L_9;
			goto IL_004d;
		}
	}
	{
		FsmEvent_t2133468028 * L_11 = __this->get_trueEvent_12();
		G_B4_0 = L_11;
		G_B4_1 = G_B2_0;
		goto IL_0053;
	}

IL_004d:
	{
		FsmEvent_t2133468028 * L_12 = __this->get_falseEvent_13();
		G_B4_0 = L_12;
		G_B4_1 = G_B3_0;
	}

IL_0053:
	{
		NullCheck(G_B4_1);
		Fsm_Event_m625948263(G_B4_1, G_B4_0, /*hidden argument*/NULL);
	}

IL_0058:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenFsmAction::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t iTweenFsmAction__ctor_m2960815636_MetadataUsageId;
extern "C"  void iTweenFsmAction__ctor_m2960815636 (iTweenFsmAction_t410382178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenFsmAction__ctor_m2960815636_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_itweenType_15(L_0);
		__this->set_itweenID_16((-1));
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenFsmAction::Reset()
extern Il2CppClass* FsmBool_t1075959796_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t iTweenFsmAction_Reset_m607248577_MetadataUsageId;
extern "C"  void iTweenFsmAction_Reset_m607248577 (iTweenFsmAction_t410382178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenFsmAction_Reset_m607248577_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmBool_t1075959796 * V_0 = NULL;
	{
		__this->set_startEvent_9((FsmEvent_t2133468028 *)NULL);
		__this->set_finishEvent_10((FsmEvent_t2133468028 *)NULL);
		FsmBool_t1075959796 * L_0 = (FsmBool_t1075959796 *)il2cpp_codegen_object_new(FsmBool_t1075959796_il2cpp_TypeInfo_var);
		FsmBool__ctor_m1553455211(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmBool_t1075959796 * L_1 = V_0;
		NullCheck(L_1);
		FsmBool_set_Value_m1126216340(L_1, (bool)0, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_2 = V_0;
		__this->set_realTime_11(L_2);
		FsmBool_t1075959796 * L_3 = (FsmBool_t1075959796 *)il2cpp_codegen_object_new(FsmBool_t1075959796_il2cpp_TypeInfo_var);
		FsmBool__ctor_m1553455211(L_3, /*hidden argument*/NULL);
		V_0 = L_3;
		FsmBool_t1075959796 * L_4 = V_0;
		NullCheck(L_4);
		FsmBool_set_Value_m1126216340(L_4, (bool)1, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_5 = V_0;
		__this->set_stopOnExit_12(L_5);
		FsmBool_t1075959796 * L_6 = (FsmBool_t1075959796 *)il2cpp_codegen_object_new(FsmBool_t1075959796_il2cpp_TypeInfo_var);
		FsmBool__ctor_m1553455211(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		FsmBool_t1075959796 * L_7 = V_0;
		NullCheck(L_7);
		FsmBool_set_Value_m1126216340(L_7, (bool)1, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_8 = V_0;
		__this->set_loopDontFinish_13(L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_itweenType_15(L_9);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenFsmAction::OnEnteriTween(HutongGames.PlayMaker.FsmOwnerDefault)
extern Il2CppClass* iTweenFSMEvents_t871409943_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisiTweenFSMEvents_t871409943_m3828280117_MethodInfo_var;
extern const uint32_t iTweenFsmAction_OnEnteriTween_m3024539459_MetadataUsageId;
extern "C"  void iTweenFsmAction_OnEnteriTween_m3024539459 (iTweenFsmAction_t410382178 * __this, FsmOwnerDefault_t251897112 * ___anOwner0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenFsmAction_OnEnteriTween_m3024539459_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	iTweenFSMEvents_t871409943 * G_B4_0 = NULL;
	iTweenFSMEvents_t871409943 * G_B3_0 = NULL;
	int32_t G_B5_0 = 0;
	iTweenFSMEvents_t871409943 * G_B5_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = ___anOwner0;
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001a;
		}
	}
	{
		return;
	}

IL_001a:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		iTweenFSMEvents_t871409943 * L_6 = GameObject_AddComponent_TisiTweenFSMEvents_t871409943_m3828280117(L_5, /*hidden argument*/GameObject_AddComponent_TisiTweenFSMEvents_t871409943_m3828280117_MethodInfo_var);
		__this->set_itweenEvents_14(L_6);
		iTweenFSMEvents_t871409943 * L_7 = __this->get_itweenEvents_14();
		NullCheck(L_7);
		L_7->set_itweenFSMAction_4(__this);
		IL2CPP_RUNTIME_CLASS_INIT(iTweenFSMEvents_t871409943_il2cpp_TypeInfo_var);
		int32_t L_8 = ((iTweenFSMEvents_t871409943_StaticFields*)iTweenFSMEvents_t871409943_il2cpp_TypeInfo_var->static_fields)->get_itweenIDCount_2();
		((iTweenFSMEvents_t871409943_StaticFields*)iTweenFSMEvents_t871409943_il2cpp_TypeInfo_var->static_fields)->set_itweenIDCount_2(((int32_t)((int32_t)L_8+(int32_t)1)));
		int32_t L_9 = ((iTweenFSMEvents_t871409943_StaticFields*)iTweenFSMEvents_t871409943_il2cpp_TypeInfo_var->static_fields)->get_itweenIDCount_2();
		__this->set_itweenID_16(L_9);
		iTweenFSMEvents_t871409943 * L_10 = __this->get_itweenEvents_14();
		int32_t L_11 = ((iTweenFSMEvents_t871409943_StaticFields*)iTweenFSMEvents_t871409943_il2cpp_TypeInfo_var->static_fields)->get_itweenIDCount_2();
		NullCheck(L_10);
		L_10->set_itweenID_3(L_11);
		iTweenFSMEvents_t871409943 * L_12 = __this->get_itweenEvents_14();
		FsmBool_t1075959796 * L_13 = __this->get_loopDontFinish_13();
		NullCheck(L_13);
		bool L_14 = NamedVariable_get_IsNone_m281035543(L_13, /*hidden argument*/NULL);
		G_B3_0 = L_12;
		if (!L_14)
		{
			G_B4_0 = L_12;
			goto IL_0075;
		}
	}
	{
		G_B5_0 = 0;
		G_B5_1 = G_B3_0;
		goto IL_0080;
	}

IL_0075:
	{
		FsmBool_t1075959796 * L_15 = __this->get_loopDontFinish_13();
		NullCheck(L_15);
		bool L_16 = FsmBool_get_Value_m3101329097(L_15, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_16));
		G_B5_1 = G_B4_0;
	}

IL_0080:
	{
		NullCheck(G_B5_1);
		G_B5_1->set_donotfinish_5((bool)G_B5_0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenFsmAction::IsLoop(System.Boolean)
extern "C"  void iTweenFsmAction_IsLoop_m2539277653 (iTweenFsmAction_t410382178 * __this, bool ___aValue0, const MethodInfo* method)
{
	{
		iTweenFSMEvents_t871409943 * L_0 = __this->get_itweenEvents_14();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		iTweenFSMEvents_t871409943 * L_2 = __this->get_itweenEvents_14();
		bool L_3 = ___aValue0;
		NullCheck(L_2);
		L_2->set_islooping_6(L_3);
	}

IL_001d:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenFsmAction::OnExitiTween(HutongGames.PlayMaker.FsmOwnerDefault)
extern Il2CppClass* iTween_t3087282050_il2cpp_TypeInfo_var;
extern const uint32_t iTweenFsmAction_OnExitiTween_m1981625761_MetadataUsageId;
extern "C"  void iTweenFsmAction_OnExitiTween_m1981625761 (iTweenFsmAction_t410382178 * __this, FsmOwnerDefault_t251897112 * ___anOwner0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenFsmAction_OnExitiTween_m1981625761_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = ___anOwner0;
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001a;
		}
	}
	{
		return;
	}

IL_001a:
	{
		iTweenFSMEvents_t871409943 * L_5 = __this->get_itweenEvents_14();
		bool L_6 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0035;
		}
	}
	{
		iTweenFSMEvents_t871409943 * L_7 = __this->get_itweenEvents_14();
		Object_Destroy_m176400816(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_0035:
	{
		FsmBool_t1075959796 * L_8 = __this->get_stopOnExit_12();
		NullCheck(L_8);
		bool L_9 = NamedVariable_get_IsNone_m281035543(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0056;
		}
	}
	{
		GameObject_t3674682005 * L_10 = V_0;
		String_t* L_11 = __this->get_itweenType_15();
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		iTween_Stop_m2101488001(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		goto IL_0072;
	}

IL_0056:
	{
		FsmBool_t1075959796 * L_12 = __this->get_stopOnExit_12();
		NullCheck(L_12);
		bool L_13 = FsmBool_get_Value_m3101329097(L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0072;
		}
	}
	{
		GameObject_t3674682005 * L_14 = V_0;
		String_t* L_15 = __this->get_itweenType_15();
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		iTween_Stop_m2101488001(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
	}

IL_0072:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenLookFrom::.ctor()
extern "C"  void iTweenLookFrom__ctor_m2311216755 (iTweenLookFrom_t3433969491 * __this, const MethodInfo* method)
{
	{
		__this->set_easeType_24(((int32_t)21));
		iTweenFsmAction__ctor_m2960815636(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenLookFrom::Reset()
extern Il2CppClass* FsmString_t952858651_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmGameObject_t1697147867_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmFloat_t2134102846_il2cpp_TypeInfo_var;
extern const uint32_t iTweenLookFrom_Reset_m4252616992_MetadataUsageId;
extern "C"  void iTweenLookFrom_Reset_m4252616992 (iTweenLookFrom_t3433969491 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenLookFrom_Reset_m4252616992_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmString_t952858651 * V_0 = NULL;
	FsmGameObject_t1697147867 * V_1 = NULL;
	FsmVector3_t533912882 * V_2 = NULL;
	FsmFloat_t2134102846 * V_3 = NULL;
	{
		iTweenFsmAction_Reset_m607248577(__this, /*hidden argument*/NULL);
		FsmString_t952858651 * L_0 = (FsmString_t952858651 *)il2cpp_codegen_object_new(FsmString_t952858651_il2cpp_TypeInfo_var);
		FsmString__ctor_m2679108260(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmString_t952858651 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmString_t952858651 * L_2 = V_0;
		__this->set_id_18(L_2);
		FsmGameObject_t1697147867 * L_3 = (FsmGameObject_t1697147867 *)il2cpp_codegen_object_new(FsmGameObject_t1697147867_il2cpp_TypeInfo_var);
		FsmGameObject__ctor_m1048269796(L_3, /*hidden argument*/NULL);
		V_1 = L_3;
		FsmGameObject_t1697147867 * L_4 = V_1;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_5 = V_1;
		__this->set_transformTarget_19(L_5);
		FsmVector3_t533912882 * L_6 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_6, /*hidden argument*/NULL);
		V_2 = L_6;
		FsmVector3_t533912882 * L_7 = V_2;
		NullCheck(L_7);
		NamedVariable_set_UseVariable_m4266138971(L_7, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_8 = V_2;
		__this->set_vectorTarget_20(L_8);
		FsmFloat_t2134102846 * L_9 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_time_21(L_9);
		FsmFloat_t2134102846 * L_10 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_delay_22(L_10);
		__this->set_loopType_25(0);
		FsmFloat_t2134102846 * L_11 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_11, /*hidden argument*/NULL);
		V_3 = L_11;
		FsmFloat_t2134102846 * L_12 = V_3;
		NullCheck(L_12);
		NamedVariable_set_UseVariable_m4266138971(L_12, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_13 = V_3;
		__this->set_speed_23(L_13);
		__this->set_axis_26(0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenLookFrom::OnEnter()
extern "C"  void iTweenLookFrom_OnEnter_m4267905418 (iTweenLookFrom_t3433969491 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnEnteriTween_m3024539459(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_loopType_25();
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		iTweenFsmAction_IsLoop_m2539277653(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_001e:
	{
		iTweenLookFrom_DoiTween_m612214014(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenLookFrom::OnExit()
extern "C"  void iTweenLookFrom_OnExit_m146593998 (iTweenLookFrom_t3433969491 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnExitiTween_m1981625761(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenLookFrom::DoiTween()
extern const Il2CppType* AxisRestriction_t3260241011_0_0_0_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* EaseType_t2734598229_il2cpp_TypeInfo_var;
extern Il2CppClass* LoopType_t1485160459_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* AxisRestriction_t3260241011_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2862688501_il2cpp_TypeInfo_var;
extern Il2CppClass* iTween_t3087282050_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3369786715;
extern Il2CppCodeGenString* _stringLiteral1258521840;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral3560141;
extern Il2CppCodeGenString* _stringLiteral109641799;
extern Il2CppCodeGenString* _stringLiteral95467907;
extern Il2CppCodeGenString* _stringLiteral3507899432;
extern Il2CppCodeGenString* _stringLiteral2258461662;
extern Il2CppCodeGenString* _stringLiteral2105864216;
extern Il2CppCodeGenString* _stringLiteral338257242;
extern Il2CppCodeGenString* _stringLiteral3696326558;
extern Il2CppCodeGenString* _stringLiteral2987624931;
extern Il2CppCodeGenString* _stringLiteral647687137;
extern Il2CppCodeGenString* _stringLiteral2052738345;
extern Il2CppCodeGenString* _stringLiteral1067506955;
extern Il2CppCodeGenString* _stringLiteral3008417;
extern const uint32_t iTweenLookFrom_DoiTween_m612214014_MetadataUsageId;
extern "C"  void iTweenLookFrom_DoiTween_m612214014 (iTweenLookFrom_t3433969491 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenLookFrom_DoiTween_m612214014_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  G_B5_0;
	memset(&G_B5_0, 0, sizeof(G_B5_0));
	int32_t G_B10_0 = 0;
	ObjectU5BU5D_t1108656482* G_B10_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B10_2 = NULL;
	GameObject_t3674682005 * G_B10_3 = NULL;
	int32_t G_B9_0 = 0;
	ObjectU5BU5D_t1108656482* G_B9_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B9_2 = NULL;
	GameObject_t3674682005 * G_B9_3 = NULL;
	String_t* G_B11_0 = NULL;
	int32_t G_B11_1 = 0;
	ObjectU5BU5D_t1108656482* G_B11_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B11_3 = NULL;
	GameObject_t3674682005 * G_B11_4 = NULL;
	int32_t G_B13_0 = 0;
	ObjectU5BU5D_t1108656482* G_B13_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B13_2 = NULL;
	GameObject_t3674682005 * G_B13_3 = NULL;
	int32_t G_B12_0 = 0;
	ObjectU5BU5D_t1108656482* G_B12_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B12_2 = NULL;
	GameObject_t3674682005 * G_B12_3 = NULL;
	String_t* G_B14_0 = NULL;
	int32_t G_B14_1 = 0;
	ObjectU5BU5D_t1108656482* G_B14_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B14_3 = NULL;
	GameObject_t3674682005 * G_B14_4 = NULL;
	int32_t G_B19_0 = 0;
	ObjectU5BU5D_t1108656482* G_B19_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B19_2 = NULL;
	GameObject_t3674682005 * G_B19_3 = NULL;
	int32_t G_B15_0 = 0;
	ObjectU5BU5D_t1108656482* G_B15_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B15_2 = NULL;
	GameObject_t3674682005 * G_B15_3 = NULL;
	int32_t G_B17_0 = 0;
	ObjectU5BU5D_t1108656482* G_B17_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B17_2 = NULL;
	GameObject_t3674682005 * G_B17_3 = NULL;
	int32_t G_B16_0 = 0;
	ObjectU5BU5D_t1108656482* G_B16_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B16_2 = NULL;
	GameObject_t3674682005 * G_B16_3 = NULL;
	float G_B18_0 = 0.0f;
	int32_t G_B18_1 = 0;
	ObjectU5BU5D_t1108656482* G_B18_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B18_3 = NULL;
	GameObject_t3674682005 * G_B18_4 = NULL;
	float G_B20_0 = 0.0f;
	int32_t G_B20_1 = 0;
	ObjectU5BU5D_t1108656482* G_B20_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B20_3 = NULL;
	GameObject_t3674682005 * G_B20_4 = NULL;
	int32_t G_B22_0 = 0;
	ObjectU5BU5D_t1108656482* G_B22_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B22_2 = NULL;
	GameObject_t3674682005 * G_B22_3 = NULL;
	int32_t G_B21_0 = 0;
	ObjectU5BU5D_t1108656482* G_B21_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B21_2 = NULL;
	GameObject_t3674682005 * G_B21_3 = NULL;
	float G_B23_0 = 0.0f;
	int32_t G_B23_1 = 0;
	ObjectU5BU5D_t1108656482* G_B23_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B23_3 = NULL;
	GameObject_t3674682005 * G_B23_4 = NULL;
	int32_t G_B25_0 = 0;
	ObjectU5BU5D_t1108656482* G_B25_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B25_2 = NULL;
	GameObject_t3674682005 * G_B25_3 = NULL;
	int32_t G_B24_0 = 0;
	ObjectU5BU5D_t1108656482* G_B24_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B24_2 = NULL;
	GameObject_t3674682005 * G_B24_3 = NULL;
	int32_t G_B26_0 = 0;
	int32_t G_B26_1 = 0;
	ObjectU5BU5D_t1108656482* G_B26_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B26_3 = NULL;
	GameObject_t3674682005 * G_B26_4 = NULL;
	int32_t G_B28_0 = 0;
	ObjectU5BU5D_t1108656482* G_B28_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B28_2 = NULL;
	GameObject_t3674682005 * G_B28_3 = NULL;
	int32_t G_B27_0 = 0;
	ObjectU5BU5D_t1108656482* G_B27_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B27_2 = NULL;
	GameObject_t3674682005 * G_B27_3 = NULL;
	String_t* G_B29_0 = NULL;
	int32_t G_B29_1 = 0;
	ObjectU5BU5D_t1108656482* G_B29_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B29_3 = NULL;
	GameObject_t3674682005 * G_B29_4 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_17();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		FsmVector3_t533912882 * L_5 = __this->get_vectorTarget_20();
		NullCheck(L_5);
		bool L_6 = NamedVariable_get_IsNone_m281035543(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0039;
		}
	}
	{
		Vector3_t4282066566  L_7 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B5_0 = L_7;
		goto IL_0044;
	}

IL_0039:
	{
		FsmVector3_t533912882 * L_8 = __this->get_vectorTarget_20();
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = FsmVector3_get_Value_m2779135117(L_8, /*hidden argument*/NULL);
		G_B5_0 = L_9;
	}

IL_0044:
	{
		V_1 = G_B5_0;
		FsmGameObject_t1697147867 * L_10 = __this->get_transformTarget_19();
		NullCheck(L_10);
		bool L_11 = NamedVariable_get_IsNone_m281035543(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0086;
		}
	}
	{
		FsmGameObject_t1697147867 * L_12 = __this->get_transformTarget_19();
		NullCheck(L_12);
		GameObject_t3674682005 * L_13 = FsmGameObject_get_Value_m673294275(L_12, /*hidden argument*/NULL);
		bool L_14 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0086;
		}
	}
	{
		FsmGameObject_t1697147867 * L_15 = __this->get_transformTarget_19();
		NullCheck(L_15);
		GameObject_t3674682005 * L_16 = FsmGameObject_get_Value_m673294275(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_t1659122786 * L_17 = GameObject_get_transform_m1278640159(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t4282066566  L_18 = Transform_get_position_m2211398607(L_17, /*hidden argument*/NULL);
		Vector3_t4282066566  L_19 = V_1;
		Vector3_t4282066566  L_20 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		V_1 = L_20;
	}

IL_0086:
	{
		((iTweenFsmAction_t410382178 *)__this)->set_itweenType_15(_stringLiteral3369786715);
		GameObject_t3674682005 * L_21 = V_0;
		ObjectU5BU5D_t1108656482* L_22 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)24)));
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 0);
		ArrayElementTypeCheck (L_22, _stringLiteral1258521840);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1258521840);
		ObjectU5BU5D_t1108656482* L_23 = L_22;
		Vector3_t4282066566  L_24 = V_1;
		Vector3_t4282066566  L_25 = L_24;
		Il2CppObject * L_26 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_25);
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 1);
		ArrayElementTypeCheck (L_23, L_26);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_26);
		ObjectU5BU5D_t1108656482* L_27 = L_23;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, 2);
		ArrayElementTypeCheck (L_27, _stringLiteral3373707);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3373707);
		ObjectU5BU5D_t1108656482* L_28 = L_27;
		FsmString_t952858651 * L_29 = __this->get_id_18();
		NullCheck(L_29);
		bool L_30 = NamedVariable_get_IsNone_m281035543(L_29, /*hidden argument*/NULL);
		G_B9_0 = 3;
		G_B9_1 = L_28;
		G_B9_2 = L_28;
		G_B9_3 = L_21;
		if (!L_30)
		{
			G_B10_0 = 3;
			G_B10_1 = L_28;
			G_B10_2 = L_28;
			G_B10_3 = L_21;
			goto IL_00ce;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_31 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B11_0 = L_31;
		G_B11_1 = G_B9_0;
		G_B11_2 = G_B9_1;
		G_B11_3 = G_B9_2;
		G_B11_4 = G_B9_3;
		goto IL_00d9;
	}

IL_00ce:
	{
		FsmString_t952858651 * L_32 = __this->get_id_18();
		NullCheck(L_32);
		String_t* L_33 = FsmString_get_Value_m872383149(L_32, /*hidden argument*/NULL);
		G_B11_0 = L_33;
		G_B11_1 = G_B10_0;
		G_B11_2 = G_B10_1;
		G_B11_3 = G_B10_2;
		G_B11_4 = G_B10_3;
	}

IL_00d9:
	{
		NullCheck(G_B11_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B11_2, G_B11_1);
		ArrayElementTypeCheck (G_B11_2, G_B11_0);
		(G_B11_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B11_1), (Il2CppObject *)G_B11_0);
		ObjectU5BU5D_t1108656482* L_34 = G_B11_3;
		FsmFloat_t2134102846 * L_35 = __this->get_speed_23();
		NullCheck(L_35);
		bool L_36 = NamedVariable_get_IsNone_m281035543(L_35, /*hidden argument*/NULL);
		G_B12_0 = 4;
		G_B12_1 = L_34;
		G_B12_2 = L_34;
		G_B12_3 = G_B11_4;
		if (!L_36)
		{
			G_B13_0 = 4;
			G_B13_1 = L_34;
			G_B13_2 = L_34;
			G_B13_3 = G_B11_4;
			goto IL_00f6;
		}
	}
	{
		G_B14_0 = _stringLiteral3560141;
		G_B14_1 = G_B12_0;
		G_B14_2 = G_B12_1;
		G_B14_3 = G_B12_2;
		G_B14_4 = G_B12_3;
		goto IL_00fb;
	}

IL_00f6:
	{
		G_B14_0 = _stringLiteral109641799;
		G_B14_1 = G_B13_0;
		G_B14_2 = G_B13_1;
		G_B14_3 = G_B13_2;
		G_B14_4 = G_B13_3;
	}

IL_00fb:
	{
		NullCheck(G_B14_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B14_2, G_B14_1);
		ArrayElementTypeCheck (G_B14_2, G_B14_0);
		(G_B14_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B14_1), (Il2CppObject *)G_B14_0);
		ObjectU5BU5D_t1108656482* L_37 = G_B14_3;
		FsmFloat_t2134102846 * L_38 = __this->get_speed_23();
		NullCheck(L_38);
		bool L_39 = NamedVariable_get_IsNone_m281035543(L_38, /*hidden argument*/NULL);
		G_B15_0 = 5;
		G_B15_1 = L_37;
		G_B15_2 = L_37;
		G_B15_3 = G_B14_4;
		if (!L_39)
		{
			G_B19_0 = 5;
			G_B19_1 = L_37;
			G_B19_2 = L_37;
			G_B19_3 = G_B14_4;
			goto IL_0138;
		}
	}
	{
		FsmFloat_t2134102846 * L_40 = __this->get_time_21();
		NullCheck(L_40);
		bool L_41 = NamedVariable_get_IsNone_m281035543(L_40, /*hidden argument*/NULL);
		G_B16_0 = G_B15_0;
		G_B16_1 = G_B15_1;
		G_B16_2 = G_B15_2;
		G_B16_3 = G_B15_3;
		if (!L_41)
		{
			G_B17_0 = G_B15_0;
			G_B17_1 = G_B15_1;
			G_B17_2 = G_B15_2;
			G_B17_3 = G_B15_3;
			goto IL_0128;
		}
	}
	{
		G_B18_0 = (1.0f);
		G_B18_1 = G_B16_0;
		G_B18_2 = G_B16_1;
		G_B18_3 = G_B16_2;
		G_B18_4 = G_B16_3;
		goto IL_0133;
	}

IL_0128:
	{
		FsmFloat_t2134102846 * L_42 = __this->get_time_21();
		NullCheck(L_42);
		float L_43 = FsmFloat_get_Value_m4137923823(L_42, /*hidden argument*/NULL);
		G_B18_0 = L_43;
		G_B18_1 = G_B17_0;
		G_B18_2 = G_B17_1;
		G_B18_3 = G_B17_2;
		G_B18_4 = G_B17_3;
	}

IL_0133:
	{
		G_B20_0 = G_B18_0;
		G_B20_1 = G_B18_1;
		G_B20_2 = G_B18_2;
		G_B20_3 = G_B18_3;
		G_B20_4 = G_B18_4;
		goto IL_0143;
	}

IL_0138:
	{
		FsmFloat_t2134102846 * L_44 = __this->get_speed_23();
		NullCheck(L_44);
		float L_45 = FsmFloat_get_Value_m4137923823(L_44, /*hidden argument*/NULL);
		G_B20_0 = L_45;
		G_B20_1 = G_B19_0;
		G_B20_2 = G_B19_1;
		G_B20_3 = G_B19_2;
		G_B20_4 = G_B19_3;
	}

IL_0143:
	{
		float L_46 = G_B20_0;
		Il2CppObject * L_47 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_46);
		NullCheck(G_B20_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B20_2, G_B20_1);
		ArrayElementTypeCheck (G_B20_2, L_47);
		(G_B20_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B20_1), (Il2CppObject *)L_47);
		ObjectU5BU5D_t1108656482* L_48 = G_B20_3;
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, 6);
		ArrayElementTypeCheck (L_48, _stringLiteral95467907);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral95467907);
		ObjectU5BU5D_t1108656482* L_49 = L_48;
		FsmFloat_t2134102846 * L_50 = __this->get_delay_22();
		NullCheck(L_50);
		bool L_51 = NamedVariable_get_IsNone_m281035543(L_50, /*hidden argument*/NULL);
		G_B21_0 = 7;
		G_B21_1 = L_49;
		G_B21_2 = L_49;
		G_B21_3 = G_B20_4;
		if (!L_51)
		{
			G_B22_0 = 7;
			G_B22_1 = L_49;
			G_B22_2 = L_49;
			G_B22_3 = G_B20_4;
			goto IL_016d;
		}
	}
	{
		G_B23_0 = (0.0f);
		G_B23_1 = G_B21_0;
		G_B23_2 = G_B21_1;
		G_B23_3 = G_B21_2;
		G_B23_4 = G_B21_3;
		goto IL_0178;
	}

IL_016d:
	{
		FsmFloat_t2134102846 * L_52 = __this->get_delay_22();
		NullCheck(L_52);
		float L_53 = FsmFloat_get_Value_m4137923823(L_52, /*hidden argument*/NULL);
		G_B23_0 = L_53;
		G_B23_1 = G_B22_0;
		G_B23_2 = G_B22_1;
		G_B23_3 = G_B22_2;
		G_B23_4 = G_B22_3;
	}

IL_0178:
	{
		float L_54 = G_B23_0;
		Il2CppObject * L_55 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_54);
		NullCheck(G_B23_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B23_2, G_B23_1);
		ArrayElementTypeCheck (G_B23_2, L_55);
		(G_B23_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B23_1), (Il2CppObject *)L_55);
		ObjectU5BU5D_t1108656482* L_56 = G_B23_3;
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, 8);
		ArrayElementTypeCheck (L_56, _stringLiteral3507899432);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral3507899432);
		ObjectU5BU5D_t1108656482* L_57 = L_56;
		int32_t L_58 = __this->get_easeType_24();
		int32_t L_59 = L_58;
		Il2CppObject * L_60 = Box(EaseType_t2734598229_il2cpp_TypeInfo_var, &L_59);
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, ((int32_t)9));
		ArrayElementTypeCheck (L_57, L_60);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)L_60);
		ObjectU5BU5D_t1108656482* L_61 = L_57;
		NullCheck(L_61);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_61, ((int32_t)10));
		ArrayElementTypeCheck (L_61, _stringLiteral2258461662);
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Il2CppObject *)_stringLiteral2258461662);
		ObjectU5BU5D_t1108656482* L_62 = L_61;
		int32_t L_63 = __this->get_loopType_25();
		int32_t L_64 = L_63;
		Il2CppObject * L_65 = Box(LoopType_t1485160459_il2cpp_TypeInfo_var, &L_64);
		NullCheck(L_62);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_62, ((int32_t)11));
		ArrayElementTypeCheck (L_62, L_65);
		(L_62)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (Il2CppObject *)L_65);
		ObjectU5BU5D_t1108656482* L_66 = L_62;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, ((int32_t)12));
		ArrayElementTypeCheck (L_66, _stringLiteral2105864216);
		(L_66)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (Il2CppObject *)_stringLiteral2105864216);
		ObjectU5BU5D_t1108656482* L_67 = L_66;
		NullCheck(L_67);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_67, ((int32_t)13));
		ArrayElementTypeCheck (L_67, _stringLiteral338257242);
		(L_67)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (Il2CppObject *)_stringLiteral338257242);
		ObjectU5BU5D_t1108656482* L_68 = L_67;
		NullCheck(L_68);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_68, ((int32_t)14));
		ArrayElementTypeCheck (L_68, _stringLiteral3696326558);
		(L_68)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (Il2CppObject *)_stringLiteral3696326558);
		ObjectU5BU5D_t1108656482* L_69 = L_68;
		int32_t L_70 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_71 = L_70;
		Il2CppObject * L_72 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_71);
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, ((int32_t)15));
		ArrayElementTypeCheck (L_69, L_72);
		(L_69)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (Il2CppObject *)L_72);
		ObjectU5BU5D_t1108656482* L_73 = L_69;
		NullCheck(L_73);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_73, ((int32_t)16));
		ArrayElementTypeCheck (L_73, _stringLiteral2987624931);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (Il2CppObject *)_stringLiteral2987624931);
		ObjectU5BU5D_t1108656482* L_74 = L_73;
		NullCheck(L_74);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_74, ((int32_t)17));
		ArrayElementTypeCheck (L_74, _stringLiteral647687137);
		(L_74)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (Il2CppObject *)_stringLiteral647687137);
		ObjectU5BU5D_t1108656482* L_75 = L_74;
		NullCheck(L_75);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_75, ((int32_t)18));
		ArrayElementTypeCheck (L_75, _stringLiteral2052738345);
		(L_75)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (Il2CppObject *)_stringLiteral2052738345);
		ObjectU5BU5D_t1108656482* L_76 = L_75;
		int32_t L_77 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_78 = L_77;
		Il2CppObject * L_79 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_78);
		NullCheck(L_76);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_76, ((int32_t)19));
		ArrayElementTypeCheck (L_76, L_79);
		(L_76)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)19)), (Il2CppObject *)L_79);
		ObjectU5BU5D_t1108656482* L_80 = L_76;
		NullCheck(L_80);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_80, ((int32_t)20));
		ArrayElementTypeCheck (L_80, _stringLiteral1067506955);
		(L_80)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)20)), (Il2CppObject *)_stringLiteral1067506955);
		ObjectU5BU5D_t1108656482* L_81 = L_80;
		FsmBool_t1075959796 * L_82 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_82);
		bool L_83 = NamedVariable_get_IsNone_m281035543(L_82, /*hidden argument*/NULL);
		G_B24_0 = ((int32_t)21);
		G_B24_1 = L_81;
		G_B24_2 = L_81;
		G_B24_3 = G_B23_4;
		if (!L_83)
		{
			G_B25_0 = ((int32_t)21);
			G_B25_1 = L_81;
			G_B25_2 = L_81;
			G_B25_3 = G_B23_4;
			goto IL_0223;
		}
	}
	{
		G_B26_0 = 0;
		G_B26_1 = G_B24_0;
		G_B26_2 = G_B24_1;
		G_B26_3 = G_B24_2;
		G_B26_4 = G_B24_3;
		goto IL_022e;
	}

IL_0223:
	{
		FsmBool_t1075959796 * L_84 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_84);
		bool L_85 = FsmBool_get_Value_m3101329097(L_84, /*hidden argument*/NULL);
		G_B26_0 = ((int32_t)(L_85));
		G_B26_1 = G_B25_0;
		G_B26_2 = G_B25_1;
		G_B26_3 = G_B25_2;
		G_B26_4 = G_B25_3;
	}

IL_022e:
	{
		bool L_86 = ((bool)G_B26_0);
		Il2CppObject * L_87 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_86);
		NullCheck(G_B26_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B26_2, G_B26_1);
		ArrayElementTypeCheck (G_B26_2, L_87);
		(G_B26_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B26_1), (Il2CppObject *)L_87);
		ObjectU5BU5D_t1108656482* L_88 = G_B26_3;
		NullCheck(L_88);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_88, ((int32_t)22));
		ArrayElementTypeCheck (L_88, _stringLiteral3008417);
		(L_88)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)22)), (Il2CppObject *)_stringLiteral3008417);
		ObjectU5BU5D_t1108656482* L_89 = L_88;
		int32_t L_90 = __this->get_axis_26();
		G_B27_0 = ((int32_t)23);
		G_B27_1 = L_89;
		G_B27_2 = L_89;
		G_B27_3 = G_B26_4;
		if (L_90)
		{
			G_B28_0 = ((int32_t)23);
			G_B28_1 = L_89;
			G_B28_2 = L_89;
			G_B28_3 = G_B26_4;
			goto IL_0255;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_91 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B29_0 = L_91;
		G_B29_1 = G_B27_0;
		G_B29_2 = G_B27_1;
		G_B29_3 = G_B27_2;
		G_B29_4 = G_B27_3;
		goto IL_026f;
	}

IL_0255:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_92 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(AxisRestriction_t3260241011_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_93 = __this->get_axis_26();
		int32_t L_94 = L_93;
		Il2CppObject * L_95 = Box(AxisRestriction_t3260241011_il2cpp_TypeInfo_var, &L_94);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		String_t* L_96 = Enum_GetName_m3333121265(NULL /*static, unused*/, L_92, L_95, /*hidden argument*/NULL);
		G_B29_0 = L_96;
		G_B29_1 = G_B28_0;
		G_B29_2 = G_B28_1;
		G_B29_3 = G_B28_2;
		G_B29_4 = G_B28_3;
	}

IL_026f:
	{
		NullCheck(G_B29_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B29_2, G_B29_1);
		ArrayElementTypeCheck (G_B29_2, G_B29_0);
		(G_B29_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B29_1), (Il2CppObject *)G_B29_0);
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		Hashtable_t1407064410 * L_97 = iTween_Hash_m2099495140(NULL /*static, unused*/, G_B29_3, /*hidden argument*/NULL);
		iTween_LookFrom_m3853452498(NULL /*static, unused*/, G_B29_4, L_97, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenLookTo::.ctor()
extern "C"  void iTweenLookTo__ctor_m1203261762 (iTweenLookTo_t440825252 * __this, const MethodInfo* method)
{
	{
		__this->set_easeType_24(((int32_t)21));
		iTweenFsmAction__ctor_m2960815636(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenLookTo::Reset()
extern Il2CppClass* FsmString_t952858651_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmGameObject_t1697147867_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmFloat_t2134102846_il2cpp_TypeInfo_var;
extern const uint32_t iTweenLookTo_Reset_m3144661999_MetadataUsageId;
extern "C"  void iTweenLookTo_Reset_m3144661999 (iTweenLookTo_t440825252 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenLookTo_Reset_m3144661999_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmString_t952858651 * V_0 = NULL;
	FsmGameObject_t1697147867 * V_1 = NULL;
	FsmVector3_t533912882 * V_2 = NULL;
	FsmFloat_t2134102846 * V_3 = NULL;
	{
		iTweenFsmAction_Reset_m607248577(__this, /*hidden argument*/NULL);
		FsmString_t952858651 * L_0 = (FsmString_t952858651 *)il2cpp_codegen_object_new(FsmString_t952858651_il2cpp_TypeInfo_var);
		FsmString__ctor_m2679108260(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmString_t952858651 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmString_t952858651 * L_2 = V_0;
		__this->set_id_18(L_2);
		FsmGameObject_t1697147867 * L_3 = (FsmGameObject_t1697147867 *)il2cpp_codegen_object_new(FsmGameObject_t1697147867_il2cpp_TypeInfo_var);
		FsmGameObject__ctor_m1048269796(L_3, /*hidden argument*/NULL);
		V_1 = L_3;
		FsmGameObject_t1697147867 * L_4 = V_1;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_5 = V_1;
		__this->set_transformTarget_19(L_5);
		FsmVector3_t533912882 * L_6 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_6, /*hidden argument*/NULL);
		V_2 = L_6;
		FsmVector3_t533912882 * L_7 = V_2;
		NullCheck(L_7);
		NamedVariable_set_UseVariable_m4266138971(L_7, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_8 = V_2;
		__this->set_vectorTarget_20(L_8);
		FsmFloat_t2134102846 * L_9 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_time_21(L_9);
		FsmFloat_t2134102846 * L_10 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_delay_22(L_10);
		__this->set_loopType_25(0);
		FsmFloat_t2134102846 * L_11 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_11, /*hidden argument*/NULL);
		V_3 = L_11;
		FsmFloat_t2134102846 * L_12 = V_3;
		NullCheck(L_12);
		NamedVariable_set_UseVariable_m4266138971(L_12, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_13 = V_3;
		__this->set_speed_23(L_13);
		__this->set_axis_26(0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenLookTo::OnEnter()
extern "C"  void iTweenLookTo_OnEnter_m380079257 (iTweenLookTo_t440825252 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnEnteriTween_m3024539459(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_loopType_25();
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		iTweenFsmAction_IsLoop_m2539277653(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_001e:
	{
		iTweenLookTo_DoiTween_m348687311(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenLookTo::OnExit()
extern "C"  void iTweenLookTo_OnExit_m159727583 (iTweenLookTo_t440825252 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnExitiTween_m1981625761(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenLookTo::DoiTween()
extern const Il2CppType* AxisRestriction_t3260241011_0_0_0_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* EaseType_t2734598229_il2cpp_TypeInfo_var;
extern Il2CppClass* LoopType_t1485160459_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* AxisRestriction_t3260241011_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2862688501_il2cpp_TypeInfo_var;
extern Il2CppClass* iTween_t3087282050_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3369786715;
extern Il2CppCodeGenString* _stringLiteral1258521840;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral3560141;
extern Il2CppCodeGenString* _stringLiteral109641799;
extern Il2CppCodeGenString* _stringLiteral95467907;
extern Il2CppCodeGenString* _stringLiteral3507899432;
extern Il2CppCodeGenString* _stringLiteral2258461662;
extern Il2CppCodeGenString* _stringLiteral2105864216;
extern Il2CppCodeGenString* _stringLiteral338257242;
extern Il2CppCodeGenString* _stringLiteral3696326558;
extern Il2CppCodeGenString* _stringLiteral2987624931;
extern Il2CppCodeGenString* _stringLiteral647687137;
extern Il2CppCodeGenString* _stringLiteral2052738345;
extern Il2CppCodeGenString* _stringLiteral1067506955;
extern Il2CppCodeGenString* _stringLiteral3008417;
extern const uint32_t iTweenLookTo_DoiTween_m348687311_MetadataUsageId;
extern "C"  void iTweenLookTo_DoiTween_m348687311 (iTweenLookTo_t440825252 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenLookTo_DoiTween_m348687311_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  G_B5_0;
	memset(&G_B5_0, 0, sizeof(G_B5_0));
	int32_t G_B10_0 = 0;
	ObjectU5BU5D_t1108656482* G_B10_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B10_2 = NULL;
	GameObject_t3674682005 * G_B10_3 = NULL;
	int32_t G_B9_0 = 0;
	ObjectU5BU5D_t1108656482* G_B9_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B9_2 = NULL;
	GameObject_t3674682005 * G_B9_3 = NULL;
	String_t* G_B11_0 = NULL;
	int32_t G_B11_1 = 0;
	ObjectU5BU5D_t1108656482* G_B11_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B11_3 = NULL;
	GameObject_t3674682005 * G_B11_4 = NULL;
	int32_t G_B13_0 = 0;
	ObjectU5BU5D_t1108656482* G_B13_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B13_2 = NULL;
	GameObject_t3674682005 * G_B13_3 = NULL;
	int32_t G_B12_0 = 0;
	ObjectU5BU5D_t1108656482* G_B12_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B12_2 = NULL;
	GameObject_t3674682005 * G_B12_3 = NULL;
	String_t* G_B14_0 = NULL;
	int32_t G_B14_1 = 0;
	ObjectU5BU5D_t1108656482* G_B14_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B14_3 = NULL;
	GameObject_t3674682005 * G_B14_4 = NULL;
	int32_t G_B19_0 = 0;
	ObjectU5BU5D_t1108656482* G_B19_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B19_2 = NULL;
	GameObject_t3674682005 * G_B19_3 = NULL;
	int32_t G_B15_0 = 0;
	ObjectU5BU5D_t1108656482* G_B15_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B15_2 = NULL;
	GameObject_t3674682005 * G_B15_3 = NULL;
	int32_t G_B17_0 = 0;
	ObjectU5BU5D_t1108656482* G_B17_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B17_2 = NULL;
	GameObject_t3674682005 * G_B17_3 = NULL;
	int32_t G_B16_0 = 0;
	ObjectU5BU5D_t1108656482* G_B16_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B16_2 = NULL;
	GameObject_t3674682005 * G_B16_3 = NULL;
	float G_B18_0 = 0.0f;
	int32_t G_B18_1 = 0;
	ObjectU5BU5D_t1108656482* G_B18_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B18_3 = NULL;
	GameObject_t3674682005 * G_B18_4 = NULL;
	float G_B20_0 = 0.0f;
	int32_t G_B20_1 = 0;
	ObjectU5BU5D_t1108656482* G_B20_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B20_3 = NULL;
	GameObject_t3674682005 * G_B20_4 = NULL;
	int32_t G_B22_0 = 0;
	ObjectU5BU5D_t1108656482* G_B22_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B22_2 = NULL;
	GameObject_t3674682005 * G_B22_3 = NULL;
	int32_t G_B21_0 = 0;
	ObjectU5BU5D_t1108656482* G_B21_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B21_2 = NULL;
	GameObject_t3674682005 * G_B21_3 = NULL;
	float G_B23_0 = 0.0f;
	int32_t G_B23_1 = 0;
	ObjectU5BU5D_t1108656482* G_B23_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B23_3 = NULL;
	GameObject_t3674682005 * G_B23_4 = NULL;
	int32_t G_B25_0 = 0;
	ObjectU5BU5D_t1108656482* G_B25_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B25_2 = NULL;
	GameObject_t3674682005 * G_B25_3 = NULL;
	int32_t G_B24_0 = 0;
	ObjectU5BU5D_t1108656482* G_B24_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B24_2 = NULL;
	GameObject_t3674682005 * G_B24_3 = NULL;
	int32_t G_B26_0 = 0;
	int32_t G_B26_1 = 0;
	ObjectU5BU5D_t1108656482* G_B26_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B26_3 = NULL;
	GameObject_t3674682005 * G_B26_4 = NULL;
	int32_t G_B28_0 = 0;
	ObjectU5BU5D_t1108656482* G_B28_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B28_2 = NULL;
	GameObject_t3674682005 * G_B28_3 = NULL;
	int32_t G_B27_0 = 0;
	ObjectU5BU5D_t1108656482* G_B27_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B27_2 = NULL;
	GameObject_t3674682005 * G_B27_3 = NULL;
	String_t* G_B29_0 = NULL;
	int32_t G_B29_1 = 0;
	ObjectU5BU5D_t1108656482* G_B29_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B29_3 = NULL;
	GameObject_t3674682005 * G_B29_4 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_17();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		FsmVector3_t533912882 * L_5 = __this->get_vectorTarget_20();
		NullCheck(L_5);
		bool L_6 = NamedVariable_get_IsNone_m281035543(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0039;
		}
	}
	{
		Vector3_t4282066566  L_7 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B5_0 = L_7;
		goto IL_0044;
	}

IL_0039:
	{
		FsmVector3_t533912882 * L_8 = __this->get_vectorTarget_20();
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = FsmVector3_get_Value_m2779135117(L_8, /*hidden argument*/NULL);
		G_B5_0 = L_9;
	}

IL_0044:
	{
		V_1 = G_B5_0;
		FsmGameObject_t1697147867 * L_10 = __this->get_transformTarget_19();
		NullCheck(L_10);
		bool L_11 = NamedVariable_get_IsNone_m281035543(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0086;
		}
	}
	{
		FsmGameObject_t1697147867 * L_12 = __this->get_transformTarget_19();
		NullCheck(L_12);
		GameObject_t3674682005 * L_13 = FsmGameObject_get_Value_m673294275(L_12, /*hidden argument*/NULL);
		bool L_14 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0086;
		}
	}
	{
		FsmGameObject_t1697147867 * L_15 = __this->get_transformTarget_19();
		NullCheck(L_15);
		GameObject_t3674682005 * L_16 = FsmGameObject_get_Value_m673294275(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_t1659122786 * L_17 = GameObject_get_transform_m1278640159(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t4282066566  L_18 = Transform_get_position_m2211398607(L_17, /*hidden argument*/NULL);
		Vector3_t4282066566  L_19 = V_1;
		Vector3_t4282066566  L_20 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		V_1 = L_20;
	}

IL_0086:
	{
		((iTweenFsmAction_t410382178 *)__this)->set_itweenType_15(_stringLiteral3369786715);
		GameObject_t3674682005 * L_21 = V_0;
		ObjectU5BU5D_t1108656482* L_22 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)24)));
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 0);
		ArrayElementTypeCheck (L_22, _stringLiteral1258521840);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1258521840);
		ObjectU5BU5D_t1108656482* L_23 = L_22;
		Vector3_t4282066566  L_24 = V_1;
		Vector3_t4282066566  L_25 = L_24;
		Il2CppObject * L_26 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_25);
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 1);
		ArrayElementTypeCheck (L_23, L_26);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_26);
		ObjectU5BU5D_t1108656482* L_27 = L_23;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, 2);
		ArrayElementTypeCheck (L_27, _stringLiteral3373707);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3373707);
		ObjectU5BU5D_t1108656482* L_28 = L_27;
		FsmString_t952858651 * L_29 = __this->get_id_18();
		NullCheck(L_29);
		bool L_30 = NamedVariable_get_IsNone_m281035543(L_29, /*hidden argument*/NULL);
		G_B9_0 = 3;
		G_B9_1 = L_28;
		G_B9_2 = L_28;
		G_B9_3 = L_21;
		if (!L_30)
		{
			G_B10_0 = 3;
			G_B10_1 = L_28;
			G_B10_2 = L_28;
			G_B10_3 = L_21;
			goto IL_00ce;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_31 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B11_0 = L_31;
		G_B11_1 = G_B9_0;
		G_B11_2 = G_B9_1;
		G_B11_3 = G_B9_2;
		G_B11_4 = G_B9_3;
		goto IL_00d9;
	}

IL_00ce:
	{
		FsmString_t952858651 * L_32 = __this->get_id_18();
		NullCheck(L_32);
		String_t* L_33 = FsmString_get_Value_m872383149(L_32, /*hidden argument*/NULL);
		G_B11_0 = L_33;
		G_B11_1 = G_B10_0;
		G_B11_2 = G_B10_1;
		G_B11_3 = G_B10_2;
		G_B11_4 = G_B10_3;
	}

IL_00d9:
	{
		NullCheck(G_B11_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B11_2, G_B11_1);
		ArrayElementTypeCheck (G_B11_2, G_B11_0);
		(G_B11_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B11_1), (Il2CppObject *)G_B11_0);
		ObjectU5BU5D_t1108656482* L_34 = G_B11_3;
		FsmFloat_t2134102846 * L_35 = __this->get_speed_23();
		NullCheck(L_35);
		bool L_36 = NamedVariable_get_IsNone_m281035543(L_35, /*hidden argument*/NULL);
		G_B12_0 = 4;
		G_B12_1 = L_34;
		G_B12_2 = L_34;
		G_B12_3 = G_B11_4;
		if (!L_36)
		{
			G_B13_0 = 4;
			G_B13_1 = L_34;
			G_B13_2 = L_34;
			G_B13_3 = G_B11_4;
			goto IL_00f6;
		}
	}
	{
		G_B14_0 = _stringLiteral3560141;
		G_B14_1 = G_B12_0;
		G_B14_2 = G_B12_1;
		G_B14_3 = G_B12_2;
		G_B14_4 = G_B12_3;
		goto IL_00fb;
	}

IL_00f6:
	{
		G_B14_0 = _stringLiteral109641799;
		G_B14_1 = G_B13_0;
		G_B14_2 = G_B13_1;
		G_B14_3 = G_B13_2;
		G_B14_4 = G_B13_3;
	}

IL_00fb:
	{
		NullCheck(G_B14_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B14_2, G_B14_1);
		ArrayElementTypeCheck (G_B14_2, G_B14_0);
		(G_B14_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B14_1), (Il2CppObject *)G_B14_0);
		ObjectU5BU5D_t1108656482* L_37 = G_B14_3;
		FsmFloat_t2134102846 * L_38 = __this->get_speed_23();
		NullCheck(L_38);
		bool L_39 = NamedVariable_get_IsNone_m281035543(L_38, /*hidden argument*/NULL);
		G_B15_0 = 5;
		G_B15_1 = L_37;
		G_B15_2 = L_37;
		G_B15_3 = G_B14_4;
		if (!L_39)
		{
			G_B19_0 = 5;
			G_B19_1 = L_37;
			G_B19_2 = L_37;
			G_B19_3 = G_B14_4;
			goto IL_0138;
		}
	}
	{
		FsmFloat_t2134102846 * L_40 = __this->get_time_21();
		NullCheck(L_40);
		bool L_41 = NamedVariable_get_IsNone_m281035543(L_40, /*hidden argument*/NULL);
		G_B16_0 = G_B15_0;
		G_B16_1 = G_B15_1;
		G_B16_2 = G_B15_2;
		G_B16_3 = G_B15_3;
		if (!L_41)
		{
			G_B17_0 = G_B15_0;
			G_B17_1 = G_B15_1;
			G_B17_2 = G_B15_2;
			G_B17_3 = G_B15_3;
			goto IL_0128;
		}
	}
	{
		G_B18_0 = (1.0f);
		G_B18_1 = G_B16_0;
		G_B18_2 = G_B16_1;
		G_B18_3 = G_B16_2;
		G_B18_4 = G_B16_3;
		goto IL_0133;
	}

IL_0128:
	{
		FsmFloat_t2134102846 * L_42 = __this->get_time_21();
		NullCheck(L_42);
		float L_43 = FsmFloat_get_Value_m4137923823(L_42, /*hidden argument*/NULL);
		G_B18_0 = L_43;
		G_B18_1 = G_B17_0;
		G_B18_2 = G_B17_1;
		G_B18_3 = G_B17_2;
		G_B18_4 = G_B17_3;
	}

IL_0133:
	{
		G_B20_0 = G_B18_0;
		G_B20_1 = G_B18_1;
		G_B20_2 = G_B18_2;
		G_B20_3 = G_B18_3;
		G_B20_4 = G_B18_4;
		goto IL_0143;
	}

IL_0138:
	{
		FsmFloat_t2134102846 * L_44 = __this->get_speed_23();
		NullCheck(L_44);
		float L_45 = FsmFloat_get_Value_m4137923823(L_44, /*hidden argument*/NULL);
		G_B20_0 = L_45;
		G_B20_1 = G_B19_0;
		G_B20_2 = G_B19_1;
		G_B20_3 = G_B19_2;
		G_B20_4 = G_B19_3;
	}

IL_0143:
	{
		float L_46 = G_B20_0;
		Il2CppObject * L_47 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_46);
		NullCheck(G_B20_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B20_2, G_B20_1);
		ArrayElementTypeCheck (G_B20_2, L_47);
		(G_B20_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B20_1), (Il2CppObject *)L_47);
		ObjectU5BU5D_t1108656482* L_48 = G_B20_3;
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, 6);
		ArrayElementTypeCheck (L_48, _stringLiteral95467907);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral95467907);
		ObjectU5BU5D_t1108656482* L_49 = L_48;
		FsmFloat_t2134102846 * L_50 = __this->get_delay_22();
		NullCheck(L_50);
		bool L_51 = NamedVariable_get_IsNone_m281035543(L_50, /*hidden argument*/NULL);
		G_B21_0 = 7;
		G_B21_1 = L_49;
		G_B21_2 = L_49;
		G_B21_3 = G_B20_4;
		if (!L_51)
		{
			G_B22_0 = 7;
			G_B22_1 = L_49;
			G_B22_2 = L_49;
			G_B22_3 = G_B20_4;
			goto IL_016d;
		}
	}
	{
		G_B23_0 = (0.0f);
		G_B23_1 = G_B21_0;
		G_B23_2 = G_B21_1;
		G_B23_3 = G_B21_2;
		G_B23_4 = G_B21_3;
		goto IL_0178;
	}

IL_016d:
	{
		FsmFloat_t2134102846 * L_52 = __this->get_delay_22();
		NullCheck(L_52);
		float L_53 = FsmFloat_get_Value_m4137923823(L_52, /*hidden argument*/NULL);
		G_B23_0 = L_53;
		G_B23_1 = G_B22_0;
		G_B23_2 = G_B22_1;
		G_B23_3 = G_B22_2;
		G_B23_4 = G_B22_3;
	}

IL_0178:
	{
		float L_54 = G_B23_0;
		Il2CppObject * L_55 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_54);
		NullCheck(G_B23_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B23_2, G_B23_1);
		ArrayElementTypeCheck (G_B23_2, L_55);
		(G_B23_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B23_1), (Il2CppObject *)L_55);
		ObjectU5BU5D_t1108656482* L_56 = G_B23_3;
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, 8);
		ArrayElementTypeCheck (L_56, _stringLiteral3507899432);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral3507899432);
		ObjectU5BU5D_t1108656482* L_57 = L_56;
		int32_t L_58 = __this->get_easeType_24();
		int32_t L_59 = L_58;
		Il2CppObject * L_60 = Box(EaseType_t2734598229_il2cpp_TypeInfo_var, &L_59);
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, ((int32_t)9));
		ArrayElementTypeCheck (L_57, L_60);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)L_60);
		ObjectU5BU5D_t1108656482* L_61 = L_57;
		NullCheck(L_61);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_61, ((int32_t)10));
		ArrayElementTypeCheck (L_61, _stringLiteral2258461662);
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Il2CppObject *)_stringLiteral2258461662);
		ObjectU5BU5D_t1108656482* L_62 = L_61;
		int32_t L_63 = __this->get_loopType_25();
		int32_t L_64 = L_63;
		Il2CppObject * L_65 = Box(LoopType_t1485160459_il2cpp_TypeInfo_var, &L_64);
		NullCheck(L_62);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_62, ((int32_t)11));
		ArrayElementTypeCheck (L_62, L_65);
		(L_62)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (Il2CppObject *)L_65);
		ObjectU5BU5D_t1108656482* L_66 = L_62;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, ((int32_t)12));
		ArrayElementTypeCheck (L_66, _stringLiteral2105864216);
		(L_66)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (Il2CppObject *)_stringLiteral2105864216);
		ObjectU5BU5D_t1108656482* L_67 = L_66;
		NullCheck(L_67);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_67, ((int32_t)13));
		ArrayElementTypeCheck (L_67, _stringLiteral338257242);
		(L_67)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (Il2CppObject *)_stringLiteral338257242);
		ObjectU5BU5D_t1108656482* L_68 = L_67;
		NullCheck(L_68);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_68, ((int32_t)14));
		ArrayElementTypeCheck (L_68, _stringLiteral3696326558);
		(L_68)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (Il2CppObject *)_stringLiteral3696326558);
		ObjectU5BU5D_t1108656482* L_69 = L_68;
		int32_t L_70 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_71 = L_70;
		Il2CppObject * L_72 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_71);
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, ((int32_t)15));
		ArrayElementTypeCheck (L_69, L_72);
		(L_69)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (Il2CppObject *)L_72);
		ObjectU5BU5D_t1108656482* L_73 = L_69;
		NullCheck(L_73);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_73, ((int32_t)16));
		ArrayElementTypeCheck (L_73, _stringLiteral2987624931);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (Il2CppObject *)_stringLiteral2987624931);
		ObjectU5BU5D_t1108656482* L_74 = L_73;
		NullCheck(L_74);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_74, ((int32_t)17));
		ArrayElementTypeCheck (L_74, _stringLiteral647687137);
		(L_74)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (Il2CppObject *)_stringLiteral647687137);
		ObjectU5BU5D_t1108656482* L_75 = L_74;
		NullCheck(L_75);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_75, ((int32_t)18));
		ArrayElementTypeCheck (L_75, _stringLiteral2052738345);
		(L_75)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (Il2CppObject *)_stringLiteral2052738345);
		ObjectU5BU5D_t1108656482* L_76 = L_75;
		int32_t L_77 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_78 = L_77;
		Il2CppObject * L_79 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_78);
		NullCheck(L_76);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_76, ((int32_t)19));
		ArrayElementTypeCheck (L_76, L_79);
		(L_76)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)19)), (Il2CppObject *)L_79);
		ObjectU5BU5D_t1108656482* L_80 = L_76;
		NullCheck(L_80);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_80, ((int32_t)20));
		ArrayElementTypeCheck (L_80, _stringLiteral1067506955);
		(L_80)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)20)), (Il2CppObject *)_stringLiteral1067506955);
		ObjectU5BU5D_t1108656482* L_81 = L_80;
		FsmBool_t1075959796 * L_82 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_82);
		bool L_83 = NamedVariable_get_IsNone_m281035543(L_82, /*hidden argument*/NULL);
		G_B24_0 = ((int32_t)21);
		G_B24_1 = L_81;
		G_B24_2 = L_81;
		G_B24_3 = G_B23_4;
		if (!L_83)
		{
			G_B25_0 = ((int32_t)21);
			G_B25_1 = L_81;
			G_B25_2 = L_81;
			G_B25_3 = G_B23_4;
			goto IL_0223;
		}
	}
	{
		G_B26_0 = 0;
		G_B26_1 = G_B24_0;
		G_B26_2 = G_B24_1;
		G_B26_3 = G_B24_2;
		G_B26_4 = G_B24_3;
		goto IL_022e;
	}

IL_0223:
	{
		FsmBool_t1075959796 * L_84 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_84);
		bool L_85 = FsmBool_get_Value_m3101329097(L_84, /*hidden argument*/NULL);
		G_B26_0 = ((int32_t)(L_85));
		G_B26_1 = G_B25_0;
		G_B26_2 = G_B25_1;
		G_B26_3 = G_B25_2;
		G_B26_4 = G_B25_3;
	}

IL_022e:
	{
		bool L_86 = ((bool)G_B26_0);
		Il2CppObject * L_87 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_86);
		NullCheck(G_B26_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B26_2, G_B26_1);
		ArrayElementTypeCheck (G_B26_2, L_87);
		(G_B26_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B26_1), (Il2CppObject *)L_87);
		ObjectU5BU5D_t1108656482* L_88 = G_B26_3;
		NullCheck(L_88);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_88, ((int32_t)22));
		ArrayElementTypeCheck (L_88, _stringLiteral3008417);
		(L_88)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)22)), (Il2CppObject *)_stringLiteral3008417);
		ObjectU5BU5D_t1108656482* L_89 = L_88;
		int32_t L_90 = __this->get_axis_26();
		G_B27_0 = ((int32_t)23);
		G_B27_1 = L_89;
		G_B27_2 = L_89;
		G_B27_3 = G_B26_4;
		if (L_90)
		{
			G_B28_0 = ((int32_t)23);
			G_B28_1 = L_89;
			G_B28_2 = L_89;
			G_B28_3 = G_B26_4;
			goto IL_0255;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_91 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B29_0 = L_91;
		G_B29_1 = G_B27_0;
		G_B29_2 = G_B27_1;
		G_B29_3 = G_B27_2;
		G_B29_4 = G_B27_3;
		goto IL_026f;
	}

IL_0255:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_92 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(AxisRestriction_t3260241011_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_93 = __this->get_axis_26();
		int32_t L_94 = L_93;
		Il2CppObject * L_95 = Box(AxisRestriction_t3260241011_il2cpp_TypeInfo_var, &L_94);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		String_t* L_96 = Enum_GetName_m3333121265(NULL /*static, unused*/, L_92, L_95, /*hidden argument*/NULL);
		G_B29_0 = L_96;
		G_B29_1 = G_B28_0;
		G_B29_2 = G_B28_1;
		G_B29_3 = G_B28_2;
		G_B29_4 = G_B28_3;
	}

IL_026f:
	{
		NullCheck(G_B29_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B29_2, G_B29_1);
		ArrayElementTypeCheck (G_B29_2, G_B29_0);
		(G_B29_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B29_1), (Il2CppObject *)G_B29_0);
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		Hashtable_t1407064410 * L_97 = iTween_Hash_m2099495140(NULL /*static, unused*/, G_B29_3, /*hidden argument*/NULL);
		iTween_LookTo_m292849761(NULL /*static, unused*/, G_B29_4, L_97, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenLookUpdate::.ctor()
extern "C"  void iTweenLookUpdate__ctor_m2414469556 (iTweenLookUpdate_t2645160178 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenLookUpdate::Reset()
extern Il2CppClass* FsmGameObject_t1697147867_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern const uint32_t iTweenLookUpdate_Reset_m60902497_MetadataUsageId;
extern "C"  void iTweenLookUpdate_Reset_m60902497 (iTweenLookUpdate_t2645160178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenLookUpdate_Reset_m60902497_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmGameObject_t1697147867 * V_0 = NULL;
	FsmVector3_t533912882 * V_1 = NULL;
	{
		FsmGameObject_t1697147867 * L_0 = (FsmGameObject_t1697147867 *)il2cpp_codegen_object_new(FsmGameObject_t1697147867_il2cpp_TypeInfo_var);
		FsmGameObject__ctor_m1048269796(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmGameObject_t1697147867 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_2 = V_0;
		__this->set_transformTarget_10(L_2);
		FsmVector3_t533912882 * L_3 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_3, /*hidden argument*/NULL);
		V_1 = L_3;
		FsmVector3_t533912882 * L_4 = V_1;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_5 = V_1;
		__this->set_vectorTarget_11(L_5);
		FsmFloat_t2134102846 * L_6 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_time_12(L_6);
		__this->set_axis_13(0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenLookUpdate::OnEnter()
extern const Il2CppType* AxisRestriction_t3260241011_0_0_0_var;
extern Il2CppClass* Hashtable_t1407064410_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* AxisRestriction_t3260241011_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2862688501_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258521840;
extern Il2CppCodeGenString* _stringLiteral3560141;
extern Il2CppCodeGenString* _stringLiteral3008417;
extern const uint32_t iTweenLookUpdate_OnEnter_m414632075_MetadataUsageId;
extern "C"  void iTweenLookUpdate_OnEnter_m414632075 (iTweenLookUpdate_t2645160178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenLookUpdate_OnEnter_m414632075_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B5_0 = NULL;
	Hashtable_t1407064410 * G_B5_1 = NULL;
	String_t* G_B4_0 = NULL;
	Hashtable_t1407064410 * G_B4_1 = NULL;
	Vector3_t4282066566  G_B6_0;
	memset(&G_B6_0, 0, sizeof(G_B6_0));
	String_t* G_B6_1 = NULL;
	Hashtable_t1407064410 * G_B6_2 = NULL;
	String_t* G_B12_0 = NULL;
	Hashtable_t1407064410 * G_B12_1 = NULL;
	String_t* G_B11_0 = NULL;
	Hashtable_t1407064410 * G_B11_1 = NULL;
	float G_B13_0 = 0.0f;
	String_t* G_B13_1 = NULL;
	Hashtable_t1407064410 * G_B13_2 = NULL;
	String_t* G_B15_0 = NULL;
	Hashtable_t1407064410 * G_B15_1 = NULL;
	String_t* G_B14_0 = NULL;
	Hashtable_t1407064410 * G_B14_1 = NULL;
	String_t* G_B16_0 = NULL;
	String_t* G_B16_1 = NULL;
	Hashtable_t1407064410 * G_B16_2 = NULL;
	{
		Hashtable_t1407064410 * L_0 = (Hashtable_t1407064410 *)il2cpp_codegen_object_new(Hashtable_t1407064410_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1514037738(L_0, /*hidden argument*/NULL);
		__this->set_hash_14(L_0);
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_2 = __this->get_gameObject_9();
		NullCheck(L_1);
		GameObject_t3674682005 * L_3 = Fsm_GetOwnerDefaultTarget_m846013999(L_1, L_2, /*hidden argument*/NULL);
		__this->set_go_15(L_3);
		GameObject_t3674682005 * L_4 = __this->get_go_15();
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003a;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_003a:
	{
		FsmGameObject_t1697147867 * L_6 = __this->get_transformTarget_10();
		NullCheck(L_6);
		bool L_7 = NamedVariable_get_IsNone_m281035543(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0089;
		}
	}
	{
		Hashtable_t1407064410 * L_8 = __this->get_hash_14();
		FsmVector3_t533912882 * L_9 = __this->get_vectorTarget_11();
		NullCheck(L_9);
		bool L_10 = NamedVariable_get_IsNone_m281035543(L_9, /*hidden argument*/NULL);
		G_B4_0 = _stringLiteral1258521840;
		G_B4_1 = L_8;
		if (!L_10)
		{
			G_B5_0 = _stringLiteral1258521840;
			G_B5_1 = L_8;
			goto IL_006f;
		}
	}
	{
		Vector3_t4282066566  L_11 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		goto IL_007a;
	}

IL_006f:
	{
		FsmVector3_t533912882 * L_12 = __this->get_vectorTarget_11();
		NullCheck(L_12);
		Vector3_t4282066566  L_13 = FsmVector3_get_Value_m2779135117(L_12, /*hidden argument*/NULL);
		G_B6_0 = L_13;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
	}

IL_007a:
	{
		Vector3_t4282066566  L_14 = G_B6_0;
		Il2CppObject * L_15 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_14);
		NullCheck(G_B6_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B6_2, G_B6_1, L_15);
		goto IL_00f8;
	}

IL_0089:
	{
		FsmVector3_t533912882 * L_16 = __this->get_vectorTarget_11();
		NullCheck(L_16);
		bool L_17 = NamedVariable_get_IsNone_m281035543(L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00be;
		}
	}
	{
		Hashtable_t1407064410 * L_18 = __this->get_hash_14();
		FsmGameObject_t1697147867 * L_19 = __this->get_transformTarget_10();
		NullCheck(L_19);
		GameObject_t3674682005 * L_20 = FsmGameObject_get_Value_m673294275(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_t1659122786 * L_21 = GameObject_get_transform_m1278640159(L_20, /*hidden argument*/NULL);
		NullCheck(L_18);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_18, _stringLiteral1258521840, L_21);
		goto IL_00f8;
	}

IL_00be:
	{
		Hashtable_t1407064410 * L_22 = __this->get_hash_14();
		FsmGameObject_t1697147867 * L_23 = __this->get_transformTarget_10();
		NullCheck(L_23);
		GameObject_t3674682005 * L_24 = FsmGameObject_get_Value_m673294275(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_t1659122786 * L_25 = GameObject_get_transform_m1278640159(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		Vector3_t4282066566  L_26 = Transform_get_position_m2211398607(L_25, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_27 = __this->get_vectorTarget_11();
		NullCheck(L_27);
		Vector3_t4282066566  L_28 = FsmVector3_get_Value_m2779135117(L_27, /*hidden argument*/NULL);
		Vector3_t4282066566  L_29 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_26, L_28, /*hidden argument*/NULL);
		Vector3_t4282066566  L_30 = L_29;
		Il2CppObject * L_31 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_30);
		NullCheck(L_22);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_22, _stringLiteral1258521840, L_31);
	}

IL_00f8:
	{
		Hashtable_t1407064410 * L_32 = __this->get_hash_14();
		FsmFloat_t2134102846 * L_33 = __this->get_time_12();
		NullCheck(L_33);
		bool L_34 = NamedVariable_get_IsNone_m281035543(L_33, /*hidden argument*/NULL);
		G_B11_0 = _stringLiteral3560141;
		G_B11_1 = L_32;
		if (!L_34)
		{
			G_B12_0 = _stringLiteral3560141;
			G_B12_1 = L_32;
			goto IL_011d;
		}
	}
	{
		G_B13_0 = (1.0f);
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		goto IL_0128;
	}

IL_011d:
	{
		FsmFloat_t2134102846 * L_35 = __this->get_time_12();
		NullCheck(L_35);
		float L_36 = FsmFloat_get_Value_m4137923823(L_35, /*hidden argument*/NULL);
		G_B13_0 = L_36;
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
	}

IL_0128:
	{
		float L_37 = G_B13_0;
		Il2CppObject * L_38 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_37);
		NullCheck(G_B13_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B13_2, G_B13_1, L_38);
		Hashtable_t1407064410 * L_39 = __this->get_hash_14();
		int32_t L_40 = __this->get_axis_13();
		G_B14_0 = _stringLiteral3008417;
		G_B14_1 = L_39;
		if (L_40)
		{
			G_B15_0 = _stringLiteral3008417;
			G_B15_1 = L_39;
			goto IL_0152;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_41 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B16_0 = L_41;
		G_B16_1 = G_B14_0;
		G_B16_2 = G_B14_1;
		goto IL_016c;
	}

IL_0152:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_42 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(AxisRestriction_t3260241011_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_43 = __this->get_axis_13();
		int32_t L_44 = L_43;
		Il2CppObject * L_45 = Box(AxisRestriction_t3260241011_il2cpp_TypeInfo_var, &L_44);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		String_t* L_46 = Enum_GetName_m3333121265(NULL /*static, unused*/, L_42, L_45, /*hidden argument*/NULL);
		G_B16_0 = L_46;
		G_B16_1 = G_B15_0;
		G_B16_2 = G_B15_1;
	}

IL_016c:
	{
		NullCheck(G_B16_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B16_2, G_B16_1, G_B16_0);
		iTweenLookUpdate_DoiTween_m1419824669(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenLookUpdate::OnExit()
extern "C"  void iTweenLookUpdate_OnExit_m3347430829 (iTweenLookUpdate_t2645160178 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenLookUpdate::OnUpdate()
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258521840;
extern const uint32_t iTweenLookUpdate_OnUpdate_m3397219096_MetadataUsageId;
extern "C"  void iTweenLookUpdate_OnUpdate_m3397219096 (iTweenLookUpdate_t2645160178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenLookUpdate_OnUpdate_m3397219096_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	Hashtable_t1407064410 * G_B3_1 = NULL;
	String_t* G_B2_0 = NULL;
	Hashtable_t1407064410 * G_B2_1 = NULL;
	Vector3_t4282066566  G_B4_0;
	memset(&G_B4_0, 0, sizeof(G_B4_0));
	String_t* G_B4_1 = NULL;
	Hashtable_t1407064410 * G_B4_2 = NULL;
	{
		Hashtable_t1407064410 * L_0 = __this->get_hash_14();
		NullCheck(L_0);
		VirtActionInvoker1< Il2CppObject * >::Invoke(37 /* System.Void System.Collections.Hashtable::Remove(System.Object) */, L_0, _stringLiteral1258521840);
		FsmGameObject_t1697147867 * L_1 = __this->get_transformTarget_10();
		NullCheck(L_1);
		bool L_2 = NamedVariable_get_IsNone_m281035543(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_005f;
		}
	}
	{
		Hashtable_t1407064410 * L_3 = __this->get_hash_14();
		FsmVector3_t533912882 * L_4 = __this->get_vectorTarget_11();
		NullCheck(L_4);
		bool L_5 = NamedVariable_get_IsNone_m281035543(L_4, /*hidden argument*/NULL);
		G_B2_0 = _stringLiteral1258521840;
		G_B2_1 = L_3;
		if (!L_5)
		{
			G_B3_0 = _stringLiteral1258521840;
			G_B3_1 = L_3;
			goto IL_0045;
		}
	}
	{
		Vector3_t4282066566  L_6 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B4_0 = L_6;
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		goto IL_0050;
	}

IL_0045:
	{
		FsmVector3_t533912882 * L_7 = __this->get_vectorTarget_11();
		NullCheck(L_7);
		Vector3_t4282066566  L_8 = FsmVector3_get_Value_m2779135117(L_7, /*hidden argument*/NULL);
		G_B4_0 = L_8;
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
	}

IL_0050:
	{
		Vector3_t4282066566  L_9 = G_B4_0;
		Il2CppObject * L_10 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_9);
		NullCheck(G_B4_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B4_2, G_B4_1, L_10);
		goto IL_00ce;
	}

IL_005f:
	{
		FsmVector3_t533912882 * L_11 = __this->get_vectorTarget_11();
		NullCheck(L_11);
		bool L_12 = NamedVariable_get_IsNone_m281035543(L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0094;
		}
	}
	{
		Hashtable_t1407064410 * L_13 = __this->get_hash_14();
		FsmGameObject_t1697147867 * L_14 = __this->get_transformTarget_10();
		NullCheck(L_14);
		GameObject_t3674682005 * L_15 = FsmGameObject_get_Value_m673294275(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_t1659122786 * L_16 = GameObject_get_transform_m1278640159(L_15, /*hidden argument*/NULL);
		NullCheck(L_13);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_13, _stringLiteral1258521840, L_16);
		goto IL_00ce;
	}

IL_0094:
	{
		Hashtable_t1407064410 * L_17 = __this->get_hash_14();
		FsmGameObject_t1697147867 * L_18 = __this->get_transformTarget_10();
		NullCheck(L_18);
		GameObject_t3674682005 * L_19 = FsmGameObject_get_Value_m673294275(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Transform_t1659122786 * L_20 = GameObject_get_transform_m1278640159(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		Vector3_t4282066566  L_21 = Transform_get_position_m2211398607(L_20, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_22 = __this->get_vectorTarget_11();
		NullCheck(L_22);
		Vector3_t4282066566  L_23 = FsmVector3_get_Value_m2779135117(L_22, /*hidden argument*/NULL);
		Vector3_t4282066566  L_24 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_21, L_23, /*hidden argument*/NULL);
		Vector3_t4282066566  L_25 = L_24;
		Il2CppObject * L_26 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_25);
		NullCheck(L_17);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_17, _stringLiteral1258521840, L_26);
	}

IL_00ce:
	{
		iTweenLookUpdate_DoiTween_m1419824669(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenLookUpdate::DoiTween()
extern Il2CppClass* iTween_t3087282050_il2cpp_TypeInfo_var;
extern const uint32_t iTweenLookUpdate_DoiTween_m1419824669_MetadataUsageId;
extern "C"  void iTweenLookUpdate_DoiTween_m1419824669 (iTweenLookUpdate_t2645160178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenLookUpdate_DoiTween_m1419824669_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_go_15();
		Hashtable_t1407064410 * L_1 = __this->get_hash_14();
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		iTween_LookUpdate_m3529502547(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveAdd::.ctor()
extern "C"  void iTweenMoveAdd__ctor_m286477722 (iTweenMoveAdd_t1025491740 * __this, const MethodInfo* method)
{
	{
		__this->set_easeType_23(((int32_t)21));
		iTweenFsmAction__ctor_m2960815636(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveAdd::Reset()
extern Il2CppClass* FsmString_t952858651_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmFloat_t2134102846_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmGameObject_t1697147867_il2cpp_TypeInfo_var;
extern const uint32_t iTweenMoveAdd_Reset_m2227877959_MetadataUsageId;
extern "C"  void iTweenMoveAdd_Reset_m2227877959 (iTweenMoveAdd_t1025491740 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenMoveAdd_Reset_m2227877959_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmString_t952858651 * V_0 = NULL;
	FsmVector3_t533912882 * V_1 = NULL;
	FsmFloat_t2134102846 * V_2 = NULL;
	FsmGameObject_t1697147867 * V_3 = NULL;
	{
		iTweenFsmAction_Reset_m607248577(__this, /*hidden argument*/NULL);
		FsmString_t952858651 * L_0 = (FsmString_t952858651 *)il2cpp_codegen_object_new(FsmString_t952858651_il2cpp_TypeInfo_var);
		FsmString__ctor_m2679108260(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmString_t952858651 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmString_t952858651 * L_2 = V_0;
		__this->set_id_18(L_2);
		FsmFloat_t2134102846 * L_3 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_time_20(L_3);
		FsmFloat_t2134102846 * L_4 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_delay_21(L_4);
		__this->set_loopType_24(0);
		FsmVector3_t533912882 * L_5 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_5, /*hidden argument*/NULL);
		V_1 = L_5;
		FsmVector3_t533912882 * L_6 = V_1;
		NullCheck(L_6);
		NamedVariable_set_UseVariable_m4266138971(L_6, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_7 = V_1;
		__this->set_vector_19(L_7);
		FsmFloat_t2134102846 * L_8 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_8, /*hidden argument*/NULL);
		V_2 = L_8;
		FsmFloat_t2134102846 * L_9 = V_2;
		NullCheck(L_9);
		NamedVariable_set_UseVariable_m4266138971(L_9, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_10 = V_2;
		__this->set_speed_22(L_10);
		__this->set_space_25(0);
		FsmBool_t1075959796 * L_11 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_orientToPath_26(L_11);
		FsmGameObject_t1697147867 * L_12 = (FsmGameObject_t1697147867 *)il2cpp_codegen_object_new(FsmGameObject_t1697147867_il2cpp_TypeInfo_var);
		FsmGameObject__ctor_m1048269796(L_12, /*hidden argument*/NULL);
		V_3 = L_12;
		FsmGameObject_t1697147867 * L_13 = V_3;
		NullCheck(L_13);
		NamedVariable_set_UseVariable_m4266138971(L_13, (bool)1, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_14 = V_3;
		__this->set_lookAtObject_27(L_14);
		FsmVector3_t533912882 * L_15 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_15, /*hidden argument*/NULL);
		V_1 = L_15;
		FsmVector3_t533912882 * L_16 = V_1;
		NullCheck(L_16);
		NamedVariable_set_UseVariable_m4266138971(L_16, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_17 = V_1;
		__this->set_lookAtVector_28(L_17);
		FsmFloat_t2134102846 * L_18 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_lookTime_29(L_18);
		__this->set_axis_30(0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveAdd::OnEnter()
extern "C"  void iTweenMoveAdd_OnEnter_m4113879793 (iTweenMoveAdd_t1025491740 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnEnteriTween_m3024539459(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_loopType_24();
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		iTweenFsmAction_IsLoop_m2539277653(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_001e:
	{
		iTweenMoveAdd_DoiTween_m132386935(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveAdd::OnExit()
extern "C"  void iTweenMoveAdd_OnExit_m1804193415 (iTweenMoveAdd_t1025491740 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnExitiTween_m1981625761(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveAdd::DoiTween()
extern const Il2CppType* AxisRestriction_t3260241011_0_0_0_var;
extern Il2CppClass* Hashtable_t1407064410_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* EaseType_t2734598229_il2cpp_TypeInfo_var;
extern Il2CppClass* LoopType_t1485160459_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* Space_t4209342076_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* AxisRestriction_t3260241011_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2862688501_il2cpp_TypeInfo_var;
extern Il2CppClass* iTween_t3087282050_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2881114200;
extern Il2CppCodeGenString* _stringLiteral3560141;
extern Il2CppCodeGenString* _stringLiteral109641799;
extern Il2CppCodeGenString* _stringLiteral95467907;
extern Il2CppCodeGenString* _stringLiteral3507899432;
extern Il2CppCodeGenString* _stringLiteral2258461662;
extern Il2CppCodeGenString* _stringLiteral2105864216;
extern Il2CppCodeGenString* _stringLiteral338257242;
extern Il2CppCodeGenString* _stringLiteral3696326558;
extern Il2CppCodeGenString* _stringLiteral2987624931;
extern Il2CppCodeGenString* _stringLiteral647687137;
extern Il2CppCodeGenString* _stringLiteral2052738345;
extern Il2CppCodeGenString* _stringLiteral1067506955;
extern Il2CppCodeGenString* _stringLiteral109637894;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral3008417;
extern Il2CppCodeGenString* _stringLiteral3159645605;
extern Il2CppCodeGenString* _stringLiteral1258521840;
extern Il2CppCodeGenString* _stringLiteral2253828588;
extern Il2CppCodeGenString* _stringLiteral3357649;
extern const uint32_t iTweenMoveAdd_DoiTween_m132386935_MetadataUsageId;
extern "C"  void iTweenMoveAdd_DoiTween_m132386935 (iTweenMoveAdd_t1025491740 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenMoveAdd_DoiTween_m132386935_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Hashtable_t1407064410 * V_1 = NULL;
	String_t* G_B4_0 = NULL;
	Hashtable_t1407064410 * G_B4_1 = NULL;
	String_t* G_B3_0 = NULL;
	Hashtable_t1407064410 * G_B3_1 = NULL;
	Vector3_t4282066566  G_B5_0;
	memset(&G_B5_0, 0, sizeof(G_B5_0));
	String_t* G_B5_1 = NULL;
	Hashtable_t1407064410 * G_B5_2 = NULL;
	Hashtable_t1407064410 * G_B7_0 = NULL;
	Hashtable_t1407064410 * G_B6_0 = NULL;
	String_t* G_B8_0 = NULL;
	Hashtable_t1407064410 * G_B8_1 = NULL;
	String_t* G_B13_0 = NULL;
	Hashtable_t1407064410 * G_B13_1 = NULL;
	String_t* G_B9_0 = NULL;
	Hashtable_t1407064410 * G_B9_1 = NULL;
	String_t* G_B11_0 = NULL;
	Hashtable_t1407064410 * G_B11_1 = NULL;
	String_t* G_B10_0 = NULL;
	Hashtable_t1407064410 * G_B10_1 = NULL;
	float G_B12_0 = 0.0f;
	String_t* G_B12_1 = NULL;
	Hashtable_t1407064410 * G_B12_2 = NULL;
	float G_B14_0 = 0.0f;
	String_t* G_B14_1 = NULL;
	Hashtable_t1407064410 * G_B14_2 = NULL;
	String_t* G_B16_0 = NULL;
	Hashtable_t1407064410 * G_B16_1 = NULL;
	String_t* G_B15_0 = NULL;
	Hashtable_t1407064410 * G_B15_1 = NULL;
	float G_B17_0 = 0.0f;
	String_t* G_B17_1 = NULL;
	Hashtable_t1407064410 * G_B17_2 = NULL;
	String_t* G_B19_0 = NULL;
	Hashtable_t1407064410 * G_B19_1 = NULL;
	String_t* G_B18_0 = NULL;
	Hashtable_t1407064410 * G_B18_1 = NULL;
	int32_t G_B20_0 = 0;
	String_t* G_B20_1 = NULL;
	Hashtable_t1407064410 * G_B20_2 = NULL;
	String_t* G_B22_0 = NULL;
	Hashtable_t1407064410 * G_B22_1 = NULL;
	String_t* G_B21_0 = NULL;
	Hashtable_t1407064410 * G_B21_1 = NULL;
	String_t* G_B23_0 = NULL;
	String_t* G_B23_1 = NULL;
	Hashtable_t1407064410 * G_B23_2 = NULL;
	String_t* G_B25_0 = NULL;
	Hashtable_t1407064410 * G_B25_1 = NULL;
	String_t* G_B24_0 = NULL;
	Hashtable_t1407064410 * G_B24_1 = NULL;
	String_t* G_B26_0 = NULL;
	String_t* G_B26_1 = NULL;
	Hashtable_t1407064410 * G_B26_2 = NULL;
	String_t* G_B31_0 = NULL;
	Hashtable_t1407064410 * G_B31_1 = NULL;
	String_t* G_B30_0 = NULL;
	Hashtable_t1407064410 * G_B30_1 = NULL;
	Vector3_t4282066566  G_B32_0;
	memset(&G_B32_0, 0, sizeof(G_B32_0));
	String_t* G_B32_1 = NULL;
	Hashtable_t1407064410 * G_B32_2 = NULL;
	String_t* G_B39_0 = NULL;
	Hashtable_t1407064410 * G_B39_1 = NULL;
	String_t* G_B38_0 = NULL;
	Hashtable_t1407064410 * G_B38_1 = NULL;
	float G_B40_0 = 0.0f;
	String_t* G_B40_1 = NULL;
	Hashtable_t1407064410 * G_B40_2 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_17();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		Hashtable_t1407064410 * L_5 = (Hashtable_t1407064410 *)il2cpp_codegen_object_new(Hashtable_t1407064410_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1514037738(L_5, /*hidden argument*/NULL);
		V_1 = L_5;
		Hashtable_t1407064410 * L_6 = V_1;
		FsmVector3_t533912882 * L_7 = __this->get_vector_19();
		NullCheck(L_7);
		bool L_8 = NamedVariable_get_IsNone_m281035543(L_7, /*hidden argument*/NULL);
		G_B3_0 = _stringLiteral2881114200;
		G_B3_1 = L_6;
		if (!L_8)
		{
			G_B4_0 = _stringLiteral2881114200;
			G_B4_1 = L_6;
			goto IL_0045;
		}
	}
	{
		Vector3_t4282066566  L_9 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B5_0 = L_9;
		G_B5_1 = G_B3_0;
		G_B5_2 = G_B3_1;
		goto IL_0050;
	}

IL_0045:
	{
		FsmVector3_t533912882 * L_10 = __this->get_vector_19();
		NullCheck(L_10);
		Vector3_t4282066566  L_11 = FsmVector3_get_Value_m2779135117(L_10, /*hidden argument*/NULL);
		G_B5_0 = L_11;
		G_B5_1 = G_B4_0;
		G_B5_2 = G_B4_1;
	}

IL_0050:
	{
		Vector3_t4282066566  L_12 = G_B5_0;
		Il2CppObject * L_13 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_12);
		NullCheck(G_B5_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B5_2, G_B5_1, L_13);
		Hashtable_t1407064410 * L_14 = V_1;
		FsmFloat_t2134102846 * L_15 = __this->get_speed_22();
		NullCheck(L_15);
		bool L_16 = NamedVariable_get_IsNone_m281035543(L_15, /*hidden argument*/NULL);
		G_B6_0 = L_14;
		if (!L_16)
		{
			G_B7_0 = L_14;
			goto IL_0075;
		}
	}
	{
		G_B8_0 = _stringLiteral3560141;
		G_B8_1 = G_B6_0;
		goto IL_007a;
	}

IL_0075:
	{
		G_B8_0 = _stringLiteral109641799;
		G_B8_1 = G_B7_0;
	}

IL_007a:
	{
		FsmFloat_t2134102846 * L_17 = __this->get_speed_22();
		NullCheck(L_17);
		bool L_18 = NamedVariable_get_IsNone_m281035543(L_17, /*hidden argument*/NULL);
		G_B9_0 = G_B8_0;
		G_B9_1 = G_B8_1;
		if (!L_18)
		{
			G_B13_0 = G_B8_0;
			G_B13_1 = G_B8_1;
			goto IL_00b4;
		}
	}
	{
		FsmFloat_t2134102846 * L_19 = __this->get_time_20();
		NullCheck(L_19);
		bool L_20 = NamedVariable_get_IsNone_m281035543(L_19, /*hidden argument*/NULL);
		G_B10_0 = G_B9_0;
		G_B10_1 = G_B9_1;
		if (!L_20)
		{
			G_B11_0 = G_B9_0;
			G_B11_1 = G_B9_1;
			goto IL_00a4;
		}
	}
	{
		G_B12_0 = (1.0f);
		G_B12_1 = G_B10_0;
		G_B12_2 = G_B10_1;
		goto IL_00af;
	}

IL_00a4:
	{
		FsmFloat_t2134102846 * L_21 = __this->get_time_20();
		NullCheck(L_21);
		float L_22 = FsmFloat_get_Value_m4137923823(L_21, /*hidden argument*/NULL);
		G_B12_0 = L_22;
		G_B12_1 = G_B11_0;
		G_B12_2 = G_B11_1;
	}

IL_00af:
	{
		G_B14_0 = G_B12_0;
		G_B14_1 = G_B12_1;
		G_B14_2 = G_B12_2;
		goto IL_00bf;
	}

IL_00b4:
	{
		FsmFloat_t2134102846 * L_23 = __this->get_speed_22();
		NullCheck(L_23);
		float L_24 = FsmFloat_get_Value_m4137923823(L_23, /*hidden argument*/NULL);
		G_B14_0 = L_24;
		G_B14_1 = G_B13_0;
		G_B14_2 = G_B13_1;
	}

IL_00bf:
	{
		float L_25 = G_B14_0;
		Il2CppObject * L_26 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_25);
		NullCheck(G_B14_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B14_2, G_B14_1, L_26);
		Hashtable_t1407064410 * L_27 = V_1;
		FsmFloat_t2134102846 * L_28 = __this->get_delay_21();
		NullCheck(L_28);
		bool L_29 = NamedVariable_get_IsNone_m281035543(L_28, /*hidden argument*/NULL);
		G_B15_0 = _stringLiteral95467907;
		G_B15_1 = L_27;
		if (!L_29)
		{
			G_B16_0 = _stringLiteral95467907;
			G_B16_1 = L_27;
			goto IL_00e9;
		}
	}
	{
		G_B17_0 = (0.0f);
		G_B17_1 = G_B15_0;
		G_B17_2 = G_B15_1;
		goto IL_00f4;
	}

IL_00e9:
	{
		FsmFloat_t2134102846 * L_30 = __this->get_delay_21();
		NullCheck(L_30);
		float L_31 = FsmFloat_get_Value_m4137923823(L_30, /*hidden argument*/NULL);
		G_B17_0 = L_31;
		G_B17_1 = G_B16_0;
		G_B17_2 = G_B16_1;
	}

IL_00f4:
	{
		float L_32 = G_B17_0;
		Il2CppObject * L_33 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_32);
		NullCheck(G_B17_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B17_2, G_B17_1, L_33);
		Hashtable_t1407064410 * L_34 = V_1;
		int32_t L_35 = __this->get_easeType_23();
		int32_t L_36 = L_35;
		Il2CppObject * L_37 = Box(EaseType_t2734598229_il2cpp_TypeInfo_var, &L_36);
		NullCheck(L_34);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_34, _stringLiteral3507899432, L_37);
		Hashtable_t1407064410 * L_38 = V_1;
		int32_t L_39 = __this->get_loopType_24();
		int32_t L_40 = L_39;
		Il2CppObject * L_41 = Box(LoopType_t1485160459_il2cpp_TypeInfo_var, &L_40);
		NullCheck(L_38);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_38, _stringLiteral2258461662, L_41);
		Hashtable_t1407064410 * L_42 = V_1;
		NullCheck(L_42);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_42, _stringLiteral2105864216, _stringLiteral338257242);
		Hashtable_t1407064410 * L_43 = V_1;
		int32_t L_44 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_45 = L_44;
		Il2CppObject * L_46 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_45);
		NullCheck(L_43);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_43, _stringLiteral3696326558, L_46);
		Hashtable_t1407064410 * L_47 = V_1;
		NullCheck(L_47);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_47, _stringLiteral2987624931, _stringLiteral647687137);
		Hashtable_t1407064410 * L_48 = V_1;
		int32_t L_49 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_50 = L_49;
		Il2CppObject * L_51 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_50);
		NullCheck(L_48);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_48, _stringLiteral2052738345, L_51);
		Hashtable_t1407064410 * L_52 = V_1;
		FsmBool_t1075959796 * L_53 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_53);
		bool L_54 = NamedVariable_get_IsNone_m281035543(L_53, /*hidden argument*/NULL);
		G_B18_0 = _stringLiteral1067506955;
		G_B18_1 = L_52;
		if (!L_54)
		{
			G_B19_0 = _stringLiteral1067506955;
			G_B19_1 = L_52;
			goto IL_0192;
		}
	}
	{
		G_B20_0 = 0;
		G_B20_1 = G_B18_0;
		G_B20_2 = G_B18_1;
		goto IL_019d;
	}

IL_0192:
	{
		FsmBool_t1075959796 * L_55 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_55);
		bool L_56 = FsmBool_get_Value_m3101329097(L_55, /*hidden argument*/NULL);
		G_B20_0 = ((int32_t)(L_56));
		G_B20_1 = G_B19_0;
		G_B20_2 = G_B19_1;
	}

IL_019d:
	{
		bool L_57 = ((bool)G_B20_0);
		Il2CppObject * L_58 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_57);
		NullCheck(G_B20_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B20_2, G_B20_1, L_58);
		Hashtable_t1407064410 * L_59 = V_1;
		int32_t L_60 = __this->get_space_25();
		int32_t L_61 = L_60;
		Il2CppObject * L_62 = Box(Space_t4209342076_il2cpp_TypeInfo_var, &L_61);
		NullCheck(L_59);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_59, _stringLiteral109637894, L_62);
		Hashtable_t1407064410 * L_63 = V_1;
		FsmString_t952858651 * L_64 = __this->get_id_18();
		NullCheck(L_64);
		bool L_65 = NamedVariable_get_IsNone_m281035543(L_64, /*hidden argument*/NULL);
		G_B21_0 = _stringLiteral3373707;
		G_B21_1 = L_63;
		if (!L_65)
		{
			G_B22_0 = _stringLiteral3373707;
			G_B22_1 = L_63;
			goto IL_01dd;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_66 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B23_0 = L_66;
		G_B23_1 = G_B21_0;
		G_B23_2 = G_B21_1;
		goto IL_01e8;
	}

IL_01dd:
	{
		FsmString_t952858651 * L_67 = __this->get_id_18();
		NullCheck(L_67);
		String_t* L_68 = FsmString_get_Value_m872383149(L_67, /*hidden argument*/NULL);
		G_B23_0 = L_68;
		G_B23_1 = G_B22_0;
		G_B23_2 = G_B22_1;
	}

IL_01e8:
	{
		NullCheck(G_B23_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B23_2, G_B23_1, G_B23_0);
		Hashtable_t1407064410 * L_69 = V_1;
		int32_t L_70 = __this->get_axis_30();
		G_B24_0 = _stringLiteral3008417;
		G_B24_1 = L_69;
		if (L_70)
		{
			G_B25_0 = _stringLiteral3008417;
			G_B25_1 = L_69;
			goto IL_0208;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_71 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B26_0 = L_71;
		G_B26_1 = G_B24_0;
		G_B26_2 = G_B24_1;
		goto IL_0222;
	}

IL_0208:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_72 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(AxisRestriction_t3260241011_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_73 = __this->get_axis_30();
		int32_t L_74 = L_73;
		Il2CppObject * L_75 = Box(AxisRestriction_t3260241011_il2cpp_TypeInfo_var, &L_74);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		String_t* L_76 = Enum_GetName_m3333121265(NULL /*static, unused*/, L_72, L_75, /*hidden argument*/NULL);
		G_B26_0 = L_76;
		G_B26_1 = G_B25_0;
		G_B26_2 = G_B25_1;
	}

IL_0222:
	{
		NullCheck(G_B26_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B26_2, G_B26_1, G_B26_0);
		FsmBool_t1075959796 * L_77 = __this->get_orientToPath_26();
		NullCheck(L_77);
		bool L_78 = NamedVariable_get_IsNone_m281035543(L_77, /*hidden argument*/NULL);
		if (L_78)
		{
			goto IL_0252;
		}
	}
	{
		Hashtable_t1407064410 * L_79 = V_1;
		FsmBool_t1075959796 * L_80 = __this->get_orientToPath_26();
		NullCheck(L_80);
		bool L_81 = FsmBool_get_Value_m3101329097(L_80, /*hidden argument*/NULL);
		bool L_82 = L_81;
		Il2CppObject * L_83 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_82);
		NullCheck(L_79);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_79, _stringLiteral3159645605, L_83);
	}

IL_0252:
	{
		FsmGameObject_t1697147867 * L_84 = __this->get_lookAtObject_27();
		NullCheck(L_84);
		bool L_85 = NamedVariable_get_IsNone_m281035543(L_84, /*hidden argument*/NULL);
		if (L_85)
		{
			goto IL_02c6;
		}
	}
	{
		Hashtable_t1407064410 * L_86 = V_1;
		FsmVector3_t533912882 * L_87 = __this->get_lookAtVector_28();
		NullCheck(L_87);
		bool L_88 = NamedVariable_get_IsNone_m281035543(L_87, /*hidden argument*/NULL);
		G_B30_0 = _stringLiteral1258521840;
		G_B30_1 = L_86;
		if (!L_88)
		{
			G_B31_0 = _stringLiteral1258521840;
			G_B31_1 = L_86;
			goto IL_0292;
		}
	}
	{
		FsmGameObject_t1697147867 * L_89 = __this->get_lookAtObject_27();
		NullCheck(L_89);
		GameObject_t3674682005 * L_90 = FsmGameObject_get_Value_m673294275(L_89, /*hidden argument*/NULL);
		NullCheck(L_90);
		Transform_t1659122786 * L_91 = GameObject_get_transform_m1278640159(L_90, /*hidden argument*/NULL);
		NullCheck(L_91);
		Vector3_t4282066566  L_92 = Transform_get_position_m2211398607(L_91, /*hidden argument*/NULL);
		G_B32_0 = L_92;
		G_B32_1 = G_B30_0;
		G_B32_2 = G_B30_1;
		goto IL_02b7;
	}

IL_0292:
	{
		FsmGameObject_t1697147867 * L_93 = __this->get_lookAtObject_27();
		NullCheck(L_93);
		GameObject_t3674682005 * L_94 = FsmGameObject_get_Value_m673294275(L_93, /*hidden argument*/NULL);
		NullCheck(L_94);
		Transform_t1659122786 * L_95 = GameObject_get_transform_m1278640159(L_94, /*hidden argument*/NULL);
		NullCheck(L_95);
		Vector3_t4282066566  L_96 = Transform_get_position_m2211398607(L_95, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_97 = __this->get_lookAtVector_28();
		NullCheck(L_97);
		Vector3_t4282066566  L_98 = FsmVector3_get_Value_m2779135117(L_97, /*hidden argument*/NULL);
		Vector3_t4282066566  L_99 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_96, L_98, /*hidden argument*/NULL);
		G_B32_0 = L_99;
		G_B32_1 = G_B31_0;
		G_B32_2 = G_B31_1;
	}

IL_02b7:
	{
		Vector3_t4282066566  L_100 = G_B32_0;
		Il2CppObject * L_101 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_100);
		NullCheck(G_B32_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B32_2, G_B32_1, L_101);
		goto IL_02f1;
	}

IL_02c6:
	{
		FsmVector3_t533912882 * L_102 = __this->get_lookAtVector_28();
		NullCheck(L_102);
		bool L_103 = NamedVariable_get_IsNone_m281035543(L_102, /*hidden argument*/NULL);
		if (L_103)
		{
			goto IL_02f1;
		}
	}
	{
		Hashtable_t1407064410 * L_104 = V_1;
		FsmVector3_t533912882 * L_105 = __this->get_lookAtVector_28();
		NullCheck(L_105);
		Vector3_t4282066566  L_106 = FsmVector3_get_Value_m2779135117(L_105, /*hidden argument*/NULL);
		Vector3_t4282066566  L_107 = L_106;
		Il2CppObject * L_108 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_107);
		NullCheck(L_104);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_104, _stringLiteral1258521840, L_108);
	}

IL_02f1:
	{
		FsmGameObject_t1697147867 * L_109 = __this->get_lookAtObject_27();
		NullCheck(L_109);
		bool L_110 = NamedVariable_get_IsNone_m281035543(L_109, /*hidden argument*/NULL);
		if (!L_110)
		{
			goto IL_0311;
		}
	}
	{
		FsmVector3_t533912882 * L_111 = __this->get_lookAtVector_28();
		NullCheck(L_111);
		bool L_112 = NamedVariable_get_IsNone_m281035543(L_111, /*hidden argument*/NULL);
		if (L_112)
		{
			goto IL_0346;
		}
	}

IL_0311:
	{
		Hashtable_t1407064410 * L_113 = V_1;
		FsmFloat_t2134102846 * L_114 = __this->get_lookTime_29();
		NullCheck(L_114);
		bool L_115 = NamedVariable_get_IsNone_m281035543(L_114, /*hidden argument*/NULL);
		G_B38_0 = _stringLiteral2253828588;
		G_B38_1 = L_113;
		if (!L_115)
		{
			G_B39_0 = _stringLiteral2253828588;
			G_B39_1 = L_113;
			goto IL_0331;
		}
	}
	{
		G_B40_0 = (0.0f);
		G_B40_1 = G_B38_0;
		G_B40_2 = G_B38_1;
		goto IL_033c;
	}

IL_0331:
	{
		FsmFloat_t2134102846 * L_116 = __this->get_lookTime_29();
		NullCheck(L_116);
		float L_117 = FsmFloat_get_Value_m4137923823(L_116, /*hidden argument*/NULL);
		G_B40_0 = L_117;
		G_B40_1 = G_B39_0;
		G_B40_2 = G_B39_1;
	}

IL_033c:
	{
		float L_118 = G_B40_0;
		Il2CppObject * L_119 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_118);
		NullCheck(G_B40_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B40_2, G_B40_1, L_119);
	}

IL_0346:
	{
		((iTweenFsmAction_t410382178 *)__this)->set_itweenType_15(_stringLiteral3357649);
		GameObject_t3674682005 * L_120 = V_0;
		Hashtable_t1407064410 * L_121 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		iTween_MoveAdd_m205305439(NULL /*static, unused*/, L_120, L_121, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveBy::.ctor()
extern "C"  void iTweenMoveBy__ctor_m197714548 (iTweenMoveBy_t469656626 * __this, const MethodInfo* method)
{
	{
		__this->set_easeType_23(((int32_t)21));
		iTweenFsmAction__ctor_m2960815636(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveBy::Reset()
extern Il2CppClass* FsmString_t952858651_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmFloat_t2134102846_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmGameObject_t1697147867_il2cpp_TypeInfo_var;
extern const uint32_t iTweenMoveBy_Reset_m2139114785_MetadataUsageId;
extern "C"  void iTweenMoveBy_Reset_m2139114785 (iTweenMoveBy_t469656626 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenMoveBy_Reset_m2139114785_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmString_t952858651 * V_0 = NULL;
	FsmVector3_t533912882 * V_1 = NULL;
	FsmFloat_t2134102846 * V_2 = NULL;
	FsmGameObject_t1697147867 * V_3 = NULL;
	{
		iTweenFsmAction_Reset_m607248577(__this, /*hidden argument*/NULL);
		FsmString_t952858651 * L_0 = (FsmString_t952858651 *)il2cpp_codegen_object_new(FsmString_t952858651_il2cpp_TypeInfo_var);
		FsmString__ctor_m2679108260(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmString_t952858651 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmString_t952858651 * L_2 = V_0;
		__this->set_id_18(L_2);
		FsmFloat_t2134102846 * L_3 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_time_20(L_3);
		FsmFloat_t2134102846 * L_4 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_delay_21(L_4);
		__this->set_loopType_24(0);
		FsmVector3_t533912882 * L_5 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_5, /*hidden argument*/NULL);
		V_1 = L_5;
		FsmVector3_t533912882 * L_6 = V_1;
		NullCheck(L_6);
		NamedVariable_set_UseVariable_m4266138971(L_6, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_7 = V_1;
		__this->set_vector_19(L_7);
		FsmFloat_t2134102846 * L_8 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_8, /*hidden argument*/NULL);
		V_2 = L_8;
		FsmFloat_t2134102846 * L_9 = V_2;
		NullCheck(L_9);
		NamedVariable_set_UseVariable_m4266138971(L_9, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_10 = V_2;
		__this->set_speed_22(L_10);
		__this->set_space_25(0);
		FsmBool_t1075959796 * L_11 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_orientToPath_26(L_11);
		FsmGameObject_t1697147867 * L_12 = (FsmGameObject_t1697147867 *)il2cpp_codegen_object_new(FsmGameObject_t1697147867_il2cpp_TypeInfo_var);
		FsmGameObject__ctor_m1048269796(L_12, /*hidden argument*/NULL);
		V_3 = L_12;
		FsmGameObject_t1697147867 * L_13 = V_3;
		NullCheck(L_13);
		NamedVariable_set_UseVariable_m4266138971(L_13, (bool)1, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_14 = V_3;
		__this->set_lookAtObject_27(L_14);
		FsmVector3_t533912882 * L_15 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_15, /*hidden argument*/NULL);
		V_1 = L_15;
		FsmVector3_t533912882 * L_16 = V_1;
		NullCheck(L_16);
		NamedVariable_set_UseVariable_m4266138971(L_16, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_17 = V_1;
		__this->set_lookAtVector_28(L_17);
		FsmFloat_t2134102846 * L_18 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_lookTime_29(L_18);
		__this->set_axis_30(0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveBy::OnEnter()
extern "C"  void iTweenMoveBy_OnEnter_m416848203 (iTweenMoveBy_t469656626 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnEnteriTween_m3024539459(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_loopType_24();
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		iTweenFsmAction_IsLoop_m2539277653(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_001e:
	{
		iTweenMoveBy_DoiTween_m1488524637(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveBy::OnExit()
extern "C"  void iTweenMoveBy_OnExit_m3347502317 (iTweenMoveBy_t469656626 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnExitiTween_m1981625761(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveBy::DoiTween()
extern const Il2CppType* AxisRestriction_t3260241011_0_0_0_var;
extern Il2CppClass* Hashtable_t1407064410_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* EaseType_t2734598229_il2cpp_TypeInfo_var;
extern Il2CppClass* LoopType_t1485160459_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* Space_t4209342076_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* AxisRestriction_t3260241011_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2862688501_il2cpp_TypeInfo_var;
extern Il2CppClass* iTween_t3087282050_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2881114200;
extern Il2CppCodeGenString* _stringLiteral3560141;
extern Il2CppCodeGenString* _stringLiteral109641799;
extern Il2CppCodeGenString* _stringLiteral95467907;
extern Il2CppCodeGenString* _stringLiteral3507899432;
extern Il2CppCodeGenString* _stringLiteral2258461662;
extern Il2CppCodeGenString* _stringLiteral2105864216;
extern Il2CppCodeGenString* _stringLiteral338257242;
extern Il2CppCodeGenString* _stringLiteral3696326558;
extern Il2CppCodeGenString* _stringLiteral2987624931;
extern Il2CppCodeGenString* _stringLiteral647687137;
extern Il2CppCodeGenString* _stringLiteral2052738345;
extern Il2CppCodeGenString* _stringLiteral1067506955;
extern Il2CppCodeGenString* _stringLiteral109637894;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral3008417;
extern Il2CppCodeGenString* _stringLiteral3159645605;
extern Il2CppCodeGenString* _stringLiteral1258521840;
extern Il2CppCodeGenString* _stringLiteral2253828588;
extern Il2CppCodeGenString* _stringLiteral3357649;
extern const uint32_t iTweenMoveBy_DoiTween_m1488524637_MetadataUsageId;
extern "C"  void iTweenMoveBy_DoiTween_m1488524637 (iTweenMoveBy_t469656626 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenMoveBy_DoiTween_m1488524637_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Hashtable_t1407064410 * V_1 = NULL;
	String_t* G_B4_0 = NULL;
	Hashtable_t1407064410 * G_B4_1 = NULL;
	String_t* G_B3_0 = NULL;
	Hashtable_t1407064410 * G_B3_1 = NULL;
	Vector3_t4282066566  G_B5_0;
	memset(&G_B5_0, 0, sizeof(G_B5_0));
	String_t* G_B5_1 = NULL;
	Hashtable_t1407064410 * G_B5_2 = NULL;
	Hashtable_t1407064410 * G_B7_0 = NULL;
	Hashtable_t1407064410 * G_B6_0 = NULL;
	String_t* G_B8_0 = NULL;
	Hashtable_t1407064410 * G_B8_1 = NULL;
	String_t* G_B13_0 = NULL;
	Hashtable_t1407064410 * G_B13_1 = NULL;
	String_t* G_B9_0 = NULL;
	Hashtable_t1407064410 * G_B9_1 = NULL;
	String_t* G_B11_0 = NULL;
	Hashtable_t1407064410 * G_B11_1 = NULL;
	String_t* G_B10_0 = NULL;
	Hashtable_t1407064410 * G_B10_1 = NULL;
	float G_B12_0 = 0.0f;
	String_t* G_B12_1 = NULL;
	Hashtable_t1407064410 * G_B12_2 = NULL;
	float G_B14_0 = 0.0f;
	String_t* G_B14_1 = NULL;
	Hashtable_t1407064410 * G_B14_2 = NULL;
	String_t* G_B16_0 = NULL;
	Hashtable_t1407064410 * G_B16_1 = NULL;
	String_t* G_B15_0 = NULL;
	Hashtable_t1407064410 * G_B15_1 = NULL;
	float G_B17_0 = 0.0f;
	String_t* G_B17_1 = NULL;
	Hashtable_t1407064410 * G_B17_2 = NULL;
	String_t* G_B19_0 = NULL;
	Hashtable_t1407064410 * G_B19_1 = NULL;
	String_t* G_B18_0 = NULL;
	Hashtable_t1407064410 * G_B18_1 = NULL;
	int32_t G_B20_0 = 0;
	String_t* G_B20_1 = NULL;
	Hashtable_t1407064410 * G_B20_2 = NULL;
	String_t* G_B22_0 = NULL;
	Hashtable_t1407064410 * G_B22_1 = NULL;
	String_t* G_B21_0 = NULL;
	Hashtable_t1407064410 * G_B21_1 = NULL;
	String_t* G_B23_0 = NULL;
	String_t* G_B23_1 = NULL;
	Hashtable_t1407064410 * G_B23_2 = NULL;
	String_t* G_B25_0 = NULL;
	Hashtable_t1407064410 * G_B25_1 = NULL;
	String_t* G_B24_0 = NULL;
	Hashtable_t1407064410 * G_B24_1 = NULL;
	String_t* G_B26_0 = NULL;
	String_t* G_B26_1 = NULL;
	Hashtable_t1407064410 * G_B26_2 = NULL;
	String_t* G_B31_0 = NULL;
	Hashtable_t1407064410 * G_B31_1 = NULL;
	String_t* G_B30_0 = NULL;
	Hashtable_t1407064410 * G_B30_1 = NULL;
	Vector3_t4282066566  G_B32_0;
	memset(&G_B32_0, 0, sizeof(G_B32_0));
	String_t* G_B32_1 = NULL;
	Hashtable_t1407064410 * G_B32_2 = NULL;
	String_t* G_B39_0 = NULL;
	Hashtable_t1407064410 * G_B39_1 = NULL;
	String_t* G_B38_0 = NULL;
	Hashtable_t1407064410 * G_B38_1 = NULL;
	float G_B40_0 = 0.0f;
	String_t* G_B40_1 = NULL;
	Hashtable_t1407064410 * G_B40_2 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_17();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		Hashtable_t1407064410 * L_5 = (Hashtable_t1407064410 *)il2cpp_codegen_object_new(Hashtable_t1407064410_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1514037738(L_5, /*hidden argument*/NULL);
		V_1 = L_5;
		Hashtable_t1407064410 * L_6 = V_1;
		FsmVector3_t533912882 * L_7 = __this->get_vector_19();
		NullCheck(L_7);
		bool L_8 = NamedVariable_get_IsNone_m281035543(L_7, /*hidden argument*/NULL);
		G_B3_0 = _stringLiteral2881114200;
		G_B3_1 = L_6;
		if (!L_8)
		{
			G_B4_0 = _stringLiteral2881114200;
			G_B4_1 = L_6;
			goto IL_0045;
		}
	}
	{
		Vector3_t4282066566  L_9 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B5_0 = L_9;
		G_B5_1 = G_B3_0;
		G_B5_2 = G_B3_1;
		goto IL_0050;
	}

IL_0045:
	{
		FsmVector3_t533912882 * L_10 = __this->get_vector_19();
		NullCheck(L_10);
		Vector3_t4282066566  L_11 = FsmVector3_get_Value_m2779135117(L_10, /*hidden argument*/NULL);
		G_B5_0 = L_11;
		G_B5_1 = G_B4_0;
		G_B5_2 = G_B4_1;
	}

IL_0050:
	{
		Vector3_t4282066566  L_12 = G_B5_0;
		Il2CppObject * L_13 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_12);
		NullCheck(G_B5_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B5_2, G_B5_1, L_13);
		Hashtable_t1407064410 * L_14 = V_1;
		FsmFloat_t2134102846 * L_15 = __this->get_speed_22();
		NullCheck(L_15);
		bool L_16 = NamedVariable_get_IsNone_m281035543(L_15, /*hidden argument*/NULL);
		G_B6_0 = L_14;
		if (!L_16)
		{
			G_B7_0 = L_14;
			goto IL_0075;
		}
	}
	{
		G_B8_0 = _stringLiteral3560141;
		G_B8_1 = G_B6_0;
		goto IL_007a;
	}

IL_0075:
	{
		G_B8_0 = _stringLiteral109641799;
		G_B8_1 = G_B7_0;
	}

IL_007a:
	{
		FsmFloat_t2134102846 * L_17 = __this->get_speed_22();
		NullCheck(L_17);
		bool L_18 = NamedVariable_get_IsNone_m281035543(L_17, /*hidden argument*/NULL);
		G_B9_0 = G_B8_0;
		G_B9_1 = G_B8_1;
		if (!L_18)
		{
			G_B13_0 = G_B8_0;
			G_B13_1 = G_B8_1;
			goto IL_00b4;
		}
	}
	{
		FsmFloat_t2134102846 * L_19 = __this->get_time_20();
		NullCheck(L_19);
		bool L_20 = NamedVariable_get_IsNone_m281035543(L_19, /*hidden argument*/NULL);
		G_B10_0 = G_B9_0;
		G_B10_1 = G_B9_1;
		if (!L_20)
		{
			G_B11_0 = G_B9_0;
			G_B11_1 = G_B9_1;
			goto IL_00a4;
		}
	}
	{
		G_B12_0 = (1.0f);
		G_B12_1 = G_B10_0;
		G_B12_2 = G_B10_1;
		goto IL_00af;
	}

IL_00a4:
	{
		FsmFloat_t2134102846 * L_21 = __this->get_time_20();
		NullCheck(L_21);
		float L_22 = FsmFloat_get_Value_m4137923823(L_21, /*hidden argument*/NULL);
		G_B12_0 = L_22;
		G_B12_1 = G_B11_0;
		G_B12_2 = G_B11_1;
	}

IL_00af:
	{
		G_B14_0 = G_B12_0;
		G_B14_1 = G_B12_1;
		G_B14_2 = G_B12_2;
		goto IL_00bf;
	}

IL_00b4:
	{
		FsmFloat_t2134102846 * L_23 = __this->get_speed_22();
		NullCheck(L_23);
		float L_24 = FsmFloat_get_Value_m4137923823(L_23, /*hidden argument*/NULL);
		G_B14_0 = L_24;
		G_B14_1 = G_B13_0;
		G_B14_2 = G_B13_1;
	}

IL_00bf:
	{
		float L_25 = G_B14_0;
		Il2CppObject * L_26 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_25);
		NullCheck(G_B14_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B14_2, G_B14_1, L_26);
		Hashtable_t1407064410 * L_27 = V_1;
		FsmFloat_t2134102846 * L_28 = __this->get_delay_21();
		NullCheck(L_28);
		bool L_29 = NamedVariable_get_IsNone_m281035543(L_28, /*hidden argument*/NULL);
		G_B15_0 = _stringLiteral95467907;
		G_B15_1 = L_27;
		if (!L_29)
		{
			G_B16_0 = _stringLiteral95467907;
			G_B16_1 = L_27;
			goto IL_00e9;
		}
	}
	{
		G_B17_0 = (0.0f);
		G_B17_1 = G_B15_0;
		G_B17_2 = G_B15_1;
		goto IL_00f4;
	}

IL_00e9:
	{
		FsmFloat_t2134102846 * L_30 = __this->get_delay_21();
		NullCheck(L_30);
		float L_31 = FsmFloat_get_Value_m4137923823(L_30, /*hidden argument*/NULL);
		G_B17_0 = L_31;
		G_B17_1 = G_B16_0;
		G_B17_2 = G_B16_1;
	}

IL_00f4:
	{
		float L_32 = G_B17_0;
		Il2CppObject * L_33 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_32);
		NullCheck(G_B17_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B17_2, G_B17_1, L_33);
		Hashtable_t1407064410 * L_34 = V_1;
		int32_t L_35 = __this->get_easeType_23();
		int32_t L_36 = L_35;
		Il2CppObject * L_37 = Box(EaseType_t2734598229_il2cpp_TypeInfo_var, &L_36);
		NullCheck(L_34);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_34, _stringLiteral3507899432, L_37);
		Hashtable_t1407064410 * L_38 = V_1;
		int32_t L_39 = __this->get_loopType_24();
		int32_t L_40 = L_39;
		Il2CppObject * L_41 = Box(LoopType_t1485160459_il2cpp_TypeInfo_var, &L_40);
		NullCheck(L_38);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_38, _stringLiteral2258461662, L_41);
		Hashtable_t1407064410 * L_42 = V_1;
		NullCheck(L_42);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_42, _stringLiteral2105864216, _stringLiteral338257242);
		Hashtable_t1407064410 * L_43 = V_1;
		int32_t L_44 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_45 = L_44;
		Il2CppObject * L_46 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_45);
		NullCheck(L_43);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_43, _stringLiteral3696326558, L_46);
		Hashtable_t1407064410 * L_47 = V_1;
		NullCheck(L_47);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_47, _stringLiteral2987624931, _stringLiteral647687137);
		Hashtable_t1407064410 * L_48 = V_1;
		int32_t L_49 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_50 = L_49;
		Il2CppObject * L_51 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_50);
		NullCheck(L_48);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_48, _stringLiteral2052738345, L_51);
		Hashtable_t1407064410 * L_52 = V_1;
		FsmBool_t1075959796 * L_53 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_53);
		bool L_54 = NamedVariable_get_IsNone_m281035543(L_53, /*hidden argument*/NULL);
		G_B18_0 = _stringLiteral1067506955;
		G_B18_1 = L_52;
		if (!L_54)
		{
			G_B19_0 = _stringLiteral1067506955;
			G_B19_1 = L_52;
			goto IL_0192;
		}
	}
	{
		G_B20_0 = 0;
		G_B20_1 = G_B18_0;
		G_B20_2 = G_B18_1;
		goto IL_019d;
	}

IL_0192:
	{
		FsmBool_t1075959796 * L_55 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_55);
		bool L_56 = FsmBool_get_Value_m3101329097(L_55, /*hidden argument*/NULL);
		G_B20_0 = ((int32_t)(L_56));
		G_B20_1 = G_B19_0;
		G_B20_2 = G_B19_1;
	}

IL_019d:
	{
		bool L_57 = ((bool)G_B20_0);
		Il2CppObject * L_58 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_57);
		NullCheck(G_B20_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B20_2, G_B20_1, L_58);
		Hashtable_t1407064410 * L_59 = V_1;
		int32_t L_60 = __this->get_space_25();
		int32_t L_61 = L_60;
		Il2CppObject * L_62 = Box(Space_t4209342076_il2cpp_TypeInfo_var, &L_61);
		NullCheck(L_59);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_59, _stringLiteral109637894, L_62);
		Hashtable_t1407064410 * L_63 = V_1;
		FsmString_t952858651 * L_64 = __this->get_id_18();
		NullCheck(L_64);
		bool L_65 = NamedVariable_get_IsNone_m281035543(L_64, /*hidden argument*/NULL);
		G_B21_0 = _stringLiteral3373707;
		G_B21_1 = L_63;
		if (!L_65)
		{
			G_B22_0 = _stringLiteral3373707;
			G_B22_1 = L_63;
			goto IL_01dd;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_66 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B23_0 = L_66;
		G_B23_1 = G_B21_0;
		G_B23_2 = G_B21_1;
		goto IL_01e8;
	}

IL_01dd:
	{
		FsmString_t952858651 * L_67 = __this->get_id_18();
		NullCheck(L_67);
		String_t* L_68 = FsmString_get_Value_m872383149(L_67, /*hidden argument*/NULL);
		G_B23_0 = L_68;
		G_B23_1 = G_B22_0;
		G_B23_2 = G_B22_1;
	}

IL_01e8:
	{
		NullCheck(G_B23_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B23_2, G_B23_1, G_B23_0);
		Hashtable_t1407064410 * L_69 = V_1;
		int32_t L_70 = __this->get_axis_30();
		G_B24_0 = _stringLiteral3008417;
		G_B24_1 = L_69;
		if (L_70)
		{
			G_B25_0 = _stringLiteral3008417;
			G_B25_1 = L_69;
			goto IL_0208;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_71 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B26_0 = L_71;
		G_B26_1 = G_B24_0;
		G_B26_2 = G_B24_1;
		goto IL_0222;
	}

IL_0208:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_72 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(AxisRestriction_t3260241011_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_73 = __this->get_axis_30();
		int32_t L_74 = L_73;
		Il2CppObject * L_75 = Box(AxisRestriction_t3260241011_il2cpp_TypeInfo_var, &L_74);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		String_t* L_76 = Enum_GetName_m3333121265(NULL /*static, unused*/, L_72, L_75, /*hidden argument*/NULL);
		G_B26_0 = L_76;
		G_B26_1 = G_B25_0;
		G_B26_2 = G_B25_1;
	}

IL_0222:
	{
		NullCheck(G_B26_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B26_2, G_B26_1, G_B26_0);
		FsmBool_t1075959796 * L_77 = __this->get_orientToPath_26();
		NullCheck(L_77);
		bool L_78 = NamedVariable_get_IsNone_m281035543(L_77, /*hidden argument*/NULL);
		if (L_78)
		{
			goto IL_0252;
		}
	}
	{
		Hashtable_t1407064410 * L_79 = V_1;
		FsmBool_t1075959796 * L_80 = __this->get_orientToPath_26();
		NullCheck(L_80);
		bool L_81 = FsmBool_get_Value_m3101329097(L_80, /*hidden argument*/NULL);
		bool L_82 = L_81;
		Il2CppObject * L_83 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_82);
		NullCheck(L_79);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_79, _stringLiteral3159645605, L_83);
	}

IL_0252:
	{
		FsmGameObject_t1697147867 * L_84 = __this->get_lookAtObject_27();
		NullCheck(L_84);
		bool L_85 = NamedVariable_get_IsNone_m281035543(L_84, /*hidden argument*/NULL);
		if (L_85)
		{
			goto IL_02c6;
		}
	}
	{
		Hashtable_t1407064410 * L_86 = V_1;
		FsmVector3_t533912882 * L_87 = __this->get_lookAtVector_28();
		NullCheck(L_87);
		bool L_88 = NamedVariable_get_IsNone_m281035543(L_87, /*hidden argument*/NULL);
		G_B30_0 = _stringLiteral1258521840;
		G_B30_1 = L_86;
		if (!L_88)
		{
			G_B31_0 = _stringLiteral1258521840;
			G_B31_1 = L_86;
			goto IL_0292;
		}
	}
	{
		FsmGameObject_t1697147867 * L_89 = __this->get_lookAtObject_27();
		NullCheck(L_89);
		GameObject_t3674682005 * L_90 = FsmGameObject_get_Value_m673294275(L_89, /*hidden argument*/NULL);
		NullCheck(L_90);
		Transform_t1659122786 * L_91 = GameObject_get_transform_m1278640159(L_90, /*hidden argument*/NULL);
		NullCheck(L_91);
		Vector3_t4282066566  L_92 = Transform_get_position_m2211398607(L_91, /*hidden argument*/NULL);
		G_B32_0 = L_92;
		G_B32_1 = G_B30_0;
		G_B32_2 = G_B30_1;
		goto IL_02b7;
	}

IL_0292:
	{
		FsmGameObject_t1697147867 * L_93 = __this->get_lookAtObject_27();
		NullCheck(L_93);
		GameObject_t3674682005 * L_94 = FsmGameObject_get_Value_m673294275(L_93, /*hidden argument*/NULL);
		NullCheck(L_94);
		Transform_t1659122786 * L_95 = GameObject_get_transform_m1278640159(L_94, /*hidden argument*/NULL);
		NullCheck(L_95);
		Vector3_t4282066566  L_96 = Transform_get_position_m2211398607(L_95, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_97 = __this->get_lookAtVector_28();
		NullCheck(L_97);
		Vector3_t4282066566  L_98 = FsmVector3_get_Value_m2779135117(L_97, /*hidden argument*/NULL);
		Vector3_t4282066566  L_99 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_96, L_98, /*hidden argument*/NULL);
		G_B32_0 = L_99;
		G_B32_1 = G_B31_0;
		G_B32_2 = G_B31_1;
	}

IL_02b7:
	{
		Vector3_t4282066566  L_100 = G_B32_0;
		Il2CppObject * L_101 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_100);
		NullCheck(G_B32_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B32_2, G_B32_1, L_101);
		goto IL_02f1;
	}

IL_02c6:
	{
		FsmVector3_t533912882 * L_102 = __this->get_lookAtVector_28();
		NullCheck(L_102);
		bool L_103 = NamedVariable_get_IsNone_m281035543(L_102, /*hidden argument*/NULL);
		if (L_103)
		{
			goto IL_02f1;
		}
	}
	{
		Hashtable_t1407064410 * L_104 = V_1;
		FsmVector3_t533912882 * L_105 = __this->get_lookAtVector_28();
		NullCheck(L_105);
		Vector3_t4282066566  L_106 = FsmVector3_get_Value_m2779135117(L_105, /*hidden argument*/NULL);
		Vector3_t4282066566  L_107 = L_106;
		Il2CppObject * L_108 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_107);
		NullCheck(L_104);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_104, _stringLiteral1258521840, L_108);
	}

IL_02f1:
	{
		FsmGameObject_t1697147867 * L_109 = __this->get_lookAtObject_27();
		NullCheck(L_109);
		bool L_110 = NamedVariable_get_IsNone_m281035543(L_109, /*hidden argument*/NULL);
		if (!L_110)
		{
			goto IL_0311;
		}
	}
	{
		FsmVector3_t533912882 * L_111 = __this->get_lookAtVector_28();
		NullCheck(L_111);
		bool L_112 = NamedVariable_get_IsNone_m281035543(L_111, /*hidden argument*/NULL);
		if (L_112)
		{
			goto IL_0346;
		}
	}

IL_0311:
	{
		Hashtable_t1407064410 * L_113 = V_1;
		FsmFloat_t2134102846 * L_114 = __this->get_lookTime_29();
		NullCheck(L_114);
		bool L_115 = NamedVariable_get_IsNone_m281035543(L_114, /*hidden argument*/NULL);
		G_B38_0 = _stringLiteral2253828588;
		G_B38_1 = L_113;
		if (!L_115)
		{
			G_B39_0 = _stringLiteral2253828588;
			G_B39_1 = L_113;
			goto IL_0331;
		}
	}
	{
		G_B40_0 = (0.0f);
		G_B40_1 = G_B38_0;
		G_B40_2 = G_B38_1;
		goto IL_033c;
	}

IL_0331:
	{
		FsmFloat_t2134102846 * L_116 = __this->get_lookTime_29();
		NullCheck(L_116);
		float L_117 = FsmFloat_get_Value_m4137923823(L_116, /*hidden argument*/NULL);
		G_B40_0 = L_117;
		G_B40_1 = G_B39_0;
		G_B40_2 = G_B39_1;
	}

IL_033c:
	{
		float L_118 = G_B40_0;
		Il2CppObject * L_119 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_118);
		NullCheck(G_B40_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B40_2, G_B40_1, L_119);
	}

IL_0346:
	{
		((iTweenFsmAction_t410382178 *)__this)->set_itweenType_15(_stringLiteral3357649);
		GameObject_t3674682005 * L_120 = V_0;
		Hashtable_t1407064410 * L_121 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		iTween_MoveBy_m1058379411(NULL /*static, unused*/, L_120, L_121, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveFrom::.ctor()
extern "C"  void iTweenMoveFrom__ctor_m70871681 (iTweenMoveFrom_t1076675461 * __this, const MethodInfo* method)
{
	{
		__this->set_easeType_24(((int32_t)21));
		iTweenFsmAction__ctor_m2960815636(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveFrom::Reset()
extern Il2CppClass* FsmString_t952858651_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmGameObject_t1697147867_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmFloat_t2134102846_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmBool_t1075959796_il2cpp_TypeInfo_var;
extern const uint32_t iTweenMoveFrom_Reset_m2012271918_MetadataUsageId;
extern "C"  void iTweenMoveFrom_Reset_m2012271918 (iTweenMoveFrom_t1076675461 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenMoveFrom_Reset_m2012271918_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmString_t952858651 * V_0 = NULL;
	FsmGameObject_t1697147867 * V_1 = NULL;
	FsmVector3_t533912882 * V_2 = NULL;
	FsmFloat_t2134102846 * V_3 = NULL;
	FsmBool_t1075959796 * V_4 = NULL;
	{
		iTweenFsmAction_Reset_m607248577(__this, /*hidden argument*/NULL);
		FsmString_t952858651 * L_0 = (FsmString_t952858651 *)il2cpp_codegen_object_new(FsmString_t952858651_il2cpp_TypeInfo_var);
		FsmString__ctor_m2679108260(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmString_t952858651 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmString_t952858651 * L_2 = V_0;
		__this->set_id_18(L_2);
		FsmGameObject_t1697147867 * L_3 = (FsmGameObject_t1697147867 *)il2cpp_codegen_object_new(FsmGameObject_t1697147867_il2cpp_TypeInfo_var);
		FsmGameObject__ctor_m1048269796(L_3, /*hidden argument*/NULL);
		V_1 = L_3;
		FsmGameObject_t1697147867 * L_4 = V_1;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_5 = V_1;
		__this->set_transformPosition_19(L_5);
		FsmVector3_t533912882 * L_6 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_6, /*hidden argument*/NULL);
		V_2 = L_6;
		FsmVector3_t533912882 * L_7 = V_2;
		NullCheck(L_7);
		NamedVariable_set_UseVariable_m4266138971(L_7, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_8 = V_2;
		__this->set_vectorPosition_20(L_8);
		FsmFloat_t2134102846 * L_9 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_time_21(L_9);
		FsmFloat_t2134102846 * L_10 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_delay_22(L_10);
		__this->set_loopType_25(0);
		FsmFloat_t2134102846 * L_11 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_11, /*hidden argument*/NULL);
		V_3 = L_11;
		FsmFloat_t2134102846 * L_12 = V_3;
		NullCheck(L_12);
		NamedVariable_set_UseVariable_m4266138971(L_12, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_13 = V_3;
		__this->set_speed_23(L_13);
		__this->set_space_26(0);
		FsmBool_t1075959796 * L_14 = (FsmBool_t1075959796 *)il2cpp_codegen_object_new(FsmBool_t1075959796_il2cpp_TypeInfo_var);
		FsmBool__ctor_m1553455211(L_14, /*hidden argument*/NULL);
		V_4 = L_14;
		FsmBool_t1075959796 * L_15 = V_4;
		NullCheck(L_15);
		FsmBool_set_Value_m1126216340(L_15, (bool)1, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_16 = V_4;
		__this->set_orientToPath_27(L_16);
		FsmGameObject_t1697147867 * L_17 = (FsmGameObject_t1697147867 *)il2cpp_codegen_object_new(FsmGameObject_t1697147867_il2cpp_TypeInfo_var);
		FsmGameObject__ctor_m1048269796(L_17, /*hidden argument*/NULL);
		V_1 = L_17;
		FsmGameObject_t1697147867 * L_18 = V_1;
		NullCheck(L_18);
		NamedVariable_set_UseVariable_m4266138971(L_18, (bool)1, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_19 = V_1;
		__this->set_lookAtObject_28(L_19);
		FsmVector3_t533912882 * L_20 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_20, /*hidden argument*/NULL);
		V_2 = L_20;
		FsmVector3_t533912882 * L_21 = V_2;
		NullCheck(L_21);
		NamedVariable_set_UseVariable_m4266138971(L_21, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_22 = V_2;
		__this->set_lookAtVector_29(L_22);
		FsmFloat_t2134102846 * L_23 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_lookTime_30(L_23);
		__this->set_axis_31(0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveFrom::OnEnter()
extern "C"  void iTweenMoveFrom_OnEnter_m3074904600 (iTweenMoveFrom_t1076675461 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnEnteriTween_m3024539459(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_loopType_25();
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		iTweenFsmAction_IsLoop_m2539277653(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_001e:
	{
		iTweenMoveFrom_DoiTween_m2283894320(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveFrom::OnExit()
extern "C"  void iTweenMoveFrom_OnExit_m3710340736 (iTweenMoveFrom_t1076675461 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnExitiTween_m1981625761(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveFrom::DoiTween()
extern const Il2CppType* AxisRestriction_t3260241011_0_0_0_var;
extern Il2CppClass* Hashtable_t1407064410_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* EaseType_t2734598229_il2cpp_TypeInfo_var;
extern Il2CppClass* LoopType_t1485160459_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* AxisRestriction_t3260241011_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2862688501_il2cpp_TypeInfo_var;
extern Il2CppClass* iTween_t3087282050_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral747804969;
extern Il2CppCodeGenString* _stringLiteral3560141;
extern Il2CppCodeGenString* _stringLiteral109641799;
extern Il2CppCodeGenString* _stringLiteral95467907;
extern Il2CppCodeGenString* _stringLiteral3507899432;
extern Il2CppCodeGenString* _stringLiteral2258461662;
extern Il2CppCodeGenString* _stringLiteral2105864216;
extern Il2CppCodeGenString* _stringLiteral338257242;
extern Il2CppCodeGenString* _stringLiteral3696326558;
extern Il2CppCodeGenString* _stringLiteral2987624931;
extern Il2CppCodeGenString* _stringLiteral647687137;
extern Il2CppCodeGenString* _stringLiteral2052738345;
extern Il2CppCodeGenString* _stringLiteral1067506955;
extern Il2CppCodeGenString* _stringLiteral2094103681;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral3008417;
extern Il2CppCodeGenString* _stringLiteral3159645605;
extern Il2CppCodeGenString* _stringLiteral1258521840;
extern Il2CppCodeGenString* _stringLiteral2253828588;
extern Il2CppCodeGenString* _stringLiteral3357649;
extern const uint32_t iTweenMoveFrom_DoiTween_m2283894320_MetadataUsageId;
extern "C"  void iTweenMoveFrom_DoiTween_m2283894320 (iTweenMoveFrom_t1076675461 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenMoveFrom_DoiTween_m2283894320_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Hashtable_t1407064410 * V_2 = NULL;
	Vector3_t4282066566  G_B5_0;
	memset(&G_B5_0, 0, sizeof(G_B5_0));
	Vector3_t4282066566  G_B11_0;
	memset(&G_B11_0, 0, sizeof(G_B11_0));
	Hashtable_t1407064410 * G_B14_0 = NULL;
	Hashtable_t1407064410 * G_B13_0 = NULL;
	String_t* G_B15_0 = NULL;
	Hashtable_t1407064410 * G_B15_1 = NULL;
	String_t* G_B20_0 = NULL;
	Hashtable_t1407064410 * G_B20_1 = NULL;
	String_t* G_B16_0 = NULL;
	Hashtable_t1407064410 * G_B16_1 = NULL;
	String_t* G_B18_0 = NULL;
	Hashtable_t1407064410 * G_B18_1 = NULL;
	String_t* G_B17_0 = NULL;
	Hashtable_t1407064410 * G_B17_1 = NULL;
	float G_B19_0 = 0.0f;
	String_t* G_B19_1 = NULL;
	Hashtable_t1407064410 * G_B19_2 = NULL;
	float G_B21_0 = 0.0f;
	String_t* G_B21_1 = NULL;
	Hashtable_t1407064410 * G_B21_2 = NULL;
	String_t* G_B23_0 = NULL;
	Hashtable_t1407064410 * G_B23_1 = NULL;
	String_t* G_B22_0 = NULL;
	Hashtable_t1407064410 * G_B22_1 = NULL;
	float G_B24_0 = 0.0f;
	String_t* G_B24_1 = NULL;
	Hashtable_t1407064410 * G_B24_2 = NULL;
	String_t* G_B26_0 = NULL;
	Hashtable_t1407064410 * G_B26_1 = NULL;
	String_t* G_B25_0 = NULL;
	Hashtable_t1407064410 * G_B25_1 = NULL;
	int32_t G_B27_0 = 0;
	String_t* G_B27_1 = NULL;
	Hashtable_t1407064410 * G_B27_2 = NULL;
	String_t* G_B29_0 = NULL;
	Hashtable_t1407064410 * G_B29_1 = NULL;
	String_t* G_B28_0 = NULL;
	Hashtable_t1407064410 * G_B28_1 = NULL;
	String_t* G_B30_0 = NULL;
	String_t* G_B30_1 = NULL;
	Hashtable_t1407064410 * G_B30_2 = NULL;
	String_t* G_B32_0 = NULL;
	Hashtable_t1407064410 * G_B32_1 = NULL;
	String_t* G_B31_0 = NULL;
	Hashtable_t1407064410 * G_B31_1 = NULL;
	String_t* G_B33_0 = NULL;
	String_t* G_B33_1 = NULL;
	Hashtable_t1407064410 * G_B33_2 = NULL;
	String_t* G_B38_0 = NULL;
	Hashtable_t1407064410 * G_B38_1 = NULL;
	String_t* G_B37_0 = NULL;
	Hashtable_t1407064410 * G_B37_1 = NULL;
	Vector3_t4282066566  G_B39_0;
	memset(&G_B39_0, 0, sizeof(G_B39_0));
	String_t* G_B39_1 = NULL;
	Hashtable_t1407064410 * G_B39_2 = NULL;
	String_t* G_B46_0 = NULL;
	Hashtable_t1407064410 * G_B46_1 = NULL;
	String_t* G_B45_0 = NULL;
	Hashtable_t1407064410 * G_B45_1 = NULL;
	float G_B47_0 = 0.0f;
	String_t* G_B47_1 = NULL;
	Hashtable_t1407064410 * G_B47_2 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_17();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		FsmVector3_t533912882 * L_5 = __this->get_vectorPosition_20();
		NullCheck(L_5);
		bool L_6 = NamedVariable_get_IsNone_m281035543(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0039;
		}
	}
	{
		Vector3_t4282066566  L_7 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B5_0 = L_7;
		goto IL_0044;
	}

IL_0039:
	{
		FsmVector3_t533912882 * L_8 = __this->get_vectorPosition_20();
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = FsmVector3_get_Value_m2779135117(L_8, /*hidden argument*/NULL);
		G_B5_0 = L_9;
	}

IL_0044:
	{
		V_1 = G_B5_0;
		FsmGameObject_t1697147867 * L_10 = __this->get_transformPosition_19();
		NullCheck(L_10);
		bool L_11 = NamedVariable_get_IsNone_m281035543(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_00d7;
		}
	}
	{
		FsmGameObject_t1697147867 * L_12 = __this->get_transformPosition_19();
		NullCheck(L_12);
		GameObject_t3674682005 * L_13 = FsmGameObject_get_Value_m673294275(L_12, /*hidden argument*/NULL);
		bool L_14 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00d7;
		}
	}
	{
		int32_t L_15 = __this->get_space_26();
		if (!L_15)
		{
			goto IL_008b;
		}
	}
	{
		GameObject_t3674682005 * L_16 = V_0;
		NullCheck(L_16);
		Transform_t1659122786 * L_17 = GameObject_get_transform_m1278640159(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_t1659122786 * L_18 = Transform_get_parent_m2236876972(L_17, /*hidden argument*/NULL);
		bool L_19 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_18, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00ab;
		}
	}

IL_008b:
	{
		FsmGameObject_t1697147867 * L_20 = __this->get_transformPosition_19();
		NullCheck(L_20);
		GameObject_t3674682005 * L_21 = FsmGameObject_get_Value_m673294275(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_t1659122786 * L_22 = GameObject_get_transform_m1278640159(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		Vector3_t4282066566  L_23 = Transform_get_position_m2211398607(L_22, /*hidden argument*/NULL);
		Vector3_t4282066566  L_24 = V_1;
		Vector3_t4282066566  L_25 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		G_B11_0 = L_25;
		goto IL_00d6;
	}

IL_00ab:
	{
		GameObject_t3674682005 * L_26 = V_0;
		NullCheck(L_26);
		Transform_t1659122786 * L_27 = GameObject_get_transform_m1278640159(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		Transform_t1659122786 * L_28 = Transform_get_parent_m2236876972(L_27, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_29 = __this->get_transformPosition_19();
		NullCheck(L_29);
		GameObject_t3674682005 * L_30 = FsmGameObject_get_Value_m673294275(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		Transform_t1659122786 * L_31 = GameObject_get_transform_m1278640159(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		Vector3_t4282066566  L_32 = Transform_get_position_m2211398607(L_31, /*hidden argument*/NULL);
		NullCheck(L_28);
		Vector3_t4282066566  L_33 = Transform_InverseTransformPoint_m1626812000(L_28, L_32, /*hidden argument*/NULL);
		Vector3_t4282066566  L_34 = V_1;
		Vector3_t4282066566  L_35 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_33, L_34, /*hidden argument*/NULL);
		G_B11_0 = L_35;
	}

IL_00d6:
	{
		V_1 = G_B11_0;
	}

IL_00d7:
	{
		Hashtable_t1407064410 * L_36 = (Hashtable_t1407064410 *)il2cpp_codegen_object_new(Hashtable_t1407064410_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1514037738(L_36, /*hidden argument*/NULL);
		V_2 = L_36;
		Hashtable_t1407064410 * L_37 = V_2;
		Vector3_t4282066566  L_38 = V_1;
		Vector3_t4282066566  L_39 = L_38;
		Il2CppObject * L_40 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_39);
		NullCheck(L_37);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_37, _stringLiteral747804969, L_40);
		Hashtable_t1407064410 * L_41 = V_2;
		FsmFloat_t2134102846 * L_42 = __this->get_speed_23();
		NullCheck(L_42);
		bool L_43 = NamedVariable_get_IsNone_m281035543(L_42, /*hidden argument*/NULL);
		G_B13_0 = L_41;
		if (!L_43)
		{
			G_B14_0 = L_41;
			goto IL_0109;
		}
	}
	{
		G_B15_0 = _stringLiteral3560141;
		G_B15_1 = G_B13_0;
		goto IL_010e;
	}

IL_0109:
	{
		G_B15_0 = _stringLiteral109641799;
		G_B15_1 = G_B14_0;
	}

IL_010e:
	{
		FsmFloat_t2134102846 * L_44 = __this->get_speed_23();
		NullCheck(L_44);
		bool L_45 = NamedVariable_get_IsNone_m281035543(L_44, /*hidden argument*/NULL);
		G_B16_0 = G_B15_0;
		G_B16_1 = G_B15_1;
		if (!L_45)
		{
			G_B20_0 = G_B15_0;
			G_B20_1 = G_B15_1;
			goto IL_0148;
		}
	}
	{
		FsmFloat_t2134102846 * L_46 = __this->get_time_21();
		NullCheck(L_46);
		bool L_47 = NamedVariable_get_IsNone_m281035543(L_46, /*hidden argument*/NULL);
		G_B17_0 = G_B16_0;
		G_B17_1 = G_B16_1;
		if (!L_47)
		{
			G_B18_0 = G_B16_0;
			G_B18_1 = G_B16_1;
			goto IL_0138;
		}
	}
	{
		G_B19_0 = (1.0f);
		G_B19_1 = G_B17_0;
		G_B19_2 = G_B17_1;
		goto IL_0143;
	}

IL_0138:
	{
		FsmFloat_t2134102846 * L_48 = __this->get_time_21();
		NullCheck(L_48);
		float L_49 = FsmFloat_get_Value_m4137923823(L_48, /*hidden argument*/NULL);
		G_B19_0 = L_49;
		G_B19_1 = G_B18_0;
		G_B19_2 = G_B18_1;
	}

IL_0143:
	{
		G_B21_0 = G_B19_0;
		G_B21_1 = G_B19_1;
		G_B21_2 = G_B19_2;
		goto IL_0153;
	}

IL_0148:
	{
		FsmFloat_t2134102846 * L_50 = __this->get_speed_23();
		NullCheck(L_50);
		float L_51 = FsmFloat_get_Value_m4137923823(L_50, /*hidden argument*/NULL);
		G_B21_0 = L_51;
		G_B21_1 = G_B20_0;
		G_B21_2 = G_B20_1;
	}

IL_0153:
	{
		float L_52 = G_B21_0;
		Il2CppObject * L_53 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_52);
		NullCheck(G_B21_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B21_2, G_B21_1, L_53);
		Hashtable_t1407064410 * L_54 = V_2;
		FsmFloat_t2134102846 * L_55 = __this->get_delay_22();
		NullCheck(L_55);
		bool L_56 = NamedVariable_get_IsNone_m281035543(L_55, /*hidden argument*/NULL);
		G_B22_0 = _stringLiteral95467907;
		G_B22_1 = L_54;
		if (!L_56)
		{
			G_B23_0 = _stringLiteral95467907;
			G_B23_1 = L_54;
			goto IL_017d;
		}
	}
	{
		G_B24_0 = (0.0f);
		G_B24_1 = G_B22_0;
		G_B24_2 = G_B22_1;
		goto IL_0188;
	}

IL_017d:
	{
		FsmFloat_t2134102846 * L_57 = __this->get_delay_22();
		NullCheck(L_57);
		float L_58 = FsmFloat_get_Value_m4137923823(L_57, /*hidden argument*/NULL);
		G_B24_0 = L_58;
		G_B24_1 = G_B23_0;
		G_B24_2 = G_B23_1;
	}

IL_0188:
	{
		float L_59 = G_B24_0;
		Il2CppObject * L_60 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_59);
		NullCheck(G_B24_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B24_2, G_B24_1, L_60);
		Hashtable_t1407064410 * L_61 = V_2;
		int32_t L_62 = __this->get_easeType_24();
		int32_t L_63 = L_62;
		Il2CppObject * L_64 = Box(EaseType_t2734598229_il2cpp_TypeInfo_var, &L_63);
		NullCheck(L_61);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_61, _stringLiteral3507899432, L_64);
		Hashtable_t1407064410 * L_65 = V_2;
		int32_t L_66 = __this->get_loopType_25();
		int32_t L_67 = L_66;
		Il2CppObject * L_68 = Box(LoopType_t1485160459_il2cpp_TypeInfo_var, &L_67);
		NullCheck(L_65);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_65, _stringLiteral2258461662, L_68);
		Hashtable_t1407064410 * L_69 = V_2;
		NullCheck(L_69);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_69, _stringLiteral2105864216, _stringLiteral338257242);
		Hashtable_t1407064410 * L_70 = V_2;
		int32_t L_71 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_72 = L_71;
		Il2CppObject * L_73 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_72);
		NullCheck(L_70);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_70, _stringLiteral3696326558, L_73);
		Hashtable_t1407064410 * L_74 = V_2;
		NullCheck(L_74);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_74, _stringLiteral2987624931, _stringLiteral647687137);
		Hashtable_t1407064410 * L_75 = V_2;
		int32_t L_76 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_77 = L_76;
		Il2CppObject * L_78 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_77);
		NullCheck(L_75);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_75, _stringLiteral2052738345, L_78);
		Hashtable_t1407064410 * L_79 = V_2;
		FsmBool_t1075959796 * L_80 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_80);
		bool L_81 = NamedVariable_get_IsNone_m281035543(L_80, /*hidden argument*/NULL);
		G_B25_0 = _stringLiteral1067506955;
		G_B25_1 = L_79;
		if (!L_81)
		{
			G_B26_0 = _stringLiteral1067506955;
			G_B26_1 = L_79;
			goto IL_0226;
		}
	}
	{
		G_B27_0 = 0;
		G_B27_1 = G_B25_0;
		G_B27_2 = G_B25_1;
		goto IL_0231;
	}

IL_0226:
	{
		FsmBool_t1075959796 * L_82 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_82);
		bool L_83 = FsmBool_get_Value_m3101329097(L_82, /*hidden argument*/NULL);
		G_B27_0 = ((int32_t)(L_83));
		G_B27_1 = G_B26_0;
		G_B27_2 = G_B26_1;
	}

IL_0231:
	{
		bool L_84 = ((bool)G_B27_0);
		Il2CppObject * L_85 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_84);
		NullCheck(G_B27_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B27_2, G_B27_1, L_85);
		Hashtable_t1407064410 * L_86 = V_2;
		int32_t L_87 = __this->get_space_26();
		bool L_88 = ((bool)((((int32_t)L_87) == ((int32_t)1))? 1 : 0));
		Il2CppObject * L_89 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_88);
		NullCheck(L_86);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_86, _stringLiteral2094103681, L_89);
		Hashtable_t1407064410 * L_90 = V_2;
		FsmString_t952858651 * L_91 = __this->get_id_18();
		NullCheck(L_91);
		bool L_92 = NamedVariable_get_IsNone_m281035543(L_91, /*hidden argument*/NULL);
		G_B28_0 = _stringLiteral3373707;
		G_B28_1 = L_90;
		if (!L_92)
		{
			G_B29_0 = _stringLiteral3373707;
			G_B29_1 = L_90;
			goto IL_0274;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_93 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B30_0 = L_93;
		G_B30_1 = G_B28_0;
		G_B30_2 = G_B28_1;
		goto IL_027f;
	}

IL_0274:
	{
		FsmString_t952858651 * L_94 = __this->get_id_18();
		NullCheck(L_94);
		String_t* L_95 = FsmString_get_Value_m872383149(L_94, /*hidden argument*/NULL);
		G_B30_0 = L_95;
		G_B30_1 = G_B29_0;
		G_B30_2 = G_B29_1;
	}

IL_027f:
	{
		NullCheck(G_B30_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B30_2, G_B30_1, G_B30_0);
		Hashtable_t1407064410 * L_96 = V_2;
		int32_t L_97 = __this->get_axis_31();
		G_B31_0 = _stringLiteral3008417;
		G_B31_1 = L_96;
		if (L_97)
		{
			G_B32_0 = _stringLiteral3008417;
			G_B32_1 = L_96;
			goto IL_029f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_98 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B33_0 = L_98;
		G_B33_1 = G_B31_0;
		G_B33_2 = G_B31_1;
		goto IL_02b9;
	}

IL_029f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_99 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(AxisRestriction_t3260241011_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_100 = __this->get_axis_31();
		int32_t L_101 = L_100;
		Il2CppObject * L_102 = Box(AxisRestriction_t3260241011_il2cpp_TypeInfo_var, &L_101);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		String_t* L_103 = Enum_GetName_m3333121265(NULL /*static, unused*/, L_99, L_102, /*hidden argument*/NULL);
		G_B33_0 = L_103;
		G_B33_1 = G_B32_0;
		G_B33_2 = G_B32_1;
	}

IL_02b9:
	{
		NullCheck(G_B33_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B33_2, G_B33_1, G_B33_0);
		FsmBool_t1075959796 * L_104 = __this->get_orientToPath_27();
		NullCheck(L_104);
		bool L_105 = NamedVariable_get_IsNone_m281035543(L_104, /*hidden argument*/NULL);
		if (L_105)
		{
			goto IL_02e9;
		}
	}
	{
		Hashtable_t1407064410 * L_106 = V_2;
		FsmBool_t1075959796 * L_107 = __this->get_orientToPath_27();
		NullCheck(L_107);
		bool L_108 = FsmBool_get_Value_m3101329097(L_107, /*hidden argument*/NULL);
		bool L_109 = L_108;
		Il2CppObject * L_110 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_109);
		NullCheck(L_106);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_106, _stringLiteral3159645605, L_110);
	}

IL_02e9:
	{
		FsmGameObject_t1697147867 * L_111 = __this->get_lookAtObject_28();
		NullCheck(L_111);
		bool L_112 = NamedVariable_get_IsNone_m281035543(L_111, /*hidden argument*/NULL);
		if (L_112)
		{
			goto IL_035d;
		}
	}
	{
		Hashtable_t1407064410 * L_113 = V_2;
		FsmVector3_t533912882 * L_114 = __this->get_lookAtVector_29();
		NullCheck(L_114);
		bool L_115 = NamedVariable_get_IsNone_m281035543(L_114, /*hidden argument*/NULL);
		G_B37_0 = _stringLiteral1258521840;
		G_B37_1 = L_113;
		if (!L_115)
		{
			G_B38_0 = _stringLiteral1258521840;
			G_B38_1 = L_113;
			goto IL_0329;
		}
	}
	{
		FsmGameObject_t1697147867 * L_116 = __this->get_lookAtObject_28();
		NullCheck(L_116);
		GameObject_t3674682005 * L_117 = FsmGameObject_get_Value_m673294275(L_116, /*hidden argument*/NULL);
		NullCheck(L_117);
		Transform_t1659122786 * L_118 = GameObject_get_transform_m1278640159(L_117, /*hidden argument*/NULL);
		NullCheck(L_118);
		Vector3_t4282066566  L_119 = Transform_get_position_m2211398607(L_118, /*hidden argument*/NULL);
		G_B39_0 = L_119;
		G_B39_1 = G_B37_0;
		G_B39_2 = G_B37_1;
		goto IL_034e;
	}

IL_0329:
	{
		FsmGameObject_t1697147867 * L_120 = __this->get_lookAtObject_28();
		NullCheck(L_120);
		GameObject_t3674682005 * L_121 = FsmGameObject_get_Value_m673294275(L_120, /*hidden argument*/NULL);
		NullCheck(L_121);
		Transform_t1659122786 * L_122 = GameObject_get_transform_m1278640159(L_121, /*hidden argument*/NULL);
		NullCheck(L_122);
		Vector3_t4282066566  L_123 = Transform_get_position_m2211398607(L_122, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_124 = __this->get_lookAtVector_29();
		NullCheck(L_124);
		Vector3_t4282066566  L_125 = FsmVector3_get_Value_m2779135117(L_124, /*hidden argument*/NULL);
		Vector3_t4282066566  L_126 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_123, L_125, /*hidden argument*/NULL);
		G_B39_0 = L_126;
		G_B39_1 = G_B38_0;
		G_B39_2 = G_B38_1;
	}

IL_034e:
	{
		Vector3_t4282066566  L_127 = G_B39_0;
		Il2CppObject * L_128 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_127);
		NullCheck(G_B39_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B39_2, G_B39_1, L_128);
		goto IL_0388;
	}

IL_035d:
	{
		FsmVector3_t533912882 * L_129 = __this->get_lookAtVector_29();
		NullCheck(L_129);
		bool L_130 = NamedVariable_get_IsNone_m281035543(L_129, /*hidden argument*/NULL);
		if (L_130)
		{
			goto IL_0388;
		}
	}
	{
		Hashtable_t1407064410 * L_131 = V_2;
		FsmVector3_t533912882 * L_132 = __this->get_lookAtVector_29();
		NullCheck(L_132);
		Vector3_t4282066566  L_133 = FsmVector3_get_Value_m2779135117(L_132, /*hidden argument*/NULL);
		Vector3_t4282066566  L_134 = L_133;
		Il2CppObject * L_135 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_134);
		NullCheck(L_131);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_131, _stringLiteral1258521840, L_135);
	}

IL_0388:
	{
		FsmGameObject_t1697147867 * L_136 = __this->get_lookAtObject_28();
		NullCheck(L_136);
		bool L_137 = NamedVariable_get_IsNone_m281035543(L_136, /*hidden argument*/NULL);
		if (!L_137)
		{
			goto IL_03a8;
		}
	}
	{
		FsmVector3_t533912882 * L_138 = __this->get_lookAtVector_29();
		NullCheck(L_138);
		bool L_139 = NamedVariable_get_IsNone_m281035543(L_138, /*hidden argument*/NULL);
		if (L_139)
		{
			goto IL_03dd;
		}
	}

IL_03a8:
	{
		Hashtable_t1407064410 * L_140 = V_2;
		FsmFloat_t2134102846 * L_141 = __this->get_lookTime_30();
		NullCheck(L_141);
		bool L_142 = NamedVariable_get_IsNone_m281035543(L_141, /*hidden argument*/NULL);
		G_B45_0 = _stringLiteral2253828588;
		G_B45_1 = L_140;
		if (!L_142)
		{
			G_B46_0 = _stringLiteral2253828588;
			G_B46_1 = L_140;
			goto IL_03c8;
		}
	}
	{
		G_B47_0 = (0.0f);
		G_B47_1 = G_B45_0;
		G_B47_2 = G_B45_1;
		goto IL_03d3;
	}

IL_03c8:
	{
		FsmFloat_t2134102846 * L_143 = __this->get_lookTime_30();
		NullCheck(L_143);
		float L_144 = FsmFloat_get_Value_m4137923823(L_143, /*hidden argument*/NULL);
		G_B47_0 = L_144;
		G_B47_1 = G_B46_0;
		G_B47_2 = G_B46_1;
	}

IL_03d3:
	{
		float L_145 = G_B47_0;
		Il2CppObject * L_146 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_145);
		NullCheck(G_B47_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B47_2, G_B47_1, L_146);
	}

IL_03dd:
	{
		((iTweenFsmAction_t410382178 *)__this)->set_itweenType_15(_stringLiteral3357649);
		GameObject_t3674682005 * L_147 = V_0;
		Hashtable_t1407064410 * L_148 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		iTween_MoveFrom_m3348018144(NULL /*static, unused*/, L_147, L_148, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveTo::.ctor()
extern "C"  void iTweenMoveTo__ctor_m4177463504 (iTweenMoveTo_t469657174 * __this, const MethodInfo* method)
{
	{
		__this->set_easeType_25(((int32_t)21));
		iTweenFsmAction__ctor_m2960815636(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveTo::OnDrawGizmos()
extern Il2CppClass* Vector3U5BU5D_t215400611_il2cpp_TypeInfo_var;
extern Il2CppClass* iTween_t3087282050_il2cpp_TypeInfo_var;
extern const uint32_t iTweenMoveTo_OnDrawGizmos_m310804048_MetadataUsageId;
extern "C"  void iTweenMoveTo_OnDrawGizmos_m310804048 (iTweenMoveTo_t469657174 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenMoveTo_OnDrawGizmos_m310804048_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Vector3_t4282066566 * G_B5_0 = NULL;
	Vector3_t4282066566 * G_B4_0 = NULL;
	Vector3_t4282066566  G_B6_0;
	memset(&G_B6_0, 0, sizeof(G_B6_0));
	Vector3_t4282066566 * G_B6_1 = NULL;
	Vector3_t4282066566 * G_B10_0 = NULL;
	Vector3_t4282066566 * G_B9_0 = NULL;
	Vector3_t4282066566  G_B11_0;
	memset(&G_B11_0, 0, sizeof(G_B11_0));
	Vector3_t4282066566 * G_B11_1 = NULL;
	Vector3_t4282066566  G_B14_0;
	memset(&G_B14_0, 0, sizeof(G_B14_0));
	Vector3_t4282066566 * G_B14_1 = NULL;
	Vector3_t4282066566  G_B13_0;
	memset(&G_B13_0, 0, sizeof(G_B13_0));
	Vector3_t4282066566 * G_B13_1 = NULL;
	Vector3_t4282066566  G_B15_0;
	memset(&G_B15_0, 0, sizeof(G_B15_0));
	Vector3_t4282066566  G_B15_1;
	memset(&G_B15_1, 0, sizeof(G_B15_1));
	Vector3_t4282066566 * G_B15_2 = NULL;
	{
		FsmGameObjectU5BU5D_t1706220122* L_0 = __this->get_transforms_34();
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) < ((int32_t)2)))
		{
			goto IL_0148;
		}
	}
	{
		FsmGameObjectU5BU5D_t1706220122* L_1 = __this->get_transforms_34();
		NullCheck(L_1);
		__this->set_tempVct3_37(((Vector3U5BU5D_t215400611*)SZArrayNew(Vector3U5BU5D_t215400611_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))))));
		V_0 = 0;
		goto IL_012a;
	}

IL_0028:
	{
		FsmGameObjectU5BU5D_t1706220122* L_2 = __this->get_transforms_34();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		FsmGameObject_t1697147867 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_5);
		bool L_6 = NamedVariable_get_IsNone_m281035543(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0079;
		}
	}
	{
		Vector3U5BU5D_t215400611* L_7 = __this->get_tempVct3_37();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		FsmVector3U5BU5D_t607182279* L_9 = __this->get_vectors_35();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		FsmVector3_t533912882 * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_12);
		bool L_13 = NamedVariable_get_IsNone_m281035543(L_12, /*hidden argument*/NULL);
		G_B4_0 = ((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8)));
		if (!L_13)
		{
			G_B5_0 = ((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8)));
			goto IL_0062;
		}
	}
	{
		Vector3_t4282066566  L_14 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B6_0 = L_14;
		G_B6_1 = G_B4_0;
		goto IL_006f;
	}

IL_0062:
	{
		FsmVector3U5BU5D_t607182279* L_15 = __this->get_vectors_35();
		int32_t L_16 = V_0;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		FsmVector3_t533912882 * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		NullCheck(L_18);
		Vector3_t4282066566  L_19 = FsmVector3_get_Value_m2779135117(L_18, /*hidden argument*/NULL);
		G_B6_0 = L_19;
		G_B6_1 = G_B5_0;
	}

IL_006f:
	{
		(*(Vector3_t4282066566 *)G_B6_1) = G_B6_0;
		goto IL_0126;
	}

IL_0079:
	{
		FsmGameObjectU5BU5D_t1706220122* L_20 = __this->get_transforms_34();
		int32_t L_21 = V_0;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		FsmGameObject_t1697147867 * L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		NullCheck(L_23);
		GameObject_t3674682005 * L_24 = FsmGameObject_get_Value_m673294275(L_23, /*hidden argument*/NULL);
		bool L_25 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_24, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00d0;
		}
	}
	{
		Vector3U5BU5D_t215400611* L_26 = __this->get_tempVct3_37();
		int32_t L_27 = V_0;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		FsmVector3U5BU5D_t607182279* L_28 = __this->get_vectors_35();
		int32_t L_29 = V_0;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, L_29);
		int32_t L_30 = L_29;
		FsmVector3_t533912882 * L_31 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_30));
		NullCheck(L_31);
		bool L_32 = NamedVariable_get_IsNone_m281035543(L_31, /*hidden argument*/NULL);
		G_B9_0 = ((L_26)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_27)));
		if (!L_32)
		{
			G_B10_0 = ((L_26)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_27)));
			goto IL_00b9;
		}
	}
	{
		Vector3_t4282066566  L_33 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B11_0 = L_33;
		G_B11_1 = G_B9_0;
		goto IL_00c6;
	}

IL_00b9:
	{
		FsmVector3U5BU5D_t607182279* L_34 = __this->get_vectors_35();
		int32_t L_35 = V_0;
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, L_35);
		int32_t L_36 = L_35;
		FsmVector3_t533912882 * L_37 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_36));
		NullCheck(L_37);
		Vector3_t4282066566  L_38 = FsmVector3_get_Value_m2779135117(L_37, /*hidden argument*/NULL);
		G_B11_0 = L_38;
		G_B11_1 = G_B10_0;
	}

IL_00c6:
	{
		(*(Vector3_t4282066566 *)G_B11_1) = G_B11_0;
		goto IL_0126;
	}

IL_00d0:
	{
		Vector3U5BU5D_t215400611* L_39 = __this->get_tempVct3_37();
		int32_t L_40 = V_0;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, L_40);
		FsmGameObjectU5BU5D_t1706220122* L_41 = __this->get_transforms_34();
		int32_t L_42 = V_0;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, L_42);
		int32_t L_43 = L_42;
		FsmGameObject_t1697147867 * L_44 = (L_41)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		NullCheck(L_44);
		GameObject_t3674682005 * L_45 = FsmGameObject_get_Value_m673294275(L_44, /*hidden argument*/NULL);
		NullCheck(L_45);
		Transform_t1659122786 * L_46 = GameObject_get_transform_m1278640159(L_45, /*hidden argument*/NULL);
		NullCheck(L_46);
		Vector3_t4282066566  L_47 = Transform_get_position_m2211398607(L_46, /*hidden argument*/NULL);
		FsmVector3U5BU5D_t607182279* L_48 = __this->get_vectors_35();
		int32_t L_49 = V_0;
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, L_49);
		int32_t L_50 = L_49;
		FsmVector3_t533912882 * L_51 = (L_48)->GetAt(static_cast<il2cpp_array_size_t>(L_50));
		NullCheck(L_51);
		bool L_52 = NamedVariable_get_IsNone_m281035543(L_51, /*hidden argument*/NULL);
		G_B13_0 = L_47;
		G_B13_1 = ((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_40)));
		if (!L_52)
		{
			G_B14_0 = L_47;
			G_B14_1 = ((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_40)));
			goto IL_010f;
		}
	}
	{
		Vector3_t4282066566  L_53 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B15_0 = L_53;
		G_B15_1 = G_B13_0;
		G_B15_2 = G_B13_1;
		goto IL_011c;
	}

IL_010f:
	{
		FsmVector3U5BU5D_t607182279* L_54 = __this->get_vectors_35();
		int32_t L_55 = V_0;
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, L_55);
		int32_t L_56 = L_55;
		FsmVector3_t533912882 * L_57 = (L_54)->GetAt(static_cast<il2cpp_array_size_t>(L_56));
		NullCheck(L_57);
		Vector3_t4282066566  L_58 = FsmVector3_get_Value_m2779135117(L_57, /*hidden argument*/NULL);
		G_B15_0 = L_58;
		G_B15_1 = G_B14_0;
		G_B15_2 = G_B14_1;
	}

IL_011c:
	{
		Vector3_t4282066566  L_59 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, G_B15_1, G_B15_0, /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)G_B15_2) = L_59;
	}

IL_0126:
	{
		int32_t L_60 = V_0;
		V_0 = ((int32_t)((int32_t)L_60+(int32_t)1));
	}

IL_012a:
	{
		int32_t L_61 = V_0;
		FsmGameObjectU5BU5D_t1706220122* L_62 = __this->get_transforms_34();
		NullCheck(L_62);
		if ((((int32_t)L_61) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_62)->max_length)))))))
		{
			goto IL_0028;
		}
	}
	{
		Vector3U5BU5D_t215400611* L_63 = __this->get_tempVct3_37();
		Color_t4194546905  L_64 = Color_get_yellow_m599454500(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		iTween_DrawPathGizmos_m704379850(NULL /*static, unused*/, L_63, L_64, /*hidden argument*/NULL);
	}

IL_0148:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveTo::Reset()
extern Il2CppClass* FsmString_t952858651_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmGameObject_t1697147867_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmFloat_t2134102846_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmBool_t1075959796_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmGameObjectU5BU5D_t1706220122_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmVector3U5BU5D_t607182279_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3U5BU5D_t215400611_il2cpp_TypeInfo_var;
extern const uint32_t iTweenMoveTo_Reset_m1823896445_MetadataUsageId;
extern "C"  void iTweenMoveTo_Reset_m1823896445 (iTweenMoveTo_t469657174 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenMoveTo_Reset_m1823896445_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmString_t952858651 * V_0 = NULL;
	FsmGameObject_t1697147867 * V_1 = NULL;
	FsmVector3_t533912882 * V_2 = NULL;
	FsmFloat_t2134102846 * V_3 = NULL;
	FsmBool_t1075959796 * V_4 = NULL;
	{
		iTweenFsmAction_Reset_m607248577(__this, /*hidden argument*/NULL);
		FsmString_t952858651 * L_0 = (FsmString_t952858651 *)il2cpp_codegen_object_new(FsmString_t952858651_il2cpp_TypeInfo_var);
		FsmString__ctor_m2679108260(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmString_t952858651 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmString_t952858651 * L_2 = V_0;
		__this->set_id_18(L_2);
		FsmGameObject_t1697147867 * L_3 = (FsmGameObject_t1697147867 *)il2cpp_codegen_object_new(FsmGameObject_t1697147867_il2cpp_TypeInfo_var);
		FsmGameObject__ctor_m1048269796(L_3, /*hidden argument*/NULL);
		V_1 = L_3;
		FsmGameObject_t1697147867 * L_4 = V_1;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_5 = V_1;
		__this->set_transformPosition_19(L_5);
		FsmVector3_t533912882 * L_6 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_6, /*hidden argument*/NULL);
		V_2 = L_6;
		FsmVector3_t533912882 * L_7 = V_2;
		NullCheck(L_7);
		NamedVariable_set_UseVariable_m4266138971(L_7, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_8 = V_2;
		__this->set_vectorPosition_20(L_8);
		FsmFloat_t2134102846 * L_9 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_time_21(L_9);
		FsmFloat_t2134102846 * L_10 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_delay_22(L_10);
		__this->set_loopType_26(0);
		FsmFloat_t2134102846 * L_11 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_11, /*hidden argument*/NULL);
		V_3 = L_11;
		FsmFloat_t2134102846 * L_12 = V_3;
		NullCheck(L_12);
		NamedVariable_set_UseVariable_m4266138971(L_12, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_13 = V_3;
		__this->set_speed_23(L_13);
		__this->set_space_24(0);
		FsmBool_t1075959796 * L_14 = (FsmBool_t1075959796 *)il2cpp_codegen_object_new(FsmBool_t1075959796_il2cpp_TypeInfo_var);
		FsmBool__ctor_m1553455211(L_14, /*hidden argument*/NULL);
		V_4 = L_14;
		FsmBool_t1075959796 * L_15 = V_4;
		NullCheck(L_15);
		FsmBool_set_Value_m1126216340(L_15, (bool)1, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_16 = V_4;
		__this->set_orientToPath_27(L_16);
		FsmGameObject_t1697147867 * L_17 = (FsmGameObject_t1697147867 *)il2cpp_codegen_object_new(FsmGameObject_t1697147867_il2cpp_TypeInfo_var);
		FsmGameObject__ctor_m1048269796(L_17, /*hidden argument*/NULL);
		V_1 = L_17;
		FsmGameObject_t1697147867 * L_18 = V_1;
		NullCheck(L_18);
		NamedVariable_set_UseVariable_m4266138971(L_18, (bool)1, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_19 = V_1;
		__this->set_lookAtObject_28(L_19);
		FsmVector3_t533912882 * L_20 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_20, /*hidden argument*/NULL);
		V_2 = L_20;
		FsmVector3_t533912882 * L_21 = V_2;
		NullCheck(L_21);
		NamedVariable_set_UseVariable_m4266138971(L_21, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_22 = V_2;
		__this->set_lookAtVector_29(L_22);
		FsmFloat_t2134102846 * L_23 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_23, /*hidden argument*/NULL);
		V_3 = L_23;
		FsmFloat_t2134102846 * L_24 = V_3;
		NullCheck(L_24);
		NamedVariable_set_UseVariable_m4266138971(L_24, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_25 = V_3;
		__this->set_lookTime_30(L_25);
		FsmBool_t1075959796 * L_26 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_moveToPath_32(L_26);
		FsmFloat_t2134102846 * L_27 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_27, /*hidden argument*/NULL);
		V_3 = L_27;
		FsmFloat_t2134102846 * L_28 = V_3;
		NullCheck(L_28);
		NamedVariable_set_UseVariable_m4266138971(L_28, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_29 = V_3;
		__this->set_lookAhead_33(L_29);
		__this->set_transforms_34(((FsmGameObjectU5BU5D_t1706220122*)SZArrayNew(FsmGameObjectU5BU5D_t1706220122_il2cpp_TypeInfo_var, (uint32_t)0)));
		__this->set_vectors_35(((FsmVector3U5BU5D_t607182279*)SZArrayNew(FsmVector3U5BU5D_t607182279_il2cpp_TypeInfo_var, (uint32_t)0)));
		__this->set_tempVct3_37(((Vector3U5BU5D_t215400611*)SZArrayNew(Vector3U5BU5D_t215400611_il2cpp_TypeInfo_var, (uint32_t)0)));
		__this->set_axis_31(0);
		FsmBool_t1075959796 * L_30 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_reverse_36(L_30);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveTo::OnEnter()
extern "C"  void iTweenMoveTo_OnEnter_m2434701479 (iTweenMoveTo_t469657174 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnEnteriTween_m3024539459(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_loopType_26();
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		iTweenFsmAction_IsLoop_m2539277653(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_001e:
	{
		iTweenMoveTo_DoiTween_m3912434049(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveTo::OnExit()
extern "C"  void iTweenMoveTo_OnExit_m2165668369 (iTweenMoveTo_t469657174 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnExitiTween_m1981625761(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveTo::DoiTween()
extern const Il2CppType* AxisRestriction_t3260241011_0_0_0_var;
extern Il2CppClass* Hashtable_t1407064410_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* EaseType_t2734598229_il2cpp_TypeInfo_var;
extern Il2CppClass* LoopType_t1485160459_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* AxisRestriction_t3260241011_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2862688501_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3U5BU5D_t215400611_il2cpp_TypeInfo_var;
extern Il2CppClass* iTween_t3087282050_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral747804969;
extern Il2CppCodeGenString* _stringLiteral3560141;
extern Il2CppCodeGenString* _stringLiteral109641799;
extern Il2CppCodeGenString* _stringLiteral95467907;
extern Il2CppCodeGenString* _stringLiteral3507899432;
extern Il2CppCodeGenString* _stringLiteral2258461662;
extern Il2CppCodeGenString* _stringLiteral2105864216;
extern Il2CppCodeGenString* _stringLiteral338257242;
extern Il2CppCodeGenString* _stringLiteral3696326558;
extern Il2CppCodeGenString* _stringLiteral2987624931;
extern Il2CppCodeGenString* _stringLiteral647687137;
extern Il2CppCodeGenString* _stringLiteral2052738345;
extern Il2CppCodeGenString* _stringLiteral1067506955;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral2094103681;
extern Il2CppCodeGenString* _stringLiteral3008417;
extern Il2CppCodeGenString* _stringLiteral3159645605;
extern Il2CppCodeGenString* _stringLiteral1258521840;
extern Il2CppCodeGenString* _stringLiteral2253828588;
extern Il2CppCodeGenString* _stringLiteral3433509;
extern Il2CppCodeGenString* _stringLiteral3654555697;
extern Il2CppCodeGenString* _stringLiteral1131625090;
extern Il2CppCodeGenString* _stringLiteral3357649;
extern const uint32_t iTweenMoveTo_DoiTween_m3912434049_MetadataUsageId;
extern "C"  void iTweenMoveTo_DoiTween_m3912434049 (iTweenMoveTo_t469657174 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenMoveTo_DoiTween_m3912434049_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Hashtable_t1407064410 * V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Vector3_t4282066566  G_B5_0;
	memset(&G_B5_0, 0, sizeof(G_B5_0));
	Vector3_t4282066566  G_B11_0;
	memset(&G_B11_0, 0, sizeof(G_B11_0));
	Hashtable_t1407064410 * G_B14_0 = NULL;
	Hashtable_t1407064410 * G_B13_0 = NULL;
	String_t* G_B15_0 = NULL;
	Hashtable_t1407064410 * G_B15_1 = NULL;
	String_t* G_B20_0 = NULL;
	Hashtable_t1407064410 * G_B20_1 = NULL;
	String_t* G_B16_0 = NULL;
	Hashtable_t1407064410 * G_B16_1 = NULL;
	String_t* G_B18_0 = NULL;
	Hashtable_t1407064410 * G_B18_1 = NULL;
	String_t* G_B17_0 = NULL;
	Hashtable_t1407064410 * G_B17_1 = NULL;
	float G_B19_0 = 0.0f;
	String_t* G_B19_1 = NULL;
	Hashtable_t1407064410 * G_B19_2 = NULL;
	float G_B21_0 = 0.0f;
	String_t* G_B21_1 = NULL;
	Hashtable_t1407064410 * G_B21_2 = NULL;
	String_t* G_B23_0 = NULL;
	Hashtable_t1407064410 * G_B23_1 = NULL;
	String_t* G_B22_0 = NULL;
	Hashtable_t1407064410 * G_B22_1 = NULL;
	float G_B24_0 = 0.0f;
	String_t* G_B24_1 = NULL;
	Hashtable_t1407064410 * G_B24_2 = NULL;
	String_t* G_B26_0 = NULL;
	Hashtable_t1407064410 * G_B26_1 = NULL;
	String_t* G_B25_0 = NULL;
	Hashtable_t1407064410 * G_B25_1 = NULL;
	int32_t G_B27_0 = 0;
	String_t* G_B27_1 = NULL;
	Hashtable_t1407064410 * G_B27_2 = NULL;
	String_t* G_B29_0 = NULL;
	Hashtable_t1407064410 * G_B29_1 = NULL;
	String_t* G_B28_0 = NULL;
	Hashtable_t1407064410 * G_B28_1 = NULL;
	String_t* G_B30_0 = NULL;
	String_t* G_B30_1 = NULL;
	Hashtable_t1407064410 * G_B30_2 = NULL;
	String_t* G_B32_0 = NULL;
	Hashtable_t1407064410 * G_B32_1 = NULL;
	String_t* G_B31_0 = NULL;
	Hashtable_t1407064410 * G_B31_1 = NULL;
	String_t* G_B33_0 = NULL;
	String_t* G_B33_1 = NULL;
	Hashtable_t1407064410 * G_B33_2 = NULL;
	String_t* G_B38_0 = NULL;
	Hashtable_t1407064410 * G_B38_1 = NULL;
	String_t* G_B37_0 = NULL;
	Hashtable_t1407064410 * G_B37_1 = NULL;
	Vector3_t4282066566  G_B39_0;
	memset(&G_B39_0, 0, sizeof(G_B39_0));
	String_t* G_B39_1 = NULL;
	Hashtable_t1407064410 * G_B39_2 = NULL;
	String_t* G_B46_0 = NULL;
	Hashtable_t1407064410 * G_B46_1 = NULL;
	String_t* G_B45_0 = NULL;
	Hashtable_t1407064410 * G_B45_1 = NULL;
	float G_B47_0 = 0.0f;
	String_t* G_B47_1 = NULL;
	Hashtable_t1407064410 * G_B47_2 = NULL;
	int32_t G_B52_0 = 0;
	Vector3_t4282066566 * G_B57_0 = NULL;
	Vector3_t4282066566 * G_B56_0 = NULL;
	Vector3_t4282066566  G_B58_0;
	memset(&G_B58_0, 0, sizeof(G_B58_0));
	Vector3_t4282066566 * G_B58_1 = NULL;
	Vector3_t4282066566 * G_B62_0 = NULL;
	Vector3_t4282066566 * G_B61_0 = NULL;
	Vector3_t4282066566  G_B63_0;
	memset(&G_B63_0, 0, sizeof(G_B63_0));
	Vector3_t4282066566 * G_B63_1 = NULL;
	Vector3_t4282066566 * G_B66_0 = NULL;
	Vector3_t4282066566 * G_B65_0 = NULL;
	Vector3_t4282066566  G_B67_0;
	memset(&G_B67_0, 0, sizeof(G_B67_0));
	Vector3_t4282066566 * G_B67_1 = NULL;
	Vector3_t4282066566  G_B69_0;
	memset(&G_B69_0, 0, sizeof(G_B69_0));
	Vector3_t4282066566 * G_B69_1 = NULL;
	Vector3_t4282066566  G_B68_0;
	memset(&G_B68_0, 0, sizeof(G_B68_0));
	Vector3_t4282066566 * G_B68_1 = NULL;
	Vector3_t4282066566  G_B70_0;
	memset(&G_B70_0, 0, sizeof(G_B70_0));
	Vector3_t4282066566  G_B70_1;
	memset(&G_B70_1, 0, sizeof(G_B70_1));
	Vector3_t4282066566 * G_B70_2 = NULL;
	Vector3_t4282066566 * G_B78_0 = NULL;
	Vector3_t4282066566 * G_B77_0 = NULL;
	Vector3_t4282066566  G_B79_0;
	memset(&G_B79_0, 0, sizeof(G_B79_0));
	Vector3_t4282066566 * G_B79_1 = NULL;
	Vector3_t4282066566 * G_B83_0 = NULL;
	Vector3_t4282066566 * G_B82_0 = NULL;
	Vector3_t4282066566  G_B84_0;
	memset(&G_B84_0, 0, sizeof(G_B84_0));
	Vector3_t4282066566 * G_B84_1 = NULL;
	Vector3_t4282066566 * G_B87_0 = NULL;
	Vector3_t4282066566 * G_B86_0 = NULL;
	Vector3_t4282066566  G_B88_0;
	memset(&G_B88_0, 0, sizeof(G_B88_0));
	Vector3_t4282066566 * G_B88_1 = NULL;
	Vector3_t4282066566  G_B90_0;
	memset(&G_B90_0, 0, sizeof(G_B90_0));
	Vector3_t4282066566 * G_B90_1 = NULL;
	Vector3_t4282066566  G_B89_0;
	memset(&G_B89_0, 0, sizeof(G_B89_0));
	Vector3_t4282066566 * G_B89_1 = NULL;
	Vector3_t4282066566  G_B91_0;
	memset(&G_B91_0, 0, sizeof(G_B91_0));
	Vector3_t4282066566  G_B91_1;
	memset(&G_B91_1, 0, sizeof(G_B91_1));
	Vector3_t4282066566 * G_B91_2 = NULL;
	String_t* G_B96_0 = NULL;
	Hashtable_t1407064410 * G_B96_1 = NULL;
	String_t* G_B95_0 = NULL;
	Hashtable_t1407064410 * G_B95_1 = NULL;
	int32_t G_B97_0 = 0;
	String_t* G_B97_1 = NULL;
	Hashtable_t1407064410 * G_B97_2 = NULL;
	String_t* G_B99_0 = NULL;
	Hashtable_t1407064410 * G_B99_1 = NULL;
	String_t* G_B98_0 = NULL;
	Hashtable_t1407064410 * G_B98_1 = NULL;
	float G_B100_0 = 0.0f;
	String_t* G_B100_1 = NULL;
	Hashtable_t1407064410 * G_B100_2 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_17();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		FsmVector3_t533912882 * L_5 = __this->get_vectorPosition_20();
		NullCheck(L_5);
		bool L_6 = NamedVariable_get_IsNone_m281035543(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0039;
		}
	}
	{
		Vector3_t4282066566  L_7 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B5_0 = L_7;
		goto IL_0044;
	}

IL_0039:
	{
		FsmVector3_t533912882 * L_8 = __this->get_vectorPosition_20();
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = FsmVector3_get_Value_m2779135117(L_8, /*hidden argument*/NULL);
		G_B5_0 = L_9;
	}

IL_0044:
	{
		V_1 = G_B5_0;
		FsmGameObject_t1697147867 * L_10 = __this->get_transformPosition_19();
		NullCheck(L_10);
		bool L_11 = NamedVariable_get_IsNone_m281035543(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_00d7;
		}
	}
	{
		FsmGameObject_t1697147867 * L_12 = __this->get_transformPosition_19();
		NullCheck(L_12);
		GameObject_t3674682005 * L_13 = FsmGameObject_get_Value_m673294275(L_12, /*hidden argument*/NULL);
		bool L_14 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00d7;
		}
	}
	{
		int32_t L_15 = __this->get_space_24();
		if (!L_15)
		{
			goto IL_008b;
		}
	}
	{
		GameObject_t3674682005 * L_16 = V_0;
		NullCheck(L_16);
		Transform_t1659122786 * L_17 = GameObject_get_transform_m1278640159(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_t1659122786 * L_18 = Transform_get_parent_m2236876972(L_17, /*hidden argument*/NULL);
		bool L_19 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_18, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00ab;
		}
	}

IL_008b:
	{
		FsmGameObject_t1697147867 * L_20 = __this->get_transformPosition_19();
		NullCheck(L_20);
		GameObject_t3674682005 * L_21 = FsmGameObject_get_Value_m673294275(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_t1659122786 * L_22 = GameObject_get_transform_m1278640159(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		Vector3_t4282066566  L_23 = Transform_get_position_m2211398607(L_22, /*hidden argument*/NULL);
		Vector3_t4282066566  L_24 = V_1;
		Vector3_t4282066566  L_25 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		G_B11_0 = L_25;
		goto IL_00d6;
	}

IL_00ab:
	{
		GameObject_t3674682005 * L_26 = V_0;
		NullCheck(L_26);
		Transform_t1659122786 * L_27 = GameObject_get_transform_m1278640159(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		Transform_t1659122786 * L_28 = Transform_get_parent_m2236876972(L_27, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_29 = __this->get_transformPosition_19();
		NullCheck(L_29);
		GameObject_t3674682005 * L_30 = FsmGameObject_get_Value_m673294275(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		Transform_t1659122786 * L_31 = GameObject_get_transform_m1278640159(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		Vector3_t4282066566  L_32 = Transform_get_position_m2211398607(L_31, /*hidden argument*/NULL);
		NullCheck(L_28);
		Vector3_t4282066566  L_33 = Transform_InverseTransformPoint_m1626812000(L_28, L_32, /*hidden argument*/NULL);
		Vector3_t4282066566  L_34 = V_1;
		Vector3_t4282066566  L_35 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_33, L_34, /*hidden argument*/NULL);
		G_B11_0 = L_35;
	}

IL_00d6:
	{
		V_1 = G_B11_0;
	}

IL_00d7:
	{
		Hashtable_t1407064410 * L_36 = (Hashtable_t1407064410 *)il2cpp_codegen_object_new(Hashtable_t1407064410_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1514037738(L_36, /*hidden argument*/NULL);
		V_2 = L_36;
		Hashtable_t1407064410 * L_37 = V_2;
		Vector3_t4282066566  L_38 = V_1;
		Vector3_t4282066566  L_39 = L_38;
		Il2CppObject * L_40 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_39);
		NullCheck(L_37);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_37, _stringLiteral747804969, L_40);
		Hashtable_t1407064410 * L_41 = V_2;
		FsmFloat_t2134102846 * L_42 = __this->get_speed_23();
		NullCheck(L_42);
		bool L_43 = NamedVariable_get_IsNone_m281035543(L_42, /*hidden argument*/NULL);
		G_B13_0 = L_41;
		if (!L_43)
		{
			G_B14_0 = L_41;
			goto IL_0109;
		}
	}
	{
		G_B15_0 = _stringLiteral3560141;
		G_B15_1 = G_B13_0;
		goto IL_010e;
	}

IL_0109:
	{
		G_B15_0 = _stringLiteral109641799;
		G_B15_1 = G_B14_0;
	}

IL_010e:
	{
		FsmFloat_t2134102846 * L_44 = __this->get_speed_23();
		NullCheck(L_44);
		bool L_45 = NamedVariable_get_IsNone_m281035543(L_44, /*hidden argument*/NULL);
		G_B16_0 = G_B15_0;
		G_B16_1 = G_B15_1;
		if (!L_45)
		{
			G_B20_0 = G_B15_0;
			G_B20_1 = G_B15_1;
			goto IL_0148;
		}
	}
	{
		FsmFloat_t2134102846 * L_46 = __this->get_time_21();
		NullCheck(L_46);
		bool L_47 = NamedVariable_get_IsNone_m281035543(L_46, /*hidden argument*/NULL);
		G_B17_0 = G_B16_0;
		G_B17_1 = G_B16_1;
		if (!L_47)
		{
			G_B18_0 = G_B16_0;
			G_B18_1 = G_B16_1;
			goto IL_0138;
		}
	}
	{
		G_B19_0 = (1.0f);
		G_B19_1 = G_B17_0;
		G_B19_2 = G_B17_1;
		goto IL_0143;
	}

IL_0138:
	{
		FsmFloat_t2134102846 * L_48 = __this->get_time_21();
		NullCheck(L_48);
		float L_49 = FsmFloat_get_Value_m4137923823(L_48, /*hidden argument*/NULL);
		G_B19_0 = L_49;
		G_B19_1 = G_B18_0;
		G_B19_2 = G_B18_1;
	}

IL_0143:
	{
		G_B21_0 = G_B19_0;
		G_B21_1 = G_B19_1;
		G_B21_2 = G_B19_2;
		goto IL_0153;
	}

IL_0148:
	{
		FsmFloat_t2134102846 * L_50 = __this->get_speed_23();
		NullCheck(L_50);
		float L_51 = FsmFloat_get_Value_m4137923823(L_50, /*hidden argument*/NULL);
		G_B21_0 = L_51;
		G_B21_1 = G_B20_0;
		G_B21_2 = G_B20_1;
	}

IL_0153:
	{
		float L_52 = G_B21_0;
		Il2CppObject * L_53 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_52);
		NullCheck(G_B21_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B21_2, G_B21_1, L_53);
		Hashtable_t1407064410 * L_54 = V_2;
		FsmFloat_t2134102846 * L_55 = __this->get_delay_22();
		NullCheck(L_55);
		bool L_56 = NamedVariable_get_IsNone_m281035543(L_55, /*hidden argument*/NULL);
		G_B22_0 = _stringLiteral95467907;
		G_B22_1 = L_54;
		if (!L_56)
		{
			G_B23_0 = _stringLiteral95467907;
			G_B23_1 = L_54;
			goto IL_017d;
		}
	}
	{
		G_B24_0 = (0.0f);
		G_B24_1 = G_B22_0;
		G_B24_2 = G_B22_1;
		goto IL_0188;
	}

IL_017d:
	{
		FsmFloat_t2134102846 * L_57 = __this->get_delay_22();
		NullCheck(L_57);
		float L_58 = FsmFloat_get_Value_m4137923823(L_57, /*hidden argument*/NULL);
		G_B24_0 = L_58;
		G_B24_1 = G_B23_0;
		G_B24_2 = G_B23_1;
	}

IL_0188:
	{
		float L_59 = G_B24_0;
		Il2CppObject * L_60 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_59);
		NullCheck(G_B24_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B24_2, G_B24_1, L_60);
		Hashtable_t1407064410 * L_61 = V_2;
		int32_t L_62 = __this->get_easeType_25();
		int32_t L_63 = L_62;
		Il2CppObject * L_64 = Box(EaseType_t2734598229_il2cpp_TypeInfo_var, &L_63);
		NullCheck(L_61);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_61, _stringLiteral3507899432, L_64);
		Hashtable_t1407064410 * L_65 = V_2;
		int32_t L_66 = __this->get_loopType_26();
		int32_t L_67 = L_66;
		Il2CppObject * L_68 = Box(LoopType_t1485160459_il2cpp_TypeInfo_var, &L_67);
		NullCheck(L_65);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_65, _stringLiteral2258461662, L_68);
		Hashtable_t1407064410 * L_69 = V_2;
		NullCheck(L_69);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_69, _stringLiteral2105864216, _stringLiteral338257242);
		Hashtable_t1407064410 * L_70 = V_2;
		int32_t L_71 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_72 = L_71;
		Il2CppObject * L_73 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_72);
		NullCheck(L_70);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_70, _stringLiteral3696326558, L_73);
		Hashtable_t1407064410 * L_74 = V_2;
		NullCheck(L_74);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_74, _stringLiteral2987624931, _stringLiteral647687137);
		Hashtable_t1407064410 * L_75 = V_2;
		int32_t L_76 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_77 = L_76;
		Il2CppObject * L_78 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_77);
		NullCheck(L_75);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_75, _stringLiteral2052738345, L_78);
		Hashtable_t1407064410 * L_79 = V_2;
		FsmBool_t1075959796 * L_80 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_80);
		bool L_81 = NamedVariable_get_IsNone_m281035543(L_80, /*hidden argument*/NULL);
		G_B25_0 = _stringLiteral1067506955;
		G_B25_1 = L_79;
		if (!L_81)
		{
			G_B26_0 = _stringLiteral1067506955;
			G_B26_1 = L_79;
			goto IL_0226;
		}
	}
	{
		G_B27_0 = 0;
		G_B27_1 = G_B25_0;
		G_B27_2 = G_B25_1;
		goto IL_0231;
	}

IL_0226:
	{
		FsmBool_t1075959796 * L_82 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_82);
		bool L_83 = FsmBool_get_Value_m3101329097(L_82, /*hidden argument*/NULL);
		G_B27_0 = ((int32_t)(L_83));
		G_B27_1 = G_B26_0;
		G_B27_2 = G_B26_1;
	}

IL_0231:
	{
		bool L_84 = ((bool)G_B27_0);
		Il2CppObject * L_85 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_84);
		NullCheck(G_B27_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B27_2, G_B27_1, L_85);
		Hashtable_t1407064410 * L_86 = V_2;
		FsmString_t952858651 * L_87 = __this->get_id_18();
		NullCheck(L_87);
		bool L_88 = NamedVariable_get_IsNone_m281035543(L_87, /*hidden argument*/NULL);
		G_B28_0 = _stringLiteral3373707;
		G_B28_1 = L_86;
		if (!L_88)
		{
			G_B29_0 = _stringLiteral3373707;
			G_B29_1 = L_86;
			goto IL_025b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_89 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B30_0 = L_89;
		G_B30_1 = G_B28_0;
		G_B30_2 = G_B28_1;
		goto IL_0266;
	}

IL_025b:
	{
		FsmString_t952858651 * L_90 = __this->get_id_18();
		NullCheck(L_90);
		String_t* L_91 = FsmString_get_Value_m872383149(L_90, /*hidden argument*/NULL);
		G_B30_0 = L_91;
		G_B30_1 = G_B29_0;
		G_B30_2 = G_B29_1;
	}

IL_0266:
	{
		NullCheck(G_B30_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B30_2, G_B30_1, G_B30_0);
		Hashtable_t1407064410 * L_92 = V_2;
		int32_t L_93 = __this->get_space_24();
		bool L_94 = ((bool)((((int32_t)L_93) == ((int32_t)1))? 1 : 0));
		Il2CppObject * L_95 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_94);
		NullCheck(L_92);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_92, _stringLiteral2094103681, L_95);
		Hashtable_t1407064410 * L_96 = V_2;
		int32_t L_97 = __this->get_axis_31();
		G_B31_0 = _stringLiteral3008417;
		G_B31_1 = L_96;
		if (L_97)
		{
			G_B32_0 = _stringLiteral3008417;
			G_B32_1 = L_96;
			goto IL_029f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_98 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B33_0 = L_98;
		G_B33_1 = G_B31_0;
		G_B33_2 = G_B31_1;
		goto IL_02b9;
	}

IL_029f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_99 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(AxisRestriction_t3260241011_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_100 = __this->get_axis_31();
		int32_t L_101 = L_100;
		Il2CppObject * L_102 = Box(AxisRestriction_t3260241011_il2cpp_TypeInfo_var, &L_101);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		String_t* L_103 = Enum_GetName_m3333121265(NULL /*static, unused*/, L_99, L_102, /*hidden argument*/NULL);
		G_B33_0 = L_103;
		G_B33_1 = G_B32_0;
		G_B33_2 = G_B32_1;
	}

IL_02b9:
	{
		NullCheck(G_B33_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B33_2, G_B33_1, G_B33_0);
		FsmBool_t1075959796 * L_104 = __this->get_orientToPath_27();
		NullCheck(L_104);
		bool L_105 = NamedVariable_get_IsNone_m281035543(L_104, /*hidden argument*/NULL);
		if (L_105)
		{
			goto IL_02e9;
		}
	}
	{
		Hashtable_t1407064410 * L_106 = V_2;
		FsmBool_t1075959796 * L_107 = __this->get_orientToPath_27();
		NullCheck(L_107);
		bool L_108 = FsmBool_get_Value_m3101329097(L_107, /*hidden argument*/NULL);
		bool L_109 = L_108;
		Il2CppObject * L_110 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_109);
		NullCheck(L_106);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_106, _stringLiteral3159645605, L_110);
	}

IL_02e9:
	{
		FsmGameObject_t1697147867 * L_111 = __this->get_lookAtObject_28();
		NullCheck(L_111);
		bool L_112 = NamedVariable_get_IsNone_m281035543(L_111, /*hidden argument*/NULL);
		if (L_112)
		{
			goto IL_035d;
		}
	}
	{
		Hashtable_t1407064410 * L_113 = V_2;
		FsmVector3_t533912882 * L_114 = __this->get_lookAtVector_29();
		NullCheck(L_114);
		bool L_115 = NamedVariable_get_IsNone_m281035543(L_114, /*hidden argument*/NULL);
		G_B37_0 = _stringLiteral1258521840;
		G_B37_1 = L_113;
		if (!L_115)
		{
			G_B38_0 = _stringLiteral1258521840;
			G_B38_1 = L_113;
			goto IL_0329;
		}
	}
	{
		FsmGameObject_t1697147867 * L_116 = __this->get_lookAtObject_28();
		NullCheck(L_116);
		GameObject_t3674682005 * L_117 = FsmGameObject_get_Value_m673294275(L_116, /*hidden argument*/NULL);
		NullCheck(L_117);
		Transform_t1659122786 * L_118 = GameObject_get_transform_m1278640159(L_117, /*hidden argument*/NULL);
		NullCheck(L_118);
		Vector3_t4282066566  L_119 = Transform_get_position_m2211398607(L_118, /*hidden argument*/NULL);
		G_B39_0 = L_119;
		G_B39_1 = G_B37_0;
		G_B39_2 = G_B37_1;
		goto IL_034e;
	}

IL_0329:
	{
		FsmGameObject_t1697147867 * L_120 = __this->get_lookAtObject_28();
		NullCheck(L_120);
		GameObject_t3674682005 * L_121 = FsmGameObject_get_Value_m673294275(L_120, /*hidden argument*/NULL);
		NullCheck(L_121);
		Transform_t1659122786 * L_122 = GameObject_get_transform_m1278640159(L_121, /*hidden argument*/NULL);
		NullCheck(L_122);
		Vector3_t4282066566  L_123 = Transform_get_position_m2211398607(L_122, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_124 = __this->get_lookAtVector_29();
		NullCheck(L_124);
		Vector3_t4282066566  L_125 = FsmVector3_get_Value_m2779135117(L_124, /*hidden argument*/NULL);
		Vector3_t4282066566  L_126 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_123, L_125, /*hidden argument*/NULL);
		G_B39_0 = L_126;
		G_B39_1 = G_B38_0;
		G_B39_2 = G_B38_1;
	}

IL_034e:
	{
		Vector3_t4282066566  L_127 = G_B39_0;
		Il2CppObject * L_128 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_127);
		NullCheck(G_B39_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B39_2, G_B39_1, L_128);
		goto IL_0388;
	}

IL_035d:
	{
		FsmVector3_t533912882 * L_129 = __this->get_lookAtVector_29();
		NullCheck(L_129);
		bool L_130 = NamedVariable_get_IsNone_m281035543(L_129, /*hidden argument*/NULL);
		if (L_130)
		{
			goto IL_0388;
		}
	}
	{
		Hashtable_t1407064410 * L_131 = V_2;
		FsmVector3_t533912882 * L_132 = __this->get_lookAtVector_29();
		NullCheck(L_132);
		Vector3_t4282066566  L_133 = FsmVector3_get_Value_m2779135117(L_132, /*hidden argument*/NULL);
		Vector3_t4282066566  L_134 = L_133;
		Il2CppObject * L_135 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_134);
		NullCheck(L_131);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_131, _stringLiteral1258521840, L_135);
	}

IL_0388:
	{
		FsmGameObject_t1697147867 * L_136 = __this->get_lookAtObject_28();
		NullCheck(L_136);
		bool L_137 = NamedVariable_get_IsNone_m281035543(L_136, /*hidden argument*/NULL);
		if (!L_137)
		{
			goto IL_03a8;
		}
	}
	{
		FsmVector3_t533912882 * L_138 = __this->get_lookAtVector_29();
		NullCheck(L_138);
		bool L_139 = NamedVariable_get_IsNone_m281035543(L_138, /*hidden argument*/NULL);
		if (L_139)
		{
			goto IL_03dd;
		}
	}

IL_03a8:
	{
		Hashtable_t1407064410 * L_140 = V_2;
		FsmFloat_t2134102846 * L_141 = __this->get_lookTime_30();
		NullCheck(L_141);
		bool L_142 = NamedVariable_get_IsNone_m281035543(L_141, /*hidden argument*/NULL);
		G_B45_0 = _stringLiteral2253828588;
		G_B45_1 = L_140;
		if (!L_142)
		{
			G_B46_0 = _stringLiteral2253828588;
			G_B46_1 = L_140;
			goto IL_03c8;
		}
	}
	{
		G_B47_0 = (0.0f);
		G_B47_1 = G_B45_0;
		G_B47_2 = G_B45_1;
		goto IL_03d3;
	}

IL_03c8:
	{
		FsmFloat_t2134102846 * L_143 = __this->get_lookTime_30();
		NullCheck(L_143);
		float L_144 = FsmFloat_get_Value_m4137923823(L_143, /*hidden argument*/NULL);
		G_B47_0 = L_144;
		G_B47_1 = G_B46_0;
		G_B47_2 = G_B46_1;
	}

IL_03d3:
	{
		float L_145 = G_B47_0;
		Il2CppObject * L_146 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_145);
		NullCheck(G_B47_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B47_2, G_B47_1, L_146);
	}

IL_03dd:
	{
		FsmGameObjectU5BU5D_t1706220122* L_147 = __this->get_transforms_34();
		NullCheck(L_147);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_147)->max_length))))) < ((int32_t)2)))
		{
			goto IL_075e;
		}
	}
	{
		FsmGameObjectU5BU5D_t1706220122* L_148 = __this->get_transforms_34();
		NullCheck(L_148);
		__this->set_tempVct3_37(((Vector3U5BU5D_t215400611*)SZArrayNew(Vector3U5BU5D_t215400611_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_148)->max_length)))))));
		FsmBool_t1075959796 * L_149 = __this->get_reverse_36();
		NullCheck(L_149);
		bool L_150 = NamedVariable_get_IsNone_m281035543(L_149, /*hidden argument*/NULL);
		if (!L_150)
		{
			goto IL_0414;
		}
	}
	{
		G_B52_0 = 0;
		goto IL_041f;
	}

IL_0414:
	{
		FsmBool_t1075959796 * L_151 = __this->get_reverse_36();
		NullCheck(L_151);
		bool L_152 = FsmBool_get_Value_m3101329097(L_151, /*hidden argument*/NULL);
		G_B52_0 = ((int32_t)(L_152));
	}

IL_041f:
	{
		if (!G_B52_0)
		{
			goto IL_0588;
		}
	}
	{
		V_3 = 0;
		goto IL_0575;
	}

IL_042b:
	{
		FsmGameObjectU5BU5D_t1706220122* L_153 = __this->get_transforms_34();
		int32_t L_154 = V_3;
		NullCheck(L_153);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_153, L_154);
		int32_t L_155 = L_154;
		FsmGameObject_t1697147867 * L_156 = (L_153)->GetAt(static_cast<il2cpp_array_size_t>(L_155));
		NullCheck(L_156);
		bool L_157 = NamedVariable_get_IsNone_m281035543(L_156, /*hidden argument*/NULL);
		if (!L_157)
		{
			goto IL_0487;
		}
	}
	{
		Vector3U5BU5D_t215400611* L_158 = __this->get_tempVct3_37();
		Vector3U5BU5D_t215400611* L_159 = __this->get_tempVct3_37();
		NullCheck(L_159);
		int32_t L_160 = V_3;
		NullCheck(L_158);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_158, ((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_159)->max_length))))-(int32_t)1))-(int32_t)L_160)));
		FsmVector3U5BU5D_t607182279* L_161 = __this->get_vectors_35();
		int32_t L_162 = V_3;
		NullCheck(L_161);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_161, L_162);
		int32_t L_163 = L_162;
		FsmVector3_t533912882 * L_164 = (L_161)->GetAt(static_cast<il2cpp_array_size_t>(L_163));
		NullCheck(L_164);
		bool L_165 = NamedVariable_get_IsNone_m281035543(L_164, /*hidden argument*/NULL);
		G_B56_0 = ((L_158)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_159)->max_length))))-(int32_t)1))-(int32_t)L_160)))));
		if (!L_165)
		{
			G_B57_0 = ((L_158)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_159)->max_length))))-(int32_t)1))-(int32_t)L_160)))));
			goto IL_0470;
		}
	}
	{
		Vector3_t4282066566  L_166 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B58_0 = L_166;
		G_B58_1 = G_B56_0;
		goto IL_047d;
	}

IL_0470:
	{
		FsmVector3U5BU5D_t607182279* L_167 = __this->get_vectors_35();
		int32_t L_168 = V_3;
		NullCheck(L_167);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_167, L_168);
		int32_t L_169 = L_168;
		FsmVector3_t533912882 * L_170 = (L_167)->GetAt(static_cast<il2cpp_array_size_t>(L_169));
		NullCheck(L_170);
		Vector3_t4282066566  L_171 = FsmVector3_get_Value_m2779135117(L_170, /*hidden argument*/NULL);
		G_B58_0 = L_171;
		G_B58_1 = G_B57_0;
	}

IL_047d:
	{
		(*(Vector3_t4282066566 *)G_B58_1) = G_B58_0;
		goto IL_0571;
	}

IL_0487:
	{
		FsmGameObjectU5BU5D_t1706220122* L_172 = __this->get_transforms_34();
		int32_t L_173 = V_3;
		NullCheck(L_172);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_172, L_173);
		int32_t L_174 = L_173;
		FsmGameObject_t1697147867 * L_175 = (L_172)->GetAt(static_cast<il2cpp_array_size_t>(L_174));
		NullCheck(L_175);
		GameObject_t3674682005 * L_176 = FsmGameObject_get_Value_m673294275(L_175, /*hidden argument*/NULL);
		bool L_177 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_176, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_177)
		{
			goto IL_04e9;
		}
	}
	{
		Vector3U5BU5D_t215400611* L_178 = __this->get_tempVct3_37();
		Vector3U5BU5D_t215400611* L_179 = __this->get_tempVct3_37();
		NullCheck(L_179);
		int32_t L_180 = V_3;
		NullCheck(L_178);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_178, ((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_179)->max_length))))-(int32_t)1))-(int32_t)L_180)));
		FsmVector3U5BU5D_t607182279* L_181 = __this->get_vectors_35();
		int32_t L_182 = V_3;
		NullCheck(L_181);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_181, L_182);
		int32_t L_183 = L_182;
		FsmVector3_t533912882 * L_184 = (L_181)->GetAt(static_cast<il2cpp_array_size_t>(L_183));
		NullCheck(L_184);
		bool L_185 = NamedVariable_get_IsNone_m281035543(L_184, /*hidden argument*/NULL);
		G_B61_0 = ((L_178)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_179)->max_length))))-(int32_t)1))-(int32_t)L_180)))));
		if (!L_185)
		{
			G_B62_0 = ((L_178)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_179)->max_length))))-(int32_t)1))-(int32_t)L_180)))));
			goto IL_04d2;
		}
	}
	{
		Vector3_t4282066566  L_186 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B63_0 = L_186;
		G_B63_1 = G_B61_0;
		goto IL_04df;
	}

IL_04d2:
	{
		FsmVector3U5BU5D_t607182279* L_187 = __this->get_vectors_35();
		int32_t L_188 = V_3;
		NullCheck(L_187);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_187, L_188);
		int32_t L_189 = L_188;
		FsmVector3_t533912882 * L_190 = (L_187)->GetAt(static_cast<il2cpp_array_size_t>(L_189));
		NullCheck(L_190);
		Vector3_t4282066566  L_191 = FsmVector3_get_Value_m2779135117(L_190, /*hidden argument*/NULL);
		G_B63_0 = L_191;
		G_B63_1 = G_B62_0;
	}

IL_04df:
	{
		(*(Vector3_t4282066566 *)G_B63_1) = G_B63_0;
		goto IL_0571;
	}

IL_04e9:
	{
		Vector3U5BU5D_t215400611* L_192 = __this->get_tempVct3_37();
		Vector3U5BU5D_t215400611* L_193 = __this->get_tempVct3_37();
		NullCheck(L_193);
		int32_t L_194 = V_3;
		NullCheck(L_192);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_192, ((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_193)->max_length))))-(int32_t)1))-(int32_t)L_194)));
		int32_t L_195 = __this->get_space_24();
		G_B65_0 = ((L_192)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_193)->max_length))))-(int32_t)1))-(int32_t)L_194)))));
		if (L_195)
		{
			G_B66_0 = ((L_192)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_193)->max_length))))-(int32_t)1))-(int32_t)L_194)))));
			goto IL_0527;
		}
	}
	{
		FsmGameObjectU5BU5D_t1706220122* L_196 = __this->get_transforms_34();
		int32_t L_197 = V_3;
		NullCheck(L_196);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_196, L_197);
		int32_t L_198 = L_197;
		FsmGameObject_t1697147867 * L_199 = (L_196)->GetAt(static_cast<il2cpp_array_size_t>(L_198));
		NullCheck(L_199);
		GameObject_t3674682005 * L_200 = FsmGameObject_get_Value_m673294275(L_199, /*hidden argument*/NULL);
		NullCheck(L_200);
		Transform_t1659122786 * L_201 = GameObject_get_transform_m1278640159(L_200, /*hidden argument*/NULL);
		NullCheck(L_201);
		Vector3_t4282066566  L_202 = Transform_get_position_m2211398607(L_201, /*hidden argument*/NULL);
		G_B67_0 = L_202;
		G_B67_1 = G_B65_0;
		goto IL_053e;
	}

IL_0527:
	{
		FsmGameObjectU5BU5D_t1706220122* L_203 = __this->get_transforms_34();
		int32_t L_204 = V_3;
		NullCheck(L_203);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_203, L_204);
		int32_t L_205 = L_204;
		FsmGameObject_t1697147867 * L_206 = (L_203)->GetAt(static_cast<il2cpp_array_size_t>(L_205));
		NullCheck(L_206);
		GameObject_t3674682005 * L_207 = FsmGameObject_get_Value_m673294275(L_206, /*hidden argument*/NULL);
		NullCheck(L_207);
		Transform_t1659122786 * L_208 = GameObject_get_transform_m1278640159(L_207, /*hidden argument*/NULL);
		NullCheck(L_208);
		Vector3_t4282066566  L_209 = Transform_get_localPosition_m668140784(L_208, /*hidden argument*/NULL);
		G_B67_0 = L_209;
		G_B67_1 = G_B66_0;
	}

IL_053e:
	{
		FsmVector3U5BU5D_t607182279* L_210 = __this->get_vectors_35();
		int32_t L_211 = V_3;
		NullCheck(L_210);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_210, L_211);
		int32_t L_212 = L_211;
		FsmVector3_t533912882 * L_213 = (L_210)->GetAt(static_cast<il2cpp_array_size_t>(L_212));
		NullCheck(L_213);
		bool L_214 = NamedVariable_get_IsNone_m281035543(L_213, /*hidden argument*/NULL);
		G_B68_0 = G_B67_0;
		G_B68_1 = G_B67_1;
		if (!L_214)
		{
			G_B69_0 = G_B67_0;
			G_B69_1 = G_B67_1;
			goto IL_055a;
		}
	}
	{
		Vector3_t4282066566  L_215 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B70_0 = L_215;
		G_B70_1 = G_B68_0;
		G_B70_2 = G_B68_1;
		goto IL_0567;
	}

IL_055a:
	{
		FsmVector3U5BU5D_t607182279* L_216 = __this->get_vectors_35();
		int32_t L_217 = V_3;
		NullCheck(L_216);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_216, L_217);
		int32_t L_218 = L_217;
		FsmVector3_t533912882 * L_219 = (L_216)->GetAt(static_cast<il2cpp_array_size_t>(L_218));
		NullCheck(L_219);
		Vector3_t4282066566  L_220 = FsmVector3_get_Value_m2779135117(L_219, /*hidden argument*/NULL);
		G_B70_0 = L_220;
		G_B70_1 = G_B69_0;
		G_B70_2 = G_B69_1;
	}

IL_0567:
	{
		Vector3_t4282066566  L_221 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, G_B70_1, G_B70_0, /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)G_B70_2) = L_221;
	}

IL_0571:
	{
		int32_t L_222 = V_3;
		V_3 = ((int32_t)((int32_t)L_222+(int32_t)1));
	}

IL_0575:
	{
		int32_t L_223 = V_3;
		FsmGameObjectU5BU5D_t1706220122* L_224 = __this->get_transforms_34();
		NullCheck(L_224);
		if ((((int32_t)L_223) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_224)->max_length)))))))
		{
			goto IL_042b;
		}
	}
	{
		goto IL_06e7;
	}

IL_0588:
	{
		V_4 = 0;
		goto IL_06d8;
	}

IL_0590:
	{
		FsmGameObjectU5BU5D_t1706220122* L_225 = __this->get_transforms_34();
		int32_t L_226 = V_4;
		NullCheck(L_225);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_225, L_226);
		int32_t L_227 = L_226;
		FsmGameObject_t1697147867 * L_228 = (L_225)->GetAt(static_cast<il2cpp_array_size_t>(L_227));
		NullCheck(L_228);
		bool L_229 = NamedVariable_get_IsNone_m281035543(L_228, /*hidden argument*/NULL);
		if (!L_229)
		{
			goto IL_05e5;
		}
	}
	{
		Vector3U5BU5D_t215400611* L_230 = __this->get_tempVct3_37();
		int32_t L_231 = V_4;
		NullCheck(L_230);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_230, L_231);
		FsmVector3U5BU5D_t607182279* L_232 = __this->get_vectors_35();
		int32_t L_233 = V_4;
		NullCheck(L_232);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_232, L_233);
		int32_t L_234 = L_233;
		FsmVector3_t533912882 * L_235 = (L_232)->GetAt(static_cast<il2cpp_array_size_t>(L_234));
		NullCheck(L_235);
		bool L_236 = NamedVariable_get_IsNone_m281035543(L_235, /*hidden argument*/NULL);
		G_B77_0 = ((L_230)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_231)));
		if (!L_236)
		{
			G_B78_0 = ((L_230)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_231)));
			goto IL_05cd;
		}
	}
	{
		Vector3_t4282066566  L_237 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B79_0 = L_237;
		G_B79_1 = G_B77_0;
		goto IL_05db;
	}

IL_05cd:
	{
		FsmVector3U5BU5D_t607182279* L_238 = __this->get_vectors_35();
		int32_t L_239 = V_4;
		NullCheck(L_238);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_238, L_239);
		int32_t L_240 = L_239;
		FsmVector3_t533912882 * L_241 = (L_238)->GetAt(static_cast<il2cpp_array_size_t>(L_240));
		NullCheck(L_241);
		Vector3_t4282066566  L_242 = FsmVector3_get_Value_m2779135117(L_241, /*hidden argument*/NULL);
		G_B79_0 = L_242;
		G_B79_1 = G_B78_0;
	}

IL_05db:
	{
		(*(Vector3_t4282066566 *)G_B79_1) = G_B79_0;
		goto IL_06d2;
	}

IL_05e5:
	{
		FsmGameObjectU5BU5D_t1706220122* L_243 = __this->get_transforms_34();
		int32_t L_244 = V_4;
		NullCheck(L_243);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_243, L_244);
		int32_t L_245 = L_244;
		FsmGameObject_t1697147867 * L_246 = (L_243)->GetAt(static_cast<il2cpp_array_size_t>(L_245));
		NullCheck(L_246);
		GameObject_t3674682005 * L_247 = FsmGameObject_get_Value_m673294275(L_246, /*hidden argument*/NULL);
		bool L_248 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_247, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_248)
		{
			goto IL_0640;
		}
	}
	{
		Vector3U5BU5D_t215400611* L_249 = __this->get_tempVct3_37();
		int32_t L_250 = V_4;
		NullCheck(L_249);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_249, L_250);
		FsmVector3U5BU5D_t607182279* L_251 = __this->get_vectors_35();
		int32_t L_252 = V_4;
		NullCheck(L_251);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_251, L_252);
		int32_t L_253 = L_252;
		FsmVector3_t533912882 * L_254 = (L_251)->GetAt(static_cast<il2cpp_array_size_t>(L_253));
		NullCheck(L_254);
		bool L_255 = NamedVariable_get_IsNone_m281035543(L_254, /*hidden argument*/NULL);
		G_B82_0 = ((L_249)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_250)));
		if (!L_255)
		{
			G_B83_0 = ((L_249)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_250)));
			goto IL_0628;
		}
	}
	{
		Vector3_t4282066566  L_256 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B84_0 = L_256;
		G_B84_1 = G_B82_0;
		goto IL_0636;
	}

IL_0628:
	{
		FsmVector3U5BU5D_t607182279* L_257 = __this->get_vectors_35();
		int32_t L_258 = V_4;
		NullCheck(L_257);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_257, L_258);
		int32_t L_259 = L_258;
		FsmVector3_t533912882 * L_260 = (L_257)->GetAt(static_cast<il2cpp_array_size_t>(L_259));
		NullCheck(L_260);
		Vector3_t4282066566  L_261 = FsmVector3_get_Value_m2779135117(L_260, /*hidden argument*/NULL);
		G_B84_0 = L_261;
		G_B84_1 = G_B83_0;
	}

IL_0636:
	{
		(*(Vector3_t4282066566 *)G_B84_1) = G_B84_0;
		goto IL_06d2;
	}

IL_0640:
	{
		Vector3U5BU5D_t215400611* L_262 = __this->get_tempVct3_37();
		int32_t L_263 = V_4;
		NullCheck(L_262);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_262, L_263);
		int32_t L_264 = __this->get_space_24();
		G_B86_0 = ((L_262)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_263)));
		if (L_264)
		{
			G_B87_0 = ((L_262)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_263)));
			goto IL_0675;
		}
	}
	{
		FsmGameObjectU5BU5D_t1706220122* L_265 = __this->get_transforms_34();
		int32_t L_266 = V_4;
		NullCheck(L_265);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_265, L_266);
		int32_t L_267 = L_266;
		FsmGameObject_t1697147867 * L_268 = (L_265)->GetAt(static_cast<il2cpp_array_size_t>(L_267));
		NullCheck(L_268);
		GameObject_t3674682005 * L_269 = FsmGameObject_get_Value_m673294275(L_268, /*hidden argument*/NULL);
		NullCheck(L_269);
		Transform_t1659122786 * L_270 = GameObject_get_transform_m1278640159(L_269, /*hidden argument*/NULL);
		NullCheck(L_270);
		Vector3_t4282066566  L_271 = Transform_get_position_m2211398607(L_270, /*hidden argument*/NULL);
		G_B88_0 = L_271;
		G_B88_1 = G_B86_0;
		goto IL_069d;
	}

IL_0675:
	{
		GameObject_t3674682005 * L_272 = V_0;
		NullCheck(L_272);
		Transform_t1659122786 * L_273 = GameObject_get_transform_m1278640159(L_272, /*hidden argument*/NULL);
		NullCheck(L_273);
		Transform_t1659122786 * L_274 = Transform_get_parent_m2236876972(L_273, /*hidden argument*/NULL);
		FsmGameObjectU5BU5D_t1706220122* L_275 = __this->get_transforms_34();
		int32_t L_276 = V_4;
		NullCheck(L_275);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_275, L_276);
		int32_t L_277 = L_276;
		FsmGameObject_t1697147867 * L_278 = (L_275)->GetAt(static_cast<il2cpp_array_size_t>(L_277));
		NullCheck(L_278);
		GameObject_t3674682005 * L_279 = FsmGameObject_get_Value_m673294275(L_278, /*hidden argument*/NULL);
		NullCheck(L_279);
		Transform_t1659122786 * L_280 = GameObject_get_transform_m1278640159(L_279, /*hidden argument*/NULL);
		NullCheck(L_280);
		Vector3_t4282066566  L_281 = Transform_get_position_m2211398607(L_280, /*hidden argument*/NULL);
		NullCheck(L_274);
		Vector3_t4282066566  L_282 = Transform_InverseTransformPoint_m1626812000(L_274, L_281, /*hidden argument*/NULL);
		G_B88_0 = L_282;
		G_B88_1 = G_B87_0;
	}

IL_069d:
	{
		FsmVector3U5BU5D_t607182279* L_283 = __this->get_vectors_35();
		int32_t L_284 = V_4;
		NullCheck(L_283);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_283, L_284);
		int32_t L_285 = L_284;
		FsmVector3_t533912882 * L_286 = (L_283)->GetAt(static_cast<il2cpp_array_size_t>(L_285));
		NullCheck(L_286);
		bool L_287 = NamedVariable_get_IsNone_m281035543(L_286, /*hidden argument*/NULL);
		G_B89_0 = G_B88_0;
		G_B89_1 = G_B88_1;
		if (!L_287)
		{
			G_B90_0 = G_B88_0;
			G_B90_1 = G_B88_1;
			goto IL_06ba;
		}
	}
	{
		Vector3_t4282066566  L_288 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B91_0 = L_288;
		G_B91_1 = G_B89_0;
		G_B91_2 = G_B89_1;
		goto IL_06c8;
	}

IL_06ba:
	{
		FsmVector3U5BU5D_t607182279* L_289 = __this->get_vectors_35();
		int32_t L_290 = V_4;
		NullCheck(L_289);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_289, L_290);
		int32_t L_291 = L_290;
		FsmVector3_t533912882 * L_292 = (L_289)->GetAt(static_cast<il2cpp_array_size_t>(L_291));
		NullCheck(L_292);
		Vector3_t4282066566  L_293 = FsmVector3_get_Value_m2779135117(L_292, /*hidden argument*/NULL);
		G_B91_0 = L_293;
		G_B91_1 = G_B90_0;
		G_B91_2 = G_B90_1;
	}

IL_06c8:
	{
		Vector3_t4282066566  L_294 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, G_B91_1, G_B91_0, /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)G_B91_2) = L_294;
	}

IL_06d2:
	{
		int32_t L_295 = V_4;
		V_4 = ((int32_t)((int32_t)L_295+(int32_t)1));
	}

IL_06d8:
	{
		int32_t L_296 = V_4;
		FsmGameObjectU5BU5D_t1706220122* L_297 = __this->get_transforms_34();
		NullCheck(L_297);
		if ((((int32_t)L_296) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_297)->max_length)))))))
		{
			goto IL_0590;
		}
	}

IL_06e7:
	{
		Hashtable_t1407064410 * L_298 = V_2;
		Vector3U5BU5D_t215400611* L_299 = __this->get_tempVct3_37();
		NullCheck(L_298);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_298, _stringLiteral3433509, (Il2CppObject *)(Il2CppObject *)L_299);
		Hashtable_t1407064410 * L_300 = V_2;
		FsmBool_t1075959796 * L_301 = __this->get_moveToPath_32();
		NullCheck(L_301);
		bool L_302 = NamedVariable_get_IsNone_m281035543(L_301, /*hidden argument*/NULL);
		G_B95_0 = _stringLiteral3654555697;
		G_B95_1 = L_300;
		if (!L_302)
		{
			G_B96_0 = _stringLiteral3654555697;
			G_B96_1 = L_300;
			goto IL_0714;
		}
	}
	{
		G_B97_0 = 1;
		G_B97_1 = G_B95_0;
		G_B97_2 = G_B95_1;
		goto IL_071f;
	}

IL_0714:
	{
		FsmBool_t1075959796 * L_303 = __this->get_moveToPath_32();
		NullCheck(L_303);
		bool L_304 = FsmBool_get_Value_m3101329097(L_303, /*hidden argument*/NULL);
		G_B97_0 = ((int32_t)(L_304));
		G_B97_1 = G_B96_0;
		G_B97_2 = G_B96_1;
	}

IL_071f:
	{
		bool L_305 = ((bool)G_B97_0);
		Il2CppObject * L_306 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_305);
		NullCheck(G_B97_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B97_2, G_B97_1, L_306);
		Hashtable_t1407064410 * L_307 = V_2;
		FsmFloat_t2134102846 * L_308 = __this->get_lookAhead_33();
		NullCheck(L_308);
		bool L_309 = NamedVariable_get_IsNone_m281035543(L_308, /*hidden argument*/NULL);
		G_B98_0 = _stringLiteral1131625090;
		G_B98_1 = L_307;
		if (!L_309)
		{
			G_B99_0 = _stringLiteral1131625090;
			G_B99_1 = L_307;
			goto IL_0749;
		}
	}
	{
		G_B100_0 = (1.0f);
		G_B100_1 = G_B98_0;
		G_B100_2 = G_B98_1;
		goto IL_0754;
	}

IL_0749:
	{
		FsmFloat_t2134102846 * L_310 = __this->get_lookAhead_33();
		NullCheck(L_310);
		float L_311 = FsmFloat_get_Value_m4137923823(L_310, /*hidden argument*/NULL);
		G_B100_0 = L_311;
		G_B100_1 = G_B99_0;
		G_B100_2 = G_B99_1;
	}

IL_0754:
	{
		float L_312 = G_B100_0;
		Il2CppObject * L_313 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_312);
		NullCheck(G_B100_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B100_2, G_B100_1, L_313);
	}

IL_075e:
	{
		((iTweenFsmAction_t410382178 *)__this)->set_itweenType_15(_stringLiteral3357649);
		GameObject_t3674682005 * L_314 = V_0;
		Hashtable_t1407064410 * L_315 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		iTween_MoveTo_m569418479(NULL /*static, unused*/, L_314, L_315, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveUpdate::.ctor()
extern "C"  void iTweenMoveUpdate__ctor_m1221468738 (iTweenMoveUpdate_t733362340 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveUpdate::Reset()
extern Il2CppClass* FsmGameObject_t1697147867_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmBool_t1075959796_il2cpp_TypeInfo_var;
extern const uint32_t iTweenMoveUpdate_Reset_m3162868975_MetadataUsageId;
extern "C"  void iTweenMoveUpdate_Reset_m3162868975 (iTweenMoveUpdate_t733362340 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenMoveUpdate_Reset_m3162868975_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmGameObject_t1697147867 * V_0 = NULL;
	FsmVector3_t533912882 * V_1 = NULL;
	FsmBool_t1075959796 * V_2 = NULL;
	{
		FsmGameObject_t1697147867 * L_0 = (FsmGameObject_t1697147867 *)il2cpp_codegen_object_new(FsmGameObject_t1697147867_il2cpp_TypeInfo_var);
		FsmGameObject__ctor_m1048269796(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmGameObject_t1697147867 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_2 = V_0;
		__this->set_transformPosition_10(L_2);
		FsmVector3_t533912882 * L_3 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_3, /*hidden argument*/NULL);
		V_1 = L_3;
		FsmVector3_t533912882 * L_4 = V_1;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_5 = V_1;
		__this->set_vectorPosition_11(L_5);
		FsmFloat_t2134102846 * L_6 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_time_12(L_6);
		__this->set_space_13(0);
		FsmBool_t1075959796 * L_7 = (FsmBool_t1075959796 *)il2cpp_codegen_object_new(FsmBool_t1075959796_il2cpp_TypeInfo_var);
		FsmBool__ctor_m1553455211(L_7, /*hidden argument*/NULL);
		V_2 = L_7;
		FsmBool_t1075959796 * L_8 = V_2;
		NullCheck(L_8);
		FsmBool_set_Value_m1126216340(L_8, (bool)1, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_9 = V_2;
		__this->set_orientToPath_14(L_9);
		FsmGameObject_t1697147867 * L_10 = (FsmGameObject_t1697147867 *)il2cpp_codegen_object_new(FsmGameObject_t1697147867_il2cpp_TypeInfo_var);
		FsmGameObject__ctor_m1048269796(L_10, /*hidden argument*/NULL);
		V_0 = L_10;
		FsmGameObject_t1697147867 * L_11 = V_0;
		NullCheck(L_11);
		NamedVariable_set_UseVariable_m4266138971(L_11, (bool)1, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_12 = V_0;
		__this->set_lookAtObject_15(L_12);
		FsmVector3_t533912882 * L_13 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_13, /*hidden argument*/NULL);
		V_1 = L_13;
		FsmVector3_t533912882 * L_14 = V_1;
		NullCheck(L_14);
		NamedVariable_set_UseVariable_m4266138971(L_14, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_15 = V_1;
		__this->set_lookAtVector_16(L_15);
		FsmFloat_t2134102846 * L_16 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_lookTime_17(L_16);
		__this->set_axis_18(0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveUpdate::OnEnter()
extern const Il2CppType* AxisRestriction_t3260241011_0_0_0_var;
extern Il2CppClass* Hashtable_t1407064410_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* AxisRestriction_t3260241011_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2862688501_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral747804969;
extern Il2CppCodeGenString* _stringLiteral3560141;
extern Il2CppCodeGenString* _stringLiteral2094103681;
extern Il2CppCodeGenString* _stringLiteral3008417;
extern Il2CppCodeGenString* _stringLiteral3159645605;
extern Il2CppCodeGenString* _stringLiteral1258521840;
extern Il2CppCodeGenString* _stringLiteral2253828588;
extern const uint32_t iTweenMoveUpdate_OnEnter_m697114009_MetadataUsageId;
extern "C"  void iTweenMoveUpdate_OnEnter_m697114009 (iTweenMoveUpdate_t733362340 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenMoveUpdate_OnEnter_m697114009_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B5_0 = NULL;
	Hashtable_t1407064410 * G_B5_1 = NULL;
	String_t* G_B4_0 = NULL;
	Hashtable_t1407064410 * G_B4_1 = NULL;
	Vector3_t4282066566  G_B6_0;
	memset(&G_B6_0, 0, sizeof(G_B6_0));
	String_t* G_B6_1 = NULL;
	Hashtable_t1407064410 * G_B6_2 = NULL;
	String_t* G_B15_0 = NULL;
	Hashtable_t1407064410 * G_B15_1 = NULL;
	String_t* G_B14_0 = NULL;
	Hashtable_t1407064410 * G_B14_1 = NULL;
	float G_B16_0 = 0.0f;
	String_t* G_B16_1 = NULL;
	Hashtable_t1407064410 * G_B16_2 = NULL;
	String_t* G_B18_0 = NULL;
	Hashtable_t1407064410 * G_B18_1 = NULL;
	String_t* G_B17_0 = NULL;
	Hashtable_t1407064410 * G_B17_1 = NULL;
	String_t* G_B19_0 = NULL;
	String_t* G_B19_1 = NULL;
	Hashtable_t1407064410 * G_B19_2 = NULL;
	String_t* G_B30_0 = NULL;
	Hashtable_t1407064410 * G_B30_1 = NULL;
	String_t* G_B29_0 = NULL;
	Hashtable_t1407064410 * G_B29_1 = NULL;
	float G_B31_0 = 0.0f;
	String_t* G_B31_1 = NULL;
	Hashtable_t1407064410 * G_B31_2 = NULL;
	{
		Hashtable_t1407064410 * L_0 = (Hashtable_t1407064410 *)il2cpp_codegen_object_new(Hashtable_t1407064410_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1514037738(L_0, /*hidden argument*/NULL);
		__this->set_hash_19(L_0);
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_2 = __this->get_gameObject_9();
		NullCheck(L_1);
		GameObject_t3674682005 * L_3 = Fsm_GetOwnerDefaultTarget_m846013999(L_1, L_2, /*hidden argument*/NULL);
		__this->set_go_20(L_3);
		GameObject_t3674682005 * L_4 = __this->get_go_20();
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003a;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_003a:
	{
		FsmGameObject_t1697147867 * L_6 = __this->get_transformPosition_10();
		NullCheck(L_6);
		bool L_7 = NamedVariable_get_IsNone_m281035543(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0089;
		}
	}
	{
		Hashtable_t1407064410 * L_8 = __this->get_hash_19();
		FsmVector3_t533912882 * L_9 = __this->get_vectorPosition_11();
		NullCheck(L_9);
		bool L_10 = NamedVariable_get_IsNone_m281035543(L_9, /*hidden argument*/NULL);
		G_B4_0 = _stringLiteral747804969;
		G_B4_1 = L_8;
		if (!L_10)
		{
			G_B5_0 = _stringLiteral747804969;
			G_B5_1 = L_8;
			goto IL_006f;
		}
	}
	{
		Vector3_t4282066566  L_11 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		goto IL_007a;
	}

IL_006f:
	{
		FsmVector3_t533912882 * L_12 = __this->get_vectorPosition_11();
		NullCheck(L_12);
		Vector3_t4282066566  L_13 = FsmVector3_get_Value_m2779135117(L_12, /*hidden argument*/NULL);
		G_B6_0 = L_13;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
	}

IL_007a:
	{
		Vector3_t4282066566  L_14 = G_B6_0;
		Il2CppObject * L_15 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_14);
		NullCheck(G_B6_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B6_2, G_B6_1, L_15);
		goto IL_0172;
	}

IL_0089:
	{
		FsmVector3_t533912882 * L_16 = __this->get_vectorPosition_11();
		NullCheck(L_16);
		bool L_17 = NamedVariable_get_IsNone_m281035543(L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00be;
		}
	}
	{
		Hashtable_t1407064410 * L_18 = __this->get_hash_19();
		FsmGameObject_t1697147867 * L_19 = __this->get_transformPosition_10();
		NullCheck(L_19);
		GameObject_t3674682005 * L_20 = FsmGameObject_get_Value_m673294275(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_t1659122786 * L_21 = GameObject_get_transform_m1278640159(L_20, /*hidden argument*/NULL);
		NullCheck(L_18);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_18, _stringLiteral747804969, L_21);
		goto IL_0172;
	}

IL_00be:
	{
		int32_t L_22 = __this->get_space_13();
		if (!L_22)
		{
			goto IL_00e4;
		}
	}
	{
		GameObject_t3674682005 * L_23 = __this->get_go_20();
		NullCheck(L_23);
		Transform_t1659122786 * L_24 = GameObject_get_transform_m1278640159(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_t1659122786 * L_25 = Transform_get_parent_m2236876972(L_24, /*hidden argument*/NULL);
		bool L_26 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_25, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_0123;
		}
	}

IL_00e4:
	{
		Hashtable_t1407064410 * L_27 = __this->get_hash_19();
		FsmGameObject_t1697147867 * L_28 = __this->get_transformPosition_10();
		NullCheck(L_28);
		GameObject_t3674682005 * L_29 = FsmGameObject_get_Value_m673294275(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		Transform_t1659122786 * L_30 = GameObject_get_transform_m1278640159(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		Vector3_t4282066566  L_31 = Transform_get_position_m2211398607(L_30, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_32 = __this->get_vectorPosition_11();
		NullCheck(L_32);
		Vector3_t4282066566  L_33 = FsmVector3_get_Value_m2779135117(L_32, /*hidden argument*/NULL);
		Vector3_t4282066566  L_34 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_31, L_33, /*hidden argument*/NULL);
		Vector3_t4282066566  L_35 = L_34;
		Il2CppObject * L_36 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_35);
		NullCheck(L_27);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_27, _stringLiteral747804969, L_36);
		goto IL_0172;
	}

IL_0123:
	{
		Hashtable_t1407064410 * L_37 = __this->get_hash_19();
		GameObject_t3674682005 * L_38 = __this->get_go_20();
		NullCheck(L_38);
		Transform_t1659122786 * L_39 = GameObject_get_transform_m1278640159(L_38, /*hidden argument*/NULL);
		NullCheck(L_39);
		Transform_t1659122786 * L_40 = Transform_get_parent_m2236876972(L_39, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_41 = __this->get_transformPosition_10();
		NullCheck(L_41);
		GameObject_t3674682005 * L_42 = FsmGameObject_get_Value_m673294275(L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		Transform_t1659122786 * L_43 = GameObject_get_transform_m1278640159(L_42, /*hidden argument*/NULL);
		NullCheck(L_43);
		Vector3_t4282066566  L_44 = Transform_get_position_m2211398607(L_43, /*hidden argument*/NULL);
		NullCheck(L_40);
		Vector3_t4282066566  L_45 = Transform_InverseTransformPoint_m1626812000(L_40, L_44, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_46 = __this->get_vectorPosition_11();
		NullCheck(L_46);
		Vector3_t4282066566  L_47 = FsmVector3_get_Value_m2779135117(L_46, /*hidden argument*/NULL);
		Vector3_t4282066566  L_48 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_45, L_47, /*hidden argument*/NULL);
		Vector3_t4282066566  L_49 = L_48;
		Il2CppObject * L_50 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_49);
		NullCheck(L_37);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_37, _stringLiteral747804969, L_50);
	}

IL_0172:
	{
		Hashtable_t1407064410 * L_51 = __this->get_hash_19();
		FsmFloat_t2134102846 * L_52 = __this->get_time_12();
		NullCheck(L_52);
		bool L_53 = NamedVariable_get_IsNone_m281035543(L_52, /*hidden argument*/NULL);
		G_B14_0 = _stringLiteral3560141;
		G_B14_1 = L_51;
		if (!L_53)
		{
			G_B15_0 = _stringLiteral3560141;
			G_B15_1 = L_51;
			goto IL_0197;
		}
	}
	{
		G_B16_0 = (1.0f);
		G_B16_1 = G_B14_0;
		G_B16_2 = G_B14_1;
		goto IL_01a2;
	}

IL_0197:
	{
		FsmFloat_t2134102846 * L_54 = __this->get_time_12();
		NullCheck(L_54);
		float L_55 = FsmFloat_get_Value_m4137923823(L_54, /*hidden argument*/NULL);
		G_B16_0 = L_55;
		G_B16_1 = G_B15_0;
		G_B16_2 = G_B15_1;
	}

IL_01a2:
	{
		float L_56 = G_B16_0;
		Il2CppObject * L_57 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_56);
		NullCheck(G_B16_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B16_2, G_B16_1, L_57);
		Hashtable_t1407064410 * L_58 = __this->get_hash_19();
		int32_t L_59 = __this->get_space_13();
		bool L_60 = ((bool)((((int32_t)L_59) == ((int32_t)1))? 1 : 0));
		Il2CppObject * L_61 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_60);
		NullCheck(L_58);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_58, _stringLiteral2094103681, L_61);
		Hashtable_t1407064410 * L_62 = __this->get_hash_19();
		int32_t L_63 = __this->get_axis_18();
		G_B17_0 = _stringLiteral3008417;
		G_B17_1 = L_62;
		if (L_63)
		{
			G_B18_0 = _stringLiteral3008417;
			G_B18_1 = L_62;
			goto IL_01ea;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_64 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B19_0 = L_64;
		G_B19_1 = G_B17_0;
		G_B19_2 = G_B17_1;
		goto IL_0204;
	}

IL_01ea:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_65 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(AxisRestriction_t3260241011_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_66 = __this->get_axis_18();
		int32_t L_67 = L_66;
		Il2CppObject * L_68 = Box(AxisRestriction_t3260241011_il2cpp_TypeInfo_var, &L_67);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		String_t* L_69 = Enum_GetName_m3333121265(NULL /*static, unused*/, L_65, L_68, /*hidden argument*/NULL);
		G_B19_0 = L_69;
		G_B19_1 = G_B18_0;
		G_B19_2 = G_B18_1;
	}

IL_0204:
	{
		NullCheck(G_B19_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B19_2, G_B19_1, G_B19_0);
		FsmBool_t1075959796 * L_70 = __this->get_orientToPath_14();
		NullCheck(L_70);
		bool L_71 = NamedVariable_get_IsNone_m281035543(L_70, /*hidden argument*/NULL);
		if (L_71)
		{
			goto IL_0239;
		}
	}
	{
		Hashtable_t1407064410 * L_72 = __this->get_hash_19();
		FsmBool_t1075959796 * L_73 = __this->get_orientToPath_14();
		NullCheck(L_73);
		bool L_74 = FsmBool_get_Value_m3101329097(L_73, /*hidden argument*/NULL);
		bool L_75 = L_74;
		Il2CppObject * L_76 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_75);
		NullCheck(L_72);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_72, _stringLiteral3159645605, L_76);
	}

IL_0239:
	{
		FsmGameObject_t1697147867 * L_77 = __this->get_lookAtObject_15();
		NullCheck(L_77);
		bool L_78 = NamedVariable_get_IsNone_m281035543(L_77, /*hidden argument*/NULL);
		if (!L_78)
		{
			goto IL_027e;
		}
	}
	{
		FsmVector3_t533912882 * L_79 = __this->get_lookAtVector_16();
		NullCheck(L_79);
		bool L_80 = NamedVariable_get_IsNone_m281035543(L_79, /*hidden argument*/NULL);
		if (L_80)
		{
			goto IL_0279;
		}
	}
	{
		Hashtable_t1407064410 * L_81 = __this->get_hash_19();
		FsmVector3_t533912882 * L_82 = __this->get_lookAtVector_16();
		NullCheck(L_82);
		Vector3_t4282066566  L_83 = FsmVector3_get_Value_m2779135117(L_82, /*hidden argument*/NULL);
		Vector3_t4282066566  L_84 = L_83;
		Il2CppObject * L_85 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_84);
		NullCheck(L_81);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_81, _stringLiteral1258521840, L_85);
	}

IL_0279:
	{
		goto IL_029e;
	}

IL_027e:
	{
		Hashtable_t1407064410 * L_86 = __this->get_hash_19();
		FsmGameObject_t1697147867 * L_87 = __this->get_lookAtObject_15();
		NullCheck(L_87);
		GameObject_t3674682005 * L_88 = FsmGameObject_get_Value_m673294275(L_87, /*hidden argument*/NULL);
		NullCheck(L_88);
		Transform_t1659122786 * L_89 = GameObject_get_transform_m1278640159(L_88, /*hidden argument*/NULL);
		NullCheck(L_86);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_86, _stringLiteral1258521840, L_89);
	}

IL_029e:
	{
		FsmGameObject_t1697147867 * L_90 = __this->get_lookAtObject_15();
		NullCheck(L_90);
		bool L_91 = NamedVariable_get_IsNone_m281035543(L_90, /*hidden argument*/NULL);
		if (!L_91)
		{
			goto IL_02be;
		}
	}
	{
		FsmVector3_t533912882 * L_92 = __this->get_lookAtVector_16();
		NullCheck(L_92);
		bool L_93 = NamedVariable_get_IsNone_m281035543(L_92, /*hidden argument*/NULL);
		if (L_93)
		{
			goto IL_02f8;
		}
	}

IL_02be:
	{
		Hashtable_t1407064410 * L_94 = __this->get_hash_19();
		FsmFloat_t2134102846 * L_95 = __this->get_lookTime_17();
		NullCheck(L_95);
		bool L_96 = NamedVariable_get_IsNone_m281035543(L_95, /*hidden argument*/NULL);
		G_B29_0 = _stringLiteral2253828588;
		G_B29_1 = L_94;
		if (!L_96)
		{
			G_B30_0 = _stringLiteral2253828588;
			G_B30_1 = L_94;
			goto IL_02e3;
		}
	}
	{
		G_B31_0 = (0.0f);
		G_B31_1 = G_B29_0;
		G_B31_2 = G_B29_1;
		goto IL_02ee;
	}

IL_02e3:
	{
		FsmFloat_t2134102846 * L_97 = __this->get_lookTime_17();
		NullCheck(L_97);
		float L_98 = FsmFloat_get_Value_m4137923823(L_97, /*hidden argument*/NULL);
		G_B31_0 = L_98;
		G_B31_1 = G_B30_0;
		G_B31_2 = G_B30_1;
	}

IL_02ee:
	{
		float L_99 = G_B31_0;
		Il2CppObject * L_100 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_99);
		NullCheck(G_B31_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B31_2, G_B31_1, L_100);
	}

IL_02f8:
	{
		iTweenMoveUpdate_DoiTween_m1586830031(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveUpdate::OnUpdate()
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral747804969;
extern const uint32_t iTweenMoveUpdate_OnUpdate_m3564224458_MetadataUsageId;
extern "C"  void iTweenMoveUpdate_OnUpdate_m3564224458 (iTweenMoveUpdate_t733362340 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenMoveUpdate_OnUpdate_m3564224458_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	Hashtable_t1407064410 * G_B3_1 = NULL;
	String_t* G_B2_0 = NULL;
	Hashtable_t1407064410 * G_B2_1 = NULL;
	Vector3_t4282066566  G_B4_0;
	memset(&G_B4_0, 0, sizeof(G_B4_0));
	String_t* G_B4_1 = NULL;
	Hashtable_t1407064410 * G_B4_2 = NULL;
	{
		Hashtable_t1407064410 * L_0 = __this->get_hash_19();
		NullCheck(L_0);
		VirtActionInvoker1< Il2CppObject * >::Invoke(37 /* System.Void System.Collections.Hashtable::Remove(System.Object) */, L_0, _stringLiteral747804969);
		FsmGameObject_t1697147867 * L_1 = __this->get_transformPosition_10();
		NullCheck(L_1);
		bool L_2 = NamedVariable_get_IsNone_m281035543(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_005f;
		}
	}
	{
		Hashtable_t1407064410 * L_3 = __this->get_hash_19();
		FsmVector3_t533912882 * L_4 = __this->get_vectorPosition_11();
		NullCheck(L_4);
		bool L_5 = NamedVariable_get_IsNone_m281035543(L_4, /*hidden argument*/NULL);
		G_B2_0 = _stringLiteral747804969;
		G_B2_1 = L_3;
		if (!L_5)
		{
			G_B3_0 = _stringLiteral747804969;
			G_B3_1 = L_3;
			goto IL_0045;
		}
	}
	{
		Vector3_t4282066566  L_6 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B4_0 = L_6;
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		goto IL_0050;
	}

IL_0045:
	{
		FsmVector3_t533912882 * L_7 = __this->get_vectorPosition_11();
		NullCheck(L_7);
		Vector3_t4282066566  L_8 = FsmVector3_get_Value_m2779135117(L_7, /*hidden argument*/NULL);
		G_B4_0 = L_8;
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
	}

IL_0050:
	{
		Vector3_t4282066566  L_9 = G_B4_0;
		Il2CppObject * L_10 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_9);
		NullCheck(G_B4_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B4_2, G_B4_1, L_10);
		goto IL_0118;
	}

IL_005f:
	{
		FsmVector3_t533912882 * L_11 = __this->get_vectorPosition_11();
		NullCheck(L_11);
		bool L_12 = NamedVariable_get_IsNone_m281035543(L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0094;
		}
	}
	{
		Hashtable_t1407064410 * L_13 = __this->get_hash_19();
		FsmGameObject_t1697147867 * L_14 = __this->get_transformPosition_10();
		NullCheck(L_14);
		GameObject_t3674682005 * L_15 = FsmGameObject_get_Value_m673294275(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_t1659122786 * L_16 = GameObject_get_transform_m1278640159(L_15, /*hidden argument*/NULL);
		NullCheck(L_13);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_13, _stringLiteral747804969, L_16);
		goto IL_0118;
	}

IL_0094:
	{
		int32_t L_17 = __this->get_space_13();
		if (L_17)
		{
			goto IL_00de;
		}
	}
	{
		Hashtable_t1407064410 * L_18 = __this->get_hash_19();
		FsmGameObject_t1697147867 * L_19 = __this->get_transformPosition_10();
		NullCheck(L_19);
		GameObject_t3674682005 * L_20 = FsmGameObject_get_Value_m673294275(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_t1659122786 * L_21 = GameObject_get_transform_m1278640159(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		Vector3_t4282066566  L_22 = Transform_get_position_m2211398607(L_21, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_23 = __this->get_vectorPosition_11();
		NullCheck(L_23);
		Vector3_t4282066566  L_24 = FsmVector3_get_Value_m2779135117(L_23, /*hidden argument*/NULL);
		Vector3_t4282066566  L_25 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_22, L_24, /*hidden argument*/NULL);
		Vector3_t4282066566  L_26 = L_25;
		Il2CppObject * L_27 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_18);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_18, _stringLiteral747804969, L_27);
		goto IL_0118;
	}

IL_00de:
	{
		Hashtable_t1407064410 * L_28 = __this->get_hash_19();
		FsmGameObject_t1697147867 * L_29 = __this->get_transformPosition_10();
		NullCheck(L_29);
		GameObject_t3674682005 * L_30 = FsmGameObject_get_Value_m673294275(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		Transform_t1659122786 * L_31 = GameObject_get_transform_m1278640159(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		Vector3_t4282066566  L_32 = Transform_get_localPosition_m668140784(L_31, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_33 = __this->get_vectorPosition_11();
		NullCheck(L_33);
		Vector3_t4282066566  L_34 = FsmVector3_get_Value_m2779135117(L_33, /*hidden argument*/NULL);
		Vector3_t4282066566  L_35 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_32, L_34, /*hidden argument*/NULL);
		Vector3_t4282066566  L_36 = L_35;
		Il2CppObject * L_37 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_36);
		NullCheck(L_28);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_28, _stringLiteral747804969, L_37);
	}

IL_0118:
	{
		iTweenMoveUpdate_DoiTween_m1586830031(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveUpdate::OnExit()
extern "C"  void iTweenMoveUpdate_OnExit_m724143839 (iTweenMoveUpdate_t733362340 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveUpdate::DoiTween()
extern Il2CppClass* iTween_t3087282050_il2cpp_TypeInfo_var;
extern const uint32_t iTweenMoveUpdate_DoiTween_m1586830031_MetadataUsageId;
extern "C"  void iTweenMoveUpdate_DoiTween_m1586830031 (iTweenMoveUpdate_t733362340 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenMoveUpdate_DoiTween_m1586830031_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_go_20();
		Hashtable_t1407064410 * L_1 = __this->get_hash_19();
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		iTween_MoveUpdate_m3138392801(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenPause::.ctor()
extern "C"  void iTweenPause__ctor_m3972124468 (iTweenPause_t1146816066 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenPause::Reset()
extern "C"  void iTweenPause_Reset_m1618557409 (iTweenPause_t1146816066 * __this, const MethodInfo* method)
{
	{
		__this->set_iTweenType_10(0);
		__this->set_includeChildren_11((bool)0);
		__this->set_inScene_12((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenPause::OnEnter()
extern "C"  void iTweenPause_OnEnter_m2672383499 (iTweenPause_t1146816066 * __this, const MethodInfo* method)
{
	{
		FsmStateAction_OnEnter_m160148417(__this, /*hidden argument*/NULL);
		iTweenPause_DoiTween_m2690642077(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenPause::DoiTween()
extern const Il2CppType* iTweenFSMType_t470630072_0_0_0_var;
extern Il2CppClass* iTween_t3087282050_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* iTweenFSMType_t470630072_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2862688501_il2cpp_TypeInfo_var;
extern const uint32_t iTweenPause_DoiTween_m2690642077_MetadataUsageId;
extern "C"  void iTweenPause_DoiTween_m2690642077 (iTweenPause_t1146816066 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenPause_DoiTween_m2690642077_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		int32_t L_0 = __this->get_iTweenType_10();
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		iTween_Pause_m3842862317(NULL /*static, unused*/, /*hidden argument*/NULL);
		goto IL_0089;
	}

IL_0015:
	{
		bool L_1 = __this->get_inScene_12();
		if (!L_1)
		{
			goto IL_0044;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(iTweenFSMType_t470630072_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_3 = __this->get_iTweenType_10();
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(iTweenFSMType_t470630072_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		String_t* L_6 = Enum_GetName_m3333121265(NULL /*static, unused*/, L_2, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		iTween_Pause_m4191640373(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		goto IL_0089;
	}

IL_0044:
	{
		Fsm_t1527112426 * L_7 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_8 = __this->get_gameObject_9();
		NullCheck(L_7);
		GameObject_t3674682005 * L_9 = Fsm_GetOwnerDefaultTarget_m846013999(L_7, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		GameObject_t3674682005 * L_10 = V_0;
		bool L_11 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_10, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0063;
		}
	}
	{
		return;
	}

IL_0063:
	{
		GameObject_t3674682005 * L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_13 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(iTweenFSMType_t470630072_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_14 = __this->get_iTweenType_10();
		int32_t L_15 = L_14;
		Il2CppObject * L_16 = Box(iTweenFSMType_t470630072_il2cpp_TypeInfo_var, &L_15);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		String_t* L_17 = Enum_GetName_m3333121265(NULL /*static, unused*/, L_13, L_16, /*hidden argument*/NULL);
		bool L_18 = __this->get_includeChildren_11();
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		iTween_Pause_m1980796220(NULL /*static, unused*/, L_12, L_17, L_18, /*hidden argument*/NULL);
	}

IL_0089:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchPosition::.ctor()
extern "C"  void iTweenPunchPosition__ctor_m3218296531 (iTweenPunchPosition_t2465480643 * __this, const MethodInfo* method)
{
	{
		iTweenFsmAction__ctor_m2960815636(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchPosition::Reset()
extern Il2CppClass* FsmString_t952858651_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern const uint32_t iTweenPunchPosition_Reset_m864729472_MetadataUsageId;
extern "C"  void iTweenPunchPosition_Reset_m864729472 (iTweenPunchPosition_t2465480643 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenPunchPosition_Reset_m864729472_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmString_t952858651 * V_0 = NULL;
	FsmVector3_t533912882 * V_1 = NULL;
	{
		iTweenFsmAction_Reset_m607248577(__this, /*hidden argument*/NULL);
		FsmString_t952858651 * L_0 = (FsmString_t952858651 *)il2cpp_codegen_object_new(FsmString_t952858651_il2cpp_TypeInfo_var);
		FsmString__ctor_m2679108260(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmString_t952858651 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmString_t952858651 * L_2 = V_0;
		__this->set_id_18(L_2);
		FsmFloat_t2134102846 * L_3 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_time_20(L_3);
		FsmFloat_t2134102846 * L_4 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_delay_21(L_4);
		__this->set_loopType_22(0);
		FsmVector3_t533912882 * L_5 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_5, /*hidden argument*/NULL);
		V_1 = L_5;
		FsmVector3_t533912882 * L_6 = V_1;
		NullCheck(L_6);
		NamedVariable_set_UseVariable_m4266138971(L_6, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_7 = V_1;
		__this->set_vector_19(L_7);
		__this->set_space_23(0);
		__this->set_axis_24(0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchPosition::OnEnter()
extern "C"  void iTweenPunchPosition_OnEnter_m4093209066 (iTweenPunchPosition_t2465480643 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnEnteriTween_m3024539459(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_loopType_22();
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		iTweenFsmAction_IsLoop_m2539277653(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_001e:
	{
		iTweenPunchPosition_DoiTween_m3786561694(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchPosition::OnExit()
extern "C"  void iTweenPunchPosition_OnExit_m2496263278 (iTweenPunchPosition_t2465480643 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnExitiTween_m1981625761(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchPosition::DoiTween()
extern const Il2CppType* AxisRestriction_t3260241011_0_0_0_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* LoopType_t1485160459_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* Space_t4209342076_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* AxisRestriction_t3260241011_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2862688501_il2cpp_TypeInfo_var;
extern Il2CppClass* iTween_t3087282050_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral107028782;
extern Il2CppCodeGenString* _stringLiteral2881114200;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral3560141;
extern Il2CppCodeGenString* _stringLiteral95467907;
extern Il2CppCodeGenString* _stringLiteral2258461662;
extern Il2CppCodeGenString* _stringLiteral2105864216;
extern Il2CppCodeGenString* _stringLiteral338257242;
extern Il2CppCodeGenString* _stringLiteral3696326558;
extern Il2CppCodeGenString* _stringLiteral2987624931;
extern Il2CppCodeGenString* _stringLiteral647687137;
extern Il2CppCodeGenString* _stringLiteral2052738345;
extern Il2CppCodeGenString* _stringLiteral1067506955;
extern Il2CppCodeGenString* _stringLiteral109637894;
extern Il2CppCodeGenString* _stringLiteral3008417;
extern const uint32_t iTweenPunchPosition_DoiTween_m3786561694_MetadataUsageId;
extern "C"  void iTweenPunchPosition_DoiTween_m3786561694 (iTweenPunchPosition_t2465480643 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenPunchPosition_DoiTween_m3786561694_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B7_0 = 0;
	ObjectU5BU5D_t1108656482* G_B7_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B7_2 = NULL;
	GameObject_t3674682005 * G_B7_3 = NULL;
	int32_t G_B6_0 = 0;
	ObjectU5BU5D_t1108656482* G_B6_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B6_2 = NULL;
	GameObject_t3674682005 * G_B6_3 = NULL;
	String_t* G_B8_0 = NULL;
	int32_t G_B8_1 = 0;
	ObjectU5BU5D_t1108656482* G_B8_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B8_3 = NULL;
	GameObject_t3674682005 * G_B8_4 = NULL;
	int32_t G_B10_0 = 0;
	ObjectU5BU5D_t1108656482* G_B10_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B10_2 = NULL;
	GameObject_t3674682005 * G_B10_3 = NULL;
	int32_t G_B9_0 = 0;
	ObjectU5BU5D_t1108656482* G_B9_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B9_2 = NULL;
	GameObject_t3674682005 * G_B9_3 = NULL;
	float G_B11_0 = 0.0f;
	int32_t G_B11_1 = 0;
	ObjectU5BU5D_t1108656482* G_B11_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B11_3 = NULL;
	GameObject_t3674682005 * G_B11_4 = NULL;
	int32_t G_B13_0 = 0;
	ObjectU5BU5D_t1108656482* G_B13_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B13_2 = NULL;
	GameObject_t3674682005 * G_B13_3 = NULL;
	int32_t G_B12_0 = 0;
	ObjectU5BU5D_t1108656482* G_B12_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B12_2 = NULL;
	GameObject_t3674682005 * G_B12_3 = NULL;
	float G_B14_0 = 0.0f;
	int32_t G_B14_1 = 0;
	ObjectU5BU5D_t1108656482* G_B14_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B14_3 = NULL;
	GameObject_t3674682005 * G_B14_4 = NULL;
	int32_t G_B16_0 = 0;
	ObjectU5BU5D_t1108656482* G_B16_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B16_2 = NULL;
	GameObject_t3674682005 * G_B16_3 = NULL;
	int32_t G_B15_0 = 0;
	ObjectU5BU5D_t1108656482* G_B15_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B15_2 = NULL;
	GameObject_t3674682005 * G_B15_3 = NULL;
	int32_t G_B17_0 = 0;
	int32_t G_B17_1 = 0;
	ObjectU5BU5D_t1108656482* G_B17_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B17_3 = NULL;
	GameObject_t3674682005 * G_B17_4 = NULL;
	int32_t G_B19_0 = 0;
	ObjectU5BU5D_t1108656482* G_B19_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B19_2 = NULL;
	GameObject_t3674682005 * G_B19_3 = NULL;
	int32_t G_B18_0 = 0;
	ObjectU5BU5D_t1108656482* G_B18_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B18_2 = NULL;
	GameObject_t3674682005 * G_B18_3 = NULL;
	String_t* G_B20_0 = NULL;
	int32_t G_B20_1 = 0;
	ObjectU5BU5D_t1108656482* G_B20_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B20_3 = NULL;
	GameObject_t3674682005 * G_B20_4 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_17();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		Vector3_t4282066566  L_5 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_5;
		FsmVector3_t533912882 * L_6 = __this->get_vector_19();
		NullCheck(L_6);
		bool L_7 = NamedVariable_get_IsNone_m281035543(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003a;
		}
	}
	{
		goto IL_0046;
	}

IL_003a:
	{
		FsmVector3_t533912882 * L_8 = __this->get_vector_19();
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = FsmVector3_get_Value_m2779135117(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
	}

IL_0046:
	{
		((iTweenFsmAction_t410382178 *)__this)->set_itweenType_15(_stringLiteral107028782);
		GameObject_t3674682005 * L_10 = V_0;
		ObjectU5BU5D_t1108656482* L_11 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)24)));
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		ArrayElementTypeCheck (L_11, _stringLiteral2881114200);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2881114200);
		ObjectU5BU5D_t1108656482* L_12 = L_11;
		Vector3_t4282066566  L_13 = V_1;
		Vector3_t4282066566  L_14 = L_13;
		Il2CppObject * L_15 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 1);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_15);
		ObjectU5BU5D_t1108656482* L_16 = L_12;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 2);
		ArrayElementTypeCheck (L_16, _stringLiteral3373707);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3373707);
		ObjectU5BU5D_t1108656482* L_17 = L_16;
		FsmString_t952858651 * L_18 = __this->get_id_18();
		NullCheck(L_18);
		bool L_19 = NamedVariable_get_IsNone_m281035543(L_18, /*hidden argument*/NULL);
		G_B6_0 = 3;
		G_B6_1 = L_17;
		G_B6_2 = L_17;
		G_B6_3 = L_10;
		if (!L_19)
		{
			G_B7_0 = 3;
			G_B7_1 = L_17;
			G_B7_2 = L_17;
			G_B7_3 = L_10;
			goto IL_008e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B8_0 = L_20;
		G_B8_1 = G_B6_0;
		G_B8_2 = G_B6_1;
		G_B8_3 = G_B6_2;
		G_B8_4 = G_B6_3;
		goto IL_0099;
	}

IL_008e:
	{
		FsmString_t952858651 * L_21 = __this->get_id_18();
		NullCheck(L_21);
		String_t* L_22 = FsmString_get_Value_m872383149(L_21, /*hidden argument*/NULL);
		G_B8_0 = L_22;
		G_B8_1 = G_B7_0;
		G_B8_2 = G_B7_1;
		G_B8_3 = G_B7_2;
		G_B8_4 = G_B7_3;
	}

IL_0099:
	{
		NullCheck(G_B8_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B8_2, G_B8_1);
		ArrayElementTypeCheck (G_B8_2, G_B8_0);
		(G_B8_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B8_1), (Il2CppObject *)G_B8_0);
		ObjectU5BU5D_t1108656482* L_23 = G_B8_3;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 4);
		ArrayElementTypeCheck (L_23, _stringLiteral3560141);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral3560141);
		ObjectU5BU5D_t1108656482* L_24 = L_23;
		FsmFloat_t2134102846 * L_25 = __this->get_time_20();
		NullCheck(L_25);
		bool L_26 = NamedVariable_get_IsNone_m281035543(L_25, /*hidden argument*/NULL);
		G_B9_0 = 5;
		G_B9_1 = L_24;
		G_B9_2 = L_24;
		G_B9_3 = G_B8_4;
		if (!L_26)
		{
			G_B10_0 = 5;
			G_B10_1 = L_24;
			G_B10_2 = L_24;
			G_B10_3 = G_B8_4;
			goto IL_00be;
		}
	}
	{
		G_B11_0 = (1.0f);
		G_B11_1 = G_B9_0;
		G_B11_2 = G_B9_1;
		G_B11_3 = G_B9_2;
		G_B11_4 = G_B9_3;
		goto IL_00c9;
	}

IL_00be:
	{
		FsmFloat_t2134102846 * L_27 = __this->get_time_20();
		NullCheck(L_27);
		float L_28 = FsmFloat_get_Value_m4137923823(L_27, /*hidden argument*/NULL);
		G_B11_0 = L_28;
		G_B11_1 = G_B10_0;
		G_B11_2 = G_B10_1;
		G_B11_3 = G_B10_2;
		G_B11_4 = G_B10_3;
	}

IL_00c9:
	{
		float L_29 = G_B11_0;
		Il2CppObject * L_30 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_29);
		NullCheck(G_B11_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B11_2, G_B11_1);
		ArrayElementTypeCheck (G_B11_2, L_30);
		(G_B11_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B11_1), (Il2CppObject *)L_30);
		ObjectU5BU5D_t1108656482* L_31 = G_B11_3;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 6);
		ArrayElementTypeCheck (L_31, _stringLiteral95467907);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral95467907);
		ObjectU5BU5D_t1108656482* L_32 = L_31;
		FsmFloat_t2134102846 * L_33 = __this->get_delay_21();
		NullCheck(L_33);
		bool L_34 = NamedVariable_get_IsNone_m281035543(L_33, /*hidden argument*/NULL);
		G_B12_0 = 7;
		G_B12_1 = L_32;
		G_B12_2 = L_32;
		G_B12_3 = G_B11_4;
		if (!L_34)
		{
			G_B13_0 = 7;
			G_B13_1 = L_32;
			G_B13_2 = L_32;
			G_B13_3 = G_B11_4;
			goto IL_00f3;
		}
	}
	{
		G_B14_0 = (0.0f);
		G_B14_1 = G_B12_0;
		G_B14_2 = G_B12_1;
		G_B14_3 = G_B12_2;
		G_B14_4 = G_B12_3;
		goto IL_00fe;
	}

IL_00f3:
	{
		FsmFloat_t2134102846 * L_35 = __this->get_delay_21();
		NullCheck(L_35);
		float L_36 = FsmFloat_get_Value_m4137923823(L_35, /*hidden argument*/NULL);
		G_B14_0 = L_36;
		G_B14_1 = G_B13_0;
		G_B14_2 = G_B13_1;
		G_B14_3 = G_B13_2;
		G_B14_4 = G_B13_3;
	}

IL_00fe:
	{
		float L_37 = G_B14_0;
		Il2CppObject * L_38 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_37);
		NullCheck(G_B14_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B14_2, G_B14_1);
		ArrayElementTypeCheck (G_B14_2, L_38);
		(G_B14_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B14_1), (Il2CppObject *)L_38);
		ObjectU5BU5D_t1108656482* L_39 = G_B14_3;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, 8);
		ArrayElementTypeCheck (L_39, _stringLiteral2258461662);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral2258461662);
		ObjectU5BU5D_t1108656482* L_40 = L_39;
		int32_t L_41 = __this->get_loopType_22();
		int32_t L_42 = L_41;
		Il2CppObject * L_43 = Box(LoopType_t1485160459_il2cpp_TypeInfo_var, &L_42);
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, ((int32_t)9));
		ArrayElementTypeCheck (L_40, L_43);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)L_43);
		ObjectU5BU5D_t1108656482* L_44 = L_40;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)10));
		ArrayElementTypeCheck (L_44, _stringLiteral2105864216);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Il2CppObject *)_stringLiteral2105864216);
		ObjectU5BU5D_t1108656482* L_45 = L_44;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, ((int32_t)11));
		ArrayElementTypeCheck (L_45, _stringLiteral338257242);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (Il2CppObject *)_stringLiteral338257242);
		ObjectU5BU5D_t1108656482* L_46 = L_45;
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, ((int32_t)12));
		ArrayElementTypeCheck (L_46, _stringLiteral3696326558);
		(L_46)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (Il2CppObject *)_stringLiteral3696326558);
		ObjectU5BU5D_t1108656482* L_47 = L_46;
		int32_t L_48 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_49 = L_48;
		Il2CppObject * L_50 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_49);
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, ((int32_t)13));
		ArrayElementTypeCheck (L_47, L_50);
		(L_47)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (Il2CppObject *)L_50);
		ObjectU5BU5D_t1108656482* L_51 = L_47;
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, ((int32_t)14));
		ArrayElementTypeCheck (L_51, _stringLiteral2987624931);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (Il2CppObject *)_stringLiteral2987624931);
		ObjectU5BU5D_t1108656482* L_52 = L_51;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, ((int32_t)15));
		ArrayElementTypeCheck (L_52, _stringLiteral647687137);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (Il2CppObject *)_stringLiteral647687137);
		ObjectU5BU5D_t1108656482* L_53 = L_52;
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, ((int32_t)16));
		ArrayElementTypeCheck (L_53, _stringLiteral2052738345);
		(L_53)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (Il2CppObject *)_stringLiteral2052738345);
		ObjectU5BU5D_t1108656482* L_54 = L_53;
		int32_t L_55 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_56 = L_55;
		Il2CppObject * L_57 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_56);
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, ((int32_t)17));
		ArrayElementTypeCheck (L_54, L_57);
		(L_54)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (Il2CppObject *)L_57);
		ObjectU5BU5D_t1108656482* L_58 = L_54;
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, ((int32_t)18));
		ArrayElementTypeCheck (L_58, _stringLiteral1067506955);
		(L_58)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (Il2CppObject *)_stringLiteral1067506955);
		ObjectU5BU5D_t1108656482* L_59 = L_58;
		FsmBool_t1075959796 * L_60 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_60);
		bool L_61 = NamedVariable_get_IsNone_m281035543(L_60, /*hidden argument*/NULL);
		G_B15_0 = ((int32_t)19);
		G_B15_1 = L_59;
		G_B15_2 = L_59;
		G_B15_3 = G_B14_4;
		if (!L_61)
		{
			G_B16_0 = ((int32_t)19);
			G_B16_1 = L_59;
			G_B16_2 = L_59;
			G_B16_3 = G_B14_4;
			goto IL_0191;
		}
	}
	{
		G_B17_0 = 0;
		G_B17_1 = G_B15_0;
		G_B17_2 = G_B15_1;
		G_B17_3 = G_B15_2;
		G_B17_4 = G_B15_3;
		goto IL_019c;
	}

IL_0191:
	{
		FsmBool_t1075959796 * L_62 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_62);
		bool L_63 = FsmBool_get_Value_m3101329097(L_62, /*hidden argument*/NULL);
		G_B17_0 = ((int32_t)(L_63));
		G_B17_1 = G_B16_0;
		G_B17_2 = G_B16_1;
		G_B17_3 = G_B16_2;
		G_B17_4 = G_B16_3;
	}

IL_019c:
	{
		bool L_64 = ((bool)G_B17_0);
		Il2CppObject * L_65 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_64);
		NullCheck(G_B17_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B17_2, G_B17_1);
		ArrayElementTypeCheck (G_B17_2, L_65);
		(G_B17_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B17_1), (Il2CppObject *)L_65);
		ObjectU5BU5D_t1108656482* L_66 = G_B17_3;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, ((int32_t)20));
		ArrayElementTypeCheck (L_66, _stringLiteral109637894);
		(L_66)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)20)), (Il2CppObject *)_stringLiteral109637894);
		ObjectU5BU5D_t1108656482* L_67 = L_66;
		int32_t L_68 = __this->get_space_23();
		int32_t L_69 = L_68;
		Il2CppObject * L_70 = Box(Space_t4209342076_il2cpp_TypeInfo_var, &L_69);
		NullCheck(L_67);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_67, ((int32_t)21));
		ArrayElementTypeCheck (L_67, L_70);
		(L_67)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)21)), (Il2CppObject *)L_70);
		ObjectU5BU5D_t1108656482* L_71 = L_67;
		NullCheck(L_71);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_71, ((int32_t)22));
		ArrayElementTypeCheck (L_71, _stringLiteral3008417);
		(L_71)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)22)), (Il2CppObject *)_stringLiteral3008417);
		ObjectU5BU5D_t1108656482* L_72 = L_71;
		int32_t L_73 = __this->get_axis_24();
		G_B18_0 = ((int32_t)23);
		G_B18_1 = L_72;
		G_B18_2 = L_72;
		G_B18_3 = G_B17_4;
		if (L_73)
		{
			G_B19_0 = ((int32_t)23);
			G_B19_1 = L_72;
			G_B19_2 = L_72;
			G_B19_3 = G_B17_4;
			goto IL_01db;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_74 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B20_0 = L_74;
		G_B20_1 = G_B18_0;
		G_B20_2 = G_B18_1;
		G_B20_3 = G_B18_2;
		G_B20_4 = G_B18_3;
		goto IL_01f5;
	}

IL_01db:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_75 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(AxisRestriction_t3260241011_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_76 = __this->get_axis_24();
		int32_t L_77 = L_76;
		Il2CppObject * L_78 = Box(AxisRestriction_t3260241011_il2cpp_TypeInfo_var, &L_77);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		String_t* L_79 = Enum_GetName_m3333121265(NULL /*static, unused*/, L_75, L_78, /*hidden argument*/NULL);
		G_B20_0 = L_79;
		G_B20_1 = G_B19_0;
		G_B20_2 = G_B19_1;
		G_B20_3 = G_B19_2;
		G_B20_4 = G_B19_3;
	}

IL_01f5:
	{
		NullCheck(G_B20_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B20_2, G_B20_1);
		ArrayElementTypeCheck (G_B20_2, G_B20_0);
		(G_B20_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B20_1), (Il2CppObject *)G_B20_0);
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		Hashtable_t1407064410 * L_80 = iTween_Hash_m2099495140(NULL /*static, unused*/, G_B20_3, /*hidden argument*/NULL);
		iTween_PunchPosition_m459194328(NULL /*static, unused*/, G_B20_4, L_80, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchRotation::.ctor()
extern "C"  void iTweenPunchRotation__ctor_m2887002142 (iTweenPunchRotation_t1677375000 * __this, const MethodInfo* method)
{
	{
		iTweenFsmAction__ctor_m2960815636(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchRotation::Reset()
extern Il2CppClass* FsmString_t952858651_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern const uint32_t iTweenPunchRotation_Reset_m533435083_MetadataUsageId;
extern "C"  void iTweenPunchRotation_Reset_m533435083 (iTweenPunchRotation_t1677375000 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenPunchRotation_Reset_m533435083_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmString_t952858651 * V_0 = NULL;
	FsmVector3_t533912882 * V_1 = NULL;
	{
		iTweenFsmAction_Reset_m607248577(__this, /*hidden argument*/NULL);
		FsmString_t952858651 * L_0 = (FsmString_t952858651 *)il2cpp_codegen_object_new(FsmString_t952858651_il2cpp_TypeInfo_var);
		FsmString__ctor_m2679108260(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmString_t952858651 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmString_t952858651 * L_2 = V_0;
		__this->set_id_18(L_2);
		FsmFloat_t2134102846 * L_3 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_time_20(L_3);
		FsmFloat_t2134102846 * L_4 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_delay_21(L_4);
		__this->set_loopType_22(0);
		FsmVector3_t533912882 * L_5 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_5, /*hidden argument*/NULL);
		V_1 = L_5;
		FsmVector3_t533912882 * L_6 = V_1;
		NullCheck(L_6);
		NamedVariable_set_UseVariable_m4266138971(L_6, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_7 = V_1;
		__this->set_vector_19(L_7);
		__this->set_space_23(0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchRotation::OnEnter()
extern "C"  void iTweenPunchRotation_OnEnter_m3546881141 (iTweenPunchRotation_t1677375000 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnEnteriTween_m3024539459(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_loopType_22();
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		iTweenFsmAction_IsLoop_m2539277653(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_001e:
	{
		iTweenPunchRotation_DoiTween_m4030265203(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchRotation::OnExit()
extern "C"  void iTweenPunchRotation_OnExit_m816071811 (iTweenPunchRotation_t1677375000 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnExitiTween_m1981625761(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchRotation::DoiTween()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* LoopType_t1485160459_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* Space_t4209342076_il2cpp_TypeInfo_var;
extern Il2CppClass* iTween_t3087282050_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral107028782;
extern Il2CppCodeGenString* _stringLiteral2881114200;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral3560141;
extern Il2CppCodeGenString* _stringLiteral95467907;
extern Il2CppCodeGenString* _stringLiteral2258461662;
extern Il2CppCodeGenString* _stringLiteral2105864216;
extern Il2CppCodeGenString* _stringLiteral338257242;
extern Il2CppCodeGenString* _stringLiteral3696326558;
extern Il2CppCodeGenString* _stringLiteral2987624931;
extern Il2CppCodeGenString* _stringLiteral647687137;
extern Il2CppCodeGenString* _stringLiteral2052738345;
extern Il2CppCodeGenString* _stringLiteral1067506955;
extern Il2CppCodeGenString* _stringLiteral109637894;
extern const uint32_t iTweenPunchRotation_DoiTween_m4030265203_MetadataUsageId;
extern "C"  void iTweenPunchRotation_DoiTween_m4030265203 (iTweenPunchRotation_t1677375000 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenPunchRotation_DoiTween_m4030265203_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B7_0 = 0;
	ObjectU5BU5D_t1108656482* G_B7_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B7_2 = NULL;
	GameObject_t3674682005 * G_B7_3 = NULL;
	int32_t G_B6_0 = 0;
	ObjectU5BU5D_t1108656482* G_B6_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B6_2 = NULL;
	GameObject_t3674682005 * G_B6_3 = NULL;
	String_t* G_B8_0 = NULL;
	int32_t G_B8_1 = 0;
	ObjectU5BU5D_t1108656482* G_B8_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B8_3 = NULL;
	GameObject_t3674682005 * G_B8_4 = NULL;
	int32_t G_B10_0 = 0;
	ObjectU5BU5D_t1108656482* G_B10_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B10_2 = NULL;
	GameObject_t3674682005 * G_B10_3 = NULL;
	int32_t G_B9_0 = 0;
	ObjectU5BU5D_t1108656482* G_B9_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B9_2 = NULL;
	GameObject_t3674682005 * G_B9_3 = NULL;
	float G_B11_0 = 0.0f;
	int32_t G_B11_1 = 0;
	ObjectU5BU5D_t1108656482* G_B11_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B11_3 = NULL;
	GameObject_t3674682005 * G_B11_4 = NULL;
	int32_t G_B13_0 = 0;
	ObjectU5BU5D_t1108656482* G_B13_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B13_2 = NULL;
	GameObject_t3674682005 * G_B13_3 = NULL;
	int32_t G_B12_0 = 0;
	ObjectU5BU5D_t1108656482* G_B12_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B12_2 = NULL;
	GameObject_t3674682005 * G_B12_3 = NULL;
	float G_B14_0 = 0.0f;
	int32_t G_B14_1 = 0;
	ObjectU5BU5D_t1108656482* G_B14_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B14_3 = NULL;
	GameObject_t3674682005 * G_B14_4 = NULL;
	int32_t G_B16_0 = 0;
	ObjectU5BU5D_t1108656482* G_B16_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B16_2 = NULL;
	GameObject_t3674682005 * G_B16_3 = NULL;
	int32_t G_B15_0 = 0;
	ObjectU5BU5D_t1108656482* G_B15_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B15_2 = NULL;
	GameObject_t3674682005 * G_B15_3 = NULL;
	int32_t G_B17_0 = 0;
	int32_t G_B17_1 = 0;
	ObjectU5BU5D_t1108656482* G_B17_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B17_3 = NULL;
	GameObject_t3674682005 * G_B17_4 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_17();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		Vector3_t4282066566  L_5 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_5;
		FsmVector3_t533912882 * L_6 = __this->get_vector_19();
		NullCheck(L_6);
		bool L_7 = NamedVariable_get_IsNone_m281035543(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003a;
		}
	}
	{
		goto IL_0046;
	}

IL_003a:
	{
		FsmVector3_t533912882 * L_8 = __this->get_vector_19();
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = FsmVector3_get_Value_m2779135117(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
	}

IL_0046:
	{
		((iTweenFsmAction_t410382178 *)__this)->set_itweenType_15(_stringLiteral107028782);
		GameObject_t3674682005 * L_10 = V_0;
		ObjectU5BU5D_t1108656482* L_11 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)22)));
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		ArrayElementTypeCheck (L_11, _stringLiteral2881114200);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2881114200);
		ObjectU5BU5D_t1108656482* L_12 = L_11;
		Vector3_t4282066566  L_13 = V_1;
		Vector3_t4282066566  L_14 = L_13;
		Il2CppObject * L_15 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 1);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_15);
		ObjectU5BU5D_t1108656482* L_16 = L_12;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 2);
		ArrayElementTypeCheck (L_16, _stringLiteral3373707);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3373707);
		ObjectU5BU5D_t1108656482* L_17 = L_16;
		FsmString_t952858651 * L_18 = __this->get_id_18();
		NullCheck(L_18);
		bool L_19 = NamedVariable_get_IsNone_m281035543(L_18, /*hidden argument*/NULL);
		G_B6_0 = 3;
		G_B6_1 = L_17;
		G_B6_2 = L_17;
		G_B6_3 = L_10;
		if (!L_19)
		{
			G_B7_0 = 3;
			G_B7_1 = L_17;
			G_B7_2 = L_17;
			G_B7_3 = L_10;
			goto IL_008e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B8_0 = L_20;
		G_B8_1 = G_B6_0;
		G_B8_2 = G_B6_1;
		G_B8_3 = G_B6_2;
		G_B8_4 = G_B6_3;
		goto IL_0099;
	}

IL_008e:
	{
		FsmString_t952858651 * L_21 = __this->get_id_18();
		NullCheck(L_21);
		String_t* L_22 = FsmString_get_Value_m872383149(L_21, /*hidden argument*/NULL);
		G_B8_0 = L_22;
		G_B8_1 = G_B7_0;
		G_B8_2 = G_B7_1;
		G_B8_3 = G_B7_2;
		G_B8_4 = G_B7_3;
	}

IL_0099:
	{
		NullCheck(G_B8_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B8_2, G_B8_1);
		ArrayElementTypeCheck (G_B8_2, G_B8_0);
		(G_B8_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B8_1), (Il2CppObject *)G_B8_0);
		ObjectU5BU5D_t1108656482* L_23 = G_B8_3;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 4);
		ArrayElementTypeCheck (L_23, _stringLiteral3560141);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral3560141);
		ObjectU5BU5D_t1108656482* L_24 = L_23;
		FsmFloat_t2134102846 * L_25 = __this->get_time_20();
		NullCheck(L_25);
		bool L_26 = NamedVariable_get_IsNone_m281035543(L_25, /*hidden argument*/NULL);
		G_B9_0 = 5;
		G_B9_1 = L_24;
		G_B9_2 = L_24;
		G_B9_3 = G_B8_4;
		if (!L_26)
		{
			G_B10_0 = 5;
			G_B10_1 = L_24;
			G_B10_2 = L_24;
			G_B10_3 = G_B8_4;
			goto IL_00be;
		}
	}
	{
		G_B11_0 = (1.0f);
		G_B11_1 = G_B9_0;
		G_B11_2 = G_B9_1;
		G_B11_3 = G_B9_2;
		G_B11_4 = G_B9_3;
		goto IL_00c9;
	}

IL_00be:
	{
		FsmFloat_t2134102846 * L_27 = __this->get_time_20();
		NullCheck(L_27);
		float L_28 = FsmFloat_get_Value_m4137923823(L_27, /*hidden argument*/NULL);
		G_B11_0 = L_28;
		G_B11_1 = G_B10_0;
		G_B11_2 = G_B10_1;
		G_B11_3 = G_B10_2;
		G_B11_4 = G_B10_3;
	}

IL_00c9:
	{
		float L_29 = G_B11_0;
		Il2CppObject * L_30 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_29);
		NullCheck(G_B11_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B11_2, G_B11_1);
		ArrayElementTypeCheck (G_B11_2, L_30);
		(G_B11_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B11_1), (Il2CppObject *)L_30);
		ObjectU5BU5D_t1108656482* L_31 = G_B11_3;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 6);
		ArrayElementTypeCheck (L_31, _stringLiteral95467907);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral95467907);
		ObjectU5BU5D_t1108656482* L_32 = L_31;
		FsmFloat_t2134102846 * L_33 = __this->get_delay_21();
		NullCheck(L_33);
		bool L_34 = NamedVariable_get_IsNone_m281035543(L_33, /*hidden argument*/NULL);
		G_B12_0 = 7;
		G_B12_1 = L_32;
		G_B12_2 = L_32;
		G_B12_3 = G_B11_4;
		if (!L_34)
		{
			G_B13_0 = 7;
			G_B13_1 = L_32;
			G_B13_2 = L_32;
			G_B13_3 = G_B11_4;
			goto IL_00f3;
		}
	}
	{
		G_B14_0 = (0.0f);
		G_B14_1 = G_B12_0;
		G_B14_2 = G_B12_1;
		G_B14_3 = G_B12_2;
		G_B14_4 = G_B12_3;
		goto IL_00fe;
	}

IL_00f3:
	{
		FsmFloat_t2134102846 * L_35 = __this->get_delay_21();
		NullCheck(L_35);
		float L_36 = FsmFloat_get_Value_m4137923823(L_35, /*hidden argument*/NULL);
		G_B14_0 = L_36;
		G_B14_1 = G_B13_0;
		G_B14_2 = G_B13_1;
		G_B14_3 = G_B13_2;
		G_B14_4 = G_B13_3;
	}

IL_00fe:
	{
		float L_37 = G_B14_0;
		Il2CppObject * L_38 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_37);
		NullCheck(G_B14_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B14_2, G_B14_1);
		ArrayElementTypeCheck (G_B14_2, L_38);
		(G_B14_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B14_1), (Il2CppObject *)L_38);
		ObjectU5BU5D_t1108656482* L_39 = G_B14_3;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, 8);
		ArrayElementTypeCheck (L_39, _stringLiteral2258461662);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral2258461662);
		ObjectU5BU5D_t1108656482* L_40 = L_39;
		int32_t L_41 = __this->get_loopType_22();
		int32_t L_42 = L_41;
		Il2CppObject * L_43 = Box(LoopType_t1485160459_il2cpp_TypeInfo_var, &L_42);
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, ((int32_t)9));
		ArrayElementTypeCheck (L_40, L_43);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)L_43);
		ObjectU5BU5D_t1108656482* L_44 = L_40;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)10));
		ArrayElementTypeCheck (L_44, _stringLiteral2105864216);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Il2CppObject *)_stringLiteral2105864216);
		ObjectU5BU5D_t1108656482* L_45 = L_44;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, ((int32_t)11));
		ArrayElementTypeCheck (L_45, _stringLiteral338257242);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (Il2CppObject *)_stringLiteral338257242);
		ObjectU5BU5D_t1108656482* L_46 = L_45;
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, ((int32_t)12));
		ArrayElementTypeCheck (L_46, _stringLiteral3696326558);
		(L_46)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (Il2CppObject *)_stringLiteral3696326558);
		ObjectU5BU5D_t1108656482* L_47 = L_46;
		int32_t L_48 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_49 = L_48;
		Il2CppObject * L_50 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_49);
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, ((int32_t)13));
		ArrayElementTypeCheck (L_47, L_50);
		(L_47)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (Il2CppObject *)L_50);
		ObjectU5BU5D_t1108656482* L_51 = L_47;
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, ((int32_t)14));
		ArrayElementTypeCheck (L_51, _stringLiteral2987624931);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (Il2CppObject *)_stringLiteral2987624931);
		ObjectU5BU5D_t1108656482* L_52 = L_51;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, ((int32_t)15));
		ArrayElementTypeCheck (L_52, _stringLiteral647687137);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (Il2CppObject *)_stringLiteral647687137);
		ObjectU5BU5D_t1108656482* L_53 = L_52;
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, ((int32_t)16));
		ArrayElementTypeCheck (L_53, _stringLiteral2052738345);
		(L_53)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (Il2CppObject *)_stringLiteral2052738345);
		ObjectU5BU5D_t1108656482* L_54 = L_53;
		int32_t L_55 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_56 = L_55;
		Il2CppObject * L_57 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_56);
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, ((int32_t)17));
		ArrayElementTypeCheck (L_54, L_57);
		(L_54)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (Il2CppObject *)L_57);
		ObjectU5BU5D_t1108656482* L_58 = L_54;
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, ((int32_t)18));
		ArrayElementTypeCheck (L_58, _stringLiteral1067506955);
		(L_58)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (Il2CppObject *)_stringLiteral1067506955);
		ObjectU5BU5D_t1108656482* L_59 = L_58;
		FsmBool_t1075959796 * L_60 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_60);
		bool L_61 = NamedVariable_get_IsNone_m281035543(L_60, /*hidden argument*/NULL);
		G_B15_0 = ((int32_t)19);
		G_B15_1 = L_59;
		G_B15_2 = L_59;
		G_B15_3 = G_B14_4;
		if (!L_61)
		{
			G_B16_0 = ((int32_t)19);
			G_B16_1 = L_59;
			G_B16_2 = L_59;
			G_B16_3 = G_B14_4;
			goto IL_0191;
		}
	}
	{
		G_B17_0 = 0;
		G_B17_1 = G_B15_0;
		G_B17_2 = G_B15_1;
		G_B17_3 = G_B15_2;
		G_B17_4 = G_B15_3;
		goto IL_019c;
	}

IL_0191:
	{
		FsmBool_t1075959796 * L_62 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_62);
		bool L_63 = FsmBool_get_Value_m3101329097(L_62, /*hidden argument*/NULL);
		G_B17_0 = ((int32_t)(L_63));
		G_B17_1 = G_B16_0;
		G_B17_2 = G_B16_1;
		G_B17_3 = G_B16_2;
		G_B17_4 = G_B16_3;
	}

IL_019c:
	{
		bool L_64 = ((bool)G_B17_0);
		Il2CppObject * L_65 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_64);
		NullCheck(G_B17_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B17_2, G_B17_1);
		ArrayElementTypeCheck (G_B17_2, L_65);
		(G_B17_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B17_1), (Il2CppObject *)L_65);
		ObjectU5BU5D_t1108656482* L_66 = G_B17_3;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, ((int32_t)20));
		ArrayElementTypeCheck (L_66, _stringLiteral109637894);
		(L_66)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)20)), (Il2CppObject *)_stringLiteral109637894);
		ObjectU5BU5D_t1108656482* L_67 = L_66;
		int32_t L_68 = __this->get_space_23();
		int32_t L_69 = L_68;
		Il2CppObject * L_70 = Box(Space_t4209342076_il2cpp_TypeInfo_var, &L_69);
		NullCheck(L_67);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_67, ((int32_t)21));
		ArrayElementTypeCheck (L_67, L_70);
		(L_67)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)21)), (Il2CppObject *)L_70);
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		Hashtable_t1407064410 * L_71 = iTween_Hash_m2099495140(NULL /*static, unused*/, L_67, /*hidden argument*/NULL);
		iTween_PunchRotation_m966202531(NULL /*static, unused*/, G_B17_4, L_71, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchScale::.ctor()
extern "C"  void iTweenPunchScale__ctor_m2219202496 (iTweenPunchScale_t4163517798 * __this, const MethodInfo* method)
{
	{
		iTweenFsmAction__ctor_m2960815636(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchScale::Reset()
extern Il2CppClass* FsmString_t952858651_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern const uint32_t iTweenPunchScale_Reset_m4160602733_MetadataUsageId;
extern "C"  void iTweenPunchScale_Reset_m4160602733 (iTweenPunchScale_t4163517798 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenPunchScale_Reset_m4160602733_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmString_t952858651 * V_0 = NULL;
	FsmVector3_t533912882 * V_1 = NULL;
	{
		iTweenFsmAction_Reset_m607248577(__this, /*hidden argument*/NULL);
		FsmString_t952858651 * L_0 = (FsmString_t952858651 *)il2cpp_codegen_object_new(FsmString_t952858651_il2cpp_TypeInfo_var);
		FsmString__ctor_m2679108260(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmString_t952858651 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmString_t952858651 * L_2 = V_0;
		__this->set_id_18(L_2);
		FsmFloat_t2134102846 * L_3 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_time_20(L_3);
		FsmFloat_t2134102846 * L_4 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_delay_21(L_4);
		__this->set_loopType_22(0);
		FsmVector3_t533912882 * L_5 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_5, /*hidden argument*/NULL);
		V_1 = L_5;
		FsmVector3_t533912882 * L_6 = V_1;
		NullCheck(L_6);
		NamedVariable_set_UseVariable_m4266138971(L_6, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_7 = V_1;
		__this->set_vector_19(L_7);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchScale::OnEnter()
extern "C"  void iTweenPunchScale_OnEnter_m1741548439 (iTweenPunchScale_t4163517798 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnEnteriTween_m3024539459(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_loopType_22();
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		iTweenFsmAction_IsLoop_m2539277653(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_001e:
	{
		iTweenPunchScale_DoiTween_m3899526289(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchScale::OnExit()
extern "C"  void iTweenPunchScale_OnExit_m1589119265 (iTweenPunchScale_t4163517798 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnExitiTween_m1981625761(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchScale::DoiTween()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* LoopType_t1485160459_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* iTween_t3087282050_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral107028782;
extern Il2CppCodeGenString* _stringLiteral2881114200;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral3560141;
extern Il2CppCodeGenString* _stringLiteral95467907;
extern Il2CppCodeGenString* _stringLiteral2258461662;
extern Il2CppCodeGenString* _stringLiteral2105864216;
extern Il2CppCodeGenString* _stringLiteral338257242;
extern Il2CppCodeGenString* _stringLiteral3696326558;
extern Il2CppCodeGenString* _stringLiteral2987624931;
extern Il2CppCodeGenString* _stringLiteral647687137;
extern Il2CppCodeGenString* _stringLiteral2052738345;
extern Il2CppCodeGenString* _stringLiteral1067506955;
extern const uint32_t iTweenPunchScale_DoiTween_m3899526289_MetadataUsageId;
extern "C"  void iTweenPunchScale_DoiTween_m3899526289 (iTweenPunchScale_t4163517798 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenPunchScale_DoiTween_m3899526289_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B7_0 = 0;
	ObjectU5BU5D_t1108656482* G_B7_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B7_2 = NULL;
	GameObject_t3674682005 * G_B7_3 = NULL;
	int32_t G_B6_0 = 0;
	ObjectU5BU5D_t1108656482* G_B6_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B6_2 = NULL;
	GameObject_t3674682005 * G_B6_3 = NULL;
	String_t* G_B8_0 = NULL;
	int32_t G_B8_1 = 0;
	ObjectU5BU5D_t1108656482* G_B8_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B8_3 = NULL;
	GameObject_t3674682005 * G_B8_4 = NULL;
	int32_t G_B10_0 = 0;
	ObjectU5BU5D_t1108656482* G_B10_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B10_2 = NULL;
	GameObject_t3674682005 * G_B10_3 = NULL;
	int32_t G_B9_0 = 0;
	ObjectU5BU5D_t1108656482* G_B9_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B9_2 = NULL;
	GameObject_t3674682005 * G_B9_3 = NULL;
	float G_B11_0 = 0.0f;
	int32_t G_B11_1 = 0;
	ObjectU5BU5D_t1108656482* G_B11_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B11_3 = NULL;
	GameObject_t3674682005 * G_B11_4 = NULL;
	int32_t G_B13_0 = 0;
	ObjectU5BU5D_t1108656482* G_B13_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B13_2 = NULL;
	GameObject_t3674682005 * G_B13_3 = NULL;
	int32_t G_B12_0 = 0;
	ObjectU5BU5D_t1108656482* G_B12_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B12_2 = NULL;
	GameObject_t3674682005 * G_B12_3 = NULL;
	float G_B14_0 = 0.0f;
	int32_t G_B14_1 = 0;
	ObjectU5BU5D_t1108656482* G_B14_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B14_3 = NULL;
	GameObject_t3674682005 * G_B14_4 = NULL;
	int32_t G_B16_0 = 0;
	ObjectU5BU5D_t1108656482* G_B16_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B16_2 = NULL;
	GameObject_t3674682005 * G_B16_3 = NULL;
	int32_t G_B15_0 = 0;
	ObjectU5BU5D_t1108656482* G_B15_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B15_2 = NULL;
	GameObject_t3674682005 * G_B15_3 = NULL;
	int32_t G_B17_0 = 0;
	int32_t G_B17_1 = 0;
	ObjectU5BU5D_t1108656482* G_B17_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B17_3 = NULL;
	GameObject_t3674682005 * G_B17_4 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_17();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		Vector3_t4282066566  L_5 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_5;
		FsmVector3_t533912882 * L_6 = __this->get_vector_19();
		NullCheck(L_6);
		bool L_7 = NamedVariable_get_IsNone_m281035543(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003a;
		}
	}
	{
		goto IL_0046;
	}

IL_003a:
	{
		FsmVector3_t533912882 * L_8 = __this->get_vector_19();
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = FsmVector3_get_Value_m2779135117(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
	}

IL_0046:
	{
		((iTweenFsmAction_t410382178 *)__this)->set_itweenType_15(_stringLiteral107028782);
		GameObject_t3674682005 * L_10 = V_0;
		ObjectU5BU5D_t1108656482* L_11 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)20)));
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		ArrayElementTypeCheck (L_11, _stringLiteral2881114200);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2881114200);
		ObjectU5BU5D_t1108656482* L_12 = L_11;
		Vector3_t4282066566  L_13 = V_1;
		Vector3_t4282066566  L_14 = L_13;
		Il2CppObject * L_15 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 1);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_15);
		ObjectU5BU5D_t1108656482* L_16 = L_12;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 2);
		ArrayElementTypeCheck (L_16, _stringLiteral3373707);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3373707);
		ObjectU5BU5D_t1108656482* L_17 = L_16;
		FsmString_t952858651 * L_18 = __this->get_id_18();
		NullCheck(L_18);
		bool L_19 = NamedVariable_get_IsNone_m281035543(L_18, /*hidden argument*/NULL);
		G_B6_0 = 3;
		G_B6_1 = L_17;
		G_B6_2 = L_17;
		G_B6_3 = L_10;
		if (!L_19)
		{
			G_B7_0 = 3;
			G_B7_1 = L_17;
			G_B7_2 = L_17;
			G_B7_3 = L_10;
			goto IL_008e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B8_0 = L_20;
		G_B8_1 = G_B6_0;
		G_B8_2 = G_B6_1;
		G_B8_3 = G_B6_2;
		G_B8_4 = G_B6_3;
		goto IL_0099;
	}

IL_008e:
	{
		FsmString_t952858651 * L_21 = __this->get_id_18();
		NullCheck(L_21);
		String_t* L_22 = FsmString_get_Value_m872383149(L_21, /*hidden argument*/NULL);
		G_B8_0 = L_22;
		G_B8_1 = G_B7_0;
		G_B8_2 = G_B7_1;
		G_B8_3 = G_B7_2;
		G_B8_4 = G_B7_3;
	}

IL_0099:
	{
		NullCheck(G_B8_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B8_2, G_B8_1);
		ArrayElementTypeCheck (G_B8_2, G_B8_0);
		(G_B8_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B8_1), (Il2CppObject *)G_B8_0);
		ObjectU5BU5D_t1108656482* L_23 = G_B8_3;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 4);
		ArrayElementTypeCheck (L_23, _stringLiteral3560141);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral3560141);
		ObjectU5BU5D_t1108656482* L_24 = L_23;
		FsmFloat_t2134102846 * L_25 = __this->get_time_20();
		NullCheck(L_25);
		bool L_26 = NamedVariable_get_IsNone_m281035543(L_25, /*hidden argument*/NULL);
		G_B9_0 = 5;
		G_B9_1 = L_24;
		G_B9_2 = L_24;
		G_B9_3 = G_B8_4;
		if (!L_26)
		{
			G_B10_0 = 5;
			G_B10_1 = L_24;
			G_B10_2 = L_24;
			G_B10_3 = G_B8_4;
			goto IL_00be;
		}
	}
	{
		G_B11_0 = (1.0f);
		G_B11_1 = G_B9_0;
		G_B11_2 = G_B9_1;
		G_B11_3 = G_B9_2;
		G_B11_4 = G_B9_3;
		goto IL_00c9;
	}

IL_00be:
	{
		FsmFloat_t2134102846 * L_27 = __this->get_time_20();
		NullCheck(L_27);
		float L_28 = FsmFloat_get_Value_m4137923823(L_27, /*hidden argument*/NULL);
		G_B11_0 = L_28;
		G_B11_1 = G_B10_0;
		G_B11_2 = G_B10_1;
		G_B11_3 = G_B10_2;
		G_B11_4 = G_B10_3;
	}

IL_00c9:
	{
		float L_29 = G_B11_0;
		Il2CppObject * L_30 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_29);
		NullCheck(G_B11_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B11_2, G_B11_1);
		ArrayElementTypeCheck (G_B11_2, L_30);
		(G_B11_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B11_1), (Il2CppObject *)L_30);
		ObjectU5BU5D_t1108656482* L_31 = G_B11_3;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 6);
		ArrayElementTypeCheck (L_31, _stringLiteral95467907);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral95467907);
		ObjectU5BU5D_t1108656482* L_32 = L_31;
		FsmFloat_t2134102846 * L_33 = __this->get_delay_21();
		NullCheck(L_33);
		bool L_34 = NamedVariable_get_IsNone_m281035543(L_33, /*hidden argument*/NULL);
		G_B12_0 = 7;
		G_B12_1 = L_32;
		G_B12_2 = L_32;
		G_B12_3 = G_B11_4;
		if (!L_34)
		{
			G_B13_0 = 7;
			G_B13_1 = L_32;
			G_B13_2 = L_32;
			G_B13_3 = G_B11_4;
			goto IL_00f3;
		}
	}
	{
		G_B14_0 = (0.0f);
		G_B14_1 = G_B12_0;
		G_B14_2 = G_B12_1;
		G_B14_3 = G_B12_2;
		G_B14_4 = G_B12_3;
		goto IL_00fe;
	}

IL_00f3:
	{
		FsmFloat_t2134102846 * L_35 = __this->get_delay_21();
		NullCheck(L_35);
		float L_36 = FsmFloat_get_Value_m4137923823(L_35, /*hidden argument*/NULL);
		G_B14_0 = L_36;
		G_B14_1 = G_B13_0;
		G_B14_2 = G_B13_1;
		G_B14_3 = G_B13_2;
		G_B14_4 = G_B13_3;
	}

IL_00fe:
	{
		float L_37 = G_B14_0;
		Il2CppObject * L_38 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_37);
		NullCheck(G_B14_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B14_2, G_B14_1);
		ArrayElementTypeCheck (G_B14_2, L_38);
		(G_B14_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B14_1), (Il2CppObject *)L_38);
		ObjectU5BU5D_t1108656482* L_39 = G_B14_3;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, 8);
		ArrayElementTypeCheck (L_39, _stringLiteral2258461662);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral2258461662);
		ObjectU5BU5D_t1108656482* L_40 = L_39;
		int32_t L_41 = __this->get_loopType_22();
		int32_t L_42 = L_41;
		Il2CppObject * L_43 = Box(LoopType_t1485160459_il2cpp_TypeInfo_var, &L_42);
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, ((int32_t)9));
		ArrayElementTypeCheck (L_40, L_43);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)L_43);
		ObjectU5BU5D_t1108656482* L_44 = L_40;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)10));
		ArrayElementTypeCheck (L_44, _stringLiteral2105864216);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Il2CppObject *)_stringLiteral2105864216);
		ObjectU5BU5D_t1108656482* L_45 = L_44;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, ((int32_t)11));
		ArrayElementTypeCheck (L_45, _stringLiteral338257242);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (Il2CppObject *)_stringLiteral338257242);
		ObjectU5BU5D_t1108656482* L_46 = L_45;
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, ((int32_t)12));
		ArrayElementTypeCheck (L_46, _stringLiteral3696326558);
		(L_46)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (Il2CppObject *)_stringLiteral3696326558);
		ObjectU5BU5D_t1108656482* L_47 = L_46;
		int32_t L_48 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_49 = L_48;
		Il2CppObject * L_50 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_49);
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, ((int32_t)13));
		ArrayElementTypeCheck (L_47, L_50);
		(L_47)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (Il2CppObject *)L_50);
		ObjectU5BU5D_t1108656482* L_51 = L_47;
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, ((int32_t)14));
		ArrayElementTypeCheck (L_51, _stringLiteral2987624931);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (Il2CppObject *)_stringLiteral2987624931);
		ObjectU5BU5D_t1108656482* L_52 = L_51;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, ((int32_t)15));
		ArrayElementTypeCheck (L_52, _stringLiteral647687137);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (Il2CppObject *)_stringLiteral647687137);
		ObjectU5BU5D_t1108656482* L_53 = L_52;
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, ((int32_t)16));
		ArrayElementTypeCheck (L_53, _stringLiteral2052738345);
		(L_53)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (Il2CppObject *)_stringLiteral2052738345);
		ObjectU5BU5D_t1108656482* L_54 = L_53;
		int32_t L_55 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_56 = L_55;
		Il2CppObject * L_57 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_56);
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, ((int32_t)17));
		ArrayElementTypeCheck (L_54, L_57);
		(L_54)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (Il2CppObject *)L_57);
		ObjectU5BU5D_t1108656482* L_58 = L_54;
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, ((int32_t)18));
		ArrayElementTypeCheck (L_58, _stringLiteral1067506955);
		(L_58)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (Il2CppObject *)_stringLiteral1067506955);
		ObjectU5BU5D_t1108656482* L_59 = L_58;
		FsmBool_t1075959796 * L_60 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_60);
		bool L_61 = NamedVariable_get_IsNone_m281035543(L_60, /*hidden argument*/NULL);
		G_B15_0 = ((int32_t)19);
		G_B15_1 = L_59;
		G_B15_2 = L_59;
		G_B15_3 = G_B14_4;
		if (!L_61)
		{
			G_B16_0 = ((int32_t)19);
			G_B16_1 = L_59;
			G_B16_2 = L_59;
			G_B16_3 = G_B14_4;
			goto IL_0191;
		}
	}
	{
		G_B17_0 = 0;
		G_B17_1 = G_B15_0;
		G_B17_2 = G_B15_1;
		G_B17_3 = G_B15_2;
		G_B17_4 = G_B15_3;
		goto IL_019c;
	}

IL_0191:
	{
		FsmBool_t1075959796 * L_62 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_62);
		bool L_63 = FsmBool_get_Value_m3101329097(L_62, /*hidden argument*/NULL);
		G_B17_0 = ((int32_t)(L_63));
		G_B17_1 = G_B16_0;
		G_B17_2 = G_B16_1;
		G_B17_3 = G_B16_2;
		G_B17_4 = G_B16_3;
	}

IL_019c:
	{
		bool L_64 = ((bool)G_B17_0);
		Il2CppObject * L_65 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_64);
		NullCheck(G_B17_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B17_2, G_B17_1);
		ArrayElementTypeCheck (G_B17_2, L_65);
		(G_B17_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B17_1), (Il2CppObject *)L_65);
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		Hashtable_t1407064410 * L_66 = iTween_Hash_m2099495140(NULL /*static, unused*/, G_B17_3, /*hidden argument*/NULL);
		iTween_PunchScale_m99386207(NULL /*static, unused*/, G_B17_4, L_66, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenResume::.ctor()
extern "C"  void iTweenResume__ctor_m2527496175 (iTweenResume_t603494487 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenResume::Reset()
extern "C"  void iTweenResume_Reset_m173929116 (iTweenResume_t603494487 * __this, const MethodInfo* method)
{
	{
		__this->set_iTweenType_10(0);
		__this->set_includeChildren_11((bool)0);
		__this->set_inScene_12((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenResume::OnEnter()
extern "C"  void iTweenResume_OnEnter_m1659030534 (iTweenResume_t603494487 * __this, const MethodInfo* method)
{
	{
		FsmStateAction_OnEnter_m160148417(__this, /*hidden argument*/NULL);
		iTweenResume_DoiTween_m1341471234(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenResume::DoiTween()
extern const Il2CppType* iTweenFSMType_t470630072_0_0_0_var;
extern Il2CppClass* iTween_t3087282050_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* iTweenFSMType_t470630072_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2862688501_il2cpp_TypeInfo_var;
extern const uint32_t iTweenResume_DoiTween_m1341471234_MetadataUsageId;
extern "C"  void iTweenResume_DoiTween_m1341471234 (iTweenResume_t603494487 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenResume_DoiTween_m1341471234_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		int32_t L_0 = __this->get_iTweenType_10();
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		iTween_Resume_m1555201336(NULL /*static, unused*/, /*hidden argument*/NULL);
		goto IL_0089;
	}

IL_0015:
	{
		bool L_1 = __this->get_inScene_12();
		if (!L_1)
		{
			goto IL_0044;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(iTweenFSMType_t470630072_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_3 = __this->get_iTweenType_10();
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(iTweenFSMType_t470630072_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		String_t* L_6 = Enum_GetName_m3333121265(NULL /*static, unused*/, L_2, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		iTween_Resume_m3739638986(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		goto IL_0089;
	}

IL_0044:
	{
		Fsm_t1527112426 * L_7 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_8 = __this->get_gameObject_9();
		NullCheck(L_7);
		GameObject_t3674682005 * L_9 = Fsm_GetOwnerDefaultTarget_m846013999(L_7, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		GameObject_t3674682005 * L_10 = V_0;
		bool L_11 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_10, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0063;
		}
	}
	{
		return;
	}

IL_0063:
	{
		GameObject_t3674682005 * L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_13 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(iTweenFSMType_t470630072_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_14 = __this->get_iTweenType_10();
		int32_t L_15 = L_14;
		Il2CppObject * L_16 = Box(iTweenFSMType_t470630072_il2cpp_TypeInfo_var, &L_15);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		String_t* L_17 = Enum_GetName_m3333121265(NULL /*static, unused*/, L_13, L_16, /*hidden argument*/NULL);
		bool L_18 = __this->get_includeChildren_11();
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		iTween_Resume_m375830289(NULL /*static, unused*/, L_12, L_17, L_18, /*hidden argument*/NULL);
	}

IL_0089:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateAdd::.ctor()
extern "C"  void iTweenRotateAdd__ctor_m3970761316 (iTweenRotateAdd_t368396562 * __this, const MethodInfo* method)
{
	{
		__this->set_easeType_23(((int32_t)21));
		iTweenFsmAction__ctor_m2960815636(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateAdd::Reset()
extern Il2CppClass* FsmString_t952858651_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmFloat_t2134102846_il2cpp_TypeInfo_var;
extern const uint32_t iTweenRotateAdd_Reset_m1617194257_MetadataUsageId;
extern "C"  void iTweenRotateAdd_Reset_m1617194257 (iTweenRotateAdd_t368396562 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenRotateAdd_Reset_m1617194257_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmString_t952858651 * V_0 = NULL;
	FsmVector3_t533912882 * V_1 = NULL;
	FsmFloat_t2134102846 * V_2 = NULL;
	{
		iTweenFsmAction_Reset_m607248577(__this, /*hidden argument*/NULL);
		FsmString_t952858651 * L_0 = (FsmString_t952858651 *)il2cpp_codegen_object_new(FsmString_t952858651_il2cpp_TypeInfo_var);
		FsmString__ctor_m2679108260(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmString_t952858651 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmString_t952858651 * L_2 = V_0;
		__this->set_id_18(L_2);
		FsmFloat_t2134102846 * L_3 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_time_20(L_3);
		FsmFloat_t2134102846 * L_4 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_delay_21(L_4);
		__this->set_loopType_24(0);
		FsmVector3_t533912882 * L_5 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_5, /*hidden argument*/NULL);
		V_1 = L_5;
		FsmVector3_t533912882 * L_6 = V_1;
		NullCheck(L_6);
		NamedVariable_set_UseVariable_m4266138971(L_6, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_7 = V_1;
		__this->set_vector_19(L_7);
		FsmFloat_t2134102846 * L_8 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_8, /*hidden argument*/NULL);
		V_2 = L_8;
		FsmFloat_t2134102846 * L_9 = V_2;
		NullCheck(L_9);
		NamedVariable_set_UseVariable_m4266138971(L_9, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_10 = V_2;
		__this->set_speed_22(L_10);
		__this->set_space_25(0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateAdd::OnEnter()
extern "C"  void iTweenRotateAdd_OnEnter_m1362394427 (iTweenRotateAdd_t368396562 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnEnteriTween_m3024539459(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_loopType_24();
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		iTweenFsmAction_IsLoop_m2539277653(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_001e:
	{
		iTweenRotateAdd_DoiTween_m735686509(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateAdd::OnExit()
extern "C"  void iTweenRotateAdd_OnExit_m52867837 (iTweenRotateAdd_t368396562 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnExitiTween_m1981625761(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateAdd::DoiTween()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* EaseType_t2734598229_il2cpp_TypeInfo_var;
extern Il2CppClass* LoopType_t1485160459_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* Space_t4209342076_il2cpp_TypeInfo_var;
extern Il2CppClass* iTween_t3087282050_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3369786715;
extern Il2CppCodeGenString* _stringLiteral2881114200;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral3560141;
extern Il2CppCodeGenString* _stringLiteral109641799;
extern Il2CppCodeGenString* _stringLiteral95467907;
extern Il2CppCodeGenString* _stringLiteral3507899432;
extern Il2CppCodeGenString* _stringLiteral2258461662;
extern Il2CppCodeGenString* _stringLiteral2105864216;
extern Il2CppCodeGenString* _stringLiteral338257242;
extern Il2CppCodeGenString* _stringLiteral3696326558;
extern Il2CppCodeGenString* _stringLiteral2987624931;
extern Il2CppCodeGenString* _stringLiteral647687137;
extern Il2CppCodeGenString* _stringLiteral2052738345;
extern Il2CppCodeGenString* _stringLiteral1067506955;
extern Il2CppCodeGenString* _stringLiteral109637894;
extern const uint32_t iTweenRotateAdd_DoiTween_m735686509_MetadataUsageId;
extern "C"  void iTweenRotateAdd_DoiTween_m735686509 (iTweenRotateAdd_t368396562 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenRotateAdd_DoiTween_m735686509_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B7_0 = 0;
	ObjectU5BU5D_t1108656482* G_B7_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B7_2 = NULL;
	GameObject_t3674682005 * G_B7_3 = NULL;
	int32_t G_B6_0 = 0;
	ObjectU5BU5D_t1108656482* G_B6_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B6_2 = NULL;
	GameObject_t3674682005 * G_B6_3 = NULL;
	String_t* G_B8_0 = NULL;
	int32_t G_B8_1 = 0;
	ObjectU5BU5D_t1108656482* G_B8_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B8_3 = NULL;
	GameObject_t3674682005 * G_B8_4 = NULL;
	int32_t G_B10_0 = 0;
	ObjectU5BU5D_t1108656482* G_B10_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B10_2 = NULL;
	GameObject_t3674682005 * G_B10_3 = NULL;
	int32_t G_B9_0 = 0;
	ObjectU5BU5D_t1108656482* G_B9_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B9_2 = NULL;
	GameObject_t3674682005 * G_B9_3 = NULL;
	String_t* G_B11_0 = NULL;
	int32_t G_B11_1 = 0;
	ObjectU5BU5D_t1108656482* G_B11_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B11_3 = NULL;
	GameObject_t3674682005 * G_B11_4 = NULL;
	int32_t G_B16_0 = 0;
	ObjectU5BU5D_t1108656482* G_B16_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B16_2 = NULL;
	GameObject_t3674682005 * G_B16_3 = NULL;
	int32_t G_B12_0 = 0;
	ObjectU5BU5D_t1108656482* G_B12_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B12_2 = NULL;
	GameObject_t3674682005 * G_B12_3 = NULL;
	int32_t G_B14_0 = 0;
	ObjectU5BU5D_t1108656482* G_B14_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B14_2 = NULL;
	GameObject_t3674682005 * G_B14_3 = NULL;
	int32_t G_B13_0 = 0;
	ObjectU5BU5D_t1108656482* G_B13_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B13_2 = NULL;
	GameObject_t3674682005 * G_B13_3 = NULL;
	float G_B15_0 = 0.0f;
	int32_t G_B15_1 = 0;
	ObjectU5BU5D_t1108656482* G_B15_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B15_3 = NULL;
	GameObject_t3674682005 * G_B15_4 = NULL;
	float G_B17_0 = 0.0f;
	int32_t G_B17_1 = 0;
	ObjectU5BU5D_t1108656482* G_B17_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B17_3 = NULL;
	GameObject_t3674682005 * G_B17_4 = NULL;
	int32_t G_B19_0 = 0;
	ObjectU5BU5D_t1108656482* G_B19_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B19_2 = NULL;
	GameObject_t3674682005 * G_B19_3 = NULL;
	int32_t G_B18_0 = 0;
	ObjectU5BU5D_t1108656482* G_B18_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B18_2 = NULL;
	GameObject_t3674682005 * G_B18_3 = NULL;
	float G_B20_0 = 0.0f;
	int32_t G_B20_1 = 0;
	ObjectU5BU5D_t1108656482* G_B20_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B20_3 = NULL;
	GameObject_t3674682005 * G_B20_4 = NULL;
	int32_t G_B22_0 = 0;
	ObjectU5BU5D_t1108656482* G_B22_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B22_2 = NULL;
	GameObject_t3674682005 * G_B22_3 = NULL;
	int32_t G_B21_0 = 0;
	ObjectU5BU5D_t1108656482* G_B21_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B21_2 = NULL;
	GameObject_t3674682005 * G_B21_3 = NULL;
	int32_t G_B23_0 = 0;
	int32_t G_B23_1 = 0;
	ObjectU5BU5D_t1108656482* G_B23_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B23_3 = NULL;
	GameObject_t3674682005 * G_B23_4 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_17();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		Vector3_t4282066566  L_5 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_5;
		FsmVector3_t533912882 * L_6 = __this->get_vector_19();
		NullCheck(L_6);
		bool L_7 = NamedVariable_get_IsNone_m281035543(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003a;
		}
	}
	{
		goto IL_0046;
	}

IL_003a:
	{
		FsmVector3_t533912882 * L_8 = __this->get_vector_19();
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = FsmVector3_get_Value_m2779135117(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
	}

IL_0046:
	{
		((iTweenFsmAction_t410382178 *)__this)->set_itweenType_15(_stringLiteral3369786715);
		GameObject_t3674682005 * L_10 = V_0;
		ObjectU5BU5D_t1108656482* L_11 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)24)));
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		ArrayElementTypeCheck (L_11, _stringLiteral2881114200);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2881114200);
		ObjectU5BU5D_t1108656482* L_12 = L_11;
		Vector3_t4282066566  L_13 = V_1;
		Vector3_t4282066566  L_14 = L_13;
		Il2CppObject * L_15 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 1);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_15);
		ObjectU5BU5D_t1108656482* L_16 = L_12;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 2);
		ArrayElementTypeCheck (L_16, _stringLiteral3373707);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3373707);
		ObjectU5BU5D_t1108656482* L_17 = L_16;
		FsmString_t952858651 * L_18 = __this->get_id_18();
		NullCheck(L_18);
		bool L_19 = NamedVariable_get_IsNone_m281035543(L_18, /*hidden argument*/NULL);
		G_B6_0 = 3;
		G_B6_1 = L_17;
		G_B6_2 = L_17;
		G_B6_3 = L_10;
		if (!L_19)
		{
			G_B7_0 = 3;
			G_B7_1 = L_17;
			G_B7_2 = L_17;
			G_B7_3 = L_10;
			goto IL_008e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B8_0 = L_20;
		G_B8_1 = G_B6_0;
		G_B8_2 = G_B6_1;
		G_B8_3 = G_B6_2;
		G_B8_4 = G_B6_3;
		goto IL_0099;
	}

IL_008e:
	{
		FsmString_t952858651 * L_21 = __this->get_id_18();
		NullCheck(L_21);
		String_t* L_22 = FsmString_get_Value_m872383149(L_21, /*hidden argument*/NULL);
		G_B8_0 = L_22;
		G_B8_1 = G_B7_0;
		G_B8_2 = G_B7_1;
		G_B8_3 = G_B7_2;
		G_B8_4 = G_B7_3;
	}

IL_0099:
	{
		NullCheck(G_B8_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B8_2, G_B8_1);
		ArrayElementTypeCheck (G_B8_2, G_B8_0);
		(G_B8_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B8_1), (Il2CppObject *)G_B8_0);
		ObjectU5BU5D_t1108656482* L_23 = G_B8_3;
		FsmFloat_t2134102846 * L_24 = __this->get_speed_22();
		NullCheck(L_24);
		bool L_25 = NamedVariable_get_IsNone_m281035543(L_24, /*hidden argument*/NULL);
		G_B9_0 = 4;
		G_B9_1 = L_23;
		G_B9_2 = L_23;
		G_B9_3 = G_B8_4;
		if (!L_25)
		{
			G_B10_0 = 4;
			G_B10_1 = L_23;
			G_B10_2 = L_23;
			G_B10_3 = G_B8_4;
			goto IL_00b6;
		}
	}
	{
		G_B11_0 = _stringLiteral3560141;
		G_B11_1 = G_B9_0;
		G_B11_2 = G_B9_1;
		G_B11_3 = G_B9_2;
		G_B11_4 = G_B9_3;
		goto IL_00bb;
	}

IL_00b6:
	{
		G_B11_0 = _stringLiteral109641799;
		G_B11_1 = G_B10_0;
		G_B11_2 = G_B10_1;
		G_B11_3 = G_B10_2;
		G_B11_4 = G_B10_3;
	}

IL_00bb:
	{
		NullCheck(G_B11_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B11_2, G_B11_1);
		ArrayElementTypeCheck (G_B11_2, G_B11_0);
		(G_B11_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B11_1), (Il2CppObject *)G_B11_0);
		ObjectU5BU5D_t1108656482* L_26 = G_B11_3;
		FsmFloat_t2134102846 * L_27 = __this->get_speed_22();
		NullCheck(L_27);
		bool L_28 = NamedVariable_get_IsNone_m281035543(L_27, /*hidden argument*/NULL);
		G_B12_0 = 5;
		G_B12_1 = L_26;
		G_B12_2 = L_26;
		G_B12_3 = G_B11_4;
		if (!L_28)
		{
			G_B16_0 = 5;
			G_B16_1 = L_26;
			G_B16_2 = L_26;
			G_B16_3 = G_B11_4;
			goto IL_00f8;
		}
	}
	{
		FsmFloat_t2134102846 * L_29 = __this->get_time_20();
		NullCheck(L_29);
		bool L_30 = NamedVariable_get_IsNone_m281035543(L_29, /*hidden argument*/NULL);
		G_B13_0 = G_B12_0;
		G_B13_1 = G_B12_1;
		G_B13_2 = G_B12_2;
		G_B13_3 = G_B12_3;
		if (!L_30)
		{
			G_B14_0 = G_B12_0;
			G_B14_1 = G_B12_1;
			G_B14_2 = G_B12_2;
			G_B14_3 = G_B12_3;
			goto IL_00e8;
		}
	}
	{
		G_B15_0 = (1.0f);
		G_B15_1 = G_B13_0;
		G_B15_2 = G_B13_1;
		G_B15_3 = G_B13_2;
		G_B15_4 = G_B13_3;
		goto IL_00f3;
	}

IL_00e8:
	{
		FsmFloat_t2134102846 * L_31 = __this->get_time_20();
		NullCheck(L_31);
		float L_32 = FsmFloat_get_Value_m4137923823(L_31, /*hidden argument*/NULL);
		G_B15_0 = L_32;
		G_B15_1 = G_B14_0;
		G_B15_2 = G_B14_1;
		G_B15_3 = G_B14_2;
		G_B15_4 = G_B14_3;
	}

IL_00f3:
	{
		G_B17_0 = G_B15_0;
		G_B17_1 = G_B15_1;
		G_B17_2 = G_B15_2;
		G_B17_3 = G_B15_3;
		G_B17_4 = G_B15_4;
		goto IL_0103;
	}

IL_00f8:
	{
		FsmFloat_t2134102846 * L_33 = __this->get_speed_22();
		NullCheck(L_33);
		float L_34 = FsmFloat_get_Value_m4137923823(L_33, /*hidden argument*/NULL);
		G_B17_0 = L_34;
		G_B17_1 = G_B16_0;
		G_B17_2 = G_B16_1;
		G_B17_3 = G_B16_2;
		G_B17_4 = G_B16_3;
	}

IL_0103:
	{
		float L_35 = G_B17_0;
		Il2CppObject * L_36 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_35);
		NullCheck(G_B17_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B17_2, G_B17_1);
		ArrayElementTypeCheck (G_B17_2, L_36);
		(G_B17_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B17_1), (Il2CppObject *)L_36);
		ObjectU5BU5D_t1108656482* L_37 = G_B17_3;
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, 6);
		ArrayElementTypeCheck (L_37, _stringLiteral95467907);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral95467907);
		ObjectU5BU5D_t1108656482* L_38 = L_37;
		FsmFloat_t2134102846 * L_39 = __this->get_delay_21();
		NullCheck(L_39);
		bool L_40 = NamedVariable_get_IsNone_m281035543(L_39, /*hidden argument*/NULL);
		G_B18_0 = 7;
		G_B18_1 = L_38;
		G_B18_2 = L_38;
		G_B18_3 = G_B17_4;
		if (!L_40)
		{
			G_B19_0 = 7;
			G_B19_1 = L_38;
			G_B19_2 = L_38;
			G_B19_3 = G_B17_4;
			goto IL_012d;
		}
	}
	{
		G_B20_0 = (0.0f);
		G_B20_1 = G_B18_0;
		G_B20_2 = G_B18_1;
		G_B20_3 = G_B18_2;
		G_B20_4 = G_B18_3;
		goto IL_0138;
	}

IL_012d:
	{
		FsmFloat_t2134102846 * L_41 = __this->get_delay_21();
		NullCheck(L_41);
		float L_42 = FsmFloat_get_Value_m4137923823(L_41, /*hidden argument*/NULL);
		G_B20_0 = L_42;
		G_B20_1 = G_B19_0;
		G_B20_2 = G_B19_1;
		G_B20_3 = G_B19_2;
		G_B20_4 = G_B19_3;
	}

IL_0138:
	{
		float L_43 = G_B20_0;
		Il2CppObject * L_44 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_43);
		NullCheck(G_B20_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B20_2, G_B20_1);
		ArrayElementTypeCheck (G_B20_2, L_44);
		(G_B20_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B20_1), (Il2CppObject *)L_44);
		ObjectU5BU5D_t1108656482* L_45 = G_B20_3;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, 8);
		ArrayElementTypeCheck (L_45, _stringLiteral3507899432);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral3507899432);
		ObjectU5BU5D_t1108656482* L_46 = L_45;
		int32_t L_47 = __this->get_easeType_23();
		int32_t L_48 = L_47;
		Il2CppObject * L_49 = Box(EaseType_t2734598229_il2cpp_TypeInfo_var, &L_48);
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, ((int32_t)9));
		ArrayElementTypeCheck (L_46, L_49);
		(L_46)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)L_49);
		ObjectU5BU5D_t1108656482* L_50 = L_46;
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, ((int32_t)10));
		ArrayElementTypeCheck (L_50, _stringLiteral2258461662);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Il2CppObject *)_stringLiteral2258461662);
		ObjectU5BU5D_t1108656482* L_51 = L_50;
		int32_t L_52 = __this->get_loopType_24();
		int32_t L_53 = L_52;
		Il2CppObject * L_54 = Box(LoopType_t1485160459_il2cpp_TypeInfo_var, &L_53);
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, ((int32_t)11));
		ArrayElementTypeCheck (L_51, L_54);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (Il2CppObject *)L_54);
		ObjectU5BU5D_t1108656482* L_55 = L_51;
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, ((int32_t)12));
		ArrayElementTypeCheck (L_55, _stringLiteral2105864216);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (Il2CppObject *)_stringLiteral2105864216);
		ObjectU5BU5D_t1108656482* L_56 = L_55;
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, ((int32_t)13));
		ArrayElementTypeCheck (L_56, _stringLiteral338257242);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (Il2CppObject *)_stringLiteral338257242);
		ObjectU5BU5D_t1108656482* L_57 = L_56;
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, ((int32_t)14));
		ArrayElementTypeCheck (L_57, _stringLiteral3696326558);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (Il2CppObject *)_stringLiteral3696326558);
		ObjectU5BU5D_t1108656482* L_58 = L_57;
		int32_t L_59 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_60 = L_59;
		Il2CppObject * L_61 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_60);
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, ((int32_t)15));
		ArrayElementTypeCheck (L_58, L_61);
		(L_58)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (Il2CppObject *)L_61);
		ObjectU5BU5D_t1108656482* L_62 = L_58;
		NullCheck(L_62);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_62, ((int32_t)16));
		ArrayElementTypeCheck (L_62, _stringLiteral2987624931);
		(L_62)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (Il2CppObject *)_stringLiteral2987624931);
		ObjectU5BU5D_t1108656482* L_63 = L_62;
		NullCheck(L_63);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_63, ((int32_t)17));
		ArrayElementTypeCheck (L_63, _stringLiteral647687137);
		(L_63)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (Il2CppObject *)_stringLiteral647687137);
		ObjectU5BU5D_t1108656482* L_64 = L_63;
		NullCheck(L_64);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_64, ((int32_t)18));
		ArrayElementTypeCheck (L_64, _stringLiteral2052738345);
		(L_64)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (Il2CppObject *)_stringLiteral2052738345);
		ObjectU5BU5D_t1108656482* L_65 = L_64;
		int32_t L_66 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_67 = L_66;
		Il2CppObject * L_68 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_67);
		NullCheck(L_65);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_65, ((int32_t)19));
		ArrayElementTypeCheck (L_65, L_68);
		(L_65)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)19)), (Il2CppObject *)L_68);
		ObjectU5BU5D_t1108656482* L_69 = L_65;
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, ((int32_t)20));
		ArrayElementTypeCheck (L_69, _stringLiteral1067506955);
		(L_69)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)20)), (Il2CppObject *)_stringLiteral1067506955);
		ObjectU5BU5D_t1108656482* L_70 = L_69;
		FsmBool_t1075959796 * L_71 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_71);
		bool L_72 = NamedVariable_get_IsNone_m281035543(L_71, /*hidden argument*/NULL);
		G_B21_0 = ((int32_t)21);
		G_B21_1 = L_70;
		G_B21_2 = L_70;
		G_B21_3 = G_B20_4;
		if (!L_72)
		{
			G_B22_0 = ((int32_t)21);
			G_B22_1 = L_70;
			G_B22_2 = L_70;
			G_B22_3 = G_B20_4;
			goto IL_01e3;
		}
	}
	{
		G_B23_0 = 0;
		G_B23_1 = G_B21_0;
		G_B23_2 = G_B21_1;
		G_B23_3 = G_B21_2;
		G_B23_4 = G_B21_3;
		goto IL_01ee;
	}

IL_01e3:
	{
		FsmBool_t1075959796 * L_73 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_73);
		bool L_74 = FsmBool_get_Value_m3101329097(L_73, /*hidden argument*/NULL);
		G_B23_0 = ((int32_t)(L_74));
		G_B23_1 = G_B22_0;
		G_B23_2 = G_B22_1;
		G_B23_3 = G_B22_2;
		G_B23_4 = G_B22_3;
	}

IL_01ee:
	{
		bool L_75 = ((bool)G_B23_0);
		Il2CppObject * L_76 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_75);
		NullCheck(G_B23_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B23_2, G_B23_1);
		ArrayElementTypeCheck (G_B23_2, L_76);
		(G_B23_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B23_1), (Il2CppObject *)L_76);
		ObjectU5BU5D_t1108656482* L_77 = G_B23_3;
		NullCheck(L_77);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_77, ((int32_t)22));
		ArrayElementTypeCheck (L_77, _stringLiteral109637894);
		(L_77)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)22)), (Il2CppObject *)_stringLiteral109637894);
		ObjectU5BU5D_t1108656482* L_78 = L_77;
		int32_t L_79 = __this->get_space_25();
		int32_t L_80 = L_79;
		Il2CppObject * L_81 = Box(Space_t4209342076_il2cpp_TypeInfo_var, &L_80);
		NullCheck(L_78);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_78, ((int32_t)23));
		ArrayElementTypeCheck (L_78, L_81);
		(L_78)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)23)), (Il2CppObject *)L_81);
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		Hashtable_t1407064410 * L_82 = iTween_Hash_m2099495140(NULL /*static, unused*/, L_78, /*hidden argument*/NULL);
		iTween_RotateAdd_m1287505769(NULL /*static, unused*/, G_B23_4, L_82, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateBy::.ctor()
extern "C"  void iTweenRotateBy__ctor_m455109738 (iTweenRotateBy_t1141196668 * __this, const MethodInfo* method)
{
	{
		__this->set_easeType_23(((int32_t)21));
		iTweenFsmAction__ctor_m2960815636(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateBy::Reset()
extern Il2CppClass* FsmString_t952858651_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmFloat_t2134102846_il2cpp_TypeInfo_var;
extern const uint32_t iTweenRotateBy_Reset_m2396509975_MetadataUsageId;
extern "C"  void iTweenRotateBy_Reset_m2396509975 (iTweenRotateBy_t1141196668 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenRotateBy_Reset_m2396509975_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmString_t952858651 * V_0 = NULL;
	FsmVector3_t533912882 * V_1 = NULL;
	FsmFloat_t2134102846 * V_2 = NULL;
	{
		iTweenFsmAction_Reset_m607248577(__this, /*hidden argument*/NULL);
		FsmString_t952858651 * L_0 = (FsmString_t952858651 *)il2cpp_codegen_object_new(FsmString_t952858651_il2cpp_TypeInfo_var);
		FsmString__ctor_m2679108260(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmString_t952858651 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmString_t952858651 * L_2 = V_0;
		__this->set_id_18(L_2);
		FsmFloat_t2134102846 * L_3 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_time_20(L_3);
		FsmFloat_t2134102846 * L_4 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_delay_21(L_4);
		__this->set_loopType_24(0);
		FsmVector3_t533912882 * L_5 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_5, /*hidden argument*/NULL);
		V_1 = L_5;
		FsmVector3_t533912882 * L_6 = V_1;
		NullCheck(L_6);
		NamedVariable_set_UseVariable_m4266138971(L_6, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_7 = V_1;
		__this->set_vector_19(L_7);
		FsmFloat_t2134102846 * L_8 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_8, /*hidden argument*/NULL);
		V_2 = L_8;
		FsmFloat_t2134102846 * L_9 = V_2;
		NullCheck(L_9);
		NamedVariable_set_UseVariable_m4266138971(L_9, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_10 = V_2;
		__this->set_speed_22(L_10);
		__this->set_space_25(0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateBy::OnEnter()
extern "C"  void iTweenRotateBy_OnEnter_m2960489921 (iTweenRotateBy_t1141196668 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnEnteriTween_m3024539459(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_loopType_24();
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		iTweenFsmAction_IsLoop_m2539277653(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_001e:
	{
		iTweenRotateBy_DoiTween_m3032006567(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateBy::OnExit()
extern "C"  void iTweenRotateBy_OnExit_m2736818615 (iTweenRotateBy_t1141196668 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnExitiTween_m1981625761(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateBy::DoiTween()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* EaseType_t2734598229_il2cpp_TypeInfo_var;
extern Il2CppClass* LoopType_t1485160459_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* Space_t4209342076_il2cpp_TypeInfo_var;
extern Il2CppClass* iTween_t3087282050_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3369786715;
extern Il2CppCodeGenString* _stringLiteral2881114200;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral3560141;
extern Il2CppCodeGenString* _stringLiteral109641799;
extern Il2CppCodeGenString* _stringLiteral95467907;
extern Il2CppCodeGenString* _stringLiteral3507899432;
extern Il2CppCodeGenString* _stringLiteral2258461662;
extern Il2CppCodeGenString* _stringLiteral2105864216;
extern Il2CppCodeGenString* _stringLiteral338257242;
extern Il2CppCodeGenString* _stringLiteral3696326558;
extern Il2CppCodeGenString* _stringLiteral2987624931;
extern Il2CppCodeGenString* _stringLiteral647687137;
extern Il2CppCodeGenString* _stringLiteral2052738345;
extern Il2CppCodeGenString* _stringLiteral1067506955;
extern Il2CppCodeGenString* _stringLiteral109637894;
extern const uint32_t iTweenRotateBy_DoiTween_m3032006567_MetadataUsageId;
extern "C"  void iTweenRotateBy_DoiTween_m3032006567 (iTweenRotateBy_t1141196668 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenRotateBy_DoiTween_m3032006567_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B7_0 = 0;
	ObjectU5BU5D_t1108656482* G_B7_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B7_2 = NULL;
	GameObject_t3674682005 * G_B7_3 = NULL;
	int32_t G_B6_0 = 0;
	ObjectU5BU5D_t1108656482* G_B6_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B6_2 = NULL;
	GameObject_t3674682005 * G_B6_3 = NULL;
	String_t* G_B8_0 = NULL;
	int32_t G_B8_1 = 0;
	ObjectU5BU5D_t1108656482* G_B8_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B8_3 = NULL;
	GameObject_t3674682005 * G_B8_4 = NULL;
	int32_t G_B10_0 = 0;
	ObjectU5BU5D_t1108656482* G_B10_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B10_2 = NULL;
	GameObject_t3674682005 * G_B10_3 = NULL;
	int32_t G_B9_0 = 0;
	ObjectU5BU5D_t1108656482* G_B9_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B9_2 = NULL;
	GameObject_t3674682005 * G_B9_3 = NULL;
	String_t* G_B11_0 = NULL;
	int32_t G_B11_1 = 0;
	ObjectU5BU5D_t1108656482* G_B11_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B11_3 = NULL;
	GameObject_t3674682005 * G_B11_4 = NULL;
	int32_t G_B16_0 = 0;
	ObjectU5BU5D_t1108656482* G_B16_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B16_2 = NULL;
	GameObject_t3674682005 * G_B16_3 = NULL;
	int32_t G_B12_0 = 0;
	ObjectU5BU5D_t1108656482* G_B12_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B12_2 = NULL;
	GameObject_t3674682005 * G_B12_3 = NULL;
	int32_t G_B14_0 = 0;
	ObjectU5BU5D_t1108656482* G_B14_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B14_2 = NULL;
	GameObject_t3674682005 * G_B14_3 = NULL;
	int32_t G_B13_0 = 0;
	ObjectU5BU5D_t1108656482* G_B13_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B13_2 = NULL;
	GameObject_t3674682005 * G_B13_3 = NULL;
	float G_B15_0 = 0.0f;
	int32_t G_B15_1 = 0;
	ObjectU5BU5D_t1108656482* G_B15_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B15_3 = NULL;
	GameObject_t3674682005 * G_B15_4 = NULL;
	float G_B17_0 = 0.0f;
	int32_t G_B17_1 = 0;
	ObjectU5BU5D_t1108656482* G_B17_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B17_3 = NULL;
	GameObject_t3674682005 * G_B17_4 = NULL;
	int32_t G_B19_0 = 0;
	ObjectU5BU5D_t1108656482* G_B19_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B19_2 = NULL;
	GameObject_t3674682005 * G_B19_3 = NULL;
	int32_t G_B18_0 = 0;
	ObjectU5BU5D_t1108656482* G_B18_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B18_2 = NULL;
	GameObject_t3674682005 * G_B18_3 = NULL;
	float G_B20_0 = 0.0f;
	int32_t G_B20_1 = 0;
	ObjectU5BU5D_t1108656482* G_B20_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B20_3 = NULL;
	GameObject_t3674682005 * G_B20_4 = NULL;
	int32_t G_B22_0 = 0;
	ObjectU5BU5D_t1108656482* G_B22_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B22_2 = NULL;
	GameObject_t3674682005 * G_B22_3 = NULL;
	int32_t G_B21_0 = 0;
	ObjectU5BU5D_t1108656482* G_B21_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B21_2 = NULL;
	GameObject_t3674682005 * G_B21_3 = NULL;
	int32_t G_B23_0 = 0;
	int32_t G_B23_1 = 0;
	ObjectU5BU5D_t1108656482* G_B23_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B23_3 = NULL;
	GameObject_t3674682005 * G_B23_4 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_17();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		Vector3_t4282066566  L_5 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_5;
		FsmVector3_t533912882 * L_6 = __this->get_vector_19();
		NullCheck(L_6);
		bool L_7 = NamedVariable_get_IsNone_m281035543(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003a;
		}
	}
	{
		goto IL_0046;
	}

IL_003a:
	{
		FsmVector3_t533912882 * L_8 = __this->get_vector_19();
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = FsmVector3_get_Value_m2779135117(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
	}

IL_0046:
	{
		((iTweenFsmAction_t410382178 *)__this)->set_itweenType_15(_stringLiteral3369786715);
		GameObject_t3674682005 * L_10 = V_0;
		ObjectU5BU5D_t1108656482* L_11 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)24)));
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		ArrayElementTypeCheck (L_11, _stringLiteral2881114200);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2881114200);
		ObjectU5BU5D_t1108656482* L_12 = L_11;
		Vector3_t4282066566  L_13 = V_1;
		Vector3_t4282066566  L_14 = L_13;
		Il2CppObject * L_15 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 1);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_15);
		ObjectU5BU5D_t1108656482* L_16 = L_12;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 2);
		ArrayElementTypeCheck (L_16, _stringLiteral3373707);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3373707);
		ObjectU5BU5D_t1108656482* L_17 = L_16;
		FsmString_t952858651 * L_18 = __this->get_id_18();
		NullCheck(L_18);
		bool L_19 = NamedVariable_get_IsNone_m281035543(L_18, /*hidden argument*/NULL);
		G_B6_0 = 3;
		G_B6_1 = L_17;
		G_B6_2 = L_17;
		G_B6_3 = L_10;
		if (!L_19)
		{
			G_B7_0 = 3;
			G_B7_1 = L_17;
			G_B7_2 = L_17;
			G_B7_3 = L_10;
			goto IL_008e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B8_0 = L_20;
		G_B8_1 = G_B6_0;
		G_B8_2 = G_B6_1;
		G_B8_3 = G_B6_2;
		G_B8_4 = G_B6_3;
		goto IL_0099;
	}

IL_008e:
	{
		FsmString_t952858651 * L_21 = __this->get_id_18();
		NullCheck(L_21);
		String_t* L_22 = FsmString_get_Value_m872383149(L_21, /*hidden argument*/NULL);
		G_B8_0 = L_22;
		G_B8_1 = G_B7_0;
		G_B8_2 = G_B7_1;
		G_B8_3 = G_B7_2;
		G_B8_4 = G_B7_3;
	}

IL_0099:
	{
		NullCheck(G_B8_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B8_2, G_B8_1);
		ArrayElementTypeCheck (G_B8_2, G_B8_0);
		(G_B8_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B8_1), (Il2CppObject *)G_B8_0);
		ObjectU5BU5D_t1108656482* L_23 = G_B8_3;
		FsmFloat_t2134102846 * L_24 = __this->get_speed_22();
		NullCheck(L_24);
		bool L_25 = NamedVariable_get_IsNone_m281035543(L_24, /*hidden argument*/NULL);
		G_B9_0 = 4;
		G_B9_1 = L_23;
		G_B9_2 = L_23;
		G_B9_3 = G_B8_4;
		if (!L_25)
		{
			G_B10_0 = 4;
			G_B10_1 = L_23;
			G_B10_2 = L_23;
			G_B10_3 = G_B8_4;
			goto IL_00b6;
		}
	}
	{
		G_B11_0 = _stringLiteral3560141;
		G_B11_1 = G_B9_0;
		G_B11_2 = G_B9_1;
		G_B11_3 = G_B9_2;
		G_B11_4 = G_B9_3;
		goto IL_00bb;
	}

IL_00b6:
	{
		G_B11_0 = _stringLiteral109641799;
		G_B11_1 = G_B10_0;
		G_B11_2 = G_B10_1;
		G_B11_3 = G_B10_2;
		G_B11_4 = G_B10_3;
	}

IL_00bb:
	{
		NullCheck(G_B11_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B11_2, G_B11_1);
		ArrayElementTypeCheck (G_B11_2, G_B11_0);
		(G_B11_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B11_1), (Il2CppObject *)G_B11_0);
		ObjectU5BU5D_t1108656482* L_26 = G_B11_3;
		FsmFloat_t2134102846 * L_27 = __this->get_speed_22();
		NullCheck(L_27);
		bool L_28 = NamedVariable_get_IsNone_m281035543(L_27, /*hidden argument*/NULL);
		G_B12_0 = 5;
		G_B12_1 = L_26;
		G_B12_2 = L_26;
		G_B12_3 = G_B11_4;
		if (!L_28)
		{
			G_B16_0 = 5;
			G_B16_1 = L_26;
			G_B16_2 = L_26;
			G_B16_3 = G_B11_4;
			goto IL_00f8;
		}
	}
	{
		FsmFloat_t2134102846 * L_29 = __this->get_time_20();
		NullCheck(L_29);
		bool L_30 = NamedVariable_get_IsNone_m281035543(L_29, /*hidden argument*/NULL);
		G_B13_0 = G_B12_0;
		G_B13_1 = G_B12_1;
		G_B13_2 = G_B12_2;
		G_B13_3 = G_B12_3;
		if (!L_30)
		{
			G_B14_0 = G_B12_0;
			G_B14_1 = G_B12_1;
			G_B14_2 = G_B12_2;
			G_B14_3 = G_B12_3;
			goto IL_00e8;
		}
	}
	{
		G_B15_0 = (1.0f);
		G_B15_1 = G_B13_0;
		G_B15_2 = G_B13_1;
		G_B15_3 = G_B13_2;
		G_B15_4 = G_B13_3;
		goto IL_00f3;
	}

IL_00e8:
	{
		FsmFloat_t2134102846 * L_31 = __this->get_time_20();
		NullCheck(L_31);
		float L_32 = FsmFloat_get_Value_m4137923823(L_31, /*hidden argument*/NULL);
		G_B15_0 = L_32;
		G_B15_1 = G_B14_0;
		G_B15_2 = G_B14_1;
		G_B15_3 = G_B14_2;
		G_B15_4 = G_B14_3;
	}

IL_00f3:
	{
		G_B17_0 = G_B15_0;
		G_B17_1 = G_B15_1;
		G_B17_2 = G_B15_2;
		G_B17_3 = G_B15_3;
		G_B17_4 = G_B15_4;
		goto IL_0103;
	}

IL_00f8:
	{
		FsmFloat_t2134102846 * L_33 = __this->get_speed_22();
		NullCheck(L_33);
		float L_34 = FsmFloat_get_Value_m4137923823(L_33, /*hidden argument*/NULL);
		G_B17_0 = L_34;
		G_B17_1 = G_B16_0;
		G_B17_2 = G_B16_1;
		G_B17_3 = G_B16_2;
		G_B17_4 = G_B16_3;
	}

IL_0103:
	{
		float L_35 = G_B17_0;
		Il2CppObject * L_36 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_35);
		NullCheck(G_B17_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B17_2, G_B17_1);
		ArrayElementTypeCheck (G_B17_2, L_36);
		(G_B17_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B17_1), (Il2CppObject *)L_36);
		ObjectU5BU5D_t1108656482* L_37 = G_B17_3;
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, 6);
		ArrayElementTypeCheck (L_37, _stringLiteral95467907);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral95467907);
		ObjectU5BU5D_t1108656482* L_38 = L_37;
		FsmFloat_t2134102846 * L_39 = __this->get_delay_21();
		NullCheck(L_39);
		bool L_40 = NamedVariable_get_IsNone_m281035543(L_39, /*hidden argument*/NULL);
		G_B18_0 = 7;
		G_B18_1 = L_38;
		G_B18_2 = L_38;
		G_B18_3 = G_B17_4;
		if (!L_40)
		{
			G_B19_0 = 7;
			G_B19_1 = L_38;
			G_B19_2 = L_38;
			G_B19_3 = G_B17_4;
			goto IL_012d;
		}
	}
	{
		G_B20_0 = (0.0f);
		G_B20_1 = G_B18_0;
		G_B20_2 = G_B18_1;
		G_B20_3 = G_B18_2;
		G_B20_4 = G_B18_3;
		goto IL_0138;
	}

IL_012d:
	{
		FsmFloat_t2134102846 * L_41 = __this->get_delay_21();
		NullCheck(L_41);
		float L_42 = FsmFloat_get_Value_m4137923823(L_41, /*hidden argument*/NULL);
		G_B20_0 = L_42;
		G_B20_1 = G_B19_0;
		G_B20_2 = G_B19_1;
		G_B20_3 = G_B19_2;
		G_B20_4 = G_B19_3;
	}

IL_0138:
	{
		float L_43 = G_B20_0;
		Il2CppObject * L_44 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_43);
		NullCheck(G_B20_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B20_2, G_B20_1);
		ArrayElementTypeCheck (G_B20_2, L_44);
		(G_B20_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B20_1), (Il2CppObject *)L_44);
		ObjectU5BU5D_t1108656482* L_45 = G_B20_3;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, 8);
		ArrayElementTypeCheck (L_45, _stringLiteral3507899432);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral3507899432);
		ObjectU5BU5D_t1108656482* L_46 = L_45;
		int32_t L_47 = __this->get_easeType_23();
		int32_t L_48 = L_47;
		Il2CppObject * L_49 = Box(EaseType_t2734598229_il2cpp_TypeInfo_var, &L_48);
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, ((int32_t)9));
		ArrayElementTypeCheck (L_46, L_49);
		(L_46)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)L_49);
		ObjectU5BU5D_t1108656482* L_50 = L_46;
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, ((int32_t)10));
		ArrayElementTypeCheck (L_50, _stringLiteral2258461662);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Il2CppObject *)_stringLiteral2258461662);
		ObjectU5BU5D_t1108656482* L_51 = L_50;
		int32_t L_52 = __this->get_loopType_24();
		int32_t L_53 = L_52;
		Il2CppObject * L_54 = Box(LoopType_t1485160459_il2cpp_TypeInfo_var, &L_53);
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, ((int32_t)11));
		ArrayElementTypeCheck (L_51, L_54);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (Il2CppObject *)L_54);
		ObjectU5BU5D_t1108656482* L_55 = L_51;
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, ((int32_t)12));
		ArrayElementTypeCheck (L_55, _stringLiteral2105864216);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (Il2CppObject *)_stringLiteral2105864216);
		ObjectU5BU5D_t1108656482* L_56 = L_55;
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, ((int32_t)13));
		ArrayElementTypeCheck (L_56, _stringLiteral338257242);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (Il2CppObject *)_stringLiteral338257242);
		ObjectU5BU5D_t1108656482* L_57 = L_56;
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, ((int32_t)14));
		ArrayElementTypeCheck (L_57, _stringLiteral3696326558);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (Il2CppObject *)_stringLiteral3696326558);
		ObjectU5BU5D_t1108656482* L_58 = L_57;
		int32_t L_59 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_60 = L_59;
		Il2CppObject * L_61 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_60);
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, ((int32_t)15));
		ArrayElementTypeCheck (L_58, L_61);
		(L_58)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (Il2CppObject *)L_61);
		ObjectU5BU5D_t1108656482* L_62 = L_58;
		NullCheck(L_62);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_62, ((int32_t)16));
		ArrayElementTypeCheck (L_62, _stringLiteral2987624931);
		(L_62)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (Il2CppObject *)_stringLiteral2987624931);
		ObjectU5BU5D_t1108656482* L_63 = L_62;
		NullCheck(L_63);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_63, ((int32_t)17));
		ArrayElementTypeCheck (L_63, _stringLiteral647687137);
		(L_63)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (Il2CppObject *)_stringLiteral647687137);
		ObjectU5BU5D_t1108656482* L_64 = L_63;
		NullCheck(L_64);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_64, ((int32_t)18));
		ArrayElementTypeCheck (L_64, _stringLiteral2052738345);
		(L_64)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (Il2CppObject *)_stringLiteral2052738345);
		ObjectU5BU5D_t1108656482* L_65 = L_64;
		int32_t L_66 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_67 = L_66;
		Il2CppObject * L_68 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_67);
		NullCheck(L_65);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_65, ((int32_t)19));
		ArrayElementTypeCheck (L_65, L_68);
		(L_65)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)19)), (Il2CppObject *)L_68);
		ObjectU5BU5D_t1108656482* L_69 = L_65;
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, ((int32_t)20));
		ArrayElementTypeCheck (L_69, _stringLiteral1067506955);
		(L_69)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)20)), (Il2CppObject *)_stringLiteral1067506955);
		ObjectU5BU5D_t1108656482* L_70 = L_69;
		FsmBool_t1075959796 * L_71 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_71);
		bool L_72 = NamedVariable_get_IsNone_m281035543(L_71, /*hidden argument*/NULL);
		G_B21_0 = ((int32_t)21);
		G_B21_1 = L_70;
		G_B21_2 = L_70;
		G_B21_3 = G_B20_4;
		if (!L_72)
		{
			G_B22_0 = ((int32_t)21);
			G_B22_1 = L_70;
			G_B22_2 = L_70;
			G_B22_3 = G_B20_4;
			goto IL_01e3;
		}
	}
	{
		G_B23_0 = 0;
		G_B23_1 = G_B21_0;
		G_B23_2 = G_B21_1;
		G_B23_3 = G_B21_2;
		G_B23_4 = G_B21_3;
		goto IL_01ee;
	}

IL_01e3:
	{
		FsmBool_t1075959796 * L_73 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_73);
		bool L_74 = FsmBool_get_Value_m3101329097(L_73, /*hidden argument*/NULL);
		G_B23_0 = ((int32_t)(L_74));
		G_B23_1 = G_B22_0;
		G_B23_2 = G_B22_1;
		G_B23_3 = G_B22_2;
		G_B23_4 = G_B22_3;
	}

IL_01ee:
	{
		bool L_75 = ((bool)G_B23_0);
		Il2CppObject * L_76 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_75);
		NullCheck(G_B23_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B23_2, G_B23_1);
		ArrayElementTypeCheck (G_B23_2, L_76);
		(G_B23_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B23_1), (Il2CppObject *)L_76);
		ObjectU5BU5D_t1108656482* L_77 = G_B23_3;
		NullCheck(L_77);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_77, ((int32_t)22));
		ArrayElementTypeCheck (L_77, _stringLiteral109637894);
		(L_77)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)22)), (Il2CppObject *)_stringLiteral109637894);
		ObjectU5BU5D_t1108656482* L_78 = L_77;
		int32_t L_79 = __this->get_space_25();
		int32_t L_80 = L_79;
		Il2CppObject * L_81 = Box(Space_t4209342076_il2cpp_TypeInfo_var, &L_80);
		NullCheck(L_78);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_78, ((int32_t)23));
		ArrayElementTypeCheck (L_78, L_81);
		(L_78)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)23)), (Il2CppObject *)L_81);
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		Hashtable_t1407064410 * L_82 = iTween_Hash_m2099495140(NULL /*static, unused*/, L_78, /*hidden argument*/NULL);
		iTween_RotateBy_m3171499081(NULL /*static, unused*/, G_B23_4, L_82, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateFrom::.ctor()
extern "C"  void iTweenRotateFrom__ctor_m2614513399 (iTweenRotateFrom_t2181561423 * __this, const MethodInfo* method)
{
	{
		__this->set_easeType_24(((int32_t)21));
		iTweenFsmAction__ctor_m2960815636(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateFrom::Reset()
extern Il2CppClass* FsmString_t952858651_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmGameObject_t1697147867_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmFloat_t2134102846_il2cpp_TypeInfo_var;
extern const uint32_t iTweenRotateFrom_Reset_m260946340_MetadataUsageId;
extern "C"  void iTweenRotateFrom_Reset_m260946340 (iTweenRotateFrom_t2181561423 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenRotateFrom_Reset_m260946340_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmString_t952858651 * V_0 = NULL;
	FsmGameObject_t1697147867 * V_1 = NULL;
	FsmVector3_t533912882 * V_2 = NULL;
	FsmFloat_t2134102846 * V_3 = NULL;
	{
		iTweenFsmAction_Reset_m607248577(__this, /*hidden argument*/NULL);
		FsmString_t952858651 * L_0 = (FsmString_t952858651 *)il2cpp_codegen_object_new(FsmString_t952858651_il2cpp_TypeInfo_var);
		FsmString__ctor_m2679108260(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmString_t952858651 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmString_t952858651 * L_2 = V_0;
		__this->set_id_18(L_2);
		FsmGameObject_t1697147867 * L_3 = (FsmGameObject_t1697147867 *)il2cpp_codegen_object_new(FsmGameObject_t1697147867_il2cpp_TypeInfo_var);
		FsmGameObject__ctor_m1048269796(L_3, /*hidden argument*/NULL);
		V_1 = L_3;
		FsmGameObject_t1697147867 * L_4 = V_1;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_5 = V_1;
		__this->set_transformRotation_19(L_5);
		FsmVector3_t533912882 * L_6 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_6, /*hidden argument*/NULL);
		V_2 = L_6;
		FsmVector3_t533912882 * L_7 = V_2;
		NullCheck(L_7);
		NamedVariable_set_UseVariable_m4266138971(L_7, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_8 = V_2;
		__this->set_vectorRotation_20(L_8);
		FsmFloat_t2134102846 * L_9 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_time_21(L_9);
		FsmFloat_t2134102846 * L_10 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_delay_22(L_10);
		__this->set_loopType_25(0);
		FsmFloat_t2134102846 * L_11 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_11, /*hidden argument*/NULL);
		V_3 = L_11;
		FsmFloat_t2134102846 * L_12 = V_3;
		NullCheck(L_12);
		NamedVariable_set_UseVariable_m4266138971(L_12, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_13 = V_3;
		__this->set_speed_23(L_13);
		__this->set_space_26(0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateFrom::OnEnter()
extern "C"  void iTweenRotateFrom_OnEnter_m3678204174 (iTweenRotateFrom_t2181561423 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnEnteriTween_m3024539459(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_loopType_25();
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		iTweenFsmAction_IsLoop_m2539277653(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_001e:
	{
		iTweenRotateFrom_DoiTween_m3806311930(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateFrom::OnExit()
extern "C"  void iTweenRotateFrom_OnExit_m958855370 (iTweenRotateFrom_t2181561423 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnExitiTween_m1981625761(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateFrom::DoiTween()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* EaseType_t2734598229_il2cpp_TypeInfo_var;
extern Il2CppClass* LoopType_t1485160459_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* iTween_t3087282050_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3369786715;
extern Il2CppCodeGenString* _stringLiteral4254666622;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral3560141;
extern Il2CppCodeGenString* _stringLiteral109641799;
extern Il2CppCodeGenString* _stringLiteral95467907;
extern Il2CppCodeGenString* _stringLiteral3507899432;
extern Il2CppCodeGenString* _stringLiteral2258461662;
extern Il2CppCodeGenString* _stringLiteral2105864216;
extern Il2CppCodeGenString* _stringLiteral338257242;
extern Il2CppCodeGenString* _stringLiteral3696326558;
extern Il2CppCodeGenString* _stringLiteral2987624931;
extern Il2CppCodeGenString* _stringLiteral647687137;
extern Il2CppCodeGenString* _stringLiteral2052738345;
extern Il2CppCodeGenString* _stringLiteral1067506955;
extern Il2CppCodeGenString* _stringLiteral2094103681;
extern const uint32_t iTweenRotateFrom_DoiTween_m3806311930_MetadataUsageId;
extern "C"  void iTweenRotateFrom_DoiTween_m3806311930 (iTweenRotateFrom_t2181561423 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenRotateFrom_DoiTween_m3806311930_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  G_B5_0;
	memset(&G_B5_0, 0, sizeof(G_B5_0));
	Vector3_t4282066566  G_B10_0;
	memset(&G_B10_0, 0, sizeof(G_B10_0));
	int32_t G_B13_0 = 0;
	ObjectU5BU5D_t1108656482* G_B13_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B13_2 = NULL;
	GameObject_t3674682005 * G_B13_3 = NULL;
	int32_t G_B12_0 = 0;
	ObjectU5BU5D_t1108656482* G_B12_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B12_2 = NULL;
	GameObject_t3674682005 * G_B12_3 = NULL;
	String_t* G_B14_0 = NULL;
	int32_t G_B14_1 = 0;
	ObjectU5BU5D_t1108656482* G_B14_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B14_3 = NULL;
	GameObject_t3674682005 * G_B14_4 = NULL;
	int32_t G_B16_0 = 0;
	ObjectU5BU5D_t1108656482* G_B16_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B16_2 = NULL;
	GameObject_t3674682005 * G_B16_3 = NULL;
	int32_t G_B15_0 = 0;
	ObjectU5BU5D_t1108656482* G_B15_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B15_2 = NULL;
	GameObject_t3674682005 * G_B15_3 = NULL;
	String_t* G_B17_0 = NULL;
	int32_t G_B17_1 = 0;
	ObjectU5BU5D_t1108656482* G_B17_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B17_3 = NULL;
	GameObject_t3674682005 * G_B17_4 = NULL;
	int32_t G_B22_0 = 0;
	ObjectU5BU5D_t1108656482* G_B22_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B22_2 = NULL;
	GameObject_t3674682005 * G_B22_3 = NULL;
	int32_t G_B18_0 = 0;
	ObjectU5BU5D_t1108656482* G_B18_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B18_2 = NULL;
	GameObject_t3674682005 * G_B18_3 = NULL;
	int32_t G_B20_0 = 0;
	ObjectU5BU5D_t1108656482* G_B20_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B20_2 = NULL;
	GameObject_t3674682005 * G_B20_3 = NULL;
	int32_t G_B19_0 = 0;
	ObjectU5BU5D_t1108656482* G_B19_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B19_2 = NULL;
	GameObject_t3674682005 * G_B19_3 = NULL;
	float G_B21_0 = 0.0f;
	int32_t G_B21_1 = 0;
	ObjectU5BU5D_t1108656482* G_B21_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B21_3 = NULL;
	GameObject_t3674682005 * G_B21_4 = NULL;
	float G_B23_0 = 0.0f;
	int32_t G_B23_1 = 0;
	ObjectU5BU5D_t1108656482* G_B23_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B23_3 = NULL;
	GameObject_t3674682005 * G_B23_4 = NULL;
	int32_t G_B25_0 = 0;
	ObjectU5BU5D_t1108656482* G_B25_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B25_2 = NULL;
	GameObject_t3674682005 * G_B25_3 = NULL;
	int32_t G_B24_0 = 0;
	ObjectU5BU5D_t1108656482* G_B24_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B24_2 = NULL;
	GameObject_t3674682005 * G_B24_3 = NULL;
	float G_B26_0 = 0.0f;
	int32_t G_B26_1 = 0;
	ObjectU5BU5D_t1108656482* G_B26_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B26_3 = NULL;
	GameObject_t3674682005 * G_B26_4 = NULL;
	int32_t G_B28_0 = 0;
	ObjectU5BU5D_t1108656482* G_B28_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B28_2 = NULL;
	GameObject_t3674682005 * G_B28_3 = NULL;
	int32_t G_B27_0 = 0;
	ObjectU5BU5D_t1108656482* G_B27_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B27_2 = NULL;
	GameObject_t3674682005 * G_B27_3 = NULL;
	int32_t G_B29_0 = 0;
	int32_t G_B29_1 = 0;
	ObjectU5BU5D_t1108656482* G_B29_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B29_3 = NULL;
	GameObject_t3674682005 * G_B29_4 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_17();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		FsmVector3_t533912882 * L_5 = __this->get_vectorRotation_20();
		NullCheck(L_5);
		bool L_6 = NamedVariable_get_IsNone_m281035543(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0039;
		}
	}
	{
		Vector3_t4282066566  L_7 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B5_0 = L_7;
		goto IL_0044;
	}

IL_0039:
	{
		FsmVector3_t533912882 * L_8 = __this->get_vectorRotation_20();
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = FsmVector3_get_Value_m2779135117(L_8, /*hidden argument*/NULL);
		G_B5_0 = L_9;
	}

IL_0044:
	{
		V_1 = G_B5_0;
		FsmGameObject_t1697147867 * L_10 = __this->get_transformRotation_19();
		NullCheck(L_10);
		bool L_11 = NamedVariable_get_IsNone_m281035543(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_00b1;
		}
	}
	{
		FsmGameObject_t1697147867 * L_12 = __this->get_transformRotation_19();
		NullCheck(L_12);
		GameObject_t3674682005 * L_13 = FsmGameObject_get_Value_m673294275(L_12, /*hidden argument*/NULL);
		bool L_14 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00b1;
		}
	}
	{
		int32_t L_15 = __this->get_space_26();
		if (L_15)
		{
			goto IL_0095;
		}
	}
	{
		FsmGameObject_t1697147867 * L_16 = __this->get_transformRotation_19();
		NullCheck(L_16);
		GameObject_t3674682005 * L_17 = FsmGameObject_get_Value_m673294275(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_t1659122786 * L_18 = GameObject_get_transform_m1278640159(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector3_t4282066566  L_19 = Transform_get_eulerAngles_m1058084741(L_18, /*hidden argument*/NULL);
		Vector3_t4282066566  L_20 = V_1;
		Vector3_t4282066566  L_21 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		G_B10_0 = L_21;
		goto IL_00b0;
	}

IL_0095:
	{
		FsmGameObject_t1697147867 * L_22 = __this->get_transformRotation_19();
		NullCheck(L_22);
		GameObject_t3674682005 * L_23 = FsmGameObject_get_Value_m673294275(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_t1659122786 * L_24 = GameObject_get_transform_m1278640159(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		Vector3_t4282066566  L_25 = Transform_get_localEulerAngles_m3489183428(L_24, /*hidden argument*/NULL);
		Vector3_t4282066566  L_26 = V_1;
		Vector3_t4282066566  L_27 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_25, L_26, /*hidden argument*/NULL);
		G_B10_0 = L_27;
	}

IL_00b0:
	{
		V_1 = G_B10_0;
	}

IL_00b1:
	{
		((iTweenFsmAction_t410382178 *)__this)->set_itweenType_15(_stringLiteral3369786715);
		GameObject_t3674682005 * L_28 = V_0;
		ObjectU5BU5D_t1108656482* L_29 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)24)));
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 0);
		ArrayElementTypeCheck (L_29, _stringLiteral4254666622);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral4254666622);
		ObjectU5BU5D_t1108656482* L_30 = L_29;
		Vector3_t4282066566  L_31 = V_1;
		Vector3_t4282066566  L_32 = L_31;
		Il2CppObject * L_33 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 1);
		ArrayElementTypeCheck (L_30, L_33);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_33);
		ObjectU5BU5D_t1108656482* L_34 = L_30;
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, 2);
		ArrayElementTypeCheck (L_34, _stringLiteral3373707);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3373707);
		ObjectU5BU5D_t1108656482* L_35 = L_34;
		FsmString_t952858651 * L_36 = __this->get_id_18();
		NullCheck(L_36);
		bool L_37 = NamedVariable_get_IsNone_m281035543(L_36, /*hidden argument*/NULL);
		G_B12_0 = 3;
		G_B12_1 = L_35;
		G_B12_2 = L_35;
		G_B12_3 = L_28;
		if (!L_37)
		{
			G_B13_0 = 3;
			G_B13_1 = L_35;
			G_B13_2 = L_35;
			G_B13_3 = L_28;
			goto IL_00f9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_38 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B14_0 = L_38;
		G_B14_1 = G_B12_0;
		G_B14_2 = G_B12_1;
		G_B14_3 = G_B12_2;
		G_B14_4 = G_B12_3;
		goto IL_0104;
	}

IL_00f9:
	{
		FsmString_t952858651 * L_39 = __this->get_id_18();
		NullCheck(L_39);
		String_t* L_40 = FsmString_get_Value_m872383149(L_39, /*hidden argument*/NULL);
		G_B14_0 = L_40;
		G_B14_1 = G_B13_0;
		G_B14_2 = G_B13_1;
		G_B14_3 = G_B13_2;
		G_B14_4 = G_B13_3;
	}

IL_0104:
	{
		NullCheck(G_B14_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B14_2, G_B14_1);
		ArrayElementTypeCheck (G_B14_2, G_B14_0);
		(G_B14_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B14_1), (Il2CppObject *)G_B14_0);
		ObjectU5BU5D_t1108656482* L_41 = G_B14_3;
		FsmFloat_t2134102846 * L_42 = __this->get_speed_23();
		NullCheck(L_42);
		bool L_43 = NamedVariable_get_IsNone_m281035543(L_42, /*hidden argument*/NULL);
		G_B15_0 = 4;
		G_B15_1 = L_41;
		G_B15_2 = L_41;
		G_B15_3 = G_B14_4;
		if (!L_43)
		{
			G_B16_0 = 4;
			G_B16_1 = L_41;
			G_B16_2 = L_41;
			G_B16_3 = G_B14_4;
			goto IL_0121;
		}
	}
	{
		G_B17_0 = _stringLiteral3560141;
		G_B17_1 = G_B15_0;
		G_B17_2 = G_B15_1;
		G_B17_3 = G_B15_2;
		G_B17_4 = G_B15_3;
		goto IL_0126;
	}

IL_0121:
	{
		G_B17_0 = _stringLiteral109641799;
		G_B17_1 = G_B16_0;
		G_B17_2 = G_B16_1;
		G_B17_3 = G_B16_2;
		G_B17_4 = G_B16_3;
	}

IL_0126:
	{
		NullCheck(G_B17_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B17_2, G_B17_1);
		ArrayElementTypeCheck (G_B17_2, G_B17_0);
		(G_B17_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B17_1), (Il2CppObject *)G_B17_0);
		ObjectU5BU5D_t1108656482* L_44 = G_B17_3;
		FsmFloat_t2134102846 * L_45 = __this->get_speed_23();
		NullCheck(L_45);
		bool L_46 = NamedVariable_get_IsNone_m281035543(L_45, /*hidden argument*/NULL);
		G_B18_0 = 5;
		G_B18_1 = L_44;
		G_B18_2 = L_44;
		G_B18_3 = G_B17_4;
		if (!L_46)
		{
			G_B22_0 = 5;
			G_B22_1 = L_44;
			G_B22_2 = L_44;
			G_B22_3 = G_B17_4;
			goto IL_0163;
		}
	}
	{
		FsmFloat_t2134102846 * L_47 = __this->get_time_21();
		NullCheck(L_47);
		bool L_48 = NamedVariable_get_IsNone_m281035543(L_47, /*hidden argument*/NULL);
		G_B19_0 = G_B18_0;
		G_B19_1 = G_B18_1;
		G_B19_2 = G_B18_2;
		G_B19_3 = G_B18_3;
		if (!L_48)
		{
			G_B20_0 = G_B18_0;
			G_B20_1 = G_B18_1;
			G_B20_2 = G_B18_2;
			G_B20_3 = G_B18_3;
			goto IL_0153;
		}
	}
	{
		G_B21_0 = (1.0f);
		G_B21_1 = G_B19_0;
		G_B21_2 = G_B19_1;
		G_B21_3 = G_B19_2;
		G_B21_4 = G_B19_3;
		goto IL_015e;
	}

IL_0153:
	{
		FsmFloat_t2134102846 * L_49 = __this->get_time_21();
		NullCheck(L_49);
		float L_50 = FsmFloat_get_Value_m4137923823(L_49, /*hidden argument*/NULL);
		G_B21_0 = L_50;
		G_B21_1 = G_B20_0;
		G_B21_2 = G_B20_1;
		G_B21_3 = G_B20_2;
		G_B21_4 = G_B20_3;
	}

IL_015e:
	{
		G_B23_0 = G_B21_0;
		G_B23_1 = G_B21_1;
		G_B23_2 = G_B21_2;
		G_B23_3 = G_B21_3;
		G_B23_4 = G_B21_4;
		goto IL_016e;
	}

IL_0163:
	{
		FsmFloat_t2134102846 * L_51 = __this->get_speed_23();
		NullCheck(L_51);
		float L_52 = FsmFloat_get_Value_m4137923823(L_51, /*hidden argument*/NULL);
		G_B23_0 = L_52;
		G_B23_1 = G_B22_0;
		G_B23_2 = G_B22_1;
		G_B23_3 = G_B22_2;
		G_B23_4 = G_B22_3;
	}

IL_016e:
	{
		float L_53 = G_B23_0;
		Il2CppObject * L_54 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_53);
		NullCheck(G_B23_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B23_2, G_B23_1);
		ArrayElementTypeCheck (G_B23_2, L_54);
		(G_B23_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B23_1), (Il2CppObject *)L_54);
		ObjectU5BU5D_t1108656482* L_55 = G_B23_3;
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, 6);
		ArrayElementTypeCheck (L_55, _stringLiteral95467907);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral95467907);
		ObjectU5BU5D_t1108656482* L_56 = L_55;
		FsmFloat_t2134102846 * L_57 = __this->get_delay_22();
		NullCheck(L_57);
		bool L_58 = NamedVariable_get_IsNone_m281035543(L_57, /*hidden argument*/NULL);
		G_B24_0 = 7;
		G_B24_1 = L_56;
		G_B24_2 = L_56;
		G_B24_3 = G_B23_4;
		if (!L_58)
		{
			G_B25_0 = 7;
			G_B25_1 = L_56;
			G_B25_2 = L_56;
			G_B25_3 = G_B23_4;
			goto IL_0198;
		}
	}
	{
		G_B26_0 = (0.0f);
		G_B26_1 = G_B24_0;
		G_B26_2 = G_B24_1;
		G_B26_3 = G_B24_2;
		G_B26_4 = G_B24_3;
		goto IL_01a3;
	}

IL_0198:
	{
		FsmFloat_t2134102846 * L_59 = __this->get_delay_22();
		NullCheck(L_59);
		float L_60 = FsmFloat_get_Value_m4137923823(L_59, /*hidden argument*/NULL);
		G_B26_0 = L_60;
		G_B26_1 = G_B25_0;
		G_B26_2 = G_B25_1;
		G_B26_3 = G_B25_2;
		G_B26_4 = G_B25_3;
	}

IL_01a3:
	{
		float L_61 = G_B26_0;
		Il2CppObject * L_62 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_61);
		NullCheck(G_B26_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B26_2, G_B26_1);
		ArrayElementTypeCheck (G_B26_2, L_62);
		(G_B26_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B26_1), (Il2CppObject *)L_62);
		ObjectU5BU5D_t1108656482* L_63 = G_B26_3;
		NullCheck(L_63);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_63, 8);
		ArrayElementTypeCheck (L_63, _stringLiteral3507899432);
		(L_63)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral3507899432);
		ObjectU5BU5D_t1108656482* L_64 = L_63;
		int32_t L_65 = __this->get_easeType_24();
		int32_t L_66 = L_65;
		Il2CppObject * L_67 = Box(EaseType_t2734598229_il2cpp_TypeInfo_var, &L_66);
		NullCheck(L_64);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_64, ((int32_t)9));
		ArrayElementTypeCheck (L_64, L_67);
		(L_64)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)L_67);
		ObjectU5BU5D_t1108656482* L_68 = L_64;
		NullCheck(L_68);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_68, ((int32_t)10));
		ArrayElementTypeCheck (L_68, _stringLiteral2258461662);
		(L_68)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Il2CppObject *)_stringLiteral2258461662);
		ObjectU5BU5D_t1108656482* L_69 = L_68;
		int32_t L_70 = __this->get_loopType_25();
		int32_t L_71 = L_70;
		Il2CppObject * L_72 = Box(LoopType_t1485160459_il2cpp_TypeInfo_var, &L_71);
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, ((int32_t)11));
		ArrayElementTypeCheck (L_69, L_72);
		(L_69)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (Il2CppObject *)L_72);
		ObjectU5BU5D_t1108656482* L_73 = L_69;
		NullCheck(L_73);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_73, ((int32_t)12));
		ArrayElementTypeCheck (L_73, _stringLiteral2105864216);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (Il2CppObject *)_stringLiteral2105864216);
		ObjectU5BU5D_t1108656482* L_74 = L_73;
		NullCheck(L_74);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_74, ((int32_t)13));
		ArrayElementTypeCheck (L_74, _stringLiteral338257242);
		(L_74)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (Il2CppObject *)_stringLiteral338257242);
		ObjectU5BU5D_t1108656482* L_75 = L_74;
		NullCheck(L_75);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_75, ((int32_t)14));
		ArrayElementTypeCheck (L_75, _stringLiteral3696326558);
		(L_75)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (Il2CppObject *)_stringLiteral3696326558);
		ObjectU5BU5D_t1108656482* L_76 = L_75;
		int32_t L_77 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_78 = L_77;
		Il2CppObject * L_79 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_78);
		NullCheck(L_76);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_76, ((int32_t)15));
		ArrayElementTypeCheck (L_76, L_79);
		(L_76)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (Il2CppObject *)L_79);
		ObjectU5BU5D_t1108656482* L_80 = L_76;
		NullCheck(L_80);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_80, ((int32_t)16));
		ArrayElementTypeCheck (L_80, _stringLiteral2987624931);
		(L_80)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (Il2CppObject *)_stringLiteral2987624931);
		ObjectU5BU5D_t1108656482* L_81 = L_80;
		NullCheck(L_81);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_81, ((int32_t)17));
		ArrayElementTypeCheck (L_81, _stringLiteral647687137);
		(L_81)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (Il2CppObject *)_stringLiteral647687137);
		ObjectU5BU5D_t1108656482* L_82 = L_81;
		NullCheck(L_82);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_82, ((int32_t)18));
		ArrayElementTypeCheck (L_82, _stringLiteral2052738345);
		(L_82)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (Il2CppObject *)_stringLiteral2052738345);
		ObjectU5BU5D_t1108656482* L_83 = L_82;
		int32_t L_84 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_85 = L_84;
		Il2CppObject * L_86 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_85);
		NullCheck(L_83);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_83, ((int32_t)19));
		ArrayElementTypeCheck (L_83, L_86);
		(L_83)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)19)), (Il2CppObject *)L_86);
		ObjectU5BU5D_t1108656482* L_87 = L_83;
		NullCheck(L_87);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_87, ((int32_t)20));
		ArrayElementTypeCheck (L_87, _stringLiteral1067506955);
		(L_87)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)20)), (Il2CppObject *)_stringLiteral1067506955);
		ObjectU5BU5D_t1108656482* L_88 = L_87;
		FsmBool_t1075959796 * L_89 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_89);
		bool L_90 = NamedVariable_get_IsNone_m281035543(L_89, /*hidden argument*/NULL);
		G_B27_0 = ((int32_t)21);
		G_B27_1 = L_88;
		G_B27_2 = L_88;
		G_B27_3 = G_B26_4;
		if (!L_90)
		{
			G_B28_0 = ((int32_t)21);
			G_B28_1 = L_88;
			G_B28_2 = L_88;
			G_B28_3 = G_B26_4;
			goto IL_024e;
		}
	}
	{
		G_B29_0 = 0;
		G_B29_1 = G_B27_0;
		G_B29_2 = G_B27_1;
		G_B29_3 = G_B27_2;
		G_B29_4 = G_B27_3;
		goto IL_0259;
	}

IL_024e:
	{
		FsmBool_t1075959796 * L_91 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_91);
		bool L_92 = FsmBool_get_Value_m3101329097(L_91, /*hidden argument*/NULL);
		G_B29_0 = ((int32_t)(L_92));
		G_B29_1 = G_B28_0;
		G_B29_2 = G_B28_1;
		G_B29_3 = G_B28_2;
		G_B29_4 = G_B28_3;
	}

IL_0259:
	{
		bool L_93 = ((bool)G_B29_0);
		Il2CppObject * L_94 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_93);
		NullCheck(G_B29_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B29_2, G_B29_1);
		ArrayElementTypeCheck (G_B29_2, L_94);
		(G_B29_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B29_1), (Il2CppObject *)L_94);
		ObjectU5BU5D_t1108656482* L_95 = G_B29_3;
		NullCheck(L_95);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_95, ((int32_t)22));
		ArrayElementTypeCheck (L_95, _stringLiteral2094103681);
		(L_95)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)22)), (Il2CppObject *)_stringLiteral2094103681);
		ObjectU5BU5D_t1108656482* L_96 = L_95;
		int32_t L_97 = __this->get_space_26();
		bool L_98 = ((bool)((((int32_t)L_97) == ((int32_t)1))? 1 : 0));
		Il2CppObject * L_99 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_98);
		NullCheck(L_96);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_96, ((int32_t)23));
		ArrayElementTypeCheck (L_96, L_99);
		(L_96)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)23)), (Il2CppObject *)L_99);
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		Hashtable_t1407064410 * L_100 = iTween_Hash_m2099495140(NULL /*static, unused*/, L_96, /*hidden argument*/NULL);
		iTween_RotateFrom_m2536490006(NULL /*static, unused*/, G_B29_4, L_100, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateTo::.ctor()
extern "C"  void iTweenRotateTo__ctor_m139891398 (iTweenRotateTo_t1141197216 * __this, const MethodInfo* method)
{
	{
		__this->set_easeType_24(((int32_t)21));
		iTweenFsmAction__ctor_m2960815636(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateTo::Reset()
extern Il2CppClass* FsmString_t952858651_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmGameObject_t1697147867_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmFloat_t2134102846_il2cpp_TypeInfo_var;
extern const uint32_t iTweenRotateTo_Reset_m2081291635_MetadataUsageId;
extern "C"  void iTweenRotateTo_Reset_m2081291635 (iTweenRotateTo_t1141197216 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenRotateTo_Reset_m2081291635_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmString_t952858651 * V_0 = NULL;
	FsmGameObject_t1697147867 * V_1 = NULL;
	FsmVector3_t533912882 * V_2 = NULL;
	FsmFloat_t2134102846 * V_3 = NULL;
	{
		iTweenFsmAction_Reset_m607248577(__this, /*hidden argument*/NULL);
		FsmString_t952858651 * L_0 = (FsmString_t952858651 *)il2cpp_codegen_object_new(FsmString_t952858651_il2cpp_TypeInfo_var);
		FsmString__ctor_m2679108260(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmString_t952858651 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmString_t952858651 * L_2 = V_0;
		__this->set_id_18(L_2);
		FsmGameObject_t1697147867 * L_3 = (FsmGameObject_t1697147867 *)il2cpp_codegen_object_new(FsmGameObject_t1697147867_il2cpp_TypeInfo_var);
		FsmGameObject__ctor_m1048269796(L_3, /*hidden argument*/NULL);
		V_1 = L_3;
		FsmGameObject_t1697147867 * L_4 = V_1;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_5 = V_1;
		__this->set_transformRotation_19(L_5);
		FsmVector3_t533912882 * L_6 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_6, /*hidden argument*/NULL);
		V_2 = L_6;
		FsmVector3_t533912882 * L_7 = V_2;
		NullCheck(L_7);
		NamedVariable_set_UseVariable_m4266138971(L_7, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_8 = V_2;
		__this->set_vectorRotation_20(L_8);
		FsmFloat_t2134102846 * L_9 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_time_21(L_9);
		FsmFloat_t2134102846 * L_10 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_delay_22(L_10);
		__this->set_loopType_25(0);
		FsmFloat_t2134102846 * L_11 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_11, /*hidden argument*/NULL);
		V_3 = L_11;
		FsmFloat_t2134102846 * L_12 = V_3;
		NullCheck(L_12);
		NamedVariable_set_UseVariable_m4266138971(L_12, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_13 = V_3;
		__this->set_speed_23(L_13);
		__this->set_space_26(0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateTo::OnEnter()
extern "C"  void iTweenRotateTo_OnEnter_m683375901 (iTweenRotateTo_t1141197216 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnEnteriTween_m3024539459(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_loopType_25();
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		iTweenFsmAction_IsLoop_m2539277653(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_001e:
	{
		iTweenRotateTo_DoiTween_m1160948683(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateTo::OnExit()
extern "C"  void iTweenRotateTo_OnExit_m1554984667 (iTweenRotateTo_t1141197216 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnExitiTween_m1981625761(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateTo::DoiTween()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* EaseType_t2734598229_il2cpp_TypeInfo_var;
extern Il2CppClass* LoopType_t1485160459_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* iTween_t3087282050_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3369786715;
extern Il2CppCodeGenString* _stringLiteral4254666622;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral3560141;
extern Il2CppCodeGenString* _stringLiteral109641799;
extern Il2CppCodeGenString* _stringLiteral95467907;
extern Il2CppCodeGenString* _stringLiteral3507899432;
extern Il2CppCodeGenString* _stringLiteral2258461662;
extern Il2CppCodeGenString* _stringLiteral2105864216;
extern Il2CppCodeGenString* _stringLiteral338257242;
extern Il2CppCodeGenString* _stringLiteral3696326558;
extern Il2CppCodeGenString* _stringLiteral2987624931;
extern Il2CppCodeGenString* _stringLiteral647687137;
extern Il2CppCodeGenString* _stringLiteral2052738345;
extern Il2CppCodeGenString* _stringLiteral1067506955;
extern Il2CppCodeGenString* _stringLiteral2094103681;
extern const uint32_t iTweenRotateTo_DoiTween_m1160948683_MetadataUsageId;
extern "C"  void iTweenRotateTo_DoiTween_m1160948683 (iTweenRotateTo_t1141197216 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenRotateTo_DoiTween_m1160948683_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  G_B5_0;
	memset(&G_B5_0, 0, sizeof(G_B5_0));
	Vector3_t4282066566  G_B10_0;
	memset(&G_B10_0, 0, sizeof(G_B10_0));
	int32_t G_B13_0 = 0;
	ObjectU5BU5D_t1108656482* G_B13_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B13_2 = NULL;
	GameObject_t3674682005 * G_B13_3 = NULL;
	int32_t G_B12_0 = 0;
	ObjectU5BU5D_t1108656482* G_B12_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B12_2 = NULL;
	GameObject_t3674682005 * G_B12_3 = NULL;
	String_t* G_B14_0 = NULL;
	int32_t G_B14_1 = 0;
	ObjectU5BU5D_t1108656482* G_B14_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B14_3 = NULL;
	GameObject_t3674682005 * G_B14_4 = NULL;
	int32_t G_B16_0 = 0;
	ObjectU5BU5D_t1108656482* G_B16_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B16_2 = NULL;
	GameObject_t3674682005 * G_B16_3 = NULL;
	int32_t G_B15_0 = 0;
	ObjectU5BU5D_t1108656482* G_B15_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B15_2 = NULL;
	GameObject_t3674682005 * G_B15_3 = NULL;
	String_t* G_B17_0 = NULL;
	int32_t G_B17_1 = 0;
	ObjectU5BU5D_t1108656482* G_B17_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B17_3 = NULL;
	GameObject_t3674682005 * G_B17_4 = NULL;
	int32_t G_B22_0 = 0;
	ObjectU5BU5D_t1108656482* G_B22_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B22_2 = NULL;
	GameObject_t3674682005 * G_B22_3 = NULL;
	int32_t G_B18_0 = 0;
	ObjectU5BU5D_t1108656482* G_B18_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B18_2 = NULL;
	GameObject_t3674682005 * G_B18_3 = NULL;
	int32_t G_B20_0 = 0;
	ObjectU5BU5D_t1108656482* G_B20_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B20_2 = NULL;
	GameObject_t3674682005 * G_B20_3 = NULL;
	int32_t G_B19_0 = 0;
	ObjectU5BU5D_t1108656482* G_B19_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B19_2 = NULL;
	GameObject_t3674682005 * G_B19_3 = NULL;
	float G_B21_0 = 0.0f;
	int32_t G_B21_1 = 0;
	ObjectU5BU5D_t1108656482* G_B21_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B21_3 = NULL;
	GameObject_t3674682005 * G_B21_4 = NULL;
	float G_B23_0 = 0.0f;
	int32_t G_B23_1 = 0;
	ObjectU5BU5D_t1108656482* G_B23_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B23_3 = NULL;
	GameObject_t3674682005 * G_B23_4 = NULL;
	int32_t G_B25_0 = 0;
	ObjectU5BU5D_t1108656482* G_B25_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B25_2 = NULL;
	GameObject_t3674682005 * G_B25_3 = NULL;
	int32_t G_B24_0 = 0;
	ObjectU5BU5D_t1108656482* G_B24_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B24_2 = NULL;
	GameObject_t3674682005 * G_B24_3 = NULL;
	float G_B26_0 = 0.0f;
	int32_t G_B26_1 = 0;
	ObjectU5BU5D_t1108656482* G_B26_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B26_3 = NULL;
	GameObject_t3674682005 * G_B26_4 = NULL;
	int32_t G_B28_0 = 0;
	ObjectU5BU5D_t1108656482* G_B28_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B28_2 = NULL;
	GameObject_t3674682005 * G_B28_3 = NULL;
	int32_t G_B27_0 = 0;
	ObjectU5BU5D_t1108656482* G_B27_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B27_2 = NULL;
	GameObject_t3674682005 * G_B27_3 = NULL;
	int32_t G_B29_0 = 0;
	int32_t G_B29_1 = 0;
	ObjectU5BU5D_t1108656482* G_B29_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B29_3 = NULL;
	GameObject_t3674682005 * G_B29_4 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_17();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		FsmVector3_t533912882 * L_5 = __this->get_vectorRotation_20();
		NullCheck(L_5);
		bool L_6 = NamedVariable_get_IsNone_m281035543(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0039;
		}
	}
	{
		Vector3_t4282066566  L_7 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B5_0 = L_7;
		goto IL_0044;
	}

IL_0039:
	{
		FsmVector3_t533912882 * L_8 = __this->get_vectorRotation_20();
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = FsmVector3_get_Value_m2779135117(L_8, /*hidden argument*/NULL);
		G_B5_0 = L_9;
	}

IL_0044:
	{
		V_1 = G_B5_0;
		FsmGameObject_t1697147867 * L_10 = __this->get_transformRotation_19();
		NullCheck(L_10);
		bool L_11 = NamedVariable_get_IsNone_m281035543(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_00b1;
		}
	}
	{
		FsmGameObject_t1697147867 * L_12 = __this->get_transformRotation_19();
		NullCheck(L_12);
		GameObject_t3674682005 * L_13 = FsmGameObject_get_Value_m673294275(L_12, /*hidden argument*/NULL);
		bool L_14 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00b1;
		}
	}
	{
		int32_t L_15 = __this->get_space_26();
		if (L_15)
		{
			goto IL_0095;
		}
	}
	{
		FsmGameObject_t1697147867 * L_16 = __this->get_transformRotation_19();
		NullCheck(L_16);
		GameObject_t3674682005 * L_17 = FsmGameObject_get_Value_m673294275(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_t1659122786 * L_18 = GameObject_get_transform_m1278640159(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector3_t4282066566  L_19 = Transform_get_eulerAngles_m1058084741(L_18, /*hidden argument*/NULL);
		Vector3_t4282066566  L_20 = V_1;
		Vector3_t4282066566  L_21 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		G_B10_0 = L_21;
		goto IL_00b0;
	}

IL_0095:
	{
		FsmGameObject_t1697147867 * L_22 = __this->get_transformRotation_19();
		NullCheck(L_22);
		GameObject_t3674682005 * L_23 = FsmGameObject_get_Value_m673294275(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_t1659122786 * L_24 = GameObject_get_transform_m1278640159(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		Vector3_t4282066566  L_25 = Transform_get_localEulerAngles_m3489183428(L_24, /*hidden argument*/NULL);
		Vector3_t4282066566  L_26 = V_1;
		Vector3_t4282066566  L_27 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_25, L_26, /*hidden argument*/NULL);
		G_B10_0 = L_27;
	}

IL_00b0:
	{
		V_1 = G_B10_0;
	}

IL_00b1:
	{
		((iTweenFsmAction_t410382178 *)__this)->set_itweenType_15(_stringLiteral3369786715);
		GameObject_t3674682005 * L_28 = V_0;
		ObjectU5BU5D_t1108656482* L_29 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)24)));
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 0);
		ArrayElementTypeCheck (L_29, _stringLiteral4254666622);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral4254666622);
		ObjectU5BU5D_t1108656482* L_30 = L_29;
		Vector3_t4282066566  L_31 = V_1;
		Vector3_t4282066566  L_32 = L_31;
		Il2CppObject * L_33 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 1);
		ArrayElementTypeCheck (L_30, L_33);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_33);
		ObjectU5BU5D_t1108656482* L_34 = L_30;
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, 2);
		ArrayElementTypeCheck (L_34, _stringLiteral3373707);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3373707);
		ObjectU5BU5D_t1108656482* L_35 = L_34;
		FsmString_t952858651 * L_36 = __this->get_id_18();
		NullCheck(L_36);
		bool L_37 = NamedVariable_get_IsNone_m281035543(L_36, /*hidden argument*/NULL);
		G_B12_0 = 3;
		G_B12_1 = L_35;
		G_B12_2 = L_35;
		G_B12_3 = L_28;
		if (!L_37)
		{
			G_B13_0 = 3;
			G_B13_1 = L_35;
			G_B13_2 = L_35;
			G_B13_3 = L_28;
			goto IL_00f9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_38 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B14_0 = L_38;
		G_B14_1 = G_B12_0;
		G_B14_2 = G_B12_1;
		G_B14_3 = G_B12_2;
		G_B14_4 = G_B12_3;
		goto IL_0104;
	}

IL_00f9:
	{
		FsmString_t952858651 * L_39 = __this->get_id_18();
		NullCheck(L_39);
		String_t* L_40 = FsmString_get_Value_m872383149(L_39, /*hidden argument*/NULL);
		G_B14_0 = L_40;
		G_B14_1 = G_B13_0;
		G_B14_2 = G_B13_1;
		G_B14_3 = G_B13_2;
		G_B14_4 = G_B13_3;
	}

IL_0104:
	{
		NullCheck(G_B14_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B14_2, G_B14_1);
		ArrayElementTypeCheck (G_B14_2, G_B14_0);
		(G_B14_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B14_1), (Il2CppObject *)G_B14_0);
		ObjectU5BU5D_t1108656482* L_41 = G_B14_3;
		FsmFloat_t2134102846 * L_42 = __this->get_speed_23();
		NullCheck(L_42);
		bool L_43 = NamedVariable_get_IsNone_m281035543(L_42, /*hidden argument*/NULL);
		G_B15_0 = 4;
		G_B15_1 = L_41;
		G_B15_2 = L_41;
		G_B15_3 = G_B14_4;
		if (!L_43)
		{
			G_B16_0 = 4;
			G_B16_1 = L_41;
			G_B16_2 = L_41;
			G_B16_3 = G_B14_4;
			goto IL_0121;
		}
	}
	{
		G_B17_0 = _stringLiteral3560141;
		G_B17_1 = G_B15_0;
		G_B17_2 = G_B15_1;
		G_B17_3 = G_B15_2;
		G_B17_4 = G_B15_3;
		goto IL_0126;
	}

IL_0121:
	{
		G_B17_0 = _stringLiteral109641799;
		G_B17_1 = G_B16_0;
		G_B17_2 = G_B16_1;
		G_B17_3 = G_B16_2;
		G_B17_4 = G_B16_3;
	}

IL_0126:
	{
		NullCheck(G_B17_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B17_2, G_B17_1);
		ArrayElementTypeCheck (G_B17_2, G_B17_0);
		(G_B17_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B17_1), (Il2CppObject *)G_B17_0);
		ObjectU5BU5D_t1108656482* L_44 = G_B17_3;
		FsmFloat_t2134102846 * L_45 = __this->get_speed_23();
		NullCheck(L_45);
		bool L_46 = NamedVariable_get_IsNone_m281035543(L_45, /*hidden argument*/NULL);
		G_B18_0 = 5;
		G_B18_1 = L_44;
		G_B18_2 = L_44;
		G_B18_3 = G_B17_4;
		if (!L_46)
		{
			G_B22_0 = 5;
			G_B22_1 = L_44;
			G_B22_2 = L_44;
			G_B22_3 = G_B17_4;
			goto IL_0163;
		}
	}
	{
		FsmFloat_t2134102846 * L_47 = __this->get_time_21();
		NullCheck(L_47);
		bool L_48 = NamedVariable_get_IsNone_m281035543(L_47, /*hidden argument*/NULL);
		G_B19_0 = G_B18_0;
		G_B19_1 = G_B18_1;
		G_B19_2 = G_B18_2;
		G_B19_3 = G_B18_3;
		if (!L_48)
		{
			G_B20_0 = G_B18_0;
			G_B20_1 = G_B18_1;
			G_B20_2 = G_B18_2;
			G_B20_3 = G_B18_3;
			goto IL_0153;
		}
	}
	{
		G_B21_0 = (1.0f);
		G_B21_1 = G_B19_0;
		G_B21_2 = G_B19_1;
		G_B21_3 = G_B19_2;
		G_B21_4 = G_B19_3;
		goto IL_015e;
	}

IL_0153:
	{
		FsmFloat_t2134102846 * L_49 = __this->get_time_21();
		NullCheck(L_49);
		float L_50 = FsmFloat_get_Value_m4137923823(L_49, /*hidden argument*/NULL);
		G_B21_0 = L_50;
		G_B21_1 = G_B20_0;
		G_B21_2 = G_B20_1;
		G_B21_3 = G_B20_2;
		G_B21_4 = G_B20_3;
	}

IL_015e:
	{
		G_B23_0 = G_B21_0;
		G_B23_1 = G_B21_1;
		G_B23_2 = G_B21_2;
		G_B23_3 = G_B21_3;
		G_B23_4 = G_B21_4;
		goto IL_016e;
	}

IL_0163:
	{
		FsmFloat_t2134102846 * L_51 = __this->get_speed_23();
		NullCheck(L_51);
		float L_52 = FsmFloat_get_Value_m4137923823(L_51, /*hidden argument*/NULL);
		G_B23_0 = L_52;
		G_B23_1 = G_B22_0;
		G_B23_2 = G_B22_1;
		G_B23_3 = G_B22_2;
		G_B23_4 = G_B22_3;
	}

IL_016e:
	{
		float L_53 = G_B23_0;
		Il2CppObject * L_54 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_53);
		NullCheck(G_B23_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B23_2, G_B23_1);
		ArrayElementTypeCheck (G_B23_2, L_54);
		(G_B23_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B23_1), (Il2CppObject *)L_54);
		ObjectU5BU5D_t1108656482* L_55 = G_B23_3;
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, 6);
		ArrayElementTypeCheck (L_55, _stringLiteral95467907);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral95467907);
		ObjectU5BU5D_t1108656482* L_56 = L_55;
		FsmFloat_t2134102846 * L_57 = __this->get_delay_22();
		NullCheck(L_57);
		bool L_58 = NamedVariable_get_IsNone_m281035543(L_57, /*hidden argument*/NULL);
		G_B24_0 = 7;
		G_B24_1 = L_56;
		G_B24_2 = L_56;
		G_B24_3 = G_B23_4;
		if (!L_58)
		{
			G_B25_0 = 7;
			G_B25_1 = L_56;
			G_B25_2 = L_56;
			G_B25_3 = G_B23_4;
			goto IL_0198;
		}
	}
	{
		G_B26_0 = (0.0f);
		G_B26_1 = G_B24_0;
		G_B26_2 = G_B24_1;
		G_B26_3 = G_B24_2;
		G_B26_4 = G_B24_3;
		goto IL_01a3;
	}

IL_0198:
	{
		FsmFloat_t2134102846 * L_59 = __this->get_delay_22();
		NullCheck(L_59);
		float L_60 = FsmFloat_get_Value_m4137923823(L_59, /*hidden argument*/NULL);
		G_B26_0 = L_60;
		G_B26_1 = G_B25_0;
		G_B26_2 = G_B25_1;
		G_B26_3 = G_B25_2;
		G_B26_4 = G_B25_3;
	}

IL_01a3:
	{
		float L_61 = G_B26_0;
		Il2CppObject * L_62 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_61);
		NullCheck(G_B26_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B26_2, G_B26_1);
		ArrayElementTypeCheck (G_B26_2, L_62);
		(G_B26_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B26_1), (Il2CppObject *)L_62);
		ObjectU5BU5D_t1108656482* L_63 = G_B26_3;
		NullCheck(L_63);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_63, 8);
		ArrayElementTypeCheck (L_63, _stringLiteral3507899432);
		(L_63)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral3507899432);
		ObjectU5BU5D_t1108656482* L_64 = L_63;
		int32_t L_65 = __this->get_easeType_24();
		int32_t L_66 = L_65;
		Il2CppObject * L_67 = Box(EaseType_t2734598229_il2cpp_TypeInfo_var, &L_66);
		NullCheck(L_64);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_64, ((int32_t)9));
		ArrayElementTypeCheck (L_64, L_67);
		(L_64)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)L_67);
		ObjectU5BU5D_t1108656482* L_68 = L_64;
		NullCheck(L_68);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_68, ((int32_t)10));
		ArrayElementTypeCheck (L_68, _stringLiteral2258461662);
		(L_68)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Il2CppObject *)_stringLiteral2258461662);
		ObjectU5BU5D_t1108656482* L_69 = L_68;
		int32_t L_70 = __this->get_loopType_25();
		int32_t L_71 = L_70;
		Il2CppObject * L_72 = Box(LoopType_t1485160459_il2cpp_TypeInfo_var, &L_71);
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, ((int32_t)11));
		ArrayElementTypeCheck (L_69, L_72);
		(L_69)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (Il2CppObject *)L_72);
		ObjectU5BU5D_t1108656482* L_73 = L_69;
		NullCheck(L_73);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_73, ((int32_t)12));
		ArrayElementTypeCheck (L_73, _stringLiteral2105864216);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (Il2CppObject *)_stringLiteral2105864216);
		ObjectU5BU5D_t1108656482* L_74 = L_73;
		NullCheck(L_74);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_74, ((int32_t)13));
		ArrayElementTypeCheck (L_74, _stringLiteral338257242);
		(L_74)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (Il2CppObject *)_stringLiteral338257242);
		ObjectU5BU5D_t1108656482* L_75 = L_74;
		NullCheck(L_75);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_75, ((int32_t)14));
		ArrayElementTypeCheck (L_75, _stringLiteral3696326558);
		(L_75)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (Il2CppObject *)_stringLiteral3696326558);
		ObjectU5BU5D_t1108656482* L_76 = L_75;
		int32_t L_77 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_78 = L_77;
		Il2CppObject * L_79 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_78);
		NullCheck(L_76);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_76, ((int32_t)15));
		ArrayElementTypeCheck (L_76, L_79);
		(L_76)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (Il2CppObject *)L_79);
		ObjectU5BU5D_t1108656482* L_80 = L_76;
		NullCheck(L_80);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_80, ((int32_t)16));
		ArrayElementTypeCheck (L_80, _stringLiteral2987624931);
		(L_80)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (Il2CppObject *)_stringLiteral2987624931);
		ObjectU5BU5D_t1108656482* L_81 = L_80;
		NullCheck(L_81);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_81, ((int32_t)17));
		ArrayElementTypeCheck (L_81, _stringLiteral647687137);
		(L_81)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (Il2CppObject *)_stringLiteral647687137);
		ObjectU5BU5D_t1108656482* L_82 = L_81;
		NullCheck(L_82);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_82, ((int32_t)18));
		ArrayElementTypeCheck (L_82, _stringLiteral2052738345);
		(L_82)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (Il2CppObject *)_stringLiteral2052738345);
		ObjectU5BU5D_t1108656482* L_83 = L_82;
		int32_t L_84 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_85 = L_84;
		Il2CppObject * L_86 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_85);
		NullCheck(L_83);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_83, ((int32_t)19));
		ArrayElementTypeCheck (L_83, L_86);
		(L_83)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)19)), (Il2CppObject *)L_86);
		ObjectU5BU5D_t1108656482* L_87 = L_83;
		NullCheck(L_87);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_87, ((int32_t)20));
		ArrayElementTypeCheck (L_87, _stringLiteral1067506955);
		(L_87)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)20)), (Il2CppObject *)_stringLiteral1067506955);
		ObjectU5BU5D_t1108656482* L_88 = L_87;
		FsmBool_t1075959796 * L_89 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_89);
		bool L_90 = NamedVariable_get_IsNone_m281035543(L_89, /*hidden argument*/NULL);
		G_B27_0 = ((int32_t)21);
		G_B27_1 = L_88;
		G_B27_2 = L_88;
		G_B27_3 = G_B26_4;
		if (!L_90)
		{
			G_B28_0 = ((int32_t)21);
			G_B28_1 = L_88;
			G_B28_2 = L_88;
			G_B28_3 = G_B26_4;
			goto IL_024e;
		}
	}
	{
		G_B29_0 = 0;
		G_B29_1 = G_B27_0;
		G_B29_2 = G_B27_1;
		G_B29_3 = G_B27_2;
		G_B29_4 = G_B27_3;
		goto IL_0259;
	}

IL_024e:
	{
		FsmBool_t1075959796 * L_91 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_91);
		bool L_92 = FsmBool_get_Value_m3101329097(L_91, /*hidden argument*/NULL);
		G_B29_0 = ((int32_t)(L_92));
		G_B29_1 = G_B28_0;
		G_B29_2 = G_B28_1;
		G_B29_3 = G_B28_2;
		G_B29_4 = G_B28_3;
	}

IL_0259:
	{
		bool L_93 = ((bool)G_B29_0);
		Il2CppObject * L_94 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_93);
		NullCheck(G_B29_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B29_2, G_B29_1);
		ArrayElementTypeCheck (G_B29_2, L_94);
		(G_B29_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B29_1), (Il2CppObject *)L_94);
		ObjectU5BU5D_t1108656482* L_95 = G_B29_3;
		NullCheck(L_95);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_95, ((int32_t)22));
		ArrayElementTypeCheck (L_95, _stringLiteral2094103681);
		(L_95)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)22)), (Il2CppObject *)_stringLiteral2094103681);
		ObjectU5BU5D_t1108656482* L_96 = L_95;
		int32_t L_97 = __this->get_space_26();
		bool L_98 = ((bool)((((int32_t)L_97) == ((int32_t)1))? 1 : 0));
		Il2CppObject * L_99 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_98);
		NullCheck(L_96);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_96, ((int32_t)23));
		ArrayElementTypeCheck (L_96, L_99);
		(L_96)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)23)), (Il2CppObject *)L_99);
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		Hashtable_t1407064410 * L_100 = iTween_Hash_m2099495140(NULL /*static, unused*/, L_96, /*hidden argument*/NULL);
		iTween_RotateTo_m2682538149(NULL /*static, unused*/, G_B29_4, L_100, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateUpdate::.ctor()
extern "C"  void iTweenRotateUpdate__ctor_m1824768312 (iTweenRotateUpdate_t1671849710 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateUpdate::Reset()
extern Il2CppClass* FsmGameObject_t1697147867_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern const uint32_t iTweenRotateUpdate_Reset_m3766168549_MetadataUsageId;
extern "C"  void iTweenRotateUpdate_Reset_m3766168549 (iTweenRotateUpdate_t1671849710 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenRotateUpdate_Reset_m3766168549_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmGameObject_t1697147867 * V_0 = NULL;
	FsmVector3_t533912882 * V_1 = NULL;
	{
		FsmGameObject_t1697147867 * L_0 = (FsmGameObject_t1697147867 *)il2cpp_codegen_object_new(FsmGameObject_t1697147867_il2cpp_TypeInfo_var);
		FsmGameObject__ctor_m1048269796(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmGameObject_t1697147867 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_2 = V_0;
		__this->set_transformRotation_10(L_2);
		FsmVector3_t533912882 * L_3 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_3, /*hidden argument*/NULL);
		V_1 = L_3;
		FsmVector3_t533912882 * L_4 = V_1;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_5 = V_1;
		__this->set_vectorRotation_11(L_5);
		FsmFloat_t2134102846 * L_6 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_time_12(L_6);
		__this->set_space_13(0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateUpdate::OnEnter()
extern Il2CppClass* Hashtable_t1407064410_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4254666622;
extern Il2CppCodeGenString* _stringLiteral3560141;
extern Il2CppCodeGenString* _stringLiteral2094103681;
extern const uint32_t iTweenRotateUpdate_OnEnter_m647419663_MetadataUsageId;
extern "C"  void iTweenRotateUpdate_OnEnter_m647419663 (iTweenRotateUpdate_t1671849710 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenRotateUpdate_OnEnter_m647419663_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B5_0 = NULL;
	Hashtable_t1407064410 * G_B5_1 = NULL;
	String_t* G_B4_0 = NULL;
	Hashtable_t1407064410 * G_B4_1 = NULL;
	Vector3_t4282066566  G_B6_0;
	memset(&G_B6_0, 0, sizeof(G_B6_0));
	String_t* G_B6_1 = NULL;
	Hashtable_t1407064410 * G_B6_2 = NULL;
	String_t* G_B14_0 = NULL;
	Hashtable_t1407064410 * G_B14_1 = NULL;
	String_t* G_B13_0 = NULL;
	Hashtable_t1407064410 * G_B13_1 = NULL;
	float G_B15_0 = 0.0f;
	String_t* G_B15_1 = NULL;
	Hashtable_t1407064410 * G_B15_2 = NULL;
	{
		Hashtable_t1407064410 * L_0 = (Hashtable_t1407064410 *)il2cpp_codegen_object_new(Hashtable_t1407064410_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1514037738(L_0, /*hidden argument*/NULL);
		__this->set_hash_14(L_0);
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_2 = __this->get_gameObject_9();
		NullCheck(L_1);
		GameObject_t3674682005 * L_3 = Fsm_GetOwnerDefaultTarget_m846013999(L_1, L_2, /*hidden argument*/NULL);
		__this->set_go_15(L_3);
		GameObject_t3674682005 * L_4 = __this->get_go_15();
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003a;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_003a:
	{
		FsmGameObject_t1697147867 * L_6 = __this->get_transformRotation_10();
		NullCheck(L_6);
		bool L_7 = NamedVariable_get_IsNone_m281035543(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0089;
		}
	}
	{
		Hashtable_t1407064410 * L_8 = __this->get_hash_14();
		FsmVector3_t533912882 * L_9 = __this->get_vectorRotation_11();
		NullCheck(L_9);
		bool L_10 = NamedVariable_get_IsNone_m281035543(L_9, /*hidden argument*/NULL);
		G_B4_0 = _stringLiteral4254666622;
		G_B4_1 = L_8;
		if (!L_10)
		{
			G_B5_0 = _stringLiteral4254666622;
			G_B5_1 = L_8;
			goto IL_006f;
		}
	}
	{
		Vector3_t4282066566  L_11 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		goto IL_007a;
	}

IL_006f:
	{
		FsmVector3_t533912882 * L_12 = __this->get_vectorRotation_11();
		NullCheck(L_12);
		Vector3_t4282066566  L_13 = FsmVector3_get_Value_m2779135117(L_12, /*hidden argument*/NULL);
		G_B6_0 = L_13;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
	}

IL_007a:
	{
		Vector3_t4282066566  L_14 = G_B6_0;
		Il2CppObject * L_15 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_14);
		NullCheck(G_B6_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B6_2, G_B6_1, L_15);
		goto IL_0142;
	}

IL_0089:
	{
		FsmVector3_t533912882 * L_16 = __this->get_vectorRotation_11();
		NullCheck(L_16);
		bool L_17 = NamedVariable_get_IsNone_m281035543(L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00be;
		}
	}
	{
		Hashtable_t1407064410 * L_18 = __this->get_hash_14();
		FsmGameObject_t1697147867 * L_19 = __this->get_transformRotation_10();
		NullCheck(L_19);
		GameObject_t3674682005 * L_20 = FsmGameObject_get_Value_m673294275(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_t1659122786 * L_21 = GameObject_get_transform_m1278640159(L_20, /*hidden argument*/NULL);
		NullCheck(L_18);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_18, _stringLiteral4254666622, L_21);
		goto IL_0142;
	}

IL_00be:
	{
		int32_t L_22 = __this->get_space_13();
		if (L_22)
		{
			goto IL_0108;
		}
	}
	{
		Hashtable_t1407064410 * L_23 = __this->get_hash_14();
		FsmGameObject_t1697147867 * L_24 = __this->get_transformRotation_10();
		NullCheck(L_24);
		GameObject_t3674682005 * L_25 = FsmGameObject_get_Value_m673294275(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_t1659122786 * L_26 = GameObject_get_transform_m1278640159(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		Vector3_t4282066566  L_27 = Transform_get_eulerAngles_m1058084741(L_26, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_28 = __this->get_vectorRotation_11();
		NullCheck(L_28);
		Vector3_t4282066566  L_29 = FsmVector3_get_Value_m2779135117(L_28, /*hidden argument*/NULL);
		Vector3_t4282066566  L_30 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_27, L_29, /*hidden argument*/NULL);
		Vector3_t4282066566  L_31 = L_30;
		Il2CppObject * L_32 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_31);
		NullCheck(L_23);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_23, _stringLiteral4254666622, L_32);
		goto IL_0142;
	}

IL_0108:
	{
		Hashtable_t1407064410 * L_33 = __this->get_hash_14();
		FsmGameObject_t1697147867 * L_34 = __this->get_transformRotation_10();
		NullCheck(L_34);
		GameObject_t3674682005 * L_35 = FsmGameObject_get_Value_m673294275(L_34, /*hidden argument*/NULL);
		NullCheck(L_35);
		Transform_t1659122786 * L_36 = GameObject_get_transform_m1278640159(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		Vector3_t4282066566  L_37 = Transform_get_localEulerAngles_m3489183428(L_36, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_38 = __this->get_vectorRotation_11();
		NullCheck(L_38);
		Vector3_t4282066566  L_39 = FsmVector3_get_Value_m2779135117(L_38, /*hidden argument*/NULL);
		Vector3_t4282066566  L_40 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_37, L_39, /*hidden argument*/NULL);
		Vector3_t4282066566  L_41 = L_40;
		Il2CppObject * L_42 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_41);
		NullCheck(L_33);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_33, _stringLiteral4254666622, L_42);
	}

IL_0142:
	{
		Hashtable_t1407064410 * L_43 = __this->get_hash_14();
		FsmFloat_t2134102846 * L_44 = __this->get_time_12();
		NullCheck(L_44);
		bool L_45 = NamedVariable_get_IsNone_m281035543(L_44, /*hidden argument*/NULL);
		G_B13_0 = _stringLiteral3560141;
		G_B13_1 = L_43;
		if (!L_45)
		{
			G_B14_0 = _stringLiteral3560141;
			G_B14_1 = L_43;
			goto IL_0167;
		}
	}
	{
		G_B15_0 = (1.0f);
		G_B15_1 = G_B13_0;
		G_B15_2 = G_B13_1;
		goto IL_0172;
	}

IL_0167:
	{
		FsmFloat_t2134102846 * L_46 = __this->get_time_12();
		NullCheck(L_46);
		float L_47 = FsmFloat_get_Value_m4137923823(L_46, /*hidden argument*/NULL);
		G_B15_0 = L_47;
		G_B15_1 = G_B14_0;
		G_B15_2 = G_B14_1;
	}

IL_0172:
	{
		float L_48 = G_B15_0;
		Il2CppObject * L_49 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_48);
		NullCheck(G_B15_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B15_2, G_B15_1, L_49);
		Hashtable_t1407064410 * L_50 = __this->get_hash_14();
		int32_t L_51 = __this->get_space_13();
		bool L_52 = ((bool)((((int32_t)L_51) == ((int32_t)1))? 1 : 0));
		Il2CppObject * L_53 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_52);
		NullCheck(L_50);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_50, _stringLiteral2094103681, L_53);
		iTweenRotateUpdate_DoiTween_m46305305(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateUpdate::OnExit()
extern "C"  void iTweenRotateUpdate_OnExit_m2246561449 (iTweenRotateUpdate_t1671849710 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateUpdate::OnUpdate()
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4254666622;
extern const uint32_t iTweenRotateUpdate_OnUpdate_m2023699732_MetadataUsageId;
extern "C"  void iTweenRotateUpdate_OnUpdate_m2023699732 (iTweenRotateUpdate_t1671849710 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenRotateUpdate_OnUpdate_m2023699732_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	Hashtable_t1407064410 * G_B3_1 = NULL;
	String_t* G_B2_0 = NULL;
	Hashtable_t1407064410 * G_B2_1 = NULL;
	Vector3_t4282066566  G_B4_0;
	memset(&G_B4_0, 0, sizeof(G_B4_0));
	String_t* G_B4_1 = NULL;
	Hashtable_t1407064410 * G_B4_2 = NULL;
	{
		Hashtable_t1407064410 * L_0 = __this->get_hash_14();
		NullCheck(L_0);
		VirtActionInvoker1< Il2CppObject * >::Invoke(37 /* System.Void System.Collections.Hashtable::Remove(System.Object) */, L_0, _stringLiteral4254666622);
		FsmGameObject_t1697147867 * L_1 = __this->get_transformRotation_10();
		NullCheck(L_1);
		bool L_2 = NamedVariable_get_IsNone_m281035543(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_005f;
		}
	}
	{
		Hashtable_t1407064410 * L_3 = __this->get_hash_14();
		FsmVector3_t533912882 * L_4 = __this->get_vectorRotation_11();
		NullCheck(L_4);
		bool L_5 = NamedVariable_get_IsNone_m281035543(L_4, /*hidden argument*/NULL);
		G_B2_0 = _stringLiteral4254666622;
		G_B2_1 = L_3;
		if (!L_5)
		{
			G_B3_0 = _stringLiteral4254666622;
			G_B3_1 = L_3;
			goto IL_0045;
		}
	}
	{
		Vector3_t4282066566  L_6 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B4_0 = L_6;
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		goto IL_0050;
	}

IL_0045:
	{
		FsmVector3_t533912882 * L_7 = __this->get_vectorRotation_11();
		NullCheck(L_7);
		Vector3_t4282066566  L_8 = FsmVector3_get_Value_m2779135117(L_7, /*hidden argument*/NULL);
		G_B4_0 = L_8;
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
	}

IL_0050:
	{
		Vector3_t4282066566  L_9 = G_B4_0;
		Il2CppObject * L_10 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_9);
		NullCheck(G_B4_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B4_2, G_B4_1, L_10);
		goto IL_0118;
	}

IL_005f:
	{
		FsmVector3_t533912882 * L_11 = __this->get_vectorRotation_11();
		NullCheck(L_11);
		bool L_12 = NamedVariable_get_IsNone_m281035543(L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0094;
		}
	}
	{
		Hashtable_t1407064410 * L_13 = __this->get_hash_14();
		FsmGameObject_t1697147867 * L_14 = __this->get_transformRotation_10();
		NullCheck(L_14);
		GameObject_t3674682005 * L_15 = FsmGameObject_get_Value_m673294275(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_t1659122786 * L_16 = GameObject_get_transform_m1278640159(L_15, /*hidden argument*/NULL);
		NullCheck(L_13);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_13, _stringLiteral4254666622, L_16);
		goto IL_0118;
	}

IL_0094:
	{
		int32_t L_17 = __this->get_space_13();
		if (L_17)
		{
			goto IL_00de;
		}
	}
	{
		Hashtable_t1407064410 * L_18 = __this->get_hash_14();
		FsmGameObject_t1697147867 * L_19 = __this->get_transformRotation_10();
		NullCheck(L_19);
		GameObject_t3674682005 * L_20 = FsmGameObject_get_Value_m673294275(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_t1659122786 * L_21 = GameObject_get_transform_m1278640159(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		Vector3_t4282066566  L_22 = Transform_get_eulerAngles_m1058084741(L_21, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_23 = __this->get_vectorRotation_11();
		NullCheck(L_23);
		Vector3_t4282066566  L_24 = FsmVector3_get_Value_m2779135117(L_23, /*hidden argument*/NULL);
		Vector3_t4282066566  L_25 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_22, L_24, /*hidden argument*/NULL);
		Vector3_t4282066566  L_26 = L_25;
		Il2CppObject * L_27 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_18);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_18, _stringLiteral4254666622, L_27);
		goto IL_0118;
	}

IL_00de:
	{
		Hashtable_t1407064410 * L_28 = __this->get_hash_14();
		FsmGameObject_t1697147867 * L_29 = __this->get_transformRotation_10();
		NullCheck(L_29);
		GameObject_t3674682005 * L_30 = FsmGameObject_get_Value_m673294275(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		Transform_t1659122786 * L_31 = GameObject_get_transform_m1278640159(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		Vector3_t4282066566  L_32 = Transform_get_localEulerAngles_m3489183428(L_31, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_33 = __this->get_vectorRotation_11();
		NullCheck(L_33);
		Vector3_t4282066566  L_34 = FsmVector3_get_Value_m2779135117(L_33, /*hidden argument*/NULL);
		Vector3_t4282066566  L_35 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_32, L_34, /*hidden argument*/NULL);
		Vector3_t4282066566  L_36 = L_35;
		Il2CppObject * L_37 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_36);
		NullCheck(L_28);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_28, _stringLiteral4254666622, L_37);
	}

IL_0118:
	{
		iTweenRotateUpdate_DoiTween_m46305305(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateUpdate::DoiTween()
extern Il2CppClass* iTween_t3087282050_il2cpp_TypeInfo_var;
extern const uint32_t iTweenRotateUpdate_DoiTween_m46305305_MetadataUsageId;
extern "C"  void iTweenRotateUpdate_DoiTween_m46305305 (iTweenRotateUpdate_t1671849710 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenRotateUpdate_DoiTween_m46305305_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_go_15();
		Hashtable_t1407064410 * L_1 = __this->get_hash_14();
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		iTween_RotateUpdate_m648932759(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleAdd::.ctor()
extern "C"  void iTweenScaleAdd__ctor_m2414050853 (iTweenScaleAdd_t289621857 * __this, const MethodInfo* method)
{
	{
		__this->set_easeType_23(((int32_t)21));
		iTweenFsmAction__ctor_m2960815636(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleAdd::Reset()
extern Il2CppClass* FsmString_t952858651_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmFloat_t2134102846_il2cpp_TypeInfo_var;
extern const uint32_t iTweenScaleAdd_Reset_m60483794_MetadataUsageId;
extern "C"  void iTweenScaleAdd_Reset_m60483794 (iTweenScaleAdd_t289621857 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenScaleAdd_Reset_m60483794_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmString_t952858651 * V_0 = NULL;
	FsmVector3_t533912882 * V_1 = NULL;
	FsmFloat_t2134102846 * V_2 = NULL;
	{
		iTweenFsmAction_Reset_m607248577(__this, /*hidden argument*/NULL);
		FsmString_t952858651 * L_0 = (FsmString_t952858651 *)il2cpp_codegen_object_new(FsmString_t952858651_il2cpp_TypeInfo_var);
		FsmString__ctor_m2679108260(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmString_t952858651 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmString_t952858651 * L_2 = V_0;
		__this->set_id_18(L_2);
		FsmFloat_t2134102846 * L_3 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_time_20(L_3);
		FsmFloat_t2134102846 * L_4 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_delay_21(L_4);
		__this->set_loopType_24(0);
		FsmVector3_t533912882 * L_5 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_5, /*hidden argument*/NULL);
		V_1 = L_5;
		FsmVector3_t533912882 * L_6 = V_1;
		NullCheck(L_6);
		NamedVariable_set_UseVariable_m4266138971(L_6, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_7 = V_1;
		__this->set_vector_19(L_7);
		FsmFloat_t2134102846 * L_8 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_8, /*hidden argument*/NULL);
		V_2 = L_8;
		FsmFloat_t2134102846 * L_9 = V_2;
		NullCheck(L_9);
		NamedVariable_set_UseVariable_m4266138971(L_9, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_10 = V_2;
		__this->set_speed_22(L_10);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleAdd::OnEnter()
extern "C"  void iTweenScaleAdd_OnEnter_m12258492 (iTweenScaleAdd_t289621857 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnEnteriTween_m3024539459(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_loopType_24();
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		iTweenFsmAction_IsLoop_m2539277653(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_001e:
	{
		iTweenScaleAdd_DoiTween_m1831145484(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleAdd::OnExit()
extern "C"  void iTweenScaleAdd_OnExit_m3334451036 (iTweenScaleAdd_t289621857 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnExitiTween_m1981625761(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleAdd::DoiTween()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* EaseType_t2734598229_il2cpp_TypeInfo_var;
extern Il2CppClass* LoopType_t1485160459_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* iTween_t3087282050_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral109250890;
extern Il2CppCodeGenString* _stringLiteral2881114200;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral3560141;
extern Il2CppCodeGenString* _stringLiteral109641799;
extern Il2CppCodeGenString* _stringLiteral95467907;
extern Il2CppCodeGenString* _stringLiteral3507899432;
extern Il2CppCodeGenString* _stringLiteral2258461662;
extern Il2CppCodeGenString* _stringLiteral2105864216;
extern Il2CppCodeGenString* _stringLiteral338257242;
extern Il2CppCodeGenString* _stringLiteral3696326558;
extern Il2CppCodeGenString* _stringLiteral2987624931;
extern Il2CppCodeGenString* _stringLiteral647687137;
extern Il2CppCodeGenString* _stringLiteral2052738345;
extern Il2CppCodeGenString* _stringLiteral1067506955;
extern const uint32_t iTweenScaleAdd_DoiTween_m1831145484_MetadataUsageId;
extern "C"  void iTweenScaleAdd_DoiTween_m1831145484 (iTweenScaleAdd_t289621857 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenScaleAdd_DoiTween_m1831145484_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B7_0 = 0;
	ObjectU5BU5D_t1108656482* G_B7_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B7_2 = NULL;
	GameObject_t3674682005 * G_B7_3 = NULL;
	int32_t G_B6_0 = 0;
	ObjectU5BU5D_t1108656482* G_B6_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B6_2 = NULL;
	GameObject_t3674682005 * G_B6_3 = NULL;
	String_t* G_B8_0 = NULL;
	int32_t G_B8_1 = 0;
	ObjectU5BU5D_t1108656482* G_B8_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B8_3 = NULL;
	GameObject_t3674682005 * G_B8_4 = NULL;
	int32_t G_B10_0 = 0;
	ObjectU5BU5D_t1108656482* G_B10_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B10_2 = NULL;
	GameObject_t3674682005 * G_B10_3 = NULL;
	int32_t G_B9_0 = 0;
	ObjectU5BU5D_t1108656482* G_B9_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B9_2 = NULL;
	GameObject_t3674682005 * G_B9_3 = NULL;
	String_t* G_B11_0 = NULL;
	int32_t G_B11_1 = 0;
	ObjectU5BU5D_t1108656482* G_B11_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B11_3 = NULL;
	GameObject_t3674682005 * G_B11_4 = NULL;
	int32_t G_B16_0 = 0;
	ObjectU5BU5D_t1108656482* G_B16_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B16_2 = NULL;
	GameObject_t3674682005 * G_B16_3 = NULL;
	int32_t G_B12_0 = 0;
	ObjectU5BU5D_t1108656482* G_B12_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B12_2 = NULL;
	GameObject_t3674682005 * G_B12_3 = NULL;
	int32_t G_B14_0 = 0;
	ObjectU5BU5D_t1108656482* G_B14_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B14_2 = NULL;
	GameObject_t3674682005 * G_B14_3 = NULL;
	int32_t G_B13_0 = 0;
	ObjectU5BU5D_t1108656482* G_B13_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B13_2 = NULL;
	GameObject_t3674682005 * G_B13_3 = NULL;
	float G_B15_0 = 0.0f;
	int32_t G_B15_1 = 0;
	ObjectU5BU5D_t1108656482* G_B15_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B15_3 = NULL;
	GameObject_t3674682005 * G_B15_4 = NULL;
	float G_B17_0 = 0.0f;
	int32_t G_B17_1 = 0;
	ObjectU5BU5D_t1108656482* G_B17_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B17_3 = NULL;
	GameObject_t3674682005 * G_B17_4 = NULL;
	int32_t G_B19_0 = 0;
	ObjectU5BU5D_t1108656482* G_B19_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B19_2 = NULL;
	GameObject_t3674682005 * G_B19_3 = NULL;
	int32_t G_B18_0 = 0;
	ObjectU5BU5D_t1108656482* G_B18_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B18_2 = NULL;
	GameObject_t3674682005 * G_B18_3 = NULL;
	float G_B20_0 = 0.0f;
	int32_t G_B20_1 = 0;
	ObjectU5BU5D_t1108656482* G_B20_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B20_3 = NULL;
	GameObject_t3674682005 * G_B20_4 = NULL;
	int32_t G_B22_0 = 0;
	ObjectU5BU5D_t1108656482* G_B22_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B22_2 = NULL;
	GameObject_t3674682005 * G_B22_3 = NULL;
	int32_t G_B21_0 = 0;
	ObjectU5BU5D_t1108656482* G_B21_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B21_2 = NULL;
	GameObject_t3674682005 * G_B21_3 = NULL;
	int32_t G_B23_0 = 0;
	int32_t G_B23_1 = 0;
	ObjectU5BU5D_t1108656482* G_B23_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B23_3 = NULL;
	GameObject_t3674682005 * G_B23_4 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_17();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		Vector3_t4282066566  L_5 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_5;
		FsmVector3_t533912882 * L_6 = __this->get_vector_19();
		NullCheck(L_6);
		bool L_7 = NamedVariable_get_IsNone_m281035543(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003a;
		}
	}
	{
		goto IL_0046;
	}

IL_003a:
	{
		FsmVector3_t533912882 * L_8 = __this->get_vector_19();
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = FsmVector3_get_Value_m2779135117(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
	}

IL_0046:
	{
		((iTweenFsmAction_t410382178 *)__this)->set_itweenType_15(_stringLiteral109250890);
		GameObject_t3674682005 * L_10 = V_0;
		ObjectU5BU5D_t1108656482* L_11 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)22)));
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		ArrayElementTypeCheck (L_11, _stringLiteral2881114200);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2881114200);
		ObjectU5BU5D_t1108656482* L_12 = L_11;
		Vector3_t4282066566  L_13 = V_1;
		Vector3_t4282066566  L_14 = L_13;
		Il2CppObject * L_15 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 1);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_15);
		ObjectU5BU5D_t1108656482* L_16 = L_12;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 2);
		ArrayElementTypeCheck (L_16, _stringLiteral3373707);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3373707);
		ObjectU5BU5D_t1108656482* L_17 = L_16;
		FsmString_t952858651 * L_18 = __this->get_id_18();
		NullCheck(L_18);
		bool L_19 = NamedVariable_get_IsNone_m281035543(L_18, /*hidden argument*/NULL);
		G_B6_0 = 3;
		G_B6_1 = L_17;
		G_B6_2 = L_17;
		G_B6_3 = L_10;
		if (!L_19)
		{
			G_B7_0 = 3;
			G_B7_1 = L_17;
			G_B7_2 = L_17;
			G_B7_3 = L_10;
			goto IL_008e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B8_0 = L_20;
		G_B8_1 = G_B6_0;
		G_B8_2 = G_B6_1;
		G_B8_3 = G_B6_2;
		G_B8_4 = G_B6_3;
		goto IL_0099;
	}

IL_008e:
	{
		FsmString_t952858651 * L_21 = __this->get_id_18();
		NullCheck(L_21);
		String_t* L_22 = FsmString_get_Value_m872383149(L_21, /*hidden argument*/NULL);
		G_B8_0 = L_22;
		G_B8_1 = G_B7_0;
		G_B8_2 = G_B7_1;
		G_B8_3 = G_B7_2;
		G_B8_4 = G_B7_3;
	}

IL_0099:
	{
		NullCheck(G_B8_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B8_2, G_B8_1);
		ArrayElementTypeCheck (G_B8_2, G_B8_0);
		(G_B8_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B8_1), (Il2CppObject *)G_B8_0);
		ObjectU5BU5D_t1108656482* L_23 = G_B8_3;
		FsmFloat_t2134102846 * L_24 = __this->get_speed_22();
		NullCheck(L_24);
		bool L_25 = NamedVariable_get_IsNone_m281035543(L_24, /*hidden argument*/NULL);
		G_B9_0 = 4;
		G_B9_1 = L_23;
		G_B9_2 = L_23;
		G_B9_3 = G_B8_4;
		if (!L_25)
		{
			G_B10_0 = 4;
			G_B10_1 = L_23;
			G_B10_2 = L_23;
			G_B10_3 = G_B8_4;
			goto IL_00b6;
		}
	}
	{
		G_B11_0 = _stringLiteral3560141;
		G_B11_1 = G_B9_0;
		G_B11_2 = G_B9_1;
		G_B11_3 = G_B9_2;
		G_B11_4 = G_B9_3;
		goto IL_00bb;
	}

IL_00b6:
	{
		G_B11_0 = _stringLiteral109641799;
		G_B11_1 = G_B10_0;
		G_B11_2 = G_B10_1;
		G_B11_3 = G_B10_2;
		G_B11_4 = G_B10_3;
	}

IL_00bb:
	{
		NullCheck(G_B11_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B11_2, G_B11_1);
		ArrayElementTypeCheck (G_B11_2, G_B11_0);
		(G_B11_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B11_1), (Il2CppObject *)G_B11_0);
		ObjectU5BU5D_t1108656482* L_26 = G_B11_3;
		FsmFloat_t2134102846 * L_27 = __this->get_speed_22();
		NullCheck(L_27);
		bool L_28 = NamedVariable_get_IsNone_m281035543(L_27, /*hidden argument*/NULL);
		G_B12_0 = 5;
		G_B12_1 = L_26;
		G_B12_2 = L_26;
		G_B12_3 = G_B11_4;
		if (!L_28)
		{
			G_B16_0 = 5;
			G_B16_1 = L_26;
			G_B16_2 = L_26;
			G_B16_3 = G_B11_4;
			goto IL_00f8;
		}
	}
	{
		FsmFloat_t2134102846 * L_29 = __this->get_time_20();
		NullCheck(L_29);
		bool L_30 = NamedVariable_get_IsNone_m281035543(L_29, /*hidden argument*/NULL);
		G_B13_0 = G_B12_0;
		G_B13_1 = G_B12_1;
		G_B13_2 = G_B12_2;
		G_B13_3 = G_B12_3;
		if (!L_30)
		{
			G_B14_0 = G_B12_0;
			G_B14_1 = G_B12_1;
			G_B14_2 = G_B12_2;
			G_B14_3 = G_B12_3;
			goto IL_00e8;
		}
	}
	{
		G_B15_0 = (1.0f);
		G_B15_1 = G_B13_0;
		G_B15_2 = G_B13_1;
		G_B15_3 = G_B13_2;
		G_B15_4 = G_B13_3;
		goto IL_00f3;
	}

IL_00e8:
	{
		FsmFloat_t2134102846 * L_31 = __this->get_time_20();
		NullCheck(L_31);
		float L_32 = FsmFloat_get_Value_m4137923823(L_31, /*hidden argument*/NULL);
		G_B15_0 = L_32;
		G_B15_1 = G_B14_0;
		G_B15_2 = G_B14_1;
		G_B15_3 = G_B14_2;
		G_B15_4 = G_B14_3;
	}

IL_00f3:
	{
		G_B17_0 = G_B15_0;
		G_B17_1 = G_B15_1;
		G_B17_2 = G_B15_2;
		G_B17_3 = G_B15_3;
		G_B17_4 = G_B15_4;
		goto IL_0103;
	}

IL_00f8:
	{
		FsmFloat_t2134102846 * L_33 = __this->get_speed_22();
		NullCheck(L_33);
		float L_34 = FsmFloat_get_Value_m4137923823(L_33, /*hidden argument*/NULL);
		G_B17_0 = L_34;
		G_B17_1 = G_B16_0;
		G_B17_2 = G_B16_1;
		G_B17_3 = G_B16_2;
		G_B17_4 = G_B16_3;
	}

IL_0103:
	{
		float L_35 = G_B17_0;
		Il2CppObject * L_36 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_35);
		NullCheck(G_B17_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B17_2, G_B17_1);
		ArrayElementTypeCheck (G_B17_2, L_36);
		(G_B17_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B17_1), (Il2CppObject *)L_36);
		ObjectU5BU5D_t1108656482* L_37 = G_B17_3;
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, 6);
		ArrayElementTypeCheck (L_37, _stringLiteral95467907);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral95467907);
		ObjectU5BU5D_t1108656482* L_38 = L_37;
		FsmFloat_t2134102846 * L_39 = __this->get_delay_21();
		NullCheck(L_39);
		bool L_40 = NamedVariable_get_IsNone_m281035543(L_39, /*hidden argument*/NULL);
		G_B18_0 = 7;
		G_B18_1 = L_38;
		G_B18_2 = L_38;
		G_B18_3 = G_B17_4;
		if (!L_40)
		{
			G_B19_0 = 7;
			G_B19_1 = L_38;
			G_B19_2 = L_38;
			G_B19_3 = G_B17_4;
			goto IL_012d;
		}
	}
	{
		G_B20_0 = (0.0f);
		G_B20_1 = G_B18_0;
		G_B20_2 = G_B18_1;
		G_B20_3 = G_B18_2;
		G_B20_4 = G_B18_3;
		goto IL_0138;
	}

IL_012d:
	{
		FsmFloat_t2134102846 * L_41 = __this->get_delay_21();
		NullCheck(L_41);
		float L_42 = FsmFloat_get_Value_m4137923823(L_41, /*hidden argument*/NULL);
		G_B20_0 = L_42;
		G_B20_1 = G_B19_0;
		G_B20_2 = G_B19_1;
		G_B20_3 = G_B19_2;
		G_B20_4 = G_B19_3;
	}

IL_0138:
	{
		float L_43 = G_B20_0;
		Il2CppObject * L_44 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_43);
		NullCheck(G_B20_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B20_2, G_B20_1);
		ArrayElementTypeCheck (G_B20_2, L_44);
		(G_B20_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B20_1), (Il2CppObject *)L_44);
		ObjectU5BU5D_t1108656482* L_45 = G_B20_3;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, 8);
		ArrayElementTypeCheck (L_45, _stringLiteral3507899432);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral3507899432);
		ObjectU5BU5D_t1108656482* L_46 = L_45;
		int32_t L_47 = __this->get_easeType_23();
		int32_t L_48 = L_47;
		Il2CppObject * L_49 = Box(EaseType_t2734598229_il2cpp_TypeInfo_var, &L_48);
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, ((int32_t)9));
		ArrayElementTypeCheck (L_46, L_49);
		(L_46)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)L_49);
		ObjectU5BU5D_t1108656482* L_50 = L_46;
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, ((int32_t)10));
		ArrayElementTypeCheck (L_50, _stringLiteral2258461662);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Il2CppObject *)_stringLiteral2258461662);
		ObjectU5BU5D_t1108656482* L_51 = L_50;
		int32_t L_52 = __this->get_loopType_24();
		int32_t L_53 = L_52;
		Il2CppObject * L_54 = Box(LoopType_t1485160459_il2cpp_TypeInfo_var, &L_53);
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, ((int32_t)11));
		ArrayElementTypeCheck (L_51, L_54);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (Il2CppObject *)L_54);
		ObjectU5BU5D_t1108656482* L_55 = L_51;
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, ((int32_t)12));
		ArrayElementTypeCheck (L_55, _stringLiteral2105864216);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (Il2CppObject *)_stringLiteral2105864216);
		ObjectU5BU5D_t1108656482* L_56 = L_55;
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, ((int32_t)13));
		ArrayElementTypeCheck (L_56, _stringLiteral338257242);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (Il2CppObject *)_stringLiteral338257242);
		ObjectU5BU5D_t1108656482* L_57 = L_56;
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, ((int32_t)14));
		ArrayElementTypeCheck (L_57, _stringLiteral3696326558);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (Il2CppObject *)_stringLiteral3696326558);
		ObjectU5BU5D_t1108656482* L_58 = L_57;
		int32_t L_59 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_60 = L_59;
		Il2CppObject * L_61 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_60);
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, ((int32_t)15));
		ArrayElementTypeCheck (L_58, L_61);
		(L_58)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (Il2CppObject *)L_61);
		ObjectU5BU5D_t1108656482* L_62 = L_58;
		NullCheck(L_62);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_62, ((int32_t)16));
		ArrayElementTypeCheck (L_62, _stringLiteral2987624931);
		(L_62)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (Il2CppObject *)_stringLiteral2987624931);
		ObjectU5BU5D_t1108656482* L_63 = L_62;
		NullCheck(L_63);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_63, ((int32_t)17));
		ArrayElementTypeCheck (L_63, _stringLiteral647687137);
		(L_63)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (Il2CppObject *)_stringLiteral647687137);
		ObjectU5BU5D_t1108656482* L_64 = L_63;
		NullCheck(L_64);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_64, ((int32_t)18));
		ArrayElementTypeCheck (L_64, _stringLiteral2052738345);
		(L_64)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (Il2CppObject *)_stringLiteral2052738345);
		ObjectU5BU5D_t1108656482* L_65 = L_64;
		int32_t L_66 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_67 = L_66;
		Il2CppObject * L_68 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_67);
		NullCheck(L_65);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_65, ((int32_t)19));
		ArrayElementTypeCheck (L_65, L_68);
		(L_65)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)19)), (Il2CppObject *)L_68);
		ObjectU5BU5D_t1108656482* L_69 = L_65;
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, ((int32_t)20));
		ArrayElementTypeCheck (L_69, _stringLiteral1067506955);
		(L_69)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)20)), (Il2CppObject *)_stringLiteral1067506955);
		ObjectU5BU5D_t1108656482* L_70 = L_69;
		FsmBool_t1075959796 * L_71 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_71);
		bool L_72 = NamedVariable_get_IsNone_m281035543(L_71, /*hidden argument*/NULL);
		G_B21_0 = ((int32_t)21);
		G_B21_1 = L_70;
		G_B21_2 = L_70;
		G_B21_3 = G_B20_4;
		if (!L_72)
		{
			G_B22_0 = ((int32_t)21);
			G_B22_1 = L_70;
			G_B22_2 = L_70;
			G_B22_3 = G_B20_4;
			goto IL_01e3;
		}
	}
	{
		G_B23_0 = 0;
		G_B23_1 = G_B21_0;
		G_B23_2 = G_B21_1;
		G_B23_3 = G_B21_2;
		G_B23_4 = G_B21_3;
		goto IL_01ee;
	}

IL_01e3:
	{
		FsmBool_t1075959796 * L_73 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_73);
		bool L_74 = FsmBool_get_Value_m3101329097(L_73, /*hidden argument*/NULL);
		G_B23_0 = ((int32_t)(L_74));
		G_B23_1 = G_B22_0;
		G_B23_2 = G_B22_1;
		G_B23_3 = G_B22_2;
		G_B23_4 = G_B22_3;
	}

IL_01ee:
	{
		bool L_75 = ((bool)G_B23_0);
		Il2CppObject * L_76 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_75);
		NullCheck(G_B23_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B23_2, G_B23_1);
		ArrayElementTypeCheck (G_B23_2, L_76);
		(G_B23_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B23_1), (Il2CppObject *)L_76);
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		Hashtable_t1407064410 * L_77 = iTween_Hash_m2099495140(NULL /*static, unused*/, G_B23_3, /*hidden argument*/NULL);
		iTween_ScaleAdd_m4188332420(NULL /*static, unused*/, G_B23_4, L_77, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleBy::.ctor()
extern "C"  void iTweenScaleBy__ctor_m2067461257 (iTweenScaleBy_t1692844877 * __this, const MethodInfo* method)
{
	{
		__this->set_easeType_23(((int32_t)21));
		iTweenFsmAction__ctor_m2960815636(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleBy::Reset()
extern Il2CppClass* FsmString_t952858651_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmFloat_t2134102846_il2cpp_TypeInfo_var;
extern const uint32_t iTweenScaleBy_Reset_m4008861494_MetadataUsageId;
extern "C"  void iTweenScaleBy_Reset_m4008861494 (iTweenScaleBy_t1692844877 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenScaleBy_Reset_m4008861494_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmString_t952858651 * V_0 = NULL;
	FsmVector3_t533912882 * V_1 = NULL;
	FsmFloat_t2134102846 * V_2 = NULL;
	{
		iTweenFsmAction_Reset_m607248577(__this, /*hidden argument*/NULL);
		FsmString_t952858651 * L_0 = (FsmString_t952858651 *)il2cpp_codegen_object_new(FsmString_t952858651_il2cpp_TypeInfo_var);
		FsmString__ctor_m2679108260(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmString_t952858651 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmString_t952858651 * L_2 = V_0;
		__this->set_id_18(L_2);
		FsmFloat_t2134102846 * L_3 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_time_20(L_3);
		FsmFloat_t2134102846 * L_4 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_delay_21(L_4);
		__this->set_loopType_24(0);
		FsmVector3_t533912882 * L_5 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_5, /*hidden argument*/NULL);
		V_1 = L_5;
		FsmVector3_t533912882 * L_6 = V_1;
		NullCheck(L_6);
		NamedVariable_set_UseVariable_m4266138971(L_6, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_7 = V_1;
		__this->set_vector_19(L_7);
		FsmFloat_t2134102846 * L_8 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_8, /*hidden argument*/NULL);
		V_2 = L_8;
		FsmFloat_t2134102846 * L_9 = V_2;
		NullCheck(L_9);
		NamedVariable_set_UseVariable_m4266138971(L_9, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_10 = V_2;
		__this->set_speed_22(L_10);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleBy::OnEnter()
extern "C"  void iTweenScaleBy_OnEnter_m1947105824 (iTweenScaleBy_t1692844877 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnEnteriTween_m3024539459(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_loopType_24();
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		iTweenFsmAction_IsLoop_m2539277653(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_001e:
	{
		iTweenScaleBy_DoiTween_m1681870632(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleBy::OnExit()
extern "C"  void iTweenScaleBy_OnExit_m1180108152 (iTweenScaleBy_t1692844877 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnExitiTween_m1981625761(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleBy::DoiTween()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* EaseType_t2734598229_il2cpp_TypeInfo_var;
extern Il2CppClass* LoopType_t1485160459_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* iTween_t3087282050_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral109250890;
extern Il2CppCodeGenString* _stringLiteral2881114200;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral3560141;
extern Il2CppCodeGenString* _stringLiteral109641799;
extern Il2CppCodeGenString* _stringLiteral95467907;
extern Il2CppCodeGenString* _stringLiteral3507899432;
extern Il2CppCodeGenString* _stringLiteral2258461662;
extern Il2CppCodeGenString* _stringLiteral2105864216;
extern Il2CppCodeGenString* _stringLiteral338257242;
extern Il2CppCodeGenString* _stringLiteral3696326558;
extern Il2CppCodeGenString* _stringLiteral2987624931;
extern Il2CppCodeGenString* _stringLiteral647687137;
extern Il2CppCodeGenString* _stringLiteral2052738345;
extern Il2CppCodeGenString* _stringLiteral1067506955;
extern const uint32_t iTweenScaleBy_DoiTween_m1681870632_MetadataUsageId;
extern "C"  void iTweenScaleBy_DoiTween_m1681870632 (iTweenScaleBy_t1692844877 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenScaleBy_DoiTween_m1681870632_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B7_0 = 0;
	ObjectU5BU5D_t1108656482* G_B7_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B7_2 = NULL;
	GameObject_t3674682005 * G_B7_3 = NULL;
	int32_t G_B6_0 = 0;
	ObjectU5BU5D_t1108656482* G_B6_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B6_2 = NULL;
	GameObject_t3674682005 * G_B6_3 = NULL;
	String_t* G_B8_0 = NULL;
	int32_t G_B8_1 = 0;
	ObjectU5BU5D_t1108656482* G_B8_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B8_3 = NULL;
	GameObject_t3674682005 * G_B8_4 = NULL;
	int32_t G_B10_0 = 0;
	ObjectU5BU5D_t1108656482* G_B10_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B10_2 = NULL;
	GameObject_t3674682005 * G_B10_3 = NULL;
	int32_t G_B9_0 = 0;
	ObjectU5BU5D_t1108656482* G_B9_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B9_2 = NULL;
	GameObject_t3674682005 * G_B9_3 = NULL;
	String_t* G_B11_0 = NULL;
	int32_t G_B11_1 = 0;
	ObjectU5BU5D_t1108656482* G_B11_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B11_3 = NULL;
	GameObject_t3674682005 * G_B11_4 = NULL;
	int32_t G_B16_0 = 0;
	ObjectU5BU5D_t1108656482* G_B16_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B16_2 = NULL;
	GameObject_t3674682005 * G_B16_3 = NULL;
	int32_t G_B12_0 = 0;
	ObjectU5BU5D_t1108656482* G_B12_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B12_2 = NULL;
	GameObject_t3674682005 * G_B12_3 = NULL;
	int32_t G_B14_0 = 0;
	ObjectU5BU5D_t1108656482* G_B14_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B14_2 = NULL;
	GameObject_t3674682005 * G_B14_3 = NULL;
	int32_t G_B13_0 = 0;
	ObjectU5BU5D_t1108656482* G_B13_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B13_2 = NULL;
	GameObject_t3674682005 * G_B13_3 = NULL;
	float G_B15_0 = 0.0f;
	int32_t G_B15_1 = 0;
	ObjectU5BU5D_t1108656482* G_B15_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B15_3 = NULL;
	GameObject_t3674682005 * G_B15_4 = NULL;
	float G_B17_0 = 0.0f;
	int32_t G_B17_1 = 0;
	ObjectU5BU5D_t1108656482* G_B17_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B17_3 = NULL;
	GameObject_t3674682005 * G_B17_4 = NULL;
	int32_t G_B19_0 = 0;
	ObjectU5BU5D_t1108656482* G_B19_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B19_2 = NULL;
	GameObject_t3674682005 * G_B19_3 = NULL;
	int32_t G_B18_0 = 0;
	ObjectU5BU5D_t1108656482* G_B18_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B18_2 = NULL;
	GameObject_t3674682005 * G_B18_3 = NULL;
	float G_B20_0 = 0.0f;
	int32_t G_B20_1 = 0;
	ObjectU5BU5D_t1108656482* G_B20_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B20_3 = NULL;
	GameObject_t3674682005 * G_B20_4 = NULL;
	int32_t G_B22_0 = 0;
	ObjectU5BU5D_t1108656482* G_B22_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B22_2 = NULL;
	GameObject_t3674682005 * G_B22_3 = NULL;
	int32_t G_B21_0 = 0;
	ObjectU5BU5D_t1108656482* G_B21_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B21_2 = NULL;
	GameObject_t3674682005 * G_B21_3 = NULL;
	int32_t G_B23_0 = 0;
	int32_t G_B23_1 = 0;
	ObjectU5BU5D_t1108656482* G_B23_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B23_3 = NULL;
	GameObject_t3674682005 * G_B23_4 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_17();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		Vector3_t4282066566  L_5 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_5;
		FsmVector3_t533912882 * L_6 = __this->get_vector_19();
		NullCheck(L_6);
		bool L_7 = NamedVariable_get_IsNone_m281035543(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003a;
		}
	}
	{
		goto IL_0046;
	}

IL_003a:
	{
		FsmVector3_t533912882 * L_8 = __this->get_vector_19();
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = FsmVector3_get_Value_m2779135117(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
	}

IL_0046:
	{
		((iTweenFsmAction_t410382178 *)__this)->set_itweenType_15(_stringLiteral109250890);
		GameObject_t3674682005 * L_10 = V_0;
		ObjectU5BU5D_t1108656482* L_11 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)22)));
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		ArrayElementTypeCheck (L_11, _stringLiteral2881114200);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2881114200);
		ObjectU5BU5D_t1108656482* L_12 = L_11;
		Vector3_t4282066566  L_13 = V_1;
		Vector3_t4282066566  L_14 = L_13;
		Il2CppObject * L_15 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 1);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_15);
		ObjectU5BU5D_t1108656482* L_16 = L_12;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 2);
		ArrayElementTypeCheck (L_16, _stringLiteral3373707);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3373707);
		ObjectU5BU5D_t1108656482* L_17 = L_16;
		FsmString_t952858651 * L_18 = __this->get_id_18();
		NullCheck(L_18);
		bool L_19 = NamedVariable_get_IsNone_m281035543(L_18, /*hidden argument*/NULL);
		G_B6_0 = 3;
		G_B6_1 = L_17;
		G_B6_2 = L_17;
		G_B6_3 = L_10;
		if (!L_19)
		{
			G_B7_0 = 3;
			G_B7_1 = L_17;
			G_B7_2 = L_17;
			G_B7_3 = L_10;
			goto IL_008e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B8_0 = L_20;
		G_B8_1 = G_B6_0;
		G_B8_2 = G_B6_1;
		G_B8_3 = G_B6_2;
		G_B8_4 = G_B6_3;
		goto IL_0099;
	}

IL_008e:
	{
		FsmString_t952858651 * L_21 = __this->get_id_18();
		NullCheck(L_21);
		String_t* L_22 = FsmString_get_Value_m872383149(L_21, /*hidden argument*/NULL);
		G_B8_0 = L_22;
		G_B8_1 = G_B7_0;
		G_B8_2 = G_B7_1;
		G_B8_3 = G_B7_2;
		G_B8_4 = G_B7_3;
	}

IL_0099:
	{
		NullCheck(G_B8_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B8_2, G_B8_1);
		ArrayElementTypeCheck (G_B8_2, G_B8_0);
		(G_B8_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B8_1), (Il2CppObject *)G_B8_0);
		ObjectU5BU5D_t1108656482* L_23 = G_B8_3;
		FsmFloat_t2134102846 * L_24 = __this->get_speed_22();
		NullCheck(L_24);
		bool L_25 = NamedVariable_get_IsNone_m281035543(L_24, /*hidden argument*/NULL);
		G_B9_0 = 4;
		G_B9_1 = L_23;
		G_B9_2 = L_23;
		G_B9_3 = G_B8_4;
		if (!L_25)
		{
			G_B10_0 = 4;
			G_B10_1 = L_23;
			G_B10_2 = L_23;
			G_B10_3 = G_B8_4;
			goto IL_00b6;
		}
	}
	{
		G_B11_0 = _stringLiteral3560141;
		G_B11_1 = G_B9_0;
		G_B11_2 = G_B9_1;
		G_B11_3 = G_B9_2;
		G_B11_4 = G_B9_3;
		goto IL_00bb;
	}

IL_00b6:
	{
		G_B11_0 = _stringLiteral109641799;
		G_B11_1 = G_B10_0;
		G_B11_2 = G_B10_1;
		G_B11_3 = G_B10_2;
		G_B11_4 = G_B10_3;
	}

IL_00bb:
	{
		NullCheck(G_B11_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B11_2, G_B11_1);
		ArrayElementTypeCheck (G_B11_2, G_B11_0);
		(G_B11_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B11_1), (Il2CppObject *)G_B11_0);
		ObjectU5BU5D_t1108656482* L_26 = G_B11_3;
		FsmFloat_t2134102846 * L_27 = __this->get_speed_22();
		NullCheck(L_27);
		bool L_28 = NamedVariable_get_IsNone_m281035543(L_27, /*hidden argument*/NULL);
		G_B12_0 = 5;
		G_B12_1 = L_26;
		G_B12_2 = L_26;
		G_B12_3 = G_B11_4;
		if (!L_28)
		{
			G_B16_0 = 5;
			G_B16_1 = L_26;
			G_B16_2 = L_26;
			G_B16_3 = G_B11_4;
			goto IL_00f8;
		}
	}
	{
		FsmFloat_t2134102846 * L_29 = __this->get_time_20();
		NullCheck(L_29);
		bool L_30 = NamedVariable_get_IsNone_m281035543(L_29, /*hidden argument*/NULL);
		G_B13_0 = G_B12_0;
		G_B13_1 = G_B12_1;
		G_B13_2 = G_B12_2;
		G_B13_3 = G_B12_3;
		if (!L_30)
		{
			G_B14_0 = G_B12_0;
			G_B14_1 = G_B12_1;
			G_B14_2 = G_B12_2;
			G_B14_3 = G_B12_3;
			goto IL_00e8;
		}
	}
	{
		G_B15_0 = (1.0f);
		G_B15_1 = G_B13_0;
		G_B15_2 = G_B13_1;
		G_B15_3 = G_B13_2;
		G_B15_4 = G_B13_3;
		goto IL_00f3;
	}

IL_00e8:
	{
		FsmFloat_t2134102846 * L_31 = __this->get_time_20();
		NullCheck(L_31);
		float L_32 = FsmFloat_get_Value_m4137923823(L_31, /*hidden argument*/NULL);
		G_B15_0 = L_32;
		G_B15_1 = G_B14_0;
		G_B15_2 = G_B14_1;
		G_B15_3 = G_B14_2;
		G_B15_4 = G_B14_3;
	}

IL_00f3:
	{
		G_B17_0 = G_B15_0;
		G_B17_1 = G_B15_1;
		G_B17_2 = G_B15_2;
		G_B17_3 = G_B15_3;
		G_B17_4 = G_B15_4;
		goto IL_0103;
	}

IL_00f8:
	{
		FsmFloat_t2134102846 * L_33 = __this->get_speed_22();
		NullCheck(L_33);
		float L_34 = FsmFloat_get_Value_m4137923823(L_33, /*hidden argument*/NULL);
		G_B17_0 = L_34;
		G_B17_1 = G_B16_0;
		G_B17_2 = G_B16_1;
		G_B17_3 = G_B16_2;
		G_B17_4 = G_B16_3;
	}

IL_0103:
	{
		float L_35 = G_B17_0;
		Il2CppObject * L_36 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_35);
		NullCheck(G_B17_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B17_2, G_B17_1);
		ArrayElementTypeCheck (G_B17_2, L_36);
		(G_B17_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B17_1), (Il2CppObject *)L_36);
		ObjectU5BU5D_t1108656482* L_37 = G_B17_3;
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, 6);
		ArrayElementTypeCheck (L_37, _stringLiteral95467907);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral95467907);
		ObjectU5BU5D_t1108656482* L_38 = L_37;
		FsmFloat_t2134102846 * L_39 = __this->get_delay_21();
		NullCheck(L_39);
		bool L_40 = NamedVariable_get_IsNone_m281035543(L_39, /*hidden argument*/NULL);
		G_B18_0 = 7;
		G_B18_1 = L_38;
		G_B18_2 = L_38;
		G_B18_3 = G_B17_4;
		if (!L_40)
		{
			G_B19_0 = 7;
			G_B19_1 = L_38;
			G_B19_2 = L_38;
			G_B19_3 = G_B17_4;
			goto IL_012d;
		}
	}
	{
		G_B20_0 = (0.0f);
		G_B20_1 = G_B18_0;
		G_B20_2 = G_B18_1;
		G_B20_3 = G_B18_2;
		G_B20_4 = G_B18_3;
		goto IL_0138;
	}

IL_012d:
	{
		FsmFloat_t2134102846 * L_41 = __this->get_delay_21();
		NullCheck(L_41);
		float L_42 = FsmFloat_get_Value_m4137923823(L_41, /*hidden argument*/NULL);
		G_B20_0 = L_42;
		G_B20_1 = G_B19_0;
		G_B20_2 = G_B19_1;
		G_B20_3 = G_B19_2;
		G_B20_4 = G_B19_3;
	}

IL_0138:
	{
		float L_43 = G_B20_0;
		Il2CppObject * L_44 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_43);
		NullCheck(G_B20_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B20_2, G_B20_1);
		ArrayElementTypeCheck (G_B20_2, L_44);
		(G_B20_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B20_1), (Il2CppObject *)L_44);
		ObjectU5BU5D_t1108656482* L_45 = G_B20_3;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, 8);
		ArrayElementTypeCheck (L_45, _stringLiteral3507899432);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral3507899432);
		ObjectU5BU5D_t1108656482* L_46 = L_45;
		int32_t L_47 = __this->get_easeType_23();
		int32_t L_48 = L_47;
		Il2CppObject * L_49 = Box(EaseType_t2734598229_il2cpp_TypeInfo_var, &L_48);
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, ((int32_t)9));
		ArrayElementTypeCheck (L_46, L_49);
		(L_46)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)L_49);
		ObjectU5BU5D_t1108656482* L_50 = L_46;
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, ((int32_t)10));
		ArrayElementTypeCheck (L_50, _stringLiteral2258461662);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Il2CppObject *)_stringLiteral2258461662);
		ObjectU5BU5D_t1108656482* L_51 = L_50;
		int32_t L_52 = __this->get_loopType_24();
		int32_t L_53 = L_52;
		Il2CppObject * L_54 = Box(LoopType_t1485160459_il2cpp_TypeInfo_var, &L_53);
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, ((int32_t)11));
		ArrayElementTypeCheck (L_51, L_54);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (Il2CppObject *)L_54);
		ObjectU5BU5D_t1108656482* L_55 = L_51;
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, ((int32_t)12));
		ArrayElementTypeCheck (L_55, _stringLiteral2105864216);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (Il2CppObject *)_stringLiteral2105864216);
		ObjectU5BU5D_t1108656482* L_56 = L_55;
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, ((int32_t)13));
		ArrayElementTypeCheck (L_56, _stringLiteral338257242);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (Il2CppObject *)_stringLiteral338257242);
		ObjectU5BU5D_t1108656482* L_57 = L_56;
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, ((int32_t)14));
		ArrayElementTypeCheck (L_57, _stringLiteral3696326558);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (Il2CppObject *)_stringLiteral3696326558);
		ObjectU5BU5D_t1108656482* L_58 = L_57;
		int32_t L_59 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_60 = L_59;
		Il2CppObject * L_61 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_60);
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, ((int32_t)15));
		ArrayElementTypeCheck (L_58, L_61);
		(L_58)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (Il2CppObject *)L_61);
		ObjectU5BU5D_t1108656482* L_62 = L_58;
		NullCheck(L_62);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_62, ((int32_t)16));
		ArrayElementTypeCheck (L_62, _stringLiteral2987624931);
		(L_62)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (Il2CppObject *)_stringLiteral2987624931);
		ObjectU5BU5D_t1108656482* L_63 = L_62;
		NullCheck(L_63);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_63, ((int32_t)17));
		ArrayElementTypeCheck (L_63, _stringLiteral647687137);
		(L_63)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (Il2CppObject *)_stringLiteral647687137);
		ObjectU5BU5D_t1108656482* L_64 = L_63;
		NullCheck(L_64);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_64, ((int32_t)18));
		ArrayElementTypeCheck (L_64, _stringLiteral2052738345);
		(L_64)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (Il2CppObject *)_stringLiteral2052738345);
		ObjectU5BU5D_t1108656482* L_65 = L_64;
		int32_t L_66 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_67 = L_66;
		Il2CppObject * L_68 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_67);
		NullCheck(L_65);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_65, ((int32_t)19));
		ArrayElementTypeCheck (L_65, L_68);
		(L_65)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)19)), (Il2CppObject *)L_68);
		ObjectU5BU5D_t1108656482* L_69 = L_65;
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, ((int32_t)20));
		ArrayElementTypeCheck (L_69, _stringLiteral1067506955);
		(L_69)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)20)), (Il2CppObject *)_stringLiteral1067506955);
		ObjectU5BU5D_t1108656482* L_70 = L_69;
		FsmBool_t1075959796 * L_71 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_71);
		bool L_72 = NamedVariable_get_IsNone_m281035543(L_71, /*hidden argument*/NULL);
		G_B21_0 = ((int32_t)21);
		G_B21_1 = L_70;
		G_B21_2 = L_70;
		G_B21_3 = G_B20_4;
		if (!L_72)
		{
			G_B22_0 = ((int32_t)21);
			G_B22_1 = L_70;
			G_B22_2 = L_70;
			G_B22_3 = G_B20_4;
			goto IL_01e3;
		}
	}
	{
		G_B23_0 = 0;
		G_B23_1 = G_B21_0;
		G_B23_2 = G_B21_1;
		G_B23_3 = G_B21_2;
		G_B23_4 = G_B21_3;
		goto IL_01ee;
	}

IL_01e3:
	{
		FsmBool_t1075959796 * L_73 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_73);
		bool L_74 = FsmBool_get_Value_m3101329097(L_73, /*hidden argument*/NULL);
		G_B23_0 = ((int32_t)(L_74));
		G_B23_1 = G_B22_0;
		G_B23_2 = G_B22_1;
		G_B23_3 = G_B22_2;
		G_B23_4 = G_B22_3;
	}

IL_01ee:
	{
		bool L_75 = ((bool)G_B23_0);
		Il2CppObject * L_76 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_75);
		NullCheck(G_B23_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B23_2, G_B23_1);
		ArrayElementTypeCheck (G_B23_2, L_76);
		(G_B23_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B23_1), (Il2CppObject *)L_76);
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		Hashtable_t1407064410 * L_77 = iTween_Hash_m2099495140(NULL /*static, unused*/, G_B23_3, /*hidden argument*/NULL);
		iTween_ScaleBy_m2987979470(NULL /*static, unused*/, G_B23_4, L_77, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleFrom::.ctor()
extern "C"  void iTweenScaleFrom__ctor_m1601129302 (iTweenScaleFrom_t4034512864 * __this, const MethodInfo* method)
{
	{
		__this->set_easeType_24(((int32_t)21));
		iTweenFsmAction__ctor_m2960815636(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleFrom::Reset()
extern Il2CppClass* FsmString_t952858651_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmGameObject_t1697147867_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmFloat_t2134102846_il2cpp_TypeInfo_var;
extern const uint32_t iTweenScaleFrom_Reset_m3542529539_MetadataUsageId;
extern "C"  void iTweenScaleFrom_Reset_m3542529539 (iTweenScaleFrom_t4034512864 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenScaleFrom_Reset_m3542529539_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmString_t952858651 * V_0 = NULL;
	FsmGameObject_t1697147867 * V_1 = NULL;
	FsmVector3_t533912882 * V_2 = NULL;
	FsmFloat_t2134102846 * V_3 = NULL;
	{
		iTweenFsmAction_Reset_m607248577(__this, /*hidden argument*/NULL);
		FsmString_t952858651 * L_0 = (FsmString_t952858651 *)il2cpp_codegen_object_new(FsmString_t952858651_il2cpp_TypeInfo_var);
		FsmString__ctor_m2679108260(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmString_t952858651 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmString_t952858651 * L_2 = V_0;
		__this->set_id_18(L_2);
		FsmGameObject_t1697147867 * L_3 = (FsmGameObject_t1697147867 *)il2cpp_codegen_object_new(FsmGameObject_t1697147867_il2cpp_TypeInfo_var);
		FsmGameObject__ctor_m1048269796(L_3, /*hidden argument*/NULL);
		V_1 = L_3;
		FsmGameObject_t1697147867 * L_4 = V_1;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_5 = V_1;
		__this->set_transformScale_19(L_5);
		FsmVector3_t533912882 * L_6 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_6, /*hidden argument*/NULL);
		V_2 = L_6;
		FsmVector3_t533912882 * L_7 = V_2;
		NullCheck(L_7);
		NamedVariable_set_UseVariable_m4266138971(L_7, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_8 = V_2;
		__this->set_vectorScale_20(L_8);
		FsmFloat_t2134102846 * L_9 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_time_21(L_9);
		FsmFloat_t2134102846 * L_10 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_delay_22(L_10);
		__this->set_loopType_25(0);
		FsmFloat_t2134102846 * L_11 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_11, /*hidden argument*/NULL);
		V_3 = L_11;
		FsmFloat_t2134102846 * L_12 = V_3;
		NullCheck(L_12);
		NamedVariable_set_UseVariable_m4266138971(L_12, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_13 = V_3;
		__this->set_speed_23(L_13);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleFrom::OnEnter()
extern "C"  void iTweenScaleFrom_OnEnter_m478695853 (iTweenScaleFrom_t4034512864 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnEnteriTween_m3024539459(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_loopType_25();
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		iTweenFsmAction_IsLoop_m2539277653(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_001e:
	{
		iTweenScaleFrom_DoiTween_m3405801787(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleFrom::OnExit()
extern "C"  void iTweenScaleFrom_OnExit_m3903686731 (iTweenScaleFrom_t4034512864 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnExitiTween_m1981625761(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleFrom::DoiTween()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* EaseType_t2734598229_il2cpp_TypeInfo_var;
extern Il2CppClass* LoopType_t1485160459_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* iTween_t3087282050_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral109250890;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral3560141;
extern Il2CppCodeGenString* _stringLiteral109641799;
extern Il2CppCodeGenString* _stringLiteral95467907;
extern Il2CppCodeGenString* _stringLiteral3507899432;
extern Il2CppCodeGenString* _stringLiteral2258461662;
extern Il2CppCodeGenString* _stringLiteral2105864216;
extern Il2CppCodeGenString* _stringLiteral338257242;
extern Il2CppCodeGenString* _stringLiteral3696326558;
extern Il2CppCodeGenString* _stringLiteral2987624931;
extern Il2CppCodeGenString* _stringLiteral647687137;
extern Il2CppCodeGenString* _stringLiteral2052738345;
extern Il2CppCodeGenString* _stringLiteral1067506955;
extern const uint32_t iTweenScaleFrom_DoiTween_m3405801787_MetadataUsageId;
extern "C"  void iTweenScaleFrom_DoiTween_m3405801787 (iTweenScaleFrom_t4034512864 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenScaleFrom_DoiTween_m3405801787_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  G_B5_0;
	memset(&G_B5_0, 0, sizeof(G_B5_0));
	int32_t G_B10_0 = 0;
	ObjectU5BU5D_t1108656482* G_B10_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B10_2 = NULL;
	GameObject_t3674682005 * G_B10_3 = NULL;
	int32_t G_B9_0 = 0;
	ObjectU5BU5D_t1108656482* G_B9_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B9_2 = NULL;
	GameObject_t3674682005 * G_B9_3 = NULL;
	String_t* G_B11_0 = NULL;
	int32_t G_B11_1 = 0;
	ObjectU5BU5D_t1108656482* G_B11_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B11_3 = NULL;
	GameObject_t3674682005 * G_B11_4 = NULL;
	int32_t G_B13_0 = 0;
	ObjectU5BU5D_t1108656482* G_B13_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B13_2 = NULL;
	GameObject_t3674682005 * G_B13_3 = NULL;
	int32_t G_B12_0 = 0;
	ObjectU5BU5D_t1108656482* G_B12_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B12_2 = NULL;
	GameObject_t3674682005 * G_B12_3 = NULL;
	String_t* G_B14_0 = NULL;
	int32_t G_B14_1 = 0;
	ObjectU5BU5D_t1108656482* G_B14_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B14_3 = NULL;
	GameObject_t3674682005 * G_B14_4 = NULL;
	int32_t G_B19_0 = 0;
	ObjectU5BU5D_t1108656482* G_B19_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B19_2 = NULL;
	GameObject_t3674682005 * G_B19_3 = NULL;
	int32_t G_B15_0 = 0;
	ObjectU5BU5D_t1108656482* G_B15_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B15_2 = NULL;
	GameObject_t3674682005 * G_B15_3 = NULL;
	int32_t G_B17_0 = 0;
	ObjectU5BU5D_t1108656482* G_B17_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B17_2 = NULL;
	GameObject_t3674682005 * G_B17_3 = NULL;
	int32_t G_B16_0 = 0;
	ObjectU5BU5D_t1108656482* G_B16_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B16_2 = NULL;
	GameObject_t3674682005 * G_B16_3 = NULL;
	float G_B18_0 = 0.0f;
	int32_t G_B18_1 = 0;
	ObjectU5BU5D_t1108656482* G_B18_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B18_3 = NULL;
	GameObject_t3674682005 * G_B18_4 = NULL;
	float G_B20_0 = 0.0f;
	int32_t G_B20_1 = 0;
	ObjectU5BU5D_t1108656482* G_B20_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B20_3 = NULL;
	GameObject_t3674682005 * G_B20_4 = NULL;
	int32_t G_B22_0 = 0;
	ObjectU5BU5D_t1108656482* G_B22_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B22_2 = NULL;
	GameObject_t3674682005 * G_B22_3 = NULL;
	int32_t G_B21_0 = 0;
	ObjectU5BU5D_t1108656482* G_B21_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B21_2 = NULL;
	GameObject_t3674682005 * G_B21_3 = NULL;
	float G_B23_0 = 0.0f;
	int32_t G_B23_1 = 0;
	ObjectU5BU5D_t1108656482* G_B23_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B23_3 = NULL;
	GameObject_t3674682005 * G_B23_4 = NULL;
	int32_t G_B25_0 = 0;
	ObjectU5BU5D_t1108656482* G_B25_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B25_2 = NULL;
	GameObject_t3674682005 * G_B25_3 = NULL;
	int32_t G_B24_0 = 0;
	ObjectU5BU5D_t1108656482* G_B24_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B24_2 = NULL;
	GameObject_t3674682005 * G_B24_3 = NULL;
	int32_t G_B26_0 = 0;
	int32_t G_B26_1 = 0;
	ObjectU5BU5D_t1108656482* G_B26_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B26_3 = NULL;
	GameObject_t3674682005 * G_B26_4 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_17();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		FsmVector3_t533912882 * L_5 = __this->get_vectorScale_20();
		NullCheck(L_5);
		bool L_6 = NamedVariable_get_IsNone_m281035543(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0039;
		}
	}
	{
		Vector3_t4282066566  L_7 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B5_0 = L_7;
		goto IL_0044;
	}

IL_0039:
	{
		FsmVector3_t533912882 * L_8 = __this->get_vectorScale_20();
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = FsmVector3_get_Value_m2779135117(L_8, /*hidden argument*/NULL);
		G_B5_0 = L_9;
	}

IL_0044:
	{
		V_1 = G_B5_0;
		FsmGameObject_t1697147867 * L_10 = __this->get_transformScale_19();
		NullCheck(L_10);
		bool L_11 = NamedVariable_get_IsNone_m281035543(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0086;
		}
	}
	{
		FsmGameObject_t1697147867 * L_12 = __this->get_transformScale_19();
		NullCheck(L_12);
		GameObject_t3674682005 * L_13 = FsmGameObject_get_Value_m673294275(L_12, /*hidden argument*/NULL);
		bool L_14 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0086;
		}
	}
	{
		FsmGameObject_t1697147867 * L_15 = __this->get_transformScale_19();
		NullCheck(L_15);
		GameObject_t3674682005 * L_16 = FsmGameObject_get_Value_m673294275(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_t1659122786 * L_17 = GameObject_get_transform_m1278640159(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t4282066566  L_18 = Transform_get_localScale_m3886572677(L_17, /*hidden argument*/NULL);
		Vector3_t4282066566  L_19 = V_1;
		Vector3_t4282066566  L_20 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		V_1 = L_20;
	}

IL_0086:
	{
		((iTweenFsmAction_t410382178 *)__this)->set_itweenType_15(_stringLiteral109250890);
		GameObject_t3674682005 * L_21 = V_0;
		ObjectU5BU5D_t1108656482* L_22 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)22)));
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 0);
		ArrayElementTypeCheck (L_22, _stringLiteral109250890);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral109250890);
		ObjectU5BU5D_t1108656482* L_23 = L_22;
		Vector3_t4282066566  L_24 = V_1;
		Vector3_t4282066566  L_25 = L_24;
		Il2CppObject * L_26 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_25);
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 1);
		ArrayElementTypeCheck (L_23, L_26);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_26);
		ObjectU5BU5D_t1108656482* L_27 = L_23;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, 2);
		ArrayElementTypeCheck (L_27, _stringLiteral3373707);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3373707);
		ObjectU5BU5D_t1108656482* L_28 = L_27;
		FsmString_t952858651 * L_29 = __this->get_id_18();
		NullCheck(L_29);
		bool L_30 = NamedVariable_get_IsNone_m281035543(L_29, /*hidden argument*/NULL);
		G_B9_0 = 3;
		G_B9_1 = L_28;
		G_B9_2 = L_28;
		G_B9_3 = L_21;
		if (!L_30)
		{
			G_B10_0 = 3;
			G_B10_1 = L_28;
			G_B10_2 = L_28;
			G_B10_3 = L_21;
			goto IL_00ce;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_31 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B11_0 = L_31;
		G_B11_1 = G_B9_0;
		G_B11_2 = G_B9_1;
		G_B11_3 = G_B9_2;
		G_B11_4 = G_B9_3;
		goto IL_00d9;
	}

IL_00ce:
	{
		FsmString_t952858651 * L_32 = __this->get_id_18();
		NullCheck(L_32);
		String_t* L_33 = FsmString_get_Value_m872383149(L_32, /*hidden argument*/NULL);
		G_B11_0 = L_33;
		G_B11_1 = G_B10_0;
		G_B11_2 = G_B10_1;
		G_B11_3 = G_B10_2;
		G_B11_4 = G_B10_3;
	}

IL_00d9:
	{
		NullCheck(G_B11_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B11_2, G_B11_1);
		ArrayElementTypeCheck (G_B11_2, G_B11_0);
		(G_B11_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B11_1), (Il2CppObject *)G_B11_0);
		ObjectU5BU5D_t1108656482* L_34 = G_B11_3;
		FsmFloat_t2134102846 * L_35 = __this->get_speed_23();
		NullCheck(L_35);
		bool L_36 = NamedVariable_get_IsNone_m281035543(L_35, /*hidden argument*/NULL);
		G_B12_0 = 4;
		G_B12_1 = L_34;
		G_B12_2 = L_34;
		G_B12_3 = G_B11_4;
		if (!L_36)
		{
			G_B13_0 = 4;
			G_B13_1 = L_34;
			G_B13_2 = L_34;
			G_B13_3 = G_B11_4;
			goto IL_00f6;
		}
	}
	{
		G_B14_0 = _stringLiteral3560141;
		G_B14_1 = G_B12_0;
		G_B14_2 = G_B12_1;
		G_B14_3 = G_B12_2;
		G_B14_4 = G_B12_3;
		goto IL_00fb;
	}

IL_00f6:
	{
		G_B14_0 = _stringLiteral109641799;
		G_B14_1 = G_B13_0;
		G_B14_2 = G_B13_1;
		G_B14_3 = G_B13_2;
		G_B14_4 = G_B13_3;
	}

IL_00fb:
	{
		NullCheck(G_B14_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B14_2, G_B14_1);
		ArrayElementTypeCheck (G_B14_2, G_B14_0);
		(G_B14_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B14_1), (Il2CppObject *)G_B14_0);
		ObjectU5BU5D_t1108656482* L_37 = G_B14_3;
		FsmFloat_t2134102846 * L_38 = __this->get_speed_23();
		NullCheck(L_38);
		bool L_39 = NamedVariable_get_IsNone_m281035543(L_38, /*hidden argument*/NULL);
		G_B15_0 = 5;
		G_B15_1 = L_37;
		G_B15_2 = L_37;
		G_B15_3 = G_B14_4;
		if (!L_39)
		{
			G_B19_0 = 5;
			G_B19_1 = L_37;
			G_B19_2 = L_37;
			G_B19_3 = G_B14_4;
			goto IL_0138;
		}
	}
	{
		FsmFloat_t2134102846 * L_40 = __this->get_time_21();
		NullCheck(L_40);
		bool L_41 = NamedVariable_get_IsNone_m281035543(L_40, /*hidden argument*/NULL);
		G_B16_0 = G_B15_0;
		G_B16_1 = G_B15_1;
		G_B16_2 = G_B15_2;
		G_B16_3 = G_B15_3;
		if (!L_41)
		{
			G_B17_0 = G_B15_0;
			G_B17_1 = G_B15_1;
			G_B17_2 = G_B15_2;
			G_B17_3 = G_B15_3;
			goto IL_0128;
		}
	}
	{
		G_B18_0 = (1.0f);
		G_B18_1 = G_B16_0;
		G_B18_2 = G_B16_1;
		G_B18_3 = G_B16_2;
		G_B18_4 = G_B16_3;
		goto IL_0133;
	}

IL_0128:
	{
		FsmFloat_t2134102846 * L_42 = __this->get_time_21();
		NullCheck(L_42);
		float L_43 = FsmFloat_get_Value_m4137923823(L_42, /*hidden argument*/NULL);
		G_B18_0 = L_43;
		G_B18_1 = G_B17_0;
		G_B18_2 = G_B17_1;
		G_B18_3 = G_B17_2;
		G_B18_4 = G_B17_3;
	}

IL_0133:
	{
		G_B20_0 = G_B18_0;
		G_B20_1 = G_B18_1;
		G_B20_2 = G_B18_2;
		G_B20_3 = G_B18_3;
		G_B20_4 = G_B18_4;
		goto IL_0143;
	}

IL_0138:
	{
		FsmFloat_t2134102846 * L_44 = __this->get_speed_23();
		NullCheck(L_44);
		float L_45 = FsmFloat_get_Value_m4137923823(L_44, /*hidden argument*/NULL);
		G_B20_0 = L_45;
		G_B20_1 = G_B19_0;
		G_B20_2 = G_B19_1;
		G_B20_3 = G_B19_2;
		G_B20_4 = G_B19_3;
	}

IL_0143:
	{
		float L_46 = G_B20_0;
		Il2CppObject * L_47 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_46);
		NullCheck(G_B20_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B20_2, G_B20_1);
		ArrayElementTypeCheck (G_B20_2, L_47);
		(G_B20_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B20_1), (Il2CppObject *)L_47);
		ObjectU5BU5D_t1108656482* L_48 = G_B20_3;
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, 6);
		ArrayElementTypeCheck (L_48, _stringLiteral95467907);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral95467907);
		ObjectU5BU5D_t1108656482* L_49 = L_48;
		FsmFloat_t2134102846 * L_50 = __this->get_delay_22();
		NullCheck(L_50);
		bool L_51 = NamedVariable_get_IsNone_m281035543(L_50, /*hidden argument*/NULL);
		G_B21_0 = 7;
		G_B21_1 = L_49;
		G_B21_2 = L_49;
		G_B21_3 = G_B20_4;
		if (!L_51)
		{
			G_B22_0 = 7;
			G_B22_1 = L_49;
			G_B22_2 = L_49;
			G_B22_3 = G_B20_4;
			goto IL_016d;
		}
	}
	{
		G_B23_0 = (0.0f);
		G_B23_1 = G_B21_0;
		G_B23_2 = G_B21_1;
		G_B23_3 = G_B21_2;
		G_B23_4 = G_B21_3;
		goto IL_0178;
	}

IL_016d:
	{
		FsmFloat_t2134102846 * L_52 = __this->get_delay_22();
		NullCheck(L_52);
		float L_53 = FsmFloat_get_Value_m4137923823(L_52, /*hidden argument*/NULL);
		G_B23_0 = L_53;
		G_B23_1 = G_B22_0;
		G_B23_2 = G_B22_1;
		G_B23_3 = G_B22_2;
		G_B23_4 = G_B22_3;
	}

IL_0178:
	{
		float L_54 = G_B23_0;
		Il2CppObject * L_55 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_54);
		NullCheck(G_B23_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B23_2, G_B23_1);
		ArrayElementTypeCheck (G_B23_2, L_55);
		(G_B23_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B23_1), (Il2CppObject *)L_55);
		ObjectU5BU5D_t1108656482* L_56 = G_B23_3;
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, 8);
		ArrayElementTypeCheck (L_56, _stringLiteral3507899432);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral3507899432);
		ObjectU5BU5D_t1108656482* L_57 = L_56;
		int32_t L_58 = __this->get_easeType_24();
		int32_t L_59 = L_58;
		Il2CppObject * L_60 = Box(EaseType_t2734598229_il2cpp_TypeInfo_var, &L_59);
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, ((int32_t)9));
		ArrayElementTypeCheck (L_57, L_60);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)L_60);
		ObjectU5BU5D_t1108656482* L_61 = L_57;
		NullCheck(L_61);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_61, ((int32_t)10));
		ArrayElementTypeCheck (L_61, _stringLiteral2258461662);
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Il2CppObject *)_stringLiteral2258461662);
		ObjectU5BU5D_t1108656482* L_62 = L_61;
		int32_t L_63 = __this->get_loopType_25();
		int32_t L_64 = L_63;
		Il2CppObject * L_65 = Box(LoopType_t1485160459_il2cpp_TypeInfo_var, &L_64);
		NullCheck(L_62);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_62, ((int32_t)11));
		ArrayElementTypeCheck (L_62, L_65);
		(L_62)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (Il2CppObject *)L_65);
		ObjectU5BU5D_t1108656482* L_66 = L_62;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, ((int32_t)12));
		ArrayElementTypeCheck (L_66, _stringLiteral2105864216);
		(L_66)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (Il2CppObject *)_stringLiteral2105864216);
		ObjectU5BU5D_t1108656482* L_67 = L_66;
		NullCheck(L_67);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_67, ((int32_t)13));
		ArrayElementTypeCheck (L_67, _stringLiteral338257242);
		(L_67)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (Il2CppObject *)_stringLiteral338257242);
		ObjectU5BU5D_t1108656482* L_68 = L_67;
		NullCheck(L_68);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_68, ((int32_t)14));
		ArrayElementTypeCheck (L_68, _stringLiteral3696326558);
		(L_68)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (Il2CppObject *)_stringLiteral3696326558);
		ObjectU5BU5D_t1108656482* L_69 = L_68;
		int32_t L_70 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_71 = L_70;
		Il2CppObject * L_72 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_71);
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, ((int32_t)15));
		ArrayElementTypeCheck (L_69, L_72);
		(L_69)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (Il2CppObject *)L_72);
		ObjectU5BU5D_t1108656482* L_73 = L_69;
		NullCheck(L_73);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_73, ((int32_t)16));
		ArrayElementTypeCheck (L_73, _stringLiteral2987624931);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (Il2CppObject *)_stringLiteral2987624931);
		ObjectU5BU5D_t1108656482* L_74 = L_73;
		NullCheck(L_74);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_74, ((int32_t)17));
		ArrayElementTypeCheck (L_74, _stringLiteral647687137);
		(L_74)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (Il2CppObject *)_stringLiteral647687137);
		ObjectU5BU5D_t1108656482* L_75 = L_74;
		NullCheck(L_75);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_75, ((int32_t)18));
		ArrayElementTypeCheck (L_75, _stringLiteral2052738345);
		(L_75)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (Il2CppObject *)_stringLiteral2052738345);
		ObjectU5BU5D_t1108656482* L_76 = L_75;
		int32_t L_77 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_78 = L_77;
		Il2CppObject * L_79 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_78);
		NullCheck(L_76);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_76, ((int32_t)19));
		ArrayElementTypeCheck (L_76, L_79);
		(L_76)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)19)), (Il2CppObject *)L_79);
		ObjectU5BU5D_t1108656482* L_80 = L_76;
		NullCheck(L_80);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_80, ((int32_t)20));
		ArrayElementTypeCheck (L_80, _stringLiteral1067506955);
		(L_80)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)20)), (Il2CppObject *)_stringLiteral1067506955);
		ObjectU5BU5D_t1108656482* L_81 = L_80;
		FsmBool_t1075959796 * L_82 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_82);
		bool L_83 = NamedVariable_get_IsNone_m281035543(L_82, /*hidden argument*/NULL);
		G_B24_0 = ((int32_t)21);
		G_B24_1 = L_81;
		G_B24_2 = L_81;
		G_B24_3 = G_B23_4;
		if (!L_83)
		{
			G_B25_0 = ((int32_t)21);
			G_B25_1 = L_81;
			G_B25_2 = L_81;
			G_B25_3 = G_B23_4;
			goto IL_0223;
		}
	}
	{
		G_B26_0 = 0;
		G_B26_1 = G_B24_0;
		G_B26_2 = G_B24_1;
		G_B26_3 = G_B24_2;
		G_B26_4 = G_B24_3;
		goto IL_022e;
	}

IL_0223:
	{
		FsmBool_t1075959796 * L_84 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_84);
		bool L_85 = FsmBool_get_Value_m3101329097(L_84, /*hidden argument*/NULL);
		G_B26_0 = ((int32_t)(L_85));
		G_B26_1 = G_B25_0;
		G_B26_2 = G_B25_1;
		G_B26_3 = G_B25_2;
		G_B26_4 = G_B25_3;
	}

IL_022e:
	{
		bool L_86 = ((bool)G_B26_0);
		Il2CppObject * L_87 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_86);
		NullCheck(G_B26_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B26_2, G_B26_1);
		ArrayElementTypeCheck (G_B26_2, L_87);
		(G_B26_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B26_1), (Il2CppObject *)L_87);
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		Hashtable_t1407064410 * L_88 = iTween_Hash_m2099495140(NULL /*static, unused*/, G_B26_3, /*hidden argument*/NULL);
		iTween_ScaleFrom_m2267802971(NULL /*static, unused*/, G_B26_4, L_88, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleTo::.ctor()
extern "C"  void iTweenScaleTo__ctor_m1752242917 (iTweenScaleTo_t1692845425 * __this, const MethodInfo* method)
{
	{
		__this->set_easeType_24(((int32_t)21));
		iTweenFsmAction__ctor_m2960815636(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleTo::Reset()
extern Il2CppClass* FsmString_t952858651_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmGameObject_t1697147867_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmFloat_t2134102846_il2cpp_TypeInfo_var;
extern const uint32_t iTweenScaleTo_Reset_m3693643154_MetadataUsageId;
extern "C"  void iTweenScaleTo_Reset_m3693643154 (iTweenScaleTo_t1692845425 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenScaleTo_Reset_m3693643154_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmString_t952858651 * V_0 = NULL;
	FsmGameObject_t1697147867 * V_1 = NULL;
	FsmVector3_t533912882 * V_2 = NULL;
	FsmFloat_t2134102846 * V_3 = NULL;
	{
		iTweenFsmAction_Reset_m607248577(__this, /*hidden argument*/NULL);
		FsmString_t952858651 * L_0 = (FsmString_t952858651 *)il2cpp_codegen_object_new(FsmString_t952858651_il2cpp_TypeInfo_var);
		FsmString__ctor_m2679108260(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmString_t952858651 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmString_t952858651 * L_2 = V_0;
		__this->set_id_18(L_2);
		FsmGameObject_t1697147867 * L_3 = (FsmGameObject_t1697147867 *)il2cpp_codegen_object_new(FsmGameObject_t1697147867_il2cpp_TypeInfo_var);
		FsmGameObject__ctor_m1048269796(L_3, /*hidden argument*/NULL);
		V_1 = L_3;
		FsmGameObject_t1697147867 * L_4 = V_1;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_5 = V_1;
		__this->set_transformScale_19(L_5);
		FsmVector3_t533912882 * L_6 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_6, /*hidden argument*/NULL);
		V_2 = L_6;
		FsmVector3_t533912882 * L_7 = V_2;
		NullCheck(L_7);
		NamedVariable_set_UseVariable_m4266138971(L_7, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_8 = V_2;
		__this->set_vectorScale_20(L_8);
		FsmFloat_t2134102846 * L_9 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_time_21(L_9);
		FsmFloat_t2134102846 * L_10 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_delay_22(L_10);
		__this->set_loopType_25(0);
		FsmFloat_t2134102846 * L_11 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_11, /*hidden argument*/NULL);
		V_3 = L_11;
		FsmFloat_t2134102846 * L_12 = V_3;
		NullCheck(L_12);
		NamedVariable_set_UseVariable_m4266138971(L_12, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_13 = V_3;
		__this->set_speed_23(L_13);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleTo::OnEnter()
extern "C"  void iTweenScaleTo_OnEnter_m3964959100 (iTweenScaleTo_t1692845425 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnEnteriTween_m3024539459(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_loopType_25();
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		iTweenFsmAction_IsLoop_m2539277653(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_001e:
	{
		iTweenScaleTo_DoiTween_m4105780044(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleTo::OnExit()
extern "C"  void iTweenScaleTo_OnExit_m4293241500 (iTweenScaleTo_t1692845425 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnExitiTween_m1981625761(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleTo::DoiTween()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* EaseType_t2734598229_il2cpp_TypeInfo_var;
extern Il2CppClass* LoopType_t1485160459_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* iTween_t3087282050_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral109250890;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral3560141;
extern Il2CppCodeGenString* _stringLiteral109641799;
extern Il2CppCodeGenString* _stringLiteral95467907;
extern Il2CppCodeGenString* _stringLiteral3507899432;
extern Il2CppCodeGenString* _stringLiteral2258461662;
extern Il2CppCodeGenString* _stringLiteral2105864216;
extern Il2CppCodeGenString* _stringLiteral338257242;
extern Il2CppCodeGenString* _stringLiteral3696326558;
extern Il2CppCodeGenString* _stringLiteral2987624931;
extern Il2CppCodeGenString* _stringLiteral647687137;
extern Il2CppCodeGenString* _stringLiteral2052738345;
extern Il2CppCodeGenString* _stringLiteral1067506955;
extern const uint32_t iTweenScaleTo_DoiTween_m4105780044_MetadataUsageId;
extern "C"  void iTweenScaleTo_DoiTween_m4105780044 (iTweenScaleTo_t1692845425 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenScaleTo_DoiTween_m4105780044_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  G_B5_0;
	memset(&G_B5_0, 0, sizeof(G_B5_0));
	int32_t G_B10_0 = 0;
	ObjectU5BU5D_t1108656482* G_B10_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B10_2 = NULL;
	GameObject_t3674682005 * G_B10_3 = NULL;
	int32_t G_B9_0 = 0;
	ObjectU5BU5D_t1108656482* G_B9_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B9_2 = NULL;
	GameObject_t3674682005 * G_B9_3 = NULL;
	String_t* G_B11_0 = NULL;
	int32_t G_B11_1 = 0;
	ObjectU5BU5D_t1108656482* G_B11_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B11_3 = NULL;
	GameObject_t3674682005 * G_B11_4 = NULL;
	int32_t G_B13_0 = 0;
	ObjectU5BU5D_t1108656482* G_B13_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B13_2 = NULL;
	GameObject_t3674682005 * G_B13_3 = NULL;
	int32_t G_B12_0 = 0;
	ObjectU5BU5D_t1108656482* G_B12_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B12_2 = NULL;
	GameObject_t3674682005 * G_B12_3 = NULL;
	String_t* G_B14_0 = NULL;
	int32_t G_B14_1 = 0;
	ObjectU5BU5D_t1108656482* G_B14_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B14_3 = NULL;
	GameObject_t3674682005 * G_B14_4 = NULL;
	int32_t G_B19_0 = 0;
	ObjectU5BU5D_t1108656482* G_B19_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B19_2 = NULL;
	GameObject_t3674682005 * G_B19_3 = NULL;
	int32_t G_B15_0 = 0;
	ObjectU5BU5D_t1108656482* G_B15_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B15_2 = NULL;
	GameObject_t3674682005 * G_B15_3 = NULL;
	int32_t G_B17_0 = 0;
	ObjectU5BU5D_t1108656482* G_B17_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B17_2 = NULL;
	GameObject_t3674682005 * G_B17_3 = NULL;
	int32_t G_B16_0 = 0;
	ObjectU5BU5D_t1108656482* G_B16_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B16_2 = NULL;
	GameObject_t3674682005 * G_B16_3 = NULL;
	float G_B18_0 = 0.0f;
	int32_t G_B18_1 = 0;
	ObjectU5BU5D_t1108656482* G_B18_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B18_3 = NULL;
	GameObject_t3674682005 * G_B18_4 = NULL;
	float G_B20_0 = 0.0f;
	int32_t G_B20_1 = 0;
	ObjectU5BU5D_t1108656482* G_B20_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B20_3 = NULL;
	GameObject_t3674682005 * G_B20_4 = NULL;
	int32_t G_B22_0 = 0;
	ObjectU5BU5D_t1108656482* G_B22_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B22_2 = NULL;
	GameObject_t3674682005 * G_B22_3 = NULL;
	int32_t G_B21_0 = 0;
	ObjectU5BU5D_t1108656482* G_B21_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B21_2 = NULL;
	GameObject_t3674682005 * G_B21_3 = NULL;
	float G_B23_0 = 0.0f;
	int32_t G_B23_1 = 0;
	ObjectU5BU5D_t1108656482* G_B23_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B23_3 = NULL;
	GameObject_t3674682005 * G_B23_4 = NULL;
	int32_t G_B25_0 = 0;
	ObjectU5BU5D_t1108656482* G_B25_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B25_2 = NULL;
	GameObject_t3674682005 * G_B25_3 = NULL;
	int32_t G_B24_0 = 0;
	ObjectU5BU5D_t1108656482* G_B24_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B24_2 = NULL;
	GameObject_t3674682005 * G_B24_3 = NULL;
	int32_t G_B26_0 = 0;
	int32_t G_B26_1 = 0;
	ObjectU5BU5D_t1108656482* G_B26_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B26_3 = NULL;
	GameObject_t3674682005 * G_B26_4 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_17();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		FsmVector3_t533912882 * L_5 = __this->get_vectorScale_20();
		NullCheck(L_5);
		bool L_6 = NamedVariable_get_IsNone_m281035543(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0039;
		}
	}
	{
		Vector3_t4282066566  L_7 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B5_0 = L_7;
		goto IL_0044;
	}

IL_0039:
	{
		FsmVector3_t533912882 * L_8 = __this->get_vectorScale_20();
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = FsmVector3_get_Value_m2779135117(L_8, /*hidden argument*/NULL);
		G_B5_0 = L_9;
	}

IL_0044:
	{
		V_1 = G_B5_0;
		FsmGameObject_t1697147867 * L_10 = __this->get_transformScale_19();
		NullCheck(L_10);
		bool L_11 = NamedVariable_get_IsNone_m281035543(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0086;
		}
	}
	{
		FsmGameObject_t1697147867 * L_12 = __this->get_transformScale_19();
		NullCheck(L_12);
		GameObject_t3674682005 * L_13 = FsmGameObject_get_Value_m673294275(L_12, /*hidden argument*/NULL);
		bool L_14 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0086;
		}
	}
	{
		FsmGameObject_t1697147867 * L_15 = __this->get_transformScale_19();
		NullCheck(L_15);
		GameObject_t3674682005 * L_16 = FsmGameObject_get_Value_m673294275(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_t1659122786 * L_17 = GameObject_get_transform_m1278640159(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t4282066566  L_18 = Transform_get_localScale_m3886572677(L_17, /*hidden argument*/NULL);
		Vector3_t4282066566  L_19 = V_1;
		Vector3_t4282066566  L_20 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		V_1 = L_20;
	}

IL_0086:
	{
		((iTweenFsmAction_t410382178 *)__this)->set_itweenType_15(_stringLiteral109250890);
		GameObject_t3674682005 * L_21 = V_0;
		ObjectU5BU5D_t1108656482* L_22 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)22)));
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 0);
		ArrayElementTypeCheck (L_22, _stringLiteral109250890);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral109250890);
		ObjectU5BU5D_t1108656482* L_23 = L_22;
		Vector3_t4282066566  L_24 = V_1;
		Vector3_t4282066566  L_25 = L_24;
		Il2CppObject * L_26 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_25);
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 1);
		ArrayElementTypeCheck (L_23, L_26);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_26);
		ObjectU5BU5D_t1108656482* L_27 = L_23;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, 2);
		ArrayElementTypeCheck (L_27, _stringLiteral3373707);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3373707);
		ObjectU5BU5D_t1108656482* L_28 = L_27;
		FsmString_t952858651 * L_29 = __this->get_id_18();
		NullCheck(L_29);
		bool L_30 = NamedVariable_get_IsNone_m281035543(L_29, /*hidden argument*/NULL);
		G_B9_0 = 3;
		G_B9_1 = L_28;
		G_B9_2 = L_28;
		G_B9_3 = L_21;
		if (!L_30)
		{
			G_B10_0 = 3;
			G_B10_1 = L_28;
			G_B10_2 = L_28;
			G_B10_3 = L_21;
			goto IL_00ce;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_31 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B11_0 = L_31;
		G_B11_1 = G_B9_0;
		G_B11_2 = G_B9_1;
		G_B11_3 = G_B9_2;
		G_B11_4 = G_B9_3;
		goto IL_00d9;
	}

IL_00ce:
	{
		FsmString_t952858651 * L_32 = __this->get_id_18();
		NullCheck(L_32);
		String_t* L_33 = FsmString_get_Value_m872383149(L_32, /*hidden argument*/NULL);
		G_B11_0 = L_33;
		G_B11_1 = G_B10_0;
		G_B11_2 = G_B10_1;
		G_B11_3 = G_B10_2;
		G_B11_4 = G_B10_3;
	}

IL_00d9:
	{
		NullCheck(G_B11_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B11_2, G_B11_1);
		ArrayElementTypeCheck (G_B11_2, G_B11_0);
		(G_B11_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B11_1), (Il2CppObject *)G_B11_0);
		ObjectU5BU5D_t1108656482* L_34 = G_B11_3;
		FsmFloat_t2134102846 * L_35 = __this->get_speed_23();
		NullCheck(L_35);
		bool L_36 = NamedVariable_get_IsNone_m281035543(L_35, /*hidden argument*/NULL);
		G_B12_0 = 4;
		G_B12_1 = L_34;
		G_B12_2 = L_34;
		G_B12_3 = G_B11_4;
		if (!L_36)
		{
			G_B13_0 = 4;
			G_B13_1 = L_34;
			G_B13_2 = L_34;
			G_B13_3 = G_B11_4;
			goto IL_00f6;
		}
	}
	{
		G_B14_0 = _stringLiteral3560141;
		G_B14_1 = G_B12_0;
		G_B14_2 = G_B12_1;
		G_B14_3 = G_B12_2;
		G_B14_4 = G_B12_3;
		goto IL_00fb;
	}

IL_00f6:
	{
		G_B14_0 = _stringLiteral109641799;
		G_B14_1 = G_B13_0;
		G_B14_2 = G_B13_1;
		G_B14_3 = G_B13_2;
		G_B14_4 = G_B13_3;
	}

IL_00fb:
	{
		NullCheck(G_B14_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B14_2, G_B14_1);
		ArrayElementTypeCheck (G_B14_2, G_B14_0);
		(G_B14_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B14_1), (Il2CppObject *)G_B14_0);
		ObjectU5BU5D_t1108656482* L_37 = G_B14_3;
		FsmFloat_t2134102846 * L_38 = __this->get_speed_23();
		NullCheck(L_38);
		bool L_39 = NamedVariable_get_IsNone_m281035543(L_38, /*hidden argument*/NULL);
		G_B15_0 = 5;
		G_B15_1 = L_37;
		G_B15_2 = L_37;
		G_B15_3 = G_B14_4;
		if (!L_39)
		{
			G_B19_0 = 5;
			G_B19_1 = L_37;
			G_B19_2 = L_37;
			G_B19_3 = G_B14_4;
			goto IL_0138;
		}
	}
	{
		FsmFloat_t2134102846 * L_40 = __this->get_time_21();
		NullCheck(L_40);
		bool L_41 = NamedVariable_get_IsNone_m281035543(L_40, /*hidden argument*/NULL);
		G_B16_0 = G_B15_0;
		G_B16_1 = G_B15_1;
		G_B16_2 = G_B15_2;
		G_B16_3 = G_B15_3;
		if (!L_41)
		{
			G_B17_0 = G_B15_0;
			G_B17_1 = G_B15_1;
			G_B17_2 = G_B15_2;
			G_B17_3 = G_B15_3;
			goto IL_0128;
		}
	}
	{
		G_B18_0 = (1.0f);
		G_B18_1 = G_B16_0;
		G_B18_2 = G_B16_1;
		G_B18_3 = G_B16_2;
		G_B18_4 = G_B16_3;
		goto IL_0133;
	}

IL_0128:
	{
		FsmFloat_t2134102846 * L_42 = __this->get_time_21();
		NullCheck(L_42);
		float L_43 = FsmFloat_get_Value_m4137923823(L_42, /*hidden argument*/NULL);
		G_B18_0 = L_43;
		G_B18_1 = G_B17_0;
		G_B18_2 = G_B17_1;
		G_B18_3 = G_B17_2;
		G_B18_4 = G_B17_3;
	}

IL_0133:
	{
		G_B20_0 = G_B18_0;
		G_B20_1 = G_B18_1;
		G_B20_2 = G_B18_2;
		G_B20_3 = G_B18_3;
		G_B20_4 = G_B18_4;
		goto IL_0143;
	}

IL_0138:
	{
		FsmFloat_t2134102846 * L_44 = __this->get_speed_23();
		NullCheck(L_44);
		float L_45 = FsmFloat_get_Value_m4137923823(L_44, /*hidden argument*/NULL);
		G_B20_0 = L_45;
		G_B20_1 = G_B19_0;
		G_B20_2 = G_B19_1;
		G_B20_3 = G_B19_2;
		G_B20_4 = G_B19_3;
	}

IL_0143:
	{
		float L_46 = G_B20_0;
		Il2CppObject * L_47 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_46);
		NullCheck(G_B20_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B20_2, G_B20_1);
		ArrayElementTypeCheck (G_B20_2, L_47);
		(G_B20_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B20_1), (Il2CppObject *)L_47);
		ObjectU5BU5D_t1108656482* L_48 = G_B20_3;
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, 6);
		ArrayElementTypeCheck (L_48, _stringLiteral95467907);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral95467907);
		ObjectU5BU5D_t1108656482* L_49 = L_48;
		FsmFloat_t2134102846 * L_50 = __this->get_delay_22();
		NullCheck(L_50);
		bool L_51 = NamedVariable_get_IsNone_m281035543(L_50, /*hidden argument*/NULL);
		G_B21_0 = 7;
		G_B21_1 = L_49;
		G_B21_2 = L_49;
		G_B21_3 = G_B20_4;
		if (!L_51)
		{
			G_B22_0 = 7;
			G_B22_1 = L_49;
			G_B22_2 = L_49;
			G_B22_3 = G_B20_4;
			goto IL_016d;
		}
	}
	{
		G_B23_0 = (0.0f);
		G_B23_1 = G_B21_0;
		G_B23_2 = G_B21_1;
		G_B23_3 = G_B21_2;
		G_B23_4 = G_B21_3;
		goto IL_0178;
	}

IL_016d:
	{
		FsmFloat_t2134102846 * L_52 = __this->get_delay_22();
		NullCheck(L_52);
		float L_53 = FsmFloat_get_Value_m4137923823(L_52, /*hidden argument*/NULL);
		G_B23_0 = L_53;
		G_B23_1 = G_B22_0;
		G_B23_2 = G_B22_1;
		G_B23_3 = G_B22_2;
		G_B23_4 = G_B22_3;
	}

IL_0178:
	{
		float L_54 = G_B23_0;
		Il2CppObject * L_55 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_54);
		NullCheck(G_B23_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B23_2, G_B23_1);
		ArrayElementTypeCheck (G_B23_2, L_55);
		(G_B23_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B23_1), (Il2CppObject *)L_55);
		ObjectU5BU5D_t1108656482* L_56 = G_B23_3;
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, 8);
		ArrayElementTypeCheck (L_56, _stringLiteral3507899432);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral3507899432);
		ObjectU5BU5D_t1108656482* L_57 = L_56;
		int32_t L_58 = __this->get_easeType_24();
		int32_t L_59 = L_58;
		Il2CppObject * L_60 = Box(EaseType_t2734598229_il2cpp_TypeInfo_var, &L_59);
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, ((int32_t)9));
		ArrayElementTypeCheck (L_57, L_60);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)L_60);
		ObjectU5BU5D_t1108656482* L_61 = L_57;
		NullCheck(L_61);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_61, ((int32_t)10));
		ArrayElementTypeCheck (L_61, _stringLiteral2258461662);
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Il2CppObject *)_stringLiteral2258461662);
		ObjectU5BU5D_t1108656482* L_62 = L_61;
		int32_t L_63 = __this->get_loopType_25();
		int32_t L_64 = L_63;
		Il2CppObject * L_65 = Box(LoopType_t1485160459_il2cpp_TypeInfo_var, &L_64);
		NullCheck(L_62);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_62, ((int32_t)11));
		ArrayElementTypeCheck (L_62, L_65);
		(L_62)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (Il2CppObject *)L_65);
		ObjectU5BU5D_t1108656482* L_66 = L_62;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, ((int32_t)12));
		ArrayElementTypeCheck (L_66, _stringLiteral2105864216);
		(L_66)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (Il2CppObject *)_stringLiteral2105864216);
		ObjectU5BU5D_t1108656482* L_67 = L_66;
		NullCheck(L_67);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_67, ((int32_t)13));
		ArrayElementTypeCheck (L_67, _stringLiteral338257242);
		(L_67)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (Il2CppObject *)_stringLiteral338257242);
		ObjectU5BU5D_t1108656482* L_68 = L_67;
		NullCheck(L_68);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_68, ((int32_t)14));
		ArrayElementTypeCheck (L_68, _stringLiteral3696326558);
		(L_68)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (Il2CppObject *)_stringLiteral3696326558);
		ObjectU5BU5D_t1108656482* L_69 = L_68;
		int32_t L_70 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_71 = L_70;
		Il2CppObject * L_72 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_71);
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, ((int32_t)15));
		ArrayElementTypeCheck (L_69, L_72);
		(L_69)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (Il2CppObject *)L_72);
		ObjectU5BU5D_t1108656482* L_73 = L_69;
		NullCheck(L_73);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_73, ((int32_t)16));
		ArrayElementTypeCheck (L_73, _stringLiteral2987624931);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (Il2CppObject *)_stringLiteral2987624931);
		ObjectU5BU5D_t1108656482* L_74 = L_73;
		NullCheck(L_74);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_74, ((int32_t)17));
		ArrayElementTypeCheck (L_74, _stringLiteral647687137);
		(L_74)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (Il2CppObject *)_stringLiteral647687137);
		ObjectU5BU5D_t1108656482* L_75 = L_74;
		NullCheck(L_75);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_75, ((int32_t)18));
		ArrayElementTypeCheck (L_75, _stringLiteral2052738345);
		(L_75)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (Il2CppObject *)_stringLiteral2052738345);
		ObjectU5BU5D_t1108656482* L_76 = L_75;
		int32_t L_77 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_78 = L_77;
		Il2CppObject * L_79 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_78);
		NullCheck(L_76);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_76, ((int32_t)19));
		ArrayElementTypeCheck (L_76, L_79);
		(L_76)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)19)), (Il2CppObject *)L_79);
		ObjectU5BU5D_t1108656482* L_80 = L_76;
		NullCheck(L_80);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_80, ((int32_t)20));
		ArrayElementTypeCheck (L_80, _stringLiteral1067506955);
		(L_80)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)20)), (Il2CppObject *)_stringLiteral1067506955);
		ObjectU5BU5D_t1108656482* L_81 = L_80;
		FsmBool_t1075959796 * L_82 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_82);
		bool L_83 = NamedVariable_get_IsNone_m281035543(L_82, /*hidden argument*/NULL);
		G_B24_0 = ((int32_t)21);
		G_B24_1 = L_81;
		G_B24_2 = L_81;
		G_B24_3 = G_B23_4;
		if (!L_83)
		{
			G_B25_0 = ((int32_t)21);
			G_B25_1 = L_81;
			G_B25_2 = L_81;
			G_B25_3 = G_B23_4;
			goto IL_0223;
		}
	}
	{
		G_B26_0 = 0;
		G_B26_1 = G_B24_0;
		G_B26_2 = G_B24_1;
		G_B26_3 = G_B24_2;
		G_B26_4 = G_B24_3;
		goto IL_022e;
	}

IL_0223:
	{
		FsmBool_t1075959796 * L_84 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_84);
		bool L_85 = FsmBool_get_Value_m3101329097(L_84, /*hidden argument*/NULL);
		G_B26_0 = ((int32_t)(L_85));
		G_B26_1 = G_B25_0;
		G_B26_2 = G_B25_1;
		G_B26_3 = G_B25_2;
		G_B26_4 = G_B25_3;
	}

IL_022e:
	{
		bool L_86 = ((bool)G_B26_0);
		Il2CppObject * L_87 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_86);
		NullCheck(G_B26_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B26_2, G_B26_1);
		ArrayElementTypeCheck (G_B26_2, L_87);
		(G_B26_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B26_1), (Il2CppObject *)L_87);
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		Hashtable_t1407064410 * L_88 = iTween_Hash_m2099495140(NULL /*static, unused*/, G_B26_3, /*hidden argument*/NULL);
		iTween_ScaleTo_m2499018538(NULL /*static, unused*/, G_B26_4, L_88, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleUpdate::.ctor()
extern "C"  void iTweenScaleUpdate__ctor_m2920227287 (iTweenScaleUpdate_t4241723967 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleUpdate::Reset()
extern Il2CppClass* FsmGameObject_t1697147867_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern const uint32_t iTweenScaleUpdate_Reset_m566660228_MetadataUsageId;
extern "C"  void iTweenScaleUpdate_Reset_m566660228 (iTweenScaleUpdate_t4241723967 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenScaleUpdate_Reset_m566660228_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmGameObject_t1697147867 * V_0 = NULL;
	FsmVector3_t533912882 * V_1 = NULL;
	{
		FsmGameObject_t1697147867 * L_0 = (FsmGameObject_t1697147867 *)il2cpp_codegen_object_new(FsmGameObject_t1697147867_il2cpp_TypeInfo_var);
		FsmGameObject__ctor_m1048269796(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmGameObject_t1697147867 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_2 = V_0;
		__this->set_transformScale_10(L_2);
		FsmVector3_t533912882 * L_3 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_3, /*hidden argument*/NULL);
		V_1 = L_3;
		FsmVector3_t533912882 * L_4 = V_1;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_5 = V_1;
		__this->set_vectorScale_11(L_5);
		FsmFloat_t2134102846 * L_6 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_time_12(L_6);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleUpdate::OnEnter()
extern Il2CppClass* Hashtable_t1407064410_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral109250890;
extern Il2CppCodeGenString* _stringLiteral3560141;
extern const uint32_t iTweenScaleUpdate_OnEnter_m1116507118_MetadataUsageId;
extern "C"  void iTweenScaleUpdate_OnEnter_m1116507118 (iTweenScaleUpdate_t4241723967 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenScaleUpdate_OnEnter_m1116507118_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B5_0 = NULL;
	Hashtable_t1407064410 * G_B5_1 = NULL;
	String_t* G_B4_0 = NULL;
	Hashtable_t1407064410 * G_B4_1 = NULL;
	Vector3_t4282066566  G_B6_0;
	memset(&G_B6_0, 0, sizeof(G_B6_0));
	String_t* G_B6_1 = NULL;
	Hashtable_t1407064410 * G_B6_2 = NULL;
	String_t* G_B12_0 = NULL;
	Hashtable_t1407064410 * G_B12_1 = NULL;
	String_t* G_B11_0 = NULL;
	Hashtable_t1407064410 * G_B11_1 = NULL;
	float G_B13_0 = 0.0f;
	String_t* G_B13_1 = NULL;
	Hashtable_t1407064410 * G_B13_2 = NULL;
	{
		Hashtable_t1407064410 * L_0 = (Hashtable_t1407064410 *)il2cpp_codegen_object_new(Hashtable_t1407064410_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1514037738(L_0, /*hidden argument*/NULL);
		__this->set_hash_13(L_0);
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_2 = __this->get_gameObject_9();
		NullCheck(L_1);
		GameObject_t3674682005 * L_3 = Fsm_GetOwnerDefaultTarget_m846013999(L_1, L_2, /*hidden argument*/NULL);
		__this->set_go_14(L_3);
		GameObject_t3674682005 * L_4 = __this->get_go_14();
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003a;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_003a:
	{
		FsmGameObject_t1697147867 * L_6 = __this->get_transformScale_10();
		NullCheck(L_6);
		bool L_7 = NamedVariable_get_IsNone_m281035543(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0089;
		}
	}
	{
		Hashtable_t1407064410 * L_8 = __this->get_hash_13();
		FsmVector3_t533912882 * L_9 = __this->get_vectorScale_11();
		NullCheck(L_9);
		bool L_10 = NamedVariable_get_IsNone_m281035543(L_9, /*hidden argument*/NULL);
		G_B4_0 = _stringLiteral109250890;
		G_B4_1 = L_8;
		if (!L_10)
		{
			G_B5_0 = _stringLiteral109250890;
			G_B5_1 = L_8;
			goto IL_006f;
		}
	}
	{
		Vector3_t4282066566  L_11 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		goto IL_007a;
	}

IL_006f:
	{
		FsmVector3_t533912882 * L_12 = __this->get_vectorScale_11();
		NullCheck(L_12);
		Vector3_t4282066566  L_13 = FsmVector3_get_Value_m2779135117(L_12, /*hidden argument*/NULL);
		G_B6_0 = L_13;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
	}

IL_007a:
	{
		Vector3_t4282066566  L_14 = G_B6_0;
		Il2CppObject * L_15 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_14);
		NullCheck(G_B6_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B6_2, G_B6_1, L_15);
		goto IL_00f8;
	}

IL_0089:
	{
		FsmVector3_t533912882 * L_16 = __this->get_vectorScale_11();
		NullCheck(L_16);
		bool L_17 = NamedVariable_get_IsNone_m281035543(L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00be;
		}
	}
	{
		Hashtable_t1407064410 * L_18 = __this->get_hash_13();
		FsmGameObject_t1697147867 * L_19 = __this->get_transformScale_10();
		NullCheck(L_19);
		GameObject_t3674682005 * L_20 = FsmGameObject_get_Value_m673294275(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_t1659122786 * L_21 = GameObject_get_transform_m1278640159(L_20, /*hidden argument*/NULL);
		NullCheck(L_18);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_18, _stringLiteral109250890, L_21);
		goto IL_00f8;
	}

IL_00be:
	{
		Hashtable_t1407064410 * L_22 = __this->get_hash_13();
		FsmGameObject_t1697147867 * L_23 = __this->get_transformScale_10();
		NullCheck(L_23);
		GameObject_t3674682005 * L_24 = FsmGameObject_get_Value_m673294275(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_t1659122786 * L_25 = GameObject_get_transform_m1278640159(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		Vector3_t4282066566  L_26 = Transform_get_localScale_m3886572677(L_25, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_27 = __this->get_vectorScale_11();
		NullCheck(L_27);
		Vector3_t4282066566  L_28 = FsmVector3_get_Value_m2779135117(L_27, /*hidden argument*/NULL);
		Vector3_t4282066566  L_29 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_26, L_28, /*hidden argument*/NULL);
		Vector3_t4282066566  L_30 = L_29;
		Il2CppObject * L_31 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_30);
		NullCheck(L_22);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_22, _stringLiteral109250890, L_31);
	}

IL_00f8:
	{
		Hashtable_t1407064410 * L_32 = __this->get_hash_13();
		FsmFloat_t2134102846 * L_33 = __this->get_time_12();
		NullCheck(L_33);
		bool L_34 = NamedVariable_get_IsNone_m281035543(L_33, /*hidden argument*/NULL);
		G_B11_0 = _stringLiteral3560141;
		G_B11_1 = L_32;
		if (!L_34)
		{
			G_B12_0 = _stringLiteral3560141;
			G_B12_1 = L_32;
			goto IL_011d;
		}
	}
	{
		G_B13_0 = (1.0f);
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		goto IL_0128;
	}

IL_011d:
	{
		FsmFloat_t2134102846 * L_35 = __this->get_time_12();
		NullCheck(L_35);
		float L_36 = FsmFloat_get_Value_m4137923823(L_35, /*hidden argument*/NULL);
		G_B13_0 = L_36;
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
	}

IL_0128:
	{
		float L_37 = G_B13_0;
		Il2CppObject * L_38 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_37);
		NullCheck(G_B13_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B13_2, G_B13_1, L_38);
		iTweenScaleUpdate_DoiTween_m1703114522(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleUpdate::OnExit()
extern "C"  void iTweenScaleUpdate_OnExit_m1846051306 (iTweenScaleUpdate_t4241723967 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleUpdate::OnUpdate()
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral109250890;
extern const uint32_t iTweenScaleUpdate_OnUpdate_m3680508949_MetadataUsageId;
extern "C"  void iTweenScaleUpdate_OnUpdate_m3680508949 (iTweenScaleUpdate_t4241723967 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenScaleUpdate_OnUpdate_m3680508949_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	Hashtable_t1407064410 * G_B3_1 = NULL;
	String_t* G_B2_0 = NULL;
	Hashtable_t1407064410 * G_B2_1 = NULL;
	Vector3_t4282066566  G_B4_0;
	memset(&G_B4_0, 0, sizeof(G_B4_0));
	String_t* G_B4_1 = NULL;
	Hashtable_t1407064410 * G_B4_2 = NULL;
	{
		Hashtable_t1407064410 * L_0 = __this->get_hash_13();
		NullCheck(L_0);
		VirtActionInvoker1< Il2CppObject * >::Invoke(37 /* System.Void System.Collections.Hashtable::Remove(System.Object) */, L_0, _stringLiteral109250890);
		FsmGameObject_t1697147867 * L_1 = __this->get_transformScale_10();
		NullCheck(L_1);
		bool L_2 = NamedVariable_get_IsNone_m281035543(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_005f;
		}
	}
	{
		Hashtable_t1407064410 * L_3 = __this->get_hash_13();
		FsmVector3_t533912882 * L_4 = __this->get_vectorScale_11();
		NullCheck(L_4);
		bool L_5 = NamedVariable_get_IsNone_m281035543(L_4, /*hidden argument*/NULL);
		G_B2_0 = _stringLiteral109250890;
		G_B2_1 = L_3;
		if (!L_5)
		{
			G_B3_0 = _stringLiteral109250890;
			G_B3_1 = L_3;
			goto IL_0045;
		}
	}
	{
		Vector3_t4282066566  L_6 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B4_0 = L_6;
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		goto IL_0050;
	}

IL_0045:
	{
		FsmVector3_t533912882 * L_7 = __this->get_vectorScale_11();
		NullCheck(L_7);
		Vector3_t4282066566  L_8 = FsmVector3_get_Value_m2779135117(L_7, /*hidden argument*/NULL);
		G_B4_0 = L_8;
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
	}

IL_0050:
	{
		Vector3_t4282066566  L_9 = G_B4_0;
		Il2CppObject * L_10 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_9);
		NullCheck(G_B4_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, G_B4_2, G_B4_1, L_10);
		goto IL_00ce;
	}

IL_005f:
	{
		FsmVector3_t533912882 * L_11 = __this->get_vectorScale_11();
		NullCheck(L_11);
		bool L_12 = NamedVariable_get_IsNone_m281035543(L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0094;
		}
	}
	{
		Hashtable_t1407064410 * L_13 = __this->get_hash_13();
		FsmGameObject_t1697147867 * L_14 = __this->get_transformScale_10();
		NullCheck(L_14);
		GameObject_t3674682005 * L_15 = FsmGameObject_get_Value_m673294275(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_t1659122786 * L_16 = GameObject_get_transform_m1278640159(L_15, /*hidden argument*/NULL);
		NullCheck(L_13);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_13, _stringLiteral109250890, L_16);
		goto IL_00ce;
	}

IL_0094:
	{
		Hashtable_t1407064410 * L_17 = __this->get_hash_13();
		FsmGameObject_t1697147867 * L_18 = __this->get_transformScale_10();
		NullCheck(L_18);
		GameObject_t3674682005 * L_19 = FsmGameObject_get_Value_m673294275(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Transform_t1659122786 * L_20 = GameObject_get_transform_m1278640159(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		Vector3_t4282066566  L_21 = Transform_get_localScale_m3886572677(L_20, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_22 = __this->get_vectorScale_11();
		NullCheck(L_22);
		Vector3_t4282066566  L_23 = FsmVector3_get_Value_m2779135117(L_22, /*hidden argument*/NULL);
		Vector3_t4282066566  L_24 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_21, L_23, /*hidden argument*/NULL);
		Vector3_t4282066566  L_25 = L_24;
		Il2CppObject * L_26 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_25);
		NullCheck(L_17);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_17, _stringLiteral109250890, L_26);
	}

IL_00ce:
	{
		iTweenScaleUpdate_DoiTween_m1703114522(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleUpdate::DoiTween()
extern Il2CppClass* iTween_t3087282050_il2cpp_TypeInfo_var;
extern const uint32_t iTweenScaleUpdate_DoiTween_m1703114522_MetadataUsageId;
extern "C"  void iTweenScaleUpdate_DoiTween_m1703114522 (iTweenScaleUpdate_t4241723967 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenScaleUpdate_DoiTween_m1703114522_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_go_14();
		Hashtable_t1407064410 * L_1 = __this->get_hash_13();
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		iTween_ScaleUpdate_m138729884(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenShakePosition::.ctor()
extern "C"  void iTweenShakePosition__ctor_m201690107 (iTweenShakePosition_t1348015515 * __this, const MethodInfo* method)
{
	{
		iTweenFsmAction__ctor_m2960815636(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenShakePosition::Reset()
extern Il2CppClass* FsmString_t952858651_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern const uint32_t iTweenShakePosition_Reset_m2143090344_MetadataUsageId;
extern "C"  void iTweenShakePosition_Reset_m2143090344 (iTweenShakePosition_t1348015515 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenShakePosition_Reset_m2143090344_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmString_t952858651 * V_0 = NULL;
	FsmVector3_t533912882 * V_1 = NULL;
	{
		iTweenFsmAction_Reset_m607248577(__this, /*hidden argument*/NULL);
		FsmString_t952858651 * L_0 = (FsmString_t952858651 *)il2cpp_codegen_object_new(FsmString_t952858651_il2cpp_TypeInfo_var);
		FsmString__ctor_m2679108260(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmString_t952858651 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmString_t952858651 * L_2 = V_0;
		__this->set_id_18(L_2);
		FsmFloat_t2134102846 * L_3 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_time_20(L_3);
		FsmFloat_t2134102846 * L_4 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_delay_21(L_4);
		__this->set_loopType_22(0);
		FsmVector3_t533912882 * L_5 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_5, /*hidden argument*/NULL);
		V_1 = L_5;
		FsmVector3_t533912882 * L_6 = V_1;
		NullCheck(L_6);
		NamedVariable_set_UseVariable_m4266138971(L_6, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_7 = V_1;
		__this->set_vector_19(L_7);
		__this->set_space_23(0);
		__this->set_axis_24(0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenShakePosition::OnEnter()
extern "C"  void iTweenShakePosition_OnEnter_m4237360402 (iTweenShakePosition_t1348015515 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnEnteriTween_m3024539459(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_loopType_22();
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		iTweenFsmAction_IsLoop_m2539277653(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_001e:
	{
		iTweenShakePosition_DoiTween_m3960285814(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenShakePosition::OnExit()
extern "C"  void iTweenShakePosition_OnExit_m3470744646 (iTweenShakePosition_t1348015515 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnExitiTween_m1981625761(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenShakePosition::DoiTween()
extern const Il2CppType* AxisRestriction_t3260241011_0_0_0_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* LoopType_t1485160459_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* Space_t4209342076_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* AxisRestriction_t3260241011_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2862688501_il2cpp_TypeInfo_var;
extern Il2CppClass* iTween_t3087282050_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral109399814;
extern Il2CppCodeGenString* _stringLiteral2881114200;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral3560141;
extern Il2CppCodeGenString* _stringLiteral95467907;
extern Il2CppCodeGenString* _stringLiteral2258461662;
extern Il2CppCodeGenString* _stringLiteral2105864216;
extern Il2CppCodeGenString* _stringLiteral338257242;
extern Il2CppCodeGenString* _stringLiteral3696326558;
extern Il2CppCodeGenString* _stringLiteral2987624931;
extern Il2CppCodeGenString* _stringLiteral647687137;
extern Il2CppCodeGenString* _stringLiteral2052738345;
extern Il2CppCodeGenString* _stringLiteral1067506955;
extern Il2CppCodeGenString* _stringLiteral109637894;
extern Il2CppCodeGenString* _stringLiteral3008417;
extern const uint32_t iTweenShakePosition_DoiTween_m3960285814_MetadataUsageId;
extern "C"  void iTweenShakePosition_DoiTween_m3960285814 (iTweenShakePosition_t1348015515 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenShakePosition_DoiTween_m3960285814_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B7_0 = 0;
	ObjectU5BU5D_t1108656482* G_B7_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B7_2 = NULL;
	GameObject_t3674682005 * G_B7_3 = NULL;
	int32_t G_B6_0 = 0;
	ObjectU5BU5D_t1108656482* G_B6_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B6_2 = NULL;
	GameObject_t3674682005 * G_B6_3 = NULL;
	String_t* G_B8_0 = NULL;
	int32_t G_B8_1 = 0;
	ObjectU5BU5D_t1108656482* G_B8_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B8_3 = NULL;
	GameObject_t3674682005 * G_B8_4 = NULL;
	int32_t G_B10_0 = 0;
	ObjectU5BU5D_t1108656482* G_B10_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B10_2 = NULL;
	GameObject_t3674682005 * G_B10_3 = NULL;
	int32_t G_B9_0 = 0;
	ObjectU5BU5D_t1108656482* G_B9_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B9_2 = NULL;
	GameObject_t3674682005 * G_B9_3 = NULL;
	float G_B11_0 = 0.0f;
	int32_t G_B11_1 = 0;
	ObjectU5BU5D_t1108656482* G_B11_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B11_3 = NULL;
	GameObject_t3674682005 * G_B11_4 = NULL;
	int32_t G_B13_0 = 0;
	ObjectU5BU5D_t1108656482* G_B13_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B13_2 = NULL;
	GameObject_t3674682005 * G_B13_3 = NULL;
	int32_t G_B12_0 = 0;
	ObjectU5BU5D_t1108656482* G_B12_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B12_2 = NULL;
	GameObject_t3674682005 * G_B12_3 = NULL;
	float G_B14_0 = 0.0f;
	int32_t G_B14_1 = 0;
	ObjectU5BU5D_t1108656482* G_B14_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B14_3 = NULL;
	GameObject_t3674682005 * G_B14_4 = NULL;
	int32_t G_B16_0 = 0;
	ObjectU5BU5D_t1108656482* G_B16_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B16_2 = NULL;
	GameObject_t3674682005 * G_B16_3 = NULL;
	int32_t G_B15_0 = 0;
	ObjectU5BU5D_t1108656482* G_B15_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B15_2 = NULL;
	GameObject_t3674682005 * G_B15_3 = NULL;
	int32_t G_B17_0 = 0;
	int32_t G_B17_1 = 0;
	ObjectU5BU5D_t1108656482* G_B17_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B17_3 = NULL;
	GameObject_t3674682005 * G_B17_4 = NULL;
	int32_t G_B19_0 = 0;
	ObjectU5BU5D_t1108656482* G_B19_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B19_2 = NULL;
	GameObject_t3674682005 * G_B19_3 = NULL;
	int32_t G_B18_0 = 0;
	ObjectU5BU5D_t1108656482* G_B18_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B18_2 = NULL;
	GameObject_t3674682005 * G_B18_3 = NULL;
	String_t* G_B20_0 = NULL;
	int32_t G_B20_1 = 0;
	ObjectU5BU5D_t1108656482* G_B20_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B20_3 = NULL;
	GameObject_t3674682005 * G_B20_4 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_17();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		Vector3_t4282066566  L_5 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_5;
		FsmVector3_t533912882 * L_6 = __this->get_vector_19();
		NullCheck(L_6);
		bool L_7 = NamedVariable_get_IsNone_m281035543(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003a;
		}
	}
	{
		goto IL_0046;
	}

IL_003a:
	{
		FsmVector3_t533912882 * L_8 = __this->get_vector_19();
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = FsmVector3_get_Value_m2779135117(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
	}

IL_0046:
	{
		((iTweenFsmAction_t410382178 *)__this)->set_itweenType_15(_stringLiteral109399814);
		GameObject_t3674682005 * L_10 = V_0;
		ObjectU5BU5D_t1108656482* L_11 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)24)));
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		ArrayElementTypeCheck (L_11, _stringLiteral2881114200);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2881114200);
		ObjectU5BU5D_t1108656482* L_12 = L_11;
		Vector3_t4282066566  L_13 = V_1;
		Vector3_t4282066566  L_14 = L_13;
		Il2CppObject * L_15 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 1);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_15);
		ObjectU5BU5D_t1108656482* L_16 = L_12;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 2);
		ArrayElementTypeCheck (L_16, _stringLiteral3373707);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3373707);
		ObjectU5BU5D_t1108656482* L_17 = L_16;
		FsmString_t952858651 * L_18 = __this->get_id_18();
		NullCheck(L_18);
		bool L_19 = NamedVariable_get_IsNone_m281035543(L_18, /*hidden argument*/NULL);
		G_B6_0 = 3;
		G_B6_1 = L_17;
		G_B6_2 = L_17;
		G_B6_3 = L_10;
		if (!L_19)
		{
			G_B7_0 = 3;
			G_B7_1 = L_17;
			G_B7_2 = L_17;
			G_B7_3 = L_10;
			goto IL_008e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B8_0 = L_20;
		G_B8_1 = G_B6_0;
		G_B8_2 = G_B6_1;
		G_B8_3 = G_B6_2;
		G_B8_4 = G_B6_3;
		goto IL_0099;
	}

IL_008e:
	{
		FsmString_t952858651 * L_21 = __this->get_id_18();
		NullCheck(L_21);
		String_t* L_22 = FsmString_get_Value_m872383149(L_21, /*hidden argument*/NULL);
		G_B8_0 = L_22;
		G_B8_1 = G_B7_0;
		G_B8_2 = G_B7_1;
		G_B8_3 = G_B7_2;
		G_B8_4 = G_B7_3;
	}

IL_0099:
	{
		NullCheck(G_B8_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B8_2, G_B8_1);
		ArrayElementTypeCheck (G_B8_2, G_B8_0);
		(G_B8_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B8_1), (Il2CppObject *)G_B8_0);
		ObjectU5BU5D_t1108656482* L_23 = G_B8_3;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 4);
		ArrayElementTypeCheck (L_23, _stringLiteral3560141);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral3560141);
		ObjectU5BU5D_t1108656482* L_24 = L_23;
		FsmFloat_t2134102846 * L_25 = __this->get_time_20();
		NullCheck(L_25);
		bool L_26 = NamedVariable_get_IsNone_m281035543(L_25, /*hidden argument*/NULL);
		G_B9_0 = 5;
		G_B9_1 = L_24;
		G_B9_2 = L_24;
		G_B9_3 = G_B8_4;
		if (!L_26)
		{
			G_B10_0 = 5;
			G_B10_1 = L_24;
			G_B10_2 = L_24;
			G_B10_3 = G_B8_4;
			goto IL_00be;
		}
	}
	{
		G_B11_0 = (1.0f);
		G_B11_1 = G_B9_0;
		G_B11_2 = G_B9_1;
		G_B11_3 = G_B9_2;
		G_B11_4 = G_B9_3;
		goto IL_00c9;
	}

IL_00be:
	{
		FsmFloat_t2134102846 * L_27 = __this->get_time_20();
		NullCheck(L_27);
		float L_28 = FsmFloat_get_Value_m4137923823(L_27, /*hidden argument*/NULL);
		G_B11_0 = L_28;
		G_B11_1 = G_B10_0;
		G_B11_2 = G_B10_1;
		G_B11_3 = G_B10_2;
		G_B11_4 = G_B10_3;
	}

IL_00c9:
	{
		float L_29 = G_B11_0;
		Il2CppObject * L_30 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_29);
		NullCheck(G_B11_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B11_2, G_B11_1);
		ArrayElementTypeCheck (G_B11_2, L_30);
		(G_B11_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B11_1), (Il2CppObject *)L_30);
		ObjectU5BU5D_t1108656482* L_31 = G_B11_3;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 6);
		ArrayElementTypeCheck (L_31, _stringLiteral95467907);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral95467907);
		ObjectU5BU5D_t1108656482* L_32 = L_31;
		FsmFloat_t2134102846 * L_33 = __this->get_delay_21();
		NullCheck(L_33);
		bool L_34 = NamedVariable_get_IsNone_m281035543(L_33, /*hidden argument*/NULL);
		G_B12_0 = 7;
		G_B12_1 = L_32;
		G_B12_2 = L_32;
		G_B12_3 = G_B11_4;
		if (!L_34)
		{
			G_B13_0 = 7;
			G_B13_1 = L_32;
			G_B13_2 = L_32;
			G_B13_3 = G_B11_4;
			goto IL_00f3;
		}
	}
	{
		G_B14_0 = (0.0f);
		G_B14_1 = G_B12_0;
		G_B14_2 = G_B12_1;
		G_B14_3 = G_B12_2;
		G_B14_4 = G_B12_3;
		goto IL_00fe;
	}

IL_00f3:
	{
		FsmFloat_t2134102846 * L_35 = __this->get_delay_21();
		NullCheck(L_35);
		float L_36 = FsmFloat_get_Value_m4137923823(L_35, /*hidden argument*/NULL);
		G_B14_0 = L_36;
		G_B14_1 = G_B13_0;
		G_B14_2 = G_B13_1;
		G_B14_3 = G_B13_2;
		G_B14_4 = G_B13_3;
	}

IL_00fe:
	{
		float L_37 = G_B14_0;
		Il2CppObject * L_38 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_37);
		NullCheck(G_B14_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B14_2, G_B14_1);
		ArrayElementTypeCheck (G_B14_2, L_38);
		(G_B14_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B14_1), (Il2CppObject *)L_38);
		ObjectU5BU5D_t1108656482* L_39 = G_B14_3;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, 8);
		ArrayElementTypeCheck (L_39, _stringLiteral2258461662);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral2258461662);
		ObjectU5BU5D_t1108656482* L_40 = L_39;
		int32_t L_41 = __this->get_loopType_22();
		int32_t L_42 = L_41;
		Il2CppObject * L_43 = Box(LoopType_t1485160459_il2cpp_TypeInfo_var, &L_42);
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, ((int32_t)9));
		ArrayElementTypeCheck (L_40, L_43);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)L_43);
		ObjectU5BU5D_t1108656482* L_44 = L_40;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)10));
		ArrayElementTypeCheck (L_44, _stringLiteral2105864216);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Il2CppObject *)_stringLiteral2105864216);
		ObjectU5BU5D_t1108656482* L_45 = L_44;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, ((int32_t)11));
		ArrayElementTypeCheck (L_45, _stringLiteral338257242);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (Il2CppObject *)_stringLiteral338257242);
		ObjectU5BU5D_t1108656482* L_46 = L_45;
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, ((int32_t)12));
		ArrayElementTypeCheck (L_46, _stringLiteral3696326558);
		(L_46)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (Il2CppObject *)_stringLiteral3696326558);
		ObjectU5BU5D_t1108656482* L_47 = L_46;
		int32_t L_48 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_49 = L_48;
		Il2CppObject * L_50 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_49);
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, ((int32_t)13));
		ArrayElementTypeCheck (L_47, L_50);
		(L_47)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (Il2CppObject *)L_50);
		ObjectU5BU5D_t1108656482* L_51 = L_47;
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, ((int32_t)14));
		ArrayElementTypeCheck (L_51, _stringLiteral2987624931);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (Il2CppObject *)_stringLiteral2987624931);
		ObjectU5BU5D_t1108656482* L_52 = L_51;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, ((int32_t)15));
		ArrayElementTypeCheck (L_52, _stringLiteral647687137);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (Il2CppObject *)_stringLiteral647687137);
		ObjectU5BU5D_t1108656482* L_53 = L_52;
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, ((int32_t)16));
		ArrayElementTypeCheck (L_53, _stringLiteral2052738345);
		(L_53)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (Il2CppObject *)_stringLiteral2052738345);
		ObjectU5BU5D_t1108656482* L_54 = L_53;
		int32_t L_55 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_56 = L_55;
		Il2CppObject * L_57 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_56);
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, ((int32_t)17));
		ArrayElementTypeCheck (L_54, L_57);
		(L_54)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (Il2CppObject *)L_57);
		ObjectU5BU5D_t1108656482* L_58 = L_54;
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, ((int32_t)18));
		ArrayElementTypeCheck (L_58, _stringLiteral1067506955);
		(L_58)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (Il2CppObject *)_stringLiteral1067506955);
		ObjectU5BU5D_t1108656482* L_59 = L_58;
		FsmBool_t1075959796 * L_60 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_60);
		bool L_61 = NamedVariable_get_IsNone_m281035543(L_60, /*hidden argument*/NULL);
		G_B15_0 = ((int32_t)19);
		G_B15_1 = L_59;
		G_B15_2 = L_59;
		G_B15_3 = G_B14_4;
		if (!L_61)
		{
			G_B16_0 = ((int32_t)19);
			G_B16_1 = L_59;
			G_B16_2 = L_59;
			G_B16_3 = G_B14_4;
			goto IL_0191;
		}
	}
	{
		G_B17_0 = 0;
		G_B17_1 = G_B15_0;
		G_B17_2 = G_B15_1;
		G_B17_3 = G_B15_2;
		G_B17_4 = G_B15_3;
		goto IL_019c;
	}

IL_0191:
	{
		FsmBool_t1075959796 * L_62 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_62);
		bool L_63 = FsmBool_get_Value_m3101329097(L_62, /*hidden argument*/NULL);
		G_B17_0 = ((int32_t)(L_63));
		G_B17_1 = G_B16_0;
		G_B17_2 = G_B16_1;
		G_B17_3 = G_B16_2;
		G_B17_4 = G_B16_3;
	}

IL_019c:
	{
		bool L_64 = ((bool)G_B17_0);
		Il2CppObject * L_65 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_64);
		NullCheck(G_B17_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B17_2, G_B17_1);
		ArrayElementTypeCheck (G_B17_2, L_65);
		(G_B17_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B17_1), (Il2CppObject *)L_65);
		ObjectU5BU5D_t1108656482* L_66 = G_B17_3;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, ((int32_t)20));
		ArrayElementTypeCheck (L_66, _stringLiteral109637894);
		(L_66)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)20)), (Il2CppObject *)_stringLiteral109637894);
		ObjectU5BU5D_t1108656482* L_67 = L_66;
		int32_t L_68 = __this->get_space_23();
		int32_t L_69 = L_68;
		Il2CppObject * L_70 = Box(Space_t4209342076_il2cpp_TypeInfo_var, &L_69);
		NullCheck(L_67);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_67, ((int32_t)21));
		ArrayElementTypeCheck (L_67, L_70);
		(L_67)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)21)), (Il2CppObject *)L_70);
		ObjectU5BU5D_t1108656482* L_71 = L_67;
		NullCheck(L_71);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_71, ((int32_t)22));
		ArrayElementTypeCheck (L_71, _stringLiteral3008417);
		(L_71)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)22)), (Il2CppObject *)_stringLiteral3008417);
		ObjectU5BU5D_t1108656482* L_72 = L_71;
		int32_t L_73 = __this->get_axis_24();
		G_B18_0 = ((int32_t)23);
		G_B18_1 = L_72;
		G_B18_2 = L_72;
		G_B18_3 = G_B17_4;
		if (L_73)
		{
			G_B19_0 = ((int32_t)23);
			G_B19_1 = L_72;
			G_B19_2 = L_72;
			G_B19_3 = G_B17_4;
			goto IL_01db;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_74 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B20_0 = L_74;
		G_B20_1 = G_B18_0;
		G_B20_2 = G_B18_1;
		G_B20_3 = G_B18_2;
		G_B20_4 = G_B18_3;
		goto IL_01f5;
	}

IL_01db:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_75 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(AxisRestriction_t3260241011_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_76 = __this->get_axis_24();
		int32_t L_77 = L_76;
		Il2CppObject * L_78 = Box(AxisRestriction_t3260241011_il2cpp_TypeInfo_var, &L_77);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		String_t* L_79 = Enum_GetName_m3333121265(NULL /*static, unused*/, L_75, L_78, /*hidden argument*/NULL);
		G_B20_0 = L_79;
		G_B20_1 = G_B19_0;
		G_B20_2 = G_B19_1;
		G_B20_3 = G_B19_2;
		G_B20_4 = G_B19_3;
	}

IL_01f5:
	{
		NullCheck(G_B20_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B20_2, G_B20_1);
		ArrayElementTypeCheck (G_B20_2, G_B20_0);
		(G_B20_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B20_1), (Il2CppObject *)G_B20_0);
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		Hashtable_t1407064410 * L_80 = iTween_Hash_m2099495140(NULL /*static, unused*/, G_B20_3, /*hidden argument*/NULL);
		iTween_ShakePosition_m3691503872(NULL /*static, unused*/, G_B20_4, L_80, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenShakeRotation::.ctor()
extern "C"  void iTweenShakeRotation__ctor_m4165363014 (iTweenShakeRotation_t559909872 * __this, const MethodInfo* method)
{
	{
		iTweenFsmAction__ctor_m2960815636(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenShakeRotation::Reset()
extern Il2CppClass* FsmString_t952858651_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern const uint32_t iTweenShakeRotation_Reset_m1811795955_MetadataUsageId;
extern "C"  void iTweenShakeRotation_Reset_m1811795955 (iTweenShakeRotation_t559909872 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenShakeRotation_Reset_m1811795955_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmString_t952858651 * V_0 = NULL;
	FsmVector3_t533912882 * V_1 = NULL;
	{
		iTweenFsmAction_Reset_m607248577(__this, /*hidden argument*/NULL);
		FsmString_t952858651 * L_0 = (FsmString_t952858651 *)il2cpp_codegen_object_new(FsmString_t952858651_il2cpp_TypeInfo_var);
		FsmString__ctor_m2679108260(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmString_t952858651 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmString_t952858651 * L_2 = V_0;
		__this->set_id_18(L_2);
		FsmFloat_t2134102846 * L_3 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_time_20(L_3);
		FsmFloat_t2134102846 * L_4 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_delay_21(L_4);
		__this->set_loopType_22(0);
		FsmVector3_t533912882 * L_5 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_5, /*hidden argument*/NULL);
		V_1 = L_5;
		FsmVector3_t533912882 * L_6 = V_1;
		NullCheck(L_6);
		NamedVariable_set_UseVariable_m4266138971(L_6, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_7 = V_1;
		__this->set_vector_19(L_7);
		__this->set_space_23(0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenShakeRotation::OnEnter()
extern "C"  void iTweenShakeRotation_OnEnter_m3691032477 (iTweenShakeRotation_t559909872 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnEnteriTween_m3024539459(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_loopType_22();
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		iTweenFsmAction_IsLoop_m2539277653(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_001e:
	{
		iTweenShakeRotation_DoiTween_m4203989323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenShakeRotation::OnExit()
extern "C"  void iTweenShakeRotation_OnExit_m1790553179 (iTweenShakeRotation_t559909872 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnExitiTween_m1981625761(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenShakeRotation::DoiTween()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* LoopType_t1485160459_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* Space_t4209342076_il2cpp_TypeInfo_var;
extern Il2CppClass* iTween_t3087282050_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral109399814;
extern Il2CppCodeGenString* _stringLiteral2881114200;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral3560141;
extern Il2CppCodeGenString* _stringLiteral95467907;
extern Il2CppCodeGenString* _stringLiteral2258461662;
extern Il2CppCodeGenString* _stringLiteral2105864216;
extern Il2CppCodeGenString* _stringLiteral338257242;
extern Il2CppCodeGenString* _stringLiteral3696326558;
extern Il2CppCodeGenString* _stringLiteral2987624931;
extern Il2CppCodeGenString* _stringLiteral647687137;
extern Il2CppCodeGenString* _stringLiteral2052738345;
extern Il2CppCodeGenString* _stringLiteral1067506955;
extern Il2CppCodeGenString* _stringLiteral109637894;
extern const uint32_t iTweenShakeRotation_DoiTween_m4203989323_MetadataUsageId;
extern "C"  void iTweenShakeRotation_DoiTween_m4203989323 (iTweenShakeRotation_t559909872 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenShakeRotation_DoiTween_m4203989323_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B7_0 = 0;
	ObjectU5BU5D_t1108656482* G_B7_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B7_2 = NULL;
	GameObject_t3674682005 * G_B7_3 = NULL;
	int32_t G_B6_0 = 0;
	ObjectU5BU5D_t1108656482* G_B6_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B6_2 = NULL;
	GameObject_t3674682005 * G_B6_3 = NULL;
	String_t* G_B8_0 = NULL;
	int32_t G_B8_1 = 0;
	ObjectU5BU5D_t1108656482* G_B8_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B8_3 = NULL;
	GameObject_t3674682005 * G_B8_4 = NULL;
	int32_t G_B10_0 = 0;
	ObjectU5BU5D_t1108656482* G_B10_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B10_2 = NULL;
	GameObject_t3674682005 * G_B10_3 = NULL;
	int32_t G_B9_0 = 0;
	ObjectU5BU5D_t1108656482* G_B9_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B9_2 = NULL;
	GameObject_t3674682005 * G_B9_3 = NULL;
	float G_B11_0 = 0.0f;
	int32_t G_B11_1 = 0;
	ObjectU5BU5D_t1108656482* G_B11_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B11_3 = NULL;
	GameObject_t3674682005 * G_B11_4 = NULL;
	int32_t G_B13_0 = 0;
	ObjectU5BU5D_t1108656482* G_B13_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B13_2 = NULL;
	GameObject_t3674682005 * G_B13_3 = NULL;
	int32_t G_B12_0 = 0;
	ObjectU5BU5D_t1108656482* G_B12_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B12_2 = NULL;
	GameObject_t3674682005 * G_B12_3 = NULL;
	float G_B14_0 = 0.0f;
	int32_t G_B14_1 = 0;
	ObjectU5BU5D_t1108656482* G_B14_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B14_3 = NULL;
	GameObject_t3674682005 * G_B14_4 = NULL;
	int32_t G_B16_0 = 0;
	ObjectU5BU5D_t1108656482* G_B16_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B16_2 = NULL;
	GameObject_t3674682005 * G_B16_3 = NULL;
	int32_t G_B15_0 = 0;
	ObjectU5BU5D_t1108656482* G_B15_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B15_2 = NULL;
	GameObject_t3674682005 * G_B15_3 = NULL;
	int32_t G_B17_0 = 0;
	int32_t G_B17_1 = 0;
	ObjectU5BU5D_t1108656482* G_B17_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B17_3 = NULL;
	GameObject_t3674682005 * G_B17_4 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_17();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		Vector3_t4282066566  L_5 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_5;
		FsmVector3_t533912882 * L_6 = __this->get_vector_19();
		NullCheck(L_6);
		bool L_7 = NamedVariable_get_IsNone_m281035543(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003a;
		}
	}
	{
		goto IL_0046;
	}

IL_003a:
	{
		FsmVector3_t533912882 * L_8 = __this->get_vector_19();
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = FsmVector3_get_Value_m2779135117(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
	}

IL_0046:
	{
		((iTweenFsmAction_t410382178 *)__this)->set_itweenType_15(_stringLiteral109399814);
		GameObject_t3674682005 * L_10 = V_0;
		ObjectU5BU5D_t1108656482* L_11 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)22)));
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		ArrayElementTypeCheck (L_11, _stringLiteral2881114200);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2881114200);
		ObjectU5BU5D_t1108656482* L_12 = L_11;
		Vector3_t4282066566  L_13 = V_1;
		Vector3_t4282066566  L_14 = L_13;
		Il2CppObject * L_15 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 1);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_15);
		ObjectU5BU5D_t1108656482* L_16 = L_12;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 2);
		ArrayElementTypeCheck (L_16, _stringLiteral3373707);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3373707);
		ObjectU5BU5D_t1108656482* L_17 = L_16;
		FsmString_t952858651 * L_18 = __this->get_id_18();
		NullCheck(L_18);
		bool L_19 = NamedVariable_get_IsNone_m281035543(L_18, /*hidden argument*/NULL);
		G_B6_0 = 3;
		G_B6_1 = L_17;
		G_B6_2 = L_17;
		G_B6_3 = L_10;
		if (!L_19)
		{
			G_B7_0 = 3;
			G_B7_1 = L_17;
			G_B7_2 = L_17;
			G_B7_3 = L_10;
			goto IL_008e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B8_0 = L_20;
		G_B8_1 = G_B6_0;
		G_B8_2 = G_B6_1;
		G_B8_3 = G_B6_2;
		G_B8_4 = G_B6_3;
		goto IL_0099;
	}

IL_008e:
	{
		FsmString_t952858651 * L_21 = __this->get_id_18();
		NullCheck(L_21);
		String_t* L_22 = FsmString_get_Value_m872383149(L_21, /*hidden argument*/NULL);
		G_B8_0 = L_22;
		G_B8_1 = G_B7_0;
		G_B8_2 = G_B7_1;
		G_B8_3 = G_B7_2;
		G_B8_4 = G_B7_3;
	}

IL_0099:
	{
		NullCheck(G_B8_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B8_2, G_B8_1);
		ArrayElementTypeCheck (G_B8_2, G_B8_0);
		(G_B8_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B8_1), (Il2CppObject *)G_B8_0);
		ObjectU5BU5D_t1108656482* L_23 = G_B8_3;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 4);
		ArrayElementTypeCheck (L_23, _stringLiteral3560141);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral3560141);
		ObjectU5BU5D_t1108656482* L_24 = L_23;
		FsmFloat_t2134102846 * L_25 = __this->get_time_20();
		NullCheck(L_25);
		bool L_26 = NamedVariable_get_IsNone_m281035543(L_25, /*hidden argument*/NULL);
		G_B9_0 = 5;
		G_B9_1 = L_24;
		G_B9_2 = L_24;
		G_B9_3 = G_B8_4;
		if (!L_26)
		{
			G_B10_0 = 5;
			G_B10_1 = L_24;
			G_B10_2 = L_24;
			G_B10_3 = G_B8_4;
			goto IL_00be;
		}
	}
	{
		G_B11_0 = (1.0f);
		G_B11_1 = G_B9_0;
		G_B11_2 = G_B9_1;
		G_B11_3 = G_B9_2;
		G_B11_4 = G_B9_3;
		goto IL_00c9;
	}

IL_00be:
	{
		FsmFloat_t2134102846 * L_27 = __this->get_time_20();
		NullCheck(L_27);
		float L_28 = FsmFloat_get_Value_m4137923823(L_27, /*hidden argument*/NULL);
		G_B11_0 = L_28;
		G_B11_1 = G_B10_0;
		G_B11_2 = G_B10_1;
		G_B11_3 = G_B10_2;
		G_B11_4 = G_B10_3;
	}

IL_00c9:
	{
		float L_29 = G_B11_0;
		Il2CppObject * L_30 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_29);
		NullCheck(G_B11_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B11_2, G_B11_1);
		ArrayElementTypeCheck (G_B11_2, L_30);
		(G_B11_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B11_1), (Il2CppObject *)L_30);
		ObjectU5BU5D_t1108656482* L_31 = G_B11_3;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 6);
		ArrayElementTypeCheck (L_31, _stringLiteral95467907);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral95467907);
		ObjectU5BU5D_t1108656482* L_32 = L_31;
		FsmFloat_t2134102846 * L_33 = __this->get_delay_21();
		NullCheck(L_33);
		bool L_34 = NamedVariable_get_IsNone_m281035543(L_33, /*hidden argument*/NULL);
		G_B12_0 = 7;
		G_B12_1 = L_32;
		G_B12_2 = L_32;
		G_B12_3 = G_B11_4;
		if (!L_34)
		{
			G_B13_0 = 7;
			G_B13_1 = L_32;
			G_B13_2 = L_32;
			G_B13_3 = G_B11_4;
			goto IL_00f3;
		}
	}
	{
		G_B14_0 = (0.0f);
		G_B14_1 = G_B12_0;
		G_B14_2 = G_B12_1;
		G_B14_3 = G_B12_2;
		G_B14_4 = G_B12_3;
		goto IL_00fe;
	}

IL_00f3:
	{
		FsmFloat_t2134102846 * L_35 = __this->get_delay_21();
		NullCheck(L_35);
		float L_36 = FsmFloat_get_Value_m4137923823(L_35, /*hidden argument*/NULL);
		G_B14_0 = L_36;
		G_B14_1 = G_B13_0;
		G_B14_2 = G_B13_1;
		G_B14_3 = G_B13_2;
		G_B14_4 = G_B13_3;
	}

IL_00fe:
	{
		float L_37 = G_B14_0;
		Il2CppObject * L_38 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_37);
		NullCheck(G_B14_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B14_2, G_B14_1);
		ArrayElementTypeCheck (G_B14_2, L_38);
		(G_B14_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B14_1), (Il2CppObject *)L_38);
		ObjectU5BU5D_t1108656482* L_39 = G_B14_3;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, 8);
		ArrayElementTypeCheck (L_39, _stringLiteral2258461662);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral2258461662);
		ObjectU5BU5D_t1108656482* L_40 = L_39;
		int32_t L_41 = __this->get_loopType_22();
		int32_t L_42 = L_41;
		Il2CppObject * L_43 = Box(LoopType_t1485160459_il2cpp_TypeInfo_var, &L_42);
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, ((int32_t)9));
		ArrayElementTypeCheck (L_40, L_43);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)L_43);
		ObjectU5BU5D_t1108656482* L_44 = L_40;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)10));
		ArrayElementTypeCheck (L_44, _stringLiteral2105864216);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Il2CppObject *)_stringLiteral2105864216);
		ObjectU5BU5D_t1108656482* L_45 = L_44;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, ((int32_t)11));
		ArrayElementTypeCheck (L_45, _stringLiteral338257242);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (Il2CppObject *)_stringLiteral338257242);
		ObjectU5BU5D_t1108656482* L_46 = L_45;
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, ((int32_t)12));
		ArrayElementTypeCheck (L_46, _stringLiteral3696326558);
		(L_46)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (Il2CppObject *)_stringLiteral3696326558);
		ObjectU5BU5D_t1108656482* L_47 = L_46;
		int32_t L_48 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_49 = L_48;
		Il2CppObject * L_50 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_49);
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, ((int32_t)13));
		ArrayElementTypeCheck (L_47, L_50);
		(L_47)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (Il2CppObject *)L_50);
		ObjectU5BU5D_t1108656482* L_51 = L_47;
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, ((int32_t)14));
		ArrayElementTypeCheck (L_51, _stringLiteral2987624931);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (Il2CppObject *)_stringLiteral2987624931);
		ObjectU5BU5D_t1108656482* L_52 = L_51;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, ((int32_t)15));
		ArrayElementTypeCheck (L_52, _stringLiteral647687137);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (Il2CppObject *)_stringLiteral647687137);
		ObjectU5BU5D_t1108656482* L_53 = L_52;
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, ((int32_t)16));
		ArrayElementTypeCheck (L_53, _stringLiteral2052738345);
		(L_53)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (Il2CppObject *)_stringLiteral2052738345);
		ObjectU5BU5D_t1108656482* L_54 = L_53;
		int32_t L_55 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_56 = L_55;
		Il2CppObject * L_57 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_56);
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, ((int32_t)17));
		ArrayElementTypeCheck (L_54, L_57);
		(L_54)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (Il2CppObject *)L_57);
		ObjectU5BU5D_t1108656482* L_58 = L_54;
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, ((int32_t)18));
		ArrayElementTypeCheck (L_58, _stringLiteral1067506955);
		(L_58)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (Il2CppObject *)_stringLiteral1067506955);
		ObjectU5BU5D_t1108656482* L_59 = L_58;
		FsmBool_t1075959796 * L_60 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_60);
		bool L_61 = NamedVariable_get_IsNone_m281035543(L_60, /*hidden argument*/NULL);
		G_B15_0 = ((int32_t)19);
		G_B15_1 = L_59;
		G_B15_2 = L_59;
		G_B15_3 = G_B14_4;
		if (!L_61)
		{
			G_B16_0 = ((int32_t)19);
			G_B16_1 = L_59;
			G_B16_2 = L_59;
			G_B16_3 = G_B14_4;
			goto IL_0191;
		}
	}
	{
		G_B17_0 = 0;
		G_B17_1 = G_B15_0;
		G_B17_2 = G_B15_1;
		G_B17_3 = G_B15_2;
		G_B17_4 = G_B15_3;
		goto IL_019c;
	}

IL_0191:
	{
		FsmBool_t1075959796 * L_62 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_62);
		bool L_63 = FsmBool_get_Value_m3101329097(L_62, /*hidden argument*/NULL);
		G_B17_0 = ((int32_t)(L_63));
		G_B17_1 = G_B16_0;
		G_B17_2 = G_B16_1;
		G_B17_3 = G_B16_2;
		G_B17_4 = G_B16_3;
	}

IL_019c:
	{
		bool L_64 = ((bool)G_B17_0);
		Il2CppObject * L_65 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_64);
		NullCheck(G_B17_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B17_2, G_B17_1);
		ArrayElementTypeCheck (G_B17_2, L_65);
		(G_B17_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B17_1), (Il2CppObject *)L_65);
		ObjectU5BU5D_t1108656482* L_66 = G_B17_3;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, ((int32_t)20));
		ArrayElementTypeCheck (L_66, _stringLiteral109637894);
		(L_66)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)20)), (Il2CppObject *)_stringLiteral109637894);
		ObjectU5BU5D_t1108656482* L_67 = L_66;
		int32_t L_68 = __this->get_space_23();
		int32_t L_69 = L_68;
		Il2CppObject * L_70 = Box(Space_t4209342076_il2cpp_TypeInfo_var, &L_69);
		NullCheck(L_67);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_67, ((int32_t)21));
		ArrayElementTypeCheck (L_67, L_70);
		(L_67)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)21)), (Il2CppObject *)L_70);
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		Hashtable_t1407064410 * L_71 = iTween_Hash_m2099495140(NULL /*static, unused*/, L_67, /*hidden argument*/NULL);
		iTween_ShakeRotation_m4198512075(NULL /*static, unused*/, G_B17_4, L_71, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenShakeScale::.ctor()
extern "C"  void iTweenShakeScale__ctor_m3627641752 (iTweenShakeScale_t2838558350 * __this, const MethodInfo* method)
{
	{
		iTweenFsmAction__ctor_m2960815636(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenShakeScale::Reset()
extern Il2CppClass* FsmString_t952858651_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern const uint32_t iTweenShakeScale_Reset_m1274074693_MetadataUsageId;
extern "C"  void iTweenShakeScale_Reset_m1274074693 (iTweenShakeScale_t2838558350 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenShakeScale_Reset_m1274074693_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmString_t952858651 * V_0 = NULL;
	FsmVector3_t533912882 * V_1 = NULL;
	{
		iTweenFsmAction_Reset_m607248577(__this, /*hidden argument*/NULL);
		FsmString_t952858651 * L_0 = (FsmString_t952858651 *)il2cpp_codegen_object_new(FsmString_t952858651_il2cpp_TypeInfo_var);
		FsmString__ctor_m2679108260(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmString_t952858651 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmString_t952858651 * L_2 = V_0;
		__this->set_id_18(L_2);
		FsmFloat_t2134102846 * L_3 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_time_20(L_3);
		FsmFloat_t2134102846 * L_4 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_delay_21(L_4);
		__this->set_loopType_22(0);
		FsmVector3_t533912882 * L_5 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_5, /*hidden argument*/NULL);
		V_1 = L_5;
		FsmVector3_t533912882 * L_6 = V_1;
		NullCheck(L_6);
		NamedVariable_set_UseVariable_m4266138971(L_6, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_7 = V_1;
		__this->set_vector_19(L_7);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenShakeScale::OnEnter()
extern "C"  void iTweenShakeScale_OnEnter_m2336975215 (iTweenShakeScale_t2838558350 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnEnteriTween_m3024539459(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_loopType_22();
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		iTweenFsmAction_IsLoop_m2539277653(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_001e:
	{
		iTweenShakeScale_DoiTween_m882919865(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenShakeScale::OnExit()
extern "C"  void iTweenShakeScale_OnExit_m2301063241 (iTweenShakeScale_t2838558350 * __this, const MethodInfo* method)
{
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_17();
		iTweenFsmAction_OnExitiTween_m1981625761(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenShakeScale::DoiTween()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* LoopType_t1485160459_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* iTween_t3087282050_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral109399814;
extern Il2CppCodeGenString* _stringLiteral2881114200;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral3560141;
extern Il2CppCodeGenString* _stringLiteral95467907;
extern Il2CppCodeGenString* _stringLiteral2258461662;
extern Il2CppCodeGenString* _stringLiteral2105864216;
extern Il2CppCodeGenString* _stringLiteral338257242;
extern Il2CppCodeGenString* _stringLiteral3696326558;
extern Il2CppCodeGenString* _stringLiteral2987624931;
extern Il2CppCodeGenString* _stringLiteral647687137;
extern Il2CppCodeGenString* _stringLiteral2052738345;
extern Il2CppCodeGenString* _stringLiteral1067506955;
extern const uint32_t iTweenShakeScale_DoiTween_m882919865_MetadataUsageId;
extern "C"  void iTweenShakeScale_DoiTween_m882919865 (iTweenShakeScale_t2838558350 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenShakeScale_DoiTween_m882919865_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B7_0 = 0;
	ObjectU5BU5D_t1108656482* G_B7_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B7_2 = NULL;
	GameObject_t3674682005 * G_B7_3 = NULL;
	int32_t G_B6_0 = 0;
	ObjectU5BU5D_t1108656482* G_B6_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B6_2 = NULL;
	GameObject_t3674682005 * G_B6_3 = NULL;
	String_t* G_B8_0 = NULL;
	int32_t G_B8_1 = 0;
	ObjectU5BU5D_t1108656482* G_B8_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B8_3 = NULL;
	GameObject_t3674682005 * G_B8_4 = NULL;
	int32_t G_B10_0 = 0;
	ObjectU5BU5D_t1108656482* G_B10_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B10_2 = NULL;
	GameObject_t3674682005 * G_B10_3 = NULL;
	int32_t G_B9_0 = 0;
	ObjectU5BU5D_t1108656482* G_B9_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B9_2 = NULL;
	GameObject_t3674682005 * G_B9_3 = NULL;
	float G_B11_0 = 0.0f;
	int32_t G_B11_1 = 0;
	ObjectU5BU5D_t1108656482* G_B11_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B11_3 = NULL;
	GameObject_t3674682005 * G_B11_4 = NULL;
	int32_t G_B13_0 = 0;
	ObjectU5BU5D_t1108656482* G_B13_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B13_2 = NULL;
	GameObject_t3674682005 * G_B13_3 = NULL;
	int32_t G_B12_0 = 0;
	ObjectU5BU5D_t1108656482* G_B12_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B12_2 = NULL;
	GameObject_t3674682005 * G_B12_3 = NULL;
	float G_B14_0 = 0.0f;
	int32_t G_B14_1 = 0;
	ObjectU5BU5D_t1108656482* G_B14_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B14_3 = NULL;
	GameObject_t3674682005 * G_B14_4 = NULL;
	int32_t G_B16_0 = 0;
	ObjectU5BU5D_t1108656482* G_B16_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B16_2 = NULL;
	GameObject_t3674682005 * G_B16_3 = NULL;
	int32_t G_B15_0 = 0;
	ObjectU5BU5D_t1108656482* G_B15_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B15_2 = NULL;
	GameObject_t3674682005 * G_B15_3 = NULL;
	int32_t G_B17_0 = 0;
	int32_t G_B17_1 = 0;
	ObjectU5BU5D_t1108656482* G_B17_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B17_3 = NULL;
	GameObject_t3674682005 * G_B17_4 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_17();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		Vector3_t4282066566  L_5 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_5;
		FsmVector3_t533912882 * L_6 = __this->get_vector_19();
		NullCheck(L_6);
		bool L_7 = NamedVariable_get_IsNone_m281035543(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003a;
		}
	}
	{
		goto IL_0046;
	}

IL_003a:
	{
		FsmVector3_t533912882 * L_8 = __this->get_vector_19();
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = FsmVector3_get_Value_m2779135117(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
	}

IL_0046:
	{
		((iTweenFsmAction_t410382178 *)__this)->set_itweenType_15(_stringLiteral109399814);
		GameObject_t3674682005 * L_10 = V_0;
		ObjectU5BU5D_t1108656482* L_11 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)20)));
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		ArrayElementTypeCheck (L_11, _stringLiteral2881114200);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2881114200);
		ObjectU5BU5D_t1108656482* L_12 = L_11;
		Vector3_t4282066566  L_13 = V_1;
		Vector3_t4282066566  L_14 = L_13;
		Il2CppObject * L_15 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 1);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_15);
		ObjectU5BU5D_t1108656482* L_16 = L_12;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 2);
		ArrayElementTypeCheck (L_16, _stringLiteral3373707);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3373707);
		ObjectU5BU5D_t1108656482* L_17 = L_16;
		FsmString_t952858651 * L_18 = __this->get_id_18();
		NullCheck(L_18);
		bool L_19 = NamedVariable_get_IsNone_m281035543(L_18, /*hidden argument*/NULL);
		G_B6_0 = 3;
		G_B6_1 = L_17;
		G_B6_2 = L_17;
		G_B6_3 = L_10;
		if (!L_19)
		{
			G_B7_0 = 3;
			G_B7_1 = L_17;
			G_B7_2 = L_17;
			G_B7_3 = L_10;
			goto IL_008e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B8_0 = L_20;
		G_B8_1 = G_B6_0;
		G_B8_2 = G_B6_1;
		G_B8_3 = G_B6_2;
		G_B8_4 = G_B6_3;
		goto IL_0099;
	}

IL_008e:
	{
		FsmString_t952858651 * L_21 = __this->get_id_18();
		NullCheck(L_21);
		String_t* L_22 = FsmString_get_Value_m872383149(L_21, /*hidden argument*/NULL);
		G_B8_0 = L_22;
		G_B8_1 = G_B7_0;
		G_B8_2 = G_B7_1;
		G_B8_3 = G_B7_2;
		G_B8_4 = G_B7_3;
	}

IL_0099:
	{
		NullCheck(G_B8_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B8_2, G_B8_1);
		ArrayElementTypeCheck (G_B8_2, G_B8_0);
		(G_B8_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B8_1), (Il2CppObject *)G_B8_0);
		ObjectU5BU5D_t1108656482* L_23 = G_B8_3;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 4);
		ArrayElementTypeCheck (L_23, _stringLiteral3560141);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral3560141);
		ObjectU5BU5D_t1108656482* L_24 = L_23;
		FsmFloat_t2134102846 * L_25 = __this->get_time_20();
		NullCheck(L_25);
		bool L_26 = NamedVariable_get_IsNone_m281035543(L_25, /*hidden argument*/NULL);
		G_B9_0 = 5;
		G_B9_1 = L_24;
		G_B9_2 = L_24;
		G_B9_3 = G_B8_4;
		if (!L_26)
		{
			G_B10_0 = 5;
			G_B10_1 = L_24;
			G_B10_2 = L_24;
			G_B10_3 = G_B8_4;
			goto IL_00be;
		}
	}
	{
		G_B11_0 = (1.0f);
		G_B11_1 = G_B9_0;
		G_B11_2 = G_B9_1;
		G_B11_3 = G_B9_2;
		G_B11_4 = G_B9_3;
		goto IL_00c9;
	}

IL_00be:
	{
		FsmFloat_t2134102846 * L_27 = __this->get_time_20();
		NullCheck(L_27);
		float L_28 = FsmFloat_get_Value_m4137923823(L_27, /*hidden argument*/NULL);
		G_B11_0 = L_28;
		G_B11_1 = G_B10_0;
		G_B11_2 = G_B10_1;
		G_B11_3 = G_B10_2;
		G_B11_4 = G_B10_3;
	}

IL_00c9:
	{
		float L_29 = G_B11_0;
		Il2CppObject * L_30 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_29);
		NullCheck(G_B11_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B11_2, G_B11_1);
		ArrayElementTypeCheck (G_B11_2, L_30);
		(G_B11_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B11_1), (Il2CppObject *)L_30);
		ObjectU5BU5D_t1108656482* L_31 = G_B11_3;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 6);
		ArrayElementTypeCheck (L_31, _stringLiteral95467907);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral95467907);
		ObjectU5BU5D_t1108656482* L_32 = L_31;
		FsmFloat_t2134102846 * L_33 = __this->get_delay_21();
		NullCheck(L_33);
		bool L_34 = NamedVariable_get_IsNone_m281035543(L_33, /*hidden argument*/NULL);
		G_B12_0 = 7;
		G_B12_1 = L_32;
		G_B12_2 = L_32;
		G_B12_3 = G_B11_4;
		if (!L_34)
		{
			G_B13_0 = 7;
			G_B13_1 = L_32;
			G_B13_2 = L_32;
			G_B13_3 = G_B11_4;
			goto IL_00f3;
		}
	}
	{
		G_B14_0 = (0.0f);
		G_B14_1 = G_B12_0;
		G_B14_2 = G_B12_1;
		G_B14_3 = G_B12_2;
		G_B14_4 = G_B12_3;
		goto IL_00fe;
	}

IL_00f3:
	{
		FsmFloat_t2134102846 * L_35 = __this->get_delay_21();
		NullCheck(L_35);
		float L_36 = FsmFloat_get_Value_m4137923823(L_35, /*hidden argument*/NULL);
		G_B14_0 = L_36;
		G_B14_1 = G_B13_0;
		G_B14_2 = G_B13_1;
		G_B14_3 = G_B13_2;
		G_B14_4 = G_B13_3;
	}

IL_00fe:
	{
		float L_37 = G_B14_0;
		Il2CppObject * L_38 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_37);
		NullCheck(G_B14_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B14_2, G_B14_1);
		ArrayElementTypeCheck (G_B14_2, L_38);
		(G_B14_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B14_1), (Il2CppObject *)L_38);
		ObjectU5BU5D_t1108656482* L_39 = G_B14_3;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, 8);
		ArrayElementTypeCheck (L_39, _stringLiteral2258461662);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral2258461662);
		ObjectU5BU5D_t1108656482* L_40 = L_39;
		int32_t L_41 = __this->get_loopType_22();
		int32_t L_42 = L_41;
		Il2CppObject * L_43 = Box(LoopType_t1485160459_il2cpp_TypeInfo_var, &L_42);
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, ((int32_t)9));
		ArrayElementTypeCheck (L_40, L_43);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)L_43);
		ObjectU5BU5D_t1108656482* L_44 = L_40;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)10));
		ArrayElementTypeCheck (L_44, _stringLiteral2105864216);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Il2CppObject *)_stringLiteral2105864216);
		ObjectU5BU5D_t1108656482* L_45 = L_44;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, ((int32_t)11));
		ArrayElementTypeCheck (L_45, _stringLiteral338257242);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (Il2CppObject *)_stringLiteral338257242);
		ObjectU5BU5D_t1108656482* L_46 = L_45;
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, ((int32_t)12));
		ArrayElementTypeCheck (L_46, _stringLiteral3696326558);
		(L_46)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (Il2CppObject *)_stringLiteral3696326558);
		ObjectU5BU5D_t1108656482* L_47 = L_46;
		int32_t L_48 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_49 = L_48;
		Il2CppObject * L_50 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_49);
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, ((int32_t)13));
		ArrayElementTypeCheck (L_47, L_50);
		(L_47)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (Il2CppObject *)L_50);
		ObjectU5BU5D_t1108656482* L_51 = L_47;
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, ((int32_t)14));
		ArrayElementTypeCheck (L_51, _stringLiteral2987624931);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (Il2CppObject *)_stringLiteral2987624931);
		ObjectU5BU5D_t1108656482* L_52 = L_51;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, ((int32_t)15));
		ArrayElementTypeCheck (L_52, _stringLiteral647687137);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (Il2CppObject *)_stringLiteral647687137);
		ObjectU5BU5D_t1108656482* L_53 = L_52;
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, ((int32_t)16));
		ArrayElementTypeCheck (L_53, _stringLiteral2052738345);
		(L_53)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (Il2CppObject *)_stringLiteral2052738345);
		ObjectU5BU5D_t1108656482* L_54 = L_53;
		int32_t L_55 = ((iTweenFsmAction_t410382178 *)__this)->get_itweenID_16();
		int32_t L_56 = L_55;
		Il2CppObject * L_57 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_56);
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, ((int32_t)17));
		ArrayElementTypeCheck (L_54, L_57);
		(L_54)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (Il2CppObject *)L_57);
		ObjectU5BU5D_t1108656482* L_58 = L_54;
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, ((int32_t)18));
		ArrayElementTypeCheck (L_58, _stringLiteral1067506955);
		(L_58)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (Il2CppObject *)_stringLiteral1067506955);
		ObjectU5BU5D_t1108656482* L_59 = L_58;
		FsmBool_t1075959796 * L_60 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_60);
		bool L_61 = NamedVariable_get_IsNone_m281035543(L_60, /*hidden argument*/NULL);
		G_B15_0 = ((int32_t)19);
		G_B15_1 = L_59;
		G_B15_2 = L_59;
		G_B15_3 = G_B14_4;
		if (!L_61)
		{
			G_B16_0 = ((int32_t)19);
			G_B16_1 = L_59;
			G_B16_2 = L_59;
			G_B16_3 = G_B14_4;
			goto IL_0191;
		}
	}
	{
		G_B17_0 = 0;
		G_B17_1 = G_B15_0;
		G_B17_2 = G_B15_1;
		G_B17_3 = G_B15_2;
		G_B17_4 = G_B15_3;
		goto IL_019c;
	}

IL_0191:
	{
		FsmBool_t1075959796 * L_62 = ((iTweenFsmAction_t410382178 *)__this)->get_realTime_11();
		NullCheck(L_62);
		bool L_63 = FsmBool_get_Value_m3101329097(L_62, /*hidden argument*/NULL);
		G_B17_0 = ((int32_t)(L_63));
		G_B17_1 = G_B16_0;
		G_B17_2 = G_B16_1;
		G_B17_3 = G_B16_2;
		G_B17_4 = G_B16_3;
	}

IL_019c:
	{
		bool L_64 = ((bool)G_B17_0);
		Il2CppObject * L_65 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_64);
		NullCheck(G_B17_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B17_2, G_B17_1);
		ArrayElementTypeCheck (G_B17_2, L_65);
		(G_B17_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B17_1), (Il2CppObject *)L_65);
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		Hashtable_t1407064410 * L_66 = iTween_Hash_m2099495140(NULL /*static, unused*/, G_B17_3, /*hidden argument*/NULL);
		iTween_ShakeScale_m3748868919(NULL /*static, unused*/, G_B17_4, L_66, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenStop::.ctor()
extern "C"  void iTweenStop__ctor_m3257348602 (iTweenStop_t3798813676 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenStop::Reset()
extern Il2CppClass* FsmString_t952858651_il2cpp_TypeInfo_var;
extern const uint32_t iTweenStop_Reset_m903781543_MetadataUsageId;
extern "C"  void iTweenStop_Reset_m903781543 (iTweenStop_t3798813676 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenStop_Reset_m903781543_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmString_t952858651 * V_0 = NULL;
	{
		FsmString_t952858651 * L_0 = (FsmString_t952858651 *)il2cpp_codegen_object_new(FsmString_t952858651_il2cpp_TypeInfo_var);
		FsmString__ctor_m2679108260(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmString_t952858651 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmString_t952858651 * L_2 = V_0;
		__this->set_id_10(L_2);
		__this->set_iTweenType_11(0);
		__this->set_includeChildren_12((bool)0);
		__this->set_inScene_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenStop::OnEnter()
extern "C"  void iTweenStop_OnEnter_m2967543633 (iTweenStop_t3798813676 * __this, const MethodInfo* method)
{
	{
		FsmStateAction_OnEnter_m160148417(__this, /*hidden argument*/NULL);
		iTweenStop_DoiTween_m3250671639(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.iTweenStop::DoiTween()
extern const Il2CppType* iTweenFSMType_t470630072_0_0_0_var;
extern Il2CppClass* iTween_t3087282050_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* iTweenFSMType_t470630072_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2862688501_il2cpp_TypeInfo_var;
extern const uint32_t iTweenStop_DoiTween_m3250671639_MetadataUsageId;
extern "C"  void iTweenStop_DoiTween_m3250671639 (iTweenStop_t3798813676 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (iTweenStop_DoiTween_m3250671639_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		FsmString_t952858651 * L_0 = __this->get_id_10();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_009e;
		}
	}
	{
		int32_t L_2 = __this->get_iTweenType_11();
		if (L_2)
		{
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		iTween_Stop_m1058498125(NULL /*static, unused*/, /*hidden argument*/NULL);
		goto IL_0099;
	}

IL_0025:
	{
		bool L_3 = __this->get_inScene_13();
		if (!L_3)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(iTweenFSMType_t470630072_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_5 = __this->get_iTweenType_11();
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(iTweenFSMType_t470630072_il2cpp_TypeInfo_var, &L_6);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		String_t* L_8 = Enum_GetName_m3333121265(NULL /*static, unused*/, L_4, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		iTween_Stop_m1656069589(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		goto IL_0099;
	}

IL_0054:
	{
		Fsm_t1527112426 * L_9 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_10 = __this->get_gameObject_9();
		NullCheck(L_9);
		GameObject_t3674682005 * L_11 = Fsm_GetOwnerDefaultTarget_m846013999(L_9, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		GameObject_t3674682005 * L_12 = V_0;
		bool L_13 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_12, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0073;
		}
	}
	{
		return;
	}

IL_0073:
	{
		GameObject_t3674682005 * L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(iTweenFSMType_t470630072_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_16 = __this->get_iTweenType_11();
		int32_t L_17 = L_16;
		Il2CppObject * L_18 = Box(iTweenFSMType_t470630072_il2cpp_TypeInfo_var, &L_17);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		String_t* L_19 = Enum_GetName_m3333121265(NULL /*static, unused*/, L_15, L_18, /*hidden argument*/NULL);
		bool L_20 = __this->get_includeChildren_12();
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		iTween_Stop_m684185564(NULL /*static, unused*/, L_14, L_19, L_20, /*hidden argument*/NULL);
	}

IL_0099:
	{
		goto IL_00ae;
	}

IL_009e:
	{
		FsmString_t952858651 * L_21 = __this->get_id_10();
		NullCheck(L_21);
		String_t* L_22 = FsmString_get_Value_m872383149(L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		iTween_StopByName_m4261169907(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
	}

IL_00ae:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.KillDelayedEvents::.ctor()
extern "C"  void KillDelayedEvents__ctor_m1882338027 (KillDelayedEvents_t3079223979 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.KillDelayedEvents::OnEnter()
extern "C"  void KillDelayedEvents_OnEnter_m137340930 (KillDelayedEvents_t3079223979 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Fsm_KillDelayedEvents_m3981231728(L_0, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.LoadLevel::.ctor()
extern "C"  void LoadLevel__ctor_m3843619114 (LoadLevel_t1482164620 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.LoadLevel::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t LoadLevel_Reset_m1490052055_MetadataUsageId;
extern "C"  void LoadLevel_Reset_m1490052055 (LoadLevel_t1482164620 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LoadLevel_Reset_m1490052055_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_levelName_9(L_1);
		__this->set_additive_10((bool)0);
		__this->set_async_11((bool)0);
		__this->set_loadedEvent_12((FsmEvent_t2133468028 *)NULL);
		FsmBool_t1075959796 * L_2 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_dontDestroyOnLoad_13(L_2);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.LoadLevel::OnEnter()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2626724748;
extern Il2CppCodeGenString* _stringLiteral2390652080;
extern Il2CppCodeGenString* _stringLiteral3657977508;
extern Il2CppCodeGenString* _stringLiteral1224875460;
extern Il2CppCodeGenString* _stringLiteral1942251315;
extern const uint32_t LoadLevel_OnEnter_m3732789889_MetadataUsageId;
extern "C"  void LoadLevel_OnEnter_m3732789889 (LoadLevel_t1482164620 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LoadLevel_OnEnter_m3732789889_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t1659122786 * V_0 = NULL;
	{
		FsmBool_t1075959796 * L_0 = __this->get_dontDestroyOnLoad_13();
		NullCheck(L_0);
		bool L_1 = FsmBool_get_Value_m3101329097(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		GameObject_t3674682005 * L_2 = FsmStateAction_get_Owner_m1855633209(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t1659122786 * L_3 = GameObject_get_transform_m1278640159(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t1659122786 * L_4 = Transform_get_root_m1064615716(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Transform_t1659122786 * L_5 = V_0;
		NullCheck(L_5);
		GameObject_t3674682005 * L_6 = Component_get_gameObject_m1170635899(L_5, /*hidden argument*/NULL);
		Object_DontDestroyOnLoad_m4064482788(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
	}

IL_002c:
	{
		bool L_7 = __this->get_additive_10();
		if (!L_7)
		{
			goto IL_00a2;
		}
	}
	{
		bool L_8 = __this->get_async_11();
		if (!L_8)
		{
			goto IL_0073;
		}
	}
	{
		FsmString_t952858651 * L_9 = __this->get_levelName_9();
		NullCheck(L_9);
		String_t* L_10 = FsmString_get_Value_m872383149(L_9, /*hidden argument*/NULL);
		AsyncOperation_t3699081103 * L_11 = Application_LoadLevelAdditiveAsync_m381683458(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		__this->set_asyncOperation_14(L_11);
		FsmString_t952858651 * L_12 = __this->get_levelName_9();
		NullCheck(L_12);
		String_t* L_13 = FsmString_get_Value_m872383149(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2626724748, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		return;
	}

IL_0073:
	{
		FsmString_t952858651 * L_15 = __this->get_levelName_9();
		NullCheck(L_15);
		String_t* L_16 = FsmString_get_Value_m872383149(L_15, /*hidden argument*/NULL);
		Application_LoadLevelAdditive_m3028901073(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		FsmString_t952858651 * L_17 = __this->get_levelName_9();
		NullCheck(L_17);
		String_t* L_18 = FsmString_get_Value_m872383149(L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2390652080, L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		goto IL_0108;
	}

IL_00a2:
	{
		bool L_20 = __this->get_async_11();
		if (!L_20)
		{
			goto IL_00de;
		}
	}
	{
		FsmString_t952858651 * L_21 = __this->get_levelName_9();
		NullCheck(L_21);
		String_t* L_22 = FsmString_get_Value_m872383149(L_21, /*hidden argument*/NULL);
		AsyncOperation_t3699081103 * L_23 = Application_LoadLevelAsync_m572913174(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		__this->set_asyncOperation_14(L_23);
		FsmString_t952858651 * L_24 = __this->get_levelName_9();
		NullCheck(L_24);
		String_t* L_25 = FsmString_get_Value_m872383149(L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3657977508, L_25, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		return;
	}

IL_00de:
	{
		FsmString_t952858651 * L_27 = __this->get_levelName_9();
		NullCheck(L_27);
		String_t* L_28 = FsmString_get_Value_m872383149(L_27, /*hidden argument*/NULL);
		Application_LoadLevel_m2722573885(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		FsmString_t952858651 * L_29 = __this->get_levelName_9();
		NullCheck(L_29);
		String_t* L_30 = FsmString_get_Value_m872383149(L_29, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_31 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1224875460, L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
	}

IL_0108:
	{
		FsmStateAction_Log_m3811764982(__this, _stringLiteral1942251315, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_32 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_33 = __this->get_loadedEvent_12();
		NullCheck(L_32);
		Fsm_Event_m625948263(L_32, L_33, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.LoadLevel::OnUpdate()
extern "C"  void LoadLevel_OnUpdate_m3180896226 (LoadLevel_t1482164620 * __this, const MethodInfo* method)
{
	{
		AsyncOperation_t3699081103 * L_0 = __this->get_asyncOperation_14();
		NullCheck(L_0);
		bool L_1 = AsyncOperation_get_isDone_m2747591837(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Fsm_t1527112426 * L_2 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_3 = __this->get_loadedEvent_12();
		NullCheck(L_2);
		Fsm_Event_m625948263(L_2, L_3, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.LoadLevelNum::.ctor()
extern "C"  void LoadLevelNum__ctor_m2451390934 (LoadLevelNum_t2730020496 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.LoadLevelNum::Reset()
extern "C"  void LoadLevelNum_Reset_m97823875 (LoadLevelNum_t2730020496 * __this, const MethodInfo* method)
{
	{
		__this->set_levelIndex_9((FsmInt_t1596138449 *)NULL);
		__this->set_additive_10((bool)0);
		__this->set_loadedEvent_11((FsmEvent_t2133468028 *)NULL);
		FsmBool_t1075959796 * L_0 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_dontDestroyOnLoad_12(L_0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.LoadLevelNum::OnEnter()
extern "C"  void LoadLevelNum_OnEnter_m1536337965 (LoadLevelNum_t2730020496 * __this, const MethodInfo* method)
{
	Transform_t1659122786 * V_0 = NULL;
	{
		FsmBool_t1075959796 * L_0 = __this->get_dontDestroyOnLoad_12();
		NullCheck(L_0);
		bool L_1 = FsmBool_get_Value_m3101329097(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		GameObject_t3674682005 * L_2 = FsmStateAction_get_Owner_m1855633209(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t1659122786 * L_3 = GameObject_get_transform_m1278640159(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t1659122786 * L_4 = Transform_get_root_m1064615716(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Transform_t1659122786 * L_5 = V_0;
		NullCheck(L_5);
		GameObject_t3674682005 * L_6 = Component_get_gameObject_m1170635899(L_5, /*hidden argument*/NULL);
		Object_DontDestroyOnLoad_m4064482788(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
	}

IL_002c:
	{
		bool L_7 = __this->get_additive_10();
		if (!L_7)
		{
			goto IL_004c;
		}
	}
	{
		FsmInt_t1596138449 * L_8 = __this->get_levelIndex_9();
		NullCheck(L_8);
		int32_t L_9 = FsmInt_get_Value_m27059446(L_8, /*hidden argument*/NULL);
		Application_LoadLevelAdditive_m3131015586(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		goto IL_005c;
	}

IL_004c:
	{
		FsmInt_t1596138449 * L_10 = __this->get_levelIndex_9();
		NullCheck(L_10);
		int32_t L_11 = FsmInt_get_Value_m27059446(L_10, /*hidden argument*/NULL);
		Application_LoadLevel_m1181471414(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
	}

IL_005c:
	{
		Fsm_t1527112426 * L_12 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_13 = __this->get_loadedEvent_11();
		NullCheck(L_12);
		Fsm_Event_m625948263(L_12, L_13, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.LookAt::.ctor()
extern "C"  void LookAt__ctor_m1391208076 (LookAt_t3138770714 * __this, const MethodInfo* method)
{
	{
		__this->set_everyFrame_16((bool)1);
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.LookAt::Reset()
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern const uint32_t LookAt_Reset_m3332608313_MetadataUsageId;
extern "C"  void LookAt_Reset_m3332608313 (LookAt_t3138770714 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LookAt_Reset_m3332608313_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmVector3_t533912882 * V_0 = NULL;
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_targetObject_10((FsmGameObject_t1697147867 *)NULL);
		FsmVector3_t533912882 * L_0 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmVector3_t533912882 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_2 = V_0;
		__this->set_targetPosition_11(L_2);
		FsmVector3_t533912882 * L_3 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_3, /*hidden argument*/NULL);
		V_0 = L_3;
		FsmVector3_t533912882 * L_4 = V_0;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_5 = V_0;
		__this->set_upVector_12(L_5);
		FsmBool_t1075959796 * L_6 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_keepVertical_13(L_6);
		FsmBool_t1075959796 * L_7 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_debug_14(L_7);
		Color_t4194546905  L_8 = Color_get_yellow_m599454500(NULL /*static, unused*/, /*hidden argument*/NULL);
		FsmColor_t2131419205 * L_9 = FsmColor_op_Implicit_m2192961033(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		__this->set_debugLineColor_15(L_9);
		__this->set_everyFrame_16((bool)1);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.LookAt::OnEnter()
extern "C"  void LookAt_OnEnter_m607860579 (LookAt_t3138770714 * __this, const MethodInfo* method)
{
	{
		LookAt_DoLookAt_m2081285653(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_16();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.LookAt::OnLateUpdate()
extern "C"  void LookAt_OnLateUpdate_m4174841094 (LookAt_t3138770714 * __this, const MethodInfo* method)
{
	{
		LookAt_DoLookAt_m2081285653(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.LookAt::DoLookAt()
extern "C"  void LookAt_DoLookAt_m2081285653 (LookAt_t3138770714 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  G_B4_0;
	memset(&G_B4_0, 0, sizeof(G_B4_0));
	Transform_t1659122786 * G_B4_1 = NULL;
	Vector3_t4282066566  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	Transform_t1659122786 * G_B3_1 = NULL;
	Vector3_t4282066566  G_B5_0;
	memset(&G_B5_0, 0, sizeof(G_B5_0));
	Vector3_t4282066566  G_B5_1;
	memset(&G_B5_1, 0, sizeof(G_B5_1));
	Transform_t1659122786 * G_B5_2 = NULL;
	{
		bool L_0 = LookAt_UpdateLookAtPosition_m1083860624(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		GameObject_t3674682005 * L_1 = __this->get_go_17();
		NullCheck(L_1);
		Transform_t1659122786 * L_2 = GameObject_get_transform_m1278640159(L_1, /*hidden argument*/NULL);
		Vector3_t4282066566  L_3 = __this->get_lookAtPos_19();
		FsmVector3_t533912882 * L_4 = __this->get_upVector_12();
		NullCheck(L_4);
		bool L_5 = NamedVariable_get_IsNone_m281035543(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = L_2;
		if (!L_5)
		{
			G_B4_0 = L_3;
			G_B4_1 = L_2;
			goto IL_0037;
		}
	}
	{
		Vector3_t4282066566  L_6 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B5_0 = L_6;
		G_B5_1 = G_B3_0;
		G_B5_2 = G_B3_1;
		goto IL_0042;
	}

IL_0037:
	{
		FsmVector3_t533912882 * L_7 = __this->get_upVector_12();
		NullCheck(L_7);
		Vector3_t4282066566  L_8 = FsmVector3_get_Value_m2779135117(L_7, /*hidden argument*/NULL);
		G_B5_0 = L_8;
		G_B5_1 = G_B4_0;
		G_B5_2 = G_B4_1;
	}

IL_0042:
	{
		NullCheck(G_B5_2);
		Transform_LookAt_m2054691043(G_B5_2, G_B5_1, G_B5_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean HutongGames.PlayMaker.Actions.LookAt::UpdateLookAtPosition()
extern "C"  bool LookAt_UpdateLookAtPosition_m1083860624 (LookAt_t3138770714 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	LookAt_t3138770714 * G_B8_0 = NULL;
	LookAt_t3138770714 * G_B7_0 = NULL;
	Vector3_t4282066566  G_B9_0;
	memset(&G_B9_0, 0, sizeof(G_B9_0));
	LookAt_t3138770714 * G_B9_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		__this->set_go_17(L_2);
		GameObject_t3674682005 * L_3 = __this->get_go_17();
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002a;
		}
	}
	{
		return (bool)0;
	}

IL_002a:
	{
		FsmGameObject_t1697147867 * L_5 = __this->get_targetObject_10();
		NullCheck(L_5);
		GameObject_t3674682005 * L_6 = FsmGameObject_get_Value_m673294275(L_5, /*hidden argument*/NULL);
		__this->set_goTarget_18(L_6);
		GameObject_t3674682005 * L_7 = __this->get_goTarget_18();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005e;
		}
	}
	{
		FsmVector3_t533912882 * L_9 = __this->get_targetPosition_11();
		NullCheck(L_9);
		bool L_10 = NamedVariable_get_IsNone_m281035543(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_005e;
		}
	}
	{
		return (bool)0;
	}

IL_005e:
	{
		GameObject_t3674682005 * L_11 = __this->get_goTarget_18();
		bool L_12 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_11, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00ba;
		}
	}
	{
		FsmVector3_t533912882 * L_13 = __this->get_targetPosition_11();
		NullCheck(L_13);
		bool L_14 = NamedVariable_get_IsNone_m281035543(L_13, /*hidden argument*/NULL);
		G_B7_0 = __this;
		if (L_14)
		{
			G_B8_0 = __this;
			goto IL_00a0;
		}
	}
	{
		GameObject_t3674682005 * L_15 = __this->get_goTarget_18();
		NullCheck(L_15);
		Transform_t1659122786 * L_16 = GameObject_get_transform_m1278640159(L_15, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_17 = __this->get_targetPosition_11();
		NullCheck(L_17);
		Vector3_t4282066566  L_18 = FsmVector3_get_Value_m2779135117(L_17, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector3_t4282066566  L_19 = Transform_TransformPoint_m437395512(L_16, L_18, /*hidden argument*/NULL);
		G_B9_0 = L_19;
		G_B9_1 = G_B7_0;
		goto IL_00b0;
	}

IL_00a0:
	{
		GameObject_t3674682005 * L_20 = __this->get_goTarget_18();
		NullCheck(L_20);
		Transform_t1659122786 * L_21 = GameObject_get_transform_m1278640159(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		Vector3_t4282066566  L_22 = Transform_get_position_m2211398607(L_21, /*hidden argument*/NULL);
		G_B9_0 = L_22;
		G_B9_1 = G_B8_0;
	}

IL_00b0:
	{
		NullCheck(G_B9_1);
		G_B9_1->set_lookAtPos_19(G_B9_0);
		goto IL_00cb;
	}

IL_00ba:
	{
		FsmVector3_t533912882 * L_23 = __this->get_targetPosition_11();
		NullCheck(L_23);
		Vector3_t4282066566  L_24 = FsmVector3_get_Value_m2779135117(L_23, /*hidden argument*/NULL);
		__this->set_lookAtPos_19(L_24);
	}

IL_00cb:
	{
		Vector3_t4282066566  L_25 = __this->get_lookAtPos_19();
		__this->set_lookAtPosWithVertical_20(L_25);
		FsmBool_t1075959796 * L_26 = __this->get_keepVertical_13();
		NullCheck(L_26);
		bool L_27 = FsmBool_get_Value_m3101329097(L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_010a;
		}
	}
	{
		Vector3_t4282066566 * L_28 = __this->get_address_of_lookAtPos_19();
		GameObject_t3674682005 * L_29 = __this->get_go_17();
		NullCheck(L_29);
		Transform_t1659122786 * L_30 = GameObject_get_transform_m1278640159(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		Vector3_t4282066566  L_31 = Transform_get_position_m2211398607(L_30, /*hidden argument*/NULL);
		V_0 = L_31;
		float L_32 = (&V_0)->get_y_2();
		L_28->set_y_2(L_32);
	}

IL_010a:
	{
		return (bool)1;
	}
}
// UnityEngine.Vector3 HutongGames.PlayMaker.Actions.LookAt::GetLookAtPosition()
extern "C"  Vector3_t4282066566  LookAt_GetLookAtPosition_m2612037921 (LookAt_t3138770714 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_lookAtPos_19();
		return L_0;
	}
}
// UnityEngine.Vector3 HutongGames.PlayMaker.Actions.LookAt::GetLookAtPositionWithVertical()
extern "C"  Vector3_t4282066566  LookAt_GetLookAtPositionWithVertical_m100816029 (LookAt_t3138770714 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_lookAtPosWithVertical_20();
		return L_0;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MasterServerClearHostList::.ctor()
extern "C"  void MasterServerClearHostList__ctor_m3245766618 (MasterServerClearHostList_t3350951132 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MasterServerClearHostList::OnEnter()
extern "C"  void MasterServerClearHostList_OnEnter_m427191601 (MasterServerClearHostList_t3350951132 * __this, const MethodInfo* method)
{
	{
		MasterServer_ClearHostList_m2859464953(NULL /*static, unused*/, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MasterServerGetHostCount::.ctor()
extern "C"  void MasterServerGetHostCount__ctor_m2794741512 (MasterServerGetHostCount_t240962846 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MasterServerGetHostCount::OnEnter()
extern "C"  void MasterServerGetHostCount_OnEnter_m783761631 (MasterServerGetHostCount_t240962846 * __this, const MethodInfo* method)
{
	{
		FsmInt_t1596138449 * L_0 = __this->get_count_9();
		HostDataU5BU5D_t4021970803* L_1 = MasterServer_PollHostList_m3223787845(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		NullCheck(L_0);
		FsmInt_set_Value_m2087583461(L_0, (((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MasterServerGetHostData::.ctor()
extern "C"  void MasterServerGetHostData__ctor_m1766034149 (MasterServerGetHostData_t2799670129 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MasterServerGetHostData::Reset()
extern "C"  void MasterServerGetHostData_Reset_m3707434386 (MasterServerGetHostData_t2799670129 * __this, const MethodInfo* method)
{
	{
		__this->set_hostIndex_9((FsmInt_t1596138449 *)NULL);
		__this->set_useNat_10((FsmBool_t1075959796 *)NULL);
		__this->set_gameType_11((FsmString_t952858651 *)NULL);
		__this->set_gameName_12((FsmString_t952858651 *)NULL);
		__this->set_connectedPlayers_13((FsmInt_t1596138449 *)NULL);
		__this->set_playerLimit_14((FsmInt_t1596138449 *)NULL);
		__this->set_ipAddress_15((FsmString_t952858651 *)NULL);
		__this->set_port_16((FsmInt_t1596138449 *)NULL);
		__this->set_passwordProtected_17((FsmBool_t1075959796 *)NULL);
		__this->set_comment_18((FsmString_t952858651 *)NULL);
		__this->set_guid_19((FsmString_t952858651 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MasterServerGetHostData::OnEnter()
extern "C"  void MasterServerGetHostData_OnEnter_m38463868 (MasterServerGetHostData_t2799670129 * __this, const MethodInfo* method)
{
	{
		MasterServerGetHostData_GetHostData_m1729793899(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MasterServerGetHostData::GetHostData()
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1623559376;
extern Il2CppCodeGenString* _stringLiteral3286686302;
extern const uint32_t MasterServerGetHostData_GetHostData_m1729793899_MetadataUsageId;
extern "C"  void MasterServerGetHostData_GetHostData_m1729793899 (MasterServerGetHostData_t2799670129 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MasterServerGetHostData_GetHostData_m1729793899_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	HostData_t3270478838 * V_2 = NULL;
	{
		HostDataU5BU5D_t4021970803* L_0 = MasterServer_PollHostList_m3223787845(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		V_0 = (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
		FsmInt_t1596138449 * L_1 = __this->get_hostIndex_9();
		NullCheck(L_1);
		int32_t L_2 = FsmInt_get_Value_m27059446(L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = V_1;
		if ((((int32_t)L_3) < ((int32_t)0)))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		if ((((int32_t)L_4) < ((int32_t)L_5)))
		{
			goto IL_002e;
		}
	}

IL_0022:
	{
		FsmStateAction_LogError_m3478223492(__this, _stringLiteral1623559376, /*hidden argument*/NULL);
		return;
	}

IL_002e:
	{
		HostDataU5BU5D_t4021970803* L_6 = MasterServer_PollHostList_m3223787845(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_7 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		HostData_t3270478838 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_2 = L_9;
		HostData_t3270478838 * L_10 = V_2;
		if (L_10)
		{
			goto IL_0053;
		}
	}
	{
		int32_t L_11 = V_1;
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_12);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral3286686302, L_13, /*hidden argument*/NULL);
		FsmStateAction_LogError_m3478223492(__this, L_14, /*hidden argument*/NULL);
		return;
	}

IL_0053:
	{
		FsmBool_t1075959796 * L_15 = __this->get_useNat_10();
		HostData_t3270478838 * L_16 = V_2;
		NullCheck(L_16);
		bool L_17 = HostData_get_useNat_m4254429010(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		FsmBool_set_Value_m1126216340(L_15, L_17, /*hidden argument*/NULL);
		FsmString_t952858651 * L_18 = __this->get_gameType_11();
		HostData_t3270478838 * L_19 = V_2;
		NullCheck(L_19);
		String_t* L_20 = HostData_get_gameType_m875859009(L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		FsmString_set_Value_m829393196(L_18, L_20, /*hidden argument*/NULL);
		FsmString_t952858651 * L_21 = __this->get_gameName_12();
		HostData_t3270478838 * L_22 = V_2;
		NullCheck(L_22);
		String_t* L_23 = HostData_get_gameName_m681830226(L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		FsmString_set_Value_m829393196(L_21, L_23, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_24 = __this->get_connectedPlayers_13();
		HostData_t3270478838 * L_25 = V_2;
		NullCheck(L_25);
		int32_t L_26 = HostData_get_connectedPlayers_m3966863803(L_25, /*hidden argument*/NULL);
		NullCheck(L_24);
		FsmInt_set_Value_m2087583461(L_24, L_26, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_27 = __this->get_playerLimit_14();
		HostData_t3270478838 * L_28 = V_2;
		NullCheck(L_28);
		int32_t L_29 = HostData_get_playerLimit_m2278605994(L_28, /*hidden argument*/NULL);
		NullCheck(L_27);
		FsmInt_set_Value_m2087583461(L_27, L_29, /*hidden argument*/NULL);
		FsmString_t952858651 * L_30 = __this->get_ipAddress_15();
		HostData_t3270478838 * L_31 = V_2;
		NullCheck(L_31);
		StringU5BU5D_t4054002952* L_32 = HostData_get_ip_m3205883706(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, 0);
		int32_t L_33 = 0;
		String_t* L_34 = (L_32)->GetAt(static_cast<il2cpp_array_size_t>(L_33));
		NullCheck(L_30);
		FsmString_set_Value_m829393196(L_30, L_34, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_35 = __this->get_port_16();
		HostData_t3270478838 * L_36 = V_2;
		NullCheck(L_36);
		int32_t L_37 = HostData_get_port_m3377460051(L_36, /*hidden argument*/NULL);
		NullCheck(L_35);
		FsmInt_set_Value_m2087583461(L_35, L_37, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_38 = __this->get_passwordProtected_17();
		HostData_t3270478838 * L_39 = V_2;
		NullCheck(L_39);
		bool L_40 = HostData_get_passwordProtected_m255763581(L_39, /*hidden argument*/NULL);
		NullCheck(L_38);
		FsmBool_set_Value_m1126216340(L_38, L_40, /*hidden argument*/NULL);
		FsmString_t952858651 * L_41 = __this->get_comment_18();
		HostData_t3270478838 * L_42 = V_2;
		NullCheck(L_42);
		String_t* L_43 = HostData_get_comment_m2806883628(L_42, /*hidden argument*/NULL);
		NullCheck(L_41);
		FsmString_set_Value_m829393196(L_41, L_43, /*hidden argument*/NULL);
		FsmString_t952858651 * L_44 = __this->get_guid_19();
		HostData_t3270478838 * L_45 = V_2;
		NullCheck(L_45);
		String_t* L_46 = HostData_get_guid_m4094499646(L_45, /*hidden argument*/NULL);
		NullCheck(L_44);
		FsmString_set_Value_m829393196(L_44, L_46, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::.ctor()
extern "C"  void MasterServerGetNextHostData__ctor_m2294102578 (MasterServerGetNextHostData_t2113983876 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::Reset()
extern "C"  void MasterServerGetNextHostData_Reset_m4235502815 (MasterServerGetNextHostData_t2113983876 * __this, const MethodInfo* method)
{
	{
		__this->set_finishedEvent_10((FsmEvent_t2133468028 *)NULL);
		__this->set_loopEvent_9((FsmEvent_t2133468028 *)NULL);
		__this->set_index_11((FsmInt_t1596138449 *)NULL);
		__this->set_useNat_12((FsmBool_t1075959796 *)NULL);
		__this->set_gameType_13((FsmString_t952858651 *)NULL);
		__this->set_gameName_14((FsmString_t952858651 *)NULL);
		__this->set_connectedPlayers_15((FsmInt_t1596138449 *)NULL);
		__this->set_playerLimit_16((FsmInt_t1596138449 *)NULL);
		__this->set_ipAddress_17((FsmString_t952858651 *)NULL);
		__this->set_port_18((FsmInt_t1596138449 *)NULL);
		__this->set_passwordProtected_19((FsmBool_t1075959796 *)NULL);
		__this->set_comment_20((FsmString_t952858651 *)NULL);
		__this->set_guid_21((FsmString_t952858651 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::OnEnter()
extern "C"  void MasterServerGetNextHostData_OnEnter_m706083209 (MasterServerGetNextHostData_t2113983876 * __this, const MethodInfo* method)
{
	{
		MasterServerGetNextHostData_DoGetNextHostData_m3470601408(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::DoGetNextHostData()
extern "C"  void MasterServerGetNextHostData_DoGetNextHostData_m3470601408 (MasterServerGetNextHostData_t2113983876 * __this, const MethodInfo* method)
{
	HostData_t3270478838 * V_0 = NULL;
	{
		int32_t L_0 = __this->get_nextItemIndex_22();
		HostDataU5BU5D_t4021970803* L_1 = MasterServer_PollHostList_m3223787845(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		if ((((int32_t)L_0) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))))))
		{
			goto IL_002b;
		}
	}
	{
		__this->set_nextItemIndex_22(0);
		Fsm_t1527112426 * L_2 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_3 = __this->get_finishedEvent_10();
		NullCheck(L_2);
		Fsm_Event_m625948263(L_2, L_3, /*hidden argument*/NULL);
		return;
	}

IL_002b:
	{
		HostDataU5BU5D_t4021970803* L_4 = MasterServer_PollHostList_m3223787845(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = __this->get_nextItemIndex_22();
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		HostData_t3270478838 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_0 = L_7;
		FsmInt_t1596138449 * L_8 = __this->get_index_11();
		int32_t L_9 = __this->get_nextItemIndex_22();
		NullCheck(L_8);
		FsmInt_set_Value_m2087583461(L_8, L_9, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_10 = __this->get_useNat_12();
		HostData_t3270478838 * L_11 = V_0;
		NullCheck(L_11);
		bool L_12 = HostData_get_useNat_m4254429010(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		FsmBool_set_Value_m1126216340(L_10, L_12, /*hidden argument*/NULL);
		FsmString_t952858651 * L_13 = __this->get_gameType_13();
		HostData_t3270478838 * L_14 = V_0;
		NullCheck(L_14);
		String_t* L_15 = HostData_get_gameType_m875859009(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		FsmString_set_Value_m829393196(L_13, L_15, /*hidden argument*/NULL);
		FsmString_t952858651 * L_16 = __this->get_gameName_14();
		HostData_t3270478838 * L_17 = V_0;
		NullCheck(L_17);
		String_t* L_18 = HostData_get_gameName_m681830226(L_17, /*hidden argument*/NULL);
		NullCheck(L_16);
		FsmString_set_Value_m829393196(L_16, L_18, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_19 = __this->get_connectedPlayers_15();
		HostData_t3270478838 * L_20 = V_0;
		NullCheck(L_20);
		int32_t L_21 = HostData_get_connectedPlayers_m3966863803(L_20, /*hidden argument*/NULL);
		NullCheck(L_19);
		FsmInt_set_Value_m2087583461(L_19, L_21, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_22 = __this->get_playerLimit_16();
		HostData_t3270478838 * L_23 = V_0;
		NullCheck(L_23);
		int32_t L_24 = HostData_get_playerLimit_m2278605994(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		FsmInt_set_Value_m2087583461(L_22, L_24, /*hidden argument*/NULL);
		FsmString_t952858651 * L_25 = __this->get_ipAddress_17();
		HostData_t3270478838 * L_26 = V_0;
		NullCheck(L_26);
		StringU5BU5D_t4054002952* L_27 = HostData_get_ip_m3205883706(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, 0);
		int32_t L_28 = 0;
		String_t* L_29 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		NullCheck(L_25);
		FsmString_set_Value_m829393196(L_25, L_29, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_30 = __this->get_port_18();
		HostData_t3270478838 * L_31 = V_0;
		NullCheck(L_31);
		int32_t L_32 = HostData_get_port_m3377460051(L_31, /*hidden argument*/NULL);
		NullCheck(L_30);
		FsmInt_set_Value_m2087583461(L_30, L_32, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_33 = __this->get_passwordProtected_19();
		HostData_t3270478838 * L_34 = V_0;
		NullCheck(L_34);
		bool L_35 = HostData_get_passwordProtected_m255763581(L_34, /*hidden argument*/NULL);
		NullCheck(L_33);
		FsmBool_set_Value_m1126216340(L_33, L_35, /*hidden argument*/NULL);
		FsmString_t952858651 * L_36 = __this->get_comment_20();
		HostData_t3270478838 * L_37 = V_0;
		NullCheck(L_37);
		String_t* L_38 = HostData_get_comment_m2806883628(L_37, /*hidden argument*/NULL);
		NullCheck(L_36);
		FsmString_set_Value_m829393196(L_36, L_38, /*hidden argument*/NULL);
		FsmString_t952858651 * L_39 = __this->get_guid_21();
		HostData_t3270478838 * L_40 = V_0;
		NullCheck(L_40);
		String_t* L_41 = HostData_get_guid_m4094499646(L_40, /*hidden argument*/NULL);
		NullCheck(L_39);
		FsmString_set_Value_m829393196(L_39, L_41, /*hidden argument*/NULL);
		int32_t L_42 = __this->get_nextItemIndex_22();
		HostDataU5BU5D_t4021970803* L_43 = MasterServer_PollHostList_m3223787845(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_43);
		if ((((int32_t)L_42) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_43)->max_length)))))))
		{
			goto IL_0120;
		}
	}
	{
		Fsm_t1527112426 * L_44 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_45 = __this->get_finishedEvent_10();
		NullCheck(L_44);
		Fsm_Event_m625948263(L_44, L_45, /*hidden argument*/NULL);
		__this->set_nextItemIndex_22(0);
		return;
	}

IL_0120:
	{
		int32_t L_46 = __this->get_nextItemIndex_22();
		__this->set_nextItemIndex_22(((int32_t)((int32_t)L_46+(int32_t)1)));
		FsmEvent_t2133468028 * L_47 = __this->get_loopEvent_9();
		if (!L_47)
		{
			goto IL_014a;
		}
	}
	{
		Fsm_t1527112426 * L_48 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_49 = __this->get_loopEvent_9();
		NullCheck(L_48);
		Fsm_Event_m625948263(L_48, L_49, /*hidden argument*/NULL);
	}

IL_014a:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MasterServerGetProperties::.ctor()
extern "C"  void MasterServerGetProperties__ctor_m384751428 (MasterServerGetProperties_t3166189106 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MasterServerGetProperties::Reset()
extern "C"  void MasterServerGetProperties_Reset_m2326151665 (MasterServerGetProperties_t3166189106 * __this, const MethodInfo* method)
{
	{
		__this->set_ipAddress_9((FsmString_t952858651 *)NULL);
		__this->set_port_10((FsmInt_t1596138449 *)NULL);
		__this->set_updateRate_11((FsmInt_t1596138449 *)NULL);
		__this->set_dedicatedServer_12((FsmBool_t1075959796 *)NULL);
		__this->set_isDedicatedServerEvent_13((FsmEvent_t2133468028 *)NULL);
		__this->set_isNotDedicatedServerEvent_14((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MasterServerGetProperties::OnEnter()
extern "C"  void MasterServerGetProperties_OnEnter_m4065630747 (MasterServerGetProperties_t3166189106 * __this, const MethodInfo* method)
{
	{
		MasterServerGetProperties_GetMasterServerProperties_m20900016(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MasterServerGetProperties::GetMasterServerProperties()
extern "C"  void MasterServerGetProperties_GetMasterServerProperties_m20900016 (MasterServerGetProperties_t3166189106 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		FsmString_t952858651 * L_0 = __this->get_ipAddress_9();
		String_t* L_1 = MasterServer_get_ipAddress_m3869729575(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmString_set_Value_m829393196(L_0, L_1, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_2 = __this->get_port_10();
		int32_t L_3 = MasterServer_get_port_m1334239910(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmInt_set_Value_m2087583461(L_2, L_3, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_4 = __this->get_updateRate_11();
		int32_t L_5 = MasterServer_get_updateRate_m3442346606(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		FsmInt_set_Value_m2087583461(L_4, L_5, /*hidden argument*/NULL);
		bool L_6 = MasterServer_get_dedicatedServer_m2497971081(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_6;
		FsmBool_t1075959796 * L_7 = __this->get_dedicatedServer_12();
		bool L_8 = V_0;
		NullCheck(L_7);
		FsmBool_set_Value_m1126216340(L_7, L_8, /*hidden argument*/NULL);
		bool L_9 = V_0;
		if (!L_9)
		{
			goto IL_0064;
		}
	}
	{
		FsmEvent_t2133468028 * L_10 = __this->get_isDedicatedServerEvent_13();
		if (!L_10)
		{
			goto IL_0064;
		}
	}
	{
		Fsm_t1527112426 * L_11 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_12 = __this->get_isDedicatedServerEvent_13();
		NullCheck(L_11);
		Fsm_Event_m625948263(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0064:
	{
		bool L_13 = V_0;
		if (L_13)
		{
			goto IL_0086;
		}
	}
	{
		FsmEvent_t2133468028 * L_14 = __this->get_isNotDedicatedServerEvent_14();
		if (!L_14)
		{
			goto IL_0086;
		}
	}
	{
		Fsm_t1527112426 * L_15 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_16 = __this->get_isNotDedicatedServerEvent_14();
		NullCheck(L_15);
		Fsm_Event_m625948263(L_15, L_16, /*hidden argument*/NULL);
	}

IL_0086:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MasterServerRegisterHost::.ctor()
extern "C"  void MasterServerRegisterHost__ctor_m3535526638 (MasterServerRegisterHost_t3580704376 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MasterServerRegisterHost::Reset()
extern "C"  void MasterServerRegisterHost_Reset_m1181959579 (MasterServerRegisterHost_t3580704376 * __this, const MethodInfo* method)
{
	{
		__this->set_gameTypeName_9((FsmString_t952858651 *)NULL);
		__this->set_gameName_10((FsmString_t952858651 *)NULL);
		__this->set_comment_11((FsmString_t952858651 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MasterServerRegisterHost::OnEnter()
extern "C"  void MasterServerRegisterHost_OnEnter_m4008663877 (MasterServerRegisterHost_t3580704376 * __this, const MethodInfo* method)
{
	{
		MasterServerRegisterHost_DoMasterServerRegisterHost_m1113379729(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MasterServerRegisterHost::DoMasterServerRegisterHost()
extern "C"  void MasterServerRegisterHost_DoMasterServerRegisterHost_m1113379729 (MasterServerRegisterHost_t3580704376 * __this, const MethodInfo* method)
{
	{
		FsmString_t952858651 * L_0 = __this->get_gameTypeName_9();
		NullCheck(L_0);
		String_t* L_1 = FsmString_get_Value_m872383149(L_0, /*hidden argument*/NULL);
		FsmString_t952858651 * L_2 = __this->get_gameName_10();
		NullCheck(L_2);
		String_t* L_3 = FsmString_get_Value_m872383149(L_2, /*hidden argument*/NULL);
		FsmString_t952858651 * L_4 = __this->get_comment_11();
		NullCheck(L_4);
		String_t* L_5 = FsmString_get_Value_m872383149(L_4, /*hidden argument*/NULL);
		MasterServer_RegisterHost_m2388323251(NULL /*static, unused*/, L_1, L_3, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MasterServerRequestHostList::.ctor()
extern "C"  void MasterServerRequestHostList__ctor_m3999972696 (MasterServerRequestHostList_t4119465886 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MasterServerRequestHostList::Reset()
extern "C"  void MasterServerRequestHostList_Reset_m1646405637 (MasterServerRequestHostList_t4119465886 * __this, const MethodInfo* method)
{
	{
		__this->set_gameTypeName_9((FsmString_t952858651 *)NULL);
		__this->set_HostListArrivedEvent_10((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MasterServerRequestHostList::OnEnter()
extern "C"  void MasterServerRequestHostList_OnEnter_m3664726831 (MasterServerRequestHostList_t4119465886 * __this, const MethodInfo* method)
{
	{
		MasterServerRequestHostList_DoMasterServerRequestHost_m3691053693(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MasterServerRequestHostList::OnUpdate()
extern "C"  void MasterServerRequestHostList_OnUpdate_m1070941428 (MasterServerRequestHostList_t4119465886 * __this, const MethodInfo* method)
{
	{
		MasterServerRequestHostList_WatchServerRequestHost_m3633021809(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MasterServerRequestHostList::DoMasterServerRequestHost()
extern "C"  void MasterServerRequestHostList_DoMasterServerRequestHost_m3691053693 (MasterServerRequestHostList_t4119465886 * __this, const MethodInfo* method)
{
	{
		MasterServer_ClearHostList_m2859464953(NULL /*static, unused*/, /*hidden argument*/NULL);
		FsmString_t952858651 * L_0 = __this->get_gameTypeName_9();
		NullCheck(L_0);
		String_t* L_1 = FsmString_get_Value_m872383149(L_0, /*hidden argument*/NULL);
		MasterServer_RequestHostList_m2054420839(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MasterServerRequestHostList::WatchServerRequestHost()
extern "C"  void MasterServerRequestHostList_WatchServerRequestHost_m3633021809 (MasterServerRequestHostList_t4119465886 * __this, const MethodInfo* method)
{
	{
		HostDataU5BU5D_t4021970803* L_0 = MasterServer_PollHostList_m3223787845(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		if (!(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length)))))
		{
			goto IL_0023;
		}
	}
	{
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_2 = __this->get_HostListArrivedEvent_10();
		NullCheck(L_1);
		Fsm_Event_m625948263(L_1, L_2, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MasterServerSetProperties::.ctor()
extern "C"  void MasterServerSetProperties__ctor_m1697879736 (MasterServerSetProperties_t3940320318 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MasterServerSetProperties::Reset()
extern Il2CppCodeGenString* _stringLiteral1505998205;
extern const uint32_t MasterServerSetProperties_Reset_m3639279973_MetadataUsageId;
extern "C"  void MasterServerSetProperties_Reset_m3639279973 (MasterServerSetProperties_t3940320318 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MasterServerSetProperties_Reset_m3639279973_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmString_t952858651 * L_0 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, _stringLiteral1505998205, /*hidden argument*/NULL);
		__this->set_ipAddress_9(L_0);
		FsmInt_t1596138449 * L_1 = FsmInt_op_Implicit_m1006909518(NULL /*static, unused*/, ((int32_t)10002), /*hidden argument*/NULL);
		__this->set_port_10(L_1);
		FsmInt_t1596138449 * L_2 = FsmInt_op_Implicit_m1006909518(NULL /*static, unused*/, ((int32_t)60), /*hidden argument*/NULL);
		__this->set_updateRate_11(L_2);
		FsmBool_t1075959796 * L_3 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_dedicatedServer_12(L_3);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MasterServerSetProperties::OnEnter()
extern "C"  void MasterServerSetProperties_OnEnter_m3261549711 (MasterServerSetProperties_t3940320318 * __this, const MethodInfo* method)
{
	{
		MasterServerSetProperties_SetMasterServerProperties_m164245296(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MasterServerSetProperties::SetMasterServerProperties()
extern "C"  void MasterServerSetProperties_SetMasterServerProperties_m164245296 (MasterServerSetProperties_t3940320318 * __this, const MethodInfo* method)
{
	{
		FsmString_t952858651 * L_0 = __this->get_ipAddress_9();
		NullCheck(L_0);
		String_t* L_1 = FsmString_get_Value_m872383149(L_0, /*hidden argument*/NULL);
		MasterServer_set_ipAddress_m4189760396(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_2 = __this->get_port_10();
		NullCheck(L_2);
		int32_t L_3 = FsmInt_get_Value_m27059446(L_2, /*hidden argument*/NULL);
		MasterServer_set_port_m850451947(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_4 = __this->get_updateRate_11();
		NullCheck(L_4);
		int32_t L_5 = FsmInt_get_Value_m27059446(L_4, /*hidden argument*/NULL);
		MasterServer_set_updateRate_m2854261427(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_6 = __this->get_dedicatedServer_12();
		NullCheck(L_6);
		bool L_7 = FsmBool_get_Value_m3101329097(L_6, /*hidden argument*/NULL);
		MasterServer_set_dedicatedServer_m1888993138(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MasterServerUnregisterHost::.ctor()
extern "C"  void MasterServerUnregisterHost__ctor_m2418978933 (MasterServerUnregisterHost_t3653261073 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MasterServerUnregisterHost::OnEnter()
extern "C"  void MasterServerUnregisterHost_OnEnter_m453176076 (MasterServerUnregisterHost_t3653261073 * __this, const MethodInfo* method)
{
	{
		MasterServer_UnregisterHost_m864917440(NULL /*static, unused*/, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MouseLook::.ctor()
extern "C"  void MouseLook__ctor_m4153572676 (MouseLook_t696688690 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MouseLook::Reset()
extern Il2CppClass* FsmFloat_t2134102846_il2cpp_TypeInfo_var;
extern const uint32_t MouseLook_Reset_m1800005617_MetadataUsageId;
extern "C"  void MouseLook_Reset_m1800005617 (MouseLook_t696688690 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MouseLook_Reset_m1800005617_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmFloat_t2134102846 * V_0 = NULL;
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_axes_10(0);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (15.0f), /*hidden argument*/NULL);
		__this->set_sensitivityX_11(L_0);
		FsmFloat_t2134102846 * L_1 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (15.0f), /*hidden argument*/NULL);
		__this->set_sensitivityY_12(L_1);
		FsmFloat_t2134102846 * L_2 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		FsmFloat_t2134102846 * L_3 = V_0;
		NullCheck(L_3);
		NamedVariable_set_UseVariable_m4266138971(L_3, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_4 = V_0;
		__this->set_minimumX_13(L_4);
		FsmFloat_t2134102846 * L_5 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_5, /*hidden argument*/NULL);
		V_0 = L_5;
		FsmFloat_t2134102846 * L_6 = V_0;
		NullCheck(L_6);
		NamedVariable_set_UseVariable_m4266138971(L_6, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_7 = V_0;
		__this->set_maximumX_14(L_7);
		FsmFloat_t2134102846 * L_8 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (-60.0f), /*hidden argument*/NULL);
		__this->set_minimumY_15(L_8);
		FsmFloat_t2134102846 * L_9 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (60.0f), /*hidden argument*/NULL);
		__this->set_maximumY_16(L_9);
		__this->set_everyFrame_17((bool)1);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MouseLook::OnEnter()
extern const MethodInfo* GameObject_GetComponent_TisRigidbody_t3346577219_m1334743405_MethodInfo_var;
extern const uint32_t MouseLook_OnEnter_m950452251_MetadataUsageId;
extern "C"  void MouseLook_OnEnter_m950452251 (MouseLook_t696688690 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MouseLook_OnEnter_m950452251_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Rigidbody_t3346577219 * V_1 = NULL;
	Quaternion_t1553702882  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Quaternion_t1553702882  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t4282066566  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Rigidbody_t3346577219 * L_6 = GameObject_GetComponent_TisRigidbody_t3346577219_m1334743405(L_5, /*hidden argument*/GameObject_GetComponent_TisRigidbody_t3346577219_m1334743405_MethodInfo_var);
		V_1 = L_6;
		Rigidbody_t3346577219 * L_7 = V_1;
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_003f;
		}
	}
	{
		Rigidbody_t3346577219 * L_9 = V_1;
		NullCheck(L_9);
		Rigidbody_set_freezeRotation_m3989473889(L_9, (bool)1, /*hidden argument*/NULL);
	}

IL_003f:
	{
		GameObject_t3674682005 * L_10 = V_0;
		NullCheck(L_10);
		Transform_t1659122786 * L_11 = GameObject_get_transform_m1278640159(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Quaternion_t1553702882  L_12 = Transform_get_localRotation_m3343229381(L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		Vector3_t4282066566  L_13 = Quaternion_get_eulerAngles_m997303795((&V_2), /*hidden argument*/NULL);
		V_3 = L_13;
		float L_14 = (&V_3)->get_y_2();
		__this->set_rotationX_18(L_14);
		GameObject_t3674682005 * L_15 = V_0;
		NullCheck(L_15);
		Transform_t1659122786 * L_16 = GameObject_get_transform_m1278640159(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		Quaternion_t1553702882  L_17 = Transform_get_localRotation_m3343229381(L_16, /*hidden argument*/NULL);
		V_4 = L_17;
		Vector3_t4282066566  L_18 = Quaternion_get_eulerAngles_m997303795((&V_4), /*hidden argument*/NULL);
		V_5 = L_18;
		float L_19 = (&V_5)->get_x_1();
		__this->set_rotationY_19(L_19);
		MouseLook_DoMouseLook_m1084452859(__this, /*hidden argument*/NULL);
		bool L_20 = __this->get_everyFrame_17();
		if (L_20)
		{
			goto IL_009a;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_009a:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MouseLook::OnUpdate()
extern "C"  void MouseLook_OnUpdate_m2827775368 (MouseLook_t696688690 * __this, const MethodInfo* method)
{
	{
		MouseLook_DoMouseLook_m1084452859(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MouseLook::DoMouseLook()
extern "C"  void MouseLook_DoMouseLook_m1084452859 (MouseLook_t696688690 * __this, const MethodInfo* method)
{
	GameObject_t3674682005 * V_0 = NULL;
	Transform_t1659122786 * V_1 = NULL;
	int32_t V_2 = 0;
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t4282066566  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Transform_t1659122786 * L_6 = GameObject_get_transform_m1278640159(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		int32_t L_7 = __this->get_axes_10();
		V_2 = L_7;
		int32_t L_8 = V_2;
		if (L_8 == 0)
		{
			goto IL_0044;
		}
		if (L_8 == 1)
		{
			goto IL_0065;
		}
		if (L_8 == 2)
		{
			goto IL_008e;
		}
	}
	{
		goto IL_00b9;
	}

IL_0044:
	{
		Transform_t1659122786 * L_9 = V_1;
		float L_10 = MouseLook_GetYRotation_m3934305877(__this, /*hidden argument*/NULL);
		float L_11 = MouseLook_GetXRotation_m1436289940(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector3__ctor_m2926210380(&L_12, L_10, L_11, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_set_localEulerAngles_m3898859559(L_9, L_12, /*hidden argument*/NULL);
		goto IL_00b9;
	}

IL_0065:
	{
		Transform_t1659122786 * L_13 = V_1;
		Transform_t1659122786 * L_14 = V_1;
		NullCheck(L_14);
		Vector3_t4282066566  L_15 = Transform_get_localEulerAngles_m3489183428(L_14, /*hidden argument*/NULL);
		V_3 = L_15;
		float L_16 = (&V_3)->get_x_1();
		float L_17 = MouseLook_GetXRotation_m1436289940(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector3__ctor_m2926210380(&L_18, L_16, L_17, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_set_localEulerAngles_m3898859559(L_13, L_18, /*hidden argument*/NULL);
		goto IL_00b9;
	}

IL_008e:
	{
		Transform_t1659122786 * L_19 = V_1;
		float L_20 = MouseLook_GetYRotation_m3934305877(__this, /*hidden argument*/NULL);
		Transform_t1659122786 * L_21 = V_1;
		NullCheck(L_21);
		Vector3_t4282066566  L_22 = Transform_get_localEulerAngles_m3489183428(L_21, /*hidden argument*/NULL);
		V_4 = L_22;
		float L_23 = (&V_4)->get_y_2();
		Vector3_t4282066566  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Vector3__ctor_m2926210380(&L_24, ((-L_20)), L_23, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_19);
		Transform_set_localEulerAngles_m3898859559(L_19, L_24, /*hidden argument*/NULL);
		goto IL_00b9;
	}

IL_00b9:
	{
		return;
	}
}
// System.Single HutongGames.PlayMaker.Actions.MouseLook::GetXRotation()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2907718525;
extern const uint32_t MouseLook_GetXRotation_m1436289940_MetadataUsageId;
extern "C"  float MouseLook_GetXRotation_m1436289940 (MouseLook_t696688690 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MouseLook_GetXRotation_m1436289940_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_rotationX_18();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		float L_1 = Input_GetAxis_m2027668530(NULL /*static, unused*/, _stringLiteral2907718525, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_2 = __this->get_sensitivityX_11();
		NullCheck(L_2);
		float L_3 = FsmFloat_get_Value_m4137923823(L_2, /*hidden argument*/NULL);
		__this->set_rotationX_18(((float)((float)L_0+(float)((float)((float)L_1*(float)L_3)))));
		float L_4 = __this->get_rotationX_18();
		FsmFloat_t2134102846 * L_5 = __this->get_minimumX_13();
		FsmFloat_t2134102846 * L_6 = __this->get_maximumX_14();
		float L_7 = MouseLook_ClampAngle_m1957514079(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		__this->set_rotationX_18(L_7);
		float L_8 = __this->get_rotationX_18();
		return L_8;
	}
}
// System.Single HutongGames.PlayMaker.Actions.MouseLook::GetYRotation()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2907718526;
extern const uint32_t MouseLook_GetYRotation_m3934305877_MetadataUsageId;
extern "C"  float MouseLook_GetYRotation_m3934305877 (MouseLook_t696688690 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MouseLook_GetYRotation_m3934305877_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_rotationY_19();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		float L_1 = Input_GetAxis_m2027668530(NULL /*static, unused*/, _stringLiteral2907718526, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_2 = __this->get_sensitivityY_12();
		NullCheck(L_2);
		float L_3 = FsmFloat_get_Value_m4137923823(L_2, /*hidden argument*/NULL);
		__this->set_rotationY_19(((float)((float)L_0+(float)((float)((float)L_1*(float)L_3)))));
		float L_4 = __this->get_rotationY_19();
		FsmFloat_t2134102846 * L_5 = __this->get_minimumY_15();
		FsmFloat_t2134102846 * L_6 = __this->get_maximumY_16();
		float L_7 = MouseLook_ClampAngle_m1957514079(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		__this->set_rotationY_19(L_7);
		float L_8 = __this->get_rotationY_19();
		return L_8;
	}
}
// System.Single HutongGames.PlayMaker.Actions.MouseLook::ClampAngle(System.Single,HutongGames.PlayMaker.FsmFloat,HutongGames.PlayMaker.FsmFloat)
extern "C"  float MouseLook_ClampAngle_m1957514079 (Il2CppObject * __this /* static, unused */, float ___angle0, FsmFloat_t2134102846 * ___min1, FsmFloat_t2134102846 * ___max2, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = ___min1;
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001f;
		}
	}
	{
		float L_2 = ___angle0;
		FsmFloat_t2134102846 * L_3 = ___min1;
		NullCheck(L_3);
		float L_4 = FsmFloat_get_Value_m4137923823(L_3, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_4))))
		{
			goto IL_001f;
		}
	}
	{
		FsmFloat_t2134102846 * L_5 = ___min1;
		NullCheck(L_5);
		float L_6 = FsmFloat_get_Value_m4137923823(L_5, /*hidden argument*/NULL);
		___angle0 = L_6;
	}

IL_001f:
	{
		FsmFloat_t2134102846 * L_7 = ___max2;
		NullCheck(L_7);
		bool L_8 = NamedVariable_get_IsNone_m281035543(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_003e;
		}
	}
	{
		float L_9 = ___angle0;
		FsmFloat_t2134102846 * L_10 = ___max2;
		NullCheck(L_10);
		float L_11 = FsmFloat_get_Value_m4137923823(L_10, /*hidden argument*/NULL);
		if ((!(((float)L_9) > ((float)L_11))))
		{
			goto IL_003e;
		}
	}
	{
		FsmFloat_t2134102846 * L_12 = ___max2;
		NullCheck(L_12);
		float L_13 = FsmFloat_get_Value_m4137923823(L_12, /*hidden argument*/NULL);
		___angle0 = L_13;
	}

IL_003e:
	{
		float L_14 = ___angle0;
		return L_14;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MouseLook2::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m3497509022_MethodInfo_var;
extern const uint32_t MouseLook2__ctor_m647578608_MetadataUsageId;
extern "C"  void MouseLook2__ctor_m647578608 (MouseLook2_t3768519990 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MouseLook2__ctor_m647578608_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m3497509022(__this, /*hidden argument*/ComponentAction_1__ctor_m3497509022_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MouseLook2::Reset()
extern "C"  void MouseLook2_Reset_m2588978845 (MouseLook2_t3768519990 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_axes_12(0);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (15.0f), /*hidden argument*/NULL);
		__this->set_sensitivityX_13(L_0);
		FsmFloat_t2134102846 * L_1 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (15.0f), /*hidden argument*/NULL);
		__this->set_sensitivityY_14(L_1);
		FsmFloat_t2134102846 * L_2 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (-360.0f), /*hidden argument*/NULL);
		__this->set_minimumX_15(L_2);
		FsmFloat_t2134102846 * L_3 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (360.0f), /*hidden argument*/NULL);
		__this->set_maximumX_16(L_3);
		FsmFloat_t2134102846 * L_4 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (-60.0f), /*hidden argument*/NULL);
		__this->set_minimumY_17(L_4);
		FsmFloat_t2134102846 * L_5 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (60.0f), /*hidden argument*/NULL);
		__this->set_maximumY_18(L_5);
		__this->set_everyFrame_19((bool)1);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MouseLook2::OnEnter()
extern const MethodInfo* ComponentAction_1_UpdateCache_m864821753_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_rigidbody_m1866302531_MethodInfo_var;
extern const uint32_t MouseLook2_OnEnter_m3239480263_MetadataUsageId;
extern "C"  void MouseLook2_OnEnter_m3239480263 (MouseLook2_t3768519990 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MouseLook2_OnEnter_m3239480263_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		bool L_6 = ComponentAction_1_UpdateCache_m864821753(__this, L_5, /*hidden argument*/ComponentAction_1_UpdateCache_m864821753_MethodInfo_var);
		if (L_6)
		{
			goto IL_004d;
		}
	}
	{
		Rigidbody_t3346577219 * L_7 = ComponentAction_1_get_rigidbody_m1866302531(__this, /*hidden argument*/ComponentAction_1_get_rigidbody_m1866302531_MethodInfo_var);
		bool L_8 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004d;
		}
	}
	{
		Rigidbody_t3346577219 * L_9 = ComponentAction_1_get_rigidbody_m1866302531(__this, /*hidden argument*/ComponentAction_1_get_rigidbody_m1866302531_MethodInfo_var);
		NullCheck(L_9);
		Rigidbody_set_freezeRotation_m3989473889(L_9, (bool)1, /*hidden argument*/NULL);
	}

IL_004d:
	{
		MouseLook2_DoMouseLook_m799901095(__this, /*hidden argument*/NULL);
		bool L_10 = __this->get_everyFrame_19();
		if (L_10)
		{
			goto IL_0064;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0064:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MouseLook2::OnUpdate()
extern "C"  void MouseLook2_OnUpdate_m773199708 (MouseLook2_t3768519990 * __this, const MethodInfo* method)
{
	{
		MouseLook2_DoMouseLook_m799901095(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MouseLook2::DoMouseLook()
extern "C"  void MouseLook2_DoMouseLook_m799901095 (MouseLook2_t3768519990 * __this, const MethodInfo* method)
{
	GameObject_t3674682005 * V_0 = NULL;
	Transform_t1659122786 * V_1 = NULL;
	int32_t V_2 = 0;
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t4282066566  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Transform_t1659122786 * L_6 = GameObject_get_transform_m1278640159(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		int32_t L_7 = __this->get_axes_12();
		V_2 = L_7;
		int32_t L_8 = V_2;
		if (L_8 == 0)
		{
			goto IL_0044;
		}
		if (L_8 == 1)
		{
			goto IL_0065;
		}
		if (L_8 == 2)
		{
			goto IL_008e;
		}
	}
	{
		goto IL_00b9;
	}

IL_0044:
	{
		Transform_t1659122786 * L_9 = V_1;
		float L_10 = MouseLook2_GetYRotation_m165689665(__this, /*hidden argument*/NULL);
		float L_11 = MouseLook2_GetXRotation_m1962641024(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector3__ctor_m2926210380(&L_12, L_10, L_11, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_set_localEulerAngles_m3898859559(L_9, L_12, /*hidden argument*/NULL);
		goto IL_00b9;
	}

IL_0065:
	{
		Transform_t1659122786 * L_13 = V_1;
		Transform_t1659122786 * L_14 = V_1;
		NullCheck(L_14);
		Vector3_t4282066566  L_15 = Transform_get_localEulerAngles_m3489183428(L_14, /*hidden argument*/NULL);
		V_3 = L_15;
		float L_16 = (&V_3)->get_x_1();
		float L_17 = MouseLook2_GetXRotation_m1962641024(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector3__ctor_m2926210380(&L_18, L_16, L_17, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_set_localEulerAngles_m3898859559(L_13, L_18, /*hidden argument*/NULL);
		goto IL_00b9;
	}

IL_008e:
	{
		Transform_t1659122786 * L_19 = V_1;
		float L_20 = MouseLook2_GetYRotation_m165689665(__this, /*hidden argument*/NULL);
		Transform_t1659122786 * L_21 = V_1;
		NullCheck(L_21);
		Vector3_t4282066566  L_22 = Transform_get_localEulerAngles_m3489183428(L_21, /*hidden argument*/NULL);
		V_4 = L_22;
		float L_23 = (&V_4)->get_y_2();
		Vector3_t4282066566  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Vector3__ctor_m2926210380(&L_24, ((-L_20)), L_23, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_19);
		Transform_set_localEulerAngles_m3898859559(L_19, L_24, /*hidden argument*/NULL);
		goto IL_00b9;
	}

IL_00b9:
	{
		return;
	}
}
// System.Single HutongGames.PlayMaker.Actions.MouseLook2::GetXRotation()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2907718525;
extern const uint32_t MouseLook2_GetXRotation_m1962641024_MetadataUsageId;
extern "C"  float MouseLook2_GetXRotation_m1962641024 (MouseLook2_t3768519990 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MouseLook2_GetXRotation_m1962641024_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_rotationX_20();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		float L_1 = Input_GetAxis_m2027668530(NULL /*static, unused*/, _stringLiteral2907718525, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_2 = __this->get_sensitivityX_13();
		NullCheck(L_2);
		float L_3 = FsmFloat_get_Value_m4137923823(L_2, /*hidden argument*/NULL);
		__this->set_rotationX_20(((float)((float)L_0+(float)((float)((float)L_1*(float)L_3)))));
		float L_4 = __this->get_rotationX_20();
		FsmFloat_t2134102846 * L_5 = __this->get_minimumX_15();
		FsmFloat_t2134102846 * L_6 = __this->get_maximumX_16();
		float L_7 = MouseLook2_ClampAngle_m3742153203(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		__this->set_rotationX_20(L_7);
		float L_8 = __this->get_rotationX_20();
		return L_8;
	}
}
// System.Single HutongGames.PlayMaker.Actions.MouseLook2::GetYRotation()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2907718526;
extern const uint32_t MouseLook2_GetYRotation_m165689665_MetadataUsageId;
extern "C"  float MouseLook2_GetYRotation_m165689665 (MouseLook2_t3768519990 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MouseLook2_GetYRotation_m165689665_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_rotationY_21();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		float L_1 = Input_GetAxis_m2027668530(NULL /*static, unused*/, _stringLiteral2907718526, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_2 = __this->get_sensitivityY_14();
		NullCheck(L_2);
		float L_3 = FsmFloat_get_Value_m4137923823(L_2, /*hidden argument*/NULL);
		__this->set_rotationY_21(((float)((float)L_0+(float)((float)((float)L_1*(float)L_3)))));
		float L_4 = __this->get_rotationY_21();
		FsmFloat_t2134102846 * L_5 = __this->get_minimumY_17();
		FsmFloat_t2134102846 * L_6 = __this->get_maximumY_18();
		float L_7 = MouseLook2_ClampAngle_m3742153203(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		__this->set_rotationY_21(L_7);
		float L_8 = __this->get_rotationY_21();
		return L_8;
	}
}
// System.Single HutongGames.PlayMaker.Actions.MouseLook2::ClampAngle(System.Single,HutongGames.PlayMaker.FsmFloat,HutongGames.PlayMaker.FsmFloat)
extern "C"  float MouseLook2_ClampAngle_m3742153203 (Il2CppObject * __this /* static, unused */, float ___angle0, FsmFloat_t2134102846 * ___min1, FsmFloat_t2134102846 * ___max2, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = ___min1;
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001f;
		}
	}
	{
		float L_2 = ___angle0;
		FsmFloat_t2134102846 * L_3 = ___min1;
		NullCheck(L_3);
		float L_4 = FsmFloat_get_Value_m4137923823(L_3, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_4))))
		{
			goto IL_001f;
		}
	}
	{
		FsmFloat_t2134102846 * L_5 = ___min1;
		NullCheck(L_5);
		float L_6 = FsmFloat_get_Value_m4137923823(L_5, /*hidden argument*/NULL);
		___angle0 = L_6;
	}

IL_001f:
	{
		FsmFloat_t2134102846 * L_7 = ___max2;
		NullCheck(L_7);
		bool L_8 = NamedVariable_get_IsNone_m281035543(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_003e;
		}
	}
	{
		float L_9 = ___angle0;
		FsmFloat_t2134102846 * L_10 = ___max2;
		NullCheck(L_10);
		float L_11 = FsmFloat_get_Value_m4137923823(L_10, /*hidden argument*/NULL);
		if ((!(((float)L_9) > ((float)L_11))))
		{
			goto IL_003e;
		}
	}
	{
		FsmFloat_t2134102846 * L_12 = ___max2;
		NullCheck(L_12);
		float L_13 = FsmFloat_get_Value_m4137923823(L_12, /*hidden argument*/NULL);
		___angle0 = L_13;
	}

IL_003e:
	{
		float L_14 = ___angle0;
		return L_14;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MousePick::.ctor()
extern "C"  void MousePick__ctor_m2294044162 (MousePick_t696801716 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (100.0f), /*hidden argument*/NULL);
		__this->set_rayDistance_9(L_0);
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MousePick::Reset()
extern Il2CppClass* FsmIntU5BU5D_t1976821196_il2cpp_TypeInfo_var;
extern const uint32_t MousePick_Reset_m4235444399_MetadataUsageId;
extern "C"  void MousePick_Reset_m4235444399 (MousePick_t696801716 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MousePick_Reset_m4235444399_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (100.0f), /*hidden argument*/NULL);
		__this->set_rayDistance_9(L_0);
		__this->set_storeDidPickObject_10((FsmBool_t1075959796 *)NULL);
		__this->set_storeGameObject_11((FsmGameObject_t1697147867 *)NULL);
		__this->set_storePoint_12((FsmVector3_t533912882 *)NULL);
		__this->set_storeNormal_13((FsmVector3_t533912882 *)NULL);
		__this->set_storeDistance_14((FsmFloat_t2134102846 *)NULL);
		__this->set_layerMask_15(((FsmIntU5BU5D_t1976821196*)SZArrayNew(FsmIntU5BU5D_t1976821196_il2cpp_TypeInfo_var, (uint32_t)0)));
		FsmBool_t1075959796 * L_1 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_invertMask_16(L_1);
		__this->set_everyFrame_17((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MousePick::OnEnter()
extern "C"  void MousePick_OnEnter_m649945433 (MousePick_t696801716 * __this, const MethodInfo* method)
{
	{
		MousePick_DoMousePick_m442803003(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_17();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MousePick::OnUpdate()
extern "C"  void MousePick_OnUpdate_m2101998602 (MousePick_t696801716 * __this, const MethodInfo* method)
{
	{
		MousePick_DoMousePick_m442803003(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MousePick::DoMousePick()
extern "C"  void MousePick_DoMousePick_m442803003 (MousePick_t696801716 * __this, const MethodInfo* method)
{
	RaycastHit_t4003175726  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	{
		FsmFloat_t2134102846 * L_0 = __this->get_rayDistance_9();
		NullCheck(L_0);
		float L_1 = FsmFloat_get_Value_m4137923823(L_0, /*hidden argument*/NULL);
		FsmIntU5BU5D_t1976821196* L_2 = __this->get_layerMask_15();
		FsmBool_t1075959796 * L_3 = __this->get_invertMask_16();
		NullCheck(L_3);
		bool L_4 = FsmBool_get_Value_m3101329097(L_3, /*hidden argument*/NULL);
		int32_t L_5 = ActionHelpers_LayerArrayToLayerMask_m3090395306(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		RaycastHit_t4003175726  L_6 = ActionHelpers_MousePick_m4005175030(NULL /*static, unused*/, L_1, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		Collider_t2939674232 * L_7 = RaycastHit_get_collider_m3116882274((&V_0), /*hidden argument*/NULL);
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		V_1 = L_8;
		FsmBool_t1075959796 * L_9 = __this->get_storeDidPickObject_10();
		bool L_10 = V_1;
		NullCheck(L_9);
		FsmBool_set_Value_m1126216340(L_9, L_10, /*hidden argument*/NULL);
		bool L_11 = V_1;
		if (!L_11)
		{
			goto IL_0099;
		}
	}
	{
		FsmGameObject_t1697147867 * L_12 = __this->get_storeGameObject_11();
		Collider_t2939674232 * L_13 = RaycastHit_get_collider_m3116882274((&V_0), /*hidden argument*/NULL);
		NullCheck(L_13);
		GameObject_t3674682005 * L_14 = Component_get_gameObject_m1170635899(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		FsmGameObject_set_Value_m297051598(L_12, L_14, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_15 = __this->get_storeDistance_14();
		float L_16 = RaycastHit_get_distance_m800944203((&V_0), /*hidden argument*/NULL);
		NullCheck(L_15);
		FsmFloat_set_Value_m1568963140(L_15, L_16, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_17 = __this->get_storePoint_12();
		Vector3_t4282066566  L_18 = RaycastHit_get_point_m4165497838((&V_0), /*hidden argument*/NULL);
		NullCheck(L_17);
		FsmVector3_set_Value_m716982822(L_17, L_18, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_19 = __this->get_storeNormal_13();
		Vector3_t4282066566  L_20 = RaycastHit_get_normal_m1346998891((&V_0), /*hidden argument*/NULL);
		NullCheck(L_19);
		FsmVector3_set_Value_m716982822(L_19, L_20, /*hidden argument*/NULL);
		goto IL_00d5;
	}

IL_0099:
	{
		FsmGameObject_t1697147867 * L_21 = __this->get_storeGameObject_11();
		NullCheck(L_21);
		FsmGameObject_set_Value_m297051598(L_21, (GameObject_t3674682005 *)NULL, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_22 = __this->get_storeDistance_14();
		NullCheck(L_22);
		FsmFloat_set_Value_m1568963140(L_22, (std::numeric_limits<float>::infinity()), /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_23 = __this->get_storePoint_12();
		Vector3_t4282066566  L_24 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_23);
		FsmVector3_set_Value_m716982822(L_23, L_24, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_25 = __this->get_storeNormal_13();
		Vector3_t4282066566  L_26 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_25);
		FsmVector3_set_Value_m716982822(L_25, L_26, /*hidden argument*/NULL);
	}

IL_00d5:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MousePickEvent::.ctor()
extern "C"  void MousePickEvent__ctor_m884406570 (MousePickEvent_t909730492 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (100.0f), /*hidden argument*/NULL);
		__this->set_rayDistance_10(L_0);
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MousePickEvent::Reset()
extern Il2CppClass* FsmIntU5BU5D_t1976821196_il2cpp_TypeInfo_var;
extern const uint32_t MousePickEvent_Reset_m2825806807_MetadataUsageId;
extern "C"  void MousePickEvent_Reset_m2825806807 (MousePickEvent_t909730492 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MousePickEvent_Reset_m2825806807_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_GameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (100.0f), /*hidden argument*/NULL);
		__this->set_rayDistance_10(L_0);
		__this->set_mouseOver_11((FsmEvent_t2133468028 *)NULL);
		__this->set_mouseDown_12((FsmEvent_t2133468028 *)NULL);
		__this->set_mouseUp_13((FsmEvent_t2133468028 *)NULL);
		__this->set_mouseOff_14((FsmEvent_t2133468028 *)NULL);
		__this->set_layerMask_15(((FsmIntU5BU5D_t1976821196*)SZArrayNew(FsmIntU5BU5D_t1976821196_il2cpp_TypeInfo_var, (uint32_t)0)));
		FsmBool_t1075959796 * L_1 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_invertMask_16(L_1);
		__this->set_everyFrame_17((bool)1);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MousePickEvent::OnEnter()
extern "C"  void MousePickEvent_OnEnter_m3197885057 (MousePickEvent_t909730492 * __this, const MethodInfo* method)
{
	{
		MousePickEvent_DoMousePickEvent_m432495321(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_17();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MousePickEvent::OnUpdate()
extern "C"  void MousePickEvent_OnUpdate_m3778715618 (MousePickEvent_t909730492 * __this, const MethodInfo* method)
{
	{
		MousePickEvent_DoMousePickEvent_m432495321(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MousePickEvent::DoMousePickEvent()
extern Il2CppClass* ActionHelpers_t1898294297_il2cpp_TypeInfo_var;
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t MousePickEvent_DoMousePickEvent_m432495321_MetadataUsageId;
extern "C"  void MousePickEvent_DoMousePickEvent_m432495321 (MousePickEvent_t909730492 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MousePickEvent_DoMousePickEvent_m432495321_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		bool L_0 = MousePickEvent_DoRaycast_m2927267442(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		RaycastHit_t4003175726  L_2 = ((ActionHelpers_t1898294297_StaticFields*)ActionHelpers_t1898294297_il2cpp_TypeInfo_var->static_fields)->get_mousePickInfo_1();
		NullCheck(L_1);
		Fsm_set_RaycastHitInfo_m638262883(L_1, L_2, /*hidden argument*/NULL);
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_008c;
		}
	}
	{
		FsmEvent_t2133468028 * L_4 = __this->get_mouseDown_12();
		if (!L_4)
		{
			goto IL_0044;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_5 = Input_GetMouseButtonDown_m2031691843(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0044;
		}
	}
	{
		Fsm_t1527112426 * L_6 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_7 = __this->get_mouseDown_12();
		NullCheck(L_6);
		Fsm_Event_m625948263(L_6, L_7, /*hidden argument*/NULL);
	}

IL_0044:
	{
		FsmEvent_t2133468028 * L_8 = __this->get_mouseOver_11();
		if (!L_8)
		{
			goto IL_0060;
		}
	}
	{
		Fsm_t1527112426 * L_9 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_10 = __this->get_mouseOver_11();
		NullCheck(L_9);
		Fsm_Event_m625948263(L_9, L_10, /*hidden argument*/NULL);
	}

IL_0060:
	{
		FsmEvent_t2133468028 * L_11 = __this->get_mouseUp_13();
		if (!L_11)
		{
			goto IL_0087;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_12 = Input_GetMouseButtonUp_m2588144188(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0087;
		}
	}
	{
		Fsm_t1527112426 * L_13 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_14 = __this->get_mouseUp_13();
		NullCheck(L_13);
		Fsm_Event_m625948263(L_13, L_14, /*hidden argument*/NULL);
	}

IL_0087:
	{
		goto IL_00a8;
	}

IL_008c:
	{
		FsmEvent_t2133468028 * L_15 = __this->get_mouseOff_14();
		if (!L_15)
		{
			goto IL_00a8;
		}
	}
	{
		Fsm_t1527112426 * L_16 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_17 = __this->get_mouseOff_14();
		NullCheck(L_16);
		Fsm_Event_m625948263(L_16, L_17, /*hidden argument*/NULL);
	}

IL_00a8:
	{
		return;
	}
}
// System.Boolean HutongGames.PlayMaker.Actions.MousePickEvent::DoRaycast()
extern "C"  bool MousePickEvent_DoRaycast_m2927267442 (MousePickEvent_t909730492 * __this, const MethodInfo* method)
{
	GameObject_t3674682005 * V_0 = NULL;
	GameObject_t3674682005 * G_B3_0 = NULL;
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_GameObject_9();
		NullCheck(L_0);
		int32_t L_1 = FsmOwnerDefault_get_OwnerOption_m3357910390(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001b;
		}
	}
	{
		GameObject_t3674682005 * L_2 = FsmStateAction_get_Owner_m1855633209(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_002b;
	}

IL_001b:
	{
		FsmOwnerDefault_t251897112 * L_3 = __this->get_GameObject_9();
		NullCheck(L_3);
		FsmGameObject_t1697147867 * L_4 = FsmOwnerDefault_get_GameObject_m3249227945(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_t3674682005 * L_5 = FsmGameObject_get_Value_m673294275(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
	}

IL_002b:
	{
		V_0 = G_B3_0;
		GameObject_t3674682005 * L_6 = V_0;
		FsmFloat_t2134102846 * L_7 = __this->get_rayDistance_10();
		NullCheck(L_7);
		float L_8 = FsmFloat_get_Value_m4137923823(L_7, /*hidden argument*/NULL);
		FsmIntU5BU5D_t1976821196* L_9 = __this->get_layerMask_15();
		FsmBool_t1075959796 * L_10 = __this->get_invertMask_16();
		NullCheck(L_10);
		bool L_11 = FsmBool_get_Value_m3101329097(L_10, /*hidden argument*/NULL);
		int32_t L_12 = ActionHelpers_LayerArrayToLayerMask_m3090395306(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		bool L_13 = ActionHelpers_IsMouseOver_m3587800431(NULL /*static, unused*/, L_6, L_8, L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.String HutongGames.PlayMaker.Actions.MousePickEvent::ErrorCheck()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t MousePickEvent_ErrorCheck_m2951563357_MetadataUsageId;
extern "C"  String_t* MousePickEvent_ErrorCheck_m2951563357 (MousePickEvent_t909730492 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MousePickEvent_ErrorCheck_m2951563357_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		String_t* L_1 = V_0;
		FsmFloat_t2134102846 * L_2 = __this->get_rayDistance_10();
		NullCheck(L_2);
		float L_3 = FsmFloat_get_Value_m4137923823(L_2, /*hidden argument*/NULL);
		String_t* L_4 = ActionHelpers_CheckRayDistance_m621902585(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		String_t* L_5 = String_Concat_m138640077(NULL /*static, unused*/, L_1, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		String_t* L_6 = V_0;
		FsmOwnerDefault_t251897112 * L_7 = __this->get_GameObject_9();
		String_t* L_8 = ActionHelpers_CheckPhysicsSetup_m2734257387(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		String_t* L_9 = String_Concat_m138640077(NULL /*static, unused*/, L_6, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		String_t* L_10 = V_0;
		return L_10;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MoveTowards::.ctor()
extern "C"  void MoveTowards__ctor_m3606246909 (MoveTowards_t2875353433 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MoveTowards::Reset()
extern "C"  void MoveTowards_Reset_m1252679850 (MoveTowards_t2875353433 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_targetObject_10((FsmGameObject_t1697147867 *)NULL);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (10.0f), /*hidden argument*/NULL);
		__this->set_maxSpeed_13(L_0);
		FsmFloat_t2134102846 * L_1 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_finishDistance_14(L_1);
		__this->set_finishEvent_15((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MoveTowards::OnUpdate()
extern "C"  void MoveTowards_OnUpdate_m1141706287 (MoveTowards_t2875353433 * __this, const MethodInfo* method)
{
	{
		MoveTowards_DoMoveTowards_m2186238555(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.MoveTowards::DoMoveTowards()
extern "C"  void MoveTowards_DoMoveTowards_m2186238555 (MoveTowards_t2875353433 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		bool L_0 = MoveTowards_UpdateTargetPos_m1445359881(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		GameObject_t3674682005 * L_1 = __this->get_go_16();
		NullCheck(L_1);
		Transform_t1659122786 * L_2 = GameObject_get_transform_m1278640159(L_1, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_3 = __this->get_go_16();
		NullCheck(L_3);
		Transform_t1659122786 * L_4 = GameObject_get_transform_m1278640159(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t4282066566  L_5 = Transform_get_position_m2211398607(L_4, /*hidden argument*/NULL);
		Vector3_t4282066566  L_6 = __this->get_targetPos_18();
		FsmFloat_t2134102846 * L_7 = __this->get_maxSpeed_13();
		NullCheck(L_7);
		float L_8 = FsmFloat_get_Value_m4137923823(L_7, /*hidden argument*/NULL);
		float L_9 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_10 = Vector3_MoveTowards_m2405650085(NULL /*static, unused*/, L_5, L_6, ((float)((float)L_8*(float)L_9)), /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_position_m3111394108(L_2, L_10, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_11 = __this->get_go_16();
		NullCheck(L_11);
		Transform_t1659122786 * L_12 = GameObject_get_transform_m1278640159(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t4282066566  L_13 = Transform_get_position_m2211398607(L_12, /*hidden argument*/NULL);
		Vector3_t4282066566  L_14 = __this->get_targetPos_18();
		Vector3_t4282066566  L_15 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		V_1 = L_15;
		float L_16 = Vector3_get_magnitude_m989985786((&V_1), /*hidden argument*/NULL);
		V_0 = L_16;
		float L_17 = V_0;
		FsmFloat_t2134102846 * L_18 = __this->get_finishDistance_14();
		NullCheck(L_18);
		float L_19 = FsmFloat_get_Value_m4137923823(L_18, /*hidden argument*/NULL);
		if ((!(((float)L_17) < ((float)L_19))))
		{
			goto IL_0094;
		}
	}
	{
		Fsm_t1527112426 * L_20 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_21 = __this->get_finishEvent_15();
		NullCheck(L_20);
		Fsm_Event_m625948263(L_20, L_21, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0094:
	{
		return;
	}
}
// System.Boolean HutongGames.PlayMaker.Actions.MoveTowards::UpdateTargetPos()
extern "C"  bool MoveTowards_UpdateTargetPos_m1445359881 (MoveTowards_t2875353433 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	MoveTowards_t2875353433 * G_B8_0 = NULL;
	MoveTowards_t2875353433 * G_B7_0 = NULL;
	Vector3_t4282066566  G_B9_0;
	memset(&G_B9_0, 0, sizeof(G_B9_0));
	MoveTowards_t2875353433 * G_B9_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		__this->set_go_16(L_2);
		GameObject_t3674682005 * L_3 = __this->get_go_16();
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002a;
		}
	}
	{
		return (bool)0;
	}

IL_002a:
	{
		FsmGameObject_t1697147867 * L_5 = __this->get_targetObject_10();
		NullCheck(L_5);
		GameObject_t3674682005 * L_6 = FsmGameObject_get_Value_m673294275(L_5, /*hidden argument*/NULL);
		__this->set_goTarget_17(L_6);
		GameObject_t3674682005 * L_7 = __this->get_goTarget_17();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005e;
		}
	}
	{
		FsmVector3_t533912882 * L_9 = __this->get_targetPosition_11();
		NullCheck(L_9);
		bool L_10 = NamedVariable_get_IsNone_m281035543(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_005e;
		}
	}
	{
		return (bool)0;
	}

IL_005e:
	{
		GameObject_t3674682005 * L_11 = __this->get_goTarget_17();
		bool L_12 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_11, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00ba;
		}
	}
	{
		FsmVector3_t533912882 * L_13 = __this->get_targetPosition_11();
		NullCheck(L_13);
		bool L_14 = NamedVariable_get_IsNone_m281035543(L_13, /*hidden argument*/NULL);
		G_B7_0 = __this;
		if (L_14)
		{
			G_B8_0 = __this;
			goto IL_00a0;
		}
	}
	{
		GameObject_t3674682005 * L_15 = __this->get_goTarget_17();
		NullCheck(L_15);
		Transform_t1659122786 * L_16 = GameObject_get_transform_m1278640159(L_15, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_17 = __this->get_targetPosition_11();
		NullCheck(L_17);
		Vector3_t4282066566  L_18 = FsmVector3_get_Value_m2779135117(L_17, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector3_t4282066566  L_19 = Transform_TransformPoint_m437395512(L_16, L_18, /*hidden argument*/NULL);
		G_B9_0 = L_19;
		G_B9_1 = G_B7_0;
		goto IL_00b0;
	}

IL_00a0:
	{
		GameObject_t3674682005 * L_20 = __this->get_goTarget_17();
		NullCheck(L_20);
		Transform_t1659122786 * L_21 = GameObject_get_transform_m1278640159(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		Vector3_t4282066566  L_22 = Transform_get_position_m2211398607(L_21, /*hidden argument*/NULL);
		G_B9_0 = L_22;
		G_B9_1 = G_B8_0;
	}

IL_00b0:
	{
		NullCheck(G_B9_1);
		G_B9_1->set_targetPos_18(G_B9_0);
		goto IL_00cb;
	}

IL_00ba:
	{
		FsmVector3_t533912882 * L_23 = __this->get_targetPosition_11();
		NullCheck(L_23);
		Vector3_t4282066566  L_24 = FsmVector3_get_Value_m2779135117(L_23, /*hidden argument*/NULL);
		__this->set_targetPos_18(L_24);
	}

IL_00cb:
	{
		Vector3_t4282066566  L_25 = __this->get_targetPos_18();
		__this->set_targetPosWithVertical_19(L_25);
		FsmBool_t1075959796 * L_26 = __this->get_ignoreVertical_12();
		NullCheck(L_26);
		bool L_27 = FsmBool_get_Value_m3101329097(L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_010a;
		}
	}
	{
		Vector3_t4282066566 * L_28 = __this->get_address_of_targetPos_18();
		GameObject_t3674682005 * L_29 = __this->get_go_16();
		NullCheck(L_29);
		Transform_t1659122786 * L_30 = GameObject_get_transform_m1278640159(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		Vector3_t4282066566  L_31 = Transform_get_position_m2211398607(L_30, /*hidden argument*/NULL);
		V_0 = L_31;
		float L_32 = (&V_0)->get_y_2();
		L_28->set_y_2(L_32);
	}

IL_010a:
	{
		return (bool)1;
	}
}
// UnityEngine.Vector3 HutongGames.PlayMaker.Actions.MoveTowards::GetTargetPos()
extern "C"  Vector3_t4282066566  MoveTowards_GetTargetPos_m23384698 (MoveTowards_t2875353433 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_targetPos_18();
		return L_0;
	}
}
// UnityEngine.Vector3 HutongGames.PlayMaker.Actions.MoveTowards::GetTargetPosWithVertical()
extern "C"  Vector3_t4282066566  MoveTowards_GetTargetPosWithVertical_m3323879542 (MoveTowards_t2875353433 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_targetPosWithVertical_19();
		return L_0;
	}
}
// System.Void HutongGames.PlayMaker.Actions.NavMeshAgentAnimatorSynchronizer::.ctor()
extern "C"  void NavMeshAgentAnimatorSynchronizer__ctor_m315230976 (NavMeshAgentAnimatorSynchronizer_t2695016998 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.NavMeshAgentAnimatorSynchronizer::Reset()
extern "C"  void NavMeshAgentAnimatorSynchronizer_Reset_m2256631213 (NavMeshAgentAnimatorSynchronizer_t2695016998 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.NavMeshAgentAnimatorSynchronizer::OnEnter()
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisNavMeshAgent_t588466745_m2114887537_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623_MethodInfo_var;
extern const MethodInfo* NavMeshAgentAnimatorSynchronizer_OnAnimatorMoveEvent_m2499140257_MethodInfo_var;
extern const uint32_t NavMeshAgentAnimatorSynchronizer_OnEnter_m1680985815_MetadataUsageId;
extern "C"  void NavMeshAgentAnimatorSynchronizer_OnEnter_m1680985815 (NavMeshAgentAnimatorSynchronizer_t2695016998 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NavMeshAgentAnimatorSynchronizer_OnEnter_m1680985815_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		NavMeshAgent_t588466745 * L_6 = GameObject_GetComponent_TisNavMeshAgent_t588466745_m2114887537(L_5, /*hidden argument*/GameObject_GetComponent_TisNavMeshAgent_t588466745_m2114887537_MethodInfo_var);
		__this->set__agent_12(L_6);
		GameObject_t3674682005 * L_7 = V_0;
		NullCheck(L_7);
		Animator_t2776330603 * L_8 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_7, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_11(L_8);
		Animator_t2776330603 * L_9 = __this->get__animator_11();
		bool L_10 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_9, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0055;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0055:
	{
		GameObject_t3674682005 * L_11 = V_0;
		NullCheck(L_11);
		Transform_t1659122786 * L_12 = GameObject_get_transform_m1278640159(L_11, /*hidden argument*/NULL);
		__this->set__trans_13(L_12);
		GameObject_t3674682005 * L_13 = V_0;
		NullCheck(L_13);
		PlayMakerAnimatorMoveProxy_t4175490694 * L_14 = GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623(L_13, /*hidden argument*/GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623_MethodInfo_var);
		__this->set__animatorProxy_10(L_14);
		PlayMakerAnimatorMoveProxy_t4175490694 * L_15 = __this->get__animatorProxy_10();
		bool L_16 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_15, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0095;
		}
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_17 = __this->get__animatorProxy_10();
		IntPtr_t L_18;
		L_18.set_m_value_0((void*)(void*)NavMeshAgentAnimatorSynchronizer_OnAnimatorMoveEvent_m2499140257_MethodInfo_var);
		Action_t3771233898 * L_19 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_19, __this, L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		PlayMakerAnimatorMoveProxy_add_OnAnimatorMoveEvent_m2003874533(L_17, L_19, /*hidden argument*/NULL);
	}

IL_0095:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.NavMeshAgentAnimatorSynchronizer::OnAnimatorMoveEvent()
extern "C"  void NavMeshAgentAnimatorSynchronizer_OnAnimatorMoveEvent_m2499140257 (NavMeshAgentAnimatorSynchronizer_t2695016998 * __this, const MethodInfo* method)
{
	{
		NavMeshAgent_t588466745 * L_0 = __this->get__agent_12();
		Animator_t2776330603 * L_1 = __this->get__animator_11();
		NullCheck(L_1);
		Vector3_t4282066566  L_2 = Animator_get_deltaPosition_m1658225602(L_1, /*hidden argument*/NULL);
		float L_3 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_4 = Vector3_op_Division_m4277988370(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		NavMeshAgent_set_velocity_m2183624243(L_0, L_4, /*hidden argument*/NULL);
		Transform_t1659122786 * L_5 = __this->get__trans_13();
		Animator_t2776330603 * L_6 = __this->get__animator_11();
		NullCheck(L_6);
		Quaternion_t1553702882  L_7 = Animator_get_rootRotation_m3309843777(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_rotation_m1525803229(L_5, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.NavMeshAgentAnimatorSynchronizer::OnExit()
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const MethodInfo* NavMeshAgentAnimatorSynchronizer_OnAnimatorMoveEvent_m2499140257_MethodInfo_var;
extern const uint32_t NavMeshAgentAnimatorSynchronizer_OnExit_m2695544289_MetadataUsageId;
extern "C"  void NavMeshAgentAnimatorSynchronizer_OnExit_m2695544289 (NavMeshAgentAnimatorSynchronizer_t2695016998 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NavMeshAgentAnimatorSynchronizer_OnExit_m2695544289_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_10();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_2 = __this->get__animatorProxy_10();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)NavMeshAgentAnimatorSynchronizer_OnAnimatorMoveEvent_m2499140257_MethodInfo_var);
		Action_t3771233898 * L_4 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_4, __this, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		PlayMakerAnimatorMoveProxy_remove_OnAnimatorMoveEvent_m2967347206(L_2, L_4, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.NetworkCloseConnection::.ctor()
extern "C"  void NetworkCloseConnection__ctor_m1981030806 (NetworkCloseConnection_t686438608 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.NetworkCloseConnection::Reset()
extern "C"  void NetworkCloseConnection_Reset_m3922431043 (NetworkCloseConnection_t686438608 * __this, const MethodInfo* method)
{
	{
		FsmInt_t1596138449 * L_0 = FsmInt_op_Implicit_m1006909518(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		__this->set_connectionIndex_9(L_0);
		__this->set_connectionGUID_10((FsmString_t952858651 *)NULL);
		__this->set_sendDisconnectionNotification_11((bool)1);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.NetworkCloseConnection::OnEnter()
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1420252380;
extern const uint32_t NetworkCloseConnection_OnEnter_m491821037_MetadataUsageId;
extern "C"  void NetworkCloseConnection_OnEnter_m491821037 (NetworkCloseConnection_t686438608 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NetworkCloseConnection_OnEnter_m491821037_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		FsmInt_t1596138449 * L_0 = __this->get_connectionIndex_9();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0023;
		}
	}
	{
		FsmInt_t1596138449 * L_2 = __this->get_connectionIndex_9();
		NullCheck(L_2);
		int32_t L_3 = FsmInt_get_Value_m27059446(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_004d;
	}

IL_0023:
	{
		FsmString_t952858651 * L_4 = __this->get_connectionGUID_10();
		NullCheck(L_4);
		bool L_5 = NamedVariable_get_IsNone_m281035543(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004d;
		}
	}
	{
		FsmString_t952858651 * L_6 = __this->get_connectionGUID_10();
		NullCheck(L_6);
		String_t* L_7 = FsmString_get_Value_m872383149(L_6, /*hidden argument*/NULL);
		bool L_8 = NetworkCloseConnection_getIndexFromGUID_m3293212166(__this, L_7, (&V_1), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004d;
		}
	}
	{
		int32_t L_9 = V_1;
		V_0 = L_9;
	}

IL_004d:
	{
		int32_t L_10 = V_0;
		if ((((int32_t)L_10) < ((int32_t)0)))
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_11 = V_0;
		NetworkPlayerU5BU5D_t132745896* L_12 = Network_get_connections_m4276471278(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		if ((((int32_t)L_11) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_007c;
		}
	}

IL_0061:
	{
		int32_t L_13 = V_0;
		int32_t L_14 = L_13;
		Il2CppObject * L_15 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_14);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral1420252380, L_15, /*hidden argument*/NULL);
		FsmStateAction_LogError_m3478223492(__this, L_16, /*hidden argument*/NULL);
		goto IL_0097;
	}

IL_007c:
	{
		NetworkPlayerU5BU5D_t132745896* L_17 = Network_get_connections_m4276471278(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_18 = V_0;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		bool L_19 = __this->get_sendDisconnectionNotification_11();
		Network_CloseConnection_m1870717282(NULL /*static, unused*/, (*(NetworkPlayer_t3231273765 *)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18)))), L_19, /*hidden argument*/NULL);
	}

IL_0097:
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean HutongGames.PlayMaker.Actions.NetworkCloseConnection::getIndexFromGUID(System.String,System.Int32&)
extern "C"  bool NetworkCloseConnection_getIndexFromGUID_m3293212166 (NetworkCloseConnection_t686438608 * __this, String_t* ___guid0, int32_t* ___guidIndex1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_002b;
	}

IL_0007:
	{
		String_t* L_0 = ___guid0;
		NetworkPlayerU5BU5D_t132745896* L_1 = Network_get_connections_m4276471278(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		String_t* L_3 = NetworkPlayer_get_guid_m1193250345(((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))), /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_4 = String_Equals_m3541721061(L_0, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0027;
		}
	}
	{
		int32_t* L_5 = ___guidIndex1;
		int32_t L_6 = V_0;
		*((int32_t*)(L_5)) = (int32_t)L_6;
		return (bool)1;
	}

IL_0027:
	{
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002b:
	{
		int32_t L_8 = V_0;
		NetworkPlayerU5BU5D_t132745896* L_9 = Network_get_connections_m4276471278(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_0007;
		}
	}
	{
		int32_t* L_10 = ___guidIndex1;
		*((int32_t*)(L_10)) = (int32_t)0;
		return (bool)0;
	}
}
// System.Void HutongGames.PlayMaker.Actions.NetworkConnect::.ctor()
extern "C"  void NetworkConnect__ctor_m482645794 (NetworkConnect_t2329652676 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.NetworkConnect::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1505998205;
extern const uint32_t NetworkConnect_Reset_m2424046031_MetadataUsageId;
extern "C"  void NetworkConnect_Reset_m2424046031 (NetworkConnect_t2329652676 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NetworkConnect_Reset_m2424046031_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmString_t952858651 * L_0 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, _stringLiteral1505998205, /*hidden argument*/NULL);
		__this->set_remoteIP_9(L_0);
		FsmInt_t1596138449 * L_1 = FsmInt_op_Implicit_m1006909518(NULL /*static, unused*/, ((int32_t)25001), /*hidden argument*/NULL);
		__this->set_remotePort_10(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_3 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_password_11(L_3);
		__this->set_errorEvent_12((FsmEvent_t2133468028 *)NULL);
		__this->set_errorString_13((FsmString_t952858651 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.NetworkConnect::OnEnter()
extern Il2CppClass* NetworkConnectionError_t1049203712_il2cpp_TypeInfo_var;
extern const uint32_t NetworkConnect_OnEnter_m3652835961_MetadataUsageId;
extern "C"  void NetworkConnect_OnEnter_m3652835961 (NetworkConnect_t2329652676 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NetworkConnect_OnEnter_m3652835961_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		FsmString_t952858651 * L_0 = __this->get_remoteIP_9();
		NullCheck(L_0);
		String_t* L_1 = FsmString_get_Value_m872383149(L_0, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_2 = __this->get_remotePort_10();
		NullCheck(L_2);
		int32_t L_3 = FsmInt_get_Value_m27059446(L_2, /*hidden argument*/NULL);
		FsmString_t952858651 * L_4 = __this->get_password_11();
		NullCheck(L_4);
		String_t* L_5 = FsmString_get_Value_m872383149(L_4, /*hidden argument*/NULL);
		int32_t L_6 = Network_Connect_m3659141856(NULL /*static, unused*/, L_1, L_3, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		int32_t L_7 = V_0;
		if (!L_7)
		{
			goto IL_0065;
		}
	}
	{
		FsmString_t952858651 * L_8 = __this->get_errorString_13();
		int32_t L_9 = V_0;
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(NetworkConnectionError_t1049203712_il2cpp_TypeInfo_var, &L_10);
		NullCheck((Enum_t2862688501 *)L_11);
		String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_11);
		NullCheck(L_8);
		FsmString_set_Value_m829393196(L_8, L_12, /*hidden argument*/NULL);
		FsmString_t952858651 * L_13 = __this->get_errorString_13();
		NullCheck(L_13);
		String_t* L_14 = FsmString_get_Value_m872383149(L_13, /*hidden argument*/NULL);
		FsmStateAction_LogError_m3478223492(__this, L_14, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_15 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_16 = __this->get_errorEvent_12();
		NullCheck(L_15);
		Fsm_Event_m625948263(L_15, L_16, /*hidden argument*/NULL);
	}

IL_0065:
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.NetworkDestroy::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m362039748_MethodInfo_var;
extern const uint32_t NetworkDestroy__ctor_m2690146834_MetadataUsageId;
extern "C"  void NetworkDestroy__ctor_m2690146834 (NetworkDestroy_t2935674068 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NetworkDestroy__ctor_m2690146834_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m362039748(__this, /*hidden argument*/ComponentAction_1__ctor_m362039748_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.NetworkDestroy::Reset()
extern "C"  void NetworkDestroy_Reset_m336579775 (NetworkDestroy_t2935674068 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		FsmBool_t1075959796 * L_0 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_removeRPCs_12(L_0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.NetworkDestroy::OnEnter()
extern "C"  void NetworkDestroy_OnEnter_m3347491177 (NetworkDestroy_t2935674068 * __this, const MethodInfo* method)
{
	{
		NetworkDestroy_DoDestroy_m949788895(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.NetworkDestroy::DoDestroy()
extern const MethodInfo* ComponentAction_1_UpdateCache_m1496191775_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_networkView_m361268533_MethodInfo_var;
extern const uint32_t NetworkDestroy_DoDestroy_m949788895_MetadataUsageId;
extern "C"  void NetworkDestroy_DoDestroy_m949788895 (NetworkDestroy_t2935674068 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NetworkDestroy_DoDestroy_m949788895_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m1496191775(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m1496191775_MethodInfo_var);
		if (L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		FsmBool_t1075959796 * L_5 = __this->get_removeRPCs_12();
		NullCheck(L_5);
		bool L_6 = FsmBool_get_Value_m3101329097(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		NetworkView_t3656680617 * L_7 = ComponentAction_1_get_networkView_m361268533(__this, /*hidden argument*/ComponentAction_1_get_networkView_m361268533_MethodInfo_var);
		NullCheck(L_7);
		NetworkPlayer_t3231273765  L_8 = NetworkView_get_owner_m1924541257(L_7, /*hidden argument*/NULL);
		Network_RemoveRPCs_m3795657583(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_003f:
	{
		NetworkView_t3656680617 * L_9 = ComponentAction_1_get_networkView_m361268533(__this, /*hidden argument*/ComponentAction_1_get_networkView_m361268533_MethodInfo_var);
		NullCheck(L_9);
		NetworkPlayer_t3231273765  L_10 = NetworkView_get_owner_m1924541257(L_9, /*hidden argument*/NULL);
		Network_DestroyPlayerObjects_m73641416(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.NetworkDisconnect::.ctor()
extern "C"  void NetworkDisconnect__ctor_m2576887422 (NetworkDisconnect_t1440046520 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.NetworkDisconnect::OnEnter()
extern "C"  void NetworkDisconnect_OnEnter_m1879378645 (NetworkDisconnect_t1440046520 * __this, const MethodInfo* method)
{
	{
		Network_Disconnect_m2068213653(NULL /*static, unused*/, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.NetworkGetConnectedPlayerProperties::.ctor()
extern "C"  void NetworkGetConnectedPlayerProperties__ctor_m1757610579 (NetworkGetConnectedPlayerProperties_t4024683587 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.NetworkGetConnectedPlayerProperties::Reset()
extern "C"  void NetworkGetConnectedPlayerProperties_Reset_m3699010816 (NetworkGetConnectedPlayerProperties_t4024683587 * __this, const MethodInfo* method)
{
	{
		__this->set_index_9((FsmInt_t1596138449 *)NULL);
		__this->set_IpAddress_10((FsmString_t952858651 *)NULL);
		__this->set_port_11((FsmInt_t1596138449 *)NULL);
		__this->set_guid_12((FsmString_t952858651 *)NULL);
		__this->set_externalIPAddress_13((FsmString_t952858651 *)NULL);
		__this->set_externalPort_14((FsmInt_t1596138449 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.NetworkGetConnectedPlayerProperties::OnEnter()
extern "C"  void NetworkGetConnectedPlayerProperties_OnEnter_m533347690 (NetworkGetConnectedPlayerProperties_t4024683587 * __this, const MethodInfo* method)
{
	{
		NetworkGetConnectedPlayerProperties_getPlayerProperties_m2457282427(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.NetworkGetConnectedPlayerProperties::getPlayerProperties()
extern Il2CppCodeGenString* _stringLiteral2801539667;
extern const uint32_t NetworkGetConnectedPlayerProperties_getPlayerProperties_m2457282427_MetadataUsageId;
extern "C"  void NetworkGetConnectedPlayerProperties_getPlayerProperties_m2457282427 (NetworkGetConnectedPlayerProperties_t4024683587 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NetworkGetConnectedPlayerProperties_getPlayerProperties_m2457282427_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	NetworkPlayer_t3231273765  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		FsmInt_t1596138449 * L_0 = __this->get_index_9();
		NullCheck(L_0);
		int32_t L_1 = FsmInt_get_Value_m27059446(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_3 = V_0;
		NetworkPlayerU5BU5D_t132745896* L_4 = Network_get_connections_m4276471278(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		if ((((int32_t)L_3) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length)))))))
		{
			goto IL_002c;
		}
	}

IL_0020:
	{
		FsmStateAction_LogError_m3478223492(__this, _stringLiteral2801539667, /*hidden argument*/NULL);
		return;
	}

IL_002c:
	{
		NetworkPlayerU5BU5D_t132745896* L_5 = Network_get_connections_m4276471278(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		V_1 = (*(NetworkPlayer_t3231273765 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6))));
		FsmString_t952858651 * L_7 = __this->get_IpAddress_10();
		String_t* L_8 = NetworkPlayer_get_ipAddress_m206939407((&V_1), /*hidden argument*/NULL);
		NullCheck(L_7);
		FsmString_set_Value_m829393196(L_7, L_8, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_9 = __this->get_port_11();
		int32_t L_10 = NetworkPlayer_get_port_m4260923428((&V_1), /*hidden argument*/NULL);
		NullCheck(L_9);
		FsmInt_set_Value_m2087583461(L_9, L_10, /*hidden argument*/NULL);
		FsmString_t952858651 * L_11 = __this->get_guid_12();
		String_t* L_12 = NetworkPlayer_get_guid_m1193250345((&V_1), /*hidden argument*/NULL);
		NullCheck(L_11);
		FsmString_set_Value_m829393196(L_11, L_12, /*hidden argument*/NULL);
		FsmString_t952858651 * L_13 = __this->get_externalIPAddress_13();
		String_t* L_14 = NetworkPlayer_get_externalIP_m420672594((&V_1), /*hidden argument*/NULL);
		NullCheck(L_13);
		FsmString_set_Value_m829393196(L_13, L_14, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_15 = __this->get_externalPort_14();
		int32_t L_16 = NetworkPlayer_get_externalPort_m2392166863((&V_1), /*hidden argument*/NULL);
		NullCheck(L_15);
		FsmInt_set_Value_m2087583461(L_15, L_16, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
