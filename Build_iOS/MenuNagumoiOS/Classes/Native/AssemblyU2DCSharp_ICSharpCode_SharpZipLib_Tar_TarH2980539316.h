﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_DateTime4283661327.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Tar.TarHeader
struct  TarHeader_t2980539316  : public Il2CppObject
{
public:
	// System.String ICSharpCode.SharpZipLib.Tar.TarHeader::name
	String_t* ___name_38;
	// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::mode
	int32_t ___mode_39;
	// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::userId
	int32_t ___userId_40;
	// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::groupId
	int32_t ___groupId_41;
	// System.Int64 ICSharpCode.SharpZipLib.Tar.TarHeader::size
	int64_t ___size_42;
	// System.DateTime ICSharpCode.SharpZipLib.Tar.TarHeader::modTime
	DateTime_t4283661327  ___modTime_43;
	// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::checksum
	int32_t ___checksum_44;
	// System.Boolean ICSharpCode.SharpZipLib.Tar.TarHeader::isChecksumValid
	bool ___isChecksumValid_45;
	// System.Byte ICSharpCode.SharpZipLib.Tar.TarHeader::typeFlag
	uint8_t ___typeFlag_46;
	// System.String ICSharpCode.SharpZipLib.Tar.TarHeader::linkName
	String_t* ___linkName_47;
	// System.String ICSharpCode.SharpZipLib.Tar.TarHeader::magic
	String_t* ___magic_48;
	// System.String ICSharpCode.SharpZipLib.Tar.TarHeader::version
	String_t* ___version_49;
	// System.String ICSharpCode.SharpZipLib.Tar.TarHeader::userName
	String_t* ___userName_50;
	// System.String ICSharpCode.SharpZipLib.Tar.TarHeader::groupName
	String_t* ___groupName_51;
	// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::devMajor
	int32_t ___devMajor_52;
	// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::devMinor
	int32_t ___devMinor_53;

public:
	inline static int32_t get_offset_of_name_38() { return static_cast<int32_t>(offsetof(TarHeader_t2980539316, ___name_38)); }
	inline String_t* get_name_38() const { return ___name_38; }
	inline String_t** get_address_of_name_38() { return &___name_38; }
	inline void set_name_38(String_t* value)
	{
		___name_38 = value;
		Il2CppCodeGenWriteBarrier(&___name_38, value);
	}

	inline static int32_t get_offset_of_mode_39() { return static_cast<int32_t>(offsetof(TarHeader_t2980539316, ___mode_39)); }
	inline int32_t get_mode_39() const { return ___mode_39; }
	inline int32_t* get_address_of_mode_39() { return &___mode_39; }
	inline void set_mode_39(int32_t value)
	{
		___mode_39 = value;
	}

	inline static int32_t get_offset_of_userId_40() { return static_cast<int32_t>(offsetof(TarHeader_t2980539316, ___userId_40)); }
	inline int32_t get_userId_40() const { return ___userId_40; }
	inline int32_t* get_address_of_userId_40() { return &___userId_40; }
	inline void set_userId_40(int32_t value)
	{
		___userId_40 = value;
	}

	inline static int32_t get_offset_of_groupId_41() { return static_cast<int32_t>(offsetof(TarHeader_t2980539316, ___groupId_41)); }
	inline int32_t get_groupId_41() const { return ___groupId_41; }
	inline int32_t* get_address_of_groupId_41() { return &___groupId_41; }
	inline void set_groupId_41(int32_t value)
	{
		___groupId_41 = value;
	}

	inline static int32_t get_offset_of_size_42() { return static_cast<int32_t>(offsetof(TarHeader_t2980539316, ___size_42)); }
	inline int64_t get_size_42() const { return ___size_42; }
	inline int64_t* get_address_of_size_42() { return &___size_42; }
	inline void set_size_42(int64_t value)
	{
		___size_42 = value;
	}

	inline static int32_t get_offset_of_modTime_43() { return static_cast<int32_t>(offsetof(TarHeader_t2980539316, ___modTime_43)); }
	inline DateTime_t4283661327  get_modTime_43() const { return ___modTime_43; }
	inline DateTime_t4283661327 * get_address_of_modTime_43() { return &___modTime_43; }
	inline void set_modTime_43(DateTime_t4283661327  value)
	{
		___modTime_43 = value;
	}

	inline static int32_t get_offset_of_checksum_44() { return static_cast<int32_t>(offsetof(TarHeader_t2980539316, ___checksum_44)); }
	inline int32_t get_checksum_44() const { return ___checksum_44; }
	inline int32_t* get_address_of_checksum_44() { return &___checksum_44; }
	inline void set_checksum_44(int32_t value)
	{
		___checksum_44 = value;
	}

	inline static int32_t get_offset_of_isChecksumValid_45() { return static_cast<int32_t>(offsetof(TarHeader_t2980539316, ___isChecksumValid_45)); }
	inline bool get_isChecksumValid_45() const { return ___isChecksumValid_45; }
	inline bool* get_address_of_isChecksumValid_45() { return &___isChecksumValid_45; }
	inline void set_isChecksumValid_45(bool value)
	{
		___isChecksumValid_45 = value;
	}

	inline static int32_t get_offset_of_typeFlag_46() { return static_cast<int32_t>(offsetof(TarHeader_t2980539316, ___typeFlag_46)); }
	inline uint8_t get_typeFlag_46() const { return ___typeFlag_46; }
	inline uint8_t* get_address_of_typeFlag_46() { return &___typeFlag_46; }
	inline void set_typeFlag_46(uint8_t value)
	{
		___typeFlag_46 = value;
	}

	inline static int32_t get_offset_of_linkName_47() { return static_cast<int32_t>(offsetof(TarHeader_t2980539316, ___linkName_47)); }
	inline String_t* get_linkName_47() const { return ___linkName_47; }
	inline String_t** get_address_of_linkName_47() { return &___linkName_47; }
	inline void set_linkName_47(String_t* value)
	{
		___linkName_47 = value;
		Il2CppCodeGenWriteBarrier(&___linkName_47, value);
	}

	inline static int32_t get_offset_of_magic_48() { return static_cast<int32_t>(offsetof(TarHeader_t2980539316, ___magic_48)); }
	inline String_t* get_magic_48() const { return ___magic_48; }
	inline String_t** get_address_of_magic_48() { return &___magic_48; }
	inline void set_magic_48(String_t* value)
	{
		___magic_48 = value;
		Il2CppCodeGenWriteBarrier(&___magic_48, value);
	}

	inline static int32_t get_offset_of_version_49() { return static_cast<int32_t>(offsetof(TarHeader_t2980539316, ___version_49)); }
	inline String_t* get_version_49() const { return ___version_49; }
	inline String_t** get_address_of_version_49() { return &___version_49; }
	inline void set_version_49(String_t* value)
	{
		___version_49 = value;
		Il2CppCodeGenWriteBarrier(&___version_49, value);
	}

	inline static int32_t get_offset_of_userName_50() { return static_cast<int32_t>(offsetof(TarHeader_t2980539316, ___userName_50)); }
	inline String_t* get_userName_50() const { return ___userName_50; }
	inline String_t** get_address_of_userName_50() { return &___userName_50; }
	inline void set_userName_50(String_t* value)
	{
		___userName_50 = value;
		Il2CppCodeGenWriteBarrier(&___userName_50, value);
	}

	inline static int32_t get_offset_of_groupName_51() { return static_cast<int32_t>(offsetof(TarHeader_t2980539316, ___groupName_51)); }
	inline String_t* get_groupName_51() const { return ___groupName_51; }
	inline String_t** get_address_of_groupName_51() { return &___groupName_51; }
	inline void set_groupName_51(String_t* value)
	{
		___groupName_51 = value;
		Il2CppCodeGenWriteBarrier(&___groupName_51, value);
	}

	inline static int32_t get_offset_of_devMajor_52() { return static_cast<int32_t>(offsetof(TarHeader_t2980539316, ___devMajor_52)); }
	inline int32_t get_devMajor_52() const { return ___devMajor_52; }
	inline int32_t* get_address_of_devMajor_52() { return &___devMajor_52; }
	inline void set_devMajor_52(int32_t value)
	{
		___devMajor_52 = value;
	}

	inline static int32_t get_offset_of_devMinor_53() { return static_cast<int32_t>(offsetof(TarHeader_t2980539316, ___devMinor_53)); }
	inline int32_t get_devMinor_53() const { return ___devMinor_53; }
	inline int32_t* get_address_of_devMinor_53() { return &___devMinor_53; }
	inline void set_devMinor_53(int32_t value)
	{
		___devMinor_53 = value;
	}
};

struct TarHeader_t2980539316_StaticFields
{
public:
	// System.DateTime ICSharpCode.SharpZipLib.Tar.TarHeader::dateTime1970
	DateTime_t4283661327  ___dateTime1970_37;
	// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::userIdAsSet
	int32_t ___userIdAsSet_54;
	// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::groupIdAsSet
	int32_t ___groupIdAsSet_55;
	// System.String ICSharpCode.SharpZipLib.Tar.TarHeader::userNameAsSet
	String_t* ___userNameAsSet_56;
	// System.String ICSharpCode.SharpZipLib.Tar.TarHeader::groupNameAsSet
	String_t* ___groupNameAsSet_57;
	// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::defaultUserId
	int32_t ___defaultUserId_58;
	// System.Int32 ICSharpCode.SharpZipLib.Tar.TarHeader::defaultGroupId
	int32_t ___defaultGroupId_59;
	// System.String ICSharpCode.SharpZipLib.Tar.TarHeader::defaultGroupName
	String_t* ___defaultGroupName_60;
	// System.String ICSharpCode.SharpZipLib.Tar.TarHeader::defaultUser
	String_t* ___defaultUser_61;

public:
	inline static int32_t get_offset_of_dateTime1970_37() { return static_cast<int32_t>(offsetof(TarHeader_t2980539316_StaticFields, ___dateTime1970_37)); }
	inline DateTime_t4283661327  get_dateTime1970_37() const { return ___dateTime1970_37; }
	inline DateTime_t4283661327 * get_address_of_dateTime1970_37() { return &___dateTime1970_37; }
	inline void set_dateTime1970_37(DateTime_t4283661327  value)
	{
		___dateTime1970_37 = value;
	}

	inline static int32_t get_offset_of_userIdAsSet_54() { return static_cast<int32_t>(offsetof(TarHeader_t2980539316_StaticFields, ___userIdAsSet_54)); }
	inline int32_t get_userIdAsSet_54() const { return ___userIdAsSet_54; }
	inline int32_t* get_address_of_userIdAsSet_54() { return &___userIdAsSet_54; }
	inline void set_userIdAsSet_54(int32_t value)
	{
		___userIdAsSet_54 = value;
	}

	inline static int32_t get_offset_of_groupIdAsSet_55() { return static_cast<int32_t>(offsetof(TarHeader_t2980539316_StaticFields, ___groupIdAsSet_55)); }
	inline int32_t get_groupIdAsSet_55() const { return ___groupIdAsSet_55; }
	inline int32_t* get_address_of_groupIdAsSet_55() { return &___groupIdAsSet_55; }
	inline void set_groupIdAsSet_55(int32_t value)
	{
		___groupIdAsSet_55 = value;
	}

	inline static int32_t get_offset_of_userNameAsSet_56() { return static_cast<int32_t>(offsetof(TarHeader_t2980539316_StaticFields, ___userNameAsSet_56)); }
	inline String_t* get_userNameAsSet_56() const { return ___userNameAsSet_56; }
	inline String_t** get_address_of_userNameAsSet_56() { return &___userNameAsSet_56; }
	inline void set_userNameAsSet_56(String_t* value)
	{
		___userNameAsSet_56 = value;
		Il2CppCodeGenWriteBarrier(&___userNameAsSet_56, value);
	}

	inline static int32_t get_offset_of_groupNameAsSet_57() { return static_cast<int32_t>(offsetof(TarHeader_t2980539316_StaticFields, ___groupNameAsSet_57)); }
	inline String_t* get_groupNameAsSet_57() const { return ___groupNameAsSet_57; }
	inline String_t** get_address_of_groupNameAsSet_57() { return &___groupNameAsSet_57; }
	inline void set_groupNameAsSet_57(String_t* value)
	{
		___groupNameAsSet_57 = value;
		Il2CppCodeGenWriteBarrier(&___groupNameAsSet_57, value);
	}

	inline static int32_t get_offset_of_defaultUserId_58() { return static_cast<int32_t>(offsetof(TarHeader_t2980539316_StaticFields, ___defaultUserId_58)); }
	inline int32_t get_defaultUserId_58() const { return ___defaultUserId_58; }
	inline int32_t* get_address_of_defaultUserId_58() { return &___defaultUserId_58; }
	inline void set_defaultUserId_58(int32_t value)
	{
		___defaultUserId_58 = value;
	}

	inline static int32_t get_offset_of_defaultGroupId_59() { return static_cast<int32_t>(offsetof(TarHeader_t2980539316_StaticFields, ___defaultGroupId_59)); }
	inline int32_t get_defaultGroupId_59() const { return ___defaultGroupId_59; }
	inline int32_t* get_address_of_defaultGroupId_59() { return &___defaultGroupId_59; }
	inline void set_defaultGroupId_59(int32_t value)
	{
		___defaultGroupId_59 = value;
	}

	inline static int32_t get_offset_of_defaultGroupName_60() { return static_cast<int32_t>(offsetof(TarHeader_t2980539316_StaticFields, ___defaultGroupName_60)); }
	inline String_t* get_defaultGroupName_60() const { return ___defaultGroupName_60; }
	inline String_t** get_address_of_defaultGroupName_60() { return &___defaultGroupName_60; }
	inline void set_defaultGroupName_60(String_t* value)
	{
		___defaultGroupName_60 = value;
		Il2CppCodeGenWriteBarrier(&___defaultGroupName_60, value);
	}

	inline static int32_t get_offset_of_defaultUser_61() { return static_cast<int32_t>(offsetof(TarHeader_t2980539316_StaticFields, ___defaultUser_61)); }
	inline String_t* get_defaultUser_61() const { return ___defaultUser_61; }
	inline String_t** get_address_of_defaultUser_61() { return &___defaultUser_61; }
	inline void set_defaultUser_61(String_t* value)
	{
		___defaultUser_61 = value;
		Il2CppCodeGenWriteBarrier(&___defaultUser_61, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
