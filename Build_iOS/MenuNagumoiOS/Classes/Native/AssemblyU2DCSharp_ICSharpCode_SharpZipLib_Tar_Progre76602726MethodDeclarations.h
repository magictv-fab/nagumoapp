﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Tar.ProgressMessageHandler
struct ProgressMessageHandler_t76602726;
// System.Object
struct Il2CppObject;
// ICSharpCode.SharpZipLib.Tar.TarArchive
struct TarArchive_t1409635571;
// ICSharpCode.SharpZipLib.Tar.TarEntry
struct TarEntry_t762185187;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Tar_TarA1409635571.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Tar_TarEn762185187.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ICSharpCode.SharpZipLib.Tar.ProgressMessageHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void ProgressMessageHandler__ctor_m532494097 (ProgressMessageHandler_t76602726 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.ProgressMessageHandler::Invoke(ICSharpCode.SharpZipLib.Tar.TarArchive,ICSharpCode.SharpZipLib.Tar.TarEntry,System.String)
extern "C"  void ProgressMessageHandler_Invoke_m2452769639 (ProgressMessageHandler_t76602726 * __this, TarArchive_t1409635571 * ___archive0, TarEntry_t762185187 * ___entry1, String_t* ___message2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ICSharpCode.SharpZipLib.Tar.ProgressMessageHandler::BeginInvoke(ICSharpCode.SharpZipLib.Tar.TarArchive,ICSharpCode.SharpZipLib.Tar.TarEntry,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ProgressMessageHandler_BeginInvoke_m1193464844 (ProgressMessageHandler_t76602726 * __this, TarArchive_t1409635571 * ___archive0, TarEntry_t762185187 * ___entry1, String_t* ___message2, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.ProgressMessageHandler::EndInvoke(System.IAsyncResult)
extern "C"  void ProgressMessageHandler_EndInvoke_m2199968161 (ProgressMessageHandler_t76602726 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
