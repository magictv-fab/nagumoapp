﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.globals.events.DeviceEvents
struct DeviceEvents_t3457131993;
// MagicTV.globals.events.DeviceEvents/OnSomething
struct OnSomething_t1610687155;
// System.String
struct String_t;
// MagicTV.globals.events.DeviceEvents/OnString
struct OnString_t2272041080;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_globals_events_DeviceEve1610687155.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_MagicTV_globals_events_DeviceEve2272041080.h"

// System.Void MagicTV.globals.events.DeviceEvents::.ctor()
extern "C"  void DeviceEvents__ctor_m2029182916 (DeviceEvents_t3457131993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::AddVibrateEventHandler(MagicTV.globals.events.DeviceEvents/OnSomething)
extern "C"  void DeviceEvents_AddVibrateEventHandler_m4008064929 (DeviceEvents_t3457131993 * __this, OnSomething_t1610687155 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::RemoveVibrateEventHandler(MagicTV.globals.events.DeviceEvents/OnSomething)
extern "C"  void DeviceEvents_RemoveVibrateEventHandler_m702279330 (DeviceEvents_t3457131993 * __this, OnSomething_t1610687155 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::RaiseDoVibrate()
extern "C"  void DeviceEvents_RaiseDoVibrate_m2802285592 (DeviceEvents_t3457131993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::RaiseSendTokenToServer(System.String)
extern "C"  void DeviceEvents_RaiseSendTokenToServer_m2143841247 (DeviceEvents_t3457131993 * __this, String_t* ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::RaiseTokenSavedIntoServer(System.String)
extern "C"  void DeviceEvents_RaiseTokenSavedIntoServer_m533493699 (DeviceEvents_t3457131993 * __this, String_t* ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::AddOnPinRecieved(MagicTV.globals.events.DeviceEvents/OnString)
extern "C"  void DeviceEvents_AddOnPinRecieved_m3566739110 (DeviceEvents_t3457131993 * __this, OnString_t2272041080 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::RemoveOnPinRecieved(MagicTV.globals.events.DeviceEvents/OnString)
extern "C"  void DeviceEvents_RemoveOnPinRecieved_m2311410309 (DeviceEvents_t3457131993 * __this, OnString_t2272041080 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::RaiseDoPinRecived(System.String)
extern "C"  void DeviceEvents_RaiseDoPinRecived_m514954850 (DeviceEvents_t3457131993 * __this, String_t* ___pin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::AddResetEventHandler(MagicTV.globals.events.DeviceEvents/OnSomething)
extern "C"  void DeviceEvents_AddResetEventHandler_m3535289409 (DeviceEvents_t3457131993 * __this, OnSomething_t1610687155 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::RemoveResetEventHandler(MagicTV.globals.events.DeviceEvents/OnSomething)
extern "C"  void DeviceEvents_RemoveResetEventHandler_m3120676738 (DeviceEvents_t3457131993 * __this, OnSomething_t1610687155 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::RaiseReset()
extern "C"  void DeviceEvents_RaiseReset_m3067051459 (DeviceEvents_t3457131993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::AddClearCacheNothingToDeleteEventHandler(MagicTV.globals.events.DeviceEvents/OnSomething)
extern "C"  void DeviceEvents_AddClearCacheNothingToDeleteEventHandler_m1981359602 (DeviceEvents_t3457131993 * __this, OnSomething_t1610687155 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::RemoveClearCacheNothingToDeleteEventHandler(MagicTV.globals.events.DeviceEvents/OnSomething)
extern "C"  void DeviceEvents_RemoveClearCacheNothingToDeleteEventHandler_m1465482931 (DeviceEvents_t3457131993 * __this, OnSomething_t1610687155 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::RaiseClearCacheNothingToDeleteEventHandler()
extern "C"  void DeviceEvents_RaiseClearCacheNothingToDeleteEventHandler_m1230047266 (DeviceEvents_t3457131993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::AddClearCacheCompletedEventHandler(MagicTV.globals.events.DeviceEvents/OnSomething)
extern "C"  void DeviceEvents_AddClearCacheCompletedEventHandler_m903002842 (DeviceEvents_t3457131993 * __this, OnSomething_t1610687155 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::RemoveClearCacheCompletedEventHandler(MagicTV.globals.events.DeviceEvents/OnSomething)
extern "C"  void DeviceEvents_RemoveClearCacheCompletedEventHandler_m1097922651 (DeviceEvents_t3457131993 * __this, OnSomething_t1610687155 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::RaiseClearCacheCompleted()
extern "C"  void DeviceEvents_RaiseClearCacheCompleted_m2415543818 (DeviceEvents_t3457131993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::AddClearCacheEventHandler(MagicTV.globals.events.DeviceEvents/OnSomething)
extern "C"  void DeviceEvents_AddClearCacheEventHandler_m3346996759 (DeviceEvents_t3457131993 * __this, OnSomething_t1610687155 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::RemoveClearCacheEventHandler(MagicTV.globals.events.DeviceEvents/OnSomething)
extern "C"  void DeviceEvents_RemoveClearCacheEventHandler_m4288314230 (DeviceEvents_t3457131993 * __this, OnSomething_t1610687155 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::RaiseClearCache()
extern "C"  void DeviceEvents_RaiseClearCache_m3901952419 (DeviceEvents_t3457131993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::AddWantToClearCacheEventHandler(MagicTV.globals.events.DeviceEvents/OnSomething)
extern "C"  void DeviceEvents_AddWantToClearCacheEventHandler_m4012753004 (DeviceEvents_t3457131993 * __this, OnSomething_t1610687155 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::RemoveWantToClearCacheEventHandler(MagicTV.globals.events.DeviceEvents/OnSomething)
extern "C"  void DeviceEvents_RemoveWantToClearCacheEventHandler_m2885638795 (DeviceEvents_t3457131993 * __this, OnSomething_t1610687155 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::RaiseWantToClearCache()
extern "C"  void DeviceEvents_RaiseWantToClearCache_m600973358 (DeviceEvents_t3457131993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::AddResetClickedEventHandler(MagicTV.globals.events.DeviceEvents/OnSomething)
extern "C"  void DeviceEvents_AddResetClickedEventHandler_m987721428 (DeviceEvents_t3457131993 * __this, OnSomething_t1610687155 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::RemoveResetClickedEventHandler(MagicTV.globals.events.DeviceEvents/OnSomething)
extern "C"  void DeviceEvents_RemoveResetClickedEventHandler_m3650678899 (DeviceEvents_t3457131993 * __this, OnSomething_t1610687155 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::RaiseResetClicked()
extern "C"  void DeviceEvents_RaiseResetClicked_m441897030 (DeviceEvents_t3457131993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::RemoveResumeEventHandler(MagicTV.globals.events.DeviceEvents/OnSomething)
extern "C"  void DeviceEvents_RemoveResumeEventHandler_m3444575742 (DeviceEvents_t3457131993 * __this, OnSomething_t1610687155 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::AddResumeEventHandler(MagicTV.globals.events.DeviceEvents/OnSomething)
extern "C"  void DeviceEvents_AddResumeEventHandler_m3412666655 (DeviceEvents_t3457131993 * __this, OnSomething_t1610687155 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::RaiseResume()
extern "C"  void DeviceEvents_RaiseResume_m603941147 (DeviceEvents_t3457131993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::RemovePauseEventHandler(MagicTV.globals.events.DeviceEvents/OnSomething)
extern "C"  void DeviceEvents_RemovePauseEventHandler_m1301776443 (DeviceEvents_t3457131993 * __this, OnSomething_t1610687155 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::AddPauseEventHandler(MagicTV.globals.events.DeviceEvents/OnSomething)
extern "C"  void DeviceEvents_AddPauseEventHandler_m1716389114 (DeviceEvents_t3457131993 * __this, OnSomething_t1610687155 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::RaisePause()
extern "C"  void DeviceEvents_RaisePause_m1179777194 (DeviceEvents_t3457131993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::RemoveAwakeEventHandler(MagicTV.globals.events.DeviceEvents/OnSomething)
extern "C"  void DeviceEvents_RemoveAwakeEventHandler_m1194487564 (DeviceEvents_t3457131993 * __this, OnSomething_t1610687155 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::AddAwakeEventHandler(MagicTV.globals.events.DeviceEvents/OnSomething)
extern "C"  void DeviceEvents_AddAwakeEventHandler_m1609100235 (DeviceEvents_t3457131993 * __this, OnSomething_t1610687155 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.DeviceEvents::RaiseAwake()
extern "C"  void DeviceEvents_RaiseAwake_m1363256441 (DeviceEvents_t3457131993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.globals.events.DeviceEvents MagicTV.globals.events.DeviceEvents::GetInstance()
extern "C"  DeviceEvents_t3457131993 * DeviceEvents_GetInstance_m3715932249 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
