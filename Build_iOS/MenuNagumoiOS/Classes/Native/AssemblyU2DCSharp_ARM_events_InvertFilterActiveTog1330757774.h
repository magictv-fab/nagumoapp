﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract
struct TransformPositionAngleFilterAbstract_t4158746314;

#include "AssemblyU2DCSharp_ARM_abstracts_ToggleOnOffAbstract832893812.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.events.InvertFilterActiveToggleOnOff
struct  InvertFilterActiveToggleOnOff_t1330757774  : public ToggleOnOffAbstract_t832893812
{
public:
	// System.Boolean ARM.events.InvertFilterActiveToggleOnOff::ligado
	bool ___ligado_7;
	// System.Boolean ARM.events.InvertFilterActiveToggleOnOff::_lastValue
	bool ____lastValue_8;
	// System.Boolean ARM.events.InvertFilterActiveToggleOnOff::_hasFilter
	bool ____hasFilter_9;
	// ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract ARM.events.InvertFilterActiveToggleOnOff::filterToInvertActive
	TransformPositionAngleFilterAbstract_t4158746314 * ___filterToInvertActive_10;

public:
	inline static int32_t get_offset_of_ligado_7() { return static_cast<int32_t>(offsetof(InvertFilterActiveToggleOnOff_t1330757774, ___ligado_7)); }
	inline bool get_ligado_7() const { return ___ligado_7; }
	inline bool* get_address_of_ligado_7() { return &___ligado_7; }
	inline void set_ligado_7(bool value)
	{
		___ligado_7 = value;
	}

	inline static int32_t get_offset_of__lastValue_8() { return static_cast<int32_t>(offsetof(InvertFilterActiveToggleOnOff_t1330757774, ____lastValue_8)); }
	inline bool get__lastValue_8() const { return ____lastValue_8; }
	inline bool* get_address_of__lastValue_8() { return &____lastValue_8; }
	inline void set__lastValue_8(bool value)
	{
		____lastValue_8 = value;
	}

	inline static int32_t get_offset_of__hasFilter_9() { return static_cast<int32_t>(offsetof(InvertFilterActiveToggleOnOff_t1330757774, ____hasFilter_9)); }
	inline bool get__hasFilter_9() const { return ____hasFilter_9; }
	inline bool* get_address_of__hasFilter_9() { return &____hasFilter_9; }
	inline void set__hasFilter_9(bool value)
	{
		____hasFilter_9 = value;
	}

	inline static int32_t get_offset_of_filterToInvertActive_10() { return static_cast<int32_t>(offsetof(InvertFilterActiveToggleOnOff_t1330757774, ___filterToInvertActive_10)); }
	inline TransformPositionAngleFilterAbstract_t4158746314 * get_filterToInvertActive_10() const { return ___filterToInvertActive_10; }
	inline TransformPositionAngleFilterAbstract_t4158746314 ** get_address_of_filterToInvertActive_10() { return &___filterToInvertActive_10; }
	inline void set_filterToInvertActive_10(TransformPositionAngleFilterAbstract_t4158746314 * value)
	{
		___filterToInvertActive_10 = value;
		Il2CppCodeGenWriteBarrier(&___filterToInvertActive_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
