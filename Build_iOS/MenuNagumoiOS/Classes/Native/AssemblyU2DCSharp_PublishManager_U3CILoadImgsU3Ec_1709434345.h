﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PublicationData[]
struct PublicationDataU5BU5D_t1155489811;
// PublicationData
struct PublicationData_t473548758;
// UnityEngine.WWW
struct WWW_t3134621005;
// UnityEngine.Sprite
struct Sprite_t3199167241;
// System.Object
struct Il2CppObject;
// PublishManager
struct PublishManager_t4010203070;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PublishManager/<ILoadImgs>c__Iterator79
struct  U3CILoadImgsU3Ec__Iterator79_t1709434345  : public Il2CppObject
{
public:
	// PublicationData[] PublishManager/<ILoadImgs>c__Iterator79::<$s_345>__0
	PublicationDataU5BU5D_t1155489811* ___U3CU24s_345U3E__0_0;
	// System.Int32 PublishManager/<ILoadImgs>c__Iterator79::<$s_346>__1
	int32_t ___U3CU24s_346U3E__1_1;
	// PublicationData PublishManager/<ILoadImgs>c__Iterator79::<publicationData>__2
	PublicationData_t473548758 * ___U3CpublicationDataU3E__2_2;
	// UnityEngine.WWW PublishManager/<ILoadImgs>c__Iterator79::<www>__3
	WWW_t3134621005 * ___U3CwwwU3E__3_3;
	// UnityEngine.Sprite PublishManager/<ILoadImgs>c__Iterator79::<spt>__4
	Sprite_t3199167241 * ___U3CsptU3E__4_4;
	// System.Int32 PublishManager/<ILoadImgs>c__Iterator79::<i>__5
	int32_t ___U3CiU3E__5_5;
	// System.Int32 PublishManager/<ILoadImgs>c__Iterator79::$PC
	int32_t ___U24PC_6;
	// System.Object PublishManager/<ILoadImgs>c__Iterator79::$current
	Il2CppObject * ___U24current_7;
	// PublishManager PublishManager/<ILoadImgs>c__Iterator79::<>f__this
	PublishManager_t4010203070 * ___U3CU3Ef__this_8;

public:
	inline static int32_t get_offset_of_U3CU24s_345U3E__0_0() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator79_t1709434345, ___U3CU24s_345U3E__0_0)); }
	inline PublicationDataU5BU5D_t1155489811* get_U3CU24s_345U3E__0_0() const { return ___U3CU24s_345U3E__0_0; }
	inline PublicationDataU5BU5D_t1155489811** get_address_of_U3CU24s_345U3E__0_0() { return &___U3CU24s_345U3E__0_0; }
	inline void set_U3CU24s_345U3E__0_0(PublicationDataU5BU5D_t1155489811* value)
	{
		___U3CU24s_345U3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_345U3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CU24s_346U3E__1_1() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator79_t1709434345, ___U3CU24s_346U3E__1_1)); }
	inline int32_t get_U3CU24s_346U3E__1_1() const { return ___U3CU24s_346U3E__1_1; }
	inline int32_t* get_address_of_U3CU24s_346U3E__1_1() { return &___U3CU24s_346U3E__1_1; }
	inline void set_U3CU24s_346U3E__1_1(int32_t value)
	{
		___U3CU24s_346U3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CpublicationDataU3E__2_2() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator79_t1709434345, ___U3CpublicationDataU3E__2_2)); }
	inline PublicationData_t473548758 * get_U3CpublicationDataU3E__2_2() const { return ___U3CpublicationDataU3E__2_2; }
	inline PublicationData_t473548758 ** get_address_of_U3CpublicationDataU3E__2_2() { return &___U3CpublicationDataU3E__2_2; }
	inline void set_U3CpublicationDataU3E__2_2(PublicationData_t473548758 * value)
	{
		___U3CpublicationDataU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpublicationDataU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__3_3() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator79_t1709434345, ___U3CwwwU3E__3_3)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__3_3() const { return ___U3CwwwU3E__3_3; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__3_3() { return &___U3CwwwU3E__3_3; }
	inline void set_U3CwwwU3E__3_3(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CsptU3E__4_4() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator79_t1709434345, ___U3CsptU3E__4_4)); }
	inline Sprite_t3199167241 * get_U3CsptU3E__4_4() const { return ___U3CsptU3E__4_4; }
	inline Sprite_t3199167241 ** get_address_of_U3CsptU3E__4_4() { return &___U3CsptU3E__4_4; }
	inline void set_U3CsptU3E__4_4(Sprite_t3199167241 * value)
	{
		___U3CsptU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsptU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U3CiU3E__5_5() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator79_t1709434345, ___U3CiU3E__5_5)); }
	inline int32_t get_U3CiU3E__5_5() const { return ___U3CiU3E__5_5; }
	inline int32_t* get_address_of_U3CiU3E__5_5() { return &___U3CiU3E__5_5; }
	inline void set_U3CiU3E__5_5(int32_t value)
	{
		___U3CiU3E__5_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator79_t1709434345, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator79_t1709434345, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_8() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator79_t1709434345, ___U3CU3Ef__this_8)); }
	inline PublishManager_t4010203070 * get_U3CU3Ef__this_8() const { return ___U3CU3Ef__this_8; }
	inline PublishManager_t4010203070 ** get_address_of_U3CU3Ef__this_8() { return &___U3CU3Ef__this_8; }
	inline void set_U3CU3Ef__this_8(PublishManager_t4010203070 * value)
	{
		___U3CU3Ef__this_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
