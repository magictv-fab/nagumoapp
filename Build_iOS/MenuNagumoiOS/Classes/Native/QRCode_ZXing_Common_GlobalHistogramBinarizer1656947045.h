﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "QRCode_ZXing_Binarizer1492033400.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.GlobalHistogramBinarizer
struct  GlobalHistogramBinarizer_t1656947045  : public Binarizer_t1492033400
{
public:
	// System.Byte[] ZXing.Common.GlobalHistogramBinarizer::luminances
	ByteU5BU5D_t4260760469* ___luminances_2;
	// System.Int32[] ZXing.Common.GlobalHistogramBinarizer::buckets
	Int32U5BU5D_t3230847821* ___buckets_3;

public:
	inline static int32_t get_offset_of_luminances_2() { return static_cast<int32_t>(offsetof(GlobalHistogramBinarizer_t1656947045, ___luminances_2)); }
	inline ByteU5BU5D_t4260760469* get_luminances_2() const { return ___luminances_2; }
	inline ByteU5BU5D_t4260760469** get_address_of_luminances_2() { return &___luminances_2; }
	inline void set_luminances_2(ByteU5BU5D_t4260760469* value)
	{
		___luminances_2 = value;
		Il2CppCodeGenWriteBarrier(&___luminances_2, value);
	}

	inline static int32_t get_offset_of_buckets_3() { return static_cast<int32_t>(offsetof(GlobalHistogramBinarizer_t1656947045, ___buckets_3)); }
	inline Int32U5BU5D_t3230847821* get_buckets_3() const { return ___buckets_3; }
	inline Int32U5BU5D_t3230847821** get_address_of_buckets_3() { return &___buckets_3; }
	inline void set_buckets_3(Int32U5BU5D_t3230847821* value)
	{
		___buckets_3 = value;
		Il2CppCodeGenWriteBarrier(&___buckets_3, value);
	}
};

struct GlobalHistogramBinarizer_t1656947045_StaticFields
{
public:
	// System.Byte[] ZXing.Common.GlobalHistogramBinarizer::EMPTY
	ByteU5BU5D_t4260760469* ___EMPTY_1;

public:
	inline static int32_t get_offset_of_EMPTY_1() { return static_cast<int32_t>(offsetof(GlobalHistogramBinarizer_t1656947045_StaticFields, ___EMPTY_1)); }
	inline ByteU5BU5D_t4260760469* get_EMPTY_1() const { return ___EMPTY_1; }
	inline ByteU5BU5D_t4260760469** get_address_of_EMPTY_1() { return &___EMPTY_1; }
	inline void set_EMPTY_1(ByteU5BU5D_t4260760469* value)
	{
		___EMPTY_1 = value;
		Il2CppCodeGenWriteBarrier(&___EMPTY_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
