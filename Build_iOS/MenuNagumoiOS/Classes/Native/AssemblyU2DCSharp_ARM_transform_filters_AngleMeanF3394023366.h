﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.utils.PoolQuaternion
struct PoolQuaternion_t2988316782;

#include "AssemblyU2DCSharp_ARM_transform_filters_abstracts_4059889395.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.transform.filters.AngleMeanFilter
struct  AngleMeanFilter_t3394023366  : public HistoryAngleFilterAbstract_t4059889395
{
public:
	// System.Int32 ARM.transform.filters.AngleMeanFilter::maxSizeCache
	int32_t ___maxSizeCache_15;
	// ARM.utils.PoolQuaternion ARM.transform.filters.AngleMeanFilter::_history
	PoolQuaternion_t2988316782 * ____history_16;
	// System.Boolean ARM.transform.filters.AngleMeanFilter::_hasItem
	bool ____hasItem_17;
	// System.Boolean ARM.transform.filters.AngleMeanFilter::_loaded
	bool ____loaded_18;
	// System.Boolean ARM.transform.filters.AngleMeanFilter::isFull
	bool ___isFull_19;
	// System.Int32 ARM.transform.filters.AngleMeanFilter::_currentIndex
	int32_t ____currentIndex_20;

public:
	inline static int32_t get_offset_of_maxSizeCache_15() { return static_cast<int32_t>(offsetof(AngleMeanFilter_t3394023366, ___maxSizeCache_15)); }
	inline int32_t get_maxSizeCache_15() const { return ___maxSizeCache_15; }
	inline int32_t* get_address_of_maxSizeCache_15() { return &___maxSizeCache_15; }
	inline void set_maxSizeCache_15(int32_t value)
	{
		___maxSizeCache_15 = value;
	}

	inline static int32_t get_offset_of__history_16() { return static_cast<int32_t>(offsetof(AngleMeanFilter_t3394023366, ____history_16)); }
	inline PoolQuaternion_t2988316782 * get__history_16() const { return ____history_16; }
	inline PoolQuaternion_t2988316782 ** get_address_of__history_16() { return &____history_16; }
	inline void set__history_16(PoolQuaternion_t2988316782 * value)
	{
		____history_16 = value;
		Il2CppCodeGenWriteBarrier(&____history_16, value);
	}

	inline static int32_t get_offset_of__hasItem_17() { return static_cast<int32_t>(offsetof(AngleMeanFilter_t3394023366, ____hasItem_17)); }
	inline bool get__hasItem_17() const { return ____hasItem_17; }
	inline bool* get_address_of__hasItem_17() { return &____hasItem_17; }
	inline void set__hasItem_17(bool value)
	{
		____hasItem_17 = value;
	}

	inline static int32_t get_offset_of__loaded_18() { return static_cast<int32_t>(offsetof(AngleMeanFilter_t3394023366, ____loaded_18)); }
	inline bool get__loaded_18() const { return ____loaded_18; }
	inline bool* get_address_of__loaded_18() { return &____loaded_18; }
	inline void set__loaded_18(bool value)
	{
		____loaded_18 = value;
	}

	inline static int32_t get_offset_of_isFull_19() { return static_cast<int32_t>(offsetof(AngleMeanFilter_t3394023366, ___isFull_19)); }
	inline bool get_isFull_19() const { return ___isFull_19; }
	inline bool* get_address_of_isFull_19() { return &___isFull_19; }
	inline void set_isFull_19(bool value)
	{
		___isFull_19 = value;
	}

	inline static int32_t get_offset_of__currentIndex_20() { return static_cast<int32_t>(offsetof(AngleMeanFilter_t3394023366, ____currentIndex_20)); }
	inline int32_t get__currentIndex_20() const { return ____currentIndex_20; }
	inline int32_t* get_address_of__currentIndex_20() { return &____currentIndex_20; }
	inline void set__currentIndex_20(int32_t value)
	{
		____currentIndex_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
