﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation
struct DecodedInformation_t1859732120;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.Decoders.BlockParsedResult
struct  BlockParsedResult_t1963745509  : public Il2CppObject
{
public:
	// ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation ZXing.OneD.RSS.Expanded.Decoders.BlockParsedResult::decodedInformation
	DecodedInformation_t1859732120 * ___decodedInformation_0;
	// System.Boolean ZXing.OneD.RSS.Expanded.Decoders.BlockParsedResult::finished
	bool ___finished_1;

public:
	inline static int32_t get_offset_of_decodedInformation_0() { return static_cast<int32_t>(offsetof(BlockParsedResult_t1963745509, ___decodedInformation_0)); }
	inline DecodedInformation_t1859732120 * get_decodedInformation_0() const { return ___decodedInformation_0; }
	inline DecodedInformation_t1859732120 ** get_address_of_decodedInformation_0() { return &___decodedInformation_0; }
	inline void set_decodedInformation_0(DecodedInformation_t1859732120 * value)
	{
		___decodedInformation_0 = value;
		Il2CppCodeGenWriteBarrier(&___decodedInformation_0, value);
	}

	inline static int32_t get_offset_of_finished_1() { return static_cast<int32_t>(offsetof(BlockParsedResult_t1963745509, ___finished_1)); }
	inline bool get_finished_1() const { return ___finished_1; }
	inline bool* get_address_of_finished_1() { return &___finished_1; }
	inline void set_finished_1(bool value)
	{
		___finished_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
