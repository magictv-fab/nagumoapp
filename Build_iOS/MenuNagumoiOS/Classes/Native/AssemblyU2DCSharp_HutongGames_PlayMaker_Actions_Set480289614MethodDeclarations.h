﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetCameraFOV
struct SetCameraFOV_t480289614;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetCameraFOV::.ctor()
extern "C"  void SetCameraFOV__ctor_m1816519384 (SetCameraFOV_t480289614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetCameraFOV::Reset()
extern "C"  void SetCameraFOV_Reset_m3757919621 (SetCameraFOV_t480289614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetCameraFOV::OnEnter()
extern "C"  void SetCameraFOV_OnEnter_m1310134447 (SetCameraFOV_t480289614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetCameraFOV::OnUpdate()
extern "C"  void SetCameraFOV_OnUpdate_m1093021556 (SetCameraFOV_t480289614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetCameraFOV::DoSetCameraFOV()
extern "C"  void SetCameraFOV_DoSetCameraFOV_m937791165 (SetCameraFOV_t480289614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
