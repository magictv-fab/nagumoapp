﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkDisconnect
struct NetworkDisconnect_t1440046520;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkDisconnect::.ctor()
extern "C"  void NetworkDisconnect__ctor_m2576887422 (NetworkDisconnect_t1440046520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkDisconnect::OnEnter()
extern "C"  void NetworkDisconnect_OnEnter_m1879378645 (NetworkDisconnect_t1440046520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
