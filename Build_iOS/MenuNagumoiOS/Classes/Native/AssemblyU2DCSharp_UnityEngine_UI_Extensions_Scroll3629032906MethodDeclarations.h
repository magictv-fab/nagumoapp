﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Extensions.ScrollSnap/PageSnapChange
struct PageSnapChange_t3629032906;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void UnityEngine.UI.Extensions.ScrollSnap/PageSnapChange::.ctor(System.Object,System.IntPtr)
extern "C"  void PageSnapChange__ctor_m1193535137 (PageSnapChange_t3629032906 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnap/PageSnapChange::Invoke(System.Int32)
extern "C"  void PageSnapChange_Invoke_m4018425612 (PageSnapChange_t3629032906 * __this, int32_t ___page0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult UnityEngine.UI.Extensions.ScrollSnap/PageSnapChange::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * PageSnapChange_BeginInvoke_m3501720317 (PageSnapChange_t3629032906 * __this, int32_t ___page0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnap/PageSnapChange::EndInvoke(System.IAsyncResult)
extern "C"  void PageSnapChange_EndInvoke_m1340985649 (PageSnapChange_t3629032906 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
