﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "QRCode_ZXing_ReaderException1784959150.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.FormatException
struct  FormatException_t3082259450  : public ReaderException_t1784959150
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
