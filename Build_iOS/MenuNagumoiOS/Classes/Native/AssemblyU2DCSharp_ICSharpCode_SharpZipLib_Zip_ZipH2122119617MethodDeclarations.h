﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.ZipHelperStream
struct ZipHelperStream_t2122119617;
// System.String
struct String_t;
// System.IO.Stream
struct Stream_t1561764144;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// ICSharpCode.SharpZipLib.Zip.ZipEntry
struct ZipEntry_t3141689087;
// ICSharpCode.SharpZipLib.Zip.EntryPatchData
struct EntryPatchData_t4202945134;
// ICSharpCode.SharpZipLib.Zip.DescriptorData
struct DescriptorData_t3973589607;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "mscorlib_System_IO_SeekOrigin4120335598.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipE3141689087.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Entr4202945134.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Desc3973589607.h"

// System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::.ctor(System.String)
extern "C"  void ZipHelperStream__ctor_m374218088 (ZipHelperStream_t2122119617 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::.ctor(System.IO.Stream)
extern "C"  void ZipHelperStream__ctor_m2598987377 (ZipHelperStream_t2122119617 * __this, Stream_t1561764144 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipHelperStream::get_IsStreamOwner()
extern "C"  bool ZipHelperStream_get_IsStreamOwner_m2879207308 (ZipHelperStream_t2122119617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::set_IsStreamOwner(System.Boolean)
extern "C"  void ZipHelperStream_set_IsStreamOwner_m2757723 (ZipHelperStream_t2122119617 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipHelperStream::get_CanRead()
extern "C"  bool ZipHelperStream_get_CanRead_m1027615689 (ZipHelperStream_t2122119617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipHelperStream::get_CanSeek()
extern "C"  bool ZipHelperStream_get_CanSeek_m1056370731 (ZipHelperStream_t2122119617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipHelperStream::get_CanTimeout()
extern "C"  bool ZipHelperStream_get_CanTimeout_m390155824 (ZipHelperStream_t2122119617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipHelperStream::get_Length()
extern "C"  int64_t ZipHelperStream_get_Length_m2099870954 (ZipHelperStream_t2122119617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipHelperStream::get_Position()
extern "C"  int64_t ZipHelperStream_get_Position_m3857826221 (ZipHelperStream_t2122119617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::set_Position(System.Int64)
extern "C"  void ZipHelperStream_set_Position_m4286451938 (ZipHelperStream_t2122119617 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipHelperStream::get_CanWrite()
extern "C"  bool ZipHelperStream_get_CanWrite_m2313968814 (ZipHelperStream_t2122119617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::Flush()
extern "C"  void ZipHelperStream_Flush_m1793655548 (ZipHelperStream_t2122119617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipHelperStream::Seek(System.Int64,System.IO.SeekOrigin)
extern "C"  int64_t ZipHelperStream_Seek_m3804302522 (ZipHelperStream_t2122119617 * __this, int64_t ___offset0, int32_t ___origin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::SetLength(System.Int64)
extern "C"  void ZipHelperStream_SetLength_m535629362 (ZipHelperStream_t2122119617 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipHelperStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t ZipHelperStream_Read_m901729847 (ZipHelperStream_t2122119617 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C"  void ZipHelperStream_Write_m2248469234 (ZipHelperStream_t2122119617 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::Close()
extern "C"  void ZipHelperStream_Close_m3420567792 (ZipHelperStream_t2122119617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::WriteLocalHeader(ICSharpCode.SharpZipLib.Zip.ZipEntry,ICSharpCode.SharpZipLib.Zip.EntryPatchData)
extern "C"  void ZipHelperStream_WriteLocalHeader_m3369775188 (ZipHelperStream_t2122119617 * __this, ZipEntry_t3141689087 * ___entry0, EntryPatchData_t4202945134 * ___patchData1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipHelperStream::LocateBlockWithSignature(System.Int32,System.Int64,System.Int32,System.Int32)
extern "C"  int64_t ZipHelperStream_LocateBlockWithSignature_m3951985813 (ZipHelperStream_t2122119617 * __this, int32_t ___signature0, int64_t ___endLocation1, int32_t ___minimumBlockSize2, int32_t ___maximumVariableData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::WriteZip64EndOfCentralDirectory(System.Int64,System.Int64,System.Int64)
extern "C"  void ZipHelperStream_WriteZip64EndOfCentralDirectory_m1778174740 (ZipHelperStream_t2122119617 * __this, int64_t ___noOfEntries0, int64_t ___sizeEntries1, int64_t ___centralDirOffset2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::WriteEndOfCentralDirectory(System.Int64,System.Int64,System.Int64,System.Byte[])
extern "C"  void ZipHelperStream_WriteEndOfCentralDirectory_m2163113738 (ZipHelperStream_t2122119617 * __this, int64_t ___noOfEntries0, int64_t ___sizeEntries1, int64_t ___startOfCentralDirectory2, ByteU5BU5D_t4260760469* ___comment3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipHelperStream::ReadLEShort()
extern "C"  int32_t ZipHelperStream_ReadLEShort_m2237284659 (ZipHelperStream_t2122119617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipHelperStream::ReadLEInt()
extern "C"  int32_t ZipHelperStream_ReadLEInt_m909474150 (ZipHelperStream_t2122119617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipHelperStream::ReadLELong()
extern "C"  int64_t ZipHelperStream_ReadLELong_m3374023334 (ZipHelperStream_t2122119617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::WriteLEShort(System.Int32)
extern "C"  void ZipHelperStream_WriteLEShort_m897698367 (ZipHelperStream_t2122119617 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::WriteLEUshort(System.UInt16)
extern "C"  void ZipHelperStream_WriteLEUshort_m1900530749 (ZipHelperStream_t2122119617 * __this, uint16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::WriteLEInt(System.Int32)
extern "C"  void ZipHelperStream_WriteLEInt_m1827545266 (ZipHelperStream_t2122119617 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::WriteLEUint(System.UInt32)
extern "C"  void ZipHelperStream_WriteLEUint_m3920379120 (ZipHelperStream_t2122119617 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::WriteLELong(System.Int64)
extern "C"  void ZipHelperStream_WriteLELong_m1330411966 (ZipHelperStream_t2122119617 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::WriteLEUlong(System.UInt64)
extern "C"  void ZipHelperStream_WriteLEUlong_m84245688 (ZipHelperStream_t2122119617 * __this, uint64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipHelperStream::WriteDataDescriptor(ICSharpCode.SharpZipLib.Zip.ZipEntry)
extern "C"  int32_t ZipHelperStream_WriteDataDescriptor_m4171226415 (ZipHelperStream_t2122119617 * __this, ZipEntry_t3141689087 * ___entry0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::ReadDataDescriptor(System.Boolean,ICSharpCode.SharpZipLib.Zip.DescriptorData)
extern "C"  void ZipHelperStream_ReadDataDescriptor_m1084832639 (ZipHelperStream_t2122119617 * __this, bool ___zip640, DescriptorData_t3973589607 * ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
