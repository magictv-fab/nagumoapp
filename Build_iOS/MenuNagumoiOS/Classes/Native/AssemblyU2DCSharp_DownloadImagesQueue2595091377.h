﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MagicTV.vo.RequestImagesItemVO[]
struct RequestImagesItemVOU5BU5D_t245365736;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;

#include "AssemblyU2DCSharp_ARM_abstracts_ProgressEventsAbst2129719228.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DownloadImagesQueue
struct  DownloadImagesQueue_t2595091377  : public ProgressEventsAbstract_t2129719228
{
public:
	// MagicTV.vo.RequestImagesItemVO[] DownloadImagesQueue::_images
	RequestImagesItemVOU5BU5D_t245365736* ____images_14;
	// System.Int32 DownloadImagesQueue::totalBytes
	int32_t ___totalBytes_15;
	// System.Single DownloadImagesQueue::totalBytesLoaded
	float ___totalBytesLoaded_16;
	// System.Collections.Generic.List`1<System.String> DownloadImagesQueue::_paths
	List_1_t1375417109 * ____paths_17;
	// System.Int32 DownloadImagesQueue::currentDownloadIndex
	int32_t ___currentDownloadIndex_18;

public:
	inline static int32_t get_offset_of__images_14() { return static_cast<int32_t>(offsetof(DownloadImagesQueue_t2595091377, ____images_14)); }
	inline RequestImagesItemVOU5BU5D_t245365736* get__images_14() const { return ____images_14; }
	inline RequestImagesItemVOU5BU5D_t245365736** get_address_of__images_14() { return &____images_14; }
	inline void set__images_14(RequestImagesItemVOU5BU5D_t245365736* value)
	{
		____images_14 = value;
		Il2CppCodeGenWriteBarrier(&____images_14, value);
	}

	inline static int32_t get_offset_of_totalBytes_15() { return static_cast<int32_t>(offsetof(DownloadImagesQueue_t2595091377, ___totalBytes_15)); }
	inline int32_t get_totalBytes_15() const { return ___totalBytes_15; }
	inline int32_t* get_address_of_totalBytes_15() { return &___totalBytes_15; }
	inline void set_totalBytes_15(int32_t value)
	{
		___totalBytes_15 = value;
	}

	inline static int32_t get_offset_of_totalBytesLoaded_16() { return static_cast<int32_t>(offsetof(DownloadImagesQueue_t2595091377, ___totalBytesLoaded_16)); }
	inline float get_totalBytesLoaded_16() const { return ___totalBytesLoaded_16; }
	inline float* get_address_of_totalBytesLoaded_16() { return &___totalBytesLoaded_16; }
	inline void set_totalBytesLoaded_16(float value)
	{
		___totalBytesLoaded_16 = value;
	}

	inline static int32_t get_offset_of__paths_17() { return static_cast<int32_t>(offsetof(DownloadImagesQueue_t2595091377, ____paths_17)); }
	inline List_1_t1375417109 * get__paths_17() const { return ____paths_17; }
	inline List_1_t1375417109 ** get_address_of__paths_17() { return &____paths_17; }
	inline void set__paths_17(List_1_t1375417109 * value)
	{
		____paths_17 = value;
		Il2CppCodeGenWriteBarrier(&____paths_17, value);
	}

	inline static int32_t get_offset_of_currentDownloadIndex_18() { return static_cast<int32_t>(offsetof(DownloadImagesQueue_t2595091377, ___currentDownloadIndex_18)); }
	inline int32_t get_currentDownloadIndex_18() const { return ___currentDownloadIndex_18; }
	inline int32_t* get_address_of_currentDownloadIndex_18() { return &___currentDownloadIndex_18; }
	inline void set_currentDownloadIndex_18(int32_t value)
	{
		___currentDownloadIndex_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
