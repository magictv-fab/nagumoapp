﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "QRCode_ZXing_OneD_RSS_Expanded_Decoders_DecodedObje746289471.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.Decoders.DecodedNumeric
struct  DecodedNumeric_t133612409  : public DecodedObject_t746289471
{
public:
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.DecodedNumeric::firstDigit
	int32_t ___firstDigit_1;
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.DecodedNumeric::secondDigit
	int32_t ___secondDigit_2;

public:
	inline static int32_t get_offset_of_firstDigit_1() { return static_cast<int32_t>(offsetof(DecodedNumeric_t133612409, ___firstDigit_1)); }
	inline int32_t get_firstDigit_1() const { return ___firstDigit_1; }
	inline int32_t* get_address_of_firstDigit_1() { return &___firstDigit_1; }
	inline void set_firstDigit_1(int32_t value)
	{
		___firstDigit_1 = value;
	}

	inline static int32_t get_offset_of_secondDigit_2() { return static_cast<int32_t>(offsetof(DecodedNumeric_t133612409, ___secondDigit_2)); }
	inline int32_t get_secondDigit_2() const { return ___secondDigit_2; }
	inline int32_t* get_address_of_secondDigit_2() { return &___secondDigit_2; }
	inline void set_secondDigit_2(int32_t value)
	{
		___secondDigit_2 = value;
	}
};

struct DecodedNumeric_t133612409_StaticFields
{
public:
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.DecodedNumeric::FNC1
	int32_t ___FNC1_3;

public:
	inline static int32_t get_offset_of_FNC1_3() { return static_cast<int32_t>(offsetof(DecodedNumeric_t133612409_StaticFields, ___FNC1_3)); }
	inline int32_t get_FNC1_3() const { return ___FNC1_3; }
	inline int32_t* get_address_of_FNC1_3() { return &___FNC1_3; }
	inline void set_FNC1_3(int32_t value)
	{
		___FNC1_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
