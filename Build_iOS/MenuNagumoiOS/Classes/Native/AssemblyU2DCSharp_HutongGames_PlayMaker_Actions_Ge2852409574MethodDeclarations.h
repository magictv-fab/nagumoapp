﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsName
struct GetAnimatorCurrentStateInfoIsName_t2852409574;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsName::.ctor()
extern "C"  void GetAnimatorCurrentStateInfoIsName__ctor_m934420240 (GetAnimatorCurrentStateInfoIsName_t2852409574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsName::Reset()
extern "C"  void GetAnimatorCurrentStateInfoIsName_Reset_m2875820477 (GetAnimatorCurrentStateInfoIsName_t2852409574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsName::OnEnter()
extern "C"  void GetAnimatorCurrentStateInfoIsName_OnEnter_m4016381671 (GetAnimatorCurrentStateInfoIsName_t2852409574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsName::OnUpdate()
extern "C"  void GetAnimatorCurrentStateInfoIsName_OnUpdate_m3382306876 (GetAnimatorCurrentStateInfoIsName_t2852409574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsName::IsName()
extern "C"  void GetAnimatorCurrentStateInfoIsName_IsName_m3222692105 (GetAnimatorCurrentStateInfoIsName_t2852409574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
