﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Texture
struct Texture_t2526458961;
// AttachedAudio
struct AttachedAudio_t711869554;
// UnityEngine.GUIStyle
struct GUIStyle_t2990928826;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_DigitsLocation3189585627.h"
#include "AssemblyU2DCSharp_UVT_PlayMode874588163.h"
#include "AssemblyU2DCSharp_LowMemoryMode1214027928.h"
#include "AssemblyU2DCSharp_UVT_PlayState1348117041.h"
#include "AssemblyU2DCSharp_UVT_PlayDirection3288864031.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VideoTexture_FullScreen
struct  VideoTexture_FullScreen_t1562162522  : public MonoBehaviour_t667441552
{
public:
	// System.Single VideoTexture_FullScreen::FPS
	float ___FPS_2;
	// System.Int32 VideoTexture_FullScreen::firstFrame
	int32_t ___firstFrame_3;
	// System.Int32 VideoTexture_FullScreen::lastFrame
	int32_t ___lastFrame_4;
	// System.String VideoTexture_FullScreen::FileName
	String_t* ___FileName_5;
	// System.String VideoTexture_FullScreen::digitsFormat
	String_t* ___digitsFormat_6;
	// DigitsLocation VideoTexture_FullScreen::digitsLocation
	int32_t ___digitsLocation_7;
	// System.Single VideoTexture_FullScreen::aspectRatio
	float ___aspectRatio_8;
	// UVT_PlayMode VideoTexture_FullScreen::playMode
	int32_t ___playMode_9;
	// System.Boolean VideoTexture_FullScreen::pingpongStartsWithReverse
	bool ___pingpongStartsWithReverse_10;
	// System.Int32 VideoTexture_FullScreen::numberOfLoops
	int32_t ___numberOfLoops_11;
	// LowMemoryMode VideoTexture_FullScreen::lowMemoryMode
	int32_t ___lowMemoryMode_12;
	// UnityEngine.Texture VideoTexture_FullScreen::ctiTexture
	Texture_t2526458961 * ___ctiTexture_13;
	// UnityEngine.Texture VideoTexture_FullScreen::backgroundTexture
	Texture_t2526458961 * ___backgroundTexture_14;
	// UnityEngine.Texture VideoTexture_FullScreen::scrollBarTexture
	Texture_t2526458961 * ___scrollBarTexture_15;
	// System.Single VideoTexture_FullScreen::scrollBarLength
	float ___scrollBarLength_16;
	// System.Single VideoTexture_FullScreen::scrollBarHeight
	float ___scrollBarHeight_17;
	// System.Single VideoTexture_FullScreen::scrollBarOffset
	float ___scrollBarOffset_18;
	// System.Int32 VideoTexture_FullScreen::timecodeSize
	int32_t ___timecodeSize_19;
	// System.Boolean VideoTexture_FullScreen::hideBackground
	bool ___hideBackground_20;
	// System.Boolean VideoTexture_FullScreen::showScrollBar
	bool ___showScrollBar_21;
	// System.Boolean VideoTexture_FullScreen::showTimecode
	bool ___showTimecode_22;
	// System.Boolean VideoTexture_FullScreen::enableTogglePlay
	bool ___enableTogglePlay_23;
	// System.Boolean VideoTexture_FullScreen::enableAudio
	bool ___enableAudio_24;
	// System.Boolean VideoTexture_FullScreen::forceAudioSync
	bool ___forceAudioSync_25;
	// System.Boolean VideoTexture_FullScreen::autoPlay
	bool ___autoPlay_26;
	// System.Boolean VideoTexture_FullScreen::autoHideWhenDone
	bool ___autoHideWhenDone_27;
	// System.Boolean VideoTexture_FullScreen::autoLoadLevelWhenDone
	bool ___autoLoadLevelWhenDone_28;
	// System.String VideoTexture_FullScreen::LevelToLoad
	String_t* ___LevelToLoad_29;
	// UVT_PlayState VideoTexture_FullScreen::playState
	int32_t ___playState_30;
	// UVT_PlayDirection VideoTexture_FullScreen::playDirection
	int32_t ___playDirection_31;
	// System.Boolean VideoTexture_FullScreen::enableControls
	bool ___enableControls_32;
	// System.Boolean VideoTexture_FullScreen::scrubbing
	bool ___scrubbing_33;
	// System.Boolean VideoTexture_FullScreen::audioAttached
	bool ___audioAttached_34;
	// System.Int32 VideoTexture_FullScreen::loopNumber
	int32_t ___loopNumber_35;
	// System.String VideoTexture_FullScreen::indexStr
	String_t* ___indexStr_36;
	// UnityEngine.Texture VideoTexture_FullScreen::newTex
	Texture_t2526458961 * ___newTex_37;
	// UnityEngine.Texture VideoTexture_FullScreen::lastTex
	Texture_t2526458961 * ___lastTex_38;
	// System.Single VideoTexture_FullScreen::playFactor
	float ___playFactor_39;
	// System.Single VideoTexture_FullScreen::currentPosition
	float ___currentPosition_40;
	// System.Single VideoTexture_FullScreen::index
	float ___index_41;
	// System.Int32 VideoTexture_FullScreen::intIndex
	int32_t ___intIndex_42;
	// System.Int32 VideoTexture_FullScreen::lastIndex
	int32_t ___lastIndex_43;
	// AttachedAudio VideoTexture_FullScreen::myAudio
	AttachedAudio_t711869554 * ___myAudio_44;
	// UnityEngine.Rect VideoTexture_FullScreen::CTI
	Rect_t4241904616  ___CTI_45;
	// UnityEngine.GUIStyle VideoTexture_FullScreen::style
	GUIStyle_t2990928826 * ___style_46;
	// System.Boolean VideoTexture_FullScreen::enableGUI
	bool ___enableGUI_47;

public:
	inline static int32_t get_offset_of_FPS_2() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___FPS_2)); }
	inline float get_FPS_2() const { return ___FPS_2; }
	inline float* get_address_of_FPS_2() { return &___FPS_2; }
	inline void set_FPS_2(float value)
	{
		___FPS_2 = value;
	}

	inline static int32_t get_offset_of_firstFrame_3() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___firstFrame_3)); }
	inline int32_t get_firstFrame_3() const { return ___firstFrame_3; }
	inline int32_t* get_address_of_firstFrame_3() { return &___firstFrame_3; }
	inline void set_firstFrame_3(int32_t value)
	{
		___firstFrame_3 = value;
	}

	inline static int32_t get_offset_of_lastFrame_4() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___lastFrame_4)); }
	inline int32_t get_lastFrame_4() const { return ___lastFrame_4; }
	inline int32_t* get_address_of_lastFrame_4() { return &___lastFrame_4; }
	inline void set_lastFrame_4(int32_t value)
	{
		___lastFrame_4 = value;
	}

	inline static int32_t get_offset_of_FileName_5() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___FileName_5)); }
	inline String_t* get_FileName_5() const { return ___FileName_5; }
	inline String_t** get_address_of_FileName_5() { return &___FileName_5; }
	inline void set_FileName_5(String_t* value)
	{
		___FileName_5 = value;
		Il2CppCodeGenWriteBarrier(&___FileName_5, value);
	}

	inline static int32_t get_offset_of_digitsFormat_6() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___digitsFormat_6)); }
	inline String_t* get_digitsFormat_6() const { return ___digitsFormat_6; }
	inline String_t** get_address_of_digitsFormat_6() { return &___digitsFormat_6; }
	inline void set_digitsFormat_6(String_t* value)
	{
		___digitsFormat_6 = value;
		Il2CppCodeGenWriteBarrier(&___digitsFormat_6, value);
	}

	inline static int32_t get_offset_of_digitsLocation_7() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___digitsLocation_7)); }
	inline int32_t get_digitsLocation_7() const { return ___digitsLocation_7; }
	inline int32_t* get_address_of_digitsLocation_7() { return &___digitsLocation_7; }
	inline void set_digitsLocation_7(int32_t value)
	{
		___digitsLocation_7 = value;
	}

	inline static int32_t get_offset_of_aspectRatio_8() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___aspectRatio_8)); }
	inline float get_aspectRatio_8() const { return ___aspectRatio_8; }
	inline float* get_address_of_aspectRatio_8() { return &___aspectRatio_8; }
	inline void set_aspectRatio_8(float value)
	{
		___aspectRatio_8 = value;
	}

	inline static int32_t get_offset_of_playMode_9() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___playMode_9)); }
	inline int32_t get_playMode_9() const { return ___playMode_9; }
	inline int32_t* get_address_of_playMode_9() { return &___playMode_9; }
	inline void set_playMode_9(int32_t value)
	{
		___playMode_9 = value;
	}

	inline static int32_t get_offset_of_pingpongStartsWithReverse_10() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___pingpongStartsWithReverse_10)); }
	inline bool get_pingpongStartsWithReverse_10() const { return ___pingpongStartsWithReverse_10; }
	inline bool* get_address_of_pingpongStartsWithReverse_10() { return &___pingpongStartsWithReverse_10; }
	inline void set_pingpongStartsWithReverse_10(bool value)
	{
		___pingpongStartsWithReverse_10 = value;
	}

	inline static int32_t get_offset_of_numberOfLoops_11() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___numberOfLoops_11)); }
	inline int32_t get_numberOfLoops_11() const { return ___numberOfLoops_11; }
	inline int32_t* get_address_of_numberOfLoops_11() { return &___numberOfLoops_11; }
	inline void set_numberOfLoops_11(int32_t value)
	{
		___numberOfLoops_11 = value;
	}

	inline static int32_t get_offset_of_lowMemoryMode_12() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___lowMemoryMode_12)); }
	inline int32_t get_lowMemoryMode_12() const { return ___lowMemoryMode_12; }
	inline int32_t* get_address_of_lowMemoryMode_12() { return &___lowMemoryMode_12; }
	inline void set_lowMemoryMode_12(int32_t value)
	{
		___lowMemoryMode_12 = value;
	}

	inline static int32_t get_offset_of_ctiTexture_13() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___ctiTexture_13)); }
	inline Texture_t2526458961 * get_ctiTexture_13() const { return ___ctiTexture_13; }
	inline Texture_t2526458961 ** get_address_of_ctiTexture_13() { return &___ctiTexture_13; }
	inline void set_ctiTexture_13(Texture_t2526458961 * value)
	{
		___ctiTexture_13 = value;
		Il2CppCodeGenWriteBarrier(&___ctiTexture_13, value);
	}

	inline static int32_t get_offset_of_backgroundTexture_14() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___backgroundTexture_14)); }
	inline Texture_t2526458961 * get_backgroundTexture_14() const { return ___backgroundTexture_14; }
	inline Texture_t2526458961 ** get_address_of_backgroundTexture_14() { return &___backgroundTexture_14; }
	inline void set_backgroundTexture_14(Texture_t2526458961 * value)
	{
		___backgroundTexture_14 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundTexture_14, value);
	}

	inline static int32_t get_offset_of_scrollBarTexture_15() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___scrollBarTexture_15)); }
	inline Texture_t2526458961 * get_scrollBarTexture_15() const { return ___scrollBarTexture_15; }
	inline Texture_t2526458961 ** get_address_of_scrollBarTexture_15() { return &___scrollBarTexture_15; }
	inline void set_scrollBarTexture_15(Texture_t2526458961 * value)
	{
		___scrollBarTexture_15 = value;
		Il2CppCodeGenWriteBarrier(&___scrollBarTexture_15, value);
	}

	inline static int32_t get_offset_of_scrollBarLength_16() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___scrollBarLength_16)); }
	inline float get_scrollBarLength_16() const { return ___scrollBarLength_16; }
	inline float* get_address_of_scrollBarLength_16() { return &___scrollBarLength_16; }
	inline void set_scrollBarLength_16(float value)
	{
		___scrollBarLength_16 = value;
	}

	inline static int32_t get_offset_of_scrollBarHeight_17() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___scrollBarHeight_17)); }
	inline float get_scrollBarHeight_17() const { return ___scrollBarHeight_17; }
	inline float* get_address_of_scrollBarHeight_17() { return &___scrollBarHeight_17; }
	inline void set_scrollBarHeight_17(float value)
	{
		___scrollBarHeight_17 = value;
	}

	inline static int32_t get_offset_of_scrollBarOffset_18() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___scrollBarOffset_18)); }
	inline float get_scrollBarOffset_18() const { return ___scrollBarOffset_18; }
	inline float* get_address_of_scrollBarOffset_18() { return &___scrollBarOffset_18; }
	inline void set_scrollBarOffset_18(float value)
	{
		___scrollBarOffset_18 = value;
	}

	inline static int32_t get_offset_of_timecodeSize_19() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___timecodeSize_19)); }
	inline int32_t get_timecodeSize_19() const { return ___timecodeSize_19; }
	inline int32_t* get_address_of_timecodeSize_19() { return &___timecodeSize_19; }
	inline void set_timecodeSize_19(int32_t value)
	{
		___timecodeSize_19 = value;
	}

	inline static int32_t get_offset_of_hideBackground_20() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___hideBackground_20)); }
	inline bool get_hideBackground_20() const { return ___hideBackground_20; }
	inline bool* get_address_of_hideBackground_20() { return &___hideBackground_20; }
	inline void set_hideBackground_20(bool value)
	{
		___hideBackground_20 = value;
	}

	inline static int32_t get_offset_of_showScrollBar_21() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___showScrollBar_21)); }
	inline bool get_showScrollBar_21() const { return ___showScrollBar_21; }
	inline bool* get_address_of_showScrollBar_21() { return &___showScrollBar_21; }
	inline void set_showScrollBar_21(bool value)
	{
		___showScrollBar_21 = value;
	}

	inline static int32_t get_offset_of_showTimecode_22() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___showTimecode_22)); }
	inline bool get_showTimecode_22() const { return ___showTimecode_22; }
	inline bool* get_address_of_showTimecode_22() { return &___showTimecode_22; }
	inline void set_showTimecode_22(bool value)
	{
		___showTimecode_22 = value;
	}

	inline static int32_t get_offset_of_enableTogglePlay_23() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___enableTogglePlay_23)); }
	inline bool get_enableTogglePlay_23() const { return ___enableTogglePlay_23; }
	inline bool* get_address_of_enableTogglePlay_23() { return &___enableTogglePlay_23; }
	inline void set_enableTogglePlay_23(bool value)
	{
		___enableTogglePlay_23 = value;
	}

	inline static int32_t get_offset_of_enableAudio_24() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___enableAudio_24)); }
	inline bool get_enableAudio_24() const { return ___enableAudio_24; }
	inline bool* get_address_of_enableAudio_24() { return &___enableAudio_24; }
	inline void set_enableAudio_24(bool value)
	{
		___enableAudio_24 = value;
	}

	inline static int32_t get_offset_of_forceAudioSync_25() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___forceAudioSync_25)); }
	inline bool get_forceAudioSync_25() const { return ___forceAudioSync_25; }
	inline bool* get_address_of_forceAudioSync_25() { return &___forceAudioSync_25; }
	inline void set_forceAudioSync_25(bool value)
	{
		___forceAudioSync_25 = value;
	}

	inline static int32_t get_offset_of_autoPlay_26() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___autoPlay_26)); }
	inline bool get_autoPlay_26() const { return ___autoPlay_26; }
	inline bool* get_address_of_autoPlay_26() { return &___autoPlay_26; }
	inline void set_autoPlay_26(bool value)
	{
		___autoPlay_26 = value;
	}

	inline static int32_t get_offset_of_autoHideWhenDone_27() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___autoHideWhenDone_27)); }
	inline bool get_autoHideWhenDone_27() const { return ___autoHideWhenDone_27; }
	inline bool* get_address_of_autoHideWhenDone_27() { return &___autoHideWhenDone_27; }
	inline void set_autoHideWhenDone_27(bool value)
	{
		___autoHideWhenDone_27 = value;
	}

	inline static int32_t get_offset_of_autoLoadLevelWhenDone_28() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___autoLoadLevelWhenDone_28)); }
	inline bool get_autoLoadLevelWhenDone_28() const { return ___autoLoadLevelWhenDone_28; }
	inline bool* get_address_of_autoLoadLevelWhenDone_28() { return &___autoLoadLevelWhenDone_28; }
	inline void set_autoLoadLevelWhenDone_28(bool value)
	{
		___autoLoadLevelWhenDone_28 = value;
	}

	inline static int32_t get_offset_of_LevelToLoad_29() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___LevelToLoad_29)); }
	inline String_t* get_LevelToLoad_29() const { return ___LevelToLoad_29; }
	inline String_t** get_address_of_LevelToLoad_29() { return &___LevelToLoad_29; }
	inline void set_LevelToLoad_29(String_t* value)
	{
		___LevelToLoad_29 = value;
		Il2CppCodeGenWriteBarrier(&___LevelToLoad_29, value);
	}

	inline static int32_t get_offset_of_playState_30() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___playState_30)); }
	inline int32_t get_playState_30() const { return ___playState_30; }
	inline int32_t* get_address_of_playState_30() { return &___playState_30; }
	inline void set_playState_30(int32_t value)
	{
		___playState_30 = value;
	}

	inline static int32_t get_offset_of_playDirection_31() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___playDirection_31)); }
	inline int32_t get_playDirection_31() const { return ___playDirection_31; }
	inline int32_t* get_address_of_playDirection_31() { return &___playDirection_31; }
	inline void set_playDirection_31(int32_t value)
	{
		___playDirection_31 = value;
	}

	inline static int32_t get_offset_of_enableControls_32() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___enableControls_32)); }
	inline bool get_enableControls_32() const { return ___enableControls_32; }
	inline bool* get_address_of_enableControls_32() { return &___enableControls_32; }
	inline void set_enableControls_32(bool value)
	{
		___enableControls_32 = value;
	}

	inline static int32_t get_offset_of_scrubbing_33() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___scrubbing_33)); }
	inline bool get_scrubbing_33() const { return ___scrubbing_33; }
	inline bool* get_address_of_scrubbing_33() { return &___scrubbing_33; }
	inline void set_scrubbing_33(bool value)
	{
		___scrubbing_33 = value;
	}

	inline static int32_t get_offset_of_audioAttached_34() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___audioAttached_34)); }
	inline bool get_audioAttached_34() const { return ___audioAttached_34; }
	inline bool* get_address_of_audioAttached_34() { return &___audioAttached_34; }
	inline void set_audioAttached_34(bool value)
	{
		___audioAttached_34 = value;
	}

	inline static int32_t get_offset_of_loopNumber_35() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___loopNumber_35)); }
	inline int32_t get_loopNumber_35() const { return ___loopNumber_35; }
	inline int32_t* get_address_of_loopNumber_35() { return &___loopNumber_35; }
	inline void set_loopNumber_35(int32_t value)
	{
		___loopNumber_35 = value;
	}

	inline static int32_t get_offset_of_indexStr_36() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___indexStr_36)); }
	inline String_t* get_indexStr_36() const { return ___indexStr_36; }
	inline String_t** get_address_of_indexStr_36() { return &___indexStr_36; }
	inline void set_indexStr_36(String_t* value)
	{
		___indexStr_36 = value;
		Il2CppCodeGenWriteBarrier(&___indexStr_36, value);
	}

	inline static int32_t get_offset_of_newTex_37() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___newTex_37)); }
	inline Texture_t2526458961 * get_newTex_37() const { return ___newTex_37; }
	inline Texture_t2526458961 ** get_address_of_newTex_37() { return &___newTex_37; }
	inline void set_newTex_37(Texture_t2526458961 * value)
	{
		___newTex_37 = value;
		Il2CppCodeGenWriteBarrier(&___newTex_37, value);
	}

	inline static int32_t get_offset_of_lastTex_38() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___lastTex_38)); }
	inline Texture_t2526458961 * get_lastTex_38() const { return ___lastTex_38; }
	inline Texture_t2526458961 ** get_address_of_lastTex_38() { return &___lastTex_38; }
	inline void set_lastTex_38(Texture_t2526458961 * value)
	{
		___lastTex_38 = value;
		Il2CppCodeGenWriteBarrier(&___lastTex_38, value);
	}

	inline static int32_t get_offset_of_playFactor_39() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___playFactor_39)); }
	inline float get_playFactor_39() const { return ___playFactor_39; }
	inline float* get_address_of_playFactor_39() { return &___playFactor_39; }
	inline void set_playFactor_39(float value)
	{
		___playFactor_39 = value;
	}

	inline static int32_t get_offset_of_currentPosition_40() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___currentPosition_40)); }
	inline float get_currentPosition_40() const { return ___currentPosition_40; }
	inline float* get_address_of_currentPosition_40() { return &___currentPosition_40; }
	inline void set_currentPosition_40(float value)
	{
		___currentPosition_40 = value;
	}

	inline static int32_t get_offset_of_index_41() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___index_41)); }
	inline float get_index_41() const { return ___index_41; }
	inline float* get_address_of_index_41() { return &___index_41; }
	inline void set_index_41(float value)
	{
		___index_41 = value;
	}

	inline static int32_t get_offset_of_intIndex_42() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___intIndex_42)); }
	inline int32_t get_intIndex_42() const { return ___intIndex_42; }
	inline int32_t* get_address_of_intIndex_42() { return &___intIndex_42; }
	inline void set_intIndex_42(int32_t value)
	{
		___intIndex_42 = value;
	}

	inline static int32_t get_offset_of_lastIndex_43() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___lastIndex_43)); }
	inline int32_t get_lastIndex_43() const { return ___lastIndex_43; }
	inline int32_t* get_address_of_lastIndex_43() { return &___lastIndex_43; }
	inline void set_lastIndex_43(int32_t value)
	{
		___lastIndex_43 = value;
	}

	inline static int32_t get_offset_of_myAudio_44() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___myAudio_44)); }
	inline AttachedAudio_t711869554 * get_myAudio_44() const { return ___myAudio_44; }
	inline AttachedAudio_t711869554 ** get_address_of_myAudio_44() { return &___myAudio_44; }
	inline void set_myAudio_44(AttachedAudio_t711869554 * value)
	{
		___myAudio_44 = value;
		Il2CppCodeGenWriteBarrier(&___myAudio_44, value);
	}

	inline static int32_t get_offset_of_CTI_45() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___CTI_45)); }
	inline Rect_t4241904616  get_CTI_45() const { return ___CTI_45; }
	inline Rect_t4241904616 * get_address_of_CTI_45() { return &___CTI_45; }
	inline void set_CTI_45(Rect_t4241904616  value)
	{
		___CTI_45 = value;
	}

	inline static int32_t get_offset_of_style_46() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___style_46)); }
	inline GUIStyle_t2990928826 * get_style_46() const { return ___style_46; }
	inline GUIStyle_t2990928826 ** get_address_of_style_46() { return &___style_46; }
	inline void set_style_46(GUIStyle_t2990928826 * value)
	{
		___style_46 = value;
		Il2CppCodeGenWriteBarrier(&___style_46, value);
	}

	inline static int32_t get_offset_of_enableGUI_47() { return static_cast<int32_t>(offsetof(VideoTexture_FullScreen_t1562162522, ___enableGUI_47)); }
	inline bool get_enableGUI_47() const { return ___enableGUI_47; }
	inline bool* get_address_of_enableGUI_47() { return &___enableGUI_47; }
	inline void set_enableGUI_47(bool value)
	{
		___enableGUI_47 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
