﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_Collection3356274529MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::.ctor()
#define Collection_1__ctor_m1413444605(__this, method) ((  void (*) (Collection_1_t2250881793 *, const MethodInfo*))Collection_1__ctor_m1690372513_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1592256081(__this, method) ((  bool (*) (Collection_1_t2250881793 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1624016570_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Collection_1_System_Collections_ICollection_CopyTo_m2707643870(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2250881793 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1285013187_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::System.Collections.IEnumerable.GetEnumerator()
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m3770750061(__this, method) ((  Il2CppObject * (*) (Collection_1_t2250881793 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m2828471038_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::System.Collections.IList.Add(System.Object)
#define Collection_1_System_Collections_IList_Add_m3981852144(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2250881793 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1708617267_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::System.Collections.IList.Contains(System.Object)
#define Collection_1_System_Collections_IList_Contains_m2134678096(__this, ___value0, method) ((  bool (*) (Collection_1_t2250881793 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m504494585_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::System.Collections.IList.IndexOf(System.Object)
#define Collection_1_System_Collections_IList_IndexOf_m1785068616(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2250881793 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1652511499_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::System.Collections.IList.Insert(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_Insert_m3935040443(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2250881793 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m2187188150_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::System.Collections.IList.Remove(System.Object)
#define Collection_1_System_Collections_IList_Remove_m2187280781(__this, ___value0, method) ((  void (*) (Collection_1_t2250881793 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m2625864114_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::System.Collections.ICollection.get_IsSynchronized()
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m2904349452(__this, method) ((  bool (*) (Collection_1_t2250881793 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1302589123_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::System.Collections.ICollection.get_SyncRoot()
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m3471839998(__this, method) ((  Il2CppObject * (*) (Collection_1_t2250881793 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1873829551_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::System.Collections.IList.get_IsFixedSize()
#define Collection_1_System_Collections_IList_get_IsFixedSize_m3826927039(__this, method) ((  bool (*) (Collection_1_t2250881793 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2813777960_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::System.Collections.IList.get_IsReadOnly()
#define Collection_1_System_Collections_IList_get_IsReadOnly_m2101478746(__this, method) ((  bool (*) (Collection_1_t2250881793 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1376059857_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::System.Collections.IList.get_Item(System.Int32)
#define Collection_1_System_Collections_IList_get_Item_m314260741(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t2250881793 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m2224513142_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_set_Item_m2616597714(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2250881793 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m2262756877_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::Add(T)
#define Collection_1_Add_m2528294297(__this, ___item0, method) ((  void (*) (Collection_1_t2250881793 *, State_t3065423635 *, const MethodInfo*))Collection_1_Add_m321765054_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::Clear()
#define Collection_1_Clear_m3074402897(__this, method) ((  void (*) (Collection_1_t2250881793 *, const MethodInfo*))Collection_1_Clear_m3391473100_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::ClearItems()
#define Collection_1_ClearItems_m4053713233(__this, method) ((  void (*) (Collection_1_t2250881793 *, const MethodInfo*))Collection_1_ClearItems_m2738199222_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::Contains(T)
#define Collection_1_Contains_m621588675(__this, ___item0, method) ((  bool (*) (Collection_1_t2250881793 *, State_t3065423635 *, const MethodInfo*))Collection_1_Contains_m1050871674_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::CopyTo(T[],System.Int32)
#define Collection_1_CopyTo_m1028245833(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2250881793 *, StateU5BU5D_t545846082*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1746187054_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::GetEnumerator()
#define Collection_1_GetEnumerator_m2562851994(__this, method) ((  Il2CppObject* (*) (Collection_1_t2250881793 *, const MethodInfo*))Collection_1_GetEnumerator_m625631581_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::IndexOf(T)
#define Collection_1_IndexOf_m1081698005(__this, ___item0, method) ((  int32_t (*) (Collection_1_t2250881793 *, State_t3065423635 *, const MethodInfo*))Collection_1_IndexOf_m3101447730_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::Insert(System.Int32,T)
#define Collection_1_Insert_m2150342144(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2250881793 *, int32_t, State_t3065423635 *, const MethodInfo*))Collection_1_Insert_m1208073509_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::InsertItem(System.Int32,T)
#define Collection_1_InsertItem_m2263075891(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2250881793 *, int32_t, State_t3065423635 *, const MethodInfo*))Collection_1_InsertItem_m714854616_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::Remove(T)
#define Collection_1_Remove_m1546438014(__this, ___item0, method) ((  bool (*) (Collection_1_t2250881793 *, State_t3065423635 *, const MethodInfo*))Collection_1_Remove_m2181520885_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::RemoveAt(System.Int32)
#define Collection_1_RemoveAt_m24195014(__this, ___index0, method) ((  void (*) (Collection_1_t2250881793 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m3376893675_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::RemoveItem(System.Int32)
#define Collection_1_RemoveItem_m381229670(__this, ___index0, method) ((  void (*) (Collection_1_t2250881793 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1099170891_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::get_Count()
#define Collection_1_get_Count_m3285174598(__this, method) ((  int32_t (*) (Collection_1_t2250881793 *, const MethodInfo*))Collection_1_get_Count_m1472906633_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::get_Item(System.Int32)
#define Collection_1_get_Item_m743004908(__this, ___index0, method) ((  State_t3065423635 * (*) (Collection_1_t2250881793 *, int32_t, const MethodInfo*))Collection_1_get_Item_m2356360623_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::set_Item(System.Int32,T)
#define Collection_1_set_Item_m2409127639(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2250881793 *, int32_t, State_t3065423635 *, const MethodInfo*))Collection_1_set_Item_m3127068860_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::SetItem(System.Int32,T)
#define Collection_1_SetItem_m3552686786(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2250881793 *, int32_t, State_t3065423635 *, const MethodInfo*))Collection_1_SetItem_m112162877_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::IsValidItem(System.Object)
#define Collection_1_IsValidItem_m1868153609(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m1993492338_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::ConvertItem(System.Object)
#define Collection_1_ConvertItem_m876852171(__this /* static, unused */, ___item0, method) ((  State_t3065423635 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m1655469326_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::CheckWritable(System.Collections.Generic.IList`1<T>)
#define Collection_1_CheckWritable_m60193673(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m651250670_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::IsSynchronized(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsSynchronized_m4163133979(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m2469749778_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ZXing.Aztec.Internal.State>::IsFixedSize(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsFixedSize_m2837156452(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3893865421_gshared)(__this /* static, unused */, ___list0, method)
