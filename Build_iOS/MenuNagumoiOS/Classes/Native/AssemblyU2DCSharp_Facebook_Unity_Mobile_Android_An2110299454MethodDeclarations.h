﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.Mobile.Android.AndroidFacebook/JavaMethodCall`1<System.Object>
struct JavaMethodCall_1_t2110299454;
// Facebook.Unity.Mobile.Android.AndroidFacebook
struct AndroidFacebook_t2774136887;
// System.String
struct String_t;
// Facebook.Unity.MethodArguments
struct MethodArguments_t3236074899;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Mobile_Android_An2774136887.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Facebook_Unity_MethodArguments3236074899.h"

// System.Void Facebook.Unity.Mobile.Android.AndroidFacebook/JavaMethodCall`1<System.Object>::.ctor(Facebook.Unity.Mobile.Android.AndroidFacebook,System.String)
extern "C"  void JavaMethodCall_1__ctor_m3083954302_gshared (JavaMethodCall_1_t2110299454 * __this, AndroidFacebook_t2774136887 * ___androidImpl0, String_t* ___methodName1, const MethodInfo* method);
#define JavaMethodCall_1__ctor_m3083954302(__this, ___androidImpl0, ___methodName1, method) ((  void (*) (JavaMethodCall_1_t2110299454 *, AndroidFacebook_t2774136887 *, String_t*, const MethodInfo*))JavaMethodCall_1__ctor_m3083954302_gshared)(__this, ___androidImpl0, ___methodName1, method)
// System.Void Facebook.Unity.Mobile.Android.AndroidFacebook/JavaMethodCall`1<System.Object>::Call(Facebook.Unity.MethodArguments)
extern "C"  void JavaMethodCall_1_Call_m1503093513_gshared (JavaMethodCall_1_t2110299454 * __this, MethodArguments_t3236074899 * ___args0, const MethodInfo* method);
#define JavaMethodCall_1_Call_m1503093513(__this, ___args0, method) ((  void (*) (JavaMethodCall_1_t2110299454 *, MethodArguments_t3236074899 *, const MethodInfo*))JavaMethodCall_1_Call_m1503093513_gshared)(__this, ___args0, method)
