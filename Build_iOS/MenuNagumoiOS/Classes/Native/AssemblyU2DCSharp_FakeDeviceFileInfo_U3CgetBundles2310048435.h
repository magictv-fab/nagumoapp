﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FakeDeviceFileInfo/<getBundles>c__AnonStorey9F
struct  U3CgetBundlesU3Ec__AnonStorey9F_t2310048435  : public Il2CppObject
{
public:
	// System.String FakeDeviceFileInfo/<getBundles>c__AnonStorey9F::appContext
	String_t* ___appContext_0;

public:
	inline static int32_t get_offset_of_appContext_0() { return static_cast<int32_t>(offsetof(U3CgetBundlesU3Ec__AnonStorey9F_t2310048435, ___appContext_0)); }
	inline String_t* get_appContext_0() const { return ___appContext_0; }
	inline String_t** get_address_of_appContext_0() { return &___appContext_0; }
	inline void set_appContext_0(String_t* value)
	{
		___appContext_0 = value;
		Il2CppCodeGenWriteBarrier(&___appContext_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
