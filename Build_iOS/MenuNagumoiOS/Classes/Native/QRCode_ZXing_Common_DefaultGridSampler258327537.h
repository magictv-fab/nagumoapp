﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "QRCode_ZXing_Common_GridSampler228036704.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.DefaultGridSampler
struct  DefaultGridSampler_t258327537  : public GridSampler_t228036704
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
