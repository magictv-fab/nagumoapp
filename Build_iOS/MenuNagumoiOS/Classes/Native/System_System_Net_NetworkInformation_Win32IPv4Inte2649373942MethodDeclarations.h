﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.Win32IPv4InterfaceStatistics
struct Win32IPv4InterfaceStatistics_t2649373942;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_NetworkInformation_Win32_MIB_IFR1777568538.h"

// System.Void System.Net.NetworkInformation.Win32IPv4InterfaceStatistics::.ctor(System.Net.NetworkInformation.Win32_MIB_IFROW)
extern "C"  void Win32IPv4InterfaceStatistics__ctor_m2721378994 (Win32IPv4InterfaceStatistics_t2649373942 * __this, Win32_MIB_IFROW_t1777568538  ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
