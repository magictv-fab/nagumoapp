﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// InApp.SimpleEvents.FixedCameraBehavior
struct FixedCameraBehavior_t4278156937;
// ARM.transform.filters.FixedPositionAngleFilter
struct FixedPositionAngleFilter_t556916690;
// MagicTV.processes.PresentationController
struct PresentationController_t206097072;
// ARM.transform.filters.SmoothPlugin
struct SmoothPlugin_t1435567013;

#include "AssemblyU2DCSharp_ConfigurableListenerEventAbstract672359087.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InApp.SimpleEvents.FixedCameraBehavior
struct  FixedCameraBehavior_t4278156937  : public ConfigurableListenerEventAbstract_t672359087
{
public:
	// ARM.transform.filters.FixedPositionAngleFilter InApp.SimpleEvents.FixedCameraBehavior::fixedFilter
	FixedPositionAngleFilter_t556916690 * ___fixedFilter_3;
	// MagicTV.processes.PresentationController InApp.SimpleEvents.FixedCameraBehavior::_presentationController
	PresentationController_t206097072 * ____presentationController_4;
	// UnityEngine.Vector3 InApp.SimpleEvents.FixedCameraBehavior::cameraFixedPosition
	Vector3_t4282066566  ___cameraFixedPosition_5;
	// UnityEngine.Vector3 InApp.SimpleEvents.FixedCameraBehavior::cameraFixedRotation
	Vector3_t4282066566  ___cameraFixedRotation_6;
	// System.Boolean InApp.SimpleEvents.FixedCameraBehavior::_isCameraFixed
	bool ____isCameraFixed_7;
	// UnityEngine.Vector3 InApp.SimpleEvents.FixedCameraBehavior::_inicialCameraFixedPosition
	Vector3_t4282066566  ____inicialCameraFixedPosition_8;
	// UnityEngine.Vector3 InApp.SimpleEvents.FixedCameraBehavior::_inicialCameraFixedRotation
	Vector3_t4282066566  ____inicialCameraFixedRotation_9;
	// System.Boolean InApp.SimpleEvents.FixedCameraBehavior::_smoothModeFirstTime
	bool ____smoothModeFirstTime_10;
	// System.Boolean InApp.SimpleEvents.FixedCameraBehavior::_alwaysLock
	bool ____alwaysLock_11;
	// ARM.transform.filters.SmoothPlugin InApp.SimpleEvents.FixedCameraBehavior::pluginSmoothOfSwing
	SmoothPlugin_t1435567013 * ___pluginSmoothOfSwing_12;
	// System.Boolean InApp.SimpleEvents.FixedCameraBehavior::_smoothMode
	bool ____smoothMode_13;

public:
	inline static int32_t get_offset_of_fixedFilter_3() { return static_cast<int32_t>(offsetof(FixedCameraBehavior_t4278156937, ___fixedFilter_3)); }
	inline FixedPositionAngleFilter_t556916690 * get_fixedFilter_3() const { return ___fixedFilter_3; }
	inline FixedPositionAngleFilter_t556916690 ** get_address_of_fixedFilter_3() { return &___fixedFilter_3; }
	inline void set_fixedFilter_3(FixedPositionAngleFilter_t556916690 * value)
	{
		___fixedFilter_3 = value;
		Il2CppCodeGenWriteBarrier(&___fixedFilter_3, value);
	}

	inline static int32_t get_offset_of__presentationController_4() { return static_cast<int32_t>(offsetof(FixedCameraBehavior_t4278156937, ____presentationController_4)); }
	inline PresentationController_t206097072 * get__presentationController_4() const { return ____presentationController_4; }
	inline PresentationController_t206097072 ** get_address_of__presentationController_4() { return &____presentationController_4; }
	inline void set__presentationController_4(PresentationController_t206097072 * value)
	{
		____presentationController_4 = value;
		Il2CppCodeGenWriteBarrier(&____presentationController_4, value);
	}

	inline static int32_t get_offset_of_cameraFixedPosition_5() { return static_cast<int32_t>(offsetof(FixedCameraBehavior_t4278156937, ___cameraFixedPosition_5)); }
	inline Vector3_t4282066566  get_cameraFixedPosition_5() const { return ___cameraFixedPosition_5; }
	inline Vector3_t4282066566 * get_address_of_cameraFixedPosition_5() { return &___cameraFixedPosition_5; }
	inline void set_cameraFixedPosition_5(Vector3_t4282066566  value)
	{
		___cameraFixedPosition_5 = value;
	}

	inline static int32_t get_offset_of_cameraFixedRotation_6() { return static_cast<int32_t>(offsetof(FixedCameraBehavior_t4278156937, ___cameraFixedRotation_6)); }
	inline Vector3_t4282066566  get_cameraFixedRotation_6() const { return ___cameraFixedRotation_6; }
	inline Vector3_t4282066566 * get_address_of_cameraFixedRotation_6() { return &___cameraFixedRotation_6; }
	inline void set_cameraFixedRotation_6(Vector3_t4282066566  value)
	{
		___cameraFixedRotation_6 = value;
	}

	inline static int32_t get_offset_of__isCameraFixed_7() { return static_cast<int32_t>(offsetof(FixedCameraBehavior_t4278156937, ____isCameraFixed_7)); }
	inline bool get__isCameraFixed_7() const { return ____isCameraFixed_7; }
	inline bool* get_address_of__isCameraFixed_7() { return &____isCameraFixed_7; }
	inline void set__isCameraFixed_7(bool value)
	{
		____isCameraFixed_7 = value;
	}

	inline static int32_t get_offset_of__inicialCameraFixedPosition_8() { return static_cast<int32_t>(offsetof(FixedCameraBehavior_t4278156937, ____inicialCameraFixedPosition_8)); }
	inline Vector3_t4282066566  get__inicialCameraFixedPosition_8() const { return ____inicialCameraFixedPosition_8; }
	inline Vector3_t4282066566 * get_address_of__inicialCameraFixedPosition_8() { return &____inicialCameraFixedPosition_8; }
	inline void set__inicialCameraFixedPosition_8(Vector3_t4282066566  value)
	{
		____inicialCameraFixedPosition_8 = value;
	}

	inline static int32_t get_offset_of__inicialCameraFixedRotation_9() { return static_cast<int32_t>(offsetof(FixedCameraBehavior_t4278156937, ____inicialCameraFixedRotation_9)); }
	inline Vector3_t4282066566  get__inicialCameraFixedRotation_9() const { return ____inicialCameraFixedRotation_9; }
	inline Vector3_t4282066566 * get_address_of__inicialCameraFixedRotation_9() { return &____inicialCameraFixedRotation_9; }
	inline void set__inicialCameraFixedRotation_9(Vector3_t4282066566  value)
	{
		____inicialCameraFixedRotation_9 = value;
	}

	inline static int32_t get_offset_of__smoothModeFirstTime_10() { return static_cast<int32_t>(offsetof(FixedCameraBehavior_t4278156937, ____smoothModeFirstTime_10)); }
	inline bool get__smoothModeFirstTime_10() const { return ____smoothModeFirstTime_10; }
	inline bool* get_address_of__smoothModeFirstTime_10() { return &____smoothModeFirstTime_10; }
	inline void set__smoothModeFirstTime_10(bool value)
	{
		____smoothModeFirstTime_10 = value;
	}

	inline static int32_t get_offset_of__alwaysLock_11() { return static_cast<int32_t>(offsetof(FixedCameraBehavior_t4278156937, ____alwaysLock_11)); }
	inline bool get__alwaysLock_11() const { return ____alwaysLock_11; }
	inline bool* get_address_of__alwaysLock_11() { return &____alwaysLock_11; }
	inline void set__alwaysLock_11(bool value)
	{
		____alwaysLock_11 = value;
	}

	inline static int32_t get_offset_of_pluginSmoothOfSwing_12() { return static_cast<int32_t>(offsetof(FixedCameraBehavior_t4278156937, ___pluginSmoothOfSwing_12)); }
	inline SmoothPlugin_t1435567013 * get_pluginSmoothOfSwing_12() const { return ___pluginSmoothOfSwing_12; }
	inline SmoothPlugin_t1435567013 ** get_address_of_pluginSmoothOfSwing_12() { return &___pluginSmoothOfSwing_12; }
	inline void set_pluginSmoothOfSwing_12(SmoothPlugin_t1435567013 * value)
	{
		___pluginSmoothOfSwing_12 = value;
		Il2CppCodeGenWriteBarrier(&___pluginSmoothOfSwing_12, value);
	}

	inline static int32_t get_offset_of__smoothMode_13() { return static_cast<int32_t>(offsetof(FixedCameraBehavior_t4278156937, ____smoothMode_13)); }
	inline bool get__smoothMode_13() const { return ____smoothMode_13; }
	inline bool* get_address_of__smoothMode_13() { return &____smoothMode_13; }
	inline void set__smoothMode_13(bool value)
	{
		____smoothMode_13 = value;
	}
};

struct FixedCameraBehavior_t4278156937_StaticFields
{
public:
	// InApp.SimpleEvents.FixedCameraBehavior InApp.SimpleEvents.FixedCameraBehavior::_instance
	FixedCameraBehavior_t4278156937 * ____instance_2;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(FixedCameraBehavior_t4278156937_StaticFields, ____instance_2)); }
	inline FixedCameraBehavior_t4278156937 * get__instance_2() const { return ____instance_2; }
	inline FixedCameraBehavior_t4278156937 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(FixedCameraBehavior_t4278156937 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier(&____instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
