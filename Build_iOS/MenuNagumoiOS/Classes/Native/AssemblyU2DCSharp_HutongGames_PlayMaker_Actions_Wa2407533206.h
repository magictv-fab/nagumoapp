﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Rigidbody[]
struct RigidbodyU5BU5D_t1403726930;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.WakeAllRigidBodies
struct  WakeAllRigidBodies_t2407533206  : public FsmStateAction_t2366529033
{
public:
	// System.Boolean HutongGames.PlayMaker.Actions.WakeAllRigidBodies::everyFrame
	bool ___everyFrame_9;
	// UnityEngine.Rigidbody[] HutongGames.PlayMaker.Actions.WakeAllRigidBodies::bodies
	RigidbodyU5BU5D_t1403726930* ___bodies_10;

public:
	inline static int32_t get_offset_of_everyFrame_9() { return static_cast<int32_t>(offsetof(WakeAllRigidBodies_t2407533206, ___everyFrame_9)); }
	inline bool get_everyFrame_9() const { return ___everyFrame_9; }
	inline bool* get_address_of_everyFrame_9() { return &___everyFrame_9; }
	inline void set_everyFrame_9(bool value)
	{
		___everyFrame_9 = value;
	}

	inline static int32_t get_offset_of_bodies_10() { return static_cast<int32_t>(offsetof(WakeAllRigidBodies_t2407533206, ___bodies_10)); }
	inline RigidbodyU5BU5D_t1403726930* get_bodies_10() const { return ___bodies_10; }
	inline RigidbodyU5BU5D_t1403726930** get_address_of_bodies_10() { return &___bodies_10; }
	inline void set_bodies_10(RigidbodyU5BU5D_t1403726930* value)
	{
		___bodies_10 = value;
		Il2CppCodeGenWriteBarrier(&___bodies_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
