﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.transform.filters.SmoothPlugin
struct SmoothPlugin_t1435567013;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_String7231557.h"

// System.Void ARM.transform.filters.SmoothPlugin::.ctor()
extern "C"  void SmoothPlugin__ctor_m1156092083 (SmoothPlugin_t1435567013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.SmoothPlugin::Start()
extern "C"  void SmoothPlugin_Start_m103229875 (SmoothPlugin_t1435567013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.SmoothPlugin::_shakeInit()
extern "C"  void SmoothPlugin__shakeInit_m2953830408 (SmoothPlugin_t1435567013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.SmoothPlugin::_shakeEnd()
extern "C"  void SmoothPlugin__shakeEnd_m2723982917 (SmoothPlugin_t1435567013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.SmoothPlugin::_On()
extern "C"  void SmoothPlugin__On_m4282718479 (SmoothPlugin_t1435567013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.SmoothPlugin::Update()
extern "C"  void SmoothPlugin_Update_m3205978298 (SmoothPlugin_t1435567013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.SmoothPlugin::turnOffQuickMovement()
extern "C"  void SmoothPlugin_turnOffQuickMovement_m2167607995 (SmoothPlugin_t1435567013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion ARM.transform.filters.SmoothPlugin::_doFilter(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  SmoothPlugin__doFilter_m930211547 (SmoothPlugin_t1435567013 * __this, Quaternion_t1553702882  ___last0, Quaternion_t1553702882  ___newPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.transform.filters.SmoothPlugin::_doFilter(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  SmoothPlugin__doFilter_m2305100499 (SmoothPlugin_t1435567013 * __this, Vector3_t4282066566  ___last0, Vector3_t4282066566  ___newPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.SmoothPlugin::configByString(System.String)
extern "C"  void SmoothPlugin_configByString_m1205001895 (SmoothPlugin_t1435567013 * __this, String_t* ___metadata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.SmoothPlugin::Reset()
extern "C"  void SmoothPlugin_Reset_m3097492320 (SmoothPlugin_t1435567013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
