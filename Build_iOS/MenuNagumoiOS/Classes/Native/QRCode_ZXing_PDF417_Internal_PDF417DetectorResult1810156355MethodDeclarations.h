﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.PDF417.Internal.PDF417DetectorResult
struct PDF417DetectorResult_t1810156355;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// System.Collections.Generic.List`1<ZXing.ResultPoint[]>
struct List_1_t2563349896;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitMatrix1058711404.h"

// ZXing.Common.BitMatrix ZXing.PDF417.Internal.PDF417DetectorResult::get_Bits()
extern "C"  BitMatrix_t1058711404 * PDF417DetectorResult_get_Bits_m2557612640 (PDF417DetectorResult_t1810156355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.PDF417DetectorResult::set_Bits(ZXing.Common.BitMatrix)
extern "C"  void PDF417DetectorResult_set_Bits_m1061225559 (PDF417DetectorResult_t1810156355 * __this, BitMatrix_t1058711404 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<ZXing.ResultPoint[]> ZXing.PDF417.Internal.PDF417DetectorResult::get_Points()
extern "C"  List_1_t2563349896 * PDF417DetectorResult_get_Points_m4131233921 (PDF417DetectorResult_t1810156355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.PDF417DetectorResult::set_Points(System.Collections.Generic.List`1<ZXing.ResultPoint[]>)
extern "C"  void PDF417DetectorResult_set_Points_m351062968 (PDF417DetectorResult_t1810156355 * __this, List_1_t2563349896 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.PDF417DetectorResult::.ctor(ZXing.Common.BitMatrix,System.Collections.Generic.List`1<ZXing.ResultPoint[]>)
extern "C"  void PDF417DetectorResult__ctor_m3441505551 (PDF417DetectorResult_t1810156355 * __this, BitMatrix_t1058711404 * ___bits0, List_1_t2563349896 * ___points1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
