﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.Win32IPInterfaceProperties2
struct Win32IPInterfaceProperties2_t3153501730;
// System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES
struct Win32_IP_ADAPTER_ADDRESSES_t3597816152;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_NetworkInformation_Win32_IP_ADAP3597816152.h"
#include "System_System_Net_NetworkInformation_Win32_MIB_IFR1777568538.h"

// System.Void System.Net.NetworkInformation.Win32IPInterfaceProperties2::.ctor(System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES,System.Net.NetworkInformation.Win32_MIB_IFROW,System.Net.NetworkInformation.Win32_MIB_IFROW)
extern "C"  void Win32IPInterfaceProperties2__ctor_m2845566044 (Win32IPInterfaceProperties2_t3153501730 * __this, Win32_IP_ADAPTER_ADDRESSES_t3597816152 * ___addr0, Win32_MIB_IFROW_t1777568538  ___mib41, Win32_MIB_IFROW_t1777568538  ___mib62, const MethodInfo* method) IL2CPP_METHOD_ATTR;
