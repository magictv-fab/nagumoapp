﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.PDF417.Internal.EC.ModulusGF
struct ModulusGF_t2738856732;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.EC.ErrorCorrection
struct  ErrorCorrection_t2629846604  : public Il2CppObject
{
public:
	// ZXing.PDF417.Internal.EC.ModulusGF ZXing.PDF417.Internal.EC.ErrorCorrection::field
	ModulusGF_t2738856732 * ___field_0;

public:
	inline static int32_t get_offset_of_field_0() { return static_cast<int32_t>(offsetof(ErrorCorrection_t2629846604, ___field_0)); }
	inline ModulusGF_t2738856732 * get_field_0() const { return ___field_0; }
	inline ModulusGF_t2738856732 ** get_address_of_field_0() { return &___field_0; }
	inline void set_field_0(ModulusGF_t2738856732 * value)
	{
		___field_0 = value;
		Il2CppCodeGenWriteBarrier(&___field_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
