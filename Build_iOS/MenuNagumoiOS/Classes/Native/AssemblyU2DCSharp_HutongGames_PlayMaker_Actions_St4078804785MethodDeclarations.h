﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.StringReplace
struct StringReplace_t4078804785;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.StringReplace::.ctor()
extern "C"  void StringReplace__ctor_m4008917285 (StringReplace_t4078804785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringReplace::Reset()
extern "C"  void StringReplace_Reset_m1655350226 (StringReplace_t4078804785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringReplace::OnEnter()
extern "C"  void StringReplace_OnEnter_m3670542268 (StringReplace_t4078804785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringReplace::OnUpdate()
extern "C"  void StringReplace_OnUpdate_m1251219975 (StringReplace_t4078804785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringReplace::DoReplace()
extern "C"  void StringReplace_DoReplace_m4017029804 (StringReplace_t4078804785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
