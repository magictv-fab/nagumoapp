﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.QrCode.Internal.Detector
struct Detector_t967800930;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// ZXing.Common.DetectorResult
struct DetectorResult_t3846928147;
// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>
struct IDictionary_2_t759989846;
// ZXing.QrCode.Internal.FinderPatternInfo
struct FinderPatternInfo_t3964498398;
// ZXing.Common.PerspectiveTransform
struct PerspectiveTransform_t2438931808;
// ZXing.ResultPoint
struct ResultPoint_t1538592853;
// ZXing.QrCode.Internal.AlignmentPattern
struct AlignmentPattern_t1786970569;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitMatrix1058711404.h"
#include "QRCode_ZXing_QrCode_Internal_FinderPatternInfo3964498398.h"
#include "QRCode_ZXing_ResultPoint1538592853.h"
#include "QRCode_ZXing_Common_PerspectiveTransform2438931808.h"

// System.Void ZXing.QrCode.Internal.Detector::.ctor(ZXing.Common.BitMatrix)
extern "C"  void Detector__ctor_m2757652997 (Detector_t967800930 * __this, BitMatrix_t1058711404 * ___image0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.DetectorResult ZXing.QrCode.Internal.Detector::detect(System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  DetectorResult_t3846928147 * Detector_detect_m3717378347 (Detector_t967800930 * __this, Il2CppObject* ___hints0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.DetectorResult ZXing.QrCode.Internal.Detector::processFinderPatternInfo(ZXing.QrCode.Internal.FinderPatternInfo)
extern "C"  DetectorResult_t3846928147 * Detector_processFinderPatternInfo_m4119108331 (Detector_t967800930 * __this, FinderPatternInfo_t3964498398 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.PerspectiveTransform ZXing.QrCode.Internal.Detector::createTransform(ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,System.Int32)
extern "C"  PerspectiveTransform_t2438931808 * Detector_createTransform_m225365987 (Il2CppObject * __this /* static, unused */, ResultPoint_t1538592853 * ___topLeft0, ResultPoint_t1538592853 * ___topRight1, ResultPoint_t1538592853 * ___bottomLeft2, ResultPoint_t1538592853 * ___alignmentPattern3, int32_t ___dimension4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.BitMatrix ZXing.QrCode.Internal.Detector::sampleGrid(ZXing.Common.BitMatrix,ZXing.Common.PerspectiveTransform,System.Int32)
extern "C"  BitMatrix_t1058711404 * Detector_sampleGrid_m2292730759 (Il2CppObject * __this /* static, unused */, BitMatrix_t1058711404 * ___image0, PerspectiveTransform_t2438931808 * ___transform1, int32_t ___dimension2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.QrCode.Internal.Detector::computeDimension(ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,System.Single,System.Int32&)
extern "C"  bool Detector_computeDimension_m487399868 (Il2CppObject * __this /* static, unused */, ResultPoint_t1538592853 * ___topLeft0, ResultPoint_t1538592853 * ___topRight1, ResultPoint_t1538592853 * ___bottomLeft2, float ___moduleSize3, int32_t* ___dimension4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ZXing.QrCode.Internal.Detector::calculateModuleSize(ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint)
extern "C"  float Detector_calculateModuleSize_m3348586320 (Detector_t967800930 * __this, ResultPoint_t1538592853 * ___topLeft0, ResultPoint_t1538592853 * ___topRight1, ResultPoint_t1538592853 * ___bottomLeft2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ZXing.QrCode.Internal.Detector::calculateModuleSizeOneWay(ZXing.ResultPoint,ZXing.ResultPoint)
extern "C"  float Detector_calculateModuleSizeOneWay_m2967916452 (Detector_t967800930 * __this, ResultPoint_t1538592853 * ___pattern0, ResultPoint_t1538592853 * ___otherPattern1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ZXing.QrCode.Internal.Detector::sizeOfBlackWhiteBlackRunBothWays(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  float Detector_sizeOfBlackWhiteBlackRunBothWays_m3563390141 (Detector_t967800930 * __this, int32_t ___fromX0, int32_t ___fromY1, int32_t ___toX2, int32_t ___toY3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ZXing.QrCode.Internal.Detector::sizeOfBlackWhiteBlackRun(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  float Detector_sizeOfBlackWhiteBlackRun_m813945666 (Detector_t967800930 * __this, int32_t ___fromX0, int32_t ___fromY1, int32_t ___toX2, int32_t ___toY3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.QrCode.Internal.AlignmentPattern ZXing.QrCode.Internal.Detector::findAlignmentInRegion(System.Single,System.Int32,System.Int32,System.Single)
extern "C"  AlignmentPattern_t1786970569 * Detector_findAlignmentInRegion_m3889610122 (Detector_t967800930 * __this, float ___overallEstModuleSize0, int32_t ___estAlignmentX1, int32_t ___estAlignmentY2, float ___allowanceFactor3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
