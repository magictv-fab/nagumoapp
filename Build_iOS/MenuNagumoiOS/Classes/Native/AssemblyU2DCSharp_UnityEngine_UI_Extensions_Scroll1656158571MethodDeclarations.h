﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Extensions.ScrollRectEx/<OnBeginDrag>c__AnonStorey8D
struct U3COnBeginDragU3Ec__AnonStorey8D_t1656158571;
// UnityEngine.EventSystems.IBeginDragHandler
struct IBeginDragHandler_t1569653796;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.Extensions.ScrollRectEx/<OnBeginDrag>c__AnonStorey8D::.ctor()
extern "C"  void U3COnBeginDragU3Ec__AnonStorey8D__ctor_m3852014176 (U3COnBeginDragU3Ec__AnonStorey8D_t1656158571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollRectEx/<OnBeginDrag>c__AnonStorey8D::<>m__2(UnityEngine.EventSystems.IBeginDragHandler)
extern "C"  void U3COnBeginDragU3Ec__AnonStorey8D_U3CU3Em__2_m3297506946 (U3COnBeginDragU3Ec__AnonStorey8D_t1656158571 * __this, Il2CppObject * ___parent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
