﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertasManagerCRM/<LoadLoadeds>c__Iterator7
struct U3CLoadLoadedsU3Ec__Iterator7_t2167093852;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OfertasManagerCRM/<LoadLoadeds>c__Iterator7::.ctor()
extern "C"  void U3CLoadLoadedsU3Ec__Iterator7__ctor_m1537247375 (U3CLoadLoadedsU3Ec__Iterator7_t2167093852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManagerCRM/<LoadLoadeds>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadLoadedsU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2767936931 (U3CLoadLoadedsU3Ec__Iterator7_t2167093852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManagerCRM/<LoadLoadeds>c__Iterator7::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadLoadedsU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m980092727 (U3CLoadLoadedsU3Ec__Iterator7_t2167093852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OfertasManagerCRM/<LoadLoadeds>c__Iterator7::MoveNext()
extern "C"  bool U3CLoadLoadedsU3Ec__Iterator7_MoveNext_m2278606085 (U3CLoadLoadedsU3Ec__Iterator7_t2167093852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM/<LoadLoadeds>c__Iterator7::Dispose()
extern "C"  void U3CLoadLoadedsU3Ec__Iterator7_Dispose_m4019227916 (U3CLoadLoadedsU3Ec__Iterator7_t2167093852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM/<LoadLoadeds>c__Iterator7::Reset()
extern "C"  void U3CLoadLoadedsU3Ec__Iterator7_Reset_m3478647612 (U3CLoadLoadedsU3Ec__Iterator7_t2167093852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
