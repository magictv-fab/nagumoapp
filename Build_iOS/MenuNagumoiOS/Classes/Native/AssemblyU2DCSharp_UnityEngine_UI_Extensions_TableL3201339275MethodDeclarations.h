﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Extensions.TableLayoutGroup
struct TableLayoutGroup_t3201339275;
// System.Single[]
struct SingleU5BU5D_t2316563989;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_Extensions_TableL4192955334.h"

// System.Void UnityEngine.UI.Extensions.TableLayoutGroup::.ctor()
extern "C"  void TableLayoutGroup__ctor_m728487037 (TableLayoutGroup_t3201339275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Extensions.TableLayoutGroup/Corner UnityEngine.UI.Extensions.TableLayoutGroup::get_StartCorner()
extern "C"  int32_t TableLayoutGroup_get_StartCorner_m2793800284 (TableLayoutGroup_t3201339275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.TableLayoutGroup::set_StartCorner(UnityEngine.UI.Extensions.TableLayoutGroup/Corner)
extern "C"  void TableLayoutGroup_set_StartCorner_m4285185847 (TableLayoutGroup_t3201339275 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] UnityEngine.UI.Extensions.TableLayoutGroup::get_ColumnWidths()
extern "C"  SingleU5BU5D_t2316563989* TableLayoutGroup_get_ColumnWidths_m23332093 (TableLayoutGroup_t3201339275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.TableLayoutGroup::set_ColumnWidths(System.Single[])
extern "C"  void TableLayoutGroup_set_ColumnWidths_m1779126242 (TableLayoutGroup_t3201339275 * __this, SingleU5BU5D_t2316563989* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.Extensions.TableLayoutGroup::get_MinimumRowHeight()
extern "C"  float TableLayoutGroup_get_MinimumRowHeight_m2914490607 (TableLayoutGroup_t3201339275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.TableLayoutGroup::set_MinimumRowHeight(System.Single)
extern "C"  void TableLayoutGroup_set_MinimumRowHeight_m2308444276 (TableLayoutGroup_t3201339275 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.Extensions.TableLayoutGroup::get_FlexibleRowHeight()
extern "C"  bool TableLayoutGroup_get_FlexibleRowHeight_m1087991444 (TableLayoutGroup_t3201339275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.TableLayoutGroup::set_FlexibleRowHeight(System.Boolean)
extern "C"  void TableLayoutGroup_set_FlexibleRowHeight_m2148043595 (TableLayoutGroup_t3201339275 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.Extensions.TableLayoutGroup::get_ColumnSpacing()
extern "C"  float TableLayoutGroup_get_ColumnSpacing_m4206989875 (TableLayoutGroup_t3201339275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.TableLayoutGroup::set_ColumnSpacing(System.Single)
extern "C"  void TableLayoutGroup_set_ColumnSpacing_m1375137280 (TableLayoutGroup_t3201339275 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.Extensions.TableLayoutGroup::get_RowSpacing()
extern "C"  float TableLayoutGroup_get_RowSpacing_m4220854661 (TableLayoutGroup_t3201339275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.TableLayoutGroup::set_RowSpacing(System.Single)
extern "C"  void TableLayoutGroup_set_RowSpacing_m2559905182 (TableLayoutGroup_t3201339275 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.TableLayoutGroup::CalculateLayoutInputHorizontal()
extern "C"  void TableLayoutGroup_CalculateLayoutInputHorizontal_m703254885 (TableLayoutGroup_t3201339275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.TableLayoutGroup::CalculateLayoutInputVertical()
extern "C"  void TableLayoutGroup_CalculateLayoutInputVertical_m2277863479 (TableLayoutGroup_t3201339275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.TableLayoutGroup::SetLayoutHorizontal()
extern "C"  void TableLayoutGroup_SetLayoutHorizontal_m312605579 (TableLayoutGroup_t3201339275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.TableLayoutGroup::SetLayoutVertical()
extern "C"  void TableLayoutGroup_SetLayoutVertical_m4234996701 (TableLayoutGroup_t3201339275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
