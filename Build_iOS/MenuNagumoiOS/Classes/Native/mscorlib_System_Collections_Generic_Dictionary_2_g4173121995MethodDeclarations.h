﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2045888271MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::.ctor()
#define Dictionary_2__ctor_m1249024601(__this, method) ((  void (*) (Dictionary_2_t4173121995 *, const MethodInfo*))Dictionary_2__ctor_m3794638399_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m2461292185(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t4173121995 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m273898294_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::.ctor(System.Int32)
#define Dictionary_2__ctor_m2694528883(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t4173121995 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m2504582416_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m110016355(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t4173121995 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m4162067200_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1040457398(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t4173121995 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1029455407_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m1682940384(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t4173121995 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m1780508229_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::System.Collections.IDictionary.get_Values()
#define Dictionary_2_System_Collections_IDictionary_get_Values_m470593230(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t4173121995 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m4038979059_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::System.Collections.IDictionary.get_IsFixedSize()
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2239122619(__this, method) ((  bool (*) (Dictionary_2_t4173121995 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m824729858_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::System.Collections.IDictionary.get_IsReadOnly()
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3574279902(__this, method) ((  bool (*) (Dictionary_2_t4173121995 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1311897015_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m3776682716(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t4173121995 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m1551250025_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m3799490379(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t4173121995 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1049066318_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m1467284518(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t4173121995 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m781609539_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m597587020(__this, ___key0, method) ((  bool (*) (Dictionary_2_t4173121995 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1783363411_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m2033192841(__this, ___key0, method) ((  void (*) (Dictionary_2_t4173121995 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m2215006604_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m76840584(__this, method) ((  bool (*) (Dictionary_2_t4173121995 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1826238689_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1453983034(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t4173121995 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2075478797_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2245246156(__this, method) ((  bool (*) (Dictionary_2_t4173121995 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1433083365_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2780013471(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t4173121995 *, KeyValuePair_2_t4071902701 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3257059362_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3861738823(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t4173121995 *, KeyValuePair_2_t4071902701 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2744049760_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3307243971(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t4173121995 *, KeyValuePair_2U5BU5D_t3204490176*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2008986502_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2874556972(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t4173121995 *, KeyValuePair_2_t4071902701 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2350489477_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m3331891810(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t4173121995 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m3550803941_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1473700465(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t4173121995 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m4115711264_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m980799016(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t4173121995 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m52259357_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m802532277(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t4173121995 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1395730616_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::get_Count()
#define Dictionary_2_get_Count_m83998018(__this, method) ((  int32_t (*) (Dictionary_2_t4173121995 *, const MethodInfo*))Dictionary_2_get_Count_m1232250407_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::get_Item(TKey)
#define Dictionary_2_get_Item_m788068843(__this, ___key0, method) ((  List_1_t3352703625 * (*) (Dictionary_2_t4173121995 *, String_t*, const MethodInfo*))Dictionary_2_get_Item_m2285357284_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m482852706(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t4173121995 *, String_t*, List_1_t3352703625 *, const MethodInfo*))Dictionary_2_set_Item_m2627891647_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m1463612570(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t4173121995 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m2966484407_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m4082603613(__this, ___size0, method) ((  void (*) (Dictionary_2_t4173121995 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2119297760_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m3482422169(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t4173121995 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2536521436_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m3789091117(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t4071902701  (*) (Il2CppObject * /* static, unused */, String_t*, List_1_t3352703625 *, const MethodInfo*))Dictionary_2_make_pair_m2083407400_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m317619953(__this /* static, unused */, ___key0, ___value1, method) ((  String_t* (*) (Il2CppObject * /* static, unused */, String_t*, List_1_t3352703625 *, const MethodInfo*))Dictionary_2_pick_key_m3909093582_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m706907789(__this /* static, unused */, ___key0, ___value1, method) ((  List_1_t3352703625 * (*) (Il2CppObject * /* static, unused */, String_t*, List_1_t3352703625 *, const MethodInfo*))Dictionary_2_pick_value_m3477594126_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m198630934(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t4173121995 *, KeyValuePair_2U5BU5D_t3204490176*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m3401241971_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::Resize()
#define Dictionary_2_Resize_m3962920790(__this, method) ((  void (*) (Dictionary_2_t4173121995 *, const MethodInfo*))Dictionary_2_Resize_m1727470041_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::Add(TKey,TValue)
#define Dictionary_2_Add_m2424062765(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t4173121995 *, String_t*, List_1_t3352703625 *, const MethodInfo*))Dictionary_2_Add_m3537188182_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::Clear()
#define Dictionary_2_Clear_m2950125188(__this, method) ((  void (*) (Dictionary_2_t4173121995 *, const MethodInfo*))Dictionary_2_Clear_m1200771690_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m2516371612(__this, ___key0, method) ((  bool (*) (Dictionary_2_t4173121995 *, String_t*, const MethodInfo*))Dictionary_2_ContainsKey_m3006991056_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m2572374711(__this, ___value0, method) ((  bool (*) (Dictionary_2_t4173121995 *, List_1_t3352703625 *, const MethodInfo*))Dictionary_2_ContainsValue_m712275664_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m1520430016(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t4173121995 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m1544184413_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m490430116(__this, ___sender0, method) ((  void (*) (Dictionary_2_t4173121995 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m1638301735_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::Remove(TKey)
#define Dictionary_2_Remove_m2848424025(__this, ___key0, method) ((  bool (*) (Dictionary_2_t4173121995 *, String_t*, const MethodInfo*))Dictionary_2_Remove_m2155719712_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m1718100880(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t4173121995 *, String_t*, List_1_t3352703625 **, const MethodInfo*))Dictionary_2_TryGetValue_m2075628329_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::get_Keys()
#define Dictionary_2_get_Keys_m2790715531(__this, method) ((  KeyCollection_t1504914150 * (*) (Dictionary_2_t4173121995 *, const MethodInfo*))Dictionary_2_get_Keys_m2624609910_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::get_Values()
#define Dictionary_2_get_Values_m3537921895(__this, method) ((  ValueCollection_t2873727708 * (*) (Dictionary_2_t4173121995 *, const MethodInfo*))Dictionary_2_get_Values_m2070602102_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m4062446156(__this, ___key0, method) ((  String_t* (*) (Dictionary_2_t4173121995 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m3358952489_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m3314189224(__this, ___value0, method) ((  List_1_t3352703625 * (*) (Dictionary_2_t4173121995 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1789908265_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m4151928380(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t4173121995 *, KeyValuePair_2_t4071902701 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m3073235459_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m3509148781(__this, method) ((  Enumerator_t1195478091  (*) (Dictionary_2_t4173121995 *, const MethodInfo*))Dictionary_2_GetEnumerator_m65675076_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>::<CopyTo>m__2(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__2_m547574754(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, String_t*, List_1_t3352703625 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__2_m2807612881_gshared)(__this /* static, unused */, ___key0, ___value1, method)
