﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Facebook.Unity.HideUnityDelegate
struct HideUnityDelegate_t3175190102;
// Facebook.Unity.InitDelegate
struct InitDelegate_t5726901;
// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FB/<Init>c__AnonStorey90
struct  U3CInitU3Ec__AnonStorey90_t3153034682  : public Il2CppObject
{
public:
	// Facebook.Unity.HideUnityDelegate Facebook.Unity.FB/<Init>c__AnonStorey90::onHideUnity
	HideUnityDelegate_t3175190102 * ___onHideUnity_0;
	// Facebook.Unity.InitDelegate Facebook.Unity.FB/<Init>c__AnonStorey90::onInitComplete
	InitDelegate_t5726901 * ___onInitComplete_1;
	// System.String Facebook.Unity.FB/<Init>c__AnonStorey90::appId
	String_t* ___appId_2;
	// System.Boolean Facebook.Unity.FB/<Init>c__AnonStorey90::cookie
	bool ___cookie_3;
	// System.Boolean Facebook.Unity.FB/<Init>c__AnonStorey90::logging
	bool ___logging_4;
	// System.Boolean Facebook.Unity.FB/<Init>c__AnonStorey90::status
	bool ___status_5;
	// System.Boolean Facebook.Unity.FB/<Init>c__AnonStorey90::xfbml
	bool ___xfbml_6;
	// System.String Facebook.Unity.FB/<Init>c__AnonStorey90::authResponse
	String_t* ___authResponse_7;
	// System.Boolean Facebook.Unity.FB/<Init>c__AnonStorey90::frictionlessRequests
	bool ___frictionlessRequests_8;
	// System.String Facebook.Unity.FB/<Init>c__AnonStorey90::jsSDKLocale
	String_t* ___jsSDKLocale_9;

public:
	inline static int32_t get_offset_of_onHideUnity_0() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__AnonStorey90_t3153034682, ___onHideUnity_0)); }
	inline HideUnityDelegate_t3175190102 * get_onHideUnity_0() const { return ___onHideUnity_0; }
	inline HideUnityDelegate_t3175190102 ** get_address_of_onHideUnity_0() { return &___onHideUnity_0; }
	inline void set_onHideUnity_0(HideUnityDelegate_t3175190102 * value)
	{
		___onHideUnity_0 = value;
		Il2CppCodeGenWriteBarrier(&___onHideUnity_0, value);
	}

	inline static int32_t get_offset_of_onInitComplete_1() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__AnonStorey90_t3153034682, ___onInitComplete_1)); }
	inline InitDelegate_t5726901 * get_onInitComplete_1() const { return ___onInitComplete_1; }
	inline InitDelegate_t5726901 ** get_address_of_onInitComplete_1() { return &___onInitComplete_1; }
	inline void set_onInitComplete_1(InitDelegate_t5726901 * value)
	{
		___onInitComplete_1 = value;
		Il2CppCodeGenWriteBarrier(&___onInitComplete_1, value);
	}

	inline static int32_t get_offset_of_appId_2() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__AnonStorey90_t3153034682, ___appId_2)); }
	inline String_t* get_appId_2() const { return ___appId_2; }
	inline String_t** get_address_of_appId_2() { return &___appId_2; }
	inline void set_appId_2(String_t* value)
	{
		___appId_2 = value;
		Il2CppCodeGenWriteBarrier(&___appId_2, value);
	}

	inline static int32_t get_offset_of_cookie_3() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__AnonStorey90_t3153034682, ___cookie_3)); }
	inline bool get_cookie_3() const { return ___cookie_3; }
	inline bool* get_address_of_cookie_3() { return &___cookie_3; }
	inline void set_cookie_3(bool value)
	{
		___cookie_3 = value;
	}

	inline static int32_t get_offset_of_logging_4() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__AnonStorey90_t3153034682, ___logging_4)); }
	inline bool get_logging_4() const { return ___logging_4; }
	inline bool* get_address_of_logging_4() { return &___logging_4; }
	inline void set_logging_4(bool value)
	{
		___logging_4 = value;
	}

	inline static int32_t get_offset_of_status_5() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__AnonStorey90_t3153034682, ___status_5)); }
	inline bool get_status_5() const { return ___status_5; }
	inline bool* get_address_of_status_5() { return &___status_5; }
	inline void set_status_5(bool value)
	{
		___status_5 = value;
	}

	inline static int32_t get_offset_of_xfbml_6() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__AnonStorey90_t3153034682, ___xfbml_6)); }
	inline bool get_xfbml_6() const { return ___xfbml_6; }
	inline bool* get_address_of_xfbml_6() { return &___xfbml_6; }
	inline void set_xfbml_6(bool value)
	{
		___xfbml_6 = value;
	}

	inline static int32_t get_offset_of_authResponse_7() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__AnonStorey90_t3153034682, ___authResponse_7)); }
	inline String_t* get_authResponse_7() const { return ___authResponse_7; }
	inline String_t** get_address_of_authResponse_7() { return &___authResponse_7; }
	inline void set_authResponse_7(String_t* value)
	{
		___authResponse_7 = value;
		Il2CppCodeGenWriteBarrier(&___authResponse_7, value);
	}

	inline static int32_t get_offset_of_frictionlessRequests_8() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__AnonStorey90_t3153034682, ___frictionlessRequests_8)); }
	inline bool get_frictionlessRequests_8() const { return ___frictionlessRequests_8; }
	inline bool* get_address_of_frictionlessRequests_8() { return &___frictionlessRequests_8; }
	inline void set_frictionlessRequests_8(bool value)
	{
		___frictionlessRequests_8 = value;
	}

	inline static int32_t get_offset_of_jsSDKLocale_9() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__AnonStorey90_t3153034682, ___jsSDKLocale_9)); }
	inline String_t* get_jsSDKLocale_9() const { return ___jsSDKLocale_9; }
	inline String_t** get_address_of_jsSDKLocale_9() { return &___jsSDKLocale_9; }
	inline void set_jsSDKLocale_9(String_t* value)
	{
		___jsSDKLocale_9 = value;
		Il2CppCodeGenWriteBarrier(&___jsSDKLocale_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
