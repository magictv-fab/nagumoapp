﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OneSignal/OnLogoutEmailSuccess
struct OnLogoutEmailSuccess_t3366364849;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void OneSignal/OnLogoutEmailSuccess::.ctor(System.Object,System.IntPtr)
extern "C"  void OnLogoutEmailSuccess__ctor_m638146776 (OnLogoutEmailSuccess_t3366364849 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal/OnLogoutEmailSuccess::Invoke()
extern "C"  void OnLogoutEmailSuccess_Invoke_m2912117554 (OnLogoutEmailSuccess_t3366364849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult OneSignal/OnLogoutEmailSuccess::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnLogoutEmailSuccess_BeginInvoke_m1768103057 (OnLogoutEmailSuccess_t3366364849 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal/OnLogoutEmailSuccess::EndInvoke(System.IAsyncResult)
extern "C"  void OnLogoutEmailSuccess_EndInvoke_m3655005928 (OnLogoutEmailSuccess_t3366364849 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
