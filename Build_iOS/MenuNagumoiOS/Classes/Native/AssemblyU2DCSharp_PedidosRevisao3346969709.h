﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<PedidoData>
struct List_1_t516228991;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PedidosRevisao
struct  PedidosRevisao_t3346969709  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject PedidosRevisao::content
	GameObject_t3674682005 * ___content_4;
	// UnityEngine.GameObject PedidosRevisao::itemPrefab
	GameObject_t3674682005 * ___itemPrefab_5;
	// UnityEngine.GameObject PedidosRevisao::loadingObj
	GameObject_t3674682005 * ___loadingObj_6;
	// UnityEngine.GameObject PedidosRevisao::popup
	GameObject_t3674682005 * ___popup_7;
	// UnityEngine.GameObject PedidosRevisao::popupPassarPeso
	GameObject_t3674682005 * ___popupPassarPeso_8;

public:
	inline static int32_t get_offset_of_content_4() { return static_cast<int32_t>(offsetof(PedidosRevisao_t3346969709, ___content_4)); }
	inline GameObject_t3674682005 * get_content_4() const { return ___content_4; }
	inline GameObject_t3674682005 ** get_address_of_content_4() { return &___content_4; }
	inline void set_content_4(GameObject_t3674682005 * value)
	{
		___content_4 = value;
		Il2CppCodeGenWriteBarrier(&___content_4, value);
	}

	inline static int32_t get_offset_of_itemPrefab_5() { return static_cast<int32_t>(offsetof(PedidosRevisao_t3346969709, ___itemPrefab_5)); }
	inline GameObject_t3674682005 * get_itemPrefab_5() const { return ___itemPrefab_5; }
	inline GameObject_t3674682005 ** get_address_of_itemPrefab_5() { return &___itemPrefab_5; }
	inline void set_itemPrefab_5(GameObject_t3674682005 * value)
	{
		___itemPrefab_5 = value;
		Il2CppCodeGenWriteBarrier(&___itemPrefab_5, value);
	}

	inline static int32_t get_offset_of_loadingObj_6() { return static_cast<int32_t>(offsetof(PedidosRevisao_t3346969709, ___loadingObj_6)); }
	inline GameObject_t3674682005 * get_loadingObj_6() const { return ___loadingObj_6; }
	inline GameObject_t3674682005 ** get_address_of_loadingObj_6() { return &___loadingObj_6; }
	inline void set_loadingObj_6(GameObject_t3674682005 * value)
	{
		___loadingObj_6 = value;
		Il2CppCodeGenWriteBarrier(&___loadingObj_6, value);
	}

	inline static int32_t get_offset_of_popup_7() { return static_cast<int32_t>(offsetof(PedidosRevisao_t3346969709, ___popup_7)); }
	inline GameObject_t3674682005 * get_popup_7() const { return ___popup_7; }
	inline GameObject_t3674682005 ** get_address_of_popup_7() { return &___popup_7; }
	inline void set_popup_7(GameObject_t3674682005 * value)
	{
		___popup_7 = value;
		Il2CppCodeGenWriteBarrier(&___popup_7, value);
	}

	inline static int32_t get_offset_of_popupPassarPeso_8() { return static_cast<int32_t>(offsetof(PedidosRevisao_t3346969709, ___popupPassarPeso_8)); }
	inline GameObject_t3674682005 * get_popupPassarPeso_8() const { return ___popupPassarPeso_8; }
	inline GameObject_t3674682005 ** get_address_of_popupPassarPeso_8() { return &___popupPassarPeso_8; }
	inline void set_popupPassarPeso_8(GameObject_t3674682005 * value)
	{
		___popupPassarPeso_8 = value;
		Il2CppCodeGenWriteBarrier(&___popupPassarPeso_8, value);
	}
};

struct PedidosRevisao_t3346969709_StaticFields
{
public:
	// System.Collections.Generic.List`1<PedidoData> PedidosRevisao::pedidosLst
	List_1_t516228991 * ___pedidosLst_2;
	// System.String PedidosRevisao::idPedido
	String_t* ___idPedido_3;

public:
	inline static int32_t get_offset_of_pedidosLst_2() { return static_cast<int32_t>(offsetof(PedidosRevisao_t3346969709_StaticFields, ___pedidosLst_2)); }
	inline List_1_t516228991 * get_pedidosLst_2() const { return ___pedidosLst_2; }
	inline List_1_t516228991 ** get_address_of_pedidosLst_2() { return &___pedidosLst_2; }
	inline void set_pedidosLst_2(List_1_t516228991 * value)
	{
		___pedidosLst_2 = value;
		Il2CppCodeGenWriteBarrier(&___pedidosLst_2, value);
	}

	inline static int32_t get_offset_of_idPedido_3() { return static_cast<int32_t>(offsetof(PedidosRevisao_t3346969709_StaticFields, ___idPedido_3)); }
	inline String_t* get_idPedido_3() const { return ___idPedido_3; }
	inline String_t** get_address_of_idPedido_3() { return &___idPedido_3; }
	inline void set_idPedido_3(String_t* value)
	{
		___idPedido_3 = value;
		Il2CppCodeGenWriteBarrier(&___idPedido_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
