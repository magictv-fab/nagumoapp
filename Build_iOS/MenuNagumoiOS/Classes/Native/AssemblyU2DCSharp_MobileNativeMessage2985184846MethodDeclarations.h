﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MobileNativeMessage
struct MobileNativeMessage_t2985184846;
// System.String
struct String_t;
// UnionAssets.FLE.CEvent
struct CEvent_t44106931;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_UnionAssets_FLE_CEvent44106931.h"

// System.Void MobileNativeMessage::.ctor(System.String,System.String)
extern "C"  void MobileNativeMessage__ctor_m3160227009 (MobileNativeMessage_t2985184846 * __this, String_t* ___title0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MobileNativeMessage::.ctor(System.String,System.String,System.String)
extern "C"  void MobileNativeMessage__ctor_m2439544637 (MobileNativeMessage_t2985184846 * __this, String_t* ___title0, String_t* ___message1, String_t* ___ok2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MobileNativeMessage::init(System.String,System.String,System.String)
extern "C"  void MobileNativeMessage_init_m131802019 (MobileNativeMessage_t2985184846 * __this, String_t* ___title0, String_t* ___message1, String_t* ___ok2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MobileNativeMessage::OnCompleteListener(UnionAssets.FLE.CEvent)
extern "C"  void MobileNativeMessage_OnCompleteListener_m1951602705 (MobileNativeMessage_t2985184846 * __this, CEvent_t44106931 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MobileNativeMessage::<OnComplete>m__A()
extern "C"  void MobileNativeMessage_U3COnCompleteU3Em__A_m788896037 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
