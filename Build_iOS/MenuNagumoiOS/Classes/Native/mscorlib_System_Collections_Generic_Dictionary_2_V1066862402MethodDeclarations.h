﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<ZXing.ResultMetadataType,System.Object>
struct Dictionary_2_t3135028994;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1066862402.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ZXing.ResultMetadataType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2977215665_gshared (Enumerator_t1066862402 * __this, Dictionary_2_t3135028994 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m2977215665(__this, ___host0, method) ((  void (*) (Enumerator_t1066862402 *, Dictionary_2_t3135028994 *, const MethodInfo*))Enumerator__ctor_m2977215665_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ZXing.ResultMetadataType,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2691974106_gshared (Enumerator_t1066862402 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2691974106(__this, method) ((  Il2CppObject * (*) (Enumerator_t1066862402 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2691974106_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ZXing.ResultMetadataType,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m620113764_gshared (Enumerator_t1066862402 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m620113764(__this, method) ((  void (*) (Enumerator_t1066862402 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m620113764_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ZXing.ResultMetadataType,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m275642771_gshared (Enumerator_t1066862402 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m275642771(__this, method) ((  void (*) (Enumerator_t1066862402 *, const MethodInfo*))Enumerator_Dispose_m275642771_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ZXing.ResultMetadataType,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m873925652_gshared (Enumerator_t1066862402 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m873925652(__this, method) ((  bool (*) (Enumerator_t1066862402 *, const MethodInfo*))Enumerator_MoveNext_m873925652_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ZXing.ResultMetadataType,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m4238498710_gshared (Enumerator_t1066862402 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m4238498710(__this, method) ((  Il2CppObject * (*) (Enumerator_t1066862402 *, const MethodInfo*))Enumerator_get_Current_m4238498710_gshared)(__this, method)
