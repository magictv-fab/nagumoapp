﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "PlayMaker_HutongGames_PlayMaker_InterpolationType930981288.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FloatInterpolate
struct  FloatInterpolate_t1095399597  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.InterpolationType HutongGames.PlayMaker.Actions.FloatInterpolate::mode
	int32_t ___mode_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatInterpolate::fromFloat
	FsmFloat_t2134102846 * ___fromFloat_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatInterpolate::toFloat
	FsmFloat_t2134102846 * ___toFloat_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatInterpolate::time
	FsmFloat_t2134102846 * ___time_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatInterpolate::storeResult
	FsmFloat_t2134102846 * ___storeResult_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.FloatInterpolate::finishEvent
	FsmEvent_t2133468028 * ___finishEvent_14;
	// System.Boolean HutongGames.PlayMaker.Actions.FloatInterpolate::realTime
	bool ___realTime_15;
	// System.Single HutongGames.PlayMaker.Actions.FloatInterpolate::startTime
	float ___startTime_16;
	// System.Single HutongGames.PlayMaker.Actions.FloatInterpolate::currentTime
	float ___currentTime_17;

public:
	inline static int32_t get_offset_of_mode_9() { return static_cast<int32_t>(offsetof(FloatInterpolate_t1095399597, ___mode_9)); }
	inline int32_t get_mode_9() const { return ___mode_9; }
	inline int32_t* get_address_of_mode_9() { return &___mode_9; }
	inline void set_mode_9(int32_t value)
	{
		___mode_9 = value;
	}

	inline static int32_t get_offset_of_fromFloat_10() { return static_cast<int32_t>(offsetof(FloatInterpolate_t1095399597, ___fromFloat_10)); }
	inline FsmFloat_t2134102846 * get_fromFloat_10() const { return ___fromFloat_10; }
	inline FsmFloat_t2134102846 ** get_address_of_fromFloat_10() { return &___fromFloat_10; }
	inline void set_fromFloat_10(FsmFloat_t2134102846 * value)
	{
		___fromFloat_10 = value;
		Il2CppCodeGenWriteBarrier(&___fromFloat_10, value);
	}

	inline static int32_t get_offset_of_toFloat_11() { return static_cast<int32_t>(offsetof(FloatInterpolate_t1095399597, ___toFloat_11)); }
	inline FsmFloat_t2134102846 * get_toFloat_11() const { return ___toFloat_11; }
	inline FsmFloat_t2134102846 ** get_address_of_toFloat_11() { return &___toFloat_11; }
	inline void set_toFloat_11(FsmFloat_t2134102846 * value)
	{
		___toFloat_11 = value;
		Il2CppCodeGenWriteBarrier(&___toFloat_11, value);
	}

	inline static int32_t get_offset_of_time_12() { return static_cast<int32_t>(offsetof(FloatInterpolate_t1095399597, ___time_12)); }
	inline FsmFloat_t2134102846 * get_time_12() const { return ___time_12; }
	inline FsmFloat_t2134102846 ** get_address_of_time_12() { return &___time_12; }
	inline void set_time_12(FsmFloat_t2134102846 * value)
	{
		___time_12 = value;
		Il2CppCodeGenWriteBarrier(&___time_12, value);
	}

	inline static int32_t get_offset_of_storeResult_13() { return static_cast<int32_t>(offsetof(FloatInterpolate_t1095399597, ___storeResult_13)); }
	inline FsmFloat_t2134102846 * get_storeResult_13() const { return ___storeResult_13; }
	inline FsmFloat_t2134102846 ** get_address_of_storeResult_13() { return &___storeResult_13; }
	inline void set_storeResult_13(FsmFloat_t2134102846 * value)
	{
		___storeResult_13 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_13, value);
	}

	inline static int32_t get_offset_of_finishEvent_14() { return static_cast<int32_t>(offsetof(FloatInterpolate_t1095399597, ___finishEvent_14)); }
	inline FsmEvent_t2133468028 * get_finishEvent_14() const { return ___finishEvent_14; }
	inline FsmEvent_t2133468028 ** get_address_of_finishEvent_14() { return &___finishEvent_14; }
	inline void set_finishEvent_14(FsmEvent_t2133468028 * value)
	{
		___finishEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___finishEvent_14, value);
	}

	inline static int32_t get_offset_of_realTime_15() { return static_cast<int32_t>(offsetof(FloatInterpolate_t1095399597, ___realTime_15)); }
	inline bool get_realTime_15() const { return ___realTime_15; }
	inline bool* get_address_of_realTime_15() { return &___realTime_15; }
	inline void set_realTime_15(bool value)
	{
		___realTime_15 = value;
	}

	inline static int32_t get_offset_of_startTime_16() { return static_cast<int32_t>(offsetof(FloatInterpolate_t1095399597, ___startTime_16)); }
	inline float get_startTime_16() const { return ___startTime_16; }
	inline float* get_address_of_startTime_16() { return &___startTime_16; }
	inline void set_startTime_16(float value)
	{
		___startTime_16 = value;
	}

	inline static int32_t get_offset_of_currentTime_17() { return static_cast<int32_t>(offsetof(FloatInterpolate_t1095399597, ___currentTime_17)); }
	inline float get_currentTime_17() const { return ___currentTime_17; }
	inline float* get_address_of_currentTime_17() { return &___currentTime_17; }
	inline void set_currentTime_17(float value)
	{
		___currentTime_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
