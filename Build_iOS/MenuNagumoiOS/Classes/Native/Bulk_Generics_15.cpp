﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityEngine.Events.CachedInvokableCall`1<System.Int32>
struct CachedInvokableCall_1_t742075359;
// UnityEngine.Object
struct Object_t3071478659;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// UnityEngine.Events.CachedInvokableCall`1<System.Object>
struct CachedInvokableCall_1_t3759053230;
// System.Object
struct Il2CppObject;
// UnityEngine.Events.CachedInvokableCall`1<System.Single>
struct CachedInvokableCall_1_t3880155831;
// UnityEngine.Events.InvokableCall`1<System.Boolean>
struct InvokableCall_1_t2626711275;
// UnityEngine.Events.UnityAction`1<System.Boolean>
struct UnityAction_1_t370978721;
// UnityEngine.Events.InvokableCall`1<System.Int32>
struct InvokableCall_1_t3303751057;
// UnityEngine.Events.UnityAction`1<System.Int32>
struct UnityAction_1_t1048018503;
// UnityEngine.Events.InvokableCall`1<System.Object>
struct InvokableCall_1_t2025761632;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t4064996374;
// UnityEngine.Events.InvokableCall`1<System.Single>
struct InvokableCall_1_t2146864233;
// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t4186098975;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Color>
struct InvokableCall_1_t2049492166;
// UnityEngine.Events.UnityAction`1<UnityEngine.Color>
struct UnityAction_1_t4088726908;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>
struct InvokableCall_1_t2137011826;
// UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>
struct UnityAction_1_t4176246568;
// UnityEngine.Events.InvokableCall`2<System.Object,System.Object>
struct InvokableCall_2_t3350606262;
// UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>
struct InvokableCall_3_t3637709590;
// UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>
struct InvokableCall_4_t2281252720;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;
// UnityEngine.Events.UnityAction`2<System.Object,System.Object>
struct UnityAction_2_t2640361832;
// UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>
struct UnityAction_3_t2271724636;
// UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>
struct UnityAction_4_t2496524882;
// UnityEngine.Events.UnityEvent`1<System.Boolean>
struct UnityEvent_1_t663396231;
// System.String
struct String_t;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t1559630662;
// UnityEngine.Events.UnityEvent`1<System.Int32>
struct UnityEvent_1_t1340436013;
// UnityEngine.Events.UnityEvent`1<System.Object>
struct UnityEvent_1_t62446588;
// UnityEngine.Events.UnityEvent`1<System.Single>
struct UnityEvent_1_t183549189;
// UnityEngine.Events.UnityEvent`1<UnityEngine.Color>
struct UnityEvent_1_t86177122;
// UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>
struct UnityEvent_1_t173696782;
// UnityEngine.Events.UnityEvent`2<System.Object,System.Object>
struct UnityEvent_2_t2262335274;
// UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>
struct UnityEvent_3_t1668724050;
// UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>
struct UnityEvent_4_t2186133700;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>
struct EventFunction_1_t864200549;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t2054899105;
// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
struct IndexedSet_1_t2695948348;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Predicate`1<System.Object>
struct Predicate_1_t3781873254;
// System.Comparison`1<System.Object>
struct Comparison_1_t2887177558;
// UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>
struct U3CStartU3Ec__Iterator0_t1674837779;
// UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>
struct U3CStartU3Ec__Iterator0_t3663265722;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t805970637;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t667441552;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>
struct TweenRunner_1_t2794398580;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1244034627;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t1967039240;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t1317283468;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1355284821;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t1355284823;
// UnityEngine.UI.ObjectPool`1<System.Object>
struct ObjectPool_1_t3970404496;
// ZXing.BarcodeReaderGeneric`1<System.Object>
struct BarcodeReaderGeneric_1_t3481712763;
// ZXing.Reader
struct Reader_t2610170425;
// System.Func`4<System.Object,System.Int32,System.Int32,ZXing.LuminanceSource>
struct Func_4_t3774219921;
// System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer>
struct Func_2_t2392815316;
// System.Func`5<System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat,ZXing.LuminanceSource>
struct Func_5_t533482363;
// ZXing.Result
struct Result_t2610723219;
// ZXing.LuminanceSource
struct LuminanceSource_t1231523093;
// ZXing.Common.DecodingOptions
struct DecodingOptions_t3608870897;
// ZXing.Binarizer
struct Binarizer_t1492033400;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>
struct ChangeNotifyDictionary_2_t1676496957;
// System.Action`2<System.Object,System.EventArgs>
struct Action_2_t2663079113;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t770439062;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t2483180780;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t3856534026;
// ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>
struct ChangeNotifyDictionary_2_t812725187;
// System.Collections.Generic.ICollection`1<ZXing.DecodeHintType>
struct ICollection_1_t2990371336;
// System.Collections.Generic.KeyValuePair`2<ZXing.DecodeHintType,System.Object>[]
struct KeyValuePair_2U5BU5D_t3618423950;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<ZXing.DecodeHintType,System.Object>>
struct IEnumerator_1_t2992762256;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_742075359.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_742075359MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "mscorlib_System_Reflection_MethodInfo318736065.h"
#include "mscorlib_System_Void2863195528.h"
#include "mscorlib_System_Int321153838500.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen3303751057MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen3303751057.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall3759053230.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall3759053230MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen2025761632MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen2025761632.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall3880155831.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall3880155831MethodDeclarations.h"
#include "mscorlib_System_Single4291918972.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen2146864233MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen2146864233.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen2626711275.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen2626711275MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall1559630662MethodDeclarations.h"
#include "mscorlib_System_Type2863145774MethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_NetFxCoreExtension2541795172MethodDeclarations.h"
#include "mscorlib_System_Delegate3310234105MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen370978721.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_RuntimeTypeHandle2669177232.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "mscorlib_System_ArgumentException928607144MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_ArgumentException928607144.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall1559630662.h"
#include "mscorlib_System_Boolean476798718.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen370978721MethodDeclarations.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen1048018503.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen1048018503MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen4064996374.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen4064996374MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen4186098975.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen4186098975MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen2049492166.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen2049492166MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen4088726908.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen4088726908MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen2137011826.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen2137011826MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen4176246568.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen4176246568MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen3350606262.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen3350606262MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen2640361832.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen2640361832MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_3_gen3637709590.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_3_gen3637709590MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_3_gen2271724636.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_3_gen2271724636MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_4_gen2281252720.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_4_gen2281252720MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_4_gen2496524882.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_4_gen2496524882MethodDeclarations.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen663396231.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen663396231MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase1020378628MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen1340436013.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen1340436013MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen62446588.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen62446588MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen183549189.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen183549189MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen86177122.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen86177122MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen173696782.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen173696782MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen2262335274.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen2262335274MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_3_gen1668724050.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_3_gen1668724050MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_4_gen2186133700.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_4_gen2186133700MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEven864200549.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEven864200549MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD2054899105.h"
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedS2695948348.h"
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedS2695948348MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3323877696.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3323877696MethodDeclarations.h"
#include "mscorlib_System_NotImplementedException1912495542MethodDeclarations.h"
#include "mscorlib_System_NotImplementedException1912495542.h"
#include "mscorlib_System_NotSupportedException1732551818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1732551818.h"
#include "mscorlib_System_Predicate_1_gen3781873254.h"
#include "mscorlib_System_Predicate_1_gen3781873254MethodDeclarations.h"
#include "mscorlib_System_Comparison_1_gen2887177558.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Tween1674837779.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Tween1674837779MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time4241968337MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf4203372500MethodDeclarations.h"
#include "mscorlib_System_UInt3224667981.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorT723277650.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorT723277650MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Tween3663265722.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Tween3663265722MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Float2711705593.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Float2711705593MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_TweenR805970637.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_TweenR805970637MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Object3071478659MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug4195163081MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3501516275MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Coroutine3621161934.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Tween2794398580.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Tween2794398580MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen251475784.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen251475784MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen2416204055.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2522024052.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen2416204055MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen2321612177.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen2321612177MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2522024052MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen3268453655.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen3268453655MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen1138214630.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen1138214630MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen1043622752.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen1043622752MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen3991458268.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen3991458268MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen1861219243.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1967039240.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen1861219243MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen1766627365.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen1766627365MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1967039240MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen3341702496.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen3341702496MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen1211463471.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1317283468.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen1211463471MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen1116871593.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen1116871593MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1317283468MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen3379703849.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen3379703849MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen1249464824.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1355284821.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen1249464824MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen1154872946.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen1154872946MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1355284821MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen3379703850.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen3379703850MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen1249464825.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1355284822.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen1249464825MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen1154872947.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen1154872947MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1355284822MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen3379703851.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen3379703851MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen1249464826.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1355284823.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen1249464826MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen1154872948.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen1154872948MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1355284823MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen3970404496.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen3970404496MethodDeclarations.h"
#include "System_System_Collections_Generic_Stack_1_gen2974409999.h"
#include "System_System_Collections_Generic_Stack_1_gen2974409999MethodDeclarations.h"
#include "mscorlib_System_Activator2714366379MethodDeclarations.h"
#include "mscorlib_System_Activator2714366379.h"
#include "QRCode_ZXing_BarcodeReaderGeneric_1_gen3481712763.h"
#include "QRCode_ZXing_BarcodeReaderGeneric_1_gen3481712763MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2392815316MethodDeclarations.h"
#include "System_Core_System_Func_5_gen533482363MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2392815316.h"
#include "QRCode_ZXing_Binarizer1492033400.h"
#include "QRCode_ZXing_LuminanceSource1231523093.h"
#include "System_Core_System_Func_5_gen533482363.h"
#include "mscorlib_System_Byte2862609660.h"
#include "QRCode_ZXing_RGBLuminanceSource_BitmapFormat3665922245.h"
#include "System_Core_System_Func_4_gen3774219921.h"
#include "QRCode_ZXing_MultiFormatReader4081038293MethodDeclarations.h"
#include "QRCode_ZXing_Common_DecodingOptions3608870897MethodDeclarations.h"
#include "System_Core_System_Action_2_gen2663079113.h"
#include "QRCode_ZXing_MultiFormatReader4081038293.h"
#include "QRCode_ZXing_Common_DecodingOptions3608870897.h"
#include "QRCode_ZXing_Result2610723219.h"
#include "QRCode_ZXing_BinaryBitmap2444664454MethodDeclarations.h"
#include "QRCode_ZXing_Result2610723219MethodDeclarations.h"
#include "QRCode_ZXing_BinaryBitmap2444664454.h"
#include "QRCode_ZXing_DecodeHintType2095781349.h"
#include "QRCode_ZXing_LuminanceSource1231523093MethodDeclarations.h"
#include "QRCode_ZXing_ResultMetadataType2923366972.h"
#include "mscorlib_System_InvalidOperationException1589641621MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3573189601MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException1589641621.h"
#include "mscorlib_System_ArgumentNullException3573189601.h"
#include "System_Core_System_Func_4_gen3774219921MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3006539355MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3006539355.h"
#include "QRCode_ZXing_Common_HybridBinarizer3517076856MethodDeclarations.h"
#include "QRCode_ZXing_Common_HybridBinarizer3517076856.h"
#include "QRCode_ZXing_RGBLuminanceSource3258519836MethodDeclarations.h"
#include "QRCode_ZXing_RGBLuminanceSource3258519836.h"
#include "QRCode_ZXing_Common_DecodingOptions_ChangeNotifyDi1676496957.h"
#include "QRCode_ZXing_Common_DecodingOptions_ChangeNotifyDi1676496957MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2045888271.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2045888271MethodDeclarations.h"
#include "System_Core_System_Action_2_gen2663079113MethodDeclarations.h"
#include "mscorlib_System_EventArgs2540831021.h"
#include "mscorlib_System_EventArgs2540831021MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977.h"
#include "QRCode_ZXing_Common_DecodingOptions_ChangeNotifyDic812725187.h"
#include "QRCode_ZXing_Common_DecodingOptions_ChangeNotifyDic812725187MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1182116501.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1182116501MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21080897207.h"

// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Boolean>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisBoolean_t476798718_m2294441611_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisBoolean_t476798718_m2294441611(__this /* static, unused */, p0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisBoolean_t476798718_m2294441611_gshared)(__this /* static, unused */, p0, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Int32>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisInt32_t1153838500_m1459945585_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisInt32_t1153838500_m1459945585(__this /* static, unused */, p0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisInt32_t1153838500_m1459945585_gshared)(__this /* static, unused */, p0, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisIl2CppObject_m1692402054_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisIl2CppObject_m1692402054(__this /* static, unused */, p0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisIl2CppObject_m1692402054_gshared)(__this /* static, unused */, p0, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Single>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisSingle_t4291918972_m1076801679_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisSingle_t4291918972_m1076801679(__this /* static, unused */, p0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisSingle_t4291918972_m1076801679_gshared)(__this /* static, unused */, p0, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Color>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisColor_t4194546905_m2441106612_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisColor_t4194546905_m2441106612(__this /* static, unused */, p0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisColor_t4194546905_m2441106612_gshared)(__this /* static, unused */, p0, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Vector2>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisVector2_t4282066565_m1471167904_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisVector2_t4282066565_m1471167904(__this /* static, unused */, p0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisVector2_t4282066565_m1471167904_gshared)(__this /* static, unused */, p0, method)
// !!0 System.Activator::CreateInstance<System.Object>()
extern "C"  Il2CppObject * Activator_CreateInstance_TisIl2CppObject_m1443760614_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Activator_CreateInstance_TisIl2CppObject_m1443760614(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Activator_CreateInstance_TisIl2CppObject_m1443760614_gshared)(__this /* static, unused */, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Int32>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t CachedInvokableCall_1__ctor_m1791898438_MetadataUsageId;
extern "C"  void CachedInvokableCall_1__ctor_m1791898438_gshared (CachedInvokableCall_1_t742075359 * __this, Object_t3071478659 * ___target0, MethodInfo_t * ___theFunction1, int32_t ___argument2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CachedInvokableCall_1__ctor_m1791898438_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_Arg1_1(((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1)));
		Object_t3071478659 * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((InvokableCall_1_t3303751057 *)__this);
		((  void (*) (InvokableCall_1_t3303751057 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((InvokableCall_1_t3303751057 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t1108656482* L_2 = (ObjectU5BU5D_t1108656482*)__this->get_m_Arg1_1();
		int32_t L_3 = ___argument2;
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_5);
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Int32>::Invoke(System.Object[])
extern "C"  void CachedInvokableCall_1_Invoke_m3818847761_gshared (CachedInvokableCall_1_t742075359 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_m_Arg1_1();
		NullCheck((InvokableCall_1_t3303751057 *)__this);
		((  void (*) (InvokableCall_1_t3303751057 *, ObjectU5BU5D_t1108656482*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((InvokableCall_1_t3303751057 *)__this, (ObjectU5BU5D_t1108656482*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Object>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t CachedInvokableCall_1__ctor_m165005445_MetadataUsageId;
extern "C"  void CachedInvokableCall_1__ctor_m165005445_gshared (CachedInvokableCall_1_t3759053230 * __this, Object_t3071478659 * ___target0, MethodInfo_t * ___theFunction1, Il2CppObject * ___argument2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CachedInvokableCall_1__ctor_m165005445_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_Arg1_1(((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1)));
		Object_t3071478659 * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((InvokableCall_1_t2025761632 *)__this);
		((  void (*) (InvokableCall_1_t2025761632 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((InvokableCall_1_t2025761632 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t1108656482* L_2 = (ObjectU5BU5D_t1108656482*)__this->get_m_Arg1_1();
		Il2CppObject * L_3 = ___argument2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Object>::Invoke(System.Object[])
extern "C"  void CachedInvokableCall_1_Invoke_m3570141426_gshared (CachedInvokableCall_1_t3759053230 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_m_Arg1_1();
		NullCheck((InvokableCall_1_t2025761632 *)__this);
		((  void (*) (InvokableCall_1_t2025761632 *, ObjectU5BU5D_t1108656482*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((InvokableCall_1_t2025761632 *)__this, (ObjectU5BU5D_t1108656482*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Single>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t CachedInvokableCall_1__ctor_m3760156124_MetadataUsageId;
extern "C"  void CachedInvokableCall_1__ctor_m3760156124_gshared (CachedInvokableCall_1_t3880155831 * __this, Object_t3071478659 * ___target0, MethodInfo_t * ___theFunction1, float ___argument2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CachedInvokableCall_1__ctor_m3760156124_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_Arg1_1(((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1)));
		Object_t3071478659 * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((InvokableCall_1_t2146864233 *)__this);
		((  void (*) (InvokableCall_1_t2146864233 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((InvokableCall_1_t2146864233 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t1108656482* L_2 = (ObjectU5BU5D_t1108656482*)__this->get_m_Arg1_1();
		float L_3 = ___argument2;
		float L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_5);
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Single>::Invoke(System.Object[])
extern "C"  void CachedInvokableCall_1_Invoke_m4174790843_gshared (CachedInvokableCall_1_t3880155831 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_m_Arg1_1();
		NullCheck((InvokableCall_1_t2146864233 *)__this);
		((  void (*) (InvokableCall_1_t2146864233 *, ObjectU5BU5D_t1108656482*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((InvokableCall_1_t2146864233 *)__this, (ObjectU5BU5D_t1108656482*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_1__ctor_m364542482_MetadataUsageId;
extern "C"  void InvokableCall_1__ctor_m364542482_gshared (InvokableCall_1_t2626711275 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m364542482_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t1559630662 *)__this);
		BaseInvokableCall__ctor_m1692884045((BaseInvokableCall_t1559630662 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		UnityAction_1_t370978721 * L_2 = (UnityAction_1_t370978721 *)__this->get_Delegate_0();
		MethodInfo_t * L_3 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_5 = ___target0;
		Delegate_t3310234105 * L_6 = NetFxCoreExtensions_CreateDelegate_m3408605866(NULL /*static, unused*/, (MethodInfo_t *)L_3, (Type_t *)L_4, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		Delegate_t3310234105 * L_7 = Delegate_Combine_m1842362874(NULL /*static, unused*/, (Delegate_t3310234105 *)L_2, (Delegate_t3310234105 *)((UnityAction_1_t370978721 *)Castclass(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_1_t370978721 *)Castclass(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m1301209584_gshared (InvokableCall_1_t2626711275 * __this, UnityAction_1_t370978721 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t1559630662 *)__this);
		BaseInvokableCall__ctor_m2839815662((BaseInvokableCall_t1559630662 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t370978721 * L_0 = (UnityAction_1_t370978721 *)__this->get_Delegate_0();
		UnityAction_1_t370978721 * L_1 = ___action0;
		Delegate_t3310234105 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, (Delegate_t3310234105 *)L_0, (Delegate_t3310234105 *)L_1, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_1_t370978721 *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3093451866;
extern const uint32_t InvokableCall_1_Invoke_m4112204073_MetadataUsageId;
extern "C"  void InvokableCall_1_Invoke_m4112204073_gshared (InvokableCall_1_t2626711275 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m4112204073_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentException_t928607144 * L_1 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_1, (String_t*)_stringLiteral3093451866, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0014:
	{
		ObjectU5BU5D_t1108656482* L_2 = ___args0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		UnityAction_1_t370978721 * L_5 = (UnityAction_1_t370978721 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m2941661095(NULL /*static, unused*/, (Delegate_t3310234105 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		UnityAction_1_t370978721 * L_7 = (UnityAction_1_t370978721 *)__this->get_Delegate_0();
		ObjectU5BU5D_t1108656482* L_8 = ___args0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t370978721 *)L_7);
		((  void (*) (UnityAction_1_t370978721 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((UnityAction_1_t370978721 *)L_7, (bool)((*(bool*)((bool*)UnBox (L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
	}

IL_003f:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Boolean>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m560012207_gshared (InvokableCall_1_t2626711275 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t370978721 * L_0 = (UnityAction_1_t370978721 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3310234105 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2860483769((Delegate_t3310234105 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0024;
		}
	}
	{
		UnityAction_1_t370978721 * L_3 = (UnityAction_1_t370978721 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m1628849205(NULL /*static, unused*/, (Delegate_t3310234105 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0025;
	}

IL_0024:
	{
		G_B3_0 = 0;
	}

IL_0025:
	{
		return (bool)G_B3_0;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_1__ctor_m629014264_MetadataUsageId;
extern "C"  void InvokableCall_1__ctor_m629014264_gshared (InvokableCall_1_t3303751057 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m629014264_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t1559630662 *)__this);
		BaseInvokableCall__ctor_m1692884045((BaseInvokableCall_t1559630662 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		UnityAction_1_t1048018503 * L_2 = (UnityAction_1_t1048018503 *)__this->get_Delegate_0();
		MethodInfo_t * L_3 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_5 = ___target0;
		Delegate_t3310234105 * L_6 = NetFxCoreExtensions_CreateDelegate_m3408605866(NULL /*static, unused*/, (MethodInfo_t *)L_3, (Type_t *)L_4, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		Delegate_t3310234105 * L_7 = Delegate_Combine_m1842362874(NULL /*static, unused*/, (Delegate_t3310234105 *)L_2, (Delegate_t3310234105 *)((UnityAction_1_t1048018503 *)Castclass(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_1_t1048018503 *)Castclass(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m1140427606_gshared (InvokableCall_1_t3303751057 * __this, UnityAction_1_t1048018503 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t1559630662 *)__this);
		BaseInvokableCall__ctor_m2839815662((BaseInvokableCall_t1559630662 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t1048018503 * L_0 = (UnityAction_1_t1048018503 *)__this->get_Delegate_0();
		UnityAction_1_t1048018503 * L_1 = ___action0;
		Delegate_t3310234105 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, (Delegate_t3310234105 *)L_0, (Delegate_t3310234105 *)L_1, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_1_t1048018503 *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3093451866;
extern const uint32_t InvokableCall_1_Invoke_m1232083343_MetadataUsageId;
extern "C"  void InvokableCall_1_Invoke_m1232083343_gshared (InvokableCall_1_t3303751057 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m1232083343_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentException_t928607144 * L_1 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_1, (String_t*)_stringLiteral3093451866, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0014:
	{
		ObjectU5BU5D_t1108656482* L_2 = ___args0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		UnityAction_1_t1048018503 * L_5 = (UnityAction_1_t1048018503 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m2941661095(NULL /*static, unused*/, (Delegate_t3310234105 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		UnityAction_1_t1048018503 * L_7 = (UnityAction_1_t1048018503 *)__this->get_Delegate_0();
		ObjectU5BU5D_t1108656482* L_8 = ___args0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t1048018503 *)L_7);
		((  void (*) (UnityAction_1_t1048018503 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((UnityAction_1_t1048018503 *)L_7, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
	}

IL_003f:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Int32>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m3482592137_gshared (InvokableCall_1_t3303751057 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t1048018503 * L_0 = (UnityAction_1_t1048018503 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3310234105 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2860483769((Delegate_t3310234105 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0024;
		}
	}
	{
		UnityAction_1_t1048018503 * L_3 = (UnityAction_1_t1048018503 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m1628849205(NULL /*static, unused*/, (Delegate_t3310234105 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0025;
	}

IL_0024:
	{
		G_B3_0 = 0;
	}

IL_0025:
	{
		return (bool)G_B3_0;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_1__ctor_m3494792797_MetadataUsageId;
extern "C"  void InvokableCall_1__ctor_m3494792797_gshared (InvokableCall_1_t2025761632 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m3494792797_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t1559630662 *)__this);
		BaseInvokableCall__ctor_m1692884045((BaseInvokableCall_t1559630662 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		UnityAction_1_t4064996374 * L_2 = (UnityAction_1_t4064996374 *)__this->get_Delegate_0();
		MethodInfo_t * L_3 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_5 = ___target0;
		Delegate_t3310234105 * L_6 = NetFxCoreExtensions_CreateDelegate_m3408605866(NULL /*static, unused*/, (MethodInfo_t *)L_3, (Type_t *)L_4, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		Delegate_t3310234105 * L_7 = Delegate_Combine_m1842362874(NULL /*static, unused*/, (Delegate_t3310234105 *)L_2, (Delegate_t3310234105 *)((UnityAction_1_t4064996374 *)Castclass(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_1_t4064996374 *)Castclass(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m2810088059_gshared (InvokableCall_1_t2025761632 * __this, UnityAction_1_t4064996374 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t1559630662 *)__this);
		BaseInvokableCall__ctor_m2839815662((BaseInvokableCall_t1559630662 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t4064996374 * L_0 = (UnityAction_1_t4064996374 *)__this->get_Delegate_0();
		UnityAction_1_t4064996374 * L_1 = ___action0;
		Delegate_t3310234105 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, (Delegate_t3310234105 *)L_0, (Delegate_t3310234105 *)L_1, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_1_t4064996374 *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3093451866;
extern const uint32_t InvokableCall_1_Invoke_m689855796_MetadataUsageId;
extern "C"  void InvokableCall_1_Invoke_m689855796_gshared (InvokableCall_1_t2025761632 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m689855796_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentException_t928607144 * L_1 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_1, (String_t*)_stringLiteral3093451866, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0014:
	{
		ObjectU5BU5D_t1108656482* L_2 = ___args0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		UnityAction_1_t4064996374 * L_5 = (UnityAction_1_t4064996374 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m2941661095(NULL /*static, unused*/, (Delegate_t3310234105 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		UnityAction_1_t4064996374 * L_7 = (UnityAction_1_t4064996374 *)__this->get_Delegate_0();
		ObjectU5BU5D_t1108656482* L_8 = ___args0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t4064996374 *)L_7);
		((  void (*) (UnityAction_1_t4064996374 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((UnityAction_1_t4064996374 *)L_7, (Il2CppObject *)((Il2CppObject *)Castclass(L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
	}

IL_003f:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Object>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m1349477752_gshared (InvokableCall_1_t2025761632 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t4064996374 * L_0 = (UnityAction_1_t4064996374 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3310234105 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2860483769((Delegate_t3310234105 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0024;
		}
	}
	{
		UnityAction_1_t4064996374 * L_3 = (UnityAction_1_t4064996374 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m1628849205(NULL /*static, unused*/, (Delegate_t3310234105 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0025;
	}

IL_0024:
	{
		G_B3_0 = 0;
	}

IL_0025:
	{
		return (bool)G_B3_0;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_1__ctor_m119008486_MetadataUsageId;
extern "C"  void InvokableCall_1__ctor_m119008486_gshared (InvokableCall_1_t2146864233 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m119008486_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t1559630662 *)__this);
		BaseInvokableCall__ctor_m1692884045((BaseInvokableCall_t1559630662 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		UnityAction_1_t4186098975 * L_2 = (UnityAction_1_t4186098975 *)__this->get_Delegate_0();
		MethodInfo_t * L_3 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_5 = ___target0;
		Delegate_t3310234105 * L_6 = NetFxCoreExtensions_CreateDelegate_m3408605866(NULL /*static, unused*/, (MethodInfo_t *)L_3, (Type_t *)L_4, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		Delegate_t3310234105 * L_7 = Delegate_Combine_m1842362874(NULL /*static, unused*/, (Delegate_t3310234105 *)L_2, (Delegate_t3310234105 *)((UnityAction_1_t4186098975 *)Castclass(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_1_t4186098975 *)Castclass(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m593494980_gshared (InvokableCall_1_t2146864233 * __this, UnityAction_1_t4186098975 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t1559630662 *)__this);
		BaseInvokableCall__ctor_m2839815662((BaseInvokableCall_t1559630662 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t4186098975 * L_0 = (UnityAction_1_t4186098975 *)__this->get_Delegate_0();
		UnityAction_1_t4186098975 * L_1 = ___action0;
		Delegate_t3310234105 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, (Delegate_t3310234105 *)L_0, (Delegate_t3310234105 *)L_1, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_1_t4186098975 *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3093451866;
extern const uint32_t InvokableCall_1_Invoke_m1294505213_MetadataUsageId;
extern "C"  void InvokableCall_1_Invoke_m1294505213_gshared (InvokableCall_1_t2146864233 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m1294505213_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentException_t928607144 * L_1 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_1, (String_t*)_stringLiteral3093451866, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0014:
	{
		ObjectU5BU5D_t1108656482* L_2 = ___args0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		UnityAction_1_t4186098975 * L_5 = (UnityAction_1_t4186098975 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m2941661095(NULL /*static, unused*/, (Delegate_t3310234105 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		UnityAction_1_t4186098975 * L_7 = (UnityAction_1_t4186098975 *)__this->get_Delegate_0();
		ObjectU5BU5D_t1108656482* L_8 = ___args0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t4186098975 *)L_7);
		((  void (*) (UnityAction_1_t4186098975 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((UnityAction_1_t4186098975 *)L_7, (float)((*(float*)((float*)UnBox (L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
	}

IL_003f:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Single>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m270750159_gshared (InvokableCall_1_t2146864233 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t4186098975 * L_0 = (UnityAction_1_t4186098975 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3310234105 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2860483769((Delegate_t3310234105 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0024;
		}
	}
	{
		UnityAction_1_t4186098975 * L_3 = (UnityAction_1_t4186098975 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m1628849205(NULL /*static, unused*/, (Delegate_t3310234105 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0025;
	}

IL_0024:
	{
		G_B3_0 = 0;
	}

IL_0025:
	{
		return (bool)G_B3_0;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_1__ctor_m2623598219_MetadataUsageId;
extern "C"  void InvokableCall_1__ctor_m2623598219_gshared (InvokableCall_1_t2049492166 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m2623598219_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t1559630662 *)__this);
		BaseInvokableCall__ctor_m1692884045((BaseInvokableCall_t1559630662 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		UnityAction_1_t4088726908 * L_2 = (UnityAction_1_t4088726908 *)__this->get_Delegate_0();
		MethodInfo_t * L_3 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_5 = ___target0;
		Delegate_t3310234105 * L_6 = NetFxCoreExtensions_CreateDelegate_m3408605866(NULL /*static, unused*/, (MethodInfo_t *)L_3, (Type_t *)L_4, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		Delegate_t3310234105 * L_7 = Delegate_Combine_m1842362874(NULL /*static, unused*/, (Delegate_t3310234105 *)L_2, (Delegate_t3310234105 *)((UnityAction_1_t4088726908 *)Castclass(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_1_t4088726908 *)Castclass(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m492856617_gshared (InvokableCall_1_t2049492166 * __this, UnityAction_1_t4088726908 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t1559630662 *)__this);
		BaseInvokableCall__ctor_m2839815662((BaseInvokableCall_t1559630662 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t4088726908 * L_0 = (UnityAction_1_t4088726908 *)__this->get_Delegate_0();
		UnityAction_1_t4088726908 * L_1 = ___action0;
		Delegate_t3310234105 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, (Delegate_t3310234105 *)L_0, (Delegate_t3310234105 *)L_1, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_1_t4088726908 *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3093451866;
extern const uint32_t InvokableCall_1_Invoke_m2460605154_MetadataUsageId;
extern "C"  void InvokableCall_1_Invoke_m2460605154_gshared (InvokableCall_1_t2049492166 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m2460605154_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentException_t928607144 * L_1 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_1, (String_t*)_stringLiteral3093451866, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0014:
	{
		ObjectU5BU5D_t1108656482* L_2 = ___args0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		UnityAction_1_t4088726908 * L_5 = (UnityAction_1_t4088726908 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m2941661095(NULL /*static, unused*/, (Delegate_t3310234105 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		UnityAction_1_t4088726908 * L_7 = (UnityAction_1_t4088726908 *)__this->get_Delegate_0();
		ObjectU5BU5D_t1108656482* L_8 = ___args0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t4088726908 *)L_7);
		((  void (*) (UnityAction_1_t4088726908 *, Color_t4194546905 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((UnityAction_1_t4088726908 *)L_7, (Color_t4194546905 )((*(Color_t4194546905 *)((Color_t4194546905 *)UnBox (L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
	}

IL_003f:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m3902655114_gshared (InvokableCall_1_t2049492166 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t4088726908 * L_0 = (UnityAction_1_t4088726908 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3310234105 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2860483769((Delegate_t3310234105 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0024;
		}
	}
	{
		UnityAction_1_t4088726908 * L_3 = (UnityAction_1_t4088726908 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m1628849205(NULL /*static, unused*/, (Delegate_t3310234105 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0025;
	}

IL_0024:
	{
		G_B3_0 = 0;
	}

IL_0025:
	{
		return (bool)G_B3_0;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_1__ctor_m416642935_MetadataUsageId;
extern "C"  void InvokableCall_1__ctor_m416642935_gshared (InvokableCall_1_t2137011826 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m416642935_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t1559630662 *)__this);
		BaseInvokableCall__ctor_m1692884045((BaseInvokableCall_t1559630662 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		UnityAction_1_t4176246568 * L_2 = (UnityAction_1_t4176246568 *)__this->get_Delegate_0();
		MethodInfo_t * L_3 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_5 = ___target0;
		Delegate_t3310234105 * L_6 = NetFxCoreExtensions_CreateDelegate_m3408605866(NULL /*static, unused*/, (MethodInfo_t *)L_3, (Type_t *)L_4, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		Delegate_t3310234105 * L_7 = Delegate_Combine_m1842362874(NULL /*static, unused*/, (Delegate_t3310234105 *)L_2, (Delegate_t3310234105 *)((UnityAction_1_t4176246568 *)Castclass(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_1_t4176246568 *)Castclass(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m175705877_gshared (InvokableCall_1_t2137011826 * __this, UnityAction_1_t4176246568 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t1559630662 *)__this);
		BaseInvokableCall__ctor_m2839815662((BaseInvokableCall_t1559630662 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t4176246568 * L_0 = (UnityAction_1_t4176246568 *)__this->get_Delegate_0();
		UnityAction_1_t4176246568 * L_1 = ___action0;
		Delegate_t3310234105 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, (Delegate_t3310234105 *)L_0, (Delegate_t3310234105 *)L_1, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_1_t4176246568 *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3093451866;
extern const uint32_t InvokableCall_1_Invoke_m638694094_MetadataUsageId;
extern "C"  void InvokableCall_1_Invoke_m638694094_gshared (InvokableCall_1_t2137011826 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m638694094_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentException_t928607144 * L_1 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_1, (String_t*)_stringLiteral3093451866, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0014:
	{
		ObjectU5BU5D_t1108656482* L_2 = ___args0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		UnityAction_1_t4176246568 * L_5 = (UnityAction_1_t4176246568 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m2941661095(NULL /*static, unused*/, (Delegate_t3310234105 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		UnityAction_1_t4176246568 * L_7 = (UnityAction_1_t4176246568 *)__this->get_Delegate_0();
		ObjectU5BU5D_t1108656482* L_8 = ___args0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t4176246568 *)L_7);
		((  void (*) (UnityAction_1_t4176246568 *, Vector2_t4282066565 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((UnityAction_1_t4176246568 *)L_7, (Vector2_t4282066565 )((*(Vector2_t4282066565 *)((Vector2_t4282066565 *)UnBox (L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
	}

IL_003f:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m2276410782_gshared (InvokableCall_1_t2137011826 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t4176246568 * L_0 = (UnityAction_1_t4176246568 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3310234105 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2860483769((Delegate_t3310234105 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0024;
		}
	}
	{
		UnityAction_1_t4176246568 * L_3 = (UnityAction_1_t4176246568 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m1628849205(NULL /*static, unused*/, (Delegate_t3310234105 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0025;
	}

IL_0024:
	{
		G_B3_0 = 0;
	}

IL_0025:
	{
		return (bool)G_B3_0;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_2__ctor_m3928141520_MetadataUsageId;
extern "C"  void InvokableCall_2__ctor_m3928141520_gshared (InvokableCall_2_t3350606262 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2__ctor_m3928141520_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t1559630662 *)__this);
		BaseInvokableCall__ctor_m1692884045((BaseInvokableCall_t1559630662 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3310234105 * L_5 = NetFxCoreExtensions_CreateDelegate_m3408605866(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_2_t2640361832 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,System.Object>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3093451866;
extern const uint32_t InvokableCall_2_Invoke_m1749346151_MetadataUsageId;
extern "C"  void InvokableCall_2_Invoke_m1749346151_gshared (InvokableCall_2_t3350606262 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2_Invoke_m1749346151_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)2)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentException_t928607144 * L_1 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_1, (String_t*)_stringLiteral3093451866, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0014:
	{
		ObjectU5BU5D_t1108656482* L_2 = ___args0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		ObjectU5BU5D_t1108656482* L_5 = ___args0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		UnityAction_2_t2640361832 * L_8 = (UnityAction_2_t2640361832 *)__this->get_Delegate_0();
		bool L_9 = BaseInvokableCall_AllowInvoke_m2941661095(NULL /*static, unused*/, (Delegate_t3310234105 *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_004f;
		}
	}
	{
		UnityAction_2_t2640361832 * L_10 = (UnityAction_2_t2640361832 *)__this->get_Delegate_0();
		ObjectU5BU5D_t1108656482* L_11 = ___args0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		int32_t L_12 = 0;
		Il2CppObject * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		ObjectU5BU5D_t1108656482* L_14 = ___args0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 1);
		int32_t L_15 = 1;
		Il2CppObject * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck((UnityAction_2_t2640361832 *)L_10);
		((  void (*) (UnityAction_2_t2640361832 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_2_t2640361832 *)L_10, (Il2CppObject *)((Il2CppObject *)Castclass(L_13, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))), (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_004f:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,System.Object>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m3489973541_gshared (InvokableCall_2_t3350606262 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		UnityAction_2_t2640361832 * L_0 = (UnityAction_2_t2640361832 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3310234105 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2860483769((Delegate_t3310234105 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0024;
		}
	}
	{
		UnityAction_2_t2640361832 * L_3 = (UnityAction_2_t2640361832 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m1628849205(NULL /*static, unused*/, (Delegate_t3310234105 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0025;
	}

IL_0024:
	{
		G_B3_0 = 0;
	}

IL_0025:
	{
		return (bool)G_B3_0;
	}
}
// System.Void UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_3__ctor_m774681667_MetadataUsageId;
extern "C"  void InvokableCall_3__ctor_m774681667_gshared (InvokableCall_3_t3637709590 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_3__ctor_m774681667_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t1559630662 *)__this);
		BaseInvokableCall__ctor_m1692884045((BaseInvokableCall_t1559630662 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3310234105 * L_5 = NetFxCoreExtensions_CreateDelegate_m3408605866(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_3_t2271724636 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3093451866;
extern const uint32_t InvokableCall_3_Invoke_m585455258_MetadataUsageId;
extern "C"  void InvokableCall_3_Invoke_m585455258_gshared (InvokableCall_3_t3637709590 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_3_Invoke_m585455258_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)3)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentException_t928607144 * L_1 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_1, (String_t*)_stringLiteral3093451866, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0014:
	{
		ObjectU5BU5D_t1108656482* L_2 = ___args0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		ObjectU5BU5D_t1108656482* L_5 = ___args0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		ObjectU5BU5D_t1108656482* L_8 = ___args0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		int32_t L_9 = 2;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_3_t2271724636 * L_11 = (UnityAction_3_t2271724636 *)__this->get_Delegate_0();
		bool L_12 = BaseInvokableCall_AllowInvoke_m2941661095(NULL /*static, unused*/, (Delegate_t3310234105 *)L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_005f;
		}
	}
	{
		UnityAction_3_t2271724636 * L_13 = (UnityAction_3_t2271724636 *)__this->get_Delegate_0();
		ObjectU5BU5D_t1108656482* L_14 = ___args0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 0);
		int32_t L_15 = 0;
		Il2CppObject * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		ObjectU5BU5D_t1108656482* L_17 = ___args0;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 1);
		int32_t L_18 = 1;
		Il2CppObject * L_19 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		ObjectU5BU5D_t1108656482* L_20 = ___args0;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 2);
		int32_t L_21 = 2;
		Il2CppObject * L_22 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck((UnityAction_3_t2271724636 *)L_13);
		((  void (*) (UnityAction_3_t2271724636 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((UnityAction_3_t2271724636 *)L_13, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))), (Il2CppObject *)((Il2CppObject *)Castclass(L_19, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (Il2CppObject *)((Il2CppObject *)Castclass(L_22, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_005f:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_3_Find_m4267409362_gshared (InvokableCall_3_t3637709590 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		UnityAction_3_t2271724636 * L_0 = (UnityAction_3_t2271724636 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3310234105 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2860483769((Delegate_t3310234105 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0024;
		}
	}
	{
		UnityAction_3_t2271724636 * L_3 = (UnityAction_3_t2271724636 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m1628849205(NULL /*static, unused*/, (Delegate_t3310234105 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0025;
	}

IL_0024:
	{
		G_B3_0 = 0;
	}

IL_0025:
	{
		return (bool)G_B3_0;
	}
}
// System.Void UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_4__ctor_m4162891446_MetadataUsageId;
extern "C"  void InvokableCall_4__ctor_m4162891446_gshared (InvokableCall_4_t2281252720 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_4__ctor_m4162891446_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t1559630662 *)__this);
		BaseInvokableCall__ctor_m1692884045((BaseInvokableCall_t1559630662 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3310234105 * L_5 = NetFxCoreExtensions_CreateDelegate_m3408605866(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_4_t2496524882 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3093451866;
extern const uint32_t InvokableCall_4_Invoke_m2239232717_MetadataUsageId;
extern "C"  void InvokableCall_4_Invoke_m2239232717_gshared (InvokableCall_4_t2281252720 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_4_Invoke_m2239232717_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)4)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentException_t928607144 * L_1 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_1, (String_t*)_stringLiteral3093451866, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0014:
	{
		ObjectU5BU5D_t1108656482* L_2 = ___args0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		ObjectU5BU5D_t1108656482* L_5 = ___args0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		ObjectU5BU5D_t1108656482* L_8 = ___args0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		int32_t L_9 = 2;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		ObjectU5BU5D_t1108656482* L_11 = ___args0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		int32_t L_12 = 3;
		Il2CppObject * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		UnityAction_4_t2496524882 * L_14 = (UnityAction_4_t2496524882 *)__this->get_Delegate_0();
		bool L_15 = BaseInvokableCall_AllowInvoke_m2941661095(NULL /*static, unused*/, (Delegate_t3310234105 *)L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_006f;
		}
	}
	{
		UnityAction_4_t2496524882 * L_16 = (UnityAction_4_t2496524882 *)__this->get_Delegate_0();
		ObjectU5BU5D_t1108656482* L_17 = ___args0;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 0);
		int32_t L_18 = 0;
		Il2CppObject * L_19 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		ObjectU5BU5D_t1108656482* L_20 = ___args0;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 1);
		int32_t L_21 = 1;
		Il2CppObject * L_22 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		ObjectU5BU5D_t1108656482* L_23 = ___args0;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 2);
		int32_t L_24 = 2;
		Il2CppObject * L_25 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		ObjectU5BU5D_t1108656482* L_26 = ___args0;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 3);
		int32_t L_27 = 3;
		Il2CppObject * L_28 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck((UnityAction_4_t2496524882 *)L_16);
		((  void (*) (UnityAction_4_t2496524882 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((UnityAction_4_t2496524882 *)L_16, (Il2CppObject *)((Il2CppObject *)Castclass(L_19, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (Il2CppObject *)((Il2CppObject *)Castclass(L_22, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))), (Il2CppObject *)((Il2CppObject *)Castclass(L_25, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))), (Il2CppObject *)((Il2CppObject *)Castclass(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_006f:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_4_Find_m2015184255_gshared (InvokableCall_4_t2281252720 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		UnityAction_4_t2496524882 * L_0 = (UnityAction_4_t2496524882 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3310234105 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2860483769((Delegate_t3310234105 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0024;
		}
	}
	{
		UnityAction_4_t2496524882 * L_3 = (UnityAction_4_t2496524882 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m1628849205(NULL /*static, unused*/, (Delegate_t3310234105 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0025;
	}

IL_0024:
	{
		G_B3_0 = 0;
	}

IL_0025:
	{
		return (bool)G_B3_0;
	}
}
// System.Void UnityEngine.Events.UnityAction`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m1354516801_gshared (UnityAction_1_t370978721 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Boolean>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m2333712627_gshared (UnityAction_1_t370978721 * __this, bool ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m2333712627((UnityAction_1_t370978721 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Boolean>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m987196326_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m987196326_gshared (UnityAction_1_t370978721 * __this, bool ___arg00, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m987196326_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &___arg00);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m535265605_gshared (UnityAction_1_t370978721 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m3485915663_gshared (UnityAction_1_t1048018503 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m698809421_gshared (UnityAction_1_t1048018503 * __this, int32_t ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m698809421((UnityAction_1_t1048018503 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Int32>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m3753424384_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m3753424384_gshared (UnityAction_1_t1048018503 * __this, int32_t ___arg00, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m3753424384_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___arg00);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m3271535519_gshared (UnityAction_1_t1048018503 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m2698834494_gshared (UnityAction_1_t4064996374 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m774762876_gshared (UnityAction_1_t4064996374 * __this, Il2CppObject * ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m774762876((UnityAction_1_t4064996374 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Object>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m1225830823_gshared (UnityAction_1_t4064996374 * __this, Il2CppObject * ___arg00, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___arg00;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m4220465998_gshared (UnityAction_1_t4064996374 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m480548041_gshared (UnityAction_1_t4186098975 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Single>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m3075983123_gshared (UnityAction_1_t4186098975 * __this, float ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m3075983123((UnityAction_1_t4186098975 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, float ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, float ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Single>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m3950699582_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m3950699582_gshared (UnityAction_1_t4186098975 * __this, float ___arg00, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m3950699582_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Single_t4291918972_il2cpp_TypeInfo_var, &___arg00);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m146102117_gshared (UnityAction_1_t4186098975 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m3352102596_gshared (UnityAction_1_t4088726908 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Color>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m3753038862_gshared (UnityAction_1_t4088726908 * __this, Color_t4194546905  ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m3753038862((UnityAction_1_t4088726908 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Color_t4194546905  ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Color_t4194546905  ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Color>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern Il2CppClass* Color_t4194546905_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m3065986105_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m3065986105_gshared (UnityAction_1_t4088726908 * __this, Color_t4194546905  ___arg00, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m3065986105_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color_t4194546905_il2cpp_TypeInfo_var, &___arg00);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m4211808480_gshared (UnityAction_1_t4088726908 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m3402788068_gshared (UnityAction_1_t4176246568 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m2612362786_gshared (UnityAction_1_t4176246568 * __this, Vector2_t4282066565  ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m2612362786((UnityAction_1_t4176246568 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector2_t4282066565  ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector2_t4282066565  ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector2_t4282066565_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m3621024589_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m3621024589_gshared (UnityAction_1_t4176246568 * __this, Vector2_t4282066565  ___arg00, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m3621024589_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector2_t4282066565_il2cpp_TypeInfo_var, &___arg00);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m822604020_gshared (UnityAction_1_t4176246568 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m1077712043_gshared (UnityAction_2_t2640361832 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,System.Object>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m1848147712_gshared (UnityAction_2_t2640361832 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_2_Invoke_m1848147712((UnityAction_2_t2640361832 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<System.Object,System.Object>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_2_BeginInvoke_m1411513831_gshared (UnityAction_2_t2640361832 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = ___arg11;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m2463306811_gshared (UnityAction_2_t2640361832 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_3__ctor_m2630406680_gshared (UnityAction_3_t2271724636 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>::Invoke(T0,T1,T2)
extern "C"  void UnityAction_3_Invoke_m743272021_gshared (UnityAction_3_t2271724636 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_3_Invoke_m743272021((UnityAction_3_t2271724636 *)__this->get_prev_9(),___arg00, ___arg11, ___arg22, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11, ___arg22,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11, ___arg22,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg11, Il2CppObject * ___arg22, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11, ___arg22,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>::BeginInvoke(T0,T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_3_BeginInvoke_m3462825058_gshared (UnityAction_3_t2271724636 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	void *__d_args[4] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = ___arg11;
	__d_args[2] = ___arg22;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_3_EndInvoke_m2479427624_gshared (UnityAction_3_t2271724636 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_4__ctor_m2430556805_gshared (UnityAction_4_t2496524882 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>::Invoke(T0,T1,T2,T3)
extern "C"  void UnityAction_4_Invoke_m295067525_gshared (UnityAction_4_t2496524882 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, Il2CppObject * ___arg33, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_4_Invoke_m295067525((UnityAction_4_t2496524882 *)__this->get_prev_9(),___arg00, ___arg11, ___arg22, ___arg33, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, Il2CppObject * ___arg33, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11, ___arg22, ___arg33,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, Il2CppObject * ___arg33, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11, ___arg22, ___arg33,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg11, Il2CppObject * ___arg22, Il2CppObject * ___arg33, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11, ___arg22, ___arg33,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T0,T1,T2,T3,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_4_BeginInvoke_m3784231950_gshared (UnityAction_4_t2496524882 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, Il2CppObject * ___arg33, AsyncCallback_t1369114871 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	void *__d_args[5] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = ___arg11;
	__d_args[2] = ___arg22;
	__d_args[3] = ___arg33;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// System.Void UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_4_EndInvoke_m2334578453_gshared (UnityAction_4_t2496524882 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Boolean>::.ctor()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1__ctor_m1579102881_MetadataUsageId;
extern "C"  void UnityEvent_1__ctor_m1579102881_gshared (UnityEvent_1_t663396231 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1__ctor_m1579102881_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1)));
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase__ctor_m199506446((UnityEventBase_t1020378628 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Boolean>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m4118700355_gshared (UnityEvent_1_t663396231 * __this, UnityAction_1_t370978721 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t370978721 * L_0 = ___call0;
		BaseInvokableCall_t1559630662 * L_1 = ((  BaseInvokableCall_t1559630662 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t370978721 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (UnityAction_1_t370978721 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase_AddCall_m1246572149((UnityEventBase_t1020378628 *)__this, (BaseInvokableCall_t1559630662 *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Boolean>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_RemoveListener_m3796628131_gshared (UnityEvent_1_t663396231 * __this, UnityAction_1_t370978721 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t370978721 * L_0 = ___call0;
		NullCheck((Delegate_t3310234105 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2860483769((Delegate_t3310234105 *)L_0, /*hidden argument*/NULL);
		UnityAction_1_t370978721 * L_2 = ___call0;
		MethodInfo_t * L_3 = NetFxCoreExtensions_GetMethodInfo_m1628849205(NULL /*static, unused*/, (Delegate_t3310234105 *)L_2, /*hidden argument*/NULL);
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase_RemoveListener_m276725997((UnityEventBase_t1020378628 *)__this, (Il2CppObject *)L_1, (MethodInfo_t *)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<System.Boolean>::FindMethod_Impl(System.String,System.Object)
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1_FindMethod_Impl_m2991727964_MetadataUsageId;
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m2991727964_gshared (UnityEvent_1_t663396231 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1_FindMethod_Impl_m2991727964_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t3339007067* L_2 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		MethodInfo_t * L_4 = UnityEventBase_GetValidMethodInfo_m2325301202(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t3339007067*)L_2, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Boolean>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t1559630662 * UnityEvent_1_GetDelegate_m183928870_gshared (UnityEvent_1_t663396231 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_1_t2626711275 * L_2 = (InvokableCall_1_t2626711275 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t2626711275 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Boolean>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  BaseInvokableCall_t1559630662 * UnityEvent_1_GetDelegate_m2888043331_gshared (Il2CppObject * __this /* static, unused */, UnityAction_1_t370978721 * ___action0, const MethodInfo* method)
{
	{
		UnityAction_1_t370978721 * L_0 = ___action0;
		InvokableCall_1_t2626711275 * L_1 = (InvokableCall_1_t2626711275 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t2626711275 *, UnityAction_1_t370978721 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (UnityAction_1_t370978721 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Boolean>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m4200629676_gshared (UnityEvent_1_t663396231 * __this, bool ___arg00, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_m_InvokeArray_4();
		bool L_1 = ___arg00;
		bool L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = (ObjectU5BU5D_t1108656482*)__this->get_m_InvokeArray_4();
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase_Invoke_m3681078084((UnityEventBase_t1020378628 *)__this, (ObjectU5BU5D_t1108656482*)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::.ctor()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1__ctor_m7858823_MetadataUsageId;
extern "C"  void UnityEvent_1__ctor_m7858823_gshared (UnityEvent_1_t1340436013 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1__ctor_m7858823_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1)));
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase__ctor_m199506446((UnityEventBase_t1020378628 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m482909706_gshared (UnityEvent_1_t1340436013 * __this, UnityAction_1_t1048018503 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t1048018503 * L_0 = ___call0;
		BaseInvokableCall_t1559630662 * L_1 = ((  BaseInvokableCall_t1559630662 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t1048018503 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (UnityAction_1_t1048018503 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase_AddCall_m1246572149((UnityEventBase_t1020378628 *)__this, (BaseInvokableCall_t1559630662 *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_RemoveListener_m356723581_gshared (UnityEvent_1_t1340436013 * __this, UnityAction_1_t1048018503 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t1048018503 * L_0 = ___call0;
		NullCheck((Delegate_t3310234105 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2860483769((Delegate_t3310234105 *)L_0, /*hidden argument*/NULL);
		UnityAction_1_t1048018503 * L_2 = ___call0;
		MethodInfo_t * L_3 = NetFxCoreExtensions_GetMethodInfo_m1628849205(NULL /*static, unused*/, (Delegate_t3310234105 *)L_2, /*hidden argument*/NULL);
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase_RemoveListener_m276725997((UnityEventBase_t1020378628 *)__this, (Il2CppObject *)L_1, (MethodInfo_t *)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<System.Int32>::FindMethod_Impl(System.String,System.Object)
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1_FindMethod_Impl_m2189312310_MetadataUsageId;
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m2189312310_gshared (UnityEvent_1_t1340436013 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1_FindMethod_Impl_m2189312310_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t3339007067* L_2 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		MethodInfo_t * L_4 = UnityEventBase_GetValidMethodInfo_m2325301202(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t3339007067*)L_2, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Int32>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t1559630662 * UnityEvent_1_GetDelegate_m3062958924_gshared (UnityEvent_1_t1340436013 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_1_t3303751057 * L_2 = (InvokableCall_1_t3303751057 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t3303751057 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Int32>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  BaseInvokableCall_t1559630662 * UnityEvent_1_GetDelegate_m2987483881_gshared (Il2CppObject * __this /* static, unused */, UnityAction_1_t1048018503 * ___action0, const MethodInfo* method)
{
	{
		UnityAction_1_t1048018503 * L_0 = ___action0;
		InvokableCall_1_t3303751057 * L_1 = (InvokableCall_1_t3303751057 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t3303751057 *, UnityAction_1_t1048018503 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (UnityAction_1_t1048018503 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m1822489606_gshared (UnityEvent_1_t1340436013 * __this, int32_t ___arg00, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_m_InvokeArray_4();
		int32_t L_1 = ___arg00;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = (ObjectU5BU5D_t1108656482*)__this->get_m_InvokeArray_4();
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase_Invoke_m3681078084((UnityEventBase_t1020378628 *)__this, (ObjectU5BU5D_t1108656482*)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::.ctor()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1__ctor_m4139691420_MetadataUsageId;
extern "C"  void UnityEvent_1__ctor_m4139691420_gshared (UnityEvent_1_t62446588 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1__ctor_m4139691420_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1)));
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase__ctor_m199506446((UnityEventBase_t1020378628 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m1298407787_gshared (UnityEvent_1_t62446588 * __this, UnityAction_1_t4064996374 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t4064996374 * L_0 = ___call0;
		BaseInvokableCall_t1559630662 * L_1 = ((  BaseInvokableCall_t1559630662 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t4064996374 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (UnityAction_1_t4064996374 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase_AddCall_m1246572149((UnityEventBase_t1020378628 *)__this, (BaseInvokableCall_t1559630662 *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_RemoveListener_m2525028476_gshared (UnityEvent_1_t62446588 * __this, UnityAction_1_t4064996374 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t4064996374 * L_0 = ___call0;
		NullCheck((Delegate_t3310234105 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2860483769((Delegate_t3310234105 *)L_0, /*hidden argument*/NULL);
		UnityAction_1_t4064996374 * L_2 = ___call0;
		MethodInfo_t * L_3 = NetFxCoreExtensions_GetMethodInfo_m1628849205(NULL /*static, unused*/, (Delegate_t3310234105 *)L_2, /*hidden argument*/NULL);
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase_RemoveListener_m276725997((UnityEventBase_t1020378628 *)__this, (Il2CppObject *)L_1, (MethodInfo_t *)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<System.Object>::FindMethod_Impl(System.String,System.Object)
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1_FindMethod_Impl_m936319469_MetadataUsageId;
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m936319469_gshared (UnityEvent_1_t62446588 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1_FindMethod_Impl_m936319469_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t3339007067* L_2 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		MethodInfo_t * L_4 = UnityEventBase_GetValidMethodInfo_m2325301202(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t3339007067*)L_2, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t1559630662 * UnityEvent_1_GetDelegate_m882504923_gshared (UnityEvent_1_t62446588 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_1_t2025761632 * L_2 = (InvokableCall_1_t2025761632 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t2025761632 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Object>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  BaseInvokableCall_t1559630662 * UnityEvent_1_GetDelegate_m2100323256_gshared (Il2CppObject * __this /* static, unused */, UnityAction_1_t4064996374 * ___action0, const MethodInfo* method)
{
	{
		UnityAction_1_t4064996374 * L_0 = ___action0;
		InvokableCall_1_t2025761632 * L_1 = (InvokableCall_1_t2025761632 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t2025761632 *, UnityAction_1_t4064996374 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (UnityAction_1_t4064996374 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m3779699780_gshared (UnityEvent_1_t62446588 * __this, Il2CppObject * ___arg00, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_m_InvokeArray_4();
		Il2CppObject * L_1 = ___arg00;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		ObjectU5BU5D_t1108656482* L_2 = (ObjectU5BU5D_t1108656482*)__this->get_m_InvokeArray_4();
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase_Invoke_m3681078084((UnityEventBase_t1020378628 *)__this, (ObjectU5BU5D_t1108656482*)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::.ctor()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1__ctor_m1354608473_MetadataUsageId;
extern "C"  void UnityEvent_1__ctor_m1354608473_gshared (UnityEvent_1_t183549189 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1__ctor_m1354608473_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1)));
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase__ctor_m199506446((UnityEventBase_t1020378628 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m994670587_gshared (UnityEvent_1_t183549189 * __this, UnityAction_1_t4186098975 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t4186098975 * L_0 = ___call0;
		BaseInvokableCall_t1559630662 * L_1 = ((  BaseInvokableCall_t1559630662 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t4186098975 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (UnityAction_1_t4186098975 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase_AddCall_m1246572149((UnityEventBase_t1020378628 *)__this, (BaseInvokableCall_t1559630662 *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_RemoveListener_m1402724082_gshared (UnityEvent_1_t183549189 * __this, UnityAction_1_t4186098975 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t4186098975 * L_0 = ___call0;
		NullCheck((Delegate_t3310234105 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2860483769((Delegate_t3310234105 *)L_0, /*hidden argument*/NULL);
		UnityAction_1_t4186098975 * L_2 = ___call0;
		MethodInfo_t * L_3 = NetFxCoreExtensions_GetMethodInfo_m1628849205(NULL /*static, unused*/, (Delegate_t3310234105 *)L_2, /*hidden argument*/NULL);
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase_RemoveListener_m276725997((UnityEventBase_t1020378628 *)__this, (Il2CppObject *)L_1, (MethodInfo_t *)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<System.Single>::FindMethod_Impl(System.String,System.Object)
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1_FindMethod_Impl_m941410756_MetadataUsageId;
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m941410756_gshared (UnityEvent_1_t183549189 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1_FindMethod_Impl_m941410756_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t3339007067* L_2 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		MethodInfo_t * L_4 = UnityEventBase_GetValidMethodInfo_m2325301202(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t3339007067*)L_2, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Single>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t1559630662 * UnityEvent_1_GetDelegate_m3630876836_gshared (UnityEvent_1_t183549189 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_1_t2146864233 * L_2 = (InvokableCall_1_t2146864233 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t2146864233 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Single>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  BaseInvokableCall_t1559630662 * UnityEvent_1_GetDelegate_m3019506241_gshared (Il2CppObject * __this /* static, unused */, UnityAction_1_t4186098975 * ___action0, const MethodInfo* method)
{
	{
		UnityAction_1_t4186098975 * L_0 = ___action0;
		InvokableCall_1_t2146864233 * L_1 = (InvokableCall_1_t2146864233 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t2146864233 *, UnityAction_1_t4186098975 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (UnityAction_1_t4186098975 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m3551800820_gshared (UnityEvent_1_t183549189 * __this, float ___arg00, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_m_InvokeArray_4();
		float L_1 = ___arg00;
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = (ObjectU5BU5D_t1108656482*)__this->get_m_InvokeArray_4();
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase_Invoke_m3681078084((UnityEventBase_t1020378628 *)__this, (ObjectU5BU5D_t1108656482*)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::.ctor()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1__ctor_m1130251838_MetadataUsageId;
extern "C"  void UnityEvent_1__ctor_m1130251838_gshared (UnityEvent_1_t86177122 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1__ctor_m1130251838_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1)));
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase__ctor_m199506446((UnityEventBase_t1020378628 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m1347854496_gshared (UnityEvent_1_t86177122 * __this, UnityAction_1_t4088726908 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t4088726908 * L_0 = ___call0;
		BaseInvokableCall_t1559630662 * L_1 = ((  BaseInvokableCall_t1559630662 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t4088726908 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (UnityAction_1_t4088726908 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase_AddCall_m1246572149((UnityEventBase_t1020378628 *)__this, (BaseInvokableCall_t1559630662 *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_RemoveListener_m2625816718_gshared (UnityEvent_1_t86177122 * __this, UnityAction_1_t4088726908 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t4088726908 * L_0 = ___call0;
		NullCheck((Delegate_t3310234105 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2860483769((Delegate_t3310234105 *)L_0, /*hidden argument*/NULL);
		UnityAction_1_t4088726908 * L_2 = ___call0;
		MethodInfo_t * L_3 = NetFxCoreExtensions_GetMethodInfo_m1628849205(NULL /*static, unused*/, (Delegate_t3310234105 *)L_2, /*hidden argument*/NULL);
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase_RemoveListener_m276725997((UnityEventBase_t1020378628 *)__this, (Il2CppObject *)L_1, (MethodInfo_t *)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::FindMethod_Impl(System.String,System.Object)
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1_FindMethod_Impl_m382043647_MetadataUsageId;
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m382043647_gshared (UnityEvent_1_t86177122 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1_FindMethod_Impl_m382043647_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t3339007067* L_2 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		MethodInfo_t * L_4 = UnityEventBase_GetValidMethodInfo_m2325301202(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t3339007067*)L_2, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t1559630662 * UnityEvent_1_GetDelegate_m441737225_gshared (UnityEvent_1_t86177122 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_1_t2049492166 * L_2 = (InvokableCall_1_t2049492166 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t2049492166 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  BaseInvokableCall_t1559630662 * UnityEvent_1_GetDelegate_m758008166_gshared (Il2CppObject * __this /* static, unused */, UnityAction_1_t4088726908 * ___action0, const MethodInfo* method)
{
	{
		UnityAction_1_t4088726908 * L_0 = ___action0;
		InvokableCall_1_t2049492166 * L_1 = (InvokableCall_1_t2049492166 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t2049492166 *, UnityAction_1_t4088726908 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (UnityAction_1_t4088726908 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m2712400111_gshared (UnityEvent_1_t86177122 * __this, Color_t4194546905  ___arg00, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_m_InvokeArray_4();
		Color_t4194546905  L_1 = ___arg00;
		Color_t4194546905  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = (ObjectU5BU5D_t1108656482*)__this->get_m_InvokeArray_4();
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase_Invoke_m3681078084((UnityEventBase_t1020378628 *)__this, (ObjectU5BU5D_t1108656482*)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::.ctor()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1__ctor_m99457450_MetadataUsageId;
extern "C"  void UnityEvent_1__ctor_m99457450_gshared (UnityEvent_1_t173696782 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1__ctor_m99457450_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1)));
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase__ctor_m199506446((UnityEventBase_t1020378628 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m274807813_gshared (UnityEvent_1_t173696782 * __this, UnityAction_1_t4176246568 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t4176246568 * L_0 = ___call0;
		BaseInvokableCall_t1559630662 * L_1 = ((  BaseInvokableCall_t1559630662 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t4176246568 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (UnityAction_1_t4176246568 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase_AddCall_m1246572149((UnityEventBase_t1020378628 *)__this, (BaseInvokableCall_t1559630662 *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_RemoveListener_m2726004642_gshared (UnityEvent_1_t173696782 * __this, UnityAction_1_t4176246568 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t4176246568 * L_0 = ___call0;
		NullCheck((Delegate_t3310234105 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2860483769((Delegate_t3310234105 *)L_0, /*hidden argument*/NULL);
		UnityAction_1_t4176246568 * L_2 = ___call0;
		MethodInfo_t * L_3 = NetFxCoreExtensions_GetMethodInfo_m1628849205(NULL /*static, unused*/, (Delegate_t3310234105 *)L_2, /*hidden argument*/NULL);
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase_RemoveListener_m276725997((UnityEventBase_t1020378628 *)__this, (Il2CppObject *)L_1, (MethodInfo_t *)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::FindMethod_Impl(System.String,System.Object)
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1_FindMethod_Impl_m3931660819_MetadataUsageId;
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m3931660819_gshared (UnityEvent_1_t173696782 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1_FindMethod_Impl_m3931660819_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t3339007067* L_2 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		MethodInfo_t * L_4 = UnityEventBase_GetValidMethodInfo_m2325301202(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t3339007067*)L_2, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t1559630662 * UnityEvent_1_GetDelegate_m1046909621_gshared (UnityEvent_1_t173696782 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_1_t2137011826 * L_2 = (InvokableCall_1_t2137011826 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t2137011826 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  BaseInvokableCall_t1559630662 * UnityEvent_1_GetDelegate_m2362895634_gshared (Il2CppObject * __this /* static, unused */, UnityAction_1_t4176246568 * ___action0, const MethodInfo* method)
{
	{
		UnityAction_1_t4176246568 * L_0 = ___action0;
		InvokableCall_1_t2137011826 * L_1 = (InvokableCall_1_t2137011826 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t2137011826 *, UnityAction_1_t4176246568 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (UnityAction_1_t4176246568 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m3332953603_gshared (UnityEvent_1_t173696782 * __this, Vector2_t4282066565  ___arg00, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_m_InvokeArray_4();
		Vector2_t4282066565  L_1 = ___arg00;
		Vector2_t4282066565  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = (ObjectU5BU5D_t1108656482*)__this->get_m_InvokeArray_4();
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase_Invoke_m3681078084((UnityEventBase_t1020378628 *)__this, (ObjectU5BU5D_t1108656482*)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::.ctor()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_2__ctor_m1950601551_MetadataUsageId;
extern "C"  void UnityEvent_2__ctor_m1950601551_gshared (UnityEvent_2_t2262335274 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_2__ctor_m1950601551_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)2)));
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase__ctor_m199506446((UnityEventBase_t1020378628 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::FindMethod_Impl(System.String,System.Object)
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_2_FindMethod_Impl_m3911325978_MetadataUsageId;
extern "C"  MethodInfo_t * UnityEvent_2_FindMethod_Impl_m3911325978_gshared (UnityEvent_2_t2262335274 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_2_FindMethod_Impl_m3911325978_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t3339007067* L_2 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)2));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)L_2;
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_5);
		MethodInfo_t * L_6 = UnityEventBase_GetValidMethodInfo_m2325301202(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t3339007067*)L_4, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t1559630662 * UnityEvent_2_GetDelegate_m3055054414_gshared (UnityEvent_2_t2262335274 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_2_t3350606262 * L_2 = (InvokableCall_2_t3350606262 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (InvokableCall_2_t3350606262 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>::.ctor()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_3__ctor_m4248091138_MetadataUsageId;
extern "C"  void UnityEvent_3__ctor_m4248091138_gshared (UnityEvent_3_t1668724050 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_3__ctor_m4248091138_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)3)));
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase__ctor_m199506446((UnityEventBase_t1020378628 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>::FindMethod_Impl(System.String,System.Object)
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_3_FindMethod_Impl_m859190087_MetadataUsageId;
extern "C"  MethodInfo_t * UnityEvent_3_FindMethod_Impl_m859190087_gshared (UnityEvent_3_t1668724050 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_3_FindMethod_Impl_m859190087_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t3339007067* L_2 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)3));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)L_2;
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_5);
		TypeU5BU5D_t3339007067* L_6 = (TypeU5BU5D_t3339007067*)L_4;
		Type_t * L_7 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_7);
		MethodInfo_t * L_8 = UnityEventBase_GetValidMethodInfo_m2325301202(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t3339007067*)L_6, /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t1559630662 * UnityEvent_3_GetDelegate_m3227786433_gshared (UnityEvent_3_t1668724050 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_3_t3637709590 * L_2 = (InvokableCall_3_t3637709590 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_3_t3637709590 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// System.Void UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>::.ctor()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_4__ctor_m492943285_MetadataUsageId;
extern "C"  void UnityEvent_4__ctor_m492943285_gshared (UnityEvent_4_t2186133700 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_4__ctor_m492943285_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4)));
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase__ctor_m199506446((UnityEventBase_t1020378628 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>::FindMethod_Impl(System.String,System.Object)
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_4_FindMethod_Impl_m1344084084_MetadataUsageId;
extern "C"  MethodInfo_t * UnityEvent_4_FindMethod_Impl_m1344084084_gshared (UnityEvent_4_t2186133700 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_4_FindMethod_Impl_m1344084084_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t3339007067* L_2 = (TypeU5BU5D_t3339007067*)((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)4));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		TypeU5BU5D_t3339007067* L_4 = (TypeU5BU5D_t3339007067*)L_2;
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_5);
		TypeU5BU5D_t3339007067* L_6 = (TypeU5BU5D_t3339007067*)L_4;
		Type_t * L_7 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_7);
		TypeU5BU5D_t3339007067* L_8 = (TypeU5BU5D_t3339007067*)L_6;
		Type_t * L_9 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 3)), /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 3);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(3), (Type_t *)L_9);
		MethodInfo_t * L_10 = UnityEventBase_GetValidMethodInfo_m2325301202(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t3339007067*)L_8, /*hidden argument*/NULL);
		return L_10;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t1559630662 * UnityEvent_4_GetDelegate_m1269387316_gshared (UnityEvent_4_t2186133700 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_4_t2281252720 * L_2 = (InvokableCall_4_t2281252720 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (InvokableCall_4_t2281252720 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_2;
	}
}
// System.Void UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void EventFunction_1__ctor_m252996987_gshared (EventFunction_1_t864200549 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>::Invoke(T1,UnityEngine.EventSystems.BaseEventData)
extern "C"  void EventFunction_1_Invoke_m1750245256_gshared (EventFunction_1_t864200549 * __this, Il2CppObject * ___handler0, BaseEventData_t2054899105 * ___eventData1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		EventFunction_1_Invoke_m1750245256((EventFunction_1_t864200549 *)__this->get_prev_9(),___handler0, ___eventData1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___handler0, BaseEventData_t2054899105 * ___eventData1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___handler0, ___eventData1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___handler0, BaseEventData_t2054899105 * ___eventData1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___handler0, ___eventData1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, BaseEventData_t2054899105 * ___eventData1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___handler0, ___eventData1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>::BeginInvoke(T1,UnityEngine.EventSystems.BaseEventData,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * EventFunction_1_BeginInvoke_m2337669695_gshared (EventFunction_1_t864200549 * __this, Il2CppObject * ___handler0, BaseEventData_t2054899105 * ___eventData1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___handler0;
	__d_args[1] = ___eventData1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void EventFunction_1_EndInvoke_m1184355595_gshared (EventFunction_1_t864200549 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::.ctor()
extern "C"  void IndexedSet_1__ctor_m1049489274_gshared (IndexedSet_1_t2695948348 * __this, const MethodInfo* method)
{
	{
		List_1_t1244034627 * L_0 = (List_1_t1244034627 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (List_1_t1244034627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_m_List_0(L_0);
		Dictionary_2_t3323877696 * L_1 = (Dictionary_2_t3323877696 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (Dictionary_2_t3323877696 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		__this->set_m_Dictionary_1(L_1);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m1636511825_gshared (IndexedSet_1_t2695948348 * __this, const MethodInfo* method)
{
	{
		NullCheck((IndexedSet_1_t2695948348 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (IndexedSet_1_t2695948348 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((IndexedSet_1_t2695948348 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_0;
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Add(T)
extern "C"  void IndexedSet_1_Add_m162544069_gshared (IndexedSet_1_t2695948348 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		List_1_t1244034627 * L_0 = (List_1_t1244034627 *)__this->get_m_List_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((List_1_t1244034627 *)L_0);
		((  void (*) (List_1_t1244034627 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t1244034627 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3323877696 * L_2 = (Dictionary_2_t3323877696 *)__this->get_m_Dictionary_1();
		Il2CppObject * L_3 = ___item0;
		List_1_t1244034627 * L_4 = (List_1_t1244034627 *)__this->get_m_List_0();
		NullCheck((List_1_t1244034627 *)L_4);
		int32_t L_5 = ((  int32_t (*) (List_1_t1244034627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t1244034627 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((Dictionary_2_t3323877696 *)L_2);
		((  void (*) (Dictionary_2_t3323877696 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t3323877696 *)L_2, (Il2CppObject *)L_3, (int32_t)((int32_t)((int32_t)L_5-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return;
	}
}
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::AddUnique(T)
extern "C"  bool IndexedSet_1_AddUnique_m1373796738_gshared (IndexedSet_1_t2695948348 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t3323877696 * L_0 = (Dictionary_2_t3323877696 *)__this->get_m_Dictionary_1();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t3323877696 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t3323877696 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Dictionary_2_t3323877696 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		return (bool)0;
	}

IL_0013:
	{
		List_1_t1244034627 * L_3 = (List_1_t1244034627 *)__this->get_m_List_0();
		Il2CppObject * L_4 = ___item0;
		NullCheck((List_1_t1244034627 *)L_3);
		((  void (*) (List_1_t1244034627 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t1244034627 *)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3323877696 * L_5 = (Dictionary_2_t3323877696 *)__this->get_m_Dictionary_1();
		Il2CppObject * L_6 = ___item0;
		List_1_t1244034627 * L_7 = (List_1_t1244034627 *)__this->get_m_List_0();
		NullCheck((List_1_t1244034627 *)L_7);
		int32_t L_8 = ((  int32_t (*) (List_1_t1244034627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t1244034627 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((Dictionary_2_t3323877696 *)L_5);
		((  void (*) (Dictionary_2_t3323877696 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t3323877696 *)L_5, (Il2CppObject *)L_6, (int32_t)((int32_t)((int32_t)L_8-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return (bool)1;
	}
}
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Remove(T)
extern "C"  bool IndexedSet_1_Remove_m2459681474_gshared (IndexedSet_1_t2695948348 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)(-1);
		Dictionary_2_t3323877696 * L_0 = (Dictionary_2_t3323877696 *)__this->get_m_Dictionary_1();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t3323877696 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t3323877696 *, Il2CppObject *, int32_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t3323877696 *)L_0, (Il2CppObject *)L_1, (int32_t*)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		int32_t L_3 = V_0;
		NullCheck((IndexedSet_1_t2695948348 *)__this);
		((  void (*) (IndexedSet_1_t2695948348 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((IndexedSet_1_t2695948348 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return (bool)1;
	}
}
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<System.Object>::GetEnumerator()
extern Il2CppClass* NotImplementedException_t1912495542_il2cpp_TypeInfo_var;
extern const uint32_t IndexedSet_1_GetEnumerator_m2607290422_MetadataUsageId;
extern "C"  Il2CppObject* IndexedSet_1_GetEnumerator_m2607290422_gshared (IndexedSet_1_t2695948348 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IndexedSet_1_GetEnumerator_m2607290422_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1912495542 * L_0 = (NotImplementedException_t1912495542 *)il2cpp_codegen_object_new(NotImplementedException_t1912495542_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m2063223793(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Clear()
extern "C"  void IndexedSet_1_Clear_m2750589861_gshared (IndexedSet_1_t2695948348 * __this, const MethodInfo* method)
{
	{
		List_1_t1244034627 * L_0 = (List_1_t1244034627 *)__this->get_m_List_0();
		NullCheck((List_1_t1244034627 *)L_0);
		((  void (*) (List_1_t1244034627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t1244034627 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		Dictionary_2_t3323877696 * L_1 = (Dictionary_2_t3323877696 *)__this->get_m_Dictionary_1();
		NullCheck((Dictionary_2_t3323877696 *)L_1);
		((  void (*) (Dictionary_2_t3323877696 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t3323877696 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return;
	}
}
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Contains(T)
extern "C"  bool IndexedSet_1_Contains_m2075225351_gshared (IndexedSet_1_t2695948348 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t3323877696 * L_0 = (Dictionary_2_t3323877696 *)__this->get_m_Dictionary_1();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t3323877696 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t3323877696 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Dictionary_2_t3323877696 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_2;
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void IndexedSet_1_CopyTo_m4273412725_gshared (IndexedSet_1_t2695948348 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		List_1_t1244034627 * L_0 = (List_1_t1244034627 *)__this->get_m_List_0();
		ObjectU5BU5D_t1108656482* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		NullCheck((List_1_t1244034627 *)L_0);
		((  void (*) (List_1_t1244034627 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t1244034627 *)L_0, (ObjectU5BU5D_t1108656482*)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return;
	}
}
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_Count()
extern "C"  int32_t IndexedSet_1_get_Count_m3523122326_gshared (IndexedSet_1_t2695948348 * __this, const MethodInfo* method)
{
	{
		List_1_t1244034627 * L_0 = (List_1_t1244034627 *)__this->get_m_List_0();
		NullCheck((List_1_t1244034627 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t1244034627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t1244034627 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_1;
	}
}
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_IsReadOnly()
extern "C"  bool IndexedSet_1_get_IsReadOnly_m581024909_gshared (IndexedSet_1_t2695948348 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<System.Object>::IndexOf(T)
extern "C"  int32_t IndexedSet_1_IndexOf_m4137415045_gshared (IndexedSet_1_t2695948348 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)(-1);
		Dictionary_2_t3323877696 * L_0 = (Dictionary_2_t3323877696 *)__this->get_m_Dictionary_1();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t3323877696 *)L_0);
		((  bool (*) (Dictionary_2_t3323877696 *, Il2CppObject *, int32_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t3323877696 *)L_0, (Il2CppObject *)L_1, (int32_t*)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Insert(System.Int32,T)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1342531814;
extern const uint32_t IndexedSet_1_Insert_m3436399148_MetadataUsageId;
extern "C"  void IndexedSet_1_Insert_m3436399148_gshared (IndexedSet_1_t2695948348 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IndexedSet_1_Insert_m3436399148_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, (String_t*)_stringLiteral1342531814, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::RemoveAt(System.Int32)
extern "C"  void IndexedSet_1_RemoveAt_m1310252018_gshared (IndexedSet_1_t2695948348 * __this, int32_t ___index0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	Il2CppObject * V_2 = NULL;
	{
		List_1_t1244034627 * L_0 = (List_1_t1244034627 *)__this->get_m_List_0();
		int32_t L_1 = ___index0;
		NullCheck((List_1_t1244034627 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (List_1_t1244034627 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t1244034627 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_0 = (Il2CppObject *)L_2;
		Dictionary_2_t3323877696 * L_3 = (Dictionary_2_t3323877696 *)__this->get_m_Dictionary_1();
		Il2CppObject * L_4 = V_0;
		NullCheck((Dictionary_2_t3323877696 *)L_3);
		((  bool (*) (Dictionary_2_t3323877696 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((Dictionary_2_t3323877696 *)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		int32_t L_5 = ___index0;
		List_1_t1244034627 * L_6 = (List_1_t1244034627 *)__this->get_m_List_0();
		NullCheck((List_1_t1244034627 *)L_6);
		int32_t L_7 = ((  int32_t (*) (List_1_t1244034627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t1244034627 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)((int32_t)L_7-(int32_t)1))))))
		{
			goto IL_003e;
		}
	}
	{
		List_1_t1244034627 * L_8 = (List_1_t1244034627 *)__this->get_m_List_0();
		int32_t L_9 = ___index0;
		NullCheck((List_1_t1244034627 *)L_8);
		((  void (*) (List_1_t1244034627 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((List_1_t1244034627 *)L_8, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		goto IL_007f;
	}

IL_003e:
	{
		List_1_t1244034627 * L_10 = (List_1_t1244034627 *)__this->get_m_List_0();
		NullCheck((List_1_t1244034627 *)L_10);
		int32_t L_11 = ((  int32_t (*) (List_1_t1244034627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t1244034627 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		V_1 = (int32_t)((int32_t)((int32_t)L_11-(int32_t)1));
		List_1_t1244034627 * L_12 = (List_1_t1244034627 *)__this->get_m_List_0();
		int32_t L_13 = V_1;
		NullCheck((List_1_t1244034627 *)L_12);
		Il2CppObject * L_14 = ((  Il2CppObject * (*) (List_1_t1244034627 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t1244034627 *)L_12, (int32_t)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_2 = (Il2CppObject *)L_14;
		List_1_t1244034627 * L_15 = (List_1_t1244034627 *)__this->get_m_List_0();
		int32_t L_16 = ___index0;
		Il2CppObject * L_17 = V_2;
		NullCheck((List_1_t1244034627 *)L_15);
		((  void (*) (List_1_t1244034627 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t1244034627 *)L_15, (int32_t)L_16, (Il2CppObject *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		Dictionary_2_t3323877696 * L_18 = (Dictionary_2_t3323877696 *)__this->get_m_Dictionary_1();
		Il2CppObject * L_19 = V_2;
		int32_t L_20 = ___index0;
		NullCheck((Dictionary_2_t3323877696 *)L_18);
		((  void (*) (Dictionary_2_t3323877696 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((Dictionary_2_t3323877696 *)L_18, (Il2CppObject *)L_19, (int32_t)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		List_1_t1244034627 * L_21 = (List_1_t1244034627 *)__this->get_m_List_0();
		int32_t L_22 = V_1;
		NullCheck((List_1_t1244034627 *)L_21);
		((  void (*) (List_1_t1244034627 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((List_1_t1244034627 *)L_21, (int32_t)L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
	}

IL_007f:
	{
		return;
	}
}
// T UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * IndexedSet_1_get_Item_m2125208246_gshared (IndexedSet_1_t2695948348 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		List_1_t1244034627 * L_0 = (List_1_t1244034627 *)__this->get_m_List_0();
		int32_t L_1 = ___index0;
		NullCheck((List_1_t1244034627 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (List_1_t1244034627 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t1244034627 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		return L_2;
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::set_Item(System.Int32,T)
extern "C"  void IndexedSet_1_set_Item_m1359327235_gshared (IndexedSet_1_t2695948348 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		List_1_t1244034627 * L_0 = (List_1_t1244034627 *)__this->get_m_List_0();
		int32_t L_1 = ___index0;
		NullCheck((List_1_t1244034627 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (List_1_t1244034627 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t1244034627 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_0 = (Il2CppObject *)L_2;
		Dictionary_2_t3323877696 * L_3 = (Dictionary_2_t3323877696 *)__this->get_m_Dictionary_1();
		Il2CppObject * L_4 = V_0;
		NullCheck((Dictionary_2_t3323877696 *)L_3);
		((  bool (*) (Dictionary_2_t3323877696 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((Dictionary_2_t3323877696 *)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		List_1_t1244034627 * L_5 = (List_1_t1244034627 *)__this->get_m_List_0();
		int32_t L_6 = ___index0;
		Il2CppObject * L_7 = ___value1;
		NullCheck((List_1_t1244034627 *)L_5);
		((  void (*) (List_1_t1244034627 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t1244034627 *)L_5, (int32_t)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		Dictionary_2_t3323877696 * L_8 = (Dictionary_2_t3323877696 *)__this->get_m_Dictionary_1();
		Il2CppObject * L_9 = V_0;
		int32_t L_10 = ___index0;
		NullCheck((Dictionary_2_t3323877696 *)L_8);
		((  void (*) (Dictionary_2_t3323877696 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t3323877696 *)L_8, (Il2CppObject *)L_9, (int32_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return;
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::RemoveAll(System.Predicate`1<T>)
extern "C"  void IndexedSet_1_RemoveAll_m4086301344_gshared (IndexedSet_1_t2695948348 * __this, Predicate_1_t3781873254 * ___match0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	{
		V_0 = (int32_t)0;
		goto IL_0031;
	}

IL_0007:
	{
		List_1_t1244034627 * L_0 = (List_1_t1244034627 *)__this->get_m_List_0();
		int32_t L_1 = V_0;
		NullCheck((List_1_t1244034627 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (List_1_t1244034627 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t1244034627 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_1 = (Il2CppObject *)L_2;
		Predicate_1_t3781873254 * L_3 = ___match0;
		Il2CppObject * L_4 = V_1;
		NullCheck((Predicate_1_t3781873254 *)L_3);
		bool L_5 = ((  bool (*) (Predicate_1_t3781873254 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((Predicate_1_t3781873254 *)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		if (!L_5)
		{
			goto IL_002d;
		}
	}
	{
		Il2CppObject * L_6 = V_1;
		NullCheck((IndexedSet_1_t2695948348 *)__this);
		((  bool (*) (IndexedSet_1_t2695948348 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20)->methodPointer)((IndexedSet_1_t2695948348 *)__this, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20));
		goto IL_0031;
	}

IL_002d:
	{
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_8 = V_0;
		List_1_t1244034627 * L_9 = (List_1_t1244034627 *)__this->get_m_List_0();
		NullCheck((List_1_t1244034627 *)L_9);
		int32_t L_10 = ((  int32_t (*) (List_1_t1244034627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t1244034627 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_8) < ((int32_t)L_10)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Sort(System.Comparison`1<T>)
extern "C"  void IndexedSet_1_Sort_m3752037115_gshared (IndexedSet_1_t2695948348 * __this, Comparison_1_t2887177558 * ___sortLayoutFunction0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	{
		List_1_t1244034627 * L_0 = (List_1_t1244034627 *)__this->get_m_List_0();
		Comparison_1_t2887177558 * L_1 = ___sortLayoutFunction0;
		NullCheck((List_1_t1244034627 *)L_0);
		((  void (*) (List_1_t1244034627 *, Comparison_1_t2887177558 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21)->methodPointer)((List_1_t1244034627 *)L_0, (Comparison_1_t2887177558 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21));
		V_0 = (int32_t)0;
		goto IL_0031;
	}

IL_0013:
	{
		List_1_t1244034627 * L_2 = (List_1_t1244034627 *)__this->get_m_List_0();
		int32_t L_3 = V_0;
		NullCheck((List_1_t1244034627 *)L_2);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (List_1_t1244034627 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t1244034627 *)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_1 = (Il2CppObject *)L_4;
		Dictionary_2_t3323877696 * L_5 = (Dictionary_2_t3323877696 *)__this->get_m_Dictionary_1();
		Il2CppObject * L_6 = V_1;
		int32_t L_7 = V_0;
		NullCheck((Dictionary_2_t3323877696 *)L_5);
		((  void (*) (Dictionary_2_t3323877696 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((Dictionary_2_t3323877696 *)L_5, (Il2CppObject *)L_6, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		int32_t L_8 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_9 = V_0;
		List_1_t1244034627 * L_10 = (List_1_t1244034627 *)__this->get_m_List_0();
		NullCheck((List_1_t1244034627 *)L_10);
		int32_t L_11 = ((  int32_t (*) (List_1_t1244034627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t1244034627 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m2991015408_gshared (U3CStartU3Ec__Iterator0_t1674837779 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1439374946_gshared (U3CStartU3Ec__Iterator0_t1674837779 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3531220470_gshared (U3CStartU3Ec__Iterator0_t1674837779 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Boolean UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::MoveNext()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__Iterator0_MoveNext_m916801450_MetadataUsageId;
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m916801450_gshared (U3CStartU3Ec__Iterator0_t1674837779 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_MoveNext_m916801450_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	float G_B7_0 = 0.0f;
	U3CStartU3Ec__Iterator0_t1674837779 * G_B7_1 = NULL;
	float G_B6_0 = 0.0f;
	U3CStartU3Ec__Iterator0_t1674837779 * G_B6_1 = NULL;
	float G_B8_0 = 0.0f;
	float G_B8_1 = 0.0f;
	U3CStartU3Ec__Iterator0_t1674837779 * G_B8_2 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00cb;
		}
	}
	{
		goto IL_0104;
	}

IL_0021:
	{
		ColorTween_t723277650 * L_2 = (ColorTween_t723277650 *)__this->get_address_of_tweenInfo_0();
		bool L_3 = ColorTween_ValidTarget_m2504867349((ColorTween_t723277650 *)L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_003c;
		}
	}
	{
		goto IL_0104;
	}

IL_003c:
	{
		__this->set_U3CelapsedTimeU3E__0_1((0.0f));
		goto IL_00cb;
	}

IL_004c:
	{
		float L_4 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		ColorTween_t723277650 * L_5 = (ColorTween_t723277650 *)__this->get_address_of_tweenInfo_0();
		bool L_6 = ColorTween_get_ignoreTimeScale_m27222826((ColorTween_t723277650 *)L_5, /*hidden argument*/NULL);
		G_B6_0 = L_4;
		G_B6_1 = ((U3CStartU3Ec__Iterator0_t1674837779 *)(__this));
		if (!L_6)
		{
			G_B7_0 = L_4;
			G_B7_1 = ((U3CStartU3Ec__Iterator0_t1674837779 *)(__this));
			goto IL_0073;
		}
	}
	{
		float L_7 = Time_get_unscaledDeltaTime_m285638843(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B8_0 = L_7;
		G_B8_1 = G_B6_0;
		G_B8_2 = ((U3CStartU3Ec__Iterator0_t1674837779 *)(G_B6_1));
		goto IL_0078;
	}

IL_0073:
	{
		float L_8 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B8_0 = L_8;
		G_B8_1 = G_B7_0;
		G_B8_2 = ((U3CStartU3Ec__Iterator0_t1674837779 *)(G_B7_1));
	}

IL_0078:
	{
		NullCheck(G_B8_2);
		G_B8_2->set_U3CelapsedTimeU3E__0_1(((float)((float)G_B8_1+(float)G_B8_0)));
		float L_9 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		ColorTween_t723277650 * L_10 = (ColorTween_t723277650 *)__this->get_address_of_tweenInfo_0();
		float L_11 = ColorTween_get_duration_m2603018997((ColorTween_t723277650 *)L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_12 = Mathf_Clamp01_m2272733930(NULL /*static, unused*/, (float)((float)((float)L_9/(float)L_11)), /*hidden argument*/NULL);
		__this->set_U3CpercentageU3E__1_2(L_12);
		ColorTween_t723277650 * L_13 = (ColorTween_t723277650 *)__this->get_address_of_tweenInfo_0();
		float L_14 = (float)__this->get_U3CpercentageU3E__1_2();
		ColorTween_TweenValue_m3209849337((ColorTween_t723277650 *)L_13, (float)L_14, /*hidden argument*/NULL);
		__this->set_U24current_4(NULL);
		__this->set_U24PC_3(1);
		goto IL_0106;
	}

IL_00cb:
	{
		float L_15 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		ColorTween_t723277650 * L_16 = (ColorTween_t723277650 *)__this->get_address_of_tweenInfo_0();
		float L_17 = ColorTween_get_duration_m2603018997((ColorTween_t723277650 *)L_16, /*hidden argument*/NULL);
		if ((((float)L_15) < ((float)L_17)))
		{
			goto IL_004c;
		}
	}
	{
		ColorTween_t723277650 * L_18 = (ColorTween_t723277650 *)__this->get_address_of_tweenInfo_0();
		ColorTween_TweenValue_m3209849337((ColorTween_t723277650 *)L_18, (float)(1.0f), /*hidden argument*/NULL);
		__this->set_U24PC_3((-1));
	}

IL_0104:
	{
		return (bool)0;
	}

IL_0106:
	{
		return (bool)1;
	}
	// Dead block : IL_0108: ldloc.1
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m930969133_gshared (U3CStartU3Ec__Iterator0_t1674837779 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__Iterator0_Reset_m637448349_MetadataUsageId;
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m637448349_gshared (U3CStartU3Ec__Iterator0_t1674837779 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_Reset_m637448349_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m2814173655_gshared (U3CStartU3Ec__Iterator0_t3663265722 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2481956059_gshared (U3CStartU3Ec__Iterator0_t3663265722 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1753529967_gshared (U3CStartU3Ec__Iterator0_t3663265722 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Boolean UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::MoveNext()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__Iterator0_MoveNext_m2549010019_MetadataUsageId;
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m2549010019_gshared (U3CStartU3Ec__Iterator0_t3663265722 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_MoveNext_m2549010019_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	float G_B7_0 = 0.0f;
	U3CStartU3Ec__Iterator0_t3663265722 * G_B7_1 = NULL;
	float G_B6_0 = 0.0f;
	U3CStartU3Ec__Iterator0_t3663265722 * G_B6_1 = NULL;
	float G_B8_0 = 0.0f;
	float G_B8_1 = 0.0f;
	U3CStartU3Ec__Iterator0_t3663265722 * G_B8_2 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00cb;
		}
	}
	{
		goto IL_0104;
	}

IL_0021:
	{
		FloatTween_t2711705593 * L_2 = (FloatTween_t2711705593 *)__this->get_address_of_tweenInfo_0();
		bool L_3 = FloatTween_ValidTarget_m3394239118((FloatTween_t2711705593 *)L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_003c;
		}
	}
	{
		goto IL_0104;
	}

IL_003c:
	{
		__this->set_U3CelapsedTimeU3E__0_1((0.0f));
		goto IL_00cb;
	}

IL_004c:
	{
		float L_4 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		FloatTween_t2711705593 * L_5 = (FloatTween_t2711705593 *)__this->get_address_of_tweenInfo_0();
		bool L_6 = FloatTween_get_ignoreTimeScale_m1786680995((FloatTween_t2711705593 *)L_5, /*hidden argument*/NULL);
		G_B6_0 = L_4;
		G_B6_1 = ((U3CStartU3Ec__Iterator0_t3663265722 *)(__this));
		if (!L_6)
		{
			G_B7_0 = L_4;
			G_B7_1 = ((U3CStartU3Ec__Iterator0_t3663265722 *)(__this));
			goto IL_0073;
		}
	}
	{
		float L_7 = Time_get_unscaledDeltaTime_m285638843(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B8_0 = L_7;
		G_B8_1 = G_B6_0;
		G_B8_2 = ((U3CStartU3Ec__Iterator0_t3663265722 *)(G_B6_1));
		goto IL_0078;
	}

IL_0073:
	{
		float L_8 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B8_0 = L_8;
		G_B8_1 = G_B7_0;
		G_B8_2 = ((U3CStartU3Ec__Iterator0_t3663265722 *)(G_B7_1));
	}

IL_0078:
	{
		NullCheck(G_B8_2);
		G_B8_2->set_U3CelapsedTimeU3E__0_1(((float)((float)G_B8_1+(float)G_B8_0)));
		float L_9 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		FloatTween_t2711705593 * L_10 = (FloatTween_t2711705593 *)__this->get_address_of_tweenInfo_0();
		float L_11 = FloatTween_get_duration_m108772764((FloatTween_t2711705593 *)L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_12 = Mathf_Clamp01_m2272733930(NULL /*static, unused*/, (float)((float)((float)L_9/(float)L_11)), /*hidden argument*/NULL);
		__this->set_U3CpercentageU3E__1_2(L_12);
		FloatTween_t2711705593 * L_13 = (FloatTween_t2711705593 *)__this->get_address_of_tweenInfo_0();
		float L_14 = (float)__this->get_U3CpercentageU3E__1_2();
		FloatTween_TweenValue_m1980315890((FloatTween_t2711705593 *)L_13, (float)L_14, /*hidden argument*/NULL);
		__this->set_U24current_4(NULL);
		__this->set_U24PC_3(1);
		goto IL_0106;
	}

IL_00cb:
	{
		float L_15 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		FloatTween_t2711705593 * L_16 = (FloatTween_t2711705593 *)__this->get_address_of_tweenInfo_0();
		float L_17 = FloatTween_get_duration_m108772764((FloatTween_t2711705593 *)L_16, /*hidden argument*/NULL);
		if ((((float)L_15) < ((float)L_17)))
		{
			goto IL_004c;
		}
	}
	{
		FloatTween_t2711705593 * L_18 = (FloatTween_t2711705593 *)__this->get_address_of_tweenInfo_0();
		FloatTween_TweenValue_m1980315890((FloatTween_t2711705593 *)L_18, (float)(1.0f), /*hidden argument*/NULL);
		__this->set_U24PC_3((-1));
	}

IL_0104:
	{
		return (bool)0;
	}

IL_0106:
	{
		return (bool)1;
	}
	// Dead block : IL_0108: ldloc.1
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m2784736340_gshared (U3CStartU3Ec__Iterator0_t3663265722 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__Iterator0_Reset_m460606596_MetadataUsageId;
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m460606596_gshared (U3CStartU3Ec__Iterator0_t3663265722 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_Reset_m460606596_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::.ctor()
extern "C"  void TweenRunner_1__ctor_m3171479704_gshared (TweenRunner_1_t805970637 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::Start(T)
extern "C"  Il2CppObject * TweenRunner_1_Start_m4199917220_gshared (Il2CppObject * __this /* static, unused */, ColorTween_t723277650  ___tweenInfo0, const MethodInfo* method)
{
	U3CStartU3Ec__Iterator0_t1674837779 * V_0 = NULL;
	{
		U3CStartU3Ec__Iterator0_t1674837779 * L_0 = (U3CStartU3Ec__Iterator0_t1674837779 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CStartU3Ec__Iterator0_t1674837779 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CStartU3Ec__Iterator0_t1674837779 *)L_0;
		U3CStartU3Ec__Iterator0_t1674837779 * L_1 = V_0;
		ColorTween_t723277650  L_2 = ___tweenInfo0;
		NullCheck(L_1);
		L_1->set_tweenInfo_0(L_2);
		U3CStartU3Ec__Iterator0_t1674837779 * L_3 = V_0;
		ColorTween_t723277650  L_4 = ___tweenInfo0;
		NullCheck(L_3);
		L_3->set_U3CU24U3EtweenInfo_5(L_4);
		U3CStartU3Ec__Iterator0_t1674837779 * L_5 = V_0;
		return L_5;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::Init(UnityEngine.MonoBehaviour)
extern "C"  void TweenRunner_1_Init_m3753968659_gshared (TweenRunner_1_t805970637 * __this, MonoBehaviour_t667441552 * ___coroutineContainer0, const MethodInfo* method)
{
	{
		MonoBehaviour_t667441552 * L_0 = ___coroutineContainer0;
		__this->set_m_CoroutineContainer_0(L_0);
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::StartTween(T)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2341039237;
extern const uint32_t TweenRunner_1_StartTween_m3060079017_MetadataUsageId;
extern "C"  void TweenRunner_1_StartTween_m3060079017_gshared (TweenRunner_1_t805970637 * __this, ColorTween_t723277650  ___info0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenRunner_1_StartTween_m3060079017_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoBehaviour_t667441552 * L_0 = (MonoBehaviour_t667441552 *)__this->get_m_CoroutineContainer_0();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, (Object_t3071478659 *)L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3123317694(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2341039237, /*hidden argument*/NULL);
		return;
	}

IL_001c:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_m_Tween_1();
		if (!L_2)
		{
			goto IL_003f;
		}
	}
	{
		MonoBehaviour_t667441552 * L_3 = (MonoBehaviour_t667441552 *)__this->get_m_CoroutineContainer_0();
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_m_Tween_1();
		NullCheck((MonoBehaviour_t667441552 *)L_3);
		MonoBehaviour_StopCoroutine_m1340700766((MonoBehaviour_t667441552 *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_m_Tween_1((Il2CppObject *)NULL);
	}

IL_003f:
	{
		MonoBehaviour_t667441552 * L_5 = (MonoBehaviour_t667441552 *)__this->get_m_CoroutineContainer_0();
		NullCheck((Component_t3501516275 *)L_5);
		GameObject_t3674682005 * L_6 = Component_get_gameObject_m1170635899((Component_t3501516275 *)L_5, /*hidden argument*/NULL);
		NullCheck((GameObject_t3674682005 *)L_6);
		bool L_7 = GameObject_get_activeInHierarchy_m612450965((GameObject_t3674682005 *)L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0067;
		}
	}
	{
		ColorTween_TweenValue_m3209849337((ColorTween_t723277650 *)(&___info0), (float)(1.0f), /*hidden argument*/NULL);
		return;
	}

IL_0067:
	{
		ColorTween_t723277650  L_8 = ___info0;
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, ColorTween_t723277650 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (ColorTween_t723277650 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		__this->set_m_Tween_1(L_9);
		MonoBehaviour_t667441552 * L_10 = (MonoBehaviour_t667441552 *)__this->get_m_CoroutineContainer_0();
		Il2CppObject * L_11 = (Il2CppObject *)__this->get_m_Tween_1();
		NullCheck((MonoBehaviour_t667441552 *)L_10);
		MonoBehaviour_StartCoroutine_m2135303124((MonoBehaviour_t667441552 *)L_10, (Il2CppObject *)L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::.ctor()
extern "C"  void TweenRunner_1__ctor_m2994637951_gshared (TweenRunner_1_t2794398580 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::Start(T)
extern "C"  Il2CppObject * TweenRunner_1_Start_m3012790173_gshared (Il2CppObject * __this /* static, unused */, FloatTween_t2711705593  ___tweenInfo0, const MethodInfo* method)
{
	U3CStartU3Ec__Iterator0_t3663265722 * V_0 = NULL;
	{
		U3CStartU3Ec__Iterator0_t3663265722 * L_0 = (U3CStartU3Ec__Iterator0_t3663265722 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CStartU3Ec__Iterator0_t3663265722 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CStartU3Ec__Iterator0_t3663265722 *)L_0;
		U3CStartU3Ec__Iterator0_t3663265722 * L_1 = V_0;
		FloatTween_t2711705593  L_2 = ___tweenInfo0;
		NullCheck(L_1);
		L_1->set_tweenInfo_0(L_2);
		U3CStartU3Ec__Iterator0_t3663265722 * L_3 = V_0;
		FloatTween_t2711705593  L_4 = ___tweenInfo0;
		NullCheck(L_3);
		L_3->set_U3CU24U3EtweenInfo_5(L_4);
		U3CStartU3Ec__Iterator0_t3663265722 * L_5 = V_0;
		return L_5;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::Init(UnityEngine.MonoBehaviour)
extern "C"  void TweenRunner_1_Init_m2355829498_gshared (TweenRunner_1_t2794398580 * __this, MonoBehaviour_t667441552 * ___coroutineContainer0, const MethodInfo* method)
{
	{
		MonoBehaviour_t667441552 * L_0 = ___coroutineContainer0;
		__this->set_m_CoroutineContainer_0(L_0);
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::StartTween(T)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2341039237;
extern const uint32_t TweenRunner_1_StartTween_m565832784_MetadataUsageId;
extern "C"  void TweenRunner_1_StartTween_m565832784_gshared (TweenRunner_1_t2794398580 * __this, FloatTween_t2711705593  ___info0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenRunner_1_StartTween_m565832784_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoBehaviour_t667441552 * L_0 = (MonoBehaviour_t667441552 *)__this->get_m_CoroutineContainer_0();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, (Object_t3071478659 *)L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3123317694(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2341039237, /*hidden argument*/NULL);
		return;
	}

IL_001c:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_m_Tween_1();
		if (!L_2)
		{
			goto IL_003f;
		}
	}
	{
		MonoBehaviour_t667441552 * L_3 = (MonoBehaviour_t667441552 *)__this->get_m_CoroutineContainer_0();
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_m_Tween_1();
		NullCheck((MonoBehaviour_t667441552 *)L_3);
		MonoBehaviour_StopCoroutine_m1340700766((MonoBehaviour_t667441552 *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_m_Tween_1((Il2CppObject *)NULL);
	}

IL_003f:
	{
		MonoBehaviour_t667441552 * L_5 = (MonoBehaviour_t667441552 *)__this->get_m_CoroutineContainer_0();
		NullCheck((Component_t3501516275 *)L_5);
		GameObject_t3674682005 * L_6 = Component_get_gameObject_m1170635899((Component_t3501516275 *)L_5, /*hidden argument*/NULL);
		NullCheck((GameObject_t3674682005 *)L_6);
		bool L_7 = GameObject_get_activeInHierarchy_m612450965((GameObject_t3674682005 *)L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0067;
		}
	}
	{
		FloatTween_TweenValue_m1980315890((FloatTween_t2711705593 *)(&___info0), (float)(1.0f), /*hidden argument*/NULL);
		return;
	}

IL_0067:
	{
		FloatTween_t2711705593  L_8 = ___info0;
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, FloatTween_t2711705593 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (FloatTween_t2711705593 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		__this->set_m_Tween_1(L_9);
		MonoBehaviour_t667441552 * L_10 = (MonoBehaviour_t667441552 *)__this->get_m_CoroutineContainer_0();
		Il2CppObject * L_11 = (Il2CppObject *)__this->get_m_Tween_1();
		NullCheck((MonoBehaviour_t667441552 *)L_10);
		MonoBehaviour_StartCoroutine_m2135303124((MonoBehaviour_t667441552 *)L_10, (Il2CppObject *)L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Int32>::.cctor()
extern "C"  void ListPool_1__cctor_m3344713728_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Il2CppObject * G_B2_0 = NULL;
	Il2CppObject * G_B1_0 = NULL;
	{
		UnityAction_1_t2416204055 * L_0 = ((ListPool_1_t251475784_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache1_1();
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		UnityAction_1_t2416204055 * L_2 = (UnityAction_1_t2416204055 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (UnityAction_1_t2416204055 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)(L_2, (Il2CppObject *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((ListPool_1_t251475784_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_U3CU3Ef__amU24cache1_1(L_2);
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t2416204055 * L_3 = ((ListPool_1_t251475784_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache1_1();
		ObjectPool_1_t2321612177 * L_4 = (ObjectPool_1_t2321612177 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ObjectPool_1_t2321612177 *, UnityAction_1_t2416204055 *, UnityAction_1_t2416204055 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_4, (UnityAction_1_t2416204055 *)G_B2_0, (UnityAction_1_t2416204055 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((ListPool_1_t251475784_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_ListPool_0(L_4);
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<System.Int32>::Get()
extern "C"  List_1_t2522024052 * ListPool_1_Get_m1671355248_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t2321612177 * L_0 = ((ListPool_1_t251475784_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t2321612177 *)L_0);
		List_1_t2522024052 * L_1 = ((  List_1_t2522024052 * (*) (ObjectPool_1_t2321612177 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)((ObjectPool_1_t2321612177 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_1;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Int32>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m1485191562_gshared (Il2CppObject * __this /* static, unused */, List_1_t2522024052 * ___toRelease0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t2321612177 * L_0 = ((ListPool_1_t251475784_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		List_1_t2522024052 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t2321612177 *)L_0);
		((  void (*) (ObjectPool_1_t2321612177 *, List_1_t2522024052 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)((ObjectPool_1_t2321612177 *)L_0, (List_1_t2522024052 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Int32>::<s_ListPool>m__14(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__14_m1194038743_gshared (Il2CppObject * __this /* static, unused */, List_1_t2522024052 * ___l0, const MethodInfo* method)
{
	{
		List_1_t2522024052 * L_0 = ___l0;
		NullCheck((List_1_t2522024052 *)L_0);
		((  void (*) (List_1_t2522024052 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t2522024052 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Object>::.cctor()
extern "C"  void ListPool_1__cctor_m4278935907_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Il2CppObject * G_B2_0 = NULL;
	Il2CppObject * G_B1_0 = NULL;
	{
		UnityAction_1_t1138214630 * L_0 = ((ListPool_1_t3268453655_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache1_1();
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		UnityAction_1_t1138214630 * L_2 = (UnityAction_1_t1138214630 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (UnityAction_1_t1138214630 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)(L_2, (Il2CppObject *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((ListPool_1_t3268453655_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_U3CU3Ef__amU24cache1_1(L_2);
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t1138214630 * L_3 = ((ListPool_1_t3268453655_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache1_1();
		ObjectPool_1_t1043622752 * L_4 = (ObjectPool_1_t1043622752 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ObjectPool_1_t1043622752 *, UnityAction_1_t1138214630 *, UnityAction_1_t1138214630 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_4, (UnityAction_1_t1138214630 *)G_B2_0, (UnityAction_1_t1138214630 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((ListPool_1_t3268453655_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_ListPool_0(L_4);
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<System.Object>::Get()
extern "C"  List_1_t1244034627 * ListPool_1_Get_m2234302831_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t1043622752 * L_0 = ((ListPool_1_t3268453655_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t1043622752 *)L_0);
		List_1_t1244034627 * L_1 = ((  List_1_t1244034627 * (*) (ObjectPool_1_t1043622752 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)((ObjectPool_1_t1043622752 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_1;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Object>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m1182093831_gshared (Il2CppObject * __this /* static, unused */, List_1_t1244034627 * ___toRelease0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t1043622752 * L_0 = ((ListPool_1_t3268453655_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		List_1_t1244034627 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t1043622752 *)L_0);
		((  void (*) (ObjectPool_1_t1043622752 *, List_1_t1244034627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)((ObjectPool_1_t1043622752 *)L_0, (List_1_t1244034627 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Object>::<s_ListPool>m__14(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__14_m4207408660_gshared (Il2CppObject * __this /* static, unused */, List_1_t1244034627 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1244034627 * L_0 = ___l0;
		NullCheck((List_1_t1244034627 *)L_0);
		((  void (*) (List_1_t1244034627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t1244034627 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Color32>::.cctor()
extern "C"  void ListPool_1__cctor_m2225857654_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Il2CppObject * G_B2_0 = NULL;
	Il2CppObject * G_B1_0 = NULL;
	{
		UnityAction_1_t1861219243 * L_0 = ((ListPool_1_t3991458268_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache1_1();
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		UnityAction_1_t1861219243 * L_2 = (UnityAction_1_t1861219243 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (UnityAction_1_t1861219243 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)(L_2, (Il2CppObject *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((ListPool_1_t3991458268_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_U3CU3Ef__amU24cache1_1(L_2);
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t1861219243 * L_3 = ((ListPool_1_t3991458268_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache1_1();
		ObjectPool_1_t1766627365 * L_4 = (ObjectPool_1_t1766627365 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ObjectPool_1_t1766627365 *, UnityAction_1_t1861219243 *, UnityAction_1_t1861219243 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_4, (UnityAction_1_t1861219243 *)G_B2_0, (UnityAction_1_t1861219243 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((ListPool_1_t3991458268_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_ListPool_0(L_4);
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Color32>::Get()
extern "C"  List_1_t1967039240 * ListPool_1_Get_m1848305276_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t1766627365 * L_0 = ((ListPool_1_t3991458268_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t1766627365 *)L_0);
		List_1_t1967039240 * L_1 = ((  List_1_t1967039240 * (*) (ObjectPool_1_t1766627365 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)((ObjectPool_1_t1766627365 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_1;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Color32>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m1142705876_gshared (Il2CppObject * __this /* static, unused */, List_1_t1967039240 * ___toRelease0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t1766627365 * L_0 = ((ListPool_1_t3991458268_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		List_1_t1967039240 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t1766627365 *)L_0);
		((  void (*) (ObjectPool_1_t1766627365 *, List_1_t1967039240 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)((ObjectPool_1_t1766627365 *)L_0, (List_1_t1967039240 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Color32>::<s_ListPool>m__14(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__14_m4228322977_gshared (Il2CppObject * __this /* static, unused */, List_1_t1967039240 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1967039240 * L_0 = ___l0;
		NullCheck((List_1_t1967039240 *)L_0);
		((  void (*) (List_1_t1967039240 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t1967039240 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::.cctor()
extern "C"  void ListPool_1__cctor_m3740250016_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Il2CppObject * G_B2_0 = NULL;
	Il2CppObject * G_B1_0 = NULL;
	{
		UnityAction_1_t1211463471 * L_0 = ((ListPool_1_t3341702496_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache1_1();
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		UnityAction_1_t1211463471 * L_2 = (UnityAction_1_t1211463471 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (UnityAction_1_t1211463471 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)(L_2, (Il2CppObject *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((ListPool_1_t3341702496_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_U3CU3Ef__amU24cache1_1(L_2);
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t1211463471 * L_3 = ((ListPool_1_t3341702496_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache1_1();
		ObjectPool_1_t1116871593 * L_4 = (ObjectPool_1_t1116871593 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ObjectPool_1_t1116871593 *, UnityAction_1_t1211463471 *, UnityAction_1_t1211463471 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_4, (UnityAction_1_t1211463471 *)G_B2_0, (UnityAction_1_t1211463471 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((ListPool_1_t3341702496_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_ListPool_0(L_4);
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::Get()
extern "C"  List_1_t1317283468 * ListPool_1_Get_m3130095824_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t1116871593 * L_0 = ((ListPool_1_t3341702496_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t1116871593 *)L_0);
		List_1_t1317283468 * L_1 = ((  List_1_t1317283468 * (*) (ObjectPool_1_t1116871593 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)((ObjectPool_1_t1116871593 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_1;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m2709067242_gshared (Il2CppObject * __this /* static, unused */, List_1_t1317283468 * ___toRelease0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t1116871593 * L_0 = ((ListPool_1_t3341702496_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		List_1_t1317283468 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t1116871593 *)L_0);
		((  void (*) (ObjectPool_1_t1116871593 *, List_1_t1317283468 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)((ObjectPool_1_t1116871593 *)L_0, (List_1_t1317283468 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::<s_ListPool>m__14(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__14_m3908991543_gshared (Il2CppObject * __this /* static, unused */, List_1_t1317283468 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1317283468 * L_0 = ___l0;
		NullCheck((List_1_t1317283468 *)L_0);
		((  void (*) (List_1_t1317283468 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t1317283468 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::.cctor()
extern "C"  void ListPool_1__cctor_m3567944713_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Il2CppObject * G_B2_0 = NULL;
	Il2CppObject * G_B1_0 = NULL;
	{
		UnityAction_1_t1249464824 * L_0 = ((ListPool_1_t3379703849_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache1_1();
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		UnityAction_1_t1249464824 * L_2 = (UnityAction_1_t1249464824 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (UnityAction_1_t1249464824 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)(L_2, (Il2CppObject *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((ListPool_1_t3379703849_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_U3CU3Ef__amU24cache1_1(L_2);
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t1249464824 * L_3 = ((ListPool_1_t3379703849_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache1_1();
		ObjectPool_1_t1154872946 * L_4 = (ObjectPool_1_t1154872946 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ObjectPool_1_t1154872946 *, UnityAction_1_t1249464824 *, UnityAction_1_t1249464824 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_4, (UnityAction_1_t1249464824 *)G_B2_0, (UnityAction_1_t1249464824 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((ListPool_1_t3379703849_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_ListPool_0(L_4);
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::Get()
extern "C"  List_1_t1355284821 * ListPool_1_Get_m1779148745_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t1154872946 * L_0 = ((ListPool_1_t3379703849_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t1154872946 *)L_0);
		List_1_t1355284821 * L_1 = ((  List_1_t1355284821 * (*) (ObjectPool_1_t1154872946 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)((ObjectPool_1_t1154872946 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_1;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m3969187617_gshared (Il2CppObject * __this /* static, unused */, List_1_t1355284821 * ___toRelease0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t1154872946 * L_0 = ((ListPool_1_t3379703849_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		List_1_t1355284821 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t1154872946 *)L_0);
		((  void (*) (ObjectPool_1_t1154872946 *, List_1_t1355284821 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)((ObjectPool_1_t1154872946 *)L_0, (List_1_t1355284821 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::<s_ListPool>m__14(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__14_m1915587246_gshared (Il2CppObject * __this /* static, unused */, List_1_t1355284821 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1355284821 * L_0 = ___l0;
		NullCheck((List_1_t1355284821 *)L_0);
		((  void (*) (List_1_t1355284821 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t1355284821 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::.cctor()
extern "C"  void ListPool_1__cctor_m3697027432_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Il2CppObject * G_B2_0 = NULL;
	Il2CppObject * G_B1_0 = NULL;
	{
		UnityAction_1_t1249464825 * L_0 = ((ListPool_1_t3379703850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache1_1();
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		UnityAction_1_t1249464825 * L_2 = (UnityAction_1_t1249464825 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (UnityAction_1_t1249464825 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)(L_2, (Il2CppObject *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((ListPool_1_t3379703850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_U3CU3Ef__amU24cache1_1(L_2);
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t1249464825 * L_3 = ((ListPool_1_t3379703850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache1_1();
		ObjectPool_1_t1154872947 * L_4 = (ObjectPool_1_t1154872947 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ObjectPool_1_t1154872947 *, UnityAction_1_t1249464825 *, UnityAction_1_t1249464825 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_4, (UnityAction_1_t1249464825 *)G_B2_0, (UnityAction_1_t1249464825 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((ListPool_1_t3379703850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_ListPool_0(L_4);
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::Get()
extern "C"  List_1_t1355284822 * ListPool_1_Get_m4266661578_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t1154872947 * L_0 = ((ListPool_1_t3379703850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t1154872947 *)L_0);
		List_1_t1355284822 * L_1 = ((  List_1_t1355284822 * (*) (ObjectPool_1_t1154872947 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)((ObjectPool_1_t1154872947 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_1;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m3961428258_gshared (Il2CppObject * __this /* static, unused */, List_1_t1355284822 * ___toRelease0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t1154872947 * L_0 = ((ListPool_1_t3379703850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		List_1_t1355284822 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t1154872947 *)L_0);
		((  void (*) (ObjectPool_1_t1154872947 *, List_1_t1355284822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)((ObjectPool_1_t1154872947 *)L_0, (List_1_t1355284822 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::<s_ListPool>m__14(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__14_m2195937135_gshared (Il2CppObject * __this /* static, unused */, List_1_t1355284822 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1355284822 * L_0 = ___l0;
		NullCheck((List_1_t1355284822 *)L_0);
		((  void (*) (List_1_t1355284822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t1355284822 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::.cctor()
extern "C"  void ListPool_1__cctor_m3826110151_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Il2CppObject * G_B2_0 = NULL;
	Il2CppObject * G_B1_0 = NULL;
	{
		UnityAction_1_t1249464826 * L_0 = ((ListPool_1_t3379703851_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache1_1();
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		UnityAction_1_t1249464826 * L_2 = (UnityAction_1_t1249464826 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (UnityAction_1_t1249464826 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)(L_2, (Il2CppObject *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((ListPool_1_t3379703851_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_U3CU3Ef__amU24cache1_1(L_2);
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t1249464826 * L_3 = ((ListPool_1_t3379703851_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache1_1();
		ObjectPool_1_t1154872948 * L_4 = (ObjectPool_1_t1154872948 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ObjectPool_1_t1154872948 *, UnityAction_1_t1249464826 *, UnityAction_1_t1249464826 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_4, (UnityAction_1_t1249464826 *)G_B2_0, (UnityAction_1_t1249464826 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((ListPool_1_t3379703851_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_ListPool_0(L_4);
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::Get()
extern "C"  List_1_t1355284823 * ListPool_1_Get_m2459207115_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t1154872948 * L_0 = ((ListPool_1_t3379703851_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t1154872948 *)L_0);
		List_1_t1355284823 * L_1 = ((  List_1_t1355284823 * (*) (ObjectPool_1_t1154872948 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)((ObjectPool_1_t1154872948 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_1;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m3953668899_gshared (Il2CppObject * __this /* static, unused */, List_1_t1355284823 * ___toRelease0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t1154872948 * L_0 = ((ListPool_1_t3379703851_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		List_1_t1355284823 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t1154872948 *)L_0);
		((  void (*) (ObjectPool_1_t1154872948 *, List_1_t1355284823 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)((ObjectPool_1_t1154872948 *)L_0, (List_1_t1355284823 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::<s_ListPool>m__14(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__14_m2476287024_gshared (Il2CppObject * __this /* static, unused */, List_1_t1355284823 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1355284823 * L_0 = ___l0;
		NullCheck((List_1_t1355284823 *)L_0);
		((  void (*) (List_1_t1355284823 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t1355284823 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
extern "C"  void ObjectPool_1__ctor_m2532712771_gshared (ObjectPool_1_t3970404496 * __this, UnityAction_1_t4064996374 * ___actionOnGet0, UnityAction_1_t4064996374 * ___actionOnRelease1, const MethodInfo* method)
{
	{
		Stack_1_t2974409999 * L_0 = (Stack_1_t2974409999 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Stack_1_t2974409999 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_m_Stack_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		UnityAction_1_t4064996374 * L_1 = ___actionOnGet0;
		__this->set_m_ActionOnGet_1(L_1);
		UnityAction_1_t4064996374 * L_2 = ___actionOnRelease1;
		__this->set_m_ActionOnRelease_2(L_2);
		return;
	}
}
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countAll()
extern "C"  int32_t ObjectPool_1_get_countAll_m3327915100_gshared (ObjectPool_1_t3970404496 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U3CcountAllU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::set_countAll(System.Int32)
extern "C"  void ObjectPool_1_set_countAll_m2125882937_gshared (ObjectPool_1_t3970404496 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CcountAllU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countActive()
extern "C"  int32_t ObjectPool_1_get_countActive_m2082506317_gshared (ObjectPool_1_t3970404496 * __this, const MethodInfo* method)
{
	{
		NullCheck((ObjectPool_1_t3970404496 *)__this);
		int32_t L_0 = ((  int32_t (*) (ObjectPool_1_t3970404496 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((ObjectPool_1_t3970404496 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		NullCheck((ObjectPool_1_t3970404496 *)__this);
		int32_t L_1 = ((  int32_t (*) (ObjectPool_1_t3970404496 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((ObjectPool_1_t3970404496 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return ((int32_t)((int32_t)L_0-(int32_t)L_1));
	}
}
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countInactive()
extern "C"  int32_t ObjectPool_1_get_countInactive_m19645682_gshared (ObjectPool_1_t3970404496 * __this, const MethodInfo* method)
{
	{
		Stack_1_t2974409999 * L_0 = (Stack_1_t2974409999 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t2974409999 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Stack_1_t2974409999 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Stack_1_t2974409999 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_1;
	}
}
// T UnityEngine.UI.ObjectPool`1<System.Object>::Get()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t ObjectPool_1_Get_m3052664832_MetadataUsageId;
extern "C"  Il2CppObject * ObjectPool_1_Get_m3052664832_gshared (ObjectPool_1_t3970404496 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObjectPool_1_Get_m3052664832_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * G_B4_0 = NULL;
	{
		Stack_1_t2974409999 * L_0 = (Stack_1_t2974409999 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t2974409999 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Stack_1_t2974409999 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Stack_1_t2974409999 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		if (L_1)
		{
			goto IL_0047;
		}
	}
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_2 = V_1;
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_3 = V_1;
		G_B4_0 = L_3;
		goto IL_0033;
	}

IL_002e:
	{
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		G_B4_0 = L_4;
	}

IL_0033:
	{
		V_0 = (Il2CppObject *)G_B4_0;
		NullCheck((ObjectPool_1_t3970404496 *)__this);
		int32_t L_5 = ((  int32_t (*) (ObjectPool_1_t3970404496 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((ObjectPool_1_t3970404496 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		NullCheck((ObjectPool_1_t3970404496 *)__this);
		((  void (*) (ObjectPool_1_t3970404496 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((ObjectPool_1_t3970404496 *)__this, (int32_t)((int32_t)((int32_t)L_5+(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		goto IL_0053;
	}

IL_0047:
	{
		Stack_1_t2974409999 * L_6 = (Stack_1_t2974409999 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t2974409999 *)L_6);
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (Stack_1_t2974409999 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Stack_1_t2974409999 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		V_0 = (Il2CppObject *)L_7;
	}

IL_0053:
	{
		UnityAction_1_t4064996374 * L_8 = (UnityAction_1_t4064996374 *)__this->get_m_ActionOnGet_1();
		if (!L_8)
		{
			goto IL_006a;
		}
	}
	{
		UnityAction_1_t4064996374 * L_9 = (UnityAction_1_t4064996374 *)__this->get_m_ActionOnGet_1();
		Il2CppObject * L_10 = V_0;
		NullCheck((UnityAction_1_t4064996374 *)L_9);
		((  void (*) (UnityAction_1_t4064996374 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((UnityAction_1_t4064996374 *)L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
	}

IL_006a:
	{
		Il2CppObject * L_11 = V_0;
		return L_11;
	}
}
// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::Release(T)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3474434835;
extern const uint32_t ObjectPool_1_Release_m1110976910_MetadataUsageId;
extern "C"  void ObjectPool_1_Release_m1110976910_gshared (ObjectPool_1_t3970404496 * __this, Il2CppObject * ___element0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObjectPool_1_Release_m1110976910_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Stack_1_t2974409999 * L_0 = (Stack_1_t2974409999 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t2974409999 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Stack_1_t2974409999 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Stack_1_t2974409999 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_003b;
		}
	}
	{
		Stack_1_t2974409999 * L_2 = (Stack_1_t2974409999 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t2974409999 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Stack_1_t2974409999 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Stack_1_t2974409999 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		Il2CppObject * L_4 = ___element0;
		bool L_5 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, (Il2CppObject *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral3474434835, /*hidden argument*/NULL);
	}

IL_003b:
	{
		UnityAction_1_t4064996374 * L_6 = (UnityAction_1_t4064996374 *)__this->get_m_ActionOnRelease_2();
		if (!L_6)
		{
			goto IL_0052;
		}
	}
	{
		UnityAction_1_t4064996374 * L_7 = (UnityAction_1_t4064996374 *)__this->get_m_ActionOnRelease_2();
		Il2CppObject * L_8 = ___element0;
		NullCheck((UnityAction_1_t4064996374 *)L_7);
		((  void (*) (UnityAction_1_t4064996374 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((UnityAction_1_t4064996374 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
	}

IL_0052:
	{
		Stack_1_t2974409999 * L_9 = (Stack_1_t2974409999 *)__this->get_m_Stack_0();
		Il2CppObject * L_10 = ___element0;
		NullCheck((Stack_1_t2974409999 *)L_9);
		((  void (*) (Stack_1_t2974409999 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((Stack_1_t2974409999 *)L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return;
	}
}
// System.Void ZXing.BarcodeReaderGeneric`1<System.Object>::.cctor()
extern Il2CppClass* Func_2_t2392815316_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_5_t533482363_il2cpp_TypeInfo_var;
extern const MethodInfo* Func_2__ctor_m3958203315_MethodInfo_var;
extern const MethodInfo* Func_5__ctor_m646890362_MethodInfo_var;
extern const uint32_t BarcodeReaderGeneric_1__cctor_m4288126543_MetadataUsageId;
extern "C"  void BarcodeReaderGeneric_1__cctor_m4288126543_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BarcodeReaderGeneric_1__cctor_m4288126543_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Func_2_t2392815316 * L_0 = ((BarcodeReaderGeneric_1_t3481712763_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_12();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Func_2_t2392815316 * L_2 = (Func_2_t2392815316 *)il2cpp_codegen_object_new(Func_2_t2392815316_il2cpp_TypeInfo_var);
		Func_2__ctor_m3958203315(L_2, (Il2CppObject *)NULL, (IntPtr_t)L_1, /*hidden argument*/Func_2__ctor_m3958203315_MethodInfo_var);
		((BarcodeReaderGeneric_1_t3481712763_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_12(L_2);
	}

IL_0018:
	{
		Func_2_t2392815316 * L_3 = ((BarcodeReaderGeneric_1_t3481712763_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_12();
		((BarcodeReaderGeneric_1_t3481712763_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_defaultCreateBinarizer_3(L_3);
		Func_5_t533482363 * L_4 = ((BarcodeReaderGeneric_1_t3481712763_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_13();
		if (L_4)
		{
			goto IL_003a;
		}
	}
	{
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Func_5_t533482363 * L_6 = (Func_5_t533482363 *)il2cpp_codegen_object_new(Func_5_t533482363_il2cpp_TypeInfo_var);
		Func_5__ctor_m646890362(L_6, (Il2CppObject *)NULL, (IntPtr_t)L_5, /*hidden argument*/Func_5__ctor_m646890362_MethodInfo_var);
		((BarcodeReaderGeneric_1_t3481712763_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_13(L_6);
	}

IL_003a:
	{
		Func_5_t533482363 * L_7 = ((BarcodeReaderGeneric_1_t3481712763_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_CSU24U3CU3E9__CachedAnonymousMethodDelegate3_13();
		((BarcodeReaderGeneric_1_t3481712763_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_defaultCreateRGBLuminanceSource_4(L_7);
		return;
	}
}
// System.Void ZXing.BarcodeReaderGeneric`1<System.Object>::.ctor(ZXing.Reader,System.Func`4<T,System.Int32,System.Int32,ZXing.LuminanceSource>,System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer>)
extern "C"  void BarcodeReaderGeneric_1__ctor_m3825891079_gshared (BarcodeReaderGeneric_1_t3481712763 * __this, Il2CppObject * ___reader0, Func_4_t3774219921 * ___createLuminanceSource1, Func_2_t2392815316 * ___createBinarizer2, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___reader0;
		Func_4_t3774219921 * L_1 = ___createLuminanceSource1;
		Func_2_t2392815316 * L_2 = ___createBinarizer2;
		NullCheck((BarcodeReaderGeneric_1_t3481712763 *)__this);
		((  void (*) (BarcodeReaderGeneric_1_t3481712763 *, Il2CppObject *, Func_4_t3774219921 *, Func_2_t2392815316 *, Func_5_t533482363 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((BarcodeReaderGeneric_1_t3481712763 *)__this, (Il2CppObject *)L_0, (Func_4_t3774219921 *)L_1, (Func_2_t2392815316 *)L_2, (Func_5_t533482363 *)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void ZXing.BarcodeReaderGeneric`1<System.Object>::.ctor(ZXing.Reader,System.Func`4<T,System.Int32,System.Int32,ZXing.LuminanceSource>,System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer>,System.Func`5<System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat,ZXing.LuminanceSource>)
extern Il2CppClass* MultiFormatReader_t4081038293_il2cpp_TypeInfo_var;
extern const uint32_t BarcodeReaderGeneric_1__ctor_m2970014716_MetadataUsageId;
extern "C"  void BarcodeReaderGeneric_1__ctor_m2970014716_gshared (BarcodeReaderGeneric_1_t3481712763 * __this, Il2CppObject * ___reader0, Func_4_t3774219921 * ___createLuminanceSource1, Func_2_t2392815316 * ___createBinarizer2, Func_5_t533482363 * ___createRGBLuminanceSource3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BarcodeReaderGeneric_1__ctor_m2970014716_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_2_t2663079113 * V_0 = NULL;
	Il2CppObject * G_B2_0 = NULL;
	BarcodeReaderGeneric_1_t3481712763 * G_B2_1 = NULL;
	Il2CppObject * G_B1_0 = NULL;
	BarcodeReaderGeneric_1_t3481712763 * G_B1_1 = NULL;
	Func_2_t2392815316 * G_B4_0 = NULL;
	BarcodeReaderGeneric_1_t3481712763 * G_B4_1 = NULL;
	Func_2_t2392815316 * G_B3_0 = NULL;
	BarcodeReaderGeneric_1_t3481712763 * G_B3_1 = NULL;
	Func_5_t533482363 * G_B6_0 = NULL;
	BarcodeReaderGeneric_1_t3481712763 * G_B6_1 = NULL;
	Func_5_t533482363 * G_B5_0 = NULL;
	BarcodeReaderGeneric_1_t3481712763 * G_B5_1 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		V_0 = (Action_2_t2663079113 *)NULL;
		Il2CppObject * L_0 = ___reader0;
		Il2CppObject * L_1 = (Il2CppObject *)L_0;
		G_B1_0 = L_1;
		G_B1_1 = ((BarcodeReaderGeneric_1_t3481712763 *)(__this));
		if (L_1)
		{
			G_B2_0 = L_1;
			G_B2_1 = ((BarcodeReaderGeneric_1_t3481712763 *)(__this));
			goto IL_0013;
		}
	}
	{
		MultiFormatReader_t4081038293 * L_2 = (MultiFormatReader_t4081038293 *)il2cpp_codegen_object_new(MultiFormatReader_t4081038293_il2cpp_TypeInfo_var);
		MultiFormatReader__ctor_m4045637922(L_2, /*hidden argument*/NULL);
		G_B2_0 = ((Il2CppObject *)(L_2));
		G_B2_1 = ((BarcodeReaderGeneric_1_t3481712763 *)(G_B1_1));
	}

IL_0013:
	{
		NullCheck(G_B2_1);
		G_B2_1->set_reader_6(G_B2_0);
		Func_4_t3774219921 * L_3 = ___createLuminanceSource1;
		__this->set_createLuminanceSource_1(L_3);
		Func_2_t2392815316 * L_4 = ___createBinarizer2;
		Func_2_t2392815316 * L_5 = (Func_2_t2392815316 *)L_4;
		G_B3_0 = L_5;
		G_B3_1 = ((BarcodeReaderGeneric_1_t3481712763 *)(__this));
		if (L_5)
		{
			G_B4_0 = L_5;
			G_B4_1 = ((BarcodeReaderGeneric_1_t3481712763 *)(__this));
			goto IL_002a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		Func_2_t2392815316 * L_6 = ((BarcodeReaderGeneric_1_t3481712763_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_defaultCreateBinarizer_3();
		G_B4_0 = L_6;
		G_B4_1 = ((BarcodeReaderGeneric_1_t3481712763 *)(G_B3_1));
	}

IL_002a:
	{
		NullCheck(G_B4_1);
		G_B4_1->set_createBinarizer_0(G_B4_0);
		Func_5_t533482363 * L_7 = ___createRGBLuminanceSource3;
		Func_5_t533482363 * L_8 = (Func_5_t533482363 *)L_7;
		G_B5_0 = L_8;
		G_B5_1 = ((BarcodeReaderGeneric_1_t3481712763 *)(__this));
		if (L_8)
		{
			G_B6_0 = L_8;
			G_B6_1 = ((BarcodeReaderGeneric_1_t3481712763 *)(__this));
			goto IL_003b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		Func_5_t533482363 * L_9 = ((BarcodeReaderGeneric_1_t3481712763_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_defaultCreateRGBLuminanceSource_4();
		G_B6_0 = L_9;
		G_B6_1 = ((BarcodeReaderGeneric_1_t3481712763 *)(G_B5_1));
	}

IL_003b:
	{
		NullCheck(G_B6_1);
		G_B6_1->set_createRGBLuminanceSource_2(G_B6_0);
		NullCheck((BarcodeReaderGeneric_1_t3481712763 *)__this);
		DecodingOptions_t3608870897 * L_10 = ((  DecodingOptions_t3608870897 * (*) (BarcodeReaderGeneric_1_t3481712763 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((BarcodeReaderGeneric_1_t3481712763 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Action_2_t2663079113 * L_11 = V_0;
		NullCheck((DecodingOptions_t3608870897 *)L_10);
		DecodingOptions_add_ValueChanged_m2818077131((DecodingOptions_t3608870897 *)L_10, (Action_2_t2663079113 *)L_11, /*hidden argument*/NULL);
		__this->set_usePreviousState_7((bool)0);
		return;
	}
}
// ZXing.Result ZXing.BarcodeReaderGeneric`1<System.Object>::Decode(ZXing.LuminanceSource)
extern Il2CppClass* BinaryBitmap_t2444664454_il2cpp_TypeInfo_var;
extern Il2CppClass* MultiFormatReader_t4081038293_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t759989846_il2cpp_TypeInfo_var;
extern Il2CppClass* Reader_t2610170425_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t2712902339_il2cpp_TypeInfo_var;
extern const MethodInfo* Func_2_Invoke_m3874378575_MethodInfo_var;
extern const uint32_t BarcodeReaderGeneric_1_Decode_m1780868875_MetadataUsageId;
extern "C"  Result_t2610723219 * BarcodeReaderGeneric_1_Decode_m1780868875_gshared (BarcodeReaderGeneric_1_t3481712763 * __this, LuminanceSource_t1231523093 * ___luminanceSource0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BarcodeReaderGeneric_1_Decode_m1780868875_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Result_t2610723219 * V_0 = NULL;
	BinaryBitmap_t2444664454 * V_1 = NULL;
	MultiFormatReader_t4081038293 * V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		V_0 = (Result_t2610723219 *)NULL;
		NullCheck((BarcodeReaderGeneric_1_t3481712763 *)__this);
		Func_2_t2392815316 * L_0 = ((  Func_2_t2392815316 * (*) (BarcodeReaderGeneric_1_t3481712763 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((BarcodeReaderGeneric_1_t3481712763 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		LuminanceSource_t1231523093 * L_1 = ___luminanceSource0;
		NullCheck((Func_2_t2392815316 *)L_0);
		Binarizer_t1492033400 * L_2 = Func_2_Invoke_m3874378575((Func_2_t2392815316 *)L_0, (LuminanceSource_t1231523093 *)L_1, /*hidden argument*/Func_2_Invoke_m3874378575_MethodInfo_var);
		BinaryBitmap_t2444664454 * L_3 = (BinaryBitmap_t2444664454 *)il2cpp_codegen_object_new(BinaryBitmap_t2444664454_il2cpp_TypeInfo_var);
		BinaryBitmap__ctor_m1263795045(L_3, (Binarizer_t1492033400 *)L_2, /*hidden argument*/NULL);
		V_1 = (BinaryBitmap_t2444664454 *)L_3;
		NullCheck((BarcodeReaderGeneric_1_t3481712763 *)__this);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (BarcodeReaderGeneric_1_t3481712763 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((BarcodeReaderGeneric_1_t3481712763 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		V_2 = (MultiFormatReader_t4081038293 *)((MultiFormatReader_t4081038293 *)IsInst(L_4, MultiFormatReader_t4081038293_il2cpp_TypeInfo_var));
		V_3 = (int32_t)0;
		V_4 = (int32_t)1;
		NullCheck((BarcodeReaderGeneric_1_t3481712763 *)__this);
		bool L_5 = ((  bool (*) (BarcodeReaderGeneric_1_t3481712763 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((BarcodeReaderGeneric_1_t3481712763 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		if (!L_5)
		{
			goto IL_004d;
		}
	}
	{
		NullCheck((BarcodeReaderGeneric_1_t3481712763 *)__this);
		DecodingOptions_t3608870897 * L_6 = ((  DecodingOptions_t3608870897 * (*) (BarcodeReaderGeneric_1_t3481712763 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((BarcodeReaderGeneric_1_t3481712763 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck((DecodingOptions_t3608870897 *)L_6);
		Il2CppObject* L_7 = DecodingOptions_get_Hints_m1725823998((DecodingOptions_t3608870897 *)L_6, /*hidden argument*/NULL);
		bool L_8 = ((bool)1);
		Il2CppObject * L_9 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_8);
		NullCheck((Il2CppObject*)L_7);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>::set_Item(!0,!1) */, IDictionary_2_t759989846_il2cpp_TypeInfo_var, (Il2CppObject*)L_7, (int32_t)((int32_t)11), (Il2CppObject *)L_9);
		V_4 = (int32_t)4;
		goto IL_013c;
	}

IL_004d:
	{
		NullCheck((BarcodeReaderGeneric_1_t3481712763 *)__this);
		DecodingOptions_t3608870897 * L_10 = ((  DecodingOptions_t3608870897 * (*) (BarcodeReaderGeneric_1_t3481712763 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((BarcodeReaderGeneric_1_t3481712763 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck((DecodingOptions_t3608870897 *)L_10);
		Il2CppObject* L_11 = DecodingOptions_get_Hints_m1725823998((DecodingOptions_t3608870897 *)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject*)L_11);
		bool L_12 = InterfaceFuncInvoker1< bool, int32_t >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>::ContainsKey(!0) */, IDictionary_2_t759989846_il2cpp_TypeInfo_var, (Il2CppObject*)L_11, (int32_t)((int32_t)11));
		if (!L_12)
		{
			goto IL_013c;
		}
	}
	{
		NullCheck((BarcodeReaderGeneric_1_t3481712763 *)__this);
		DecodingOptions_t3608870897 * L_13 = ((  DecodingOptions_t3608870897 * (*) (BarcodeReaderGeneric_1_t3481712763 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((BarcodeReaderGeneric_1_t3481712763 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck((DecodingOptions_t3608870897 *)L_13);
		Il2CppObject* L_14 = DecodingOptions_get_Hints_m1725823998((DecodingOptions_t3608870897 *)L_13, /*hidden argument*/NULL);
		NullCheck((Il2CppObject*)L_14);
		InterfaceFuncInvoker1< bool, int32_t >::Invoke(2 /* System.Boolean System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>::Remove(!0) */, IDictionary_2_t759989846_il2cpp_TypeInfo_var, (Il2CppObject*)L_14, (int32_t)((int32_t)11));
		goto IL_013c;
	}

IL_007c:
	{
		bool L_15 = (bool)__this->get_usePreviousState_7();
		if (!L_15)
		{
			goto IL_0091;
		}
	}
	{
		MultiFormatReader_t4081038293 * L_16 = V_2;
		if (!L_16)
		{
			goto IL_0091;
		}
	}
	{
		MultiFormatReader_t4081038293 * L_17 = V_2;
		BinaryBitmap_t2444664454 * L_18 = V_1;
		NullCheck((MultiFormatReader_t4081038293 *)L_17);
		Result_t2610723219 * L_19 = MultiFormatReader_decodeWithState_m2378665753((MultiFormatReader_t4081038293 *)L_17, (BinaryBitmap_t2444664454 *)L_18, /*hidden argument*/NULL);
		V_0 = (Result_t2610723219 *)L_19;
		goto IL_00b0;
	}

IL_0091:
	{
		NullCheck((BarcodeReaderGeneric_1_t3481712763 *)__this);
		Il2CppObject * L_20 = ((  Il2CppObject * (*) (BarcodeReaderGeneric_1_t3481712763 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((BarcodeReaderGeneric_1_t3481712763 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		BinaryBitmap_t2444664454 * L_21 = V_1;
		NullCheck((BarcodeReaderGeneric_1_t3481712763 *)__this);
		DecodingOptions_t3608870897 * L_22 = ((  DecodingOptions_t3608870897 * (*) (BarcodeReaderGeneric_1_t3481712763 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((BarcodeReaderGeneric_1_t3481712763 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck((DecodingOptions_t3608870897 *)L_22);
		Il2CppObject* L_23 = DecodingOptions_get_Hints_m1725823998((DecodingOptions_t3608870897 *)L_22, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_20);
		Result_t2610723219 * L_24 = InterfaceFuncInvoker2< Result_t2610723219 *, BinaryBitmap_t2444664454 *, Il2CppObject* >::Invoke(0 /* ZXing.Result ZXing.Reader::decode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>) */, Reader_t2610170425_il2cpp_TypeInfo_var, (Il2CppObject *)L_20, (BinaryBitmap_t2444664454 *)L_21, (Il2CppObject*)L_23);
		V_0 = (Result_t2610723219 *)L_24;
		__this->set_usePreviousState_7((bool)1);
	}

IL_00b0:
	{
		Result_t2610723219 * L_25 = V_0;
		if (L_25)
		{
			goto IL_010e;
		}
	}
	{
		NullCheck((BarcodeReaderGeneric_1_t3481712763 *)__this);
		bool L_26 = ((  bool (*) (BarcodeReaderGeneric_1_t3481712763 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((BarcodeReaderGeneric_1_t3481712763 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		if (!L_26)
		{
			goto IL_010e;
		}
	}
	{
		LuminanceSource_t1231523093 * L_27 = ___luminanceSource0;
		NullCheck((LuminanceSource_t1231523093 *)L_27);
		bool L_28 = VirtFuncInvoker0< bool >::Invoke(10 /* System.Boolean ZXing.LuminanceSource::get_InversionSupported() */, (LuminanceSource_t1231523093 *)L_27);
		if (!L_28)
		{
			goto IL_010e;
		}
	}
	{
		NullCheck((BarcodeReaderGeneric_1_t3481712763 *)__this);
		Func_2_t2392815316 * L_29 = ((  Func_2_t2392815316 * (*) (BarcodeReaderGeneric_1_t3481712763 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((BarcodeReaderGeneric_1_t3481712763 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		LuminanceSource_t1231523093 * L_30 = ___luminanceSource0;
		NullCheck((LuminanceSource_t1231523093 *)L_30);
		LuminanceSource_t1231523093 * L_31 = VirtFuncInvoker0< LuminanceSource_t1231523093 * >::Invoke(11 /* ZXing.LuminanceSource ZXing.LuminanceSource::invert() */, (LuminanceSource_t1231523093 *)L_30);
		NullCheck((Func_2_t2392815316 *)L_29);
		Binarizer_t1492033400 * L_32 = Func_2_Invoke_m3874378575((Func_2_t2392815316 *)L_29, (LuminanceSource_t1231523093 *)L_31, /*hidden argument*/Func_2_Invoke_m3874378575_MethodInfo_var);
		BinaryBitmap_t2444664454 * L_33 = (BinaryBitmap_t2444664454 *)il2cpp_codegen_object_new(BinaryBitmap_t2444664454_il2cpp_TypeInfo_var);
		BinaryBitmap__ctor_m1263795045(L_33, (Binarizer_t1492033400 *)L_32, /*hidden argument*/NULL);
		V_1 = (BinaryBitmap_t2444664454 *)L_33;
		bool L_34 = (bool)__this->get_usePreviousState_7();
		if (!L_34)
		{
			goto IL_00ef;
		}
	}
	{
		MultiFormatReader_t4081038293 * L_35 = V_2;
		if (!L_35)
		{
			goto IL_00ef;
		}
	}
	{
		MultiFormatReader_t4081038293 * L_36 = V_2;
		BinaryBitmap_t2444664454 * L_37 = V_1;
		NullCheck((MultiFormatReader_t4081038293 *)L_36);
		Result_t2610723219 * L_38 = MultiFormatReader_decodeWithState_m2378665753((MultiFormatReader_t4081038293 *)L_36, (BinaryBitmap_t2444664454 *)L_37, /*hidden argument*/NULL);
		V_0 = (Result_t2610723219 *)L_38;
		goto IL_010e;
	}

IL_00ef:
	{
		NullCheck((BarcodeReaderGeneric_1_t3481712763 *)__this);
		Il2CppObject * L_39 = ((  Il2CppObject * (*) (BarcodeReaderGeneric_1_t3481712763 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((BarcodeReaderGeneric_1_t3481712763 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		BinaryBitmap_t2444664454 * L_40 = V_1;
		NullCheck((BarcodeReaderGeneric_1_t3481712763 *)__this);
		DecodingOptions_t3608870897 * L_41 = ((  DecodingOptions_t3608870897 * (*) (BarcodeReaderGeneric_1_t3481712763 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((BarcodeReaderGeneric_1_t3481712763 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck((DecodingOptions_t3608870897 *)L_41);
		Il2CppObject* L_42 = DecodingOptions_get_Hints_m1725823998((DecodingOptions_t3608870897 *)L_41, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_39);
		Result_t2610723219 * L_43 = InterfaceFuncInvoker2< Result_t2610723219 *, BinaryBitmap_t2444664454 *, Il2CppObject* >::Invoke(0 /* ZXing.Result ZXing.Reader::decode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>) */, Reader_t2610170425_il2cpp_TypeInfo_var, (Il2CppObject *)L_39, (BinaryBitmap_t2444664454 *)L_40, (Il2CppObject*)L_42);
		V_0 = (Result_t2610723219 *)L_43;
		__this->set_usePreviousState_7((bool)1);
	}

IL_010e:
	{
		Result_t2610723219 * L_44 = V_0;
		if (L_44)
		{
			goto IL_0144;
		}
	}
	{
		LuminanceSource_t1231523093 * L_45 = ___luminanceSource0;
		NullCheck((LuminanceSource_t1231523093 *)L_45);
		bool L_46 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean ZXing.LuminanceSource::get_RotateSupported() */, (LuminanceSource_t1231523093 *)L_45);
		if (!L_46)
		{
			goto IL_0144;
		}
	}
	{
		NullCheck((BarcodeReaderGeneric_1_t3481712763 *)__this);
		bool L_47 = ((  bool (*) (BarcodeReaderGeneric_1_t3481712763 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((BarcodeReaderGeneric_1_t3481712763 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		if (!L_47)
		{
			goto IL_0144;
		}
	}
	{
		NullCheck((BarcodeReaderGeneric_1_t3481712763 *)__this);
		Func_2_t2392815316 * L_48 = ((  Func_2_t2392815316 * (*) (BarcodeReaderGeneric_1_t3481712763 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((BarcodeReaderGeneric_1_t3481712763 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		LuminanceSource_t1231523093 * L_49 = ___luminanceSource0;
		NullCheck((LuminanceSource_t1231523093 *)L_49);
		LuminanceSource_t1231523093 * L_50 = VirtFuncInvoker0< LuminanceSource_t1231523093 * >::Invoke(9 /* ZXing.LuminanceSource ZXing.LuminanceSource::rotateCounterClockwise() */, (LuminanceSource_t1231523093 *)L_49);
		NullCheck((Func_2_t2392815316 *)L_48);
		Binarizer_t1492033400 * L_51 = Func_2_Invoke_m3874378575((Func_2_t2392815316 *)L_48, (LuminanceSource_t1231523093 *)L_50, /*hidden argument*/Func_2_Invoke_m3874378575_MethodInfo_var);
		BinaryBitmap_t2444664454 * L_52 = (BinaryBitmap_t2444664454 *)il2cpp_codegen_object_new(BinaryBitmap_t2444664454_il2cpp_TypeInfo_var);
		BinaryBitmap__ctor_m1263795045(L_52, (Binarizer_t1492033400 *)L_51, /*hidden argument*/NULL);
		V_1 = (BinaryBitmap_t2444664454 *)L_52;
		int32_t L_53 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_53+(int32_t)1));
	}

IL_013c:
	{
		int32_t L_54 = V_3;
		int32_t L_55 = V_4;
		if ((((int32_t)L_54) < ((int32_t)L_55)))
		{
			goto IL_007c;
		}
	}

IL_0144:
	{
		Result_t2610723219 * L_56 = V_0;
		if (!L_56)
		{
			goto IL_01ba;
		}
	}
	{
		Result_t2610723219 * L_57 = V_0;
		NullCheck((Result_t2610723219 *)L_57);
		Il2CppObject* L_58 = Result_get_ResultMetadata_m2127799040((Result_t2610723219 *)L_57, /*hidden argument*/NULL);
		if (L_58)
		{
			goto IL_0161;
		}
	}
	{
		Result_t2610723219 * L_59 = V_0;
		int32_t L_60 = V_3;
		int32_t L_61 = ((int32_t)((int32_t)L_60*(int32_t)((int32_t)90)));
		Il2CppObject * L_62 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_61);
		NullCheck((Result_t2610723219 *)L_59);
		Result_putMetadata_m3924971406((Result_t2610723219 *)L_59, (int32_t)1, (Il2CppObject *)L_62, /*hidden argument*/NULL);
		goto IL_01b3;
	}

IL_0161:
	{
		Result_t2610723219 * L_63 = V_0;
		NullCheck((Result_t2610723219 *)L_63);
		Il2CppObject* L_64 = Result_get_ResultMetadata_m2127799040((Result_t2610723219 *)L_63, /*hidden argument*/NULL);
		NullCheck((Il2CppObject*)L_64);
		bool L_65 = InterfaceFuncInvoker1< bool, int32_t >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<ZXing.ResultMetadataType,System.Object>::ContainsKey(!0) */, IDictionary_2_t2712902339_il2cpp_TypeInfo_var, (Il2CppObject*)L_64, (int32_t)1);
		if (L_65)
		{
			goto IL_0186;
		}
	}
	{
		Result_t2610723219 * L_66 = V_0;
		NullCheck((Result_t2610723219 *)L_66);
		Il2CppObject* L_67 = Result_get_ResultMetadata_m2127799040((Result_t2610723219 *)L_66, /*hidden argument*/NULL);
		int32_t L_68 = V_3;
		int32_t L_69 = ((int32_t)((int32_t)L_68*(int32_t)((int32_t)90)));
		Il2CppObject * L_70 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_69);
		NullCheck((Il2CppObject*)L_67);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<ZXing.ResultMetadataType,System.Object>::set_Item(!0,!1) */, IDictionary_2_t2712902339_il2cpp_TypeInfo_var, (Il2CppObject*)L_67, (int32_t)1, (Il2CppObject *)L_70);
		goto IL_01b3;
	}

IL_0186:
	{
		Result_t2610723219 * L_71 = V_0;
		NullCheck((Result_t2610723219 *)L_71);
		Il2CppObject* L_72 = Result_get_ResultMetadata_m2127799040((Result_t2610723219 *)L_71, /*hidden argument*/NULL);
		Result_t2610723219 * L_73 = V_0;
		NullCheck((Result_t2610723219 *)L_73);
		Il2CppObject* L_74 = Result_get_ResultMetadata_m2127799040((Result_t2610723219 *)L_73, /*hidden argument*/NULL);
		NullCheck((Il2CppObject*)L_74);
		Il2CppObject * L_75 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(4 /* !1 System.Collections.Generic.IDictionary`2<ZXing.ResultMetadataType,System.Object>::get_Item(!0) */, IDictionary_2_t2712902339_il2cpp_TypeInfo_var, (Il2CppObject*)L_74, (int32_t)1);
		int32_t L_76 = V_3;
		int32_t L_77 = ((int32_t)((int32_t)((int32_t)((int32_t)((*(int32_t*)((int32_t*)UnBox (L_75, Int32_t1153838500_il2cpp_TypeInfo_var))))+(int32_t)((int32_t)((int32_t)L_76*(int32_t)((int32_t)90)))))%(int32_t)((int32_t)360)));
		Il2CppObject * L_78 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_77);
		NullCheck((Il2CppObject*)L_72);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<ZXing.ResultMetadataType,System.Object>::set_Item(!0,!1) */, IDictionary_2_t2712902339_il2cpp_TypeInfo_var, (Il2CppObject*)L_72, (int32_t)1, (Il2CppObject *)L_78);
	}

IL_01b3:
	{
		Result_t2610723219 * L_79 = V_0;
		NullCheck((BarcodeReaderGeneric_1_t3481712763 *)__this);
		((  void (*) (BarcodeReaderGeneric_1_t3481712763 *, Result_t2610723219 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((BarcodeReaderGeneric_1_t3481712763 *)__this, (Result_t2610723219 *)L_79, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
	}

IL_01ba:
	{
		Result_t2610723219 * L_80 = V_0;
		return L_80;
	}
}
// ZXing.Result ZXing.BarcodeReaderGeneric`1<System.Object>::Decode(T,System.Int32,System.Int32)
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1115308047;
extern Il2CppCodeGenString* _stringLiteral3356930949;
extern const uint32_t BarcodeReaderGeneric_1_Decode_m693721600_MetadataUsageId;
extern "C"  Result_t2610723219 * BarcodeReaderGeneric_1_Decode_m693721600_gshared (BarcodeReaderGeneric_1_t3481712763 * __this, Il2CppObject * ___rawRGB0, int32_t ___width1, int32_t ___height2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BarcodeReaderGeneric_1_Decode_m693721600_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LuminanceSource_t1231523093 * V_0 = NULL;
	{
		NullCheck((BarcodeReaderGeneric_1_t3481712763 *)__this);
		Func_4_t3774219921 * L_0 = ((  Func_4_t3774219921 * (*) (BarcodeReaderGeneric_1_t3481712763 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((BarcodeReaderGeneric_1_t3481712763 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral1115308047, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0013:
	{
		Il2CppObject * L_2 = ___rawRGB0;
		if (L_2)
		{
			goto IL_0026;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_3 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_3, (String_t*)_stringLiteral3356930949, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0026:
	{
		NullCheck((BarcodeReaderGeneric_1_t3481712763 *)__this);
		Func_4_t3774219921 * L_4 = ((  Func_4_t3774219921 * (*) (BarcodeReaderGeneric_1_t3481712763 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((BarcodeReaderGeneric_1_t3481712763 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		Il2CppObject * L_5 = ___rawRGB0;
		int32_t L_6 = ___width1;
		int32_t L_7 = ___height2;
		NullCheck((Func_4_t3774219921 *)L_4);
		LuminanceSource_t1231523093 * L_8 = ((  LuminanceSource_t1231523093 * (*) (Func_4_t3774219921 *, Il2CppObject *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Func_4_t3774219921 *)L_4, (Il2CppObject *)L_5, (int32_t)L_6, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		V_0 = (LuminanceSource_t1231523093 *)L_8;
		LuminanceSource_t1231523093 * L_9 = V_0;
		NullCheck((BarcodeReaderGeneric_1_t3481712763 *)__this);
		Result_t2610723219 * L_10 = VirtFuncInvoker1< Result_t2610723219 *, LuminanceSource_t1231523093 * >::Invoke(4 /* ZXing.Result ZXing.BarcodeReaderGeneric`1<System.Object>::Decode(ZXing.LuminanceSource) */, (BarcodeReaderGeneric_1_t3481712763 *)__this, (LuminanceSource_t1231523093 *)L_9);
		return L_10;
	}
}
// System.Void ZXing.BarcodeReaderGeneric`1<System.Object>::OnResultFound(ZXing.Result)
extern const MethodInfo* Action_1_Invoke_m2317974709_MethodInfo_var;
extern const uint32_t BarcodeReaderGeneric_1_OnResultFound_m1942802843_MetadataUsageId;
extern "C"  void BarcodeReaderGeneric_1_OnResultFound_m1942802843_gshared (BarcodeReaderGeneric_1_t3481712763 * __this, Result_t2610723219 * ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BarcodeReaderGeneric_1_OnResultFound_m1942802843_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t3006539355 * L_0 = (Action_1_t3006539355 *)__this->get_ResultFound_9();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		Action_1_t3006539355 * L_1 = (Action_1_t3006539355 *)__this->get_ResultFound_9();
		Result_t2610723219 * L_2 = ___result0;
		NullCheck((Action_1_t3006539355 *)L_1);
		Action_1_Invoke_m2317974709((Action_1_t3006539355 *)L_1, (Result_t2610723219 *)L_2, /*hidden argument*/Action_1_Invoke_m2317974709_MethodInfo_var);
	}

IL_0014:
	{
		return;
	}
}
// System.Boolean ZXing.BarcodeReaderGeneric`1<System.Object>::get_AutoRotate()
extern "C"  bool BarcodeReaderGeneric_1_get_AutoRotate_m3605742181_gshared (BarcodeReaderGeneric_1_t3481712763 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_U3CAutoRotateU3Ek__BackingField_10();
		return L_0;
	}
}
// System.Void ZXing.BarcodeReaderGeneric`1<System.Object>::set_AutoRotate(System.Boolean)
extern "C"  void BarcodeReaderGeneric_1_set_AutoRotate_m3182146308_gshared (BarcodeReaderGeneric_1_t3481712763 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CAutoRotateU3Ek__BackingField_10(L_0);
		return;
	}
}
// System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer> ZXing.BarcodeReaderGeneric`1<System.Object>::get_CreateBinarizer()
extern "C"  Func_2_t2392815316 * BarcodeReaderGeneric_1_get_CreateBinarizer_m3095086350_gshared (BarcodeReaderGeneric_1_t3481712763 * __this, const MethodInfo* method)
{
	Func_2_t2392815316 * G_B2_0 = NULL;
	Func_2_t2392815316 * G_B1_0 = NULL;
	{
		Func_2_t2392815316 * L_0 = (Func_2_t2392815316 *)__this->get_createBinarizer_0();
		Func_2_t2392815316 * L_1 = (Func_2_t2392815316 *)L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		Func_2_t2392815316 * L_2 = ((BarcodeReaderGeneric_1_t3481712763_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_defaultCreateBinarizer_3();
		G_B2_0 = L_2;
	}

IL_000f:
	{
		return G_B2_0;
	}
}
// System.Func`4<T,System.Int32,System.Int32,ZXing.LuminanceSource> ZXing.BarcodeReaderGeneric`1<System.Object>::get_CreateLuminanceSource()
extern "C"  Func_4_t3774219921 * BarcodeReaderGeneric_1_get_CreateLuminanceSource_m3272449141_gshared (BarcodeReaderGeneric_1_t3481712763 * __this, const MethodInfo* method)
{
	{
		Func_4_t3774219921 * L_0 = (Func_4_t3774219921 *)__this->get_createLuminanceSource_1();
		return L_0;
	}
}
// ZXing.Common.DecodingOptions ZXing.BarcodeReaderGeneric`1<System.Object>::get_Options()
extern Il2CppClass* DecodingOptions_t3608870897_il2cpp_TypeInfo_var;
extern const uint32_t BarcodeReaderGeneric_1_get_Options_m4237311540_MetadataUsageId;
extern "C"  DecodingOptions_t3608870897 * BarcodeReaderGeneric_1_get_Options_m4237311540_gshared (BarcodeReaderGeneric_1_t3481712763 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BarcodeReaderGeneric_1_get_Options_m4237311540_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DecodingOptions_t3608870897 * V_0 = NULL;
	DecodingOptions_t3608870897 * G_B2_0 = NULL;
	DecodingOptions_t3608870897 * G_B1_0 = NULL;
	{
		DecodingOptions_t3608870897 * L_0 = (DecodingOptions_t3608870897 *)__this->get_options_5();
		DecodingOptions_t3608870897 * L_1 = (DecodingOptions_t3608870897 *)L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_0018;
		}
	}
	{
		DecodingOptions_t3608870897 * L_2 = (DecodingOptions_t3608870897 *)il2cpp_codegen_object_new(DecodingOptions_t3608870897_il2cpp_TypeInfo_var);
		DecodingOptions__ctor_m1101142081(L_2, /*hidden argument*/NULL);
		DecodingOptions_t3608870897 * L_3 = (DecodingOptions_t3608870897 *)L_2;
		V_0 = (DecodingOptions_t3608870897 *)L_3;
		__this->set_options_5(L_3);
		DecodingOptions_t3608870897 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_0018:
	{
		return G_B2_0;
	}
}
// ZXing.Reader ZXing.BarcodeReaderGeneric`1<System.Object>::get_Reader()
extern Il2CppClass* MultiFormatReader_t4081038293_il2cpp_TypeInfo_var;
extern const uint32_t BarcodeReaderGeneric_1_get_Reader_m3072341634_MetadataUsageId;
extern "C"  Il2CppObject * BarcodeReaderGeneric_1_get_Reader_m3072341634_gshared (BarcodeReaderGeneric_1_t3481712763 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BarcodeReaderGeneric_1_get_Reader_m3072341634_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * G_B2_0 = NULL;
	Il2CppObject * G_B1_0 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_reader_6();
		Il2CppObject * L_1 = (Il2CppObject *)L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_0018;
		}
	}
	{
		MultiFormatReader_t4081038293 * L_2 = (MultiFormatReader_t4081038293 *)il2cpp_codegen_object_new(MultiFormatReader_t4081038293_il2cpp_TypeInfo_var);
		MultiFormatReader__ctor_m4045637922(L_2, /*hidden argument*/NULL);
		MultiFormatReader_t4081038293 * L_3 = (MultiFormatReader_t4081038293 *)L_2;
		V_0 = (Il2CppObject *)L_3;
		__this->set_reader_6(L_3);
		Il2CppObject * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_0018:
	{
		return G_B2_0;
	}
}
// System.Boolean ZXing.BarcodeReaderGeneric`1<System.Object>::get_TryInverted()
extern "C"  bool BarcodeReaderGeneric_1_get_TryInverted_m1524396567_gshared (BarcodeReaderGeneric_1_t3481712763 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_U3CTryInvertedU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void ZXing.BarcodeReaderGeneric`1<System.Object>::set_TryInverted(System.Boolean)
extern "C"  void BarcodeReaderGeneric_1_set_TryInverted_m4063223910_gshared (BarcodeReaderGeneric_1_t3481712763 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CTryInvertedU3Ek__BackingField_11(L_0);
		return;
	}
}
// ZXing.Binarizer ZXing.BarcodeReaderGeneric`1<System.Object>::<.cctor>b__0(ZXing.LuminanceSource)
extern Il2CppClass* HybridBinarizer_t3517076856_il2cpp_TypeInfo_var;
extern const uint32_t BarcodeReaderGeneric_1_U3C_cctorU3Eb__0_m2217776963_MetadataUsageId;
extern "C"  Binarizer_t1492033400 * BarcodeReaderGeneric_1_U3C_cctorU3Eb__0_m2217776963_gshared (Il2CppObject * __this /* static, unused */, LuminanceSource_t1231523093 * ___luminanceSource0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BarcodeReaderGeneric_1_U3C_cctorU3Eb__0_m2217776963_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LuminanceSource_t1231523093 * L_0 = ___luminanceSource0;
		HybridBinarizer_t3517076856 * L_1 = (HybridBinarizer_t3517076856 *)il2cpp_codegen_object_new(HybridBinarizer_t3517076856_il2cpp_TypeInfo_var);
		HybridBinarizer__ctor_m2380500591(L_1, (LuminanceSource_t1231523093 *)L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// ZXing.LuminanceSource ZXing.BarcodeReaderGeneric`1<System.Object>::<.cctor>b__1(System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat)
extern Il2CppClass* RGBLuminanceSource_t3258519836_il2cpp_TypeInfo_var;
extern const uint32_t BarcodeReaderGeneric_1_U3C_cctorU3Eb__1_m4225005580_MetadataUsageId;
extern "C"  LuminanceSource_t1231523093 * BarcodeReaderGeneric_1_U3C_cctorU3Eb__1_m4225005580_gshared (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___rawBytes0, int32_t ___width1, int32_t ___height2, int32_t ___format3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BarcodeReaderGeneric_1_U3C_cctorU3Eb__1_m4225005580_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t4260760469* L_0 = ___rawBytes0;
		int32_t L_1 = ___width1;
		int32_t L_2 = ___height2;
		int32_t L_3 = ___format3;
		RGBLuminanceSource_t3258519836 * L_4 = (RGBLuminanceSource_t3258519836 *)il2cpp_codegen_object_new(RGBLuminanceSource_t3258519836_il2cpp_TypeInfo_var);
		RGBLuminanceSource__ctor_m3721951397(L_4, (ByteU5BU5D_t4260760469*)L_0, (int32_t)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::add_ValueChanged(System.Action`2<System.Object,System.EventArgs>)
extern Il2CppClass* Action_2_t2663079113_il2cpp_TypeInfo_var;
extern const uint32_t ChangeNotifyDictionary_2_add_ValueChanged_m2227302673_MetadataUsageId;
extern "C"  void ChangeNotifyDictionary_2_add_ValueChanged_m2227302673_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, Action_2_t2663079113 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ChangeNotifyDictionary_2_add_ValueChanged_m2227302673_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_2_t2663079113 * L_0 = (Action_2_t2663079113 *)__this->get_ValueChanged_1();
		Action_2_t2663079113 * L_1 = ___value0;
		Delegate_t3310234105 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, (Delegate_t3310234105 *)L_0, (Delegate_t3310234105 *)L_1, /*hidden argument*/NULL);
		__this->set_ValueChanged_1(((Action_2_t2663079113 *)Castclass(L_2, Action_2_t2663079113_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::remove_ValueChanged(System.Action`2<System.Object,System.EventArgs>)
extern Il2CppClass* Action_2_t2663079113_il2cpp_TypeInfo_var;
extern const uint32_t ChangeNotifyDictionary_2_remove_ValueChanged_m1419530924_MetadataUsageId;
extern "C"  void ChangeNotifyDictionary_2_remove_ValueChanged_m1419530924_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, Action_2_t2663079113 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ChangeNotifyDictionary_2_remove_ValueChanged_m1419530924_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_2_t2663079113 * L_0 = (Action_2_t2663079113 *)__this->get_ValueChanged_1();
		Action_2_t2663079113 * L_1 = ___value0;
		Delegate_t3310234105 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, (Delegate_t3310234105 *)L_0, (Delegate_t3310234105 *)L_1, /*hidden argument*/NULL);
		__this->set_ValueChanged_1(((Action_2_t2663079113 *)Castclass(L_2, Action_2_t2663079113_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::.ctor()
extern "C"  void ChangeNotifyDictionary_2__ctor_m1576909575_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2045888271 * L_0 = (Dictionary_2_t2045888271 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Dictionary_2_t2045888271 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_values_0(L_0);
		return;
	}
}
// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::OnValueChanged()
extern Il2CppClass* EventArgs_t2540831021_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_2_Invoke_m2366895921_MethodInfo_var;
extern const uint32_t ChangeNotifyDictionary_2_OnValueChanged_m2036333919_MetadataUsageId;
extern "C"  void ChangeNotifyDictionary_2_OnValueChanged_m2036333919_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ChangeNotifyDictionary_2_OnValueChanged_m2036333919_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_2_t2663079113 * L_0 = (Action_2_t2663079113 *)__this->get_ValueChanged_1();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		Action_2_t2663079113 * L_1 = (Action_2_t2663079113 *)__this->get_ValueChanged_1();
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t2540831021_il2cpp_TypeInfo_var);
		EventArgs_t2540831021 * L_2 = ((EventArgs_t2540831021_StaticFields*)EventArgs_t2540831021_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		NullCheck((Action_2_t2663079113 *)L_1);
		Action_2_Invoke_m2366895921((Action_2_t2663079113 *)L_1, (Il2CppObject *)__this, (EventArgs_t2540831021 *)L_2, /*hidden argument*/Action_2_Invoke_m2366895921_MethodInfo_var);
	}

IL_0019:
	{
		return;
	}
}
// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::Add(TKey,TValue)
extern "C"  void ChangeNotifyDictionary_2_Add_m3461214606_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_values_0();
		Il2CppObject * L_1 = ___key0;
		Il2CppObject * L_2 = ___value1;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.Object,System.Object>::Add(!0,!1) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0, (Il2CppObject *)L_1, (Il2CppObject *)L_2);
		NullCheck((ChangeNotifyDictionary_2_t1676496957 *)__this);
		((  void (*) (ChangeNotifyDictionary_2_t1676496957 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((ChangeNotifyDictionary_2_t1676496957 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::ContainsKey(TKey)
extern "C"  bool ChangeNotifyDictionary_2_ContainsKey_m512756582_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_values_0();
		Il2CppObject * L_1 = ___key0;
		NullCheck((Il2CppObject*)L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Object,System.Object>::ContainsKey(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Collections.Generic.ICollection`1<TKey> ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::get_Keys()
extern "C"  Il2CppObject* ChangeNotifyDictionary_2_get_Keys_m3793725911_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_values_0();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(6 /* System.Collections.Generic.ICollection`1<!0> System.Collections.Generic.IDictionary`2<System.Object,System.Object>::get_Keys() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::Remove(TKey)
extern "C"  bool ChangeNotifyDictionary_2_Remove_m1365517770_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_values_0();
		Il2CppObject * L_1 = ___key0;
		NullCheck((Il2CppObject*)L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(2 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Object,System.Object>::Remove(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		V_0 = (bool)L_2;
		NullCheck((ChangeNotifyDictionary_2_t1676496957 *)__this);
		((  void (*) (ChangeNotifyDictionary_2_t1676496957 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((ChangeNotifyDictionary_2_t1676496957 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool ChangeNotifyDictionary_2_TryGetValue_m2720374207_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, Il2CppObject * ___key0, Il2CppObject ** ___value1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_values_0();
		Il2CppObject * L_1 = ___key0;
		Il2CppObject ** L_2 = ___value1;
		NullCheck((Il2CppObject*)L_0);
		bool L_3 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject ** >::Invoke(3 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0, (Il2CppObject *)L_1, (Il2CppObject **)L_2);
		return L_3;
	}
}
// TValue ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * ChangeNotifyDictionary_2_get_Item_m1152805902_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_values_0();
		Il2CppObject * L_1 = ___key0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(4 /* !1 System.Collections.Generic.IDictionary`2<System.Object,System.Object>::get_Item(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::set_Item(TKey,TValue)
extern "C"  void ChangeNotifyDictionary_2_set_Item_m986617991_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_values_0();
		Il2CppObject * L_1 = ___key0;
		Il2CppObject * L_2 = ___value1;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.Object,System.Object>::set_Item(!0,!1) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0, (Il2CppObject *)L_1, (Il2CppObject *)L_2);
		NullCheck((ChangeNotifyDictionary_2_t1676496957 *)__this);
		((  void (*) (ChangeNotifyDictionary_2_t1676496957 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((ChangeNotifyDictionary_2_t1676496957 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void ChangeNotifyDictionary_2_Add_m4097002519_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, KeyValuePair_2_t1944668977  ___item0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_values_0();
		KeyValuePair_2_t1944668977  L_1 = ___item0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< KeyValuePair_2_t1944668977  >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Add(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_0, (KeyValuePair_2_t1944668977 )L_1);
		NullCheck((ChangeNotifyDictionary_2_t1676496957 *)__this);
		((  void (*) (ChangeNotifyDictionary_2_t1676496957 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((ChangeNotifyDictionary_2_t1676496957 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::Clear()
extern "C"  void ChangeNotifyDictionary_2_Clear_m3278010162_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_values_0();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(3 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Clear() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_0);
		NullCheck((ChangeNotifyDictionary_2_t1676496957 *)__this);
		((  void (*) (ChangeNotifyDictionary_2_t1676496957 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((ChangeNotifyDictionary_2_t1676496957 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool ChangeNotifyDictionary_2_Contains_m3248197057_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, KeyValuePair_2_t1944668977  ___item0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_values_0();
		KeyValuePair_2_t1944668977  L_1 = ___item0;
		NullCheck((Il2CppObject*)L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, KeyValuePair_2_t1944668977  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Contains(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_0, (KeyValuePair_2_t1944668977 )L_1);
		return L_2;
	}
}
// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void ChangeNotifyDictionary_2_CopyTo_m1239680059_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, KeyValuePair_2U5BU5D_t2483180780* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_values_0();
		KeyValuePair_2U5BU5D_t2483180780* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker2< KeyValuePair_2U5BU5D_t2483180780*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CopyTo(!0[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_0, (KeyValuePair_2U5BU5D_t2483180780*)L_1, (int32_t)L_2);
		return;
	}
}
// System.Int32 ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::get_Count()
extern "C"  int32_t ChangeNotifyDictionary_2_get_Count_m4278201981_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_values_0();
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::get_IsReadOnly()
extern "C"  bool ChangeNotifyDictionary_2_get_IsReadOnly_m90599750_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_values_0();
		NullCheck((Il2CppObject*)L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool ChangeNotifyDictionary_2_Remove_m2592354598_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, KeyValuePair_2_t1944668977  ___item0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_values_0();
		KeyValuePair_2_t1944668977  L_1 = ___item0;
		NullCheck((Il2CppObject*)L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, KeyValuePair_2_t1944668977  >::Invoke(6 /* System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Remove(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_0, (KeyValuePair_2_t1944668977 )L_1);
		V_0 = (bool)L_2;
		NullCheck((ChangeNotifyDictionary_2_t1676496957 *)__this);
		((  void (*) (ChangeNotifyDictionary_2_t1676496957 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((ChangeNotifyDictionary_2_t1676496957 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ChangeNotifyDictionary_2_GetEnumerator_m3718556676_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_values_0();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Collections.IEnumerator ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern Il2CppClass* IEnumerable_t3464557803_il2cpp_TypeInfo_var;
extern const uint32_t ChangeNotifyDictionary_2_System_Collections_IEnumerable_GetEnumerator_m3495645002_MetadataUsageId;
extern "C"  Il2CppObject * ChangeNotifyDictionary_2_System_Collections_IEnumerable_GetEnumerator_m3495645002_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ChangeNotifyDictionary_2_System_Collections_IEnumerable_GetEnumerator_m3495645002_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_values_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t3464557803_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::add_ValueChanged(System.Action`2<System.Object,System.EventArgs>)
extern Il2CppClass* Action_2_t2663079113_il2cpp_TypeInfo_var;
extern const uint32_t ChangeNotifyDictionary_2_add_ValueChanged_m4255839784_MetadataUsageId;
extern "C"  void ChangeNotifyDictionary_2_add_ValueChanged_m4255839784_gshared (ChangeNotifyDictionary_2_t812725187 * __this, Action_2_t2663079113 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ChangeNotifyDictionary_2_add_ValueChanged_m4255839784_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_2_t2663079113 * L_0 = (Action_2_t2663079113 *)__this->get_ValueChanged_1();
		Action_2_t2663079113 * L_1 = ___value0;
		Delegate_t3310234105 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, (Delegate_t3310234105 *)L_0, (Delegate_t3310234105 *)L_1, /*hidden argument*/NULL);
		__this->set_ValueChanged_1(((Action_2_t2663079113 *)Castclass(L_2, Action_2_t2663079113_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::remove_ValueChanged(System.Action`2<System.Object,System.EventArgs>)
extern Il2CppClass* Action_2_t2663079113_il2cpp_TypeInfo_var;
extern const uint32_t ChangeNotifyDictionary_2_remove_ValueChanged_m3378750005_MetadataUsageId;
extern "C"  void ChangeNotifyDictionary_2_remove_ValueChanged_m3378750005_gshared (ChangeNotifyDictionary_2_t812725187 * __this, Action_2_t2663079113 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ChangeNotifyDictionary_2_remove_ValueChanged_m3378750005_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_2_t2663079113 * L_0 = (Action_2_t2663079113 *)__this->get_ValueChanged_1();
		Action_2_t2663079113 * L_1 = ___value0;
		Delegate_t3310234105 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, (Delegate_t3310234105 *)L_0, (Delegate_t3310234105 *)L_1, /*hidden argument*/NULL);
		__this->set_ValueChanged_1(((Action_2_t2663079113 *)Castclass(L_2, Action_2_t2663079113_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::.ctor()
extern "C"  void ChangeNotifyDictionary_2__ctor_m908626654_gshared (ChangeNotifyDictionary_2_t812725187 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1182116501 * L_0 = (Dictionary_2_t1182116501 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Dictionary_2_t1182116501 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_values_0(L_0);
		return;
	}
}
// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::OnValueChanged()
extern Il2CppClass* EventArgs_t2540831021_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_2_Invoke_m2366895921_MethodInfo_var;
extern const uint32_t ChangeNotifyDictionary_2_OnValueChanged_m3164344168_MetadataUsageId;
extern "C"  void ChangeNotifyDictionary_2_OnValueChanged_m3164344168_gshared (ChangeNotifyDictionary_2_t812725187 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ChangeNotifyDictionary_2_OnValueChanged_m3164344168_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_2_t2663079113 * L_0 = (Action_2_t2663079113 *)__this->get_ValueChanged_1();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		Action_2_t2663079113 * L_1 = (Action_2_t2663079113 *)__this->get_ValueChanged_1();
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t2540831021_il2cpp_TypeInfo_var);
		EventArgs_t2540831021 * L_2 = ((EventArgs_t2540831021_StaticFields*)EventArgs_t2540831021_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		NullCheck((Action_2_t2663079113 *)L_1);
		Action_2_Invoke_m2366895921((Action_2_t2663079113 *)L_1, (Il2CppObject *)__this, (EventArgs_t2540831021 *)L_2, /*hidden argument*/Action_2_Invoke_m2366895921_MethodInfo_var);
	}

IL_0019:
	{
		return;
	}
}
// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::Add(TKey,TValue)
extern "C"  void ChangeNotifyDictionary_2_Add_m294257559_gshared (ChangeNotifyDictionary_2_t812725187 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_values_0();
		int32_t L_1 = ___key0;
		Il2CppObject * L_2 = ___value1;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>::Add(!0,!1) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0, (int32_t)L_1, (Il2CppObject *)L_2);
		NullCheck((ChangeNotifyDictionary_2_t812725187 *)__this);
		((  void (*) (ChangeNotifyDictionary_2_t812725187 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((ChangeNotifyDictionary_2_t812725187 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::ContainsKey(TKey)
extern "C"  bool ChangeNotifyDictionary_2_ContainsKey_m3860855781_gshared (ChangeNotifyDictionary_2_t812725187 * __this, int32_t ___key0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_values_0();
		int32_t L_1 = ___key0;
		NullCheck((Il2CppObject*)L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, int32_t >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>::ContainsKey(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.Collections.Generic.ICollection`1<TKey> ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::get_Keys()
extern "C"  Il2CppObject* ChangeNotifyDictionary_2_get_Keys_m2515074822_gshared (ChangeNotifyDictionary_2_t812725187 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_values_0();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(6 /* System.Collections.Generic.ICollection`1<!0> System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>::get_Keys() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::Remove(TKey)
extern "C"  bool ChangeNotifyDictionary_2_Remove_m1932632555_gshared (ChangeNotifyDictionary_2_t812725187 * __this, int32_t ___key0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_values_0();
		int32_t L_1 = ___key0;
		NullCheck((Il2CppObject*)L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, int32_t >::Invoke(2 /* System.Boolean System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>::Remove(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0, (int32_t)L_1);
		V_0 = (bool)L_2;
		NullCheck((ChangeNotifyDictionary_2_t812725187 *)__this);
		((  void (*) (ChangeNotifyDictionary_2_t812725187 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((ChangeNotifyDictionary_2_t812725187 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool ChangeNotifyDictionary_2_TryGetValue_m548970302_gshared (ChangeNotifyDictionary_2_t812725187 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_values_0();
		int32_t L_1 = ___key0;
		Il2CppObject ** L_2 = ___value1;
		NullCheck((Il2CppObject*)L_0);
		bool L_3 = InterfaceFuncInvoker2< bool, int32_t, Il2CppObject ** >::Invoke(3 /* System.Boolean System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>::TryGetValue(!0,!1&) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0, (int32_t)L_1, (Il2CppObject **)L_2);
		return L_3;
	}
}
// TValue ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * ChangeNotifyDictionary_2_get_Item_m3244924311_gshared (ChangeNotifyDictionary_2_t812725187 * __this, int32_t ___key0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_values_0();
		int32_t L_1 = ___key0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(4 /* !1 System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>::get_Item(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::set_Item(TKey,TValue)
extern "C"  void ChangeNotifyDictionary_2_set_Item_m1672045598_gshared (ChangeNotifyDictionary_2_t812725187 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_values_0();
		int32_t L_1 = ___key0;
		Il2CppObject * L_2 = ___value1;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>::set_Item(!0,!1) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0, (int32_t)L_1, (Il2CppObject *)L_2);
		NullCheck((ChangeNotifyDictionary_2_t812725187 *)__this);
		((  void (*) (ChangeNotifyDictionary_2_t812725187 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((ChangeNotifyDictionary_2_t812725187 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void ChangeNotifyDictionary_2_Add_m4247760494_gshared (ChangeNotifyDictionary_2_t812725187 * __this, KeyValuePair_2_t1080897207  ___item0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_values_0();
		KeyValuePair_2_t1080897207  L_1 = ___item0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< KeyValuePair_2_t1080897207  >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<ZXing.DecodeHintType,System.Object>>::Add(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_0, (KeyValuePair_2_t1080897207 )L_1);
		NullCheck((ChangeNotifyDictionary_2_t812725187 *)__this);
		((  void (*) (ChangeNotifyDictionary_2_t812725187 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((ChangeNotifyDictionary_2_t812725187 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::Clear()
extern "C"  void ChangeNotifyDictionary_2_Clear_m2609727241_gshared (ChangeNotifyDictionary_2_t812725187 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_values_0();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(3 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<ZXing.DecodeHintType,System.Object>>::Clear() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_0);
		NullCheck((ChangeNotifyDictionary_2_t812725187 *)__this);
		((  void (*) (ChangeNotifyDictionary_2_t812725187 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((ChangeNotifyDictionary_2_t812725187 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool ChangeNotifyDictionary_2_Contains_m695462754_gshared (ChangeNotifyDictionary_2_t812725187 * __this, KeyValuePair_2_t1080897207  ___item0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_values_0();
		KeyValuePair_2_t1080897207  L_1 = ___item0;
		NullCheck((Il2CppObject*)L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, KeyValuePair_2_t1080897207  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<ZXing.DecodeHintType,System.Object>>::Contains(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_0, (KeyValuePair_2_t1080897207 )L_1);
		return L_2;
	}
}
// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void ChangeNotifyDictionary_2_CopyTo_m2855476946_gshared (ChangeNotifyDictionary_2_t812725187 * __this, KeyValuePair_2U5BU5D_t3618423950* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_values_0();
		KeyValuePair_2U5BU5D_t3618423950* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker2< KeyValuePair_2U5BU5D_t3618423950*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<ZXing.DecodeHintType,System.Object>>::CopyTo(!0[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_0, (KeyValuePair_2U5BU5D_t3618423950*)L_1, (int32_t)L_2);
		return;
	}
}
// System.Int32 ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::get_Count()
extern "C"  int32_t ChangeNotifyDictionary_2_get_Count_m192141488_gshared (ChangeNotifyDictionary_2_t812725187 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_values_0();
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<ZXing.DecodeHintType,System.Object>>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::get_IsReadOnly()
extern "C"  bool ChangeNotifyDictionary_2_get_IsReadOnly_m1306981607_gshared (ChangeNotifyDictionary_2_t812725187 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_values_0();
		NullCheck((Il2CppObject*)L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<ZXing.DecodeHintType,System.Object>>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool ChangeNotifyDictionary_2_Remove_m1718190855_gshared (ChangeNotifyDictionary_2_t812725187 * __this, KeyValuePair_2_t1080897207  ___item0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_values_0();
		KeyValuePair_2_t1080897207  L_1 = ___item0;
		NullCheck((Il2CppObject*)L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, KeyValuePair_2_t1080897207  >::Invoke(6 /* System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<ZXing.DecodeHintType,System.Object>>::Remove(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_0, (KeyValuePair_2_t1080897207 )L_1);
		V_0 = (bool)L_2;
		NullCheck((ChangeNotifyDictionary_2_t812725187 *)__this);
		((  void (*) (ChangeNotifyDictionary_2_t812725187 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((ChangeNotifyDictionary_2_t812725187 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ChangeNotifyDictionary_2_GetEnumerator_m2460357617_gshared (ChangeNotifyDictionary_2_t812725187 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_values_0();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<ZXing.DecodeHintType,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Collections.IEnumerator ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern Il2CppClass* IEnumerable_t3464557803_il2cpp_TypeInfo_var;
extern const uint32_t ChangeNotifyDictionary_2_System_Collections_IEnumerable_GetEnumerator_m2392211331_MetadataUsageId;
extern "C"  Il2CppObject * ChangeNotifyDictionary_2_System_Collections_IEnumerable_GetEnumerator_m2392211331_gshared (ChangeNotifyDictionary_2_t812725187 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ChangeNotifyDictionary_2_System_Collections_IEnumerable_GetEnumerator_m2392211331_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_values_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t3464557803_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
