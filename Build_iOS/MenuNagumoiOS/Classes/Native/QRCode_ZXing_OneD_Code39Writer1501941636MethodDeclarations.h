﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.Code39Writer
struct Code39Writer_t1501941636;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// System.String
struct String_t;
// System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>
struct IDictionary_2_t3297010830;
// System.Boolean[]
struct BooleanU5BU5D_t3456302923;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "QRCode_ZXing_BarcodeFormat4201805817.h"

// ZXing.Common.BitMatrix ZXing.OneD.Code39Writer::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern "C"  BitMatrix_t1058711404 * Code39Writer_encode_m3780439847 (Code39Writer_t1501941636 * __this, String_t* ___contents0, int32_t ___format1, int32_t ___width2, int32_t ___height3, Il2CppObject* ___hints4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean[] ZXing.OneD.Code39Writer::encode(System.String)
extern "C"  BooleanU5BU5D_t3456302923* Code39Writer_encode_m773378937 (Code39Writer_t1501941636 * __this, String_t* ___contents0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.Code39Writer::toIntArray(System.Int32,System.Int32[])
extern "C"  void Code39Writer_toIntArray_m2495442266 (Il2CppObject * __this /* static, unused */, int32_t ___a0, Int32U5BU5D_t3230847821* ___toReturn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.Code39Writer::.ctor()
extern "C"  void Code39Writer__ctor_m2907305759 (Code39Writer_t1501941636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
