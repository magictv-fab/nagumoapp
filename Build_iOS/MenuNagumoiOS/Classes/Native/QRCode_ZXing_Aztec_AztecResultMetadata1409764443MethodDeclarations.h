﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Aztec.AztecResultMetadata
struct AztecResultMetadata_t1409764443;

#include "codegen/il2cpp-codegen.h"

// System.Void ZXing.Aztec.AztecResultMetadata::set_Compact(System.Boolean)
extern "C"  void AztecResultMetadata_set_Compact_m2101161888 (AztecResultMetadata_t1409764443 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Aztec.AztecResultMetadata::set_Datablocks(System.Int32)
extern "C"  void AztecResultMetadata_set_Datablocks_m3124684125 (AztecResultMetadata_t1409764443 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Aztec.AztecResultMetadata::set_Layers(System.Int32)
extern "C"  void AztecResultMetadata_set_Layers_m2938102511 (AztecResultMetadata_t1409764443 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Aztec.AztecResultMetadata::.ctor(System.Boolean,System.Int32,System.Int32)
extern "C"  void AztecResultMetadata__ctor_m4080548508 (AztecResultMetadata_t1409764443 * __this, bool ___compact0, int32_t ___datablocks1, int32_t ___layers2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
