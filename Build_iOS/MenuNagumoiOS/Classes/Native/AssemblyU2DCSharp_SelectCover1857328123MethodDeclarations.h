﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SelectCover
struct SelectCover_t1857328123;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void SelectCover::.ctor()
extern "C"  void SelectCover__ctor_m3740152272 (SelectCover_t1857328123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectCover::.cctor()
extern "C"  void SelectCover__cctor_m3793474525 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectCover::ClickCover(UnityEngine.GameObject)
extern "C"  void SelectCover_ClickCover_m2675328443 (SelectCover_t1857328123 * __this, GameObject_t3674682005 * ___sender0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectCover::ClickEdition()
extern "C"  void SelectCover_ClickEdition_m3243208714 (SelectCover_t1857328123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
