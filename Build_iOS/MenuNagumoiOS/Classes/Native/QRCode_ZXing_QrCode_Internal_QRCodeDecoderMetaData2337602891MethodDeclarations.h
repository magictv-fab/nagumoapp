﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.QrCode.Internal.QRCodeDecoderMetaData
struct QRCodeDecoderMetaData_t2337602891;
// ZXing.ResultPoint[]
struct ResultPointU5BU5D_t1195164344;

#include "codegen/il2cpp-codegen.h"

// System.Void ZXing.QrCode.Internal.QRCodeDecoderMetaData::.ctor(System.Boolean)
extern "C"  void QRCodeDecoderMetaData__ctor_m230092888 (QRCodeDecoderMetaData_t2337602891 * __this, bool ___mirrored0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.QRCodeDecoderMetaData::applyMirroredCorrection(ZXing.ResultPoint[])
extern "C"  void QRCodeDecoderMetaData_applyMirroredCorrection_m1257258430 (QRCodeDecoderMetaData_t2337602891 * __this, ResultPointU5BU5D_t1195164344* ___points0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
