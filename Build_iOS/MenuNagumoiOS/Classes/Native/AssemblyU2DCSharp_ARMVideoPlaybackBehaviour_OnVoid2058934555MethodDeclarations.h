﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARMVideoPlaybackBehaviour/OnVoidEvent
struct OnVoidEvent_t2058934555;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ARMVideoPlaybackBehaviour/OnVoidEvent::.ctor(System.Object,System.IntPtr)
extern "C"  void OnVoidEvent__ctor_m4264103794 (OnVoidEvent_t2058934555 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVideoPlaybackBehaviour/OnVoidEvent::Invoke()
extern "C"  void OnVoidEvent_Invoke_m3673499212 (OnVoidEvent_t2058934555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ARMVideoPlaybackBehaviour/OnVoidEvent::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnVoidEvent_BeginInvoke_m1931676863 (OnVoidEvent_t2058934555 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVideoPlaybackBehaviour/OnVoidEvent::EndInvoke(System.IAsyncResult)
extern "C"  void OnVoidEvent_EndInvoke_m2839677570 (OnVoidEvent_t2058934555 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
