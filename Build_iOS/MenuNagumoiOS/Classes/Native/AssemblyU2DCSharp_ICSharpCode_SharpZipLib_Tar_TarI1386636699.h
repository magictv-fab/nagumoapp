﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t4260760469;
// ICSharpCode.SharpZipLib.Tar.TarBuffer
struct TarBuffer_t2823691623;
// ICSharpCode.SharpZipLib.Tar.TarEntry
struct TarEntry_t762185187;
// ICSharpCode.SharpZipLib.Tar.TarInputStream/IEntryFactory
struct IEntryFactory_t441050801;
// System.IO.Stream
struct Stream_t1561764144;

#include "mscorlib_System_IO_Stream1561764144.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Tar.TarInputStream
struct  TarInputStream_t1386636699  : public Stream_t1561764144
{
public:
	// System.Boolean ICSharpCode.SharpZipLib.Tar.TarInputStream::hasHitEOF
	bool ___hasHitEOF_2;
	// System.Int64 ICSharpCode.SharpZipLib.Tar.TarInputStream::entrySize
	int64_t ___entrySize_3;
	// System.Int64 ICSharpCode.SharpZipLib.Tar.TarInputStream::entryOffset
	int64_t ___entryOffset_4;
	// System.Byte[] ICSharpCode.SharpZipLib.Tar.TarInputStream::readBuffer
	ByteU5BU5D_t4260760469* ___readBuffer_5;
	// ICSharpCode.SharpZipLib.Tar.TarBuffer ICSharpCode.SharpZipLib.Tar.TarInputStream::tarBuffer
	TarBuffer_t2823691623 * ___tarBuffer_6;
	// ICSharpCode.SharpZipLib.Tar.TarEntry ICSharpCode.SharpZipLib.Tar.TarInputStream::currentEntry
	TarEntry_t762185187 * ___currentEntry_7;
	// ICSharpCode.SharpZipLib.Tar.TarInputStream/IEntryFactory ICSharpCode.SharpZipLib.Tar.TarInputStream::entryFactory
	Il2CppObject * ___entryFactory_8;
	// System.IO.Stream ICSharpCode.SharpZipLib.Tar.TarInputStream::inputStream
	Stream_t1561764144 * ___inputStream_9;

public:
	inline static int32_t get_offset_of_hasHitEOF_2() { return static_cast<int32_t>(offsetof(TarInputStream_t1386636699, ___hasHitEOF_2)); }
	inline bool get_hasHitEOF_2() const { return ___hasHitEOF_2; }
	inline bool* get_address_of_hasHitEOF_2() { return &___hasHitEOF_2; }
	inline void set_hasHitEOF_2(bool value)
	{
		___hasHitEOF_2 = value;
	}

	inline static int32_t get_offset_of_entrySize_3() { return static_cast<int32_t>(offsetof(TarInputStream_t1386636699, ___entrySize_3)); }
	inline int64_t get_entrySize_3() const { return ___entrySize_3; }
	inline int64_t* get_address_of_entrySize_3() { return &___entrySize_3; }
	inline void set_entrySize_3(int64_t value)
	{
		___entrySize_3 = value;
	}

	inline static int32_t get_offset_of_entryOffset_4() { return static_cast<int32_t>(offsetof(TarInputStream_t1386636699, ___entryOffset_4)); }
	inline int64_t get_entryOffset_4() const { return ___entryOffset_4; }
	inline int64_t* get_address_of_entryOffset_4() { return &___entryOffset_4; }
	inline void set_entryOffset_4(int64_t value)
	{
		___entryOffset_4 = value;
	}

	inline static int32_t get_offset_of_readBuffer_5() { return static_cast<int32_t>(offsetof(TarInputStream_t1386636699, ___readBuffer_5)); }
	inline ByteU5BU5D_t4260760469* get_readBuffer_5() const { return ___readBuffer_5; }
	inline ByteU5BU5D_t4260760469** get_address_of_readBuffer_5() { return &___readBuffer_5; }
	inline void set_readBuffer_5(ByteU5BU5D_t4260760469* value)
	{
		___readBuffer_5 = value;
		Il2CppCodeGenWriteBarrier(&___readBuffer_5, value);
	}

	inline static int32_t get_offset_of_tarBuffer_6() { return static_cast<int32_t>(offsetof(TarInputStream_t1386636699, ___tarBuffer_6)); }
	inline TarBuffer_t2823691623 * get_tarBuffer_6() const { return ___tarBuffer_6; }
	inline TarBuffer_t2823691623 ** get_address_of_tarBuffer_6() { return &___tarBuffer_6; }
	inline void set_tarBuffer_6(TarBuffer_t2823691623 * value)
	{
		___tarBuffer_6 = value;
		Il2CppCodeGenWriteBarrier(&___tarBuffer_6, value);
	}

	inline static int32_t get_offset_of_currentEntry_7() { return static_cast<int32_t>(offsetof(TarInputStream_t1386636699, ___currentEntry_7)); }
	inline TarEntry_t762185187 * get_currentEntry_7() const { return ___currentEntry_7; }
	inline TarEntry_t762185187 ** get_address_of_currentEntry_7() { return &___currentEntry_7; }
	inline void set_currentEntry_7(TarEntry_t762185187 * value)
	{
		___currentEntry_7 = value;
		Il2CppCodeGenWriteBarrier(&___currentEntry_7, value);
	}

	inline static int32_t get_offset_of_entryFactory_8() { return static_cast<int32_t>(offsetof(TarInputStream_t1386636699, ___entryFactory_8)); }
	inline Il2CppObject * get_entryFactory_8() const { return ___entryFactory_8; }
	inline Il2CppObject ** get_address_of_entryFactory_8() { return &___entryFactory_8; }
	inline void set_entryFactory_8(Il2CppObject * value)
	{
		___entryFactory_8 = value;
		Il2CppCodeGenWriteBarrier(&___entryFactory_8, value);
	}

	inline static int32_t get_offset_of_inputStream_9() { return static_cast<int32_t>(offsetof(TarInputStream_t1386636699, ___inputStream_9)); }
	inline Stream_t1561764144 * get_inputStream_9() const { return ___inputStream_9; }
	inline Stream_t1561764144 ** get_address_of_inputStream_9() { return &___inputStream_9; }
	inline void set_inputStream_9(Stream_t1561764144 * value)
	{
		___inputStream_9 = value;
		Il2CppCodeGenWriteBarrier(&___inputStream_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
