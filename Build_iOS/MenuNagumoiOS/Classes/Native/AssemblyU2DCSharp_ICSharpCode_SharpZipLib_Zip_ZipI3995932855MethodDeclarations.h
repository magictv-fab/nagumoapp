﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.ZipInputStream
struct ZipInputStream_t3995932855;
// System.IO.Stream
struct Stream_t1561764144;
// System.String
struct String_t;
// ICSharpCode.SharpZipLib.Zip.ZipEntry
struct ZipEntry_t3141689087;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "mscorlib_System_String7231557.h"

// System.Void ICSharpCode.SharpZipLib.Zip.ZipInputStream::.ctor(System.IO.Stream)
extern "C"  void ZipInputStream__ctor_m2024531307 (ZipInputStream_t3995932855 * __this, Stream_t1561764144 * ___baseInputStream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipInputStream::.ctor(System.IO.Stream,System.Int32)
extern "C"  void ZipInputStream__ctor_m4050033996 (ZipInputStream_t3995932855 * __this, Stream_t1561764144 * ___baseInputStream0, int32_t ___bufferSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Zip.ZipInputStream::get_Password()
extern "C"  String_t* ZipInputStream_get_Password_m4246018487 (ZipInputStream_t3995932855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipInputStream::set_Password(System.String)
extern "C"  void ZipInputStream_set_Password_m222908218 (ZipInputStream_t3995932855 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipInputStream::get_CanDecompressEntry()
extern "C"  bool ZipInputStream_get_CanDecompressEntry_m580372940 (ZipInputStream_t3995932855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Zip.ZipEntry ICSharpCode.SharpZipLib.Zip.ZipInputStream::GetNextEntry()
extern "C"  ZipEntry_t3141689087 * ZipInputStream_GetNextEntry_m1434092775 (ZipInputStream_t3995932855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipInputStream::ReadDataDescriptor()
extern "C"  void ZipInputStream_ReadDataDescriptor_m815065055 (ZipInputStream_t3995932855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipInputStream::CompleteCloseEntry(System.Boolean)
extern "C"  void ZipInputStream_CompleteCloseEntry_m2727278746 (ZipInputStream_t3995932855 * __this, bool ___testCrc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipInputStream::CloseEntry()
extern "C"  void ZipInputStream_CloseEntry_m167695242 (ZipInputStream_t3995932855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream::get_Available()
extern "C"  int32_t ZipInputStream_get_Available_m4193186116 (ZipInputStream_t3995932855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipInputStream::get_Length()
extern "C"  int64_t ZipInputStream_get_Length_m841772398 (ZipInputStream_t3995932855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream::ReadByte()
extern "C"  int32_t ZipInputStream_ReadByte_m3871044668 (ZipInputStream_t3995932855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream::ReadingNotAvailable(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t ZipInputStream_ReadingNotAvailable_m2121841123 (ZipInputStream_t3995932855 * __this, ByteU5BU5D_t4260760469* ___destination0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream::ReadingNotSupported(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t ZipInputStream_ReadingNotSupported_m4019656094 (ZipInputStream_t3995932855 * __this, ByteU5BU5D_t4260760469* ___destination0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream::InitialRead(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t ZipInputStream_InitialRead_m1081060555 (ZipInputStream_t3995932855 * __this, ByteU5BU5D_t4260760469* ___destination0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t ZipInputStream_Read_m448918229 (ZipInputStream_t3995932855 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream::BodyRead(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t ZipInputStream_BodyRead_m3908107987 (ZipInputStream_t3995932855 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipInputStream::Close()
extern "C"  void ZipInputStream_Close_m761621482 (ZipInputStream_t3995932855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
