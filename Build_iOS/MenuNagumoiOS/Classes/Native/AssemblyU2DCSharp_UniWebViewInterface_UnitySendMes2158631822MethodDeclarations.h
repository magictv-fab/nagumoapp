﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniWebViewInterface/UnitySendMessageDelegate
struct UnitySendMessageDelegate_t2158631822;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void UniWebViewInterface/UnitySendMessageDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void UnitySendMessageDelegate__ctor_m2310778165 (UnitySendMessageDelegate_t2158631822 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface/UnitySendMessageDelegate::Invoke(System.IntPtr,System.IntPtr,System.IntPtr)
extern "C"  void UnitySendMessageDelegate_Invoke_m530893825 (UnitySendMessageDelegate_t2158631822 * __this, IntPtr_t ___objectName0, IntPtr_t ___methodName1, IntPtr_t ___parameter2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult UniWebViewInterface/UnitySendMessageDelegate::BeginInvoke(System.IntPtr,System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnitySendMessageDelegate_BeginInvoke_m4088831082 (UnitySendMessageDelegate_t2158631822 * __this, IntPtr_t ___objectName0, IntPtr_t ___methodName1, IntPtr_t ___parameter2, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewInterface/UnitySendMessageDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void UnitySendMessageDelegate_EndInvoke_m2063073733 (UnitySendMessageDelegate_t2158631822 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
