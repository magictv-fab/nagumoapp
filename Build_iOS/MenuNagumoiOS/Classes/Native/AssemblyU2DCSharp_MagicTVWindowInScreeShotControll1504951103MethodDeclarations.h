﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTVWindowInScreeShotControllerScript
struct MagicTVWindowInScreeShotControllerScript_t1504951103;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTVWindowInScreeShotControllerScript::.ctor()
extern "C"  void MagicTVWindowInScreeShotControllerScript__ctor_m566483516 (MagicTVWindowInScreeShotControllerScript_t1504951103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
