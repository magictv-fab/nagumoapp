﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Char,ZXing.Aztec.Internal.Decoder/Table>
struct ValueCollection_t4093612683;
// System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>
struct Dictionary_2_t1098039674;
// System.Collections.Generic.IEnumerator`1<ZXing.Aztec.Internal.Decoder/Table>
struct IEnumerator_1_t2919343562;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// ZXing.Aztec.Internal.Decoder/Table[]
struct TableU5BU5D_t1820856876;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Aztec_Internal_Decoder_Table1007478513.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3324840378.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Char,ZXing.Aztec.Internal.Decoder/Table>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m3768808702_gshared (ValueCollection_t4093612683 * __this, Dictionary_2_t1098039674 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m3768808702(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t4093612683 *, Dictionary_2_t1098039674 *, const MethodInfo*))ValueCollection__ctor_m3768808702_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m129576820_gshared (ValueCollection_t4093612683 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m129576820(__this, ___item0, method) ((  void (*) (ValueCollection_t4093612683 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m129576820_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3631806013_gshared (ValueCollection_t4093612683 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3631806013(__this, method) ((  void (*) (ValueCollection_t4093612683 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3631806013_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3694060534_gshared (ValueCollection_t4093612683 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3694060534(__this, ___item0, method) ((  bool (*) (ValueCollection_t4093612683 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3694060534_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2990309915_gshared (ValueCollection_t4093612683 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2990309915(__this, ___item0, method) ((  bool (*) (ValueCollection_t4093612683 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2990309915_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1312733565_gshared (ValueCollection_t4093612683 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1312733565(__this, method) ((  Il2CppObject* (*) (ValueCollection_t4093612683 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1312733565_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m754102017_gshared (ValueCollection_t4093612683 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m754102017(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t4093612683 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m754102017_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1710837840_gshared (ValueCollection_t4093612683 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1710837840(__this, method) ((  Il2CppObject * (*) (ValueCollection_t4093612683 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1710837840_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1969236393_gshared (ValueCollection_t4093612683 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1969236393(__this, method) ((  bool (*) (ValueCollection_t4093612683 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1969236393_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m551609609_gshared (ValueCollection_t4093612683 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m551609609(__this, method) ((  bool (*) (ValueCollection_t4093612683 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m551609609_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m1333596859_gshared (ValueCollection_t4093612683 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1333596859(__this, method) ((  Il2CppObject * (*) (ValueCollection_t4093612683 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1333596859_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Char,ZXing.Aztec.Internal.Decoder/Table>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m3086903749_gshared (ValueCollection_t4093612683 * __this, TableU5BU5D_t1820856876* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m3086903749(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t4093612683 *, TableU5BU5D_t1820856876*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m3086903749_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Char,ZXing.Aztec.Internal.Decoder/Table>::GetEnumerator()
extern "C"  Enumerator_t3324840378  ValueCollection_GetEnumerator_m3806773102_gshared (ValueCollection_t4093612683 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m3806773102(__this, method) ((  Enumerator_t3324840378  (*) (ValueCollection_t4093612683 *, const MethodInfo*))ValueCollection_GetEnumerator_m3806773102_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Char,ZXing.Aztec.Internal.Decoder/Table>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m848587843_gshared (ValueCollection_t4093612683 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m848587843(__this, method) ((  int32_t (*) (ValueCollection_t4093612683 *, const MethodInfo*))ValueCollection_get_Count_m848587843_gshared)(__this, method)
