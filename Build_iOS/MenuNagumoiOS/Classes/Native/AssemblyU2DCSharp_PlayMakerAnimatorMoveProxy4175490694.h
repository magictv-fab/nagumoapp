﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerAnimatorMoveProxy
struct  PlayMakerAnimatorMoveProxy_t4175490694  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean PlayMakerAnimatorMoveProxy::applyRootMotion
	bool ___applyRootMotion_2;
	// System.Action PlayMakerAnimatorMoveProxy::OnAnimatorMoveEvent
	Action_t3771233898 * ___OnAnimatorMoveEvent_3;

public:
	inline static int32_t get_offset_of_applyRootMotion_2() { return static_cast<int32_t>(offsetof(PlayMakerAnimatorMoveProxy_t4175490694, ___applyRootMotion_2)); }
	inline bool get_applyRootMotion_2() const { return ___applyRootMotion_2; }
	inline bool* get_address_of_applyRootMotion_2() { return &___applyRootMotion_2; }
	inline void set_applyRootMotion_2(bool value)
	{
		___applyRootMotion_2 = value;
	}

	inline static int32_t get_offset_of_OnAnimatorMoveEvent_3() { return static_cast<int32_t>(offsetof(PlayMakerAnimatorMoveProxy_t4175490694, ___OnAnimatorMoveEvent_3)); }
	inline Action_t3771233898 * get_OnAnimatorMoveEvent_3() const { return ___OnAnimatorMoveEvent_3; }
	inline Action_t3771233898 ** get_address_of_OnAnimatorMoveEvent_3() { return &___OnAnimatorMoveEvent_3; }
	inline void set_OnAnimatorMoveEvent_3(Action_t3771233898 * value)
	{
		___OnAnimatorMoveEvent_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnAnimatorMoveEvent_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
