﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Comment
struct  Comment_t2764299885  : public FsmStateAction_t2366529033
{
public:
	// System.String HutongGames.PlayMaker.Actions.Comment::comment
	String_t* ___comment_9;

public:
	inline static int32_t get_offset_of_comment_9() { return static_cast<int32_t>(offsetof(Comment_t2764299885, ___comment_9)); }
	inline String_t* get_comment_9() const { return ___comment_9; }
	inline String_t** get_address_of_comment_9() { return &___comment_9; }
	inline void set_comment_9(String_t* value)
	{
		___comment_9 = value;
		Il2CppCodeGenWriteBarrier(&___comment_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
