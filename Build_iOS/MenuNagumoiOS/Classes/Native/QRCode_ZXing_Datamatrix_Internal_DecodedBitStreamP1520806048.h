﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Char[]
struct CharU5BU5D_t3324145743;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Datamatrix.Internal.DecodedBitStreamParser
struct  DecodedBitStreamParser_t1520806048  : public Il2CppObject
{
public:

public:
};

struct DecodedBitStreamParser_t1520806048_StaticFields
{
public:
	// System.Char[] ZXing.Datamatrix.Internal.DecodedBitStreamParser::C40_BASIC_SET_CHARS
	CharU5BU5D_t3324145743* ___C40_BASIC_SET_CHARS_0;
	// System.Char[] ZXing.Datamatrix.Internal.DecodedBitStreamParser::C40_SHIFT2_SET_CHARS
	CharU5BU5D_t3324145743* ___C40_SHIFT2_SET_CHARS_1;
	// System.Char[] ZXing.Datamatrix.Internal.DecodedBitStreamParser::TEXT_BASIC_SET_CHARS
	CharU5BU5D_t3324145743* ___TEXT_BASIC_SET_CHARS_2;
	// System.Char[] ZXing.Datamatrix.Internal.DecodedBitStreamParser::TEXT_SHIFT2_SET_CHARS
	CharU5BU5D_t3324145743* ___TEXT_SHIFT2_SET_CHARS_3;
	// System.Char[] ZXing.Datamatrix.Internal.DecodedBitStreamParser::TEXT_SHIFT3_SET_CHARS
	CharU5BU5D_t3324145743* ___TEXT_SHIFT3_SET_CHARS_4;

public:
	inline static int32_t get_offset_of_C40_BASIC_SET_CHARS_0() { return static_cast<int32_t>(offsetof(DecodedBitStreamParser_t1520806048_StaticFields, ___C40_BASIC_SET_CHARS_0)); }
	inline CharU5BU5D_t3324145743* get_C40_BASIC_SET_CHARS_0() const { return ___C40_BASIC_SET_CHARS_0; }
	inline CharU5BU5D_t3324145743** get_address_of_C40_BASIC_SET_CHARS_0() { return &___C40_BASIC_SET_CHARS_0; }
	inline void set_C40_BASIC_SET_CHARS_0(CharU5BU5D_t3324145743* value)
	{
		___C40_BASIC_SET_CHARS_0 = value;
		Il2CppCodeGenWriteBarrier(&___C40_BASIC_SET_CHARS_0, value);
	}

	inline static int32_t get_offset_of_C40_SHIFT2_SET_CHARS_1() { return static_cast<int32_t>(offsetof(DecodedBitStreamParser_t1520806048_StaticFields, ___C40_SHIFT2_SET_CHARS_1)); }
	inline CharU5BU5D_t3324145743* get_C40_SHIFT2_SET_CHARS_1() const { return ___C40_SHIFT2_SET_CHARS_1; }
	inline CharU5BU5D_t3324145743** get_address_of_C40_SHIFT2_SET_CHARS_1() { return &___C40_SHIFT2_SET_CHARS_1; }
	inline void set_C40_SHIFT2_SET_CHARS_1(CharU5BU5D_t3324145743* value)
	{
		___C40_SHIFT2_SET_CHARS_1 = value;
		Il2CppCodeGenWriteBarrier(&___C40_SHIFT2_SET_CHARS_1, value);
	}

	inline static int32_t get_offset_of_TEXT_BASIC_SET_CHARS_2() { return static_cast<int32_t>(offsetof(DecodedBitStreamParser_t1520806048_StaticFields, ___TEXT_BASIC_SET_CHARS_2)); }
	inline CharU5BU5D_t3324145743* get_TEXT_BASIC_SET_CHARS_2() const { return ___TEXT_BASIC_SET_CHARS_2; }
	inline CharU5BU5D_t3324145743** get_address_of_TEXT_BASIC_SET_CHARS_2() { return &___TEXT_BASIC_SET_CHARS_2; }
	inline void set_TEXT_BASIC_SET_CHARS_2(CharU5BU5D_t3324145743* value)
	{
		___TEXT_BASIC_SET_CHARS_2 = value;
		Il2CppCodeGenWriteBarrier(&___TEXT_BASIC_SET_CHARS_2, value);
	}

	inline static int32_t get_offset_of_TEXT_SHIFT2_SET_CHARS_3() { return static_cast<int32_t>(offsetof(DecodedBitStreamParser_t1520806048_StaticFields, ___TEXT_SHIFT2_SET_CHARS_3)); }
	inline CharU5BU5D_t3324145743* get_TEXT_SHIFT2_SET_CHARS_3() const { return ___TEXT_SHIFT2_SET_CHARS_3; }
	inline CharU5BU5D_t3324145743** get_address_of_TEXT_SHIFT2_SET_CHARS_3() { return &___TEXT_SHIFT2_SET_CHARS_3; }
	inline void set_TEXT_SHIFT2_SET_CHARS_3(CharU5BU5D_t3324145743* value)
	{
		___TEXT_SHIFT2_SET_CHARS_3 = value;
		Il2CppCodeGenWriteBarrier(&___TEXT_SHIFT2_SET_CHARS_3, value);
	}

	inline static int32_t get_offset_of_TEXT_SHIFT3_SET_CHARS_4() { return static_cast<int32_t>(offsetof(DecodedBitStreamParser_t1520806048_StaticFields, ___TEXT_SHIFT3_SET_CHARS_4)); }
	inline CharU5BU5D_t3324145743* get_TEXT_SHIFT3_SET_CHARS_4() const { return ___TEXT_SHIFT3_SET_CHARS_4; }
	inline CharU5BU5D_t3324145743** get_address_of_TEXT_SHIFT3_SET_CHARS_4() { return &___TEXT_SHIFT3_SET_CHARS_4; }
	inline void set_TEXT_SHIFT3_SET_CHARS_4(CharU5BU5D_t3324145743* value)
	{
		___TEXT_SHIFT3_SET_CHARS_4 = value;
		Il2CppCodeGenWriteBarrier(&___TEXT_SHIFT3_SET_CHARS_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
