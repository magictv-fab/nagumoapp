﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.globals.events.UserEvents/OnConfirmEventHandler
struct OnConfirmEventHandler_t3783866142;
// System.Object
struct Il2CppObject;
// MagicTV.vo.result.ConfirmResultVO
struct ConfirmResultVO_t1731700412;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_MagicTV_vo_result_ConfirmResultV1731700412.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void MagicTV.globals.events.UserEvents/OnConfirmEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnConfirmEventHandler__ctor_m330011125 (OnConfirmEventHandler_t3783866142 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.UserEvents/OnConfirmEventHandler::Invoke(MagicTV.vo.result.ConfirmResultVO)
extern "C"  void OnConfirmEventHandler_Invoke_m915236072 (OnConfirmEventHandler_t3783866142 * __this, ConfirmResultVO_t1731700412 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult MagicTV.globals.events.UserEvents/OnConfirmEventHandler::BeginInvoke(MagicTV.vo.result.ConfirmResultVO,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnConfirmEventHandler_BeginInvoke_m497673259 (OnConfirmEventHandler_t3783866142 * __this, ConfirmResultVO_t1731700412 * ___p0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.UserEvents/OnConfirmEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnConfirmEventHandler_EndInvoke_m3424142981 (OnConfirmEventHandler_t3783866142 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
