﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PedidoAcougue/<LoadLoadeds>c__Iterator71
struct U3CLoadLoadedsU3Ec__Iterator71_t4226752574;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PedidoAcougue/<LoadLoadeds>c__Iterator71::.ctor()
extern "C"  void U3CLoadLoadedsU3Ec__Iterator71__ctor_m3611337757 (U3CLoadLoadedsU3Ec__Iterator71_t4226752574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PedidoAcougue/<LoadLoadeds>c__Iterator71::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadLoadedsU3Ec__Iterator71_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1375373535 (U3CLoadLoadedsU3Ec__Iterator71_t4226752574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PedidoAcougue/<LoadLoadeds>c__Iterator71::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadLoadedsU3Ec__Iterator71_System_Collections_IEnumerator_get_Current_m272640627 (U3CLoadLoadedsU3Ec__Iterator71_t4226752574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PedidoAcougue/<LoadLoadeds>c__Iterator71::MoveNext()
extern "C"  bool U3CLoadLoadedsU3Ec__Iterator71_MoveNext_m3687218335 (U3CLoadLoadedsU3Ec__Iterator71_t4226752574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PedidoAcougue/<LoadLoadeds>c__Iterator71::Dispose()
extern "C"  void U3CLoadLoadedsU3Ec__Iterator71_Dispose_m60292378 (U3CLoadLoadedsU3Ec__Iterator71_t4226752574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PedidoAcougue/<LoadLoadeds>c__Iterator71::Reset()
extern "C"  void U3CLoadLoadedsU3Ec__Iterator71_Reset_m1257770698 (U3CLoadLoadedsU3Ec__Iterator71_t4226752574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
