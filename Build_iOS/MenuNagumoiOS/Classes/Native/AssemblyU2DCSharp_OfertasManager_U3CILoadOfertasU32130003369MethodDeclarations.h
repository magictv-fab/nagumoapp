﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertasManager/<ILoadOfertas>c__Iterator6A
struct U3CILoadOfertasU3Ec__Iterator6A_t2130003369;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OfertasManager/<ILoadOfertas>c__Iterator6A::.ctor()
extern "C"  void U3CILoadOfertasU3Ec__Iterator6A__ctor_m3026796818 (U3CILoadOfertasU3Ec__Iterator6A_t2130003369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManager/<ILoadOfertas>c__Iterator6A::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CILoadOfertasU3Ec__Iterator6A_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1589522890 (U3CILoadOfertasU3Ec__Iterator6A_t2130003369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManager/<ILoadOfertas>c__Iterator6A::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CILoadOfertasU3Ec__Iterator6A_System_Collections_IEnumerator_get_Current_m1653672286 (U3CILoadOfertasU3Ec__Iterator6A_t2130003369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OfertasManager/<ILoadOfertas>c__Iterator6A::MoveNext()
extern "C"  bool U3CILoadOfertasU3Ec__Iterator6A_MoveNext_m1694196362 (U3CILoadOfertasU3Ec__Iterator6A_t2130003369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManager/<ILoadOfertas>c__Iterator6A::Dispose()
extern "C"  void U3CILoadOfertasU3Ec__Iterator6A_Dispose_m957165775 (U3CILoadOfertasU3Ec__Iterator6A_t2130003369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManager/<ILoadOfertas>c__Iterator6A::Reset()
extern "C"  void U3CILoadOfertasU3Ec__Iterator6A_Reset_m673229759 (U3CILoadOfertasU3Ec__Iterator6A_t2130003369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
