﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.utils.cron.EasyTimerDisposable/<SetInterval>c__AnonStorey92
struct U3CSetIntervalU3Ec__AnonStorey92_t619398154;
// System.Object
struct Il2CppObject;
// System.Timers.ElapsedEventArgs
struct ElapsedEventArgs_t2035959611;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "System_System_Timers_ElapsedEventArgs2035959611.h"

// System.Void ARM.utils.cron.EasyTimerDisposable/<SetInterval>c__AnonStorey92::.ctor()
extern "C"  void U3CSetIntervalU3Ec__AnonStorey92__ctor_m2894528161 (U3CSetIntervalU3Ec__AnonStorey92_t619398154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.cron.EasyTimerDisposable/<SetInterval>c__AnonStorey92::<>m__16(System.Object,System.Timers.ElapsedEventArgs)
extern "C"  void U3CSetIntervalU3Ec__AnonStorey92_U3CU3Em__16_m2190094533 (U3CSetIntervalU3Ec__AnonStorey92_t619398154 * __this, Il2CppObject * ___source0, ElapsedEventArgs_t2035959611 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
