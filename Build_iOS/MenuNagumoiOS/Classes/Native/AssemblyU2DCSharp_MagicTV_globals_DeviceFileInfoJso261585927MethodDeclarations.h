﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.globals.DeviceFileInfoJson/<getFileById>c__AnonStorey9E
struct U3CgetFileByIdU3Ec__AnonStorey9E_t261585927;
// MagicTV.vo.FileVO
struct FileVO_t1935348659;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_vo_FileVO1935348659.h"

// System.Void MagicTV.globals.DeviceFileInfoJson/<getFileById>c__AnonStorey9E::.ctor()
extern "C"  void U3CgetFileByIdU3Ec__AnonStorey9E__ctor_m926774084 (U3CgetFileByIdU3Ec__AnonStorey9E_t261585927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicTV.globals.DeviceFileInfoJson/<getFileById>c__AnonStorey9E::<>m__3D(MagicTV.vo.FileVO)
extern "C"  bool U3CgetFileByIdU3Ec__AnonStorey9E_U3CU3Em__3D_m1120962581 (U3CgetFileByIdU3Ec__AnonStorey9E_t261585927 * __this, FileVO_t1935348659 * ___f0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
