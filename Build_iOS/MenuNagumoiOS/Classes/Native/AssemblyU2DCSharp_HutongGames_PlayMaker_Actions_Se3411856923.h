﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// PlayMakerAnimatorMoveProxy
struct PlayMakerAnimatorMoveProxy_t4175490694;
// UnityEngine.Animator
struct Animator_t2776330603;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAnimatorFloat
struct  SetAnimatorFloat_t3411856923  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAnimatorFloat::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetAnimatorFloat::parameter
	FsmString_t952858651 * ___parameter_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetAnimatorFloat::Value
	FsmFloat_t2134102846 * ___Value_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetAnimatorFloat::dampTime
	FsmFloat_t2134102846 * ___dampTime_12;
	// System.Boolean HutongGames.PlayMaker.Actions.SetAnimatorFloat::everyFrame
	bool ___everyFrame_13;
	// PlayMakerAnimatorMoveProxy HutongGames.PlayMaker.Actions.SetAnimatorFloat::_animatorProxy
	PlayMakerAnimatorMoveProxy_t4175490694 * ____animatorProxy_14;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.SetAnimatorFloat::_animator
	Animator_t2776330603 * ____animator_15;
	// System.Int32 HutongGames.PlayMaker.Actions.SetAnimatorFloat::_paramID
	int32_t ____paramID_16;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(SetAnimatorFloat_t3411856923, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_parameter_10() { return static_cast<int32_t>(offsetof(SetAnimatorFloat_t3411856923, ___parameter_10)); }
	inline FsmString_t952858651 * get_parameter_10() const { return ___parameter_10; }
	inline FsmString_t952858651 ** get_address_of_parameter_10() { return &___parameter_10; }
	inline void set_parameter_10(FsmString_t952858651 * value)
	{
		___parameter_10 = value;
		Il2CppCodeGenWriteBarrier(&___parameter_10, value);
	}

	inline static int32_t get_offset_of_Value_11() { return static_cast<int32_t>(offsetof(SetAnimatorFloat_t3411856923, ___Value_11)); }
	inline FsmFloat_t2134102846 * get_Value_11() const { return ___Value_11; }
	inline FsmFloat_t2134102846 ** get_address_of_Value_11() { return &___Value_11; }
	inline void set_Value_11(FsmFloat_t2134102846 * value)
	{
		___Value_11 = value;
		Il2CppCodeGenWriteBarrier(&___Value_11, value);
	}

	inline static int32_t get_offset_of_dampTime_12() { return static_cast<int32_t>(offsetof(SetAnimatorFloat_t3411856923, ___dampTime_12)); }
	inline FsmFloat_t2134102846 * get_dampTime_12() const { return ___dampTime_12; }
	inline FsmFloat_t2134102846 ** get_address_of_dampTime_12() { return &___dampTime_12; }
	inline void set_dampTime_12(FsmFloat_t2134102846 * value)
	{
		___dampTime_12 = value;
		Il2CppCodeGenWriteBarrier(&___dampTime_12, value);
	}

	inline static int32_t get_offset_of_everyFrame_13() { return static_cast<int32_t>(offsetof(SetAnimatorFloat_t3411856923, ___everyFrame_13)); }
	inline bool get_everyFrame_13() const { return ___everyFrame_13; }
	inline bool* get_address_of_everyFrame_13() { return &___everyFrame_13; }
	inline void set_everyFrame_13(bool value)
	{
		___everyFrame_13 = value;
	}

	inline static int32_t get_offset_of__animatorProxy_14() { return static_cast<int32_t>(offsetof(SetAnimatorFloat_t3411856923, ____animatorProxy_14)); }
	inline PlayMakerAnimatorMoveProxy_t4175490694 * get__animatorProxy_14() const { return ____animatorProxy_14; }
	inline PlayMakerAnimatorMoveProxy_t4175490694 ** get_address_of__animatorProxy_14() { return &____animatorProxy_14; }
	inline void set__animatorProxy_14(PlayMakerAnimatorMoveProxy_t4175490694 * value)
	{
		____animatorProxy_14 = value;
		Il2CppCodeGenWriteBarrier(&____animatorProxy_14, value);
	}

	inline static int32_t get_offset_of__animator_15() { return static_cast<int32_t>(offsetof(SetAnimatorFloat_t3411856923, ____animator_15)); }
	inline Animator_t2776330603 * get__animator_15() const { return ____animator_15; }
	inline Animator_t2776330603 ** get_address_of__animator_15() { return &____animator_15; }
	inline void set__animator_15(Animator_t2776330603 * value)
	{
		____animator_15 = value;
		Il2CppCodeGenWriteBarrier(&____animator_15, value);
	}

	inline static int32_t get_offset_of__paramID_16() { return static_cast<int32_t>(offsetof(SetAnimatorFloat_t3411856923, ____paramID_16)); }
	inline int32_t get__paramID_16() const { return ____paramID_16; }
	inline int32_t* get_address_of__paramID_16() { return &____paramID_16; }
	inline void set__paramID_16(int32_t value)
	{
		____paramID_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
