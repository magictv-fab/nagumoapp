﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va746493984MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ARM.components.GroupInAppAbstractComponentsManager>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m2169296100(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t260358877 *, Dictionary_2_t1559753164 *, const MethodInfo*))ValueCollection__ctor_m4177258586_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ARM.components.GroupInAppAbstractComponentsManager>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2987473614(__this, ___item0, method) ((  void (*) (ValueCollection_t260358877 *, GroupInAppAbstractComponentsManager_t739334794 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1528225944_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ARM.components.GroupInAppAbstractComponentsManager>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m385924759(__this, method) ((  void (*) (ValueCollection_t260358877 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2827278689_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ARM.components.GroupInAppAbstractComponentsManager>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m439575644(__this, ___item0, method) ((  bool (*) (ValueCollection_t260358877 *, GroupInAppAbstractComponentsManager_t739334794 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2015709838_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ARM.components.GroupInAppAbstractComponentsManager>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m269607937(__this, ___item0, method) ((  bool (*) (ValueCollection_t260358877 *, GroupInAppAbstractComponentsManager_t739334794 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3578506931_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ARM.components.GroupInAppAbstractComponentsManager>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m327581591(__this, method) ((  Il2CppObject* (*) (ValueCollection_t260358877 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4041606511_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ARM.components.GroupInAppAbstractComponentsManager>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m851922651(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t260358877 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1709700389_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ARM.components.GroupInAppAbstractComponentsManager>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1433893930(__this, method) ((  Il2CppObject * (*) (ValueCollection_t260358877 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2843387488_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ARM.components.GroupInAppAbstractComponentsManager>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3009718799(__this, method) ((  bool (*) (ValueCollection_t260358877 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m290885697_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ARM.components.GroupInAppAbstractComponentsManager>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3901116015(__this, method) ((  bool (*) (ValueCollection_t260358877 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1930063777_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ARM.components.GroupInAppAbstractComponentsManager>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2827270625(__this, method) ((  Il2CppObject * (*) (ValueCollection_t260358877 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1874108365_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ARM.components.GroupInAppAbstractComponentsManager>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m4122116139(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t260358877 *, GroupInAppAbstractComponentsManagerU5BU5D_t820496399*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1735386657_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ARM.components.GroupInAppAbstractComponentsManager>::GetEnumerator()
#define ValueCollection_GetEnumerator_m2116749716(__this, method) ((  Enumerator_t3786553868  (*) (ValueCollection_t260358877 *, const MethodInfo*))ValueCollection_GetEnumerator_m1204216004_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.String,ARM.components.GroupInAppAbstractComponentsManager>::get_Count()
#define ValueCollection_get_Count_m2905907497(__this, method) ((  int32_t (*) (ValueCollection_t260358877 *, const MethodInfo*))ValueCollection_get_Count_m2709231847_gshared)(__this, method)
