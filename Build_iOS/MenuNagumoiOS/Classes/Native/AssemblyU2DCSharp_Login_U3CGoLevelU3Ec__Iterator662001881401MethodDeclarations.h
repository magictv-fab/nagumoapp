﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Login/<GoLevel>c__Iterator66
struct U3CGoLevelU3Ec__Iterator66_t2001881401;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Login/<GoLevel>c__Iterator66::.ctor()
extern "C"  void U3CGoLevelU3Ec__Iterator66__ctor_m237685122 (U3CGoLevelU3Ec__Iterator66_t2001881401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Login/<GoLevel>c__Iterator66::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGoLevelU3Ec__Iterator66_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m621183002 (U3CGoLevelU3Ec__Iterator66_t2001881401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Login/<GoLevel>c__Iterator66::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGoLevelU3Ec__Iterator66_System_Collections_IEnumerator_get_Current_m1667233710 (U3CGoLevelU3Ec__Iterator66_t2001881401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Login/<GoLevel>c__Iterator66::MoveNext()
extern "C"  bool U3CGoLevelU3Ec__Iterator66_MoveNext_m3244986138 (U3CGoLevelU3Ec__Iterator66_t2001881401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login/<GoLevel>c__Iterator66::Dispose()
extern "C"  void U3CGoLevelU3Ec__Iterator66_Dispose_m680418623 (U3CGoLevelU3Ec__Iterator66_t2001881401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login/<GoLevel>c__Iterator66::Reset()
extern "C"  void U3CGoLevelU3Ec__Iterator66_Reset_m2179085359 (U3CGoLevelU3Ec__Iterator66_t2001881401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
