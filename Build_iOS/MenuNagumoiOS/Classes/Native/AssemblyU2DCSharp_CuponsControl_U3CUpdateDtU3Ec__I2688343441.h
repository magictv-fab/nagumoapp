﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// UnityEngine.WWW
struct WWW_t3134621005;
// UserData
struct UserData_t4092646965;
// System.Object
struct Il2CppObject;
// CuponsControl
struct CuponsControl_t4225786087;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CuponsControl/<UpdateDt>c__Iterator4E
struct  U3CUpdateDtU3Ec__Iterator4E_t2688343441  : public Il2CppObject
{
public:
	// System.String CuponsControl/<UpdateDt>c__Iterator4E::<json>__0
	String_t* ___U3CjsonU3E__0_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> CuponsControl/<UpdateDt>c__Iterator4E::<headers>__1
	Dictionary_2_t827649927 * ___U3CheadersU3E__1_1;
	// System.Byte[] CuponsControl/<UpdateDt>c__Iterator4E::<pData>__2
	ByteU5BU5D_t4260760469* ___U3CpDataU3E__2_2;
	// UnityEngine.WWW CuponsControl/<UpdateDt>c__Iterator4E::<www>__3
	WWW_t3134621005 * ___U3CwwwU3E__3_3;
	// System.String CuponsControl/<UpdateDt>c__Iterator4E::<code>__4
	String_t* ___U3CcodeU3E__4_4;
	// System.String CuponsControl/<UpdateDt>c__Iterator4E::<message>__5
	String_t* ___U3CmessageU3E__5_5;
	// UserData CuponsControl/<UpdateDt>c__Iterator4E::<userData>__6
	UserData_t4092646965 * ___U3CuserDataU3E__6_6;
	// System.Int32 CuponsControl/<UpdateDt>c__Iterator4E::<i>__7
	int32_t ___U3CiU3E__7_7;
	// System.Int32 CuponsControl/<UpdateDt>c__Iterator4E::$PC
	int32_t ___U24PC_8;
	// System.Object CuponsControl/<UpdateDt>c__Iterator4E::$current
	Il2CppObject * ___U24current_9;
	// CuponsControl CuponsControl/<UpdateDt>c__Iterator4E::<>f__this
	CuponsControl_t4225786087 * ___U3CU3Ef__this_10;

public:
	inline static int32_t get_offset_of_U3CjsonU3E__0_0() { return static_cast<int32_t>(offsetof(U3CUpdateDtU3Ec__Iterator4E_t2688343441, ___U3CjsonU3E__0_0)); }
	inline String_t* get_U3CjsonU3E__0_0() const { return ___U3CjsonU3E__0_0; }
	inline String_t** get_address_of_U3CjsonU3E__0_0() { return &___U3CjsonU3E__0_0; }
	inline void set_U3CjsonU3E__0_0(String_t* value)
	{
		___U3CjsonU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjsonU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CheadersU3E__1_1() { return static_cast<int32_t>(offsetof(U3CUpdateDtU3Ec__Iterator4E_t2688343441, ___U3CheadersU3E__1_1)); }
	inline Dictionary_2_t827649927 * get_U3CheadersU3E__1_1() const { return ___U3CheadersU3E__1_1; }
	inline Dictionary_2_t827649927 ** get_address_of_U3CheadersU3E__1_1() { return &___U3CheadersU3E__1_1; }
	inline void set_U3CheadersU3E__1_1(Dictionary_2_t827649927 * value)
	{
		___U3CheadersU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CheadersU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CpDataU3E__2_2() { return static_cast<int32_t>(offsetof(U3CUpdateDtU3Ec__Iterator4E_t2688343441, ___U3CpDataU3E__2_2)); }
	inline ByteU5BU5D_t4260760469* get_U3CpDataU3E__2_2() const { return ___U3CpDataU3E__2_2; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CpDataU3E__2_2() { return &___U3CpDataU3E__2_2; }
	inline void set_U3CpDataU3E__2_2(ByteU5BU5D_t4260760469* value)
	{
		___U3CpDataU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpDataU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__3_3() { return static_cast<int32_t>(offsetof(U3CUpdateDtU3Ec__Iterator4E_t2688343441, ___U3CwwwU3E__3_3)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__3_3() const { return ___U3CwwwU3E__3_3; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__3_3() { return &___U3CwwwU3E__3_3; }
	inline void set_U3CwwwU3E__3_3(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CcodeU3E__4_4() { return static_cast<int32_t>(offsetof(U3CUpdateDtU3Ec__Iterator4E_t2688343441, ___U3CcodeU3E__4_4)); }
	inline String_t* get_U3CcodeU3E__4_4() const { return ___U3CcodeU3E__4_4; }
	inline String_t** get_address_of_U3CcodeU3E__4_4() { return &___U3CcodeU3E__4_4; }
	inline void set_U3CcodeU3E__4_4(String_t* value)
	{
		___U3CcodeU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcodeU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U3CmessageU3E__5_5() { return static_cast<int32_t>(offsetof(U3CUpdateDtU3Ec__Iterator4E_t2688343441, ___U3CmessageU3E__5_5)); }
	inline String_t* get_U3CmessageU3E__5_5() const { return ___U3CmessageU3E__5_5; }
	inline String_t** get_address_of_U3CmessageU3E__5_5() { return &___U3CmessageU3E__5_5; }
	inline void set_U3CmessageU3E__5_5(String_t* value)
	{
		___U3CmessageU3E__5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmessageU3E__5_5, value);
	}

	inline static int32_t get_offset_of_U3CuserDataU3E__6_6() { return static_cast<int32_t>(offsetof(U3CUpdateDtU3Ec__Iterator4E_t2688343441, ___U3CuserDataU3E__6_6)); }
	inline UserData_t4092646965 * get_U3CuserDataU3E__6_6() const { return ___U3CuserDataU3E__6_6; }
	inline UserData_t4092646965 ** get_address_of_U3CuserDataU3E__6_6() { return &___U3CuserDataU3E__6_6; }
	inline void set_U3CuserDataU3E__6_6(UserData_t4092646965 * value)
	{
		___U3CuserDataU3E__6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CuserDataU3E__6_6, value);
	}

	inline static int32_t get_offset_of_U3CiU3E__7_7() { return static_cast<int32_t>(offsetof(U3CUpdateDtU3Ec__Iterator4E_t2688343441, ___U3CiU3E__7_7)); }
	inline int32_t get_U3CiU3E__7_7() const { return ___U3CiU3E__7_7; }
	inline int32_t* get_address_of_U3CiU3E__7_7() { return &___U3CiU3E__7_7; }
	inline void set_U3CiU3E__7_7(int32_t value)
	{
		___U3CiU3E__7_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CUpdateDtU3Ec__Iterator4E_t2688343441, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CUpdateDtU3Ec__Iterator4E_t2688343441, ___U24current_9)); }
	inline Il2CppObject * get_U24current_9() const { return ___U24current_9; }
	inline Il2CppObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(Il2CppObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_10() { return static_cast<int32_t>(offsetof(U3CUpdateDtU3Ec__Iterator4E_t2688343441, ___U3CU3Ef__this_10)); }
	inline CuponsControl_t4225786087 * get_U3CU3Ef__this_10() const { return ___U3CU3Ef__this_10; }
	inline CuponsControl_t4225786087 ** get_address_of_U3CU3Ef__this_10() { return &___U3CU3Ef__this_10; }
	inline void set_U3CU3Ef__this_10(CuponsControl_t4225786087 * value)
	{
		___U3CU3Ef__this_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
