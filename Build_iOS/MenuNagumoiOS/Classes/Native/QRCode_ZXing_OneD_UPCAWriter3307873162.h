﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.OneD.EAN13Writer
struct EAN13Writer_t2474944089;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.UPCAWriter
struct  UPCAWriter_t3307873162  : public Il2CppObject
{
public:
	// ZXing.OneD.EAN13Writer ZXing.OneD.UPCAWriter::subWriter
	EAN13Writer_t2474944089 * ___subWriter_0;

public:
	inline static int32_t get_offset_of_subWriter_0() { return static_cast<int32_t>(offsetof(UPCAWriter_t3307873162, ___subWriter_0)); }
	inline EAN13Writer_t2474944089 * get_subWriter_0() const { return ___subWriter_0; }
	inline EAN13Writer_t2474944089 ** get_address_of_subWriter_0() { return &___subWriter_0; }
	inline void set_subWriter_0(EAN13Writer_t2474944089 * value)
	{
		___subWriter_0 = value;
		Il2CppCodeGenWriteBarrier(&___subWriter_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
