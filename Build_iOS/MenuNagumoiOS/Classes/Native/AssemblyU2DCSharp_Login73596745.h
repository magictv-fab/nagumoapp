﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UserData
struct UserData_t4092646965;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// UnityEngine.UI.InputField
struct InputField_t609046876;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// MagicTV.globals.InAppAnalytics
struct InAppAnalytics_t2316734034;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Login
struct  Login_t73596745  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.InputField Login::loginInput
	InputField_t609046876 * ___loginInput_5;
	// UnityEngine.UI.InputField Login::senhaInput
	InputField_t609046876 * ___senhaInput_6;
	// UnityEngine.GameObject Login::loadingObj
	GameObject_t3674682005 * ___loadingObj_7;
	// UnityEngine.GameObject Login::popup
	GameObject_t3674682005 * ___popup_8;
	// System.Boolean Login::back
	bool ___back_10;

public:
	inline static int32_t get_offset_of_loginInput_5() { return static_cast<int32_t>(offsetof(Login_t73596745, ___loginInput_5)); }
	inline InputField_t609046876 * get_loginInput_5() const { return ___loginInput_5; }
	inline InputField_t609046876 ** get_address_of_loginInput_5() { return &___loginInput_5; }
	inline void set_loginInput_5(InputField_t609046876 * value)
	{
		___loginInput_5 = value;
		Il2CppCodeGenWriteBarrier(&___loginInput_5, value);
	}

	inline static int32_t get_offset_of_senhaInput_6() { return static_cast<int32_t>(offsetof(Login_t73596745, ___senhaInput_6)); }
	inline InputField_t609046876 * get_senhaInput_6() const { return ___senhaInput_6; }
	inline InputField_t609046876 ** get_address_of_senhaInput_6() { return &___senhaInput_6; }
	inline void set_senhaInput_6(InputField_t609046876 * value)
	{
		___senhaInput_6 = value;
		Il2CppCodeGenWriteBarrier(&___senhaInput_6, value);
	}

	inline static int32_t get_offset_of_loadingObj_7() { return static_cast<int32_t>(offsetof(Login_t73596745, ___loadingObj_7)); }
	inline GameObject_t3674682005 * get_loadingObj_7() const { return ___loadingObj_7; }
	inline GameObject_t3674682005 ** get_address_of_loadingObj_7() { return &___loadingObj_7; }
	inline void set_loadingObj_7(GameObject_t3674682005 * value)
	{
		___loadingObj_7 = value;
		Il2CppCodeGenWriteBarrier(&___loadingObj_7, value);
	}

	inline static int32_t get_offset_of_popup_8() { return static_cast<int32_t>(offsetof(Login_t73596745, ___popup_8)); }
	inline GameObject_t3674682005 * get_popup_8() const { return ___popup_8; }
	inline GameObject_t3674682005 ** get_address_of_popup_8() { return &___popup_8; }
	inline void set_popup_8(GameObject_t3674682005 * value)
	{
		___popup_8 = value;
		Il2CppCodeGenWriteBarrier(&___popup_8, value);
	}

	inline static int32_t get_offset_of_back_10() { return static_cast<int32_t>(offsetof(Login_t73596745, ___back_10)); }
	inline bool get_back_10() const { return ___back_10; }
	inline bool* get_address_of_back_10() { return &___back_10; }
	inline void set_back_10(bool value)
	{
		___back_10 = value;
	}
};

struct Login_t73596745_StaticFields
{
public:
	// UserData Login::userData
	UserData_t4092646965 * ___userData_2;
	// System.Boolean Login::isLogged
	bool ___isLogged_3;
	// UnityEngine.Texture2D Login::photo_profile
	Texture2D_t3884108195 * ___photo_profile_4;
	// MagicTV.globals.InAppAnalytics Login::_analytics
	InAppAnalytics_t2316734034 * ____analytics_9;

public:
	inline static int32_t get_offset_of_userData_2() { return static_cast<int32_t>(offsetof(Login_t73596745_StaticFields, ___userData_2)); }
	inline UserData_t4092646965 * get_userData_2() const { return ___userData_2; }
	inline UserData_t4092646965 ** get_address_of_userData_2() { return &___userData_2; }
	inline void set_userData_2(UserData_t4092646965 * value)
	{
		___userData_2 = value;
		Il2CppCodeGenWriteBarrier(&___userData_2, value);
	}

	inline static int32_t get_offset_of_isLogged_3() { return static_cast<int32_t>(offsetof(Login_t73596745_StaticFields, ___isLogged_3)); }
	inline bool get_isLogged_3() const { return ___isLogged_3; }
	inline bool* get_address_of_isLogged_3() { return &___isLogged_3; }
	inline void set_isLogged_3(bool value)
	{
		___isLogged_3 = value;
	}

	inline static int32_t get_offset_of_photo_profile_4() { return static_cast<int32_t>(offsetof(Login_t73596745_StaticFields, ___photo_profile_4)); }
	inline Texture2D_t3884108195 * get_photo_profile_4() const { return ___photo_profile_4; }
	inline Texture2D_t3884108195 ** get_address_of_photo_profile_4() { return &___photo_profile_4; }
	inline void set_photo_profile_4(Texture2D_t3884108195 * value)
	{
		___photo_profile_4 = value;
		Il2CppCodeGenWriteBarrier(&___photo_profile_4, value);
	}

	inline static int32_t get_offset_of__analytics_9() { return static_cast<int32_t>(offsetof(Login_t73596745_StaticFields, ____analytics_9)); }
	inline InAppAnalytics_t2316734034 * get__analytics_9() const { return ____analytics_9; }
	inline InAppAnalytics_t2316734034 ** get_address_of__analytics_9() { return &____analytics_9; }
	inline void set__analytics_9(InAppAnalytics_t2316734034 * value)
	{
		____analytics_9 = value;
		Il2CppCodeGenWriteBarrier(&____analytics_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
