﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V4285270722MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m1047254148(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t4005191582 *, Dictionary_2_t1009618573 *, const MethodInfo*))ValueCollection__ctor_m1099873897_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m457426734(__this, ___item0, method) ((  void (*) (ValueCollection_t4005191582 *, Func_1_t3890737231 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m182029481_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2368240375(__this, method) ((  void (*) (ValueCollection_t4005191582 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m844772850_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2616428216(__this, ___item0, method) ((  bool (*) (ValueCollection_t4005191582 *, Func_1_t3890737231 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2854902429_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1424944477(__this, ___item0, method) ((  bool (*) (ValueCollection_t4005191582 *, Func_1_t3890737231 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3726866050_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3489443717(__this, method) ((  Il2CppObject* (*) (ValueCollection_t4005191582 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2634734528_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m758584123(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t4005191582 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m4014182518_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1981257462(__this, method) ((  Il2CppObject * (*) (ValueCollection_t4005191582 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1222187889_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m891604075(__this, method) ((  bool (*) (ValueCollection_t4005191582 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1130078288_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1786911435(__this, method) ((  bool (*) (ValueCollection_t4005191582 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m580116016_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m305846775(__this, method) ((  Il2CppObject * (*) (ValueCollection_t4005191582 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1351921628_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m1851168715(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t4005191582 *, Func_1U5BU5D_t2057474454*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1572499568_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::GetEnumerator()
#define ValueCollection_GetEnumerator_m1367305070(__this, method) ((  Enumerator_t3236419277  (*) (ValueCollection_t4005191582 *, const MethodInfo*))ValueCollection_GetEnumerator_m1412598675_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::get_Count()
#define ValueCollection_get_Count_m3594954513(__this, method) ((  int32_t (*) (ValueCollection_t4005191582 *, const MethodInfo*))ValueCollection_get_Count_m1179143670_gshared)(__this, method)
