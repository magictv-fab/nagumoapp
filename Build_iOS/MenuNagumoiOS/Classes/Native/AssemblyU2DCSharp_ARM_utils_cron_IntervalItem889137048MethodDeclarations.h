﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.utils.cron.IntervalItem
struct IntervalItem_t889137048;

#include "codegen/il2cpp-codegen.h"

// System.Void ARM.utils.cron.IntervalItem::.ctor()
extern "C"  void IntervalItem__ctor_m481856066 (IntervalItem_t889137048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.cron.IntervalItem::Play()
extern "C"  void IntervalItem_Play_m1273782518 (IntervalItem_t889137048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.cron.IntervalItem::Update()
extern "C"  void IntervalItem_Update_m3779498251 (IntervalItem_t889137048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.cron.IntervalItem::Stop()
extern "C"  void IntervalItem_Stop_m1367466564 (IntervalItem_t889137048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
