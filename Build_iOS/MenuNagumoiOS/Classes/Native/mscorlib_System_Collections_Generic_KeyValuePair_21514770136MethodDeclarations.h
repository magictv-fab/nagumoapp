﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UniWebViewNativeListener>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m317231263(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1514770136 *, String_t*, UniWebViewNativeListener_t795571060 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,UniWebViewNativeListener>::get_Key()
#define KeyValuePair_2_get_Key_m3783961705(__this, method) ((  String_t* (*) (KeyValuePair_2_t1514770136 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UniWebViewNativeListener>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2603131434(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1514770136 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,UniWebViewNativeListener>::get_Value()
#define KeyValuePair_2_get_Value_m4133210985(__this, method) ((  UniWebViewNativeListener_t795571060 * (*) (KeyValuePair_2_t1514770136 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UniWebViewNativeListener>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2830291242(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1514770136 *, UniWebViewNativeListener_t795571060 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,UniWebViewNativeListener>::ToString()
#define KeyValuePair_2_ToString_m169706232(__this, method) ((  String_t* (*) (KeyValuePair_2_t1514770136 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
