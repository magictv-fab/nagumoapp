﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Common.GlobalHistogramBinarizer
struct GlobalHistogramBinarizer_t1656947045;
// ZXing.LuminanceSource
struct LuminanceSource_t1231523093;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// ZXing.Binarizer
struct Binarizer_t1492033400;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_LuminanceSource1231523093.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"

// System.Void ZXing.Common.GlobalHistogramBinarizer::.ctor(ZXing.LuminanceSource)
extern "C"  void GlobalHistogramBinarizer__ctor_m1184826892 (GlobalHistogramBinarizer_t1656947045 * __this, LuminanceSource_t1231523093 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.BitArray ZXing.Common.GlobalHistogramBinarizer::getBlackRow(System.Int32,ZXing.Common.BitArray)
extern "C"  BitArray_t4163851164 * GlobalHistogramBinarizer_getBlackRow_m3911576434 (GlobalHistogramBinarizer_t1656947045 * __this, int32_t ___y0, BitArray_t4163851164 * ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.BitMatrix ZXing.Common.GlobalHistogramBinarizer::get_BlackMatrix()
extern "C"  BitMatrix_t1058711404 * GlobalHistogramBinarizer_get_BlackMatrix_m303636250 (GlobalHistogramBinarizer_t1656947045 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Binarizer ZXing.Common.GlobalHistogramBinarizer::createBinarizer(ZXing.LuminanceSource)
extern "C"  Binarizer_t1492033400 * GlobalHistogramBinarizer_createBinarizer_m1933168269 (GlobalHistogramBinarizer_t1656947045 * __this, LuminanceSource_t1231523093 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.GlobalHistogramBinarizer::initArrays(System.Int32)
extern "C"  void GlobalHistogramBinarizer_initArrays_m1817706978 (GlobalHistogramBinarizer_t1656947045 * __this, int32_t ___luminanceSize0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Common.GlobalHistogramBinarizer::estimateBlackPoint(System.Int32[],System.Int32&)
extern "C"  bool GlobalHistogramBinarizer_estimateBlackPoint_m3516099408 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___buckets0, int32_t* ___blackPoint1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.GlobalHistogramBinarizer::.cctor()
extern "C"  void GlobalHistogramBinarizer__cctor_m3312172144 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
