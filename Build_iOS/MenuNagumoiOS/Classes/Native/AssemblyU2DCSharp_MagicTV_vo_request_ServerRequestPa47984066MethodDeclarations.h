﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.vo.request.ServerRequestParamsUpdateVO
struct ServerRequestParamsUpdateVO_t47984066;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MagicTV.vo.request.ServerRequestParamsUpdateVO::.ctor()
extern "C"  void ServerRequestParamsUpdateVO__ctor_m93981532 (ServerRequestParamsUpdateVO_t47984066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.vo.request.ServerRequestParamsUpdateVO::get_revision()
extern "C"  String_t* ServerRequestParamsUpdateVO_get_revision_m1368921039 (ServerRequestParamsUpdateVO_t47984066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.vo.request.ServerRequestParamsUpdateVO::set_revision(System.String)
extern "C"  void ServerRequestParamsUpdateVO_set_revision_m3568698018 (ServerRequestParamsUpdateVO_t47984066 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.vo.request.ServerRequestParamsUpdateVO::ToURLString()
extern "C"  String_t* ServerRequestParamsUpdateVO_ToURLString_m2310352732 (ServerRequestParamsUpdateVO_t47984066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
