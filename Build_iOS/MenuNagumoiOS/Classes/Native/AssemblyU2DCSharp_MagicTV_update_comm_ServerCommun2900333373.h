﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// MagicTV.update.comm.ServerCommunication
struct ServerCommunication_t2900333373;
// MagicTV.update.comm.ServerCommunication/OnBooleanEventHandler
struct OnBooleanEventHandler_t2032344769;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "AssemblyU2DCSharp_MagicTV_abstracts_comm_ServerCom3244961639.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.update.comm.ServerCommunication
struct  ServerCommunication_t2900333373  : public ServerCommunicationAbstract_t3244961639
{
public:
	// System.String MagicTV.update.comm.ServerCommunication::urlToGetJson
	String_t* ___urlToGetJson_7;
	// System.Int32 MagicTV.update.comm.ServerCommunication::delayMilisecondsToDownloadImages
	int32_t ___delayMilisecondsToDownloadImages_9;
	// MagicTV.update.comm.ServerCommunication/OnBooleanEventHandler MagicTV.update.comm.ServerCommunication::onUpdateMetadataRequestComplete
	OnBooleanEventHandler_t2032344769 * ___onUpdateMetadataRequestComplete_10;
	// System.Boolean MagicTV.update.comm.ServerCommunication::requestIsFinished
	bool ___requestIsFinished_11;
	// System.Collections.IEnumerator MagicTV.update.comm.ServerCommunication::currentRoutine
	Il2CppObject * ___currentRoutine_12;
	// System.Int32 MagicTV.update.comm.ServerCommunication::_timerTimeout
	int32_t ____timerTimeout_13;

public:
	inline static int32_t get_offset_of_urlToGetJson_7() { return static_cast<int32_t>(offsetof(ServerCommunication_t2900333373, ___urlToGetJson_7)); }
	inline String_t* get_urlToGetJson_7() const { return ___urlToGetJson_7; }
	inline String_t** get_address_of_urlToGetJson_7() { return &___urlToGetJson_7; }
	inline void set_urlToGetJson_7(String_t* value)
	{
		___urlToGetJson_7 = value;
		Il2CppCodeGenWriteBarrier(&___urlToGetJson_7, value);
	}

	inline static int32_t get_offset_of_delayMilisecondsToDownloadImages_9() { return static_cast<int32_t>(offsetof(ServerCommunication_t2900333373, ___delayMilisecondsToDownloadImages_9)); }
	inline int32_t get_delayMilisecondsToDownloadImages_9() const { return ___delayMilisecondsToDownloadImages_9; }
	inline int32_t* get_address_of_delayMilisecondsToDownloadImages_9() { return &___delayMilisecondsToDownloadImages_9; }
	inline void set_delayMilisecondsToDownloadImages_9(int32_t value)
	{
		___delayMilisecondsToDownloadImages_9 = value;
	}

	inline static int32_t get_offset_of_onUpdateMetadataRequestComplete_10() { return static_cast<int32_t>(offsetof(ServerCommunication_t2900333373, ___onUpdateMetadataRequestComplete_10)); }
	inline OnBooleanEventHandler_t2032344769 * get_onUpdateMetadataRequestComplete_10() const { return ___onUpdateMetadataRequestComplete_10; }
	inline OnBooleanEventHandler_t2032344769 ** get_address_of_onUpdateMetadataRequestComplete_10() { return &___onUpdateMetadataRequestComplete_10; }
	inline void set_onUpdateMetadataRequestComplete_10(OnBooleanEventHandler_t2032344769 * value)
	{
		___onUpdateMetadataRequestComplete_10 = value;
		Il2CppCodeGenWriteBarrier(&___onUpdateMetadataRequestComplete_10, value);
	}

	inline static int32_t get_offset_of_requestIsFinished_11() { return static_cast<int32_t>(offsetof(ServerCommunication_t2900333373, ___requestIsFinished_11)); }
	inline bool get_requestIsFinished_11() const { return ___requestIsFinished_11; }
	inline bool* get_address_of_requestIsFinished_11() { return &___requestIsFinished_11; }
	inline void set_requestIsFinished_11(bool value)
	{
		___requestIsFinished_11 = value;
	}

	inline static int32_t get_offset_of_currentRoutine_12() { return static_cast<int32_t>(offsetof(ServerCommunication_t2900333373, ___currentRoutine_12)); }
	inline Il2CppObject * get_currentRoutine_12() const { return ___currentRoutine_12; }
	inline Il2CppObject ** get_address_of_currentRoutine_12() { return &___currentRoutine_12; }
	inline void set_currentRoutine_12(Il2CppObject * value)
	{
		___currentRoutine_12 = value;
		Il2CppCodeGenWriteBarrier(&___currentRoutine_12, value);
	}

	inline static int32_t get_offset_of__timerTimeout_13() { return static_cast<int32_t>(offsetof(ServerCommunication_t2900333373, ____timerTimeout_13)); }
	inline int32_t get__timerTimeout_13() const { return ____timerTimeout_13; }
	inline int32_t* get_address_of__timerTimeout_13() { return &____timerTimeout_13; }
	inline void set__timerTimeout_13(int32_t value)
	{
		____timerTimeout_13 = value;
	}
};

struct ServerCommunication_t2900333373_StaticFields
{
public:
	// MagicTV.update.comm.ServerCommunication MagicTV.update.comm.ServerCommunication::Instance
	ServerCommunication_t2900333373 * ___Instance_8;

public:
	inline static int32_t get_offset_of_Instance_8() { return static_cast<int32_t>(offsetof(ServerCommunication_t2900333373_StaticFields, ___Instance_8)); }
	inline ServerCommunication_t2900333373 * get_Instance_8() const { return ___Instance_8; }
	inline ServerCommunication_t2900333373 ** get_address_of_Instance_8() { return &___Instance_8; }
	inline void set_Instance_8(ServerCommunication_t2900333373 * value)
	{
		___Instance_8 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
