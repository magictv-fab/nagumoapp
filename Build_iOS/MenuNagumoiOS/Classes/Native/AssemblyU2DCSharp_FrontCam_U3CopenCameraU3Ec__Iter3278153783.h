﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// FrontCam
struct FrontCam_t3116790022;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_WebCamDevice3274004757.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FrontCam/<openCamera>c__Iterator57
struct  U3CopenCameraU3Ec__Iterator57_t3278153783  : public Il2CppObject
{
public:
	// UnityEngine.WebCamDevice FrontCam/<openCamera>c__Iterator57::<device>__0
	WebCamDevice_t3274004757  ___U3CdeviceU3E__0_0;
	// UnityEngine.WebCamDevice FrontCam/<openCamera>c__Iterator57::<device>__1
	WebCamDevice_t3274004757  ___U3CdeviceU3E__1_1;
	// System.Int32 FrontCam/<openCamera>c__Iterator57::$PC
	int32_t ___U24PC_2;
	// System.Object FrontCam/<openCamera>c__Iterator57::$current
	Il2CppObject * ___U24current_3;
	// FrontCam FrontCam/<openCamera>c__Iterator57::<>f__this
	FrontCam_t3116790022 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_U3CdeviceU3E__0_0() { return static_cast<int32_t>(offsetof(U3CopenCameraU3Ec__Iterator57_t3278153783, ___U3CdeviceU3E__0_0)); }
	inline WebCamDevice_t3274004757  get_U3CdeviceU3E__0_0() const { return ___U3CdeviceU3E__0_0; }
	inline WebCamDevice_t3274004757 * get_address_of_U3CdeviceU3E__0_0() { return &___U3CdeviceU3E__0_0; }
	inline void set_U3CdeviceU3E__0_0(WebCamDevice_t3274004757  value)
	{
		___U3CdeviceU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CdeviceU3E__1_1() { return static_cast<int32_t>(offsetof(U3CopenCameraU3Ec__Iterator57_t3278153783, ___U3CdeviceU3E__1_1)); }
	inline WebCamDevice_t3274004757  get_U3CdeviceU3E__1_1() const { return ___U3CdeviceU3E__1_1; }
	inline WebCamDevice_t3274004757 * get_address_of_U3CdeviceU3E__1_1() { return &___U3CdeviceU3E__1_1; }
	inline void set_U3CdeviceU3E__1_1(WebCamDevice_t3274004757  value)
	{
		___U3CdeviceU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CopenCameraU3Ec__Iterator57_t3278153783, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CopenCameraU3Ec__Iterator57_t3278153783, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CopenCameraU3Ec__Iterator57_t3278153783, ___U3CU3Ef__this_4)); }
	inline FrontCam_t3116790022 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline FrontCam_t3116790022 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(FrontCam_t3116790022 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
