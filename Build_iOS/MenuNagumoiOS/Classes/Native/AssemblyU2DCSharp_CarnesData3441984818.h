﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// CortesData[]
struct CortesDataU5BU5D_t1695339363;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CarnesData
struct  CarnesData_t3441984818  : public Il2CppObject
{
public:
	// System.String CarnesData::nome_produto
	String_t* ___nome_produto_0;
	// System.String CarnesData::corte
	String_t* ___corte_1;
	// System.String CarnesData::observacao
	String_t* ___observacao_2;
	// System.String CarnesData::peso
	String_t* ___peso_3;
	// System.String CarnesData::imagem
	String_t* ___imagem_4;
	// System.Int32 CarnesData::id_carne
	int32_t ___id_carne_5;
	// CortesData[] CarnesData::cortes
	CortesDataU5BU5D_t1695339363* ___cortes_6;

public:
	inline static int32_t get_offset_of_nome_produto_0() { return static_cast<int32_t>(offsetof(CarnesData_t3441984818, ___nome_produto_0)); }
	inline String_t* get_nome_produto_0() const { return ___nome_produto_0; }
	inline String_t** get_address_of_nome_produto_0() { return &___nome_produto_0; }
	inline void set_nome_produto_0(String_t* value)
	{
		___nome_produto_0 = value;
		Il2CppCodeGenWriteBarrier(&___nome_produto_0, value);
	}

	inline static int32_t get_offset_of_corte_1() { return static_cast<int32_t>(offsetof(CarnesData_t3441984818, ___corte_1)); }
	inline String_t* get_corte_1() const { return ___corte_1; }
	inline String_t** get_address_of_corte_1() { return &___corte_1; }
	inline void set_corte_1(String_t* value)
	{
		___corte_1 = value;
		Il2CppCodeGenWriteBarrier(&___corte_1, value);
	}

	inline static int32_t get_offset_of_observacao_2() { return static_cast<int32_t>(offsetof(CarnesData_t3441984818, ___observacao_2)); }
	inline String_t* get_observacao_2() const { return ___observacao_2; }
	inline String_t** get_address_of_observacao_2() { return &___observacao_2; }
	inline void set_observacao_2(String_t* value)
	{
		___observacao_2 = value;
		Il2CppCodeGenWriteBarrier(&___observacao_2, value);
	}

	inline static int32_t get_offset_of_peso_3() { return static_cast<int32_t>(offsetof(CarnesData_t3441984818, ___peso_3)); }
	inline String_t* get_peso_3() const { return ___peso_3; }
	inline String_t** get_address_of_peso_3() { return &___peso_3; }
	inline void set_peso_3(String_t* value)
	{
		___peso_3 = value;
		Il2CppCodeGenWriteBarrier(&___peso_3, value);
	}

	inline static int32_t get_offset_of_imagem_4() { return static_cast<int32_t>(offsetof(CarnesData_t3441984818, ___imagem_4)); }
	inline String_t* get_imagem_4() const { return ___imagem_4; }
	inline String_t** get_address_of_imagem_4() { return &___imagem_4; }
	inline void set_imagem_4(String_t* value)
	{
		___imagem_4 = value;
		Il2CppCodeGenWriteBarrier(&___imagem_4, value);
	}

	inline static int32_t get_offset_of_id_carne_5() { return static_cast<int32_t>(offsetof(CarnesData_t3441984818, ___id_carne_5)); }
	inline int32_t get_id_carne_5() const { return ___id_carne_5; }
	inline int32_t* get_address_of_id_carne_5() { return &___id_carne_5; }
	inline void set_id_carne_5(int32_t value)
	{
		___id_carne_5 = value;
	}

	inline static int32_t get_offset_of_cortes_6() { return static_cast<int32_t>(offsetof(CarnesData_t3441984818, ___cortes_6)); }
	inline CortesDataU5BU5D_t1695339363* get_cortes_6() const { return ___cortes_6; }
	inline CortesDataU5BU5D_t1695339363** get_address_of_cortes_6() { return &___cortes_6; }
	inline void set_cortes_6(CortesDataU5BU5D_t1695339363* value)
	{
		___cortes_6 = value;
		Il2CppCodeGenWriteBarrier(&___cortes_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
