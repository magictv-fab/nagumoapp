﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetFsmQuaternion
struct GetFsmQuaternion_t2004323440;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetFsmQuaternion::.ctor()
extern "C"  void GetFsmQuaternion__ctor_m543183862 (GetFsmQuaternion_t2004323440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmQuaternion::Reset()
extern "C"  void GetFsmQuaternion_Reset_m2484584099 (GetFsmQuaternion_t2004323440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmQuaternion::OnEnter()
extern "C"  void GetFsmQuaternion_OnEnter_m1700377165 (GetFsmQuaternion_t2004323440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmQuaternion::OnUpdate()
extern "C"  void GetFsmQuaternion_OnUpdate_m305643926 (GetFsmQuaternion_t2004323440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmQuaternion::DoGetFsmVariable()
extern "C"  void GetFsmQuaternion_DoGetFsmVariable_m2558044895 (GetFsmQuaternion_t2004323440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
