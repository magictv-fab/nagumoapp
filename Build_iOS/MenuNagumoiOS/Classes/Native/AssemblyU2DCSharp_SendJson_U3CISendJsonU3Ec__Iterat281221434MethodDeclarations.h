﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SendJson/<ISendJson>c__Iterator7B
struct U3CISendJsonU3Ec__Iterator7B_t281221434;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SendJson/<ISendJson>c__Iterator7B::.ctor()
extern "C"  void U3CISendJsonU3Ec__Iterator7B__ctor_m1112114545 (U3CISendJsonU3Ec__Iterator7B_t281221434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SendJson/<ISendJson>c__Iterator7B::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CISendJsonU3Ec__Iterator7B_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m685458369 (U3CISendJsonU3Ec__Iterator7B_t281221434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SendJson/<ISendJson>c__Iterator7B::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CISendJsonU3Ec__Iterator7B_System_Collections_IEnumerator_get_Current_m2858871125 (U3CISendJsonU3Ec__Iterator7B_t281221434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SendJson/<ISendJson>c__Iterator7B::MoveNext()
extern "C"  bool U3CISendJsonU3Ec__Iterator7B_MoveNext_m3294155747 (U3CISendJsonU3Ec__Iterator7B_t281221434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SendJson/<ISendJson>c__Iterator7B::Dispose()
extern "C"  void U3CISendJsonU3Ec__Iterator7B_Dispose_m3488471406 (U3CISendJsonU3Ec__Iterator7B_t281221434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SendJson/<ISendJson>c__Iterator7B::Reset()
extern "C"  void U3CISendJsonU3Ec__Iterator7B_Reset_m3053514782 (U3CISendJsonU3Ec__Iterator7B_t281221434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
