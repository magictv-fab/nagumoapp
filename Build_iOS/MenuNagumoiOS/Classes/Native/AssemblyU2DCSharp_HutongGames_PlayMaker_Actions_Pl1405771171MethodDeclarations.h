﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.PlayerPrefsGetFloat
struct PlayerPrefsGetFloat_t1405771171;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsGetFloat::.ctor()
extern "C"  void PlayerPrefsGetFloat__ctor_m2714815731 (PlayerPrefsGetFloat_t1405771171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsGetFloat::Reset()
extern "C"  void PlayerPrefsGetFloat_Reset_m361248672 (PlayerPrefsGetFloat_t1405771171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsGetFloat::OnEnter()
extern "C"  void PlayerPrefsGetFloat_OnEnter_m1284497418 (PlayerPrefsGetFloat_t1405771171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
