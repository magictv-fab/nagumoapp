﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SampleAppsUILayout
struct SampleAppsUILayout_t3082371642;
// UnityEngine.Font
struct Font_t4241557075;
// SampleAppUILabel
struct SampleAppUILabel_t416684265;
// System.String
struct String_t;
// SampleAppUICheckButton
struct SampleAppUICheckButton_t4241727631;
// SampleAppUIRadioButton
struct SampleAppUIRadioButton_t3067317570;
// System.String[]
struct StringU5BU5D_t4054002952;
// SampleAppUIButton
struct SampleAppUIButton_t4060007389;
// UnityEngine.Texture
struct Texture_t2526458961;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"

// System.Void SampleAppsUILayout::.ctor()
extern "C"  void SampleAppsUILayout__ctor_m140156833 (SampleAppsUILayout_t3082371642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Font SampleAppsUILayout::get_StyleFont()
extern "C"  Font_t4241557075 * SampleAppsUILayout_get_StyleFont_m3582125855 (SampleAppsUILayout_t3082371642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SampleAppsUILayout::Draw()
extern "C"  void SampleAppsUILayout_Draw_m1894580679 (SampleAppsUILayout_t3082371642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SampleAppUILabel SampleAppsUILayout::AddLabel(System.String)
extern "C"  SampleAppUILabel_t416684265 * SampleAppsUILayout_AddLabel_m1695336376 (SampleAppsUILayout_t3082371642 * __this, String_t* ___title0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SampleAppUILabel SampleAppsUILayout::AddGroupLabel(System.String)
extern "C"  SampleAppUILabel_t416684265 * SampleAppsUILayout_AddGroupLabel_m3165317281 (SampleAppsUILayout_t3082371642 * __this, String_t* ___title0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SampleAppUICheckButton SampleAppsUILayout::AddSimpleButton(System.String)
extern "C"  SampleAppUICheckButton_t4241727631 * SampleAppsUILayout_AddSimpleButton_m2759488140 (SampleAppsUILayout_t3082371642 * __this, String_t* ___title0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SampleAppUICheckButton SampleAppsUILayout::AddSlider(System.String,System.Boolean)
extern "C"  SampleAppUICheckButton_t4241727631 * SampleAppsUILayout_AddSlider_m251310286 (SampleAppsUILayout_t3082371642 * __this, String_t* ___title0, bool ___tf1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SampleAppUICheckButton SampleAppsUILayout::AddSlider_Type2(System.String,System.Boolean)
extern "C"  SampleAppUICheckButton_t4241727631 * SampleAppsUILayout_AddSlider_Type2_m314619047 (SampleAppsUILayout_t3082371642 * __this, String_t* ___title0, bool ___tf1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SampleAppUIRadioButton SampleAppsUILayout::AddToggleOptions(System.String[],System.Int32)
extern "C"  SampleAppUIRadioButton_t3067317570 * SampleAppsUILayout_AddToggleOptions_m506470206 (SampleAppsUILayout_t3082371642 * __this, StringU5BU5D_t4054002952* ___titleList0, int32_t ___selectedId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SampleAppUIButton SampleAppsUILayout::AddButton(System.String,UnityEngine.Rect)
extern "C"  SampleAppUIButton_t4060007389 * SampleAppsUILayout_AddButton_m793693413 (SampleAppsUILayout_t3082371642 * __this, String_t* ___title0, Rect_t4241904616  ___rect1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SampleAppUIButton SampleAppsUILayout::AddButton(UnityEngine.Texture,UnityEngine.Rect)
extern "C"  SampleAppUIButton_t4060007389 * SampleAppsUILayout_AddButton_m3568402681 (SampleAppsUILayout_t3082371642 * __this, Texture_t2526458961 * ___buttonTexture0, Rect_t4241904616  ___rect1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SampleAppsUILayout::AddGap(System.Single)
extern "C"  void SampleAppsUILayout_AddGap_m506223219 (SampleAppsUILayout_t3082371642 * __this, float ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
