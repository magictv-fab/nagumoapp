﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetJointConnectedBody
struct  SetJointConnectedBody_t2876069553  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetJointConnectedBody::joint
	FsmOwnerDefault_t251897112 * ___joint_9;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.SetJointConnectedBody::rigidBody
	FsmGameObject_t1697147867 * ___rigidBody_10;

public:
	inline static int32_t get_offset_of_joint_9() { return static_cast<int32_t>(offsetof(SetJointConnectedBody_t2876069553, ___joint_9)); }
	inline FsmOwnerDefault_t251897112 * get_joint_9() const { return ___joint_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_joint_9() { return &___joint_9; }
	inline void set_joint_9(FsmOwnerDefault_t251897112 * value)
	{
		___joint_9 = value;
		Il2CppCodeGenWriteBarrier(&___joint_9, value);
	}

	inline static int32_t get_offset_of_rigidBody_10() { return static_cast<int32_t>(offsetof(SetJointConnectedBody_t2876069553, ___rigidBody_10)); }
	inline FsmGameObject_t1697147867 * get_rigidBody_10() const { return ___rigidBody_10; }
	inline FsmGameObject_t1697147867 ** get_address_of_rigidBody_10() { return &___rigidBody_10; }
	inline void set_rigidBody_10(FsmGameObject_t1697147867 * value)
	{
		___rigidBody_10 = value;
		Il2CppCodeGenWriteBarrier(&___rigidBody_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
