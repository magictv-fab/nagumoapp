﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Extensions.VerticalScrollSnap
struct VerticalScrollSnap_t3205272913;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2662109048;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1848751023;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1848751023.h"

// System.Void UnityEngine.UI.Extensions.VerticalScrollSnap::.ctor()
extern "C"  void VerticalScrollSnap__ctor_m3804710775 (VerticalScrollSnap_t3205272913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.VerticalScrollSnap::Start()
extern "C"  void VerticalScrollSnap_Start_m2751848567 (VerticalScrollSnap_t3205272913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.VerticalScrollSnap::Update()
extern "C"  void VerticalScrollSnap_Update_m3708779126 (VerticalScrollSnap_t3205272913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.Extensions.VerticalScrollSnap::IsRectMovingSlowerThanThreshold(System.Single)
extern "C"  bool VerticalScrollSnap_IsRectMovingSlowerThanThreshold_m3713004202 (VerticalScrollSnap_t3205272913 * __this, float ___startingSpeed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.VerticalScrollSnap::DistributePages()
extern "C"  void VerticalScrollSnap_DistributePages_m1613468984 (VerticalScrollSnap_t3205272913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.VerticalScrollSnap::AddChild(UnityEngine.GameObject)
extern "C"  void VerticalScrollSnap_AddChild_m1434705056 (VerticalScrollSnap_t3205272913 * __this, GameObject_t3674682005 * ___GO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.VerticalScrollSnap::AddChild(UnityEngine.GameObject,System.Boolean)
extern "C"  void VerticalScrollSnap_AddChild_m4113801949 (VerticalScrollSnap_t3205272913 * __this, GameObject_t3674682005 * ___GO0, bool ___WorldPositionStays1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.VerticalScrollSnap::RemoveChild(System.Int32,UnityEngine.GameObject&)
extern "C"  void VerticalScrollSnap_RemoveChild_m1320707692 (VerticalScrollSnap_t3205272913 * __this, int32_t ___index0, GameObject_t3674682005 ** ___ChildRemoved1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.VerticalScrollSnap::RemoveAllChildren(UnityEngine.GameObject[]&)
extern "C"  void VerticalScrollSnap_RemoveAllChildren_m3530566629 (VerticalScrollSnap_t3205272913 * __this, GameObjectU5BU5D_t2662109048** ___ChildrenRemoved0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.VerticalScrollSnap::SetScrollContainerPosition()
extern "C"  void VerticalScrollSnap_SetScrollContainerPosition_m2484703720 (VerticalScrollSnap_t3205272913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.VerticalScrollSnap::UpdateLayout()
extern "C"  void VerticalScrollSnap_UpdateLayout_m3003124608 (VerticalScrollSnap_t3205272913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.VerticalScrollSnap::OnRectTransformDimensionsChange()
extern "C"  void VerticalScrollSnap_OnRectTransformDimensionsChange_m1839209563 (VerticalScrollSnap_t3205272913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.VerticalScrollSnap::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void VerticalScrollSnap_OnEndDrag_m3942610617 (VerticalScrollSnap_t3205272913 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
