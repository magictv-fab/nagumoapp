﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// ARMVideoPlaybackBehaviour
struct ARMVideoPlaybackBehaviour_t1131738883;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// MagicTV.vo.UrlInfoVO
struct UrlInfoVO_t1761987528;
// ARM.display.SimpleView
struct SimpleView_t902624229;
// UnityEngine.Light
struct Light_t4202674828;

#include "AssemblyU2DCSharp_InApp_InAppActorAbstract1901198337.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.in_apps.InAppVuforiaVideoPresentation
struct  InAppVuforiaVideoPresentation_t3711261773  : public InAppActorAbstract_t1901198337
{
public:
	// UnityEngine.Vector3 MagicTV.in_apps.InAppVuforiaVideoPresentation::_originalScale
	Vector3_t4282066566  ____originalScale_26;
	// System.Boolean MagicTV.in_apps.InAppVuforiaVideoPresentation::_canPrepare
	bool ____canPrepare_27;
	// System.Boolean MagicTV.in_apps.InAppVuforiaVideoPresentation::_needPrepare
	bool ____needPrepare_28;
	// ARMVideoPlaybackBehaviour MagicTV.in_apps.InAppVuforiaVideoPresentation::_videoController
	ARMVideoPlaybackBehaviour_t1131738883 * ____videoController_29;
	// UnityEngine.GameObject MagicTV.in_apps.InAppVuforiaVideoPresentation::VideoPlaneRender
	GameObject_t3674682005 * ___VideoPlaneRender_30;
	// System.Boolean MagicTV.in_apps.InAppVuforiaVideoPresentation::_doPrepareAlreadyCalled
	bool ____doPrepareAlreadyCalled_31;
	// MagicTV.vo.UrlInfoVO MagicTV.in_apps.InAppVuforiaVideoPresentation::_videoUrlInfoVO
	UrlInfoVO_t1761987528 * ____videoUrlInfoVO_32;
	// System.Boolean MagicTV.in_apps.InAppVuforiaVideoPresentation::_videoComponentIsReady
	bool ____videoComponentIsReady_33;
	// ARM.display.SimpleView MagicTV.in_apps.InAppVuforiaVideoPresentation::SimpleView
	SimpleView_t902624229 * ___SimpleView_34;
	// System.Boolean MagicTV.in_apps.InAppVuforiaVideoPresentation::_isDisposed
	bool ____isDisposed_35;
	// System.Boolean MagicTV.in_apps.InAppVuforiaVideoPresentation::_loading
	bool ____loading_36;
	// UnityEngine.Light MagicTV.in_apps.InAppVuforiaVideoPresentation::luz
	Light_t4202674828 * ___luz_37;
	// System.Boolean MagicTV.in_apps.InAppVuforiaVideoPresentation::_hideOnStart
	bool ____hideOnStart_38;
	// System.Int32 MagicTV.in_apps.InAppVuforiaVideoPresentation::_timer
	int32_t ____timer_39;
	// System.String MagicTV.in_apps.InAppVuforiaVideoPresentation::material_name
	String_t* ___material_name_40;
	// System.Boolean MagicTV.in_apps.InAppVuforiaVideoPresentation::_pauseLock
	bool ____pauseLock_41;

public:
	inline static int32_t get_offset_of__originalScale_26() { return static_cast<int32_t>(offsetof(InAppVuforiaVideoPresentation_t3711261773, ____originalScale_26)); }
	inline Vector3_t4282066566  get__originalScale_26() const { return ____originalScale_26; }
	inline Vector3_t4282066566 * get_address_of__originalScale_26() { return &____originalScale_26; }
	inline void set__originalScale_26(Vector3_t4282066566  value)
	{
		____originalScale_26 = value;
	}

	inline static int32_t get_offset_of__canPrepare_27() { return static_cast<int32_t>(offsetof(InAppVuforiaVideoPresentation_t3711261773, ____canPrepare_27)); }
	inline bool get__canPrepare_27() const { return ____canPrepare_27; }
	inline bool* get_address_of__canPrepare_27() { return &____canPrepare_27; }
	inline void set__canPrepare_27(bool value)
	{
		____canPrepare_27 = value;
	}

	inline static int32_t get_offset_of__needPrepare_28() { return static_cast<int32_t>(offsetof(InAppVuforiaVideoPresentation_t3711261773, ____needPrepare_28)); }
	inline bool get__needPrepare_28() const { return ____needPrepare_28; }
	inline bool* get_address_of__needPrepare_28() { return &____needPrepare_28; }
	inline void set__needPrepare_28(bool value)
	{
		____needPrepare_28 = value;
	}

	inline static int32_t get_offset_of__videoController_29() { return static_cast<int32_t>(offsetof(InAppVuforiaVideoPresentation_t3711261773, ____videoController_29)); }
	inline ARMVideoPlaybackBehaviour_t1131738883 * get__videoController_29() const { return ____videoController_29; }
	inline ARMVideoPlaybackBehaviour_t1131738883 ** get_address_of__videoController_29() { return &____videoController_29; }
	inline void set__videoController_29(ARMVideoPlaybackBehaviour_t1131738883 * value)
	{
		____videoController_29 = value;
		Il2CppCodeGenWriteBarrier(&____videoController_29, value);
	}

	inline static int32_t get_offset_of_VideoPlaneRender_30() { return static_cast<int32_t>(offsetof(InAppVuforiaVideoPresentation_t3711261773, ___VideoPlaneRender_30)); }
	inline GameObject_t3674682005 * get_VideoPlaneRender_30() const { return ___VideoPlaneRender_30; }
	inline GameObject_t3674682005 ** get_address_of_VideoPlaneRender_30() { return &___VideoPlaneRender_30; }
	inline void set_VideoPlaneRender_30(GameObject_t3674682005 * value)
	{
		___VideoPlaneRender_30 = value;
		Il2CppCodeGenWriteBarrier(&___VideoPlaneRender_30, value);
	}

	inline static int32_t get_offset_of__doPrepareAlreadyCalled_31() { return static_cast<int32_t>(offsetof(InAppVuforiaVideoPresentation_t3711261773, ____doPrepareAlreadyCalled_31)); }
	inline bool get__doPrepareAlreadyCalled_31() const { return ____doPrepareAlreadyCalled_31; }
	inline bool* get_address_of__doPrepareAlreadyCalled_31() { return &____doPrepareAlreadyCalled_31; }
	inline void set__doPrepareAlreadyCalled_31(bool value)
	{
		____doPrepareAlreadyCalled_31 = value;
	}

	inline static int32_t get_offset_of__videoUrlInfoVO_32() { return static_cast<int32_t>(offsetof(InAppVuforiaVideoPresentation_t3711261773, ____videoUrlInfoVO_32)); }
	inline UrlInfoVO_t1761987528 * get__videoUrlInfoVO_32() const { return ____videoUrlInfoVO_32; }
	inline UrlInfoVO_t1761987528 ** get_address_of__videoUrlInfoVO_32() { return &____videoUrlInfoVO_32; }
	inline void set__videoUrlInfoVO_32(UrlInfoVO_t1761987528 * value)
	{
		____videoUrlInfoVO_32 = value;
		Il2CppCodeGenWriteBarrier(&____videoUrlInfoVO_32, value);
	}

	inline static int32_t get_offset_of__videoComponentIsReady_33() { return static_cast<int32_t>(offsetof(InAppVuforiaVideoPresentation_t3711261773, ____videoComponentIsReady_33)); }
	inline bool get__videoComponentIsReady_33() const { return ____videoComponentIsReady_33; }
	inline bool* get_address_of__videoComponentIsReady_33() { return &____videoComponentIsReady_33; }
	inline void set__videoComponentIsReady_33(bool value)
	{
		____videoComponentIsReady_33 = value;
	}

	inline static int32_t get_offset_of_SimpleView_34() { return static_cast<int32_t>(offsetof(InAppVuforiaVideoPresentation_t3711261773, ___SimpleView_34)); }
	inline SimpleView_t902624229 * get_SimpleView_34() const { return ___SimpleView_34; }
	inline SimpleView_t902624229 ** get_address_of_SimpleView_34() { return &___SimpleView_34; }
	inline void set_SimpleView_34(SimpleView_t902624229 * value)
	{
		___SimpleView_34 = value;
		Il2CppCodeGenWriteBarrier(&___SimpleView_34, value);
	}

	inline static int32_t get_offset_of__isDisposed_35() { return static_cast<int32_t>(offsetof(InAppVuforiaVideoPresentation_t3711261773, ____isDisposed_35)); }
	inline bool get__isDisposed_35() const { return ____isDisposed_35; }
	inline bool* get_address_of__isDisposed_35() { return &____isDisposed_35; }
	inline void set__isDisposed_35(bool value)
	{
		____isDisposed_35 = value;
	}

	inline static int32_t get_offset_of__loading_36() { return static_cast<int32_t>(offsetof(InAppVuforiaVideoPresentation_t3711261773, ____loading_36)); }
	inline bool get__loading_36() const { return ____loading_36; }
	inline bool* get_address_of__loading_36() { return &____loading_36; }
	inline void set__loading_36(bool value)
	{
		____loading_36 = value;
	}

	inline static int32_t get_offset_of_luz_37() { return static_cast<int32_t>(offsetof(InAppVuforiaVideoPresentation_t3711261773, ___luz_37)); }
	inline Light_t4202674828 * get_luz_37() const { return ___luz_37; }
	inline Light_t4202674828 ** get_address_of_luz_37() { return &___luz_37; }
	inline void set_luz_37(Light_t4202674828 * value)
	{
		___luz_37 = value;
		Il2CppCodeGenWriteBarrier(&___luz_37, value);
	}

	inline static int32_t get_offset_of__hideOnStart_38() { return static_cast<int32_t>(offsetof(InAppVuforiaVideoPresentation_t3711261773, ____hideOnStart_38)); }
	inline bool get__hideOnStart_38() const { return ____hideOnStart_38; }
	inline bool* get_address_of__hideOnStart_38() { return &____hideOnStart_38; }
	inline void set__hideOnStart_38(bool value)
	{
		____hideOnStart_38 = value;
	}

	inline static int32_t get_offset_of__timer_39() { return static_cast<int32_t>(offsetof(InAppVuforiaVideoPresentation_t3711261773, ____timer_39)); }
	inline int32_t get__timer_39() const { return ____timer_39; }
	inline int32_t* get_address_of__timer_39() { return &____timer_39; }
	inline void set__timer_39(int32_t value)
	{
		____timer_39 = value;
	}

	inline static int32_t get_offset_of_material_name_40() { return static_cast<int32_t>(offsetof(InAppVuforiaVideoPresentation_t3711261773, ___material_name_40)); }
	inline String_t* get_material_name_40() const { return ___material_name_40; }
	inline String_t** get_address_of_material_name_40() { return &___material_name_40; }
	inline void set_material_name_40(String_t* value)
	{
		___material_name_40 = value;
		Il2CppCodeGenWriteBarrier(&___material_name_40, value);
	}

	inline static int32_t get_offset_of__pauseLock_41() { return static_cast<int32_t>(offsetof(InAppVuforiaVideoPresentation_t3711261773, ____pauseLock_41)); }
	inline bool get__pauseLock_41() const { return ____pauseLock_41; }
	inline bool* get_address_of__pauseLock_41() { return &____pauseLock_41; }
	inline void set__pauseLock_41(bool value)
	{
		____pauseLock_41 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
