﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t1297217088;
// OneSignal/NotificationReceived
struct NotificationReceived_t1402323693;
// OneSignal/NotificationOpened
struct NotificationOpened_t3322442869;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OneSignal/UnityBuilder
struct  UnityBuilder_t2457889383  : public Il2CppObject
{
public:
	// System.String OneSignal/UnityBuilder::appID
	String_t* ___appID_0;
	// System.String OneSignal/UnityBuilder::googleProjectNumber
	String_t* ___googleProjectNumber_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Boolean> OneSignal/UnityBuilder::iOSSettings
	Dictionary_2_t1297217088 * ___iOSSettings_2;
	// OneSignal/NotificationReceived OneSignal/UnityBuilder::notificationReceivedDelegate
	NotificationReceived_t1402323693 * ___notificationReceivedDelegate_3;
	// OneSignal/NotificationOpened OneSignal/UnityBuilder::notificationOpenedDelegate
	NotificationOpened_t3322442869 * ___notificationOpenedDelegate_4;

public:
	inline static int32_t get_offset_of_appID_0() { return static_cast<int32_t>(offsetof(UnityBuilder_t2457889383, ___appID_0)); }
	inline String_t* get_appID_0() const { return ___appID_0; }
	inline String_t** get_address_of_appID_0() { return &___appID_0; }
	inline void set_appID_0(String_t* value)
	{
		___appID_0 = value;
		Il2CppCodeGenWriteBarrier(&___appID_0, value);
	}

	inline static int32_t get_offset_of_googleProjectNumber_1() { return static_cast<int32_t>(offsetof(UnityBuilder_t2457889383, ___googleProjectNumber_1)); }
	inline String_t* get_googleProjectNumber_1() const { return ___googleProjectNumber_1; }
	inline String_t** get_address_of_googleProjectNumber_1() { return &___googleProjectNumber_1; }
	inline void set_googleProjectNumber_1(String_t* value)
	{
		___googleProjectNumber_1 = value;
		Il2CppCodeGenWriteBarrier(&___googleProjectNumber_1, value);
	}

	inline static int32_t get_offset_of_iOSSettings_2() { return static_cast<int32_t>(offsetof(UnityBuilder_t2457889383, ___iOSSettings_2)); }
	inline Dictionary_2_t1297217088 * get_iOSSettings_2() const { return ___iOSSettings_2; }
	inline Dictionary_2_t1297217088 ** get_address_of_iOSSettings_2() { return &___iOSSettings_2; }
	inline void set_iOSSettings_2(Dictionary_2_t1297217088 * value)
	{
		___iOSSettings_2 = value;
		Il2CppCodeGenWriteBarrier(&___iOSSettings_2, value);
	}

	inline static int32_t get_offset_of_notificationReceivedDelegate_3() { return static_cast<int32_t>(offsetof(UnityBuilder_t2457889383, ___notificationReceivedDelegate_3)); }
	inline NotificationReceived_t1402323693 * get_notificationReceivedDelegate_3() const { return ___notificationReceivedDelegate_3; }
	inline NotificationReceived_t1402323693 ** get_address_of_notificationReceivedDelegate_3() { return &___notificationReceivedDelegate_3; }
	inline void set_notificationReceivedDelegate_3(NotificationReceived_t1402323693 * value)
	{
		___notificationReceivedDelegate_3 = value;
		Il2CppCodeGenWriteBarrier(&___notificationReceivedDelegate_3, value);
	}

	inline static int32_t get_offset_of_notificationOpenedDelegate_4() { return static_cast<int32_t>(offsetof(UnityBuilder_t2457889383, ___notificationOpenedDelegate_4)); }
	inline NotificationOpened_t3322442869 * get_notificationOpenedDelegate_4() const { return ___notificationOpenedDelegate_4; }
	inline NotificationOpened_t3322442869 ** get_address_of_notificationOpenedDelegate_4() { return &___notificationOpenedDelegate_4; }
	inline void set_notificationOpenedDelegate_4(NotificationOpened_t3322442869 * value)
	{
		___notificationOpenedDelegate_4 = value;
		Il2CppCodeGenWriteBarrier(&___notificationOpenedDelegate_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
