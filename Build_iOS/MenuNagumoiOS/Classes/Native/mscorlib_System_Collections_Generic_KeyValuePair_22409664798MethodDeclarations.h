﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22409664798.h"
#include "AssemblyU2DCSharp_MagicTV_globals_Perspective644553802.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.KeyValuePair`2<MagicTV.globals.Perspective,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1794069694_gshared (KeyValuePair_2_t2409664798 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m1794069694(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2409664798 *, int32_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m1794069694_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<MagicTV.globals.Perspective,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1012642026_gshared (KeyValuePair_2_t2409664798 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m1012642026(__this, method) ((  int32_t (*) (KeyValuePair_2_t2409664798 *, const MethodInfo*))KeyValuePair_2_get_Key_m1012642026_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<MagicTV.globals.Perspective,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m892438187_gshared (KeyValuePair_2_t2409664798 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m892438187(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2409664798 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m892438187_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<MagicTV.globals.Perspective,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3824131982_gshared (KeyValuePair_2_t2409664798 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m3824131982(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t2409664798 *, const MethodInfo*))KeyValuePair_2_get_Value_m3824131982_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<MagicTV.globals.Perspective,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2462289195_gshared (KeyValuePair_2_t2409664798 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m2462289195(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2409664798 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m2462289195_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<MagicTV.globals.Perspective,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m4140516093_gshared (KeyValuePair_2_t2409664798 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m4140516093(__this, method) ((  String_t* (*) (KeyValuePair_2_t2409664798 *, const MethodInfo*))KeyValuePair_2_ToString_m4140516093_gshared)(__this, method)
