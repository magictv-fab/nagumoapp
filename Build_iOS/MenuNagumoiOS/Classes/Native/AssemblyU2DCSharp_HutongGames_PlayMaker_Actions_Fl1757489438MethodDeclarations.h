﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FloatAbs
struct FloatAbs_t1757489438;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FloatAbs::.ctor()
extern "C"  void FloatAbs__ctor_m1440920328 (FloatAbs_t1757489438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatAbs::Reset()
extern "C"  void FloatAbs_Reset_m3382320565 (FloatAbs_t1757489438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatAbs::OnEnter()
extern "C"  void FloatAbs_OnEnter_m1136694495 (FloatAbs_t1757489438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatAbs::OnUpdate()
extern "C"  void FloatAbs_OnUpdate_m11350340 (FloatAbs_t1757489438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatAbs::DoFloatAbs()
extern "C"  void FloatAbs_DoFloatAbs_m2648064733 (FloatAbs_t1757489438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
