﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkGetMinimumAllocatableViewIDs
struct NetworkGetMinimumAllocatableViewIDs_t2099050725;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkGetMinimumAllocatableViewIDs::.ctor()
extern "C"  void NetworkGetMinimumAllocatableViewIDs__ctor_m3784633329 (NetworkGetMinimumAllocatableViewIDs_t2099050725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetMinimumAllocatableViewIDs::Reset()
extern "C"  void NetworkGetMinimumAllocatableViewIDs_Reset_m1431066270 (NetworkGetMinimumAllocatableViewIDs_t2099050725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetMinimumAllocatableViewIDs::OnEnter()
extern "C"  void NetworkGetMinimumAllocatableViewIDs_OnEnter_m2882025352 (NetworkGetMinimumAllocatableViewIDs_t2099050725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
