﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Extensions.HorizontalScrollSnap
struct HorizontalScrollSnap_t2651831999;
// Header_toggle_buttons
struct Header_toggle_buttons_t797597032;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRM_ScrollViewsManager
struct  CRM_ScrollViewsManager_t2923429133  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean CRM_ScrollViewsManager::initScroollSnaps
	bool ___initScroollSnaps_3;
	// UnityEngine.UI.Extensions.HorizontalScrollSnap CRM_ScrollViewsManager::_horizontalScrollSnapOfertas
	HorizontalScrollSnap_t2651831999 * ____horizontalScrollSnapOfertas_4;
	// UnityEngine.UI.Extensions.HorizontalScrollSnap CRM_ScrollViewsManager::_horizontalScrollSnapHeader
	HorizontalScrollSnap_t2651831999 * ____horizontalScrollSnapHeader_5;
	// Header_toggle_buttons CRM_ScrollViewsManager::togglesbuttonscript
	Header_toggle_buttons_t797597032 * ___togglesbuttonscript_6;

public:
	inline static int32_t get_offset_of_initScroollSnaps_3() { return static_cast<int32_t>(offsetof(CRM_ScrollViewsManager_t2923429133, ___initScroollSnaps_3)); }
	inline bool get_initScroollSnaps_3() const { return ___initScroollSnaps_3; }
	inline bool* get_address_of_initScroollSnaps_3() { return &___initScroollSnaps_3; }
	inline void set_initScroollSnaps_3(bool value)
	{
		___initScroollSnaps_3 = value;
	}

	inline static int32_t get_offset_of__horizontalScrollSnapOfertas_4() { return static_cast<int32_t>(offsetof(CRM_ScrollViewsManager_t2923429133, ____horizontalScrollSnapOfertas_4)); }
	inline HorizontalScrollSnap_t2651831999 * get__horizontalScrollSnapOfertas_4() const { return ____horizontalScrollSnapOfertas_4; }
	inline HorizontalScrollSnap_t2651831999 ** get_address_of__horizontalScrollSnapOfertas_4() { return &____horizontalScrollSnapOfertas_4; }
	inline void set__horizontalScrollSnapOfertas_4(HorizontalScrollSnap_t2651831999 * value)
	{
		____horizontalScrollSnapOfertas_4 = value;
		Il2CppCodeGenWriteBarrier(&____horizontalScrollSnapOfertas_4, value);
	}

	inline static int32_t get_offset_of__horizontalScrollSnapHeader_5() { return static_cast<int32_t>(offsetof(CRM_ScrollViewsManager_t2923429133, ____horizontalScrollSnapHeader_5)); }
	inline HorizontalScrollSnap_t2651831999 * get__horizontalScrollSnapHeader_5() const { return ____horizontalScrollSnapHeader_5; }
	inline HorizontalScrollSnap_t2651831999 ** get_address_of__horizontalScrollSnapHeader_5() { return &____horizontalScrollSnapHeader_5; }
	inline void set__horizontalScrollSnapHeader_5(HorizontalScrollSnap_t2651831999 * value)
	{
		____horizontalScrollSnapHeader_5 = value;
		Il2CppCodeGenWriteBarrier(&____horizontalScrollSnapHeader_5, value);
	}

	inline static int32_t get_offset_of_togglesbuttonscript_6() { return static_cast<int32_t>(offsetof(CRM_ScrollViewsManager_t2923429133, ___togglesbuttonscript_6)); }
	inline Header_toggle_buttons_t797597032 * get_togglesbuttonscript_6() const { return ___togglesbuttonscript_6; }
	inline Header_toggle_buttons_t797597032 ** get_address_of_togglesbuttonscript_6() { return &___togglesbuttonscript_6; }
	inline void set_togglesbuttonscript_6(Header_toggle_buttons_t797597032 * value)
	{
		___togglesbuttonscript_6 = value;
		Il2CppCodeGenWriteBarrier(&___togglesbuttonscript_6, value);
	}
};

struct CRM_ScrollViewsManager_t2923429133_StaticFields
{
public:
	// System.Int32 CRM_ScrollViewsManager::dowloaded
	int32_t ___dowloaded_2;

public:
	inline static int32_t get_offset_of_dowloaded_2() { return static_cast<int32_t>(offsetof(CRM_ScrollViewsManager_t2923429133_StaticFields, ___dowloaded_2)); }
	inline int32_t get_dowloaded_2() const { return ___dowloaded_2; }
	inline int32_t* get_address_of_dowloaded_2() { return &___dowloaded_2; }
	inline void set_dowloaded_2(int32_t value)
	{
		___dowloaded_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
