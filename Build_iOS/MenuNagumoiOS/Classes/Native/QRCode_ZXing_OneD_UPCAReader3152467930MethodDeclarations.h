﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.UPCAReader
struct UPCAReader_t3152467930;
// ZXing.Result
struct Result_t2610723219;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>
struct IDictionary_2_t759989846;
// ZXing.BinaryBitmap
struct BinaryBitmap_t2444664454;
// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"
#include "QRCode_ZXing_BinaryBitmap2444664454.h"
#include "QRCode_ZXing_BarcodeFormat4201805817.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "QRCode_ZXing_Result2610723219.h"

// ZXing.Result ZXing.OneD.UPCAReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Int32[],System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  Result_t2610723219 * UPCAReader_decodeRow_m4024385753 (UPCAReader_t3152467930 * __this, int32_t ___rowNumber0, BitArray_t4163851164 * ___row1, Int32U5BU5D_t3230847821* ___startGuardRange2, Il2CppObject* ___hints3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Result ZXing.OneD.UPCAReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  Result_t2610723219 * UPCAReader_decodeRow_m2735433164 (UPCAReader_t3152467930 * __this, int32_t ___rowNumber0, BitArray_t4163851164 * ___row1, Il2CppObject* ___hints2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Result ZXing.OneD.UPCAReader::decode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  Result_t2610723219 * UPCAReader_decode_m2592405972 (UPCAReader_t3152467930 * __this, BinaryBitmap_t2444664454 * ___image0, Il2CppObject* ___hints1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.BarcodeFormat ZXing.OneD.UPCAReader::get_BarcodeFormat()
extern "C"  int32_t UPCAReader_get_BarcodeFormat_m1461341275 (UPCAReader_t3152467930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.UPCAReader::decodeMiddle(ZXing.Common.BitArray,System.Int32[],System.Text.StringBuilder)
extern "C"  int32_t UPCAReader_decodeMiddle_m2105618332 (UPCAReader_t3152467930 * __this, BitArray_t4163851164 * ___row0, Int32U5BU5D_t3230847821* ___startRange1, StringBuilder_t243639308 * ___resultString2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Result ZXing.OneD.UPCAReader::maybeReturnResult(ZXing.Result)
extern "C"  Result_t2610723219 * UPCAReader_maybeReturnResult_m681546667 (Il2CppObject * __this /* static, unused */, Result_t2610723219 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.UPCAReader::.ctor()
extern "C"  void UPCAReader__ctor_m31515017 (UPCAReader_t3152467930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
