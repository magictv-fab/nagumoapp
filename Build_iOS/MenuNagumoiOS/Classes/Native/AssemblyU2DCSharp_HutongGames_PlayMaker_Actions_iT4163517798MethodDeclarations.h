﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenPunchScale
struct iTweenPunchScale_t4163517798;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenPunchScale::.ctor()
extern "C"  void iTweenPunchScale__ctor_m2219202496 (iTweenPunchScale_t4163517798 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchScale::Reset()
extern "C"  void iTweenPunchScale_Reset_m4160602733 (iTweenPunchScale_t4163517798 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchScale::OnEnter()
extern "C"  void iTweenPunchScale_OnEnter_m1741548439 (iTweenPunchScale_t4163517798 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchScale::OnExit()
extern "C"  void iTweenPunchScale_OnExit_m1589119265 (iTweenPunchScale_t4163517798 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchScale::DoiTween()
extern "C"  void iTweenPunchScale_DoiTween_m3899526289 (iTweenPunchScale_t4163517798 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
