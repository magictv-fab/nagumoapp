﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.display.RotateObject
struct RotateObject_t3491987816;
// ARM.display.RotateObject/OnRotateComplete
struct OnRotateComplete_t2396770826;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ARM_display_RotateObject_OnRotat2396770826.h"

// System.Void ARM.display.RotateObject::.ctor()
extern "C"  void RotateObject__ctor_m1715442403 (RotateObject_t3491987816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.display.RotateObject::AddOnRotateCompleteEventHandler(ARM.display.RotateObject/OnRotateComplete)
extern "C"  void RotateObject_AddOnRotateCompleteEventHandler_m238403683 (RotateObject_t3491987816 * __this, OnRotateComplete_t2396770826 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.display.RotateObject::RemoveOnRotateCompleteEventHandler(ARM.display.RotateObject/OnRotateComplete)
extern "C"  void RotateObject_RemoveOnRotateCompleteEventHandler_m54675744 (RotateObject_t3491987816 * __this, OnRotateComplete_t2396770826 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.display.RotateObject::RaiseOnRotateComplete()
extern "C"  void RotateObject_RaiseOnRotateComplete_m2751707072 (RotateObject_t3491987816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.display.RotateObject::Start()
extern "C"  void RotateObject_Start_m662580195 (RotateObject_t3491987816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.display.RotateObject::Update()
extern "C"  void RotateObject_Update_m3365969034 (RotateObject_t3491987816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
