﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_EventArgs2540831021.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Core.ScanEventArgs
struct  ScanEventArgs_t1070518092  : public EventArgs_t2540831021
{
public:
	// System.String ICSharpCode.SharpZipLib.Core.ScanEventArgs::name_
	String_t* ___name__1;
	// System.Boolean ICSharpCode.SharpZipLib.Core.ScanEventArgs::continueRunning_
	bool ___continueRunning__2;

public:
	inline static int32_t get_offset_of_name__1() { return static_cast<int32_t>(offsetof(ScanEventArgs_t1070518092, ___name__1)); }
	inline String_t* get_name__1() const { return ___name__1; }
	inline String_t** get_address_of_name__1() { return &___name__1; }
	inline void set_name__1(String_t* value)
	{
		___name__1 = value;
		Il2CppCodeGenWriteBarrier(&___name__1, value);
	}

	inline static int32_t get_offset_of_continueRunning__2() { return static_cast<int32_t>(offsetof(ScanEventArgs_t1070518092, ___continueRunning__2)); }
	inline bool get_continueRunning__2() const { return ___continueRunning__2; }
	inline bool* get_address_of_continueRunning__2() { return &___continueRunning__2; }
	inline void set_continueRunning__2(bool value)
	{
		___continueRunning__2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
