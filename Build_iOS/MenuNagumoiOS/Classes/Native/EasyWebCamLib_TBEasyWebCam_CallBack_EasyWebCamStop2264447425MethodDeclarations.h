﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TBEasyWebCam.CallBack.EasyWebCamStopedDelegate
struct EasyWebCamStopedDelegate_t2264447425;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void TBEasyWebCam.CallBack.EasyWebCamStopedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void EasyWebCamStopedDelegate__ctor_m4247637174 (EasyWebCamStopedDelegate_t2264447425 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBEasyWebCam.CallBack.EasyWebCamStopedDelegate::Invoke()
extern "C"  void EasyWebCamStopedDelegate_Invoke_m4130992272 (EasyWebCamStopedDelegate_t2264447425 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult TBEasyWebCam.CallBack.EasyWebCamStopedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * EasyWebCamStopedDelegate_BeginInvoke_m2210574195 (EasyWebCamStopedDelegate_t2264447425 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBEasyWebCam.CallBack.EasyWebCamStopedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void EasyWebCamStopedDelegate_EndInvoke_m295700934 (EasyWebCamStopedDelegate_t2264447425 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
