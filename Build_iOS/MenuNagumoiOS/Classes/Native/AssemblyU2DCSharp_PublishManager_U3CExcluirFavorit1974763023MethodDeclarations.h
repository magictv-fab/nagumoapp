﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PublishManager/<ExcluirFavorito>c__Iterator76
struct U3CExcluirFavoritoU3Ec__Iterator76_t1974763023;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PublishManager/<ExcluirFavorito>c__Iterator76::.ctor()
extern "C"  void U3CExcluirFavoritoU3Ec__Iterator76__ctor_m665035836 (U3CExcluirFavoritoU3Ec__Iterator76_t1974763023 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PublishManager/<ExcluirFavorito>c__Iterator76::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CExcluirFavoritoU3Ec__Iterator76_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1402774870 (U3CExcluirFavoritoU3Ec__Iterator76_t1974763023 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PublishManager/<ExcluirFavorito>c__Iterator76::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CExcluirFavoritoU3Ec__Iterator76_System_Collections_IEnumerator_get_Current_m2885686506 (U3CExcluirFavoritoU3Ec__Iterator76_t1974763023 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PublishManager/<ExcluirFavorito>c__Iterator76::MoveNext()
extern "C"  bool U3CExcluirFavoritoU3Ec__Iterator76_MoveNext_m1018686264 (U3CExcluirFavoritoU3Ec__Iterator76_t1974763023 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PublishManager/<ExcluirFavorito>c__Iterator76::Dispose()
extern "C"  void U3CExcluirFavoritoU3Ec__Iterator76_Dispose_m3342561657 (U3CExcluirFavoritoU3Ec__Iterator76_t1974763023 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PublishManager/<ExcluirFavorito>c__Iterator76::Reset()
extern "C"  void U3CExcluirFavoritoU3Ec__Iterator76_Reset_m2606436073 (U3CExcluirFavoritoU3Ec__Iterator76_t1974763023 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
