﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// UnityEngine.AnimationState
struct AnimationState_t3682323633;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EnableAnimation
struct  EnableAnimation_t3592025679  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.EnableAnimation::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.EnableAnimation::animName
	FsmString_t952858651 * ___animName_10;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.EnableAnimation::enable
	FsmBool_t1075959796 * ___enable_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.EnableAnimation::resetOnExit
	FsmBool_t1075959796 * ___resetOnExit_12;
	// UnityEngine.AnimationState HutongGames.PlayMaker.Actions.EnableAnimation::anim
	AnimationState_t3682323633 * ___anim_13;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(EnableAnimation_t3592025679, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_animName_10() { return static_cast<int32_t>(offsetof(EnableAnimation_t3592025679, ___animName_10)); }
	inline FsmString_t952858651 * get_animName_10() const { return ___animName_10; }
	inline FsmString_t952858651 ** get_address_of_animName_10() { return &___animName_10; }
	inline void set_animName_10(FsmString_t952858651 * value)
	{
		___animName_10 = value;
		Il2CppCodeGenWriteBarrier(&___animName_10, value);
	}

	inline static int32_t get_offset_of_enable_11() { return static_cast<int32_t>(offsetof(EnableAnimation_t3592025679, ___enable_11)); }
	inline FsmBool_t1075959796 * get_enable_11() const { return ___enable_11; }
	inline FsmBool_t1075959796 ** get_address_of_enable_11() { return &___enable_11; }
	inline void set_enable_11(FsmBool_t1075959796 * value)
	{
		___enable_11 = value;
		Il2CppCodeGenWriteBarrier(&___enable_11, value);
	}

	inline static int32_t get_offset_of_resetOnExit_12() { return static_cast<int32_t>(offsetof(EnableAnimation_t3592025679, ___resetOnExit_12)); }
	inline FsmBool_t1075959796 * get_resetOnExit_12() const { return ___resetOnExit_12; }
	inline FsmBool_t1075959796 ** get_address_of_resetOnExit_12() { return &___resetOnExit_12; }
	inline void set_resetOnExit_12(FsmBool_t1075959796 * value)
	{
		___resetOnExit_12 = value;
		Il2CppCodeGenWriteBarrier(&___resetOnExit_12, value);
	}

	inline static int32_t get_offset_of_anim_13() { return static_cast<int32_t>(offsetof(EnableAnimation_t3592025679, ___anim_13)); }
	inline AnimationState_t3682323633 * get_anim_13() const { return ___anim_13; }
	inline AnimationState_t3682323633 ** get_address_of_anim_13() { return &___anim_13; }
	inline void set_anim_13(AnimationState_t3682323633 * value)
	{
		___anim_13 = value;
		Il2CppCodeGenWriteBarrier(&___anim_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
