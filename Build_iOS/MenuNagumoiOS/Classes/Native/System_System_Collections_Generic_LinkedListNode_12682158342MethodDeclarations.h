﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedListNode_13787551078MethodDeclarations.h"

// System.Void System.Collections.Generic.LinkedListNode`1<ZXing.Aztec.Internal.State>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
#define LinkedListNode_1__ctor_m2881431255(__this, ___list0, ___value1, method) ((  void (*) (LinkedListNode_1_t2682158342 *, LinkedList_1_t940058804 *, State_t3065423635 *, const MethodInfo*))LinkedListNode_1__ctor_m648136130_gshared)(__this, ___list0, ___value1, method)
// System.Void System.Collections.Generic.LinkedListNode`1<ZXing.Aztec.Internal.State>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedListNode_1__ctor_m322097015(__this, ___list0, ___value1, ___previousNode2, ___nextNode3, method) ((  void (*) (LinkedListNode_1_t2682158342 *, LinkedList_1_t940058804 *, State_t3065423635 *, LinkedListNode_1_t2682158342 *, LinkedListNode_1_t2682158342 *, const MethodInfo*))LinkedListNode_1__ctor_m448391458_gshared)(__this, ___list0, ___value1, ___previousNode2, ___nextNode3, method)
// System.Void System.Collections.Generic.LinkedListNode`1<ZXing.Aztec.Internal.State>::Detach()
#define LinkedListNode_1_Detach_m1437514089(__this, method) ((  void (*) (LinkedListNode_1_t2682158342 *, const MethodInfo*))LinkedListNode_1_Detach_m3406254942_gshared)(__this, method)
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<ZXing.Aztec.Internal.State>::get_List()
#define LinkedListNode_1_get_List_m721228813(__this, method) ((  LinkedList_1_t940058804 * (*) (LinkedListNode_1_t2682158342 *, const MethodInfo*))LinkedListNode_1_get_List_m3467110818_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<ZXing.Aztec.Internal.State>::get_Next()
#define LinkedListNode_1_get_Next_m768465728(__this, method) ((  LinkedListNode_1_t2682158342 * (*) (LinkedListNode_1_t2682158342 *, const MethodInfo*))LinkedListNode_1_get_Next_m1427618777_gshared)(__this, method)
// T System.Collections.Generic.LinkedListNode`1<ZXing.Aztec.Internal.State>::get_Value()
#define LinkedListNode_1_get_Value_m2768121171(__this, method) ((  State_t3065423635 * (*) (LinkedListNode_1_t2682158342 *, const MethodInfo*))LinkedListNode_1_get_Value_m702633824_gshared)(__this, method)
