﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// CarrosselController
struct CarrosselController_t3044697396;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CarrosselController
struct  CarrosselController_t3044697396  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject CarrosselController::carrocel
	GameObject_t3674682005 * ___carrocel_2;

public:
	inline static int32_t get_offset_of_carrocel_2() { return static_cast<int32_t>(offsetof(CarrosselController_t3044697396, ___carrocel_2)); }
	inline GameObject_t3674682005 * get_carrocel_2() const { return ___carrocel_2; }
	inline GameObject_t3674682005 ** get_address_of_carrocel_2() { return &___carrocel_2; }
	inline void set_carrocel_2(GameObject_t3674682005 * value)
	{
		___carrocel_2 = value;
		Il2CppCodeGenWriteBarrier(&___carrocel_2, value);
	}
};

struct CarrosselController_t3044697396_StaticFields
{
public:
	// CarrosselController CarrosselController::instance
	CarrosselController_t3044697396 * ___instance_3;

public:
	inline static int32_t get_offset_of_instance_3() { return static_cast<int32_t>(offsetof(CarrosselController_t3044697396_StaticFields, ___instance_3)); }
	inline CarrosselController_t3044697396 * get_instance_3() const { return ___instance_3; }
	inline CarrosselController_t3044697396 ** get_address_of_instance_3() { return &___instance_3; }
	inline void set_instance_3(CarrosselController_t3044697396 * value)
	{
		___instance_3 = value;
		Il2CppCodeGenWriteBarrier(&___instance_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
