﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OneSignal/NotificationOpened
struct NotificationOpened_t3322442869;
// System.Object
struct Il2CppObject;
// OSNotificationOpenedResult
struct OSNotificationOpenedResult_t158818421;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_OSNotificationOpenedResult158818421.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void OneSignal/NotificationOpened::.ctor(System.Object,System.IntPtr)
extern "C"  void NotificationOpened__ctor_m3531124380 (NotificationOpened_t3322442869 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal/NotificationOpened::Invoke(OSNotificationOpenedResult)
extern "C"  void NotificationOpened_Invoke_m990592481 (NotificationOpened_t3322442869 * __this, OSNotificationOpenedResult_t158818421 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult OneSignal/NotificationOpened::BeginInvoke(OSNotificationOpenedResult,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * NotificationOpened_BeginInvoke_m62993958 (NotificationOpened_t3322442869 * __this, OSNotificationOpenedResult_t158818421 * ___result0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal/NotificationOpened::EndInvoke(System.IAsyncResult)
extern "C"  void NotificationOpened_EndInvoke_m3653488300 (NotificationOpened_t3322442869 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
