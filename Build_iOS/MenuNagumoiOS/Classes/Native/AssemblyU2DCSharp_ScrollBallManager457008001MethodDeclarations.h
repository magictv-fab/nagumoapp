﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScrollBallManager
struct ScrollBallManager_t457008001;

#include "codegen/il2cpp-codegen.h"

// System.Void ScrollBallManager::.ctor()
extern "C"  void ScrollBallManager__ctor_m1358408458 (ScrollBallManager_t457008001 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollBallManager::Start()
extern "C"  void ScrollBallManager_Start_m305546250 (ScrollBallManager_t457008001 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollBallManager::Update()
extern "C"  void ScrollBallManager_Update_m887851331 (ScrollBallManager_t457008001 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
