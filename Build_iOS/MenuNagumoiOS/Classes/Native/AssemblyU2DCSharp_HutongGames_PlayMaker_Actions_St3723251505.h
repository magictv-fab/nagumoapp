﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// System.String
struct String_t;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.StringChanged
struct  StringChanged_t3723251505  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.StringChanged::stringVariable
	FsmString_t952858651 * ___stringVariable_9;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.StringChanged::changedEvent
	FsmEvent_t2133468028 * ___changedEvent_10;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.StringChanged::storeResult
	FsmBool_t1075959796 * ___storeResult_11;
	// System.String HutongGames.PlayMaker.Actions.StringChanged::previousValue
	String_t* ___previousValue_12;

public:
	inline static int32_t get_offset_of_stringVariable_9() { return static_cast<int32_t>(offsetof(StringChanged_t3723251505, ___stringVariable_9)); }
	inline FsmString_t952858651 * get_stringVariable_9() const { return ___stringVariable_9; }
	inline FsmString_t952858651 ** get_address_of_stringVariable_9() { return &___stringVariable_9; }
	inline void set_stringVariable_9(FsmString_t952858651 * value)
	{
		___stringVariable_9 = value;
		Il2CppCodeGenWriteBarrier(&___stringVariable_9, value);
	}

	inline static int32_t get_offset_of_changedEvent_10() { return static_cast<int32_t>(offsetof(StringChanged_t3723251505, ___changedEvent_10)); }
	inline FsmEvent_t2133468028 * get_changedEvent_10() const { return ___changedEvent_10; }
	inline FsmEvent_t2133468028 ** get_address_of_changedEvent_10() { return &___changedEvent_10; }
	inline void set_changedEvent_10(FsmEvent_t2133468028 * value)
	{
		___changedEvent_10 = value;
		Il2CppCodeGenWriteBarrier(&___changedEvent_10, value);
	}

	inline static int32_t get_offset_of_storeResult_11() { return static_cast<int32_t>(offsetof(StringChanged_t3723251505, ___storeResult_11)); }
	inline FsmBool_t1075959796 * get_storeResult_11() const { return ___storeResult_11; }
	inline FsmBool_t1075959796 ** get_address_of_storeResult_11() { return &___storeResult_11; }
	inline void set_storeResult_11(FsmBool_t1075959796 * value)
	{
		___storeResult_11 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_11, value);
	}

	inline static int32_t get_offset_of_previousValue_12() { return static_cast<int32_t>(offsetof(StringChanged_t3723251505, ___previousValue_12)); }
	inline String_t* get_previousValue_12() const { return ___previousValue_12; }
	inline String_t** get_address_of_previousValue_12() { return &___previousValue_12; }
	inline void set_previousValue_12(String_t* value)
	{
		___previousValue_12 = value;
		Il2CppCodeGenWriteBarrier(&___previousValue_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
