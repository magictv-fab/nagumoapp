﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// PlayMakerAnimatorMoveProxy
struct PlayMakerAnimatorMoveProxy_t4175490694;
// UnityEngine.Animator
struct Animator_t2776330603;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo
struct  GetAnimatorNextStateInfo_t3502000247  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::layerIndex
	FsmInt_t1596138449 * ___layerIndex_10;
	// System.Boolean HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::everyFrame
	bool ___everyFrame_11;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::name
	FsmString_t952858651 * ___name_12;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::nameHash
	FsmInt_t1596138449 * ___nameHash_13;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::fullPathHash
	FsmInt_t1596138449 * ___fullPathHash_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::shortPathHash
	FsmInt_t1596138449 * ___shortPathHash_15;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::tagHash
	FsmInt_t1596138449 * ___tagHash_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::isStateLooping
	FsmBool_t1075959796 * ___isStateLooping_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::length
	FsmFloat_t2134102846 * ___length_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::normalizedTime
	FsmFloat_t2134102846 * ___normalizedTime_19;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::loopCount
	FsmInt_t1596138449 * ___loopCount_20;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::currentLoopProgress
	FsmFloat_t2134102846 * ___currentLoopProgress_21;
	// PlayMakerAnimatorMoveProxy HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::_animatorProxy
	PlayMakerAnimatorMoveProxy_t4175490694 * ____animatorProxy_22;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::_animator
	Animator_t2776330603 * ____animator_23;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t3502000247, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_layerIndex_10() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t3502000247, ___layerIndex_10)); }
	inline FsmInt_t1596138449 * get_layerIndex_10() const { return ___layerIndex_10; }
	inline FsmInt_t1596138449 ** get_address_of_layerIndex_10() { return &___layerIndex_10; }
	inline void set_layerIndex_10(FsmInt_t1596138449 * value)
	{
		___layerIndex_10 = value;
		Il2CppCodeGenWriteBarrier(&___layerIndex_10, value);
	}

	inline static int32_t get_offset_of_everyFrame_11() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t3502000247, ___everyFrame_11)); }
	inline bool get_everyFrame_11() const { return ___everyFrame_11; }
	inline bool* get_address_of_everyFrame_11() { return &___everyFrame_11; }
	inline void set_everyFrame_11(bool value)
	{
		___everyFrame_11 = value;
	}

	inline static int32_t get_offset_of_name_12() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t3502000247, ___name_12)); }
	inline FsmString_t952858651 * get_name_12() const { return ___name_12; }
	inline FsmString_t952858651 ** get_address_of_name_12() { return &___name_12; }
	inline void set_name_12(FsmString_t952858651 * value)
	{
		___name_12 = value;
		Il2CppCodeGenWriteBarrier(&___name_12, value);
	}

	inline static int32_t get_offset_of_nameHash_13() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t3502000247, ___nameHash_13)); }
	inline FsmInt_t1596138449 * get_nameHash_13() const { return ___nameHash_13; }
	inline FsmInt_t1596138449 ** get_address_of_nameHash_13() { return &___nameHash_13; }
	inline void set_nameHash_13(FsmInt_t1596138449 * value)
	{
		___nameHash_13 = value;
		Il2CppCodeGenWriteBarrier(&___nameHash_13, value);
	}

	inline static int32_t get_offset_of_fullPathHash_14() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t3502000247, ___fullPathHash_14)); }
	inline FsmInt_t1596138449 * get_fullPathHash_14() const { return ___fullPathHash_14; }
	inline FsmInt_t1596138449 ** get_address_of_fullPathHash_14() { return &___fullPathHash_14; }
	inline void set_fullPathHash_14(FsmInt_t1596138449 * value)
	{
		___fullPathHash_14 = value;
		Il2CppCodeGenWriteBarrier(&___fullPathHash_14, value);
	}

	inline static int32_t get_offset_of_shortPathHash_15() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t3502000247, ___shortPathHash_15)); }
	inline FsmInt_t1596138449 * get_shortPathHash_15() const { return ___shortPathHash_15; }
	inline FsmInt_t1596138449 ** get_address_of_shortPathHash_15() { return &___shortPathHash_15; }
	inline void set_shortPathHash_15(FsmInt_t1596138449 * value)
	{
		___shortPathHash_15 = value;
		Il2CppCodeGenWriteBarrier(&___shortPathHash_15, value);
	}

	inline static int32_t get_offset_of_tagHash_16() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t3502000247, ___tagHash_16)); }
	inline FsmInt_t1596138449 * get_tagHash_16() const { return ___tagHash_16; }
	inline FsmInt_t1596138449 ** get_address_of_tagHash_16() { return &___tagHash_16; }
	inline void set_tagHash_16(FsmInt_t1596138449 * value)
	{
		___tagHash_16 = value;
		Il2CppCodeGenWriteBarrier(&___tagHash_16, value);
	}

	inline static int32_t get_offset_of_isStateLooping_17() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t3502000247, ___isStateLooping_17)); }
	inline FsmBool_t1075959796 * get_isStateLooping_17() const { return ___isStateLooping_17; }
	inline FsmBool_t1075959796 ** get_address_of_isStateLooping_17() { return &___isStateLooping_17; }
	inline void set_isStateLooping_17(FsmBool_t1075959796 * value)
	{
		___isStateLooping_17 = value;
		Il2CppCodeGenWriteBarrier(&___isStateLooping_17, value);
	}

	inline static int32_t get_offset_of_length_18() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t3502000247, ___length_18)); }
	inline FsmFloat_t2134102846 * get_length_18() const { return ___length_18; }
	inline FsmFloat_t2134102846 ** get_address_of_length_18() { return &___length_18; }
	inline void set_length_18(FsmFloat_t2134102846 * value)
	{
		___length_18 = value;
		Il2CppCodeGenWriteBarrier(&___length_18, value);
	}

	inline static int32_t get_offset_of_normalizedTime_19() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t3502000247, ___normalizedTime_19)); }
	inline FsmFloat_t2134102846 * get_normalizedTime_19() const { return ___normalizedTime_19; }
	inline FsmFloat_t2134102846 ** get_address_of_normalizedTime_19() { return &___normalizedTime_19; }
	inline void set_normalizedTime_19(FsmFloat_t2134102846 * value)
	{
		___normalizedTime_19 = value;
		Il2CppCodeGenWriteBarrier(&___normalizedTime_19, value);
	}

	inline static int32_t get_offset_of_loopCount_20() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t3502000247, ___loopCount_20)); }
	inline FsmInt_t1596138449 * get_loopCount_20() const { return ___loopCount_20; }
	inline FsmInt_t1596138449 ** get_address_of_loopCount_20() { return &___loopCount_20; }
	inline void set_loopCount_20(FsmInt_t1596138449 * value)
	{
		___loopCount_20 = value;
		Il2CppCodeGenWriteBarrier(&___loopCount_20, value);
	}

	inline static int32_t get_offset_of_currentLoopProgress_21() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t3502000247, ___currentLoopProgress_21)); }
	inline FsmFloat_t2134102846 * get_currentLoopProgress_21() const { return ___currentLoopProgress_21; }
	inline FsmFloat_t2134102846 ** get_address_of_currentLoopProgress_21() { return &___currentLoopProgress_21; }
	inline void set_currentLoopProgress_21(FsmFloat_t2134102846 * value)
	{
		___currentLoopProgress_21 = value;
		Il2CppCodeGenWriteBarrier(&___currentLoopProgress_21, value);
	}

	inline static int32_t get_offset_of__animatorProxy_22() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t3502000247, ____animatorProxy_22)); }
	inline PlayMakerAnimatorMoveProxy_t4175490694 * get__animatorProxy_22() const { return ____animatorProxy_22; }
	inline PlayMakerAnimatorMoveProxy_t4175490694 ** get_address_of__animatorProxy_22() { return &____animatorProxy_22; }
	inline void set__animatorProxy_22(PlayMakerAnimatorMoveProxy_t4175490694 * value)
	{
		____animatorProxy_22 = value;
		Il2CppCodeGenWriteBarrier(&____animatorProxy_22, value);
	}

	inline static int32_t get_offset_of__animator_23() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t3502000247, ____animator_23)); }
	inline Animator_t2776330603 * get__animator_23() const { return ____animator_23; }
	inline Animator_t2776330603 ** get_address_of__animator_23() { return &____animator_23; }
	inline void set__animator_23(Animator_t2776330603 * value)
	{
		____animator_23 = value;
		Il2CppCodeGenWriteBarrier(&____animator_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
