﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t3134621005;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Object
struct Il2CppObject;
// GoogleUniversalAnalytics
struct GoogleUniversalAnalytics_t2200773556;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleUniversalAnalytics/<doWWWRequestAndCheckResult>c__Iterator34
struct  U3CdoWWWRequestAndCheckResultU3Ec__Iterator34_t3302283590  : public Il2CppObject
{
public:
	// System.String GoogleUniversalAnalytics/<doWWWRequestAndCheckResult>c__Iterator34::postDataString
	String_t* ___postDataString_0;
	// UnityEngine.WWW GoogleUniversalAnalytics/<doWWWRequestAndCheckResult>c__Iterator34::<www>__0
	WWW_t3134621005 * ___U3CwwwU3E__0_1;
	// System.Boolean GoogleUniversalAnalytics/<doWWWRequestAndCheckResult>c__Iterator34::<sendFailed_saveToOffline>__1
	bool ___U3CsendFailed_saveToOfflineU3E__1_2;
	// System.Byte[] GoogleUniversalAnalytics/<doWWWRequestAndCheckResult>c__Iterator34::<result>__2
	ByteU5BU5D_t4260760469* ___U3CresultU3E__2_3;
	// System.Int32 GoogleUniversalAnalytics/<doWWWRequestAndCheckResult>c__Iterator34::$PC
	int32_t ___U24PC_4;
	// System.Object GoogleUniversalAnalytics/<doWWWRequestAndCheckResult>c__Iterator34::$current
	Il2CppObject * ___U24current_5;
	// System.String GoogleUniversalAnalytics/<doWWWRequestAndCheckResult>c__Iterator34::<$>postDataString
	String_t* ___U3CU24U3EpostDataString_6;
	// GoogleUniversalAnalytics GoogleUniversalAnalytics/<doWWWRequestAndCheckResult>c__Iterator34::<>f__this
	GoogleUniversalAnalytics_t2200773556 * ___U3CU3Ef__this_7;

public:
	inline static int32_t get_offset_of_postDataString_0() { return static_cast<int32_t>(offsetof(U3CdoWWWRequestAndCheckResultU3Ec__Iterator34_t3302283590, ___postDataString_0)); }
	inline String_t* get_postDataString_0() const { return ___postDataString_0; }
	inline String_t** get_address_of_postDataString_0() { return &___postDataString_0; }
	inline void set_postDataString_0(String_t* value)
	{
		___postDataString_0 = value;
		Il2CppCodeGenWriteBarrier(&___postDataString_0, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CdoWWWRequestAndCheckResultU3Ec__Iterator34_t3302283590, ___U3CwwwU3E__0_1)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CsendFailed_saveToOfflineU3E__1_2() { return static_cast<int32_t>(offsetof(U3CdoWWWRequestAndCheckResultU3Ec__Iterator34_t3302283590, ___U3CsendFailed_saveToOfflineU3E__1_2)); }
	inline bool get_U3CsendFailed_saveToOfflineU3E__1_2() const { return ___U3CsendFailed_saveToOfflineU3E__1_2; }
	inline bool* get_address_of_U3CsendFailed_saveToOfflineU3E__1_2() { return &___U3CsendFailed_saveToOfflineU3E__1_2; }
	inline void set_U3CsendFailed_saveToOfflineU3E__1_2(bool value)
	{
		___U3CsendFailed_saveToOfflineU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U3CresultU3E__2_3() { return static_cast<int32_t>(offsetof(U3CdoWWWRequestAndCheckResultU3Ec__Iterator34_t3302283590, ___U3CresultU3E__2_3)); }
	inline ByteU5BU5D_t4260760469* get_U3CresultU3E__2_3() const { return ___U3CresultU3E__2_3; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CresultU3E__2_3() { return &___U3CresultU3E__2_3; }
	inline void set_U3CresultU3E__2_3(ByteU5BU5D_t4260760469* value)
	{
		___U3CresultU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CresultU3E__2_3, value);
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CdoWWWRequestAndCheckResultU3Ec__Iterator34_t3302283590, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CdoWWWRequestAndCheckResultU3Ec__Iterator34_t3302283590, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EpostDataString_6() { return static_cast<int32_t>(offsetof(U3CdoWWWRequestAndCheckResultU3Ec__Iterator34_t3302283590, ___U3CU24U3EpostDataString_6)); }
	inline String_t* get_U3CU24U3EpostDataString_6() const { return ___U3CU24U3EpostDataString_6; }
	inline String_t** get_address_of_U3CU24U3EpostDataString_6() { return &___U3CU24U3EpostDataString_6; }
	inline void set_U3CU24U3EpostDataString_6(String_t* value)
	{
		___U3CU24U3EpostDataString_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EpostDataString_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_7() { return static_cast<int32_t>(offsetof(U3CdoWWWRequestAndCheckResultU3Ec__Iterator34_t3302283590, ___U3CU3Ef__this_7)); }
	inline GoogleUniversalAnalytics_t2200773556 * get_U3CU3Ef__this_7() const { return ___U3CU3Ef__this_7; }
	inline GoogleUniversalAnalytics_t2200773556 ** get_address_of_U3CU3Ef__this_7() { return &___U3CU3Ef__this_7; }
	inline void set_U3CU3Ef__this_7(GoogleUniversalAnalytics_t2200773556 * value)
	{
		___U3CU3Ef__this_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
