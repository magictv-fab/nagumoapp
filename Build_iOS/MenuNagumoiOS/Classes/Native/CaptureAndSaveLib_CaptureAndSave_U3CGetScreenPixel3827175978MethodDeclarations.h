﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CaptureAndSave/<GetScreenPixels>c__Iterator1
struct U3CGetScreenPixelsU3Ec__Iterator1_t3827175978;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void CaptureAndSave/<GetScreenPixels>c__Iterator1::.ctor()
extern "C"  void U3CGetScreenPixelsU3Ec__Iterator1__ctor_m3292122010 (U3CGetScreenPixelsU3Ec__Iterator1_t3827175978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CaptureAndSave/<GetScreenPixels>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetScreenPixelsU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1637417282 (U3CGetScreenPixelsU3Ec__Iterator1_t3827175978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CaptureAndSave/<GetScreenPixels>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetScreenPixelsU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m153733334 (U3CGetScreenPixelsU3Ec__Iterator1_t3827175978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CaptureAndSave/<GetScreenPixels>c__Iterator1::MoveNext()
extern "C"  bool U3CGetScreenPixelsU3Ec__Iterator1_MoveNext_m676718516 (U3CGetScreenPixelsU3Ec__Iterator1_t3827175978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSave/<GetScreenPixels>c__Iterator1::Dispose()
extern "C"  void U3CGetScreenPixelsU3Ec__Iterator1_Dispose_m2531604823 (U3CGetScreenPixelsU3Ec__Iterator1_t3827175978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSave/<GetScreenPixels>c__Iterator1::Reset()
extern "C"  void U3CGetScreenPixelsU3Ec__Iterator1_Reset_m938554951 (U3CGetScreenPixelsU3Ec__Iterator1_t3827175978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
