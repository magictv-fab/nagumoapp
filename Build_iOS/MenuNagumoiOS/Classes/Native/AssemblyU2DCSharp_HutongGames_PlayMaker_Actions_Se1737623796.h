﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetGravity
struct  SetGravity_t1737623796  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SetGravity::vector
	FsmVector3_t533912882 * ___vector_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetGravity::x
	FsmFloat_t2134102846 * ___x_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetGravity::y
	FsmFloat_t2134102846 * ___y_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetGravity::z
	FsmFloat_t2134102846 * ___z_12;
	// System.Boolean HutongGames.PlayMaker.Actions.SetGravity::everyFrame
	bool ___everyFrame_13;

public:
	inline static int32_t get_offset_of_vector_9() { return static_cast<int32_t>(offsetof(SetGravity_t1737623796, ___vector_9)); }
	inline FsmVector3_t533912882 * get_vector_9() const { return ___vector_9; }
	inline FsmVector3_t533912882 ** get_address_of_vector_9() { return &___vector_9; }
	inline void set_vector_9(FsmVector3_t533912882 * value)
	{
		___vector_9 = value;
		Il2CppCodeGenWriteBarrier(&___vector_9, value);
	}

	inline static int32_t get_offset_of_x_10() { return static_cast<int32_t>(offsetof(SetGravity_t1737623796, ___x_10)); }
	inline FsmFloat_t2134102846 * get_x_10() const { return ___x_10; }
	inline FsmFloat_t2134102846 ** get_address_of_x_10() { return &___x_10; }
	inline void set_x_10(FsmFloat_t2134102846 * value)
	{
		___x_10 = value;
		Il2CppCodeGenWriteBarrier(&___x_10, value);
	}

	inline static int32_t get_offset_of_y_11() { return static_cast<int32_t>(offsetof(SetGravity_t1737623796, ___y_11)); }
	inline FsmFloat_t2134102846 * get_y_11() const { return ___y_11; }
	inline FsmFloat_t2134102846 ** get_address_of_y_11() { return &___y_11; }
	inline void set_y_11(FsmFloat_t2134102846 * value)
	{
		___y_11 = value;
		Il2CppCodeGenWriteBarrier(&___y_11, value);
	}

	inline static int32_t get_offset_of_z_12() { return static_cast<int32_t>(offsetof(SetGravity_t1737623796, ___z_12)); }
	inline FsmFloat_t2134102846 * get_z_12() const { return ___z_12; }
	inline FsmFloat_t2134102846 ** get_address_of_z_12() { return &___z_12; }
	inline void set_z_12(FsmFloat_t2134102846 * value)
	{
		___z_12 = value;
		Il2CppCodeGenWriteBarrier(&___z_12, value);
	}

	inline static int32_t get_offset_of_everyFrame_13() { return static_cast<int32_t>(offsetof(SetGravity_t1737623796, ___everyFrame_13)); }
	inline bool get_everyFrame_13() const { return ___everyFrame_13; }
	inline bool* get_address_of_everyFrame_13() { return &___everyFrame_13; }
	inline void set_everyFrame_13(bool value)
	{
		___everyFrame_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
