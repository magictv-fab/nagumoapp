﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkSetIsMessageQueueRunning
struct  NetworkSetIsMessageQueueRunning_t2388838309  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.NetworkSetIsMessageQueueRunning::isMessageQueueRunning
	FsmBool_t1075959796 * ___isMessageQueueRunning_9;

public:
	inline static int32_t get_offset_of_isMessageQueueRunning_9() { return static_cast<int32_t>(offsetof(NetworkSetIsMessageQueueRunning_t2388838309, ___isMessageQueueRunning_9)); }
	inline FsmBool_t1075959796 * get_isMessageQueueRunning_9() const { return ___isMessageQueueRunning_9; }
	inline FsmBool_t1075959796 ** get_address_of_isMessageQueueRunning_9() { return &___isMessageQueueRunning_9; }
	inline void set_isMessageQueueRunning_9(FsmBool_t1075959796 * value)
	{
		___isMessageQueueRunning_9 = value;
		Il2CppCodeGenWriteBarrier(&___isMessageQueueRunning_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
