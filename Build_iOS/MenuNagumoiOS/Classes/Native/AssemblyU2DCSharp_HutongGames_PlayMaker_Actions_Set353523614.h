﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmColor
struct FsmColor_t2131419205;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetColorValue
struct  SetColorValue_t353523614  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.SetColorValue::colorVariable
	FsmColor_t2131419205 * ___colorVariable_9;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.SetColorValue::color
	FsmColor_t2131419205 * ___color_10;
	// System.Boolean HutongGames.PlayMaker.Actions.SetColorValue::everyFrame
	bool ___everyFrame_11;

public:
	inline static int32_t get_offset_of_colorVariable_9() { return static_cast<int32_t>(offsetof(SetColorValue_t353523614, ___colorVariable_9)); }
	inline FsmColor_t2131419205 * get_colorVariable_9() const { return ___colorVariable_9; }
	inline FsmColor_t2131419205 ** get_address_of_colorVariable_9() { return &___colorVariable_9; }
	inline void set_colorVariable_9(FsmColor_t2131419205 * value)
	{
		___colorVariable_9 = value;
		Il2CppCodeGenWriteBarrier(&___colorVariable_9, value);
	}

	inline static int32_t get_offset_of_color_10() { return static_cast<int32_t>(offsetof(SetColorValue_t353523614, ___color_10)); }
	inline FsmColor_t2131419205 * get_color_10() const { return ___color_10; }
	inline FsmColor_t2131419205 ** get_address_of_color_10() { return &___color_10; }
	inline void set_color_10(FsmColor_t2131419205 * value)
	{
		___color_10 = value;
		Il2CppCodeGenWriteBarrier(&___color_10, value);
	}

	inline static int32_t get_offset_of_everyFrame_11() { return static_cast<int32_t>(offsetof(SetColorValue_t353523614, ___everyFrame_11)); }
	inline bool get_everyFrame_11() const { return ___everyFrame_11; }
	inline bool* get_address_of_everyFrame_11() { return &___everyFrame_11; }
	inline void set_everyFrame_11(bool value)
	{
		___everyFrame_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
