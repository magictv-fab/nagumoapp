﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CrossPlatformPushNotificationController
struct  CrossPlatformPushNotificationController_t3726717364  : public MonoBehaviour_t667441552
{
public:
	// System.String CrossPlatformPushNotificationController::_IOSOneSignalId
	String_t* ____IOSOneSignalId_2;
	// System.String CrossPlatformPushNotificationController::_androidOneSignalId
	String_t* ____androidOneSignalId_3;
	// System.String CrossPlatformPushNotificationController::_gmsAppId
	String_t* ____gmsAppId_4;

public:
	inline static int32_t get_offset_of__IOSOneSignalId_2() { return static_cast<int32_t>(offsetof(CrossPlatformPushNotificationController_t3726717364, ____IOSOneSignalId_2)); }
	inline String_t* get__IOSOneSignalId_2() const { return ____IOSOneSignalId_2; }
	inline String_t** get_address_of__IOSOneSignalId_2() { return &____IOSOneSignalId_2; }
	inline void set__IOSOneSignalId_2(String_t* value)
	{
		____IOSOneSignalId_2 = value;
		Il2CppCodeGenWriteBarrier(&____IOSOneSignalId_2, value);
	}

	inline static int32_t get_offset_of__androidOneSignalId_3() { return static_cast<int32_t>(offsetof(CrossPlatformPushNotificationController_t3726717364, ____androidOneSignalId_3)); }
	inline String_t* get__androidOneSignalId_3() const { return ____androidOneSignalId_3; }
	inline String_t** get_address_of__androidOneSignalId_3() { return &____androidOneSignalId_3; }
	inline void set__androidOneSignalId_3(String_t* value)
	{
		____androidOneSignalId_3 = value;
		Il2CppCodeGenWriteBarrier(&____androidOneSignalId_3, value);
	}

	inline static int32_t get_offset_of__gmsAppId_4() { return static_cast<int32_t>(offsetof(CrossPlatformPushNotificationController_t3726717364, ____gmsAppId_4)); }
	inline String_t* get__gmsAppId_4() const { return ____gmsAppId_4; }
	inline String_t** get_address_of__gmsAppId_4() { return &____gmsAppId_4; }
	inline void set__gmsAppId_4(String_t* value)
	{
		____gmsAppId_4 = value;
		Il2CppCodeGenWriteBarrier(&____gmsAppId_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
