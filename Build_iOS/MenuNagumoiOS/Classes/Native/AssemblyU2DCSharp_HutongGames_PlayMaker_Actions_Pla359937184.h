﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString[]
struct FsmStringU5BU5D_t2523845914;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.PlayerPrefsGetString
struct  PlayerPrefsGetString_t359937184  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.Actions.PlayerPrefsGetString::keys
	FsmStringU5BU5D_t2523845914* ___keys_9;
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.Actions.PlayerPrefsGetString::variables
	FsmStringU5BU5D_t2523845914* ___variables_10;

public:
	inline static int32_t get_offset_of_keys_9() { return static_cast<int32_t>(offsetof(PlayerPrefsGetString_t359937184, ___keys_9)); }
	inline FsmStringU5BU5D_t2523845914* get_keys_9() const { return ___keys_9; }
	inline FsmStringU5BU5D_t2523845914** get_address_of_keys_9() { return &___keys_9; }
	inline void set_keys_9(FsmStringU5BU5D_t2523845914* value)
	{
		___keys_9 = value;
		Il2CppCodeGenWriteBarrier(&___keys_9, value);
	}

	inline static int32_t get_offset_of_variables_10() { return static_cast<int32_t>(offsetof(PlayerPrefsGetString_t359937184, ___variables_10)); }
	inline FsmStringU5BU5D_t2523845914* get_variables_10() const { return ___variables_10; }
	inline FsmStringU5BU5D_t2523845914** get_address_of_variables_10() { return &___variables_10; }
	inline void set_variables_10(FsmStringU5BU5D_t2523845914* value)
	{
		___variables_10 = value;
		Il2CppCodeGenWriteBarrier(&___variables_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
