﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2662109048;
// System.Collections.Generic.List`1<OnOffListenerEventInterface>
struct List_1_t1033361267;
// System.Collections.Generic.Dictionary`2<System.String,OnOffListenerEventInterface>
struct Dictionary_2_t485594085;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;

#include "AssemblyU2DCSharp_ConfigurableListenerEventAbstract672359087.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InApp.SimpleEvents.GenericOnOff
struct  GenericOnOff_t3142219673  : public ConfigurableListenerEventAbstract_t672359087
{
public:
	// UnityEngine.GameObject[] InApp.SimpleEvents.GenericOnOff::gameObjectsToFind
	GameObjectU5BU5D_t2662109048* ___gameObjectsToFind_2;
	// System.Collections.Generic.List`1<OnOffListenerEventInterface> InApp.SimpleEvents.GenericOnOff::configurables
	List_1_t1033361267 * ___configurables_3;
	// System.Collections.Generic.Dictionary`2<System.String,OnOffListenerEventInterface> InApp.SimpleEvents.GenericOnOff::_configurableComponents
	Dictionary_2_t485594085 * ____configurableComponents_4;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> InApp.SimpleEvents.GenericOnOff::DebugConfigurableComponents
	List_1_t747900261 * ___DebugConfigurableComponents_5;

public:
	inline static int32_t get_offset_of_gameObjectsToFind_2() { return static_cast<int32_t>(offsetof(GenericOnOff_t3142219673, ___gameObjectsToFind_2)); }
	inline GameObjectU5BU5D_t2662109048* get_gameObjectsToFind_2() const { return ___gameObjectsToFind_2; }
	inline GameObjectU5BU5D_t2662109048** get_address_of_gameObjectsToFind_2() { return &___gameObjectsToFind_2; }
	inline void set_gameObjectsToFind_2(GameObjectU5BU5D_t2662109048* value)
	{
		___gameObjectsToFind_2 = value;
		Il2CppCodeGenWriteBarrier(&___gameObjectsToFind_2, value);
	}

	inline static int32_t get_offset_of_configurables_3() { return static_cast<int32_t>(offsetof(GenericOnOff_t3142219673, ___configurables_3)); }
	inline List_1_t1033361267 * get_configurables_3() const { return ___configurables_3; }
	inline List_1_t1033361267 ** get_address_of_configurables_3() { return &___configurables_3; }
	inline void set_configurables_3(List_1_t1033361267 * value)
	{
		___configurables_3 = value;
		Il2CppCodeGenWriteBarrier(&___configurables_3, value);
	}

	inline static int32_t get_offset_of__configurableComponents_4() { return static_cast<int32_t>(offsetof(GenericOnOff_t3142219673, ____configurableComponents_4)); }
	inline Dictionary_2_t485594085 * get__configurableComponents_4() const { return ____configurableComponents_4; }
	inline Dictionary_2_t485594085 ** get_address_of__configurableComponents_4() { return &____configurableComponents_4; }
	inline void set__configurableComponents_4(Dictionary_2_t485594085 * value)
	{
		____configurableComponents_4 = value;
		Il2CppCodeGenWriteBarrier(&____configurableComponents_4, value);
	}

	inline static int32_t get_offset_of_DebugConfigurableComponents_5() { return static_cast<int32_t>(offsetof(GenericOnOff_t3142219673, ___DebugConfigurableComponents_5)); }
	inline List_1_t747900261 * get_DebugConfigurableComponents_5() const { return ___DebugConfigurableComponents_5; }
	inline List_1_t747900261 ** get_address_of_DebugConfigurableComponents_5() { return &___DebugConfigurableComponents_5; }
	inline void set_DebugConfigurableComponents_5(List_1_t747900261 * value)
	{
		___DebugConfigurableComponents_5 = value;
		Il2CppCodeGenWriteBarrier(&___DebugConfigurableComponents_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
