﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Char[]
struct CharU5BU5D_t3324145743;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.DecodedBitStreamParser
struct  DecodedBitStreamParser_t640865202  : public Il2CppObject
{
public:

public:
};

struct DecodedBitStreamParser_t640865202_StaticFields
{
public:
	// System.Char[] ZXing.QrCode.Internal.DecodedBitStreamParser::ALPHANUMERIC_CHARS
	CharU5BU5D_t3324145743* ___ALPHANUMERIC_CHARS_0;

public:
	inline static int32_t get_offset_of_ALPHANUMERIC_CHARS_0() { return static_cast<int32_t>(offsetof(DecodedBitStreamParser_t640865202_StaticFields, ___ALPHANUMERIC_CHARS_0)); }
	inline CharU5BU5D_t3324145743* get_ALPHANUMERIC_CHARS_0() const { return ___ALPHANUMERIC_CHARS_0; }
	inline CharU5BU5D_t3324145743** get_address_of_ALPHANUMERIC_CHARS_0() { return &___ALPHANUMERIC_CHARS_0; }
	inline void set_ALPHANUMERIC_CHARS_0(CharU5BU5D_t3324145743* value)
	{
		___ALPHANUMERIC_CHARS_0 = value;
		Il2CppCodeGenWriteBarrier(&___ALPHANUMERIC_CHARS_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
