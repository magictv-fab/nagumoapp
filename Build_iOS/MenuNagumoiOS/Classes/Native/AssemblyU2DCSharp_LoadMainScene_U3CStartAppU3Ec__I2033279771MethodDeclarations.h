﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoadMainScene/<StartApp>c__Iterator3B
struct U3CStartAppU3Ec__Iterator3B_t2033279771;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void LoadMainScene/<StartApp>c__Iterator3B::.ctor()
extern "C"  void U3CStartAppU3Ec__Iterator3B__ctor_m2934248624 (U3CStartAppU3Ec__Iterator3B_t2033279771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LoadMainScene/<StartApp>c__Iterator3B::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartAppU3Ec__Iterator3B_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3589206114 (U3CStartAppU3Ec__Iterator3B_t2033279771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LoadMainScene/<StartApp>c__Iterator3B::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartAppU3Ec__Iterator3B_System_Collections_IEnumerator_get_Current_m3900831734 (U3CStartAppU3Ec__Iterator3B_t2033279771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LoadMainScene/<StartApp>c__Iterator3B::MoveNext()
extern "C"  bool U3CStartAppU3Ec__Iterator3B_MoveNext_m360432196 (U3CStartAppU3Ec__Iterator3B_t2033279771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadMainScene/<StartApp>c__Iterator3B::Dispose()
extern "C"  void U3CStartAppU3Ec__Iterator3B_Dispose_m2212664557 (U3CStartAppU3Ec__Iterator3B_t2033279771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadMainScene/<StartApp>c__Iterator3B::Reset()
extern "C"  void U3CStartAppU3Ec__Iterator3B_Reset_m580681565 (U3CStartAppU3Ec__Iterator3B_t2033279771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
