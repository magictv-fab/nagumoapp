﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Encryption.ZipAESStream
struct ZipAESStream_t513042148;
// System.IO.Stream
struct Stream_t1561764144;
// ICSharpCode.SharpZipLib.Encryption.ZipAESTransform
struct ZipAESTransform_t496812608;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Encryptio496812608.h"
#include "mscorlib_System_Security_Cryptography_CryptoStream3444626768.h"

// System.Void ICSharpCode.SharpZipLib.Encryption.ZipAESStream::.ctor(System.IO.Stream,ICSharpCode.SharpZipLib.Encryption.ZipAESTransform,System.Security.Cryptography.CryptoStreamMode)
extern "C"  void ZipAESStream__ctor_m2448114909 (ZipAESStream_t513042148 * __this, Stream_t1561764144 * ___stream0, ZipAESTransform_t496812608 * ___transform1, int32_t ___mode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t ZipAESStream_Read_m2212300736 (ZipAESStream_t513042148 * __this, ByteU5BU5D_t4260760469* ___outBuffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Encryption.ZipAESStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C"  void ZipAESStream_Write_m3785378697 (ZipAESStream_t513042148 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
