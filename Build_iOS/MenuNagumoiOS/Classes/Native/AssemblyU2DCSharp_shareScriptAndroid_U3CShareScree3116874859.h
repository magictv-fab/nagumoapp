﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.AndroidJavaClass
struct AndroidJavaClass_t1816259147;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t2362096582;
// System.Object
struct Il2CppObject;
// shareScriptAndroid
struct shareScriptAndroid_t2567206277;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// shareScriptAndroid/<ShareScreenshot>c__Iterator27
struct  U3CShareScreenshotU3Ec__Iterator27_t3116874859  : public Il2CppObject
{
public:
	// System.String shareScriptAndroid/<ShareScreenshot>c__Iterator27::<destination>__0
	String_t* ___U3CdestinationU3E__0_0;
	// UnityEngine.AndroidJavaClass shareScriptAndroid/<ShareScreenshot>c__Iterator27::<intentClass>__1
	AndroidJavaClass_t1816259147 * ___U3CintentClassU3E__1_1;
	// UnityEngine.AndroidJavaObject shareScriptAndroid/<ShareScreenshot>c__Iterator27::<intentObject>__2
	AndroidJavaObject_t2362096582 * ___U3CintentObjectU3E__2_2;
	// UnityEngine.AndroidJavaClass shareScriptAndroid/<ShareScreenshot>c__Iterator27::<uriClass>__3
	AndroidJavaClass_t1816259147 * ___U3CuriClassU3E__3_3;
	// UnityEngine.AndroidJavaObject shareScriptAndroid/<ShareScreenshot>c__Iterator27::<uriObject>__4
	AndroidJavaObject_t2362096582 * ___U3CuriObjectU3E__4_4;
	// UnityEngine.AndroidJavaClass shareScriptAndroid/<ShareScreenshot>c__Iterator27::<unity>__5
	AndroidJavaClass_t1816259147 * ___U3CunityU3E__5_5;
	// UnityEngine.AndroidJavaObject shareScriptAndroid/<ShareScreenshot>c__Iterator27::<currentActivity>__6
	AndroidJavaObject_t2362096582 * ___U3CcurrentActivityU3E__6_6;
	// UnityEngine.AndroidJavaObject shareScriptAndroid/<ShareScreenshot>c__Iterator27::<chooser>__7
	AndroidJavaObject_t2362096582 * ___U3CchooserU3E__7_7;
	// System.Int32 shareScriptAndroid/<ShareScreenshot>c__Iterator27::$PC
	int32_t ___U24PC_8;
	// System.Object shareScriptAndroid/<ShareScreenshot>c__Iterator27::$current
	Il2CppObject * ___U24current_9;
	// shareScriptAndroid shareScriptAndroid/<ShareScreenshot>c__Iterator27::<>f__this
	shareScriptAndroid_t2567206277 * ___U3CU3Ef__this_10;

public:
	inline static int32_t get_offset_of_U3CdestinationU3E__0_0() { return static_cast<int32_t>(offsetof(U3CShareScreenshotU3Ec__Iterator27_t3116874859, ___U3CdestinationU3E__0_0)); }
	inline String_t* get_U3CdestinationU3E__0_0() const { return ___U3CdestinationU3E__0_0; }
	inline String_t** get_address_of_U3CdestinationU3E__0_0() { return &___U3CdestinationU3E__0_0; }
	inline void set_U3CdestinationU3E__0_0(String_t* value)
	{
		___U3CdestinationU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdestinationU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CintentClassU3E__1_1() { return static_cast<int32_t>(offsetof(U3CShareScreenshotU3Ec__Iterator27_t3116874859, ___U3CintentClassU3E__1_1)); }
	inline AndroidJavaClass_t1816259147 * get_U3CintentClassU3E__1_1() const { return ___U3CintentClassU3E__1_1; }
	inline AndroidJavaClass_t1816259147 ** get_address_of_U3CintentClassU3E__1_1() { return &___U3CintentClassU3E__1_1; }
	inline void set_U3CintentClassU3E__1_1(AndroidJavaClass_t1816259147 * value)
	{
		___U3CintentClassU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CintentClassU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CintentObjectU3E__2_2() { return static_cast<int32_t>(offsetof(U3CShareScreenshotU3Ec__Iterator27_t3116874859, ___U3CintentObjectU3E__2_2)); }
	inline AndroidJavaObject_t2362096582 * get_U3CintentObjectU3E__2_2() const { return ___U3CintentObjectU3E__2_2; }
	inline AndroidJavaObject_t2362096582 ** get_address_of_U3CintentObjectU3E__2_2() { return &___U3CintentObjectU3E__2_2; }
	inline void set_U3CintentObjectU3E__2_2(AndroidJavaObject_t2362096582 * value)
	{
		___U3CintentObjectU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CintentObjectU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CuriClassU3E__3_3() { return static_cast<int32_t>(offsetof(U3CShareScreenshotU3Ec__Iterator27_t3116874859, ___U3CuriClassU3E__3_3)); }
	inline AndroidJavaClass_t1816259147 * get_U3CuriClassU3E__3_3() const { return ___U3CuriClassU3E__3_3; }
	inline AndroidJavaClass_t1816259147 ** get_address_of_U3CuriClassU3E__3_3() { return &___U3CuriClassU3E__3_3; }
	inline void set_U3CuriClassU3E__3_3(AndroidJavaClass_t1816259147 * value)
	{
		___U3CuriClassU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CuriClassU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CuriObjectU3E__4_4() { return static_cast<int32_t>(offsetof(U3CShareScreenshotU3Ec__Iterator27_t3116874859, ___U3CuriObjectU3E__4_4)); }
	inline AndroidJavaObject_t2362096582 * get_U3CuriObjectU3E__4_4() const { return ___U3CuriObjectU3E__4_4; }
	inline AndroidJavaObject_t2362096582 ** get_address_of_U3CuriObjectU3E__4_4() { return &___U3CuriObjectU3E__4_4; }
	inline void set_U3CuriObjectU3E__4_4(AndroidJavaObject_t2362096582 * value)
	{
		___U3CuriObjectU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CuriObjectU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U3CunityU3E__5_5() { return static_cast<int32_t>(offsetof(U3CShareScreenshotU3Ec__Iterator27_t3116874859, ___U3CunityU3E__5_5)); }
	inline AndroidJavaClass_t1816259147 * get_U3CunityU3E__5_5() const { return ___U3CunityU3E__5_5; }
	inline AndroidJavaClass_t1816259147 ** get_address_of_U3CunityU3E__5_5() { return &___U3CunityU3E__5_5; }
	inline void set_U3CunityU3E__5_5(AndroidJavaClass_t1816259147 * value)
	{
		___U3CunityU3E__5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CunityU3E__5_5, value);
	}

	inline static int32_t get_offset_of_U3CcurrentActivityU3E__6_6() { return static_cast<int32_t>(offsetof(U3CShareScreenshotU3Ec__Iterator27_t3116874859, ___U3CcurrentActivityU3E__6_6)); }
	inline AndroidJavaObject_t2362096582 * get_U3CcurrentActivityU3E__6_6() const { return ___U3CcurrentActivityU3E__6_6; }
	inline AndroidJavaObject_t2362096582 ** get_address_of_U3CcurrentActivityU3E__6_6() { return &___U3CcurrentActivityU3E__6_6; }
	inline void set_U3CcurrentActivityU3E__6_6(AndroidJavaObject_t2362096582 * value)
	{
		___U3CcurrentActivityU3E__6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcurrentActivityU3E__6_6, value);
	}

	inline static int32_t get_offset_of_U3CchooserU3E__7_7() { return static_cast<int32_t>(offsetof(U3CShareScreenshotU3Ec__Iterator27_t3116874859, ___U3CchooserU3E__7_7)); }
	inline AndroidJavaObject_t2362096582 * get_U3CchooserU3E__7_7() const { return ___U3CchooserU3E__7_7; }
	inline AndroidJavaObject_t2362096582 ** get_address_of_U3CchooserU3E__7_7() { return &___U3CchooserU3E__7_7; }
	inline void set_U3CchooserU3E__7_7(AndroidJavaObject_t2362096582 * value)
	{
		___U3CchooserU3E__7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CchooserU3E__7_7, value);
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CShareScreenshotU3Ec__Iterator27_t3116874859, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CShareScreenshotU3Ec__Iterator27_t3116874859, ___U24current_9)); }
	inline Il2CppObject * get_U24current_9() const { return ___U24current_9; }
	inline Il2CppObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(Il2CppObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_10() { return static_cast<int32_t>(offsetof(U3CShareScreenshotU3Ec__Iterator27_t3116874859, ___U3CU3Ef__this_10)); }
	inline shareScriptAndroid_t2567206277 * get_U3CU3Ef__this_10() const { return ___U3CU3Ef__this_10; }
	inline shareScriptAndroid_t2567206277 ** get_address_of_U3CU3Ef__this_10() { return &___U3CU3Ef__this_10; }
	inline void set_U3CU3Ef__this_10(shareScriptAndroid_t2567206277 * value)
	{
		___U3CU3Ef__this_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
