﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// NotificationsManager
struct NotificationsManager_t3392303557;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotificationsScroll
struct  NotificationsScroll_t2084299189  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject NotificationsScroll::notificatonObj
	GameObject_t3674682005 * ___notificatonObj_2;
	// UnityEngine.GameObject NotificationsScroll::notificationContainer
	GameObject_t3674682005 * ___notificationContainer_3;
	// NotificationsManager NotificationsScroll::notificationsManager
	NotificationsManager_t3392303557 * ___notificationsManager_4;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> NotificationsScroll::objsList
	List_1_t747900261 * ___objsList_5;
	// System.Collections.Generic.List`1<System.String> NotificationsScroll::idNotificationsLst
	List_1_t1375417109 * ___idNotificationsLst_6;

public:
	inline static int32_t get_offset_of_notificatonObj_2() { return static_cast<int32_t>(offsetof(NotificationsScroll_t2084299189, ___notificatonObj_2)); }
	inline GameObject_t3674682005 * get_notificatonObj_2() const { return ___notificatonObj_2; }
	inline GameObject_t3674682005 ** get_address_of_notificatonObj_2() { return &___notificatonObj_2; }
	inline void set_notificatonObj_2(GameObject_t3674682005 * value)
	{
		___notificatonObj_2 = value;
		Il2CppCodeGenWriteBarrier(&___notificatonObj_2, value);
	}

	inline static int32_t get_offset_of_notificationContainer_3() { return static_cast<int32_t>(offsetof(NotificationsScroll_t2084299189, ___notificationContainer_3)); }
	inline GameObject_t3674682005 * get_notificationContainer_3() const { return ___notificationContainer_3; }
	inline GameObject_t3674682005 ** get_address_of_notificationContainer_3() { return &___notificationContainer_3; }
	inline void set_notificationContainer_3(GameObject_t3674682005 * value)
	{
		___notificationContainer_3 = value;
		Il2CppCodeGenWriteBarrier(&___notificationContainer_3, value);
	}

	inline static int32_t get_offset_of_notificationsManager_4() { return static_cast<int32_t>(offsetof(NotificationsScroll_t2084299189, ___notificationsManager_4)); }
	inline NotificationsManager_t3392303557 * get_notificationsManager_4() const { return ___notificationsManager_4; }
	inline NotificationsManager_t3392303557 ** get_address_of_notificationsManager_4() { return &___notificationsManager_4; }
	inline void set_notificationsManager_4(NotificationsManager_t3392303557 * value)
	{
		___notificationsManager_4 = value;
		Il2CppCodeGenWriteBarrier(&___notificationsManager_4, value);
	}

	inline static int32_t get_offset_of_objsList_5() { return static_cast<int32_t>(offsetof(NotificationsScroll_t2084299189, ___objsList_5)); }
	inline List_1_t747900261 * get_objsList_5() const { return ___objsList_5; }
	inline List_1_t747900261 ** get_address_of_objsList_5() { return &___objsList_5; }
	inline void set_objsList_5(List_1_t747900261 * value)
	{
		___objsList_5 = value;
		Il2CppCodeGenWriteBarrier(&___objsList_5, value);
	}

	inline static int32_t get_offset_of_idNotificationsLst_6() { return static_cast<int32_t>(offsetof(NotificationsScroll_t2084299189, ___idNotificationsLst_6)); }
	inline List_1_t1375417109 * get_idNotificationsLst_6() const { return ___idNotificationsLst_6; }
	inline List_1_t1375417109 ** get_address_of_idNotificationsLst_6() { return &___idNotificationsLst_6; }
	inline void set_idNotificationsLst_6(List_1_t1375417109 * value)
	{
		___idNotificationsLst_6 = value;
		Il2CppCodeGenWriteBarrier(&___idNotificationsLst_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
