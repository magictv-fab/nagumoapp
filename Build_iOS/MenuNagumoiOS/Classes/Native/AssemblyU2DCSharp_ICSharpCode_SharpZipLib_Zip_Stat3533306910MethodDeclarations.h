﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.StaticDiskDataSource
struct StaticDiskDataSource_t3533306910;
// System.String
struct String_t;
// System.IO.Stream
struct Stream_t1561764144;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ICSharpCode.SharpZipLib.Zip.StaticDiskDataSource::.ctor(System.String)
extern "C"  void StaticDiskDataSource__ctor_m4183625429 (StaticDiskDataSource_t3533306910 * __this, String_t* ___fileName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream ICSharpCode.SharpZipLib.Zip.StaticDiskDataSource::GetSource()
extern "C"  Stream_t1561764144 * StaticDiskDataSource_GetSource_m3880776808 (StaticDiskDataSource_t3533306910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
