﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ButtonPromptExample
struct ButtonPromptExample_t804079924;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ButtonPromptExample::.ctor()
extern "C"  void ButtonPromptExample__ctor_m2633884855 (ButtonPromptExample_t804079924 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonPromptExample::Click()
extern "C"  void ButtonPromptExample_Click_m43765085 (ButtonPromptExample_t804079924 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonPromptExample::OkClick(System.String)
extern "C"  void ButtonPromptExample_OkClick_m840872385 (ButtonPromptExample_t804079924 * __this, String_t* ___textMessage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonPromptExample::CancelClick()
extern "C"  void ButtonPromptExample_CancelClick_m3340757635 (ButtonPromptExample_t804079924 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
