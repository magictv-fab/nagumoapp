﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.components.GroupStartableComponentsManager
struct GroupStartableComponentsManager_t3626371340;
// ARM.animation.GenericStartableComponentControllAbstract[]
struct GenericStartableComponentControllAbstractU5BU5D_t3957787650;

#include "codegen/il2cpp-codegen.h"

// System.Void ARM.components.GroupStartableComponentsManager::.ctor()
extern "C"  void GroupStartableComponentsManager__ctor_m2786227943 (GroupStartableComponentsManager_t3626371340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.components.GroupStartableComponentsManager::SetComponents(ARM.animation.GenericStartableComponentControllAbstract[])
extern "C"  void GroupStartableComponentsManager_SetComponents_m3253610566 (GroupStartableComponentsManager_t3626371340 * __this, GenericStartableComponentControllAbstractU5BU5D_t3957787650* ___components0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.components.GroupStartableComponentsManager::checkAllIsReady()
extern "C"  void GroupStartableComponentsManager_checkAllIsReady_m2482750469 (GroupStartableComponentsManager_t3626371340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.components.GroupStartableComponentsManager::prepare()
extern "C"  void GroupStartableComponentsManager_prepare_m2727408236 (GroupStartableComponentsManager_t3626371340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.components.GroupStartableComponentsManager::Play()
extern "C"  void GroupStartableComponentsManager_Play_m3841969073 (GroupStartableComponentsManager_t3626371340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.components.GroupStartableComponentsManager::Stop()
extern "C"  void GroupStartableComponentsManager_Stop_m3935653119 (GroupStartableComponentsManager_t3626371340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.components.GroupStartableComponentsManager::Pause()
extern "C"  void GroupStartableComponentsManager_Pause_m2840353915 (GroupStartableComponentsManager_t3626371340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
