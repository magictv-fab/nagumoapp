﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GameObjectCompare
struct  GameObjectCompare_t853174818  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GameObjectCompare::gameObjectVariable
	FsmOwnerDefault_t251897112 * ___gameObjectVariable_9;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GameObjectCompare::compareTo
	FsmGameObject_t1697147867 * ___compareTo_10;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GameObjectCompare::equalEvent
	FsmEvent_t2133468028 * ___equalEvent_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GameObjectCompare::notEqualEvent
	FsmEvent_t2133468028 * ___notEqualEvent_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GameObjectCompare::storeResult
	FsmBool_t1075959796 * ___storeResult_13;
	// System.Boolean HutongGames.PlayMaker.Actions.GameObjectCompare::everyFrame
	bool ___everyFrame_14;

public:
	inline static int32_t get_offset_of_gameObjectVariable_9() { return static_cast<int32_t>(offsetof(GameObjectCompare_t853174818, ___gameObjectVariable_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObjectVariable_9() const { return ___gameObjectVariable_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObjectVariable_9() { return &___gameObjectVariable_9; }
	inline void set_gameObjectVariable_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObjectVariable_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObjectVariable_9, value);
	}

	inline static int32_t get_offset_of_compareTo_10() { return static_cast<int32_t>(offsetof(GameObjectCompare_t853174818, ___compareTo_10)); }
	inline FsmGameObject_t1697147867 * get_compareTo_10() const { return ___compareTo_10; }
	inline FsmGameObject_t1697147867 ** get_address_of_compareTo_10() { return &___compareTo_10; }
	inline void set_compareTo_10(FsmGameObject_t1697147867 * value)
	{
		___compareTo_10 = value;
		Il2CppCodeGenWriteBarrier(&___compareTo_10, value);
	}

	inline static int32_t get_offset_of_equalEvent_11() { return static_cast<int32_t>(offsetof(GameObjectCompare_t853174818, ___equalEvent_11)); }
	inline FsmEvent_t2133468028 * get_equalEvent_11() const { return ___equalEvent_11; }
	inline FsmEvent_t2133468028 ** get_address_of_equalEvent_11() { return &___equalEvent_11; }
	inline void set_equalEvent_11(FsmEvent_t2133468028 * value)
	{
		___equalEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___equalEvent_11, value);
	}

	inline static int32_t get_offset_of_notEqualEvent_12() { return static_cast<int32_t>(offsetof(GameObjectCompare_t853174818, ___notEqualEvent_12)); }
	inline FsmEvent_t2133468028 * get_notEqualEvent_12() const { return ___notEqualEvent_12; }
	inline FsmEvent_t2133468028 ** get_address_of_notEqualEvent_12() { return &___notEqualEvent_12; }
	inline void set_notEqualEvent_12(FsmEvent_t2133468028 * value)
	{
		___notEqualEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___notEqualEvent_12, value);
	}

	inline static int32_t get_offset_of_storeResult_13() { return static_cast<int32_t>(offsetof(GameObjectCompare_t853174818, ___storeResult_13)); }
	inline FsmBool_t1075959796 * get_storeResult_13() const { return ___storeResult_13; }
	inline FsmBool_t1075959796 ** get_address_of_storeResult_13() { return &___storeResult_13; }
	inline void set_storeResult_13(FsmBool_t1075959796 * value)
	{
		___storeResult_13 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_13, value);
	}

	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(GameObjectCompare_t853174818, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
