﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Extend90301089.h"
#include "mscorlib_System_DateTime4283661327.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ExtendedUnixData
struct  ExtendedUnixData_t1879413977  : public Il2CppObject
{
public:
	// ICSharpCode.SharpZipLib.Zip.ExtendedUnixData/Flags ICSharpCode.SharpZipLib.Zip.ExtendedUnixData::_flags
	uint8_t ____flags_0;
	// System.DateTime ICSharpCode.SharpZipLib.Zip.ExtendedUnixData::_modificationTime
	DateTime_t4283661327  ____modificationTime_1;
	// System.DateTime ICSharpCode.SharpZipLib.Zip.ExtendedUnixData::_lastAccessTime
	DateTime_t4283661327  ____lastAccessTime_2;
	// System.DateTime ICSharpCode.SharpZipLib.Zip.ExtendedUnixData::_createTime
	DateTime_t4283661327  ____createTime_3;

public:
	inline static int32_t get_offset_of__flags_0() { return static_cast<int32_t>(offsetof(ExtendedUnixData_t1879413977, ____flags_0)); }
	inline uint8_t get__flags_0() const { return ____flags_0; }
	inline uint8_t* get_address_of__flags_0() { return &____flags_0; }
	inline void set__flags_0(uint8_t value)
	{
		____flags_0 = value;
	}

	inline static int32_t get_offset_of__modificationTime_1() { return static_cast<int32_t>(offsetof(ExtendedUnixData_t1879413977, ____modificationTime_1)); }
	inline DateTime_t4283661327  get__modificationTime_1() const { return ____modificationTime_1; }
	inline DateTime_t4283661327 * get_address_of__modificationTime_1() { return &____modificationTime_1; }
	inline void set__modificationTime_1(DateTime_t4283661327  value)
	{
		____modificationTime_1 = value;
	}

	inline static int32_t get_offset_of__lastAccessTime_2() { return static_cast<int32_t>(offsetof(ExtendedUnixData_t1879413977, ____lastAccessTime_2)); }
	inline DateTime_t4283661327  get__lastAccessTime_2() const { return ____lastAccessTime_2; }
	inline DateTime_t4283661327 * get_address_of__lastAccessTime_2() { return &____lastAccessTime_2; }
	inline void set__lastAccessTime_2(DateTime_t4283661327  value)
	{
		____lastAccessTime_2 = value;
	}

	inline static int32_t get_offset_of__createTime_3() { return static_cast<int32_t>(offsetof(ExtendedUnixData_t1879413977, ____createTime_3)); }
	inline DateTime_t4283661327  get__createTime_3() const { return ____createTime_3; }
	inline DateTime_t4283661327 * get_address_of__createTime_3() { return &____createTime_3; }
	inline void set__createTime_3(DateTime_t4283661327  value)
	{
		____createTime_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
