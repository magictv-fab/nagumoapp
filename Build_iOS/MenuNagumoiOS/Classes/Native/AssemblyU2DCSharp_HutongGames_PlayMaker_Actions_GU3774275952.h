﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2615417833.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutTextField
struct  GUILayoutTextField_t3774275952  : public GUILayoutAction_t2615417833
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutTextField::text
	FsmString_t952858651 * ___text_11;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GUILayoutTextField::maxLength
	FsmInt_t1596138449 * ___maxLength_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutTextField::style
	FsmString_t952858651 * ___style_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GUILayoutTextField::changedEvent
	FsmEvent_t2133468028 * ___changedEvent_14;

public:
	inline static int32_t get_offset_of_text_11() { return static_cast<int32_t>(offsetof(GUILayoutTextField_t3774275952, ___text_11)); }
	inline FsmString_t952858651 * get_text_11() const { return ___text_11; }
	inline FsmString_t952858651 ** get_address_of_text_11() { return &___text_11; }
	inline void set_text_11(FsmString_t952858651 * value)
	{
		___text_11 = value;
		Il2CppCodeGenWriteBarrier(&___text_11, value);
	}

	inline static int32_t get_offset_of_maxLength_12() { return static_cast<int32_t>(offsetof(GUILayoutTextField_t3774275952, ___maxLength_12)); }
	inline FsmInt_t1596138449 * get_maxLength_12() const { return ___maxLength_12; }
	inline FsmInt_t1596138449 ** get_address_of_maxLength_12() { return &___maxLength_12; }
	inline void set_maxLength_12(FsmInt_t1596138449 * value)
	{
		___maxLength_12 = value;
		Il2CppCodeGenWriteBarrier(&___maxLength_12, value);
	}

	inline static int32_t get_offset_of_style_13() { return static_cast<int32_t>(offsetof(GUILayoutTextField_t3774275952, ___style_13)); }
	inline FsmString_t952858651 * get_style_13() const { return ___style_13; }
	inline FsmString_t952858651 ** get_address_of_style_13() { return &___style_13; }
	inline void set_style_13(FsmString_t952858651 * value)
	{
		___style_13 = value;
		Il2CppCodeGenWriteBarrier(&___style_13, value);
	}

	inline static int32_t get_offset_of_changedEvent_14() { return static_cast<int32_t>(offsetof(GUILayoutTextField_t3774275952, ___changedEvent_14)); }
	inline FsmEvent_t2133468028 * get_changedEvent_14() const { return ___changedEvent_14; }
	inline FsmEvent_t2133468028 ** get_address_of_changedEvent_14() { return &___changedEvent_14; }
	inline void set_changedEvent_14(FsmEvent_t2133468028 * value)
	{
		___changedEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___changedEvent_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
