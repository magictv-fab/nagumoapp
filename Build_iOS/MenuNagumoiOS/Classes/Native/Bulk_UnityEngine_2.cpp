﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.String
struct String_t;
// UnityEngine.HostData[]
struct HostDataU5BU5D_t4021970803;
// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.Shader
struct Shader_t3191267369;
// UnityEngine.Texture
struct Texture_t2526458961;
// System.Single[]
struct SingleU5BU5D_t2316563989;
// System.Object
struct Il2CppObject;
// UnityEngine.Mesh
struct Mesh_t4241756145;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t701588350;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t1355284823;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t4024180168;
// System.Array
struct Il2CppArray;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1355284821;
// UnityEngine.Color32[]
struct Color32U5BU5D_t2960766953;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t1967039240;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// UnityEngine.MeshCollider
struct MeshCollider_t2667653125;
// UnityEngine.MeshFilter
struct MeshFilter_t3839065225;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t667441552;
// UnityEngine.Coroutine
struct Coroutine_t3621161934;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.Motion
struct Motion_t3026528250;
// UnityEngine.NavMeshAgent
struct NavMeshAgent_t588466745;
// UnityEngine.NetworkPlayer[]
struct NetworkPlayerU5BU5D_t132745896;
// UnityEngine.Object
struct Object_t3071478659;
// UnityEngine.NetworkView
struct NetworkView_t3656680617;
// UnityEngine.Object[]
struct ObjectU5BU5D_t1015136018;
// System.Type
struct Type_t;
// UnityEngine.ParticleSystem
struct ParticleSystem_t381473177;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.ParticleCollisionEvent[]
struct ParticleCollisionEventU5BU5D_t4168492807;
// UnityEngine.ParticleSystem/IteratorDelegate
struct IteratorDelegate_t4269758102;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t528650843;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t2697150633;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t889400257;
// UnityEngine.PlayerPrefsException
struct PlayerPrefsException_t3680716996;
// UnityEngine.PropertyAttribute
struct PropertyAttribute_t3531521085;
// UnityEngine.RangeAttribute
struct RangeAttribute_t912008995;
// UnityEngine.Collider
struct Collider_t2939674232;
// UnityEngine.Rigidbody
struct Rigidbody_t3346577219;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.Collider2D
struct Collider2D_t1552025098;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t1743771669;
// UnityEngine.RectOffset
struct RectOffset_t3056157787;
// UnityEngine.GUIStyle
struct GUIStyle_t2990928826;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t779639188;
// UnityEngine.RectTransform
struct RectTransform_t972643934;
// UnityEngine.Camera
struct Camera_t2727095145;
// UnityEngine.Canvas
struct Canvas_t2727140764;
// UnityEngine.Renderer
struct Renderer_t3076687687;
// UnityEngine.Material[]
struct MaterialU5BU5D_t170856778;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;
// UnityEngine.RequireComponent
struct RequireComponent_t1687166108;
// UnityEngine.ResourceRequest
struct ResourceRequest_t3731857623;
// UnityEngine.AsyncOperation
struct AsyncOperation_t3699081103;
// UnityEngine.RPC
struct RPC_t3134615963;
// UnityEngine.ScriptableObject
struct ScriptableObject_t2970544072;
// UnityEngine.Scripting.RequiredByNativeCodeAttribute
struct RequiredByNativeCodeAttribute_t3165457172;
// UnityEngine.Scripting.UsedByNativeCodeAttribute
struct UsedByNativeCodeAttribute_t3197444790;
// UnityEngine.SelectionBaseAttribute
struct SelectionBaseAttribute_t204085987;
// UnityEngine.GUILayer
struct GUILayer_t2983897946;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t2216353654;
// UnityEngine.SerializeField
struct SerializeField_t3754825534;
// UnityEngine.SerializePrivateVariables
struct SerializePrivateVariables_t2835496234;
// UnityEngine.SharedBetweenAnimatorsAttribute
struct SharedBetweenAnimatorsAttribute_t1486273289;
// UnityEngine.Event
struct Event_t4196595728;
// UnityEngine.SliderState
struct SliderState_t1233388262;
// UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform
struct GameCenterPlatform_t3570684786;
// UnityEngine.SocialPlatforms.ILocalUser
struct ILocalUser_t2654168339;
// System.Action`1<System.Boolean>
struct Action_1_t872614854;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// System.String[]
struct StringU5BU5D_t4054002952;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
struct GcAchievementDataU5BU5D_t1726768202;
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
struct GcScoreDataU5BU5D_t1670395707;
// System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>
struct Action_1_t229750097;
// System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>
struct Action_1_t2349069933;
// System.Action`1<UnityEngine.SocialPlatforms.IScore[]>
struct Action_1_t645920862;
// UnityEngine.SocialPlatforms.ILeaderboard
struct ILeaderboard_t3799088250;
// System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>
struct Action_1_t3814920354;
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct UserProfileU5BU5D_t2378268441;
// UnityEngine.SocialPlatforms.IAchievement
struct IAchievement_t2957812780;
// UnityEngine.SocialPlatforms.Impl.Achievement
struct Achievement_t344600729;
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
struct AchievementDescription_t2116066607;
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
struct GcLeaderboard_t1820874799;
// UnityEngine.SocialPlatforms.Impl.Leaderboard
struct Leaderboard_t1185876199;
// UnityEngine.SocialPlatforms.Impl.Score
struct Score_t3396031228;
// UnityEngine.SocialPlatforms.Impl.UserProfile
struct UserProfile_t2280656072;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "UnityEngine_UnityEngine_MasterServer2117519177.h"
#include "UnityEngine_UnityEngine_MasterServer2117519177MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Void2863195528.h"
#include "mscorlib_System_Int321153838500.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_HostData3270478838.h"
#include "mscorlib_System_Boolean476798718.h"
#include "UnityEngine_UnityEngine_MasterServerEvent2733244747.h"
#include "UnityEngine_UnityEngine_MasterServerEvent2733244747MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MatchTargetWeightMask258413904.h"
#include "UnityEngine_UnityEngine_MatchTargetWeightMask258413904MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_Single4291918972.h"
#include "UnityEngine_UnityEngine_Vector34282066566MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "UnityEngine_UnityEngine_Material3870600107MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3071478659MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Shader3191267369.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Shader3191267369MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "UnityEngine_UnityEngine_Color4194546905MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf4203372500.h"
#include "UnityEngine_UnityEngine_Mathf4203372500MethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_MathfInternal4096243933.h"
#include "UnityEngine_UnityEngineInternal_MathfInternal4096243933MethodDeclarations.h"
#include "mscorlib_System_Math2862914300MethodDeclarations.h"
#include "mscorlib_System_Double3868226565.h"
#include "mscorlib_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Time4241968337MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333MethodDeclarations.h"
#include "mscorlib_System_IndexOutOfRangeException3456360697MethodDeclarations.h"
#include "mscorlib_System_IndexOutOfRangeException3456360697.h"
#include "UnityEngine_UnityEngine_Vector44282066567MethodDeclarations.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_UnityString3369712284MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mesh4241756145.h"
#include "UnityEngine_UnityEngine_Mesh4241756145MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1355284822.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1355284823.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1355284821.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1355284821MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UnityEngine_Color32598853688.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1967039240.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2522024052.h"
#include "UnityEngine_UnityEngine_MeshCollider2667653125.h"
#include "UnityEngine_UnityEngine_MeshCollider2667653125MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshFilter3839065225.h"
#include "UnityEngine_UnityEngine_MeshFilter3839065225MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshRenderer2804666580.h"
#include "UnityEngine_UnityEngine_MeshRenderer2804666580MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour200106419MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Coroutine3621161934.h"
#include "UnityEngine_UnityEngine_Debug4195163081MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Motion3026528250.h"
#include "UnityEngine_UnityEngine_Motion3026528250MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NavMeshAgent588466745.h"
#include "UnityEngine_UnityEngine_NavMeshAgent588466745MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Network1492793700.h"
#include "UnityEngine_UnityEngine_Network1492793700MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkConnectionError1049203712.h"
#include "UnityEngine_UnityEngine_NetworkLogLevel2722760996.h"
#include "UnityEngine_UnityEngine_NetworkPlayer3231273765.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "UnityEngine_UnityEngine_NetworkViewID3400394436.h"
#include "mscorlib_System_UInt3224667981.h"
#include "UnityEngine_UnityEngine_NetworkViewID3400394436MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkPlayer3231273765MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkPeerType796297792.h"
#include "UnityEngine_UnityEngine_NetworkConnectionError1049203712MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkDisconnection468395618.h"
#include "UnityEngine_UnityEngine_NetworkDisconnection468395618MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkLogLevel2722760996MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkMessageInfo3807997963.h"
#include "UnityEngine_UnityEngine_NetworkMessageInfo3807997963MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkView3656680617.h"
#include "UnityEngine_UnityEngine_NetworkView3656680617MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkPeerType796297792MethodDeclarations.h"
#include "mscorlib_System_Int321153838500MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkReachability612403035.h"
#include "UnityEngine_UnityEngine_NetworkReachability612403035MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NPOTSupport1787002238.h"
#include "UnityEngine_UnityEngine_NPOTSupport1787002238MethodDeclarations.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "mscorlib_System_Type2863145774.h"
#include "UnityEngine_UnityEngine_HideFlags1436803931.h"
#include "mscorlib_System_IntPtr4010401971MethodDeclarations.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_ArgumentException928607144MethodDeclarations.h"
#include "mscorlib_System_ArgumentException928607144.h"
#include "UnityEngine_UnityEngine_ParticleCollisionEvent2700926194.h"
#include "UnityEngine_UnityEngine_ParticleCollisionEvent2700926194MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ParticlePhysicsExtensions3791554251.h"
#include "UnityEngine_UnityEngine_ParticlePhysicsExtensions3791554251MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ParticleSystem381473177.h"
#include "UnityEngine_UnityEngine_ParticleSystemExtensionsIm1014071565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_ParticleSystem381473177MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ParticleSystem_IteratorDel4269758102.h"
#include "UnityEngine_UnityEngine_ParticleSystem_IteratorDel4269758102MethodDeclarations.h"
#include "mscorlib_System_AsyncCallback1369114871.h"
#include "UnityEngine_UnityEngine_ParticleSystemExtensionsIm1014071565.h"
#include "UnityEngine_UnityEngine_PhysicMaterial211873335.h"
#include "UnityEngine_UnityEngine_PhysicMaterial211873335MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Physics3358180733.h"
#include "UnityEngine_UnityEngine_Physics3358180733MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteraction577895768.h"
#include "UnityEngine_UnityEngine_Ray3134616544.h"
#include "UnityEngine_UnityEngine_Ray3134616544MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"
#include "UnityEngine_UnityEngine_Physics2D9846735.h"
#include "UnityEngine_UnityEngine_Physics2D9846735MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3111957221MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3111957221.h"
#include "UnityEngine_UnityEngine_RaycastHit2D1374744384.h"
#include "UnityEngine_UnityEngine_Plane4206452690.h"
#include "UnityEngine_UnityEngine_Plane4206452690MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayerPrefs1845493509.h"
#include "UnityEngine_UnityEngine_PlayerPrefs1845493509MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayerPrefsException3680716996MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayerPrefsException3680716996.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "mscorlib_System_Exception3991598821MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayMode1155122555.h"
#include "UnityEngine_UnityEngine_PlayMode1155122555MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PrimitiveType1035833655.h"
#include "UnityEngine_UnityEngine_PrimitiveType1035833655MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PropertyAttribute3531521085.h"
#include "UnityEngine_UnityEngine_PropertyAttribute3531521085MethodDeclarations.h"
#include "mscorlib_System_Attribute2523058482MethodDeclarations.h"
#include "UnityEngine_UnityEngine_QualitySettings719345784.h"
#include "UnityEngine_UnityEngine_QualitySettings719345784MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ColorSpace161844263.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882MethodDeclarations.h"
#include "mscorlib_System_Single4291918972MethodDeclarations.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteraction577895768MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random3156561159.h"
#include "UnityEngine_UnityEngine_Random3156561159MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RangeAttribute912008995.h"
#include "UnityEngine_UnityEngine_RangeAttribute912008995MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody3346577219.h"
#include "UnityEngine_UnityEngine_Collider2939674232MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UnityEngine_Component3501516275MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector24282066565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit2D1374744384MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098.h"
#include "UnityEngine_UnityEngine_Rigidbody2D1743771669.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Rect4241904616MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectOffset3056157787.h"
#include "UnityEngine_UnityEngine_RectOffset3056157787MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyle2990928826.h"
#include "UnityEngine_UnityEngine_GUIStyle2990928826MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform972643934.h"
#include "UnityEngine_UnityEngine_RectTransform972643934MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDriven779639188.h"
#include "mscorlib_System_Delegate3310234105MethodDeclarations.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "UnityEngine_UnityEngine_Transform1659122786MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDriven779639188MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform_Edge1676794651.h"
#include "UnityEngine_UnityEngine_RectTransform_Axis1676694783.h"
#include "UnityEngine_UnityEngine_RectTransform_Axis1676694783MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform_Edge1676794651MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransformUtility3025555048.h"
#include "UnityEngine_UnityEngine_RectTransformUtility3025555048MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "UnityEngine_UnityEngine_Canvas2727140764.h"
#include "UnityEngine_UnityEngine_Camera2727095145MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer3076687687.h"
#include "UnityEngine_UnityEngine_Renderer3076687687MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_ColorWriteMask3214369688.h"
#include "UnityEngine_UnityEngine_Rendering_ColorWriteMask3214369688MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_CompareFunction2661816155.h"
#include "UnityEngine_UnityEngine_Rendering_CompareFunction2661816155MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_StencilOp3324967291.h"
#include "UnityEngine_UnityEngine_Rendering_StencilOp3324967291MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderMode77252893.h"
#include "UnityEngine_UnityEngine_RenderMode77252893MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderSettings425127197.h"
#include "UnityEngine_UnityEngine_RenderSettings425127197MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2526458961MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderTextureFormat2841883826.h"
#include "UnityEngine_UnityEngine_RenderTextureReadWrite2502600968.h"
#include "UnityEngine_UnityEngine_RenderTextureFormat2841883826MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderTextureReadWrite2502600968MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RequireComponent1687166108.h"
#include "UnityEngine_UnityEngine_RequireComponent1687166108MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resolution1578306928.h"
#include "UnityEngine_UnityEngine_Resolution1578306928MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ResourceRequest3731857623.h"
#include "UnityEngine_UnityEngine_ResourceRequest3731857623MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AsyncOperation3699081103MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources2918352667MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources2918352667.h"
#include "mscorlib_System_Type2863145774MethodDeclarations.h"
#include "mscorlib_System_RuntimeTypeHandle2669177232.h"
#include "UnityEngine_UnityEngine_AsyncOperation3699081103.h"
#include "UnityEngine_UnityEngine_Rigidbody3346577219MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ForceMode2134283300.h"
#include "UnityEngine_UnityEngine_Rigidbody2D1743771669MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RPC3134615963.h"
#include "UnityEngine_UnityEngine_RPC3134615963MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController274649809.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController274649809MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform3050318497.h"
#include "UnityEngine_UnityEngine_RuntimePlatform3050318497MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScaleMode3023293187.h"
#include "UnityEngine_UnityEngine_ScaleMode3023293187MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_LoadSceneM3067001883.h"
#include "UnityEngine_UnityEngine_SceneManagement_LoadSceneM3067001883MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1080795294.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1080795294MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManag2940962239.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManag2940962239MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen3187157168.h"
#include "UnityEngine_UnityEngine_Screen3187157168MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Cursor2745727898MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CursorLockMode1155278888.h"
#include "UnityEngine_UnityEngine_ScreenOrientation1849668026.h"
#include "UnityEngine_UnityEngine_ScreenOrientation1849668026MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScriptableObject2970544072.h"
#include "UnityEngine_UnityEngine_ScriptableObject2970544072MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Scripting_RequiredByNative3165457172.h"
#include "UnityEngine_UnityEngine_Scripting_RequiredByNative3165457172MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Scripting_UsedByNativeCode3197444790.h"
#include "UnityEngine_UnityEngine_Scripting_UsedByNativeCode3197444790MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SelectionBaseAttribute204085987.h"
#include "UnityEngine_UnityEngine_SelectionBaseAttribute204085987MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SendMessageOptions3856946179.h"
#include "UnityEngine_UnityEngine_SendMessageOptions3856946179MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SendMouseEvents3965481900.h"
#include "UnityEngine_UnityEngine_SendMouseEvents3965481900MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo3209134097.h"
#include "UnityEngine_UnityEngine_Input4200062272MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayer2983897946MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayer2983897946.h"
#include "UnityEngine_UnityEngine_GUIElement3775428101.h"
#include "UnityEngine_UnityEngine_Component3501516275.h"
#include "UnityEngine_UnityEngine_CameraClearFlags2093155523.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo3209134097MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySeri2216353654.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySeri2216353654MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SerializeField3754825534.h"
#include "UnityEngine_UnityEngine_SerializeField3754825534MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SerializePrivateVariables2835496234.h"
#include "UnityEngine_UnityEngine_SerializePrivateVariables2835496234MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SetupCoroutine1459791391.h"
#include "UnityEngine_UnityEngine_SetupCoroutine1459791391MethodDeclarations.h"
#include "mscorlib_System_Reflection_BindingFlags1523912596.h"
#include "mscorlib_System_Reflection_Binder1074302268.h"
#include "mscorlib_System_Reflection_ParameterModifier741930026.h"
#include "mscorlib_System_Globalization_CultureInfo1065375142.h"
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttr1486273289.h"
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttr1486273289MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SkeletonBone421858229.h"
#include "UnityEngine_UnityEngine_SkeletonBone421858229MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SliderHandler783692703.h"
#include "UnityEngine_UnityEngine_SliderHandler783692703MethodDeclarations.h"
#include "UnityEngine_UnityEngine_EventType637886954.h"
#include "UnityEngine_UnityEngine_Event4196595728MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI3134605553MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIUtility1028319349MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SystemClock4036018645MethodDeclarations.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "UnityEngine_UnityEngine_Event4196595728.h"
#include "UnityEngine_UnityEngine_SliderState1233388262.h"
#include "mscorlib_System_DateTime4283661327MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIContent2094828418.h"
#include "UnityEngine_UnityEngine_GUIContent2094828418MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SliderState1233388262MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3570684786.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3570684786MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3189060351MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achie2116066607.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserP2280656072.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3189060351.h"
#include "mscorlib_System_Action_1_gen872614854.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "mscorlib_System_Int641153838595.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope1305796361.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2242891083.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2242891083MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achie2116066607MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen229750097MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen229750097.h"
#include "mscorlib_System_Action_1_gen872614854MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_657441114.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_657441114MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Local1307362368MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Local1307362368.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3481375915.h"
#include "mscorlib_System_Action_1_gen2349069933MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achiev344600729.h"
#include "mscorlib_System_Action_1_gen2349069933.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3481375915MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2181296590.h"
#include "mscorlib_System_Action_1_gen645920862MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score3396031228.h"
#include "mscorlib_System_Action_1_gen645920862.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2181296590MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserP2280656072MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter1820874799MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leade1185876199MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leade1185876199.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter1820874799.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range1533311935.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope1608660171.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3208733121.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3208733121MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3814920354MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3814920354.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achiev344600729MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score3396031228MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState1609153288.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t3501516275 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.GUILayer>()
#define Component_GetComponent_TisGUILayer_t2983897946_m2891371969(__this, method) ((  GUILayer_t2983897946 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String UnityEngine.MasterServer::get_ipAddress()
extern "C"  String_t* MasterServer_get_ipAddress_m3869729575 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*MasterServer_get_ipAddress_m3869729575_ftn) ();
	static MasterServer_get_ipAddress_m3869729575_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MasterServer_get_ipAddress_m3869729575_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MasterServer::get_ipAddress()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.MasterServer::set_ipAddress(System.String)
extern "C"  void MasterServer_set_ipAddress_m4189760396 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	typedef void (*MasterServer_set_ipAddress_m4189760396_ftn) (String_t*);
	static MasterServer_set_ipAddress_m4189760396_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MasterServer_set_ipAddress_m4189760396_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MasterServer::set_ipAddress(System.String)");
	_il2cpp_icall_func(___value0);
}
// System.Int32 UnityEngine.MasterServer::get_port()
extern "C"  int32_t MasterServer_get_port_m1334239910 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*MasterServer_get_port_m1334239910_ftn) ();
	static MasterServer_get_port_m1334239910_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MasterServer_get_port_m1334239910_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MasterServer::get_port()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.MasterServer::set_port(System.Int32)
extern "C"  void MasterServer_set_port_m850451947 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*MasterServer_set_port_m850451947_ftn) (int32_t);
	static MasterServer_set_port_m850451947_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MasterServer_set_port_m850451947_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MasterServer::set_port(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.MasterServer::RequestHostList(System.String)
extern "C"  void MasterServer_RequestHostList_m2054420839 (Il2CppObject * __this /* static, unused */, String_t* ___gameTypeName0, const MethodInfo* method)
{
	typedef void (*MasterServer_RequestHostList_m2054420839_ftn) (String_t*);
	static MasterServer_RequestHostList_m2054420839_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MasterServer_RequestHostList_m2054420839_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MasterServer::RequestHostList(System.String)");
	_il2cpp_icall_func(___gameTypeName0);
}
// UnityEngine.HostData[] UnityEngine.MasterServer::PollHostList()
extern "C"  HostDataU5BU5D_t4021970803* MasterServer_PollHostList_m3223787845 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef HostDataU5BU5D_t4021970803* (*MasterServer_PollHostList_m3223787845_ftn) ();
	static MasterServer_PollHostList_m3223787845_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MasterServer_PollHostList_m3223787845_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MasterServer::PollHostList()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.MasterServer::RegisterHost(System.String,System.String,System.String)
extern "C"  void MasterServer_RegisterHost_m2388323251 (Il2CppObject * __this /* static, unused */, String_t* ___gameTypeName0, String_t* ___gameName1, String_t* ___comment2, const MethodInfo* method)
{
	typedef void (*MasterServer_RegisterHost_m2388323251_ftn) (String_t*, String_t*, String_t*);
	static MasterServer_RegisterHost_m2388323251_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MasterServer_RegisterHost_m2388323251_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MasterServer::RegisterHost(System.String,System.String,System.String)");
	_il2cpp_icall_func(___gameTypeName0, ___gameName1, ___comment2);
}
// System.Void UnityEngine.MasterServer::UnregisterHost()
extern "C"  void MasterServer_UnregisterHost_m864917440 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*MasterServer_UnregisterHost_m864917440_ftn) ();
	static MasterServer_UnregisterHost_m864917440_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MasterServer_UnregisterHost_m864917440_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MasterServer::UnregisterHost()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.MasterServer::ClearHostList()
extern "C"  void MasterServer_ClearHostList_m2859464953 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*MasterServer_ClearHostList_m2859464953_ftn) ();
	static MasterServer_ClearHostList_m2859464953_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MasterServer_ClearHostList_m2859464953_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MasterServer::ClearHostList()");
	_il2cpp_icall_func();
}
// System.Int32 UnityEngine.MasterServer::get_updateRate()
extern "C"  int32_t MasterServer_get_updateRate_m3442346606 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*MasterServer_get_updateRate_m3442346606_ftn) ();
	static MasterServer_get_updateRate_m3442346606_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MasterServer_get_updateRate_m3442346606_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MasterServer::get_updateRate()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.MasterServer::set_updateRate(System.Int32)
extern "C"  void MasterServer_set_updateRate_m2854261427 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*MasterServer_set_updateRate_m2854261427_ftn) (int32_t);
	static MasterServer_set_updateRate_m2854261427_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MasterServer_set_updateRate_m2854261427_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MasterServer::set_updateRate(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.MasterServer::get_dedicatedServer()
extern "C"  bool MasterServer_get_dedicatedServer_m2497971081 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*MasterServer_get_dedicatedServer_m2497971081_ftn) ();
	static MasterServer_get_dedicatedServer_m2497971081_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MasterServer_get_dedicatedServer_m2497971081_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MasterServer::get_dedicatedServer()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.MasterServer::set_dedicatedServer(System.Boolean)
extern "C"  void MasterServer_set_dedicatedServer_m1888993138 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*MasterServer_set_dedicatedServer_m1888993138_ftn) (bool);
	static MasterServer_set_dedicatedServer_m1888993138_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MasterServer_set_dedicatedServer_m1888993138_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MasterServer::set_dedicatedServer(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.MatchTargetWeightMask::.ctor(UnityEngine.Vector3,System.Single)
extern "C"  void MatchTargetWeightMask__ctor_m2493400991 (MatchTargetWeightMask_t258413904 * __this, Vector3_t4282066566  ___positionXYZWeight0, float ___rotationWeight1, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = ___positionXYZWeight0;
		__this->set_m_PositionXYZWeight_0(L_0);
		float L_1 = ___rotationWeight1;
		__this->set_m_RotationWeight_1(L_1);
		return;
	}
}
extern "C"  void MatchTargetWeightMask__ctor_m2493400991_AdjustorThunk (Il2CppObject * __this, Vector3_t4282066566  ___positionXYZWeight0, float ___rotationWeight1, const MethodInfo* method)
{
	MatchTargetWeightMask_t258413904 * _thisAdjusted = reinterpret_cast<MatchTargetWeightMask_t258413904 *>(__this + 1);
	MatchTargetWeightMask__ctor_m2493400991(_thisAdjusted, ___positionXYZWeight0, ___rotationWeight1, method);
}
// Conversion methods for marshalling of: UnityEngine.MatchTargetWeightMask
extern "C" void MatchTargetWeightMask_t258413904_marshal_pinvoke(const MatchTargetWeightMask_t258413904& unmarshaled, MatchTargetWeightMask_t258413904_marshaled_pinvoke& marshaled)
{
	Vector3_t4282066566_marshal_pinvoke(unmarshaled.get_m_PositionXYZWeight_0(), marshaled.___m_PositionXYZWeight_0);
	marshaled.___m_RotationWeight_1 = unmarshaled.get_m_RotationWeight_1();
}
extern "C" void MatchTargetWeightMask_t258413904_marshal_pinvoke_back(const MatchTargetWeightMask_t258413904_marshaled_pinvoke& marshaled, MatchTargetWeightMask_t258413904& unmarshaled)
{
	Vector3_t4282066566  unmarshaled_m_PositionXYZWeight_temp_0;
	memset(&unmarshaled_m_PositionXYZWeight_temp_0, 0, sizeof(unmarshaled_m_PositionXYZWeight_temp_0));
	Vector3_t4282066566_marshal_pinvoke_back(marshaled.___m_PositionXYZWeight_0, unmarshaled_m_PositionXYZWeight_temp_0);
	unmarshaled.set_m_PositionXYZWeight_0(unmarshaled_m_PositionXYZWeight_temp_0);
	float unmarshaled_m_RotationWeight_temp_1 = 0.0f;
	unmarshaled_m_RotationWeight_temp_1 = marshaled.___m_RotationWeight_1;
	unmarshaled.set_m_RotationWeight_1(unmarshaled_m_RotationWeight_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.MatchTargetWeightMask
extern "C" void MatchTargetWeightMask_t258413904_marshal_pinvoke_cleanup(MatchTargetWeightMask_t258413904_marshaled_pinvoke& marshaled)
{
	Vector3_t4282066566_marshal_pinvoke_cleanup(marshaled.___m_PositionXYZWeight_0);
}
// Conversion methods for marshalling of: UnityEngine.MatchTargetWeightMask
extern "C" void MatchTargetWeightMask_t258413904_marshal_com(const MatchTargetWeightMask_t258413904& unmarshaled, MatchTargetWeightMask_t258413904_marshaled_com& marshaled)
{
	Vector3_t4282066566_marshal_com(unmarshaled.get_m_PositionXYZWeight_0(), marshaled.___m_PositionXYZWeight_0);
	marshaled.___m_RotationWeight_1 = unmarshaled.get_m_RotationWeight_1();
}
extern "C" void MatchTargetWeightMask_t258413904_marshal_com_back(const MatchTargetWeightMask_t258413904_marshaled_com& marshaled, MatchTargetWeightMask_t258413904& unmarshaled)
{
	Vector3_t4282066566  unmarshaled_m_PositionXYZWeight_temp_0;
	memset(&unmarshaled_m_PositionXYZWeight_temp_0, 0, sizeof(unmarshaled_m_PositionXYZWeight_temp_0));
	Vector3_t4282066566_marshal_com_back(marshaled.___m_PositionXYZWeight_0, unmarshaled_m_PositionXYZWeight_temp_0);
	unmarshaled.set_m_PositionXYZWeight_0(unmarshaled_m_PositionXYZWeight_temp_0);
	float unmarshaled_m_RotationWeight_temp_1 = 0.0f;
	unmarshaled_m_RotationWeight_temp_1 = marshaled.___m_RotationWeight_1;
	unmarshaled.set_m_RotationWeight_1(unmarshaled_m_RotationWeight_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.MatchTargetWeightMask
extern "C" void MatchTargetWeightMask_t258413904_marshal_com_cleanup(MatchTargetWeightMask_t258413904_marshaled_com& marshaled)
{
	Vector3_t4282066566_marshal_com_cleanup(marshaled.___m_PositionXYZWeight_0);
}
// System.Void UnityEngine.Material::.ctor(System.String)
extern "C"  void Material__ctor_m1122544796 (Material_t3870600107 * __this, String_t* ___contents0, const MethodInfo* method)
{
	{
		Object__ctor_m570634126(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___contents0;
		Material_Internal_CreateWithString_m3386087895(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::.ctor(UnityEngine.Shader)
extern "C"  void Material__ctor_m2685909642 (Material_t3870600107 * __this, Shader_t3191267369 * ___shader0, const MethodInfo* method)
{
	{
		Object__ctor_m570634126(__this, /*hidden argument*/NULL);
		Shader_t3191267369 * L_0 = ___shader0;
		Material_Internal_CreateWithShader_m701341915(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::.ctor(UnityEngine.Material)
extern "C"  void Material__ctor_m2546967560 (Material_t3870600107 * __this, Material_t3870600107 * ___source0, const MethodInfo* method)
{
	{
		Object__ctor_m570634126(__this, /*hidden argument*/NULL);
		Material_t3870600107 * L_0 = ___source0;
		Material_Internal_CreateWithMaterial_m2349411671(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Shader UnityEngine.Material::get_shader()
extern "C"  Shader_t3191267369 * Material_get_shader_m2881845503 (Material_t3870600107 * __this, const MethodInfo* method)
{
	typedef Shader_t3191267369 * (*Material_get_shader_m2881845503_ftn) (Material_t3870600107 *);
	static Material_get_shader_m2881845503_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_get_shader_m2881845503_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::get_shader()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Material::set_shader(UnityEngine.Shader)
extern "C"  void Material_set_shader_m3742529604 (Material_t3870600107 * __this, Shader_t3191267369 * ___value0, const MethodInfo* method)
{
	typedef void (*Material_set_shader_m3742529604_ftn) (Material_t3870600107 *, Shader_t3191267369 *);
	static Material_set_shader_m3742529604_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_set_shader_m3742529604_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::set_shader(UnityEngine.Shader)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Color UnityEngine.Material::get_color()
extern Il2CppCodeGenString* _stringLiteral2785059396;
extern const uint32_t Material_get_color_m2268945527_MetadataUsageId;
extern "C"  Color_t4194546905  Material_get_color_m2268945527 (Material_t3870600107 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Material_get_color_m2268945527_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Color_t4194546905  L_0 = Material_GetColor_m1709543664(__this, _stringLiteral2785059396, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Material::set_color(UnityEngine.Color)
extern Il2CppCodeGenString* _stringLiteral2785059396;
extern const uint32_t Material_set_color_m3296857020_MetadataUsageId;
extern "C"  void Material_set_color_m3296857020 (Material_t3870600107 * __this, Color_t4194546905  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Material_set_color_m3296857020_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Color_t4194546905  L_0 = ___value0;
		Material_SetColor_m1918430019(__this, _stringLiteral2785059396, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Texture UnityEngine.Material::get_mainTexture()
extern Il2CppCodeGenString* _stringLiteral558922319;
extern const uint32_t Material_get_mainTexture_m1012267054_MetadataUsageId;
extern "C"  Texture_t2526458961 * Material_get_mainTexture_m1012267054 (Material_t3870600107 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Material_get_mainTexture_m1012267054_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture_t2526458961 * L_0 = Material_GetTexture_m1284113328(__this, _stringLiteral558922319, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Material::set_mainTexture(UnityEngine.Texture)
extern Il2CppCodeGenString* _stringLiteral558922319;
extern const uint32_t Material_set_mainTexture_m3116438437_MetadataUsageId;
extern "C"  void Material_set_mainTexture_m3116438437 (Material_t3870600107 * __this, Texture_t2526458961 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Material_set_mainTexture_m3116438437_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture_t2526458961 * L_0 = ___value0;
		Material_SetTexture_m1833724755(__this, _stringLiteral558922319, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::set_mainTextureScale(UnityEngine.Vector2)
extern Il2CppCodeGenString* _stringLiteral558922319;
extern const uint32_t Material_set_mainTextureScale_m2180744791_MetadataUsageId;
extern "C"  void Material_set_mainTextureScale_m2180744791 (Material_t3870600107 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Material_set_mainTextureScale_m2180744791_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2_t4282066565  L_0 = ___value0;
		Material_SetTextureScale_m1752758881(__this, _stringLiteral558922319, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetColor(System.String,UnityEngine.Color)
extern "C"  void Material_SetColor_m1918430019 (Material_t3870600107 * __this, String_t* ___propertyName0, Color_t4194546905  ___color1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName0;
		int32_t L_1 = Shader_PropertyToID_m3019342011(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Color_t4194546905  L_2 = ___color1;
		Material_SetColor_m54957808(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetColor(System.Int32,UnityEngine.Color)
extern "C"  void Material_SetColor_m54957808 (Material_t3870600107 * __this, int32_t ___nameID0, Color_t4194546905  ___color1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___nameID0;
		Material_INTERNAL_CALL_SetColor_m3209011477(NULL /*static, unused*/, __this, L_0, (&___color1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::INTERNAL_CALL_SetColor(UnityEngine.Material,System.Int32,UnityEngine.Color&)
extern "C"  void Material_INTERNAL_CALL_SetColor_m3209011477 (Il2CppObject * __this /* static, unused */, Material_t3870600107 * ___self0, int32_t ___nameID1, Color_t4194546905 * ___color2, const MethodInfo* method)
{
	typedef void (*Material_INTERNAL_CALL_SetColor_m3209011477_ftn) (Material_t3870600107 *, int32_t, Color_t4194546905 *);
	static Material_INTERNAL_CALL_SetColor_m3209011477_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_INTERNAL_CALL_SetColor_m3209011477_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::INTERNAL_CALL_SetColor(UnityEngine.Material,System.Int32,UnityEngine.Color&)");
	_il2cpp_icall_func(___self0, ___nameID1, ___color2);
}
// UnityEngine.Color UnityEngine.Material::GetColor(System.String)
extern "C"  Color_t4194546905  Material_GetColor_m1709543664 (Material_t3870600107 * __this, String_t* ___propertyName0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName0;
		int32_t L_1 = Shader_PropertyToID_m3019342011(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Color_t4194546905  L_2 = Material_GetColor_m317509027(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Color UnityEngine.Material::GetColor(System.Int32)
extern "C"  Color_t4194546905  Material_GetColor_m317509027 (Material_t3870600107 * __this, int32_t ___nameID0, const MethodInfo* method)
{
	Color_t4194546905  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___nameID0;
		Material_INTERNAL_CALL_GetColor_m3428345249(NULL /*static, unused*/, __this, L_0, (&V_0), /*hidden argument*/NULL);
		Color_t4194546905  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Material::INTERNAL_CALL_GetColor(UnityEngine.Material,System.Int32,UnityEngine.Color&)
extern "C"  void Material_INTERNAL_CALL_GetColor_m3428345249 (Il2CppObject * __this /* static, unused */, Material_t3870600107 * ___self0, int32_t ___nameID1, Color_t4194546905 * ___value2, const MethodInfo* method)
{
	typedef void (*Material_INTERNAL_CALL_GetColor_m3428345249_ftn) (Material_t3870600107 *, int32_t, Color_t4194546905 *);
	static Material_INTERNAL_CALL_GetColor_m3428345249_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_INTERNAL_CALL_GetColor_m3428345249_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::INTERNAL_CALL_GetColor(UnityEngine.Material,System.Int32,UnityEngine.Color&)");
	_il2cpp_icall_func(___self0, ___nameID1, ___value2);
}
// System.Void UnityEngine.Material::SetVector(System.String,UnityEngine.Vector4)
extern "C"  void Material_SetVector_m3505096203 (Material_t3870600107 * __this, String_t* ___propertyName0, Vector4_t4282066567  ___vector1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName0;
		float L_1 = (&___vector1)->get_x_1();
		float L_2 = (&___vector1)->get_y_2();
		float L_3 = (&___vector1)->get_z_3();
		float L_4 = (&___vector1)->get_w_4();
		Color_t4194546905  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Color__ctor_m2252924356(&L_5, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		Material_SetColor_m1918430019(__this, L_0, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetTexture(System.String,UnityEngine.Texture)
extern "C"  void Material_SetTexture_m1833724755 (Material_t3870600107 * __this, String_t* ___propertyName0, Texture_t2526458961 * ___texture1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName0;
		int32_t L_1 = Shader_PropertyToID_m3019342011(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Texture_t2526458961 * L_2 = ___texture1;
		Material_SetTexture_m3847256752(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetTexture(System.Int32,UnityEngine.Texture)
extern "C"  void Material_SetTexture_m3847256752 (Material_t3870600107 * __this, int32_t ___nameID0, Texture_t2526458961 * ___texture1, const MethodInfo* method)
{
	typedef void (*Material_SetTexture_m3847256752_ftn) (Material_t3870600107 *, int32_t, Texture_t2526458961 *);
	static Material_SetTexture_m3847256752_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_SetTexture_m3847256752_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::SetTexture(System.Int32,UnityEngine.Texture)");
	_il2cpp_icall_func(__this, ___nameID0, ___texture1);
}
// UnityEngine.Texture UnityEngine.Material::GetTexture(System.String)
extern "C"  Texture_t2526458961 * Material_GetTexture_m1284113328 (Material_t3870600107 * __this, String_t* ___propertyName0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName0;
		int32_t L_1 = Shader_PropertyToID_m3019342011(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Texture_t2526458961 * L_2 = Material_GetTexture_m3767468771(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Texture UnityEngine.Material::GetTexture(System.Int32)
extern "C"  Texture_t2526458961 * Material_GetTexture_m3767468771 (Material_t3870600107 * __this, int32_t ___nameID0, const MethodInfo* method)
{
	typedef Texture_t2526458961 * (*Material_GetTexture_m3767468771_ftn) (Material_t3870600107 *, int32_t);
	static Material_GetTexture_m3767468771_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_GetTexture_m3767468771_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::GetTexture(System.Int32)");
	return _il2cpp_icall_func(__this, ___nameID0);
}
// System.Void UnityEngine.Material::SetTextureOffset(System.String,UnityEngine.Vector2)
extern "C"  void Material_SetTextureOffset_m1301408396 (Material_t3870600107 * __this, String_t* ___propertyName0, Vector2_t4282066565  ___offset1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName0;
		Material_INTERNAL_CALL_SetTextureOffset_m3039341169(NULL /*static, unused*/, __this, L_0, (&___offset1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::INTERNAL_CALL_SetTextureOffset(UnityEngine.Material,System.String,UnityEngine.Vector2&)
extern "C"  void Material_INTERNAL_CALL_SetTextureOffset_m3039341169 (Il2CppObject * __this /* static, unused */, Material_t3870600107 * ___self0, String_t* ___propertyName1, Vector2_t4282066565 * ___offset2, const MethodInfo* method)
{
	typedef void (*Material_INTERNAL_CALL_SetTextureOffset_m3039341169_ftn) (Material_t3870600107 *, String_t*, Vector2_t4282066565 *);
	static Material_INTERNAL_CALL_SetTextureOffset_m3039341169_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_INTERNAL_CALL_SetTextureOffset_m3039341169_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::INTERNAL_CALL_SetTextureOffset(UnityEngine.Material,System.String,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___self0, ___propertyName1, ___offset2);
}
// System.Void UnityEngine.Material::SetTextureScale(System.String,UnityEngine.Vector2)
extern "C"  void Material_SetTextureScale_m1752758881 (Material_t3870600107 * __this, String_t* ___propertyName0, Vector2_t4282066565  ___scale1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName0;
		Material_INTERNAL_CALL_SetTextureScale_m708056228(NULL /*static, unused*/, __this, L_0, (&___scale1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::INTERNAL_CALL_SetTextureScale(UnityEngine.Material,System.String,UnityEngine.Vector2&)
extern "C"  void Material_INTERNAL_CALL_SetTextureScale_m708056228 (Il2CppObject * __this /* static, unused */, Material_t3870600107 * ___self0, String_t* ___propertyName1, Vector2_t4282066565 * ___scale2, const MethodInfo* method)
{
	typedef void (*Material_INTERNAL_CALL_SetTextureScale_m708056228_ftn) (Material_t3870600107 *, String_t*, Vector2_t4282066565 *);
	static Material_INTERNAL_CALL_SetTextureScale_m708056228_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_INTERNAL_CALL_SetTextureScale_m708056228_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::INTERNAL_CALL_SetTextureScale(UnityEngine.Material,System.String,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___self0, ___propertyName1, ___scale2);
}
// System.Void UnityEngine.Material::SetFloat(System.String,System.Single)
extern "C"  void Material_SetFloat_m981710063 (Material_t3870600107 * __this, String_t* ___propertyName0, float ___value1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName0;
		int32_t L_1 = Shader_PropertyToID_m3019342011(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = ___value1;
		Material_SetFloat_m170145518(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetFloat(System.Int32,System.Single)
extern "C"  void Material_SetFloat_m170145518 (Material_t3870600107 * __this, int32_t ___nameID0, float ___value1, const MethodInfo* method)
{
	typedef void (*Material_SetFloat_m170145518_ftn) (Material_t3870600107 *, int32_t, float);
	static Material_SetFloat_m170145518_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_SetFloat_m170145518_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::SetFloat(System.Int32,System.Single)");
	_il2cpp_icall_func(__this, ___nameID0, ___value1);
}
// System.Single UnityEngine.Material::GetFloat(System.String)
extern "C"  float Material_GetFloat_m2541456626 (Material_t3870600107 * __this, String_t* ___propertyName0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName0;
		int32_t L_1 = Shader_PropertyToID_m3019342011(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = Material_GetFloat_m344344929(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Single UnityEngine.Material::GetFloat(System.Int32)
extern "C"  float Material_GetFloat_m344344929 (Material_t3870600107 * __this, int32_t ___nameID0, const MethodInfo* method)
{
	typedef float (*Material_GetFloat_m344344929_ftn) (Material_t3870600107 *, int32_t);
	static Material_GetFloat_m344344929_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_GetFloat_m344344929_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::GetFloat(System.Int32)");
	return _il2cpp_icall_func(__this, ___nameID0);
}
// System.Void UnityEngine.Material::SetInt(System.String,System.Int32)
extern "C"  void Material_SetInt_m2649395040 (Material_t3870600107 * __this, String_t* ___propertyName0, int32_t ___value1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName0;
		int32_t L_1 = ___value1;
		Material_SetFloat_m981710063(__this, L_0, (((float)((float)L_1))), /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Material::HasProperty(System.String)
extern "C"  bool Material_HasProperty_m2077312757 (Material_t3870600107 * __this, String_t* ___propertyName0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName0;
		int32_t L_1 = Shader_PropertyToID_m3019342011(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		bool L_2 = Material_HasProperty_m190825214(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Material::HasProperty(System.Int32)
extern "C"  bool Material_HasProperty_m190825214 (Material_t3870600107 * __this, int32_t ___nameID0, const MethodInfo* method)
{
	typedef bool (*Material_HasProperty_m190825214_ftn) (Material_t3870600107 *, int32_t);
	static Material_HasProperty_m190825214_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_HasProperty_m190825214_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::HasProperty(System.Int32)");
	return _il2cpp_icall_func(__this, ___nameID0);
}
// System.Boolean UnityEngine.Material::SetPass(System.Int32)
extern "C"  bool Material_SetPass_m4241824642 (Material_t3870600107 * __this, int32_t ___pass0, const MethodInfo* method)
{
	typedef bool (*Material_SetPass_m4241824642_ftn) (Material_t3870600107 *, int32_t);
	static Material_SetPass_m4241824642_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_SetPass_m4241824642_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::SetPass(System.Int32)");
	return _il2cpp_icall_func(__this, ___pass0);
}
// System.Void UnityEngine.Material::Internal_CreateWithString(UnityEngine.Material,System.String)
extern "C"  void Material_Internal_CreateWithString_m3386087895 (Il2CppObject * __this /* static, unused */, Material_t3870600107 * ___mono0, String_t* ___contents1, const MethodInfo* method)
{
	typedef void (*Material_Internal_CreateWithString_m3386087895_ftn) (Material_t3870600107 *, String_t*);
	static Material_Internal_CreateWithString_m3386087895_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_Internal_CreateWithString_m3386087895_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::Internal_CreateWithString(UnityEngine.Material,System.String)");
	_il2cpp_icall_func(___mono0, ___contents1);
}
// System.Void UnityEngine.Material::Internal_CreateWithShader(UnityEngine.Material,UnityEngine.Shader)
extern "C"  void Material_Internal_CreateWithShader_m701341915 (Il2CppObject * __this /* static, unused */, Material_t3870600107 * ___mono0, Shader_t3191267369 * ___shader1, const MethodInfo* method)
{
	typedef void (*Material_Internal_CreateWithShader_m701341915_ftn) (Material_t3870600107 *, Shader_t3191267369 *);
	static Material_Internal_CreateWithShader_m701341915_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_Internal_CreateWithShader_m701341915_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::Internal_CreateWithShader(UnityEngine.Material,UnityEngine.Shader)");
	_il2cpp_icall_func(___mono0, ___shader1);
}
// System.Void UnityEngine.Material::Internal_CreateWithMaterial(UnityEngine.Material,UnityEngine.Material)
extern "C"  void Material_Internal_CreateWithMaterial_m2349411671 (Il2CppObject * __this /* static, unused */, Material_t3870600107 * ___mono0, Material_t3870600107 * ___source1, const MethodInfo* method)
{
	typedef void (*Material_Internal_CreateWithMaterial_m2349411671_ftn) (Material_t3870600107 *, Material_t3870600107 *);
	static Material_Internal_CreateWithMaterial_m2349411671_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_Internal_CreateWithMaterial_m2349411671_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::Internal_CreateWithMaterial(UnityEngine.Material,UnityEngine.Material)");
	_il2cpp_icall_func(___mono0, ___source1);
}
// System.Void UnityEngine.Material::EnableKeyword(System.String)
extern "C"  void Material_EnableKeyword_m3802712984 (Material_t3870600107 * __this, String_t* ___keyword0, const MethodInfo* method)
{
	typedef void (*Material_EnableKeyword_m3802712984_ftn) (Material_t3870600107 *, String_t*);
	static Material_EnableKeyword_m3802712984_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_EnableKeyword_m3802712984_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::EnableKeyword(System.String)");
	_il2cpp_icall_func(__this, ___keyword0);
}
// System.Void UnityEngine.Material::DisableKeyword(System.String)
extern "C"  void Material_DisableKeyword_m572736419 (Material_t3870600107 * __this, String_t* ___keyword0, const MethodInfo* method)
{
	typedef void (*Material_DisableKeyword_m572736419_ftn) (Material_t3870600107 *, String_t*);
	static Material_DisableKeyword_m572736419_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_DisableKeyword_m572736419_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::DisableKeyword(System.String)");
	_il2cpp_icall_func(__this, ___keyword0);
}
// System.Void UnityEngine.Mathf::.cctor()
extern Il2CppClass* MathfInternal_t4096243933_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Mathf__cctor_m1875403730_MetadataUsageId;
extern "C"  void Mathf__cctor_m1875403730 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mathf__cctor_m1875403730_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(MathfInternal_t4096243933_il2cpp_TypeInfo_var);
		bool L_0 = ((MathfInternal_t4096243933_StaticFields*)MathfInternal_t4096243933_il2cpp_TypeInfo_var->static_fields)->get_IsFlushToZeroEnabled_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MathfInternal_t4096243933_il2cpp_TypeInfo_var);
		float L_1 = ((MathfInternal_t4096243933_StaticFields*)MathfInternal_t4096243933_il2cpp_TypeInfo_var->static_fields)->get_FloatMinNormal_0();
		il2cpp_codegen_memory_barrier();
		G_B3_0 = L_1;
		goto IL_001d;
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MathfInternal_t4096243933_il2cpp_TypeInfo_var);
		float L_2 = ((MathfInternal_t4096243933_StaticFields*)MathfInternal_t4096243933_il2cpp_TypeInfo_var->static_fields)->get_FloatMinDenormal_1();
		il2cpp_codegen_memory_barrier();
		G_B3_0 = L_2;
	}

IL_001d:
	{
		((Mathf_t4203372500_StaticFields*)Mathf_t4203372500_il2cpp_TypeInfo_var->static_fields)->set_Epsilon_0(G_B3_0);
		return;
	}
}
// System.Single UnityEngine.Mathf::Sin(System.Single)
extern "C"  float Mathf_Sin_m2014639246 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		double L_1 = sin((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Cos(System.Single)
extern "C"  float Mathf_Cos_m2060147711 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		double L_1 = cos((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Tan(System.Single)
extern "C"  float Mathf_Tan_m3075991205 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		double L_1 = tan((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Asin(System.Single)
extern "C"  float Mathf_Asin_m3387069943 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		double L_1 = asin((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Acos(System.Single)
extern "C"  float Mathf_Acos_m3432578408 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		double L_1 = acos((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Atan2(System.Single,System.Single)
extern "C"  float Mathf_Atan2_m3138013817 (Il2CppObject * __this /* static, unused */, float ___y0, float ___x1, const MethodInfo* method)
{
	{
		float L_0 = ___y0;
		float L_1 = ___x1;
		double L_2 = atan2((((double)((double)L_0))), (((double)((double)L_1))));
		return (((float)((float)L_2)));
	}
}
// System.Single UnityEngine.Mathf::Sqrt(System.Single)
extern "C"  float Mathf_Sqrt_m3592270478 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		double L_1 = sqrt((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Abs(System.Single)
extern "C"  float Mathf_Abs_m3641135540 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		float L_1 = fabsf(L_0);
		return (((float)((float)L_1)));
	}
}
// System.Int32 UnityEngine.Mathf::Abs(System.Int32)
extern "C"  int32_t Mathf_Abs_m4265466780 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = abs(L_0);
		return L_1;
	}
}
// System.Single UnityEngine.Mathf::Min(System.Single,System.Single)
extern "C"  float Mathf_Min_m2322067385 (Il2CppObject * __this /* static, unused */, float ___a0, float ___b1, const MethodInfo* method)
{
	float G_B3_0 = 0.0f;
	{
		float L_0 = ___a0;
		float L_1 = ___b1;
		if ((!(((float)L_0) < ((float)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		float L_2 = ___a0;
		G_B3_0 = L_2;
		goto IL_000e;
	}

IL_000d:
	{
		float L_3 = ___b1;
		G_B3_0 = L_3;
	}

IL_000e:
	{
		return G_B3_0;
	}
}
// System.Single UnityEngine.Mathf::Min(System.Single[])
extern "C"  float Mathf_Min_m1491061234 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t2316563989* ___values0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	{
		SingleU5BU5D_t2316563989* L_0 = ___values0;
		NullCheck(L_0);
		V_0 = (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
		int32_t L_1 = V_0;
		if (L_1)
		{
			goto IL_0010;
		}
	}
	{
		return (0.0f);
	}

IL_0010:
	{
		SingleU5BU5D_t2316563989* L_2 = ___values0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		int32_t L_3 = 0;
		float L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_1 = L_4;
		V_2 = 1;
		goto IL_002c;
	}

IL_001b:
	{
		SingleU5BU5D_t2316563989* L_5 = ___values0;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		float L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		float L_9 = V_1;
		if ((!(((float)L_8) < ((float)L_9))))
		{
			goto IL_0028;
		}
	}
	{
		SingleU5BU5D_t2316563989* L_10 = ___values0;
		int32_t L_11 = V_2;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		float L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		V_1 = L_13;
	}

IL_0028:
	{
		int32_t L_14 = V_2;
		V_2 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_002c:
	{
		int32_t L_15 = V_2;
		int32_t L_16 = V_0;
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_001b;
		}
	}
	{
		float L_17 = V_1;
		return L_17;
	}
}
// System.Int32 UnityEngine.Mathf::Min(System.Int32,System.Int32)
extern "C"  int32_t Mathf_Min_m2413438171 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___a0;
		int32_t L_1 = ___b1;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_2 = ___a0;
		G_B3_0 = L_2;
		goto IL_000e;
	}

IL_000d:
	{
		int32_t L_3 = ___b1;
		G_B3_0 = L_3;
	}

IL_000e:
	{
		return G_B3_0;
	}
}
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
extern "C"  float Mathf_Max_m3923796455 (Il2CppObject * __this /* static, unused */, float ___a0, float ___b1, const MethodInfo* method)
{
	float G_B3_0 = 0.0f;
	{
		float L_0 = ___a0;
		float L_1 = ___b1;
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		float L_2 = ___a0;
		G_B3_0 = L_2;
		goto IL_000e;
	}

IL_000d:
	{
		float L_3 = ___b1;
		G_B3_0 = L_3;
	}

IL_000e:
	{
		return G_B3_0;
	}
}
// System.Single UnityEngine.Mathf::Max(System.Single[])
extern "C"  float Mathf_Max_m2571079968 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t2316563989* ___values0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	{
		SingleU5BU5D_t2316563989* L_0 = ___values0;
		NullCheck(L_0);
		V_0 = (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
		int32_t L_1 = V_0;
		if (L_1)
		{
			goto IL_0010;
		}
	}
	{
		return (0.0f);
	}

IL_0010:
	{
		SingleU5BU5D_t2316563989* L_2 = ___values0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		int32_t L_3 = 0;
		float L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_1 = L_4;
		V_2 = 1;
		goto IL_002c;
	}

IL_001b:
	{
		SingleU5BU5D_t2316563989* L_5 = ___values0;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		float L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		float L_9 = V_1;
		if ((!(((float)L_8) > ((float)L_9))))
		{
			goto IL_0028;
		}
	}
	{
		SingleU5BU5D_t2316563989* L_10 = ___values0;
		int32_t L_11 = V_2;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		float L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		V_1 = L_13;
	}

IL_0028:
	{
		int32_t L_14 = V_2;
		V_2 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_002c:
	{
		int32_t L_15 = V_2;
		int32_t L_16 = V_0;
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_001b;
		}
	}
	{
		float L_17 = V_1;
		return L_17;
	}
}
// System.Int32 UnityEngine.Mathf::Max(System.Int32,System.Int32)
extern "C"  int32_t Mathf_Max_m2911193737 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___a0;
		int32_t L_1 = ___b1;
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_2 = ___a0;
		G_B3_0 = L_2;
		goto IL_000e;
	}

IL_000d:
	{
		int32_t L_3 = ___b1;
		G_B3_0 = L_3;
	}

IL_000e:
	{
		return G_B3_0;
	}
}
// System.Single UnityEngine.Mathf::Pow(System.Single,System.Single)
extern "C"  float Mathf_Pow_m2793367923 (Il2CppObject * __this /* static, unused */, float ___f0, float ___p1, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		float L_1 = ___p1;
		double L_2 = pow((((double)((double)L_0))), (((double)((double)L_1))));
		return (((float)((float)L_2)));
	}
}
// System.Single UnityEngine.Mathf::Log(System.Single,System.Single)
extern "C"  float Mathf_Log_m2062790663 (Il2CppObject * __this /* static, unused */, float ___f0, float ___p1, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		float L_1 = ___p1;
		double L_2 = Math_Log_m2309558476(NULL /*static, unused*/, (((double)((double)L_0))), (((double)((double)L_1))), /*hidden argument*/NULL);
		return (((float)((float)L_2)));
	}
}
// System.Single UnityEngine.Mathf::Log(System.Single)
extern "C"  float Mathf_Log_m3507873954 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		double L_1 = log((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Ceil(System.Single)
extern "C"  float Mathf_Ceil_m3793305609 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		double L_1 = ceil((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Floor(System.Single)
extern "C"  float Mathf_Floor_m1715447770 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		double L_1 = floor((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Round(System.Single)
extern "C"  float Mathf_Round_m2677810712 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		double L_1 = bankers_round((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Int32 UnityEngine.Mathf::CeilToInt(System.Single)
extern "C"  int32_t Mathf_CeilToInt_m3621832739 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		double L_1 = ceil((((double)((double)L_0))));
		return (((int32_t)((int32_t)L_1)));
	}
}
// System.Int32 UnityEngine.Mathf::FloorToInt(System.Single)
extern "C"  int32_t Mathf_FloorToInt_m268511322 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		double L_1 = floor((((double)((double)L_0))));
		return (((int32_t)((int32_t)L_1)));
	}
}
// System.Int32 UnityEngine.Mathf::RoundToInt(System.Single)
extern "C"  int32_t Mathf_RoundToInt_m3163545820 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		double L_1 = bankers_round((((double)((double)L_0))));
		return (((int32_t)((int32_t)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Sign(System.Single)
extern "C"  float Mathf_Sign_m4040614993 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	float G_B3_0 = 0.0f;
	{
		float L_0 = ___f0;
		if ((!(((float)L_0) >= ((float)(0.0f)))))
		{
			goto IL_0015;
		}
	}
	{
		G_B3_0 = (1.0f);
		goto IL_001a;
	}

IL_0015:
	{
		G_B3_0 = (-1.0f);
	}

IL_001a:
	{
		return G_B3_0;
	}
}
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_Clamp_m3872743893 (Il2CppObject * __this /* static, unused */, float ___value0, float ___min1, float ___max2, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		float L_1 = ___min1;
		if ((!(((float)L_0) < ((float)L_1))))
		{
			goto IL_000f;
		}
	}
	{
		float L_2 = ___min1;
		___value0 = L_2;
		goto IL_0019;
	}

IL_000f:
	{
		float L_3 = ___value0;
		float L_4 = ___max2;
		if ((!(((float)L_3) > ((float)L_4))))
		{
			goto IL_0019;
		}
	}
	{
		float L_5 = ___max2;
		___value0 = L_5;
	}

IL_0019:
	{
		float L_6 = ___value0;
		return L_6;
	}
}
// System.Int32 UnityEngine.Mathf::Clamp(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t Mathf_Clamp_m510460741 (Il2CppObject * __this /* static, unused */, int32_t ___value0, int32_t ___min1, int32_t ___max2, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = ___min1;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_000f;
		}
	}
	{
		int32_t L_2 = ___min1;
		___value0 = L_2;
		goto IL_0019;
	}

IL_000f:
	{
		int32_t L_3 = ___value0;
		int32_t L_4 = ___max2;
		if ((((int32_t)L_3) <= ((int32_t)L_4)))
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_5 = ___max2;
		___value0 = L_5;
	}

IL_0019:
	{
		int32_t L_6 = ___value0;
		return L_6;
	}
}
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
extern "C"  float Mathf_Clamp01_m2272733930 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		if ((!(((float)L_0) < ((float)(0.0f)))))
		{
			goto IL_0011;
		}
	}
	{
		return (0.0f);
	}

IL_0011:
	{
		float L_1 = ___value0;
		if ((!(((float)L_1) > ((float)(1.0f)))))
		{
			goto IL_0022;
		}
	}
	{
		return (1.0f);
	}

IL_0022:
	{
		float L_2 = ___value0;
		return L_2;
	}
}
// System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Mathf_Lerp_m3257777633_MetadataUsageId;
extern "C"  float Mathf_Lerp_m3257777633 (Il2CppObject * __this /* static, unused */, float ___a0, float ___b1, float ___t2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mathf_Lerp_m3257777633_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___a0;
		float L_1 = ___b1;
		float L_2 = ___a0;
		float L_3 = ___t2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_4 = Mathf_Clamp01_m2272733930(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return ((float)((float)L_0+(float)((float)((float)((float)((float)L_1-(float)L_2))*(float)L_4))));
	}
}
// System.Single UnityEngine.Mathf::LerpAngle(System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Mathf_LerpAngle_m1852538964_MetadataUsageId;
extern "C"  float Mathf_LerpAngle_m1852538964 (Il2CppObject * __this /* static, unused */, float ___a0, float ___b1, float ___t2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mathf_LerpAngle_m1852538964_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = ___b1;
		float L_1 = ___a0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Repeat_m3424250200(NULL /*static, unused*/, ((float)((float)L_0-(float)L_1)), (360.0f), /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = V_0;
		if ((!(((float)L_3) > ((float)(180.0f)))))
		{
			goto IL_0021;
		}
	}
	{
		float L_4 = V_0;
		V_0 = ((float)((float)L_4-(float)(360.0f)));
	}

IL_0021:
	{
		float L_5 = ___a0;
		float L_6 = V_0;
		float L_7 = ___t2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_8 = Mathf_Clamp01_m2272733930(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return ((float)((float)L_5+(float)((float)((float)L_6*(float)L_8))));
	}
}
// System.Single UnityEngine.Mathf::SmoothStep(System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Mathf_SmoothStep_m1876640478_MetadataUsageId;
extern "C"  float Mathf_SmoothStep_m1876640478 (Il2CppObject * __this /* static, unused */, float ___from0, float ___to1, float ___t2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mathf_SmoothStep_m1876640478_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___t2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m2272733930(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		___t2 = L_1;
		float L_2 = ___t2;
		float L_3 = ___t2;
		float L_4 = ___t2;
		float L_5 = ___t2;
		float L_6 = ___t2;
		___t2 = ((float)((float)((float)((float)((float)((float)((float)((float)(-2.0f)*(float)L_2))*(float)L_3))*(float)L_4))+(float)((float)((float)((float)((float)(3.0f)*(float)L_5))*(float)L_6))));
		float L_7 = ___to1;
		float L_8 = ___t2;
		float L_9 = ___from0;
		float L_10 = ___t2;
		return ((float)((float)((float)((float)L_7*(float)L_8))+(float)((float)((float)L_9*(float)((float)((float)(1.0f)-(float)L_10))))));
	}
}
// System.Boolean UnityEngine.Mathf::Approximately(System.Single,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Mathf_Approximately_m1395529776_MetadataUsageId;
extern "C"  bool Mathf_Approximately_m1395529776 (Il2CppObject * __this /* static, unused */, float ___a0, float ___b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mathf_Approximately_m1395529776_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___b1;
		float L_1 = ___a0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_2 = fabsf(((float)((float)L_0-(float)L_1)));
		float L_3 = ___a0;
		float L_4 = fabsf(L_3);
		float L_5 = ___b1;
		float L_6 = fabsf(L_5);
		float L_7 = Mathf_Max_m3923796455(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		float L_8 = ((Mathf_t4203372500_StaticFields*)Mathf_t4203372500_il2cpp_TypeInfo_var->static_fields)->get_Epsilon_0();
		float L_9 = Mathf_Max_m3923796455(NULL /*static, unused*/, ((float)((float)(1.0E-06f)*(float)L_7)), ((float)((float)L_8*(float)(8.0f))), /*hidden argument*/NULL);
		return (bool)((((float)L_2) < ((float)L_9))? 1 : 0);
	}
}
// System.Single UnityEngine.Mathf::SmoothDamp(System.Single,System.Single,System.Single&,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Mathf_SmoothDamp_m488292903_MetadataUsageId;
extern "C"  float Mathf_SmoothDamp_m488292903 (Il2CppObject * __this /* static, unused */, float ___current0, float ___target1, float* ___currentVelocity2, float ___smoothTime3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mathf_SmoothDamp_m488292903_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = (std::numeric_limits<float>::infinity());
		float L_1 = ___current0;
		float L_2 = ___target1;
		float* L_3 = ___currentVelocity2;
		float L_4 = ___smoothTime3;
		float L_5 = V_1;
		float L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_7 = Mathf_SmoothDamp_m779170481(NULL /*static, unused*/, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Single UnityEngine.Mathf::SmoothDamp(System.Single,System.Single,System.Single&,System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Mathf_SmoothDamp_m779170481_MetadataUsageId;
extern "C"  float Mathf_SmoothDamp_m779170481 (Il2CppObject * __this /* static, unused */, float ___current0, float ___target1, float* ___currentVelocity2, float ___smoothTime3, float ___maxSpeed4, float ___deltaTime5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mathf_SmoothDamp_m779170481_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	{
		float L_0 = ___smoothTime3;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Max_m3923796455(NULL /*static, unused*/, (0.0001f), L_0, /*hidden argument*/NULL);
		___smoothTime3 = L_1;
		float L_2 = ___smoothTime3;
		V_0 = ((float)((float)(2.0f)/(float)L_2));
		float L_3 = V_0;
		float L_4 = ___deltaTime5;
		V_1 = ((float)((float)L_3*(float)L_4));
		float L_5 = V_1;
		float L_6 = V_1;
		float L_7 = V_1;
		float L_8 = V_1;
		float L_9 = V_1;
		float L_10 = V_1;
		V_2 = ((float)((float)(1.0f)/(float)((float)((float)((float)((float)((float)((float)(1.0f)+(float)L_5))+(float)((float)((float)((float)((float)(0.48f)*(float)L_6))*(float)L_7))))+(float)((float)((float)((float)((float)((float)((float)(0.235f)*(float)L_8))*(float)L_9))*(float)L_10))))));
		float L_11 = ___current0;
		float L_12 = ___target1;
		V_3 = ((float)((float)L_11-(float)L_12));
		float L_13 = ___target1;
		V_4 = L_13;
		float L_14 = ___maxSpeed4;
		float L_15 = ___smoothTime3;
		V_5 = ((float)((float)L_14*(float)L_15));
		float L_16 = V_3;
		float L_17 = V_5;
		float L_18 = V_5;
		float L_19 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_16, ((-L_17)), L_18, /*hidden argument*/NULL);
		V_3 = L_19;
		float L_20 = ___current0;
		float L_21 = V_3;
		___target1 = ((float)((float)L_20-(float)L_21));
		float* L_22 = ___currentVelocity2;
		float L_23 = V_0;
		float L_24 = V_3;
		float L_25 = ___deltaTime5;
		V_6 = ((float)((float)((float)((float)(*((float*)L_22))+(float)((float)((float)L_23*(float)L_24))))*(float)L_25));
		float* L_26 = ___currentVelocity2;
		float* L_27 = ___currentVelocity2;
		float L_28 = V_0;
		float L_29 = V_6;
		float L_30 = V_2;
		*((float*)(L_26)) = (float)((float)((float)((float)((float)(*((float*)L_27))-(float)((float)((float)L_28*(float)L_29))))*(float)L_30));
		float L_31 = ___target1;
		float L_32 = V_3;
		float L_33 = V_6;
		float L_34 = V_2;
		V_7 = ((float)((float)L_31+(float)((float)((float)((float)((float)L_32+(float)L_33))*(float)L_34))));
		float L_35 = V_4;
		float L_36 = ___current0;
		float L_37 = V_7;
		float L_38 = V_4;
		if ((!(((uint32_t)((((float)((float)((float)L_35-(float)L_36))) > ((float)(0.0f)))? 1 : 0)) == ((uint32_t)((((float)L_37) > ((float)L_38))? 1 : 0)))))
		{
			goto IL_00a0;
		}
	}
	{
		float L_39 = V_4;
		V_7 = L_39;
		float* L_40 = ___currentVelocity2;
		float L_41 = V_7;
		float L_42 = V_4;
		float L_43 = ___deltaTime5;
		*((float*)(L_40)) = (float)((float)((float)((float)((float)L_41-(float)L_42))/(float)L_43));
	}

IL_00a0:
	{
		float L_44 = V_7;
		return L_44;
	}
}
// System.Single UnityEngine.Mathf::SmoothDampAngle(System.Single,System.Single,System.Single&,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Mathf_SmoothDampAngle_m1358351558_MetadataUsageId;
extern "C"  float Mathf_SmoothDampAngle_m1358351558 (Il2CppObject * __this /* static, unused */, float ___current0, float ___target1, float* ___currentVelocity2, float ___smoothTime3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mathf_SmoothDampAngle_m1358351558_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = (std::numeric_limits<float>::infinity());
		float L_1 = ___current0;
		float L_2 = ___target1;
		float* L_3 = ___currentVelocity2;
		float L_4 = ___smoothTime3;
		float L_5 = V_1;
		float L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_7 = Mathf_SmoothDampAngle_m131672272(NULL /*static, unused*/, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Single UnityEngine.Mathf::SmoothDampAngle(System.Single,System.Single,System.Single&,System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Mathf_SmoothDampAngle_m131672272_MetadataUsageId;
extern "C"  float Mathf_SmoothDampAngle_m131672272 (Il2CppObject * __this /* static, unused */, float ___current0, float ___target1, float* ___currentVelocity2, float ___smoothTime3, float ___maxSpeed4, float ___deltaTime5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mathf_SmoothDampAngle_m131672272_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___current0;
		float L_1 = ___current0;
		float L_2 = ___target1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_3 = Mathf_DeltaAngle_m226689272(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		___target1 = ((float)((float)L_0+(float)L_3));
		float L_4 = ___current0;
		float L_5 = ___target1;
		float* L_6 = ___currentVelocity2;
		float L_7 = ___smoothTime3;
		float L_8 = ___maxSpeed4;
		float L_9 = ___deltaTime5;
		float L_10 = Mathf_SmoothDamp_m779170481(NULL /*static, unused*/, L_4, L_5, L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Single UnityEngine.Mathf::Repeat(System.Single,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Mathf_Repeat_m3424250200_MetadataUsageId;
extern "C"  float Mathf_Repeat_m3424250200 (Il2CppObject * __this /* static, unused */, float ___t0, float ___length1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mathf_Repeat_m3424250200_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___t0;
		float L_1 = ___t0;
		float L_2 = ___length1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_3 = floorf(((float)((float)L_1/(float)L_2)));
		float L_4 = ___length1;
		return ((float)((float)L_0-(float)((float)((float)L_3*(float)L_4))));
	}
}
// System.Single UnityEngine.Mathf::InverseLerp(System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Mathf_InverseLerp_m152689993_MetadataUsageId;
extern "C"  float Mathf_InverseLerp_m152689993 (Il2CppObject * __this /* static, unused */, float ___a0, float ___b1, float ___value2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mathf_InverseLerp_m152689993_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___a0;
		float L_1 = ___b1;
		if ((((float)L_0) == ((float)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		float L_2 = ___value2;
		float L_3 = ___a0;
		float L_4 = ___b1;
		float L_5 = ___a0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_6 = Mathf_Clamp01_m2272733930(NULL /*static, unused*/, ((float)((float)((float)((float)L_2-(float)L_3))/(float)((float)((float)L_4-(float)L_5)))), /*hidden argument*/NULL);
		return L_6;
	}

IL_0014:
	{
		return (0.0f);
	}
}
// System.Single UnityEngine.Mathf::DeltaAngle(System.Single,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Mathf_DeltaAngle_m226689272_MetadataUsageId;
extern "C"  float Mathf_DeltaAngle_m226689272 (Il2CppObject * __this /* static, unused */, float ___current0, float ___target1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mathf_DeltaAngle_m226689272_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = ___target1;
		float L_1 = ___current0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Repeat_m3424250200(NULL /*static, unused*/, ((float)((float)L_0-(float)L_1)), (360.0f), /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = V_0;
		if ((!(((float)L_3) > ((float)(180.0f)))))
		{
			goto IL_0021;
		}
	}
	{
		float L_4 = V_0;
		V_0 = ((float)((float)L_4-(float)(360.0f)));
	}

IL_0021:
	{
		float L_5 = V_0;
		return L_5;
	}
}
// Conversion methods for marshalling of: UnityEngine.Mathf
extern "C" void Mathf_t4203372500_marshal_pinvoke(const Mathf_t4203372500& unmarshaled, Mathf_t4203372500_marshaled_pinvoke& marshaled)
{
}
extern "C" void Mathf_t4203372500_marshal_pinvoke_back(const Mathf_t4203372500_marshaled_pinvoke& marshaled, Mathf_t4203372500& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngine.Mathf
extern "C" void Mathf_t4203372500_marshal_pinvoke_cleanup(Mathf_t4203372500_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Mathf
extern "C" void Mathf_t4203372500_marshal_com(const Mathf_t4203372500& unmarshaled, Mathf_t4203372500_marshaled_com& marshaled)
{
}
extern "C" void Mathf_t4203372500_marshal_com_back(const Mathf_t4203372500_marshaled_com& marshaled, Mathf_t4203372500& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngine.Mathf
extern "C" void Mathf_t4203372500_marshal_com_cleanup(Mathf_t4203372500_marshaled_com& marshaled)
{
}
// System.Single UnityEngine.Matrix4x4::get_Item(System.Int32,System.Int32)
extern "C"  float Matrix4x4_get_Item_m2279862332 (Matrix4x4_t1651859333 * __this, int32_t ___row0, int32_t ___column1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___row0;
		int32_t L_1 = ___column1;
		float L_2 = Matrix4x4_get_Item_m1280478331(__this, ((int32_t)((int32_t)L_0+(int32_t)((int32_t)((int32_t)L_1*(int32_t)4)))), /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  float Matrix4x4_get_Item_m2279862332_AdjustorThunk (Il2CppObject * __this, int32_t ___row0, int32_t ___column1, const MethodInfo* method)
{
	Matrix4x4_t1651859333 * _thisAdjusted = reinterpret_cast<Matrix4x4_t1651859333 *>(__this + 1);
	return Matrix4x4_get_Item_m2279862332(_thisAdjusted, ___row0, ___column1, method);
}
// System.Void UnityEngine.Matrix4x4::set_Item(System.Int32,System.Int32,System.Single)
extern "C"  void Matrix4x4_set_Item_m2343951137 (Matrix4x4_t1651859333 * __this, int32_t ___row0, int32_t ___column1, float ___value2, const MethodInfo* method)
{
	{
		int32_t L_0 = ___row0;
		int32_t L_1 = ___column1;
		float L_2 = ___value2;
		Matrix4x4_set_Item_m3979676448(__this, ((int32_t)((int32_t)L_0+(int32_t)((int32_t)((int32_t)L_1*(int32_t)4)))), L_2, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Matrix4x4_set_Item_m2343951137_AdjustorThunk (Il2CppObject * __this, int32_t ___row0, int32_t ___column1, float ___value2, const MethodInfo* method)
{
	Matrix4x4_t1651859333 * _thisAdjusted = reinterpret_cast<Matrix4x4_t1651859333 *>(__this + 1);
	Matrix4x4_set_Item_m2343951137(_thisAdjusted, ___row0, ___column1, ___value2, method);
}
// System.Single UnityEngine.Matrix4x4::get_Item(System.Int32)
extern Il2CppClass* IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3642945605;
extern const uint32_t Matrix4x4_get_Item_m1280478331_MetadataUsageId;
extern "C"  float Matrix4x4_get_Item_m1280478331 (Matrix4x4_t1651859333 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_get_Item_m1280478331_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_004d;
		}
		if (L_1 == 1)
		{
			goto IL_0054;
		}
		if (L_1 == 2)
		{
			goto IL_005b;
		}
		if (L_1 == 3)
		{
			goto IL_0062;
		}
		if (L_1 == 4)
		{
			goto IL_0069;
		}
		if (L_1 == 5)
		{
			goto IL_0070;
		}
		if (L_1 == 6)
		{
			goto IL_0077;
		}
		if (L_1 == 7)
		{
			goto IL_007e;
		}
		if (L_1 == 8)
		{
			goto IL_0085;
		}
		if (L_1 == 9)
		{
			goto IL_008c;
		}
		if (L_1 == 10)
		{
			goto IL_0093;
		}
		if (L_1 == 11)
		{
			goto IL_009a;
		}
		if (L_1 == 12)
		{
			goto IL_00a1;
		}
		if (L_1 == 13)
		{
			goto IL_00a8;
		}
		if (L_1 == 14)
		{
			goto IL_00af;
		}
		if (L_1 == 15)
		{
			goto IL_00b6;
		}
	}
	{
		goto IL_00bd;
	}

IL_004d:
	{
		float L_2 = __this->get_m00_0();
		return L_2;
	}

IL_0054:
	{
		float L_3 = __this->get_m10_1();
		return L_3;
	}

IL_005b:
	{
		float L_4 = __this->get_m20_2();
		return L_4;
	}

IL_0062:
	{
		float L_5 = __this->get_m30_3();
		return L_5;
	}

IL_0069:
	{
		float L_6 = __this->get_m01_4();
		return L_6;
	}

IL_0070:
	{
		float L_7 = __this->get_m11_5();
		return L_7;
	}

IL_0077:
	{
		float L_8 = __this->get_m21_6();
		return L_8;
	}

IL_007e:
	{
		float L_9 = __this->get_m31_7();
		return L_9;
	}

IL_0085:
	{
		float L_10 = __this->get_m02_8();
		return L_10;
	}

IL_008c:
	{
		float L_11 = __this->get_m12_9();
		return L_11;
	}

IL_0093:
	{
		float L_12 = __this->get_m22_10();
		return L_12;
	}

IL_009a:
	{
		float L_13 = __this->get_m32_11();
		return L_13;
	}

IL_00a1:
	{
		float L_14 = __this->get_m03_12();
		return L_14;
	}

IL_00a8:
	{
		float L_15 = __this->get_m13_13();
		return L_15;
	}

IL_00af:
	{
		float L_16 = __this->get_m23_14();
		return L_16;
	}

IL_00b6:
	{
		float L_17 = __this->get_m33_15();
		return L_17;
	}

IL_00bd:
	{
		IndexOutOfRangeException_t3456360697 * L_18 = (IndexOutOfRangeException_t3456360697 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1621772274(L_18, _stringLiteral3642945605, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_18);
	}
}
extern "C"  float Matrix4x4_get_Item_m1280478331_AdjustorThunk (Il2CppObject * __this, int32_t ___index0, const MethodInfo* method)
{
	Matrix4x4_t1651859333 * _thisAdjusted = reinterpret_cast<Matrix4x4_t1651859333 *>(__this + 1);
	return Matrix4x4_get_Item_m1280478331(_thisAdjusted, ___index0, method);
}
// System.Void UnityEngine.Matrix4x4::set_Item(System.Int32,System.Single)
extern Il2CppClass* IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3642945605;
extern const uint32_t Matrix4x4_set_Item_m3979676448_MetadataUsageId;
extern "C"  void Matrix4x4_set_Item_m3979676448 (Matrix4x4_t1651859333 * __this, int32_t ___index0, float ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_set_Item_m3979676448_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_004d;
		}
		if (L_1 == 1)
		{
			goto IL_0059;
		}
		if (L_1 == 2)
		{
			goto IL_0065;
		}
		if (L_1 == 3)
		{
			goto IL_0071;
		}
		if (L_1 == 4)
		{
			goto IL_007d;
		}
		if (L_1 == 5)
		{
			goto IL_0089;
		}
		if (L_1 == 6)
		{
			goto IL_0095;
		}
		if (L_1 == 7)
		{
			goto IL_00a1;
		}
		if (L_1 == 8)
		{
			goto IL_00ad;
		}
		if (L_1 == 9)
		{
			goto IL_00b9;
		}
		if (L_1 == 10)
		{
			goto IL_00c5;
		}
		if (L_1 == 11)
		{
			goto IL_00d1;
		}
		if (L_1 == 12)
		{
			goto IL_00dd;
		}
		if (L_1 == 13)
		{
			goto IL_00e9;
		}
		if (L_1 == 14)
		{
			goto IL_00f5;
		}
		if (L_1 == 15)
		{
			goto IL_0101;
		}
	}
	{
		goto IL_010d;
	}

IL_004d:
	{
		float L_2 = ___value1;
		__this->set_m00_0(L_2);
		goto IL_0118;
	}

IL_0059:
	{
		float L_3 = ___value1;
		__this->set_m10_1(L_3);
		goto IL_0118;
	}

IL_0065:
	{
		float L_4 = ___value1;
		__this->set_m20_2(L_4);
		goto IL_0118;
	}

IL_0071:
	{
		float L_5 = ___value1;
		__this->set_m30_3(L_5);
		goto IL_0118;
	}

IL_007d:
	{
		float L_6 = ___value1;
		__this->set_m01_4(L_6);
		goto IL_0118;
	}

IL_0089:
	{
		float L_7 = ___value1;
		__this->set_m11_5(L_7);
		goto IL_0118;
	}

IL_0095:
	{
		float L_8 = ___value1;
		__this->set_m21_6(L_8);
		goto IL_0118;
	}

IL_00a1:
	{
		float L_9 = ___value1;
		__this->set_m31_7(L_9);
		goto IL_0118;
	}

IL_00ad:
	{
		float L_10 = ___value1;
		__this->set_m02_8(L_10);
		goto IL_0118;
	}

IL_00b9:
	{
		float L_11 = ___value1;
		__this->set_m12_9(L_11);
		goto IL_0118;
	}

IL_00c5:
	{
		float L_12 = ___value1;
		__this->set_m22_10(L_12);
		goto IL_0118;
	}

IL_00d1:
	{
		float L_13 = ___value1;
		__this->set_m32_11(L_13);
		goto IL_0118;
	}

IL_00dd:
	{
		float L_14 = ___value1;
		__this->set_m03_12(L_14);
		goto IL_0118;
	}

IL_00e9:
	{
		float L_15 = ___value1;
		__this->set_m13_13(L_15);
		goto IL_0118;
	}

IL_00f5:
	{
		float L_16 = ___value1;
		__this->set_m23_14(L_16);
		goto IL_0118;
	}

IL_0101:
	{
		float L_17 = ___value1;
		__this->set_m33_15(L_17);
		goto IL_0118;
	}

IL_010d:
	{
		IndexOutOfRangeException_t3456360697 * L_18 = (IndexOutOfRangeException_t3456360697 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1621772274(L_18, _stringLiteral3642945605, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_18);
	}

IL_0118:
	{
		return;
	}
}
extern "C"  void Matrix4x4_set_Item_m3979676448_AdjustorThunk (Il2CppObject * __this, int32_t ___index0, float ___value1, const MethodInfo* method)
{
	Matrix4x4_t1651859333 * _thisAdjusted = reinterpret_cast<Matrix4x4_t1651859333 *>(__this + 1);
	Matrix4x4_set_Item_m3979676448(_thisAdjusted, ___index0, ___value1, method);
}
// System.Int32 UnityEngine.Matrix4x4::GetHashCode()
extern "C"  int32_t Matrix4x4_GetHashCode_m4083477721 (Matrix4x4_t1651859333 * __this, const MethodInfo* method)
{
	Vector4_t4282066567  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector4_t4282066567  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector4_t4282066567  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector4_t4282066567  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Vector4_t4282066567  L_0 = Matrix4x4_GetColumn_m786071102(__this, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Vector4_GetHashCode_m3402333527((&V_0), /*hidden argument*/NULL);
		Vector4_t4282066567  L_2 = Matrix4x4_GetColumn_m786071102(__this, 1, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Vector4_GetHashCode_m3402333527((&V_1), /*hidden argument*/NULL);
		Vector4_t4282066567  L_4 = Matrix4x4_GetColumn_m786071102(__this, 2, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = Vector4_GetHashCode_m3402333527((&V_2), /*hidden argument*/NULL);
		Vector4_t4282066567  L_6 = Matrix4x4_GetColumn_m786071102(__this, 3, /*hidden argument*/NULL);
		V_3 = L_6;
		int32_t L_7 = Vector4_GetHashCode_m3402333527((&V_3), /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
	}
}
extern "C"  int32_t Matrix4x4_GetHashCode_m4083477721_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Matrix4x4_t1651859333 * _thisAdjusted = reinterpret_cast<Matrix4x4_t1651859333 *>(__this + 1);
	return Matrix4x4_GetHashCode_m4083477721(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Matrix4x4::Equals(System.Object)
extern Il2CppClass* Matrix4x4_t1651859333_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector4_t4282066567_il2cpp_TypeInfo_var;
extern const uint32_t Matrix4x4_Equals_m3534208385_MetadataUsageId;
extern "C"  bool Matrix4x4_Equals_m3534208385 (Matrix4x4_t1651859333 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_Equals_m3534208385_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t1651859333  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector4_t4282066567  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector4_t4282066567  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector4_t4282066567  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector4_t4282066567  V_4;
	memset(&V_4, 0, sizeof(V_4));
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Matrix4x4_t1651859333_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(Matrix4x4_t1651859333 *)((Matrix4x4_t1651859333 *)UnBox (L_1, Matrix4x4_t1651859333_il2cpp_TypeInfo_var))));
		Vector4_t4282066567  L_2 = Matrix4x4_GetColumn_m786071102(__this, 0, /*hidden argument*/NULL);
		V_1 = L_2;
		Vector4_t4282066567  L_3 = Matrix4x4_GetColumn_m786071102((&V_0), 0, /*hidden argument*/NULL);
		Vector4_t4282066567  L_4 = L_3;
		Il2CppObject * L_5 = Box(Vector4_t4282066567_il2cpp_TypeInfo_var, &L_4);
		bool L_6 = Vector4_Equals_m3270185343((&V_1), L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0096;
		}
	}
	{
		Vector4_t4282066567  L_7 = Matrix4x4_GetColumn_m786071102(__this, 1, /*hidden argument*/NULL);
		V_2 = L_7;
		Vector4_t4282066567  L_8 = Matrix4x4_GetColumn_m786071102((&V_0), 1, /*hidden argument*/NULL);
		Vector4_t4282066567  L_9 = L_8;
		Il2CppObject * L_10 = Box(Vector4_t4282066567_il2cpp_TypeInfo_var, &L_9);
		bool L_11 = Vector4_Equals_m3270185343((&V_2), L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0096;
		}
	}
	{
		Vector4_t4282066567  L_12 = Matrix4x4_GetColumn_m786071102(__this, 2, /*hidden argument*/NULL);
		V_3 = L_12;
		Vector4_t4282066567  L_13 = Matrix4x4_GetColumn_m786071102((&V_0), 2, /*hidden argument*/NULL);
		Vector4_t4282066567  L_14 = L_13;
		Il2CppObject * L_15 = Box(Vector4_t4282066567_il2cpp_TypeInfo_var, &L_14);
		bool L_16 = Vector4_Equals_m3270185343((&V_3), L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0096;
		}
	}
	{
		Vector4_t4282066567  L_17 = Matrix4x4_GetColumn_m786071102(__this, 3, /*hidden argument*/NULL);
		V_4 = L_17;
		Vector4_t4282066567  L_18 = Matrix4x4_GetColumn_m786071102((&V_0), 3, /*hidden argument*/NULL);
		Vector4_t4282066567  L_19 = L_18;
		Il2CppObject * L_20 = Box(Vector4_t4282066567_il2cpp_TypeInfo_var, &L_19);
		bool L_21 = Vector4_Equals_m3270185343((&V_4), L_20, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_21));
		goto IL_0097;
	}

IL_0096:
	{
		G_B7_0 = 0;
	}

IL_0097:
	{
		return (bool)G_B7_0;
	}
}
extern "C"  bool Matrix4x4_Equals_m3534208385_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Matrix4x4_t1651859333 * _thisAdjusted = reinterpret_cast<Matrix4x4_t1651859333 *>(__this + 1);
	return Matrix4x4_Equals_m3534208385(_thisAdjusted, ___other0, method);
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::Inverse(UnityEngine.Matrix4x4)
extern "C"  Matrix4x4_t1651859333  Matrix4x4_Inverse_m1483646919 (Il2CppObject * __this /* static, unused */, Matrix4x4_t1651859333  ___m0, const MethodInfo* method)
{
	Matrix4x4_t1651859333  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Matrix4x4_INTERNAL_CALL_Inverse_m362567117(NULL /*static, unused*/, (&___m0), (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t1651859333  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Matrix4x4::INTERNAL_CALL_Inverse(UnityEngine.Matrix4x4&,UnityEngine.Matrix4x4&)
extern "C"  void Matrix4x4_INTERNAL_CALL_Inverse_m362567117 (Il2CppObject * __this /* static, unused */, Matrix4x4_t1651859333 * ___m0, Matrix4x4_t1651859333 * ___value1, const MethodInfo* method)
{
	typedef void (*Matrix4x4_INTERNAL_CALL_Inverse_m362567117_ftn) (Matrix4x4_t1651859333 *, Matrix4x4_t1651859333 *);
	static Matrix4x4_INTERNAL_CALL_Inverse_m362567117_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_INTERNAL_CALL_Inverse_m362567117_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::INTERNAL_CALL_Inverse(UnityEngine.Matrix4x4&,UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(___m0, ___value1);
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_inverse()
extern "C"  Matrix4x4_t1651859333  Matrix4x4_get_inverse_m2596073482 (Matrix4x4_t1651859333 * __this, const MethodInfo* method)
{
	{
		Matrix4x4_t1651859333  L_0 = Matrix4x4_Inverse_m1483646919(NULL /*static, unused*/, (*(Matrix4x4_t1651859333 *)__this), /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  Matrix4x4_t1651859333  Matrix4x4_get_inverse_m2596073482_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Matrix4x4_t1651859333 * _thisAdjusted = reinterpret_cast<Matrix4x4_t1651859333 *>(__this + 1);
	return Matrix4x4_get_inverse_m2596073482(_thisAdjusted, method);
}
// UnityEngine.Vector4 UnityEngine.Matrix4x4::GetColumn(System.Int32)
extern "C"  Vector4_t4282066567  Matrix4x4_GetColumn_m786071102 (Matrix4x4_t1651859333 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___i0;
		float L_1 = Matrix4x4_get_Item_m2279862332(__this, 0, L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___i0;
		float L_3 = Matrix4x4_get_Item_m2279862332(__this, 1, L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___i0;
		float L_5 = Matrix4x4_get_Item_m2279862332(__this, 2, L_4, /*hidden argument*/NULL);
		int32_t L_6 = ___i0;
		float L_7 = Matrix4x4_get_Item_m2279862332(__this, 3, L_6, /*hidden argument*/NULL);
		Vector4_t4282066567  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector4__ctor_m2441427762(&L_8, L_1, L_3, L_5, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
extern "C"  Vector4_t4282066567  Matrix4x4_GetColumn_m786071102_AdjustorThunk (Il2CppObject * __this, int32_t ___i0, const MethodInfo* method)
{
	Matrix4x4_t1651859333 * _thisAdjusted = reinterpret_cast<Matrix4x4_t1651859333 *>(__this + 1);
	return Matrix4x4_GetColumn_m786071102(_thisAdjusted, ___i0, method);
}
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyPoint3x4(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Matrix4x4_MultiplyPoint3x4_m2198174902 (Matrix4x4_t1651859333 * __this, Vector3_t4282066566  ___v0, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = __this->get_m00_0();
		float L_1 = (&___v0)->get_x_1();
		float L_2 = __this->get_m01_4();
		float L_3 = (&___v0)->get_y_2();
		float L_4 = __this->get_m02_8();
		float L_5 = (&___v0)->get_z_3();
		float L_6 = __this->get_m03_12();
		(&V_0)->set_x_1(((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)L_6)));
		float L_7 = __this->get_m10_1();
		float L_8 = (&___v0)->get_x_1();
		float L_9 = __this->get_m11_5();
		float L_10 = (&___v0)->get_y_2();
		float L_11 = __this->get_m12_9();
		float L_12 = (&___v0)->get_z_3();
		float L_13 = __this->get_m13_13();
		(&V_0)->set_y_2(((float)((float)((float)((float)((float)((float)((float)((float)L_7*(float)L_8))+(float)((float)((float)L_9*(float)L_10))))+(float)((float)((float)L_11*(float)L_12))))+(float)L_13)));
		float L_14 = __this->get_m20_2();
		float L_15 = (&___v0)->get_x_1();
		float L_16 = __this->get_m21_6();
		float L_17 = (&___v0)->get_y_2();
		float L_18 = __this->get_m22_10();
		float L_19 = (&___v0)->get_z_3();
		float L_20 = __this->get_m23_14();
		(&V_0)->set_z_3(((float)((float)((float)((float)((float)((float)((float)((float)L_14*(float)L_15))+(float)((float)((float)L_16*(float)L_17))))+(float)((float)((float)L_18*(float)L_19))))+(float)L_20)));
		Vector3_t4282066566  L_21 = V_0;
		return L_21;
	}
}
extern "C"  Vector3_t4282066566  Matrix4x4_MultiplyPoint3x4_m2198174902_AdjustorThunk (Il2CppObject * __this, Vector3_t4282066566  ___v0, const MethodInfo* method)
{
	Matrix4x4_t1651859333 * _thisAdjusted = reinterpret_cast<Matrix4x4_t1651859333 *>(__this + 1);
	return Matrix4x4_MultiplyPoint3x4_m2198174902(_thisAdjusted, ___v0, method);
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_zero()
extern Il2CppClass* Matrix4x4_t1651859333_il2cpp_TypeInfo_var;
extern const uint32_t Matrix4x4_get_zero_m1808471152_MetadataUsageId;
extern "C"  Matrix4x4_t1651859333  Matrix4x4_get_zero_m1808471152 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_get_zero_m1808471152_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t1651859333  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Matrix4x4_t1651859333_il2cpp_TypeInfo_var, (&V_0));
		(&V_0)->set_m00_0((0.0f));
		(&V_0)->set_m01_4((0.0f));
		(&V_0)->set_m02_8((0.0f));
		(&V_0)->set_m03_12((0.0f));
		(&V_0)->set_m10_1((0.0f));
		(&V_0)->set_m11_5((0.0f));
		(&V_0)->set_m12_9((0.0f));
		(&V_0)->set_m13_13((0.0f));
		(&V_0)->set_m20_2((0.0f));
		(&V_0)->set_m21_6((0.0f));
		(&V_0)->set_m22_10((0.0f));
		(&V_0)->set_m23_14((0.0f));
		(&V_0)->set_m30_3((0.0f));
		(&V_0)->set_m31_7((0.0f));
		(&V_0)->set_m32_11((0.0f));
		(&V_0)->set_m33_15((0.0f));
		Matrix4x4_t1651859333  L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_identity()
extern Il2CppClass* Matrix4x4_t1651859333_il2cpp_TypeInfo_var;
extern const uint32_t Matrix4x4_get_identity_m3946683782_MetadataUsageId;
extern "C"  Matrix4x4_t1651859333  Matrix4x4_get_identity_m3946683782 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_get_identity_m3946683782_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t1651859333  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Matrix4x4_t1651859333_il2cpp_TypeInfo_var, (&V_0));
		(&V_0)->set_m00_0((1.0f));
		(&V_0)->set_m01_4((0.0f));
		(&V_0)->set_m02_8((0.0f));
		(&V_0)->set_m03_12((0.0f));
		(&V_0)->set_m10_1((0.0f));
		(&V_0)->set_m11_5((1.0f));
		(&V_0)->set_m12_9((0.0f));
		(&V_0)->set_m13_13((0.0f));
		(&V_0)->set_m20_2((0.0f));
		(&V_0)->set_m21_6((0.0f));
		(&V_0)->set_m22_10((1.0f));
		(&V_0)->set_m23_14((0.0f));
		(&V_0)->set_m30_3((0.0f));
		(&V_0)->set_m31_7((0.0f));
		(&V_0)->set_m32_11((0.0f));
		(&V_0)->set_m33_15((1.0f));
		Matrix4x4_t1651859333  L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::TRS(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Matrix4x4_t1651859333  Matrix4x4_TRS_m3596398659 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___pos0, Quaternion_t1553702882  ___q1, Vector3_t4282066566  ___s2, const MethodInfo* method)
{
	Matrix4x4_t1651859333  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Matrix4x4_INTERNAL_CALL_TRS_m1769439979(NULL /*static, unused*/, (&___pos0), (&___q1), (&___s2), (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t1651859333  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Matrix4x4::INTERNAL_CALL_TRS(UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&,UnityEngine.Matrix4x4&)
extern "C"  void Matrix4x4_INTERNAL_CALL_TRS_m1769439979 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___pos0, Quaternion_t1553702882 * ___q1, Vector3_t4282066566 * ___s2, Matrix4x4_t1651859333 * ___value3, const MethodInfo* method)
{
	typedef void (*Matrix4x4_INTERNAL_CALL_TRS_m1769439979_ftn) (Vector3_t4282066566 *, Quaternion_t1553702882 *, Vector3_t4282066566 *, Matrix4x4_t1651859333 *);
	static Matrix4x4_INTERNAL_CALL_TRS_m1769439979_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_INTERNAL_CALL_TRS_m1769439979_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::INTERNAL_CALL_TRS(UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&,UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(___pos0, ___q1, ___s2, ___value3);
}
// System.String UnityEngine.Matrix4x4::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4122729682;
extern const uint32_t Matrix4x4_ToString_m294134723_MetadataUsageId;
extern "C"  String_t* Matrix4x4_ToString_m294134723 (Matrix4x4_t1651859333 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_ToString_m294134723_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16)));
		float L_1 = __this->get_m00_0();
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_0;
		float L_5 = __this->get_m01_4();
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t1108656482* L_8 = L_4;
		float L_9 = __this->get_m02_8();
		float L_10 = L_9;
		Il2CppObject * L_11 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t1108656482* L_12 = L_8;
		float L_13 = __this->get_m03_12();
		float L_14 = L_13;
		Il2CppObject * L_15 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		ObjectU5BU5D_t1108656482* L_16 = L_12;
		float L_17 = __this->get_m10_1();
		float L_18 = L_17;
		Il2CppObject * L_19 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 4);
		ArrayElementTypeCheck (L_16, L_19);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_19);
		ObjectU5BU5D_t1108656482* L_20 = L_16;
		float L_21 = __this->get_m11_5();
		float L_22 = L_21;
		Il2CppObject * L_23 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_22);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 5);
		ArrayElementTypeCheck (L_20, L_23);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_23);
		ObjectU5BU5D_t1108656482* L_24 = L_20;
		float L_25 = __this->get_m12_9();
		float L_26 = L_25;
		Il2CppObject * L_27 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 6);
		ArrayElementTypeCheck (L_24, L_27);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)L_27);
		ObjectU5BU5D_t1108656482* L_28 = L_24;
		float L_29 = __this->get_m13_13();
		float L_30 = L_29;
		Il2CppObject * L_31 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_30);
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 7);
		ArrayElementTypeCheck (L_28, L_31);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)L_31);
		ObjectU5BU5D_t1108656482* L_32 = L_28;
		float L_33 = __this->get_m20_2();
		float L_34 = L_33;
		Il2CppObject * L_35 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_34);
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, 8);
		ArrayElementTypeCheck (L_32, L_35);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)L_35);
		ObjectU5BU5D_t1108656482* L_36 = L_32;
		float L_37 = __this->get_m21_6();
		float L_38 = L_37;
		Il2CppObject * L_39 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_38);
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, ((int32_t)9));
		ArrayElementTypeCheck (L_36, L_39);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)L_39);
		ObjectU5BU5D_t1108656482* L_40 = L_36;
		float L_41 = __this->get_m22_10();
		float L_42 = L_41;
		Il2CppObject * L_43 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_42);
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, ((int32_t)10));
		ArrayElementTypeCheck (L_40, L_43);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Il2CppObject *)L_43);
		ObjectU5BU5D_t1108656482* L_44 = L_40;
		float L_45 = __this->get_m23_14();
		float L_46 = L_45;
		Il2CppObject * L_47 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_46);
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)11));
		ArrayElementTypeCheck (L_44, L_47);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (Il2CppObject *)L_47);
		ObjectU5BU5D_t1108656482* L_48 = L_44;
		float L_49 = __this->get_m30_3();
		float L_50 = L_49;
		Il2CppObject * L_51 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_50);
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, ((int32_t)12));
		ArrayElementTypeCheck (L_48, L_51);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (Il2CppObject *)L_51);
		ObjectU5BU5D_t1108656482* L_52 = L_48;
		float L_53 = __this->get_m31_7();
		float L_54 = L_53;
		Il2CppObject * L_55 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_54);
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, ((int32_t)13));
		ArrayElementTypeCheck (L_52, L_55);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (Il2CppObject *)L_55);
		ObjectU5BU5D_t1108656482* L_56 = L_52;
		float L_57 = __this->get_m32_11();
		float L_58 = L_57;
		Il2CppObject * L_59 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_58);
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, ((int32_t)14));
		ArrayElementTypeCheck (L_56, L_59);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (Il2CppObject *)L_59);
		ObjectU5BU5D_t1108656482* L_60 = L_56;
		float L_61 = __this->get_m33_15();
		float L_62 = L_61;
		Il2CppObject * L_63 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_62);
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, ((int32_t)15));
		ArrayElementTypeCheck (L_60, L_63);
		(L_60)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (Il2CppObject *)L_63);
		String_t* L_64 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral4122729682, L_60, /*hidden argument*/NULL);
		return L_64;
	}
}
extern "C"  String_t* Matrix4x4_ToString_m294134723_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Matrix4x4_t1651859333 * _thisAdjusted = reinterpret_cast<Matrix4x4_t1651859333 *>(__this + 1);
	return Matrix4x4_ToString_m294134723(_thisAdjusted, method);
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::op_Multiply(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern Il2CppClass* Matrix4x4_t1651859333_il2cpp_TypeInfo_var;
extern const uint32_t Matrix4x4_op_Multiply_m4108203689_MetadataUsageId;
extern "C"  Matrix4x4_t1651859333  Matrix4x4_op_Multiply_m4108203689 (Il2CppObject * __this /* static, unused */, Matrix4x4_t1651859333  ___lhs0, Matrix4x4_t1651859333  ___rhs1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_op_Multiply_m4108203689_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t1651859333  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Matrix4x4_t1651859333_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = (&___lhs0)->get_m00_0();
		float L_1 = (&___rhs1)->get_m00_0();
		float L_2 = (&___lhs0)->get_m01_4();
		float L_3 = (&___rhs1)->get_m10_1();
		float L_4 = (&___lhs0)->get_m02_8();
		float L_5 = (&___rhs1)->get_m20_2();
		float L_6 = (&___lhs0)->get_m03_12();
		float L_7 = (&___rhs1)->get_m30_3();
		(&V_0)->set_m00_0(((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7)))));
		float L_8 = (&___lhs0)->get_m00_0();
		float L_9 = (&___rhs1)->get_m01_4();
		float L_10 = (&___lhs0)->get_m01_4();
		float L_11 = (&___rhs1)->get_m11_5();
		float L_12 = (&___lhs0)->get_m02_8();
		float L_13 = (&___rhs1)->get_m21_6();
		float L_14 = (&___lhs0)->get_m03_12();
		float L_15 = (&___rhs1)->get_m31_7();
		(&V_0)->set_m01_4(((float)((float)((float)((float)((float)((float)((float)((float)L_8*(float)L_9))+(float)((float)((float)L_10*(float)L_11))))+(float)((float)((float)L_12*(float)L_13))))+(float)((float)((float)L_14*(float)L_15)))));
		float L_16 = (&___lhs0)->get_m00_0();
		float L_17 = (&___rhs1)->get_m02_8();
		float L_18 = (&___lhs0)->get_m01_4();
		float L_19 = (&___rhs1)->get_m12_9();
		float L_20 = (&___lhs0)->get_m02_8();
		float L_21 = (&___rhs1)->get_m22_10();
		float L_22 = (&___lhs0)->get_m03_12();
		float L_23 = (&___rhs1)->get_m32_11();
		(&V_0)->set_m02_8(((float)((float)((float)((float)((float)((float)((float)((float)L_16*(float)L_17))+(float)((float)((float)L_18*(float)L_19))))+(float)((float)((float)L_20*(float)L_21))))+(float)((float)((float)L_22*(float)L_23)))));
		float L_24 = (&___lhs0)->get_m00_0();
		float L_25 = (&___rhs1)->get_m03_12();
		float L_26 = (&___lhs0)->get_m01_4();
		float L_27 = (&___rhs1)->get_m13_13();
		float L_28 = (&___lhs0)->get_m02_8();
		float L_29 = (&___rhs1)->get_m23_14();
		float L_30 = (&___lhs0)->get_m03_12();
		float L_31 = (&___rhs1)->get_m33_15();
		(&V_0)->set_m03_12(((float)((float)((float)((float)((float)((float)((float)((float)L_24*(float)L_25))+(float)((float)((float)L_26*(float)L_27))))+(float)((float)((float)L_28*(float)L_29))))+(float)((float)((float)L_30*(float)L_31)))));
		float L_32 = (&___lhs0)->get_m10_1();
		float L_33 = (&___rhs1)->get_m00_0();
		float L_34 = (&___lhs0)->get_m11_5();
		float L_35 = (&___rhs1)->get_m10_1();
		float L_36 = (&___lhs0)->get_m12_9();
		float L_37 = (&___rhs1)->get_m20_2();
		float L_38 = (&___lhs0)->get_m13_13();
		float L_39 = (&___rhs1)->get_m30_3();
		(&V_0)->set_m10_1(((float)((float)((float)((float)((float)((float)((float)((float)L_32*(float)L_33))+(float)((float)((float)L_34*(float)L_35))))+(float)((float)((float)L_36*(float)L_37))))+(float)((float)((float)L_38*(float)L_39)))));
		float L_40 = (&___lhs0)->get_m10_1();
		float L_41 = (&___rhs1)->get_m01_4();
		float L_42 = (&___lhs0)->get_m11_5();
		float L_43 = (&___rhs1)->get_m11_5();
		float L_44 = (&___lhs0)->get_m12_9();
		float L_45 = (&___rhs1)->get_m21_6();
		float L_46 = (&___lhs0)->get_m13_13();
		float L_47 = (&___rhs1)->get_m31_7();
		(&V_0)->set_m11_5(((float)((float)((float)((float)((float)((float)((float)((float)L_40*(float)L_41))+(float)((float)((float)L_42*(float)L_43))))+(float)((float)((float)L_44*(float)L_45))))+(float)((float)((float)L_46*(float)L_47)))));
		float L_48 = (&___lhs0)->get_m10_1();
		float L_49 = (&___rhs1)->get_m02_8();
		float L_50 = (&___lhs0)->get_m11_5();
		float L_51 = (&___rhs1)->get_m12_9();
		float L_52 = (&___lhs0)->get_m12_9();
		float L_53 = (&___rhs1)->get_m22_10();
		float L_54 = (&___lhs0)->get_m13_13();
		float L_55 = (&___rhs1)->get_m32_11();
		(&V_0)->set_m12_9(((float)((float)((float)((float)((float)((float)((float)((float)L_48*(float)L_49))+(float)((float)((float)L_50*(float)L_51))))+(float)((float)((float)L_52*(float)L_53))))+(float)((float)((float)L_54*(float)L_55)))));
		float L_56 = (&___lhs0)->get_m10_1();
		float L_57 = (&___rhs1)->get_m03_12();
		float L_58 = (&___lhs0)->get_m11_5();
		float L_59 = (&___rhs1)->get_m13_13();
		float L_60 = (&___lhs0)->get_m12_9();
		float L_61 = (&___rhs1)->get_m23_14();
		float L_62 = (&___lhs0)->get_m13_13();
		float L_63 = (&___rhs1)->get_m33_15();
		(&V_0)->set_m13_13(((float)((float)((float)((float)((float)((float)((float)((float)L_56*(float)L_57))+(float)((float)((float)L_58*(float)L_59))))+(float)((float)((float)L_60*(float)L_61))))+(float)((float)((float)L_62*(float)L_63)))));
		float L_64 = (&___lhs0)->get_m20_2();
		float L_65 = (&___rhs1)->get_m00_0();
		float L_66 = (&___lhs0)->get_m21_6();
		float L_67 = (&___rhs1)->get_m10_1();
		float L_68 = (&___lhs0)->get_m22_10();
		float L_69 = (&___rhs1)->get_m20_2();
		float L_70 = (&___lhs0)->get_m23_14();
		float L_71 = (&___rhs1)->get_m30_3();
		(&V_0)->set_m20_2(((float)((float)((float)((float)((float)((float)((float)((float)L_64*(float)L_65))+(float)((float)((float)L_66*(float)L_67))))+(float)((float)((float)L_68*(float)L_69))))+(float)((float)((float)L_70*(float)L_71)))));
		float L_72 = (&___lhs0)->get_m20_2();
		float L_73 = (&___rhs1)->get_m01_4();
		float L_74 = (&___lhs0)->get_m21_6();
		float L_75 = (&___rhs1)->get_m11_5();
		float L_76 = (&___lhs0)->get_m22_10();
		float L_77 = (&___rhs1)->get_m21_6();
		float L_78 = (&___lhs0)->get_m23_14();
		float L_79 = (&___rhs1)->get_m31_7();
		(&V_0)->set_m21_6(((float)((float)((float)((float)((float)((float)((float)((float)L_72*(float)L_73))+(float)((float)((float)L_74*(float)L_75))))+(float)((float)((float)L_76*(float)L_77))))+(float)((float)((float)L_78*(float)L_79)))));
		float L_80 = (&___lhs0)->get_m20_2();
		float L_81 = (&___rhs1)->get_m02_8();
		float L_82 = (&___lhs0)->get_m21_6();
		float L_83 = (&___rhs1)->get_m12_9();
		float L_84 = (&___lhs0)->get_m22_10();
		float L_85 = (&___rhs1)->get_m22_10();
		float L_86 = (&___lhs0)->get_m23_14();
		float L_87 = (&___rhs1)->get_m32_11();
		(&V_0)->set_m22_10(((float)((float)((float)((float)((float)((float)((float)((float)L_80*(float)L_81))+(float)((float)((float)L_82*(float)L_83))))+(float)((float)((float)L_84*(float)L_85))))+(float)((float)((float)L_86*(float)L_87)))));
		float L_88 = (&___lhs0)->get_m20_2();
		float L_89 = (&___rhs1)->get_m03_12();
		float L_90 = (&___lhs0)->get_m21_6();
		float L_91 = (&___rhs1)->get_m13_13();
		float L_92 = (&___lhs0)->get_m22_10();
		float L_93 = (&___rhs1)->get_m23_14();
		float L_94 = (&___lhs0)->get_m23_14();
		float L_95 = (&___rhs1)->get_m33_15();
		(&V_0)->set_m23_14(((float)((float)((float)((float)((float)((float)((float)((float)L_88*(float)L_89))+(float)((float)((float)L_90*(float)L_91))))+(float)((float)((float)L_92*(float)L_93))))+(float)((float)((float)L_94*(float)L_95)))));
		float L_96 = (&___lhs0)->get_m30_3();
		float L_97 = (&___rhs1)->get_m00_0();
		float L_98 = (&___lhs0)->get_m31_7();
		float L_99 = (&___rhs1)->get_m10_1();
		float L_100 = (&___lhs0)->get_m32_11();
		float L_101 = (&___rhs1)->get_m20_2();
		float L_102 = (&___lhs0)->get_m33_15();
		float L_103 = (&___rhs1)->get_m30_3();
		(&V_0)->set_m30_3(((float)((float)((float)((float)((float)((float)((float)((float)L_96*(float)L_97))+(float)((float)((float)L_98*(float)L_99))))+(float)((float)((float)L_100*(float)L_101))))+(float)((float)((float)L_102*(float)L_103)))));
		float L_104 = (&___lhs0)->get_m30_3();
		float L_105 = (&___rhs1)->get_m01_4();
		float L_106 = (&___lhs0)->get_m31_7();
		float L_107 = (&___rhs1)->get_m11_5();
		float L_108 = (&___lhs0)->get_m32_11();
		float L_109 = (&___rhs1)->get_m21_6();
		float L_110 = (&___lhs0)->get_m33_15();
		float L_111 = (&___rhs1)->get_m31_7();
		(&V_0)->set_m31_7(((float)((float)((float)((float)((float)((float)((float)((float)L_104*(float)L_105))+(float)((float)((float)L_106*(float)L_107))))+(float)((float)((float)L_108*(float)L_109))))+(float)((float)((float)L_110*(float)L_111)))));
		float L_112 = (&___lhs0)->get_m30_3();
		float L_113 = (&___rhs1)->get_m02_8();
		float L_114 = (&___lhs0)->get_m31_7();
		float L_115 = (&___rhs1)->get_m12_9();
		float L_116 = (&___lhs0)->get_m32_11();
		float L_117 = (&___rhs1)->get_m22_10();
		float L_118 = (&___lhs0)->get_m33_15();
		float L_119 = (&___rhs1)->get_m32_11();
		(&V_0)->set_m32_11(((float)((float)((float)((float)((float)((float)((float)((float)L_112*(float)L_113))+(float)((float)((float)L_114*(float)L_115))))+(float)((float)((float)L_116*(float)L_117))))+(float)((float)((float)L_118*(float)L_119)))));
		float L_120 = (&___lhs0)->get_m30_3();
		float L_121 = (&___rhs1)->get_m03_12();
		float L_122 = (&___lhs0)->get_m31_7();
		float L_123 = (&___rhs1)->get_m13_13();
		float L_124 = (&___lhs0)->get_m32_11();
		float L_125 = (&___rhs1)->get_m23_14();
		float L_126 = (&___lhs0)->get_m33_15();
		float L_127 = (&___rhs1)->get_m33_15();
		(&V_0)->set_m33_15(((float)((float)((float)((float)((float)((float)((float)((float)L_120*(float)L_121))+(float)((float)((float)L_122*(float)L_123))))+(float)((float)((float)L_124*(float)L_125))))+(float)((float)((float)L_126*(float)L_127)))));
		Matrix4x4_t1651859333  L_128 = V_0;
		return L_128;
	}
}
// UnityEngine.Vector4 UnityEngine.Matrix4x4::op_Multiply(UnityEngine.Matrix4x4,UnityEngine.Vector4)
extern "C"  Vector4_t4282066567  Matrix4x4_op_Multiply_m314399977 (Il2CppObject * __this /* static, unused */, Matrix4x4_t1651859333  ___lhs0, Vector4_t4282066567  ___v1, const MethodInfo* method)
{
	Vector4_t4282066567  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___lhs0)->get_m00_0();
		float L_1 = (&___v1)->get_x_1();
		float L_2 = (&___lhs0)->get_m01_4();
		float L_3 = (&___v1)->get_y_2();
		float L_4 = (&___lhs0)->get_m02_8();
		float L_5 = (&___v1)->get_z_3();
		float L_6 = (&___lhs0)->get_m03_12();
		float L_7 = (&___v1)->get_w_4();
		(&V_0)->set_x_1(((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7)))));
		float L_8 = (&___lhs0)->get_m10_1();
		float L_9 = (&___v1)->get_x_1();
		float L_10 = (&___lhs0)->get_m11_5();
		float L_11 = (&___v1)->get_y_2();
		float L_12 = (&___lhs0)->get_m12_9();
		float L_13 = (&___v1)->get_z_3();
		float L_14 = (&___lhs0)->get_m13_13();
		float L_15 = (&___v1)->get_w_4();
		(&V_0)->set_y_2(((float)((float)((float)((float)((float)((float)((float)((float)L_8*(float)L_9))+(float)((float)((float)L_10*(float)L_11))))+(float)((float)((float)L_12*(float)L_13))))+(float)((float)((float)L_14*(float)L_15)))));
		float L_16 = (&___lhs0)->get_m20_2();
		float L_17 = (&___v1)->get_x_1();
		float L_18 = (&___lhs0)->get_m21_6();
		float L_19 = (&___v1)->get_y_2();
		float L_20 = (&___lhs0)->get_m22_10();
		float L_21 = (&___v1)->get_z_3();
		float L_22 = (&___lhs0)->get_m23_14();
		float L_23 = (&___v1)->get_w_4();
		(&V_0)->set_z_3(((float)((float)((float)((float)((float)((float)((float)((float)L_16*(float)L_17))+(float)((float)((float)L_18*(float)L_19))))+(float)((float)((float)L_20*(float)L_21))))+(float)((float)((float)L_22*(float)L_23)))));
		float L_24 = (&___lhs0)->get_m30_3();
		float L_25 = (&___v1)->get_x_1();
		float L_26 = (&___lhs0)->get_m31_7();
		float L_27 = (&___v1)->get_y_2();
		float L_28 = (&___lhs0)->get_m32_11();
		float L_29 = (&___v1)->get_z_3();
		float L_30 = (&___lhs0)->get_m33_15();
		float L_31 = (&___v1)->get_w_4();
		(&V_0)->set_w_4(((float)((float)((float)((float)((float)((float)((float)((float)L_24*(float)L_25))+(float)((float)((float)L_26*(float)L_27))))+(float)((float)((float)L_28*(float)L_29))))+(float)((float)((float)L_30*(float)L_31)))));
		Vector4_t4282066567  L_32 = V_0;
		return L_32;
	}
}
// Conversion methods for marshalling of: UnityEngine.Matrix4x4
extern "C" void Matrix4x4_t1651859333_marshal_pinvoke(const Matrix4x4_t1651859333& unmarshaled, Matrix4x4_t1651859333_marshaled_pinvoke& marshaled)
{
	marshaled.___m00_0 = unmarshaled.get_m00_0();
	marshaled.___m10_1 = unmarshaled.get_m10_1();
	marshaled.___m20_2 = unmarshaled.get_m20_2();
	marshaled.___m30_3 = unmarshaled.get_m30_3();
	marshaled.___m01_4 = unmarshaled.get_m01_4();
	marshaled.___m11_5 = unmarshaled.get_m11_5();
	marshaled.___m21_6 = unmarshaled.get_m21_6();
	marshaled.___m31_7 = unmarshaled.get_m31_7();
	marshaled.___m02_8 = unmarshaled.get_m02_8();
	marshaled.___m12_9 = unmarshaled.get_m12_9();
	marshaled.___m22_10 = unmarshaled.get_m22_10();
	marshaled.___m32_11 = unmarshaled.get_m32_11();
	marshaled.___m03_12 = unmarshaled.get_m03_12();
	marshaled.___m13_13 = unmarshaled.get_m13_13();
	marshaled.___m23_14 = unmarshaled.get_m23_14();
	marshaled.___m33_15 = unmarshaled.get_m33_15();
}
extern "C" void Matrix4x4_t1651859333_marshal_pinvoke_back(const Matrix4x4_t1651859333_marshaled_pinvoke& marshaled, Matrix4x4_t1651859333& unmarshaled)
{
	float unmarshaled_m00_temp_0 = 0.0f;
	unmarshaled_m00_temp_0 = marshaled.___m00_0;
	unmarshaled.set_m00_0(unmarshaled_m00_temp_0);
	float unmarshaled_m10_temp_1 = 0.0f;
	unmarshaled_m10_temp_1 = marshaled.___m10_1;
	unmarshaled.set_m10_1(unmarshaled_m10_temp_1);
	float unmarshaled_m20_temp_2 = 0.0f;
	unmarshaled_m20_temp_2 = marshaled.___m20_2;
	unmarshaled.set_m20_2(unmarshaled_m20_temp_2);
	float unmarshaled_m30_temp_3 = 0.0f;
	unmarshaled_m30_temp_3 = marshaled.___m30_3;
	unmarshaled.set_m30_3(unmarshaled_m30_temp_3);
	float unmarshaled_m01_temp_4 = 0.0f;
	unmarshaled_m01_temp_4 = marshaled.___m01_4;
	unmarshaled.set_m01_4(unmarshaled_m01_temp_4);
	float unmarshaled_m11_temp_5 = 0.0f;
	unmarshaled_m11_temp_5 = marshaled.___m11_5;
	unmarshaled.set_m11_5(unmarshaled_m11_temp_5);
	float unmarshaled_m21_temp_6 = 0.0f;
	unmarshaled_m21_temp_6 = marshaled.___m21_6;
	unmarshaled.set_m21_6(unmarshaled_m21_temp_6);
	float unmarshaled_m31_temp_7 = 0.0f;
	unmarshaled_m31_temp_7 = marshaled.___m31_7;
	unmarshaled.set_m31_7(unmarshaled_m31_temp_7);
	float unmarshaled_m02_temp_8 = 0.0f;
	unmarshaled_m02_temp_8 = marshaled.___m02_8;
	unmarshaled.set_m02_8(unmarshaled_m02_temp_8);
	float unmarshaled_m12_temp_9 = 0.0f;
	unmarshaled_m12_temp_9 = marshaled.___m12_9;
	unmarshaled.set_m12_9(unmarshaled_m12_temp_9);
	float unmarshaled_m22_temp_10 = 0.0f;
	unmarshaled_m22_temp_10 = marshaled.___m22_10;
	unmarshaled.set_m22_10(unmarshaled_m22_temp_10);
	float unmarshaled_m32_temp_11 = 0.0f;
	unmarshaled_m32_temp_11 = marshaled.___m32_11;
	unmarshaled.set_m32_11(unmarshaled_m32_temp_11);
	float unmarshaled_m03_temp_12 = 0.0f;
	unmarshaled_m03_temp_12 = marshaled.___m03_12;
	unmarshaled.set_m03_12(unmarshaled_m03_temp_12);
	float unmarshaled_m13_temp_13 = 0.0f;
	unmarshaled_m13_temp_13 = marshaled.___m13_13;
	unmarshaled.set_m13_13(unmarshaled_m13_temp_13);
	float unmarshaled_m23_temp_14 = 0.0f;
	unmarshaled_m23_temp_14 = marshaled.___m23_14;
	unmarshaled.set_m23_14(unmarshaled_m23_temp_14);
	float unmarshaled_m33_temp_15 = 0.0f;
	unmarshaled_m33_temp_15 = marshaled.___m33_15;
	unmarshaled.set_m33_15(unmarshaled_m33_temp_15);
}
// Conversion method for clean up from marshalling of: UnityEngine.Matrix4x4
extern "C" void Matrix4x4_t1651859333_marshal_pinvoke_cleanup(Matrix4x4_t1651859333_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Matrix4x4
extern "C" void Matrix4x4_t1651859333_marshal_com(const Matrix4x4_t1651859333& unmarshaled, Matrix4x4_t1651859333_marshaled_com& marshaled)
{
	marshaled.___m00_0 = unmarshaled.get_m00_0();
	marshaled.___m10_1 = unmarshaled.get_m10_1();
	marshaled.___m20_2 = unmarshaled.get_m20_2();
	marshaled.___m30_3 = unmarshaled.get_m30_3();
	marshaled.___m01_4 = unmarshaled.get_m01_4();
	marshaled.___m11_5 = unmarshaled.get_m11_5();
	marshaled.___m21_6 = unmarshaled.get_m21_6();
	marshaled.___m31_7 = unmarshaled.get_m31_7();
	marshaled.___m02_8 = unmarshaled.get_m02_8();
	marshaled.___m12_9 = unmarshaled.get_m12_9();
	marshaled.___m22_10 = unmarshaled.get_m22_10();
	marshaled.___m32_11 = unmarshaled.get_m32_11();
	marshaled.___m03_12 = unmarshaled.get_m03_12();
	marshaled.___m13_13 = unmarshaled.get_m13_13();
	marshaled.___m23_14 = unmarshaled.get_m23_14();
	marshaled.___m33_15 = unmarshaled.get_m33_15();
}
extern "C" void Matrix4x4_t1651859333_marshal_com_back(const Matrix4x4_t1651859333_marshaled_com& marshaled, Matrix4x4_t1651859333& unmarshaled)
{
	float unmarshaled_m00_temp_0 = 0.0f;
	unmarshaled_m00_temp_0 = marshaled.___m00_0;
	unmarshaled.set_m00_0(unmarshaled_m00_temp_0);
	float unmarshaled_m10_temp_1 = 0.0f;
	unmarshaled_m10_temp_1 = marshaled.___m10_1;
	unmarshaled.set_m10_1(unmarshaled_m10_temp_1);
	float unmarshaled_m20_temp_2 = 0.0f;
	unmarshaled_m20_temp_2 = marshaled.___m20_2;
	unmarshaled.set_m20_2(unmarshaled_m20_temp_2);
	float unmarshaled_m30_temp_3 = 0.0f;
	unmarshaled_m30_temp_3 = marshaled.___m30_3;
	unmarshaled.set_m30_3(unmarshaled_m30_temp_3);
	float unmarshaled_m01_temp_4 = 0.0f;
	unmarshaled_m01_temp_4 = marshaled.___m01_4;
	unmarshaled.set_m01_4(unmarshaled_m01_temp_4);
	float unmarshaled_m11_temp_5 = 0.0f;
	unmarshaled_m11_temp_5 = marshaled.___m11_5;
	unmarshaled.set_m11_5(unmarshaled_m11_temp_5);
	float unmarshaled_m21_temp_6 = 0.0f;
	unmarshaled_m21_temp_6 = marshaled.___m21_6;
	unmarshaled.set_m21_6(unmarshaled_m21_temp_6);
	float unmarshaled_m31_temp_7 = 0.0f;
	unmarshaled_m31_temp_7 = marshaled.___m31_7;
	unmarshaled.set_m31_7(unmarshaled_m31_temp_7);
	float unmarshaled_m02_temp_8 = 0.0f;
	unmarshaled_m02_temp_8 = marshaled.___m02_8;
	unmarshaled.set_m02_8(unmarshaled_m02_temp_8);
	float unmarshaled_m12_temp_9 = 0.0f;
	unmarshaled_m12_temp_9 = marshaled.___m12_9;
	unmarshaled.set_m12_9(unmarshaled_m12_temp_9);
	float unmarshaled_m22_temp_10 = 0.0f;
	unmarshaled_m22_temp_10 = marshaled.___m22_10;
	unmarshaled.set_m22_10(unmarshaled_m22_temp_10);
	float unmarshaled_m32_temp_11 = 0.0f;
	unmarshaled_m32_temp_11 = marshaled.___m32_11;
	unmarshaled.set_m32_11(unmarshaled_m32_temp_11);
	float unmarshaled_m03_temp_12 = 0.0f;
	unmarshaled_m03_temp_12 = marshaled.___m03_12;
	unmarshaled.set_m03_12(unmarshaled_m03_temp_12);
	float unmarshaled_m13_temp_13 = 0.0f;
	unmarshaled_m13_temp_13 = marshaled.___m13_13;
	unmarshaled.set_m13_13(unmarshaled_m13_temp_13);
	float unmarshaled_m23_temp_14 = 0.0f;
	unmarshaled_m23_temp_14 = marshaled.___m23_14;
	unmarshaled.set_m23_14(unmarshaled_m23_temp_14);
	float unmarshaled_m33_temp_15 = 0.0f;
	unmarshaled_m33_temp_15 = marshaled.___m33_15;
	unmarshaled.set_m33_15(unmarshaled_m33_temp_15);
}
// Conversion method for clean up from marshalling of: UnityEngine.Matrix4x4
extern "C" void Matrix4x4_t1651859333_marshal_com_cleanup(Matrix4x4_t1651859333_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Mesh::.ctor()
extern "C"  void Mesh__ctor_m2684203808 (Mesh_t4241756145 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m570634126(__this, /*hidden argument*/NULL);
		Mesh_Internal_Create_m3749730360(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::Internal_Create(UnityEngine.Mesh)
extern "C"  void Mesh_Internal_Create_m3749730360 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mono0, const MethodInfo* method)
{
	typedef void (*Mesh_Internal_Create_m3749730360_ftn) (Mesh_t4241756145 *);
	static Mesh_Internal_Create_m3749730360_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_Internal_Create_m3749730360_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::Internal_Create(UnityEngine.Mesh)");
	_il2cpp_icall_func(___mono0);
}
// System.Void UnityEngine.Mesh::Clear(System.Boolean)
extern "C"  void Mesh_Clear_m1789068674 (Mesh_t4241756145 * __this, bool ___keepVertexLayout0, const MethodInfo* method)
{
	typedef void (*Mesh_Clear_m1789068674_ftn) (Mesh_t4241756145 *, bool);
	static Mesh_Clear_m1789068674_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_Clear_m1789068674_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::Clear(System.Boolean)");
	_il2cpp_icall_func(__this, ___keepVertexLayout0);
}
// System.Void UnityEngine.Mesh::Clear()
extern "C"  void Mesh_Clear_m90337099 (Mesh_t4241756145 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)1;
		bool L_0 = V_0;
		Mesh_Clear_m1789068674(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3[] UnityEngine.Mesh::get_vertices()
extern "C"  Vector3U5BU5D_t215400611* Mesh_get_vertices_m3685486174 (Mesh_t4241756145 * __this, const MethodInfo* method)
{
	typedef Vector3U5BU5D_t215400611* (*Mesh_get_vertices_m3685486174_ftn) (Mesh_t4241756145 *);
	static Mesh_get_vertices_m3685486174_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_get_vertices_m3685486174_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::get_vertices()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Mesh::set_vertices(UnityEngine.Vector3[])
extern "C"  void Mesh_set_vertices_m2628866109 (Mesh_t4241756145 * __this, Vector3U5BU5D_t215400611* ___value0, const MethodInfo* method)
{
	typedef void (*Mesh_set_vertices_m2628866109_ftn) (Mesh_t4241756145 *, Vector3U5BU5D_t215400611*);
	static Mesh_set_vertices_m2628866109_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_set_vertices_m2628866109_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::set_vertices(UnityEngine.Vector3[])");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Mesh::SetVertices(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C"  void Mesh_SetVertices_m701834806 (Mesh_t4241756145 * __this, List_1_t1355284822 * ___inVertices0, const MethodInfo* method)
{
	{
		List_1_t1355284822 * L_0 = ___inVertices0;
		Mesh_SetVerticesInternal_m1274639230(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetVerticesInternal(System.Object)
extern "C"  void Mesh_SetVerticesInternal_m1274639230 (Mesh_t4241756145 * __this, Il2CppObject * ___vertices0, const MethodInfo* method)
{
	typedef void (*Mesh_SetVerticesInternal_m1274639230_ftn) (Mesh_t4241756145 *, Il2CppObject *);
	static Mesh_SetVerticesInternal_m1274639230_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_SetVerticesInternal_m1274639230_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::SetVerticesInternal(System.Object)");
	_il2cpp_icall_func(__this, ___vertices0);
}
// UnityEngine.Vector3[] UnityEngine.Mesh::get_normals()
extern "C"  Vector3U5BU5D_t215400611* Mesh_get_normals_m3396909641 (Mesh_t4241756145 * __this, const MethodInfo* method)
{
	typedef Vector3U5BU5D_t215400611* (*Mesh_get_normals_m3396909641_ftn) (Mesh_t4241756145 *);
	static Mesh_get_normals_m3396909641_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_get_normals_m3396909641_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::get_normals()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Mesh::set_normals(UnityEngine.Vector3[])
extern "C"  void Mesh_set_normals_m3763698282 (Mesh_t4241756145 * __this, Vector3U5BU5D_t215400611* ___value0, const MethodInfo* method)
{
	typedef void (*Mesh_set_normals_m3763698282_ftn) (Mesh_t4241756145 *, Vector3U5BU5D_t215400611*);
	static Mesh_set_normals_m3763698282_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_set_normals_m3763698282_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::set_normals(UnityEngine.Vector3[])");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Mesh::SetNormals(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C"  void Mesh_SetNormals_m2039144779 (Mesh_t4241756145 * __this, List_1_t1355284822 * ___inNormals0, const MethodInfo* method)
{
	{
		List_1_t1355284822 * L_0 = ___inNormals0;
		Mesh_SetNormalsInternal_m1710845385(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetNormalsInternal(System.Object)
extern "C"  void Mesh_SetNormalsInternal_m1710845385 (Mesh_t4241756145 * __this, Il2CppObject * ___normals0, const MethodInfo* method)
{
	typedef void (*Mesh_SetNormalsInternal_m1710845385_ftn) (Mesh_t4241756145 *, Il2CppObject *);
	static Mesh_SetNormalsInternal_m1710845385_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_SetNormalsInternal_m1710845385_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::SetNormalsInternal(System.Object)");
	_il2cpp_icall_func(__this, ___normals0);
}
// UnityEngine.Vector4[] UnityEngine.Mesh::get_tangents()
extern "C"  Vector4U5BU5D_t701588350* Mesh_get_tangents_m3235865682 (Mesh_t4241756145 * __this, const MethodInfo* method)
{
	typedef Vector4U5BU5D_t701588350* (*Mesh_get_tangents_m3235865682_ftn) (Mesh_t4241756145 *);
	static Mesh_get_tangents_m3235865682_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_get_tangents_m3235865682_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::get_tangents()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Mesh::SetTangents(System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern "C"  void Mesh_SetTangents_m2005345740 (Mesh_t4241756145 * __this, List_1_t1355284823 * ___inTangents0, const MethodInfo* method)
{
	{
		List_1_t1355284823 * L_0 = ___inTangents0;
		Mesh_SetTangentsInternal_m763303177(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetTangentsInternal(System.Object)
extern "C"  void Mesh_SetTangentsInternal_m763303177 (Mesh_t4241756145 * __this, Il2CppObject * ___tangents0, const MethodInfo* method)
{
	typedef void (*Mesh_SetTangentsInternal_m763303177_ftn) (Mesh_t4241756145 *, Il2CppObject *);
	static Mesh_SetTangentsInternal_m763303177_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_SetTangentsInternal_m763303177_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::SetTangentsInternal(System.Object)");
	_il2cpp_icall_func(__this, ___tangents0);
}
// UnityEngine.Vector2[] UnityEngine.Mesh::get_uv()
extern "C"  Vector2U5BU5D_t4024180168* Mesh_get_uv_m558008935 (Mesh_t4241756145 * __this, const MethodInfo* method)
{
	typedef Vector2U5BU5D_t4024180168* (*Mesh_get_uv_m558008935_ftn) (Mesh_t4241756145 *);
	static Mesh_get_uv_m558008935_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_get_uv_m558008935_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::get_uv()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Mesh::set_uv(UnityEngine.Vector2[])
extern "C"  void Mesh_set_uv_m498907190 (Mesh_t4241756145 * __this, Vector2U5BU5D_t4024180168* ___value0, const MethodInfo* method)
{
	typedef void (*Mesh_set_uv_m498907190_ftn) (Mesh_t4241756145 *, Vector2U5BU5D_t4024180168*);
	static Mesh_set_uv_m498907190_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_set_uv_m498907190_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::set_uv(UnityEngine.Vector2[])");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2[] UnityEngine.Mesh::get_uv2()
extern "C"  Vector2U5BU5D_t4024180168* Mesh_get_uv2_m118417421 (Mesh_t4241756145 * __this, const MethodInfo* method)
{
	typedef Vector2U5BU5D_t4024180168* (*Mesh_get_uv2_m118417421_ftn) (Mesh_t4241756145 *);
	static Mesh_get_uv2_m118417421_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_get_uv2_m118417421_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::get_uv2()");
	return _il2cpp_icall_func(__this);
}
// System.Array UnityEngine.Mesh::ExtractListData(System.Object)
extern "C"  Il2CppArray * Mesh_ExtractListData_m2453560898 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___list0, const MethodInfo* method)
{
	typedef Il2CppArray * (*Mesh_ExtractListData_m2453560898_ftn) (Il2CppObject *);
	static Mesh_ExtractListData_m2453560898_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_ExtractListData_m2453560898_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::ExtractListData(System.Object)");
	return _il2cpp_icall_func(___list0);
}
// System.Void UnityEngine.Mesh::SetUVsInternal(System.Array,System.Int32,System.Int32,System.Int32)
extern "C"  void Mesh_SetUVsInternal_m4014622176 (Mesh_t4241756145 * __this, Il2CppArray * ___uvs0, int32_t ___channel1, int32_t ___dim2, int32_t ___arraySize3, const MethodInfo* method)
{
	typedef void (*Mesh_SetUVsInternal_m4014622176_ftn) (Mesh_t4241756145 *, Il2CppArray *, int32_t, int32_t, int32_t);
	static Mesh_SetUVsInternal_m4014622176_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_SetUVsInternal_m4014622176_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::SetUVsInternal(System.Array,System.Int32,System.Int32,System.Int32)");
	_il2cpp_icall_func(__this, ___uvs0, ___channel1, ___dim2, ___arraySize3);
}
// System.Void UnityEngine.Mesh::SetUVs(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector2>)
extern const MethodInfo* List_1_get_Count_m1814928708_MethodInfo_var;
extern const uint32_t Mesh_SetUVs_m116216925_MetadataUsageId;
extern "C"  void Mesh_SetUVs_m116216925 (Mesh_t4241756145 * __this, int32_t ___channel0, List_1_t1355284821 * ___uvs1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mesh_SetUVs_m116216925_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1355284821 * L_0 = ___uvs1;
		Il2CppArray * L_1 = Mesh_ExtractListData_m2453560898(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___channel0;
		List_1_t1355284821 * L_3 = ___uvs1;
		NullCheck(L_3);
		int32_t L_4 = List_1_get_Count_m1814928708(L_3, /*hidden argument*/List_1_get_Count_m1814928708_MethodInfo_var);
		Mesh_SetUVsInternal_m4014622176(__this, L_1, L_2, 2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Bounds UnityEngine.Mesh::get_bounds()
extern "C"  Bounds_t2711641849  Mesh_get_bounds_m1625335237 (Mesh_t4241756145 * __this, const MethodInfo* method)
{
	Bounds_t2711641849  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Mesh_INTERNAL_get_bounds_m739771994(__this, (&V_0), /*hidden argument*/NULL);
		Bounds_t2711641849  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Mesh::INTERNAL_get_bounds(UnityEngine.Bounds&)
extern "C"  void Mesh_INTERNAL_get_bounds_m739771994 (Mesh_t4241756145 * __this, Bounds_t2711641849 * ___value0, const MethodInfo* method)
{
	typedef void (*Mesh_INTERNAL_get_bounds_m739771994_ftn) (Mesh_t4241756145 *, Bounds_t2711641849 *);
	static Mesh_INTERNAL_get_bounds_m739771994_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_INTERNAL_get_bounds_m739771994_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::INTERNAL_get_bounds(UnityEngine.Bounds&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Color32[] UnityEngine.Mesh::get_colors32()
extern "C"  Color32U5BU5D_t2960766953* Mesh_get_colors32_m192356802 (Mesh_t4241756145 * __this, const MethodInfo* method)
{
	typedef Color32U5BU5D_t2960766953* (*Mesh_get_colors32_m192356802_ftn) (Mesh_t4241756145 *);
	static Mesh_get_colors32_m192356802_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_get_colors32_m192356802_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::get_colors32()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Mesh::SetColors(System.Collections.Generic.List`1<UnityEngine.Color32>)
extern "C"  void Mesh_SetColors_m3313707935 (Mesh_t4241756145 * __this, List_1_t1967039240 * ___inColors0, const MethodInfo* method)
{
	{
		List_1_t1967039240 * L_0 = ___inColors0;
		Mesh_SetColors32Internal_m1830241000(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetColors32Internal(System.Object)
extern "C"  void Mesh_SetColors32Internal_m1830241000 (Mesh_t4241756145 * __this, Il2CppObject * ___colors0, const MethodInfo* method)
{
	typedef void (*Mesh_SetColors32Internal_m1830241000_ftn) (Mesh_t4241756145 *, Il2CppObject *);
	static Mesh_SetColors32Internal_m1830241000_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_SetColors32Internal_m1830241000_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::SetColors32Internal(System.Object)");
	_il2cpp_icall_func(__this, ___colors0);
}
// System.Void UnityEngine.Mesh::RecalculateBounds()
extern "C"  void Mesh_RecalculateBounds_m3754336742 (Mesh_t4241756145 * __this, const MethodInfo* method)
{
	typedef void (*Mesh_RecalculateBounds_m3754336742_ftn) (Mesh_t4241756145 *);
	static Mesh_RecalculateBounds_m3754336742_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_RecalculateBounds_m3754336742_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::RecalculateBounds()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Mesh::RecalculateNormals()
extern "C"  void Mesh_RecalculateNormals_m1806625021 (Mesh_t4241756145 * __this, const MethodInfo* method)
{
	typedef void (*Mesh_RecalculateNormals_m1806625021_ftn) (Mesh_t4241756145 *);
	static Mesh_RecalculateNormals_m1806625021_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_RecalculateNormals_m1806625021_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::RecalculateNormals()");
	_il2cpp_icall_func(__this);
}
// System.Int32[] UnityEngine.Mesh::get_triangles()
extern "C"  Int32U5BU5D_t3230847821* Mesh_get_triangles_m2145908418 (Mesh_t4241756145 * __this, const MethodInfo* method)
{
	typedef Int32U5BU5D_t3230847821* (*Mesh_get_triangles_m2145908418_ftn) (Mesh_t4241756145 *);
	static Mesh_get_triangles_m2145908418_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_get_triangles_m2145908418_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::get_triangles()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Mesh::set_triangles(System.Int32[])
extern "C"  void Mesh_set_triangles_m2341339867 (Mesh_t4241756145 * __this, Int32U5BU5D_t3230847821* ___value0, const MethodInfo* method)
{
	typedef void (*Mesh_set_triangles_m2341339867_ftn) (Mesh_t4241756145 *, Int32U5BU5D_t3230847821*);
	static Mesh_set_triangles_m2341339867_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_set_triangles_m2341339867_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::set_triangles(System.Int32[])");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Mesh::SetTriangles(System.Collections.Generic.List`1<System.Int32>,System.Int32)
extern "C"  void Mesh_SetTriangles_m456382467 (Mesh_t4241756145 * __this, List_1_t2522024052 * ___inTriangles0, int32_t ___submesh1, const MethodInfo* method)
{
	{
		List_1_t2522024052 * L_0 = ___inTriangles0;
		int32_t L_1 = ___submesh1;
		Mesh_SetTrianglesInternal_m2955775213(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetTrianglesInternal(System.Object,System.Int32)
extern "C"  void Mesh_SetTrianglesInternal_m2955775213 (Mesh_t4241756145 * __this, Il2CppObject * ___triangles0, int32_t ___submesh1, const MethodInfo* method)
{
	typedef void (*Mesh_SetTrianglesInternal_m2955775213_ftn) (Mesh_t4241756145 *, Il2CppObject *, int32_t);
	static Mesh_SetTrianglesInternal_m2955775213_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_SetTrianglesInternal_m2955775213_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::SetTrianglesInternal(System.Object,System.Int32)");
	_il2cpp_icall_func(__this, ___triangles0, ___submesh1);
}
// System.Int32[] UnityEngine.Mesh::GetIndices(System.Int32)
extern "C"  Int32U5BU5D_t3230847821* Mesh_GetIndices_m637494532 (Mesh_t4241756145 * __this, int32_t ___submesh0, const MethodInfo* method)
{
	typedef Int32U5BU5D_t3230847821* (*Mesh_GetIndices_m637494532_ftn) (Mesh_t4241756145 *, int32_t);
	static Mesh_GetIndices_m637494532_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_GetIndices_m637494532_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::GetIndices(System.Int32)");
	return _il2cpp_icall_func(__this, ___submesh0);
}
// System.Int32 UnityEngine.Mesh::get_vertexCount()
extern "C"  int32_t Mesh_get_vertexCount_m538235264 (Mesh_t4241756145 * __this, const MethodInfo* method)
{
	typedef int32_t (*Mesh_get_vertexCount_m538235264_ftn) (Mesh_t4241756145 *);
	static Mesh_get_vertexCount_m538235264_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_get_vertexCount_m538235264_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::get_vertexCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MeshCollider::set_sharedMesh(UnityEngine.Mesh)
extern "C"  void MeshCollider_set_sharedMesh_m3062685539 (MeshCollider_t2667653125 * __this, Mesh_t4241756145 * ___value0, const MethodInfo* method)
{
	typedef void (*MeshCollider_set_sharedMesh_m3062685539_ftn) (MeshCollider_t2667653125 *, Mesh_t4241756145 *);
	static MeshCollider_set_sharedMesh_m3062685539_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MeshCollider_set_sharedMesh_m3062685539_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MeshCollider::set_sharedMesh(UnityEngine.Mesh)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Mesh UnityEngine.MeshFilter::get_mesh()
extern "C"  Mesh_t4241756145 * MeshFilter_get_mesh_m484001117 (MeshFilter_t3839065225 * __this, const MethodInfo* method)
{
	typedef Mesh_t4241756145 * (*MeshFilter_get_mesh_m484001117_ftn) (MeshFilter_t3839065225 *);
	static MeshFilter_get_mesh_m484001117_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MeshFilter_get_mesh_m484001117_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MeshFilter::get_mesh()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MeshFilter::set_mesh(UnityEngine.Mesh)
extern "C"  void MeshFilter_set_mesh_m1404580322 (MeshFilter_t3839065225 * __this, Mesh_t4241756145 * ___value0, const MethodInfo* method)
{
	typedef void (*MeshFilter_set_mesh_m1404580322_ftn) (MeshFilter_t3839065225 *, Mesh_t4241756145 *);
	static MeshFilter_set_mesh_m1404580322_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MeshFilter_set_mesh_m1404580322_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MeshFilter::set_mesh(UnityEngine.Mesh)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Mesh UnityEngine.MeshFilter::get_sharedMesh()
extern "C"  Mesh_t4241756145 * MeshFilter_get_sharedMesh_m2700148450 (MeshFilter_t3839065225 * __this, const MethodInfo* method)
{
	typedef Mesh_t4241756145 * (*MeshFilter_get_sharedMesh_m2700148450_ftn) (MeshFilter_t3839065225 *);
	static MeshFilter_get_sharedMesh_m2700148450_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MeshFilter_get_sharedMesh_m2700148450_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MeshFilter::get_sharedMesh()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MeshFilter::set_sharedMesh(UnityEngine.Mesh)
extern "C"  void MeshFilter_set_sharedMesh_m793190055 (MeshFilter_t3839065225 * __this, Mesh_t4241756145 * ___value0, const MethodInfo* method)
{
	typedef void (*MeshFilter_set_sharedMesh_m793190055_ftn) (MeshFilter_t3839065225 *, Mesh_t4241756145 *);
	static MeshFilter_set_sharedMesh_m793190055_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MeshFilter_set_sharedMesh_m793190055_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MeshFilter::set_sharedMesh(UnityEngine.Mesh)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2022291967 (MonoBehaviour_t667441552 * __this, const MethodInfo* method)
{
	{
		Behaviour__ctor_m1624944828(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::Internal_CancelInvokeAll()
extern "C"  void MonoBehaviour_Internal_CancelInvokeAll_m972795186 (MonoBehaviour_t667441552 * __this, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_Internal_CancelInvokeAll_m972795186_ftn) (MonoBehaviour_t667441552 *);
	static MonoBehaviour_Internal_CancelInvokeAll_m972795186_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_Internal_CancelInvokeAll_m972795186_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::Internal_CancelInvokeAll()");
	_il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.MonoBehaviour::Internal_IsInvokingAll()
extern "C"  bool MonoBehaviour_Internal_IsInvokingAll_m3154030143 (MonoBehaviour_t667441552 * __this, const MethodInfo* method)
{
	typedef bool (*MonoBehaviour_Internal_IsInvokingAll_m3154030143_ftn) (MonoBehaviour_t667441552 *);
	static MonoBehaviour_Internal_IsInvokingAll_m3154030143_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_Internal_IsInvokingAll_m3154030143_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::Internal_IsInvokingAll()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)
extern "C"  void MonoBehaviour_Invoke_m2825545578 (MonoBehaviour_t667441552 * __this, String_t* ___methodName0, float ___time1, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_Invoke_m2825545578_ftn) (MonoBehaviour_t667441552 *, String_t*, float);
	static MonoBehaviour_Invoke_m2825545578_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_Invoke_m2825545578_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)");
	_il2cpp_icall_func(__this, ___methodName0, ___time1);
}
// System.Void UnityEngine.MonoBehaviour::InvokeRepeating(System.String,System.Single,System.Single)
extern "C"  void MonoBehaviour_InvokeRepeating_m1115468640 (MonoBehaviour_t667441552 * __this, String_t* ___methodName0, float ___time1, float ___repeatRate2, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_InvokeRepeating_m1115468640_ftn) (MonoBehaviour_t667441552 *, String_t*, float, float);
	static MonoBehaviour_InvokeRepeating_m1115468640_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_InvokeRepeating_m1115468640_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::InvokeRepeating(System.String,System.Single,System.Single)");
	_il2cpp_icall_func(__this, ___methodName0, ___time1, ___repeatRate2);
}
// System.Void UnityEngine.MonoBehaviour::CancelInvoke()
extern "C"  void MonoBehaviour_CancelInvoke_m3230208631 (MonoBehaviour_t667441552 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour_Internal_CancelInvokeAll_m972795186(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::CancelInvoke(System.String)
extern "C"  void MonoBehaviour_CancelInvoke_m2461959659 (MonoBehaviour_t667441552 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_CancelInvoke_m2461959659_ftn) (MonoBehaviour_t667441552 *, String_t*);
	static MonoBehaviour_CancelInvoke_m2461959659_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_CancelInvoke_m2461959659_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::CancelInvoke(System.String)");
	_il2cpp_icall_func(__this, ___methodName0);
}
// System.Boolean UnityEngine.MonoBehaviour::IsInvoking(System.String)
extern "C"  bool MonoBehaviour_IsInvoking_m1460913732 (MonoBehaviour_t667441552 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	typedef bool (*MonoBehaviour_IsInvoking_m1460913732_ftn) (MonoBehaviour_t667441552 *, String_t*);
	static MonoBehaviour_IsInvoking_m1460913732_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_IsInvoking_m1460913732_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::IsInvoking(System.String)");
	return _il2cpp_icall_func(__this, ___methodName0);
}
// System.Boolean UnityEngine.MonoBehaviour::IsInvoking()
extern "C"  bool MonoBehaviour_IsInvoking_m3881121150 (MonoBehaviour_t667441552 * __this, const MethodInfo* method)
{
	{
		bool L_0 = MonoBehaviour_Internal_IsInvokingAll_m3154030143(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t3621161934 * MonoBehaviour_StartCoroutine_m2135303124 (MonoBehaviour_t667441552 * __this, Il2CppObject * ___routine0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___routine0;
		Coroutine_t3621161934 * L_1 = MonoBehaviour_StartCoroutine_Auto_m1831125106(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine_Auto(System.Collections.IEnumerator)
extern "C"  Coroutine_t3621161934 * MonoBehaviour_StartCoroutine_Auto_m1831125106 (MonoBehaviour_t667441552 * __this, Il2CppObject * ___routine0, const MethodInfo* method)
{
	typedef Coroutine_t3621161934 * (*MonoBehaviour_StartCoroutine_Auto_m1831125106_ftn) (MonoBehaviour_t667441552 *, Il2CppObject *);
	static MonoBehaviour_StartCoroutine_Auto_m1831125106_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StartCoroutine_Auto_m1831125106_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StartCoroutine_Auto(System.Collections.IEnumerator)");
	return _il2cpp_icall_func(__this, ___routine0);
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String,System.Object)
extern "C"  Coroutine_t3621161934 * MonoBehaviour_StartCoroutine_m2964903975 (MonoBehaviour_t667441552 * __this, String_t* ___methodName0, Il2CppObject * ___value1, const MethodInfo* method)
{
	typedef Coroutine_t3621161934 * (*MonoBehaviour_StartCoroutine_m2964903975_ftn) (MonoBehaviour_t667441552 *, String_t*, Il2CppObject *);
	static MonoBehaviour_StartCoroutine_m2964903975_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StartCoroutine_m2964903975_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StartCoroutine(System.String,System.Object)");
	return _il2cpp_icall_func(__this, ___methodName0, ___value1);
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String)
extern "C"  Coroutine_t3621161934 * MonoBehaviour_StartCoroutine_m2272783641 (MonoBehaviour_t667441552 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		V_0 = NULL;
		String_t* L_0 = ___methodName0;
		Il2CppObject * L_1 = V_0;
		Coroutine_t3621161934 * L_2 = MonoBehaviour_StartCoroutine_m2964903975(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.String)
extern "C"  void MonoBehaviour_StopCoroutine_m2790918991 (MonoBehaviour_t667441552 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopCoroutine_m2790918991_ftn) (MonoBehaviour_t667441552 *, String_t*);
	static MonoBehaviour_StopCoroutine_m2790918991_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutine_m2790918991_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutine(System.String)");
	_il2cpp_icall_func(__this, ___methodName0);
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.Collections.IEnumerator)
extern "C"  void MonoBehaviour_StopCoroutine_m1340700766 (MonoBehaviour_t667441552 * __this, Il2CppObject * ___routine0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___routine0;
		MonoBehaviour_StopCoroutineViaEnumerator_Auto_m2181342074(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(UnityEngine.Coroutine)
extern "C"  void MonoBehaviour_StopCoroutine_m1762309278 (MonoBehaviour_t667441552 * __this, Coroutine_t3621161934 * ___routine0, const MethodInfo* method)
{
	{
		Coroutine_t3621161934 * L_0 = ___routine0;
		MonoBehaviour_StopCoroutine_Auto_m1074098068(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)
extern "C"  void MonoBehaviour_StopCoroutineViaEnumerator_Auto_m2181342074 (MonoBehaviour_t667441552 * __this, Il2CppObject * ___routine0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopCoroutineViaEnumerator_Auto_m2181342074_ftn) (MonoBehaviour_t667441552 *, Il2CppObject *);
	static MonoBehaviour_StopCoroutineViaEnumerator_Auto_m2181342074_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutineViaEnumerator_Auto_m2181342074_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)");
	_il2cpp_icall_func(__this, ___routine0);
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)
extern "C"  void MonoBehaviour_StopCoroutine_Auto_m1074098068 (MonoBehaviour_t667441552 * __this, Coroutine_t3621161934 * ___routine0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopCoroutine_Auto_m1074098068_ftn) (MonoBehaviour_t667441552 *, Coroutine_t3621161934 *);
	static MonoBehaviour_StopCoroutine_Auto_m1074098068_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutine_Auto_m1074098068_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)");
	_il2cpp_icall_func(__this, ___routine0);
}
// System.Void UnityEngine.MonoBehaviour::StopAllCoroutines()
extern "C"  void MonoBehaviour_StopAllCoroutines_m1437893335 (MonoBehaviour_t667441552 * __this, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopAllCoroutines_m1437893335_ftn) (MonoBehaviour_t667441552 *);
	static MonoBehaviour_StopAllCoroutines_m1437893335_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopAllCoroutines_m1437893335_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopAllCoroutines()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MonoBehaviour::print(System.Object)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const uint32_t MonoBehaviour_print_m1497342762_MetadataUsageId;
extern "C"  void MonoBehaviour_print_m1497342762 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoBehaviour_print_m1497342762_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.MonoBehaviour::get_useGUILayout()
extern "C"  bool MonoBehaviour_get_useGUILayout_m4058409766 (MonoBehaviour_t667441552 * __this, const MethodInfo* method)
{
	typedef bool (*MonoBehaviour_get_useGUILayout_m4058409766_ftn) (MonoBehaviour_t667441552 *);
	static MonoBehaviour_get_useGUILayout_m4058409766_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_get_useGUILayout_m4058409766_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::get_useGUILayout()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MonoBehaviour::set_useGUILayout(System.Boolean)
extern "C"  void MonoBehaviour_set_useGUILayout_m589898551 (MonoBehaviour_t667441552 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_set_useGUILayout_m589898551_ftn) (MonoBehaviour_t667441552 *, bool);
	static MonoBehaviour_set_useGUILayout_m589898551_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_set_useGUILayout_m589898551_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::set_useGUILayout(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Motion::.ctor()
extern "C"  void Motion__ctor_m2540972279 (Motion_t3026528250 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m570634126(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.NavMeshAgent::set_velocity(UnityEngine.Vector3)
extern "C"  void NavMeshAgent_set_velocity_m2183624243 (NavMeshAgent_t588466745 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		NavMeshAgent_INTERNAL_set_velocity_m2849407365(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.NavMeshAgent::INTERNAL_set_velocity(UnityEngine.Vector3&)
extern "C"  void NavMeshAgent_INTERNAL_set_velocity_m2849407365 (NavMeshAgent_t588466745 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*NavMeshAgent_INTERNAL_set_velocity_m2849407365_ftn) (NavMeshAgent_t588466745 *, Vector3_t4282066566 *);
	static NavMeshAgent_INTERNAL_set_velocity_m2849407365_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NavMeshAgent_INTERNAL_set_velocity_m2849407365_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NavMeshAgent::INTERNAL_set_velocity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.NetworkConnectionError UnityEngine.Network::InitializeServer(System.Int32,System.Int32,System.Boolean)
extern "C"  int32_t Network_InitializeServer_m1086070119 (Il2CppObject * __this /* static, unused */, int32_t ___connections0, int32_t ___listenPort1, bool ___useNat2, const MethodInfo* method)
{
	typedef int32_t (*Network_InitializeServer_m1086070119_ftn) (int32_t, int32_t, bool);
	static Network_InitializeServer_m1086070119_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_InitializeServer_m1086070119_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::InitializeServer(System.Int32,System.Int32,System.Boolean)");
	return _il2cpp_icall_func(___connections0, ___listenPort1, ___useNat2);
}
// System.Void UnityEngine.Network::set_incomingPassword(System.String)
extern "C"  void Network_set_incomingPassword_m515962091 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	typedef void (*Network_set_incomingPassword_m515962091_ftn) (String_t*);
	static Network_set_incomingPassword_m515962091_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_set_incomingPassword_m515962091_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::set_incomingPassword(System.String)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.Network::set_logLevel(UnityEngine.NetworkLogLevel)
extern "C"  void Network_set_logLevel_m294590405 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Network_set_logLevel_m294590405_ftn) (int32_t);
	static Network_set_logLevel_m294590405_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_set_logLevel_m294590405_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::set_logLevel(UnityEngine.NetworkLogLevel)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.Network::InitializeSecurity()
extern "C"  void Network_InitializeSecurity_m64123113 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*Network_InitializeSecurity_m64123113_ftn) ();
	static Network_InitializeSecurity_m64123113_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_InitializeSecurity_m64123113_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::InitializeSecurity()");
	_il2cpp_icall_func();
}
// UnityEngine.NetworkConnectionError UnityEngine.Network::Internal_ConnectToSingleIP(System.String,System.Int32,System.Int32,System.String)
extern "C"  int32_t Network_Internal_ConnectToSingleIP_m3562152815 (Il2CppObject * __this /* static, unused */, String_t* ___IP0, int32_t ___remotePort1, int32_t ___localPort2, String_t* ___password3, const MethodInfo* method)
{
	typedef int32_t (*Network_Internal_ConnectToSingleIP_m3562152815_ftn) (String_t*, int32_t, int32_t, String_t*);
	static Network_Internal_ConnectToSingleIP_m3562152815_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_Internal_ConnectToSingleIP_m3562152815_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::Internal_ConnectToSingleIP(System.String,System.Int32,System.Int32,System.String)");
	return _il2cpp_icall_func(___IP0, ___remotePort1, ___localPort2, ___password3);
}
// UnityEngine.NetworkConnectionError UnityEngine.Network::Connect(System.String,System.Int32,System.String)
extern "C"  int32_t Network_Connect_m3659141856 (Il2CppObject * __this /* static, unused */, String_t* ___IP0, int32_t ___remotePort1, String_t* ___password2, const MethodInfo* method)
{
	{
		String_t* L_0 = ___IP0;
		int32_t L_1 = ___remotePort1;
		String_t* L_2 = ___password2;
		int32_t L_3 = Network_Internal_ConnectToSingleIP_m3562152815(NULL /*static, unused*/, L_0, L_1, 0, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void UnityEngine.Network::Disconnect(System.Int32)
extern "C"  void Network_Disconnect_m2062052710 (Il2CppObject * __this /* static, unused */, int32_t ___timeout0, const MethodInfo* method)
{
	typedef void (*Network_Disconnect_m2062052710_ftn) (int32_t);
	static Network_Disconnect_m2062052710_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_Disconnect_m2062052710_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::Disconnect(System.Int32)");
	_il2cpp_icall_func(___timeout0);
}
// System.Void UnityEngine.Network::Disconnect()
extern "C"  void Network_Disconnect_m2068213653 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = ((int32_t)200);
		int32_t L_0 = V_0;
		Network_Disconnect_m2062052710(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Network::CloseConnection(UnityEngine.NetworkPlayer,System.Boolean)
extern "C"  void Network_CloseConnection_m1870717282 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t3231273765  ___target0, bool ___sendDisconnectionNotification1, const MethodInfo* method)
{
	typedef void (*Network_CloseConnection_m1870717282_ftn) (NetworkPlayer_t3231273765 , bool);
	static Network_CloseConnection_m1870717282_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_CloseConnection_m1870717282_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::CloseConnection(UnityEngine.NetworkPlayer,System.Boolean)");
	_il2cpp_icall_func(___target0, ___sendDisconnectionNotification1);
}
// UnityEngine.NetworkPlayer[] UnityEngine.Network::get_connections()
extern "C"  NetworkPlayerU5BU5D_t132745896* Network_get_connections_m4276471278 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef NetworkPlayerU5BU5D_t132745896* (*Network_get_connections_m4276471278_ftn) ();
	static Network_get_connections_m4276471278_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_get_connections_m4276471278_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::get_connections()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Network::Internal_GetPlayer()
extern "C"  int32_t Network_Internal_GetPlayer_m776499950 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Network_Internal_GetPlayer_m776499950_ftn) ();
	static Network_Internal_GetPlayer_m776499950_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_Internal_GetPlayer_m776499950_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::Internal_GetPlayer()");
	return _il2cpp_icall_func();
}
// UnityEngine.NetworkPlayer UnityEngine.Network::get_player()
extern "C"  NetworkPlayer_t3231273765  Network_get_player_m1200723560 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	NetworkPlayer_t3231273765  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = Network_Internal_GetPlayer_m776499950(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->set_index_0(L_0);
		NetworkPlayer_t3231273765  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Object UnityEngine.Network::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32)
extern "C"  Object_t3071478659 * Network_Instantiate_m2753034863 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___prefab0, Vector3_t4282066566  ___position1, Quaternion_t1553702882  ___rotation2, int32_t ___group3, const MethodInfo* method)
{
	{
		Object_t3071478659 * L_0 = ___prefab0;
		int32_t L_1 = ___group3;
		Object_t3071478659 * L_2 = Network_INTERNAL_CALL_Instantiate_m4087140154(NULL /*static, unused*/, L_0, (&___position1), (&___rotation2), L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Object UnityEngine.Network::INTERNAL_CALL_Instantiate(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&,System.Int32)
extern "C"  Object_t3071478659 * Network_INTERNAL_CALL_Instantiate_m4087140154 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___prefab0, Vector3_t4282066566 * ___position1, Quaternion_t1553702882 * ___rotation2, int32_t ___group3, const MethodInfo* method)
{
	typedef Object_t3071478659 * (*Network_INTERNAL_CALL_Instantiate_m4087140154_ftn) (Object_t3071478659 *, Vector3_t4282066566 *, Quaternion_t1553702882 *, int32_t);
	static Network_INTERNAL_CALL_Instantiate_m4087140154_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_INTERNAL_CALL_Instantiate_m4087140154_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::INTERNAL_CALL_Instantiate(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&,System.Int32)");
	return _il2cpp_icall_func(___prefab0, ___position1, ___rotation2, ___group3);
}
// System.Void UnityEngine.Network::DestroyPlayerObjects(UnityEngine.NetworkPlayer)
extern "C"  void Network_DestroyPlayerObjects_m73641416 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t3231273765  ___playerID0, const MethodInfo* method)
{
	typedef void (*Network_DestroyPlayerObjects_m73641416_ftn) (NetworkPlayer_t3231273765 );
	static Network_DestroyPlayerObjects_m73641416_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_DestroyPlayerObjects_m73641416_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::DestroyPlayerObjects(UnityEngine.NetworkPlayer)");
	_il2cpp_icall_func(___playerID0);
}
// System.Void UnityEngine.Network::Internal_RemoveRPCs(UnityEngine.NetworkPlayer,UnityEngine.NetworkViewID,System.UInt32)
extern "C"  void Network_Internal_RemoveRPCs_m3085275014 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t3231273765  ___playerID0, NetworkViewID_t3400394436  ___viewID1, uint32_t ___channelMask2, const MethodInfo* method)
{
	{
		NetworkPlayer_t3231273765  L_0 = ___playerID0;
		uint32_t L_1 = ___channelMask2;
		Network_INTERNAL_CALL_Internal_RemoveRPCs_m315609935(NULL /*static, unused*/, L_0, (&___viewID1), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Network::INTERNAL_CALL_Internal_RemoveRPCs(UnityEngine.NetworkPlayer,UnityEngine.NetworkViewID&,System.UInt32)
extern "C"  void Network_INTERNAL_CALL_Internal_RemoveRPCs_m315609935 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t3231273765  ___playerID0, NetworkViewID_t3400394436 * ___viewID1, uint32_t ___channelMask2, const MethodInfo* method)
{
	typedef void (*Network_INTERNAL_CALL_Internal_RemoveRPCs_m315609935_ftn) (NetworkPlayer_t3231273765 , NetworkViewID_t3400394436 *, uint32_t);
	static Network_INTERNAL_CALL_Internal_RemoveRPCs_m315609935_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_INTERNAL_CALL_Internal_RemoveRPCs_m315609935_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::INTERNAL_CALL_Internal_RemoveRPCs(UnityEngine.NetworkPlayer,UnityEngine.NetworkViewID&,System.UInt32)");
	_il2cpp_icall_func(___playerID0, ___viewID1, ___channelMask2);
}
// System.Void UnityEngine.Network::RemoveRPCs(UnityEngine.NetworkPlayer)
extern "C"  void Network_RemoveRPCs_m3795657583 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t3231273765  ___playerID0, const MethodInfo* method)
{
	{
		NetworkPlayer_t3231273765  L_0 = ___playerID0;
		NetworkViewID_t3400394436  L_1 = NetworkViewID_get_unassigned_m2302461037(NULL /*static, unused*/, /*hidden argument*/NULL);
		Network_Internal_RemoveRPCs_m3085275014(NULL /*static, unused*/, L_0, L_1, (-1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Network::RemoveRPCs(UnityEngine.NetworkViewID)
extern "C"  void Network_RemoveRPCs_m448463792 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436  ___viewID0, const MethodInfo* method)
{
	{
		NetworkPlayer_t3231273765  L_0 = NetworkPlayer_get_unassigned_m865556847(NULL /*static, unused*/, /*hidden argument*/NULL);
		NetworkViewID_t3400394436  L_1 = ___viewID0;
		Network_Internal_RemoveRPCs_m3085275014(NULL /*static, unused*/, L_0, L_1, (-1), /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Network::get_isClient()
extern "C"  bool Network_get_isClient_m4143111185 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Network_get_isClient_m4143111185_ftn) ();
	static Network_get_isClient_m4143111185_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_get_isClient_m4143111185_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::get_isClient()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.Network::get_isServer()
extern "C"  bool Network_get_isServer_m318839177 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Network_get_isServer_m318839177_ftn) ();
	static Network_get_isServer_m318839177_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_get_isServer_m318839177_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::get_isServer()");
	return _il2cpp_icall_func();
}
// UnityEngine.NetworkPeerType UnityEngine.Network::get_peerType()
extern "C"  int32_t Network_get_peerType_m115170590 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Network_get_peerType_m115170590_ftn) ();
	static Network_get_peerType_m115170590_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_get_peerType_m115170590_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::get_peerType()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Network::SetLevelPrefix(System.Int32)
extern "C"  void Network_SetLevelPrefix_m3013656638 (Il2CppObject * __this /* static, unused */, int32_t ___prefix0, const MethodInfo* method)
{
	typedef void (*Network_SetLevelPrefix_m3013656638_ftn) (int32_t);
	static Network_SetLevelPrefix_m3013656638_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_SetLevelPrefix_m3013656638_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::SetLevelPrefix(System.Int32)");
	_il2cpp_icall_func(___prefix0);
}
// System.Int32 UnityEngine.Network::GetLastPing(UnityEngine.NetworkPlayer)
extern "C"  int32_t Network_GetLastPing_m3340324755 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t3231273765  ___player0, const MethodInfo* method)
{
	typedef int32_t (*Network_GetLastPing_m3340324755_ftn) (NetworkPlayer_t3231273765 );
	static Network_GetLastPing_m3340324755_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_GetLastPing_m3340324755_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::GetLastPing(UnityEngine.NetworkPlayer)");
	return _il2cpp_icall_func(___player0);
}
// System.Int32 UnityEngine.Network::GetAveragePing(UnityEngine.NetworkPlayer)
extern "C"  int32_t Network_GetAveragePing_m3083558248 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t3231273765  ___player0, const MethodInfo* method)
{
	typedef int32_t (*Network_GetAveragePing_m3083558248_ftn) (NetworkPlayer_t3231273765 );
	static Network_GetAveragePing_m3083558248_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_GetAveragePing_m3083558248_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::GetAveragePing(UnityEngine.NetworkPlayer)");
	return _il2cpp_icall_func(___player0);
}
// System.Single UnityEngine.Network::get_sendRate()
extern "C"  float Network_get_sendRate_m1227422814 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Network_get_sendRate_m1227422814_ftn) ();
	static Network_get_sendRate_m1227422814_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_get_sendRate_m1227422814_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::get_sendRate()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Network::set_sendRate(System.Single)
extern "C"  void Network_set_sendRate_m2111190029 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*Network_set_sendRate_m2111190029_ftn) (float);
	static Network_set_sendRate_m2111190029_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_set_sendRate_m2111190029_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::set_sendRate(System.Single)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.Network::get_isMessageQueueRunning()
extern "C"  bool Network_get_isMessageQueueRunning_m5081841 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Network_get_isMessageQueueRunning_m5081841_ftn) ();
	static Network_get_isMessageQueueRunning_m5081841_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_get_isMessageQueueRunning_m5081841_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::get_isMessageQueueRunning()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Network::set_isMessageQueueRunning(System.Boolean)
extern "C"  void Network_set_isMessageQueueRunning_m1610613326 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*Network_set_isMessageQueueRunning_m1610613326_ftn) (bool);
	static Network_set_isMessageQueueRunning_m1610613326_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_set_isMessageQueueRunning_m1610613326_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::set_isMessageQueueRunning(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.Network::Internal_GetTime(System.Double&)
extern "C"  void Network_Internal_GetTime_m2753432228 (Il2CppObject * __this /* static, unused */, double* ___t0, const MethodInfo* method)
{
	typedef void (*Network_Internal_GetTime_m2753432228_ftn) (double*);
	static Network_Internal_GetTime_m2753432228_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_Internal_GetTime_m2753432228_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::Internal_GetTime(System.Double&)");
	_il2cpp_icall_func(___t0);
}
// System.Double UnityEngine.Network::get_time()
extern "C"  double Network_get_time_m4227697516 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	double V_0 = 0.0;
	{
		Network_Internal_GetTime_m2753432228(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		double L_0 = V_0;
		return L_0;
	}
}
// System.Int32 UnityEngine.Network::get_minimumAllocatableViewIDs()
extern "C"  int32_t Network_get_minimumAllocatableViewIDs_m2986052927 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Network_get_minimumAllocatableViewIDs_m2986052927_ftn) ();
	static Network_get_minimumAllocatableViewIDs_m2986052927_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_get_minimumAllocatableViewIDs_m2986052927_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::get_minimumAllocatableViewIDs()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Network::set_minimumAllocatableViewIDs(System.Int32)
extern "C"  void Network_set_minimumAllocatableViewIDs_m2130893980 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Network_set_minimumAllocatableViewIDs_m2130893980_ftn) (int32_t);
	static Network_set_minimumAllocatableViewIDs_m2130893980_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_set_minimumAllocatableViewIDs_m2130893980_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::set_minimumAllocatableViewIDs(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.Network::HavePublicAddress()
extern "C"  bool Network_HavePublicAddress_m1494895090 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Network_HavePublicAddress_m1494895090_ftn) ();
	static Network_HavePublicAddress_m1494895090_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_HavePublicAddress_m1494895090_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::HavePublicAddress()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Network::get_maxConnections()
extern "C"  int32_t Network_get_maxConnections_m3468981299 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Network_get_maxConnections_m3468981299_ftn) ();
	static Network_get_maxConnections_m3468981299_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_get_maxConnections_m3468981299_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::get_maxConnections()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Network::set_maxConnections(System.Int32)
extern "C"  void Network_set_maxConnections_m2445464568 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Network_set_maxConnections_m2445464568_ftn) (int32_t);
	static Network_set_maxConnections_m2445464568_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_set_maxConnections_m2445464568_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::set_maxConnections(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// System.Double UnityEngine.NetworkMessageInfo::get_timestamp()
extern "C"  double NetworkMessageInfo_get_timestamp_m3597535726 (NetworkMessageInfo_t3807997963 * __this, const MethodInfo* method)
{
	{
		double L_0 = __this->get_m_TimeStamp_0();
		return L_0;
	}
}
extern "C"  double NetworkMessageInfo_get_timestamp_m3597535726_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkMessageInfo_t3807997963 * _thisAdjusted = reinterpret_cast<NetworkMessageInfo_t3807997963 *>(__this + 1);
	return NetworkMessageInfo_get_timestamp_m3597535726(_thisAdjusted, method);
}
// UnityEngine.NetworkPlayer UnityEngine.NetworkMessageInfo::get_sender()
extern "C"  NetworkPlayer_t3231273765  NetworkMessageInfo_get_sender_m2720861079 (NetworkMessageInfo_t3807997963 * __this, const MethodInfo* method)
{
	{
		NetworkPlayer_t3231273765  L_0 = __this->get_m_Sender_1();
		return L_0;
	}
}
extern "C"  NetworkPlayer_t3231273765  NetworkMessageInfo_get_sender_m2720861079_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkMessageInfo_t3807997963 * _thisAdjusted = reinterpret_cast<NetworkMessageInfo_t3807997963 *>(__this + 1);
	return NetworkMessageInfo_get_sender_m2720861079(_thisAdjusted, method);
}
// UnityEngine.NetworkView UnityEngine.NetworkMessageInfo::get_networkView()
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral907697851;
extern const uint32_t NetworkMessageInfo_get_networkView_m221874487_MetadataUsageId;
extern "C"  NetworkView_t3656680617 * NetworkMessageInfo_get_networkView_m221874487 (NetworkMessageInfo_t3807997963 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NetworkMessageInfo_get_networkView_m221874487_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NetworkViewID_t3400394436  L_0 = __this->get_m_ViewID_2();
		NetworkViewID_t3400394436  L_1 = NetworkViewID_get_unassigned_m2302461037(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_2 = NetworkViewID_op_Equality_m776200489(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral907697851, /*hidden argument*/NULL);
		NetworkView_t3656680617 * L_3 = NetworkMessageInfo_NullNetworkView_m516412409(__this, /*hidden argument*/NULL);
		return L_3;
	}

IL_0026:
	{
		NetworkViewID_t3400394436  L_4 = __this->get_m_ViewID_2();
		NetworkView_t3656680617 * L_5 = NetworkView_Find_m2926682619(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
extern "C"  NetworkView_t3656680617 * NetworkMessageInfo_get_networkView_m221874487_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkMessageInfo_t3807997963 * _thisAdjusted = reinterpret_cast<NetworkMessageInfo_t3807997963 *>(__this + 1);
	return NetworkMessageInfo_get_networkView_m221874487(_thisAdjusted, method);
}
// UnityEngine.NetworkView UnityEngine.NetworkMessageInfo::NullNetworkView()
extern "C"  NetworkView_t3656680617 * NetworkMessageInfo_NullNetworkView_m516412409 (NetworkMessageInfo_t3807997963 * __this, const MethodInfo* method)
{
	typedef NetworkView_t3656680617 * (*NetworkMessageInfo_NullNetworkView_m516412409_ftn) (NetworkMessageInfo_t3807997963 *);
	static NetworkMessageInfo_NullNetworkView_m516412409_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkMessageInfo_NullNetworkView_m516412409_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkMessageInfo::NullNetworkView()");
	return _il2cpp_icall_func(__this);
}
extern "C"  NetworkView_t3656680617 * NetworkMessageInfo_NullNetworkView_m516412409_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkMessageInfo_t3807997963 * _thisAdjusted = reinterpret_cast<NetworkMessageInfo_t3807997963 *>(__this + 1);
	return NetworkMessageInfo_NullNetworkView_m516412409(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.NetworkMessageInfo
extern "C" void NetworkMessageInfo_t3807997963_marshal_pinvoke(const NetworkMessageInfo_t3807997963& unmarshaled, NetworkMessageInfo_t3807997963_marshaled_pinvoke& marshaled)
{
	marshaled.___m_TimeStamp_0 = unmarshaled.get_m_TimeStamp_0();
	NetworkPlayer_t3231273765_marshal_pinvoke(unmarshaled.get_m_Sender_1(), marshaled.___m_Sender_1);
	NetworkViewID_t3400394436_marshal_pinvoke(unmarshaled.get_m_ViewID_2(), marshaled.___m_ViewID_2);
}
extern "C" void NetworkMessageInfo_t3807997963_marshal_pinvoke_back(const NetworkMessageInfo_t3807997963_marshaled_pinvoke& marshaled, NetworkMessageInfo_t3807997963& unmarshaled)
{
	double unmarshaled_m_TimeStamp_temp_0 = 0.0;
	unmarshaled_m_TimeStamp_temp_0 = marshaled.___m_TimeStamp_0;
	unmarshaled.set_m_TimeStamp_0(unmarshaled_m_TimeStamp_temp_0);
	NetworkPlayer_t3231273765  unmarshaled_m_Sender_temp_1;
	memset(&unmarshaled_m_Sender_temp_1, 0, sizeof(unmarshaled_m_Sender_temp_1));
	NetworkPlayer_t3231273765_marshal_pinvoke_back(marshaled.___m_Sender_1, unmarshaled_m_Sender_temp_1);
	unmarshaled.set_m_Sender_1(unmarshaled_m_Sender_temp_1);
	NetworkViewID_t3400394436  unmarshaled_m_ViewID_temp_2;
	memset(&unmarshaled_m_ViewID_temp_2, 0, sizeof(unmarshaled_m_ViewID_temp_2));
	NetworkViewID_t3400394436_marshal_pinvoke_back(marshaled.___m_ViewID_2, unmarshaled_m_ViewID_temp_2);
	unmarshaled.set_m_ViewID_2(unmarshaled_m_ViewID_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.NetworkMessageInfo
extern "C" void NetworkMessageInfo_t3807997963_marshal_pinvoke_cleanup(NetworkMessageInfo_t3807997963_marshaled_pinvoke& marshaled)
{
	NetworkPlayer_t3231273765_marshal_pinvoke_cleanup(marshaled.___m_Sender_1);
	NetworkViewID_t3400394436_marshal_pinvoke_cleanup(marshaled.___m_ViewID_2);
}
// Conversion methods for marshalling of: UnityEngine.NetworkMessageInfo
extern "C" void NetworkMessageInfo_t3807997963_marshal_com(const NetworkMessageInfo_t3807997963& unmarshaled, NetworkMessageInfo_t3807997963_marshaled_com& marshaled)
{
	marshaled.___m_TimeStamp_0 = unmarshaled.get_m_TimeStamp_0();
	NetworkPlayer_t3231273765_marshal_com(unmarshaled.get_m_Sender_1(), marshaled.___m_Sender_1);
	NetworkViewID_t3400394436_marshal_com(unmarshaled.get_m_ViewID_2(), marshaled.___m_ViewID_2);
}
extern "C" void NetworkMessageInfo_t3807997963_marshal_com_back(const NetworkMessageInfo_t3807997963_marshaled_com& marshaled, NetworkMessageInfo_t3807997963& unmarshaled)
{
	double unmarshaled_m_TimeStamp_temp_0 = 0.0;
	unmarshaled_m_TimeStamp_temp_0 = marshaled.___m_TimeStamp_0;
	unmarshaled.set_m_TimeStamp_0(unmarshaled_m_TimeStamp_temp_0);
	NetworkPlayer_t3231273765  unmarshaled_m_Sender_temp_1;
	memset(&unmarshaled_m_Sender_temp_1, 0, sizeof(unmarshaled_m_Sender_temp_1));
	NetworkPlayer_t3231273765_marshal_com_back(marshaled.___m_Sender_1, unmarshaled_m_Sender_temp_1);
	unmarshaled.set_m_Sender_1(unmarshaled_m_Sender_temp_1);
	NetworkViewID_t3400394436  unmarshaled_m_ViewID_temp_2;
	memset(&unmarshaled_m_ViewID_temp_2, 0, sizeof(unmarshaled_m_ViewID_temp_2));
	NetworkViewID_t3400394436_marshal_com_back(marshaled.___m_ViewID_2, unmarshaled_m_ViewID_temp_2);
	unmarshaled.set_m_ViewID_2(unmarshaled_m_ViewID_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.NetworkMessageInfo
extern "C" void NetworkMessageInfo_t3807997963_marshal_com_cleanup(NetworkMessageInfo_t3807997963_marshaled_com& marshaled)
{
	NetworkPlayer_t3231273765_marshal_com_cleanup(marshaled.___m_Sender_1);
	NetworkViewID_t3400394436_marshal_com_cleanup(marshaled.___m_ViewID_2);
}
// System.Void UnityEngine.NetworkPlayer::.ctor(System.String,System.Int32)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2814584925;
extern const uint32_t NetworkPlayer__ctor_m978047135_MetadataUsageId;
extern "C"  void NetworkPlayer__ctor_m978047135 (NetworkPlayer_t3231273765 * __this, String_t* ___ip0, int32_t ___port1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NetworkPlayer__ctor_m978047135_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral2814584925, /*hidden argument*/NULL);
		__this->set_index_0(0);
		return;
	}
}
extern "C"  void NetworkPlayer__ctor_m978047135_AdjustorThunk (Il2CppObject * __this, String_t* ___ip0, int32_t ___port1, const MethodInfo* method)
{
	NetworkPlayer_t3231273765 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t3231273765 *>(__this + 1);
	NetworkPlayer__ctor_m978047135(_thisAdjusted, ___ip0, ___port1, method);
}
// System.String UnityEngine.NetworkPlayer::Internal_GetIPAddress(System.Int32)
extern "C"  String_t* NetworkPlayer_Internal_GetIPAddress_m2705922677 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method)
{
	typedef String_t* (*NetworkPlayer_Internal_GetIPAddress_m2705922677_ftn) (int32_t);
	static NetworkPlayer_Internal_GetIPAddress_m2705922677_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetIPAddress_m2705922677_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetIPAddress(System.Int32)");
	return _il2cpp_icall_func(___index0);
}
// System.Int32 UnityEngine.NetworkPlayer::Internal_GetPort(System.Int32)
extern "C"  int32_t NetworkPlayer_Internal_GetPort_m2646691040 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method)
{
	typedef int32_t (*NetworkPlayer_Internal_GetPort_m2646691040_ftn) (int32_t);
	static NetworkPlayer_Internal_GetPort_m2646691040_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetPort_m2646691040_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetPort(System.Int32)");
	return _il2cpp_icall_func(___index0);
}
// System.String UnityEngine.NetworkPlayer::Internal_GetExternalIP()
extern "C"  String_t* NetworkPlayer_Internal_GetExternalIP_m4147820797 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*NetworkPlayer_Internal_GetExternalIP_m4147820797_ftn) ();
	static NetworkPlayer_Internal_GetExternalIP_m4147820797_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetExternalIP_m4147820797_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetExternalIP()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.NetworkPlayer::Internal_GetExternalPort()
extern "C"  int32_t NetworkPlayer_Internal_GetExternalPort_m152642746 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*NetworkPlayer_Internal_GetExternalPort_m152642746_ftn) ();
	static NetworkPlayer_Internal_GetExternalPort_m152642746_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetExternalPort_m152642746_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetExternalPort()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.NetworkPlayer::Internal_GetLocalIP()
extern "C"  String_t* NetworkPlayer_Internal_GetLocalIP_m3142644809 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*NetworkPlayer_Internal_GetLocalIP_m3142644809_ftn) ();
	static NetworkPlayer_Internal_GetLocalIP_m3142644809_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetLocalIP_m3142644809_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetLocalIP()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.NetworkPlayer::Internal_GetLocalPort()
extern "C"  int32_t NetworkPlayer_Internal_GetLocalPort_m4290647008 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*NetworkPlayer_Internal_GetLocalPort_m4290647008_ftn) ();
	static NetworkPlayer_Internal_GetLocalPort_m4290647008_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetLocalPort_m4290647008_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetLocalPort()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.NetworkPlayer::Internal_GetPlayerIndex()
extern "C"  int32_t NetworkPlayer_Internal_GetPlayerIndex_m1155344837 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*NetworkPlayer_Internal_GetPlayerIndex_m1155344837_ftn) ();
	static NetworkPlayer_Internal_GetPlayerIndex_m1155344837_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetPlayerIndex_m1155344837_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetPlayerIndex()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.NetworkPlayer::Internal_GetGUID(System.Int32)
extern "C"  String_t* NetworkPlayer_Internal_GetGUID_m82698821 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method)
{
	typedef String_t* (*NetworkPlayer_Internal_GetGUID_m82698821_ftn) (int32_t);
	static NetworkPlayer_Internal_GetGUID_m82698821_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetGUID_m82698821_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetGUID(System.Int32)");
	return _il2cpp_icall_func(___index0);
}
// System.String UnityEngine.NetworkPlayer::Internal_GetLocalGUID()
extern "C"  String_t* NetworkPlayer_Internal_GetLocalGUID_m668021995 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*NetworkPlayer_Internal_GetLocalGUID_m668021995_ftn) ();
	static NetworkPlayer_Internal_GetLocalGUID_m668021995_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetLocalGUID_m668021995_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetLocalGUID()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.NetworkPlayer::GetHashCode()
extern "C"  int32_t NetworkPlayer_GetHashCode_m2832997817 (NetworkPlayer_t3231273765 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = __this->get_address_of_index_0();
		int32_t L_1 = Int32_GetHashCode_m3396943446(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
extern "C"  int32_t NetworkPlayer_GetHashCode_m2832997817_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t3231273765 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t3231273765 *>(__this + 1);
	return NetworkPlayer_GetHashCode_m2832997817(_thisAdjusted, method);
}
// System.Boolean UnityEngine.NetworkPlayer::Equals(System.Object)
extern Il2CppClass* NetworkPlayer_t3231273765_il2cpp_TypeInfo_var;
extern const uint32_t NetworkPlayer_Equals_m1380117345_MetadataUsageId;
extern "C"  bool NetworkPlayer_Equals_m1380117345 (NetworkPlayer_t3231273765 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NetworkPlayer_Equals_m1380117345_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	NetworkPlayer_t3231273765  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, NetworkPlayer_t3231273765_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(NetworkPlayer_t3231273765 *)((NetworkPlayer_t3231273765 *)UnBox (L_1, NetworkPlayer_t3231273765_il2cpp_TypeInfo_var))));
		int32_t L_2 = (&V_0)->get_index_0();
		int32_t L_3 = __this->get_index_0();
		return (bool)((((int32_t)L_2) == ((int32_t)L_3))? 1 : 0);
	}
}
extern "C"  bool NetworkPlayer_Equals_m1380117345_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	NetworkPlayer_t3231273765 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t3231273765 *>(__this + 1);
	return NetworkPlayer_Equals_m1380117345(_thisAdjusted, ___other0, method);
}
// System.String UnityEngine.NetworkPlayer::get_ipAddress()
extern "C"  String_t* NetworkPlayer_get_ipAddress_m206939407 (NetworkPlayer_t3231273765 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_index_0();
		int32_t L_1 = NetworkPlayer_Internal_GetPlayerIndex_m1155344837(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0016;
		}
	}
	{
		String_t* L_2 = NetworkPlayer_Internal_GetLocalIP_m3142644809(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}

IL_0016:
	{
		int32_t L_3 = __this->get_index_0();
		String_t* L_4 = NetworkPlayer_Internal_GetIPAddress_m2705922677(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
extern "C"  String_t* NetworkPlayer_get_ipAddress_m206939407_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t3231273765 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t3231273765 *>(__this + 1);
	return NetworkPlayer_get_ipAddress_m206939407(_thisAdjusted, method);
}
// System.Int32 UnityEngine.NetworkPlayer::get_port()
extern "C"  int32_t NetworkPlayer_get_port_m4260923428 (NetworkPlayer_t3231273765 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_index_0();
		int32_t L_1 = NetworkPlayer_Internal_GetPlayerIndex_m1155344837(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_2 = NetworkPlayer_Internal_GetLocalPort_m4290647008(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}

IL_0016:
	{
		int32_t L_3 = __this->get_index_0();
		int32_t L_4 = NetworkPlayer_Internal_GetPort_m2646691040(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
extern "C"  int32_t NetworkPlayer_get_port_m4260923428_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t3231273765 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t3231273765 *>(__this + 1);
	return NetworkPlayer_get_port_m4260923428(_thisAdjusted, method);
}
// System.String UnityEngine.NetworkPlayer::get_guid()
extern "C"  String_t* NetworkPlayer_get_guid_m1193250345 (NetworkPlayer_t3231273765 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_index_0();
		int32_t L_1 = NetworkPlayer_Internal_GetPlayerIndex_m1155344837(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0016;
		}
	}
	{
		String_t* L_2 = NetworkPlayer_Internal_GetLocalGUID_m668021995(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}

IL_0016:
	{
		int32_t L_3 = __this->get_index_0();
		String_t* L_4 = NetworkPlayer_Internal_GetGUID_m82698821(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
extern "C"  String_t* NetworkPlayer_get_guid_m1193250345_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t3231273765 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t3231273765 *>(__this + 1);
	return NetworkPlayer_get_guid_m1193250345(_thisAdjusted, method);
}
// System.String UnityEngine.NetworkPlayer::ToString()
extern "C"  String_t* NetworkPlayer_ToString_m4115863651 (NetworkPlayer_t3231273765 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = __this->get_address_of_index_0();
		String_t* L_1 = Int32_ToString_m1286526384(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
extern "C"  String_t* NetworkPlayer_ToString_m4115863651_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t3231273765 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t3231273765 *>(__this + 1);
	return NetworkPlayer_ToString_m4115863651(_thisAdjusted, method);
}
// System.String UnityEngine.NetworkPlayer::get_externalIP()
extern "C"  String_t* NetworkPlayer_get_externalIP_m420672594 (NetworkPlayer_t3231273765 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = NetworkPlayer_Internal_GetExternalIP_m4147820797(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  String_t* NetworkPlayer_get_externalIP_m420672594_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t3231273765 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t3231273765 *>(__this + 1);
	return NetworkPlayer_get_externalIP_m420672594(_thisAdjusted, method);
}
// System.Int32 UnityEngine.NetworkPlayer::get_externalPort()
extern "C"  int32_t NetworkPlayer_get_externalPort_m2392166863 (NetworkPlayer_t3231273765 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = NetworkPlayer_Internal_GetExternalPort_m152642746(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  int32_t NetworkPlayer_get_externalPort_m2392166863_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t3231273765 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t3231273765 *>(__this + 1);
	return NetworkPlayer_get_externalPort_m2392166863(_thisAdjusted, method);
}
// UnityEngine.NetworkPlayer UnityEngine.NetworkPlayer::get_unassigned()
extern "C"  NetworkPlayer_t3231273765  NetworkPlayer_get_unassigned_m865556847 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	NetworkPlayer_t3231273765  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		(&V_0)->set_index_0((-1));
		NetworkPlayer_t3231273765  L_0 = V_0;
		return L_0;
	}
}
// System.Boolean UnityEngine.NetworkPlayer::op_Equality(UnityEngine.NetworkPlayer,UnityEngine.NetworkPlayer)
extern "C"  bool NetworkPlayer_op_Equality_m1190866952 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t3231273765  ___lhs0, NetworkPlayer_t3231273765  ___rhs1, const MethodInfo* method)
{
	{
		int32_t L_0 = (&___lhs0)->get_index_0();
		int32_t L_1 = (&___rhs1)->get_index_0();
		return (bool)((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0);
	}
}
// System.Boolean UnityEngine.NetworkPlayer::op_Inequality(UnityEngine.NetworkPlayer,UnityEngine.NetworkPlayer)
extern "C"  bool NetworkPlayer_op_Inequality_m3921017795 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t3231273765  ___lhs0, NetworkPlayer_t3231273765  ___rhs1, const MethodInfo* method)
{
	{
		int32_t L_0 = (&___lhs0)->get_index_0();
		int32_t L_1 = (&___rhs1)->get_index_0();
		return (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.NetworkPlayer
extern "C" void NetworkPlayer_t3231273765_marshal_pinvoke(const NetworkPlayer_t3231273765& unmarshaled, NetworkPlayer_t3231273765_marshaled_pinvoke& marshaled)
{
	marshaled.___index_0 = unmarshaled.get_index_0();
}
extern "C" void NetworkPlayer_t3231273765_marshal_pinvoke_back(const NetworkPlayer_t3231273765_marshaled_pinvoke& marshaled, NetworkPlayer_t3231273765& unmarshaled)
{
	int32_t unmarshaled_index_temp_0 = 0;
	unmarshaled_index_temp_0 = marshaled.___index_0;
	unmarshaled.set_index_0(unmarshaled_index_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.NetworkPlayer
extern "C" void NetworkPlayer_t3231273765_marshal_pinvoke_cleanup(NetworkPlayer_t3231273765_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.NetworkPlayer
extern "C" void NetworkPlayer_t3231273765_marshal_com(const NetworkPlayer_t3231273765& unmarshaled, NetworkPlayer_t3231273765_marshaled_com& marshaled)
{
	marshaled.___index_0 = unmarshaled.get_index_0();
}
extern "C" void NetworkPlayer_t3231273765_marshal_com_back(const NetworkPlayer_t3231273765_marshaled_com& marshaled, NetworkPlayer_t3231273765& unmarshaled)
{
	int32_t unmarshaled_index_temp_0 = 0;
	unmarshaled_index_temp_0 = marshaled.___index_0;
	unmarshaled.set_index_0(unmarshaled_index_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.NetworkPlayer
extern "C" void NetworkPlayer_t3231273765_marshal_com_cleanup(NetworkPlayer_t3231273765_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.NetworkView::Internal_GetViewID(UnityEngine.NetworkViewID&)
extern "C"  void NetworkView_Internal_GetViewID_m3457400931 (NetworkView_t3656680617 * __this, NetworkViewID_t3400394436 * ___viewID0, const MethodInfo* method)
{
	typedef void (*NetworkView_Internal_GetViewID_m3457400931_ftn) (NetworkView_t3656680617 *, NetworkViewID_t3400394436 *);
	static NetworkView_Internal_GetViewID_m3457400931_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkView_Internal_GetViewID_m3457400931_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkView::Internal_GetViewID(UnityEngine.NetworkViewID&)");
	_il2cpp_icall_func(__this, ___viewID0);
}
// UnityEngine.NetworkViewID UnityEngine.NetworkView::get_viewID()
extern "C"  NetworkViewID_t3400394436  NetworkView_get_viewID_m148368939 (NetworkView_t3656680617 * __this, const MethodInfo* method)
{
	NetworkViewID_t3400394436  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		NetworkView_Internal_GetViewID_m3457400931(__this, (&V_0), /*hidden argument*/NULL);
		NetworkViewID_t3400394436  L_0 = V_0;
		return L_0;
	}
}
// System.Boolean UnityEngine.NetworkView::get_isMine()
extern "C"  bool NetworkView_get_isMine_m4033327326 (NetworkView_t3656680617 * __this, const MethodInfo* method)
{
	NetworkViewID_t3400394436  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		NetworkViewID_t3400394436  L_0 = NetworkView_get_viewID_m148368939(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = NetworkViewID_get_isMine_m2754407673((&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.NetworkPlayer UnityEngine.NetworkView::get_owner()
extern "C"  NetworkPlayer_t3231273765  NetworkView_get_owner_m1924541257 (NetworkView_t3656680617 * __this, const MethodInfo* method)
{
	NetworkViewID_t3400394436  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		NetworkViewID_t3400394436  L_0 = NetworkView_get_viewID_m148368939(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		NetworkPlayer_t3231273765  L_1 = NetworkViewID_get_owner_m576069262((&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.NetworkView UnityEngine.NetworkView::Find(UnityEngine.NetworkViewID)
extern "C"  NetworkView_t3656680617 * NetworkView_Find_m2926682619 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436  ___viewID0, const MethodInfo* method)
{
	{
		NetworkView_t3656680617 * L_0 = NetworkView_INTERNAL_CALL_Find_m752431216(NULL /*static, unused*/, (&___viewID0), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.NetworkView UnityEngine.NetworkView::INTERNAL_CALL_Find(UnityEngine.NetworkViewID&)
extern "C"  NetworkView_t3656680617 * NetworkView_INTERNAL_CALL_Find_m752431216 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436 * ___viewID0, const MethodInfo* method)
{
	typedef NetworkView_t3656680617 * (*NetworkView_INTERNAL_CALL_Find_m752431216_ftn) (NetworkViewID_t3400394436 *);
	static NetworkView_INTERNAL_CALL_Find_m752431216_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkView_INTERNAL_CALL_Find_m752431216_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkView::INTERNAL_CALL_Find(UnityEngine.NetworkViewID&)");
	return _il2cpp_icall_func(___viewID0);
}
// UnityEngine.NetworkViewID UnityEngine.NetworkViewID::get_unassigned()
extern "C"  NetworkViewID_t3400394436  NetworkViewID_get_unassigned_m2302461037 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	NetworkViewID_t3400394436  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		NetworkViewID_INTERNAL_get_unassigned_m3953385100(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		NetworkViewID_t3400394436  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.NetworkViewID::INTERNAL_get_unassigned(UnityEngine.NetworkViewID&)
extern "C"  void NetworkViewID_INTERNAL_get_unassigned_m3953385100 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436 * ___value0, const MethodInfo* method)
{
	typedef void (*NetworkViewID_INTERNAL_get_unassigned_m3953385100_ftn) (NetworkViewID_t3400394436 *);
	static NetworkViewID_INTERNAL_get_unassigned_m3953385100_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_get_unassigned_m3953385100_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_get_unassigned(UnityEngine.NetworkViewID&)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.NetworkViewID::Internal_IsMine(UnityEngine.NetworkViewID)
extern "C"  bool NetworkViewID_Internal_IsMine_m619939661 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436  ___value0, const MethodInfo* method)
{
	{
		bool L_0 = NetworkViewID_INTERNAL_CALL_Internal_IsMine_m2052798080(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_IsMine(UnityEngine.NetworkViewID&)
extern "C"  bool NetworkViewID_INTERNAL_CALL_Internal_IsMine_m2052798080 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436 * ___value0, const MethodInfo* method)
{
	typedef bool (*NetworkViewID_INTERNAL_CALL_Internal_IsMine_m2052798080_ftn) (NetworkViewID_t3400394436 *);
	static NetworkViewID_INTERNAL_CALL_Internal_IsMine_m2052798080_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_CALL_Internal_IsMine_m2052798080_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_IsMine(UnityEngine.NetworkViewID&)");
	return _il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.NetworkViewID::Internal_GetOwner(UnityEngine.NetworkViewID,UnityEngine.NetworkPlayer&)
extern "C"  void NetworkViewID_Internal_GetOwner_m874421061 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436  ___value0, NetworkPlayer_t3231273765 * ___player1, const MethodInfo* method)
{
	{
		NetworkPlayer_t3231273765 * L_0 = ___player1;
		NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m2405750590(NULL /*static, unused*/, (&___value0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_GetOwner(UnityEngine.NetworkViewID&,UnityEngine.NetworkPlayer&)
extern "C"  void NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m2405750590 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436 * ___value0, NetworkPlayer_t3231273765 * ___player1, const MethodInfo* method)
{
	typedef void (*NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m2405750590_ftn) (NetworkViewID_t3400394436 *, NetworkPlayer_t3231273765 *);
	static NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m2405750590_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m2405750590_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_GetOwner(UnityEngine.NetworkViewID&,UnityEngine.NetworkPlayer&)");
	_il2cpp_icall_func(___value0, ___player1);
}
// System.String UnityEngine.NetworkViewID::Internal_GetString(UnityEngine.NetworkViewID)
extern "C"  String_t* NetworkViewID_Internal_GetString_m679444096 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436  ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = NetworkViewID_INTERNAL_CALL_Internal_GetString_m1523080203(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_GetString(UnityEngine.NetworkViewID&)
extern "C"  String_t* NetworkViewID_INTERNAL_CALL_Internal_GetString_m1523080203 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436 * ___value0, const MethodInfo* method)
{
	typedef String_t* (*NetworkViewID_INTERNAL_CALL_Internal_GetString_m1523080203_ftn) (NetworkViewID_t3400394436 *);
	static NetworkViewID_INTERNAL_CALL_Internal_GetString_m1523080203_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_CALL_Internal_GetString_m1523080203_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_GetString(UnityEngine.NetworkViewID&)");
	return _il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.NetworkViewID::Internal_Compare(UnityEngine.NetworkViewID,UnityEngine.NetworkViewID)
extern "C"  bool NetworkViewID_Internal_Compare_m2541220858 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436  ___lhs0, NetworkViewID_t3400394436  ___rhs1, const MethodInfo* method)
{
	{
		bool L_0 = NetworkViewID_INTERNAL_CALL_Internal_Compare_m1319175441(NULL /*static, unused*/, (&___lhs0), (&___rhs1), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_Compare(UnityEngine.NetworkViewID&,UnityEngine.NetworkViewID&)
extern "C"  bool NetworkViewID_INTERNAL_CALL_Internal_Compare_m1319175441 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436 * ___lhs0, NetworkViewID_t3400394436 * ___rhs1, const MethodInfo* method)
{
	typedef bool (*NetworkViewID_INTERNAL_CALL_Internal_Compare_m1319175441_ftn) (NetworkViewID_t3400394436 *, NetworkViewID_t3400394436 *);
	static NetworkViewID_INTERNAL_CALL_Internal_Compare_m1319175441_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_CALL_Internal_Compare_m1319175441_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_Compare(UnityEngine.NetworkViewID&,UnityEngine.NetworkViewID&)");
	return _il2cpp_icall_func(___lhs0, ___rhs1);
}
// System.Int32 UnityEngine.NetworkViewID::GetHashCode()
extern "C"  int32_t NetworkViewID_GetHashCode_m2420635706 (NetworkViewID_t3400394436 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_a_0();
		int32_t L_1 = __this->get_b_1();
		int32_t L_2 = __this->get_c_2();
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_0^(int32_t)L_1))^(int32_t)L_2));
	}
}
extern "C"  int32_t NetworkViewID_GetHashCode_m2420635706_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkViewID_t3400394436 * _thisAdjusted = reinterpret_cast<NetworkViewID_t3400394436 *>(__this + 1);
	return NetworkViewID_GetHashCode_m2420635706(_thisAdjusted, method);
}
// System.Boolean UnityEngine.NetworkViewID::Equals(System.Object)
extern Il2CppClass* NetworkViewID_t3400394436_il2cpp_TypeInfo_var;
extern const uint32_t NetworkViewID_Equals_m2612049122_MetadataUsageId;
extern "C"  bool NetworkViewID_Equals_m2612049122 (NetworkViewID_t3400394436 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NetworkViewID_Equals_m2612049122_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	NetworkViewID_t3400394436  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, NetworkViewID_t3400394436_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(NetworkViewID_t3400394436 *)((NetworkViewID_t3400394436 *)UnBox (L_1, NetworkViewID_t3400394436_il2cpp_TypeInfo_var))));
		NetworkViewID_t3400394436  L_2 = V_0;
		bool L_3 = NetworkViewID_Internal_Compare_m2541220858(NULL /*static, unused*/, (*(NetworkViewID_t3400394436 *)__this), L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
extern "C"  bool NetworkViewID_Equals_m2612049122_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	NetworkViewID_t3400394436 * _thisAdjusted = reinterpret_cast<NetworkViewID_t3400394436 *>(__this + 1);
	return NetworkViewID_Equals_m2612049122(_thisAdjusted, ___other0, method);
}
// System.Boolean UnityEngine.NetworkViewID::get_isMine()
extern "C"  bool NetworkViewID_get_isMine_m2754407673 (NetworkViewID_t3400394436 * __this, const MethodInfo* method)
{
	{
		bool L_0 = NetworkViewID_Internal_IsMine_m619939661(NULL /*static, unused*/, (*(NetworkViewID_t3400394436 *)__this), /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  bool NetworkViewID_get_isMine_m2754407673_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkViewID_t3400394436 * _thisAdjusted = reinterpret_cast<NetworkViewID_t3400394436 *>(__this + 1);
	return NetworkViewID_get_isMine_m2754407673(_thisAdjusted, method);
}
// UnityEngine.NetworkPlayer UnityEngine.NetworkViewID::get_owner()
extern "C"  NetworkPlayer_t3231273765  NetworkViewID_get_owner_m576069262 (NetworkViewID_t3400394436 * __this, const MethodInfo* method)
{
	NetworkPlayer_t3231273765  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		NetworkViewID_Internal_GetOwner_m874421061(NULL /*static, unused*/, (*(NetworkViewID_t3400394436 *)__this), (&V_0), /*hidden argument*/NULL);
		NetworkPlayer_t3231273765  L_0 = V_0;
		return L_0;
	}
}
extern "C"  NetworkPlayer_t3231273765  NetworkViewID_get_owner_m576069262_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkViewID_t3400394436 * _thisAdjusted = reinterpret_cast<NetworkViewID_t3400394436 *>(__this + 1);
	return NetworkViewID_get_owner_m576069262(_thisAdjusted, method);
}
// System.String UnityEngine.NetworkViewID::ToString()
extern "C"  String_t* NetworkViewID_ToString_m3783538050 (NetworkViewID_t3400394436 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = NetworkViewID_Internal_GetString_m679444096(NULL /*static, unused*/, (*(NetworkViewID_t3400394436 *)__this), /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  String_t* NetworkViewID_ToString_m3783538050_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkViewID_t3400394436 * _thisAdjusted = reinterpret_cast<NetworkViewID_t3400394436 *>(__this + 1);
	return NetworkViewID_ToString_m3783538050(_thisAdjusted, method);
}
// System.Boolean UnityEngine.NetworkViewID::op_Equality(UnityEngine.NetworkViewID,UnityEngine.NetworkViewID)
extern "C"  bool NetworkViewID_op_Equality_m776200489 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436  ___lhs0, NetworkViewID_t3400394436  ___rhs1, const MethodInfo* method)
{
	{
		NetworkViewID_t3400394436  L_0 = ___lhs0;
		NetworkViewID_t3400394436  L_1 = ___rhs1;
		bool L_2 = NetworkViewID_Internal_Compare_m2541220858(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.NetworkViewID::op_Inequality(UnityEngine.NetworkViewID,UnityEngine.NetworkViewID)
extern "C"  bool NetworkViewID_op_Inequality_m4224856356 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436  ___lhs0, NetworkViewID_t3400394436  ___rhs1, const MethodInfo* method)
{
	{
		NetworkViewID_t3400394436  L_0 = ___lhs0;
		NetworkViewID_t3400394436  L_1 = ___rhs1;
		bool L_2 = NetworkViewID_Internal_Compare_m2541220858(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.NetworkViewID
extern "C" void NetworkViewID_t3400394436_marshal_pinvoke(const NetworkViewID_t3400394436& unmarshaled, NetworkViewID_t3400394436_marshaled_pinvoke& marshaled)
{
	marshaled.___a_0 = unmarshaled.get_a_0();
	marshaled.___b_1 = unmarshaled.get_b_1();
	marshaled.___c_2 = unmarshaled.get_c_2();
}
extern "C" void NetworkViewID_t3400394436_marshal_pinvoke_back(const NetworkViewID_t3400394436_marshaled_pinvoke& marshaled, NetworkViewID_t3400394436& unmarshaled)
{
	int32_t unmarshaled_a_temp_0 = 0;
	unmarshaled_a_temp_0 = marshaled.___a_0;
	unmarshaled.set_a_0(unmarshaled_a_temp_0);
	int32_t unmarshaled_b_temp_1 = 0;
	unmarshaled_b_temp_1 = marshaled.___b_1;
	unmarshaled.set_b_1(unmarshaled_b_temp_1);
	int32_t unmarshaled_c_temp_2 = 0;
	unmarshaled_c_temp_2 = marshaled.___c_2;
	unmarshaled.set_c_2(unmarshaled_c_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.NetworkViewID
extern "C" void NetworkViewID_t3400394436_marshal_pinvoke_cleanup(NetworkViewID_t3400394436_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.NetworkViewID
extern "C" void NetworkViewID_t3400394436_marshal_com(const NetworkViewID_t3400394436& unmarshaled, NetworkViewID_t3400394436_marshaled_com& marshaled)
{
	marshaled.___a_0 = unmarshaled.get_a_0();
	marshaled.___b_1 = unmarshaled.get_b_1();
	marshaled.___c_2 = unmarshaled.get_c_2();
}
extern "C" void NetworkViewID_t3400394436_marshal_com_back(const NetworkViewID_t3400394436_marshaled_com& marshaled, NetworkViewID_t3400394436& unmarshaled)
{
	int32_t unmarshaled_a_temp_0 = 0;
	unmarshaled_a_temp_0 = marshaled.___a_0;
	unmarshaled.set_a_0(unmarshaled_a_temp_0);
	int32_t unmarshaled_b_temp_1 = 0;
	unmarshaled_b_temp_1 = marshaled.___b_1;
	unmarshaled.set_b_1(unmarshaled_b_temp_1);
	int32_t unmarshaled_c_temp_2 = 0;
	unmarshaled_c_temp_2 = marshaled.___c_2;
	unmarshaled.set_c_2(unmarshaled_c_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.NetworkViewID
extern "C" void NetworkViewID_t3400394436_marshal_com_cleanup(NetworkViewID_t3400394436_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Object::.ctor()
extern "C"  void Object__ctor_m570634126 (Object_t3071478659 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)
extern "C"  Object_t3071478659 * Object_Internal_CloneSingle_m3129073756 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___data0, const MethodInfo* method)
{
	typedef Object_t3071478659 * (*Object_Internal_CloneSingle_m3129073756_ftn) (Object_t3071478659 *);
	static Object_Internal_CloneSingle_m3129073756_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Internal_CloneSingle_m3129073756_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)");
	return _il2cpp_icall_func(___data0);
}
// UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Object_t3071478659 * Object_Internal_InstantiateSingle_m3047563925 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___data0, Vector3_t4282066566  ___pos1, Quaternion_t1553702882  ___rot2, const MethodInfo* method)
{
	{
		Object_t3071478659 * L_0 = ___data0;
		Object_t3071478659 * L_1 = Object_INTERNAL_CALL_Internal_InstantiateSingle_m1201424140(NULL /*static, unused*/, L_0, (&___pos1), (&___rot2), /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Object UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  Object_t3071478659 * Object_INTERNAL_CALL_Internal_InstantiateSingle_m1201424140 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___data0, Vector3_t4282066566 * ___pos1, Quaternion_t1553702882 * ___rot2, const MethodInfo* method)
{
	typedef Object_t3071478659 * (*Object_INTERNAL_CALL_Internal_InstantiateSingle_m1201424140_ftn) (Object_t3071478659 *, Vector3_t4282066566 *, Quaternion_t1553702882 *);
	static Object_INTERNAL_CALL_Internal_InstantiateSingle_m1201424140_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_INTERNAL_CALL_Internal_InstantiateSingle_m1201424140_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	return _il2cpp_icall_func(___data0, ___pos1, ___rot2);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
extern "C"  void Object_Destroy_m2260435093 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___obj0, float ___t1, const MethodInfo* method)
{
	typedef void (*Object_Destroy_m2260435093_ftn) (Object_t3071478659 *, float);
	static Object_Destroy_m2260435093_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Destroy_m2260435093_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)");
	_il2cpp_icall_func(___obj0, ___t1);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m176400816 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___obj0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (0.0f);
		Object_t3071478659 * L_0 = ___obj0;
		float L_1 = V_0;
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)
extern "C"  void Object_DestroyImmediate_m1826427014 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___obj0, bool ___allowDestroyingAssets1, const MethodInfo* method)
{
	typedef void (*Object_DestroyImmediate_m1826427014_ftn) (Object_t3071478659 *, bool);
	static Object_DestroyImmediate_m1826427014_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DestroyImmediate_m1826427014_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)");
	_il2cpp_icall_func(___obj0, ___allowDestroyingAssets1);
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
extern "C"  void Object_DestroyImmediate_m349958679 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___obj0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		Object_t3071478659 * L_0 = ___obj0;
		bool L_1 = V_0;
		Object_DestroyImmediate_m1826427014(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object[] UnityEngine.Object::FindObjectsOfType(System.Type)
extern "C"  ObjectU5BU5D_t1015136018* Object_FindObjectsOfType_m975740280 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t1015136018* (*Object_FindObjectsOfType_m975740280_ftn) (Type_t *);
	static Object_FindObjectsOfType_m975740280_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_FindObjectsOfType_m975740280_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::FindObjectsOfType(System.Type)");
	return _il2cpp_icall_func(___type0);
}
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m3709440845 (Object_t3071478659 * __this, const MethodInfo* method)
{
	typedef String_t* (*Object_get_name_m3709440845_ftn) (Object_t3071478659 *);
	static Object_get_name_m3709440845_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_get_name_m3709440845_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::get_name()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Object::set_name(System.String)
extern "C"  void Object_set_name_m1123518500 (Object_t3071478659 * __this, String_t* ___value0, const MethodInfo* method)
{
	typedef void (*Object_set_name_m1123518500_ftn) (Object_t3071478659 *, String_t*);
	static Object_set_name_m1123518500_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_name_m1123518500_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_name(System.String)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C"  void Object_DontDestroyOnLoad_m4064482788 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___target0, const MethodInfo* method)
{
	typedef void (*Object_DontDestroyOnLoad_m4064482788_ftn) (Object_t3071478659 *);
	static Object_DontDestroyOnLoad_m4064482788_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DontDestroyOnLoad_m4064482788_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)");
	_il2cpp_icall_func(___target0);
}
// UnityEngine.HideFlags UnityEngine.Object::get_hideFlags()
extern "C"  int32_t Object_get_hideFlags_m1893459363 (Object_t3071478659 * __this, const MethodInfo* method)
{
	typedef int32_t (*Object_get_hideFlags_m1893459363_ftn) (Object_t3071478659 *);
	static Object_get_hideFlags_m1893459363_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_get_hideFlags_m1893459363_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::get_hideFlags()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C"  void Object_set_hideFlags_m41317712 (Object_t3071478659 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Object_set_hideFlags_m41317712_ftn) (Object_t3071478659 *, int32_t);
	static Object_set_hideFlags_m41317712_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_hideFlags_m41317712_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Object::DestroyObject(UnityEngine.Object,System.Single)
extern "C"  void Object_DestroyObject_m3324336244 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___obj0, float ___t1, const MethodInfo* method)
{
	typedef void (*Object_DestroyObject_m3324336244_ftn) (Object_t3071478659 *, float);
	static Object_DestroyObject_m3324336244_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DestroyObject_m3324336244_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DestroyObject(UnityEngine.Object,System.Single)");
	_il2cpp_icall_func(___obj0, ___t1);
}
// System.Void UnityEngine.Object::DestroyObject(UnityEngine.Object)
extern "C"  void Object_DestroyObject_m3900253135 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___obj0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (0.0f);
		Object_t3071478659 * L_0 = ___obj0;
		float L_1 = V_0;
		Object_DestroyObject_m3324336244(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object[] UnityEngine.Object::FindSceneObjectsOfType(System.Type)
extern "C"  ObjectU5BU5D_t1015136018* Object_FindSceneObjectsOfType_m2168852346 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t1015136018* (*Object_FindSceneObjectsOfType_m2168852346_ftn) (Type_t *);
	static Object_FindSceneObjectsOfType_m2168852346_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_FindSceneObjectsOfType_m2168852346_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::FindSceneObjectsOfType(System.Type)");
	return _il2cpp_icall_func(___type0);
}
// UnityEngine.Object[] UnityEngine.Object::FindObjectsOfTypeIncludingAssets(System.Type)
extern "C"  ObjectU5BU5D_t1015136018* Object_FindObjectsOfTypeIncludingAssets_m1276784656 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t1015136018* (*Object_FindObjectsOfTypeIncludingAssets_m1276784656_ftn) (Type_t *);
	static Object_FindObjectsOfTypeIncludingAssets_m1276784656_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_FindObjectsOfTypeIncludingAssets_m1276784656_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::FindObjectsOfTypeIncludingAssets(System.Type)");
	return _il2cpp_icall_func(___type0);
}
// System.String UnityEngine.Object::ToString()
extern "C"  String_t* Object_ToString_m2155033093 (Object_t3071478659 * __this, const MethodInfo* method)
{
	typedef String_t* (*Object_ToString_m2155033093_ftn) (Object_t3071478659 *);
	static Object_ToString_m2155033093_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_ToString_m2155033093_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::ToString()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Object::DoesObjectWithInstanceIDExist(System.Int32)
extern "C"  bool Object_DoesObjectWithInstanceIDExist_m3762691104 (Il2CppObject * __this /* static, unused */, int32_t ___instanceID0, const MethodInfo* method)
{
	typedef bool (*Object_DoesObjectWithInstanceIDExist_m3762691104_ftn) (int32_t);
	static Object_DoesObjectWithInstanceIDExist_m3762691104_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DoesObjectWithInstanceIDExist_m3762691104_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DoesObjectWithInstanceIDExist(System.Int32)");
	return _il2cpp_icall_func(___instanceID0);
}
// System.Boolean UnityEngine.Object::Equals(System.Object)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t Object_Equals_m1697651929_MetadataUsageId;
extern "C"  bool Object_Equals_m1697651929 (Object_t3071478659 * __this, Il2CppObject * ___o0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_Equals_m1697651929_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___o0;
		bool L_1 = Object_CompareBaseObjects_m2625210252(NULL /*static, unused*/, __this, ((Object_t3071478659 *)IsInstClass(L_0, Object_t3071478659_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 UnityEngine.Object::GetHashCode()
extern "C"  int32_t Object_GetHashCode_m1758859581 (Object_t3071478659 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Object_GetInstanceID_m200424466(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.Object::CompareBaseObjects(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_CompareBaseObjects_m2625210252 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___lhs0, Object_t3071478659 * ___rhs1, const MethodInfo* method)
{
	bool V_0 = false;
	bool V_1 = false;
	{
		Object_t3071478659 * L_0 = ___lhs0;
		V_0 = (bool)((((Il2CppObject*)(Object_t3071478659 *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		Object_t3071478659 * L_1 = ___rhs1;
		V_1 = (bool)((((Il2CppObject*)(Object_t3071478659 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_2 = V_1;
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		return (bool)1;
	}

IL_0018:
	{
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0028;
		}
	}
	{
		Object_t3071478659 * L_5 = ___lhs0;
		bool L_6 = Object_IsNativeObjectAlive_m434626365(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
	}

IL_0028:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0038;
		}
	}
	{
		Object_t3071478659 * L_8 = ___rhs1;
		bool L_9 = Object_IsNativeObjectAlive_m434626365(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_9) == ((int32_t)0))? 1 : 0);
	}

IL_0038:
	{
		Object_t3071478659 * L_10 = ___lhs0;
		NullCheck(L_10);
		int32_t L_11 = L_10->get_m_InstanceID_0();
		Object_t3071478659 * L_12 = ___rhs1;
		NullCheck(L_12);
		int32_t L_13 = L_12->get_m_InstanceID_0();
		return (bool)((((int32_t)L_11) == ((int32_t)L_13))? 1 : 0);
	}
}
// System.Boolean UnityEngine.Object::IsNativeObjectAlive(UnityEngine.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t Object_IsNativeObjectAlive_m434626365_MetadataUsageId;
extern "C"  bool Object_IsNativeObjectAlive_m434626365 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___o0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_IsNativeObjectAlive_m434626365_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t3071478659 * L_0 = ___o0;
		NullCheck(L_0);
		IntPtr_t L_1 = Object_GetCachedPtr_m1583421857(L_0, /*hidden argument*/NULL);
		IntPtr_t L_2 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_3 = IntPtr_op_Inequality_m10053967(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 UnityEngine.Object::GetInstanceID()
extern "C"  int32_t Object_GetInstanceID_m200424466 (Object_t3071478659 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_InstanceID_0();
		return L_0;
	}
}
// System.IntPtr UnityEngine.Object::GetCachedPtr()
extern "C"  IntPtr_t Object_GetCachedPtr_m1583421857 (Object_t3071478659 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = __this->get_m_CachedPtr_1();
		return L_0;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern Il2CppCodeGenString* _stringLiteral3473406;
extern const uint32_t Object_Instantiate_m2255090103_MetadataUsageId;
extern "C"  Object_t3071478659 * Object_Instantiate_m2255090103 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___original0, Vector3_t4282066566  ___position1, Quaternion_t1553702882  ___rotation2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m2255090103_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t3071478659 * L_0 = ___original0;
		Object_CheckNullArgument_m264735768(NULL /*static, unused*/, L_0, _stringLiteral3473406, /*hidden argument*/NULL);
		Object_t3071478659 * L_1 = ___original0;
		Vector3_t4282066566  L_2 = ___position1;
		Quaternion_t1553702882  L_3 = ___rotation2;
		Object_t3071478659 * L_4 = Object_Internal_InstantiateSingle_m3047563925(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object)
extern Il2CppCodeGenString* _stringLiteral3473406;
extern const uint32_t Object_Instantiate_m3040600263_MetadataUsageId;
extern "C"  Object_t3071478659 * Object_Instantiate_m3040600263 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___original0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m3040600263_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t3071478659 * L_0 = ___original0;
		Object_CheckNullArgument_m264735768(NULL /*static, unused*/, L_0, _stringLiteral3473406, /*hidden argument*/NULL);
		Object_t3071478659 * L_1 = ___original0;
		Object_t3071478659 * L_2 = Object_Internal_CloneSingle_m3129073756(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Object::CheckNullArgument(System.Object,System.String)
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern const uint32_t Object_CheckNullArgument_m264735768_MetadataUsageId;
extern "C"  void Object_CheckNullArgument_m264735768 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, String_t* ___message1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_CheckNullArgument_m264735768_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___arg0;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		String_t* L_1 = ___message1;
		ArgumentException_t928607144 * L_2 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_000d:
	{
		return;
	}
}
// UnityEngine.Object UnityEngine.Object::FindObjectOfType(System.Type)
extern "C"  Object_t3071478659 * Object_FindObjectOfType_m3820159265 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	ObjectU5BU5D_t1015136018* V_0 = NULL;
	{
		Type_t * L_0 = ___type0;
		ObjectU5BU5D_t1015136018* L_1 = Object_FindObjectsOfType_m975740280(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ObjectU5BU5D_t1015136018* L_2 = V_0;
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		ObjectU5BU5D_t1015136018* L_3 = V_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		int32_t L_4 = 0;
		Object_t3071478659 * L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		return L_5;
	}

IL_0014:
	{
		return (Object_t3071478659 *)NULL;
	}
}
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m2106766291 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___exists0, const MethodInfo* method)
{
	{
		Object_t3071478659 * L_0 = ___exists0;
		bool L_1 = Object_CompareBaseObjects_m2625210252(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m3964590952 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___x0, Object_t3071478659 * ___y1, const MethodInfo* method)
{
	{
		Object_t3071478659 * L_0 = ___x0;
		Object_t3071478659 * L_1 = ___y1;
		bool L_2 = Object_CompareBaseObjects_m2625210252(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m1296218211 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___x0, Object_t3071478659 * ___y1, const MethodInfo* method)
{
	{
		Object_t3071478659 * L_0 = ___x0;
		Object_t3071478659 * L_1 = ___y1;
		bool L_2 = Object_CompareBaseObjects_m2625210252(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.Object
extern "C" void Object_t3071478659_marshal_pinvoke(const Object_t3071478659& unmarshaled, Object_t3071478659_marshaled_pinvoke& marshaled)
{
	marshaled.___m_InstanceID_0 = unmarshaled.get_m_InstanceID_0();
	marshaled.___m_CachedPtr_1 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_1()).get_m_value_0());
}
extern "C" void Object_t3071478659_marshal_pinvoke_back(const Object_t3071478659_marshaled_pinvoke& marshaled, Object_t3071478659& unmarshaled)
{
	int32_t unmarshaled_m_InstanceID_temp_0 = 0;
	unmarshaled_m_InstanceID_temp_0 = marshaled.___m_InstanceID_0;
	unmarshaled.set_m_InstanceID_0(unmarshaled_m_InstanceID_temp_0);
	IntPtr_t unmarshaled_m_CachedPtr_temp_1;
	memset(&unmarshaled_m_CachedPtr_temp_1, 0, sizeof(unmarshaled_m_CachedPtr_temp_1));
	IntPtr_t unmarshaled_m_CachedPtr_temp_1_temp;
	unmarshaled_m_CachedPtr_temp_1_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_CachedPtr_1));
	unmarshaled_m_CachedPtr_temp_1 = unmarshaled_m_CachedPtr_temp_1_temp;
	unmarshaled.set_m_CachedPtr_1(unmarshaled_m_CachedPtr_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Object
extern "C" void Object_t3071478659_marshal_pinvoke_cleanup(Object_t3071478659_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Object
extern "C" void Object_t3071478659_marshal_com(const Object_t3071478659& unmarshaled, Object_t3071478659_marshaled_com& marshaled)
{
	marshaled.___m_InstanceID_0 = unmarshaled.get_m_InstanceID_0();
	marshaled.___m_CachedPtr_1 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_1()).get_m_value_0());
}
extern "C" void Object_t3071478659_marshal_com_back(const Object_t3071478659_marshaled_com& marshaled, Object_t3071478659& unmarshaled)
{
	int32_t unmarshaled_m_InstanceID_temp_0 = 0;
	unmarshaled_m_InstanceID_temp_0 = marshaled.___m_InstanceID_0;
	unmarshaled.set_m_InstanceID_0(unmarshaled_m_InstanceID_temp_0);
	IntPtr_t unmarshaled_m_CachedPtr_temp_1;
	memset(&unmarshaled_m_CachedPtr_temp_1, 0, sizeof(unmarshaled_m_CachedPtr_temp_1));
	IntPtr_t unmarshaled_m_CachedPtr_temp_1_temp;
	unmarshaled_m_CachedPtr_temp_1_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_CachedPtr_1));
	unmarshaled_m_CachedPtr_temp_1 = unmarshaled_m_CachedPtr_temp_1_temp;
	unmarshaled.set_m_CachedPtr_1(unmarshaled_m_CachedPtr_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Object
extern "C" void Object_t3071478659_marshal_com_cleanup(Object_t3071478659_marshaled_com& marshaled)
{
}
// UnityEngine.Vector3 UnityEngine.ParticleCollisionEvent::get_intersection()
extern "C"  Vector3_t4282066566  ParticleCollisionEvent_get_intersection_m1127029137 (ParticleCollisionEvent_t2700926194 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_m_Intersection_0();
		return L_0;
	}
}
extern "C"  Vector3_t4282066566  ParticleCollisionEvent_get_intersection_m1127029137_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ParticleCollisionEvent_t2700926194 * _thisAdjusted = reinterpret_cast<ParticleCollisionEvent_t2700926194 *>(__this + 1);
	return ParticleCollisionEvent_get_intersection_m1127029137(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.ParticleCollisionEvent::get_normal()
extern "C"  Vector3_t4282066566  ParticleCollisionEvent_get_normal_m65277359 (ParticleCollisionEvent_t2700926194 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_m_Normal_1();
		return L_0;
	}
}
extern "C"  Vector3_t4282066566  ParticleCollisionEvent_get_normal_m65277359_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ParticleCollisionEvent_t2700926194 * _thisAdjusted = reinterpret_cast<ParticleCollisionEvent_t2700926194 *>(__this + 1);
	return ParticleCollisionEvent_get_normal_m65277359(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.ParticleCollisionEvent::get_velocity()
extern "C"  Vector3_t4282066566  ParticleCollisionEvent_get_velocity_m1889477221 (ParticleCollisionEvent_t2700926194 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_m_Velocity_2();
		return L_0;
	}
}
extern "C"  Vector3_t4282066566  ParticleCollisionEvent_get_velocity_m1889477221_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ParticleCollisionEvent_t2700926194 * _thisAdjusted = reinterpret_cast<ParticleCollisionEvent_t2700926194 *>(__this + 1);
	return ParticleCollisionEvent_get_velocity_m1889477221(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.ParticleCollisionEvent
extern "C" void ParticleCollisionEvent_t2700926194_marshal_pinvoke(const ParticleCollisionEvent_t2700926194& unmarshaled, ParticleCollisionEvent_t2700926194_marshaled_pinvoke& marshaled)
{
	Vector3_t4282066566_marshal_pinvoke(unmarshaled.get_m_Intersection_0(), marshaled.___m_Intersection_0);
	Vector3_t4282066566_marshal_pinvoke(unmarshaled.get_m_Normal_1(), marshaled.___m_Normal_1);
	Vector3_t4282066566_marshal_pinvoke(unmarshaled.get_m_Velocity_2(), marshaled.___m_Velocity_2);
	marshaled.___m_ColliderInstanceID_3 = unmarshaled.get_m_ColliderInstanceID_3();
}
extern "C" void ParticleCollisionEvent_t2700926194_marshal_pinvoke_back(const ParticleCollisionEvent_t2700926194_marshaled_pinvoke& marshaled, ParticleCollisionEvent_t2700926194& unmarshaled)
{
	Vector3_t4282066566  unmarshaled_m_Intersection_temp_0;
	memset(&unmarshaled_m_Intersection_temp_0, 0, sizeof(unmarshaled_m_Intersection_temp_0));
	Vector3_t4282066566_marshal_pinvoke_back(marshaled.___m_Intersection_0, unmarshaled_m_Intersection_temp_0);
	unmarshaled.set_m_Intersection_0(unmarshaled_m_Intersection_temp_0);
	Vector3_t4282066566  unmarshaled_m_Normal_temp_1;
	memset(&unmarshaled_m_Normal_temp_1, 0, sizeof(unmarshaled_m_Normal_temp_1));
	Vector3_t4282066566_marshal_pinvoke_back(marshaled.___m_Normal_1, unmarshaled_m_Normal_temp_1);
	unmarshaled.set_m_Normal_1(unmarshaled_m_Normal_temp_1);
	Vector3_t4282066566  unmarshaled_m_Velocity_temp_2;
	memset(&unmarshaled_m_Velocity_temp_2, 0, sizeof(unmarshaled_m_Velocity_temp_2));
	Vector3_t4282066566_marshal_pinvoke_back(marshaled.___m_Velocity_2, unmarshaled_m_Velocity_temp_2);
	unmarshaled.set_m_Velocity_2(unmarshaled_m_Velocity_temp_2);
	int32_t unmarshaled_m_ColliderInstanceID_temp_3 = 0;
	unmarshaled_m_ColliderInstanceID_temp_3 = marshaled.___m_ColliderInstanceID_3;
	unmarshaled.set_m_ColliderInstanceID_3(unmarshaled_m_ColliderInstanceID_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleCollisionEvent
extern "C" void ParticleCollisionEvent_t2700926194_marshal_pinvoke_cleanup(ParticleCollisionEvent_t2700926194_marshaled_pinvoke& marshaled)
{
	Vector3_t4282066566_marshal_pinvoke_cleanup(marshaled.___m_Intersection_0);
	Vector3_t4282066566_marshal_pinvoke_cleanup(marshaled.___m_Normal_1);
	Vector3_t4282066566_marshal_pinvoke_cleanup(marshaled.___m_Velocity_2);
}
// Conversion methods for marshalling of: UnityEngine.ParticleCollisionEvent
extern "C" void ParticleCollisionEvent_t2700926194_marshal_com(const ParticleCollisionEvent_t2700926194& unmarshaled, ParticleCollisionEvent_t2700926194_marshaled_com& marshaled)
{
	Vector3_t4282066566_marshal_com(unmarshaled.get_m_Intersection_0(), marshaled.___m_Intersection_0);
	Vector3_t4282066566_marshal_com(unmarshaled.get_m_Normal_1(), marshaled.___m_Normal_1);
	Vector3_t4282066566_marshal_com(unmarshaled.get_m_Velocity_2(), marshaled.___m_Velocity_2);
	marshaled.___m_ColliderInstanceID_3 = unmarshaled.get_m_ColliderInstanceID_3();
}
extern "C" void ParticleCollisionEvent_t2700926194_marshal_com_back(const ParticleCollisionEvent_t2700926194_marshaled_com& marshaled, ParticleCollisionEvent_t2700926194& unmarshaled)
{
	Vector3_t4282066566  unmarshaled_m_Intersection_temp_0;
	memset(&unmarshaled_m_Intersection_temp_0, 0, sizeof(unmarshaled_m_Intersection_temp_0));
	Vector3_t4282066566_marshal_com_back(marshaled.___m_Intersection_0, unmarshaled_m_Intersection_temp_0);
	unmarshaled.set_m_Intersection_0(unmarshaled_m_Intersection_temp_0);
	Vector3_t4282066566  unmarshaled_m_Normal_temp_1;
	memset(&unmarshaled_m_Normal_temp_1, 0, sizeof(unmarshaled_m_Normal_temp_1));
	Vector3_t4282066566_marshal_com_back(marshaled.___m_Normal_1, unmarshaled_m_Normal_temp_1);
	unmarshaled.set_m_Normal_1(unmarshaled_m_Normal_temp_1);
	Vector3_t4282066566  unmarshaled_m_Velocity_temp_2;
	memset(&unmarshaled_m_Velocity_temp_2, 0, sizeof(unmarshaled_m_Velocity_temp_2));
	Vector3_t4282066566_marshal_com_back(marshaled.___m_Velocity_2, unmarshaled_m_Velocity_temp_2);
	unmarshaled.set_m_Velocity_2(unmarshaled_m_Velocity_temp_2);
	int32_t unmarshaled_m_ColliderInstanceID_temp_3 = 0;
	unmarshaled_m_ColliderInstanceID_temp_3 = marshaled.___m_ColliderInstanceID_3;
	unmarshaled.set_m_ColliderInstanceID_3(unmarshaled_m_ColliderInstanceID_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleCollisionEvent
extern "C" void ParticleCollisionEvent_t2700926194_marshal_com_cleanup(ParticleCollisionEvent_t2700926194_marshaled_com& marshaled)
{
	Vector3_t4282066566_marshal_com_cleanup(marshaled.___m_Intersection_0);
	Vector3_t4282066566_marshal_com_cleanup(marshaled.___m_Normal_1);
	Vector3_t4282066566_marshal_com_cleanup(marshaled.___m_Velocity_2);
}
// System.Int32 UnityEngine.ParticlePhysicsExtensions::GetSafeCollisionEventSize(UnityEngine.ParticleSystem)
extern "C"  int32_t ParticlePhysicsExtensions_GetSafeCollisionEventSize_m2314153154 (Il2CppObject * __this /* static, unused */, ParticleSystem_t381473177 * ___ps0, const MethodInfo* method)
{
	{
		ParticleSystem_t381473177 * L_0 = ___ps0;
		int32_t L_1 = ParticleSystemExtensionsImpl_GetSafeCollisionEventSize_m1273158050(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 UnityEngine.ParticlePhysicsExtensions::GetCollisionEvents(UnityEngine.ParticleSystem,UnityEngine.GameObject,UnityEngine.ParticleCollisionEvent[])
extern "C"  int32_t ParticlePhysicsExtensions_GetCollisionEvents_m302976490 (Il2CppObject * __this /* static, unused */, ParticleSystem_t381473177 * ___ps0, GameObject_t3674682005 * ___go1, ParticleCollisionEventU5BU5D_t4168492807* ___collisionEvents2, const MethodInfo* method)
{
	{
		ParticleSystem_t381473177 * L_0 = ___ps0;
		GameObject_t3674682005 * L_1 = ___go1;
		ParticleCollisionEventU5BU5D_t4168492807* L_2 = ___collisionEvents2;
		int32_t L_3 = ParticleSystemExtensionsImpl_GetCollisionEvents_m1931324682(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.ParticleSystem::get_isStopped()
extern "C"  bool ParticleSystem_get_isStopped_m3698458602 (ParticleSystem_t381473177 * __this, const MethodInfo* method)
{
	typedef bool (*ParticleSystem_get_isStopped_m3698458602_ftn) (ParticleSystem_t381473177 *);
	static ParticleSystem_get_isStopped_m3698458602_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_isStopped_m3698458602_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_isStopped()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.ParticleSystem/IteratorDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void IteratorDelegate__ctor_m2534678614 (IteratorDelegate_t4269758102 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean UnityEngine.ParticleSystem/IteratorDelegate::Invoke(UnityEngine.ParticleSystem)
extern "C"  bool IteratorDelegate_Invoke_m3641019902 (IteratorDelegate_t4269758102 * __this, ParticleSystem_t381473177 * ___ps0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		IteratorDelegate_Invoke_m3641019902((IteratorDelegate_t4269758102 *)__this->get_prev_9(),___ps0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, ParticleSystem_t381473177 * ___ps0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___ps0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (void* __this, ParticleSystem_t381473177 * ___ps0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___ps0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___ps0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.ParticleSystem/IteratorDelegate::BeginInvoke(UnityEngine.ParticleSystem,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * IteratorDelegate_BeginInvoke_m3274238639 (IteratorDelegate_t4269758102 * __this, ParticleSystem_t381473177 * ___ps0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___ps0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean UnityEngine.ParticleSystem/IteratorDelegate::EndInvoke(System.IAsyncResult)
extern "C"  bool IteratorDelegate_EndInvoke_m3581459136 (IteratorDelegate_t4269758102 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Int32 UnityEngine.ParticleSystemExtensionsImpl::GetSafeCollisionEventSize(UnityEngine.ParticleSystem)
extern "C"  int32_t ParticleSystemExtensionsImpl_GetSafeCollisionEventSize_m1273158050 (Il2CppObject * __this /* static, unused */, ParticleSystem_t381473177 * ___ps0, const MethodInfo* method)
{
	typedef int32_t (*ParticleSystemExtensionsImpl_GetSafeCollisionEventSize_m1273158050_ftn) (ParticleSystem_t381473177 *);
	static ParticleSystemExtensionsImpl_GetSafeCollisionEventSize_m1273158050_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemExtensionsImpl_GetSafeCollisionEventSize_m1273158050_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemExtensionsImpl::GetSafeCollisionEventSize(UnityEngine.ParticleSystem)");
	return _il2cpp_icall_func(___ps0);
}
// System.Int32 UnityEngine.ParticleSystemExtensionsImpl::GetCollisionEvents(UnityEngine.ParticleSystem,UnityEngine.GameObject,UnityEngine.ParticleCollisionEvent[])
extern "C"  int32_t ParticleSystemExtensionsImpl_GetCollisionEvents_m1931324682 (Il2CppObject * __this /* static, unused */, ParticleSystem_t381473177 * ___ps0, GameObject_t3674682005 * ___go1, ParticleCollisionEventU5BU5D_t4168492807* ___collisionEvents2, const MethodInfo* method)
{
	typedef int32_t (*ParticleSystemExtensionsImpl_GetCollisionEvents_m1931324682_ftn) (ParticleSystem_t381473177 *, GameObject_t3674682005 *, ParticleCollisionEventU5BU5D_t4168492807*);
	static ParticleSystemExtensionsImpl_GetCollisionEvents_m1931324682_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemExtensionsImpl_GetCollisionEvents_m1931324682_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemExtensionsImpl::GetCollisionEvents(UnityEngine.ParticleSystem,UnityEngine.GameObject,UnityEngine.ParticleCollisionEvent[])");
	return _il2cpp_icall_func(___ps0, ___go1, ___collisionEvents2);
}
// System.Void UnityEngine.Physics::set_gravity(UnityEngine.Vector3)
extern "C"  void Physics_set_gravity_m2814881048 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		Physics_INTERNAL_set_gravity_m2886173364(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Physics::INTERNAL_set_gravity(UnityEngine.Vector3&)
extern "C"  void Physics_INTERNAL_set_gravity_m2886173364 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Physics_INTERNAL_set_gravity_m2886173364_ftn) (Vector3_t4282066566 *);
	static Physics_INTERNAL_set_gravity_m2886173364_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_set_gravity_m2886173364_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_set_gravity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C"  bool Physics_Raycast_m267364350 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___direction1, RaycastHit_t4003175726 * ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		Vector3_t4282066566  L_0 = ___origin0;
		Vector3_t4282066566  L_1 = ___direction1;
		RaycastHit_t4003175726 * L_2 = ___hitInfo2;
		float L_3 = ___maxDistance3;
		int32_t L_4 = ___layerMask4;
		int32_t L_5 = V_0;
		bool L_6 = Physics_Raycast_m1758069759(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m1758069759 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___direction1, RaycastHit_t4003175726 * ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = ___origin0;
		Vector3_t4282066566  L_1 = ___direction1;
		RaycastHit_t4003175726 * L_2 = ___hitInfo2;
		float L_3 = ___maxDistance3;
		int32_t L_4 = ___layerMask4;
		int32_t L_5 = ___queryTriggerInteraction5;
		bool L_6 = Physics_Internal_Raycast_m3365413907(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C"  bool Physics_Raycast_m1600345803 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, RaycastHit_t4003175726 * ___hitInfo1, float ___maxDistance2, int32_t ___layerMask3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		Ray_t3134616544  L_0 = ___ray0;
		RaycastHit_t4003175726 * L_1 = ___hitInfo1;
		float L_2 = ___maxDistance2;
		int32_t L_3 = ___layerMask3;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m165875788(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single)
extern "C"  bool Physics_Raycast_m1235528076 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, RaycastHit_t4003175726 * ___hitInfo1, float ___maxDistance2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		Ray_t3134616544  L_0 = ___ray0;
		RaycastHit_t4003175726 * L_1 = ___hitInfo1;
		float L_2 = ___maxDistance2;
		int32_t L_3 = V_1;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m165875788(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&)
extern "C"  bool Physics_Raycast_m1343340263 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, RaycastHit_t4003175726 * ___hitInfo1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		V_2 = (std::numeric_limits<float>::infinity());
		Ray_t3134616544  L_0 = ___ray0;
		RaycastHit_t4003175726 * L_1 = ___hitInfo1;
		float L_2 = V_2;
		int32_t L_3 = V_1;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m165875788(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m165875788 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, RaycastHit_t4003175726 * ___hitInfo1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = Ray_get_origin_m3064983562((&___ray0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_1 = Ray_get_direction_m3201866877((&___ray0), /*hidden argument*/NULL);
		RaycastHit_t4003175726 * L_2 = ___hitInfo1;
		float L_3 = ___maxDistance2;
		int32_t L_4 = ___layerMask3;
		int32_t L_5 = ___queryTriggerInteraction4;
		bool L_6 = Physics_Raycast_m1758069759(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_RaycastAll_m1771931441 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___maxDistance1, int32_t ___layerMask2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		Ray_t3134616544  L_0 = ___ray0;
		float L_1 = ___maxDistance1;
		int32_t L_2 = ___layerMask2;
		int32_t L_3 = V_0;
		RaycastHitU5BU5D_t528650843* L_4 = Physics_RaycastAll_m1269007794(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_RaycastAll_m1269007794 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___maxDistance1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = Ray_get_origin_m3064983562((&___ray0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_1 = Ray_get_direction_m3201866877((&___ray0), /*hidden argument*/NULL);
		float L_2 = ___maxDistance1;
		int32_t L_3 = ___layerMask2;
		int32_t L_4 = ___queryTriggerInteraction3;
		RaycastHitU5BU5D_t528650843* L_5 = Physics_RaycastAll_m892728677(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_RaycastAll_m892728677 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	{
		float L_0 = ___maxDistance2;
		int32_t L_1 = ___layermask3;
		int32_t L_2 = ___queryTriggerInteraction4;
		RaycastHitU5BU5D_t528650843* L_3 = Physics_INTERNAL_CALL_RaycastAll_m2642095530(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_INTERNAL_CALL_RaycastAll_m2642095530 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___origin0, Vector3_t4282066566 * ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	typedef RaycastHitU5BU5D_t528650843* (*Physics_INTERNAL_CALL_RaycastAll_m2642095530_ftn) (Vector3_t4282066566 *, Vector3_t4282066566 *, float, int32_t, int32_t);
	static Physics_INTERNAL_CALL_RaycastAll_m2642095530_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_RaycastAll_m2642095530_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	return _il2cpp_icall_func(___origin0, ___direction1, ___maxDistance2, ___layermask3, ___queryTriggerInteraction4);
}
// UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere(UnityEngine.Vector3,System.Single)
extern "C"  ColliderU5BU5D_t2697150633* Physics_OverlapSphere_m359079608 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___position0, float ___radius1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = (-1);
		float L_0 = ___radius1;
		int32_t L_1 = V_1;
		int32_t L_2 = V_0;
		ColliderU5BU5D_t2697150633* L_3 = Physics_INTERNAL_CALL_OverlapSphere_m4255329177(NULL /*static, unused*/, (&___position0), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Collider[] UnityEngine.Physics::INTERNAL_CALL_OverlapSphere(UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  ColliderU5BU5D_t2697150633* Physics_INTERNAL_CALL_OverlapSphere_m4255329177 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___position0, float ___radius1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const MethodInfo* method)
{
	typedef ColliderU5BU5D_t2697150633* (*Physics_INTERNAL_CALL_OverlapSphere_m4255329177_ftn) (Vector3_t4282066566 *, float, int32_t, int32_t);
	static Physics_INTERNAL_CALL_OverlapSphere_m4255329177_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_OverlapSphere_m4255329177_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_OverlapSphere(UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	return _il2cpp_icall_func(___position0, ___radius1, ___layerMask2, ___queryTriggerInteraction3);
}
// System.Boolean UnityEngine.Physics::Internal_Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Internal_Raycast_m3365413907 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___direction1, RaycastHit_t4003175726 * ___hitInfo2, float ___maxDistance3, int32_t ___layermask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method)
{
	{
		RaycastHit_t4003175726 * L_0 = ___hitInfo2;
		float L_1 = ___maxDistance3;
		int32_t L_2 = ___layermask4;
		int32_t L_3 = ___queryTriggerInteraction5;
		bool L_4 = Physics_INTERNAL_CALL_Internal_Raycast_m1291554392(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_INTERNAL_CALL_Internal_Raycast_m1291554392 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___origin0, Vector3_t4282066566 * ___direction1, RaycastHit_t4003175726 * ___hitInfo2, float ___maxDistance3, int32_t ___layermask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method)
{
	typedef bool (*Physics_INTERNAL_CALL_Internal_Raycast_m1291554392_ftn) (Vector3_t4282066566 *, Vector3_t4282066566 *, RaycastHit_t4003175726 *, float, int32_t, int32_t);
	static Physics_INTERNAL_CALL_Internal_Raycast_m1291554392_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_Internal_Raycast_m1291554392_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	return _il2cpp_icall_func(___origin0, ___direction1, ___hitInfo2, ___maxDistance3, ___layermask4, ___queryTriggerInteraction5);
}
// System.Void UnityEngine.Physics2D::.cctor()
extern Il2CppClass* List_1_t3111957221_il2cpp_TypeInfo_var;
extern Il2CppClass* Physics2D_t9846735_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1832106448_MethodInfo_var;
extern const uint32_t Physics2D__cctor_m2087591309_MetadataUsageId;
extern "C"  void Physics2D__cctor_m2087591309 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D__cctor_m2087591309_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t3111957221 * L_0 = (List_1_t3111957221 *)il2cpp_codegen_object_new(List_1_t3111957221_il2cpp_TypeInfo_var);
		List_1__ctor_m1832106448(L_0, /*hidden argument*/List_1__ctor_m1832106448_MethodInfo_var);
		((Physics2D_t9846735_StaticFields*)Physics2D_t9846735_il2cpp_TypeInfo_var->static_fields)->set_m_LastDisabledRigidbody2D_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Physics2D::Internal_Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern Il2CppClass* Physics2D_t9846735_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_Internal_Raycast_m4294843026_MetadataUsageId;
extern "C"  void Physics2D_Internal_Raycast_m4294843026 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, RaycastHit2D_t1374744384 * ___raycastHit6, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Internal_Raycast_m4294843026_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___distance2;
		int32_t L_1 = ___layerMask3;
		float L_2 = ___minDepth4;
		float L_3 = ___maxDepth5;
		RaycastHit2D_t1374744384 * L_4 = ___raycastHit6;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		Physics2D_INTERNAL_CALL_Internal_Raycast_m1210233913(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern "C"  void Physics2D_INTERNAL_CALL_Internal_Raycast_m1210233913 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___origin0, Vector2_t4282066565 * ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, RaycastHit2D_t1374744384 * ___raycastHit6, const MethodInfo* method)
{
	typedef void (*Physics2D_INTERNAL_CALL_Internal_Raycast_m1210233913_ftn) (Vector2_t4282066565 *, Vector2_t4282066565 *, float, int32_t, float, float, RaycastHit2D_t1374744384 *);
	static Physics2D_INTERNAL_CALL_Internal_Raycast_m1210233913_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_Internal_Raycast_m1210233913_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)");
	_il2cpp_icall_func(___origin0, ___direction1, ___distance2, ___layerMask3, ___minDepth4, ___maxDepth5, ___raycastHit6);
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern Il2CppClass* Physics2D_t9846735_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_Raycast_m1435321255_MetadataUsageId;
extern "C"  RaycastHit2D_t1374744384  Physics2D_Raycast_m1435321255 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___direction1, float ___distance2, int32_t ___layerMask3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Raycast_m1435321255_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		Vector2_t4282066565  L_0 = ___origin0;
		Vector2_t4282066565  L_1 = ___direction1;
		float L_2 = ___distance2;
		int32_t L_3 = ___layerMask3;
		float L_4 = V_1;
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		RaycastHit2D_t1374744384  L_6 = Physics2D_Raycast_m301626417(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern Il2CppClass* Physics2D_t9846735_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_Raycast_m301626417_MetadataUsageId;
extern "C"  RaycastHit2D_t1374744384  Physics2D_Raycast_m301626417 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Raycast_m301626417_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RaycastHit2D_t1374744384  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t4282066565  L_0 = ___origin0;
		Vector2_t4282066565  L_1 = ___direction1;
		float L_2 = ___distance2;
		int32_t L_3 = ___layerMask3;
		float L_4 = ___minDepth4;
		float L_5 = ___maxDepth5;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		Physics2D_Internal_Raycast_m4294843026(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, (&V_0), /*hidden argument*/NULL);
		RaycastHit2D_t1374744384  L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll(UnityEngine.Ray,System.Single,System.Int32)
extern Il2CppClass* Physics2D_t9846735_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_GetRayIntersectionAll_m2520210479_MetadataUsageId;
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_GetRayIntersectionAll_m2520210479 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___distance1, int32_t ___layerMask2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_GetRayIntersectionAll_m2520210479_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___distance1;
		int32_t L_1 = ___layerMask2;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t889400257* L_2 = Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m2968135304(NULL /*static, unused*/, (&___ray0), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_GetRayIntersectionAll(UnityEngine.Ray&,System.Single,System.Int32)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m2968135304 (Il2CppObject * __this /* static, unused */, Ray_t3134616544 * ___ray0, float ___distance1, int32_t ___layerMask2, const MethodInfo* method)
{
	typedef RaycastHit2DU5BU5D_t889400257* (*Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m2968135304_ftn) (Ray_t3134616544 *, float, int32_t);
	static Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m2968135304_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m2968135304_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_GetRayIntersectionAll(UnityEngine.Ray&,System.Single,System.Int32)");
	return _il2cpp_icall_func(___ray0, ___distance1, ___layerMask2);
}
// System.Void UnityEngine.Plane::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Plane__ctor_m2201046863 (Plane_t4206452690 * __this, Vector3_t4282066566  ___inNormal0, Vector3_t4282066566  ___inPoint1, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = ___inNormal0;
		Vector3_t4282066566  L_1 = Vector3_Normalize_m3047997355(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_m_Normal_0(L_1);
		Vector3_t4282066566  L_2 = ___inNormal0;
		Vector3_t4282066566  L_3 = ___inPoint1;
		float L_4 = Vector3_Dot_m2370485424(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		__this->set_m_Distance_1(((-L_4)));
		return;
	}
}
extern "C"  void Plane__ctor_m2201046863_AdjustorThunk (Il2CppObject * __this, Vector3_t4282066566  ___inNormal0, Vector3_t4282066566  ___inPoint1, const MethodInfo* method)
{
	Plane_t4206452690 * _thisAdjusted = reinterpret_cast<Plane_t4206452690 *>(__this + 1);
	Plane__ctor_m2201046863(_thisAdjusted, ___inNormal0, ___inPoint1, method);
}
// UnityEngine.Vector3 UnityEngine.Plane::get_normal()
extern "C"  Vector3_t4282066566  Plane_get_normal_m3534129213 (Plane_t4206452690 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_m_Normal_0();
		return L_0;
	}
}
extern "C"  Vector3_t4282066566  Plane_get_normal_m3534129213_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Plane_t4206452690 * _thisAdjusted = reinterpret_cast<Plane_t4206452690 *>(__this + 1);
	return Plane_get_normal_m3534129213(_thisAdjusted, method);
}
// System.Single UnityEngine.Plane::get_distance()
extern "C"  float Plane_get_distance_m2612484153 (Plane_t4206452690 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Distance_1();
		return L_0;
	}
}
extern "C"  float Plane_get_distance_m2612484153_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Plane_t4206452690 * _thisAdjusted = reinterpret_cast<Plane_t4206452690 *>(__this + 1);
	return Plane_get_distance_m2612484153(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Plane::Raycast(UnityEngine.Ray,System.Single&)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Plane_Raycast_m2829769106_MetadataUsageId;
extern "C"  bool Plane_Raycast_m2829769106 (Plane_t4206452690 * __this, Ray_t3134616544  ___ray0, float* ___enter1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Plane_Raycast_m2829769106_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		Vector3_t4282066566  L_0 = Ray_get_direction_m3201866877((&___ray0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_1 = Plane_get_normal_m3534129213(__this, /*hidden argument*/NULL);
		float L_2 = Vector3_Dot_m2370485424(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t4282066566  L_3 = Ray_get_origin_m3064983562((&___ray0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_4 = Plane_get_normal_m3534129213(__this, /*hidden argument*/NULL);
		float L_5 = Vector3_Dot_m2370485424(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = Plane_get_distance_m2612484153(__this, /*hidden argument*/NULL);
		V_1 = ((float)((float)((-L_5))-(float)L_6));
		float L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		bool L_8 = Mathf_Approximately_m1395529776(NULL /*static, unused*/, L_7, (0.0f), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0047;
		}
	}
	{
		float* L_9 = ___enter1;
		*((float*)(L_9)) = (float)(0.0f);
		return (bool)0;
	}

IL_0047:
	{
		float* L_10 = ___enter1;
		float L_11 = V_1;
		float L_12 = V_0;
		*((float*)(L_10)) = (float)((float)((float)L_11/(float)L_12));
		float* L_13 = ___enter1;
		return (bool)((((float)(*((float*)L_13))) > ((float)(0.0f)))? 1 : 0);
	}
}
extern "C"  bool Plane_Raycast_m2829769106_AdjustorThunk (Il2CppObject * __this, Ray_t3134616544  ___ray0, float* ___enter1, const MethodInfo* method)
{
	Plane_t4206452690 * _thisAdjusted = reinterpret_cast<Plane_t4206452690 *>(__this + 1);
	return Plane_Raycast_m2829769106(_thisAdjusted, ___ray0, ___enter1, method);
}
// Conversion methods for marshalling of: UnityEngine.Plane
extern "C" void Plane_t4206452690_marshal_pinvoke(const Plane_t4206452690& unmarshaled, Plane_t4206452690_marshaled_pinvoke& marshaled)
{
	Vector3_t4282066566_marshal_pinvoke(unmarshaled.get_m_Normal_0(), marshaled.___m_Normal_0);
	marshaled.___m_Distance_1 = unmarshaled.get_m_Distance_1();
}
extern "C" void Plane_t4206452690_marshal_pinvoke_back(const Plane_t4206452690_marshaled_pinvoke& marshaled, Plane_t4206452690& unmarshaled)
{
	Vector3_t4282066566  unmarshaled_m_Normal_temp_0;
	memset(&unmarshaled_m_Normal_temp_0, 0, sizeof(unmarshaled_m_Normal_temp_0));
	Vector3_t4282066566_marshal_pinvoke_back(marshaled.___m_Normal_0, unmarshaled_m_Normal_temp_0);
	unmarshaled.set_m_Normal_0(unmarshaled_m_Normal_temp_0);
	float unmarshaled_m_Distance_temp_1 = 0.0f;
	unmarshaled_m_Distance_temp_1 = marshaled.___m_Distance_1;
	unmarshaled.set_m_Distance_1(unmarshaled_m_Distance_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Plane
extern "C" void Plane_t4206452690_marshal_pinvoke_cleanup(Plane_t4206452690_marshaled_pinvoke& marshaled)
{
	Vector3_t4282066566_marshal_pinvoke_cleanup(marshaled.___m_Normal_0);
}
// Conversion methods for marshalling of: UnityEngine.Plane
extern "C" void Plane_t4206452690_marshal_com(const Plane_t4206452690& unmarshaled, Plane_t4206452690_marshaled_com& marshaled)
{
	Vector3_t4282066566_marshal_com(unmarshaled.get_m_Normal_0(), marshaled.___m_Normal_0);
	marshaled.___m_Distance_1 = unmarshaled.get_m_Distance_1();
}
extern "C" void Plane_t4206452690_marshal_com_back(const Plane_t4206452690_marshaled_com& marshaled, Plane_t4206452690& unmarshaled)
{
	Vector3_t4282066566  unmarshaled_m_Normal_temp_0;
	memset(&unmarshaled_m_Normal_temp_0, 0, sizeof(unmarshaled_m_Normal_temp_0));
	Vector3_t4282066566_marshal_com_back(marshaled.___m_Normal_0, unmarshaled_m_Normal_temp_0);
	unmarshaled.set_m_Normal_0(unmarshaled_m_Normal_temp_0);
	float unmarshaled_m_Distance_temp_1 = 0.0f;
	unmarshaled_m_Distance_temp_1 = marshaled.___m_Distance_1;
	unmarshaled.set_m_Distance_1(unmarshaled_m_Distance_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Plane
extern "C" void Plane_t4206452690_marshal_com_cleanup(Plane_t4206452690_marshaled_com& marshaled)
{
	Vector3_t4282066566_marshal_com_cleanup(marshaled.___m_Normal_0);
}
// System.Boolean UnityEngine.PlayerPrefs::TrySetInt(System.String,System.Int32)
extern "C"  bool PlayerPrefs_TrySetInt_m2066630347 (Il2CppObject * __this /* static, unused */, String_t* ___key0, int32_t ___value1, const MethodInfo* method)
{
	typedef bool (*PlayerPrefs_TrySetInt_m2066630347_ftn) (String_t*, int32_t);
	static PlayerPrefs_TrySetInt_m2066630347_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_TrySetInt_m2066630347_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::TrySetInt(System.String,System.Int32)");
	return _il2cpp_icall_func(___key0, ___value1);
}
// System.Boolean UnityEngine.PlayerPrefs::TrySetFloat(System.String,System.Single)
extern "C"  bool PlayerPrefs_TrySetFloat_m96551332 (Il2CppObject * __this /* static, unused */, String_t* ___key0, float ___value1, const MethodInfo* method)
{
	typedef bool (*PlayerPrefs_TrySetFloat_m96551332_ftn) (String_t*, float);
	static PlayerPrefs_TrySetFloat_m96551332_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_TrySetFloat_m96551332_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::TrySetFloat(System.String,System.Single)");
	return _il2cpp_icall_func(___key0, ___value1);
}
// System.Boolean UnityEngine.PlayerPrefs::TrySetSetString(System.String,System.String)
extern "C"  bool PlayerPrefs_TrySetSetString_m452988068 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___value1, const MethodInfo* method)
{
	typedef bool (*PlayerPrefs_TrySetSetString_m452988068_ftn) (String_t*, String_t*);
	static PlayerPrefs_TrySetSetString_m452988068_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_TrySetSetString_m452988068_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::TrySetSetString(System.String,System.String)");
	return _il2cpp_icall_func(___key0, ___value1);
}
// System.Void UnityEngine.PlayerPrefs::SetInt(System.String,System.Int32)
extern Il2CppClass* PlayerPrefsException_t3680716996_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2586178071;
extern const uint32_t PlayerPrefs_SetInt_m3485171996_MetadataUsageId;
extern "C"  void PlayerPrefs_SetInt_m3485171996 (Il2CppObject * __this /* static, unused */, String_t* ___key0, int32_t ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefs_SetInt_m3485171996_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___key0;
		int32_t L_1 = ___value1;
		bool L_2 = PlayerPrefs_TrySetInt_m2066630347(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		PlayerPrefsException_t3680716996 * L_3 = (PlayerPrefsException_t3680716996 *)il2cpp_codegen_object_new(PlayerPrefsException_t3680716996_il2cpp_TypeInfo_var);
		PlayerPrefsException__ctor_m3661687413(L_3, _stringLiteral2586178071, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0017:
	{
		return;
	}
}
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)
extern "C"  int32_t PlayerPrefs_GetInt_m3632746280 (Il2CppObject * __this /* static, unused */, String_t* ___key0, int32_t ___defaultValue1, const MethodInfo* method)
{
	typedef int32_t (*PlayerPrefs_GetInt_m3632746280_ftn) (String_t*, int32_t);
	static PlayerPrefs_GetInt_m3632746280_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_GetInt_m3632746280_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)");
	return _il2cpp_icall_func(___key0, ___defaultValue1);
}
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String)
extern "C"  int32_t PlayerPrefs_GetInt_m1334009359 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		String_t* L_0 = ___key0;
		int32_t L_1 = V_0;
		int32_t L_2 = PlayerPrefs_GetInt_m3632746280(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.PlayerPrefs::SetFloat(System.String,System.Single)
extern Il2CppClass* PlayerPrefsException_t3680716996_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2586178071;
extern const uint32_t PlayerPrefs_SetFloat_m1687591347_MetadataUsageId;
extern "C"  void PlayerPrefs_SetFloat_m1687591347 (Il2CppObject * __this /* static, unused */, String_t* ___key0, float ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefs_SetFloat_m1687591347_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___key0;
		float L_1 = ___value1;
		bool L_2 = PlayerPrefs_TrySetFloat_m96551332(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		PlayerPrefsException_t3680716996 * L_3 = (PlayerPrefsException_t3680716996 *)il2cpp_codegen_object_new(PlayerPrefsException_t3680716996_il2cpp_TypeInfo_var);
		PlayerPrefsException__ctor_m3661687413(L_3, _stringLiteral2586178071, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0017:
	{
		return;
	}
}
// System.Single UnityEngine.PlayerPrefs::GetFloat(System.String,System.Single)
extern "C"  float PlayerPrefs_GetFloat_m1210224051 (Il2CppObject * __this /* static, unused */, String_t* ___key0, float ___defaultValue1, const MethodInfo* method)
{
	typedef float (*PlayerPrefs_GetFloat_m1210224051_ftn) (String_t*, float);
	static PlayerPrefs_GetFloat_m1210224051_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_GetFloat_m1210224051_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::GetFloat(System.String,System.Single)");
	return _il2cpp_icall_func(___key0, ___defaultValue1);
}
// System.Void UnityEngine.PlayerPrefs::SetString(System.String,System.String)
extern Il2CppClass* PlayerPrefsException_t3680716996_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2586178071;
extern const uint32_t PlayerPrefs_SetString_m989974275_MetadataUsageId;
extern "C"  void PlayerPrefs_SetString_m989974275 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefs_SetString_m989974275_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___key0;
		String_t* L_1 = ___value1;
		bool L_2 = PlayerPrefs_TrySetSetString_m452988068(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		PlayerPrefsException_t3680716996 * L_3 = (PlayerPrefsException_t3680716996 *)il2cpp_codegen_object_new(PlayerPrefsException_t3680716996_il2cpp_TypeInfo_var);
		PlayerPrefsException__ctor_m3661687413(L_3, _stringLiteral2586178071, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0017:
	{
		return;
	}
}
// System.String UnityEngine.PlayerPrefs::GetString(System.String,System.String)
extern "C"  String_t* PlayerPrefs_GetString_m3230559948 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___defaultValue1, const MethodInfo* method)
{
	typedef String_t* (*PlayerPrefs_GetString_m3230559948_ftn) (String_t*, String_t*);
	static PlayerPrefs_GetString_m3230559948_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_GetString_m3230559948_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::GetString(System.String,System.String)");
	return _il2cpp_icall_func(___key0, ___defaultValue1);
}
// System.String UnityEngine.PlayerPrefs::GetString(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t PlayerPrefs_GetString_m378864272_MetadataUsageId;
extern "C"  String_t* PlayerPrefs_GetString_m378864272 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefs_GetString_m378864272_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		String_t* L_1 = ___key0;
		String_t* L_2 = V_0;
		String_t* L_3 = PlayerPrefs_GetString_m3230559948(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.PlayerPrefs::HasKey(System.String)
extern "C"  bool PlayerPrefs_HasKey_m2032560073 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	typedef bool (*PlayerPrefs_HasKey_m2032560073_ftn) (String_t*);
	static PlayerPrefs_HasKey_m2032560073_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_HasKey_m2032560073_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::HasKey(System.String)");
	return _il2cpp_icall_func(___key0);
}
// System.Void UnityEngine.PlayerPrefs::DeleteKey(System.String)
extern "C"  void PlayerPrefs_DeleteKey_m1547199302 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	typedef void (*PlayerPrefs_DeleteKey_m1547199302_ftn) (String_t*);
	static PlayerPrefs_DeleteKey_m1547199302_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_DeleteKey_m1547199302_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::DeleteKey(System.String)");
	_il2cpp_icall_func(___key0);
}
// System.Void UnityEngine.PlayerPrefs::DeleteAll()
extern "C"  void PlayerPrefs_DeleteAll_m2619453438 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*PlayerPrefs_DeleteAll_m2619453438_ftn) ();
	static PlayerPrefs_DeleteAll_m2619453438_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_DeleteAll_m2619453438_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::DeleteAll()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.PlayerPrefs::Save()
extern "C"  void PlayerPrefs_Save_m3891538519 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*PlayerPrefs_Save_m3891538519_ftn) ();
	static PlayerPrefs_Save_m3891538519_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_Save_m3891538519_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::Save()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.PlayerPrefsException::.ctor(System.String)
extern "C"  void PlayerPrefsException__ctor_m3661687413 (PlayerPrefsException_t3680716996 * __this, String_t* ___error0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___error0;
		Exception__ctor_m3870771296(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.PropertyAttribute::.ctor()
extern "C"  void PropertyAttribute__ctor_m1741701746 (PropertyAttribute_t3531521085 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.QualitySettings::SetQualityLevel(System.Int32,System.Boolean)
extern "C"  void QualitySettings_SetQualityLevel_m1369916048 (Il2CppObject * __this /* static, unused */, int32_t ___index0, bool ___applyExpensiveChanges1, const MethodInfo* method)
{
	typedef void (*QualitySettings_SetQualityLevel_m1369916048_ftn) (int32_t, bool);
	static QualitySettings_SetQualityLevel_m1369916048_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_SetQualityLevel_m1369916048_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::SetQualityLevel(System.Int32,System.Boolean)");
	_il2cpp_icall_func(___index0, ___applyExpensiveChanges1);
}
// System.Void UnityEngine.QualitySettings::SetQualityLevel(System.Int32)
extern "C"  void QualitySettings_SetQualityLevel_m962145613 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)1;
		int32_t L_0 = ___index0;
		bool L_1 = V_0;
		QualitySettings_SetQualityLevel_m1369916048(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.ColorSpace UnityEngine.QualitySettings::get_activeColorSpace()
extern "C"  int32_t QualitySettings_get_activeColorSpace_m2993616266 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*QualitySettings_get_activeColorSpace_m2993616266_ftn) ();
	static QualitySettings_get_activeColorSpace_m2993616266_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_activeColorSpace_m2993616266_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_activeColorSpace()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Quaternion__ctor_m1100844011 (Quaternion_t1553702882 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_1(L_0);
		float L_1 = ___y1;
		__this->set_y_2(L_1);
		float L_2 = ___z2;
		__this->set_z_3(L_2);
		float L_3 = ___w3;
		__this->set_w_4(L_3);
		return;
	}
}
extern "C"  void Quaternion__ctor_m1100844011_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, float ___z2, float ___w3, const MethodInfo* method)
{
	Quaternion_t1553702882 * _thisAdjusted = reinterpret_cast<Quaternion_t1553702882 *>(__this + 1);
	Quaternion__ctor_m1100844011(_thisAdjusted, ___x0, ___y1, ___z2, ___w3, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C"  Quaternion_t1553702882  Quaternion_get_identity_m1743882806 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Quaternion_t1553702882  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Quaternion__ctor_m1100844011(&L_0, (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.Quaternion::Dot(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  float Quaternion_Dot_m580284 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___a0, Quaternion_t1553702882  ___b1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___b1)->get_z_3();
		float L_6 = (&___a0)->get_w_4();
		float L_7 = (&___b1)->get_w_4();
		return ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7))));
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::AngleAxis(System.Single,UnityEngine.Vector3)
extern "C"  Quaternion_t1553702882  Quaternion_AngleAxis_m644124247 (Il2CppObject * __this /* static, unused */, float ___angle0, Vector3_t4282066566  ___axis1, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___angle0;
		Quaternion_INTERNAL_CALL_AngleAxis_m1562314763(NULL /*static, unused*/, L_0, (&___axis1), (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_AngleAxis(System.Single,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_AngleAxis_m1562314763 (Il2CppObject * __this /* static, unused */, float ___angle0, Vector3_t4282066566 * ___axis1, Quaternion_t1553702882 * ___value2, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_AngleAxis_m1562314763_ftn) (float, Vector3_t4282066566 *, Quaternion_t1553702882 *);
	static Quaternion_INTERNAL_CALL_AngleAxis_m1562314763_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_AngleAxis_m1562314763_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_AngleAxis(System.Single,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___angle0, ___axis1, ___value2);
}
// System.Void UnityEngine.Quaternion::ToAngleAxis(System.Single&,UnityEngine.Vector3&)
extern "C"  void Quaternion_ToAngleAxis_m791330738 (Quaternion_t1553702882 * __this, float* ___angle0, Vector3_t4282066566 * ___axis1, const MethodInfo* method)
{
	{
		Vector3_t4282066566 * L_0 = ___axis1;
		float* L_1 = ___angle0;
		Quaternion_Internal_ToAxisAngleRad_m3414012038(NULL /*static, unused*/, (*(Quaternion_t1553702882 *)__this), L_0, L_1, /*hidden argument*/NULL);
		float* L_2 = ___angle0;
		float* L_3 = ___angle0;
		*((float*)(L_2)) = (float)((float)((float)(*((float*)L_3))*(float)(57.29578f)));
		return;
	}
}
extern "C"  void Quaternion_ToAngleAxis_m791330738_AdjustorThunk (Il2CppObject * __this, float* ___angle0, Vector3_t4282066566 * ___axis1, const MethodInfo* method)
{
	Quaternion_t1553702882 * _thisAdjusted = reinterpret_cast<Quaternion_t1553702882 *>(__this + 1);
	Quaternion_ToAngleAxis_m791330738(_thisAdjusted, ___angle0, ___axis1, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::FromToRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Quaternion_t1553702882  Quaternion_FromToRotation_m2335489018 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___fromDirection0, Vector3_t4282066566  ___toDirection1, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_INTERNAL_CALL_FromToRotation_m3717286698(NULL /*static, unused*/, (&___fromDirection0), (&___toDirection1), (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_FromToRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_FromToRotation_m3717286698 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___fromDirection0, Vector3_t4282066566 * ___toDirection1, Quaternion_t1553702882 * ___value2, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_FromToRotation_m3717286698_ftn) (Vector3_t4282066566 *, Vector3_t4282066566 *, Quaternion_t1553702882 *);
	static Quaternion_INTERNAL_CALL_FromToRotation_m3717286698_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_FromToRotation_m3717286698_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_FromToRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___fromDirection0, ___toDirection1, ___value2);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Quaternion_t1553702882  Quaternion_LookRotation_m2869326048 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___forward0, Vector3_t4282066566  ___upwards1, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_INTERNAL_CALL_LookRotation_m1501255504(NULL /*static, unused*/, (&___forward0), (&___upwards1), (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_LookRotation_m1501255504 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___forward0, Vector3_t4282066566 * ___upwards1, Quaternion_t1553702882 * ___value2, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_LookRotation_m1501255504_ftn) (Vector3_t4282066566 *, Vector3_t4282066566 *, Quaternion_t1553702882 *);
	static Quaternion_INTERNAL_CALL_LookRotation_m1501255504_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_LookRotation_m1501255504_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___forward0, ___upwards1, ___value2);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Slerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C"  Quaternion_t1553702882  Quaternion_Slerp_m844700366 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___a0, Quaternion_t1553702882  ___b1, float ___t2, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___t2;
		Quaternion_INTERNAL_CALL_Slerp_m2927410052(NULL /*static, unused*/, (&___a0), (&___b1), L_0, (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Slerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Slerp_m2927410052 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882 * ___a0, Quaternion_t1553702882 * ___b1, float ___t2, Quaternion_t1553702882 * ___value3, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Slerp_m2927410052_ftn) (Quaternion_t1553702882 *, Quaternion_t1553702882 *, float, Quaternion_t1553702882 *);
	static Quaternion_INTERNAL_CALL_Slerp_m2927410052_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Slerp_m2927410052_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Slerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___a0, ___b1, ___t2, ___value3);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Lerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C"  Quaternion_t1553702882  Quaternion_Lerp_m1693481477 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___a0, Quaternion_t1553702882  ___b1, float ___t2, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___t2;
		Quaternion_INTERNAL_CALL_Lerp_m3955033425(NULL /*static, unused*/, (&___a0), (&___b1), L_0, (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Lerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Lerp_m3955033425 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882 * ___a0, Quaternion_t1553702882 * ___b1, float ___t2, Quaternion_t1553702882 * ___value3, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Lerp_m3955033425_ftn) (Quaternion_t1553702882 *, Quaternion_t1553702882 *, float, Quaternion_t1553702882 *);
	static Quaternion_INTERNAL_CALL_Lerp_m3955033425_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Lerp_m3955033425_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Lerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___a0, ___b1, ___t2, ___value3);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Inverse(UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  Quaternion_Inverse_m3542515566 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___rotation0, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_INTERNAL_CALL_Inverse_m4175627710(NULL /*static, unused*/, (&___rotation0), (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Inverse_m4175627710 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882 * ___rotation0, Quaternion_t1553702882 * ___value1, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Inverse_m4175627710_ftn) (Quaternion_t1553702882 *, Quaternion_t1553702882 *);
	static Quaternion_INTERNAL_CALL_Inverse_m4175627710_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Inverse_m4175627710_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___rotation0, ___value1);
}
// System.String UnityEngine.Quaternion::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral843281963;
extern const uint32_t Quaternion_ToString_m1793285860_MetadataUsageId;
extern "C"  String_t* Quaternion_ToString_m1793285860 (Quaternion_t1553702882 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_ToString_m1793285860_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = __this->get_x_1();
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_0;
		float L_5 = __this->get_y_2();
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t1108656482* L_8 = L_4;
		float L_9 = __this->get_z_3();
		float L_10 = L_9;
		Il2CppObject * L_11 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t1108656482* L_12 = L_8;
		float L_13 = __this->get_w_4();
		float L_14 = L_13;
		Il2CppObject * L_15 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral843281963, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
extern "C"  String_t* Quaternion_ToString_m1793285860_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Quaternion_t1553702882 * _thisAdjusted = reinterpret_cast<Quaternion_t1553702882 *>(__this + 1);
	return Quaternion_ToString_m1793285860(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Quaternion::get_eulerAngles()
extern "C"  Vector3_t4282066566  Quaternion_get_eulerAngles_m997303795 (Quaternion_t1553702882 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = Quaternion_Internal_ToEulerRad_m1608666215(NULL /*static, unused*/, (*(Quaternion_t1553702882 *)__this), /*hidden argument*/NULL);
		Vector3_t4282066566  L_1 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_0, (57.29578f), /*hidden argument*/NULL);
		return L_1;
	}
}
extern "C"  Vector3_t4282066566  Quaternion_get_eulerAngles_m997303795_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Quaternion_t1553702882 * _thisAdjusted = reinterpret_cast<Quaternion_t1553702882 *>(__this + 1);
	return Quaternion_get_eulerAngles_m997303795(_thisAdjusted, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
extern "C"  Quaternion_t1553702882  Quaternion_Euler_m1204688217 (Il2CppObject * __this /* static, unused */, float ___x0, float ___y1, float ___z2, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		float L_1 = ___y1;
		float L_2 = ___z2;
		Vector3_t4282066566  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m2926210380(&L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t4282066566  L_4 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_3, (0.0174532924f), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_5 = Quaternion_Internal_FromEulerRad_m3681319598(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(UnityEngine.Vector3)
extern "C"  Quaternion_t1553702882  Quaternion_Euler_m1940911101 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___euler0, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = ___euler0;
		Vector3_t4282066566  L_1 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_0, (0.0174532924f), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_2 = Quaternion_Internal_FromEulerRad_m3681319598(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::Internal_ToEulerRad(UnityEngine.Quaternion)
extern "C"  Vector3_t4282066566  Quaternion_Internal_ToEulerRad_m1608666215 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___rotation0, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1660134351(NULL /*static, unused*/, (&___rotation0), (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToEulerRad(UnityEngine.Quaternion&,UnityEngine.Vector3&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1660134351 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882 * ___rotation0, Vector3_t4282066566 * ___value1, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1660134351_ftn) (Quaternion_t1553702882 *, Vector3_t4282066566 *);
	static Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1660134351_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1660134351_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToEulerRad(UnityEngine.Quaternion&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___rotation0, ___value1);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Internal_FromEulerRad(UnityEngine.Vector3)
extern "C"  Quaternion_t1553702882  Quaternion_Internal_FromEulerRad_m3681319598 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___euler0, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1312441940(NULL /*static, unused*/, (&___euler0), (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1312441940 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___euler0, Quaternion_t1553702882 * ___value1, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1312441940_ftn) (Vector3_t4282066566 *, Quaternion_t1553702882 *);
	static Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1312441940_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1312441940_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___euler0, ___value1);
}
// System.Void UnityEngine.Quaternion::Internal_ToAxisAngleRad(UnityEngine.Quaternion,UnityEngine.Vector3&,System.Single&)
extern "C"  void Quaternion_Internal_ToAxisAngleRad_m3414012038 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___q0, Vector3_t4282066566 * ___axis1, float* ___angle2, const MethodInfo* method)
{
	{
		Vector3_t4282066566 * L_0 = ___axis1;
		float* L_1 = ___angle2;
		Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m2360346913(NULL /*static, unused*/, (&___q0), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToAxisAngleRad(UnityEngine.Quaternion&,UnityEngine.Vector3&,System.Single&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m2360346913 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882 * ___q0, Vector3_t4282066566 * ___axis1, float* ___angle2, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m2360346913_ftn) (Quaternion_t1553702882 *, Vector3_t4282066566 *, float*);
	static Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m2360346913_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m2360346913_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToAxisAngleRad(UnityEngine.Quaternion&,UnityEngine.Vector3&,System.Single&)");
	_il2cpp_icall_func(___q0, ___axis1, ___angle2);
}
// System.Int32 UnityEngine.Quaternion::GetHashCode()
extern "C"  int32_t Quaternion_GetHashCode_m3823258238 (Quaternion_t1553702882 * __this, const MethodInfo* method)
{
	{
		float* L_0 = __this->get_address_of_x_1();
		int32_t L_1 = Single_GetHashCode_m65342520(L_0, /*hidden argument*/NULL);
		float* L_2 = __this->get_address_of_y_2();
		int32_t L_3 = Single_GetHashCode_m65342520(L_2, /*hidden argument*/NULL);
		float* L_4 = __this->get_address_of_z_3();
		int32_t L_5 = Single_GetHashCode_m65342520(L_4, /*hidden argument*/NULL);
		float* L_6 = __this->get_address_of_w_4();
		int32_t L_7 = Single_GetHashCode_m65342520(L_6, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
	}
}
extern "C"  int32_t Quaternion_GetHashCode_m3823258238_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Quaternion_t1553702882 * _thisAdjusted = reinterpret_cast<Quaternion_t1553702882 *>(__this + 1);
	return Quaternion_GetHashCode_m3823258238(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Quaternion::Equals(System.Object)
extern Il2CppClass* Quaternion_t1553702882_il2cpp_TypeInfo_var;
extern const uint32_t Quaternion_Equals_m3843409946_MetadataUsageId;
extern "C"  bool Quaternion_Equals_m3843409946 (Quaternion_t1553702882 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_Equals_m3843409946_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Quaternion_t1553702882_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(Quaternion_t1553702882 *)((Quaternion_t1553702882 *)UnBox (L_1, Quaternion_t1553702882_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_x_1();
		float L_3 = (&V_0)->get_x_1();
		bool L_4 = Single_Equals_m2110115959(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_006d;
		}
	}
	{
		float* L_5 = __this->get_address_of_y_2();
		float L_6 = (&V_0)->get_y_2();
		bool L_7 = Single_Equals_m2110115959(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006d;
		}
	}
	{
		float* L_8 = __this->get_address_of_z_3();
		float L_9 = (&V_0)->get_z_3();
		bool L_10 = Single_Equals_m2110115959(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006d;
		}
	}
	{
		float* L_11 = __this->get_address_of_w_4();
		float L_12 = (&V_0)->get_w_4();
		bool L_13 = Single_Equals_m2110115959(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_006e;
	}

IL_006d:
	{
		G_B7_0 = 0;
	}

IL_006e:
	{
		return (bool)G_B7_0;
	}
}
extern "C"  bool Quaternion_Equals_m3843409946_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Quaternion_t1553702882 * _thisAdjusted = reinterpret_cast<Quaternion_t1553702882 *>(__this + 1);
	return Quaternion_Equals_m3843409946(_thisAdjusted, ___other0, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  Quaternion_op_Multiply_m3077481361 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___lhs0, Quaternion_t1553702882  ___rhs1, const MethodInfo* method)
{
	{
		float L_0 = (&___lhs0)->get_w_4();
		float L_1 = (&___rhs1)->get_x_1();
		float L_2 = (&___lhs0)->get_x_1();
		float L_3 = (&___rhs1)->get_w_4();
		float L_4 = (&___lhs0)->get_y_2();
		float L_5 = (&___rhs1)->get_z_3();
		float L_6 = (&___lhs0)->get_z_3();
		float L_7 = (&___rhs1)->get_y_2();
		float L_8 = (&___lhs0)->get_w_4();
		float L_9 = (&___rhs1)->get_y_2();
		float L_10 = (&___lhs0)->get_y_2();
		float L_11 = (&___rhs1)->get_w_4();
		float L_12 = (&___lhs0)->get_z_3();
		float L_13 = (&___rhs1)->get_x_1();
		float L_14 = (&___lhs0)->get_x_1();
		float L_15 = (&___rhs1)->get_z_3();
		float L_16 = (&___lhs0)->get_w_4();
		float L_17 = (&___rhs1)->get_z_3();
		float L_18 = (&___lhs0)->get_z_3();
		float L_19 = (&___rhs1)->get_w_4();
		float L_20 = (&___lhs0)->get_x_1();
		float L_21 = (&___rhs1)->get_y_2();
		float L_22 = (&___lhs0)->get_y_2();
		float L_23 = (&___rhs1)->get_x_1();
		float L_24 = (&___lhs0)->get_w_4();
		float L_25 = (&___rhs1)->get_w_4();
		float L_26 = (&___lhs0)->get_x_1();
		float L_27 = (&___rhs1)->get_x_1();
		float L_28 = (&___lhs0)->get_y_2();
		float L_29 = (&___rhs1)->get_y_2();
		float L_30 = (&___lhs0)->get_z_3();
		float L_31 = (&___rhs1)->get_z_3();
		Quaternion_t1553702882  L_32;
		memset(&L_32, 0, sizeof(L_32));
		Quaternion__ctor_m1100844011(&L_32, ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))-(float)((float)((float)L_6*(float)L_7)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_8*(float)L_9))+(float)((float)((float)L_10*(float)L_11))))+(float)((float)((float)L_12*(float)L_13))))-(float)((float)((float)L_14*(float)L_15)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_16*(float)L_17))+(float)((float)((float)L_18*(float)L_19))))+(float)((float)((float)L_20*(float)L_21))))-(float)((float)((float)L_22*(float)L_23)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_24*(float)L_25))-(float)((float)((float)L_26*(float)L_27))))-(float)((float)((float)L_28*(float)L_29))))-(float)((float)((float)L_30*(float)L_31)))), /*hidden argument*/NULL);
		return L_32;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Quaternion_op_Multiply_m3771288979 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___rotation0, Vector3_t4282066566  ___point1, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	Vector3_t4282066566  V_12;
	memset(&V_12, 0, sizeof(V_12));
	{
		float L_0 = (&___rotation0)->get_x_1();
		V_0 = ((float)((float)L_0*(float)(2.0f)));
		float L_1 = (&___rotation0)->get_y_2();
		V_1 = ((float)((float)L_1*(float)(2.0f)));
		float L_2 = (&___rotation0)->get_z_3();
		V_2 = ((float)((float)L_2*(float)(2.0f)));
		float L_3 = (&___rotation0)->get_x_1();
		float L_4 = V_0;
		V_3 = ((float)((float)L_3*(float)L_4));
		float L_5 = (&___rotation0)->get_y_2();
		float L_6 = V_1;
		V_4 = ((float)((float)L_5*(float)L_6));
		float L_7 = (&___rotation0)->get_z_3();
		float L_8 = V_2;
		V_5 = ((float)((float)L_7*(float)L_8));
		float L_9 = (&___rotation0)->get_x_1();
		float L_10 = V_1;
		V_6 = ((float)((float)L_9*(float)L_10));
		float L_11 = (&___rotation0)->get_x_1();
		float L_12 = V_2;
		V_7 = ((float)((float)L_11*(float)L_12));
		float L_13 = (&___rotation0)->get_y_2();
		float L_14 = V_2;
		V_8 = ((float)((float)L_13*(float)L_14));
		float L_15 = (&___rotation0)->get_w_4();
		float L_16 = V_0;
		V_9 = ((float)((float)L_15*(float)L_16));
		float L_17 = (&___rotation0)->get_w_4();
		float L_18 = V_1;
		V_10 = ((float)((float)L_17*(float)L_18));
		float L_19 = (&___rotation0)->get_w_4();
		float L_20 = V_2;
		V_11 = ((float)((float)L_19*(float)L_20));
		float L_21 = V_4;
		float L_22 = V_5;
		float L_23 = (&___point1)->get_x_1();
		float L_24 = V_6;
		float L_25 = V_11;
		float L_26 = (&___point1)->get_y_2();
		float L_27 = V_7;
		float L_28 = V_10;
		float L_29 = (&___point1)->get_z_3();
		(&V_12)->set_x_1(((float)((float)((float)((float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_21+(float)L_22))))*(float)L_23))+(float)((float)((float)((float)((float)L_24-(float)L_25))*(float)L_26))))+(float)((float)((float)((float)((float)L_27+(float)L_28))*(float)L_29)))));
		float L_30 = V_6;
		float L_31 = V_11;
		float L_32 = (&___point1)->get_x_1();
		float L_33 = V_3;
		float L_34 = V_5;
		float L_35 = (&___point1)->get_y_2();
		float L_36 = V_8;
		float L_37 = V_9;
		float L_38 = (&___point1)->get_z_3();
		(&V_12)->set_y_2(((float)((float)((float)((float)((float)((float)((float)((float)L_30+(float)L_31))*(float)L_32))+(float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_33+(float)L_34))))*(float)L_35))))+(float)((float)((float)((float)((float)L_36-(float)L_37))*(float)L_38)))));
		float L_39 = V_7;
		float L_40 = V_10;
		float L_41 = (&___point1)->get_x_1();
		float L_42 = V_8;
		float L_43 = V_9;
		float L_44 = (&___point1)->get_y_2();
		float L_45 = V_3;
		float L_46 = V_4;
		float L_47 = (&___point1)->get_z_3();
		(&V_12)->set_z_3(((float)((float)((float)((float)((float)((float)((float)((float)L_39-(float)L_40))*(float)L_41))+(float)((float)((float)((float)((float)L_42+(float)L_43))*(float)L_44))))+(float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_45+(float)L_46))))*(float)L_47)))));
		Vector3_t4282066566  L_48 = V_12;
		return L_48;
	}
}
// System.Boolean UnityEngine.Quaternion::op_Inequality(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  bool Quaternion_op_Inequality_m4197259746 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___lhs0, Quaternion_t1553702882  ___rhs1, const MethodInfo* method)
{
	{
		Quaternion_t1553702882  L_0 = ___lhs0;
		Quaternion_t1553702882  L_1 = ___rhs1;
		float L_2 = Quaternion_Dot_m580284(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)((!(((float)L_2) <= ((float)(0.999999f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.Quaternion
extern "C" void Quaternion_t1553702882_marshal_pinvoke(const Quaternion_t1553702882& unmarshaled, Quaternion_t1553702882_marshaled_pinvoke& marshaled)
{
	marshaled.___x_1 = unmarshaled.get_x_1();
	marshaled.___y_2 = unmarshaled.get_y_2();
	marshaled.___z_3 = unmarshaled.get_z_3();
	marshaled.___w_4 = unmarshaled.get_w_4();
}
extern "C" void Quaternion_t1553702882_marshal_pinvoke_back(const Quaternion_t1553702882_marshaled_pinvoke& marshaled, Quaternion_t1553702882& unmarshaled)
{
	float unmarshaled_x_temp_0 = 0.0f;
	unmarshaled_x_temp_0 = marshaled.___x_1;
	unmarshaled.set_x_1(unmarshaled_x_temp_0);
	float unmarshaled_y_temp_1 = 0.0f;
	unmarshaled_y_temp_1 = marshaled.___y_2;
	unmarshaled.set_y_2(unmarshaled_y_temp_1);
	float unmarshaled_z_temp_2 = 0.0f;
	unmarshaled_z_temp_2 = marshaled.___z_3;
	unmarshaled.set_z_3(unmarshaled_z_temp_2);
	float unmarshaled_w_temp_3 = 0.0f;
	unmarshaled_w_temp_3 = marshaled.___w_4;
	unmarshaled.set_w_4(unmarshaled_w_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Quaternion
extern "C" void Quaternion_t1553702882_marshal_pinvoke_cleanup(Quaternion_t1553702882_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Quaternion
extern "C" void Quaternion_t1553702882_marshal_com(const Quaternion_t1553702882& unmarshaled, Quaternion_t1553702882_marshaled_com& marshaled)
{
	marshaled.___x_1 = unmarshaled.get_x_1();
	marshaled.___y_2 = unmarshaled.get_y_2();
	marshaled.___z_3 = unmarshaled.get_z_3();
	marshaled.___w_4 = unmarshaled.get_w_4();
}
extern "C" void Quaternion_t1553702882_marshal_com_back(const Quaternion_t1553702882_marshaled_com& marshaled, Quaternion_t1553702882& unmarshaled)
{
	float unmarshaled_x_temp_0 = 0.0f;
	unmarshaled_x_temp_0 = marshaled.___x_1;
	unmarshaled.set_x_1(unmarshaled_x_temp_0);
	float unmarshaled_y_temp_1 = 0.0f;
	unmarshaled_y_temp_1 = marshaled.___y_2;
	unmarshaled.set_y_2(unmarshaled_y_temp_1);
	float unmarshaled_z_temp_2 = 0.0f;
	unmarshaled_z_temp_2 = marshaled.___z_3;
	unmarshaled.set_z_3(unmarshaled_z_temp_2);
	float unmarshaled_w_temp_3 = 0.0f;
	unmarshaled_w_temp_3 = marshaled.___w_4;
	unmarshaled.set_w_4(unmarshaled_w_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Quaternion
extern "C" void Quaternion_t1553702882_marshal_com_cleanup(Quaternion_t1553702882_marshaled_com& marshaled)
{
}
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
extern "C"  float Random_Range_m3362417303 (Il2CppObject * __this /* static, unused */, float ___min0, float ___max1, const MethodInfo* method)
{
	typedef float (*Random_Range_m3362417303_ftn) (float, float);
	static Random_Range_m3362417303_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_Range_m3362417303_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::Range(System.Single,System.Single)");
	return _il2cpp_icall_func(___min0, ___max1);
}
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C"  int32_t Random_Range_m75452833 (Il2CppObject * __this /* static, unused */, int32_t ___min0, int32_t ___max1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___min0;
		int32_t L_1 = ___max1;
		int32_t L_2 = Random_RandomRangeInt_m1203631415(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)
extern "C"  int32_t Random_RandomRangeInt_m1203631415 (Il2CppObject * __this /* static, unused */, int32_t ___min0, int32_t ___max1, const MethodInfo* method)
{
	typedef int32_t (*Random_RandomRangeInt_m1203631415_ftn) (int32_t, int32_t);
	static Random_RandomRangeInt_m1203631415_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_RandomRangeInt_m1203631415_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)");
	return _il2cpp_icall_func(___min0, ___max1);
}
// System.Single UnityEngine.Random::get_value()
extern "C"  float Random_get_value_m2402066692 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Random_get_value_m2402066692_ftn) ();
	static Random_get_value_m2402066692_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_get_value_m2402066692_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::get_value()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
extern "C"  void RangeAttribute__ctor_m1279576482 (RangeAttribute_t912008995 * __this, float ___min0, float ___max1, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m1741701746(__this, /*hidden argument*/NULL);
		float L_0 = ___min0;
		__this->set_min_0(L_0);
		float L_1 = ___max1;
		__this->set_max_1(L_1);
		return;
	}
}
// System.Void UnityEngine.Ray::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Ray__ctor_m2662468509 (Ray_t3134616544 * __this, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___direction1, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = ___origin0;
		__this->set_m_Origin_0(L_0);
		Vector3_t4282066566  L_1 = Vector3_get_normalized_m2650940353((&___direction1), /*hidden argument*/NULL);
		__this->set_m_Direction_1(L_1);
		return;
	}
}
extern "C"  void Ray__ctor_m2662468509_AdjustorThunk (Il2CppObject * __this, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___direction1, const MethodInfo* method)
{
	Ray_t3134616544 * _thisAdjusted = reinterpret_cast<Ray_t3134616544 *>(__this + 1);
	Ray__ctor_m2662468509(_thisAdjusted, ___origin0, ___direction1, method);
}
// UnityEngine.Vector3 UnityEngine.Ray::get_origin()
extern "C"  Vector3_t4282066566  Ray_get_origin_m3064983562 (Ray_t3134616544 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_m_Origin_0();
		return L_0;
	}
}
extern "C"  Vector3_t4282066566  Ray_get_origin_m3064983562_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Ray_t3134616544 * _thisAdjusted = reinterpret_cast<Ray_t3134616544 *>(__this + 1);
	return Ray_get_origin_m3064983562(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Ray::get_direction()
extern "C"  Vector3_t4282066566  Ray_get_direction_m3201866877 (Ray_t3134616544 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_m_Direction_1();
		return L_0;
	}
}
extern "C"  Vector3_t4282066566  Ray_get_direction_m3201866877_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Ray_t3134616544 * _thisAdjusted = reinterpret_cast<Ray_t3134616544 *>(__this + 1);
	return Ray_get_direction_m3201866877(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Ray::GetPoint(System.Single)
extern "C"  Vector3_t4282066566  Ray_GetPoint_m1171104822 (Ray_t3134616544 * __this, float ___distance0, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_m_Origin_0();
		Vector3_t4282066566  L_1 = __this->get_m_Direction_1();
		float L_2 = ___distance0;
		Vector3_t4282066566  L_3 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t4282066566  L_4 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
extern "C"  Vector3_t4282066566  Ray_GetPoint_m1171104822_AdjustorThunk (Il2CppObject * __this, float ___distance0, const MethodInfo* method)
{
	Ray_t3134616544 * _thisAdjusted = reinterpret_cast<Ray_t3134616544 *>(__this + 1);
	return Ray_GetPoint_m1171104822(_thisAdjusted, ___distance0, method);
}
// System.String UnityEngine.Ray::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1741034756;
extern const uint32_t Ray_ToString_m1391619614_MetadataUsageId;
extern "C"  String_t* Ray_ToString_m1391619614 (Ray_t3134616544 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Ray_ToString_m1391619614_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)2));
		Vector3_t4282066566  L_1 = __this->get_m_Origin_0();
		Vector3_t4282066566  L_2 = L_1;
		Il2CppObject * L_3 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_0;
		Vector3_t4282066566  L_5 = __this->get_m_Direction_1();
		Vector3_t4282066566  L_6 = L_5;
		Il2CppObject * L_7 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		String_t* L_8 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral1741034756, L_4, /*hidden argument*/NULL);
		return L_8;
	}
}
extern "C"  String_t* Ray_ToString_m1391619614_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Ray_t3134616544 * _thisAdjusted = reinterpret_cast<Ray_t3134616544 *>(__this + 1);
	return Ray_ToString_m1391619614(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.Ray
extern "C" void Ray_t3134616544_marshal_pinvoke(const Ray_t3134616544& unmarshaled, Ray_t3134616544_marshaled_pinvoke& marshaled)
{
	Vector3_t4282066566_marshal_pinvoke(unmarshaled.get_m_Origin_0(), marshaled.___m_Origin_0);
	Vector3_t4282066566_marshal_pinvoke(unmarshaled.get_m_Direction_1(), marshaled.___m_Direction_1);
}
extern "C" void Ray_t3134616544_marshal_pinvoke_back(const Ray_t3134616544_marshaled_pinvoke& marshaled, Ray_t3134616544& unmarshaled)
{
	Vector3_t4282066566  unmarshaled_m_Origin_temp_0;
	memset(&unmarshaled_m_Origin_temp_0, 0, sizeof(unmarshaled_m_Origin_temp_0));
	Vector3_t4282066566_marshal_pinvoke_back(marshaled.___m_Origin_0, unmarshaled_m_Origin_temp_0);
	unmarshaled.set_m_Origin_0(unmarshaled_m_Origin_temp_0);
	Vector3_t4282066566  unmarshaled_m_Direction_temp_1;
	memset(&unmarshaled_m_Direction_temp_1, 0, sizeof(unmarshaled_m_Direction_temp_1));
	Vector3_t4282066566_marshal_pinvoke_back(marshaled.___m_Direction_1, unmarshaled_m_Direction_temp_1);
	unmarshaled.set_m_Direction_1(unmarshaled_m_Direction_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Ray
extern "C" void Ray_t3134616544_marshal_pinvoke_cleanup(Ray_t3134616544_marshaled_pinvoke& marshaled)
{
	Vector3_t4282066566_marshal_pinvoke_cleanup(marshaled.___m_Origin_0);
	Vector3_t4282066566_marshal_pinvoke_cleanup(marshaled.___m_Direction_1);
}
// Conversion methods for marshalling of: UnityEngine.Ray
extern "C" void Ray_t3134616544_marshal_com(const Ray_t3134616544& unmarshaled, Ray_t3134616544_marshaled_com& marshaled)
{
	Vector3_t4282066566_marshal_com(unmarshaled.get_m_Origin_0(), marshaled.___m_Origin_0);
	Vector3_t4282066566_marshal_com(unmarshaled.get_m_Direction_1(), marshaled.___m_Direction_1);
}
extern "C" void Ray_t3134616544_marshal_com_back(const Ray_t3134616544_marshaled_com& marshaled, Ray_t3134616544& unmarshaled)
{
	Vector3_t4282066566  unmarshaled_m_Origin_temp_0;
	memset(&unmarshaled_m_Origin_temp_0, 0, sizeof(unmarshaled_m_Origin_temp_0));
	Vector3_t4282066566_marshal_com_back(marshaled.___m_Origin_0, unmarshaled_m_Origin_temp_0);
	unmarshaled.set_m_Origin_0(unmarshaled_m_Origin_temp_0);
	Vector3_t4282066566  unmarshaled_m_Direction_temp_1;
	memset(&unmarshaled_m_Direction_temp_1, 0, sizeof(unmarshaled_m_Direction_temp_1));
	Vector3_t4282066566_marshal_com_back(marshaled.___m_Direction_1, unmarshaled_m_Direction_temp_1);
	unmarshaled.set_m_Direction_1(unmarshaled_m_Direction_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Ray
extern "C" void Ray_t3134616544_marshal_com_cleanup(Ray_t3134616544_marshaled_com& marshaled)
{
	Vector3_t4282066566_marshal_com_cleanup(marshaled.___m_Origin_0);
	Vector3_t4282066566_marshal_com_cleanup(marshaled.___m_Direction_1);
}
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
extern "C"  Vector3_t4282066566  RaycastHit_get_point_m4165497838 (RaycastHit_t4003175726 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_m_Point_0();
		return L_0;
	}
}
extern "C"  Vector3_t4282066566  RaycastHit_get_point_m4165497838_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t4003175726 * _thisAdjusted = reinterpret_cast<RaycastHit_t4003175726 *>(__this + 1);
	return RaycastHit_get_point_m4165497838(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
extern "C"  Vector3_t4282066566  RaycastHit_get_normal_m1346998891 (RaycastHit_t4003175726 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_m_Normal_1();
		return L_0;
	}
}
extern "C"  Vector3_t4282066566  RaycastHit_get_normal_m1346998891_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t4003175726 * _thisAdjusted = reinterpret_cast<RaycastHit_t4003175726 *>(__this + 1);
	return RaycastHit_get_normal_m1346998891(_thisAdjusted, method);
}
// System.Single UnityEngine.RaycastHit::get_distance()
extern "C"  float RaycastHit_get_distance_m800944203 (RaycastHit_t4003175726 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Distance_3();
		return L_0;
	}
}
extern "C"  float RaycastHit_get_distance_m800944203_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t4003175726 * _thisAdjusted = reinterpret_cast<RaycastHit_t4003175726 *>(__this + 1);
	return RaycastHit_get_distance_m800944203(_thisAdjusted, method);
}
// UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
extern "C"  Collider_t2939674232 * RaycastHit_get_collider_m3116882274 (RaycastHit_t4003175726 * __this, const MethodInfo* method)
{
	{
		Collider_t2939674232 * L_0 = __this->get_m_Collider_5();
		return L_0;
	}
}
extern "C"  Collider_t2939674232 * RaycastHit_get_collider_m3116882274_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t4003175726 * _thisAdjusted = reinterpret_cast<RaycastHit_t4003175726 *>(__this + 1);
	return RaycastHit_get_collider_m3116882274(_thisAdjusted, method);
}
// UnityEngine.Rigidbody UnityEngine.RaycastHit::get_rigidbody()
extern "C"  Rigidbody_t3346577219 * RaycastHit_get_rigidbody_m4137883432 (RaycastHit_t4003175726 * __this, const MethodInfo* method)
{
	Rigidbody_t3346577219 * G_B3_0 = NULL;
	{
		Collider_t2939674232 * L_0 = RaycastHit_get_collider_m3116882274(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Collider_t2939674232 * L_2 = RaycastHit_get_collider_m3116882274(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Rigidbody_t3346577219 * L_3 = Collider_get_attachedRigidbody_m2821754842(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = ((Rigidbody_t3346577219 *)(NULL));
	}

IL_0022:
	{
		return G_B3_0;
	}
}
extern "C"  Rigidbody_t3346577219 * RaycastHit_get_rigidbody_m4137883432_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t4003175726 * _thisAdjusted = reinterpret_cast<RaycastHit_t4003175726 *>(__this + 1);
	return RaycastHit_get_rigidbody_m4137883432(_thisAdjusted, method);
}
// UnityEngine.Transform UnityEngine.RaycastHit::get_transform()
extern "C"  Transform_t1659122786 * RaycastHit_get_transform_m905149094 (RaycastHit_t4003175726 * __this, const MethodInfo* method)
{
	Rigidbody_t3346577219 * V_0 = NULL;
	{
		Rigidbody_t3346577219 * L_0 = RaycastHit_get_rigidbody_m4137883432(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Rigidbody_t3346577219 * L_1 = V_0;
		bool L_2 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		Rigidbody_t3346577219 * L_3 = V_0;
		NullCheck(L_3);
		Transform_t1659122786 * L_4 = Component_get_transform_m4257140443(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_001a:
	{
		Collider_t2939674232 * L_5 = RaycastHit_get_collider_m3116882274(__this, /*hidden argument*/NULL);
		bool L_6 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_5, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		Collider_t2939674232 * L_7 = RaycastHit_get_collider_m3116882274(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t1659122786 * L_8 = Component_get_transform_m4257140443(L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0037:
	{
		return (Transform_t1659122786 *)NULL;
	}
}
extern "C"  Transform_t1659122786 * RaycastHit_get_transform_m905149094_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t4003175726 * _thisAdjusted = reinterpret_cast<RaycastHit_t4003175726 *>(__this + 1);
	return RaycastHit_get_transform_m905149094(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.RaycastHit
extern "C" void RaycastHit_t4003175726_marshal_pinvoke(const RaycastHit_t4003175726& unmarshaled, RaycastHit_t4003175726_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
extern "C" void RaycastHit_t4003175726_marshal_pinvoke_back(const RaycastHit_t4003175726_marshaled_pinvoke& marshaled, RaycastHit_t4003175726& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RaycastHit
extern "C" void RaycastHit_t4003175726_marshal_pinvoke_cleanup(RaycastHit_t4003175726_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.RaycastHit
extern "C" void RaycastHit_t4003175726_marshal_com(const RaycastHit_t4003175726& unmarshaled, RaycastHit_t4003175726_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
extern "C" void RaycastHit_t4003175726_marshal_com_back(const RaycastHit_t4003175726_marshaled_com& marshaled, RaycastHit_t4003175726& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RaycastHit
extern "C" void RaycastHit_t4003175726_marshal_com_cleanup(RaycastHit_t4003175726_marshaled_com& marshaled)
{
}
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_point()
extern "C"  Vector2_t4282066565  RaycastHit2D_get_point_m2072691227 (RaycastHit2D_t1374744384 * __this, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0 = __this->get_m_Point_1();
		return L_0;
	}
}
extern "C"  Vector2_t4282066565  RaycastHit2D_get_point_m2072691227_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t1374744384 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t1374744384 *>(__this + 1);
	return RaycastHit2D_get_point_m2072691227(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_normal()
extern "C"  Vector2_t4282066565  RaycastHit2D_get_normal_m894503390 (RaycastHit2D_t1374744384 * __this, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0 = __this->get_m_Normal_2();
		return L_0;
	}
}
extern "C"  Vector2_t4282066565  RaycastHit2D_get_normal_m894503390_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t1374744384 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t1374744384 *>(__this + 1);
	return RaycastHit2D_get_normal_m894503390(_thisAdjusted, method);
}
// System.Single UnityEngine.RaycastHit2D::get_fraction()
extern "C"  float RaycastHit2D_get_fraction_m2313516650 (RaycastHit2D_t1374744384 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Fraction_4();
		return L_0;
	}
}
extern "C"  float RaycastHit2D_get_fraction_m2313516650_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t1374744384 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t1374744384 *>(__this + 1);
	return RaycastHit2D_get_fraction_m2313516650(_thisAdjusted, method);
}
// UnityEngine.Collider2D UnityEngine.RaycastHit2D::get_collider()
extern "C"  Collider2D_t1552025098 * RaycastHit2D_get_collider_m789902306 (RaycastHit2D_t1374744384 * __this, const MethodInfo* method)
{
	{
		Collider2D_t1552025098 * L_0 = __this->get_m_Collider_5();
		return L_0;
	}
}
extern "C"  Collider2D_t1552025098 * RaycastHit2D_get_collider_m789902306_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t1374744384 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t1374744384 *>(__this + 1);
	return RaycastHit2D_get_collider_m789902306(_thisAdjusted, method);
}
// UnityEngine.Rigidbody2D UnityEngine.RaycastHit2D::get_rigidbody()
extern "C"  Rigidbody2D_t1743771669 * RaycastHit2D_get_rigidbody_m1059160360 (RaycastHit2D_t1374744384 * __this, const MethodInfo* method)
{
	Rigidbody2D_t1743771669 * G_B3_0 = NULL;
	{
		Collider2D_t1552025098 * L_0 = RaycastHit2D_get_collider_m789902306(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Collider2D_t1552025098 * L_2 = RaycastHit2D_get_collider_m789902306(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Rigidbody2D_t1743771669 * L_3 = Collider2D_get_attachedRigidbody_m2908627162(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = ((Rigidbody2D_t1743771669 *)(NULL));
	}

IL_0022:
	{
		return G_B3_0;
	}
}
extern "C"  Rigidbody2D_t1743771669 * RaycastHit2D_get_rigidbody_m1059160360_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t1374744384 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t1374744384 *>(__this + 1);
	return RaycastHit2D_get_rigidbody_m1059160360(_thisAdjusted, method);
}
// UnityEngine.Transform UnityEngine.RaycastHit2D::get_transform()
extern "C"  Transform_t1659122786 * RaycastHit2D_get_transform_m1318597140 (RaycastHit2D_t1374744384 * __this, const MethodInfo* method)
{
	Rigidbody2D_t1743771669 * V_0 = NULL;
	{
		Rigidbody2D_t1743771669 * L_0 = RaycastHit2D_get_rigidbody_m1059160360(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Rigidbody2D_t1743771669 * L_1 = V_0;
		bool L_2 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		Rigidbody2D_t1743771669 * L_3 = V_0;
		NullCheck(L_3);
		Transform_t1659122786 * L_4 = Component_get_transform_m4257140443(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_001a:
	{
		Collider2D_t1552025098 * L_5 = RaycastHit2D_get_collider_m789902306(__this, /*hidden argument*/NULL);
		bool L_6 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_5, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		Collider2D_t1552025098 * L_7 = RaycastHit2D_get_collider_m789902306(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t1659122786 * L_8 = Component_get_transform_m4257140443(L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0037:
	{
		return (Transform_t1659122786 *)NULL;
	}
}
extern "C"  Transform_t1659122786 * RaycastHit2D_get_transform_m1318597140_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t1374744384 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t1374744384 *>(__this + 1);
	return RaycastHit2D_get_transform_m1318597140(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.RaycastHit2D
extern "C" void RaycastHit2D_t1374744384_marshal_pinvoke(const RaycastHit2D_t1374744384& unmarshaled, RaycastHit2D_t1374744384_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
extern "C" void RaycastHit2D_t1374744384_marshal_pinvoke_back(const RaycastHit2D_t1374744384_marshaled_pinvoke& marshaled, RaycastHit2D_t1374744384& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RaycastHit2D
extern "C" void RaycastHit2D_t1374744384_marshal_pinvoke_cleanup(RaycastHit2D_t1374744384_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.RaycastHit2D
extern "C" void RaycastHit2D_t1374744384_marshal_com(const RaycastHit2D_t1374744384& unmarshaled, RaycastHit2D_t1374744384_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
extern "C" void RaycastHit2D_t1374744384_marshal_com_back(const RaycastHit2D_t1374744384_marshaled_com& marshaled, RaycastHit2D_t1374744384& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RaycastHit2D
extern "C" void RaycastHit2D_t1374744384_marshal_com_cleanup(RaycastHit2D_t1374744384_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m3291325233 (Rect_t4241904616 * __this, float ___x0, float ___y1, float ___width2, float ___height3, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_m_XMin_0(L_0);
		float L_1 = ___y1;
		__this->set_m_YMin_1(L_1);
		float L_2 = ___width2;
		__this->set_m_Width_2(L_2);
		float L_3 = ___height3;
		__this->set_m_Height_3(L_3);
		return;
	}
}
extern "C"  void Rect__ctor_m3291325233_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, float ___width2, float ___height3, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect__ctor_m3291325233(_thisAdjusted, ___x0, ___y1, ___width2, ___height3, method);
}
// System.Void UnityEngine.Rect::.ctor(UnityEngine.Rect)
extern "C"  void Rect__ctor_m3858381006 (Rect_t4241904616 * __this, Rect_t4241904616  ___source0, const MethodInfo* method)
{
	{
		float L_0 = (&___source0)->get_m_XMin_0();
		__this->set_m_XMin_0(L_0);
		float L_1 = (&___source0)->get_m_YMin_1();
		__this->set_m_YMin_1(L_1);
		float L_2 = (&___source0)->get_m_Width_2();
		__this->set_m_Width_2(L_2);
		float L_3 = (&___source0)->get_m_Height_3();
		__this->set_m_Height_3(L_3);
		return;
	}
}
extern "C"  void Rect__ctor_m3858381006_AdjustorThunk (Il2CppObject * __this, Rect_t4241904616  ___source0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect__ctor_m3858381006(_thisAdjusted, ___source0, method);
}
// UnityEngine.Rect UnityEngine.Rect::MinMaxRect(System.Single,System.Single,System.Single,System.Single)
extern "C"  Rect_t4241904616  Rect_MinMaxRect_m1534690677 (Il2CppObject * __this /* static, unused */, float ___xmin0, float ___ymin1, float ___xmax2, float ___ymax3, const MethodInfo* method)
{
	{
		float L_0 = ___xmin0;
		float L_1 = ___ymin1;
		float L_2 = ___xmax2;
		float L_3 = ___xmin0;
		float L_4 = ___ymax3;
		float L_5 = ___ymin1;
		Rect_t4241904616  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Rect__ctor_m3291325233(&L_6, L_0, L_1, ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Single UnityEngine.Rect::get_x()
extern "C"  float Rect_get_x_m982385354 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_XMin_0();
		return L_0;
	}
}
extern "C"  float Rect_get_x_m982385354_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_x_m982385354(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_x(System.Single)
extern "C"  void Rect_set_x_m577970569 (Rect_t4241904616 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_XMin_0(L_0);
		return;
	}
}
extern "C"  void Rect_set_x_m577970569_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_set_x_m577970569(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_y()
extern "C"  float Rect_get_y_m982386315 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_YMin_1();
		return L_0;
	}
}
extern "C"  float Rect_get_y_m982386315_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_y_m982386315(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_y(System.Single)
extern "C"  void Rect_set_y_m67436392 (Rect_t4241904616 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_YMin_1(L_0);
		return;
	}
}
extern "C"  void Rect_set_y_m67436392_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_set_y_m67436392(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_position()
extern "C"  Vector2_t4282066565  Rect_get_position_m2933356232 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_XMin_0();
		float L_1 = __this->get_m_YMin_1();
		Vector2_t4282066565  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m1517109030(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  Vector2_t4282066565  Rect_get_position_m2933356232_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_position_m2933356232(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_center()
extern "C"  Vector2_t4282066565  Rect_get_center_m610643572 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = Rect_get_x_m982385354(__this, /*hidden argument*/NULL);
		float L_1 = __this->get_m_Width_2();
		float L_2 = Rect_get_y_m982386315(__this, /*hidden argument*/NULL);
		float L_3 = __this->get_m_Height_3();
		Vector2_t4282066565  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m1517109030(&L_4, ((float)((float)L_0+(float)((float)((float)L_1/(float)(2.0f))))), ((float)((float)L_2+(float)((float)((float)L_3/(float)(2.0f))))), /*hidden argument*/NULL);
		return L_4;
	}
}
extern "C"  Vector2_t4282066565  Rect_get_center_m610643572_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_center_m610643572(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_min()
extern "C"  Vector2_t4282066565  Rect_get_min_m275942709 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = Rect_get_xMin_m371109962(__this, /*hidden argument*/NULL);
		float L_1 = Rect_get_yMin_m399739113(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m1517109030(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  Vector2_t4282066565  Rect_get_min_m275942709_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_min_m275942709(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_max()
extern "C"  Vector2_t4282066565  Rect_get_max_m275713991 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = Rect_get_xMax_m370881244(__this, /*hidden argument*/NULL);
		float L_1 = Rect_get_yMax_m399510395(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m1517109030(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  Vector2_t4282066565  Rect_get_max_m275713991_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_max_m275713991(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_width()
extern "C"  float Rect_get_width_m2824209432 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Width_2();
		return L_0;
	}
}
extern "C"  float Rect_get_width_m2824209432_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_width_m2824209432(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_width(System.Single)
extern "C"  void Rect_set_width_m3771513595 (Rect_t4241904616 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Width_2(L_0);
		return;
	}
}
extern "C"  void Rect_set_width_m3771513595_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_set_width_m3771513595(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_height()
extern "C"  float Rect_get_height_m2154960823 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Height_3();
		return L_0;
	}
}
extern "C"  float Rect_get_height_m2154960823_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_height_m2154960823(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_height(System.Single)
extern "C"  void Rect_set_height_m3398820332 (Rect_t4241904616 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Height_3(L_0);
		return;
	}
}
extern "C"  void Rect_set_height_m3398820332_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_set_height_m3398820332(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_size()
extern "C"  Vector2_t4282066565  Rect_get_size_m136480416 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Width_2();
		float L_1 = __this->get_m_Height_3();
		Vector2_t4282066565  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m1517109030(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  Vector2_t4282066565  Rect_get_size_m136480416_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_size_m136480416(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_xMin()
extern "C"  float Rect_get_xMin_m371109962 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_XMin_0();
		return L_0;
	}
}
extern "C"  float Rect_get_xMin_m371109962_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_xMin_m371109962(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_xMin(System.Single)
extern "C"  void Rect_set_xMin_m265803321 (Rect_t4241904616 * __this, float ___value0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = Rect_get_xMax_m370881244(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___value0;
		__this->set_m_XMin_0(L_1);
		float L_2 = V_0;
		float L_3 = __this->get_m_XMin_0();
		__this->set_m_Width_2(((float)((float)L_2-(float)L_3)));
		return;
	}
}
extern "C"  void Rect_set_xMin_m265803321_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_set_xMin_m265803321(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_yMin()
extern "C"  float Rect_get_yMin_m399739113 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_YMin_1();
		return L_0;
	}
}
extern "C"  float Rect_get_yMin_m399739113_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_yMin_m399739113(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_yMin(System.Single)
extern "C"  void Rect_set_yMin_m3716298746 (Rect_t4241904616 * __this, float ___value0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = Rect_get_yMax_m399510395(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___value0;
		__this->set_m_YMin_1(L_1);
		float L_2 = V_0;
		float L_3 = __this->get_m_YMin_1();
		__this->set_m_Height_3(((float)((float)L_2-(float)L_3)));
		return;
	}
}
extern "C"  void Rect_set_yMin_m3716298746_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_set_yMin_m3716298746(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_xMax()
extern "C"  float Rect_get_xMax_m370881244 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Width_2();
		float L_1 = __this->get_m_XMin_0();
		return ((float)((float)L_0+(float)L_1));
	}
}
extern "C"  float Rect_get_xMax_m370881244_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_xMax_m370881244(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_xMax(System.Single)
extern "C"  void Rect_set_xMax_m1513853159 (Rect_t4241904616 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		float L_1 = __this->get_m_XMin_0();
		__this->set_m_Width_2(((float)((float)L_0-(float)L_1)));
		return;
	}
}
extern "C"  void Rect_set_xMax_m1513853159_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_set_xMax_m1513853159(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_yMax()
extern "C"  float Rect_get_yMax_m399510395 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Height_3();
		float L_1 = __this->get_m_YMin_1();
		return ((float)((float)L_0+(float)L_1));
	}
}
extern "C"  float Rect_get_yMax_m399510395_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_yMax_m399510395(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_yMax(System.Single)
extern "C"  void Rect_set_yMax_m669381288 (Rect_t4241904616 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		float L_1 = __this->get_m_YMin_1();
		__this->set_m_Height_3(((float)((float)L_0-(float)L_1)));
		return;
	}
}
extern "C"  void Rect_set_yMax_m669381288_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_set_yMax_m669381288(_thisAdjusted, ___value0, method);
}
// System.String UnityEngine.Rect::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2721079081;
extern const uint32_t Rect_ToString_m2093687658_MetadataUsageId;
extern "C"  String_t* Rect_ToString_m2093687658 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Rect_ToString_m2093687658_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = Rect_get_x_m982385354(__this, /*hidden argument*/NULL);
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_0;
		float L_5 = Rect_get_y_m982386315(__this, /*hidden argument*/NULL);
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t1108656482* L_8 = L_4;
		float L_9 = Rect_get_width_m2824209432(__this, /*hidden argument*/NULL);
		float L_10 = L_9;
		Il2CppObject * L_11 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t1108656482* L_12 = L_8;
		float L_13 = Rect_get_height_m2154960823(__this, /*hidden argument*/NULL);
		float L_14 = L_13;
		Il2CppObject * L_15 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral2721079081, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
extern "C"  String_t* Rect_ToString_m2093687658_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_ToString_m2093687658(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector2)
extern "C"  bool Rect_Contains_m3556594010 (Rect_t4241904616 * __this, Vector2_t4282066565  ___point0, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = (&___point0)->get_x_1();
		float L_1 = Rect_get_xMin_m371109962(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) >= ((float)L_1))))
		{
			goto IL_0047;
		}
	}
	{
		float L_2 = (&___point0)->get_x_1();
		float L_3 = Rect_get_xMax_m370881244(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0047;
		}
	}
	{
		float L_4 = (&___point0)->get_y_2();
		float L_5 = Rect_get_yMin_m399739113(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) >= ((float)L_5))))
		{
			goto IL_0047;
		}
	}
	{
		float L_6 = (&___point0)->get_y_2();
		float L_7 = Rect_get_yMax_m399510395(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0048;
	}

IL_0047:
	{
		G_B5_0 = 0;
	}

IL_0048:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool Rect_Contains_m3556594010_AdjustorThunk (Il2CppObject * __this, Vector2_t4282066565  ___point0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_Contains_m3556594010(_thisAdjusted, ___point0, method);
}
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector3)
extern "C"  bool Rect_Contains_m3556594041 (Rect_t4241904616 * __this, Vector3_t4282066566  ___point0, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = (&___point0)->get_x_1();
		float L_1 = Rect_get_xMin_m371109962(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) >= ((float)L_1))))
		{
			goto IL_0047;
		}
	}
	{
		float L_2 = (&___point0)->get_x_1();
		float L_3 = Rect_get_xMax_m370881244(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0047;
		}
	}
	{
		float L_4 = (&___point0)->get_y_2();
		float L_5 = Rect_get_yMin_m399739113(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) >= ((float)L_5))))
		{
			goto IL_0047;
		}
	}
	{
		float L_6 = (&___point0)->get_y_2();
		float L_7 = Rect_get_yMax_m399510395(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0048;
	}

IL_0047:
	{
		G_B5_0 = 0;
	}

IL_0048:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool Rect_Contains_m3556594041_AdjustorThunk (Il2CppObject * __this, Vector3_t4282066566  ___point0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_Contains_m3556594041(_thisAdjusted, ___point0, method);
}
// UnityEngine.Rect UnityEngine.Rect::OrderMinMax(UnityEngine.Rect)
extern "C"  Rect_t4241904616  Rect_OrderMinMax_m3424313368 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___rect0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = Rect_get_xMin_m371109962((&___rect0), /*hidden argument*/NULL);
		float L_1 = Rect_get_xMax_m370881244((&___rect0), /*hidden argument*/NULL);
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_0031;
		}
	}
	{
		float L_2 = Rect_get_xMin_m371109962((&___rect0), /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_xMax_m370881244((&___rect0), /*hidden argument*/NULL);
		Rect_set_xMin_m265803321((&___rect0), L_3, /*hidden argument*/NULL);
		float L_4 = V_0;
		Rect_set_xMax_m1513853159((&___rect0), L_4, /*hidden argument*/NULL);
	}

IL_0031:
	{
		float L_5 = Rect_get_yMin_m399739113((&___rect0), /*hidden argument*/NULL);
		float L_6 = Rect_get_yMax_m399510395((&___rect0), /*hidden argument*/NULL);
		if ((!(((float)L_5) > ((float)L_6))))
		{
			goto IL_0062;
		}
	}
	{
		float L_7 = Rect_get_yMin_m399739113((&___rect0), /*hidden argument*/NULL);
		V_1 = L_7;
		float L_8 = Rect_get_yMax_m399510395((&___rect0), /*hidden argument*/NULL);
		Rect_set_yMin_m3716298746((&___rect0), L_8, /*hidden argument*/NULL);
		float L_9 = V_1;
		Rect_set_yMax_m669381288((&___rect0), L_9, /*hidden argument*/NULL);
	}

IL_0062:
	{
		Rect_t4241904616  L_10 = ___rect0;
		return L_10;
	}
}
// System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect)
extern "C"  bool Rect_Overlaps_m669681106 (Rect_t4241904616 * __this, Rect_t4241904616  ___other0, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = Rect_get_xMax_m370881244((&___other0), /*hidden argument*/NULL);
		float L_1 = Rect_get_xMin_m371109962(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_0047;
		}
	}
	{
		float L_2 = Rect_get_xMin_m371109962((&___other0), /*hidden argument*/NULL);
		float L_3 = Rect_get_xMax_m370881244(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0047;
		}
	}
	{
		float L_4 = Rect_get_yMax_m399510395((&___other0), /*hidden argument*/NULL);
		float L_5 = Rect_get_yMin_m399739113(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) > ((float)L_5))))
		{
			goto IL_0047;
		}
	}
	{
		float L_6 = Rect_get_yMin_m399739113((&___other0), /*hidden argument*/NULL);
		float L_7 = Rect_get_yMax_m399510395(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0048;
	}

IL_0047:
	{
		G_B5_0 = 0;
	}

IL_0048:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool Rect_Overlaps_m669681106_AdjustorThunk (Il2CppObject * __this, Rect_t4241904616  ___other0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_Overlaps_m669681106(_thisAdjusted, ___other0, method);
}
// System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect,System.Boolean)
extern "C"  bool Rect_Overlaps_m2751672171 (Rect_t4241904616 * __this, Rect_t4241904616  ___other0, bool ___allowInverse1, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		V_0 = (*(Rect_t4241904616 *)__this);
		bool L_0 = ___allowInverse1;
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Rect_t4241904616  L_1 = V_0;
		Rect_t4241904616  L_2 = Rect_OrderMinMax_m3424313368(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Rect_t4241904616  L_3 = ___other0;
		Rect_t4241904616  L_4 = Rect_OrderMinMax_m3424313368(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		___other0 = L_4;
	}

IL_001c:
	{
		Rect_t4241904616  L_5 = ___other0;
		bool L_6 = Rect_Overlaps_m669681106((&V_0), L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  bool Rect_Overlaps_m2751672171_AdjustorThunk (Il2CppObject * __this, Rect_t4241904616  ___other0, bool ___allowInverse1, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_Overlaps_m2751672171(_thisAdjusted, ___other0, ___allowInverse1, method);
}
// System.Int32 UnityEngine.Rect::GetHashCode()
extern "C"  int32_t Rect_GetHashCode_m89026168 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		float L_0 = Rect_get_x_m982385354(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Single_GetHashCode_m65342520((&V_0), /*hidden argument*/NULL);
		float L_2 = Rect_get_width_m2824209432(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Single_GetHashCode_m65342520((&V_1), /*hidden argument*/NULL);
		float L_4 = Rect_get_y_m982386315(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = Single_GetHashCode_m65342520((&V_2), /*hidden argument*/NULL);
		float L_6 = Rect_get_height_m2154960823(__this, /*hidden argument*/NULL);
		V_3 = L_6;
		int32_t L_7 = Single_GetHashCode_m65342520((&V_3), /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
	}
}
extern "C"  int32_t Rect_GetHashCode_m89026168_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_GetHashCode_m89026168(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Rect::Equals(System.Object)
extern Il2CppClass* Rect_t4241904616_il2cpp_TypeInfo_var;
extern const uint32_t Rect_Equals_m1091722644_MetadataUsageId;
extern "C"  bool Rect_Equals_m1091722644 (Rect_t4241904616 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Rect_Equals_m1091722644_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Rect_t4241904616_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(Rect_t4241904616 *)((Rect_t4241904616 *)UnBox (L_1, Rect_t4241904616_il2cpp_TypeInfo_var))));
		float L_2 = Rect_get_x_m982385354(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		float L_3 = Rect_get_x_m982385354((&V_0), /*hidden argument*/NULL);
		bool L_4 = Single_Equals_m2110115959((&V_1), L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_007a;
		}
	}
	{
		float L_5 = Rect_get_y_m982386315(__this, /*hidden argument*/NULL);
		V_2 = L_5;
		float L_6 = Rect_get_y_m982386315((&V_0), /*hidden argument*/NULL);
		bool L_7 = Single_Equals_m2110115959((&V_2), L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_007a;
		}
	}
	{
		float L_8 = Rect_get_width_m2824209432(__this, /*hidden argument*/NULL);
		V_3 = L_8;
		float L_9 = Rect_get_width_m2824209432((&V_0), /*hidden argument*/NULL);
		bool L_10 = Single_Equals_m2110115959((&V_3), L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_007a;
		}
	}
	{
		float L_11 = Rect_get_height_m2154960823(__this, /*hidden argument*/NULL);
		V_4 = L_11;
		float L_12 = Rect_get_height_m2154960823((&V_0), /*hidden argument*/NULL);
		bool L_13 = Single_Equals_m2110115959((&V_4), L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_007b;
	}

IL_007a:
	{
		G_B7_0 = 0;
	}

IL_007b:
	{
		return (bool)G_B7_0;
	}
}
extern "C"  bool Rect_Equals_m1091722644_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_Equals_m1091722644(_thisAdjusted, ___other0, method);
}
// System.Boolean UnityEngine.Rect::op_Inequality(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool Rect_op_Inequality_m2236552616 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___lhs0, Rect_t4241904616  ___rhs1, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = Rect_get_x_m982385354((&___lhs0), /*hidden argument*/NULL);
		float L_1 = Rect_get_x_m982385354((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_004e;
		}
	}
	{
		float L_2 = Rect_get_y_m982386315((&___lhs0), /*hidden argument*/NULL);
		float L_3 = Rect_get_y_m982386315((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_2) == ((float)L_3))))
		{
			goto IL_004e;
		}
	}
	{
		float L_4 = Rect_get_width_m2824209432((&___lhs0), /*hidden argument*/NULL);
		float L_5 = Rect_get_width_m2824209432((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_4) == ((float)L_5))))
		{
			goto IL_004e;
		}
	}
	{
		float L_6 = Rect_get_height_m2154960823((&___lhs0), /*hidden argument*/NULL);
		float L_7 = Rect_get_height_m2154960823((&___rhs1), /*hidden argument*/NULL);
		G_B5_0 = ((((int32_t)((((float)L_6) == ((float)L_7))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_004f;
	}

IL_004e:
	{
		G_B5_0 = 1;
	}

IL_004f:
	{
		return (bool)G_B5_0;
	}
}
// System.Boolean UnityEngine.Rect::op_Equality(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool Rect_op_Equality_m1552341101 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___lhs0, Rect_t4241904616  ___rhs1, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = Rect_get_x_m982385354((&___lhs0), /*hidden argument*/NULL);
		float L_1 = Rect_get_x_m982385354((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_004b;
		}
	}
	{
		float L_2 = Rect_get_y_m982386315((&___lhs0), /*hidden argument*/NULL);
		float L_3 = Rect_get_y_m982386315((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_2) == ((float)L_3))))
		{
			goto IL_004b;
		}
	}
	{
		float L_4 = Rect_get_width_m2824209432((&___lhs0), /*hidden argument*/NULL);
		float L_5 = Rect_get_width_m2824209432((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_4) == ((float)L_5))))
		{
			goto IL_004b;
		}
	}
	{
		float L_6 = Rect_get_height_m2154960823((&___lhs0), /*hidden argument*/NULL);
		float L_7 = Rect_get_height_m2154960823((&___rhs1), /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) == ((float)L_7))? 1 : 0);
		goto IL_004c;
	}

IL_004b:
	{
		G_B5_0 = 0;
	}

IL_004c:
	{
		return (bool)G_B5_0;
	}
}
// Conversion methods for marshalling of: UnityEngine.Rect
extern "C" void Rect_t4241904616_marshal_pinvoke(const Rect_t4241904616& unmarshaled, Rect_t4241904616_marshaled_pinvoke& marshaled)
{
	marshaled.___m_XMin_0 = unmarshaled.get_m_XMin_0();
	marshaled.___m_YMin_1 = unmarshaled.get_m_YMin_1();
	marshaled.___m_Width_2 = unmarshaled.get_m_Width_2();
	marshaled.___m_Height_3 = unmarshaled.get_m_Height_3();
}
extern "C" void Rect_t4241904616_marshal_pinvoke_back(const Rect_t4241904616_marshaled_pinvoke& marshaled, Rect_t4241904616& unmarshaled)
{
	float unmarshaled_m_XMin_temp_0 = 0.0f;
	unmarshaled_m_XMin_temp_0 = marshaled.___m_XMin_0;
	unmarshaled.set_m_XMin_0(unmarshaled_m_XMin_temp_0);
	float unmarshaled_m_YMin_temp_1 = 0.0f;
	unmarshaled_m_YMin_temp_1 = marshaled.___m_YMin_1;
	unmarshaled.set_m_YMin_1(unmarshaled_m_YMin_temp_1);
	float unmarshaled_m_Width_temp_2 = 0.0f;
	unmarshaled_m_Width_temp_2 = marshaled.___m_Width_2;
	unmarshaled.set_m_Width_2(unmarshaled_m_Width_temp_2);
	float unmarshaled_m_Height_temp_3 = 0.0f;
	unmarshaled_m_Height_temp_3 = marshaled.___m_Height_3;
	unmarshaled.set_m_Height_3(unmarshaled_m_Height_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Rect
extern "C" void Rect_t4241904616_marshal_pinvoke_cleanup(Rect_t4241904616_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Rect
extern "C" void Rect_t4241904616_marshal_com(const Rect_t4241904616& unmarshaled, Rect_t4241904616_marshaled_com& marshaled)
{
	marshaled.___m_XMin_0 = unmarshaled.get_m_XMin_0();
	marshaled.___m_YMin_1 = unmarshaled.get_m_YMin_1();
	marshaled.___m_Width_2 = unmarshaled.get_m_Width_2();
	marshaled.___m_Height_3 = unmarshaled.get_m_Height_3();
}
extern "C" void Rect_t4241904616_marshal_com_back(const Rect_t4241904616_marshaled_com& marshaled, Rect_t4241904616& unmarshaled)
{
	float unmarshaled_m_XMin_temp_0 = 0.0f;
	unmarshaled_m_XMin_temp_0 = marshaled.___m_XMin_0;
	unmarshaled.set_m_XMin_0(unmarshaled_m_XMin_temp_0);
	float unmarshaled_m_YMin_temp_1 = 0.0f;
	unmarshaled_m_YMin_temp_1 = marshaled.___m_YMin_1;
	unmarshaled.set_m_YMin_1(unmarshaled_m_YMin_temp_1);
	float unmarshaled_m_Width_temp_2 = 0.0f;
	unmarshaled_m_Width_temp_2 = marshaled.___m_Width_2;
	unmarshaled.set_m_Width_2(unmarshaled_m_Width_temp_2);
	float unmarshaled_m_Height_temp_3 = 0.0f;
	unmarshaled_m_Height_temp_3 = marshaled.___m_Height_3;
	unmarshaled.set_m_Height_3(unmarshaled_m_Height_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Rect
extern "C" void Rect_t4241904616_marshal_com_cleanup(Rect_t4241904616_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.RectOffset::.ctor()
extern "C"  void RectOffset__ctor_m2395783478 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		RectOffset_Init_m3353955934(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectOffset::.ctor(UnityEngine.GUIStyle,System.IntPtr)
extern "C"  void RectOffset__ctor_m358348983 (RectOffset_t3056157787 * __this, GUIStyle_t2990928826 * ___sourceStyle0, IntPtr_t ___source1, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_0 = ___sourceStyle0;
		__this->set_m_SourceStyle_1(L_0);
		IntPtr_t L_1 = ___source1;
		__this->set_m_Ptr_0(L_1);
		return;
	}
}
// System.Void UnityEngine.RectOffset::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void RectOffset__ctor_m2631865360 (RectOffset_t3056157787 * __this, int32_t ___left0, int32_t ___right1, int32_t ___top2, int32_t ___bottom3, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		RectOffset_Init_m3353955934(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___left0;
		RectOffset_set_left_m901965251(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___right1;
		RectOffset_set_right_m2119805444(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___top2;
		RectOffset_set_top_m3043172093(__this, L_2, /*hidden argument*/NULL);
		int32_t L_3 = ___bottom3;
		RectOffset_set_bottom_m3840454247(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectOffset::Init()
extern "C"  void RectOffset_Init_m3353955934 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	typedef void (*RectOffset_Init_m3353955934_ftn) (RectOffset_t3056157787 *);
	static RectOffset_Init_m3353955934_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_Init_m3353955934_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::Cleanup()
extern "C"  void RectOffset_Cleanup_m2914212664 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	typedef void (*RectOffset_Cleanup_m2914212664_ftn) (RectOffset_t3056157787 *);
	static RectOffset_Cleanup_m2914212664_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_Cleanup_m2914212664_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.RectOffset::get_left()
extern "C"  int32_t RectOffset_get_left_m4104523390 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_left_m4104523390_ftn) (RectOffset_t3056157787 *);
	static RectOffset_get_left_m4104523390_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_left_m4104523390_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_left()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_left(System.Int32)
extern "C"  void RectOffset_set_left_m901965251 (RectOffset_t3056157787 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RectOffset_set_left_m901965251_ftn) (RectOffset_t3056157787 *, int32_t);
	static RectOffset_set_left_m901965251_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_left_m901965251_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_left(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_right()
extern "C"  int32_t RectOffset_get_right_m3831383975 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_right_m3831383975_ftn) (RectOffset_t3056157787 *);
	static RectOffset_get_right_m3831383975_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_right_m3831383975_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_right()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_right(System.Int32)
extern "C"  void RectOffset_set_right_m2119805444 (RectOffset_t3056157787 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RectOffset_set_right_m2119805444_ftn) (RectOffset_t3056157787 *, int32_t);
	static RectOffset_set_right_m2119805444_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_right_m2119805444_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_right(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_top()
extern "C"  int32_t RectOffset_get_top_m140097312 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_top_m140097312_ftn) (RectOffset_t3056157787 *);
	static RectOffset_get_top_m140097312_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_top_m140097312_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_top()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_top(System.Int32)
extern "C"  void RectOffset_set_top_m3043172093 (RectOffset_t3056157787 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RectOffset_set_top_m3043172093_ftn) (RectOffset_t3056157787 *, int32_t);
	static RectOffset_set_top_m3043172093_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_top_m3043172093_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_top(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_bottom()
extern "C"  int32_t RectOffset_get_bottom_m2106858018 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_bottom_m2106858018_ftn) (RectOffset_t3056157787 *);
	static RectOffset_get_bottom_m2106858018_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_bottom_m2106858018_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_bottom()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_bottom(System.Int32)
extern "C"  void RectOffset_set_bottom_m3840454247 (RectOffset_t3056157787 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RectOffset_set_bottom_m3840454247_ftn) (RectOffset_t3056157787 *, int32_t);
	static RectOffset_set_bottom_m3840454247_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_bottom_m3840454247_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_bottom(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_horizontal()
extern "C"  int32_t RectOffset_get_horizontal_m1186440923 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_horizontal_m1186440923_ftn) (RectOffset_t3056157787 *);
	static RectOffset_get_horizontal_m1186440923_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_horizontal_m1186440923_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_horizontal()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.RectOffset::get_vertical()
extern "C"  int32_t RectOffset_get_vertical_m3650431789 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_vertical_m3650431789_ftn) (RectOffset_t3056157787 *);
	static RectOffset_get_vertical_m3650431789_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_vertical_m3650431789_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_vertical()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Rect UnityEngine.RectOffset::Add(UnityEngine.Rect)
extern "C"  Rect_t4241904616  RectOffset_Add_m396754502 (RectOffset_t3056157787 * __this, Rect_t4241904616  ___rect0, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectOffset_INTERNAL_CALL_Add_m775012682(NULL /*static, unused*/, __this, (&___rect0), (&V_0), /*hidden argument*/NULL);
		Rect_t4241904616  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectOffset::INTERNAL_CALL_Add(UnityEngine.RectOffset,UnityEngine.Rect&,UnityEngine.Rect&)
extern "C"  void RectOffset_INTERNAL_CALL_Add_m775012682 (Il2CppObject * __this /* static, unused */, RectOffset_t3056157787 * ___self0, Rect_t4241904616 * ___rect1, Rect_t4241904616 * ___value2, const MethodInfo* method)
{
	typedef void (*RectOffset_INTERNAL_CALL_Add_m775012682_ftn) (RectOffset_t3056157787 *, Rect_t4241904616 *, Rect_t4241904616 *);
	static RectOffset_INTERNAL_CALL_Add_m775012682_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_INTERNAL_CALL_Add_m775012682_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::INTERNAL_CALL_Add(UnityEngine.RectOffset,UnityEngine.Rect&,UnityEngine.Rect&)");
	_il2cpp_icall_func(___self0, ___rect1, ___value2);
}
// UnityEngine.Rect UnityEngine.RectOffset::Remove(UnityEngine.Rect)
extern "C"  Rect_t4241904616  RectOffset_Remove_m843726027 (RectOffset_t3056157787 * __this, Rect_t4241904616  ___rect0, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectOffset_INTERNAL_CALL_Remove_m1782283077(NULL /*static, unused*/, __this, (&___rect0), (&V_0), /*hidden argument*/NULL);
		Rect_t4241904616  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectOffset::INTERNAL_CALL_Remove(UnityEngine.RectOffset,UnityEngine.Rect&,UnityEngine.Rect&)
extern "C"  void RectOffset_INTERNAL_CALL_Remove_m1782283077 (Il2CppObject * __this /* static, unused */, RectOffset_t3056157787 * ___self0, Rect_t4241904616 * ___rect1, Rect_t4241904616 * ___value2, const MethodInfo* method)
{
	typedef void (*RectOffset_INTERNAL_CALL_Remove_m1782283077_ftn) (RectOffset_t3056157787 *, Rect_t4241904616 *, Rect_t4241904616 *);
	static RectOffset_INTERNAL_CALL_Remove_m1782283077_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_INTERNAL_CALL_Remove_m1782283077_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::INTERNAL_CALL_Remove(UnityEngine.RectOffset,UnityEngine.Rect&,UnityEngine.Rect&)");
	_il2cpp_icall_func(___self0, ___rect1, ___value2);
}
// System.Void UnityEngine.RectOffset::Finalize()
extern "C"  void RectOffset_Finalize_m3416542060 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			GUIStyle_t2990928826 * L_0 = __this->get_m_SourceStyle_1();
			if (L_0)
			{
				goto IL_0011;
			}
		}

IL_000b:
		{
			RectOffset_Cleanup_m2914212664(__this, /*hidden argument*/NULL);
		}

IL_0011:
		{
			IL2CPP_LEAVE(0x1D, FINALLY_0016);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0016;
	}

FINALLY_0016:
	{ // begin finally (depth: 1)
		Object_Finalize_m3027285644(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(22)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(22)
	{
		IL2CPP_JUMP_TBL(0x1D, IL_001d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_001d:
	{
		return;
	}
}
// System.String UnityEngine.RectOffset::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2126166178;
extern const uint32_t RectOffset_ToString_m2231965149_MetadataUsageId;
extern "C"  String_t* RectOffset_ToString_m2231965149 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectOffset_ToString_m2231965149_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		int32_t L_1 = RectOffset_get_left_m4104523390(__this, /*hidden argument*/NULL);
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_0;
		int32_t L_5 = RectOffset_get_right_m3831383975(__this, /*hidden argument*/NULL);
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t1108656482* L_8 = L_4;
		int32_t L_9 = RectOffset_get_top_m140097312(__this, /*hidden argument*/NULL);
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t1108656482* L_12 = L_8;
		int32_t L_13 = RectOffset_get_bottom_m2106858018(__this, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		Il2CppObject * L_15 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral2126166178, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
// Conversion methods for marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t3056157787_marshal_pinvoke(const RectOffset_t3056157787& unmarshaled, RectOffset_t3056157787_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_SourceStyle_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_SourceStyle' of type 'RectOffset': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_SourceStyle_1Exception);
}
extern "C" void RectOffset_t3056157787_marshal_pinvoke_back(const RectOffset_t3056157787_marshaled_pinvoke& marshaled, RectOffset_t3056157787& unmarshaled)
{
	Il2CppCodeGenException* ___m_SourceStyle_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_SourceStyle' of type 'RectOffset': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_SourceStyle_1Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t3056157787_marshal_pinvoke_cleanup(RectOffset_t3056157787_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t3056157787_marshal_com(const RectOffset_t3056157787& unmarshaled, RectOffset_t3056157787_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_SourceStyle_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_SourceStyle' of type 'RectOffset': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_SourceStyle_1Exception);
}
extern "C" void RectOffset_t3056157787_marshal_com_back(const RectOffset_t3056157787_marshaled_com& marshaled, RectOffset_t3056157787& unmarshaled)
{
	Il2CppCodeGenException* ___m_SourceStyle_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_SourceStyle' of type 'RectOffset': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_SourceStyle_1Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t3056157787_marshal_com_cleanup(RectOffset_t3056157787_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.RectTransform::add_reapplyDrivenProperties(UnityEngine.RectTransform/ReapplyDrivenProperties)
extern Il2CppClass* RectTransform_t972643934_il2cpp_TypeInfo_var;
extern Il2CppClass* ReapplyDrivenProperties_t779639188_il2cpp_TypeInfo_var;
extern const uint32_t RectTransform_add_reapplyDrivenProperties_m1968705467_MetadataUsageId;
extern "C"  void RectTransform_add_reapplyDrivenProperties_m1968705467 (Il2CppObject * __this /* static, unused */, ReapplyDrivenProperties_t779639188 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_add_reapplyDrivenProperties_m1968705467_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReapplyDrivenProperties_t779639188 * L_0 = ((RectTransform_t972643934_StaticFields*)RectTransform_t972643934_il2cpp_TypeInfo_var->static_fields)->get_reapplyDrivenProperties_2();
		ReapplyDrivenProperties_t779639188 * L_1 = ___value0;
		Delegate_t3310234105 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((RectTransform_t972643934_StaticFields*)RectTransform_t972643934_il2cpp_TypeInfo_var->static_fields)->set_reapplyDrivenProperties_2(((ReapplyDrivenProperties_t779639188 *)CastclassSealed(L_2, ReapplyDrivenProperties_t779639188_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UnityEngine.RectTransform::remove_reapplyDrivenProperties(UnityEngine.RectTransform/ReapplyDrivenProperties)
extern Il2CppClass* RectTransform_t972643934_il2cpp_TypeInfo_var;
extern Il2CppClass* ReapplyDrivenProperties_t779639188_il2cpp_TypeInfo_var;
extern const uint32_t RectTransform_remove_reapplyDrivenProperties_m2607613076_MetadataUsageId;
extern "C"  void RectTransform_remove_reapplyDrivenProperties_m2607613076 (Il2CppObject * __this /* static, unused */, ReapplyDrivenProperties_t779639188 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_remove_reapplyDrivenProperties_m2607613076_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReapplyDrivenProperties_t779639188 * L_0 = ((RectTransform_t972643934_StaticFields*)RectTransform_t972643934_il2cpp_TypeInfo_var->static_fields)->get_reapplyDrivenProperties_2();
		ReapplyDrivenProperties_t779639188 * L_1 = ___value0;
		Delegate_t3310234105 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((RectTransform_t972643934_StaticFields*)RectTransform_t972643934_il2cpp_TypeInfo_var->static_fields)->set_reapplyDrivenProperties_2(((ReapplyDrivenProperties_t779639188 *)CastclassSealed(L_2, ReapplyDrivenProperties_t779639188_il2cpp_TypeInfo_var)));
		return;
	}
}
// UnityEngine.Rect UnityEngine.RectTransform::get_rect()
extern "C"  Rect_t4241904616  RectTransform_get_rect_m1566017036 (RectTransform_t972643934 * __this, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectTransform_INTERNAL_get_rect_m1980775561(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t4241904616  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C"  void RectTransform_INTERNAL_get_rect_m1980775561 (RectTransform_t972643934 * __this, Rect_t4241904616 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_rect_m1980775561_ftn) (RectTransform_t972643934 *, Rect_t4241904616 *);
	static RectTransform_INTERNAL_get_rect_m1980775561_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_rect_m1980775561_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMin()
extern "C"  Vector2_t4282066565  RectTransform_get_anchorMin_m688674174 (RectTransform_t972643934 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectTransform_INTERNAL_get_anchorMin_m1139643287(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_anchorMin(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMin_m989253483 (RectTransform_t972643934 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_anchorMin_m370577571(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchorMin_m1139643287 (RectTransform_t972643934 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchorMin_m1139643287_ftn) (RectTransform_t972643934 *, Vector2_t4282066565 *);
	static RectTransform_INTERNAL_get_anchorMin_m1139643287_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchorMin_m1139643287_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchorMin_m370577571 (RectTransform_t972643934 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchorMin_m370577571_ftn) (RectTransform_t972643934 *, Vector2_t4282066565 *);
	static RectTransform_INTERNAL_set_anchorMin_m370577571_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchorMin_m370577571_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMax()
extern "C"  Vector2_t4282066565  RectTransform_get_anchorMax_m688445456 (RectTransform_t972643934 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectTransform_INTERNAL_get_anchorMax_m1238440233(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_anchorMax(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMax_m715345817 (RectTransform_t972643934 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_anchorMax_m469374517(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchorMax_m1238440233 (RectTransform_t972643934 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchorMax_m1238440233_ftn) (RectTransform_t972643934 *, Vector2_t4282066565 *);
	static RectTransform_INTERNAL_get_anchorMax_m1238440233_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchorMax_m1238440233_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchorMax_m469374517 (RectTransform_t972643934 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchorMax_m469374517_ftn) (RectTransform_t972643934 *, Vector2_t4282066565 *);
	static RectTransform_INTERNAL_set_anchorMax_m469374517_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchorMax_m469374517_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.RectTransform::get_anchoredPosition3D()
extern "C"  Vector3_t4282066566  RectTransform_get_anchoredPosition3D_m1317976368 (RectTransform_t972643934 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Vector2_t4282066565  L_0 = RectTransform_get_anchoredPosition_m2318455998(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&V_0)->get_x_1();
		float L_2 = (&V_0)->get_y_2();
		Vector3_t4282066566  L_3 = Transform_get_localPosition_m668140784(__this, /*hidden argument*/NULL);
		V_1 = L_3;
		float L_4 = (&V_1)->get_z_3();
		Vector3_t4282066566  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector3__ctor_m2926210380(&L_5, L_1, L_2, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void UnityEngine.RectTransform::set_anchoredPosition3D(UnityEngine.Vector3)
extern "C"  void RectTransform_set_anchoredPosition3D_m3457056443 (RectTransform_t972643934 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___value0)->get_x_1();
		float L_1 = (&___value0)->get_y_2();
		Vector2_t4282066565  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m1517109030(&L_2, L_0, L_1, /*hidden argument*/NULL);
		RectTransform_set_anchoredPosition_m1498949997(__this, L_2, /*hidden argument*/NULL);
		Vector3_t4282066566  L_3 = Transform_get_localPosition_m668140784(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4 = (&___value0)->get_z_3();
		(&V_0)->set_z_3(L_4);
		Vector3_t4282066566  L_5 = V_0;
		Transform_set_localPosition_m3504330903(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchoredPosition()
extern "C"  Vector2_t4282066565  RectTransform_get_anchoredPosition_m2318455998 (RectTransform_t972643934 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectTransform_INTERNAL_get_anchoredPosition_m840986985(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchoredPosition_m1498949997 (RectTransform_t972643934 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_anchoredPosition_m2329865949(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchoredPosition_m840986985 (RectTransform_t972643934 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchoredPosition_m840986985_ftn) (RectTransform_t972643934 *, Vector2_t4282066565 *);
	static RectTransform_INTERNAL_get_anchoredPosition_m840986985_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchoredPosition_m840986985_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchoredPosition_m2329865949 (RectTransform_t972643934 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchoredPosition_m2329865949_ftn) (RectTransform_t972643934 *, Vector2_t4282066565 *);
	static RectTransform_INTERNAL_set_anchoredPosition_m2329865949_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchoredPosition_m2329865949_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_sizeDelta()
extern "C"  Vector2_t4282066565  RectTransform_get_sizeDelta_m4279424984 (RectTransform_t972643934 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectTransform_INTERNAL_get_sizeDelta_m4117062897(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_sizeDelta(UnityEngine.Vector2)
extern "C"  void RectTransform_set_sizeDelta_m1223846609 (RectTransform_t972643934 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_sizeDelta_m3347997181(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_sizeDelta_m4117062897 (RectTransform_t972643934 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_sizeDelta_m4117062897_ftn) (RectTransform_t972643934 *, Vector2_t4282066565 *);
	static RectTransform_INTERNAL_get_sizeDelta_m4117062897_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_sizeDelta_m4117062897_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_sizeDelta_m3347997181 (RectTransform_t972643934 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_sizeDelta_m3347997181_ftn) (RectTransform_t972643934 *, Vector2_t4282066565 *);
	static RectTransform_INTERNAL_set_sizeDelta_m3347997181_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_sizeDelta_m3347997181_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_pivot()
extern "C"  Vector2_t4282066565  RectTransform_get_pivot_m3785570595 (RectTransform_t972643934 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectTransform_INTERNAL_get_pivot_m322514492(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_pivot(UnityEngine.Vector2)
extern "C"  void RectTransform_set_pivot_m457344806 (RectTransform_t972643934 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_pivot_m237146440(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_pivot_m322514492 (RectTransform_t972643934 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_pivot_m322514492_ftn) (RectTransform_t972643934 *, Vector2_t4282066565 *);
	static RectTransform_INTERNAL_get_pivot_m322514492_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_pivot_m322514492_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_pivot_m237146440 (RectTransform_t972643934 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_pivot_m237146440_ftn) (RectTransform_t972643934 *, Vector2_t4282066565 *);
	static RectTransform_INTERNAL_set_pivot_m237146440_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_pivot_m237146440_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::SendReapplyDrivenProperties(UnityEngine.RectTransform)
extern Il2CppClass* RectTransform_t972643934_il2cpp_TypeInfo_var;
extern const uint32_t RectTransform_SendReapplyDrivenProperties_m2261331528_MetadataUsageId;
extern "C"  void RectTransform_SendReapplyDrivenProperties_m2261331528 (Il2CppObject * __this /* static, unused */, RectTransform_t972643934 * ___driven0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_SendReapplyDrivenProperties_m2261331528_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReapplyDrivenProperties_t779639188 * L_0 = ((RectTransform_t972643934_StaticFields*)RectTransform_t972643934_il2cpp_TypeInfo_var->static_fields)->get_reapplyDrivenProperties_2();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		ReapplyDrivenProperties_t779639188 * L_1 = ((RectTransform_t972643934_StaticFields*)RectTransform_t972643934_il2cpp_TypeInfo_var->static_fields)->get_reapplyDrivenProperties_2();
		RectTransform_t972643934 * L_2 = ___driven0;
		NullCheck(L_1);
		ReapplyDrivenProperties_Invoke_m3880635155(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::GetLocalCorners(UnityEngine.Vector3[])
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4014291545;
extern const uint32_t RectTransform_GetLocalCorners_m1867617311_MetadataUsageId;
extern "C"  void RectTransform_GetLocalCorners_m1867617311 (RectTransform_t972643934 * __this, Vector3U5BU5D_t215400611* ___fourCornersArray0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_GetLocalCorners_m1867617311_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		Vector3U5BU5D_t215400611* L_0 = ___fourCornersArray0;
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		Vector3U5BU5D_t215400611* L_1 = ___fourCornersArray0;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))) >= ((int32_t)4)))
		{
			goto IL_001a;
		}
	}

IL_000f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral4014291545, /*hidden argument*/NULL);
		return;
	}

IL_001a:
	{
		Rect_t4241904616  L_2 = RectTransform_get_rect_m1566017036(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_x_m982385354((&V_0), /*hidden argument*/NULL);
		V_1 = L_3;
		float L_4 = Rect_get_y_m982386315((&V_0), /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = Rect_get_xMax_m370881244((&V_0), /*hidden argument*/NULL);
		V_3 = L_5;
		float L_6 = Rect_get_yMax_m399510395((&V_0), /*hidden argument*/NULL);
		V_4 = L_6;
		Vector3U5BU5D_t215400611* L_7 = ___fourCornersArray0;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		float L_8 = V_1;
		float L_9 = V_2;
		Vector3_t4282066566  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector3__ctor_m2926210380(&L_10, L_8, L_9, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_10;
		Vector3U5BU5D_t215400611* L_11 = ___fourCornersArray0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 1);
		float L_12 = V_1;
		float L_13 = V_4;
		Vector3_t4282066566  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m2926210380(&L_14, L_12, L_13, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_14;
		Vector3U5BU5D_t215400611* L_15 = ___fourCornersArray0;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 2);
		float L_16 = V_3;
		float L_17 = V_4;
		Vector3_t4282066566  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector3__ctor_m2926210380(&L_18, L_16, L_17, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_18;
		Vector3U5BU5D_t215400611* L_19 = ___fourCornersArray0;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 3);
		float L_20 = V_3;
		float L_21 = V_2;
		Vector3_t4282066566  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Vector3__ctor_m2926210380(&L_22, L_20, L_21, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)((L_19)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_22;
		return;
	}
}
// System.Void UnityEngine.RectTransform::GetWorldCorners(UnityEngine.Vector3[])
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1157240146;
extern const uint32_t RectTransform_GetWorldCorners_m1829917190_MetadataUsageId;
extern "C"  void RectTransform_GetWorldCorners_m1829917190 (RectTransform_t972643934 * __this, Vector3U5BU5D_t215400611* ___fourCornersArray0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_GetWorldCorners_m1829917190_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t1659122786 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		Vector3U5BU5D_t215400611* L_0 = ___fourCornersArray0;
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		Vector3U5BU5D_t215400611* L_1 = ___fourCornersArray0;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))) >= ((int32_t)4)))
		{
			goto IL_001a;
		}
	}

IL_000f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral1157240146, /*hidden argument*/NULL);
		return;
	}

IL_001a:
	{
		Vector3U5BU5D_t215400611* L_2 = ___fourCornersArray0;
		RectTransform_GetLocalCorners_m1867617311(__this, L_2, /*hidden argument*/NULL);
		Transform_t1659122786 * L_3 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0051;
	}

IL_002f:
	{
		Vector3U5BU5D_t215400611* L_4 = ___fourCornersArray0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		Transform_t1659122786 * L_6 = V_0;
		Vector3U5BU5D_t215400611* L_7 = ___fourCornersArray0;
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		NullCheck(L_6);
		Vector3_t4282066566  L_9 = Transform_TransformPoint_m437395512(L_6, (*(Vector3_t4282066566 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8)))), /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5)))) = L_9;
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0051:
	{
		int32_t L_11 = V_1;
		if ((((int32_t)L_11) < ((int32_t)4)))
		{
			goto IL_002f;
		}
	}
	{
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_offsetMin()
extern "C"  Vector2_t4282066565  RectTransform_get_offsetMin_m3509071456 (RectTransform_t972643934 * __this, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0 = RectTransform_get_anchoredPosition_m2318455998(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_1 = RectTransform_get_sizeDelta_m4279424984(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_2 = RectTransform_get_pivot_m3785570595(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_3 = Vector2_Scale_m1743563745(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Vector2_t4282066565  L_4 = Vector2_op_Subtraction_m2097149401(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void UnityEngine.RectTransform::set_offsetMin(UnityEngine.Vector2)
extern "C"  void RectTransform_set_offsetMin_m951793481 (RectTransform_t972643934 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t4282066565  L_0 = ___value0;
		Vector2_t4282066565  L_1 = RectTransform_get_anchoredPosition_m2318455998(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_2 = RectTransform_get_sizeDelta_m4279424984(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_3 = RectTransform_get_pivot_m3785570595(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_4 = Vector2_Scale_m1743563745(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Vector2_t4282066565  L_5 = Vector2_op_Subtraction_m2097149401(NULL /*static, unused*/, L_1, L_4, /*hidden argument*/NULL);
		Vector2_t4282066565  L_6 = Vector2_op_Subtraction_m2097149401(NULL /*static, unused*/, L_0, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		Vector2_t4282066565  L_7 = RectTransform_get_sizeDelta_m4279424984(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_8 = V_0;
		Vector2_t4282066565  L_9 = Vector2_op_Subtraction_m2097149401(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		RectTransform_set_sizeDelta_m1223846609(__this, L_9, /*hidden argument*/NULL);
		Vector2_t4282066565  L_10 = RectTransform_get_anchoredPosition_m2318455998(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_11 = V_0;
		Vector2_t4282066565  L_12 = Vector2_get_one_m2767488832(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t4282066565  L_13 = RectTransform_get_pivot_m3785570595(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_14 = Vector2_op_Subtraction_m2097149401(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		Vector2_t4282066565  L_15 = Vector2_Scale_m1743563745(NULL /*static, unused*/, L_11, L_14, /*hidden argument*/NULL);
		Vector2_t4282066565  L_16 = Vector2_op_Addition_m1173049553(NULL /*static, unused*/, L_10, L_15, /*hidden argument*/NULL);
		RectTransform_set_anchoredPosition_m1498949997(__this, L_16, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_offsetMax()
extern "C"  Vector2_t4282066565  RectTransform_get_offsetMax_m3508842738 (RectTransform_t972643934 * __this, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0 = RectTransform_get_anchoredPosition_m2318455998(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_1 = RectTransform_get_sizeDelta_m4279424984(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_2 = Vector2_get_one_m2767488832(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t4282066565  L_3 = RectTransform_get_pivot_m3785570595(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_4 = Vector2_op_Subtraction_m2097149401(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Vector2_t4282066565  L_5 = Vector2_Scale_m1743563745(NULL /*static, unused*/, L_1, L_4, /*hidden argument*/NULL);
		Vector2_t4282066565  L_6 = Vector2_op_Addition_m1173049553(NULL /*static, unused*/, L_0, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.RectTransform::set_offsetMax(UnityEngine.Vector2)
extern "C"  void RectTransform_set_offsetMax_m677885815 (RectTransform_t972643934 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t4282066565  L_0 = ___value0;
		Vector2_t4282066565  L_1 = RectTransform_get_anchoredPosition_m2318455998(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_2 = RectTransform_get_sizeDelta_m4279424984(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_3 = Vector2_get_one_m2767488832(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t4282066565  L_4 = RectTransform_get_pivot_m3785570595(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_5 = Vector2_op_Subtraction_m2097149401(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Vector2_t4282066565  L_6 = Vector2_Scale_m1743563745(NULL /*static, unused*/, L_2, L_5, /*hidden argument*/NULL);
		Vector2_t4282066565  L_7 = Vector2_op_Addition_m1173049553(NULL /*static, unused*/, L_1, L_6, /*hidden argument*/NULL);
		Vector2_t4282066565  L_8 = Vector2_op_Subtraction_m2097149401(NULL /*static, unused*/, L_0, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		Vector2_t4282066565  L_9 = RectTransform_get_sizeDelta_m4279424984(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_10 = V_0;
		Vector2_t4282066565  L_11 = Vector2_op_Addition_m1173049553(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		RectTransform_set_sizeDelta_m1223846609(__this, L_11, /*hidden argument*/NULL);
		Vector2_t4282066565  L_12 = RectTransform_get_anchoredPosition_m2318455998(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_13 = V_0;
		Vector2_t4282066565  L_14 = RectTransform_get_pivot_m3785570595(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_15 = Vector2_Scale_m1743563745(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		Vector2_t4282066565  L_16 = Vector2_op_Addition_m1173049553(NULL /*static, unused*/, L_12, L_15, /*hidden argument*/NULL);
		RectTransform_set_anchoredPosition_m1498949997(__this, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::SetInsetAndSizeFromParentEdge(UnityEngine.RectTransform/Edge,System.Single,System.Single)
extern "C"  void RectTransform_SetInsetAndSizeFromParentEdge_m1924354604 (RectTransform_t972643934 * __this, int32_t ___edge0, float ___inset1, float ___size2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	float V_2 = 0.0f;
	Vector2_t4282066565  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t4282066565  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector2_t4282066565  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector2_t4282066565  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector2_t4282066565  V_7;
	memset(&V_7, 0, sizeof(V_7));
	int32_t G_B4_0 = 0;
	int32_t G_B7_0 = 0;
	int32_t G_B10_0 = 0;
	int32_t G_B12_0 = 0;
	Vector2_t4282066565 * G_B12_1 = NULL;
	int32_t G_B11_0 = 0;
	Vector2_t4282066565 * G_B11_1 = NULL;
	float G_B13_0 = 0.0f;
	int32_t G_B13_1 = 0;
	Vector2_t4282066565 * G_B13_2 = NULL;
	{
		int32_t L_0 = ___edge0;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___edge0;
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0014;
		}
	}

IL_000e:
	{
		G_B4_0 = 1;
		goto IL_0015;
	}

IL_0014:
	{
		G_B4_0 = 0;
	}

IL_0015:
	{
		V_0 = G_B4_0;
		int32_t L_2 = ___edge0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0023;
		}
	}
	{
		int32_t L_3 = ___edge0;
		G_B7_0 = ((((int32_t)L_3) == ((int32_t)1))? 1 : 0);
		goto IL_0024;
	}

IL_0023:
	{
		G_B7_0 = 1;
	}

IL_0024:
	{
		V_1 = (bool)G_B7_0;
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0031;
		}
	}
	{
		G_B10_0 = 1;
		goto IL_0032;
	}

IL_0031:
	{
		G_B10_0 = 0;
	}

IL_0032:
	{
		V_2 = (((float)((float)G_B10_0)));
		Vector2_t4282066565  L_5 = RectTransform_get_anchorMin_m688674174(__this, /*hidden argument*/NULL);
		V_3 = L_5;
		int32_t L_6 = V_0;
		float L_7 = V_2;
		Vector2_set_Item_m2767519328((&V_3), L_6, L_7, /*hidden argument*/NULL);
		Vector2_t4282066565  L_8 = V_3;
		RectTransform_set_anchorMin_m989253483(__this, L_8, /*hidden argument*/NULL);
		Vector2_t4282066565  L_9 = RectTransform_get_anchorMax_m688445456(__this, /*hidden argument*/NULL);
		V_3 = L_9;
		int32_t L_10 = V_0;
		float L_11 = V_2;
		Vector2_set_Item_m2767519328((&V_3), L_10, L_11, /*hidden argument*/NULL);
		Vector2_t4282066565  L_12 = V_3;
		RectTransform_set_anchorMax_m715345817(__this, L_12, /*hidden argument*/NULL);
		Vector2_t4282066565  L_13 = RectTransform_get_sizeDelta_m4279424984(__this, /*hidden argument*/NULL);
		V_4 = L_13;
		int32_t L_14 = V_0;
		float L_15 = ___size2;
		Vector2_set_Item_m2767519328((&V_4), L_14, L_15, /*hidden argument*/NULL);
		Vector2_t4282066565  L_16 = V_4;
		RectTransform_set_sizeDelta_m1223846609(__this, L_16, /*hidden argument*/NULL);
		Vector2_t4282066565  L_17 = RectTransform_get_anchoredPosition_m2318455998(__this, /*hidden argument*/NULL);
		V_5 = L_17;
		int32_t L_18 = V_0;
		bool L_19 = V_1;
		G_B11_0 = L_18;
		G_B11_1 = (&V_5);
		if (!L_19)
		{
			G_B12_0 = L_18;
			G_B12_1 = (&V_5);
			goto IL_00ac;
		}
	}
	{
		float L_20 = ___inset1;
		float L_21 = ___size2;
		Vector2_t4282066565  L_22 = RectTransform_get_pivot_m3785570595(__this, /*hidden argument*/NULL);
		V_6 = L_22;
		int32_t L_23 = V_0;
		float L_24 = Vector2_get_Item_m2185542843((&V_6), L_23, /*hidden argument*/NULL);
		G_B13_0 = ((float)((float)((-L_20))-(float)((float)((float)L_21*(float)((float)((float)(1.0f)-(float)L_24))))));
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		goto IL_00c0;
	}

IL_00ac:
	{
		float L_25 = ___inset1;
		float L_26 = ___size2;
		Vector2_t4282066565  L_27 = RectTransform_get_pivot_m3785570595(__this, /*hidden argument*/NULL);
		V_7 = L_27;
		int32_t L_28 = V_0;
		float L_29 = Vector2_get_Item_m2185542843((&V_7), L_28, /*hidden argument*/NULL);
		G_B13_0 = ((float)((float)L_25+(float)((float)((float)L_26*(float)L_29))));
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
	}

IL_00c0:
	{
		Vector2_set_Item_m2767519328(G_B13_2, G_B13_1, G_B13_0, /*hidden argument*/NULL);
		Vector2_t4282066565  L_30 = V_5;
		RectTransform_set_anchoredPosition_m1498949997(__this, L_30, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::SetSizeWithCurrentAnchors(UnityEngine.RectTransform/Axis,System.Single)
extern "C"  void RectTransform_SetSizeWithCurrentAnchors_m4019722691 (RectTransform_t972643934 * __this, int32_t ___axis0, float ___size1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Vector2_t4282066565  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t4282066565  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t4282066565  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t4282066565  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		int32_t L_0 = ___axis0;
		V_0 = L_0;
		Vector2_t4282066565  L_1 = RectTransform_get_sizeDelta_m4279424984(__this, /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = V_0;
		float L_3 = ___size1;
		Vector2_t4282066565  L_4 = RectTransform_GetParentSize_m3092718635(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = V_0;
		float L_6 = Vector2_get_Item_m2185542843((&V_2), L_5, /*hidden argument*/NULL);
		Vector2_t4282066565  L_7 = RectTransform_get_anchorMax_m688445456(__this, /*hidden argument*/NULL);
		V_3 = L_7;
		int32_t L_8 = V_0;
		float L_9 = Vector2_get_Item_m2185542843((&V_3), L_8, /*hidden argument*/NULL);
		Vector2_t4282066565  L_10 = RectTransform_get_anchorMin_m688674174(__this, /*hidden argument*/NULL);
		V_4 = L_10;
		int32_t L_11 = V_0;
		float L_12 = Vector2_get_Item_m2185542843((&V_4), L_11, /*hidden argument*/NULL);
		Vector2_set_Item_m2767519328((&V_1), L_2, ((float)((float)L_3-(float)((float)((float)L_6*(float)((float)((float)L_9-(float)L_12)))))), /*hidden argument*/NULL);
		Vector2_t4282066565  L_13 = V_1;
		RectTransform_set_sizeDelta_m1223846609(__this, L_13, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransform::GetParentSize()
extern Il2CppClass* RectTransform_t972643934_il2cpp_TypeInfo_var;
extern const uint32_t RectTransform_GetParentSize_m3092718635_MetadataUsageId;
extern "C"  Vector2_t4282066565  RectTransform_GetParentSize_m3092718635 (RectTransform_t972643934 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_GetParentSize_m3092718635_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RectTransform_t972643934 * V_0 = NULL;
	Rect_t4241904616  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_t1659122786 * L_0 = Transform_get_parent_m2236876972(__this, /*hidden argument*/NULL);
		V_0 = ((RectTransform_t972643934 *)IsInstSealed(L_0, RectTransform_t972643934_il2cpp_TypeInfo_var));
		RectTransform_t972643934 * L_1 = V_0;
		bool L_2 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001d;
		}
	}
	{
		Vector2_t4282066565  L_3 = Vector2_get_zero_m199872368(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}

IL_001d:
	{
		RectTransform_t972643934 * L_4 = V_0;
		NullCheck(L_4);
		Rect_t4241904616  L_5 = RectTransform_get_rect_m1566017036(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		Vector2_t4282066565  L_6 = Rect_get_size_m136480416((&V_1), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::.ctor(System.Object,System.IntPtr)
extern "C"  void ReapplyDrivenProperties__ctor_m3710908308 (ReapplyDrivenProperties_t779639188 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::Invoke(UnityEngine.RectTransform)
extern "C"  void ReapplyDrivenProperties_Invoke_m3880635155 (ReapplyDrivenProperties_t779639188 * __this, RectTransform_t972643934 * ___driven0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ReapplyDrivenProperties_Invoke_m3880635155((ReapplyDrivenProperties_t779639188 *)__this->get_prev_9(),___driven0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, RectTransform_t972643934 * ___driven0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___driven0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, RectTransform_t972643934 * ___driven0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___driven0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___driven0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.RectTransform/ReapplyDrivenProperties::BeginInvoke(UnityEngine.RectTransform,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ReapplyDrivenProperties_BeginInvoke_m1851329218 (ReapplyDrivenProperties_t779639188 * __this, RectTransform_t972643934 * ___driven0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___driven0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::EndInvoke(System.IAsyncResult)
extern "C"  void ReapplyDrivenProperties_EndInvoke_m3686159268 (ReapplyDrivenProperties_t779639188 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.RectTransformUtility::.cctor()
extern Il2CppClass* Vector3U5BU5D_t215400611_il2cpp_TypeInfo_var;
extern Il2CppClass* RectTransformUtility_t3025555048_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility__cctor_m4293768260_MetadataUsageId;
extern "C"  void RectTransformUtility__cctor_m4293768260 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility__cctor_m4293768260_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((RectTransformUtility_t3025555048_StaticFields*)RectTransformUtility_t3025555048_il2cpp_TypeInfo_var->static_fields)->set_s_Corners_0(((Vector3U5BU5D_t215400611*)SZArrayNew(Vector3U5BU5D_t215400611_il2cpp_TypeInfo_var, (uint32_t)4)));
		return;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera)
extern Il2CppClass* RectTransformUtility_t3025555048_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_RectangleContainsScreenPoint_m1460676684_MetadataUsageId;
extern "C"  bool RectTransformUtility_RectangleContainsScreenPoint_m1460676684 (Il2CppObject * __this /* static, unused */, RectTransform_t972643934 * ___rect0, Vector2_t4282066565  ___screenPoint1, Camera_t2727095145 * ___cam2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_RectangleContainsScreenPoint_m1460676684_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectTransform_t972643934 * L_0 = ___rect0;
		Camera_t2727095145 * L_1 = ___cam2;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		bool L_2 = RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m1592514141(NULL /*static, unused*/, L_0, (&___screenPoint1), L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)
extern "C"  bool RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m1592514141 (Il2CppObject * __this /* static, unused */, RectTransform_t972643934 * ___rect0, Vector2_t4282066565 * ___screenPoint1, Camera_t2727095145 * ___cam2, const MethodInfo* method)
{
	typedef bool (*RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m1592514141_ftn) (RectTransform_t972643934 *, Vector2_t4282066565 *, Camera_t2727095145 *);
	static RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m1592514141_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m1592514141_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)");
	return _il2cpp_icall_func(___rect0, ___screenPoint1, ___cam2);
}
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::PixelAdjustPoint(UnityEngine.Vector2,UnityEngine.Transform,UnityEngine.Canvas)
extern Il2CppClass* RectTransformUtility_t3025555048_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_PixelAdjustPoint_m2518308759_MetadataUsageId;
extern "C"  Vector2_t4282066565  RectTransformUtility_PixelAdjustPoint_m2518308759 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, Transform_t1659122786 * ___elementTransform1, Canvas_t2727140764 * ___canvas2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_PixelAdjustPoint_m2518308759_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t4282066565  L_0 = ___point0;
		Transform_t1659122786 * L_1 = ___elementTransform1;
		Canvas_t2727140764 * L_2 = ___canvas2;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		RectTransformUtility_PixelAdjustPoint_m1313063708(NULL /*static, unused*/, L_0, L_1, L_2, (&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.RectTransformUtility::PixelAdjustPoint(UnityEngine.Vector2,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
extern Il2CppClass* RectTransformUtility_t3025555048_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_PixelAdjustPoint_m1313063708_MetadataUsageId;
extern "C"  void RectTransformUtility_PixelAdjustPoint_m1313063708 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, Transform_t1659122786 * ___elementTransform1, Canvas_t2727140764 * ___canvas2, Vector2_t4282066565 * ___output3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_PixelAdjustPoint_m1313063708_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t1659122786 * L_0 = ___elementTransform1;
		Canvas_t2727140764 * L_1 = ___canvas2;
		Vector2_t4282066565 * L_2 = ___output3;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m1509640863(NULL /*static, unused*/, (&___point0), L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
extern "C"  void RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m1509640863 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___point0, Transform_t1659122786 * ___elementTransform1, Canvas_t2727140764 * ___canvas2, Vector2_t4282066565 * ___output3, const MethodInfo* method)
{
	typedef void (*RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m1509640863_ftn) (Vector2_t4282066565 *, Transform_t1659122786 *, Canvas_t2727140764 *, Vector2_t4282066565 *);
	static RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m1509640863_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m1509640863_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___point0, ___elementTransform1, ___canvas2, ___output3);
}
// UnityEngine.Rect UnityEngine.RectTransformUtility::PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas)
extern Il2CppClass* RectTransformUtility_t3025555048_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_PixelAdjustRect_m3727716130_MetadataUsageId;
extern "C"  Rect_t4241904616  RectTransformUtility_PixelAdjustRect_m3727716130 (Il2CppObject * __this /* static, unused */, RectTransform_t972643934 * ___rectTransform0, Canvas_t2727140764 * ___canvas1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_PixelAdjustRect_m3727716130_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectTransform_t972643934 * L_0 = ___rectTransform0;
		Canvas_t2727140764 * L_1 = ___canvas1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1038323288(NULL /*static, unused*/, L_0, L_1, (&V_0), /*hidden argument*/NULL);
		Rect_t4241904616  L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas,UnityEngine.Rect&)
extern "C"  void RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1038323288 (Il2CppObject * __this /* static, unused */, RectTransform_t972643934 * ___rectTransform0, Canvas_t2727140764 * ___canvas1, Rect_t4241904616 * ___value2, const MethodInfo* method)
{
	typedef void (*RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1038323288_ftn) (RectTransform_t972643934 *, Canvas_t2727140764 *, Rect_t4241904616 *);
	static RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1038323288_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1038323288_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas,UnityEngine.Rect&)");
	_il2cpp_icall_func(___rectTransform0, ___canvas1, ___value2);
}
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToWorldPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector3&)
extern Il2CppClass* RectTransformUtility_t3025555048_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_ScreenPointToWorldPointInRectangle_m3856201718_MetadataUsageId;
extern "C"  bool RectTransformUtility_ScreenPointToWorldPointInRectangle_m3856201718 (Il2CppObject * __this /* static, unused */, RectTransform_t972643934 * ___rect0, Vector2_t4282066565  ___screenPoint1, Camera_t2727095145 * ___cam2, Vector3_t4282066566 * ___worldPoint3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_ScreenPointToWorldPointInRectangle_m3856201718_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Ray_t3134616544  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Plane_t4206452690  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	{
		Vector3_t4282066566 * L_0 = ___worldPoint3;
		Vector2_t4282066565  L_1 = Vector2_get_zero_m199872368(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_2 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)L_0) = L_2;
		Camera_t2727095145 * L_3 = ___cam2;
		Vector2_t4282066565  L_4 = ___screenPoint1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		Ray_t3134616544  L_5 = RectTransformUtility_ScreenPointToRay_m1216104542(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		RectTransform_t972643934 * L_6 = ___rect0;
		NullCheck(L_6);
		Quaternion_t1553702882  L_7 = Transform_get_rotation_m11483428(L_6, /*hidden argument*/NULL);
		Vector3_t4282066566  L_8 = Vector3_get_back_m1326515313(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_9 = Quaternion_op_Multiply_m3771288979(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		RectTransform_t972643934 * L_10 = ___rect0;
		NullCheck(L_10);
		Vector3_t4282066566  L_11 = Transform_get_position_m2211398607(L_10, /*hidden argument*/NULL);
		Plane__ctor_m2201046863((&V_1), L_9, L_11, /*hidden argument*/NULL);
		Ray_t3134616544  L_12 = V_0;
		bool L_13 = Plane_Raycast_m2829769106((&V_1), L_12, (&V_2), /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0046;
		}
	}
	{
		return (bool)0;
	}

IL_0046:
	{
		Vector3_t4282066566 * L_14 = ___worldPoint3;
		float L_15 = V_2;
		Vector3_t4282066566  L_16 = Ray_GetPoint_m1171104822((&V_0), L_15, /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)L_14) = L_16;
		return (bool)1;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToLocalPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector2&)
extern Il2CppClass* RectTransformUtility_t3025555048_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_ScreenPointToLocalPointInRectangle_m666650172_MetadataUsageId;
extern "C"  bool RectTransformUtility_ScreenPointToLocalPointInRectangle_m666650172 (Il2CppObject * __this /* static, unused */, RectTransform_t972643934 * ___rect0, Vector2_t4282066565  ___screenPoint1, Camera_t2727095145 * ___cam2, Vector2_t4282066565 * ___localPoint3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_ScreenPointToLocalPointInRectangle_m666650172_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t4282066565 * L_0 = ___localPoint3;
		Vector2_t4282066565  L_1 = Vector2_get_zero_m199872368(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)L_0) = L_1;
		RectTransform_t972643934 * L_2 = ___rect0;
		Vector2_t4282066565  L_3 = ___screenPoint1;
		Camera_t2727095145 * L_4 = ___cam2;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		bool L_5 = RectTransformUtility_ScreenPointToWorldPointInRectangle_m3856201718(NULL /*static, unused*/, L_2, L_3, L_4, (&V_0), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		Vector2_t4282066565 * L_6 = ___localPoint3;
		RectTransform_t972643934 * L_7 = ___rect0;
		Vector3_t4282066566  L_8 = V_0;
		NullCheck(L_7);
		Vector3_t4282066566  L_9 = Transform_InverseTransformPoint_m1626812000(L_7, L_8, /*hidden argument*/NULL);
		Vector2_t4282066565  L_10 = Vector2_op_Implicit_m4083860659(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)L_6) = L_10;
		return (bool)1;
	}

IL_002e:
	{
		return (bool)0;
	}
}
// UnityEngine.Ray UnityEngine.RectTransformUtility::ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector2)
extern "C"  Ray_t3134616544  RectTransformUtility_ScreenPointToRay_m1216104542 (Il2CppObject * __this /* static, unused */, Camera_t2727095145 * ___cam0, Vector2_t4282066565  ___screenPos1, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Camera_t2727095145 * L_0 = ___cam0;
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		Camera_t2727095145 * L_2 = ___cam0;
		Vector2_t4282066565  L_3 = ___screenPos1;
		Vector3_t4282066566  L_4 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		Ray_t3134616544  L_5 = Camera_ScreenPointToRay_m1733083890(L_2, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0019:
	{
		Vector2_t4282066565  L_6 = ___screenPos1;
		Vector3_t4282066566  L_7 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		Vector3_t4282066566 * L_8 = (&V_0);
		float L_9 = L_8->get_z_3();
		L_8->set_z_3(((float)((float)L_9-(float)(100.0f))));
		Vector3_t4282066566  L_10 = V_0;
		Vector3_t4282066566  L_11 = Vector3_get_forward_m1039372701(NULL /*static, unused*/, /*hidden argument*/NULL);
		Ray_t3134616544  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Ray__ctor_m2662468509(&L_12, L_10, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Void UnityEngine.RectTransformUtility::FlipLayoutOnAxis(UnityEngine.RectTransform,System.Int32,System.Boolean,System.Boolean)
extern Il2CppClass* RectTransform_t972643934_il2cpp_TypeInfo_var;
extern Il2CppClass* RectTransformUtility_t3025555048_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_FlipLayoutOnAxis_m3487429352_MetadataUsageId;
extern "C"  void RectTransformUtility_FlipLayoutOnAxis_m3487429352 (Il2CppObject * __this /* static, unused */, RectTransform_t972643934 * ___rect0, int32_t ___axis1, bool ___keepPositioning2, bool ___recursive3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_FlipLayoutOnAxis_m3487429352_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t972643934 * V_1 = NULL;
	Vector2_t4282066565  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t4282066565  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t4282066565  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector2_t4282066565  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float V_6 = 0.0f;
	{
		RectTransform_t972643934 * L_0 = ___rect0;
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		bool L_2 = ___recursive3;
		if (!L_2)
		{
			goto IL_004c;
		}
	}
	{
		V_0 = 0;
		goto IL_0040;
	}

IL_001a:
	{
		RectTransform_t972643934 * L_3 = ___rect0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t1659122786 * L_5 = Transform_GetChild_m4040462992(L_3, L_4, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t972643934 *)IsInstSealed(L_5, RectTransform_t972643934_il2cpp_TypeInfo_var));
		RectTransform_t972643934 * L_6 = V_1;
		bool L_7 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003c;
		}
	}
	{
		RectTransform_t972643934 * L_8 = V_1;
		int32_t L_9 = ___axis1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		RectTransformUtility_FlipLayoutOnAxis_m3487429352(NULL /*static, unused*/, L_8, L_9, (bool)0, (bool)1, /*hidden argument*/NULL);
	}

IL_003c:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0040:
	{
		int32_t L_11 = V_0;
		RectTransform_t972643934 * L_12 = ___rect0;
		NullCheck(L_12);
		int32_t L_13 = Transform_get_childCount_m2107810675(L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_001a;
		}
	}

IL_004c:
	{
		RectTransform_t972643934 * L_14 = ___rect0;
		NullCheck(L_14);
		Vector2_t4282066565  L_15 = RectTransform_get_pivot_m3785570595(L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		int32_t L_16 = ___axis1;
		int32_t L_17 = ___axis1;
		float L_18 = Vector2_get_Item_m2185542843((&V_2), L_17, /*hidden argument*/NULL);
		Vector2_set_Item_m2767519328((&V_2), L_16, ((float)((float)(1.0f)-(float)L_18)), /*hidden argument*/NULL);
		RectTransform_t972643934 * L_19 = ___rect0;
		Vector2_t4282066565  L_20 = V_2;
		NullCheck(L_19);
		RectTransform_set_pivot_m457344806(L_19, L_20, /*hidden argument*/NULL);
		bool L_21 = ___keepPositioning2;
		if (!L_21)
		{
			goto IL_0077;
		}
	}
	{
		return;
	}

IL_0077:
	{
		RectTransform_t972643934 * L_22 = ___rect0;
		NullCheck(L_22);
		Vector2_t4282066565  L_23 = RectTransform_get_anchoredPosition_m2318455998(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		int32_t L_24 = ___axis1;
		int32_t L_25 = ___axis1;
		float L_26 = Vector2_get_Item_m2185542843((&V_3), L_25, /*hidden argument*/NULL);
		Vector2_set_Item_m2767519328((&V_3), L_24, ((-L_26)), /*hidden argument*/NULL);
		RectTransform_t972643934 * L_27 = ___rect0;
		Vector2_t4282066565  L_28 = V_3;
		NullCheck(L_27);
		RectTransform_set_anchoredPosition_m1498949997(L_27, L_28, /*hidden argument*/NULL);
		RectTransform_t972643934 * L_29 = ___rect0;
		NullCheck(L_29);
		Vector2_t4282066565  L_30 = RectTransform_get_anchorMin_m688674174(L_29, /*hidden argument*/NULL);
		V_4 = L_30;
		RectTransform_t972643934 * L_31 = ___rect0;
		NullCheck(L_31);
		Vector2_t4282066565  L_32 = RectTransform_get_anchorMax_m688445456(L_31, /*hidden argument*/NULL);
		V_5 = L_32;
		int32_t L_33 = ___axis1;
		float L_34 = Vector2_get_Item_m2185542843((&V_4), L_33, /*hidden argument*/NULL);
		V_6 = L_34;
		int32_t L_35 = ___axis1;
		int32_t L_36 = ___axis1;
		float L_37 = Vector2_get_Item_m2185542843((&V_5), L_36, /*hidden argument*/NULL);
		Vector2_set_Item_m2767519328((&V_4), L_35, ((float)((float)(1.0f)-(float)L_37)), /*hidden argument*/NULL);
		int32_t L_38 = ___axis1;
		float L_39 = V_6;
		Vector2_set_Item_m2767519328((&V_5), L_38, ((float)((float)(1.0f)-(float)L_39)), /*hidden argument*/NULL);
		RectTransform_t972643934 * L_40 = ___rect0;
		Vector2_t4282066565  L_41 = V_4;
		NullCheck(L_40);
		RectTransform_set_anchorMin_m989253483(L_40, L_41, /*hidden argument*/NULL);
		RectTransform_t972643934 * L_42 = ___rect0;
		Vector2_t4282066565  L_43 = V_5;
		NullCheck(L_42);
		RectTransform_set_anchorMax_m715345817(L_42, L_43, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransformUtility::FlipLayoutAxes(UnityEngine.RectTransform,System.Boolean,System.Boolean)
extern Il2CppClass* RectTransform_t972643934_il2cpp_TypeInfo_var;
extern Il2CppClass* RectTransformUtility_t3025555048_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_FlipLayoutAxes_m2163490602_MetadataUsageId;
extern "C"  void RectTransformUtility_FlipLayoutAxes_m2163490602 (Il2CppObject * __this /* static, unused */, RectTransform_t972643934 * ___rect0, bool ___keepPositioning1, bool ___recursive2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_FlipLayoutAxes_m2163490602_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t972643934 * V_1 = NULL;
	{
		RectTransform_t972643934 * L_0 = ___rect0;
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		bool L_2 = ___recursive2;
		if (!L_2)
		{
			goto IL_004b;
		}
	}
	{
		V_0 = 0;
		goto IL_003f;
	}

IL_001a:
	{
		RectTransform_t972643934 * L_3 = ___rect0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t1659122786 * L_5 = Transform_GetChild_m4040462992(L_3, L_4, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t972643934 *)IsInstSealed(L_5, RectTransform_t972643934_il2cpp_TypeInfo_var));
		RectTransform_t972643934 * L_6 = V_1;
		bool L_7 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003b;
		}
	}
	{
		RectTransform_t972643934 * L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		RectTransformUtility_FlipLayoutAxes_m2163490602(NULL /*static, unused*/, L_8, (bool)0, (bool)1, /*hidden argument*/NULL);
	}

IL_003b:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_003f:
	{
		int32_t L_10 = V_0;
		RectTransform_t972643934 * L_11 = ___rect0;
		NullCheck(L_11);
		int32_t L_12 = Transform_get_childCount_m2107810675(L_11, /*hidden argument*/NULL);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_001a;
		}
	}

IL_004b:
	{
		RectTransform_t972643934 * L_13 = ___rect0;
		RectTransform_t972643934 * L_14 = ___rect0;
		NullCheck(L_14);
		Vector2_t4282066565  L_15 = RectTransform_get_pivot_m3785570595(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		Vector2_t4282066565  L_16 = RectTransformUtility_GetTransposed_m2060823533(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		NullCheck(L_13);
		RectTransform_set_pivot_m457344806(L_13, L_16, /*hidden argument*/NULL);
		RectTransform_t972643934 * L_17 = ___rect0;
		RectTransform_t972643934 * L_18 = ___rect0;
		NullCheck(L_18);
		Vector2_t4282066565  L_19 = RectTransform_get_sizeDelta_m4279424984(L_18, /*hidden argument*/NULL);
		Vector2_t4282066565  L_20 = RectTransformUtility_GetTransposed_m2060823533(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		NullCheck(L_17);
		RectTransform_set_sizeDelta_m1223846609(L_17, L_20, /*hidden argument*/NULL);
		bool L_21 = ___keepPositioning1;
		if (!L_21)
		{
			goto IL_0074;
		}
	}
	{
		return;
	}

IL_0074:
	{
		RectTransform_t972643934 * L_22 = ___rect0;
		RectTransform_t972643934 * L_23 = ___rect0;
		NullCheck(L_23);
		Vector2_t4282066565  L_24 = RectTransform_get_anchoredPosition_m2318455998(L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		Vector2_t4282066565  L_25 = RectTransformUtility_GetTransposed_m2060823533(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		RectTransform_set_anchoredPosition_m1498949997(L_22, L_25, /*hidden argument*/NULL);
		RectTransform_t972643934 * L_26 = ___rect0;
		RectTransform_t972643934 * L_27 = ___rect0;
		NullCheck(L_27);
		Vector2_t4282066565  L_28 = RectTransform_get_anchorMin_m688674174(L_27, /*hidden argument*/NULL);
		Vector2_t4282066565  L_29 = RectTransformUtility_GetTransposed_m2060823533(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		RectTransform_set_anchorMin_m989253483(L_26, L_29, /*hidden argument*/NULL);
		RectTransform_t972643934 * L_30 = ___rect0;
		RectTransform_t972643934 * L_31 = ___rect0;
		NullCheck(L_31);
		Vector2_t4282066565  L_32 = RectTransform_get_anchorMax_m688445456(L_31, /*hidden argument*/NULL);
		Vector2_t4282066565  L_33 = RectTransformUtility_GetTransposed_m2060823533(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		RectTransform_set_anchorMax_m715345817(L_30, L_33, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::GetTransposed(UnityEngine.Vector2)
extern "C"  Vector2_t4282066565  RectTransformUtility_GetTransposed_m2060823533 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___input0, const MethodInfo* method)
{
	{
		float L_0 = (&___input0)->get_y_2();
		float L_1 = (&___input0)->get_x_1();
		Vector2_t4282066565  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m1517109030(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Renderer::get_enabled()
extern "C"  bool Renderer_get_enabled_m1971819706 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef bool (*Renderer_get_enabled_m1971819706_ftn) (Renderer_t3076687687 *);
	static Renderer_get_enabled_m1971819706_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_enabled_m1971819706_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_enabled()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
extern "C"  void Renderer_set_enabled_m2514140131 (Renderer_t3076687687 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_enabled_m2514140131_ftn) (Renderer_t3076687687 *, bool);
	static Renderer_set_enabled_m2514140131_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_enabled_m2514140131_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_enabled(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Material UnityEngine.Renderer::get_material()
extern "C"  Material_t3870600107 * Renderer_get_material_m2720864603 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef Material_t3870600107 * (*Renderer_get_material_m2720864603_ftn) (Renderer_t3076687687 *);
	static Renderer_get_material_m2720864603_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_material_m2720864603_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_material()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_material(UnityEngine.Material)
extern "C"  void Renderer_set_material_m1012580896 (Renderer_t3076687687 * __this, Material_t3870600107 * ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_material_m1012580896_ftn) (Renderer_t3076687687 *, Material_t3870600107 *);
	static Renderer_set_material_m1012580896_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_material_m1012580896_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_material(UnityEngine.Material)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Material UnityEngine.Renderer::get_sharedMaterial()
extern "C"  Material_t3870600107 * Renderer_get_sharedMaterial_m835478880 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef Material_t3870600107 * (*Renderer_get_sharedMaterial_m835478880_ftn) (Renderer_t3076687687 *);
	static Renderer_get_sharedMaterial_m835478880_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sharedMaterial_m835478880_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sharedMaterial()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_sharedMaterial(UnityEngine.Material)
extern "C"  void Renderer_set_sharedMaterial_m1064371045 (Renderer_t3076687687 * __this, Material_t3870600107 * ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_sharedMaterial_m1064371045_ftn) (Renderer_t3076687687 *, Material_t3870600107 *);
	static Renderer_set_sharedMaterial_m1064371045_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_sharedMaterial_m1064371045_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_sharedMaterial(UnityEngine.Material)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Material[] UnityEngine.Renderer::get_materials()
extern "C"  MaterialU5BU5D_t170856778* Renderer_get_materials_m3755041148 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef MaterialU5BU5D_t170856778* (*Renderer_get_materials_m3755041148_ftn) (Renderer_t3076687687 *);
	static Renderer_get_materials_m3755041148_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_materials_m3755041148_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_materials()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_materials(UnityEngine.Material[])
extern "C"  void Renderer_set_materials_m268031319 (Renderer_t3076687687 * __this, MaterialU5BU5D_t170856778* ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_materials_m268031319_ftn) (Renderer_t3076687687 *, MaterialU5BU5D_t170856778*);
	static Renderer_set_materials_m268031319_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_materials_m268031319_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_materials(UnityEngine.Material[])");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Material[] UnityEngine.Renderer::get_sharedMaterials()
extern "C"  MaterialU5BU5D_t170856778* Renderer_get_sharedMaterials_m1981818007 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef MaterialU5BU5D_t170856778* (*Renderer_get_sharedMaterials_m1981818007_ftn) (Renderer_t3076687687 *);
	static Renderer_get_sharedMaterials_m1981818007_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sharedMaterials_m1981818007_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sharedMaterials()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_sharedMaterials(UnityEngine.Material[])
extern "C"  void Renderer_set_sharedMaterials_m1255100914 (Renderer_t3076687687 * __this, MaterialU5BU5D_t170856778* ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_sharedMaterials_m1255100914_ftn) (Renderer_t3076687687 *, MaterialU5BU5D_t170856778*);
	static Renderer_set_sharedMaterials_m1255100914_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_sharedMaterials_m1255100914_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_sharedMaterials(UnityEngine.Material[])");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Renderer::get_isVisible()
extern "C"  bool Renderer_get_isVisible_m1011967393 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef bool (*Renderer_get_isVisible_m1011967393_ftn) (Renderer_t3076687687 *);
	static Renderer_get_isVisible_m1011967393_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_isVisible_m1011967393_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_isVisible()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Renderer::get_sortingLayerID()
extern "C"  int32_t Renderer_get_sortingLayerID_m1954594923 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef int32_t (*Renderer_get_sortingLayerID_m1954594923_ftn) (Renderer_t3076687687 *);
	static Renderer_get_sortingLayerID_m1954594923_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sortingLayerID_m1954594923_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sortingLayerID()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Renderer::get_sortingOrder()
extern "C"  int32_t Renderer_get_sortingOrder_m3623465101 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef int32_t (*Renderer_get_sortingOrder_m3623465101_ftn) (Renderer_t3076687687 *);
	static Renderer_get_sortingOrder_m3623465101_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sortingOrder_m3623465101_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sortingOrder()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RenderSettings::set_fog(System.Boolean)
extern "C"  void RenderSettings_set_fog_m1757489802 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_fog_m1757489802_ftn) (bool);
	static RenderSettings_set_fog_m1757489802_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_fog_m1757489802_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_fog(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RenderSettings::set_fogColor(UnityEngine.Color)
extern "C"  void RenderSettings_set_fogColor_m1507677940 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___value0, const MethodInfo* method)
{
	{
		RenderSettings_INTERNAL_set_fogColor_m2299508764(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderSettings::INTERNAL_set_fogColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_set_fogColor_m2299508764 (Il2CppObject * __this /* static, unused */, Color_t4194546905 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_set_fogColor_m2299508764_ftn) (Color_t4194546905 *);
	static RenderSettings_INTERNAL_set_fogColor_m2299508764_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_set_fogColor_m2299508764_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_set_fogColor(UnityEngine.Color&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RenderSettings::set_fogDensity(System.Single)
extern "C"  void RenderSettings_set_fogDensity_m998518996 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_fogDensity_m998518996_ftn) (float);
	static RenderSettings_set_fogDensity_m998518996_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_fogDensity_m998518996_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_fogDensity(System.Single)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.Color UnityEngine.RenderSettings::get_ambientSkyColor()
extern "C"  Color_t4194546905  RenderSettings_get_ambientSkyColor_m1963190236 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Color_t4194546905  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RenderSettings_INTERNAL_get_ambientSkyColor_m4179463781(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Color_t4194546905  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RenderSettings::set_ambientSkyColor(UnityEngine.Color)
extern "C"  void RenderSettings_set_ambientSkyColor_m2161476599 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___value0, const MethodInfo* method)
{
	{
		RenderSettings_INTERNAL_set_ambientSkyColor_m1697195377(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderSettings::INTERNAL_get_ambientSkyColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_get_ambientSkyColor_m4179463781 (Il2CppObject * __this /* static, unused */, Color_t4194546905 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_get_ambientSkyColor_m4179463781_ftn) (Color_t4194546905 *);
	static RenderSettings_INTERNAL_get_ambientSkyColor_m4179463781_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_get_ambientSkyColor_m4179463781_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_get_ambientSkyColor(UnityEngine.Color&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RenderSettings::INTERNAL_set_ambientSkyColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_set_ambientSkyColor_m1697195377 (Il2CppObject * __this /* static, unused */, Color_t4194546905 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_set_ambientSkyColor_m1697195377_ftn) (Color_t4194546905 *);
	static RenderSettings_INTERNAL_set_ambientSkyColor_m1697195377_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_set_ambientSkyColor_m1697195377_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_set_ambientSkyColor(UnityEngine.Color&)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.Color UnityEngine.RenderSettings::get_ambientEquatorColor()
extern "C"  Color_t4194546905  RenderSettings_get_ambientEquatorColor_m276013214 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Color_t4194546905  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RenderSettings_INTERNAL_get_ambientEquatorColor_m952963367(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Color_t4194546905  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RenderSettings::set_ambientEquatorColor(UnityEngine.Color)
extern "C"  void RenderSettings_set_ambientEquatorColor_m3763161333 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___value0, const MethodInfo* method)
{
	{
		RenderSettings_INTERNAL_set_ambientEquatorColor_m4158538291(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderSettings::INTERNAL_get_ambientEquatorColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_get_ambientEquatorColor_m952963367 (Il2CppObject * __this /* static, unused */, Color_t4194546905 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_get_ambientEquatorColor_m952963367_ftn) (Color_t4194546905 *);
	static RenderSettings_INTERNAL_get_ambientEquatorColor_m952963367_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_get_ambientEquatorColor_m952963367_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_get_ambientEquatorColor(UnityEngine.Color&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RenderSettings::INTERNAL_set_ambientEquatorColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_set_ambientEquatorColor_m4158538291 (Il2CppObject * __this /* static, unused */, Color_t4194546905 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_set_ambientEquatorColor_m4158538291_ftn) (Color_t4194546905 *);
	static RenderSettings_INTERNAL_set_ambientEquatorColor_m4158538291_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_set_ambientEquatorColor_m4158538291_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_set_ambientEquatorColor(UnityEngine.Color&)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.Color UnityEngine.RenderSettings::get_ambientGroundColor()
extern "C"  Color_t4194546905  RenderSettings_get_ambientGroundColor_m847528164 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Color_t4194546905  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RenderSettings_INTERNAL_get_ambientGroundColor_m1254844487(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Color_t4194546905  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RenderSettings::set_ambientGroundColor(UnityEngine.Color)
extern "C"  void RenderSettings_set_ambientGroundColor_m2739647605 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___value0, const MethodInfo* method)
{
	{
		RenderSettings_INTERNAL_set_ambientGroundColor_m2743723451(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderSettings::INTERNAL_get_ambientGroundColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_get_ambientGroundColor_m1254844487 (Il2CppObject * __this /* static, unused */, Color_t4194546905 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_get_ambientGroundColor_m1254844487_ftn) (Color_t4194546905 *);
	static RenderSettings_INTERNAL_get_ambientGroundColor_m1254844487_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_get_ambientGroundColor_m1254844487_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_get_ambientGroundColor(UnityEngine.Color&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RenderSettings::INTERNAL_set_ambientGroundColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_set_ambientGroundColor_m2743723451 (Il2CppObject * __this /* static, unused */, Color_t4194546905 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_set_ambientGroundColor_m2743723451_ftn) (Color_t4194546905 *);
	static RenderSettings_INTERNAL_set_ambientGroundColor_m2743723451_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_set_ambientGroundColor_m2743723451_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_set_ambientGroundColor(UnityEngine.Color&)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.Color UnityEngine.RenderSettings::get_ambientLight()
extern "C"  Color_t4194546905  RenderSettings_get_ambientLight_m929316510 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Color_t4194546905  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RenderSettings_INTERNAL_get_ambientLight_m691711297(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Color_t4194546905  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RenderSettings::set_ambientLight(UnityEngine.Color)
extern "C"  void RenderSettings_set_ambientLight_m791635003 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___value0, const MethodInfo* method)
{
	{
		RenderSettings_INTERNAL_set_ambientLight_m2620477877(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderSettings::INTERNAL_get_ambientLight(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_get_ambientLight_m691711297 (Il2CppObject * __this /* static, unused */, Color_t4194546905 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_get_ambientLight_m691711297_ftn) (Color_t4194546905 *);
	static RenderSettings_INTERNAL_get_ambientLight_m691711297_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_get_ambientLight_m691711297_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_get_ambientLight(UnityEngine.Color&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RenderSettings::INTERNAL_set_ambientLight(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_set_ambientLight_m2620477877 (Il2CppObject * __this /* static, unused */, Color_t4194546905 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_set_ambientLight_m2620477877_ftn) (Color_t4194546905 *);
	static RenderSettings_INTERNAL_set_ambientLight_m2620477877_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_set_ambientLight_m2620477877_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_set_ambientLight(UnityEngine.Color&)");
	_il2cpp_icall_func(___value0);
}
// System.Single UnityEngine.RenderSettings::get_ambientIntensity()
extern "C"  float RenderSettings_get_ambientIntensity_m1272966080 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*RenderSettings_get_ambientIntensity_m1272966080_ftn) ();
	static RenderSettings_get_ambientIntensity_m1272966080_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_get_ambientIntensity_m1272966080_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::get_ambientIntensity()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.RenderSettings::set_ambientIntensity(System.Single)
extern "C"  void RenderSettings_set_ambientIntensity_m2896643971 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_ambientIntensity_m2896643971_ftn) (float);
	static RenderSettings_set_ambientIntensity_m2896643971_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_ambientIntensity_m2896643971_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_ambientIntensity(System.Single)");
	_il2cpp_icall_func(___value0);
}
// System.Single UnityEngine.RenderSettings::get_reflectionIntensity()
extern "C"  float RenderSettings_get_reflectionIntensity_m1862510533 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*RenderSettings_get_reflectionIntensity_m1862510533_ftn) ();
	static RenderSettings_get_reflectionIntensity_m1862510533_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_get_reflectionIntensity_m1862510533_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::get_reflectionIntensity()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.RenderSettings::set_reflectionIntensity(System.Single)
extern "C"  void RenderSettings_set_reflectionIntensity_m4083040622 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_reflectionIntensity_m4083040622_ftn) (float);
	static RenderSettings_set_reflectionIntensity_m4083040622_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_reflectionIntensity_m4083040622_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_reflectionIntensity(System.Single)");
	_il2cpp_icall_func(___value0);
}
// System.Int32 UnityEngine.RenderSettings::get_reflectionBounces()
extern "C"  int32_t RenderSettings_get_reflectionBounces_m2858699465 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*RenderSettings_get_reflectionBounces_m2858699465_ftn) ();
	static RenderSettings_get_reflectionBounces_m2858699465_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_get_reflectionBounces_m2858699465_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::get_reflectionBounces()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.RenderSettings::set_reflectionBounces(System.Int32)
extern "C"  void RenderSettings_set_reflectionBounces_m2938034214 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_reflectionBounces_m2938034214_ftn) (int32_t);
	static RenderSettings_set_reflectionBounces_m2938034214_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_reflectionBounces_m2938034214_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_reflectionBounces(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// System.Single UnityEngine.RenderSettings::get_haloStrength()
extern "C"  float RenderSettings_get_haloStrength_m4164965730 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*RenderSettings_get_haloStrength_m4164965730_ftn) ();
	static RenderSettings_get_haloStrength_m4164965730_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_get_haloStrength_m4164965730_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::get_haloStrength()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.RenderSettings::set_haloStrength(System.Single)
extern "C"  void RenderSettings_set_haloStrength_m1179623713 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_haloStrength_m1179623713_ftn) (float);
	static RenderSettings_set_haloStrength_m1179623713_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_haloStrength_m1179623713_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_haloStrength(System.Single)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RenderSettings::set_flareStrength(System.Single)
extern "C"  void RenderSettings_set_flareStrength_m38596007 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_flareStrength_m38596007_ftn) (float);
	static RenderSettings_set_flareStrength_m38596007_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_flareStrength_m38596007_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_flareStrength(System.Single)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RenderSettings::set_skybox(UnityEngine.Material)
extern "C"  void RenderSettings_set_skybox_m3777670233 (Il2CppObject * __this /* static, unused */, Material_t3870600107 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_skybox_m3777670233_ftn) (Material_t3870600107 *);
	static RenderSettings_set_skybox_m3777670233_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_skybox_m3777670233_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_skybox(UnityEngine.Material)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32)
extern "C"  void RenderTexture__ctor_m591418693 (RenderTexture_t1963041563 * __this, int32_t ___width0, int32_t ___height1, int32_t ___depth2, const MethodInfo* method)
{
	{
		Texture__ctor_m516856734(__this, /*hidden argument*/NULL);
		RenderTexture_Internal_CreateRenderTexture_m1444121965(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		RenderTexture_set_width_m2449777612(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___height1;
		RenderTexture_set_height_m4249514117(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___depth2;
		RenderTexture_set_depth_m68043273(__this, L_2, /*hidden argument*/NULL);
		RenderTexture_set_format_m2539201833(__this, 7, /*hidden argument*/NULL);
		int32_t L_3 = QualitySettings_get_activeColorSpace_m2993616266(NULL /*static, unused*/, /*hidden argument*/NULL);
		RenderTexture_Internal_SetSRGBReadWrite_m2486231130(NULL /*static, unused*/, __this, (bool)((((int32_t)L_3) == ((int32_t)1))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::Internal_CreateRenderTexture(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_Internal_CreateRenderTexture_m1444121965 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___rt0, const MethodInfo* method)
{
	typedef void (*RenderTexture_Internal_CreateRenderTexture_m1444121965_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_Internal_CreateRenderTexture_m1444121965_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_CreateRenderTexture_m1444121965_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_CreateRenderTexture(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___rt0);
}
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite,System.Int32)
extern "C"  RenderTexture_t1963041563 * RenderTexture_GetTemporary_m800385162 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, int32_t ___depthBuffer2, int32_t ___format3, int32_t ___readWrite4, int32_t ___antiAliasing5, const MethodInfo* method)
{
	typedef RenderTexture_t1963041563 * (*RenderTexture_GetTemporary_m800385162_ftn) (int32_t, int32_t, int32_t, int32_t, int32_t, int32_t);
	static RenderTexture_GetTemporary_m800385162_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_GetTemporary_m800385162_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite,System.Int32)");
	return _il2cpp_icall_func(___width0, ___height1, ___depthBuffer2, ___format3, ___readWrite4, ___antiAliasing5);
}
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32)
extern "C"  RenderTexture_t1963041563 * RenderTexture_GetTemporary_m52998487 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, int32_t ___depthBuffer2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 1;
		V_1 = 0;
		V_2 = 7;
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		int32_t L_2 = ___depthBuffer2;
		int32_t L_3 = V_2;
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		RenderTexture_t1963041563 * L_6 = RenderTexture_GetTemporary_m800385162(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.RenderTexture::ReleaseTemporary(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_ReleaseTemporary_m3045961066 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___temp0, const MethodInfo* method)
{
	typedef void (*RenderTexture_ReleaseTemporary_m3045961066_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_ReleaseTemporary_m3045961066_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_ReleaseTemporary_m3045961066_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::ReleaseTemporary(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___temp0);
}
// System.Int32 UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)
extern "C"  int32_t RenderTexture_Internal_GetWidth_m1030655936 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___mono0, const MethodInfo* method)
{
	typedef int32_t (*RenderTexture_Internal_GetWidth_m1030655936_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_Internal_GetWidth_m1030655936_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_GetWidth_m1030655936_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___mono0);
}
// System.Void UnityEngine.RenderTexture::Internal_SetWidth(UnityEngine.RenderTexture,System.Int32)
extern "C"  void RenderTexture_Internal_SetWidth_m3535686411 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___mono0, int32_t ___width1, const MethodInfo* method)
{
	typedef void (*RenderTexture_Internal_SetWidth_m3535686411_ftn) (RenderTexture_t1963041563 *, int32_t);
	static RenderTexture_Internal_SetWidth_m3535686411_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_SetWidth_m3535686411_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_SetWidth(UnityEngine.RenderTexture,System.Int32)");
	_il2cpp_icall_func(___mono0, ___width1);
}
// System.Int32 UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)
extern "C"  int32_t RenderTexture_Internal_GetHeight_m1940011033 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___mono0, const MethodInfo* method)
{
	typedef int32_t (*RenderTexture_Internal_GetHeight_m1940011033_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_Internal_GetHeight_m1940011033_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_GetHeight_m1940011033_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___mono0);
}
// System.Void UnityEngine.RenderTexture::Internal_SetHeight(UnityEngine.RenderTexture,System.Int32)
extern "C"  void RenderTexture_Internal_SetHeight_m4218891754 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___mono0, int32_t ___width1, const MethodInfo* method)
{
	typedef void (*RenderTexture_Internal_SetHeight_m4218891754_ftn) (RenderTexture_t1963041563 *, int32_t);
	static RenderTexture_Internal_SetHeight_m4218891754_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_SetHeight_m4218891754_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_SetHeight(UnityEngine.RenderTexture,System.Int32)");
	_il2cpp_icall_func(___mono0, ___width1);
}
// System.Void UnityEngine.RenderTexture::Internal_SetSRGBReadWrite(UnityEngine.RenderTexture,System.Boolean)
extern "C"  void RenderTexture_Internal_SetSRGBReadWrite_m2486231130 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___mono0, bool ___sRGB1, const MethodInfo* method)
{
	typedef void (*RenderTexture_Internal_SetSRGBReadWrite_m2486231130_ftn) (RenderTexture_t1963041563 *, bool);
	static RenderTexture_Internal_SetSRGBReadWrite_m2486231130_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_SetSRGBReadWrite_m2486231130_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_SetSRGBReadWrite(UnityEngine.RenderTexture,System.Boolean)");
	_il2cpp_icall_func(___mono0, ___sRGB1);
}
// System.Int32 UnityEngine.RenderTexture::get_width()
extern "C"  int32_t RenderTexture_get_width_m1498578543 (RenderTexture_t1963041563 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = RenderTexture_Internal_GetWidth_m1030655936(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.RenderTexture::set_width(System.Int32)
extern "C"  void RenderTexture_set_width_m2449777612 (RenderTexture_t1963041563 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		RenderTexture_Internal_SetWidth_m3535686411(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.RenderTexture::get_height()
extern "C"  int32_t RenderTexture_get_height_m4010076224 (RenderTexture_t1963041563 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = RenderTexture_Internal_GetHeight_m1940011033(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.RenderTexture::set_height(System.Int32)
extern "C"  void RenderTexture_set_height_m4249514117 (RenderTexture_t1963041563 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		RenderTexture_Internal_SetHeight_m4218891754(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::set_depth(System.Int32)
extern "C"  void RenderTexture_set_depth_m68043273 (RenderTexture_t1963041563 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_depth_m68043273_ftn) (RenderTexture_t1963041563 *, int32_t);
	static RenderTexture_set_depth_m68043273_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_depth_m68043273_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_depth(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RenderTexture::set_format(UnityEngine.RenderTextureFormat)
extern "C"  void RenderTexture_set_format_m2539201833 (RenderTexture_t1963041563 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_format_m2539201833_ftn) (RenderTexture_t1963041563 *, int32_t);
	static RenderTexture_set_format_m2539201833_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_format_m2539201833_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_format(UnityEngine.RenderTextureFormat)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.RenderTexture UnityEngine.RenderTexture::get_active()
extern "C"  RenderTexture_t1963041563 * RenderTexture_get_active_m1725644858 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef RenderTexture_t1963041563 * (*RenderTexture_get_active_m1725644858_ftn) ();
	static RenderTexture_get_active_m1725644858_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_get_active_m1725644858_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::get_active()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.RenderTexture::set_active(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_set_active_m1002947377 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_active_m1002947377_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_set_active_m1002947377_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_active_m1002947377_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_active(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
extern "C"  void RequireComponent__ctor_m2023271172 (RequireComponent_t1687166108 * __this, Type_t * ___requiredComponent0, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___requiredComponent0;
		__this->set_m_Type0_0(L_0);
		return;
	}
}
// System.Int32 UnityEngine.Resolution::get_width()
extern "C"  int32_t Resolution_get_width_m3522089436 (Resolution_t1578306928 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Width_0();
		return L_0;
	}
}
extern "C"  int32_t Resolution_get_width_m3522089436_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Resolution_t1578306928 * _thisAdjusted = reinterpret_cast<Resolution_t1578306928 *>(__this + 1);
	return Resolution_get_width_m3522089436(_thisAdjusted, method);
}
// System.Int32 UnityEngine.Resolution::get_height()
extern "C"  int32_t Resolution_get_height_m2314404467 (Resolution_t1578306928 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Height_1();
		return L_0;
	}
}
extern "C"  int32_t Resolution_get_height_m2314404467_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Resolution_t1578306928 * _thisAdjusted = reinterpret_cast<Resolution_t1578306928 *>(__this + 1);
	return Resolution_get_height_m2314404467(_thisAdjusted, method);
}
// System.String UnityEngine.Resolution::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral284898143;
extern const uint32_t Resolution_ToString_m139257714_MetadataUsageId;
extern "C"  String_t* Resolution_ToString_m139257714 (Resolution_t1578306928 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Resolution_ToString_m139257714_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)3));
		int32_t L_1 = __this->get_m_Width_0();
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_0;
		int32_t L_5 = __this->get_m_Height_1();
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t1108656482* L_8 = L_4;
		int32_t L_9 = __this->get_m_RefreshRate_2();
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		String_t* L_12 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral284898143, L_8, /*hidden argument*/NULL);
		return L_12;
	}
}
extern "C"  String_t* Resolution_ToString_m139257714_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Resolution_t1578306928 * _thisAdjusted = reinterpret_cast<Resolution_t1578306928 *>(__this + 1);
	return Resolution_ToString_m139257714(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.Resolution
extern "C" void Resolution_t1578306928_marshal_pinvoke(const Resolution_t1578306928& unmarshaled, Resolution_t1578306928_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Width_0 = unmarshaled.get_m_Width_0();
	marshaled.___m_Height_1 = unmarshaled.get_m_Height_1();
	marshaled.___m_RefreshRate_2 = unmarshaled.get_m_RefreshRate_2();
}
extern "C" void Resolution_t1578306928_marshal_pinvoke_back(const Resolution_t1578306928_marshaled_pinvoke& marshaled, Resolution_t1578306928& unmarshaled)
{
	int32_t unmarshaled_m_Width_temp_0 = 0;
	unmarshaled_m_Width_temp_0 = marshaled.___m_Width_0;
	unmarshaled.set_m_Width_0(unmarshaled_m_Width_temp_0);
	int32_t unmarshaled_m_Height_temp_1 = 0;
	unmarshaled_m_Height_temp_1 = marshaled.___m_Height_1;
	unmarshaled.set_m_Height_1(unmarshaled_m_Height_temp_1);
	int32_t unmarshaled_m_RefreshRate_temp_2 = 0;
	unmarshaled_m_RefreshRate_temp_2 = marshaled.___m_RefreshRate_2;
	unmarshaled.set_m_RefreshRate_2(unmarshaled_m_RefreshRate_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.Resolution
extern "C" void Resolution_t1578306928_marshal_pinvoke_cleanup(Resolution_t1578306928_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Resolution
extern "C" void Resolution_t1578306928_marshal_com(const Resolution_t1578306928& unmarshaled, Resolution_t1578306928_marshaled_com& marshaled)
{
	marshaled.___m_Width_0 = unmarshaled.get_m_Width_0();
	marshaled.___m_Height_1 = unmarshaled.get_m_Height_1();
	marshaled.___m_RefreshRate_2 = unmarshaled.get_m_RefreshRate_2();
}
extern "C" void Resolution_t1578306928_marshal_com_back(const Resolution_t1578306928_marshaled_com& marshaled, Resolution_t1578306928& unmarshaled)
{
	int32_t unmarshaled_m_Width_temp_0 = 0;
	unmarshaled_m_Width_temp_0 = marshaled.___m_Width_0;
	unmarshaled.set_m_Width_0(unmarshaled_m_Width_temp_0);
	int32_t unmarshaled_m_Height_temp_1 = 0;
	unmarshaled_m_Height_temp_1 = marshaled.___m_Height_1;
	unmarshaled.set_m_Height_1(unmarshaled_m_Height_temp_1);
	int32_t unmarshaled_m_RefreshRate_temp_2 = 0;
	unmarshaled_m_RefreshRate_temp_2 = marshaled.___m_RefreshRate_2;
	unmarshaled.set_m_RefreshRate_2(unmarshaled_m_RefreshRate_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.Resolution
extern "C" void Resolution_t1578306928_marshal_com_cleanup(Resolution_t1578306928_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ResourceRequest::.ctor()
extern "C"  void ResourceRequest__ctor_m2863879896 (ResourceRequest_t3731857623 * __this, const MethodInfo* method)
{
	{
		AsyncOperation__ctor_m162101250(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.ResourceRequest::get_asset()
extern "C"  Object_t3071478659 * ResourceRequest_get_asset_m670320982 (ResourceRequest_t3731857623 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_Path_1();
		Type_t * L_1 = __this->get_m_Type_2();
		Object_t3071478659 * L_2 = Resources_Load_m3601699608(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// Conversion methods for marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t3731857623_marshal_pinvoke(const ResourceRequest_t3731857623& unmarshaled, ResourceRequest_t3731857623_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
extern "C" void ResourceRequest_t3731857623_marshal_pinvoke_back(const ResourceRequest_t3731857623_marshaled_pinvoke& marshaled, ResourceRequest_t3731857623& unmarshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t3731857623_marshal_pinvoke_cleanup(ResourceRequest_t3731857623_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t3731857623_marshal_com(const ResourceRequest_t3731857623& unmarshaled, ResourceRequest_t3731857623_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
extern "C" void ResourceRequest_t3731857623_marshal_com_back(const ResourceRequest_t3731857623_marshaled_com& marshaled, ResourceRequest_t3731857623& unmarshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t3731857623_marshal_com_cleanup(ResourceRequest_t3731857623_marshaled_com& marshaled)
{
}
// UnityEngine.Object UnityEngine.Resources::Load(System.String)
extern const Il2CppType* Object_t3071478659_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Resources_Load_m2187391845_MetadataUsageId;
extern "C"  Object_t3071478659 * Resources_Load_m2187391845 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Resources_Load_m2187391845_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___path0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Object_t3071478659_0_0_0_var), /*hidden argument*/NULL);
		Object_t3071478659 * L_2 = Resources_Load_m3601699608(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
extern "C"  Object_t3071478659 * Resources_Load_m3601699608 (Il2CppObject * __this /* static, unused */, String_t* ___path0, Type_t * ___systemTypeInstance1, const MethodInfo* method)
{
	typedef Object_t3071478659 * (*Resources_Load_m3601699608_ftn) (String_t*, Type_t *);
	static Resources_Load_m3601699608_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_Load_m3601699608_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::Load(System.String,System.Type)");
	return _il2cpp_icall_func(___path0, ___systemTypeInstance1);
}
// System.Void UnityEngine.Resources::UnloadAsset(UnityEngine.Object)
extern "C"  void Resources_UnloadAsset_m3142802605 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___assetToUnload0, const MethodInfo* method)
{
	typedef void (*Resources_UnloadAsset_m3142802605_ftn) (Object_t3071478659 *);
	static Resources_UnloadAsset_m3142802605_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_UnloadAsset_m3142802605_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::UnloadAsset(UnityEngine.Object)");
	_il2cpp_icall_func(___assetToUnload0);
}
// UnityEngine.AsyncOperation UnityEngine.Resources::UnloadUnusedAssets()
extern "C"  AsyncOperation_t3699081103 * Resources_UnloadUnusedAssets_m3831138427 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef AsyncOperation_t3699081103 * (*Resources_UnloadUnusedAssets_m3831138427_ftn) ();
	static Resources_UnloadUnusedAssets_m3831138427_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_UnloadUnusedAssets_m3831138427_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::UnloadUnusedAssets()");
	return _il2cpp_icall_func();
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_velocity()
extern "C"  Vector3_t4282066566  Rigidbody_get_velocity_m2696244068 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rigidbody_INTERNAL_get_velocity_m1063590501(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody::set_velocity(UnityEngine.Vector3)
extern "C"  void Rigidbody_set_velocity_m799562119 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_set_velocity_m484592601(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_get_velocity(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_get_velocity_m1063590501 (Rigidbody_t3346577219 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_get_velocity_m1063590501_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *);
	static Rigidbody_INTERNAL_get_velocity_m1063590501_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_get_velocity_m1063590501_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_get_velocity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::INTERNAL_set_velocity(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_set_velocity_m484592601 (Rigidbody_t3346577219 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_set_velocity_m484592601_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *);
	static Rigidbody_INTERNAL_set_velocity_m484592601_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_set_velocity_m484592601_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_set_velocity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::set_drag(System.Single)
extern "C"  void Rigidbody_set_drag_m4061586082 (Rigidbody_t3346577219 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_drag_m4061586082_ftn) (Rigidbody_t3346577219 *, float);
	static Rigidbody_set_drag_m4061586082_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_drag_m4061586082_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_drag(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Rigidbody::get_mass()
extern "C"  float Rigidbody_get_mass_m3953106025 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	typedef float (*Rigidbody_get_mass_m3953106025_ftn) (Rigidbody_t3346577219 *);
	static Rigidbody_get_mass_m3953106025_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_mass_m3953106025_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_mass()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody::set_mass(System.Single)
extern "C"  void Rigidbody_set_mass_m1579962594 (Rigidbody_t3346577219 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_mass_m1579962594_ftn) (Rigidbody_t3346577219 *, float);
	static Rigidbody_set_mass_m1579962594_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_mass_m1579962594_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_mass(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::set_useGravity(System.Boolean)
extern "C"  void Rigidbody_set_useGravity_m2620827635 (Rigidbody_t3346577219 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_useGravity_m2620827635_ftn) (Rigidbody_t3346577219 *, bool);
	static Rigidbody_set_useGravity_m2620827635_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_useGravity_m2620827635_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_useGravity(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Rigidbody::get_isKinematic()
extern "C"  bool Rigidbody_get_isKinematic_m3963857442 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	typedef bool (*Rigidbody_get_isKinematic_m3963857442_ftn) (Rigidbody_t3346577219 *);
	static Rigidbody_get_isKinematic_m3963857442_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_isKinematic_m3963857442_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_isKinematic()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody::set_isKinematic(System.Boolean)
extern "C"  void Rigidbody_set_isKinematic_m294703295 (Rigidbody_t3346577219 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_isKinematic_m294703295_ftn) (Rigidbody_t3346577219 *, bool);
	static Rigidbody_set_isKinematic_m294703295_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_isKinematic_m294703295_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_isKinematic(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::set_freezeRotation(System.Boolean)
extern "C"  void Rigidbody_set_freezeRotation_m3989473889 (Rigidbody_t3346577219 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_freezeRotation_m3989473889_ftn) (Rigidbody_t3346577219 *, bool);
	static Rigidbody_set_freezeRotation_m3989473889_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_freezeRotation_m3989473889_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_freezeRotation(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddForce_m557267180 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___force0, int32_t ___mode1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode1;
		Rigidbody_INTERNAL_CALL_AddForce_m3651654387(NULL /*static, unused*/, __this, (&___force0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddForce_m3651654387 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, Vector3_t4282066566 * ___force1, int32_t ___mode2, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddForce_m3651654387_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *, int32_t);
	static Rigidbody_INTERNAL_CALL_AddForce_m3651654387_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddForce_m3651654387_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___force1, ___mode2);
}
// System.Void UnityEngine.Rigidbody::AddRelativeForce(UnityEngine.Vector3,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddRelativeForce_m2803598808 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___force0, int32_t ___mode1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode1;
		Rigidbody_INTERNAL_CALL_AddRelativeForce_m3589442951(NULL /*static, unused*/, __this, (&___force0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddRelativeForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddRelativeForce_m3589442951 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, Vector3_t4282066566 * ___force1, int32_t ___mode2, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddRelativeForce_m3589442951_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *, int32_t);
	static Rigidbody_INTERNAL_CALL_AddRelativeForce_m3589442951_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddRelativeForce_m3589442951_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddRelativeForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___force1, ___mode2);
}
// System.Void UnityEngine.Rigidbody::AddTorque(UnityEngine.Vector3,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddTorque_m3009708185 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___torque0, int32_t ___mode1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode1;
		Rigidbody_INTERNAL_CALL_AddTorque_m4194065160(NULL /*static, unused*/, __this, (&___torque0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddTorque(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddTorque_m4194065160 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, Vector3_t4282066566 * ___torque1, int32_t ___mode2, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddTorque_m4194065160_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *, int32_t);
	static Rigidbody_INTERNAL_CALL_AddTorque_m4194065160_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddTorque_m4194065160_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddTorque(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___torque1, ___mode2);
}
// System.Void UnityEngine.Rigidbody::AddRelativeTorque(UnityEngine.Vector3,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddRelativeTorque_m3926511917 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___torque0, int32_t ___mode1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode1;
		Rigidbody_INTERNAL_CALL_AddRelativeTorque_m2265510644(NULL /*static, unused*/, __this, (&___torque0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddRelativeTorque(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddRelativeTorque_m2265510644 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, Vector3_t4282066566 * ___torque1, int32_t ___mode2, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddRelativeTorque_m2265510644_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *, int32_t);
	static Rigidbody_INTERNAL_CALL_AddRelativeTorque_m2265510644_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddRelativeTorque_m2265510644_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddRelativeTorque(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___torque1, ___mode2);
}
// System.Void UnityEngine.Rigidbody::AddForceAtPosition(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddForceAtPosition_m1266619363 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___force0, Vector3_t4282066566  ___position1, int32_t ___mode2, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode2;
		Rigidbody_INTERNAL_CALL_AddForceAtPosition_m1493622644(NULL /*static, unused*/, __this, (&___force0), (&___position1), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddForceAtPosition(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddForceAtPosition_m1493622644 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, Vector3_t4282066566 * ___force1, Vector3_t4282066566 * ___position2, int32_t ___mode3, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddForceAtPosition_m1493622644_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *, Vector3_t4282066566 *, int32_t);
	static Rigidbody_INTERNAL_CALL_AddForceAtPosition_m1493622644_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddForceAtPosition_m1493622644_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddForceAtPosition(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___force1, ___position2, ___mode3);
}
// System.Void UnityEngine.Rigidbody::AddExplosionForce(System.Single,UnityEngine.Vector3,System.Single,System.Single,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddExplosionForce_m196999228 (Rigidbody_t3346577219 * __this, float ___explosionForce0, Vector3_t4282066566  ___explosionPosition1, float ___explosionRadius2, float ___upwardsModifier3, int32_t ___mode4, const MethodInfo* method)
{
	{
		float L_0 = ___explosionForce0;
		float L_1 = ___explosionRadius2;
		float L_2 = ___upwardsModifier3;
		int32_t L_3 = ___mode4;
		Rigidbody_INTERNAL_CALL_AddExplosionForce_m3109367769(NULL /*static, unused*/, __this, L_0, (&___explosionPosition1), L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddExplosionForce(UnityEngine.Rigidbody,System.Single,UnityEngine.Vector3&,System.Single,System.Single,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddExplosionForce_m3109367769 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, float ___explosionForce1, Vector3_t4282066566 * ___explosionPosition2, float ___explosionRadius3, float ___upwardsModifier4, int32_t ___mode5, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddExplosionForce_m3109367769_ftn) (Rigidbody_t3346577219 *, float, Vector3_t4282066566 *, float, float, int32_t);
	static Rigidbody_INTERNAL_CALL_AddExplosionForce_m3109367769_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddExplosionForce_m3109367769_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddExplosionForce(UnityEngine.Rigidbody,System.Single,UnityEngine.Vector3&,System.Single,System.Single,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___explosionForce1, ___explosionPosition2, ___explosionRadius3, ___upwardsModifier4, ___mode5);
}
// System.Void UnityEngine.Rigidbody::MovePosition(UnityEngine.Vector3)
extern "C"  void Rigidbody_MovePosition_m1515094375 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___position0, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_CALL_MovePosition_m2416276686(NULL /*static, unused*/, __this, (&___position0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody,UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_CALL_MovePosition_m2416276686 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, Vector3_t4282066566 * ___position1, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_MovePosition_m2416276686_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *);
	static Rigidbody_INTERNAL_CALL_MovePosition_m2416276686_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_MovePosition_m2416276686_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1);
}
// System.Void UnityEngine.Rigidbody::MoveRotation(UnityEngine.Quaternion)
extern "C"  void Rigidbody_MoveRotation_m38358738 (Rigidbody_t3346577219 * __this, Quaternion_t1553702882  ___rot0, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_CALL_MoveRotation_m4110814929(NULL /*static, unused*/, __this, (&___rot0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_MoveRotation(UnityEngine.Rigidbody,UnityEngine.Quaternion&)
extern "C"  void Rigidbody_INTERNAL_CALL_MoveRotation_m4110814929 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, Quaternion_t1553702882 * ___rot1, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_MoveRotation_m4110814929_ftn) (Rigidbody_t3346577219 *, Quaternion_t1553702882 *);
	static Rigidbody_INTERNAL_CALL_MoveRotation_m4110814929_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_MoveRotation_m4110814929_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_MoveRotation(UnityEngine.Rigidbody,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___self0, ___rot1);
}
// System.Void UnityEngine.Rigidbody::Sleep()
extern "C"  void Rigidbody_Sleep_m4049131361 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_CALL_Sleep_m1292822714(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_Sleep(UnityEngine.Rigidbody)
extern "C"  void Rigidbody_INTERNAL_CALL_Sleep_m1292822714 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_Sleep_m1292822714_ftn) (Rigidbody_t3346577219 *);
	static Rigidbody_INTERNAL_CALL_Sleep_m1292822714_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_Sleep_m1292822714_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_Sleep(UnityEngine.Rigidbody)");
	_il2cpp_icall_func(___self0);
}
// System.Boolean UnityEngine.Rigidbody::IsSleeping()
extern "C"  bool Rigidbody_IsSleeping_m435617895 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Rigidbody_INTERNAL_CALL_IsSleeping_m4112513622(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.Rigidbody::INTERNAL_CALL_IsSleeping(UnityEngine.Rigidbody)
extern "C"  bool Rigidbody_INTERNAL_CALL_IsSleeping_m4112513622 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, const MethodInfo* method)
{
	typedef bool (*Rigidbody_INTERNAL_CALL_IsSleeping_m4112513622_ftn) (Rigidbody_t3346577219 *);
	static Rigidbody_INTERNAL_CALL_IsSleeping_m4112513622_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_IsSleeping_m4112513622_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_IsSleeping(UnityEngine.Rigidbody)");
	return _il2cpp_icall_func(___self0);
}
// System.Void UnityEngine.Rigidbody::WakeUp()
extern "C"  void Rigidbody_WakeUp_m2643728503 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_CALL_WakeUp_m2627563334(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_WakeUp(UnityEngine.Rigidbody)
extern "C"  void Rigidbody_INTERNAL_CALL_WakeUp_m2627563334 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_WakeUp_m2627563334_ftn) (Rigidbody_t3346577219 *);
	static Rigidbody_INTERNAL_CALL_WakeUp_m2627563334_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_WakeUp_m2627563334_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_WakeUp(UnityEngine.Rigidbody)");
	_il2cpp_icall_func(___self0);
}
// System.Void UnityEngine.RPC::.ctor()
extern "C"  void RPC__ctor_m281827604 (RPC_t3134615963 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.SceneManagement.Scene::get_handle()
extern "C"  int32_t Scene_get_handle_m2277248521 (Scene_t1080795294 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Handle_0();
		return L_0;
	}
}
extern "C"  int32_t Scene_get_handle_m2277248521_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Scene_t1080795294 * _thisAdjusted = reinterpret_cast<Scene_t1080795294 *>(__this + 1);
	return Scene_get_handle_m2277248521(_thisAdjusted, method);
}
// System.String UnityEngine.SceneManagement.Scene::get_name()
extern "C"  String_t* Scene_get_name_m894591657 (Scene_t1080795294 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Scene_get_handle_m2277248521(__this, /*hidden argument*/NULL);
		String_t* L_1 = Scene_GetNameInternal_m2496405436(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
extern "C"  String_t* Scene_get_name_m894591657_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Scene_t1080795294 * _thisAdjusted = reinterpret_cast<Scene_t1080795294 *>(__this + 1);
	return Scene_get_name_m894591657(_thisAdjusted, method);
}
// System.Int32 UnityEngine.SceneManagement.Scene::get_buildIndex()
extern "C"  int32_t Scene_get_buildIndex_m3533090789 (Scene_t1080795294 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Scene_get_handle_m2277248521(__this, /*hidden argument*/NULL);
		int32_t L_1 = Scene_GetBuildIndexInternal_m76376146(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
extern "C"  int32_t Scene_get_buildIndex_m3533090789_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Scene_t1080795294 * _thisAdjusted = reinterpret_cast<Scene_t1080795294 *>(__this + 1);
	return Scene_get_buildIndex_m3533090789(_thisAdjusted, method);
}
// System.Int32 UnityEngine.SceneManagement.Scene::GetHashCode()
extern "C"  int32_t Scene_GetHashCode_m2000109307 (Scene_t1080795294 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Handle_0();
		return L_0;
	}
}
extern "C"  int32_t Scene_GetHashCode_m2000109307_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Scene_t1080795294 * _thisAdjusted = reinterpret_cast<Scene_t1080795294 *>(__this + 1);
	return Scene_GetHashCode_m2000109307(_thisAdjusted, method);
}
// System.Boolean UnityEngine.SceneManagement.Scene::Equals(System.Object)
extern Il2CppClass* Scene_t1080795294_il2cpp_TypeInfo_var;
extern const uint32_t Scene_Equals_m93578403_MetadataUsageId;
extern "C"  bool Scene_Equals_m93578403 (Scene_t1080795294 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Scene_Equals_m93578403_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Scene_t1080795294  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Scene_t1080795294_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(Scene_t1080795294 *)((Scene_t1080795294 *)UnBox (L_1, Scene_t1080795294_il2cpp_TypeInfo_var))));
		int32_t L_2 = Scene_get_handle_m2277248521(__this, /*hidden argument*/NULL);
		int32_t L_3 = Scene_get_handle_m2277248521((&V_0), /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)L_3))? 1 : 0);
	}
}
extern "C"  bool Scene_Equals_m93578403_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Scene_t1080795294 * _thisAdjusted = reinterpret_cast<Scene_t1080795294 *>(__this + 1);
	return Scene_Equals_m93578403(_thisAdjusted, ___other0, method);
}
// System.String UnityEngine.SceneManagement.Scene::GetNameInternal(System.Int32)
extern "C"  String_t* Scene_GetNameInternal_m2496405436 (Il2CppObject * __this /* static, unused */, int32_t ___sceneHandle0, const MethodInfo* method)
{
	typedef String_t* (*Scene_GetNameInternal_m2496405436_ftn) (int32_t);
	static Scene_GetNameInternal_m2496405436_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Scene_GetNameInternal_m2496405436_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SceneManagement.Scene::GetNameInternal(System.Int32)");
	return _il2cpp_icall_func(___sceneHandle0);
}
// System.Int32 UnityEngine.SceneManagement.Scene::GetBuildIndexInternal(System.Int32)
extern "C"  int32_t Scene_GetBuildIndexInternal_m76376146 (Il2CppObject * __this /* static, unused */, int32_t ___sceneHandle0, const MethodInfo* method)
{
	typedef int32_t (*Scene_GetBuildIndexInternal_m76376146_ftn) (int32_t);
	static Scene_GetBuildIndexInternal_m76376146_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Scene_GetBuildIndexInternal_m76376146_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SceneManagement.Scene::GetBuildIndexInternal(System.Int32)");
	return _il2cpp_icall_func(___sceneHandle0);
}
// Conversion methods for marshalling of: UnityEngine.SceneManagement.Scene
extern "C" void Scene_t1080795294_marshal_pinvoke(const Scene_t1080795294& unmarshaled, Scene_t1080795294_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Handle_0 = unmarshaled.get_m_Handle_0();
}
extern "C" void Scene_t1080795294_marshal_pinvoke_back(const Scene_t1080795294_marshaled_pinvoke& marshaled, Scene_t1080795294& unmarshaled)
{
	int32_t unmarshaled_m_Handle_temp_0 = 0;
	unmarshaled_m_Handle_temp_0 = marshaled.___m_Handle_0;
	unmarshaled.set_m_Handle_0(unmarshaled_m_Handle_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.SceneManagement.Scene
extern "C" void Scene_t1080795294_marshal_pinvoke_cleanup(Scene_t1080795294_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SceneManagement.Scene
extern "C" void Scene_t1080795294_marshal_com(const Scene_t1080795294& unmarshaled, Scene_t1080795294_marshaled_com& marshaled)
{
	marshaled.___m_Handle_0 = unmarshaled.get_m_Handle_0();
}
extern "C" void Scene_t1080795294_marshal_com_back(const Scene_t1080795294_marshaled_com& marshaled, Scene_t1080795294& unmarshaled)
{
	int32_t unmarshaled_m_Handle_temp_0 = 0;
	unmarshaled_m_Handle_temp_0 = marshaled.___m_Handle_0;
	unmarshaled.set_m_Handle_0(unmarshaled_m_Handle_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.SceneManagement.Scene
extern "C" void Scene_t1080795294_marshal_com_cleanup(Scene_t1080795294_marshaled_com& marshaled)
{
}
// System.Int32 UnityEngine.SceneManagement.SceneManager::get_sceneCount()
extern "C"  int32_t SceneManager_get_sceneCount_m1279679683 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*SceneManager_get_sceneCount_m1279679683_ftn) ();
	static SceneManager_get_sceneCount_m1279679683_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SceneManager_get_sceneCount_m1279679683_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SceneManagement.SceneManager::get_sceneCount()");
	return _il2cpp_icall_func();
}
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetActiveScene()
extern "C"  Scene_t1080795294  SceneManager_GetActiveScene_m3062973092 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Scene_t1080795294  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		SceneManager_INTERNAL_CALL_GetActiveScene_m3594338006(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Scene_t1080795294  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_GetActiveScene(UnityEngine.SceneManagement.Scene&)
extern "C"  void SceneManager_INTERNAL_CALL_GetActiveScene_m3594338006 (Il2CppObject * __this /* static, unused */, Scene_t1080795294 * ___value0, const MethodInfo* method)
{
	typedef void (*SceneManager_INTERNAL_CALL_GetActiveScene_m3594338006_ftn) (Scene_t1080795294 *);
	static SceneManager_INTERNAL_CALL_GetActiveScene_m3594338006_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SceneManager_INTERNAL_CALL_GetActiveScene_m3594338006_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_GetActiveScene(UnityEngine.SceneManagement.Scene&)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetSceneAt(System.Int32)
extern "C"  Scene_t1080795294  SceneManager_GetSceneAt_m2396963182 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method)
{
	Scene_t1080795294  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		SceneManager_INTERNAL_CALL_GetSceneAt_m1242004550(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		Scene_t1080795294  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_GetSceneAt(System.Int32,UnityEngine.SceneManagement.Scene&)
extern "C"  void SceneManager_INTERNAL_CALL_GetSceneAt_m1242004550 (Il2CppObject * __this /* static, unused */, int32_t ___index0, Scene_t1080795294 * ___value1, const MethodInfo* method)
{
	typedef void (*SceneManager_INTERNAL_CALL_GetSceneAt_m1242004550_ftn) (int32_t, Scene_t1080795294 *);
	static SceneManager_INTERNAL_CALL_GetSceneAt_m1242004550_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SceneManager_INTERNAL_CALL_GetSceneAt_m1242004550_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_GetSceneAt(System.Int32,UnityEngine.SceneManagement.Scene&)");
	_il2cpp_icall_func(___index0, ___value1);
}
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
extern "C"  void SceneManager_LoadScene_m2167814033 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		String_t* L_0 = ___sceneName0;
		int32_t L_1 = V_0;
		SceneManager_LoadScene_m3907168970(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String,UnityEngine.SceneManagement.LoadSceneMode)
extern "C"  void SceneManager_LoadScene_m3907168970 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, int32_t ___mode1, const MethodInfo* method)
{
	int32_t G_B2_0 = 0;
	String_t* G_B2_1 = NULL;
	int32_t G_B1_0 = 0;
	String_t* G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	String_t* G_B3_2 = NULL;
	{
		String_t* L_0 = ___sceneName0;
		int32_t L_1 = ___mode1;
		G_B1_0 = (-1);
		G_B1_1 = L_0;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			G_B2_0 = (-1);
			G_B2_1 = L_0;
			goto IL_000f;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0010;
	}

IL_000f:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0010:
	{
		SceneManager_LoadSceneAsyncNameIndexInternal_m3775081569(NULL /*static, unused*/, G_B3_2, G_B3_1, (bool)G_B3_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.Int32,UnityEngine.SceneManagement.LoadSceneMode)
extern "C"  void SceneManager_LoadScene_m2455768283 (Il2CppObject * __this /* static, unused */, int32_t ___sceneBuildIndex0, int32_t ___mode1, const MethodInfo* method)
{
	int32_t G_B2_0 = 0;
	Il2CppObject * G_B2_1 = NULL;
	int32_t G_B1_0 = 0;
	Il2CppObject * G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	Il2CppObject * G_B3_2 = NULL;
	{
		int32_t L_0 = ___sceneBuildIndex0;
		int32_t L_1 = ___mode1;
		G_B1_0 = L_0;
		G_B1_1 = NULL;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			G_B2_0 = L_0;
			G_B2_1 = NULL;
			goto IL_000f;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0010;
	}

IL_000f:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0010:
	{
		SceneManager_LoadSceneAsyncNameIndexInternal_m3775081569(NULL /*static, unused*/, (String_t*)G_B3_2, G_B3_1, (bool)G_B3_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsync(System.String,UnityEngine.SceneManagement.LoadSceneMode)
extern "C"  AsyncOperation_t3699081103 * SceneManager_LoadSceneAsync_m1685951617 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, int32_t ___mode1, const MethodInfo* method)
{
	int32_t G_B2_0 = 0;
	String_t* G_B2_1 = NULL;
	int32_t G_B1_0 = 0;
	String_t* G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	String_t* G_B3_2 = NULL;
	{
		String_t* L_0 = ___sceneName0;
		int32_t L_1 = ___mode1;
		G_B1_0 = (-1);
		G_B1_1 = L_0;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			G_B2_0 = (-1);
			G_B2_1 = L_0;
			goto IL_000f;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0010;
	}

IL_000f:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0010:
	{
		AsyncOperation_t3699081103 * L_2 = SceneManager_LoadSceneAsyncNameIndexInternal_m3775081569(NULL /*static, unused*/, G_B3_2, G_B3_1, (bool)G_B3_0, (bool)0, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsyncNameIndexInternal(System.String,System.Int32,System.Boolean,System.Boolean)
extern "C"  AsyncOperation_t3699081103 * SceneManager_LoadSceneAsyncNameIndexInternal_m3775081569 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, int32_t ___sceneBuildIndex1, bool ___isAdditive2, bool ___mustCompleteNextFrame3, const MethodInfo* method)
{
	typedef AsyncOperation_t3699081103 * (*SceneManager_LoadSceneAsyncNameIndexInternal_m3775081569_ftn) (String_t*, int32_t, bool, bool);
	static SceneManager_LoadSceneAsyncNameIndexInternal_m3775081569_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SceneManager_LoadSceneAsyncNameIndexInternal_m3775081569_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SceneManagement.SceneManager::LoadSceneAsyncNameIndexInternal(System.String,System.Int32,System.Boolean,System.Boolean)");
	return _il2cpp_icall_func(___sceneName0, ___sceneBuildIndex1, ___isAdditive2, ___mustCompleteNextFrame3);
}
// System.Boolean UnityEngine.SceneManagement.SceneManager::UnloadScene(System.String)
extern "C"  bool SceneManager_UnloadScene_m4140861840 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___sceneName0;
		bool L_1 = SceneManager_UnloadSceneNameIndexInternal_m3857679177(NULL /*static, unused*/, L_0, (-1), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.SceneManagement.SceneManager::UnloadSceneNameIndexInternal(System.String,System.Int32)
extern "C"  bool SceneManager_UnloadSceneNameIndexInternal_m3857679177 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, int32_t ___sceneBuildIndex1, const MethodInfo* method)
{
	typedef bool (*SceneManager_UnloadSceneNameIndexInternal_m3857679177_ftn) (String_t*, int32_t);
	static SceneManager_UnloadSceneNameIndexInternal_m3857679177_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SceneManager_UnloadSceneNameIndexInternal_m3857679177_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SceneManagement.SceneManager::UnloadSceneNameIndexInternal(System.String,System.Int32)");
	return _il2cpp_icall_func(___sceneName0, ___sceneBuildIndex1);
}
// System.Boolean UnityEngine.Screen::get_lockCursor()
extern "C"  bool Screen_get_lockCursor_m3224339411 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		int32_t L_0 = Cursor_get_lockState_m2795946910(NULL /*static, unused*/, /*hidden argument*/NULL);
		return (bool)((((int32_t)1) == ((int32_t)L_0))? 1 : 0);
	}
}
// System.Void UnityEngine.Screen::set_lockCursor(System.Boolean)
extern "C"  void Screen_set_lockCursor_m286058008 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Cursor_set_visible_m4101409761(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		Cursor_set_lockState_m3065915939(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		goto IL_0023;
	}

IL_0017:
	{
		Cursor_set_lockState_m3065915939(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		Cursor_set_visible_m4101409761(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// UnityEngine.Resolution UnityEngine.Screen::get_currentResolution()
extern "C"  Resolution_t1578306928  Screen_get_currentResolution_m2532370351 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef Resolution_t1578306928  (*Screen_get_currentResolution_m2532370351_ftn) ();
	static Screen_get_currentResolution_m2532370351_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_currentResolution_m2532370351_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_currentResolution()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Screen::get_width()
extern "C"  int32_t Screen_get_width_m3080333084 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Screen_get_width_m3080333084_ftn) ();
	static Screen_get_width_m3080333084_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_width_m3080333084_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_width()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Screen::get_height()
extern "C"  int32_t Screen_get_height_m1504859443 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Screen_get_height_m1504859443_ftn) ();
	static Screen_get_height_m1504859443_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_height_m1504859443_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_height()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Screen::get_dpi()
extern "C"  float Screen_get_dpi_m3780069159 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Screen_get_dpi_m3780069159_ftn) ();
	static Screen_get_dpi_m3780069159_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_dpi_m3780069159_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_dpi()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.Screen::get_fullScreen()
extern "C"  bool Screen_get_fullScreen_m4283694285 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Screen_get_fullScreen_m4283694285_ftn) ();
	static Screen_get_fullScreen_m4283694285_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_fullScreen_m4283694285_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_fullScreen()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Screen::set_fullScreen(System.Boolean)
extern "C"  void Screen_set_fullScreen_m2604999058 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*Screen_set_fullScreen_m2604999058_ftn) (bool);
	static Screen_set_fullScreen_m2604999058_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_fullScreen_m2604999058_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_fullScreen(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.Screen::get_autorotateToPortrait()
extern "C"  bool Screen_get_autorotateToPortrait_m187839762 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Screen_get_autorotateToPortrait_m187839762_ftn) ();
	static Screen_get_autorotateToPortrait_m187839762_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_autorotateToPortrait_m187839762_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_autorotateToPortrait()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Screen::set_autorotateToPortrait(System.Boolean)
extern "C"  void Screen_set_autorotateToPortrait_m3130934167 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*Screen_set_autorotateToPortrait_m3130934167_ftn) (bool);
	static Screen_set_autorotateToPortrait_m3130934167_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_autorotateToPortrait_m3130934167_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_autorotateToPortrait(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.Screen::get_autorotateToPortraitUpsideDown()
extern "C"  bool Screen_get_autorotateToPortraitUpsideDown_m631759142 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Screen_get_autorotateToPortraitUpsideDown_m631759142_ftn) ();
	static Screen_get_autorotateToPortraitUpsideDown_m631759142_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_autorotateToPortraitUpsideDown_m631759142_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_autorotateToPortraitUpsideDown()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Screen::set_autorotateToPortraitUpsideDown(System.Boolean)
extern "C"  void Screen_set_autorotateToPortraitUpsideDown_m493773611 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*Screen_set_autorotateToPortraitUpsideDown_m493773611_ftn) (bool);
	static Screen_set_autorotateToPortraitUpsideDown_m493773611_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_autorotateToPortraitUpsideDown_m493773611_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_autorotateToPortraitUpsideDown(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.Screen::get_autorotateToLandscapeLeft()
extern "C"  bool Screen_get_autorotateToLandscapeLeft_m3574433901 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Screen_get_autorotateToLandscapeLeft_m3574433901_ftn) ();
	static Screen_get_autorotateToLandscapeLeft_m3574433901_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_autorotateToLandscapeLeft_m3574433901_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_autorotateToLandscapeLeft()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Screen::set_autorotateToLandscapeLeft(System.Boolean)
extern "C"  void Screen_set_autorotateToLandscapeLeft_m3928884054 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*Screen_set_autorotateToLandscapeLeft_m3928884054_ftn) (bool);
	static Screen_set_autorotateToLandscapeLeft_m3928884054_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_autorotateToLandscapeLeft_m3928884054_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_autorotateToLandscapeLeft(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.Screen::get_autorotateToLandscapeRight()
extern "C"  bool Screen_get_autorotateToLandscapeRight_m283511704 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Screen_get_autorotateToLandscapeRight_m283511704_ftn) ();
	static Screen_get_autorotateToLandscapeRight_m283511704_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_autorotateToLandscapeRight_m283511704_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_autorotateToLandscapeRight()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Screen::set_autorotateToLandscapeRight(System.Boolean)
extern "C"  void Screen_set_autorotateToLandscapeRight_m158995741 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*Screen_set_autorotateToLandscapeRight_m158995741_ftn) (bool);
	static Screen_set_autorotateToLandscapeRight_m158995741_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_autorotateToLandscapeRight_m158995741_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_autorotateToLandscapeRight(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.ScreenOrientation UnityEngine.Screen::get_orientation()
extern "C"  int32_t Screen_get_orientation_m1193220576 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Screen_get_orientation_m1193220576_ftn) ();
	static Screen_get_orientation_m1193220576_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_orientation_m1193220576_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_orientation()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Screen::set_orientation(UnityEngine.ScreenOrientation)
extern "C"  void Screen_set_orientation_m931760051 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Screen_set_orientation_m931760051_ftn) (int32_t);
	static Screen_set_orientation_m931760051_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_orientation_m931760051_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_orientation(UnityEngine.ScreenOrientation)");
	_il2cpp_icall_func(___value0);
}
// System.Int32 UnityEngine.Screen::get_sleepTimeout()
extern "C"  int32_t Screen_get_sleepTimeout_m4077361558 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Screen_get_sleepTimeout_m4077361558_ftn) ();
	static Screen_get_sleepTimeout_m4077361558_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_sleepTimeout_m4077361558_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_sleepTimeout()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Screen::set_sleepTimeout(System.Int32)
extern "C"  void Screen_set_sleepTimeout_m4188281051 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Screen_set_sleepTimeout_m4188281051_ftn) (int32_t);
	static Screen_set_sleepTimeout_m4188281051_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_sleepTimeout_m4188281051_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_sleepTimeout(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.ScriptableObject::.ctor()
extern "C"  void ScriptableObject__ctor_m1827087273 (ScriptableObject_t2970544072 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m570634126(__this, /*hidden argument*/NULL);
		ScriptableObject_Internal_CreateScriptableObject_m2334361070(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)
extern "C"  void ScriptableObject_Internal_CreateScriptableObject_m2334361070 (Il2CppObject * __this /* static, unused */, ScriptableObject_t2970544072 * ___self0, const MethodInfo* method)
{
	typedef void (*ScriptableObject_Internal_CreateScriptableObject_m2334361070_ftn) (ScriptableObject_t2970544072 *);
	static ScriptableObject_Internal_CreateScriptableObject_m2334361070_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_Internal_CreateScriptableObject_m2334361070_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)");
	_il2cpp_icall_func(___self0);
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.String)
extern "C"  ScriptableObject_t2970544072 * ScriptableObject_CreateInstance_m750914562 (Il2CppObject * __this /* static, unused */, String_t* ___className0, const MethodInfo* method)
{
	typedef ScriptableObject_t2970544072 * (*ScriptableObject_CreateInstance_m750914562_ftn) (String_t*);
	static ScriptableObject_CreateInstance_m750914562_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_CreateInstance_m750914562_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::CreateInstance(System.String)");
	return _il2cpp_icall_func(___className0);
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.Type)
extern "C"  ScriptableObject_t2970544072 * ScriptableObject_CreateInstance_m3255479417 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type0;
		ScriptableObject_t2970544072 * L_1 = ScriptableObject_CreateInstanceFromType_m3795352533(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)
extern "C"  ScriptableObject_t2970544072 * ScriptableObject_CreateInstanceFromType_m3795352533 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	typedef ScriptableObject_t2970544072 * (*ScriptableObject_CreateInstanceFromType_m3795352533_ftn) (Type_t *);
	static ScriptableObject_CreateInstanceFromType_m3795352533_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_CreateInstanceFromType_m3795352533_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)");
	return _il2cpp_icall_func(___type0);
}
// Conversion methods for marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t2970544072_marshal_pinvoke(const ScriptableObject_t2970544072& unmarshaled, ScriptableObject_t2970544072_marshaled_pinvoke& marshaled)
{
	marshaled.___m_InstanceID_0 = unmarshaled.get_m_InstanceID_0();
	marshaled.___m_CachedPtr_1 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_1()).get_m_value_0());
}
extern "C" void ScriptableObject_t2970544072_marshal_pinvoke_back(const ScriptableObject_t2970544072_marshaled_pinvoke& marshaled, ScriptableObject_t2970544072& unmarshaled)
{
	int32_t unmarshaled_m_InstanceID_temp_0 = 0;
	unmarshaled_m_InstanceID_temp_0 = marshaled.___m_InstanceID_0;
	unmarshaled.set_m_InstanceID_0(unmarshaled_m_InstanceID_temp_0);
	IntPtr_t unmarshaled_m_CachedPtr_temp_1;
	memset(&unmarshaled_m_CachedPtr_temp_1, 0, sizeof(unmarshaled_m_CachedPtr_temp_1));
	IntPtr_t unmarshaled_m_CachedPtr_temp_1_temp;
	unmarshaled_m_CachedPtr_temp_1_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_CachedPtr_1));
	unmarshaled_m_CachedPtr_temp_1 = unmarshaled_m_CachedPtr_temp_1_temp;
	unmarshaled.set_m_CachedPtr_1(unmarshaled_m_CachedPtr_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t2970544072_marshal_pinvoke_cleanup(ScriptableObject_t2970544072_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t2970544072_marshal_com(const ScriptableObject_t2970544072& unmarshaled, ScriptableObject_t2970544072_marshaled_com& marshaled)
{
	marshaled.___m_InstanceID_0 = unmarshaled.get_m_InstanceID_0();
	marshaled.___m_CachedPtr_1 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_1()).get_m_value_0());
}
extern "C" void ScriptableObject_t2970544072_marshal_com_back(const ScriptableObject_t2970544072_marshaled_com& marshaled, ScriptableObject_t2970544072& unmarshaled)
{
	int32_t unmarshaled_m_InstanceID_temp_0 = 0;
	unmarshaled_m_InstanceID_temp_0 = marshaled.___m_InstanceID_0;
	unmarshaled.set_m_InstanceID_0(unmarshaled_m_InstanceID_temp_0);
	IntPtr_t unmarshaled_m_CachedPtr_temp_1;
	memset(&unmarshaled_m_CachedPtr_temp_1, 0, sizeof(unmarshaled_m_CachedPtr_temp_1));
	IntPtr_t unmarshaled_m_CachedPtr_temp_1_temp;
	unmarshaled_m_CachedPtr_temp_1_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_CachedPtr_1));
	unmarshaled_m_CachedPtr_temp_1 = unmarshaled_m_CachedPtr_temp_1_temp;
	unmarshaled.set_m_CachedPtr_1(unmarshaled_m_CachedPtr_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t2970544072_marshal_com_cleanup(ScriptableObject_t2970544072_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Scripting.RequiredByNativeCodeAttribute::.ctor()
extern "C"  void RequiredByNativeCodeAttribute__ctor_m940065582 (RequiredByNativeCodeAttribute_t3165457172 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Scripting.UsedByNativeCodeAttribute::.ctor()
extern "C"  void UsedByNativeCodeAttribute__ctor_m3320039756 (UsedByNativeCodeAttribute_t3197444790 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SelectionBaseAttribute::.ctor()
extern "C"  void SelectionBaseAttribute__ctor_m3830336046 (SelectionBaseAttribute_t204085987 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::.cctor()
extern Il2CppClass* SendMouseEvents_t3965481900_il2cpp_TypeInfo_var;
extern Il2CppClass* HitInfoU5BU5D_t3452915852_il2cpp_TypeInfo_var;
extern Il2CppClass* HitInfo_t3209134097_il2cpp_TypeInfo_var;
extern const uint32_t SendMouseEvents__cctor_m2731695210_MetadataUsageId;
extern "C"  void SendMouseEvents__cctor_m2731695210 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents__cctor_m2731695210_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	HitInfo_t3209134097  V_0;
	memset(&V_0, 0, sizeof(V_0));
	HitInfo_t3209134097  V_1;
	memset(&V_1, 0, sizeof(V_1));
	HitInfo_t3209134097  V_2;
	memset(&V_2, 0, sizeof(V_2));
	HitInfo_t3209134097  V_3;
	memset(&V_3, 0, sizeof(V_3));
	HitInfo_t3209134097  V_4;
	memset(&V_4, 0, sizeof(V_4));
	HitInfo_t3209134097  V_5;
	memset(&V_5, 0, sizeof(V_5));
	HitInfo_t3209134097  V_6;
	memset(&V_6, 0, sizeof(V_6));
	HitInfo_t3209134097  V_7;
	memset(&V_7, 0, sizeof(V_7));
	HitInfo_t3209134097  V_8;
	memset(&V_8, 0, sizeof(V_8));
	{
		((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->set_s_MouseUsed_0((bool)0);
		HitInfoU5BU5D_t3452915852* L_0 = ((HitInfoU5BU5D_t3452915852*)SZArrayNew(HitInfoU5BU5D_t3452915852_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_0));
		HitInfo_t3209134097  L_1 = V_0;
		(*(HitInfo_t3209134097 *)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_1;
		HitInfoU5BU5D_t3452915852* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_1));
		HitInfo_t3209134097  L_3 = V_1;
		(*(HitInfo_t3209134097 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_3;
		HitInfoU5BU5D_t3452915852* L_4 = L_2;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_2));
		HitInfo_t3209134097  L_5 = V_2;
		(*(HitInfo_t3209134097 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_5;
		((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->set_m_LastHit_1(L_4);
		HitInfoU5BU5D_t3452915852* L_6 = ((HitInfoU5BU5D_t3452915852*)SZArrayNew(HitInfoU5BU5D_t3452915852_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_3));
		HitInfo_t3209134097  L_7 = V_3;
		(*(HitInfo_t3209134097 *)((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_7;
		HitInfoU5BU5D_t3452915852* L_8 = L_6;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_4));
		HitInfo_t3209134097  L_9 = V_4;
		(*(HitInfo_t3209134097 *)((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_9;
		HitInfoU5BU5D_t3452915852* L_10 = L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_5));
		HitInfo_t3209134097  L_11 = V_5;
		(*(HitInfo_t3209134097 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_11;
		((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->set_m_MouseDownHit_2(L_10);
		HitInfoU5BU5D_t3452915852* L_12 = ((HitInfoU5BU5D_t3452915852*)SZArrayNew(HitInfoU5BU5D_t3452915852_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 0);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_6));
		HitInfo_t3209134097  L_13 = V_6;
		(*(HitInfo_t3209134097 *)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_13;
		HitInfoU5BU5D_t3452915852* L_14 = L_12;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 1);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_7));
		HitInfo_t3209134097  L_15 = V_7;
		(*(HitInfo_t3209134097 *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_15;
		HitInfoU5BU5D_t3452915852* L_16 = L_14;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 2);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_8));
		HitInfo_t3209134097  L_17 = V_8;
		(*(HitInfo_t3209134097 *)((L_16)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_17;
		((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->set_m_CurrentHit_3(L_16);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::SetMouseMoved()
extern Il2CppClass* SendMouseEvents_t3965481900_il2cpp_TypeInfo_var;
extern const uint32_t SendMouseEvents_SetMouseMoved_m2590456785_MetadataUsageId;
extern "C"  void SendMouseEvents_SetMouseMoved_m2590456785 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents_SetMouseMoved_m2590456785_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->set_s_MouseUsed_0((bool)1);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::DoSendMouseEvents(System.Int32)
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppClass* SendMouseEvents_t3965481900_il2cpp_TypeInfo_var;
extern Il2CppClass* CameraU5BU5D_t2716570836_il2cpp_TypeInfo_var;
extern Il2CppClass* HitInfo_t3209134097_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisGUILayer_t2983897946_m2891371969_MethodInfo_var;
extern const uint32_t SendMouseEvents_DoSendMouseEvents_m4104134333_MetadataUsageId;
extern "C"  void SendMouseEvents_DoSendMouseEvents_m4104134333 (Il2CppObject * __this /* static, unused */, int32_t ___skipRTCameras0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents_DoSendMouseEvents_m4104134333_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Camera_t2727095145 * V_3 = NULL;
	CameraU5BU5D_t2716570836* V_4 = NULL;
	int32_t V_5 = 0;
	Rect_t4241904616  V_6;
	memset(&V_6, 0, sizeof(V_6));
	GUILayer_t2983897946 * V_7 = NULL;
	GUIElement_t3775428101 * V_8 = NULL;
	Ray_t3134616544  V_9;
	memset(&V_9, 0, sizeof(V_9));
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	GameObject_t3674682005 * V_12 = NULL;
	GameObject_t3674682005 * V_13 = NULL;
	int32_t V_14 = 0;
	HitInfo_t3209134097  V_15;
	memset(&V_15, 0, sizeof(V_15));
	Vector3_t4282066566  V_16;
	memset(&V_16, 0, sizeof(V_16));
	float G_B23_0 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		Vector3_t4282066566  L_0 = Input_get_mousePosition_m4020233228(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Camera_get_allCamerasCount_m3993434431(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		CameraU5BU5D_t2716570836* L_2 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_Cameras_4();
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		CameraU5BU5D_t2716570836* L_3 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_Cameras_4();
		NullCheck(L_3);
		int32_t L_4 = V_1;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))) == ((int32_t)L_4)))
		{
			goto IL_002e;
		}
	}

IL_0023:
	{
		int32_t L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->set_m_Cameras_4(((CameraU5BU5D_t2716570836*)SZArrayNew(CameraU5BU5D_t2716570836_il2cpp_TypeInfo_var, (uint32_t)L_5)));
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		CameraU5BU5D_t2716570836* L_6 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_Cameras_4();
		Camera_GetAllCameras_m3771867787(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_2 = 0;
		goto IL_005e;
	}

IL_0040:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_7 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		int32_t L_8 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_15));
		HitInfo_t3209134097  L_9 = V_15;
		(*(HitInfo_t3209134097 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8)))) = L_9;
		int32_t L_10 = V_2;
		V_2 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_005e:
	{
		int32_t L_11 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_12 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		bool L_13 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_s_MouseUsed_0();
		if (L_13)
		{
			goto IL_02c3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		CameraU5BU5D_t2716570836* L_14 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_Cameras_4();
		V_4 = L_14;
		V_5 = 0;
		goto IL_02b8;
	}

IL_0084:
	{
		CameraU5BU5D_t2716570836* L_15 = V_4;
		int32_t L_16 = V_5;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		Camera_t2727095145 * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		V_3 = L_18;
		Camera_t2727095145 * L_19 = V_3;
		bool L_20 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_19, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_00ad;
		}
	}
	{
		int32_t L_21 = ___skipRTCameras0;
		if (!L_21)
		{
			goto IL_00b2;
		}
	}
	{
		Camera_t2727095145 * L_22 = V_3;
		NullCheck(L_22);
		RenderTexture_t1963041563 * L_23 = Camera_get_targetTexture_m1468336738(L_22, /*hidden argument*/NULL);
		bool L_24 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_23, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00b2;
		}
	}

IL_00ad:
	{
		goto IL_02b2;
	}

IL_00b2:
	{
		Camera_t2727095145 * L_25 = V_3;
		NullCheck(L_25);
		Rect_t4241904616  L_26 = Camera_get_pixelRect_m936851539(L_25, /*hidden argument*/NULL);
		V_6 = L_26;
		Vector3_t4282066566  L_27 = V_0;
		bool L_28 = Rect_Contains_m3556594041((&V_6), L_27, /*hidden argument*/NULL);
		if (L_28)
		{
			goto IL_00cc;
		}
	}
	{
		goto IL_02b2;
	}

IL_00cc:
	{
		Camera_t2727095145 * L_29 = V_3;
		NullCheck(L_29);
		GUILayer_t2983897946 * L_30 = Component_GetComponent_TisGUILayer_t2983897946_m2891371969(L_29, /*hidden argument*/Component_GetComponent_TisGUILayer_t2983897946_m2891371969_MethodInfo_var);
		V_7 = L_30;
		GUILayer_t2983897946 * L_31 = V_7;
		bool L_32 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0145;
		}
	}
	{
		GUILayer_t2983897946 * L_33 = V_7;
		Vector3_t4282066566  L_34 = V_0;
		NullCheck(L_33);
		GUIElement_t3775428101 * L_35 = GUILayer_HitTest_m3356120918(L_33, L_34, /*hidden argument*/NULL);
		V_8 = L_35;
		GUIElement_t3775428101 * L_36 = V_8;
		bool L_37 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_0123;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_38 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		GUIElement_t3775428101 * L_39 = V_8;
		NullCheck(L_39);
		GameObject_t3674682005 * L_40 = Component_get_gameObject_m1170635899(L_39, /*hidden argument*/NULL);
		((L_38)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_target_0(L_40);
		HitInfoU5BU5D_t3452915852* L_41 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, 0);
		Camera_t2727095145 * L_42 = V_3;
		((L_41)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_camera_1(L_42);
		goto IL_0145;
	}

IL_0123:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_43 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, 0);
		((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_target_0((GameObject_t3674682005 *)NULL);
		HitInfoU5BU5D_t3452915852* L_44 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, 0);
		((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_camera_1((Camera_t2727095145 *)NULL);
	}

IL_0145:
	{
		Camera_t2727095145 * L_45 = V_3;
		NullCheck(L_45);
		int32_t L_46 = Camera_get_eventMask_m3669132771(L_45, /*hidden argument*/NULL);
		if (L_46)
		{
			goto IL_0155;
		}
	}
	{
		goto IL_02b2;
	}

IL_0155:
	{
		Camera_t2727095145 * L_47 = V_3;
		Vector3_t4282066566  L_48 = V_0;
		NullCheck(L_47);
		Ray_t3134616544  L_49 = Camera_ScreenPointToRay_m1733083890(L_47, L_48, /*hidden argument*/NULL);
		V_9 = L_49;
		Vector3_t4282066566  L_50 = Ray_get_direction_m3201866877((&V_9), /*hidden argument*/NULL);
		V_16 = L_50;
		float L_51 = (&V_16)->get_z_3();
		V_10 = L_51;
		float L_52 = V_10;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		bool L_53 = Mathf_Approximately_m1395529776(NULL /*static, unused*/, (0.0f), L_52, /*hidden argument*/NULL);
		if (!L_53)
		{
			goto IL_018b;
		}
	}
	{
		G_B23_0 = (std::numeric_limits<float>::infinity());
		goto IL_01a0;
	}

IL_018b:
	{
		Camera_t2727095145 * L_54 = V_3;
		NullCheck(L_54);
		float L_55 = Camera_get_farClipPlane_m388706726(L_54, /*hidden argument*/NULL);
		Camera_t2727095145 * L_56 = V_3;
		NullCheck(L_56);
		float L_57 = Camera_get_nearClipPlane_m4074655061(L_56, /*hidden argument*/NULL);
		float L_58 = V_10;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_59 = fabsf(((float)((float)((float)((float)L_55-(float)L_57))/(float)L_58)));
		G_B23_0 = L_59;
	}

IL_01a0:
	{
		V_11 = G_B23_0;
		Camera_t2727095145 * L_60 = V_3;
		Ray_t3134616544  L_61 = V_9;
		float L_62 = V_11;
		Camera_t2727095145 * L_63 = V_3;
		NullCheck(L_63);
		int32_t L_64 = Camera_get_cullingMask_m1045975289(L_63, /*hidden argument*/NULL);
		Camera_t2727095145 * L_65 = V_3;
		NullCheck(L_65);
		int32_t L_66 = Camera_get_eventMask_m3669132771(L_65, /*hidden argument*/NULL);
		NullCheck(L_60);
		GameObject_t3674682005 * L_67 = Camera_RaycastTry_m569221064(L_60, L_61, L_62, ((int32_t)((int32_t)L_64&(int32_t)L_66)), /*hidden argument*/NULL);
		V_12 = L_67;
		GameObject_t3674682005 * L_68 = V_12;
		bool L_69 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_68, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_69)
		{
			goto IL_01f0;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_70 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_70);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_70, 1);
		GameObject_t3674682005 * L_71 = V_12;
		((L_70)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_target_0(L_71);
		HitInfoU5BU5D_t3452915852* L_72 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_72);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_72, 1);
		Camera_t2727095145 * L_73 = V_3;
		((L_72)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_camera_1(L_73);
		goto IL_022a;
	}

IL_01f0:
	{
		Camera_t2727095145 * L_74 = V_3;
		NullCheck(L_74);
		int32_t L_75 = Camera_get_clearFlags_m192466552(L_74, /*hidden argument*/NULL);
		if ((((int32_t)L_75) == ((int32_t)1)))
		{
			goto IL_0208;
		}
	}
	{
		Camera_t2727095145 * L_76 = V_3;
		NullCheck(L_76);
		int32_t L_77 = Camera_get_clearFlags_m192466552(L_76, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_77) == ((uint32_t)2))))
		{
			goto IL_022a;
		}
	}

IL_0208:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_78 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_78);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_78, 1);
		((L_78)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_target_0((GameObject_t3674682005 *)NULL);
		HitInfoU5BU5D_t3452915852* L_79 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_79);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_79, 1);
		((L_79)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_camera_1((Camera_t2727095145 *)NULL);
	}

IL_022a:
	{
		Camera_t2727095145 * L_80 = V_3;
		Ray_t3134616544  L_81 = V_9;
		float L_82 = V_11;
		Camera_t2727095145 * L_83 = V_3;
		NullCheck(L_83);
		int32_t L_84 = Camera_get_cullingMask_m1045975289(L_83, /*hidden argument*/NULL);
		Camera_t2727095145 * L_85 = V_3;
		NullCheck(L_85);
		int32_t L_86 = Camera_get_eventMask_m3669132771(L_85, /*hidden argument*/NULL);
		NullCheck(L_80);
		GameObject_t3674682005 * L_87 = Camera_RaycastTry2D_m3256311322(L_80, L_81, L_82, ((int32_t)((int32_t)L_84&(int32_t)L_86)), /*hidden argument*/NULL);
		V_13 = L_87;
		GameObject_t3674682005 * L_88 = V_13;
		bool L_89 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_88, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_89)
		{
			goto IL_0278;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_90 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_90);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_90, 2);
		GameObject_t3674682005 * L_91 = V_13;
		((L_90)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_target_0(L_91);
		HitInfoU5BU5D_t3452915852* L_92 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_92);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_92, 2);
		Camera_t2727095145 * L_93 = V_3;
		((L_92)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_camera_1(L_93);
		goto IL_02b2;
	}

IL_0278:
	{
		Camera_t2727095145 * L_94 = V_3;
		NullCheck(L_94);
		int32_t L_95 = Camera_get_clearFlags_m192466552(L_94, /*hidden argument*/NULL);
		if ((((int32_t)L_95) == ((int32_t)1)))
		{
			goto IL_0290;
		}
	}
	{
		Camera_t2727095145 * L_96 = V_3;
		NullCheck(L_96);
		int32_t L_97 = Camera_get_clearFlags_m192466552(L_96, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_97) == ((uint32_t)2))))
		{
			goto IL_02b2;
		}
	}

IL_0290:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_98 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_98);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_98, 2);
		((L_98)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_target_0((GameObject_t3674682005 *)NULL);
		HitInfoU5BU5D_t3452915852* L_99 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_99);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_99, 2);
		((L_99)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_camera_1((Camera_t2727095145 *)NULL);
	}

IL_02b2:
	{
		int32_t L_100 = V_5;
		V_5 = ((int32_t)((int32_t)L_100+(int32_t)1));
	}

IL_02b8:
	{
		int32_t L_101 = V_5;
		CameraU5BU5D_t2716570836* L_102 = V_4;
		NullCheck(L_102);
		if ((((int32_t)L_101) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_102)->max_length)))))))
		{
			goto IL_0084;
		}
	}

IL_02c3:
	{
		V_14 = 0;
		goto IL_02e9;
	}

IL_02cb:
	{
		int32_t L_103 = V_14;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_104 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		int32_t L_105 = V_14;
		NullCheck(L_104);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_104, L_105);
		SendMouseEvents_SendEvents_m3877180750(NULL /*static, unused*/, L_103, (*(HitInfo_t3209134097 *)((L_104)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_105)))), /*hidden argument*/NULL);
		int32_t L_106 = V_14;
		V_14 = ((int32_t)((int32_t)L_106+(int32_t)1));
	}

IL_02e9:
	{
		int32_t L_107 = V_14;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_108 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_108);
		if ((((int32_t)L_107) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_108)->max_length)))))))
		{
			goto IL_02cb;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->set_s_MouseUsed_0((bool)0);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::SendEvents(System.Int32,UnityEngine.SendMouseEvents/HitInfo)
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppClass* SendMouseEvents_t3965481900_il2cpp_TypeInfo_var;
extern Il2CppClass* HitInfo_t3209134097_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2222972840;
extern Il2CppCodeGenString* _stringLiteral2438936645;
extern Il2CppCodeGenString* _stringLiteral288346913;
extern Il2CppCodeGenString* _stringLiteral2222975034;
extern Il2CppCodeGenString* _stringLiteral2223306714;
extern Il2CppCodeGenString* _stringLiteral2223010852;
extern Il2CppCodeGenString* _stringLiteral193571986;
extern const uint32_t SendMouseEvents_SendEvents_m3877180750_MetadataUsageId;
extern "C"  void SendMouseEvents_SendEvents_m3877180750 (Il2CppObject * __this /* static, unused */, int32_t ___i0, HitInfo_t3209134097  ___hit1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents_SendEvents_m3877180750_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	HitInfo_t3209134097  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetMouseButtonDown_m2031691843(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = Input_GetMouseButton_m4080958081(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_1 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_004a;
		}
	}
	{
		HitInfo_t3209134097  L_3 = ___hit1;
		bool L_4 = HitInfo_op_Implicit_m1943931337(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0045;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_5 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_6 = ___i0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		HitInfo_t3209134097  L_7 = ___hit1;
		(*(HitInfo_t3209134097 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))) = L_7;
		HitInfoU5BU5D_t3452915852* L_8 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_9 = ___i0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		HitInfo_SendMessage_m2569183060(((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9))), _stringLiteral2222972840, /*hidden argument*/NULL);
	}

IL_0045:
	{
		goto IL_00fc;
	}

IL_004a:
	{
		bool L_10 = V_1;
		if (L_10)
		{
			goto IL_00cd;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_11 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_12 = ___i0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		bool L_13 = HitInfo_op_Implicit_m1943931337(NULL /*static, unused*/, (*(HitInfo_t3209134097 *)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_12)))), /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00c8;
		}
	}
	{
		HitInfo_t3209134097  L_14 = ___hit1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_15 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_16 = ___i0;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		bool L_17 = HitInfo_Compare_m4083757090(NULL /*static, unused*/, L_14, (*(HitInfo_t3209134097 *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_16)))), /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_009a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_18 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_19 = ___i0;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		HitInfo_SendMessage_m2569183060(((L_18)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_19))), _stringLiteral2438936645, /*hidden argument*/NULL);
	}

IL_009a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_20 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_21 = ___i0;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		HitInfo_SendMessage_m2569183060(((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_21))), _stringLiteral288346913, /*hidden argument*/NULL);
		HitInfoU5BU5D_t3452915852* L_22 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_23 = ___i0;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_2));
		HitInfo_t3209134097  L_24 = V_2;
		(*(HitInfo_t3209134097 *)((L_22)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_23)))) = L_24;
	}

IL_00c8:
	{
		goto IL_00fc;
	}

IL_00cd:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_25 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_26 = ___i0;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_26);
		bool L_27 = HitInfo_op_Implicit_m1943931337(NULL /*static, unused*/, (*(HitInfo_t3209134097 *)((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_26)))), /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_00fc;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_28 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_29 = ___i0;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, L_29);
		HitInfo_SendMessage_m2569183060(((L_28)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_29))), _stringLiteral2222975034, /*hidden argument*/NULL);
	}

IL_00fc:
	{
		HitInfo_t3209134097  L_30 = ___hit1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_31 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_LastHit_1();
		int32_t L_32 = ___i0;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		bool L_33 = HitInfo_Compare_m4083757090(NULL /*static, unused*/, L_30, (*(HitInfo_t3209134097 *)((L_31)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_32)))), /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0133;
		}
	}
	{
		HitInfo_t3209134097  L_34 = ___hit1;
		bool L_35 = HitInfo_op_Implicit_m1943931337(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_012e;
		}
	}
	{
		HitInfo_SendMessage_m2569183060((&___hit1), _stringLiteral2223306714, /*hidden argument*/NULL);
	}

IL_012e:
	{
		goto IL_0185;
	}

IL_0133:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_36 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_LastHit_1();
		int32_t L_37 = ___i0;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_37);
		bool L_38 = HitInfo_op_Implicit_m1943931337(NULL /*static, unused*/, (*(HitInfo_t3209134097 *)((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_37)))), /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_0162;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_39 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_LastHit_1();
		int32_t L_40 = ___i0;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, L_40);
		HitInfo_SendMessage_m2569183060(((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_40))), _stringLiteral2223010852, /*hidden argument*/NULL);
	}

IL_0162:
	{
		HitInfo_t3209134097  L_41 = ___hit1;
		bool L_42 = HitInfo_op_Implicit_m1943931337(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_0185;
		}
	}
	{
		HitInfo_SendMessage_m2569183060((&___hit1), _stringLiteral193571986, /*hidden argument*/NULL);
		HitInfo_SendMessage_m2569183060((&___hit1), _stringLiteral2223306714, /*hidden argument*/NULL);
	}

IL_0185:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_43 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_LastHit_1();
		int32_t L_44 = ___i0;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, L_44);
		HitInfo_t3209134097  L_45 = ___hit1;
		(*(HitInfo_t3209134097 *)((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_44)))) = L_45;
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents/HitInfo::SendMessage(System.String)
extern "C"  void HitInfo_SendMessage_m2569183060 (HitInfo_t3209134097 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		GameObject_t3674682005 * L_0 = __this->get_target_0();
		String_t* L_1 = ___name0;
		NullCheck(L_0);
		GameObject_SendMessage_m423373689(L_0, L_1, NULL, 1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void HitInfo_SendMessage_m2569183060_AdjustorThunk (Il2CppObject * __this, String_t* ___name0, const MethodInfo* method)
{
	HitInfo_t3209134097 * _thisAdjusted = reinterpret_cast<HitInfo_t3209134097 *>(__this + 1);
	HitInfo_SendMessage_m2569183060(_thisAdjusted, ___name0, method);
}
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::Compare(UnityEngine.SendMouseEvents/HitInfo,UnityEngine.SendMouseEvents/HitInfo)
extern "C"  bool HitInfo_Compare_m4083757090 (Il2CppObject * __this /* static, unused */, HitInfo_t3209134097  ___lhs0, HitInfo_t3209134097  ___rhs1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		GameObject_t3674682005 * L_0 = (&___lhs0)->get_target_0();
		GameObject_t3674682005 * L_1 = (&___rhs1)->get_target_0();
		bool L_2 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		Camera_t2727095145 * L_3 = (&___lhs0)->get_camera_1();
		Camera_t2727095145 * L_4 = (&___rhs1)->get_camera_1();
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002e;
	}

IL_002d:
	{
		G_B3_0 = 0;
	}

IL_002e:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::op_Implicit(UnityEngine.SendMouseEvents/HitInfo)
extern "C"  bool HitInfo_op_Implicit_m1943931337 (Il2CppObject * __this /* static, unused */, HitInfo_t3209134097  ___exists0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		GameObject_t3674682005 * L_0 = (&___exists0)->get_target_0();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Camera_t2727095145 * L_2 = (&___exists0)->get_camera_1();
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return (bool)G_B3_0;
	}
}
// Conversion methods for marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t3209134097_marshal_pinvoke(const HitInfo_t3209134097& unmarshaled, HitInfo_t3209134097_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
extern "C" void HitInfo_t3209134097_marshal_pinvoke_back(const HitInfo_t3209134097_marshaled_pinvoke& marshaled, HitInfo_t3209134097& unmarshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t3209134097_marshal_pinvoke_cleanup(HitInfo_t3209134097_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t3209134097_marshal_com(const HitInfo_t3209134097& unmarshaled, HitInfo_t3209134097_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
extern "C" void HitInfo_t3209134097_marshal_com_back(const HitInfo_t3209134097_marshaled_com& marshaled, HitInfo_t3209134097& unmarshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t3209134097_marshal_com_cleanup(HitInfo_t3209134097_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
extern "C"  void FormerlySerializedAsAttribute__ctor_m2703462003 (FormerlySerializedAsAttribute_t2216353654 * __this, String_t* ___oldName0, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___oldName0;
		__this->set_m_oldName_0(L_0);
		return;
	}
}
// System.String UnityEngine.Serialization.FormerlySerializedAsAttribute::get_oldName()
extern "C"  String_t* FormerlySerializedAsAttribute_get_oldName_m2935479929 (FormerlySerializedAsAttribute_t2216353654 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_oldName_0();
		return L_0;
	}
}
// System.Void UnityEngine.SerializeField::.ctor()
extern "C"  void SerializeField__ctor_m4068807987 (SerializeField_t3754825534 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SerializePrivateVariables::.ctor()
extern "C"  void SerializePrivateVariables__ctor_m889466149 (SerializePrivateVariables_t2835496234 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SetupCoroutine::InvokeMoveNext(System.Collections.IEnumerator,System.IntPtr)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1859371625;
extern Il2CppCodeGenString* _stringLiteral2712312467;
extern const uint32_t SetupCoroutine_InvokeMoveNext_m3556339879_MetadataUsageId;
extern "C"  void SetupCoroutine_InvokeMoveNext_m3556339879 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___enumerator0, IntPtr_t ___returnValueAddress1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetupCoroutine_InvokeMoveNext_m3556339879_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___returnValueAddress1;
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_2 = IntPtr_op_Equality_m72843924(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		ArgumentException_t928607144 * L_3 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_3, _stringLiteral1859371625, _stringLiteral2712312467, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0020:
	{
		IntPtr_t L_4 = ___returnValueAddress1;
		void* L_5 = IntPtr_op_Explicit_m2322222010(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Il2CppObject * L_6 = ___enumerator0;
		NullCheck(L_6);
		bool L_7 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_6);
		*((int8_t*)(L_5)) = (int8_t)L_7;
		return;
	}
}
// System.Object UnityEngine.SetupCoroutine::InvokeMember(System.Object,System.String,System.Object)
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t SetupCoroutine_InvokeMember_m3691215301_MetadataUsageId;
extern "C"  Il2CppObject * SetupCoroutine_InvokeMember_m3691215301 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___behaviour0, String_t* ___name1, Il2CppObject * ___variable2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetupCoroutine_InvokeMember_m3691215301_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t1108656482* V_0 = NULL;
	{
		V_0 = (ObjectU5BU5D_t1108656482*)NULL;
		Il2CppObject * L_0 = ___variable2;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		V_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		ObjectU5BU5D_t1108656482* L_1 = V_0;
		Il2CppObject * L_2 = ___variable2;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_2);
	}

IL_0013:
	{
		Il2CppObject * L_3 = ___behaviour0;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m2022236990(L_3, /*hidden argument*/NULL);
		String_t* L_5 = ___name1;
		Il2CppObject * L_6 = ___behaviour0;
		ObjectU5BU5D_t1108656482* L_7 = V_0;
		NullCheck(L_4);
		Il2CppObject * L_8 = VirtFuncInvoker8< Il2CppObject *, String_t*, int32_t, Binder_t1074302268 *, Il2CppObject *, ObjectU5BU5D_t1108656482*, ParameterModifierU5BU5D_t3896472559*, CultureInfo_t1065375142 *, StringU5BU5D_t4054002952* >::Invoke(80 /* System.Object System.Type::InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[],System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[]) */, L_4, L_5, ((int32_t)308), (Binder_t1074302268 *)NULL, L_6, L_7, (ParameterModifierU5BU5D_t3896472559*)(ParameterModifierU5BU5D_t3896472559*)NULL, (CultureInfo_t1065375142 *)NULL, (StringU5BU5D_t4054002952*)(StringU5BU5D_t4054002952*)NULL);
		return L_8;
	}
}
// UnityEngine.Shader UnityEngine.Shader::Find(System.String)
extern "C"  Shader_t3191267369 * Shader_Find_m4048047578 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	typedef Shader_t3191267369 * (*Shader_Find_m4048047578_ftn) (String_t*);
	static Shader_Find_m4048047578_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_Find_m4048047578_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::Find(System.String)");
	return _il2cpp_icall_func(___name0);
}
// System.Int32 UnityEngine.Shader::PropertyToID(System.String)
extern "C"  int32_t Shader_PropertyToID_m3019342011 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	typedef int32_t (*Shader_PropertyToID_m3019342011_ftn) (String_t*);
	static Shader_PropertyToID_m3019342011_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_PropertyToID_m3019342011_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::PropertyToID(System.String)");
	return _il2cpp_icall_func(___name0);
}
// System.Void UnityEngine.SharedBetweenAnimatorsAttribute::.ctor()
extern "C"  void SharedBetweenAnimatorsAttribute__ctor_m2764338918 (SharedBetweenAnimatorsAttribute_t1486273289 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t421858229_marshal_pinvoke(const SkeletonBone_t421858229& unmarshaled, SkeletonBone_t421858229_marshaled_pinvoke& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_string(unmarshaled.get_name_0());
	Vector3_t4282066566_marshal_pinvoke(unmarshaled.get_position_1(), marshaled.___position_1);
	Quaternion_t1553702882_marshal_pinvoke(unmarshaled.get_rotation_2(), marshaled.___rotation_2);
	Vector3_t4282066566_marshal_pinvoke(unmarshaled.get_scale_3(), marshaled.___scale_3);
	marshaled.___transformModified_4 = unmarshaled.get_transformModified_4();
}
extern "C" void SkeletonBone_t421858229_marshal_pinvoke_back(const SkeletonBone_t421858229_marshaled_pinvoke& marshaled, SkeletonBone_t421858229& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_string_result(marshaled.___name_0));
	Vector3_t4282066566  unmarshaled_position_temp_1;
	memset(&unmarshaled_position_temp_1, 0, sizeof(unmarshaled_position_temp_1));
	Vector3_t4282066566_marshal_pinvoke_back(marshaled.___position_1, unmarshaled_position_temp_1);
	unmarshaled.set_position_1(unmarshaled_position_temp_1);
	Quaternion_t1553702882  unmarshaled_rotation_temp_2;
	memset(&unmarshaled_rotation_temp_2, 0, sizeof(unmarshaled_rotation_temp_2));
	Quaternion_t1553702882_marshal_pinvoke_back(marshaled.___rotation_2, unmarshaled_rotation_temp_2);
	unmarshaled.set_rotation_2(unmarshaled_rotation_temp_2);
	Vector3_t4282066566  unmarshaled_scale_temp_3;
	memset(&unmarshaled_scale_temp_3, 0, sizeof(unmarshaled_scale_temp_3));
	Vector3_t4282066566_marshal_pinvoke_back(marshaled.___scale_3, unmarshaled_scale_temp_3);
	unmarshaled.set_scale_3(unmarshaled_scale_temp_3);
	int32_t unmarshaled_transformModified_temp_4 = 0;
	unmarshaled_transformModified_temp_4 = marshaled.___transformModified_4;
	unmarshaled.set_transformModified_4(unmarshaled_transformModified_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t421858229_marshal_pinvoke_cleanup(SkeletonBone_t421858229_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___name_0);
	marshaled.___name_0 = NULL;
	Vector3_t4282066566_marshal_pinvoke_cleanup(marshaled.___position_1);
	Quaternion_t1553702882_marshal_pinvoke_cleanup(marshaled.___rotation_2);
	Vector3_t4282066566_marshal_pinvoke_cleanup(marshaled.___scale_3);
}
// Conversion methods for marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t421858229_marshal_com(const SkeletonBone_t421858229& unmarshaled, SkeletonBone_t421858229_marshaled_com& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_name_0());
	Vector3_t4282066566_marshal_com(unmarshaled.get_position_1(), marshaled.___position_1);
	Quaternion_t1553702882_marshal_com(unmarshaled.get_rotation_2(), marshaled.___rotation_2);
	Vector3_t4282066566_marshal_com(unmarshaled.get_scale_3(), marshaled.___scale_3);
	marshaled.___transformModified_4 = unmarshaled.get_transformModified_4();
}
extern "C" void SkeletonBone_t421858229_marshal_com_back(const SkeletonBone_t421858229_marshaled_com& marshaled, SkeletonBone_t421858229& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_bstring_result(marshaled.___name_0));
	Vector3_t4282066566  unmarshaled_position_temp_1;
	memset(&unmarshaled_position_temp_1, 0, sizeof(unmarshaled_position_temp_1));
	Vector3_t4282066566_marshal_com_back(marshaled.___position_1, unmarshaled_position_temp_1);
	unmarshaled.set_position_1(unmarshaled_position_temp_1);
	Quaternion_t1553702882  unmarshaled_rotation_temp_2;
	memset(&unmarshaled_rotation_temp_2, 0, sizeof(unmarshaled_rotation_temp_2));
	Quaternion_t1553702882_marshal_com_back(marshaled.___rotation_2, unmarshaled_rotation_temp_2);
	unmarshaled.set_rotation_2(unmarshaled_rotation_temp_2);
	Vector3_t4282066566  unmarshaled_scale_temp_3;
	memset(&unmarshaled_scale_temp_3, 0, sizeof(unmarshaled_scale_temp_3));
	Vector3_t4282066566_marshal_com_back(marshaled.___scale_3, unmarshaled_scale_temp_3);
	unmarshaled.set_scale_3(unmarshaled_scale_temp_3);
	int32_t unmarshaled_transformModified_temp_4 = 0;
	unmarshaled_transformModified_temp_4 = marshaled.___transformModified_4;
	unmarshaled.set_transformModified_4(unmarshaled_transformModified_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t421858229_marshal_com_cleanup(SkeletonBone_t421858229_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___name_0);
	marshaled.___name_0 = NULL;
	Vector3_t4282066566_marshal_com_cleanup(marshaled.___position_1);
	Quaternion_t1553702882_marshal_com_cleanup(marshaled.___rotation_2);
	Vector3_t4282066566_marshal_com_cleanup(marshaled.___scale_3);
}
// System.Void UnityEngine.SliderHandler::.ctor(UnityEngine.Rect,System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUIStyle,System.Boolean,System.Int32)
extern "C"  void SliderHandler__ctor_m4217573891 (SliderHandler_t783692703 * __this, Rect_t4241904616  ___position0, float ___currentValue1, float ___size2, float ___start3, float ___end4, GUIStyle_t2990928826 * ___slider5, GUIStyle_t2990928826 * ___thumb6, bool ___horiz7, int32_t ___id8, const MethodInfo* method)
{
	{
		Rect_t4241904616  L_0 = ___position0;
		__this->set_position_0(L_0);
		float L_1 = ___currentValue1;
		__this->set_currentValue_1(L_1);
		float L_2 = ___size2;
		__this->set_size_2(L_2);
		float L_3 = ___start3;
		__this->set_start_3(L_3);
		float L_4 = ___end4;
		__this->set_end_4(L_4);
		GUIStyle_t2990928826 * L_5 = ___slider5;
		__this->set_slider_5(L_5);
		GUIStyle_t2990928826 * L_6 = ___thumb6;
		__this->set_thumb_6(L_6);
		bool L_7 = ___horiz7;
		__this->set_horiz_7(L_7);
		int32_t L_8 = ___id8;
		__this->set_id_8(L_8);
		return;
	}
}
extern "C"  void SliderHandler__ctor_m4217573891_AdjustorThunk (Il2CppObject * __this, Rect_t4241904616  ___position0, float ___currentValue1, float ___size2, float ___start3, float ___end4, GUIStyle_t2990928826 * ___slider5, GUIStyle_t2990928826 * ___thumb6, bool ___horiz7, int32_t ___id8, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	SliderHandler__ctor_m4217573891(_thisAdjusted, ___position0, ___currentValue1, ___size2, ___start3, ___end4, ___slider5, ___thumb6, ___horiz7, ___id8, method);
}
// System.Single UnityEngine.SliderHandler::Handle()
extern "C"  float SliderHandler_Handle_m2409586512 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		GUIStyle_t2990928826 * L_0 = __this->get_slider_5();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		GUIStyle_t2990928826 * L_1 = __this->get_thumb_6();
		if (L_1)
		{
			goto IL_001d;
		}
	}

IL_0016:
	{
		float L_2 = __this->get_currentValue_1();
		return L_2;
	}

IL_001d:
	{
		int32_t L_3 = SliderHandler_CurrentEventType_m3978233689(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if (L_4 == 0)
		{
			goto IL_004f;
		}
		if (L_4 == 1)
		{
			goto IL_005d;
		}
		if (L_4 == 2)
		{
			goto IL_006b;
		}
		if (L_4 == 3)
		{
			goto IL_0056;
		}
		if (L_4 == 4)
		{
			goto IL_006b;
		}
		if (L_4 == 5)
		{
			goto IL_006b;
		}
		if (L_4 == 6)
		{
			goto IL_006b;
		}
		if (L_4 == 7)
		{
			goto IL_0064;
		}
	}
	{
		goto IL_006b;
	}

IL_004f:
	{
		float L_5 = SliderHandler_OnMouseDown_m3958002946(__this, /*hidden argument*/NULL);
		return L_5;
	}

IL_0056:
	{
		float L_6 = SliderHandler_OnMouseDrag_m3960111380(__this, /*hidden argument*/NULL);
		return L_6;
	}

IL_005d:
	{
		float L_7 = SliderHandler_OnMouseUp_m1318588539(__this, /*hidden argument*/NULL);
		return L_7;
	}

IL_0064:
	{
		float L_8 = SliderHandler_OnRepaint_m4264550822(__this, /*hidden argument*/NULL);
		return L_8;
	}

IL_006b:
	{
		float L_9 = __this->get_currentValue_1();
		return L_9;
	}
}
extern "C"  float SliderHandler_Handle_m2409586512_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_Handle_m2409586512(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::OnMouseDown()
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIUtility_t1028319349_il2cpp_TypeInfo_var;
extern Il2CppClass* SystemClock_t4036018645_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_OnMouseDown_m3958002946_MetadataUsageId;
extern "C"  float SliderHandler_OnMouseDown_m3958002946 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_OnMouseDown_m3958002946_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	Rect_t4241904616  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t4241904616  V_2;
	memset(&V_2, 0, sizeof(V_2));
	DateTime_t4283661327  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Rect_t4241904616  L_0 = __this->get_position_0();
		V_1 = L_0;
		Event_t4196595728 * L_1 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector2_t4282066565  L_2 = Event_get_mousePosition_m3610425949(L_1, /*hidden argument*/NULL);
		bool L_3 = Rect_Contains_m3556594010((&V_1), L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		bool L_4 = SliderHandler_IsEmptySlider_m2523580504(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0030;
		}
	}

IL_0029:
	{
		float L_5 = __this->get_currentValue_1();
		return L_5;
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_scrollTroughSide_m1228634973(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		int32_t L_6 = __this->get_id_8();
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		GUIUtility_set_hotControl_m300477798(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Event_t4196595728 * L_7 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Event_Use_m310777444(L_7, /*hidden argument*/NULL);
		Rect_t4241904616  L_8 = SliderHandler_ThumbSelectionRect_m3697323546(__this, /*hidden argument*/NULL);
		V_2 = L_8;
		Event_t4196595728 * L_9 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector2_t4282066565  L_10 = Event_get_mousePosition_m3610425949(L_9, /*hidden argument*/NULL);
		bool L_11 = Rect_Contains_m3556594010((&V_2), L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_007d;
		}
	}
	{
		float L_12 = SliderHandler_ClampedCurrentValue_m3006511340(__this, /*hidden argument*/NULL);
		SliderHandler_StartDraggingWithValue_m3874943933(__this, L_12, /*hidden argument*/NULL);
		float L_13 = __this->get_currentValue_1();
		return L_13;
	}

IL_007d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_changed_m727947722(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		bool L_14 = SliderHandler_SupportsPageMovements_m4216612037(__this, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00c7;
		}
	}
	{
		SliderState_t1233388262 * L_15 = SliderHandler_SliderState_m1456191352(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		L_15->set_isDragging_2((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(SystemClock_t4036018645_il2cpp_TypeInfo_var);
		DateTime_t4283661327  L_16 = SystemClock_get_now_m175136990(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_16;
		DateTime_t4283661327  L_17 = DateTime_AddMilliseconds_m1717403134((&V_3), (250.0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_nextScrollStepTime_m3820512796(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		int32_t L_18 = SliderHandler_CurrentScrollTroughSide_m1606169264(__this, /*hidden argument*/NULL);
		GUI_set_scrollTroughSide_m1228634973(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		float L_19 = SliderHandler_PageMovementValue_m2660885357(__this, /*hidden argument*/NULL);
		return L_19;
	}

IL_00c7:
	{
		float L_20 = SliderHandler_ValueForCurrentMousePosition_m493574741(__this, /*hidden argument*/NULL);
		V_0 = L_20;
		float L_21 = V_0;
		SliderHandler_StartDraggingWithValue_m3874943933(__this, L_21, /*hidden argument*/NULL);
		float L_22 = V_0;
		float L_23 = SliderHandler_Clamp_m4218954710(__this, L_22, /*hidden argument*/NULL);
		return L_23;
	}
}
extern "C"  float SliderHandler_OnMouseDown_m3958002946_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_OnMouseDown_m3958002946(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::OnMouseDrag()
extern Il2CppClass* GUIUtility_t1028319349_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_OnMouseDrag_m3960111380_MetadataUsageId;
extern "C"  float SliderHandler_OnMouseDrag_m3960111380 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_OnMouseDrag_m3960111380_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SliderState_t1233388262 * V_0 = NULL;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		int32_t L_0 = GUIUtility_get_hotControl_m4135893409(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_id_8();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0017;
		}
	}
	{
		float L_2 = __this->get_currentValue_1();
		return L_2;
	}

IL_0017:
	{
		SliderState_t1233388262 * L_3 = SliderHandler_SliderState_m1456191352(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		SliderState_t1233388262 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = L_4->get_isDragging_2();
		if (L_5)
		{
			goto IL_0030;
		}
	}
	{
		float L_6 = __this->get_currentValue_1();
		return L_6;
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_changed_m727947722(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		Event_t4196595728 * L_7 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Event_Use_m310777444(L_7, /*hidden argument*/NULL);
		float L_8 = SliderHandler_MousePosition_m303335016(__this, /*hidden argument*/NULL);
		SliderState_t1233388262 * L_9 = V_0;
		NullCheck(L_9);
		float L_10 = L_9->get_dragStartPos_0();
		V_1 = ((float)((float)L_8-(float)L_10));
		SliderState_t1233388262 * L_11 = V_0;
		NullCheck(L_11);
		float L_12 = L_11->get_dragStartValue_1();
		float L_13 = V_1;
		float L_14 = SliderHandler_ValuesPerPixel_m41869331(__this, /*hidden argument*/NULL);
		V_2 = ((float)((float)L_12+(float)((float)((float)L_13/(float)L_14))));
		float L_15 = V_2;
		float L_16 = SliderHandler_Clamp_m4218954710(__this, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
extern "C"  float SliderHandler_OnMouseDrag_m3960111380_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_OnMouseDrag_m3960111380(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::OnMouseUp()
extern Il2CppClass* GUIUtility_t1028319349_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_OnMouseUp_m1318588539_MetadataUsageId;
extern "C"  float SliderHandler_OnMouseUp_m1318588539 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_OnMouseUp_m1318588539_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		int32_t L_0 = GUIUtility_get_hotControl_m4135893409(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_id_8();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0021;
		}
	}
	{
		Event_t4196595728 * L_2 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Event_Use_m310777444(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		GUIUtility_set_hotControl_m300477798(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_0021:
	{
		float L_3 = __this->get_currentValue_1();
		return L_3;
	}
}
extern "C"  float SliderHandler_OnMouseUp_m1318588539_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_OnMouseUp_m1318588539(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::OnRepaint()
extern Il2CppClass* GUIContent_t2094828418_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIUtility_t1028319349_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppClass* SystemClock_t4036018645_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_OnRepaint_m4264550822_MetadataUsageId;
extern "C"  float SliderHandler_OnRepaint_m4264550822 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_OnRepaint_m4264550822_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t4241904616  V_1;
	memset(&V_1, 0, sizeof(V_1));
	DateTime_t4283661327  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		GUIStyle_t2990928826 * L_0 = __this->get_slider_5();
		Rect_t4241904616  L_1 = __this->get_position_0();
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t2094828418_il2cpp_TypeInfo_var);
		GUIContent_t2094828418 * L_2 = ((GUIContent_t2094828418_StaticFields*)GUIContent_t2094828418_il2cpp_TypeInfo_var->static_fields)->get_none_6();
		int32_t L_3 = __this->get_id_8();
		NullCheck(L_0);
		GUIStyle_Draw_m2994577084(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		bool L_4 = SliderHandler_IsEmptySlider_m2523580504(__this, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0043;
		}
	}
	{
		GUIStyle_t2990928826 * L_5 = __this->get_thumb_6();
		Rect_t4241904616  L_6 = SliderHandler_ThumbRect_m1881961532(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t2094828418_il2cpp_TypeInfo_var);
		GUIContent_t2094828418 * L_7 = ((GUIContent_t2094828418_StaticFields*)GUIContent_t2094828418_il2cpp_TypeInfo_var->static_fields)->get_none_6();
		int32_t L_8 = __this->get_id_8();
		NullCheck(L_5);
		GUIStyle_Draw_m2994577084(L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
	}

IL_0043:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		int32_t L_9 = GUIUtility_get_hotControl_m4135893409(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_10 = __this->get_id_8();
		if ((!(((uint32_t)L_9) == ((uint32_t)L_10))))
		{
			goto IL_007c;
		}
	}
	{
		Rect_t4241904616  L_11 = __this->get_position_0();
		V_0 = L_11;
		Event_t4196595728 * L_12 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector2_t4282066565  L_13 = Event_get_mousePosition_m3610425949(L_12, /*hidden argument*/NULL);
		bool L_14 = Rect_Contains_m3556594010((&V_0), L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_007c;
		}
	}
	{
		bool L_15 = SliderHandler_IsEmptySlider_m2523580504(__this, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0083;
		}
	}

IL_007c:
	{
		float L_16 = __this->get_currentValue_1();
		return L_16;
	}

IL_0083:
	{
		Rect_t4241904616  L_17 = SliderHandler_ThumbRect_m1881961532(__this, /*hidden argument*/NULL);
		V_1 = L_17;
		Event_t4196595728 * L_18 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector2_t4282066565  L_19 = Event_get_mousePosition_m3610425949(L_18, /*hidden argument*/NULL);
		bool L_20 = Rect_Contains_m3556594010((&V_1), L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00b8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		int32_t L_21 = GUI_get_scrollTroughSide_m3369891864(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00b1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		GUIUtility_set_hotControl_m300477798(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_00b1:
	{
		float L_22 = __this->get_currentValue_1();
		return L_22;
	}

IL_00b8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_InternalRepaintEditorWindow_m3223206407(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SystemClock_t4036018645_il2cpp_TypeInfo_var);
		DateTime_t4283661327  L_23 = SystemClock_get_now_m175136990(NULL /*static, unused*/, /*hidden argument*/NULL);
		DateTime_t4283661327  L_24 = GUI_get_nextScrollStepTime_m719800559(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t4283661327_il2cpp_TypeInfo_var);
		bool L_25 = DateTime_op_LessThan_m35073816(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00d8;
		}
	}
	{
		float L_26 = __this->get_currentValue_1();
		return L_26;
	}

IL_00d8:
	{
		int32_t L_27 = SliderHandler_CurrentScrollTroughSide_m1606169264(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		int32_t L_28 = GUI_get_scrollTroughSide_m3369891864(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_27) == ((int32_t)L_28)))
		{
			goto IL_00ef;
		}
	}
	{
		float L_29 = __this->get_currentValue_1();
		return L_29;
	}

IL_00ef:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SystemClock_t4036018645_il2cpp_TypeInfo_var);
		DateTime_t4283661327  L_30 = SystemClock_get_now_m175136990(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_30;
		DateTime_t4283661327  L_31 = DateTime_AddMilliseconds_m1717403134((&V_2), (30.0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_nextScrollStepTime_m3820512796(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		bool L_32 = SliderHandler_SupportsPageMovements_m4216612037(__this, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_012e;
		}
	}
	{
		SliderState_t1233388262 * L_33 = SliderHandler_SliderState_m1456191352(__this, /*hidden argument*/NULL);
		NullCheck(L_33);
		L_33->set_isDragging_2((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_changed_m727947722(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		float L_34 = SliderHandler_PageMovementValue_m2660885357(__this, /*hidden argument*/NULL);
		return L_34;
	}

IL_012e:
	{
		float L_35 = SliderHandler_ClampedCurrentValue_m3006511340(__this, /*hidden argument*/NULL);
		return L_35;
	}
}
extern "C"  float SliderHandler_OnRepaint_m4264550822_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_OnRepaint_m4264550822(_thisAdjusted, method);
}
// UnityEngine.EventType UnityEngine.SliderHandler::CurrentEventType()
extern "C"  int32_t SliderHandler_CurrentEventType_m3978233689 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	{
		Event_t4196595728 * L_0 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_id_8();
		NullCheck(L_0);
		int32_t L_2 = Event_GetTypeForControl_m854773288(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t SliderHandler_CurrentEventType_m3978233689_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_CurrentEventType_m3978233689(_thisAdjusted, method);
}
// System.Int32 UnityEngine.SliderHandler::CurrentScrollTroughSide()
extern "C"  int32_t SliderHandler_CurrentScrollTroughSide_m1606169264 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Vector2_t4282066565  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t4282066565  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t4241904616  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Rect_t4241904616  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float G_B3_0 = 0.0f;
	float G_B6_0 = 0.0f;
	int32_t G_B9_0 = 0;
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_0023;
		}
	}
	{
		Event_t4196595728 * L_1 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector2_t4282066565  L_2 = Event_get_mousePosition_m3610425949(L_1, /*hidden argument*/NULL);
		V_2 = L_2;
		float L_3 = (&V_2)->get_x_1();
		G_B3_0 = L_3;
		goto IL_0036;
	}

IL_0023:
	{
		Event_t4196595728 * L_4 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector2_t4282066565  L_5 = Event_get_mousePosition_m3610425949(L_4, /*hidden argument*/NULL);
		V_3 = L_5;
		float L_6 = (&V_3)->get_y_2();
		G_B3_0 = L_6;
	}

IL_0036:
	{
		V_0 = G_B3_0;
		bool L_7 = __this->get_horiz_7();
		if (!L_7)
		{
			goto IL_0056;
		}
	}
	{
		Rect_t4241904616  L_8 = SliderHandler_ThumbRect_m1881961532(__this, /*hidden argument*/NULL);
		V_4 = L_8;
		float L_9 = Rect_get_x_m982385354((&V_4), /*hidden argument*/NULL);
		G_B6_0 = L_9;
		goto IL_0065;
	}

IL_0056:
	{
		Rect_t4241904616  L_10 = SliderHandler_ThumbRect_m1881961532(__this, /*hidden argument*/NULL);
		V_5 = L_10;
		float L_11 = Rect_get_y_m982386315((&V_5), /*hidden argument*/NULL);
		G_B6_0 = L_11;
	}

IL_0065:
	{
		V_1 = G_B6_0;
		float L_12 = V_0;
		float L_13 = V_1;
		if ((!(((float)L_12) > ((float)L_13))))
		{
			goto IL_0073;
		}
	}
	{
		G_B9_0 = 1;
		goto IL_0074;
	}

IL_0073:
	{
		G_B9_0 = (-1);
	}

IL_0074:
	{
		return G_B9_0;
	}
}
extern "C"  int32_t SliderHandler_CurrentScrollTroughSide_m1606169264_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_CurrentScrollTroughSide_m1606169264(_thisAdjusted, method);
}
// System.Boolean UnityEngine.SliderHandler::IsEmptySlider()
extern "C"  bool SliderHandler_IsEmptySlider_m2523580504 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_start_3();
		float L_1 = __this->get_end_4();
		return (bool)((((float)L_0) == ((float)L_1))? 1 : 0);
	}
}
extern "C"  bool SliderHandler_IsEmptySlider_m2523580504_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_IsEmptySlider_m2523580504(_thisAdjusted, method);
}
// System.Boolean UnityEngine.SliderHandler::SupportsPageMovements()
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_SupportsPageMovements_m4216612037_MetadataUsageId;
extern "C"  bool SliderHandler_SupportsPageMovements_m4216612037 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_SupportsPageMovements_m4216612037_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		float L_0 = __this->get_size_2();
		if ((((float)L_0) == ((float)(0.0f))))
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		bool L_1 = GUI_get_usePageScrollbars_m944581596(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_1));
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = 0;
	}

IL_0018:
	{
		return (bool)G_B3_0;
	}
}
extern "C"  bool SliderHandler_SupportsPageMovements_m4216612037_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_SupportsPageMovements_m4216612037(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::PageMovementValue()
extern "C"  float SliderHandler_PageMovementValue_m2660885357 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	int32_t G_B3_0 = 0;
	{
		float L_0 = __this->get_currentValue_1();
		V_0 = L_0;
		float L_1 = __this->get_start_3();
		float L_2 = __this->get_end_4();
		if ((!(((float)L_1) > ((float)L_2))))
		{
			goto IL_001e;
		}
	}
	{
		G_B3_0 = (-1);
		goto IL_001f;
	}

IL_001e:
	{
		G_B3_0 = 1;
	}

IL_001f:
	{
		V_1 = G_B3_0;
		float L_3 = SliderHandler_MousePosition_m303335016(__this, /*hidden argument*/NULL);
		float L_4 = SliderHandler_PageUpMovementBound_m2543741343(__this, /*hidden argument*/NULL);
		if ((!(((float)L_3) > ((float)L_4))))
		{
			goto IL_0048;
		}
	}
	{
		float L_5 = V_0;
		float L_6 = __this->get_size_2();
		int32_t L_7 = V_1;
		V_0 = ((float)((float)L_5+(float)((float)((float)((float)((float)L_6*(float)(((float)((float)L_7)))))*(float)(0.9f)))));
		goto IL_005a;
	}

IL_0048:
	{
		float L_8 = V_0;
		float L_9 = __this->get_size_2();
		int32_t L_10 = V_1;
		V_0 = ((float)((float)L_8-(float)((float)((float)((float)((float)L_9*(float)(((float)((float)L_10)))))*(float)(0.9f)))));
	}

IL_005a:
	{
		float L_11 = V_0;
		float L_12 = SliderHandler_Clamp_m4218954710(__this, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
extern "C"  float SliderHandler_PageMovementValue_m2660885357_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_PageMovementValue_m2660885357(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::PageUpMovementBound()
extern "C"  float SliderHandler_PageUpMovementBound_m2543741343 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t4241904616  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t4241904616  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t4241904616  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		Rect_t4241904616  L_1 = SliderHandler_ThumbRect_m1881961532(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = Rect_get_xMax_m370881244((&V_0), /*hidden argument*/NULL);
		Rect_t4241904616  L_3 = __this->get_position_0();
		V_1 = L_3;
		float L_4 = Rect_get_x_m982385354((&V_1), /*hidden argument*/NULL);
		return ((float)((float)L_2-(float)L_4));
	}

IL_0029:
	{
		Rect_t4241904616  L_5 = SliderHandler_ThumbRect_m1881961532(__this, /*hidden argument*/NULL);
		V_2 = L_5;
		float L_6 = Rect_get_yMax_m399510395((&V_2), /*hidden argument*/NULL);
		Rect_t4241904616  L_7 = __this->get_position_0();
		V_3 = L_7;
		float L_8 = Rect_get_y_m982386315((&V_3), /*hidden argument*/NULL);
		return ((float)((float)L_6-(float)L_8));
	}
}
extern "C"  float SliderHandler_PageUpMovementBound_m2543741343_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_PageUpMovementBound_m2543741343(_thisAdjusted, method);
}
// UnityEngine.Event UnityEngine.SliderHandler::CurrentEvent()
extern "C"  Event_t4196595728 * SliderHandler_CurrentEvent_m721596197 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	{
		Event_t4196595728 * L_0 = Event_get_current_m238587645(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  Event_t4196595728 * SliderHandler_CurrentEvent_m721596197_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_CurrentEvent_m721596197(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::ValueForCurrentMousePosition()
extern "C"  float SliderHandler_ValueForCurrentMousePosition_m493574741 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t4241904616  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_0042;
		}
	}
	{
		float L_1 = SliderHandler_MousePosition_m303335016(__this, /*hidden argument*/NULL);
		Rect_t4241904616  L_2 = SliderHandler_ThumbRect_m1881961532(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_width_m2824209432((&V_0), /*hidden argument*/NULL);
		float L_4 = SliderHandler_ValuesPerPixel_m41869331(__this, /*hidden argument*/NULL);
		float L_5 = __this->get_start_3();
		float L_6 = __this->get_size_2();
		return ((float)((float)((float)((float)((float)((float)((float)((float)L_1-(float)((float)((float)L_3*(float)(0.5f)))))/(float)L_4))+(float)L_5))-(float)((float)((float)L_6*(float)(0.5f)))));
	}

IL_0042:
	{
		float L_7 = SliderHandler_MousePosition_m303335016(__this, /*hidden argument*/NULL);
		Rect_t4241904616  L_8 = SliderHandler_ThumbRect_m1881961532(__this, /*hidden argument*/NULL);
		V_1 = L_8;
		float L_9 = Rect_get_height_m2154960823((&V_1), /*hidden argument*/NULL);
		float L_10 = SliderHandler_ValuesPerPixel_m41869331(__this, /*hidden argument*/NULL);
		float L_11 = __this->get_start_3();
		float L_12 = __this->get_size_2();
		return ((float)((float)((float)((float)((float)((float)((float)((float)L_7-(float)((float)((float)L_9*(float)(0.5f)))))/(float)L_10))+(float)L_11))-(float)((float)((float)L_12*(float)(0.5f)))));
	}
}
extern "C"  float SliderHandler_ValueForCurrentMousePosition_m493574741_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_ValueForCurrentMousePosition_m493574741(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::Clamp(System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_Clamp_m4218954710_MetadataUsageId;
extern "C"  float SliderHandler_Clamp_m4218954710 (SliderHandler_t783692703 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_Clamp_m4218954710_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___value0;
		float L_1 = SliderHandler_MinValue_m2516678631(__this, /*hidden argument*/NULL);
		float L_2 = SliderHandler_MaxValue_m44679317(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
extern "C"  float SliderHandler_Clamp_m4218954710_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_Clamp_m4218954710(_thisAdjusted, ___value0, method);
}
// UnityEngine.Rect UnityEngine.SliderHandler::ThumbSelectionRect()
extern "C"  Rect_t4241904616  SliderHandler_ThumbSelectionRect_m3697323546 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		Rect_t4241904616  L_0 = SliderHandler_ThumbRect_m1881961532(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = ((int32_t)12);
		float L_1 = Rect_get_width_m2824209432((&V_0), /*hidden argument*/NULL);
		int32_t L_2 = V_1;
		if ((!(((float)L_1) < ((float)(((float)((float)L_2)))))))
		{
			goto IL_003f;
		}
	}
	{
		Rect_t4241904616 * L_3 = (&V_0);
		float L_4 = Rect_get_x_m982385354(L_3, /*hidden argument*/NULL);
		int32_t L_5 = V_1;
		float L_6 = Rect_get_width_m2824209432((&V_0), /*hidden argument*/NULL);
		Rect_set_x_m577970569(L_3, ((float)((float)L_4-(float)((float)((float)((float)((float)(((float)((float)L_5)))-(float)L_6))/(float)(2.0f))))), /*hidden argument*/NULL);
		int32_t L_7 = V_1;
		Rect_set_width_m3771513595((&V_0), (((float)((float)L_7))), /*hidden argument*/NULL);
	}

IL_003f:
	{
		float L_8 = Rect_get_height_m2154960823((&V_0), /*hidden argument*/NULL);
		int32_t L_9 = V_1;
		if ((!(((float)L_8) < ((float)(((float)((float)L_9)))))))
		{
			goto IL_0074;
		}
	}
	{
		Rect_t4241904616 * L_10 = (&V_0);
		float L_11 = Rect_get_y_m982386315(L_10, /*hidden argument*/NULL);
		int32_t L_12 = V_1;
		float L_13 = Rect_get_height_m2154960823((&V_0), /*hidden argument*/NULL);
		Rect_set_y_m67436392(L_10, ((float)((float)L_11-(float)((float)((float)((float)((float)(((float)((float)L_12)))-(float)L_13))/(float)(2.0f))))), /*hidden argument*/NULL);
		int32_t L_14 = V_1;
		Rect_set_height_m3398820332((&V_0), (((float)((float)L_14))), /*hidden argument*/NULL);
	}

IL_0074:
	{
		Rect_t4241904616  L_15 = V_0;
		return L_15;
	}
}
extern "C"  Rect_t4241904616  SliderHandler_ThumbSelectionRect_m3697323546_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_ThumbSelectionRect_m3697323546(_thisAdjusted, method);
}
// System.Void UnityEngine.SliderHandler::StartDraggingWithValue(System.Single)
extern "C"  void SliderHandler_StartDraggingWithValue_m3874943933 (SliderHandler_t783692703 * __this, float ___dragStartValue0, const MethodInfo* method)
{
	SliderState_t1233388262 * V_0 = NULL;
	{
		SliderState_t1233388262 * L_0 = SliderHandler_SliderState_m1456191352(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		SliderState_t1233388262 * L_1 = V_0;
		float L_2 = SliderHandler_MousePosition_m303335016(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->set_dragStartPos_0(L_2);
		SliderState_t1233388262 * L_3 = V_0;
		float L_4 = ___dragStartValue0;
		NullCheck(L_3);
		L_3->set_dragStartValue_1(L_4);
		SliderState_t1233388262 * L_5 = V_0;
		NullCheck(L_5);
		L_5->set_isDragging_2((bool)1);
		return;
	}
}
extern "C"  void SliderHandler_StartDraggingWithValue_m3874943933_AdjustorThunk (Il2CppObject * __this, float ___dragStartValue0, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	SliderHandler_StartDraggingWithValue_m3874943933(_thisAdjusted, ___dragStartValue0, method);
}
// UnityEngine.SliderState UnityEngine.SliderHandler::SliderState()
extern const Il2CppType* SliderState_t1233388262_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIUtility_t1028319349_il2cpp_TypeInfo_var;
extern Il2CppClass* SliderState_t1233388262_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_SliderState_m1456191352_MetadataUsageId;
extern "C"  SliderState_t1233388262 * SliderHandler_SliderState_m1456191352 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_SliderState_m1456191352_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(SliderState_t1233388262_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_1 = __this->get_id_8();
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = GUIUtility_GetStateObject_m2379308309(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return ((SliderState_t1233388262 *)CastclassClass(L_2, SliderState_t1233388262_il2cpp_TypeInfo_var));
	}
}
extern "C"  SliderState_t1233388262 * SliderHandler_SliderState_m1456191352_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_SliderState_m1456191352(_thisAdjusted, method);
}
// UnityEngine.Rect UnityEngine.SliderHandler::ThumbRect()
extern "C"  Rect_t4241904616  SliderHandler_ThumbRect_m1881961532 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	Rect_t4241904616  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Rect_t4241904616  L_1 = SliderHandler_HorizontalThumbRect_m2689559608(__this, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_001c;
	}

IL_0016:
	{
		Rect_t4241904616  L_2 = SliderHandler_VerticalThumbRect_m1406344678(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
extern "C"  Rect_t4241904616  SliderHandler_ThumbRect_m1881961532_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_ThumbRect_m1881961532(_thisAdjusted, method);
}
// UnityEngine.Rect UnityEngine.SliderHandler::VerticalThumbRect()
extern "C"  Rect_t4241904616  SliderHandler_VerticalThumbRect_m1406344678 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Rect_t4241904616  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t4241904616  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t4241904616  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t4241904616  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Rect_t4241904616  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Rect_t4241904616  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		float L_0 = SliderHandler_ValuesPerPixel_m41869331(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = __this->get_start_3();
		float L_2 = __this->get_end_4();
		if ((!(((float)L_1) < ((float)L_2))))
		{
			goto IL_009d;
		}
	}
	{
		Rect_t4241904616  L_3 = __this->get_position_0();
		V_1 = L_3;
		float L_4 = Rect_get_x_m982385354((&V_1), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_5 = __this->get_slider_5();
		NullCheck(L_5);
		RectOffset_t3056157787 * L_6 = GUIStyle_get_padding_m3072941276(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = RectOffset_get_left_m4104523390(L_6, /*hidden argument*/NULL);
		float L_8 = SliderHandler_ClampedCurrentValue_m3006511340(__this, /*hidden argument*/NULL);
		float L_9 = __this->get_start_3();
		float L_10 = V_0;
		Rect_t4241904616  L_11 = __this->get_position_0();
		V_2 = L_11;
		float L_12 = Rect_get_y_m982386315((&V_2), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_13 = __this->get_slider_5();
		NullCheck(L_13);
		RectOffset_t3056157787 * L_14 = GUIStyle_get_padding_m3072941276(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		int32_t L_15 = RectOffset_get_top_m140097312(L_14, /*hidden argument*/NULL);
		Rect_t4241904616  L_16 = __this->get_position_0();
		V_3 = L_16;
		float L_17 = Rect_get_width_m2824209432((&V_3), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_18 = __this->get_slider_5();
		NullCheck(L_18);
		RectOffset_t3056157787 * L_19 = GUIStyle_get_padding_m3072941276(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		int32_t L_20 = RectOffset_get_horizontal_m1186440923(L_19, /*hidden argument*/NULL);
		float L_21 = __this->get_size_2();
		float L_22 = V_0;
		float L_23 = SliderHandler_ThumbSize_m3034411697(__this, /*hidden argument*/NULL);
		Rect_t4241904616  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Rect__ctor_m3291325233(&L_24, ((float)((float)L_4+(float)(((float)((float)L_7))))), ((float)((float)((float)((float)((float)((float)((float)((float)L_8-(float)L_9))*(float)L_10))+(float)L_12))+(float)(((float)((float)L_15))))), ((float)((float)L_17-(float)(((float)((float)L_20))))), ((float)((float)((float)((float)L_21*(float)L_22))+(float)L_23)), /*hidden argument*/NULL);
		return L_24;
	}

IL_009d:
	{
		Rect_t4241904616  L_25 = __this->get_position_0();
		V_4 = L_25;
		float L_26 = Rect_get_x_m982385354((&V_4), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_27 = __this->get_slider_5();
		NullCheck(L_27);
		RectOffset_t3056157787 * L_28 = GUIStyle_get_padding_m3072941276(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		int32_t L_29 = RectOffset_get_left_m4104523390(L_28, /*hidden argument*/NULL);
		float L_30 = SliderHandler_ClampedCurrentValue_m3006511340(__this, /*hidden argument*/NULL);
		float L_31 = __this->get_size_2();
		float L_32 = __this->get_start_3();
		float L_33 = V_0;
		Rect_t4241904616  L_34 = __this->get_position_0();
		V_5 = L_34;
		float L_35 = Rect_get_y_m982386315((&V_5), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_36 = __this->get_slider_5();
		NullCheck(L_36);
		RectOffset_t3056157787 * L_37 = GUIStyle_get_padding_m3072941276(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		int32_t L_38 = RectOffset_get_top_m140097312(L_37, /*hidden argument*/NULL);
		Rect_t4241904616  L_39 = __this->get_position_0();
		V_6 = L_39;
		float L_40 = Rect_get_width_m2824209432((&V_6), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_41 = __this->get_slider_5();
		NullCheck(L_41);
		RectOffset_t3056157787 * L_42 = GUIStyle_get_padding_m3072941276(L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		int32_t L_43 = RectOffset_get_horizontal_m1186440923(L_42, /*hidden argument*/NULL);
		float L_44 = __this->get_size_2();
		float L_45 = V_0;
		float L_46 = SliderHandler_ThumbSize_m3034411697(__this, /*hidden argument*/NULL);
		Rect_t4241904616  L_47;
		memset(&L_47, 0, sizeof(L_47));
		Rect__ctor_m3291325233(&L_47, ((float)((float)L_26+(float)(((float)((float)L_29))))), ((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)L_30+(float)L_31))-(float)L_32))*(float)L_33))+(float)L_35))+(float)(((float)((float)L_38))))), ((float)((float)L_40-(float)(((float)((float)L_43))))), ((float)((float)((float)((float)L_44*(float)((-L_45))))+(float)L_46)), /*hidden argument*/NULL);
		return L_47;
	}
}
extern "C"  Rect_t4241904616  SliderHandler_VerticalThumbRect_m1406344678_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_VerticalThumbRect_m1406344678(_thisAdjusted, method);
}
// UnityEngine.Rect UnityEngine.SliderHandler::HorizontalThumbRect()
extern "C"  Rect_t4241904616  SliderHandler_HorizontalThumbRect_m2689559608 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Rect_t4241904616  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t4241904616  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t4241904616  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t4241904616  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Rect_t4241904616  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Rect_t4241904616  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		float L_0 = SliderHandler_ValuesPerPixel_m41869331(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = __this->get_start_3();
		float L_2 = __this->get_end_4();
		if ((!(((float)L_1) < ((float)L_2))))
		{
			goto IL_009d;
		}
	}
	{
		float L_3 = SliderHandler_ClampedCurrentValue_m3006511340(__this, /*hidden argument*/NULL);
		float L_4 = __this->get_start_3();
		float L_5 = V_0;
		Rect_t4241904616  L_6 = __this->get_position_0();
		V_1 = L_6;
		float L_7 = Rect_get_x_m982385354((&V_1), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_8 = __this->get_slider_5();
		NullCheck(L_8);
		RectOffset_t3056157787 * L_9 = GUIStyle_get_padding_m3072941276(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_10 = RectOffset_get_left_m4104523390(L_9, /*hidden argument*/NULL);
		Rect_t4241904616  L_11 = __this->get_position_0();
		V_2 = L_11;
		float L_12 = Rect_get_y_m982386315((&V_2), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_13 = __this->get_slider_5();
		NullCheck(L_13);
		RectOffset_t3056157787 * L_14 = GUIStyle_get_padding_m3072941276(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		int32_t L_15 = RectOffset_get_top_m140097312(L_14, /*hidden argument*/NULL);
		float L_16 = __this->get_size_2();
		float L_17 = V_0;
		float L_18 = SliderHandler_ThumbSize_m3034411697(__this, /*hidden argument*/NULL);
		Rect_t4241904616  L_19 = __this->get_position_0();
		V_3 = L_19;
		float L_20 = Rect_get_height_m2154960823((&V_3), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_21 = __this->get_slider_5();
		NullCheck(L_21);
		RectOffset_t3056157787 * L_22 = GUIStyle_get_padding_m3072941276(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		int32_t L_23 = RectOffset_get_vertical_m3650431789(L_22, /*hidden argument*/NULL);
		Rect_t4241904616  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Rect__ctor_m3291325233(&L_24, ((float)((float)((float)((float)((float)((float)((float)((float)L_3-(float)L_4))*(float)L_5))+(float)L_7))+(float)(((float)((float)L_10))))), ((float)((float)L_12+(float)(((float)((float)L_15))))), ((float)((float)((float)((float)L_16*(float)L_17))+(float)L_18)), ((float)((float)L_20-(float)(((float)((float)L_23))))), /*hidden argument*/NULL);
		return L_24;
	}

IL_009d:
	{
		float L_25 = SliderHandler_ClampedCurrentValue_m3006511340(__this, /*hidden argument*/NULL);
		float L_26 = __this->get_size_2();
		float L_27 = __this->get_start_3();
		float L_28 = V_0;
		Rect_t4241904616  L_29 = __this->get_position_0();
		V_4 = L_29;
		float L_30 = Rect_get_x_m982385354((&V_4), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_31 = __this->get_slider_5();
		NullCheck(L_31);
		RectOffset_t3056157787 * L_32 = GUIStyle_get_padding_m3072941276(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		int32_t L_33 = RectOffset_get_left_m4104523390(L_32, /*hidden argument*/NULL);
		Rect_t4241904616  L_34 = __this->get_position_0();
		V_5 = L_34;
		float L_35 = Rect_get_y_m982386315((&V_5), /*hidden argument*/NULL);
		float L_36 = __this->get_size_2();
		float L_37 = V_0;
		float L_38 = SliderHandler_ThumbSize_m3034411697(__this, /*hidden argument*/NULL);
		Rect_t4241904616  L_39 = __this->get_position_0();
		V_6 = L_39;
		float L_40 = Rect_get_height_m2154960823((&V_6), /*hidden argument*/NULL);
		Rect_t4241904616  L_41;
		memset(&L_41, 0, sizeof(L_41));
		Rect__ctor_m3291325233(&L_41, ((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)L_25+(float)L_26))-(float)L_27))*(float)L_28))+(float)L_30))+(float)(((float)((float)L_33))))), L_35, ((float)((float)((float)((float)L_36*(float)((-L_37))))+(float)L_38)), L_40, /*hidden argument*/NULL);
		return L_41;
	}
}
extern "C"  Rect_t4241904616  SliderHandler_HorizontalThumbRect_m2689559608_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_HorizontalThumbRect_m2689559608(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::ClampedCurrentValue()
extern "C"  float SliderHandler_ClampedCurrentValue_m3006511340 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_currentValue_1();
		float L_1 = SliderHandler_Clamp_m4218954710(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
extern "C"  float SliderHandler_ClampedCurrentValue_m3006511340_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_ClampedCurrentValue_m3006511340(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::MousePosition()
extern "C"  float SliderHandler_MousePosition_m303335016 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t4241904616  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t4282066565  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t4241904616  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_002e;
		}
	}
	{
		Event_t4196595728 * L_1 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector2_t4282066565  L_2 = Event_get_mousePosition_m3610425949(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = (&V_0)->get_x_1();
		Rect_t4241904616  L_4 = __this->get_position_0();
		V_1 = L_4;
		float L_5 = Rect_get_x_m982385354((&V_1), /*hidden argument*/NULL);
		return ((float)((float)L_3-(float)L_5));
	}

IL_002e:
	{
		Event_t4196595728 * L_6 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector2_t4282066565  L_7 = Event_get_mousePosition_m3610425949(L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		float L_8 = (&V_2)->get_y_2();
		Rect_t4241904616  L_9 = __this->get_position_0();
		V_3 = L_9;
		float L_10 = Rect_get_y_m982386315((&V_3), /*hidden argument*/NULL);
		return ((float)((float)L_8-(float)L_10));
	}
}
extern "C"  float SliderHandler_MousePosition_m303335016_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_MousePosition_m303335016(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::ValuesPerPixel()
extern "C"  float SliderHandler_ValuesPerPixel_m41869331 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t4241904616  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_0041;
		}
	}
	{
		Rect_t4241904616  L_1 = __this->get_position_0();
		V_0 = L_1;
		float L_2 = Rect_get_width_m2824209432((&V_0), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_3 = __this->get_slider_5();
		NullCheck(L_3);
		RectOffset_t3056157787 * L_4 = GUIStyle_get_padding_m3072941276(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = RectOffset_get_horizontal_m1186440923(L_4, /*hidden argument*/NULL);
		float L_6 = SliderHandler_ThumbSize_m3034411697(__this, /*hidden argument*/NULL);
		float L_7 = __this->get_end_4();
		float L_8 = __this->get_start_3();
		return ((float)((float)((float)((float)((float)((float)L_2-(float)(((float)((float)L_5)))))-(float)L_6))/(float)((float)((float)L_7-(float)L_8))));
	}

IL_0041:
	{
		Rect_t4241904616  L_9 = __this->get_position_0();
		V_1 = L_9;
		float L_10 = Rect_get_height_m2154960823((&V_1), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_11 = __this->get_slider_5();
		NullCheck(L_11);
		RectOffset_t3056157787 * L_12 = GUIStyle_get_padding_m3072941276(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		int32_t L_13 = RectOffset_get_vertical_m3650431789(L_12, /*hidden argument*/NULL);
		float L_14 = SliderHandler_ThumbSize_m3034411697(__this, /*hidden argument*/NULL);
		float L_15 = __this->get_end_4();
		float L_16 = __this->get_start_3();
		return ((float)((float)((float)((float)((float)((float)L_10-(float)(((float)((float)L_13)))))-(float)L_14))/(float)((float)((float)L_15-(float)L_16))));
	}
}
extern "C"  float SliderHandler_ValuesPerPixel_m41869331_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_ValuesPerPixel_m41869331(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::ThumbSize()
extern "C"  float SliderHandler_ThumbSize_m3034411697 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	float G_B4_0 = 0.0f;
	float G_B8_0 = 0.0f;
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_0042;
		}
	}
	{
		GUIStyle_t2990928826 * L_1 = __this->get_thumb_6();
		NullCheck(L_1);
		float L_2 = GUIStyle_get_fixedWidth_m3249098964(L_1, /*hidden argument*/NULL);
		if ((((float)L_2) == ((float)(0.0f))))
		{
			goto IL_0030;
		}
	}
	{
		GUIStyle_t2990928826 * L_3 = __this->get_thumb_6();
		NullCheck(L_3);
		float L_4 = GUIStyle_get_fixedWidth_m3249098964(L_3, /*hidden argument*/NULL);
		G_B4_0 = L_4;
		goto IL_0041;
	}

IL_0030:
	{
		GUIStyle_t2990928826 * L_5 = __this->get_thumb_6();
		NullCheck(L_5);
		RectOffset_t3056157787 * L_6 = GUIStyle_get_padding_m3072941276(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = RectOffset_get_horizontal_m1186440923(L_6, /*hidden argument*/NULL);
		G_B4_0 = (((float)((float)L_7)));
	}

IL_0041:
	{
		return G_B4_0;
	}

IL_0042:
	{
		GUIStyle_t2990928826 * L_8 = __this->get_thumb_6();
		NullCheck(L_8);
		float L_9 = GUIStyle_get_fixedHeight_m2441634427(L_8, /*hidden argument*/NULL);
		if ((((float)L_9) == ((float)(0.0f))))
		{
			goto IL_0067;
		}
	}
	{
		GUIStyle_t2990928826 * L_10 = __this->get_thumb_6();
		NullCheck(L_10);
		float L_11 = GUIStyle_get_fixedHeight_m2441634427(L_10, /*hidden argument*/NULL);
		G_B8_0 = L_11;
		goto IL_0078;
	}

IL_0067:
	{
		GUIStyle_t2990928826 * L_12 = __this->get_thumb_6();
		NullCheck(L_12);
		RectOffset_t3056157787 * L_13 = GUIStyle_get_padding_m3072941276(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		int32_t L_14 = RectOffset_get_vertical_m3650431789(L_13, /*hidden argument*/NULL);
		G_B8_0 = (((float)((float)L_14)));
	}

IL_0078:
	{
		return G_B8_0;
	}
}
extern "C"  float SliderHandler_ThumbSize_m3034411697_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_ThumbSize_m3034411697(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::MaxValue()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_MaxValue_m44679317_MetadataUsageId;
extern "C"  float SliderHandler_MaxValue_m44679317 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_MaxValue_m44679317_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_start_3();
		float L_1 = __this->get_end_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Max_m3923796455(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = __this->get_size_2();
		return ((float)((float)L_2-(float)L_3));
	}
}
extern "C"  float SliderHandler_MaxValue_m44679317_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_MaxValue_m44679317(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::MinValue()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_MinValue_m2516678631_MetadataUsageId;
extern "C"  float SliderHandler_MinValue_m2516678631 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_MinValue_m2516678631_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_start_3();
		float L_1 = __this->get_end_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Min_m2322067385(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  float SliderHandler_MinValue_m2516678631_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_MinValue_m2516678631(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.SliderHandler
extern "C" void SliderHandler_t783692703_marshal_pinvoke(const SliderHandler_t783692703& unmarshaled, SliderHandler_t783692703_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___slider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'slider' of type 'SliderHandler': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___slider_5Exception);
}
extern "C" void SliderHandler_t783692703_marshal_pinvoke_back(const SliderHandler_t783692703_marshaled_pinvoke& marshaled, SliderHandler_t783692703& unmarshaled)
{
	Il2CppCodeGenException* ___slider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'slider' of type 'SliderHandler': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___slider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SliderHandler
extern "C" void SliderHandler_t783692703_marshal_pinvoke_cleanup(SliderHandler_t783692703_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SliderHandler
extern "C" void SliderHandler_t783692703_marshal_com(const SliderHandler_t783692703& unmarshaled, SliderHandler_t783692703_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___slider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'slider' of type 'SliderHandler': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___slider_5Exception);
}
extern "C" void SliderHandler_t783692703_marshal_com_back(const SliderHandler_t783692703_marshaled_com& marshaled, SliderHandler_t783692703& unmarshaled)
{
	Il2CppCodeGenException* ___slider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'slider' of type 'SliderHandler': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___slider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SliderHandler
extern "C" void SliderHandler_t783692703_marshal_com_cleanup(SliderHandler_t783692703_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.SliderState::.ctor()
extern "C"  void SliderState__ctor_m3732503849 (SliderState_t1233388262 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::.ctor()
extern "C"  void GameCenterPlatform__ctor_m573039859 (GameCenterPlatform_t3570684786 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::.cctor()
extern Il2CppClass* AchievementDescriptionU5BU5D_t759444790_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t3189060351_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1721872494_MethodInfo_var;
extern const uint32_t GameCenterPlatform__cctor_m102270234_MetadataUsageId;
extern "C"  void GameCenterPlatform__cctor_m102270234 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform__cctor_m102270234_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_adCache_9(((AchievementDescriptionU5BU5D_t759444790*)SZArrayNew(AchievementDescriptionU5BU5D_t759444790_il2cpp_TypeInfo_var, (uint32_t)0)));
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_friends_10(((UserProfileU5BU5D_t2378268441*)SZArrayNew(UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var, (uint32_t)0)));
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_users_11(((UserProfileU5BU5D_t2378268441*)SZArrayNew(UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var, (uint32_t)0)));
		List_1_t3189060351 * L_0 = (List_1_t3189060351 *)il2cpp_codegen_object_new(List_1_t3189060351_il2cpp_TypeInfo_var);
		List_1__ctor_m1721872494(L_0, /*hidden argument*/List_1__ctor_m1721872494_MethodInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_m_GcBoards_14(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::UnityEngine.SocialPlatforms.ISocialPlatform.LoadFriends(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m2468032119_MetadataUsageId;
extern "C"  void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m2468032119 (GameCenterPlatform_t3570684786 * __this, Il2CppObject * ___user0, Action_1_t872614854 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m2468032119_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t872614854 * L_0 = ___callback1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_FriendsCallback_1(L_0);
		GameCenterPlatform_Internal_LoadFriends_m1921936862(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::UnityEngine.SocialPlatforms.ISocialPlatform.Authenticate(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m305325355_MetadataUsageId;
extern "C"  void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m305325355 (GameCenterPlatform_t3570684786 * __this, Il2CppObject * ___user0, Action_1_t872614854 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m305325355_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t872614854 * L_0 = ___callback1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_AuthenticateCallback_0(L_0);
		GameCenterPlatform_Internal_Authenticate_m582381960(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticate()
extern "C"  void GameCenterPlatform_Internal_Authenticate_m582381960 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_Authenticate_m582381960_ftn) ();
	static GameCenterPlatform_Internal_Authenticate_m582381960_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_Authenticate_m582381960_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticate()");
	_il2cpp_icall_func();
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticated()
extern "C"  bool GameCenterPlatform_Internal_Authenticated_m2780967960 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*GameCenterPlatform_Internal_Authenticated_m2780967960_ftn) ();
	static GameCenterPlatform_Internal_Authenticated_m2780967960_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_Authenticated_m2780967960_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticated()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserName()
extern "C"  String_t* GameCenterPlatform_Internal_UserName_m1252299660 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*GameCenterPlatform_Internal_UserName_m1252299660_ftn) ();
	static GameCenterPlatform_Internal_UserName_m1252299660_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_UserName_m1252299660_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserName()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserID()
extern "C"  String_t* GameCenterPlatform_Internal_UserID_m385481212 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*GameCenterPlatform_Internal_UserID_m385481212_ftn) ();
	static GameCenterPlatform_Internal_UserID_m385481212_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_UserID_m385481212_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserID()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Underage()
extern "C"  bool GameCenterPlatform_Internal_Underage_m4169738944 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*GameCenterPlatform_Internal_Underage_m4169738944_ftn) ();
	static GameCenterPlatform_Internal_Underage_m4169738944_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_Underage_m4169738944_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Underage()");
	return _il2cpp_icall_func();
}
// UnityEngine.Texture2D UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserImage()
extern "C"  Texture2D_t3884108195 * GameCenterPlatform_Internal_UserImage_m3175776130 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef Texture2D_t3884108195 * (*GameCenterPlatform_Internal_UserImage_m3175776130_ftn) ();
	static GameCenterPlatform_Internal_UserImage_m3175776130_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_UserImage_m3175776130_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserImage()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadFriends()
extern "C"  void GameCenterPlatform_Internal_LoadFriends_m1921936862 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadFriends_m1921936862_ftn) ();
	static GameCenterPlatform_Internal_LoadFriends_m1921936862_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadFriends_m1921936862_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadFriends()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievementDescriptions()
extern "C"  void GameCenterPlatform_Internal_LoadAchievementDescriptions_m1394384079 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadAchievementDescriptions_m1394384079_ftn) ();
	static GameCenterPlatform_Internal_LoadAchievementDescriptions_m1394384079_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadAchievementDescriptions_m1394384079_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievementDescriptions()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievements()
extern "C"  void GameCenterPlatform_Internal_LoadAchievements_m817891229 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadAchievements_m817891229_ftn) ();
	static GameCenterPlatform_Internal_LoadAchievements_m817891229_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadAchievements_m817891229_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievements()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportProgress(System.String,System.Double)
extern "C"  void GameCenterPlatform_Internal_ReportProgress_m2511520970 (Il2CppObject * __this /* static, unused */, String_t* ___id0, double ___progress1, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ReportProgress_m2511520970_ftn) (String_t*, double);
	static GameCenterPlatform_Internal_ReportProgress_m2511520970_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ReportProgress_m2511520970_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportProgress(System.String,System.Double)");
	_il2cpp_icall_func(___id0, ___progress1);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportScore(System.Int64,System.String)
extern "C"  void GameCenterPlatform_Internal_ReportScore_m408601947 (Il2CppObject * __this /* static, unused */, int64_t ___score0, String_t* ___category1, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ReportScore_m408601947_ftn) (int64_t, String_t*);
	static GameCenterPlatform_Internal_ReportScore_m408601947_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ReportScore_m408601947_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportScore(System.Int64,System.String)");
	_il2cpp_icall_func(___score0, ___category1);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadScores(System.String)
extern "C"  void GameCenterPlatform_Internal_LoadScores_m523283944 (Il2CppObject * __this /* static, unused */, String_t* ___category0, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadScores_m523283944_ftn) (String_t*);
	static GameCenterPlatform_Internal_LoadScores_m523283944_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadScores_m523283944_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadScores(System.String)");
	_il2cpp_icall_func(___category0);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowAchievementsUI()
extern "C"  void GameCenterPlatform_Internal_ShowAchievementsUI_m1934331464 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowAchievementsUI_m1934331464_ftn) ();
	static GameCenterPlatform_Internal_ShowAchievementsUI_m1934331464_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowAchievementsUI_m1934331464_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowAchievementsUI()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowLeaderboardUI()
extern "C"  void GameCenterPlatform_Internal_ShowLeaderboardUI_m3057704739 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowLeaderboardUI_m3057704739_ftn) ();
	static GameCenterPlatform_Internal_ShowLeaderboardUI_m3057704739_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowLeaderboardUI_m3057704739_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowLeaderboardUI()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadUsers(System.String[])
extern "C"  void GameCenterPlatform_Internal_LoadUsers_m4218820079 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t4054002952* ___userIds0, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadUsers_m4218820079_ftn) (StringU5BU5D_t4054002952*);
	static GameCenterPlatform_Internal_LoadUsers_m4218820079_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadUsers_m4218820079_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadUsers(System.String[])");
	_il2cpp_icall_func(___userIds0);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ResetAllAchievements()
extern "C"  void GameCenterPlatform_Internal_ResetAllAchievements_m165059209 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ResetAllAchievements_m165059209_ftn) ();
	static GameCenterPlatform_Internal_ResetAllAchievements_m165059209_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ResetAllAchievements_m165059209_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ResetAllAchievements()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowDefaultAchievementBanner(System.Boolean)
extern "C"  void GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m3108376897 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m3108376897_ftn) (bool);
	static GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m3108376897_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m3108376897_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowDefaultAchievementBanner(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ResetAllAchievements(System.Action`1<System.Boolean>)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ResetAllAchievements_m878609996_MetadataUsageId;
extern "C"  void GameCenterPlatform_ResetAllAchievements_m878609996 (Il2CppObject * __this /* static, unused */, Action_1_t872614854 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ResetAllAchievements_m878609996_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t872614854 * L_0 = ___callback0;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_ResetAchievements_12(L_0);
		GameCenterPlatform_Internal_ResetAllAchievements_m165059209(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowDefaultAchievementCompletionBanner(System.Boolean)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m2516168699_MetadataUsageId;
extern "C"  void GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m2516168699 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m2516168699_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m3108376897(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowLeaderboardUI(System.String,UnityEngine.SocialPlatforms.TimeScope)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ShowLeaderboardUI_m3791866548_MetadataUsageId;
extern "C"  void GameCenterPlatform_ShowLeaderboardUI_m3791866548 (Il2CppObject * __this /* static, unused */, String_t* ___leaderboardID0, int32_t ___timeScope1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ShowLeaderboardUI_m3791866548_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___leaderboardID0;
		int32_t L_1 = ___timeScope1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m1768304742(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowSpecificLeaderboardUI(System.String,System.Int32)
extern "C"  void GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m1768304742 (Il2CppObject * __this /* static, unused */, String_t* ___leaderboardID0, int32_t ___timeScope1, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m1768304742_ftn) (String_t*, int32_t);
	static GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m1768304742_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m1768304742_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowSpecificLeaderboardUI(System.String,System.Int32)");
	_il2cpp_icall_func(___leaderboardID0, ___timeScope1);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ClearAchievementDescriptions(System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* AchievementDescriptionU5BU5D_t759444790_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ClearAchievementDescriptions_m3158758843_MetadataUsageId;
extern "C"  void GameCenterPlatform_ClearAchievementDescriptions_m3158758843 (Il2CppObject * __this /* static, unused */, int32_t ___size0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ClearAchievementDescriptions_m3158758843_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t759444790* L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t759444790* L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		NullCheck(L_1);
		int32_t L_2 = ___size0;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))) == ((int32_t)L_2)))
		{
			goto IL_0022;
		}
	}

IL_0017:
	{
		int32_t L_3 = ___size0;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_adCache_9(((AchievementDescriptionU5BU5D_t759444790*)SZArrayNew(AchievementDescriptionU5BU5D_t759444790_il2cpp_TypeInfo_var, (uint32_t)L_3)));
	}

IL_0022:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetAchievementDescription(UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData,System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SetAchievementDescription_m3174496109_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetAchievementDescription_m3174496109 (Il2CppObject * __this /* static, unused */, GcAchievementDescriptionData_t2242891083  ___data0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetAchievementDescription_m3174496109_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t759444790* L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		int32_t L_1 = ___number1;
		AchievementDescription_t2116066607 * L_2 = GcAchievementDescriptionData_ToAchievementDescription_m3125480712((&___data0), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		ArrayElementTypeCheck (L_0, L_2);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (AchievementDescription_t2116066607 *)L_2);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetAchievementDescriptionImage(UnityEngine.Texture2D,System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2873143867;
extern const uint32_t GameCenterPlatform_SetAchievementDescriptionImage_m3728674360_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetAchievementDescriptionImage_m3728674360 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___texture0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetAchievementDescriptionImage_m3728674360_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t759444790* L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		NullCheck(L_0);
		int32_t L_1 = ___number1;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) <= ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = ___number1;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_001f;
		}
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral2873143867, /*hidden argument*/NULL);
		return;
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t759444790* L_3 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		int32_t L_4 = ___number1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		AchievementDescription_t2116066607 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		Texture2D_t3884108195 * L_7 = ___texture0;
		NullCheck(L_6);
		AchievementDescription_SetImage_m1092175896(L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerAchievementDescriptionCallback()
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2766540343_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral48254646;
extern const uint32_t GameCenterPlatform_TriggerAchievementDescriptionCallback_m1497473051_MetadataUsageId;
extern "C"  void GameCenterPlatform_TriggerAchievementDescriptionCallback_m1497473051 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_TriggerAchievementDescriptionCallback_m1497473051_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t229750097 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_AchievementDescriptionLoaderCallback_2();
		if (!L_0)
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t759444790* L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		if (!L_1)
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t759444790* L_2 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		NullCheck(L_2);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))))
		{
			goto IL_002a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral48254646, /*hidden argument*/NULL);
	}

IL_002a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t229750097 * L_3 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_AchievementDescriptionLoaderCallback_2();
		AchievementDescriptionU5BU5D_t759444790* L_4 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		NullCheck(L_3);
		Action_1_Invoke_m2766540343(L_3, (IAchievementDescriptionU5BU5D_t4128901257*)(IAchievementDescriptionU5BU5D_t4128901257*)L_4, /*hidden argument*/Action_1_Invoke_m2766540343_MethodInfo_var);
	}

IL_0039:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::AuthenticateCallbackWrapper(System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const uint32_t GameCenterPlatform_AuthenticateCallbackWrapper_m2896042779_MetadataUsageId;
extern "C"  void GameCenterPlatform_AuthenticateCallbackWrapper_m2896042779 (Il2CppObject * __this /* static, unused */, int32_t ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_AuthenticateCallbackWrapper_m2896042779_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t872614854 * G_B3_0 = NULL;
	Action_1_t872614854 * G_B2_0 = NULL;
	int32_t G_B4_0 = 0;
	Action_1_t872614854 * G_B4_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		GameCenterPlatform_PopulateLocalUser_m2583301917(NULL /*static, unused*/, /*hidden argument*/NULL);
		Action_1_t872614854 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_AuthenticateCallback_0();
		if (!L_0)
		{
			goto IL_002d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_AuthenticateCallback_0();
		int32_t L_2 = ___result0;
		G_B2_0 = L_1;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			G_B3_0 = L_1;
			goto IL_0021;
		}
	}
	{
		G_B4_0 = 1;
		G_B4_1 = G_B2_0;
		goto IL_0022;
	}

IL_0021:
	{
		G_B4_0 = 0;
		G_B4_1 = G_B3_0;
	}

IL_0022:
	{
		NullCheck(G_B4_1);
		Action_1_Invoke_m3594021162(G_B4_1, (bool)G_B4_0, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_AuthenticateCallback_0((Action_1_t872614854 *)NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ClearFriends(System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ClearFriends_m1761222218_MetadataUsageId;
extern "C"  void GameCenterPlatform_ClearFriends_m1761222218 (Il2CppObject * __this /* static, unused */, int32_t ___size0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ClearFriends_m1761222218_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		int32_t L_0 = ___size0;
		GameCenterPlatform_SafeClearArray_m2546851889(NULL /*static, unused*/, (((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_friends_10()), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetFriends(UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData,System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SetFriends_m3378244042_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetFriends_m3378244042 (Il2CppObject * __this /* static, unused */, GcUserProfileData_t657441114  ___data0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetFriends_m3378244042_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		int32_t L_0 = ___number1;
		GcUserProfileData_AddToArray_m3757655355((&___data0), (((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_friends_10()), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetFriendImage(UnityEngine.Texture2D,System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SetFriendImage_m4228294663_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetFriendImage_m4228294663 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___texture0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetFriendImage_m4228294663_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Texture2D_t3884108195 * L_0 = ___texture0;
		int32_t L_1 = ___number1;
		GameCenterPlatform_SafeSetUserImage_m3650098397(NULL /*static, unused*/, (((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_friends_10()), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerFriendsCallbackWrapper(System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const uint32_t GameCenterPlatform_TriggerFriendsCallbackWrapper_m3845044787_MetadataUsageId;
extern "C"  void GameCenterPlatform_TriggerFriendsCallbackWrapper_m3845044787 (Il2CppObject * __this /* static, unused */, int32_t ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_TriggerFriendsCallbackWrapper_m3845044787_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t872614854 * G_B5_0 = NULL;
	Action_1_t872614854 * G_B4_0 = NULL;
	int32_t G_B6_0 = 0;
	Action_1_t872614854 * G_B6_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		UserProfileU5BU5D_t2378268441* L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_friends_10();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		LocalUser_t1307362368 * L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		UserProfileU5BU5D_t2378268441* L_2 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_friends_10();
		NullCheck(L_1);
		LocalUser_SetFriends_m3475409220(L_1, (IUserProfileU5BU5D_t3419104218*)(IUserProfileU5BU5D_t3419104218*)L_2, /*hidden argument*/NULL);
	}

IL_0019:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_3 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_FriendsCallback_1();
		if (!L_3)
		{
			goto IL_003b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_4 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_FriendsCallback_1();
		int32_t L_5 = ___result0;
		G_B4_0 = L_4;
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			G_B5_0 = L_4;
			goto IL_0035;
		}
	}
	{
		G_B6_0 = 1;
		G_B6_1 = G_B4_0;
		goto IL_0036;
	}

IL_0035:
	{
		G_B6_0 = 0;
		G_B6_1 = G_B5_0;
	}

IL_0036:
	{
		NullCheck(G_B6_1);
		Action_1_Invoke_m3594021162(G_B6_1, (bool)G_B6_0, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
	}

IL_003b:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::AchievementCallbackWrapper(UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[])
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* AchievementU5BU5D_t912418020_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m222677977_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3379715660;
extern const uint32_t GameCenterPlatform_AchievementCallbackWrapper_m2031411110_MetadataUsageId;
extern "C"  void GameCenterPlatform_AchievementCallbackWrapper_m2031411110 (Il2CppObject * __this /* static, unused */, GcAchievementDataU5BU5D_t1726768202* ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_AchievementCallbackWrapper_m2031411110_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AchievementU5BU5D_t912418020* V_0 = NULL;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t2349069933 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_AchievementLoaderCallback_3();
		if (!L_0)
		{
			goto IL_0053;
		}
	}
	{
		GcAchievementDataU5BU5D_t1726768202* L_1 = ___result0;
		NullCheck(L_1);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))))
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral3379715660, /*hidden argument*/NULL);
	}

IL_001c:
	{
		GcAchievementDataU5BU5D_t1726768202* L_2 = ___result0;
		NullCheck(L_2);
		V_0 = ((AchievementU5BU5D_t912418020*)SZArrayNew(AchievementU5BU5D_t912418020_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))));
		V_1 = 0;
		goto IL_003f;
	}

IL_002c:
	{
		AchievementU5BU5D_t912418020* L_3 = V_0;
		int32_t L_4 = V_1;
		GcAchievementDataU5BU5D_t1726768202* L_5 = ___result0;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		Achievement_t344600729 * L_7 = GcAchievementData_ToAchievement_m3239514930(((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6))), /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		ArrayElementTypeCheck (L_3, L_7);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (Achievement_t344600729 *)L_7);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003f:
	{
		int32_t L_9 = V_1;
		GcAchievementDataU5BU5D_t1726768202* L_10 = ___result0;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t2349069933 * L_11 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_AchievementLoaderCallback_3();
		AchievementU5BU5D_t912418020* L_12 = V_0;
		NullCheck(L_11);
		Action_1_Invoke_m222677977(L_11, (IAchievementU5BU5D_t1953253797*)(IAchievementU5BU5D_t1953253797*)L_12, /*hidden argument*/Action_1_Invoke_m222677977_MethodInfo_var);
	}

IL_0053:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ProgressCallbackWrapper(System.Boolean)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const uint32_t GameCenterPlatform_ProgressCallbackWrapper_m165794409_MetadataUsageId;
extern "C"  void GameCenterPlatform_ProgressCallbackWrapper_m165794409 (Il2CppObject * __this /* static, unused */, bool ___success0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ProgressCallbackWrapper_m165794409_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_ProgressCallback_4();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_ProgressCallback_4();
		bool L_2 = ___success0;
		NullCheck(L_1);
		Action_1_Invoke_m3594021162(L_1, L_2, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ScoreCallbackWrapper(System.Boolean)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const uint32_t GameCenterPlatform_ScoreCallbackWrapper_m2797312324_MetadataUsageId;
extern "C"  void GameCenterPlatform_ScoreCallbackWrapper_m2797312324 (Il2CppObject * __this /* static, unused */, bool ___success0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ScoreCallbackWrapper_m2797312324_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_ScoreCallback_5();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_ScoreCallback_5();
		bool L_2 = ___success0;
		NullCheck(L_1);
		Action_1_Invoke_m3594021162(L_1, L_2, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ScoreLoaderCallbackWrapper(UnityEngine.SocialPlatforms.GameCenter.GcScoreData[])
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* ScoreU5BU5D_t2926278037_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m50340758_MethodInfo_var;
extern const uint32_t GameCenterPlatform_ScoreLoaderCallbackWrapper_m2588839053_MetadataUsageId;
extern "C"  void GameCenterPlatform_ScoreLoaderCallbackWrapper_m2588839053 (Il2CppObject * __this /* static, unused */, GcScoreDataU5BU5D_t1670395707* ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ScoreLoaderCallbackWrapper_m2588839053_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ScoreU5BU5D_t2926278037* V_0 = NULL;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t645920862 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_ScoreLoaderCallback_6();
		if (!L_0)
		{
			goto IL_0041;
		}
	}
	{
		GcScoreDataU5BU5D_t1670395707* L_1 = ___result0;
		NullCheck(L_1);
		V_0 = ((ScoreU5BU5D_t2926278037*)SZArrayNew(ScoreU5BU5D_t2926278037_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))));
		V_1 = 0;
		goto IL_002d;
	}

IL_001a:
	{
		ScoreU5BU5D_t2926278037* L_2 = V_0;
		int32_t L_3 = V_1;
		GcScoreDataU5BU5D_t1670395707* L_4 = ___result0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		Score_t3396031228 * L_6 = GcScoreData_ToScore_m2728389301(((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5))), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		ArrayElementTypeCheck (L_2, L_6);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (Score_t3396031228 *)L_6);
		int32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002d:
	{
		int32_t L_8 = V_1;
		GcScoreDataU5BU5D_t1670395707* L_9 = ___result0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t645920862 * L_10 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_ScoreLoaderCallback_6();
		ScoreU5BU5D_t2926278037* L_11 = V_0;
		NullCheck(L_10);
		Action_1_Invoke_m50340758(L_10, (IScoreU5BU5D_t250104726*)(IScoreU5BU5D_t250104726*)L_11, /*hidden argument*/Action_1_Invoke_m50340758_MethodInfo_var);
	}

IL_0041:
	{
		return;
	}
}
// UnityEngine.SocialPlatforms.ILocalUser UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::get_localUser()
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* LocalUser_t1307362368_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral48;
extern const uint32_t GameCenterPlatform_get_localUser_m1634439374_MetadataUsageId;
extern "C"  Il2CppObject * GameCenterPlatform_get_localUser_m1634439374 (GameCenterPlatform_t3570684786 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_get_localUser_m1634439374_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		LocalUser_t1307362368 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		LocalUser_t1307362368 * L_1 = (LocalUser_t1307362368 *)il2cpp_codegen_object_new(LocalUser_t1307362368_il2cpp_TypeInfo_var);
		LocalUser__ctor_m1052633066(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_m_LocalUser_13(L_1);
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		bool L_2 = GameCenterPlatform_Internal_Authenticated_m2780967960(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		LocalUser_t1307362368 * L_3 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		NullCheck(L_3);
		String_t* L_4 = UserProfile_get_id_m2095754825(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_4, _stringLiteral48, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		GameCenterPlatform_PopulateLocalUser_m2583301917(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_003c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		LocalUser_t1307362368 * L_6 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		return L_6;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::PopulateLocalUser()
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_PopulateLocalUser_m2583301917_MetadataUsageId;
extern "C"  void GameCenterPlatform_PopulateLocalUser_m2583301917 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_PopulateLocalUser_m2583301917_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		LocalUser_t1307362368 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		bool L_1 = GameCenterPlatform_Internal_Authenticated_m2780967960(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		LocalUser_SetAuthenticated_m653377406(L_0, L_1, /*hidden argument*/NULL);
		LocalUser_t1307362368 * L_2 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		String_t* L_3 = GameCenterPlatform_Internal_UserName_m1252299660(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		UserProfile_SetUserName_m914181770(L_2, L_3, /*hidden argument*/NULL);
		LocalUser_t1307362368 * L_4 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		String_t* L_5 = GameCenterPlatform_Internal_UserID_m385481212(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		UserProfile_SetUserID_m1515238170(L_4, L_5, /*hidden argument*/NULL);
		LocalUser_t1307362368 * L_6 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		bool L_7 = GameCenterPlatform_Internal_Underage_m4169738944(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		LocalUser_SetUnderage_m2968368872(L_6, L_7, /*hidden argument*/NULL);
		LocalUser_t1307362368 * L_8 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		Texture2D_t3884108195 * L_9 = GameCenterPlatform_Internal_UserImage_m3175776130(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		UserProfile_SetImage_m1928130753(L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadAchievementDescriptions(System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>)
extern Il2CppClass* AchievementDescriptionU5BU5D_t759444790_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2766540343_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LoadAchievementDescriptions_m232801667_MetadataUsageId;
extern "C"  void GameCenterPlatform_LoadAchievementDescriptions_m232801667 (GameCenterPlatform_t3570684786 * __this, Action_1_t229750097 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LoadAchievementDescriptions_m232801667_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4096949980(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t229750097 * L_1 = ___callback0;
		NullCheck(L_1);
		Action_1_Invoke_m2766540343(L_1, (IAchievementDescriptionU5BU5D_t4128901257*)(IAchievementDescriptionU5BU5D_t4128901257*)((AchievementDescriptionU5BU5D_t759444790*)SZArrayNew(AchievementDescriptionU5BU5D_t759444790_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/Action_1_Invoke_m2766540343_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t229750097 * L_2 = ___callback0;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_AchievementDescriptionLoaderCallback_2(L_2);
		GameCenterPlatform_Internal_LoadAchievementDescriptions_m1394384079(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ReportProgress(System.String,System.Double,System.Action`1<System.Boolean>)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const uint32_t GameCenterPlatform_ReportProgress_m4110499833_MetadataUsageId;
extern "C"  void GameCenterPlatform_ReportProgress_m4110499833 (GameCenterPlatform_t3570684786 * __this, String_t* ___id0, double ___progress1, Action_1_t872614854 * ___callback2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ReportProgress_m4110499833_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4096949980(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t872614854 * L_1 = ___callback2;
		NullCheck(L_1);
		Action_1_Invoke_m3594021162(L_1, (bool)0, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
		return;
	}

IL_0013:
	{
		Action_1_t872614854 * L_2 = ___callback2;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_ProgressCallback_4(L_2);
		String_t* L_3 = ___id0;
		double L_4 = ___progress1;
		GameCenterPlatform_Internal_ReportProgress_m2511520970(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadAchievements(System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>)
extern Il2CppClass* AchievementU5BU5D_t912418020_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m222677977_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LoadAchievements_m2745782249_MetadataUsageId;
extern "C"  void GameCenterPlatform_LoadAchievements_m2745782249 (GameCenterPlatform_t3570684786 * __this, Action_1_t2349069933 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LoadAchievements_m2745782249_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4096949980(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t2349069933 * L_1 = ___callback0;
		NullCheck(L_1);
		Action_1_Invoke_m222677977(L_1, (IAchievementU5BU5D_t1953253797*)(IAchievementU5BU5D_t1953253797*)((AchievementU5BU5D_t912418020*)SZArrayNew(AchievementU5BU5D_t912418020_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/Action_1_Invoke_m222677977_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t2349069933 * L_2 = ___callback0;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_AchievementLoaderCallback_3(L_2);
		GameCenterPlatform_Internal_LoadAchievements_m817891229(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ReportScore(System.Int64,System.String,System.Action`1<System.Boolean>)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const uint32_t GameCenterPlatform_ReportScore_m1009544586_MetadataUsageId;
extern "C"  void GameCenterPlatform_ReportScore_m1009544586 (GameCenterPlatform_t3570684786 * __this, int64_t ___score0, String_t* ___board1, Action_1_t872614854 * ___callback2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ReportScore_m1009544586_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4096949980(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t872614854 * L_1 = ___callback2;
		NullCheck(L_1);
		Action_1_Invoke_m3594021162(L_1, (bool)0, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
		return;
	}

IL_0013:
	{
		Action_1_t872614854 * L_2 = ___callback2;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_ScoreCallback_5(L_2);
		int64_t L_3 = ___score0;
		String_t* L_4 = ___board1;
		GameCenterPlatform_Internal_ReportScore_m408601947(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadScores(System.String,System.Action`1<UnityEngine.SocialPlatforms.IScore[]>)
extern Il2CppClass* ScoreU5BU5D_t2926278037_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m50340758_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LoadScores_m3562614827_MetadataUsageId;
extern "C"  void GameCenterPlatform_LoadScores_m3562614827 (GameCenterPlatform_t3570684786 * __this, String_t* ___category0, Action_1_t645920862 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LoadScores_m3562614827_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4096949980(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t645920862 * L_1 = ___callback1;
		NullCheck(L_1);
		Action_1_Invoke_m50340758(L_1, (IScoreU5BU5D_t250104726*)(IScoreU5BU5D_t250104726*)((ScoreU5BU5D_t2926278037*)SZArrayNew(ScoreU5BU5D_t2926278037_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/Action_1_Invoke_m50340758_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t645920862 * L_2 = ___callback1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_ScoreLoaderCallback_6(L_2);
		String_t* L_3 = ___category0;
		GameCenterPlatform_Internal_LoadScores_m523283944(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadScores(UnityEngine.SocialPlatforms.ILeaderboard,System.Action`1<System.Boolean>)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* Leaderboard_t1185876199_il2cpp_TypeInfo_var;
extern Il2CppClass* GcLeaderboard_t1820874799_il2cpp_TypeInfo_var;
extern Il2CppClass* ILeaderboard_t3799088250_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1416233310_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LoadScores_m2883394111_MetadataUsageId;
extern "C"  void GameCenterPlatform_LoadScores_m2883394111 (GameCenterPlatform_t3570684786 * __this, Il2CppObject * ___board0, Action_1_t872614854 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LoadScores_m2883394111_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Leaderboard_t1185876199 * V_0 = NULL;
	GcLeaderboard_t1820874799 * V_1 = NULL;
	Range_t1533311935  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Range_t1533311935  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4096949980(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t872614854 * L_1 = ___callback1;
		NullCheck(L_1);
		Action_1_Invoke_m3594021162(L_1, (bool)0, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
		return;
	}

IL_0013:
	{
		Action_1_t872614854 * L_2 = ___callback1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_LeaderboardCallback_7(L_2);
		Il2CppObject * L_3 = ___board0;
		V_0 = ((Leaderboard_t1185876199 *)CastclassClass(L_3, Leaderboard_t1185876199_il2cpp_TypeInfo_var));
		Leaderboard_t1185876199 * L_4 = V_0;
		GcLeaderboard_t1820874799 * L_5 = (GcLeaderboard_t1820874799 *)il2cpp_codegen_object_new(GcLeaderboard_t1820874799_il2cpp_TypeInfo_var);
		GcLeaderboard__ctor_m4042810199(L_5, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		List_1_t3189060351 * L_6 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_GcBoards_14();
		GcLeaderboard_t1820874799 * L_7 = V_1;
		NullCheck(L_6);
		List_1_Add_m1416233310(L_6, L_7, /*hidden argument*/List_1_Add_m1416233310_MethodInfo_var);
		Leaderboard_t1185876199 * L_8 = V_0;
		NullCheck(L_8);
		StringU5BU5D_t4054002952* L_9 = Leaderboard_GetUserFilter_m3119905721(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		GcLeaderboard_t1820874799 * L_10 = V_1;
		Il2CppObject * L_11 = ___board0;
		NullCheck(L_11);
		String_t* L_12 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String UnityEngine.SocialPlatforms.ILeaderboard::get_id() */, ILeaderboard_t3799088250_il2cpp_TypeInfo_var, L_11);
		Il2CppObject * L_13 = ___board0;
		NullCheck(L_13);
		int32_t L_14 = InterfaceFuncInvoker0< int32_t >::Invoke(3 /* UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.ILeaderboard::get_timeScope() */, ILeaderboard_t3799088250_il2cpp_TypeInfo_var, L_13);
		Leaderboard_t1185876199 * L_15 = V_0;
		NullCheck(L_15);
		StringU5BU5D_t4054002952* L_16 = Leaderboard_GetUserFilter_m3119905721(L_15, /*hidden argument*/NULL);
		NullCheck(L_10);
		GcLeaderboard_Internal_LoadScoresWithUsers_m1315210452(L_10, L_12, L_14, L_16, /*hidden argument*/NULL);
		goto IL_0091;
	}

IL_005d:
	{
		GcLeaderboard_t1820874799 * L_17 = V_1;
		Il2CppObject * L_18 = ___board0;
		NullCheck(L_18);
		String_t* L_19 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String UnityEngine.SocialPlatforms.ILeaderboard::get_id() */, ILeaderboard_t3799088250_il2cpp_TypeInfo_var, L_18);
		Il2CppObject * L_20 = ___board0;
		NullCheck(L_20);
		Range_t1533311935  L_21 = InterfaceFuncInvoker0< Range_t1533311935  >::Invoke(2 /* UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.ILeaderboard::get_range() */, ILeaderboard_t3799088250_il2cpp_TypeInfo_var, L_20);
		V_2 = L_21;
		int32_t L_22 = (&V_2)->get_from_0();
		Il2CppObject * L_23 = ___board0;
		NullCheck(L_23);
		Range_t1533311935  L_24 = InterfaceFuncInvoker0< Range_t1533311935  >::Invoke(2 /* UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.ILeaderboard::get_range() */, ILeaderboard_t3799088250_il2cpp_TypeInfo_var, L_23);
		V_3 = L_24;
		int32_t L_25 = (&V_3)->get_count_1();
		Il2CppObject * L_26 = ___board0;
		NullCheck(L_26);
		int32_t L_27 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.ILeaderboard::get_userScope() */, ILeaderboard_t3799088250_il2cpp_TypeInfo_var, L_26);
		Il2CppObject * L_28 = ___board0;
		NullCheck(L_28);
		int32_t L_29 = InterfaceFuncInvoker0< int32_t >::Invoke(3 /* UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.ILeaderboard::get_timeScope() */, ILeaderboard_t3799088250_il2cpp_TypeInfo_var, L_28);
		NullCheck(L_17);
		GcLeaderboard_Internal_LoadScores_m1783152707(L_17, L_19, L_22, L_25, L_27, L_29, /*hidden argument*/NULL);
	}

IL_0091:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LeaderboardCallbackWrapper(System.Boolean)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LeaderboardCallbackWrapper_m2165153529_MetadataUsageId;
extern "C"  void GameCenterPlatform_LeaderboardCallbackWrapper_m2165153529 (Il2CppObject * __this /* static, unused */, bool ___success0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LeaderboardCallbackWrapper_m2165153529_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_LeaderboardCallback_7();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_LeaderboardCallback_7();
		bool L_2 = ___success0;
		NullCheck(L_1);
		Action_1_Invoke_m3594021162(L_1, L_2, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::GetLoading(UnityEngine.SocialPlatforms.ILeaderboard)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* Leaderboard_t1185876199_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t3208733121_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m173797613_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3622080807_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4196185109_MethodInfo_var;
extern const uint32_t GameCenterPlatform_GetLoading_m2084830155_MetadataUsageId;
extern "C"  bool GameCenterPlatform_GetLoading_m2084830155 (GameCenterPlatform_t3570684786 * __this, Il2CppObject * ___board0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_GetLoading_m2084830155_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GcLeaderboard_t1820874799 * V_0 = NULL;
	Enumerator_t3208733121  V_1;
	memset(&V_1, 0, sizeof(V_1));
	bool V_2 = false;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4096949980(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		List_1_t3189060351 * L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_GcBoards_14();
		NullCheck(L_1);
		Enumerator_t3208733121  L_2 = List_1_GetEnumerator_m173797613(L_1, /*hidden argument*/List_1_GetEnumerator_m173797613_MethodInfo_var);
		V_1 = L_2;
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0042;
		}

IL_001d:
		{
			GcLeaderboard_t1820874799 * L_3 = Enumerator_get_Current_m3622080807((&V_1), /*hidden argument*/Enumerator_get_Current_m3622080807_MethodInfo_var);
			V_0 = L_3;
			GcLeaderboard_t1820874799 * L_4 = V_0;
			Il2CppObject * L_5 = ___board0;
			NullCheck(L_4);
			bool L_6 = GcLeaderboard_Contains_m100384368(L_4, ((Leaderboard_t1185876199 *)CastclassClass(L_5, Leaderboard_t1185876199_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			if (!L_6)
			{
				goto IL_0042;
			}
		}

IL_0036:
		{
			GcLeaderboard_t1820874799 * L_7 = V_0;
			NullCheck(L_7);
			bool L_8 = GcLeaderboard_Loading_m294711596(L_7, /*hidden argument*/NULL);
			V_2 = L_8;
			IL2CPP_LEAVE(0x61, FINALLY_0053);
		}

IL_0042:
		{
			bool L_9 = Enumerator_MoveNext_m4196185109((&V_1), /*hidden argument*/Enumerator_MoveNext_m4196185109_MethodInfo_var);
			if (L_9)
			{
				goto IL_001d;
			}
		}

IL_004e:
		{
			IL2CPP_LEAVE(0x5F, FINALLY_0053);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0053;
	}

FINALLY_0053:
	{ // begin finally (depth: 1)
		Enumerator_t3208733121  L_10 = V_1;
		Enumerator_t3208733121  L_11 = L_10;
		Il2CppObject * L_12 = Box(Enumerator_t3208733121_il2cpp_TypeInfo_var, &L_11);
		NullCheck((Il2CppObject *)L_12);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
		IL2CPP_END_FINALLY(83)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(83)
	{
		IL2CPP_JUMP_TBL(0x61, IL_0061)
		IL2CPP_JUMP_TBL(0x5F, IL_005f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_005f:
	{
		return (bool)0;
	}

IL_0061:
	{
		bool L_13 = V_2;
		return L_13;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::VerifyAuthentication()
extern Il2CppClass* ILocalUser_t2654168339_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral708567292;
extern const uint32_t GameCenterPlatform_VerifyAuthentication_m4096949980_MetadataUsageId;
extern "C"  bool GameCenterPlatform_VerifyAuthentication_m4096949980 (GameCenterPlatform_t3570684786 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_VerifyAuthentication_m4096949980_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = GameCenterPlatform_get_localUser_m1634439374(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean UnityEngine.SocialPlatforms.ILocalUser::get_authenticated() */, ILocalUser_t2654168339_il2cpp_TypeInfo_var, L_0);
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral708567292, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_001c:
	{
		return (bool)1;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowAchievementsUI()
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ShowAchievementsUI_m2437339590_MetadataUsageId;
extern "C"  void GameCenterPlatform_ShowAchievementsUI_m2437339590 (GameCenterPlatform_t3570684786 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ShowAchievementsUI_m2437339590_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowAchievementsUI_m1934331464(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowLeaderboardUI()
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ShowLeaderboardUI_m302984165_MetadataUsageId;
extern "C"  void GameCenterPlatform_ShowLeaderboardUI_m302984165 (GameCenterPlatform_t3570684786 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ShowLeaderboardUI_m302984165_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowLeaderboardUI_m3057704739(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ClearUsers(System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ClearUsers_m4212910653_MetadataUsageId;
extern "C"  void GameCenterPlatform_ClearUsers_m4212910653 (Il2CppObject * __this /* static, unused */, int32_t ___size0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ClearUsers_m4212910653_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		int32_t L_0 = ___size0;
		GameCenterPlatform_SafeClearArray_m2546851889(NULL /*static, unused*/, (((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_users_11()), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetUser(UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData,System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SetUser_m847940592_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetUser_m847940592 (Il2CppObject * __this /* static, unused */, GcUserProfileData_t657441114  ___data0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetUser_m847940592_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		int32_t L_0 = ___number1;
		GcUserProfileData_AddToArray_m3757655355((&___data0), (((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_users_11()), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetUserImage(UnityEngine.Texture2D,System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SetUserImage_m3066589434_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetUserImage_m3066589434 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___texture0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetUserImage_m3066589434_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Texture2D_t3884108195 * L_0 = ___texture0;
		int32_t L_1 = ___number1;
		GameCenterPlatform_SafeSetUserImage_m3650098397(NULL /*static, unused*/, (((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_users_11()), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerUsersCallbackWrapper()
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2276731850_MethodInfo_var;
extern const uint32_t GameCenterPlatform_TriggerUsersCallbackWrapper_m2446471631_MetadataUsageId;
extern "C"  void GameCenterPlatform_TriggerUsersCallbackWrapper_m2446471631 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_TriggerUsersCallbackWrapper_m2446471631_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t3814920354 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_UsersCallback_8();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t3814920354 * L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_UsersCallback_8();
		UserProfileU5BU5D_t2378268441* L_2 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_users_11();
		NullCheck(L_1);
		Action_1_Invoke_m2276731850(L_1, (IUserProfileU5BU5D_t3419104218*)(IUserProfileU5BU5D_t3419104218*)L_2, /*hidden argument*/Action_1_Invoke_m2276731850_MethodInfo_var);
	}

IL_0019:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadUsers(System.String[],System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>)
extern Il2CppClass* UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2276731850_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LoadUsers_m981272890_MetadataUsageId;
extern "C"  void GameCenterPlatform_LoadUsers_m981272890 (GameCenterPlatform_t3570684786 * __this, StringU5BU5D_t4054002952* ___userIds0, Action_1_t3814920354 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LoadUsers_m981272890_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4096949980(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t3814920354 * L_1 = ___callback1;
		NullCheck(L_1);
		Action_1_Invoke_m2276731850(L_1, (IUserProfileU5BU5D_t3419104218*)(IUserProfileU5BU5D_t3419104218*)((UserProfileU5BU5D_t2378268441*)SZArrayNew(UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/Action_1_Invoke_m2276731850_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t3814920354 * L_2 = ___callback1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_UsersCallback_8(L_2);
		StringU5BU5D_t4054002952* L_3 = ___userIds0;
		GameCenterPlatform_Internal_LoadUsers_m4218820079(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SafeSetUserImage(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,UnityEngine.Texture2D,System.Int32)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* Texture2D_t3884108195_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral183309550;
extern Il2CppCodeGenString* _stringLiteral2135574971;
extern const uint32_t GameCenterPlatform_SafeSetUserImage_m3650098397_MetadataUsageId;
extern "C"  void GameCenterPlatform_SafeSetUserImage_m3650098397 (Il2CppObject * __this /* static, unused */, UserProfileU5BU5D_t2378268441** ___array0, Texture2D_t3884108195 * ___texture1, int32_t ___number2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SafeSetUserImage_m3650098397_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UserProfileU5BU5D_t2378268441** L_0 = ___array0;
		NullCheck((*((UserProfileU5BU5D_t2378268441**)L_0)));
		int32_t L_1 = ___number2;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)(*((UserProfileU5BU5D_t2378268441**)L_0)))->max_length))))) <= ((int32_t)L_1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = ___number2;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0026;
		}
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral183309550, /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_3 = (Texture2D_t3884108195 *)il2cpp_codegen_object_new(Texture2D_t3884108195_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1883511258(L_3, ((int32_t)76), ((int32_t)76), /*hidden argument*/NULL);
		___texture1 = L_3;
	}

IL_0026:
	{
		UserProfileU5BU5D_t2378268441** L_4 = ___array0;
		NullCheck((*((UserProfileU5BU5D_t2378268441**)L_4)));
		int32_t L_5 = ___number2;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)(*((UserProfileU5BU5D_t2378268441**)L_4)))->max_length))))) <= ((int32_t)L_5)))
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_6 = ___number2;
		if ((((int32_t)L_6) < ((int32_t)0)))
		{
			goto IL_0046;
		}
	}
	{
		UserProfileU5BU5D_t2378268441** L_7 = ___array0;
		int32_t L_8 = ___number2;
		NullCheck((*((UserProfileU5BU5D_t2378268441**)L_7)));
		IL2CPP_ARRAY_BOUNDS_CHECK((*((UserProfileU5BU5D_t2378268441**)L_7)), L_8);
		int32_t L_9 = L_8;
		UserProfile_t2280656072 * L_10 = ((*((UserProfileU5BU5D_t2378268441**)L_7)))->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		Texture2D_t3884108195 * L_11 = ___texture1;
		NullCheck(L_10);
		UserProfile_SetImage_m1928130753(L_10, L_11, /*hidden argument*/NULL);
		goto IL_0050;
	}

IL_0046:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral2135574971, /*hidden argument*/NULL);
	}

IL_0050:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SafeClearArray(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,System.Int32)
extern Il2CppClass* UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SafeClearArray_m2546851889_MetadataUsageId;
extern "C"  void GameCenterPlatform_SafeClearArray_m2546851889 (Il2CppObject * __this /* static, unused */, UserProfileU5BU5D_t2378268441** ___array0, int32_t ___size1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SafeClearArray_m2546851889_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UserProfileU5BU5D_t2378268441** L_0 = ___array0;
		if (!(*((UserProfileU5BU5D_t2378268441**)L_0)))
		{
			goto IL_0011;
		}
	}
	{
		UserProfileU5BU5D_t2378268441** L_1 = ___array0;
		NullCheck((*((UserProfileU5BU5D_t2378268441**)L_1)));
		int32_t L_2 = ___size1;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)(*((UserProfileU5BU5D_t2378268441**)L_1)))->max_length))))) == ((int32_t)L_2)))
		{
			goto IL_0019;
		}
	}

IL_0011:
	{
		UserProfileU5BU5D_t2378268441** L_3 = ___array0;
		int32_t L_4 = ___size1;
		*((Il2CppObject **)(L_3)) = (Il2CppObject *)((UserProfileU5BU5D_t2378268441*)SZArrayNew(UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var, (uint32_t)L_4));
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_3), (Il2CppObject *)((UserProfileU5BU5D_t2378268441*)SZArrayNew(UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var, (uint32_t)L_4)));
	}

IL_0019:
	{
		return;
	}
}
// UnityEngine.SocialPlatforms.ILeaderboard UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::CreateLeaderboard()
extern Il2CppClass* Leaderboard_t1185876199_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_CreateLeaderboard_m2295049883_MetadataUsageId;
extern "C"  Il2CppObject * GameCenterPlatform_CreateLeaderboard_m2295049883 (GameCenterPlatform_t3570684786 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_CreateLeaderboard_m2295049883_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Leaderboard_t1185876199 * V_0 = NULL;
	{
		Leaderboard_t1185876199 * L_0 = (Leaderboard_t1185876199 *)il2cpp_codegen_object_new(Leaderboard_t1185876199_il2cpp_TypeInfo_var);
		Leaderboard__ctor_m596857571(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Leaderboard_t1185876199 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.SocialPlatforms.IAchievement UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::CreateAchievement()
extern Il2CppClass* Achievement_t344600729_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_CreateAchievement_m1828880347_MetadataUsageId;
extern "C"  Il2CppObject * GameCenterPlatform_CreateAchievement_m1828880347 (GameCenterPlatform_t3570684786 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_CreateAchievement_m1828880347_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Achievement_t344600729 * V_0 = NULL;
	{
		Achievement_t344600729 * L_0 = (Achievement_t344600729 *)il2cpp_codegen_object_new(Achievement_t344600729_il2cpp_TypeInfo_var);
		Achievement__ctor_m3345265521(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Achievement_t344600729 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerResetAchievementCallback(System.Boolean)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const uint32_t GameCenterPlatform_TriggerResetAchievementCallback_m1285257317_MetadataUsageId;
extern "C"  void GameCenterPlatform_TriggerResetAchievementCallback_m1285257317 (Il2CppObject * __this /* static, unused */, bool ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_TriggerResetAchievementCallback_m1285257317_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_ResetAchievements_12();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_ResetAchievements_12();
		bool L_2 = ___result0;
		NullCheck(L_1);
		Action_1_Invoke_m3594021162(L_1, L_2, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// UnityEngine.SocialPlatforms.Impl.Achievement UnityEngine.SocialPlatforms.GameCenter.GcAchievementData::ToAchievement()
extern Il2CppClass* Achievement_t344600729_il2cpp_TypeInfo_var;
extern const uint32_t GcAchievementData_ToAchievement_m3239514930_MetadataUsageId;
extern "C"  Achievement_t344600729 * GcAchievementData_ToAchievement_m3239514930 (GcAchievementData_t3481375915 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GcAchievementData_ToAchievement_m3239514930_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t4283661327  V_0;
	memset(&V_0, 0, sizeof(V_0));
	double G_B2_0 = 0.0;
	String_t* G_B2_1 = NULL;
	double G_B1_0 = 0.0;
	String_t* G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	double G_B3_1 = 0.0;
	String_t* G_B3_2 = NULL;
	int32_t G_B5_0 = 0;
	double G_B5_1 = 0.0;
	String_t* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	double G_B4_1 = 0.0;
	String_t* G_B4_2 = NULL;
	int32_t G_B6_0 = 0;
	int32_t G_B6_1 = 0;
	double G_B6_2 = 0.0;
	String_t* G_B6_3 = NULL;
	{
		String_t* L_0 = __this->get_m_Identifier_0();
		double L_1 = __this->get_m_PercentCompleted_1();
		int32_t L_2 = __this->get_m_Completed_2();
		G_B1_0 = L_1;
		G_B1_1 = L_0;
		if (L_2)
		{
			G_B2_0 = L_1;
			G_B2_1 = L_0;
			goto IL_001d;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_001e:
	{
		int32_t L_3 = __this->get_m_Hidden_3();
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
		G_B4_2 = G_B3_2;
		if (L_3)
		{
			G_B5_0 = G_B3_0;
			G_B5_1 = G_B3_1;
			G_B5_2 = G_B3_2;
			goto IL_002f;
		}
	}
	{
		G_B6_0 = 0;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0030;
	}

IL_002f:
	{
		G_B6_0 = 1;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0030:
	{
		DateTime__ctor_m1594789867((&V_0), ((int32_t)1970), 1, 1, 0, 0, 0, 0, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_m_LastReportedDate_4();
		DateTime_t4283661327  L_5 = DateTime_AddSeconds_m2515640243((&V_0), (((double)((double)L_4))), /*hidden argument*/NULL);
		Achievement_t344600729 * L_6 = (Achievement_t344600729 *)il2cpp_codegen_object_new(Achievement_t344600729_il2cpp_TypeInfo_var);
		Achievement__ctor_m377036415(L_6, G_B6_3, G_B6_2, (bool)G_B6_1, (bool)G_B6_0, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  Achievement_t344600729 * GcAchievementData_ToAchievement_m3239514930_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	GcAchievementData_t3481375915 * _thisAdjusted = reinterpret_cast<GcAchievementData_t3481375915 *>(__this + 1);
	return GcAchievementData_ToAchievement_m3239514930(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern "C" void GcAchievementData_t3481375915_marshal_pinvoke(const GcAchievementData_t3481375915& unmarshaled, GcAchievementData_t3481375915_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Identifier_0 = il2cpp_codegen_marshal_string(unmarshaled.get_m_Identifier_0());
	marshaled.___m_PercentCompleted_1 = unmarshaled.get_m_PercentCompleted_1();
	marshaled.___m_Completed_2 = unmarshaled.get_m_Completed_2();
	marshaled.___m_Hidden_3 = unmarshaled.get_m_Hidden_3();
	marshaled.___m_LastReportedDate_4 = unmarshaled.get_m_LastReportedDate_4();
}
extern "C" void GcAchievementData_t3481375915_marshal_pinvoke_back(const GcAchievementData_t3481375915_marshaled_pinvoke& marshaled, GcAchievementData_t3481375915& unmarshaled)
{
	unmarshaled.set_m_Identifier_0(il2cpp_codegen_marshal_string_result(marshaled.___m_Identifier_0));
	double unmarshaled_m_PercentCompleted_temp_1 = 0.0;
	unmarshaled_m_PercentCompleted_temp_1 = marshaled.___m_PercentCompleted_1;
	unmarshaled.set_m_PercentCompleted_1(unmarshaled_m_PercentCompleted_temp_1);
	int32_t unmarshaled_m_Completed_temp_2 = 0;
	unmarshaled_m_Completed_temp_2 = marshaled.___m_Completed_2;
	unmarshaled.set_m_Completed_2(unmarshaled_m_Completed_temp_2);
	int32_t unmarshaled_m_Hidden_temp_3 = 0;
	unmarshaled_m_Hidden_temp_3 = marshaled.___m_Hidden_3;
	unmarshaled.set_m_Hidden_3(unmarshaled_m_Hidden_temp_3);
	int32_t unmarshaled_m_LastReportedDate_temp_4 = 0;
	unmarshaled_m_LastReportedDate_temp_4 = marshaled.___m_LastReportedDate_4;
	unmarshaled.set_m_LastReportedDate_4(unmarshaled_m_LastReportedDate_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern "C" void GcAchievementData_t3481375915_marshal_pinvoke_cleanup(GcAchievementData_t3481375915_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_Identifier_0);
	marshaled.___m_Identifier_0 = NULL;
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern "C" void GcAchievementData_t3481375915_marshal_com(const GcAchievementData_t3481375915& unmarshaled, GcAchievementData_t3481375915_marshaled_com& marshaled)
{
	marshaled.___m_Identifier_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_m_Identifier_0());
	marshaled.___m_PercentCompleted_1 = unmarshaled.get_m_PercentCompleted_1();
	marshaled.___m_Completed_2 = unmarshaled.get_m_Completed_2();
	marshaled.___m_Hidden_3 = unmarshaled.get_m_Hidden_3();
	marshaled.___m_LastReportedDate_4 = unmarshaled.get_m_LastReportedDate_4();
}
extern "C" void GcAchievementData_t3481375915_marshal_com_back(const GcAchievementData_t3481375915_marshaled_com& marshaled, GcAchievementData_t3481375915& unmarshaled)
{
	unmarshaled.set_m_Identifier_0(il2cpp_codegen_marshal_bstring_result(marshaled.___m_Identifier_0));
	double unmarshaled_m_PercentCompleted_temp_1 = 0.0;
	unmarshaled_m_PercentCompleted_temp_1 = marshaled.___m_PercentCompleted_1;
	unmarshaled.set_m_PercentCompleted_1(unmarshaled_m_PercentCompleted_temp_1);
	int32_t unmarshaled_m_Completed_temp_2 = 0;
	unmarshaled_m_Completed_temp_2 = marshaled.___m_Completed_2;
	unmarshaled.set_m_Completed_2(unmarshaled_m_Completed_temp_2);
	int32_t unmarshaled_m_Hidden_temp_3 = 0;
	unmarshaled_m_Hidden_temp_3 = marshaled.___m_Hidden_3;
	unmarshaled.set_m_Hidden_3(unmarshaled_m_Hidden_temp_3);
	int32_t unmarshaled_m_LastReportedDate_temp_4 = 0;
	unmarshaled_m_LastReportedDate_temp_4 = marshaled.___m_LastReportedDate_4;
	unmarshaled.set_m_LastReportedDate_4(unmarshaled_m_LastReportedDate_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern "C" void GcAchievementData_t3481375915_marshal_com_cleanup(GcAchievementData_t3481375915_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___m_Identifier_0);
	marshaled.___m_Identifier_0 = NULL;
}
// UnityEngine.SocialPlatforms.Impl.AchievementDescription UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData::ToAchievementDescription()
extern Il2CppClass* AchievementDescription_t2116066607_il2cpp_TypeInfo_var;
extern const uint32_t GcAchievementDescriptionData_ToAchievementDescription_m3125480712_MetadataUsageId;
extern "C"  AchievementDescription_t2116066607 * GcAchievementDescriptionData_ToAchievementDescription_m3125480712 (GcAchievementDescriptionData_t2242891083 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GcAchievementDescriptionData_ToAchievementDescription_m3125480712_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B2_1 = NULL;
	Texture2D_t3884108195 * G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	String_t* G_B2_4 = NULL;
	String_t* G_B1_0 = NULL;
	String_t* G_B1_1 = NULL;
	Texture2D_t3884108195 * G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	String_t* G_B1_4 = NULL;
	int32_t G_B3_0 = 0;
	String_t* G_B3_1 = NULL;
	String_t* G_B3_2 = NULL;
	Texture2D_t3884108195 * G_B3_3 = NULL;
	String_t* G_B3_4 = NULL;
	String_t* G_B3_5 = NULL;
	{
		String_t* L_0 = __this->get_m_Identifier_0();
		String_t* L_1 = __this->get_m_Title_1();
		Texture2D_t3884108195 * L_2 = __this->get_m_Image_2();
		String_t* L_3 = __this->get_m_AchievedDescription_3();
		String_t* L_4 = __this->get_m_UnachievedDescription_4();
		int32_t L_5 = __this->get_m_Hidden_5();
		G_B1_0 = L_4;
		G_B1_1 = L_3;
		G_B1_2 = L_2;
		G_B1_3 = L_1;
		G_B1_4 = L_0;
		if (L_5)
		{
			G_B2_0 = L_4;
			G_B2_1 = L_3;
			G_B2_2 = L_2;
			G_B2_3 = L_1;
			G_B2_4 = L_0;
			goto IL_002f;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		G_B3_5 = G_B1_4;
		goto IL_0030;
	}

IL_002f:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
		G_B3_5 = G_B2_4;
	}

IL_0030:
	{
		int32_t L_6 = __this->get_m_Points_6();
		AchievementDescription_t2116066607 * L_7 = (AchievementDescription_t2116066607 *)il2cpp_codegen_object_new(AchievementDescription_t2116066607_il2cpp_TypeInfo_var);
		AchievementDescription__ctor_m3032164909(L_7, G_B3_5, G_B3_4, G_B3_3, G_B3_2, G_B3_1, (bool)G_B3_0, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  AchievementDescription_t2116066607 * GcAchievementDescriptionData_ToAchievementDescription_m3125480712_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	GcAchievementDescriptionData_t2242891083 * _thisAdjusted = reinterpret_cast<GcAchievementDescriptionData_t2242891083 *>(__this + 1);
	return GcAchievementDescriptionData_ToAchievementDescription_m3125480712(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
extern "C" void GcAchievementDescriptionData_t2242891083_marshal_pinvoke(const GcAchievementDescriptionData_t2242891083& unmarshaled, GcAchievementDescriptionData_t2242891083_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Image_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Image' of type 'GcAchievementDescriptionData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Image_2Exception);
}
extern "C" void GcAchievementDescriptionData_t2242891083_marshal_pinvoke_back(const GcAchievementDescriptionData_t2242891083_marshaled_pinvoke& marshaled, GcAchievementDescriptionData_t2242891083& unmarshaled)
{
	Il2CppCodeGenException* ___m_Image_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Image' of type 'GcAchievementDescriptionData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Image_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
extern "C" void GcAchievementDescriptionData_t2242891083_marshal_pinvoke_cleanup(GcAchievementDescriptionData_t2242891083_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
extern "C" void GcAchievementDescriptionData_t2242891083_marshal_com(const GcAchievementDescriptionData_t2242891083& unmarshaled, GcAchievementDescriptionData_t2242891083_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Image_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Image' of type 'GcAchievementDescriptionData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Image_2Exception);
}
extern "C" void GcAchievementDescriptionData_t2242891083_marshal_com_back(const GcAchievementDescriptionData_t2242891083_marshaled_com& marshaled, GcAchievementDescriptionData_t2242891083& unmarshaled)
{
	Il2CppCodeGenException* ___m_Image_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Image' of type 'GcAchievementDescriptionData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Image_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
extern "C" void GcAchievementDescriptionData_t2242891083_marshal_com_cleanup(GcAchievementDescriptionData_t2242891083_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::.ctor(UnityEngine.SocialPlatforms.Impl.Leaderboard)
extern "C"  void GcLeaderboard__ctor_m4042810199 (GcLeaderboard_t1820874799 * __this, Leaderboard_t1185876199 * ___board0, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Leaderboard_t1185876199 * L_0 = ___board0;
		__this->set_m_GenericLeaderboard_1(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Finalize()
extern "C"  void GcLeaderboard_Finalize_m4015840938 (GcLeaderboard_t1820874799 * __this, const MethodInfo* method)
{
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		GcLeaderboard_Dispose_m301614325(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m3027285644(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Contains(UnityEngine.SocialPlatforms.Impl.Leaderboard)
extern "C"  bool GcLeaderboard_Contains_m100384368 (GcLeaderboard_t1820874799 * __this, Leaderboard_t1185876199 * ___board0, const MethodInfo* method)
{
	{
		Leaderboard_t1185876199 * L_0 = __this->get_m_GenericLeaderboard_1();
		Leaderboard_t1185876199 * L_1 = ___board0;
		return (bool)((((Il2CppObject*)(Leaderboard_t1185876199 *)L_0) == ((Il2CppObject*)(Leaderboard_t1185876199 *)L_1))? 1 : 0);
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetScores(UnityEngine.SocialPlatforms.GameCenter.GcScoreData[])
extern Il2CppClass* ScoreU5BU5D_t2926278037_il2cpp_TypeInfo_var;
extern const uint32_t GcLeaderboard_SetScores_m1734279820_MetadataUsageId;
extern "C"  void GcLeaderboard_SetScores_m1734279820 (GcLeaderboard_t1820874799 * __this, GcScoreDataU5BU5D_t1670395707* ___scoreDatas0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GcLeaderboard_SetScores_m1734279820_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ScoreU5BU5D_t2926278037* V_0 = NULL;
	int32_t V_1 = 0;
	{
		Leaderboard_t1185876199 * L_0 = __this->get_m_GenericLeaderboard_1();
		if (!L_0)
		{
			goto IL_0043;
		}
	}
	{
		GcScoreDataU5BU5D_t1670395707* L_1 = ___scoreDatas0;
		NullCheck(L_1);
		V_0 = ((ScoreU5BU5D_t2926278037*)SZArrayNew(ScoreU5BU5D_t2926278037_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))));
		V_1 = 0;
		goto IL_002e;
	}

IL_001b:
	{
		ScoreU5BU5D_t2926278037* L_2 = V_0;
		int32_t L_3 = V_1;
		GcScoreDataU5BU5D_t1670395707* L_4 = ___scoreDatas0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		Score_t3396031228 * L_6 = GcScoreData_ToScore_m2728389301(((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5))), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		ArrayElementTypeCheck (L_2, L_6);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (Score_t3396031228 *)L_6);
		int32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_8 = V_1;
		GcScoreDataU5BU5D_t1670395707* L_9 = ___scoreDatas0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_001b;
		}
	}
	{
		Leaderboard_t1185876199 * L_10 = __this->get_m_GenericLeaderboard_1();
		ScoreU5BU5D_t2926278037* L_11 = V_0;
		NullCheck(L_10);
		Leaderboard_SetScores_m1463319879(L_10, (IScoreU5BU5D_t250104726*)(IScoreU5BU5D_t250104726*)L_11, /*hidden argument*/NULL);
	}

IL_0043:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetLocalScore(UnityEngine.SocialPlatforms.GameCenter.GcScoreData)
extern "C"  void GcLeaderboard_SetLocalScore_m3970132532 (GcLeaderboard_t1820874799 * __this, GcScoreData_t2181296590  ___scoreData0, const MethodInfo* method)
{
	{
		Leaderboard_t1185876199 * L_0 = __this->get_m_GenericLeaderboard_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Leaderboard_t1185876199 * L_1 = __this->get_m_GenericLeaderboard_1();
		Score_t3396031228 * L_2 = GcScoreData_ToScore_m2728389301((&___scoreData0), /*hidden argument*/NULL);
		NullCheck(L_1);
		Leaderboard_SetLocalUserScore_m700491556(L_1, L_2, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetMaxRange(System.UInt32)
extern "C"  void GcLeaderboard_SetMaxRange_m3160374921 (GcLeaderboard_t1820874799 * __this, uint32_t ___maxRange0, const MethodInfo* method)
{
	{
		Leaderboard_t1185876199 * L_0 = __this->get_m_GenericLeaderboard_1();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Leaderboard_t1185876199 * L_1 = __this->get_m_GenericLeaderboard_1();
		uint32_t L_2 = ___maxRange0;
		NullCheck(L_1);
		Leaderboard_SetMaxRange_m3779908734(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetTitle(System.String)
extern "C"  void GcLeaderboard_SetTitle_m3781988000 (GcLeaderboard_t1820874799 * __this, String_t* ___title0, const MethodInfo* method)
{
	{
		Leaderboard_t1185876199 * L_0 = __this->get_m_GenericLeaderboard_1();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Leaderboard_t1185876199 * L_1 = __this->get_m_GenericLeaderboard_1();
		String_t* L_2 = ___title0;
		NullCheck(L_1);
		Leaderboard_SetTitle_m771163339(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScores(System.String,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void GcLeaderboard_Internal_LoadScores_m1783152707 (GcLeaderboard_t1820874799 * __this, String_t* ___category0, int32_t ___from1, int32_t ___count2, int32_t ___playerScope3, int32_t ___timeScope4, const MethodInfo* method)
{
	typedef void (*GcLeaderboard_Internal_LoadScores_m1783152707_ftn) (GcLeaderboard_t1820874799 *, String_t*, int32_t, int32_t, int32_t, int32_t);
	static GcLeaderboard_Internal_LoadScores_m1783152707_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Internal_LoadScores_m1783152707_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScores(System.String,System.Int32,System.Int32,System.Int32,System.Int32)");
	_il2cpp_icall_func(__this, ___category0, ___from1, ___count2, ___playerScope3, ___timeScope4);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScoresWithUsers(System.String,System.Int32,System.String[])
extern "C"  void GcLeaderboard_Internal_LoadScoresWithUsers_m1315210452 (GcLeaderboard_t1820874799 * __this, String_t* ___category0, int32_t ___timeScope1, StringU5BU5D_t4054002952* ___userIDs2, const MethodInfo* method)
{
	typedef void (*GcLeaderboard_Internal_LoadScoresWithUsers_m1315210452_ftn) (GcLeaderboard_t1820874799 *, String_t*, int32_t, StringU5BU5D_t4054002952*);
	static GcLeaderboard_Internal_LoadScoresWithUsers_m1315210452_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Internal_LoadScoresWithUsers_m1315210452_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScoresWithUsers(System.String,System.Int32,System.String[])");
	_il2cpp_icall_func(__this, ___category0, ___timeScope1, ___userIDs2);
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Loading()
extern "C"  bool GcLeaderboard_Loading_m294711596 (GcLeaderboard_t1820874799 * __this, const MethodInfo* method)
{
	typedef bool (*GcLeaderboard_Loading_m294711596_ftn) (GcLeaderboard_t1820874799 *);
	static GcLeaderboard_Loading_m294711596_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Loading_m294711596_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Loading()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Dispose()
extern "C"  void GcLeaderboard_Dispose_m301614325 (GcLeaderboard_t1820874799 * __this, const MethodInfo* method)
{
	typedef void (*GcLeaderboard_Dispose_m301614325_ftn) (GcLeaderboard_t1820874799 *);
	static GcLeaderboard_Dispose_m301614325_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Dispose_m301614325_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Dispose()");
	_il2cpp_icall_func(__this);
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
extern "C" void GcLeaderboard_t1820874799_marshal_pinvoke(const GcLeaderboard_t1820874799& unmarshaled, GcLeaderboard_t1820874799_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_GenericLeaderboard_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_GenericLeaderboard' of type 'GcLeaderboard': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_GenericLeaderboard_1Exception);
}
extern "C" void GcLeaderboard_t1820874799_marshal_pinvoke_back(const GcLeaderboard_t1820874799_marshaled_pinvoke& marshaled, GcLeaderboard_t1820874799& unmarshaled)
{
	Il2CppCodeGenException* ___m_GenericLeaderboard_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_GenericLeaderboard' of type 'GcLeaderboard': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_GenericLeaderboard_1Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
extern "C" void GcLeaderboard_t1820874799_marshal_pinvoke_cleanup(GcLeaderboard_t1820874799_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
extern "C" void GcLeaderboard_t1820874799_marshal_com(const GcLeaderboard_t1820874799& unmarshaled, GcLeaderboard_t1820874799_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_GenericLeaderboard_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_GenericLeaderboard' of type 'GcLeaderboard': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_GenericLeaderboard_1Exception);
}
extern "C" void GcLeaderboard_t1820874799_marshal_com_back(const GcLeaderboard_t1820874799_marshaled_com& marshaled, GcLeaderboard_t1820874799& unmarshaled)
{
	Il2CppCodeGenException* ___m_GenericLeaderboard_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_GenericLeaderboard' of type 'GcLeaderboard': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_GenericLeaderboard_1Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
extern "C" void GcLeaderboard_t1820874799_marshal_com_cleanup(GcLeaderboard_t1820874799_marshaled_com& marshaled)
{
}
// UnityEngine.SocialPlatforms.Impl.Score UnityEngine.SocialPlatforms.GameCenter.GcScoreData::ToScore()
extern Il2CppClass* Score_t3396031228_il2cpp_TypeInfo_var;
extern const uint32_t GcScoreData_ToScore_m2728389301_MetadataUsageId;
extern "C"  Score_t3396031228 * GcScoreData_ToScore_m2728389301 (GcScoreData_t2181296590 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GcScoreData_ToScore_m2728389301_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t4283661327  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		String_t* L_0 = __this->get_m_Category_0();
		int32_t L_1 = __this->get_m_ValueHigh_2();
		int32_t L_2 = __this->get_m_ValueLow_1();
		String_t* L_3 = __this->get_m_PlayerID_5();
		DateTime__ctor_m1594789867((&V_0), ((int32_t)1970), 1, 1, 0, 0, 0, 0, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_m_Date_3();
		DateTime_t4283661327  L_5 = DateTime_AddSeconds_m2515640243((&V_0), (((double)((double)L_4))), /*hidden argument*/NULL);
		String_t* L_6 = __this->get_m_FormattedValue_4();
		int32_t L_7 = __this->get_m_Rank_6();
		Score_t3396031228 * L_8 = (Score_t3396031228 *)il2cpp_codegen_object_new(Score_t3396031228_il2cpp_TypeInfo_var);
		Score__ctor_m3768037481(L_8, L_0, ((int64_t)((int64_t)((int64_t)((int64_t)(((int64_t)((int64_t)L_1)))<<(int32_t)((int32_t)32)))+(int64_t)(((int64_t)((int64_t)L_2))))), L_3, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
extern "C"  Score_t3396031228 * GcScoreData_ToScore_m2728389301_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	GcScoreData_t2181296590 * _thisAdjusted = reinterpret_cast<GcScoreData_t2181296590 *>(__this + 1);
	return GcScoreData_ToScore_m2728389301(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcScoreData
extern "C" void GcScoreData_t2181296590_marshal_pinvoke(const GcScoreData_t2181296590& unmarshaled, GcScoreData_t2181296590_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Category_0 = il2cpp_codegen_marshal_string(unmarshaled.get_m_Category_0());
	marshaled.___m_ValueLow_1 = unmarshaled.get_m_ValueLow_1();
	marshaled.___m_ValueHigh_2 = unmarshaled.get_m_ValueHigh_2();
	marshaled.___m_Date_3 = unmarshaled.get_m_Date_3();
	marshaled.___m_FormattedValue_4 = il2cpp_codegen_marshal_string(unmarshaled.get_m_FormattedValue_4());
	marshaled.___m_PlayerID_5 = il2cpp_codegen_marshal_string(unmarshaled.get_m_PlayerID_5());
	marshaled.___m_Rank_6 = unmarshaled.get_m_Rank_6();
}
extern "C" void GcScoreData_t2181296590_marshal_pinvoke_back(const GcScoreData_t2181296590_marshaled_pinvoke& marshaled, GcScoreData_t2181296590& unmarshaled)
{
	unmarshaled.set_m_Category_0(il2cpp_codegen_marshal_string_result(marshaled.___m_Category_0));
	int32_t unmarshaled_m_ValueLow_temp_1 = 0;
	unmarshaled_m_ValueLow_temp_1 = marshaled.___m_ValueLow_1;
	unmarshaled.set_m_ValueLow_1(unmarshaled_m_ValueLow_temp_1);
	int32_t unmarshaled_m_ValueHigh_temp_2 = 0;
	unmarshaled_m_ValueHigh_temp_2 = marshaled.___m_ValueHigh_2;
	unmarshaled.set_m_ValueHigh_2(unmarshaled_m_ValueHigh_temp_2);
	int32_t unmarshaled_m_Date_temp_3 = 0;
	unmarshaled_m_Date_temp_3 = marshaled.___m_Date_3;
	unmarshaled.set_m_Date_3(unmarshaled_m_Date_temp_3);
	unmarshaled.set_m_FormattedValue_4(il2cpp_codegen_marshal_string_result(marshaled.___m_FormattedValue_4));
	unmarshaled.set_m_PlayerID_5(il2cpp_codegen_marshal_string_result(marshaled.___m_PlayerID_5));
	int32_t unmarshaled_m_Rank_temp_6 = 0;
	unmarshaled_m_Rank_temp_6 = marshaled.___m_Rank_6;
	unmarshaled.set_m_Rank_6(unmarshaled_m_Rank_temp_6);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcScoreData
extern "C" void GcScoreData_t2181296590_marshal_pinvoke_cleanup(GcScoreData_t2181296590_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_Category_0);
	marshaled.___m_Category_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___m_FormattedValue_4);
	marshaled.___m_FormattedValue_4 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___m_PlayerID_5);
	marshaled.___m_PlayerID_5 = NULL;
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcScoreData
extern "C" void GcScoreData_t2181296590_marshal_com(const GcScoreData_t2181296590& unmarshaled, GcScoreData_t2181296590_marshaled_com& marshaled)
{
	marshaled.___m_Category_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_m_Category_0());
	marshaled.___m_ValueLow_1 = unmarshaled.get_m_ValueLow_1();
	marshaled.___m_ValueHigh_2 = unmarshaled.get_m_ValueHigh_2();
	marshaled.___m_Date_3 = unmarshaled.get_m_Date_3();
	marshaled.___m_FormattedValue_4 = il2cpp_codegen_marshal_bstring(unmarshaled.get_m_FormattedValue_4());
	marshaled.___m_PlayerID_5 = il2cpp_codegen_marshal_bstring(unmarshaled.get_m_PlayerID_5());
	marshaled.___m_Rank_6 = unmarshaled.get_m_Rank_6();
}
extern "C" void GcScoreData_t2181296590_marshal_com_back(const GcScoreData_t2181296590_marshaled_com& marshaled, GcScoreData_t2181296590& unmarshaled)
{
	unmarshaled.set_m_Category_0(il2cpp_codegen_marshal_bstring_result(marshaled.___m_Category_0));
	int32_t unmarshaled_m_ValueLow_temp_1 = 0;
	unmarshaled_m_ValueLow_temp_1 = marshaled.___m_ValueLow_1;
	unmarshaled.set_m_ValueLow_1(unmarshaled_m_ValueLow_temp_1);
	int32_t unmarshaled_m_ValueHigh_temp_2 = 0;
	unmarshaled_m_ValueHigh_temp_2 = marshaled.___m_ValueHigh_2;
	unmarshaled.set_m_ValueHigh_2(unmarshaled_m_ValueHigh_temp_2);
	int32_t unmarshaled_m_Date_temp_3 = 0;
	unmarshaled_m_Date_temp_3 = marshaled.___m_Date_3;
	unmarshaled.set_m_Date_3(unmarshaled_m_Date_temp_3);
	unmarshaled.set_m_FormattedValue_4(il2cpp_codegen_marshal_bstring_result(marshaled.___m_FormattedValue_4));
	unmarshaled.set_m_PlayerID_5(il2cpp_codegen_marshal_bstring_result(marshaled.___m_PlayerID_5));
	int32_t unmarshaled_m_Rank_temp_6 = 0;
	unmarshaled_m_Rank_temp_6 = marshaled.___m_Rank_6;
	unmarshaled.set_m_Rank_6(unmarshaled_m_Rank_temp_6);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcScoreData
extern "C" void GcScoreData_t2181296590_marshal_com_cleanup(GcScoreData_t2181296590_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___m_Category_0);
	marshaled.___m_Category_0 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___m_FormattedValue_4);
	marshaled.___m_FormattedValue_4 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___m_PlayerID_5);
	marshaled.___m_PlayerID_5 = NULL;
}
// UnityEngine.SocialPlatforms.Impl.UserProfile UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::ToUserProfile()
extern Il2CppClass* UserProfile_t2280656072_il2cpp_TypeInfo_var;
extern const uint32_t GcUserProfileData_ToUserProfile_m1509702721_MetadataUsageId;
extern "C"  UserProfile_t2280656072 * GcUserProfileData_ToUserProfile_m1509702721 (GcUserProfileData_t657441114 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GcUserProfileData_ToUserProfile_m1509702721_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B2_1 = NULL;
	String_t* G_B1_0 = NULL;
	String_t* G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	String_t* G_B3_1 = NULL;
	String_t* G_B3_2 = NULL;
	{
		String_t* L_0 = __this->get_userName_0();
		String_t* L_1 = __this->get_userID_1();
		int32_t L_2 = __this->get_isFriend_2();
		G_B1_0 = L_1;
		G_B1_1 = L_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			G_B2_0 = L_1;
			G_B2_1 = L_0;
			goto IL_001e;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_001f;
	}

IL_001e:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_001f:
	{
		Texture2D_t3884108195 * L_3 = __this->get_image_3();
		UserProfile_t2280656072 * L_4 = (UserProfile_t2280656072 *)il2cpp_codegen_object_new(UserProfile_t2280656072_il2cpp_TypeInfo_var);
		UserProfile__ctor_m2682768015(L_4, G_B3_2, G_B3_1, (bool)G_B3_0, 3, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
extern "C"  UserProfile_t2280656072 * GcUserProfileData_ToUserProfile_m1509702721_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	GcUserProfileData_t657441114 * _thisAdjusted = reinterpret_cast<GcUserProfileData_t657441114 *>(__this + 1);
	return GcUserProfileData_ToUserProfile_m1509702721(_thisAdjusted, method);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::AddToArray(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,System.Int32)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1596528006;
extern const uint32_t GcUserProfileData_AddToArray_m3757655355_MetadataUsageId;
extern "C"  void GcUserProfileData_AddToArray_m3757655355 (GcUserProfileData_t657441114 * __this, UserProfileU5BU5D_t2378268441** ___array0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GcUserProfileData_AddToArray_m3757655355_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UserProfileU5BU5D_t2378268441** L_0 = ___array0;
		NullCheck((*((UserProfileU5BU5D_t2378268441**)L_0)));
		int32_t L_1 = ___number1;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)(*((UserProfileU5BU5D_t2378268441**)L_0)))->max_length))))) <= ((int32_t)L_1)))
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_2 = ___number1;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0020;
		}
	}
	{
		UserProfileU5BU5D_t2378268441** L_3 = ___array0;
		int32_t L_4 = ___number1;
		UserProfile_t2280656072 * L_5 = GcUserProfileData_ToUserProfile_m1509702721(__this, /*hidden argument*/NULL);
		NullCheck((*((UserProfileU5BU5D_t2378268441**)L_3)));
		IL2CPP_ARRAY_BOUNDS_CHECK((*((UserProfileU5BU5D_t2378268441**)L_3)), L_4);
		ArrayElementTypeCheck ((*((UserProfileU5BU5D_t2378268441**)L_3)), L_5);
		((*((UserProfileU5BU5D_t2378268441**)L_3)))->SetAt(static_cast<il2cpp_array_size_t>(L_4), (UserProfile_t2280656072 *)L_5);
		goto IL_002a;
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral1596528006, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
extern "C"  void GcUserProfileData_AddToArray_m3757655355_AdjustorThunk (Il2CppObject * __this, UserProfileU5BU5D_t2378268441** ___array0, int32_t ___number1, const MethodInfo* method)
{
	GcUserProfileData_t657441114 * _thisAdjusted = reinterpret_cast<GcUserProfileData_t657441114 *>(__this + 1);
	GcUserProfileData_AddToArray_m3757655355(_thisAdjusted, ___array0, ___number1, method);
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
extern "C" void GcUserProfileData_t657441114_marshal_pinvoke(const GcUserProfileData_t657441114& unmarshaled, GcUserProfileData_t657441114_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___image_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'image' of type 'GcUserProfileData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___image_3Exception);
}
extern "C" void GcUserProfileData_t657441114_marshal_pinvoke_back(const GcUserProfileData_t657441114_marshaled_pinvoke& marshaled, GcUserProfileData_t657441114& unmarshaled)
{
	Il2CppCodeGenException* ___image_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'image' of type 'GcUserProfileData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___image_3Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
extern "C" void GcUserProfileData_t657441114_marshal_pinvoke_cleanup(GcUserProfileData_t657441114_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
extern "C" void GcUserProfileData_t657441114_marshal_com(const GcUserProfileData_t657441114& unmarshaled, GcUserProfileData_t657441114_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___image_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'image' of type 'GcUserProfileData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___image_3Exception);
}
extern "C" void GcUserProfileData_t657441114_marshal_com_back(const GcUserProfileData_t657441114_marshaled_com& marshaled, GcUserProfileData_t657441114& unmarshaled)
{
	Il2CppCodeGenException* ___image_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'image' of type 'GcUserProfileData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___image_3Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
extern "C" void GcUserProfileData_t657441114_marshal_com_cleanup(GcUserProfileData_t657441114_marshaled_com& marshaled)
{
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
