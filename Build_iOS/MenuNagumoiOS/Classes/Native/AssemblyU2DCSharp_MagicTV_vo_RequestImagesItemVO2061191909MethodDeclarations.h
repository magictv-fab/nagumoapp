﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.vo.RequestImagesItemVO
struct RequestImagesItemVO_t2061191909;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.vo.RequestImagesItemVO::.ctor()
extern "C"  void RequestImagesItemVO__ctor_m211041854 (RequestImagesItemVO_t2061191909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.vo.RequestImagesItemVO::ToString()
extern "C"  String_t* RequestImagesItemVO_ToString_m2944387733 (RequestImagesItemVO_t2061191909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
