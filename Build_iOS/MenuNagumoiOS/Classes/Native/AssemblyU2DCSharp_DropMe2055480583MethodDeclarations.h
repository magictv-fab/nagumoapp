﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DropMe
struct DropMe_t2055480583;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1848751023;
// UnityEngine.Sprite
struct Sprite_t3199167241;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1848751023.h"

// System.Void DropMe::.ctor()
extern "C"  void DropMe__ctor_m2979985268 (DropMe_t2055480583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DropMe::OnEnable()
extern "C"  void DropMe_OnEnable_m1838689170 (DropMe_t2055480583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DropMe::OnDrop(UnityEngine.EventSystems.PointerEventData)
extern "C"  void DropMe_OnDrop_m3131110848 (DropMe_t2055480583 * __this, PointerEventData_t1848751023 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DropMe::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern "C"  void DropMe_OnPointerEnter_m745851028 (DropMe_t2055480583 * __this, PointerEventData_t1848751023 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DropMe::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern "C"  void DropMe_OnPointerExit_m94818064 (DropMe_t2055480583 * __this, PointerEventData_t1848751023 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite DropMe::GetDropSprite(UnityEngine.EventSystems.PointerEventData)
extern "C"  Sprite_t3199167241 * DropMe_GetDropSprite_m2031272099 (DropMe_t2055480583 * __this, PointerEventData_t1848751023 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
