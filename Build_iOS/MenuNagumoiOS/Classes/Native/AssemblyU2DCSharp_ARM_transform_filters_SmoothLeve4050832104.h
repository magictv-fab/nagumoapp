﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.transform.filters.SmoothPlugin
struct SmoothPlugin_t1435567013;
// ARM.device.AccelerometerEventDispacher
struct AccelerometerEventDispacher_t1386858712;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.transform.filters.SmoothLevelController
struct  SmoothLevelController_t4050832104  : public MonoBehaviour_t667441552
{
public:
	// ARM.transform.filters.SmoothPlugin ARM.transform.filters.SmoothLevelController::smoothFollowToControl
	SmoothPlugin_t1435567013 * ___smoothFollowToControl_2;
	// ARM.device.AccelerometerEventDispacher ARM.transform.filters.SmoothLevelController::accelerometerEventDispatcher
	AccelerometerEventDispacher_t1386858712 * ___accelerometerEventDispatcher_3;
	// System.Boolean ARM.transform.filters.SmoothLevelController::_hasAccelerometer
	bool ____hasAccelerometer_4;
	// System.Boolean ARM.transform.filters.SmoothLevelController::_hasSmooth
	bool ____hasSmooth_5;
	// System.Single ARM.transform.filters.SmoothLevelController::angleRatio
	float ___angleRatio_6;
	// System.Single ARM.transform.filters.SmoothLevelController::positionRatio
	float ___positionRatio_7;
	// System.Single ARM.transform.filters.SmoothLevelController::angleMinSmooth
	float ___angleMinSmooth_8;
	// System.Single ARM.transform.filters.SmoothLevelController::angleMaxSmooth
	float ___angleMaxSmooth_9;
	// System.Single ARM.transform.filters.SmoothLevelController::positionMinSmooth
	float ___positionMinSmooth_10;
	// System.Single ARM.transform.filters.SmoothLevelController::positionMaxSmooth
	float ___positionMaxSmooth_11;
	// System.Single ARM.transform.filters.SmoothLevelController::_difMinMaxPosition
	float ____difMinMaxPosition_12;
	// System.Single ARM.transform.filters.SmoothLevelController::_difMinMaxAngle
	float ____difMinMaxAngle_13;
	// System.Single ARM.transform.filters.SmoothLevelController::_defaultSpeedPosition
	float ____defaultSpeedPosition_14;
	// System.Single ARM.transform.filters.SmoothLevelController::_defaultSpeedAngle
	float ____defaultSpeedAngle_15;
	// System.Single ARM.transform.filters.SmoothLevelController::debugMag
	float ___debugMag_16;

public:
	inline static int32_t get_offset_of_smoothFollowToControl_2() { return static_cast<int32_t>(offsetof(SmoothLevelController_t4050832104, ___smoothFollowToControl_2)); }
	inline SmoothPlugin_t1435567013 * get_smoothFollowToControl_2() const { return ___smoothFollowToControl_2; }
	inline SmoothPlugin_t1435567013 ** get_address_of_smoothFollowToControl_2() { return &___smoothFollowToControl_2; }
	inline void set_smoothFollowToControl_2(SmoothPlugin_t1435567013 * value)
	{
		___smoothFollowToControl_2 = value;
		Il2CppCodeGenWriteBarrier(&___smoothFollowToControl_2, value);
	}

	inline static int32_t get_offset_of_accelerometerEventDispatcher_3() { return static_cast<int32_t>(offsetof(SmoothLevelController_t4050832104, ___accelerometerEventDispatcher_3)); }
	inline AccelerometerEventDispacher_t1386858712 * get_accelerometerEventDispatcher_3() const { return ___accelerometerEventDispatcher_3; }
	inline AccelerometerEventDispacher_t1386858712 ** get_address_of_accelerometerEventDispatcher_3() { return &___accelerometerEventDispatcher_3; }
	inline void set_accelerometerEventDispatcher_3(AccelerometerEventDispacher_t1386858712 * value)
	{
		___accelerometerEventDispatcher_3 = value;
		Il2CppCodeGenWriteBarrier(&___accelerometerEventDispatcher_3, value);
	}

	inline static int32_t get_offset_of__hasAccelerometer_4() { return static_cast<int32_t>(offsetof(SmoothLevelController_t4050832104, ____hasAccelerometer_4)); }
	inline bool get__hasAccelerometer_4() const { return ____hasAccelerometer_4; }
	inline bool* get_address_of__hasAccelerometer_4() { return &____hasAccelerometer_4; }
	inline void set__hasAccelerometer_4(bool value)
	{
		____hasAccelerometer_4 = value;
	}

	inline static int32_t get_offset_of__hasSmooth_5() { return static_cast<int32_t>(offsetof(SmoothLevelController_t4050832104, ____hasSmooth_5)); }
	inline bool get__hasSmooth_5() const { return ____hasSmooth_5; }
	inline bool* get_address_of__hasSmooth_5() { return &____hasSmooth_5; }
	inline void set__hasSmooth_5(bool value)
	{
		____hasSmooth_5 = value;
	}

	inline static int32_t get_offset_of_angleRatio_6() { return static_cast<int32_t>(offsetof(SmoothLevelController_t4050832104, ___angleRatio_6)); }
	inline float get_angleRatio_6() const { return ___angleRatio_6; }
	inline float* get_address_of_angleRatio_6() { return &___angleRatio_6; }
	inline void set_angleRatio_6(float value)
	{
		___angleRatio_6 = value;
	}

	inline static int32_t get_offset_of_positionRatio_7() { return static_cast<int32_t>(offsetof(SmoothLevelController_t4050832104, ___positionRatio_7)); }
	inline float get_positionRatio_7() const { return ___positionRatio_7; }
	inline float* get_address_of_positionRatio_7() { return &___positionRatio_7; }
	inline void set_positionRatio_7(float value)
	{
		___positionRatio_7 = value;
	}

	inline static int32_t get_offset_of_angleMinSmooth_8() { return static_cast<int32_t>(offsetof(SmoothLevelController_t4050832104, ___angleMinSmooth_8)); }
	inline float get_angleMinSmooth_8() const { return ___angleMinSmooth_8; }
	inline float* get_address_of_angleMinSmooth_8() { return &___angleMinSmooth_8; }
	inline void set_angleMinSmooth_8(float value)
	{
		___angleMinSmooth_8 = value;
	}

	inline static int32_t get_offset_of_angleMaxSmooth_9() { return static_cast<int32_t>(offsetof(SmoothLevelController_t4050832104, ___angleMaxSmooth_9)); }
	inline float get_angleMaxSmooth_9() const { return ___angleMaxSmooth_9; }
	inline float* get_address_of_angleMaxSmooth_9() { return &___angleMaxSmooth_9; }
	inline void set_angleMaxSmooth_9(float value)
	{
		___angleMaxSmooth_9 = value;
	}

	inline static int32_t get_offset_of_positionMinSmooth_10() { return static_cast<int32_t>(offsetof(SmoothLevelController_t4050832104, ___positionMinSmooth_10)); }
	inline float get_positionMinSmooth_10() const { return ___positionMinSmooth_10; }
	inline float* get_address_of_positionMinSmooth_10() { return &___positionMinSmooth_10; }
	inline void set_positionMinSmooth_10(float value)
	{
		___positionMinSmooth_10 = value;
	}

	inline static int32_t get_offset_of_positionMaxSmooth_11() { return static_cast<int32_t>(offsetof(SmoothLevelController_t4050832104, ___positionMaxSmooth_11)); }
	inline float get_positionMaxSmooth_11() const { return ___positionMaxSmooth_11; }
	inline float* get_address_of_positionMaxSmooth_11() { return &___positionMaxSmooth_11; }
	inline void set_positionMaxSmooth_11(float value)
	{
		___positionMaxSmooth_11 = value;
	}

	inline static int32_t get_offset_of__difMinMaxPosition_12() { return static_cast<int32_t>(offsetof(SmoothLevelController_t4050832104, ____difMinMaxPosition_12)); }
	inline float get__difMinMaxPosition_12() const { return ____difMinMaxPosition_12; }
	inline float* get_address_of__difMinMaxPosition_12() { return &____difMinMaxPosition_12; }
	inline void set__difMinMaxPosition_12(float value)
	{
		____difMinMaxPosition_12 = value;
	}

	inline static int32_t get_offset_of__difMinMaxAngle_13() { return static_cast<int32_t>(offsetof(SmoothLevelController_t4050832104, ____difMinMaxAngle_13)); }
	inline float get__difMinMaxAngle_13() const { return ____difMinMaxAngle_13; }
	inline float* get_address_of__difMinMaxAngle_13() { return &____difMinMaxAngle_13; }
	inline void set__difMinMaxAngle_13(float value)
	{
		____difMinMaxAngle_13 = value;
	}

	inline static int32_t get_offset_of__defaultSpeedPosition_14() { return static_cast<int32_t>(offsetof(SmoothLevelController_t4050832104, ____defaultSpeedPosition_14)); }
	inline float get__defaultSpeedPosition_14() const { return ____defaultSpeedPosition_14; }
	inline float* get_address_of__defaultSpeedPosition_14() { return &____defaultSpeedPosition_14; }
	inline void set__defaultSpeedPosition_14(float value)
	{
		____defaultSpeedPosition_14 = value;
	}

	inline static int32_t get_offset_of__defaultSpeedAngle_15() { return static_cast<int32_t>(offsetof(SmoothLevelController_t4050832104, ____defaultSpeedAngle_15)); }
	inline float get__defaultSpeedAngle_15() const { return ____defaultSpeedAngle_15; }
	inline float* get_address_of__defaultSpeedAngle_15() { return &____defaultSpeedAngle_15; }
	inline void set__defaultSpeedAngle_15(float value)
	{
		____defaultSpeedAngle_15 = value;
	}

	inline static int32_t get_offset_of_debugMag_16() { return static_cast<int32_t>(offsetof(SmoothLevelController_t4050832104, ___debugMag_16)); }
	inline float get_debugMag_16() const { return ___debugMag_16; }
	inline float* get_address_of_debugMag_16() { return &___debugMag_16; }
	inline void set_debugMag_16(float value)
	{
		___debugMag_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
