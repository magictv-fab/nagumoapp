﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.NetworkView
struct NetworkView_t3656680617;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.NetworkView>
struct  ComponentAction_1_t2632148816  : public FsmStateAction_t2366529033
{
public:
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ComponentAction`1::cachedGameObject
	GameObject_t3674682005 * ___cachedGameObject_9;
	// T HutongGames.PlayMaker.Actions.ComponentAction`1::component
	NetworkView_t3656680617 * ___component_10;

public:
	inline static int32_t get_offset_of_cachedGameObject_9() { return static_cast<int32_t>(offsetof(ComponentAction_1_t2632148816, ___cachedGameObject_9)); }
	inline GameObject_t3674682005 * get_cachedGameObject_9() const { return ___cachedGameObject_9; }
	inline GameObject_t3674682005 ** get_address_of_cachedGameObject_9() { return &___cachedGameObject_9; }
	inline void set_cachedGameObject_9(GameObject_t3674682005 * value)
	{
		___cachedGameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___cachedGameObject_9, value);
	}

	inline static int32_t get_offset_of_component_10() { return static_cast<int32_t>(offsetof(ComponentAction_1_t2632148816, ___component_10)); }
	inline NetworkView_t3656680617 * get_component_10() const { return ___component_10; }
	inline NetworkView_t3656680617 ** get_address_of_component_10() { return &___component_10; }
	inline void set_component_10(NetworkView_t3656680617 * value)
	{
		___component_10 = value;
		Il2CppCodeGenWriteBarrier(&___component_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
