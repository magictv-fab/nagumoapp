﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;
// UnityEngine.UI.Dropdown
struct Dropdown_t4201779933;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AutoPreencherDropDown
struct  AutoPreencherDropDown_t4173649056  : public MonoBehaviour_t667441552
{
public:
	// System.String AutoPreencherDropDown::text
	String_t* ___text_2;
	// System.String[] AutoPreencherDropDown::separator
	StringU5BU5D_t4054002952* ___separator_3;
	// UnityEngine.UI.Dropdown AutoPreencherDropDown::dropdown
	Dropdown_t4201779933 * ___dropdown_4;

public:
	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(AutoPreencherDropDown_t4173649056, ___text_2)); }
	inline String_t* get_text_2() const { return ___text_2; }
	inline String_t** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(String_t* value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier(&___text_2, value);
	}

	inline static int32_t get_offset_of_separator_3() { return static_cast<int32_t>(offsetof(AutoPreencherDropDown_t4173649056, ___separator_3)); }
	inline StringU5BU5D_t4054002952* get_separator_3() const { return ___separator_3; }
	inline StringU5BU5D_t4054002952** get_address_of_separator_3() { return &___separator_3; }
	inline void set_separator_3(StringU5BU5D_t4054002952* value)
	{
		___separator_3 = value;
		Il2CppCodeGenWriteBarrier(&___separator_3, value);
	}

	inline static int32_t get_offset_of_dropdown_4() { return static_cast<int32_t>(offsetof(AutoPreencherDropDown_t4173649056, ___dropdown_4)); }
	inline Dropdown_t4201779933 * get_dropdown_4() const { return ___dropdown_4; }
	inline Dropdown_t4201779933 ** get_address_of_dropdown_4() { return &___dropdown_4; }
	inline void set_dropdown_4(Dropdown_t4201779933 * value)
	{
		___dropdown_4 = value;
		Il2CppCodeGenWriteBarrier(&___dropdown_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
