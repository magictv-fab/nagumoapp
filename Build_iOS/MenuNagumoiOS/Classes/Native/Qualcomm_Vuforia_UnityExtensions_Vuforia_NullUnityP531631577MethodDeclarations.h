﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.NullUnityPlayer
struct NullUnityPlayer_t531631577;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUni432217960.h"
#include "mscorlib_System_String7231557.h"

// System.Void Vuforia.NullUnityPlayer::LoadNativeLibraries()
extern "C"  void NullUnityPlayer_LoadNativeLibraries_m3461067238 (NullUnityPlayer_t531631577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::InitializePlatform()
extern "C"  void NullUnityPlayer_InitializePlatform_m2816530907 (NullUnityPlayer_t531631577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VuforiaUnity/InitError Vuforia.NullUnityPlayer::Start(System.String)
extern "C"  int32_t NullUnityPlayer_Start_m191972745 (NullUnityPlayer_t531631577 * __this, String_t* ___licenseKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::Update()
extern "C"  void NullUnityPlayer_Update_m4069634945 (NullUnityPlayer_t531631577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::Dispose()
extern "C"  void NullUnityPlayer_Dispose_m3804764745 (NullUnityPlayer_t531631577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::OnPause()
extern "C"  void NullUnityPlayer_OnPause_m4099965313 (NullUnityPlayer_t531631577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::OnResume()
extern "C"  void NullUnityPlayer_OnResume_m935459620 (NullUnityPlayer_t531631577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::OnDestroy()
extern "C"  void NullUnityPlayer_OnDestroy_m2740367365 (NullUnityPlayer_t531631577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::.ctor()
extern "C"  void NullUnityPlayer__ctor_m2430877964 (NullUnityPlayer_t531631577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
