﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// UnityEngine.Flare
struct Flare_t4197217604;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co3178143027.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetLightFlare
struct  SetLightFlare_t3135785000  : public ComponentAction_1_t3178143027
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetLightFlare::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// UnityEngine.Flare HutongGames.PlayMaker.Actions.SetLightFlare::lightFlare
	Flare_t4197217604 * ___lightFlare_12;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(SetLightFlare_t3135785000, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_lightFlare_12() { return static_cast<int32_t>(offsetof(SetLightFlare_t3135785000, ___lightFlare_12)); }
	inline Flare_t4197217604 * get_lightFlare_12() const { return ___lightFlare_12; }
	inline Flare_t4197217604 ** get_address_of_lightFlare_12() { return &___lightFlare_12; }
	inline void set_lightFlare_12(Flare_t4197217604 * value)
	{
		___lightFlare_12 = value;
		Il2CppCodeGenWriteBarrier(&___lightFlare_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
