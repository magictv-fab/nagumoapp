﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Char,ZXing.Aztec.Internal.Decoder/Table>
struct ShimEnumerator_t813817701;
// System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>
struct Dictionary_2_t1098039674;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Char,ZXing.Aztec.Internal.Decoder/Table>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m316912990_gshared (ShimEnumerator_t813817701 * __this, Dictionary_2_t1098039674 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m316912990(__this, ___host0, method) ((  void (*) (ShimEnumerator_t813817701 *, Dictionary_2_t1098039674 *, const MethodInfo*))ShimEnumerator__ctor_m316912990_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Char,ZXing.Aztec.Internal.Decoder/Table>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1593779399_gshared (ShimEnumerator_t813817701 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m1593779399(__this, method) ((  bool (*) (ShimEnumerator_t813817701 *, const MethodInfo*))ShimEnumerator_MoveNext_m1593779399_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Char,ZXing.Aztec.Internal.Decoder/Table>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m51187139_gshared (ShimEnumerator_t813817701 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m51187139(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t813817701 *, const MethodInfo*))ShimEnumerator_get_Entry_m51187139_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Char,ZXing.Aztec.Internal.Decoder/Table>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m1010456514_gshared (ShimEnumerator_t813817701 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m1010456514(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t813817701 *, const MethodInfo*))ShimEnumerator_get_Key_m1010456514_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Char,ZXing.Aztec.Internal.Decoder/Table>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1434537428_gshared (ShimEnumerator_t813817701 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m1434537428(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t813817701 *, const MethodInfo*))ShimEnumerator_get_Value_m1434537428_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Char,ZXing.Aztec.Internal.Decoder/Table>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m1330657116_gshared (ShimEnumerator_t813817701 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m1330657116(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t813817701 *, const MethodInfo*))ShimEnumerator_get_Current_m1330657116_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Char,ZXing.Aztec.Internal.Decoder/Table>::Reset()
extern "C"  void ShimEnumerator_Reset_m1335829168_gshared (ShimEnumerator_t813817701 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m1335829168(__this, method) ((  void (*) (ShimEnumerator_t813817701 *, const MethodInfo*))ShimEnumerator_Reset_m1335829168_gshared)(__this, method)
