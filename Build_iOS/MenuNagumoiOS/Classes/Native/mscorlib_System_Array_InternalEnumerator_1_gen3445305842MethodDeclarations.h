﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3445305842.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_MagicTV_globals_StateMachine367995870.h"

// System.Void System.Array/InternalEnumerator`1<MagicTV.globals.StateMachine>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3463841316_gshared (InternalEnumerator_1_t3445305842 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3463841316(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3445305842 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3463841316_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<MagicTV.globals.StateMachine>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3420493052_gshared (InternalEnumerator_1_t3445305842 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3420493052(__this, method) ((  void (*) (InternalEnumerator_1_t3445305842 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3420493052_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<MagicTV.globals.StateMachine>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3121257384_gshared (InternalEnumerator_1_t3445305842 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3121257384(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3445305842 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3121257384_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<MagicTV.globals.StateMachine>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3445239035_gshared (InternalEnumerator_1_t3445305842 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3445239035(__this, method) ((  void (*) (InternalEnumerator_1_t3445305842 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3445239035_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<MagicTV.globals.StateMachine>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1815029736_gshared (InternalEnumerator_1_t3445305842 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1815029736(__this, method) ((  bool (*) (InternalEnumerator_1_t3445305842 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1815029736_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<MagicTV.globals.StateMachine>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m952203051_gshared (InternalEnumerator_1_t3445305842 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m952203051(__this, method) ((  int32_t (*) (InternalEnumerator_1_t3445305842 *, const MethodInfo*))InternalEnumerator_1_get_Current_m952203051_gshared)(__this, method)
