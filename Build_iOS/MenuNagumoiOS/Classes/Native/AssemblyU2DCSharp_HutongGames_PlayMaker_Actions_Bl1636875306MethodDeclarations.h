﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Blink
struct Blink_t1636875306;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Blink::.ctor()
extern "C"  void Blink__ctor_m4010019916 (Blink_t1636875306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Blink::Reset()
extern "C"  void Blink_Reset_m1656452857 (Blink_t1636875306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Blink::OnEnter()
extern "C"  void Blink_OnEnter_m435203363 (Blink_t1636875306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Blink::OnUpdate()
extern "C"  void Blink_OnUpdate_m4034929024 (Blink_t1636875306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Blink::UpdateBlinkState(System.Boolean)
extern "C"  void Blink_UpdateBlinkState_m4080935085 (Blink_t1636875306 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
