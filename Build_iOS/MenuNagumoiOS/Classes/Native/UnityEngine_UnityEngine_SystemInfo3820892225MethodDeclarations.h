﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_NPOTSupport1787002238.h"
#include "UnityEngine_UnityEngine_DeviceType3959528308.h"

// System.String UnityEngine.SystemInfo::get_operatingSystem()
extern "C"  String_t* SystemInfo_get_operatingSystem_m2538828082 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.SystemInfo::get_processorType()
extern "C"  String_t* SystemInfo_get_processorType_m3719165582 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SystemInfo::get_processorCount()
extern "C"  int32_t SystemInfo_get_processorCount_m3548598394 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SystemInfo::get_systemMemorySize()
extern "C"  int32_t SystemInfo_get_systemMemorySize_m114183438 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SystemInfo::get_graphicsMemorySize()
extern "C"  int32_t SystemInfo_get_graphicsMemorySize_m4144186474 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.SystemInfo::get_graphicsDeviceName()
extern "C"  String_t* SystemInfo_get_graphicsDeviceName_m4186183660 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.SystemInfo::get_graphicsDeviceVendor()
extern "C"  String_t* SystemInfo_get_graphicsDeviceVendor_m3175628649 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SystemInfo::get_graphicsDeviceID()
extern "C"  int32_t SystemInfo_get_graphicsDeviceID_m2460390329 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SystemInfo::get_graphicsDeviceVendorID()
extern "C"  int32_t SystemInfo_get_graphicsDeviceVendorID_m3448682337 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.SystemInfo::get_graphicsDeviceVersion()
extern "C"  String_t* SystemInfo_get_graphicsDeviceVersion_m3634129081 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SystemInfo::get_graphicsShaderLevel()
extern "C"  int32_t SystemInfo_get_graphicsShaderLevel_m1169417593 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SystemInfo::get_graphicsPixelFillrate()
extern "C"  int32_t SystemInfo_get_graphicsPixelFillrate_m2941685667 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SystemInfo::get_supportsShadows()
extern "C"  bool SystemInfo_get_supportsShadows_m1667108142 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SystemInfo::get_supportsRenderTextures()
extern "C"  bool SystemInfo_get_supportsRenderTextures_m3098351893 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SystemInfo::get_supportsImageEffects()
extern "C"  bool SystemInfo_get_supportsImageEffects_m2392300814 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SystemInfo::get_supports3DTextures()
extern "C"  bool SystemInfo_get_supports3DTextures_m3554473744 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SystemInfo::get_supportsComputeShaders()
extern "C"  bool SystemInfo_get_supportsComputeShaders_m940660062 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SystemInfo::get_supportsInstancing()
extern "C"  bool SystemInfo_get_supportsInstancing_m2109416633 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SystemInfo::get_supportedRenderTargetCount()
extern "C"  int32_t SystemInfo_get_supportedRenderTargetCount_m2248175383 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.NPOTSupport UnityEngine.SystemInfo::get_npotSupport()
extern "C"  int32_t SystemInfo_get_npotSupport_m2935155019 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.SystemInfo::get_deviceUniqueIdentifier()
extern "C"  String_t* SystemInfo_get_deviceUniqueIdentifier_m983206480 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.SystemInfo::get_deviceName()
extern "C"  String_t* SystemInfo_get_deviceName_m3161260225 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.SystemInfo::get_deviceModel()
extern "C"  String_t* SystemInfo_get_deviceModel_m3014844565 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SystemInfo::get_supportsAccelerometer()
extern "C"  bool SystemInfo_get_supportsAccelerometer_m1352574890 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SystemInfo::get_supportsGyroscope()
extern "C"  bool SystemInfo_get_supportsGyroscope_m1121004512 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SystemInfo::get_supportsLocationService()
extern "C"  bool SystemInfo_get_supportsLocationService_m2852518363 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SystemInfo::get_supportsVibration()
extern "C"  bool SystemInfo_get_supportsVibration_m259686957 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.DeviceType UnityEngine.SystemInfo::get_deviceType()
extern "C"  int32_t SystemInfo_get_deviceType_m2827604277 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SystemInfo::get_maxTextureSize()
extern "C"  int32_t SystemInfo_get_maxTextureSize_m1524512213 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
