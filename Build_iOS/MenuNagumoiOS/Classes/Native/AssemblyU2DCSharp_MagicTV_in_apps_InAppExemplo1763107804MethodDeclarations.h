﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.in_apps.InAppExemplo
struct InAppExemplo_t1763107804;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.in_apps.InAppExemplo::.ctor()
extern "C"  void InAppExemplo__ctor_m1190920414 (InAppExemplo_t1763107804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppExemplo::prepare()
extern "C"  void InAppExemplo_prepare_m2940197539 (InAppExemplo_t1763107804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppExemplo::DoRun()
extern "C"  void InAppExemplo_DoRun_m3848457212 (InAppExemplo_t1763107804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppExemplo::DoStop()
extern "C"  void InAppExemplo_DoStop_m3365861203 (InAppExemplo_t1763107804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
