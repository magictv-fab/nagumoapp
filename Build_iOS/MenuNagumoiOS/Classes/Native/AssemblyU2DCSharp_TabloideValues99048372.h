﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Sprite
struct Sprite_t3199167241;
// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.UI.Image
struct Image_t538875265;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TabloideValues
struct  TabloideValues_t99048372  : public MonoBehaviour_t667441552
{
public:
	// System.String TabloideValues::id
	String_t* ___id_2;
	// System.String TabloideValues::txt
	String_t* ___txt_3;
	// System.String TabloideValues::link
	String_t* ___link_4;
	// UnityEngine.Sprite TabloideValues::spt
	Sprite_t3199167241 * ___spt_5;
	// UnityEngine.UI.Text TabloideValues::txtTxt
	Text_t9039225 * ___txtTxt_6;
	// UnityEngine.UI.Image TabloideValues::imgImg
	Image_t538875265 * ___imgImg_7;

public:
	inline static int32_t get_offset_of_id_2() { return static_cast<int32_t>(offsetof(TabloideValues_t99048372, ___id_2)); }
	inline String_t* get_id_2() const { return ___id_2; }
	inline String_t** get_address_of_id_2() { return &___id_2; }
	inline void set_id_2(String_t* value)
	{
		___id_2 = value;
		Il2CppCodeGenWriteBarrier(&___id_2, value);
	}

	inline static int32_t get_offset_of_txt_3() { return static_cast<int32_t>(offsetof(TabloideValues_t99048372, ___txt_3)); }
	inline String_t* get_txt_3() const { return ___txt_3; }
	inline String_t** get_address_of_txt_3() { return &___txt_3; }
	inline void set_txt_3(String_t* value)
	{
		___txt_3 = value;
		Il2CppCodeGenWriteBarrier(&___txt_3, value);
	}

	inline static int32_t get_offset_of_link_4() { return static_cast<int32_t>(offsetof(TabloideValues_t99048372, ___link_4)); }
	inline String_t* get_link_4() const { return ___link_4; }
	inline String_t** get_address_of_link_4() { return &___link_4; }
	inline void set_link_4(String_t* value)
	{
		___link_4 = value;
		Il2CppCodeGenWriteBarrier(&___link_4, value);
	}

	inline static int32_t get_offset_of_spt_5() { return static_cast<int32_t>(offsetof(TabloideValues_t99048372, ___spt_5)); }
	inline Sprite_t3199167241 * get_spt_5() const { return ___spt_5; }
	inline Sprite_t3199167241 ** get_address_of_spt_5() { return &___spt_5; }
	inline void set_spt_5(Sprite_t3199167241 * value)
	{
		___spt_5 = value;
		Il2CppCodeGenWriteBarrier(&___spt_5, value);
	}

	inline static int32_t get_offset_of_txtTxt_6() { return static_cast<int32_t>(offsetof(TabloideValues_t99048372, ___txtTxt_6)); }
	inline Text_t9039225 * get_txtTxt_6() const { return ___txtTxt_6; }
	inline Text_t9039225 ** get_address_of_txtTxt_6() { return &___txtTxt_6; }
	inline void set_txtTxt_6(Text_t9039225 * value)
	{
		___txtTxt_6 = value;
		Il2CppCodeGenWriteBarrier(&___txtTxt_6, value);
	}

	inline static int32_t get_offset_of_imgImg_7() { return static_cast<int32_t>(offsetof(TabloideValues_t99048372, ___imgImg_7)); }
	inline Image_t538875265 * get_imgImg_7() const { return ___imgImg_7; }
	inline Image_t538875265 ** get_address_of_imgImg_7() { return &___imgImg_7; }
	inline void set_imgImg_7(Image_t538875265 * value)
	{
		___imgImg_7 = value;
		Il2CppCodeGenWriteBarrier(&___imgImg_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
