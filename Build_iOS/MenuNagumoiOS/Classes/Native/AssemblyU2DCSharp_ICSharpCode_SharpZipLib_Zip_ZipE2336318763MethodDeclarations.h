﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.ZipEntryFactory
struct ZipEntryFactory_t2336318763;
// ICSharpCode.SharpZipLib.Core.INameTransform
struct INameTransform_t2173030;
// ICSharpCode.SharpZipLib.Zip.ZipEntry
struct ZipEntry_t3141689087;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipE3036340399.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_String7231557.h"

// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntryFactory::.ctor()
extern "C"  void ZipEntryFactory__ctor_m1314185136 (ZipEntryFactory_t2336318763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntryFactory::.ctor(ICSharpCode.SharpZipLib.Zip.ZipEntryFactory/TimeSetting)
extern "C"  void ZipEntryFactory__ctor_m56830451 (ZipEntryFactory_t2336318763 * __this, int32_t ___timeSetting0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntryFactory::.ctor(System.DateTime)
extern "C"  void ZipEntryFactory__ctor_m2751692936 (ZipEntryFactory_t2336318763 * __this, DateTime_t4283661327  ___time0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Core.INameTransform ICSharpCode.SharpZipLib.Zip.ZipEntryFactory::get_NameTransform()
extern "C"  Il2CppObject * ZipEntryFactory_get_NameTransform_m1652519683 (ZipEntryFactory_t2336318763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntryFactory::set_NameTransform(ICSharpCode.SharpZipLib.Core.INameTransform)
extern "C"  void ZipEntryFactory_set_NameTransform_m926916746 (ZipEntryFactory_t2336318763 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Zip.ZipEntryFactory/TimeSetting ICSharpCode.SharpZipLib.Zip.ZipEntryFactory::get_Setting()
extern "C"  int32_t ZipEntryFactory_get_Setting_m3285233401 (ZipEntryFactory_t2336318763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntryFactory::set_Setting(ICSharpCode.SharpZipLib.Zip.ZipEntryFactory/TimeSetting)
extern "C"  void ZipEntryFactory_set_Setting_m790791362 (ZipEntryFactory_t2336318763 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime ICSharpCode.SharpZipLib.Zip.ZipEntryFactory::get_FixedDateTime()
extern "C"  DateTime_t4283661327  ZipEntryFactory_get_FixedDateTime_m4218119853 (ZipEntryFactory_t2336318763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntryFactory::set_FixedDateTime(System.DateTime)
extern "C"  void ZipEntryFactory_set_FixedDateTime_m931162968 (ZipEntryFactory_t2336318763 * __this, DateTime_t4283661327  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntryFactory::get_GetAttributes()
extern "C"  int32_t ZipEntryFactory_get_GetAttributes_m2785629824 (ZipEntryFactory_t2336318763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntryFactory::set_GetAttributes(System.Int32)
extern "C"  void ZipEntryFactory_set_GetAttributes_m3826753999 (ZipEntryFactory_t2336318763 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntryFactory::get_SetAttributes()
extern "C"  int32_t ZipEntryFactory_get_SetAttributes_m3696382348 (ZipEntryFactory_t2336318763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntryFactory::set_SetAttributes(System.Int32)
extern "C"  void ZipEntryFactory_set_SetAttributes_m2481220315 (ZipEntryFactory_t2336318763 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntryFactory::get_IsUnicodeText()
extern "C"  bool ZipEntryFactory_get_IsUnicodeText_m3898917689 (ZipEntryFactory_t2336318763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntryFactory::set_IsUnicodeText(System.Boolean)
extern "C"  void ZipEntryFactory_set_IsUnicodeText_m2963877192 (ZipEntryFactory_t2336318763 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Zip.ZipEntry ICSharpCode.SharpZipLib.Zip.ZipEntryFactory::MakeFileEntry(System.String)
extern "C"  ZipEntry_t3141689087 * ZipEntryFactory_MakeFileEntry_m619337086 (ZipEntryFactory_t2336318763 * __this, String_t* ___fileName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Zip.ZipEntry ICSharpCode.SharpZipLib.Zip.ZipEntryFactory::MakeFileEntry(System.String,System.Boolean)
extern "C"  ZipEntry_t3141689087 * ZipEntryFactory_MakeFileEntry_m2844561983 (ZipEntryFactory_t2336318763 * __this, String_t* ___fileName0, bool ___useFileSystem1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Zip.ZipEntry ICSharpCode.SharpZipLib.Zip.ZipEntryFactory::MakeFileEntry(System.String,System.String,System.Boolean)
extern "C"  ZipEntry_t3141689087 * ZipEntryFactory_MakeFileEntry_m1741713155 (ZipEntryFactory_t2336318763 * __this, String_t* ___fileName0, String_t* ___entryName1, bool ___useFileSystem2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Zip.ZipEntry ICSharpCode.SharpZipLib.Zip.ZipEntryFactory::MakeDirectoryEntry(System.String)
extern "C"  ZipEntry_t3141689087 * ZipEntryFactory_MakeDirectoryEntry_m959793641 (ZipEntryFactory_t2336318763 * __this, String_t* ___directoryName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Zip.ZipEntry ICSharpCode.SharpZipLib.Zip.ZipEntryFactory::MakeDirectoryEntry(System.String,System.Boolean)
extern "C"  ZipEntry_t3141689087 * ZipEntryFactory_MakeDirectoryEntry_m2272451188 (ZipEntryFactory_t2336318763 * __this, String_t* ___directoryName0, bool ___useFileSystem1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
