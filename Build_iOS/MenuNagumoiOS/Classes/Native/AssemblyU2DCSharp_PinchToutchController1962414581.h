﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PinchToutchController
struct PinchToutchController_t1962414581;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_TouchControllerState2242910518.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PinchToutchController
struct  PinchToutchController_t1962414581  : public MonoBehaviour_t667441552
{
public:
	// System.Single PinchToutchController::touchAngle
	float ___touchAngle_2;
	// System.Single PinchToutchController::zoom
	float ___zoom_3;
	// System.Single PinchToutchController::touchDistance
	float ___touchDistance_4;
	// System.Single PinchToutchController::deltaAngle
	float ___deltaAngle_5;
	// System.Single PinchToutchController::deltaAngleTolerance
	float ___deltaAngleTolerance_6;
	// System.Single PinchToutchController::deltaAngleToleranceReset
	float ___deltaAngleToleranceReset_7;
	// System.Single PinchToutchController::deltaZoom
	float ___deltaZoom_8;
	// System.Boolean PinchToutchController::fixAxisVertical
	bool ___fixAxisVertical_9;
	// System.Boolean PinchToutchController::fixAxisHorizontal
	bool ___fixAxisHorizontal_10;
	// System.Boolean PinchToutchController::fixAxisDepth
	bool ___fixAxisDepth_11;
	// System.Boolean PinchToutchController::canPan
	bool ___canPan_12;
	// System.Boolean PinchToutchController::canZoom
	bool ___canZoom_13;
	// System.Boolean PinchToutchController::canRotate
	bool ___canRotate_14;
	// UnityEngine.Vector2 PinchToutchController::_startClick
	Vector2_t4282066565  ____startClick_15;
	// UnityEngine.Vector2 PinchToutchController::_startRotationClick
	Vector2_t4282066565  ____startRotationClick_16;
	// UnityEngine.Vector3 PinchToutchController::_screenPoint
	Vector3_t4282066566  ____screenPoint_17;
	// UnityEngine.Vector3 PinchToutchController::_offset
	Vector3_t4282066566  ____offset_18;
	// System.Single PinchToutchController::_scaleAcurracy
	float ____scaleAcurracy_19;
	// System.Single PinchToutchController::PanSpeedFactor
	float ___PanSpeedFactor_20;
	// System.Single PinchToutchController::RotateSpeedFactor
	float ___RotateSpeedFactor_21;
	// System.Single PinchToutchController::RotateDeltaMinimum
	float ___RotateDeltaMinimum_22;
	// System.Single PinchToutchController::ScaleDeltaMinimum
	float ___ScaleDeltaMinimum_23;
	// System.Single PinchToutchController::ScaleDeltaMaximum
	float ___ScaleDeltaMaximum_24;
	// System.Single PinchToutchController::ScaleSpeedFactor
	float ___ScaleSpeedFactor_25;
	// System.Single PinchToutchController::ScaleMaximumTolerance
	float ___ScaleMaximumTolerance_26;
	// System.Single PinchToutchController::ScaleMinimumTolerance
	float ___ScaleMinimumTolerance_27;
	// System.Boolean PinchToutchController::tapToSelect
	bool ___tapToSelect_28;
	// System.Boolean PinchToutchController::PanRelativeToWorld
	bool ___PanRelativeToWorld_29;
	// UnityEngine.GameObject PinchToutchController::currentCamera
	GameObject_t3674682005 * ___currentCamera_31;
	// UnityEngine.GameObject PinchToutchController::cameraToControl
	GameObject_t3674682005 * ___cameraToControl_32;
	// UnityEngine.GameObject PinchToutchController::cameraToReference
	GameObject_t3674682005 * ___cameraToReference_33;
	// UnityEngine.Vector3 PinchToutchController::cameraToControlTargetPivot
	Vector3_t4282066566  ___cameraToControlTargetPivot_34;
	// UnityEngine.Vector3 PinchToutchController::cameraToReferenceTargetPivot
	Vector3_t4282066566  ___cameraToReferenceTargetPivot_35;
	// System.Single PinchToutchController::cameraToControlInitalTargetDistance
	float ___cameraToControlInitalTargetDistance_36;
	// System.Single PinchToutchController::cameraToReferenceInitalTargetDistance
	float ___cameraToReferenceInitalTargetDistance_37;
	// System.Single PinchToutchController::cameraToControlTargetDistance
	float ___cameraToControlTargetDistance_38;
	// System.Single PinchToutchController::cameraToReferenceTargetDistance
	float ___cameraToReferenceTargetDistance_39;
	// System.Single PinchToutchController::cameraToControlCurrentPercentage
	float ___cameraToControlCurrentPercentage_40;
	// System.Single PinchToutchController::cameraToReferenceCurrentPercentage
	float ___cameraToReferenceCurrentPercentage_41;
	// System.Single PinchToutchController::cameraCurrentZoom
	float ___cameraCurrentZoom_42;
	// System.Single PinchToutchController::cameraZoom
	float ___cameraZoom_43;
	// System.Single PinchToutchController::cameraZoomBonus
	float ___cameraZoomBonus_44;
	// System.Single PinchToutchController::cameraRelativeRatio
	float ___cameraRelativeRatio_45;
	// System.Single PinchToutchController::camerasDistance
	float ___camerasDistance_46;
	// System.Single PinchToutchController::camerasMaxDistance
	float ___camerasMaxDistance_47;
	// System.Single PinchToutchController::camerasMinDistance
	float ___camerasMinDistance_48;
	// TouchControllerState PinchToutchController::currentState
	int32_t ___currentState_49;
	// System.Single PinchToutchController::_angle2Touches
	float ____angle2Touches_50;
	// System.Int32 PinchToutchController::_touchCount
	int32_t ____touchCount_51;

public:
	inline static int32_t get_offset_of_touchAngle_2() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___touchAngle_2)); }
	inline float get_touchAngle_2() const { return ___touchAngle_2; }
	inline float* get_address_of_touchAngle_2() { return &___touchAngle_2; }
	inline void set_touchAngle_2(float value)
	{
		___touchAngle_2 = value;
	}

	inline static int32_t get_offset_of_zoom_3() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___zoom_3)); }
	inline float get_zoom_3() const { return ___zoom_3; }
	inline float* get_address_of_zoom_3() { return &___zoom_3; }
	inline void set_zoom_3(float value)
	{
		___zoom_3 = value;
	}

	inline static int32_t get_offset_of_touchDistance_4() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___touchDistance_4)); }
	inline float get_touchDistance_4() const { return ___touchDistance_4; }
	inline float* get_address_of_touchDistance_4() { return &___touchDistance_4; }
	inline void set_touchDistance_4(float value)
	{
		___touchDistance_4 = value;
	}

	inline static int32_t get_offset_of_deltaAngle_5() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___deltaAngle_5)); }
	inline float get_deltaAngle_5() const { return ___deltaAngle_5; }
	inline float* get_address_of_deltaAngle_5() { return &___deltaAngle_5; }
	inline void set_deltaAngle_5(float value)
	{
		___deltaAngle_5 = value;
	}

	inline static int32_t get_offset_of_deltaAngleTolerance_6() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___deltaAngleTolerance_6)); }
	inline float get_deltaAngleTolerance_6() const { return ___deltaAngleTolerance_6; }
	inline float* get_address_of_deltaAngleTolerance_6() { return &___deltaAngleTolerance_6; }
	inline void set_deltaAngleTolerance_6(float value)
	{
		___deltaAngleTolerance_6 = value;
	}

	inline static int32_t get_offset_of_deltaAngleToleranceReset_7() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___deltaAngleToleranceReset_7)); }
	inline float get_deltaAngleToleranceReset_7() const { return ___deltaAngleToleranceReset_7; }
	inline float* get_address_of_deltaAngleToleranceReset_7() { return &___deltaAngleToleranceReset_7; }
	inline void set_deltaAngleToleranceReset_7(float value)
	{
		___deltaAngleToleranceReset_7 = value;
	}

	inline static int32_t get_offset_of_deltaZoom_8() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___deltaZoom_8)); }
	inline float get_deltaZoom_8() const { return ___deltaZoom_8; }
	inline float* get_address_of_deltaZoom_8() { return &___deltaZoom_8; }
	inline void set_deltaZoom_8(float value)
	{
		___deltaZoom_8 = value;
	}

	inline static int32_t get_offset_of_fixAxisVertical_9() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___fixAxisVertical_9)); }
	inline bool get_fixAxisVertical_9() const { return ___fixAxisVertical_9; }
	inline bool* get_address_of_fixAxisVertical_9() { return &___fixAxisVertical_9; }
	inline void set_fixAxisVertical_9(bool value)
	{
		___fixAxisVertical_9 = value;
	}

	inline static int32_t get_offset_of_fixAxisHorizontal_10() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___fixAxisHorizontal_10)); }
	inline bool get_fixAxisHorizontal_10() const { return ___fixAxisHorizontal_10; }
	inline bool* get_address_of_fixAxisHorizontal_10() { return &___fixAxisHorizontal_10; }
	inline void set_fixAxisHorizontal_10(bool value)
	{
		___fixAxisHorizontal_10 = value;
	}

	inline static int32_t get_offset_of_fixAxisDepth_11() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___fixAxisDepth_11)); }
	inline bool get_fixAxisDepth_11() const { return ___fixAxisDepth_11; }
	inline bool* get_address_of_fixAxisDepth_11() { return &___fixAxisDepth_11; }
	inline void set_fixAxisDepth_11(bool value)
	{
		___fixAxisDepth_11 = value;
	}

	inline static int32_t get_offset_of_canPan_12() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___canPan_12)); }
	inline bool get_canPan_12() const { return ___canPan_12; }
	inline bool* get_address_of_canPan_12() { return &___canPan_12; }
	inline void set_canPan_12(bool value)
	{
		___canPan_12 = value;
	}

	inline static int32_t get_offset_of_canZoom_13() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___canZoom_13)); }
	inline bool get_canZoom_13() const { return ___canZoom_13; }
	inline bool* get_address_of_canZoom_13() { return &___canZoom_13; }
	inline void set_canZoom_13(bool value)
	{
		___canZoom_13 = value;
	}

	inline static int32_t get_offset_of_canRotate_14() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___canRotate_14)); }
	inline bool get_canRotate_14() const { return ___canRotate_14; }
	inline bool* get_address_of_canRotate_14() { return &___canRotate_14; }
	inline void set_canRotate_14(bool value)
	{
		___canRotate_14 = value;
	}

	inline static int32_t get_offset_of__startClick_15() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ____startClick_15)); }
	inline Vector2_t4282066565  get__startClick_15() const { return ____startClick_15; }
	inline Vector2_t4282066565 * get_address_of__startClick_15() { return &____startClick_15; }
	inline void set__startClick_15(Vector2_t4282066565  value)
	{
		____startClick_15 = value;
	}

	inline static int32_t get_offset_of__startRotationClick_16() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ____startRotationClick_16)); }
	inline Vector2_t4282066565  get__startRotationClick_16() const { return ____startRotationClick_16; }
	inline Vector2_t4282066565 * get_address_of__startRotationClick_16() { return &____startRotationClick_16; }
	inline void set__startRotationClick_16(Vector2_t4282066565  value)
	{
		____startRotationClick_16 = value;
	}

	inline static int32_t get_offset_of__screenPoint_17() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ____screenPoint_17)); }
	inline Vector3_t4282066566  get__screenPoint_17() const { return ____screenPoint_17; }
	inline Vector3_t4282066566 * get_address_of__screenPoint_17() { return &____screenPoint_17; }
	inline void set__screenPoint_17(Vector3_t4282066566  value)
	{
		____screenPoint_17 = value;
	}

	inline static int32_t get_offset_of__offset_18() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ____offset_18)); }
	inline Vector3_t4282066566  get__offset_18() const { return ____offset_18; }
	inline Vector3_t4282066566 * get_address_of__offset_18() { return &____offset_18; }
	inline void set__offset_18(Vector3_t4282066566  value)
	{
		____offset_18 = value;
	}

	inline static int32_t get_offset_of__scaleAcurracy_19() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ____scaleAcurracy_19)); }
	inline float get__scaleAcurracy_19() const { return ____scaleAcurracy_19; }
	inline float* get_address_of__scaleAcurracy_19() { return &____scaleAcurracy_19; }
	inline void set__scaleAcurracy_19(float value)
	{
		____scaleAcurracy_19 = value;
	}

	inline static int32_t get_offset_of_PanSpeedFactor_20() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___PanSpeedFactor_20)); }
	inline float get_PanSpeedFactor_20() const { return ___PanSpeedFactor_20; }
	inline float* get_address_of_PanSpeedFactor_20() { return &___PanSpeedFactor_20; }
	inline void set_PanSpeedFactor_20(float value)
	{
		___PanSpeedFactor_20 = value;
	}

	inline static int32_t get_offset_of_RotateSpeedFactor_21() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___RotateSpeedFactor_21)); }
	inline float get_RotateSpeedFactor_21() const { return ___RotateSpeedFactor_21; }
	inline float* get_address_of_RotateSpeedFactor_21() { return &___RotateSpeedFactor_21; }
	inline void set_RotateSpeedFactor_21(float value)
	{
		___RotateSpeedFactor_21 = value;
	}

	inline static int32_t get_offset_of_RotateDeltaMinimum_22() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___RotateDeltaMinimum_22)); }
	inline float get_RotateDeltaMinimum_22() const { return ___RotateDeltaMinimum_22; }
	inline float* get_address_of_RotateDeltaMinimum_22() { return &___RotateDeltaMinimum_22; }
	inline void set_RotateDeltaMinimum_22(float value)
	{
		___RotateDeltaMinimum_22 = value;
	}

	inline static int32_t get_offset_of_ScaleDeltaMinimum_23() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___ScaleDeltaMinimum_23)); }
	inline float get_ScaleDeltaMinimum_23() const { return ___ScaleDeltaMinimum_23; }
	inline float* get_address_of_ScaleDeltaMinimum_23() { return &___ScaleDeltaMinimum_23; }
	inline void set_ScaleDeltaMinimum_23(float value)
	{
		___ScaleDeltaMinimum_23 = value;
	}

	inline static int32_t get_offset_of_ScaleDeltaMaximum_24() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___ScaleDeltaMaximum_24)); }
	inline float get_ScaleDeltaMaximum_24() const { return ___ScaleDeltaMaximum_24; }
	inline float* get_address_of_ScaleDeltaMaximum_24() { return &___ScaleDeltaMaximum_24; }
	inline void set_ScaleDeltaMaximum_24(float value)
	{
		___ScaleDeltaMaximum_24 = value;
	}

	inline static int32_t get_offset_of_ScaleSpeedFactor_25() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___ScaleSpeedFactor_25)); }
	inline float get_ScaleSpeedFactor_25() const { return ___ScaleSpeedFactor_25; }
	inline float* get_address_of_ScaleSpeedFactor_25() { return &___ScaleSpeedFactor_25; }
	inline void set_ScaleSpeedFactor_25(float value)
	{
		___ScaleSpeedFactor_25 = value;
	}

	inline static int32_t get_offset_of_ScaleMaximumTolerance_26() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___ScaleMaximumTolerance_26)); }
	inline float get_ScaleMaximumTolerance_26() const { return ___ScaleMaximumTolerance_26; }
	inline float* get_address_of_ScaleMaximumTolerance_26() { return &___ScaleMaximumTolerance_26; }
	inline void set_ScaleMaximumTolerance_26(float value)
	{
		___ScaleMaximumTolerance_26 = value;
	}

	inline static int32_t get_offset_of_ScaleMinimumTolerance_27() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___ScaleMinimumTolerance_27)); }
	inline float get_ScaleMinimumTolerance_27() const { return ___ScaleMinimumTolerance_27; }
	inline float* get_address_of_ScaleMinimumTolerance_27() { return &___ScaleMinimumTolerance_27; }
	inline void set_ScaleMinimumTolerance_27(float value)
	{
		___ScaleMinimumTolerance_27 = value;
	}

	inline static int32_t get_offset_of_tapToSelect_28() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___tapToSelect_28)); }
	inline bool get_tapToSelect_28() const { return ___tapToSelect_28; }
	inline bool* get_address_of_tapToSelect_28() { return &___tapToSelect_28; }
	inline void set_tapToSelect_28(bool value)
	{
		___tapToSelect_28 = value;
	}

	inline static int32_t get_offset_of_PanRelativeToWorld_29() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___PanRelativeToWorld_29)); }
	inline bool get_PanRelativeToWorld_29() const { return ___PanRelativeToWorld_29; }
	inline bool* get_address_of_PanRelativeToWorld_29() { return &___PanRelativeToWorld_29; }
	inline void set_PanRelativeToWorld_29(bool value)
	{
		___PanRelativeToWorld_29 = value;
	}

	inline static int32_t get_offset_of_currentCamera_31() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___currentCamera_31)); }
	inline GameObject_t3674682005 * get_currentCamera_31() const { return ___currentCamera_31; }
	inline GameObject_t3674682005 ** get_address_of_currentCamera_31() { return &___currentCamera_31; }
	inline void set_currentCamera_31(GameObject_t3674682005 * value)
	{
		___currentCamera_31 = value;
		Il2CppCodeGenWriteBarrier(&___currentCamera_31, value);
	}

	inline static int32_t get_offset_of_cameraToControl_32() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___cameraToControl_32)); }
	inline GameObject_t3674682005 * get_cameraToControl_32() const { return ___cameraToControl_32; }
	inline GameObject_t3674682005 ** get_address_of_cameraToControl_32() { return &___cameraToControl_32; }
	inline void set_cameraToControl_32(GameObject_t3674682005 * value)
	{
		___cameraToControl_32 = value;
		Il2CppCodeGenWriteBarrier(&___cameraToControl_32, value);
	}

	inline static int32_t get_offset_of_cameraToReference_33() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___cameraToReference_33)); }
	inline GameObject_t3674682005 * get_cameraToReference_33() const { return ___cameraToReference_33; }
	inline GameObject_t3674682005 ** get_address_of_cameraToReference_33() { return &___cameraToReference_33; }
	inline void set_cameraToReference_33(GameObject_t3674682005 * value)
	{
		___cameraToReference_33 = value;
		Il2CppCodeGenWriteBarrier(&___cameraToReference_33, value);
	}

	inline static int32_t get_offset_of_cameraToControlTargetPivot_34() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___cameraToControlTargetPivot_34)); }
	inline Vector3_t4282066566  get_cameraToControlTargetPivot_34() const { return ___cameraToControlTargetPivot_34; }
	inline Vector3_t4282066566 * get_address_of_cameraToControlTargetPivot_34() { return &___cameraToControlTargetPivot_34; }
	inline void set_cameraToControlTargetPivot_34(Vector3_t4282066566  value)
	{
		___cameraToControlTargetPivot_34 = value;
	}

	inline static int32_t get_offset_of_cameraToReferenceTargetPivot_35() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___cameraToReferenceTargetPivot_35)); }
	inline Vector3_t4282066566  get_cameraToReferenceTargetPivot_35() const { return ___cameraToReferenceTargetPivot_35; }
	inline Vector3_t4282066566 * get_address_of_cameraToReferenceTargetPivot_35() { return &___cameraToReferenceTargetPivot_35; }
	inline void set_cameraToReferenceTargetPivot_35(Vector3_t4282066566  value)
	{
		___cameraToReferenceTargetPivot_35 = value;
	}

	inline static int32_t get_offset_of_cameraToControlInitalTargetDistance_36() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___cameraToControlInitalTargetDistance_36)); }
	inline float get_cameraToControlInitalTargetDistance_36() const { return ___cameraToControlInitalTargetDistance_36; }
	inline float* get_address_of_cameraToControlInitalTargetDistance_36() { return &___cameraToControlInitalTargetDistance_36; }
	inline void set_cameraToControlInitalTargetDistance_36(float value)
	{
		___cameraToControlInitalTargetDistance_36 = value;
	}

	inline static int32_t get_offset_of_cameraToReferenceInitalTargetDistance_37() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___cameraToReferenceInitalTargetDistance_37)); }
	inline float get_cameraToReferenceInitalTargetDistance_37() const { return ___cameraToReferenceInitalTargetDistance_37; }
	inline float* get_address_of_cameraToReferenceInitalTargetDistance_37() { return &___cameraToReferenceInitalTargetDistance_37; }
	inline void set_cameraToReferenceInitalTargetDistance_37(float value)
	{
		___cameraToReferenceInitalTargetDistance_37 = value;
	}

	inline static int32_t get_offset_of_cameraToControlTargetDistance_38() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___cameraToControlTargetDistance_38)); }
	inline float get_cameraToControlTargetDistance_38() const { return ___cameraToControlTargetDistance_38; }
	inline float* get_address_of_cameraToControlTargetDistance_38() { return &___cameraToControlTargetDistance_38; }
	inline void set_cameraToControlTargetDistance_38(float value)
	{
		___cameraToControlTargetDistance_38 = value;
	}

	inline static int32_t get_offset_of_cameraToReferenceTargetDistance_39() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___cameraToReferenceTargetDistance_39)); }
	inline float get_cameraToReferenceTargetDistance_39() const { return ___cameraToReferenceTargetDistance_39; }
	inline float* get_address_of_cameraToReferenceTargetDistance_39() { return &___cameraToReferenceTargetDistance_39; }
	inline void set_cameraToReferenceTargetDistance_39(float value)
	{
		___cameraToReferenceTargetDistance_39 = value;
	}

	inline static int32_t get_offset_of_cameraToControlCurrentPercentage_40() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___cameraToControlCurrentPercentage_40)); }
	inline float get_cameraToControlCurrentPercentage_40() const { return ___cameraToControlCurrentPercentage_40; }
	inline float* get_address_of_cameraToControlCurrentPercentage_40() { return &___cameraToControlCurrentPercentage_40; }
	inline void set_cameraToControlCurrentPercentage_40(float value)
	{
		___cameraToControlCurrentPercentage_40 = value;
	}

	inline static int32_t get_offset_of_cameraToReferenceCurrentPercentage_41() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___cameraToReferenceCurrentPercentage_41)); }
	inline float get_cameraToReferenceCurrentPercentage_41() const { return ___cameraToReferenceCurrentPercentage_41; }
	inline float* get_address_of_cameraToReferenceCurrentPercentage_41() { return &___cameraToReferenceCurrentPercentage_41; }
	inline void set_cameraToReferenceCurrentPercentage_41(float value)
	{
		___cameraToReferenceCurrentPercentage_41 = value;
	}

	inline static int32_t get_offset_of_cameraCurrentZoom_42() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___cameraCurrentZoom_42)); }
	inline float get_cameraCurrentZoom_42() const { return ___cameraCurrentZoom_42; }
	inline float* get_address_of_cameraCurrentZoom_42() { return &___cameraCurrentZoom_42; }
	inline void set_cameraCurrentZoom_42(float value)
	{
		___cameraCurrentZoom_42 = value;
	}

	inline static int32_t get_offset_of_cameraZoom_43() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___cameraZoom_43)); }
	inline float get_cameraZoom_43() const { return ___cameraZoom_43; }
	inline float* get_address_of_cameraZoom_43() { return &___cameraZoom_43; }
	inline void set_cameraZoom_43(float value)
	{
		___cameraZoom_43 = value;
	}

	inline static int32_t get_offset_of_cameraZoomBonus_44() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___cameraZoomBonus_44)); }
	inline float get_cameraZoomBonus_44() const { return ___cameraZoomBonus_44; }
	inline float* get_address_of_cameraZoomBonus_44() { return &___cameraZoomBonus_44; }
	inline void set_cameraZoomBonus_44(float value)
	{
		___cameraZoomBonus_44 = value;
	}

	inline static int32_t get_offset_of_cameraRelativeRatio_45() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___cameraRelativeRatio_45)); }
	inline float get_cameraRelativeRatio_45() const { return ___cameraRelativeRatio_45; }
	inline float* get_address_of_cameraRelativeRatio_45() { return &___cameraRelativeRatio_45; }
	inline void set_cameraRelativeRatio_45(float value)
	{
		___cameraRelativeRatio_45 = value;
	}

	inline static int32_t get_offset_of_camerasDistance_46() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___camerasDistance_46)); }
	inline float get_camerasDistance_46() const { return ___camerasDistance_46; }
	inline float* get_address_of_camerasDistance_46() { return &___camerasDistance_46; }
	inline void set_camerasDistance_46(float value)
	{
		___camerasDistance_46 = value;
	}

	inline static int32_t get_offset_of_camerasMaxDistance_47() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___camerasMaxDistance_47)); }
	inline float get_camerasMaxDistance_47() const { return ___camerasMaxDistance_47; }
	inline float* get_address_of_camerasMaxDistance_47() { return &___camerasMaxDistance_47; }
	inline void set_camerasMaxDistance_47(float value)
	{
		___camerasMaxDistance_47 = value;
	}

	inline static int32_t get_offset_of_camerasMinDistance_48() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___camerasMinDistance_48)); }
	inline float get_camerasMinDistance_48() const { return ___camerasMinDistance_48; }
	inline float* get_address_of_camerasMinDistance_48() { return &___camerasMinDistance_48; }
	inline void set_camerasMinDistance_48(float value)
	{
		___camerasMinDistance_48 = value;
	}

	inline static int32_t get_offset_of_currentState_49() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ___currentState_49)); }
	inline int32_t get_currentState_49() const { return ___currentState_49; }
	inline int32_t* get_address_of_currentState_49() { return &___currentState_49; }
	inline void set_currentState_49(int32_t value)
	{
		___currentState_49 = value;
	}

	inline static int32_t get_offset_of__angle2Touches_50() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ____angle2Touches_50)); }
	inline float get__angle2Touches_50() const { return ____angle2Touches_50; }
	inline float* get_address_of__angle2Touches_50() { return &____angle2Touches_50; }
	inline void set__angle2Touches_50(float value)
	{
		____angle2Touches_50 = value;
	}

	inline static int32_t get_offset_of__touchCount_51() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581, ____touchCount_51)); }
	inline int32_t get__touchCount_51() const { return ____touchCount_51; }
	inline int32_t* get_address_of__touchCount_51() { return &____touchCount_51; }
	inline void set__touchCount_51(int32_t value)
	{
		____touchCount_51 = value;
	}
};

struct PinchToutchController_t1962414581_StaticFields
{
public:
	// PinchToutchController PinchToutchController::SelectedObject
	PinchToutchController_t1962414581 * ___SelectedObject_30;

public:
	inline static int32_t get_offset_of_SelectedObject_30() { return static_cast<int32_t>(offsetof(PinchToutchController_t1962414581_StaticFields, ___SelectedObject_30)); }
	inline PinchToutchController_t1962414581 * get_SelectedObject_30() const { return ___SelectedObject_30; }
	inline PinchToutchController_t1962414581 ** get_address_of_SelectedObject_30() { return &___SelectedObject_30; }
	inline void set_SelectedObject_30(PinchToutchController_t1962414581 * value)
	{
		___SelectedObject_30 = value;
		Il2CppCodeGenWriteBarrier(&___SelectedObject_30, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
