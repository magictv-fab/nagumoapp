﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[][]
struct Int32U5BU5DU5BU5D_t1820556512;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// ZXing.QrCode.Internal.ErrorCorrectionLevel
struct ErrorCorrectionLevel_t1225927610;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.FormatInformation
struct  FormatInformation_t3156512315  : public Il2CppObject
{
public:
	// ZXing.QrCode.Internal.ErrorCorrectionLevel ZXing.QrCode.Internal.FormatInformation::errorCorrectionLevel
	ErrorCorrectionLevel_t1225927610 * ___errorCorrectionLevel_2;
	// System.Byte ZXing.QrCode.Internal.FormatInformation::dataMask
	uint8_t ___dataMask_3;

public:
	inline static int32_t get_offset_of_errorCorrectionLevel_2() { return static_cast<int32_t>(offsetof(FormatInformation_t3156512315, ___errorCorrectionLevel_2)); }
	inline ErrorCorrectionLevel_t1225927610 * get_errorCorrectionLevel_2() const { return ___errorCorrectionLevel_2; }
	inline ErrorCorrectionLevel_t1225927610 ** get_address_of_errorCorrectionLevel_2() { return &___errorCorrectionLevel_2; }
	inline void set_errorCorrectionLevel_2(ErrorCorrectionLevel_t1225927610 * value)
	{
		___errorCorrectionLevel_2 = value;
		Il2CppCodeGenWriteBarrier(&___errorCorrectionLevel_2, value);
	}

	inline static int32_t get_offset_of_dataMask_3() { return static_cast<int32_t>(offsetof(FormatInformation_t3156512315, ___dataMask_3)); }
	inline uint8_t get_dataMask_3() const { return ___dataMask_3; }
	inline uint8_t* get_address_of_dataMask_3() { return &___dataMask_3; }
	inline void set_dataMask_3(uint8_t value)
	{
		___dataMask_3 = value;
	}
};

struct FormatInformation_t3156512315_StaticFields
{
public:
	// System.Int32[][] ZXing.QrCode.Internal.FormatInformation::FORMAT_INFO_DECODE_LOOKUP
	Int32U5BU5DU5BU5D_t1820556512* ___FORMAT_INFO_DECODE_LOOKUP_0;
	// System.Int32[] ZXing.QrCode.Internal.FormatInformation::BITS_SET_IN_HALF_BYTE
	Int32U5BU5D_t3230847821* ___BITS_SET_IN_HALF_BYTE_1;

public:
	inline static int32_t get_offset_of_FORMAT_INFO_DECODE_LOOKUP_0() { return static_cast<int32_t>(offsetof(FormatInformation_t3156512315_StaticFields, ___FORMAT_INFO_DECODE_LOOKUP_0)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_FORMAT_INFO_DECODE_LOOKUP_0() const { return ___FORMAT_INFO_DECODE_LOOKUP_0; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_FORMAT_INFO_DECODE_LOOKUP_0() { return &___FORMAT_INFO_DECODE_LOOKUP_0; }
	inline void set_FORMAT_INFO_DECODE_LOOKUP_0(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___FORMAT_INFO_DECODE_LOOKUP_0 = value;
		Il2CppCodeGenWriteBarrier(&___FORMAT_INFO_DECODE_LOOKUP_0, value);
	}

	inline static int32_t get_offset_of_BITS_SET_IN_HALF_BYTE_1() { return static_cast<int32_t>(offsetof(FormatInformation_t3156512315_StaticFields, ___BITS_SET_IN_HALF_BYTE_1)); }
	inline Int32U5BU5D_t3230847821* get_BITS_SET_IN_HALF_BYTE_1() const { return ___BITS_SET_IN_HALF_BYTE_1; }
	inline Int32U5BU5D_t3230847821** get_address_of_BITS_SET_IN_HALF_BYTE_1() { return &___BITS_SET_IN_HALF_BYTE_1; }
	inline void set_BITS_SET_IN_HALF_BYTE_1(Int32U5BU5D_t3230847821* value)
	{
		___BITS_SET_IN_HALF_BYTE_1 = value;
		Il2CppCodeGenWriteBarrier(&___BITS_SET_IN_HALF_BYTE_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
