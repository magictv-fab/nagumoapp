﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SampleAppUIRadioButton
struct SampleAppUIRadioButton_t3067317570;
// System.String[]
struct StringU5BU5D_t4054002952;
// UnityEngine.GUIStyle[]
struct GUIStyleU5BU5D_t565654559;
// System.Action`1<System.Int32>
struct Action_1_t1549654636;

#include "codegen/il2cpp-codegen.h"

// System.Void SampleAppUIRadioButton::.ctor(System.Single,System.Int32,System.String[],UnityEngine.GUIStyle[])
extern "C"  void SampleAppUIRadioButton__ctor_m3636846712 (SampleAppUIRadioButton_t3067317570 * __this, float ___index0, int32_t ___selectedId1, StringU5BU5D_t4054002952* ___title2, GUIStyleU5BU5D_t565654559* ___styles3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SampleAppUIRadioButton::add_TappedOnOption(System.Action`1<System.Int32>)
extern "C"  void SampleAppUIRadioButton_add_TappedOnOption_m2279324272 (SampleAppUIRadioButton_t3067317570 * __this, Action_1_t1549654636 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SampleAppUIRadioButton::remove_TappedOnOption(System.Action`1<System.Int32>)
extern "C"  void SampleAppUIRadioButton_remove_TappedOnOption_m199893159 (SampleAppUIRadioButton_t3067317570 * __this, Action_1_t1549654636 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SampleAppUIRadioButton::get_Width()
extern "C"  float SampleAppUIRadioButton_get_Width_m3077218248 (SampleAppUIRadioButton_t3067317570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SampleAppUIRadioButton::get_Height()
extern "C"  float SampleAppUIRadioButton_get_Height_m1408299527 (SampleAppUIRadioButton_t3067317570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SampleAppUIRadioButton::EnableIndex(System.Int32)
extern "C"  void SampleAppUIRadioButton_EnableIndex_m4061648855 (SampleAppUIRadioButton_t3067317570 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SampleAppUIRadioButton::SetToTrue()
extern "C"  bool SampleAppUIRadioButton_SetToTrue_m2090589038 (SampleAppUIRadioButton_t3067317570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SampleAppUIRadioButton::Draw()
extern "C"  void SampleAppUIRadioButton_Draw_m272830671 (SampleAppUIRadioButton_t3067317570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
