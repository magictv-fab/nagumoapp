﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LitJson.JsonMapper
struct JsonMapper_t863513565;
// System.Type
struct Type_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Object
struct Il2CppObject;
// LitJson.JsonReader
struct JsonReader_t1009895007;
// LitJson.IJsonWrapper
struct IJsonWrapper_t2026182966;
// LitJson.WrapperFactory
struct WrapperFactory_t3264289803;
// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>>
struct IDictionary_2_t1504911478;
// LitJson.ImporterFunc
struct ImporterFunc_t2138319818;
// LitJson.JsonWriter
struct JsonWriter_t1165300239;
// System.String
struct String_t;
// LitJson.JsonData
struct JsonData_t1715015430;
// System.IO.TextReader
struct TextReader_t2148718976;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Reflection.FieldInfo
struct FieldInfo_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_LitJson_JsonReader1009895007.h"
#include "AssemblyU2DCSharp_LitJson_WrapperFactory3264289803.h"
#include "AssemblyU2DCSharp_LitJson_ImporterFunc2138319818.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_LitJson_JsonWriter1165300239.h"
#include "mscorlib_System_IO_TextReader2148718976.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Reflection_PropertyInfo924268725.h"
#include "mscorlib_System_Reflection_FieldInfo3973053266.h"

// System.Void LitJson.JsonMapper::.ctor()
extern "C"  void JsonMapper__ctor_m2397169891 (JsonMapper_t863513565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMapper::.cctor()
extern "C"  void JsonMapper__cctor_m815726378 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMapper::AddArrayMetadata(System.Type)
extern "C"  void JsonMapper_AddArrayMetadata_m1106054833 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMapper::AddObjectMetadata(System.Type)
extern "C"  void JsonMapper_AddObjectMetadata_m3549891753 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMapper::AddTypeProperties(System.Type)
extern "C"  void JsonMapper_AddTypeProperties_m4244483722 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo LitJson.JsonMapper::GetConvOp(System.Type,System.Type)
extern "C"  MethodInfo_t * JsonMapper_GetConvOp_m1713943530 (Il2CppObject * __this /* static, unused */, Type_t * ___t10, Type_t * ___t21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LitJson.JsonMapper::ReadValue(System.Type,LitJson.JsonReader)
extern "C"  Il2CppObject * JsonMapper_ReadValue_m3418598490 (Il2CppObject * __this /* static, unused */, Type_t * ___inst_type0, JsonReader_t1009895007 * ___reader1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LitJson.IJsonWrapper LitJson.JsonMapper::ReadValue(LitJson.WrapperFactory,LitJson.JsonReader)
extern "C"  Il2CppObject * JsonMapper_ReadValue_m2487666306 (Il2CppObject * __this /* static, unused */, WrapperFactory_t3264289803 * ___factory0, JsonReader_t1009895007 * ___reader1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMapper::RegisterBaseExporters()
extern "C"  void JsonMapper_RegisterBaseExporters_m3881051647 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMapper::RegisterBaseImporters()
extern "C"  void JsonMapper_RegisterBaseImporters_m3149862062 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMapper::RegisterImporter(System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>>,System.Type,System.Type,LitJson.ImporterFunc)
extern "C"  void JsonMapper_RegisterImporter_m910348426 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___table0, Type_t * ___json_type1, Type_t * ___value_type2, ImporterFunc_t2138319818 * ___importer3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMapper::WriteValue(System.Object,LitJson.JsonWriter,System.Boolean,System.Int32)
extern "C"  void JsonMapper_WriteValue_m1759016917 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsonWriter_t1165300239 * ___writer1, bool ___writer_is_private2, int32_t ___depth3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LitJson.JsonMapper::ToJson(System.Object)
extern "C"  String_t* JsonMapper_ToJson_m620795917 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMapper::ToJson(System.Object,LitJson.JsonWriter)
extern "C"  void JsonMapper_ToJson_m740995788 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsonWriter_t1165300239 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LitJson.JsonData LitJson.JsonMapper::ToObject(LitJson.JsonReader)
extern "C"  JsonData_t1715015430 * JsonMapper_ToObject_m1791763101 (Il2CppObject * __this /* static, unused */, JsonReader_t1009895007 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LitJson.JsonData LitJson.JsonMapper::ToObject(System.IO.TextReader)
extern "C"  JsonData_t1715015430 * JsonMapper_ToObject_m2806513502 (Il2CppObject * __this /* static, unused */, TextReader_t2148718976 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LitJson.JsonData LitJson.JsonMapper::ToObject(System.String)
extern "C"  JsonData_t1715015430 * JsonMapper_ToObject_m4013727819 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LitJson.IJsonWrapper LitJson.JsonMapper::ToWrapper(LitJson.WrapperFactory,LitJson.JsonReader)
extern "C"  Il2CppObject * JsonMapper_ToWrapper_m2617093701 (Il2CppObject * __this /* static, unused */, WrapperFactory_t3264289803 * ___factory0, JsonReader_t1009895007 * ___reader1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LitJson.IJsonWrapper LitJson.JsonMapper::ToWrapper(LitJson.WrapperFactory,System.String)
extern "C"  Il2CppObject * JsonMapper_ToWrapper_m4150029219 (Il2CppObject * __this /* static, unused */, WrapperFactory_t3264289803 * ___factory0, String_t* ___json1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMapper::UnregisterExporters()
extern "C"  void JsonMapper_UnregisterExporters_m462211287 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMapper::UnregisterImporters()
extern "C"  void JsonMapper_UnregisterImporters_m4025988998 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMapper::<AddObjectMetadata>m__4F(System.Reflection.PropertyInfo)
extern "C"  bool JsonMapper_U3CAddObjectMetadataU3Em__4F_m4073888610 (Il2CppObject * __this /* static, unused */, PropertyInfo_t * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMapper::<AddObjectMetadata>m__50(System.Object)
extern "C"  bool JsonMapper_U3CAddObjectMetadataU3Em__50_m1743624226 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMapper::<AddObjectMetadata>m__51(System.Reflection.FieldInfo)
extern "C"  bool JsonMapper_U3CAddObjectMetadataU3Em__51_m1916028699 (Il2CppObject * __this /* static, unused */, FieldInfo_t * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMapper::<AddObjectMetadata>m__52(System.Object)
extern "C"  bool JsonMapper_U3CAddObjectMetadataU3Em__52_m722555872 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMapper::<RegisterBaseExporters>m__55(System.Object,LitJson.JsonWriter)
extern "C"  void JsonMapper_U3CRegisterBaseExportersU3Em__55_m3330967354 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsonWriter_t1165300239 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMapper::<RegisterBaseExporters>m__56(System.Object,LitJson.JsonWriter)
extern "C"  void JsonMapper_U3CRegisterBaseExportersU3Em__56_m761480955 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsonWriter_t1165300239 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMapper::<RegisterBaseExporters>m__57(System.Object,LitJson.JsonWriter)
extern "C"  void JsonMapper_U3CRegisterBaseExportersU3Em__57_m2486961852 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsonWriter_t1165300239 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMapper::<RegisterBaseExporters>m__58(System.Object,LitJson.JsonWriter)
extern "C"  void JsonMapper_U3CRegisterBaseExportersU3Em__58_m4212442749 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsonWriter_t1165300239 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMapper::<RegisterBaseExporters>m__59(System.Object,LitJson.JsonWriter)
extern "C"  void JsonMapper_U3CRegisterBaseExportersU3Em__59_m1642956350 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsonWriter_t1165300239 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMapper::<RegisterBaseExporters>m__5A(System.Object,LitJson.JsonWriter)
extern "C"  void JsonMapper_U3CRegisterBaseExportersU3Em__5A_m2561901638 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsonWriter_t1165300239 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMapper::<RegisterBaseExporters>m__5B(System.Object,LitJson.JsonWriter)
extern "C"  void JsonMapper_U3CRegisterBaseExportersU3Em__5B_m4287382535 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsonWriter_t1165300239 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMapper::<RegisterBaseExporters>m__5C(System.Object,LitJson.JsonWriter)
extern "C"  void JsonMapper_U3CRegisterBaseExportersU3Em__5C_m1717896136 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsonWriter_t1165300239 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMapper::<RegisterBaseExporters>m__5D(System.Object,LitJson.JsonWriter)
extern "C"  void JsonMapper_U3CRegisterBaseExportersU3Em__5D_m3443377033 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsonWriter_t1165300239 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMapper::<RegisterBaseExporters>m__5E(System.Object,LitJson.JsonWriter)
extern "C"  void JsonMapper_U3CRegisterBaseExportersU3Em__5E_m873890634 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsonWriter_t1165300239 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__5F(System.Object)
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__5F_m2992037517 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__60(System.Object)
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__60_m2692197220 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__61(System.Object)
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__61_m2181663043 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__62(System.Object)
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__62_m1671128866 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__63(System.Object)
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__63_m1160594689 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__64(System.Object)
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__64_m650060512 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__65(System.Object)
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__65_m139526335 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__66(System.Object)
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__66_m3923959454 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__67(System.Object)
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__67_m3413425277 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__68(System.Object)
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__68_m2902891100 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__69(System.Object)
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__69_m2392356923 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__6A(System.Object)
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__6A_m2603050803 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__6B(System.Object)
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__6B_m2092516626 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMapper::<WriteValue>m__6C(System.Object)
extern "C"  bool JsonMapper_U3CWriteValueU3Em__6C_m1368273341 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMapper::<WriteValue>m__6D(System.Object)
extern "C"  bool JsonMapper_U3CWriteValueU3Em__6D_m857739164 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LitJson.IJsonWrapper LitJson.JsonMapper::<ToObject>m__6E()
extern "C"  Il2CppObject * JsonMapper_U3CToObjectU3Em__6E_m3227098601 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LitJson.IJsonWrapper LitJson.JsonMapper::<ToObject>m__6F()
extern "C"  Il2CppObject * JsonMapper_U3CToObjectU3Em__6F_m3227099562 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LitJson.IJsonWrapper LitJson.JsonMapper::<ToObject>m__70()
extern "C"  Il2CppObject * JsonMapper_U3CToObjectU3Em__70_m3227108211 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
