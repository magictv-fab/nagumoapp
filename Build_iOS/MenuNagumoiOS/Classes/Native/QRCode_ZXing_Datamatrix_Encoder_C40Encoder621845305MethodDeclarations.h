﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Datamatrix.Encoder.C40Encoder
struct C40Encoder_t621845305;
// ZXing.Datamatrix.Encoder.EncoderContext
struct EncoderContext_t1774722223;
// System.Text.StringBuilder
struct StringBuilder_t243639308;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Datamatrix_Encoder_EncoderContext1774722223.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"

// System.Int32 ZXing.Datamatrix.Encoder.C40Encoder::get_EncodingMode()
extern "C"  int32_t C40Encoder_get_EncodingMode_m1035840178 (C40Encoder_t621845305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.C40Encoder::encode(ZXing.Datamatrix.Encoder.EncoderContext)
extern "C"  void C40Encoder_encode_m1106101309 (C40Encoder_t621845305 * __this, EncoderContext_t1774722223 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Encoder.C40Encoder::backtrackOneCharacter(ZXing.Datamatrix.Encoder.EncoderContext,System.Text.StringBuilder,System.Text.StringBuilder,System.Int32)
extern "C"  int32_t C40Encoder_backtrackOneCharacter_m2265959473 (C40Encoder_t621845305 * __this, EncoderContext_t1774722223 * ___context0, StringBuilder_t243639308 * ___buffer1, StringBuilder_t243639308 * ___removed2, int32_t ___lastCharSize3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.C40Encoder::writeNextTriplet(ZXing.Datamatrix.Encoder.EncoderContext,System.Text.StringBuilder)
extern "C"  void C40Encoder_writeNextTriplet_m3925279603 (Il2CppObject * __this /* static, unused */, EncoderContext_t1774722223 * ___context0, StringBuilder_t243639308 * ___buffer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.C40Encoder::handleEOD(ZXing.Datamatrix.Encoder.EncoderContext,System.Text.StringBuilder)
extern "C"  void C40Encoder_handleEOD_m1763613701 (C40Encoder_t621845305 * __this, EncoderContext_t1774722223 * ___context0, StringBuilder_t243639308 * ___buffer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Encoder.C40Encoder::encodeChar(System.Char,System.Text.StringBuilder)
extern "C"  int32_t C40Encoder_encodeChar_m2909245090 (C40Encoder_t621845305 * __this, Il2CppChar ___c0, StringBuilder_t243639308 * ___sb1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.Datamatrix.Encoder.C40Encoder::encodeToCodewords(System.Text.StringBuilder,System.Int32)
extern "C"  String_t* C40Encoder_encodeToCodewords_m339359100 (Il2CppObject * __this /* static, unused */, StringBuilder_t243639308 * ___sb0, int32_t ___startPos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.C40Encoder::.ctor()
extern "C"  void C40Encoder__ctor_m3307832131 (C40Encoder_t621845305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
