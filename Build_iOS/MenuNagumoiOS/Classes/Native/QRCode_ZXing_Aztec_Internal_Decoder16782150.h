﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t4054002952;
// System.Collections.Generic.IDictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.String[]>
struct IDictionary_2_t2219148591;
// System.Collections.Generic.IDictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>
struct IDictionary_2_t675913019;
// ZXing.Aztec.Internal.AztecDetectorResult
struct AztecDetectorResult_t1662194462;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Aztec.Internal.Decoder
struct  Decoder_t16782150  : public Il2CppObject
{
public:
	// ZXing.Aztec.Internal.AztecDetectorResult ZXing.Aztec.Internal.Decoder::ddata
	AztecDetectorResult_t1662194462 * ___ddata_7;

public:
	inline static int32_t get_offset_of_ddata_7() { return static_cast<int32_t>(offsetof(Decoder_t16782150, ___ddata_7)); }
	inline AztecDetectorResult_t1662194462 * get_ddata_7() const { return ___ddata_7; }
	inline AztecDetectorResult_t1662194462 ** get_address_of_ddata_7() { return &___ddata_7; }
	inline void set_ddata_7(AztecDetectorResult_t1662194462 * value)
	{
		___ddata_7 = value;
		Il2CppCodeGenWriteBarrier(&___ddata_7, value);
	}
};

struct Decoder_t16782150_StaticFields
{
public:
	// System.String[] ZXing.Aztec.Internal.Decoder::UPPER_TABLE
	StringU5BU5D_t4054002952* ___UPPER_TABLE_0;
	// System.String[] ZXing.Aztec.Internal.Decoder::LOWER_TABLE
	StringU5BU5D_t4054002952* ___LOWER_TABLE_1;
	// System.String[] ZXing.Aztec.Internal.Decoder::MIXED_TABLE
	StringU5BU5D_t4054002952* ___MIXED_TABLE_2;
	// System.String[] ZXing.Aztec.Internal.Decoder::PUNCT_TABLE
	StringU5BU5D_t4054002952* ___PUNCT_TABLE_3;
	// System.String[] ZXing.Aztec.Internal.Decoder::DIGIT_TABLE
	StringU5BU5D_t4054002952* ___DIGIT_TABLE_4;
	// System.Collections.Generic.IDictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.String[]> ZXing.Aztec.Internal.Decoder::codeTables
	Il2CppObject* ___codeTables_5;
	// System.Collections.Generic.IDictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table> ZXing.Aztec.Internal.Decoder::codeTableMap
	Il2CppObject* ___codeTableMap_6;

public:
	inline static int32_t get_offset_of_UPPER_TABLE_0() { return static_cast<int32_t>(offsetof(Decoder_t16782150_StaticFields, ___UPPER_TABLE_0)); }
	inline StringU5BU5D_t4054002952* get_UPPER_TABLE_0() const { return ___UPPER_TABLE_0; }
	inline StringU5BU5D_t4054002952** get_address_of_UPPER_TABLE_0() { return &___UPPER_TABLE_0; }
	inline void set_UPPER_TABLE_0(StringU5BU5D_t4054002952* value)
	{
		___UPPER_TABLE_0 = value;
		Il2CppCodeGenWriteBarrier(&___UPPER_TABLE_0, value);
	}

	inline static int32_t get_offset_of_LOWER_TABLE_1() { return static_cast<int32_t>(offsetof(Decoder_t16782150_StaticFields, ___LOWER_TABLE_1)); }
	inline StringU5BU5D_t4054002952* get_LOWER_TABLE_1() const { return ___LOWER_TABLE_1; }
	inline StringU5BU5D_t4054002952** get_address_of_LOWER_TABLE_1() { return &___LOWER_TABLE_1; }
	inline void set_LOWER_TABLE_1(StringU5BU5D_t4054002952* value)
	{
		___LOWER_TABLE_1 = value;
		Il2CppCodeGenWriteBarrier(&___LOWER_TABLE_1, value);
	}

	inline static int32_t get_offset_of_MIXED_TABLE_2() { return static_cast<int32_t>(offsetof(Decoder_t16782150_StaticFields, ___MIXED_TABLE_2)); }
	inline StringU5BU5D_t4054002952* get_MIXED_TABLE_2() const { return ___MIXED_TABLE_2; }
	inline StringU5BU5D_t4054002952** get_address_of_MIXED_TABLE_2() { return &___MIXED_TABLE_2; }
	inline void set_MIXED_TABLE_2(StringU5BU5D_t4054002952* value)
	{
		___MIXED_TABLE_2 = value;
		Il2CppCodeGenWriteBarrier(&___MIXED_TABLE_2, value);
	}

	inline static int32_t get_offset_of_PUNCT_TABLE_3() { return static_cast<int32_t>(offsetof(Decoder_t16782150_StaticFields, ___PUNCT_TABLE_3)); }
	inline StringU5BU5D_t4054002952* get_PUNCT_TABLE_3() const { return ___PUNCT_TABLE_3; }
	inline StringU5BU5D_t4054002952** get_address_of_PUNCT_TABLE_3() { return &___PUNCT_TABLE_3; }
	inline void set_PUNCT_TABLE_3(StringU5BU5D_t4054002952* value)
	{
		___PUNCT_TABLE_3 = value;
		Il2CppCodeGenWriteBarrier(&___PUNCT_TABLE_3, value);
	}

	inline static int32_t get_offset_of_DIGIT_TABLE_4() { return static_cast<int32_t>(offsetof(Decoder_t16782150_StaticFields, ___DIGIT_TABLE_4)); }
	inline StringU5BU5D_t4054002952* get_DIGIT_TABLE_4() const { return ___DIGIT_TABLE_4; }
	inline StringU5BU5D_t4054002952** get_address_of_DIGIT_TABLE_4() { return &___DIGIT_TABLE_4; }
	inline void set_DIGIT_TABLE_4(StringU5BU5D_t4054002952* value)
	{
		___DIGIT_TABLE_4 = value;
		Il2CppCodeGenWriteBarrier(&___DIGIT_TABLE_4, value);
	}

	inline static int32_t get_offset_of_codeTables_5() { return static_cast<int32_t>(offsetof(Decoder_t16782150_StaticFields, ___codeTables_5)); }
	inline Il2CppObject* get_codeTables_5() const { return ___codeTables_5; }
	inline Il2CppObject** get_address_of_codeTables_5() { return &___codeTables_5; }
	inline void set_codeTables_5(Il2CppObject* value)
	{
		___codeTables_5 = value;
		Il2CppCodeGenWriteBarrier(&___codeTables_5, value);
	}

	inline static int32_t get_offset_of_codeTableMap_6() { return static_cast<int32_t>(offsetof(Decoder_t16782150_StaticFields, ___codeTableMap_6)); }
	inline Il2CppObject* get_codeTableMap_6() const { return ___codeTableMap_6; }
	inline Il2CppObject** get_address_of_codeTableMap_6() { return &___codeTableMap_6; }
	inline void set_codeTableMap_6(Il2CppObject* value)
	{
		___codeTableMap_6 = value;
		Il2CppCodeGenWriteBarrier(&___codeTableMap_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
