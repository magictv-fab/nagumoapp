﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARMVVPChromakeyGUIConfig
struct ARMVVPChromakeyGUIConfig_t1243100968;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ARMVVPChromakeyGUIConfig::.ctor()
extern "C"  void ARMVVPChromakeyGUIConfig__ctor_m82895987 (ARMVVPChromakeyGUIConfig_t1243100968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVVPChromakeyGUIConfig::Start()
extern "C"  void ARMVVPChromakeyGUIConfig_Start_m3325001075 (ARMVVPChromakeyGUIConfig_t1243100968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVVPChromakeyGUIConfig::GUIResetAction()
extern "C"  void ARMVVPChromakeyGUIConfig_GUIResetAction_m592095259 (ARMVVPChromakeyGUIConfig_t1243100968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVVPChromakeyGUIConfig::GUISetSmooth(System.String)
extern "C"  void ARMVVPChromakeyGUIConfig_GUISetSmooth_m1288146396 (ARMVVPChromakeyGUIConfig_t1243100968 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVVPChromakeyGUIConfig::GUISetSmoothSensibility(System.String)
extern "C"  void ARMVVPChromakeyGUIConfig_GUISetSmoothSensibility_m1105186187 (ARMVVPChromakeyGUIConfig_t1243100968 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVVPChromakeyGUIConfig::SetRecorteFromGUI(System.String)
extern "C"  void ARMVVPChromakeyGUIConfig_SetRecorteFromGUI_m3636900802 (ARMVVPChromakeyGUIConfig_t1243100968 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVVPChromakeyGUIConfig::SetSensibilityFromGUI(System.String)
extern "C"  void ARMVVPChromakeyGUIConfig_SetSensibilityFromGUI_m1166821657 (ARMVVPChromakeyGUIConfig_t1243100968 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVVPChromakeyGUIConfig::SetBundleIDFromGUI(System.String)
extern "C"  void ARMVVPChromakeyGUIConfig_SetBundleIDFromGUI_m3936846175 (ARMVVPChromakeyGUIConfig_t1243100968 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVVPChromakeyGUIConfig::SaveString()
extern "C"  void ARMVVPChromakeyGUIConfig_SaveString_m430379743 (ARMVVPChromakeyGUIConfig_t1243100968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVVPChromakeyGUIConfig::doneToSaveChromaqui(System.Boolean)
extern "C"  void ARMVVPChromakeyGUIConfig_doneToSaveChromaqui_m4182787165 (ARMVVPChromakeyGUIConfig_t1243100968 * __this, bool ___success0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVVPChromakeyGUIConfig::changedShder(System.String)
extern "C"  void ARMVVPChromakeyGUIConfig_changedShder_m3012487177 (ARMVVPChromakeyGUIConfig_t1243100968 * __this, String_t* ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVVPChromakeyGUIConfig::changed1(System.Single)
extern "C"  void ARMVVPChromakeyGUIConfig_changed1_m2408588637 (ARMVVPChromakeyGUIConfig_t1243100968 * __this, float ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVVPChromakeyGUIConfig::changed2(System.Single)
extern "C"  void ARMVVPChromakeyGUIConfig_changed2_m1898054460 (ARMVVPChromakeyGUIConfig_t1243100968 * __this, float ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVVPChromakeyGUIConfig::changed3(System.Single)
extern "C"  void ARMVVPChromakeyGUIConfig_changed3_m1387520283 (ARMVVPChromakeyGUIConfig_t1243100968 * __this, float ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVVPChromakeyGUIConfig::changed4(System.Single)
extern "C"  void ARMVVPChromakeyGUIConfig_changed4_m876986106 (ARMVVPChromakeyGUIConfig_t1243100968 * __this, float ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVVPChromakeyGUIConfig::OnGUI()
extern "C"  void ARMVVPChromakeyGUIConfig_OnGUI_m3873261933 (ARMVVPChromakeyGUIConfig_t1243100968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVVPChromakeyGUIConfig::LateUpdate()
extern "C"  void ARMVVPChromakeyGUIConfig_LateUpdate_m3742381504 (ARMVVPChromakeyGUIConfig_t1243100968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVVPChromakeyGUIConfig::changeSmoothSensiblityValue(System.Boolean)
extern "C"  void ARMVVPChromakeyGUIConfig_changeSmoothSensiblityValue_m3204466409 (ARMVVPChromakeyGUIConfig_t1243100968 * __this, bool ___plusorless0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVVPChromakeyGUIConfig::changeSmoothValue(System.Boolean)
extern "C"  void ARMVVPChromakeyGUIConfig_changeSmoothValue_m2368404987 (ARMVVPChromakeyGUIConfig_t1243100968 * __this, bool ___plusorless0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVVPChromakeyGUIConfig::changeSensiblityValue(System.Boolean)
extern "C"  void ARMVVPChromakeyGUIConfig_changeSensiblityValue_m975610487 (ARMVVPChromakeyGUIConfig_t1243100968 * __this, bool ___plusorless0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVVPChromakeyGUIConfig::changeRecorteValue(System.Boolean)
extern "C"  void ARMVVPChromakeyGUIConfig_changeRecorteValue_m2909669253 (ARMVVPChromakeyGUIConfig_t1243100968 * __this, bool ___plusorless0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
