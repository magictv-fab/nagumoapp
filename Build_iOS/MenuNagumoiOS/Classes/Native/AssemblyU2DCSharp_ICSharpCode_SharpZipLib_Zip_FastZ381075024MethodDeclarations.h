﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.FastZipEvents
struct FastZipEvents_t381075024;
// System.String
struct String_t;
// System.Exception
struct Exception_t3991598821;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Exception3991598821.h"
#include "mscorlib_System_TimeSpan413522987.h"

// System.Void ICSharpCode.SharpZipLib.Zip.FastZipEvents::.ctor()
extern "C"  void FastZipEvents__ctor_m578329131 (FastZipEvents_t381075024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.FastZipEvents::OnDirectoryFailure(System.String,System.Exception)
extern "C"  bool FastZipEvents_OnDirectoryFailure_m1891477653 (FastZipEvents_t381075024 * __this, String_t* ___directory0, Exception_t3991598821 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.FastZipEvents::OnFileFailure(System.String,System.Exception)
extern "C"  bool FastZipEvents_OnFileFailure_m702925760 (FastZipEvents_t381075024 * __this, String_t* ___file0, Exception_t3991598821 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.FastZipEvents::OnProcessFile(System.String)
extern "C"  bool FastZipEvents_OnProcessFile_m4119561849 (FastZipEvents_t381075024 * __this, String_t* ___file0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.FastZipEvents::OnCompletedFile(System.String)
extern "C"  bool FastZipEvents_OnCompletedFile_m137421917 (FastZipEvents_t381075024 * __this, String_t* ___file0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.FastZipEvents::OnProcessDirectory(System.String,System.Boolean)
extern "C"  bool FastZipEvents_OnProcessDirectory_m83806525 (FastZipEvents_t381075024 * __this, String_t* ___directory0, bool ___hasMatchingFiles1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan ICSharpCode.SharpZipLib.Zip.FastZipEvents::get_ProgressInterval()
extern "C"  TimeSpan_t413522987  FastZipEvents_get_ProgressInterval_m3019974199 (FastZipEvents_t381075024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.FastZipEvents::set_ProgressInterval(System.TimeSpan)
extern "C"  void FastZipEvents_set_ProgressInterval_m2196853332 (FastZipEvents_t381075024 * __this, TimeSpan_t413522987  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
