﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Translate
struct Translate_t1163392156;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Translate::.ctor()
extern "C"  void Translate__ctor_m3597172762 (Translate_t1163392156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Translate::Reset()
extern "C"  void Translate_Reset_m1243605703 (Translate_t1163392156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Translate::Awake()
extern "C"  void Translate_Awake_m3834777981 (Translate_t1163392156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Translate::OnEnter()
extern "C"  void Translate_OnEnter_m3121046897 (Translate_t1163392156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Translate::OnUpdate()
extern "C"  void Translate_OnUpdate_m1396732658 (Translate_t1163392156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Translate::OnLateUpdate()
extern "C"  void Translate_OnLateUpdate_m3109777336 (Translate_t1163392156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Translate::OnFixedUpdate()
extern "C"  void Translate_OnFixedUpdate_m2789762230 (Translate_t1163392156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Translate::DoTranslate()
extern "C"  void Translate_DoTranslate_m2561891387 (Translate_t1163392156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
