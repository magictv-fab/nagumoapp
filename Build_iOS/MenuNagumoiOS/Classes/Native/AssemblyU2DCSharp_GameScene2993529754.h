﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Animator
struct Animator_t2776330603;
// UnityEngine.AudioClip
struct AudioClip_t794140988;
// UnityEngine.AudioSource
struct AudioSource_t1740077639;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameScene
struct  GameScene_t2993529754  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject GameScene::copterL
	GameObject_t3674682005 * ___copterL_2;
	// UnityEngine.GameObject GameScene::WallSingle
	GameObject_t3674682005 * ___WallSingle_10;
	// UnityEngine.GameObject GameScene::WallDouble
	GameObject_t3674682005 * ___WallDouble_11;
	// UnityEngine.GameObject GameScene::lastWall
	GameObject_t3674682005 * ___lastWall_12;
	// UnityEngine.GameObject GameScene::scoreText
	GameObject_t3674682005 * ___scoreText_13;
	// UnityEngine.GameObject GameScene::bestText
	GameObject_t3674682005 * ___bestText_14;
	// System.Int32 GameScene::scoreNum
	int32_t ___scoreNum_15;
	// System.Int32 GameScene::bestNum
	int32_t ___bestNum_16;
	// System.Single GameScene::diffY1
	float ___diffY1_19;
	// System.Single GameScene::diffY2
	float ___diffY2_20;
	// UnityEngine.Animator GameScene::anim
	Animator_t2776330603 * ___anim_22;
	// UnityEngine.AudioClip GameScene::heliClip
	AudioClip_t794140988 * ___heliClip_23;
	// UnityEngine.AudioClip GameScene::explodeClip
	AudioClip_t794140988 * ___explodeClip_25;

public:
	inline static int32_t get_offset_of_copterL_2() { return static_cast<int32_t>(offsetof(GameScene_t2993529754, ___copterL_2)); }
	inline GameObject_t3674682005 * get_copterL_2() const { return ___copterL_2; }
	inline GameObject_t3674682005 ** get_address_of_copterL_2() { return &___copterL_2; }
	inline void set_copterL_2(GameObject_t3674682005 * value)
	{
		___copterL_2 = value;
		Il2CppCodeGenWriteBarrier(&___copterL_2, value);
	}

	inline static int32_t get_offset_of_WallSingle_10() { return static_cast<int32_t>(offsetof(GameScene_t2993529754, ___WallSingle_10)); }
	inline GameObject_t3674682005 * get_WallSingle_10() const { return ___WallSingle_10; }
	inline GameObject_t3674682005 ** get_address_of_WallSingle_10() { return &___WallSingle_10; }
	inline void set_WallSingle_10(GameObject_t3674682005 * value)
	{
		___WallSingle_10 = value;
		Il2CppCodeGenWriteBarrier(&___WallSingle_10, value);
	}

	inline static int32_t get_offset_of_WallDouble_11() { return static_cast<int32_t>(offsetof(GameScene_t2993529754, ___WallDouble_11)); }
	inline GameObject_t3674682005 * get_WallDouble_11() const { return ___WallDouble_11; }
	inline GameObject_t3674682005 ** get_address_of_WallDouble_11() { return &___WallDouble_11; }
	inline void set_WallDouble_11(GameObject_t3674682005 * value)
	{
		___WallDouble_11 = value;
		Il2CppCodeGenWriteBarrier(&___WallDouble_11, value);
	}

	inline static int32_t get_offset_of_lastWall_12() { return static_cast<int32_t>(offsetof(GameScene_t2993529754, ___lastWall_12)); }
	inline GameObject_t3674682005 * get_lastWall_12() const { return ___lastWall_12; }
	inline GameObject_t3674682005 ** get_address_of_lastWall_12() { return &___lastWall_12; }
	inline void set_lastWall_12(GameObject_t3674682005 * value)
	{
		___lastWall_12 = value;
		Il2CppCodeGenWriteBarrier(&___lastWall_12, value);
	}

	inline static int32_t get_offset_of_scoreText_13() { return static_cast<int32_t>(offsetof(GameScene_t2993529754, ___scoreText_13)); }
	inline GameObject_t3674682005 * get_scoreText_13() const { return ___scoreText_13; }
	inline GameObject_t3674682005 ** get_address_of_scoreText_13() { return &___scoreText_13; }
	inline void set_scoreText_13(GameObject_t3674682005 * value)
	{
		___scoreText_13 = value;
		Il2CppCodeGenWriteBarrier(&___scoreText_13, value);
	}

	inline static int32_t get_offset_of_bestText_14() { return static_cast<int32_t>(offsetof(GameScene_t2993529754, ___bestText_14)); }
	inline GameObject_t3674682005 * get_bestText_14() const { return ___bestText_14; }
	inline GameObject_t3674682005 ** get_address_of_bestText_14() { return &___bestText_14; }
	inline void set_bestText_14(GameObject_t3674682005 * value)
	{
		___bestText_14 = value;
		Il2CppCodeGenWriteBarrier(&___bestText_14, value);
	}

	inline static int32_t get_offset_of_scoreNum_15() { return static_cast<int32_t>(offsetof(GameScene_t2993529754, ___scoreNum_15)); }
	inline int32_t get_scoreNum_15() const { return ___scoreNum_15; }
	inline int32_t* get_address_of_scoreNum_15() { return &___scoreNum_15; }
	inline void set_scoreNum_15(int32_t value)
	{
		___scoreNum_15 = value;
	}

	inline static int32_t get_offset_of_bestNum_16() { return static_cast<int32_t>(offsetof(GameScene_t2993529754, ___bestNum_16)); }
	inline int32_t get_bestNum_16() const { return ___bestNum_16; }
	inline int32_t* get_address_of_bestNum_16() { return &___bestNum_16; }
	inline void set_bestNum_16(int32_t value)
	{
		___bestNum_16 = value;
	}

	inline static int32_t get_offset_of_diffY1_19() { return static_cast<int32_t>(offsetof(GameScene_t2993529754, ___diffY1_19)); }
	inline float get_diffY1_19() const { return ___diffY1_19; }
	inline float* get_address_of_diffY1_19() { return &___diffY1_19; }
	inline void set_diffY1_19(float value)
	{
		___diffY1_19 = value;
	}

	inline static int32_t get_offset_of_diffY2_20() { return static_cast<int32_t>(offsetof(GameScene_t2993529754, ___diffY2_20)); }
	inline float get_diffY2_20() const { return ___diffY2_20; }
	inline float* get_address_of_diffY2_20() { return &___diffY2_20; }
	inline void set_diffY2_20(float value)
	{
		___diffY2_20 = value;
	}

	inline static int32_t get_offset_of_anim_22() { return static_cast<int32_t>(offsetof(GameScene_t2993529754, ___anim_22)); }
	inline Animator_t2776330603 * get_anim_22() const { return ___anim_22; }
	inline Animator_t2776330603 ** get_address_of_anim_22() { return &___anim_22; }
	inline void set_anim_22(Animator_t2776330603 * value)
	{
		___anim_22 = value;
		Il2CppCodeGenWriteBarrier(&___anim_22, value);
	}

	inline static int32_t get_offset_of_heliClip_23() { return static_cast<int32_t>(offsetof(GameScene_t2993529754, ___heliClip_23)); }
	inline AudioClip_t794140988 * get_heliClip_23() const { return ___heliClip_23; }
	inline AudioClip_t794140988 ** get_address_of_heliClip_23() { return &___heliClip_23; }
	inline void set_heliClip_23(AudioClip_t794140988 * value)
	{
		___heliClip_23 = value;
		Il2CppCodeGenWriteBarrier(&___heliClip_23, value);
	}

	inline static int32_t get_offset_of_explodeClip_25() { return static_cast<int32_t>(offsetof(GameScene_t2993529754, ___explodeClip_25)); }
	inline AudioClip_t794140988 * get_explodeClip_25() const { return ___explodeClip_25; }
	inline AudioClip_t794140988 ** get_address_of_explodeClip_25() { return &___explodeClip_25; }
	inline void set_explodeClip_25(AudioClip_t794140988 * value)
	{
		___explodeClip_25 = value;
		Il2CppCodeGenWriteBarrier(&___explodeClip_25, value);
	}
};

struct GameScene_t2993529754_StaticFields
{
public:
	// System.Boolean GameScene::leftUp
	bool ___leftUp_3;
	// System.Boolean GameScene::leftDown
	bool ___leftDown_4;
	// System.Boolean GameScene::gameOver
	bool ___gameOver_5;
	// System.Boolean GameScene::gameStart
	bool ___gameStart_6;
	// System.Single GameScene::speedDown
	float ___speedDown_7;
	// System.Single GameScene::speedWall
	float ___speedWall_8;
	// UnityEngine.GameObject GameScene::btnPlay
	GameObject_t3674682005 * ___btnPlay_9;
	// System.Boolean GameScene::makeNewWall
	bool ___makeNewWall_17;
	// System.Boolean GameScene::initGame
	bool ___initGame_18;
	// System.Int32 GameScene::difficultyNum
	int32_t ___difficultyNum_21;
	// UnityEngine.AudioSource GameScene::heliSource
	AudioSource_t1740077639 * ___heliSource_24;
	// UnityEngine.AudioSource GameScene::explodeSource
	AudioSource_t1740077639 * ___explodeSource_26;

public:
	inline static int32_t get_offset_of_leftUp_3() { return static_cast<int32_t>(offsetof(GameScene_t2993529754_StaticFields, ___leftUp_3)); }
	inline bool get_leftUp_3() const { return ___leftUp_3; }
	inline bool* get_address_of_leftUp_3() { return &___leftUp_3; }
	inline void set_leftUp_3(bool value)
	{
		___leftUp_3 = value;
	}

	inline static int32_t get_offset_of_leftDown_4() { return static_cast<int32_t>(offsetof(GameScene_t2993529754_StaticFields, ___leftDown_4)); }
	inline bool get_leftDown_4() const { return ___leftDown_4; }
	inline bool* get_address_of_leftDown_4() { return &___leftDown_4; }
	inline void set_leftDown_4(bool value)
	{
		___leftDown_4 = value;
	}

	inline static int32_t get_offset_of_gameOver_5() { return static_cast<int32_t>(offsetof(GameScene_t2993529754_StaticFields, ___gameOver_5)); }
	inline bool get_gameOver_5() const { return ___gameOver_5; }
	inline bool* get_address_of_gameOver_5() { return &___gameOver_5; }
	inline void set_gameOver_5(bool value)
	{
		___gameOver_5 = value;
	}

	inline static int32_t get_offset_of_gameStart_6() { return static_cast<int32_t>(offsetof(GameScene_t2993529754_StaticFields, ___gameStart_6)); }
	inline bool get_gameStart_6() const { return ___gameStart_6; }
	inline bool* get_address_of_gameStart_6() { return &___gameStart_6; }
	inline void set_gameStart_6(bool value)
	{
		___gameStart_6 = value;
	}

	inline static int32_t get_offset_of_speedDown_7() { return static_cast<int32_t>(offsetof(GameScene_t2993529754_StaticFields, ___speedDown_7)); }
	inline float get_speedDown_7() const { return ___speedDown_7; }
	inline float* get_address_of_speedDown_7() { return &___speedDown_7; }
	inline void set_speedDown_7(float value)
	{
		___speedDown_7 = value;
	}

	inline static int32_t get_offset_of_speedWall_8() { return static_cast<int32_t>(offsetof(GameScene_t2993529754_StaticFields, ___speedWall_8)); }
	inline float get_speedWall_8() const { return ___speedWall_8; }
	inline float* get_address_of_speedWall_8() { return &___speedWall_8; }
	inline void set_speedWall_8(float value)
	{
		___speedWall_8 = value;
	}

	inline static int32_t get_offset_of_btnPlay_9() { return static_cast<int32_t>(offsetof(GameScene_t2993529754_StaticFields, ___btnPlay_9)); }
	inline GameObject_t3674682005 * get_btnPlay_9() const { return ___btnPlay_9; }
	inline GameObject_t3674682005 ** get_address_of_btnPlay_9() { return &___btnPlay_9; }
	inline void set_btnPlay_9(GameObject_t3674682005 * value)
	{
		___btnPlay_9 = value;
		Il2CppCodeGenWriteBarrier(&___btnPlay_9, value);
	}

	inline static int32_t get_offset_of_makeNewWall_17() { return static_cast<int32_t>(offsetof(GameScene_t2993529754_StaticFields, ___makeNewWall_17)); }
	inline bool get_makeNewWall_17() const { return ___makeNewWall_17; }
	inline bool* get_address_of_makeNewWall_17() { return &___makeNewWall_17; }
	inline void set_makeNewWall_17(bool value)
	{
		___makeNewWall_17 = value;
	}

	inline static int32_t get_offset_of_initGame_18() { return static_cast<int32_t>(offsetof(GameScene_t2993529754_StaticFields, ___initGame_18)); }
	inline bool get_initGame_18() const { return ___initGame_18; }
	inline bool* get_address_of_initGame_18() { return &___initGame_18; }
	inline void set_initGame_18(bool value)
	{
		___initGame_18 = value;
	}

	inline static int32_t get_offset_of_difficultyNum_21() { return static_cast<int32_t>(offsetof(GameScene_t2993529754_StaticFields, ___difficultyNum_21)); }
	inline int32_t get_difficultyNum_21() const { return ___difficultyNum_21; }
	inline int32_t* get_address_of_difficultyNum_21() { return &___difficultyNum_21; }
	inline void set_difficultyNum_21(int32_t value)
	{
		___difficultyNum_21 = value;
	}

	inline static int32_t get_offset_of_heliSource_24() { return static_cast<int32_t>(offsetof(GameScene_t2993529754_StaticFields, ___heliSource_24)); }
	inline AudioSource_t1740077639 * get_heliSource_24() const { return ___heliSource_24; }
	inline AudioSource_t1740077639 ** get_address_of_heliSource_24() { return &___heliSource_24; }
	inline void set_heliSource_24(AudioSource_t1740077639 * value)
	{
		___heliSource_24 = value;
		Il2CppCodeGenWriteBarrier(&___heliSource_24, value);
	}

	inline static int32_t get_offset_of_explodeSource_26() { return static_cast<int32_t>(offsetof(GameScene_t2993529754_StaticFields, ___explodeSource_26)); }
	inline AudioSource_t1740077639 * get_explodeSource_26() const { return ___explodeSource_26; }
	inline AudioSource_t1740077639 ** get_address_of_explodeSource_26() { return &___explodeSource_26; }
	inline void set_explodeSource_26(AudioSource_t1740077639 * value)
	{
		___explodeSource_26 = value;
		Il2CppCodeGenWriteBarrier(&___explodeSource_26, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
