﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.RSS.Expanded.Decoders.DecodedChar
struct DecodedChar_t1702476758;

#include "codegen/il2cpp-codegen.h"

// System.Void ZXing.OneD.RSS.Expanded.Decoders.DecodedChar::.ctor(System.Int32,System.Char)
extern "C"  void DecodedChar__ctor_m406768995 (DecodedChar_t1702476758 * __this, int32_t ___newPosition0, Il2CppChar ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char ZXing.OneD.RSS.Expanded.Decoders.DecodedChar::getValue()
extern "C"  Il2CppChar DecodedChar_getValue_m393596738 (DecodedChar_t1702476758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.RSS.Expanded.Decoders.DecodedChar::isFNC1()
extern "C"  bool DecodedChar_isFNC1_m868418557 (DecodedChar_t1702476758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.Decoders.DecodedChar::.cctor()
extern "C"  void DecodedChar__cctor_m2626834194 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
