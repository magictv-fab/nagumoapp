﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.PlayAnimation
struct PlayAnimation_t3759522942;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.PlayAnimation::.ctor()
extern "C"  void PlayAnimation__ctor_m812366456 (PlayAnimation_t3759522942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayAnimation::Reset()
extern "C"  void PlayAnimation_Reset_m2753766693 (PlayAnimation_t3759522942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayAnimation::OnEnter()
extern "C"  void PlayAnimation_OnEnter_m2686812239 (PlayAnimation_t3759522942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayAnimation::DoPlayAnimation()
extern "C"  void PlayAnimation_DoPlayAnimation_m2086104059 (PlayAnimation_t3759522942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayAnimation::OnUpdate()
extern "C"  void PlayAnimation_OnUpdate_m820360148 (PlayAnimation_t3759522942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayAnimation::OnExit()
extern "C"  void PlayAnimation_OnExit_m926874985 (PlayAnimation_t3759522942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayAnimation::StopAnimation()
extern "C"  void PlayAnimation_StopAnimation_m2569924536 (PlayAnimation_t3759522942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
