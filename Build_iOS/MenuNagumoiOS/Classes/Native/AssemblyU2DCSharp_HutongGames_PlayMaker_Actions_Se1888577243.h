﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// HutongGames.PlayMaker.DelayedEvent
struct DelayedEvent_t1938906778;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SendEventToFsm
struct  SendEventToFsm_t1888577243  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SendEventToFsm::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SendEventToFsm::fsmName
	FsmString_t952858651 * ___fsmName_10;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SendEventToFsm::sendEvent
	FsmString_t952858651 * ___sendEvent_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SendEventToFsm::delay
	FsmFloat_t2134102846 * ___delay_12;
	// System.Boolean HutongGames.PlayMaker.Actions.SendEventToFsm::requireReceiver
	bool ___requireReceiver_13;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.SendEventToFsm::go
	GameObject_t3674682005 * ___go_14;
	// HutongGames.PlayMaker.DelayedEvent HutongGames.PlayMaker.Actions.SendEventToFsm::delayedEvent
	DelayedEvent_t1938906778 * ___delayedEvent_15;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(SendEventToFsm_t1888577243, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_fsmName_10() { return static_cast<int32_t>(offsetof(SendEventToFsm_t1888577243, ___fsmName_10)); }
	inline FsmString_t952858651 * get_fsmName_10() const { return ___fsmName_10; }
	inline FsmString_t952858651 ** get_address_of_fsmName_10() { return &___fsmName_10; }
	inline void set_fsmName_10(FsmString_t952858651 * value)
	{
		___fsmName_10 = value;
		Il2CppCodeGenWriteBarrier(&___fsmName_10, value);
	}

	inline static int32_t get_offset_of_sendEvent_11() { return static_cast<int32_t>(offsetof(SendEventToFsm_t1888577243, ___sendEvent_11)); }
	inline FsmString_t952858651 * get_sendEvent_11() const { return ___sendEvent_11; }
	inline FsmString_t952858651 ** get_address_of_sendEvent_11() { return &___sendEvent_11; }
	inline void set_sendEvent_11(FsmString_t952858651 * value)
	{
		___sendEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___sendEvent_11, value);
	}

	inline static int32_t get_offset_of_delay_12() { return static_cast<int32_t>(offsetof(SendEventToFsm_t1888577243, ___delay_12)); }
	inline FsmFloat_t2134102846 * get_delay_12() const { return ___delay_12; }
	inline FsmFloat_t2134102846 ** get_address_of_delay_12() { return &___delay_12; }
	inline void set_delay_12(FsmFloat_t2134102846 * value)
	{
		___delay_12 = value;
		Il2CppCodeGenWriteBarrier(&___delay_12, value);
	}

	inline static int32_t get_offset_of_requireReceiver_13() { return static_cast<int32_t>(offsetof(SendEventToFsm_t1888577243, ___requireReceiver_13)); }
	inline bool get_requireReceiver_13() const { return ___requireReceiver_13; }
	inline bool* get_address_of_requireReceiver_13() { return &___requireReceiver_13; }
	inline void set_requireReceiver_13(bool value)
	{
		___requireReceiver_13 = value;
	}

	inline static int32_t get_offset_of_go_14() { return static_cast<int32_t>(offsetof(SendEventToFsm_t1888577243, ___go_14)); }
	inline GameObject_t3674682005 * get_go_14() const { return ___go_14; }
	inline GameObject_t3674682005 ** get_address_of_go_14() { return &___go_14; }
	inline void set_go_14(GameObject_t3674682005 * value)
	{
		___go_14 = value;
		Il2CppCodeGenWriteBarrier(&___go_14, value);
	}

	inline static int32_t get_offset_of_delayedEvent_15() { return static_cast<int32_t>(offsetof(SendEventToFsm_t1888577243, ___delayedEvent_15)); }
	inline DelayedEvent_t1938906778 * get_delayedEvent_15() const { return ___delayedEvent_15; }
	inline DelayedEvent_t1938906778 ** get_address_of_delayedEvent_15() { return &___delayedEvent_15; }
	inline void set_delayedEvent_15(DelayedEvent_t1938906778 * value)
	{
		___delayedEvent_15 = value;
		Il2CppCodeGenWriteBarrier(&___delayedEvent_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
