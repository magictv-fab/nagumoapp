﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.update.comm.ServerCommunication/<WaitForRequestMetas>c__Iterator41
struct U3CWaitForRequestMetasU3Ec__Iterator41_t518132223;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.update.comm.ServerCommunication/<WaitForRequestMetas>c__Iterator41::.ctor()
extern "C"  void U3CWaitForRequestMetasU3Ec__Iterator41__ctor_m172463484 (U3CWaitForRequestMetasU3Ec__Iterator41_t518132223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MagicTV.update.comm.ServerCommunication/<WaitForRequestMetas>c__Iterator41::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForRequestMetasU3Ec__Iterator41_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m317372320 (U3CWaitForRequestMetasU3Ec__Iterator41_t518132223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MagicTV.update.comm.ServerCommunication/<WaitForRequestMetas>c__Iterator41::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForRequestMetasU3Ec__Iterator41_System_Collections_IEnumerator_get_Current_m3670014772 (U3CWaitForRequestMetasU3Ec__Iterator41_t518132223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicTV.update.comm.ServerCommunication/<WaitForRequestMetas>c__Iterator41::MoveNext()
extern "C"  bool U3CWaitForRequestMetasU3Ec__Iterator41_MoveNext_m3476090336 (U3CWaitForRequestMetasU3Ec__Iterator41_t518132223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.comm.ServerCommunication/<WaitForRequestMetas>c__Iterator41::Dispose()
extern "C"  void U3CWaitForRequestMetasU3Ec__Iterator41_Dispose_m2426933945 (U3CWaitForRequestMetasU3Ec__Iterator41_t518132223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.comm.ServerCommunication/<WaitForRequestMetas>c__Iterator41::Reset()
extern "C"  void U3CWaitForRequestMetasU3Ec__Iterator41_Reset_m2113863721 (U3CWaitForRequestMetasU3Ec__Iterator41_t518132223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
