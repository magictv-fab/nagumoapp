﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidPermissionCallback
struct AndroidPermissionCallback_t1784561635;
// System.Action`1<System.String>
struct Action_1_t403047693;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void AndroidPermissionCallback::.ctor(System.Action`1<System.String>,System.Action`1<System.String>)
extern "C"  void AndroidPermissionCallback__ctor_m2373014174 (AndroidPermissionCallback_t1784561635 * __this, Action_1_t403047693 * ___onGrantedCallback0, Action_1_t403047693 * ___onDeniedCallback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidPermissionCallback::add_OnPermissionGrantedAction(System.Action`1<System.String>)
extern "C"  void AndroidPermissionCallback_add_OnPermissionGrantedAction_m2665905255 (AndroidPermissionCallback_t1784561635 * __this, Action_1_t403047693 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidPermissionCallback::remove_OnPermissionGrantedAction(System.Action`1<System.String>)
extern "C"  void AndroidPermissionCallback_remove_OnPermissionGrantedAction_m567817632 (AndroidPermissionCallback_t1784561635 * __this, Action_1_t403047693 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidPermissionCallback::add_OnPermissionDeniedAction(System.Action`1<System.String>)
extern "C"  void AndroidPermissionCallback_add_OnPermissionDeniedAction_m3368962229 (AndroidPermissionCallback_t1784561635 * __this, Action_1_t403047693 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidPermissionCallback::remove_OnPermissionDeniedAction(System.Action`1<System.String>)
extern "C"  void AndroidPermissionCallback_remove_OnPermissionDeniedAction_m4271113308 (AndroidPermissionCallback_t1784561635 * __this, Action_1_t403047693 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidPermissionCallback::OnPermissionGranted(System.String)
extern "C"  void AndroidPermissionCallback_OnPermissionGranted_m1069924879 (AndroidPermissionCallback_t1784561635 * __this, String_t* ___permissionName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidPermissionCallback::OnPermissionDenied(System.String)
extern "C"  void AndroidPermissionCallback_OnPermissionDenied_m3377473277 (AndroidPermissionCallback_t1784561635 * __this, String_t* ___permissionName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
