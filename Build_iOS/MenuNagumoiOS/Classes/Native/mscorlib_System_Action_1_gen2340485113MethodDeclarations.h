﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Action`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Action_1_t2340485113;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void System.Action`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m372701337_gshared (Action_1_t2340485113 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Action_1__ctor_m372701337(__this, ___object0, ___method1, method) ((  void (*) (Action_1_t2340485113 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m372701337_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Invoke(T)
extern "C"  void Action_1_Invoke_m2500958603_gshared (Action_1_t2340485113 * __this, KeyValuePair_2_t1944668977  ___obj0, const MethodInfo* method);
#define Action_1_Invoke_m2500958603(__this, ___obj0, method) ((  void (*) (Action_1_t2340485113 *, KeyValuePair_2_t1944668977 , const MethodInfo*))Action_1_Invoke_m2500958603_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Action`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m192152288_gshared (Action_1_t2340485113 * __this, KeyValuePair_2_t1944668977  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define Action_1_BeginInvoke_m192152288(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Action_1_t2340485113 *, KeyValuePair_2_t1944668977 , AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Action_1_BeginInvoke_m192152288_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Void System.Action`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m629710633_gshared (Action_1_t2340485113 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Action_1_EndInvoke_m629710633(__this, ___result0, method) ((  void (*) (Action_1_t2340485113 *, Il2CppObject *, const MethodInfo*))Action_1_EndInvoke_m629710633_gshared)(__this, ___result0, method)
