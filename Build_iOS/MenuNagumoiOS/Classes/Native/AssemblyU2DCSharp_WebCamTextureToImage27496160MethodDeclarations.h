﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebCamTextureToImage
struct WebCamTextureToImage_t27496160;

#include "codegen/il2cpp-codegen.h"

// System.Void WebCamTextureToImage::.ctor()
extern "C"  void WebCamTextureToImage__ctor_m3118958523 (WebCamTextureToImage_t27496160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebCamTextureToImage::Start()
extern "C"  void WebCamTextureToImage_Start_m2066096315 (WebCamTextureToImage_t27496160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebCamTextureToImage::OnDestroy()
extern "C"  void WebCamTextureToImage_OnDestroy_m1994983220 (WebCamTextureToImage_t27496160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebCamTextureToImage::Update()
extern "C"  void WebCamTextureToImage_Update_m3925295794 (WebCamTextureToImage_t27496160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
