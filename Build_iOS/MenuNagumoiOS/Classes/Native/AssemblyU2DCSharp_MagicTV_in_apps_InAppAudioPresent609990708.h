﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// ARM.utils.request.ARMSingleRequest
struct ARMSingleRequest_t410726329;
// UnityEngine.AudioSource
struct AudioSource_t1740077639;
// MagicTV.vo.UrlInfoVO
struct UrlInfoVO_t1761987528;

#include "AssemblyU2DCSharp_InApp_InAppActorAbstract1901198337.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.in_apps.InAppAudioPresentation
struct  InAppAudioPresentation_t609990708  : public InAppActorAbstract_t1901198337
{
public:
	// ARM.utils.request.ARMSingleRequest MagicTV.in_apps.InAppAudioPresentation::_audioRequest
	ARMSingleRequest_t410726329 * ____audioRequest_22;
	// UnityEngine.AudioSource MagicTV.in_apps.InAppAudioPresentation::_audioSource
	AudioSource_t1740077639 * ____audioSource_23;
	// System.Boolean MagicTV.in_apps.InAppAudioPresentation::_audioComponentIsReady
	bool ____audioComponentIsReady_24;
	// MagicTV.vo.UrlInfoVO MagicTV.in_apps.InAppAudioPresentation::_audioUrlInfoVO
	UrlInfoVO_t1761987528 * ____audioUrlInfoVO_25;
	// System.Boolean MagicTV.in_apps.InAppAudioPresentation::_doPrepareAlreadyCalled
	bool ____doPrepareAlreadyCalled_26;
	// System.String MagicTV.in_apps.InAppAudioPresentation::debugAudioUrl
	String_t* ___debugAudioUrl_27;
	// System.Single MagicTV.in_apps.InAppAudioPresentation::DebugTimeAudio
	float ___DebugTimeAudio_28;
	// System.Single MagicTV.in_apps.InAppAudioPresentation::DebugCurrentTimeAudio
	float ___DebugCurrentTimeAudio_29;

public:
	inline static int32_t get_offset_of__audioRequest_22() { return static_cast<int32_t>(offsetof(InAppAudioPresentation_t609990708, ____audioRequest_22)); }
	inline ARMSingleRequest_t410726329 * get__audioRequest_22() const { return ____audioRequest_22; }
	inline ARMSingleRequest_t410726329 ** get_address_of__audioRequest_22() { return &____audioRequest_22; }
	inline void set__audioRequest_22(ARMSingleRequest_t410726329 * value)
	{
		____audioRequest_22 = value;
		Il2CppCodeGenWriteBarrier(&____audioRequest_22, value);
	}

	inline static int32_t get_offset_of__audioSource_23() { return static_cast<int32_t>(offsetof(InAppAudioPresentation_t609990708, ____audioSource_23)); }
	inline AudioSource_t1740077639 * get__audioSource_23() const { return ____audioSource_23; }
	inline AudioSource_t1740077639 ** get_address_of__audioSource_23() { return &____audioSource_23; }
	inline void set__audioSource_23(AudioSource_t1740077639 * value)
	{
		____audioSource_23 = value;
		Il2CppCodeGenWriteBarrier(&____audioSource_23, value);
	}

	inline static int32_t get_offset_of__audioComponentIsReady_24() { return static_cast<int32_t>(offsetof(InAppAudioPresentation_t609990708, ____audioComponentIsReady_24)); }
	inline bool get__audioComponentIsReady_24() const { return ____audioComponentIsReady_24; }
	inline bool* get_address_of__audioComponentIsReady_24() { return &____audioComponentIsReady_24; }
	inline void set__audioComponentIsReady_24(bool value)
	{
		____audioComponentIsReady_24 = value;
	}

	inline static int32_t get_offset_of__audioUrlInfoVO_25() { return static_cast<int32_t>(offsetof(InAppAudioPresentation_t609990708, ____audioUrlInfoVO_25)); }
	inline UrlInfoVO_t1761987528 * get__audioUrlInfoVO_25() const { return ____audioUrlInfoVO_25; }
	inline UrlInfoVO_t1761987528 ** get_address_of__audioUrlInfoVO_25() { return &____audioUrlInfoVO_25; }
	inline void set__audioUrlInfoVO_25(UrlInfoVO_t1761987528 * value)
	{
		____audioUrlInfoVO_25 = value;
		Il2CppCodeGenWriteBarrier(&____audioUrlInfoVO_25, value);
	}

	inline static int32_t get_offset_of__doPrepareAlreadyCalled_26() { return static_cast<int32_t>(offsetof(InAppAudioPresentation_t609990708, ____doPrepareAlreadyCalled_26)); }
	inline bool get__doPrepareAlreadyCalled_26() const { return ____doPrepareAlreadyCalled_26; }
	inline bool* get_address_of__doPrepareAlreadyCalled_26() { return &____doPrepareAlreadyCalled_26; }
	inline void set__doPrepareAlreadyCalled_26(bool value)
	{
		____doPrepareAlreadyCalled_26 = value;
	}

	inline static int32_t get_offset_of_debugAudioUrl_27() { return static_cast<int32_t>(offsetof(InAppAudioPresentation_t609990708, ___debugAudioUrl_27)); }
	inline String_t* get_debugAudioUrl_27() const { return ___debugAudioUrl_27; }
	inline String_t** get_address_of_debugAudioUrl_27() { return &___debugAudioUrl_27; }
	inline void set_debugAudioUrl_27(String_t* value)
	{
		___debugAudioUrl_27 = value;
		Il2CppCodeGenWriteBarrier(&___debugAudioUrl_27, value);
	}

	inline static int32_t get_offset_of_DebugTimeAudio_28() { return static_cast<int32_t>(offsetof(InAppAudioPresentation_t609990708, ___DebugTimeAudio_28)); }
	inline float get_DebugTimeAudio_28() const { return ___DebugTimeAudio_28; }
	inline float* get_address_of_DebugTimeAudio_28() { return &___DebugTimeAudio_28; }
	inline void set_DebugTimeAudio_28(float value)
	{
		___DebugTimeAudio_28 = value;
	}

	inline static int32_t get_offset_of_DebugCurrentTimeAudio_29() { return static_cast<int32_t>(offsetof(InAppAudioPresentation_t609990708, ___DebugCurrentTimeAudio_29)); }
	inline float get_DebugCurrentTimeAudio_29() const { return ___DebugCurrentTimeAudio_29; }
	inline float* get_address_of_DebugCurrentTimeAudio_29() { return &___DebugCurrentTimeAudio_29; }
	inline void set_DebugCurrentTimeAudio_29(float value)
	{
		___DebugCurrentTimeAudio_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
