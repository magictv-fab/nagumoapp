﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MathScreen
struct MathScreen_t3403688372;
// ARM.interfaces.FilterVector2Interface
struct FilterVector2Interface_t2638068040;
// ARM.interfaces.FilterFloatInterface
struct FilterFloatInterface_t739564379;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void MathScreen::.ctor()
extern "C"  void MathScreen__ctor_m2477594983 (MathScreen_t3403688372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 MathScreen::cm2px(UnityEngine.Vector2,ARM.interfaces.FilterVector2Interface)
extern "C"  Vector2_t4282066565  MathScreen_cm2px_m2979604520 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___cm0, Il2CppObject * ___filter1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 MathScreen::cm2px(UnityEngine.Vector2)
extern "C"  Vector2_t4282066565  MathScreen_cm2px_m2191731456 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___cm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MathScreen::cm2px(System.Single,ARM.interfaces.FilterFloatInterface)
extern "C"  float MathScreen_cm2px_m2511877655 (Il2CppObject * __this /* static, unused */, float ___cm0, Il2CppObject * ___filter1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MathScreen::cm2px(System.Single)
extern "C"  float MathScreen_cm2px_m2796915938 (Il2CppObject * __this /* static, unused */, float ___cm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 MathScreen::inches2px(UnityEngine.Vector2,ARM.interfaces.FilterVector2Interface)
extern "C"  Vector2_t4282066565  MathScreen_inches2px_m1375709750 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___inches0, Il2CppObject * ___filter1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 MathScreen::inches2px(UnityEngine.Vector2)
extern "C"  Vector2_t4282066565  MathScreen_inches2px_m1025869710 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___inches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MathScreen::inches2px(System.Single,ARM.interfaces.FilterFloatInterface)
extern "C"  float MathScreen_inches2px_m1810220453 (Il2CppObject * __this /* static, unused */, float ___inches0, Il2CppObject * ___filter1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MathScreen::inches2px(System.Single)
extern "C"  float MathScreen_inches2px_m454216560 (Il2CppObject * __this /* static, unused */, float ___inches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MathScreen::inches2cm(System.Single)
extern "C"  float MathScreen_inches2cm_m1361968334 (Il2CppObject * __this /* static, unused */, float ___inches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MathScreen::cm2inches(System.Single)
extern "C"  float MathScreen_cm2inches_m394417778 (Il2CppObject * __this /* static, unused */, float ___cm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
