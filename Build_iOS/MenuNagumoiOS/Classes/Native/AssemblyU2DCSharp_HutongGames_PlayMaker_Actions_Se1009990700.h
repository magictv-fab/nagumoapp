﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t821476169;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Com715545838.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAudioClip
struct  SetAudioClip_t1009990700  : public ComponentAction_1_t715545838
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAudioClip::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.SetAudioClip::audioClip
	FsmObject_t821476169 * ___audioClip_12;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(SetAudioClip_t1009990700, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_audioClip_12() { return static_cast<int32_t>(offsetof(SetAudioClip_t1009990700, ___audioClip_12)); }
	inline FsmObject_t821476169 * get_audioClip_12() const { return ___audioClip_12; }
	inline FsmObject_t821476169 ** get_address_of_audioClip_12() { return &___audioClip_12; }
	inline void set_audioClip_12(FsmObject_t821476169 * value)
	{
		___audioClip_12 = value;
		Il2CppCodeGenWriteBarrier(&___audioClip_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
