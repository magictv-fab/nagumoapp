﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SelectRandomVector3
struct SelectRandomVector3_t3679113471;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SelectRandomVector3::.ctor()
extern "C"  void SelectRandomVector3__ctor_m1384538391 (SelectRandomVector3_t3679113471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SelectRandomVector3::Reset()
extern "C"  void SelectRandomVector3_Reset_m3325938628 (SelectRandomVector3_t3679113471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SelectRandomVector3::OnEnter()
extern "C"  void SelectRandomVector3_OnEnter_m2788227886 (SelectRandomVector3_t3679113471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SelectRandomVector3::DoSelectRandomColor()
extern "C"  void SelectRandomVector3_DoSelectRandomColor_m3450303854 (SelectRandomVector3_t3679113471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
