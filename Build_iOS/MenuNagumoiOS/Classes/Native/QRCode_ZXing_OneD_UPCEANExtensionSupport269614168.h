﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;
// ZXing.OneD.UPCEANExtension2Support
struct UPCEANExtension2Support_t2671369572;
// ZXing.OneD.UPCEANExtension5Support
struct UPCEANExtension5Support_t3604833281;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.UPCEANExtensionSupport
struct  UPCEANExtensionSupport_t269614168  : public Il2CppObject
{
public:
	// ZXing.OneD.UPCEANExtension2Support ZXing.OneD.UPCEANExtensionSupport::twoSupport
	UPCEANExtension2Support_t2671369572 * ___twoSupport_1;
	// ZXing.OneD.UPCEANExtension5Support ZXing.OneD.UPCEANExtensionSupport::fiveSupport
	UPCEANExtension5Support_t3604833281 * ___fiveSupport_2;

public:
	inline static int32_t get_offset_of_twoSupport_1() { return static_cast<int32_t>(offsetof(UPCEANExtensionSupport_t269614168, ___twoSupport_1)); }
	inline UPCEANExtension2Support_t2671369572 * get_twoSupport_1() const { return ___twoSupport_1; }
	inline UPCEANExtension2Support_t2671369572 ** get_address_of_twoSupport_1() { return &___twoSupport_1; }
	inline void set_twoSupport_1(UPCEANExtension2Support_t2671369572 * value)
	{
		___twoSupport_1 = value;
		Il2CppCodeGenWriteBarrier(&___twoSupport_1, value);
	}

	inline static int32_t get_offset_of_fiveSupport_2() { return static_cast<int32_t>(offsetof(UPCEANExtensionSupport_t269614168, ___fiveSupport_2)); }
	inline UPCEANExtension5Support_t3604833281 * get_fiveSupport_2() const { return ___fiveSupport_2; }
	inline UPCEANExtension5Support_t3604833281 ** get_address_of_fiveSupport_2() { return &___fiveSupport_2; }
	inline void set_fiveSupport_2(UPCEANExtension5Support_t3604833281 * value)
	{
		___fiveSupport_2 = value;
		Il2CppCodeGenWriteBarrier(&___fiveSupport_2, value);
	}
};

struct UPCEANExtensionSupport_t269614168_StaticFields
{
public:
	// System.Int32[] ZXing.OneD.UPCEANExtensionSupport::EXTENSION_START_PATTERN
	Int32U5BU5D_t3230847821* ___EXTENSION_START_PATTERN_0;

public:
	inline static int32_t get_offset_of_EXTENSION_START_PATTERN_0() { return static_cast<int32_t>(offsetof(UPCEANExtensionSupport_t269614168_StaticFields, ___EXTENSION_START_PATTERN_0)); }
	inline Int32U5BU5D_t3230847821* get_EXTENSION_START_PATTERN_0() const { return ___EXTENSION_START_PATTERN_0; }
	inline Int32U5BU5D_t3230847821** get_address_of_EXTENSION_START_PATTERN_0() { return &___EXTENSION_START_PATTERN_0; }
	inline void set_EXTENSION_START_PATTERN_0(Int32U5BU5D_t3230847821* value)
	{
		___EXTENSION_START_PATTERN_0 = value;
		Il2CppCodeGenWriteBarrier(&___EXTENSION_START_PATTERN_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
