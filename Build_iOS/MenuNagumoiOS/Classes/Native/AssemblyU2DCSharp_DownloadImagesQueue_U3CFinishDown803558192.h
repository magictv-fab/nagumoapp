﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t3134621005;
// System.Object
struct Il2CppObject;
// DownloadImagesQueue
struct DownloadImagesQueue_t2595091377;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DownloadImagesQueue/<FinishDownload>c__Iterator37
struct  U3CFinishDownloadU3Ec__Iterator37_t803558192  : public Il2CppObject
{
public:
	// System.String DownloadImagesQueue/<FinishDownload>c__Iterator37::url
	String_t* ___url_0;
	// UnityEngine.WWW DownloadImagesQueue/<FinishDownload>c__Iterator37::<hs_get>__0
	WWW_t3134621005 * ___U3Chs_getU3E__0_1;
	// System.Int32 DownloadImagesQueue/<FinishDownload>c__Iterator37::$PC
	int32_t ___U24PC_2;
	// System.Object DownloadImagesQueue/<FinishDownload>c__Iterator37::$current
	Il2CppObject * ___U24current_3;
	// System.String DownloadImagesQueue/<FinishDownload>c__Iterator37::<$>url
	String_t* ___U3CU24U3Eurl_4;
	// DownloadImagesQueue DownloadImagesQueue/<FinishDownload>c__Iterator37::<>f__this
	DownloadImagesQueue_t2595091377 * ___U3CU3Ef__this_5;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CFinishDownloadU3Ec__Iterator37_t803558192, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier(&___url_0, value);
	}

	inline static int32_t get_offset_of_U3Chs_getU3E__0_1() { return static_cast<int32_t>(offsetof(U3CFinishDownloadU3Ec__Iterator37_t803558192, ___U3Chs_getU3E__0_1)); }
	inline WWW_t3134621005 * get_U3Chs_getU3E__0_1() const { return ___U3Chs_getU3E__0_1; }
	inline WWW_t3134621005 ** get_address_of_U3Chs_getU3E__0_1() { return &___U3Chs_getU3E__0_1; }
	inline void set_U3Chs_getU3E__0_1(WWW_t3134621005 * value)
	{
		___U3Chs_getU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3Chs_getU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CFinishDownloadU3Ec__Iterator37_t803558192, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CFinishDownloadU3Ec__Iterator37_t803558192, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eurl_4() { return static_cast<int32_t>(offsetof(U3CFinishDownloadU3Ec__Iterator37_t803558192, ___U3CU24U3Eurl_4)); }
	inline String_t* get_U3CU24U3Eurl_4() const { return ___U3CU24U3Eurl_4; }
	inline String_t** get_address_of_U3CU24U3Eurl_4() { return &___U3CU24U3Eurl_4; }
	inline void set_U3CU24U3Eurl_4(String_t* value)
	{
		___U3CU24U3Eurl_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eurl_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_5() { return static_cast<int32_t>(offsetof(U3CFinishDownloadU3Ec__Iterator37_t803558192, ___U3CU3Ef__this_5)); }
	inline DownloadImagesQueue_t2595091377 * get_U3CU3Ef__this_5() const { return ___U3CU3Ef__this_5; }
	inline DownloadImagesQueue_t2595091377 ** get_address_of_U3CU3Ef__this_5() { return &___U3CU3Ef__this_5; }
	inline void set_U3CU3Ef__this_5(DownloadImagesQueue_t2595091377 * value)
	{
		___U3CU3Ef__this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
