﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GenericWindowControllerScript
struct  GenericWindowControllerScript_t248075822  : public MonoBehaviour_t667441552
{
public:
	// System.Action GenericWindowControllerScript::onShow
	Action_t3771233898 * ___onShow_2;
	// System.Action GenericWindowControllerScript::onHide
	Action_t3771233898 * ___onHide_3;

public:
	inline static int32_t get_offset_of_onShow_2() { return static_cast<int32_t>(offsetof(GenericWindowControllerScript_t248075822, ___onShow_2)); }
	inline Action_t3771233898 * get_onShow_2() const { return ___onShow_2; }
	inline Action_t3771233898 ** get_address_of_onShow_2() { return &___onShow_2; }
	inline void set_onShow_2(Action_t3771233898 * value)
	{
		___onShow_2 = value;
		Il2CppCodeGenWriteBarrier(&___onShow_2, value);
	}

	inline static int32_t get_offset_of_onHide_3() { return static_cast<int32_t>(offsetof(GenericWindowControllerScript_t248075822, ___onHide_3)); }
	inline Action_t3771233898 * get_onHide_3() const { return ___onHide_3; }
	inline Action_t3771233898 ** get_address_of_onHide_3() { return &___onHide_3; }
	inline void set_onHide_3(Action_t3771233898 * value)
	{
		___onHide_3 = value;
		Il2CppCodeGenWriteBarrier(&___onHide_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
