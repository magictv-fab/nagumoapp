﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Loom/<QueueOnMainThread>c__AnonStoreyA5
struct U3CQueueOnMainThreadU3Ec__AnonStoreyA5_t275976994;
// Loom/DelayedQueueItem
struct DelayedQueueItem_t3219765808;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Loom_DelayedQueueItem3219765808.h"

// System.Void Loom/<QueueOnMainThread>c__AnonStoreyA5::.ctor()
extern "C"  void U3CQueueOnMainThreadU3Ec__AnonStoreyA5__ctor_m3483820681 (U3CQueueOnMainThreadU3Ec__AnonStoreyA5_t275976994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Loom/<QueueOnMainThread>c__AnonStoreyA5::<>m__7E(Loom/DelayedQueueItem)
extern "C"  bool U3CQueueOnMainThreadU3Ec__AnonStoreyA5_U3CU3Em__7E_m1299670222 (U3CQueueOnMainThreadU3Ec__AnonStoreyA5_t275976994 * __this, DelayedQueueItem_t3219765808 * ___d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
