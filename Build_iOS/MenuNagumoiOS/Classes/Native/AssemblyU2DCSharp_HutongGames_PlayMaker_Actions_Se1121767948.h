﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// PlayMakerFSM
struct PlayMakerFSM_t3799847376;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetFsmFloat
struct  SetFsmFloat_t1121767948  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetFsmFloat::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmFloat::fsmName
	FsmString_t952858651 * ___fsmName_10;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmFloat::variableName
	FsmString_t952858651 * ___variableName_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetFsmFloat::setValue
	FsmFloat_t2134102846 * ___setValue_12;
	// System.Boolean HutongGames.PlayMaker.Actions.SetFsmFloat::everyFrame
	bool ___everyFrame_13;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.SetFsmFloat::goLastFrame
	GameObject_t3674682005 * ___goLastFrame_14;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.SetFsmFloat::fsm
	PlayMakerFSM_t3799847376 * ___fsm_15;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(SetFsmFloat_t1121767948, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_fsmName_10() { return static_cast<int32_t>(offsetof(SetFsmFloat_t1121767948, ___fsmName_10)); }
	inline FsmString_t952858651 * get_fsmName_10() const { return ___fsmName_10; }
	inline FsmString_t952858651 ** get_address_of_fsmName_10() { return &___fsmName_10; }
	inline void set_fsmName_10(FsmString_t952858651 * value)
	{
		___fsmName_10 = value;
		Il2CppCodeGenWriteBarrier(&___fsmName_10, value);
	}

	inline static int32_t get_offset_of_variableName_11() { return static_cast<int32_t>(offsetof(SetFsmFloat_t1121767948, ___variableName_11)); }
	inline FsmString_t952858651 * get_variableName_11() const { return ___variableName_11; }
	inline FsmString_t952858651 ** get_address_of_variableName_11() { return &___variableName_11; }
	inline void set_variableName_11(FsmString_t952858651 * value)
	{
		___variableName_11 = value;
		Il2CppCodeGenWriteBarrier(&___variableName_11, value);
	}

	inline static int32_t get_offset_of_setValue_12() { return static_cast<int32_t>(offsetof(SetFsmFloat_t1121767948, ___setValue_12)); }
	inline FsmFloat_t2134102846 * get_setValue_12() const { return ___setValue_12; }
	inline FsmFloat_t2134102846 ** get_address_of_setValue_12() { return &___setValue_12; }
	inline void set_setValue_12(FsmFloat_t2134102846 * value)
	{
		___setValue_12 = value;
		Il2CppCodeGenWriteBarrier(&___setValue_12, value);
	}

	inline static int32_t get_offset_of_everyFrame_13() { return static_cast<int32_t>(offsetof(SetFsmFloat_t1121767948, ___everyFrame_13)); }
	inline bool get_everyFrame_13() const { return ___everyFrame_13; }
	inline bool* get_address_of_everyFrame_13() { return &___everyFrame_13; }
	inline void set_everyFrame_13(bool value)
	{
		___everyFrame_13 = value;
	}

	inline static int32_t get_offset_of_goLastFrame_14() { return static_cast<int32_t>(offsetof(SetFsmFloat_t1121767948, ___goLastFrame_14)); }
	inline GameObject_t3674682005 * get_goLastFrame_14() const { return ___goLastFrame_14; }
	inline GameObject_t3674682005 ** get_address_of_goLastFrame_14() { return &___goLastFrame_14; }
	inline void set_goLastFrame_14(GameObject_t3674682005 * value)
	{
		___goLastFrame_14 = value;
		Il2CppCodeGenWriteBarrier(&___goLastFrame_14, value);
	}

	inline static int32_t get_offset_of_fsm_15() { return static_cast<int32_t>(offsetof(SetFsmFloat_t1121767948, ___fsm_15)); }
	inline PlayMakerFSM_t3799847376 * get_fsm_15() const { return ___fsm_15; }
	inline PlayMakerFSM_t3799847376 ** get_address_of_fsm_15() { return &___fsm_15; }
	inline void set_fsm_15(PlayMakerFSM_t3799847376 * value)
	{
		___fsm_15 = value;
		Il2CppCodeGenWriteBarrier(&___fsm_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
