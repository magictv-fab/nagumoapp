﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.IntClamp
struct IntClamp_t286226100;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.IntClamp::.ctor()
extern "C"  void IntClamp__ctor_m1600343602 (IntClamp_t286226100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntClamp::Reset()
extern "C"  void IntClamp_Reset_m3541743839 (IntClamp_t286226100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntClamp::OnEnter()
extern "C"  void IntClamp_OnEnter_m4018605449 (IntClamp_t286226100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntClamp::OnUpdate()
extern "C"  void IntClamp_OnUpdate_m3451243994 (IntClamp_t286226100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntClamp::DoClamp()
extern "C"  void IntClamp_DoClamp_m2319002240 (IntClamp_t286226100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
