﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Confirmacao
struct Confirmacao_t2012661360;

#include "codegen/il2cpp-codegen.h"

// System.Void Confirmacao::.ctor()
extern "C"  void Confirmacao__ctor_m496300795 (Confirmacao_t2012661360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Confirmacao::Start()
extern "C"  void Confirmacao_Start_m3738405883 (Confirmacao_t2012661360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Confirmacao::Update()
extern "C"  void Confirmacao_Update_m4227284850 (Confirmacao_t2012661360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
