﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Core.FileSystemScanner
struct FileSystemScanner_t1416526437;
// System.String
struct String_t;
// ICSharpCode.SharpZipLib.Core.IScanFilter
struct IScanFilter_t337021136;
// System.Exception
struct Exception_t3991598821;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Exception3991598821.h"

// System.Void ICSharpCode.SharpZipLib.Core.FileSystemScanner::.ctor(System.String)
extern "C"  void FileSystemScanner__ctor_m3555192092 (FileSystemScanner_t1416526437 * __this, String_t* ___filter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.FileSystemScanner::.ctor(System.String,System.String)
extern "C"  void FileSystemScanner__ctor_m2038297688 (FileSystemScanner_t1416526437 * __this, String_t* ___fileFilter0, String_t* ___directoryFilter1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.FileSystemScanner::.ctor(ICSharpCode.SharpZipLib.Core.IScanFilter)
extern "C"  void FileSystemScanner__ctor_m1875323814 (FileSystemScanner_t1416526437 * __this, Il2CppObject * ___fileFilter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.FileSystemScanner::.ctor(ICSharpCode.SharpZipLib.Core.IScanFilter,ICSharpCode.SharpZipLib.Core.IScanFilter)
extern "C"  void FileSystemScanner__ctor_m2937037728 (FileSystemScanner_t1416526437 * __this, Il2CppObject * ___fileFilter0, Il2CppObject * ___directoryFilter1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Core.FileSystemScanner::OnDirectoryFailure(System.String,System.Exception)
extern "C"  bool FileSystemScanner_OnDirectoryFailure_m3723458594 (FileSystemScanner_t1416526437 * __this, String_t* ___directory0, Exception_t3991598821 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Core.FileSystemScanner::OnFileFailure(System.String,System.Exception)
extern "C"  bool FileSystemScanner_OnFileFailure_m3421091859 (FileSystemScanner_t1416526437 * __this, String_t* ___file0, Exception_t3991598821 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.FileSystemScanner::OnProcessFile(System.String)
extern "C"  void FileSystemScanner_OnProcessFile_m515349778 (FileSystemScanner_t1416526437 * __this, String_t* ___file0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.FileSystemScanner::OnCompleteFile(System.String)
extern "C"  void FileSystemScanner_OnCompleteFile_m2384587472 (FileSystemScanner_t1416526437 * __this, String_t* ___file0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.FileSystemScanner::OnProcessDirectory(System.String,System.Boolean)
extern "C"  void FileSystemScanner_OnProcessDirectory_m2360252758 (FileSystemScanner_t1416526437 * __this, String_t* ___directory0, bool ___hasMatchingFiles1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.FileSystemScanner::Scan(System.String,System.Boolean)
extern "C"  void FileSystemScanner_Scan_m3278181846 (FileSystemScanner_t1416526437 * __this, String_t* ___directory0, bool ___recurse1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.FileSystemScanner::ScanDir(System.String,System.Boolean)
extern "C"  void FileSystemScanner_ScanDir_m3931143951 (FileSystemScanner_t1416526437 * __this, String_t* ___directory0, bool ___recurse1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
