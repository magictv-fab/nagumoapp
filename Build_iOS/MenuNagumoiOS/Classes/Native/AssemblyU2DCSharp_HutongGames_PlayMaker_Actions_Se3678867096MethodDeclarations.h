﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetObjectValue
struct SetObjectValue_t3678867096;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetObjectValue::.ctor()
extern "C"  void SetObjectValue__ctor_m3958826190 (SetObjectValue_t3678867096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetObjectValue::Reset()
extern "C"  void SetObjectValue_Reset_m1605259131 (SetObjectValue_t3678867096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetObjectValue::OnEnter()
extern "C"  void SetObjectValue_OnEnter_m2777640229 (SetObjectValue_t3678867096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetObjectValue::OnUpdate()
extern "C"  void SetObjectValue_OnUpdate_m3636027838 (SetObjectValue_t3678867096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
