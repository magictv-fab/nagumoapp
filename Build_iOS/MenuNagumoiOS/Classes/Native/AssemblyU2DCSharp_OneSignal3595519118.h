﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// OneSignal/IdsAvailableCallback
struct IdsAvailableCallback_t3679464791;
// OneSignal/TagsReceived
struct TagsReceived_t3348492571;
// OneSignal/PromptForPushNotificationsUserResponse
struct PromptForPushNotificationsUserResponse_t2193543734;
// OneSignal/PermissionObservable
struct PermissionObservable_t1360586611;
// OneSignal/SubscriptionObservable
struct SubscriptionObservable_t2046194945;
// OneSignal/EmailSubscriptionObservable
struct EmailSubscriptionObservable_t3601566427;
// OneSignal/UnityBuilder
struct UnityBuilder_t2457889383;
// OneSignalPlatform
struct OneSignalPlatform_t4133004065;
// OneSignal/OnPostNotificationSuccess
struct OnPostNotificationSuccess_t2053930136;
// OneSignal/OnPostNotificationFailure
struct OnPostNotificationFailure_t2834525727;
// OneSignal/OnSetEmailSuccess
struct OnSetEmailSuccess_t2952831753;
// OneSignal/OnSetEmailFailure
struct OnSetEmailFailure_t3733427344;
// OneSignal/OnLogoutEmailSuccess
struct OnLogoutEmailSuccess_t3366364849;
// OneSignal/OnLogoutEmailFailure
struct OnLogoutEmailFailure_t4146960440;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_OneSignal_LOG_LEVEL343241288.h"
#include "AssemblyU2DCSharp_OneSignal_OSInFocusDisplayOption2441910921.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OneSignal
struct  OneSignal_t3595519118  : public MonoBehaviour_t667441552
{
public:

public:
};

struct OneSignal_t3595519118_StaticFields
{
public:
	// OneSignal/IdsAvailableCallback OneSignal::idsAvailableDelegate
	IdsAvailableCallback_t3679464791 * ___idsAvailableDelegate_5;
	// OneSignal/TagsReceived OneSignal::tagsReceivedDelegate
	TagsReceived_t3348492571 * ___tagsReceivedDelegate_6;
	// OneSignal/PromptForPushNotificationsUserResponse OneSignal::notificationUserResponseDelegate
	PromptForPushNotificationsUserResponse_t2193543734 * ___notificationUserResponseDelegate_7;
	// OneSignal/PermissionObservable OneSignal::internalPermissionObserver
	PermissionObservable_t1360586611 * ___internalPermissionObserver_8;
	// System.Boolean OneSignal::addedPermissionObserver
	bool ___addedPermissionObserver_9;
	// OneSignal/SubscriptionObservable OneSignal::internalSubscriptionObserver
	SubscriptionObservable_t2046194945 * ___internalSubscriptionObserver_10;
	// System.Boolean OneSignal::addedSubscriptionObserver
	bool ___addedSubscriptionObserver_11;
	// OneSignal/EmailSubscriptionObservable OneSignal::internalEmailSubscriptionObserver
	EmailSubscriptionObservable_t3601566427 * ___internalEmailSubscriptionObserver_12;
	// System.Boolean OneSignal::addedEmailSubscriptionObserver
	bool ___addedEmailSubscriptionObserver_13;
	// OneSignal/UnityBuilder OneSignal::builder
	UnityBuilder_t2457889383 * ___builder_14;
	// OneSignalPlatform OneSignal::oneSignalPlatform
	Il2CppObject * ___oneSignalPlatform_15;
	// OneSignal/LOG_LEVEL OneSignal::logLevel
	int32_t ___logLevel_16;
	// OneSignal/LOG_LEVEL OneSignal::visualLogLevel
	int32_t ___visualLogLevel_17;
	// System.Boolean OneSignal::requiresUserConsent
	bool ___requiresUserConsent_18;
	// OneSignal/OnPostNotificationSuccess OneSignal::postNotificationSuccessDelegate
	OnPostNotificationSuccess_t2053930136 * ___postNotificationSuccessDelegate_19;
	// OneSignal/OnPostNotificationFailure OneSignal::postNotificationFailureDelegate
	OnPostNotificationFailure_t2834525727 * ___postNotificationFailureDelegate_20;
	// OneSignal/OnSetEmailSuccess OneSignal::setEmailSuccessDelegate
	OnSetEmailSuccess_t2952831753 * ___setEmailSuccessDelegate_21;
	// OneSignal/OnSetEmailFailure OneSignal::setEmailFailureDelegate
	OnSetEmailFailure_t3733427344 * ___setEmailFailureDelegate_22;
	// OneSignal/OnLogoutEmailSuccess OneSignal::logoutEmailSuccessDelegate
	OnLogoutEmailSuccess_t3366364849 * ___logoutEmailSuccessDelegate_23;
	// OneSignal/OnLogoutEmailFailure OneSignal::logoutEmailFailureDelegate
	OnLogoutEmailFailure_t4146960440 * ___logoutEmailFailureDelegate_24;
	// OneSignal/OSInFocusDisplayOption OneSignal::_inFocusDisplayType
	int32_t ____inFocusDisplayType_25;

public:
	inline static int32_t get_offset_of_idsAvailableDelegate_5() { return static_cast<int32_t>(offsetof(OneSignal_t3595519118_StaticFields, ___idsAvailableDelegate_5)); }
	inline IdsAvailableCallback_t3679464791 * get_idsAvailableDelegate_5() const { return ___idsAvailableDelegate_5; }
	inline IdsAvailableCallback_t3679464791 ** get_address_of_idsAvailableDelegate_5() { return &___idsAvailableDelegate_5; }
	inline void set_idsAvailableDelegate_5(IdsAvailableCallback_t3679464791 * value)
	{
		___idsAvailableDelegate_5 = value;
		Il2CppCodeGenWriteBarrier(&___idsAvailableDelegate_5, value);
	}

	inline static int32_t get_offset_of_tagsReceivedDelegate_6() { return static_cast<int32_t>(offsetof(OneSignal_t3595519118_StaticFields, ___tagsReceivedDelegate_6)); }
	inline TagsReceived_t3348492571 * get_tagsReceivedDelegate_6() const { return ___tagsReceivedDelegate_6; }
	inline TagsReceived_t3348492571 ** get_address_of_tagsReceivedDelegate_6() { return &___tagsReceivedDelegate_6; }
	inline void set_tagsReceivedDelegate_6(TagsReceived_t3348492571 * value)
	{
		___tagsReceivedDelegate_6 = value;
		Il2CppCodeGenWriteBarrier(&___tagsReceivedDelegate_6, value);
	}

	inline static int32_t get_offset_of_notificationUserResponseDelegate_7() { return static_cast<int32_t>(offsetof(OneSignal_t3595519118_StaticFields, ___notificationUserResponseDelegate_7)); }
	inline PromptForPushNotificationsUserResponse_t2193543734 * get_notificationUserResponseDelegate_7() const { return ___notificationUserResponseDelegate_7; }
	inline PromptForPushNotificationsUserResponse_t2193543734 ** get_address_of_notificationUserResponseDelegate_7() { return &___notificationUserResponseDelegate_7; }
	inline void set_notificationUserResponseDelegate_7(PromptForPushNotificationsUserResponse_t2193543734 * value)
	{
		___notificationUserResponseDelegate_7 = value;
		Il2CppCodeGenWriteBarrier(&___notificationUserResponseDelegate_7, value);
	}

	inline static int32_t get_offset_of_internalPermissionObserver_8() { return static_cast<int32_t>(offsetof(OneSignal_t3595519118_StaticFields, ___internalPermissionObserver_8)); }
	inline PermissionObservable_t1360586611 * get_internalPermissionObserver_8() const { return ___internalPermissionObserver_8; }
	inline PermissionObservable_t1360586611 ** get_address_of_internalPermissionObserver_8() { return &___internalPermissionObserver_8; }
	inline void set_internalPermissionObserver_8(PermissionObservable_t1360586611 * value)
	{
		___internalPermissionObserver_8 = value;
		Il2CppCodeGenWriteBarrier(&___internalPermissionObserver_8, value);
	}

	inline static int32_t get_offset_of_addedPermissionObserver_9() { return static_cast<int32_t>(offsetof(OneSignal_t3595519118_StaticFields, ___addedPermissionObserver_9)); }
	inline bool get_addedPermissionObserver_9() const { return ___addedPermissionObserver_9; }
	inline bool* get_address_of_addedPermissionObserver_9() { return &___addedPermissionObserver_9; }
	inline void set_addedPermissionObserver_9(bool value)
	{
		___addedPermissionObserver_9 = value;
	}

	inline static int32_t get_offset_of_internalSubscriptionObserver_10() { return static_cast<int32_t>(offsetof(OneSignal_t3595519118_StaticFields, ___internalSubscriptionObserver_10)); }
	inline SubscriptionObservable_t2046194945 * get_internalSubscriptionObserver_10() const { return ___internalSubscriptionObserver_10; }
	inline SubscriptionObservable_t2046194945 ** get_address_of_internalSubscriptionObserver_10() { return &___internalSubscriptionObserver_10; }
	inline void set_internalSubscriptionObserver_10(SubscriptionObservable_t2046194945 * value)
	{
		___internalSubscriptionObserver_10 = value;
		Il2CppCodeGenWriteBarrier(&___internalSubscriptionObserver_10, value);
	}

	inline static int32_t get_offset_of_addedSubscriptionObserver_11() { return static_cast<int32_t>(offsetof(OneSignal_t3595519118_StaticFields, ___addedSubscriptionObserver_11)); }
	inline bool get_addedSubscriptionObserver_11() const { return ___addedSubscriptionObserver_11; }
	inline bool* get_address_of_addedSubscriptionObserver_11() { return &___addedSubscriptionObserver_11; }
	inline void set_addedSubscriptionObserver_11(bool value)
	{
		___addedSubscriptionObserver_11 = value;
	}

	inline static int32_t get_offset_of_internalEmailSubscriptionObserver_12() { return static_cast<int32_t>(offsetof(OneSignal_t3595519118_StaticFields, ___internalEmailSubscriptionObserver_12)); }
	inline EmailSubscriptionObservable_t3601566427 * get_internalEmailSubscriptionObserver_12() const { return ___internalEmailSubscriptionObserver_12; }
	inline EmailSubscriptionObservable_t3601566427 ** get_address_of_internalEmailSubscriptionObserver_12() { return &___internalEmailSubscriptionObserver_12; }
	inline void set_internalEmailSubscriptionObserver_12(EmailSubscriptionObservable_t3601566427 * value)
	{
		___internalEmailSubscriptionObserver_12 = value;
		Il2CppCodeGenWriteBarrier(&___internalEmailSubscriptionObserver_12, value);
	}

	inline static int32_t get_offset_of_addedEmailSubscriptionObserver_13() { return static_cast<int32_t>(offsetof(OneSignal_t3595519118_StaticFields, ___addedEmailSubscriptionObserver_13)); }
	inline bool get_addedEmailSubscriptionObserver_13() const { return ___addedEmailSubscriptionObserver_13; }
	inline bool* get_address_of_addedEmailSubscriptionObserver_13() { return &___addedEmailSubscriptionObserver_13; }
	inline void set_addedEmailSubscriptionObserver_13(bool value)
	{
		___addedEmailSubscriptionObserver_13 = value;
	}

	inline static int32_t get_offset_of_builder_14() { return static_cast<int32_t>(offsetof(OneSignal_t3595519118_StaticFields, ___builder_14)); }
	inline UnityBuilder_t2457889383 * get_builder_14() const { return ___builder_14; }
	inline UnityBuilder_t2457889383 ** get_address_of_builder_14() { return &___builder_14; }
	inline void set_builder_14(UnityBuilder_t2457889383 * value)
	{
		___builder_14 = value;
		Il2CppCodeGenWriteBarrier(&___builder_14, value);
	}

	inline static int32_t get_offset_of_oneSignalPlatform_15() { return static_cast<int32_t>(offsetof(OneSignal_t3595519118_StaticFields, ___oneSignalPlatform_15)); }
	inline Il2CppObject * get_oneSignalPlatform_15() const { return ___oneSignalPlatform_15; }
	inline Il2CppObject ** get_address_of_oneSignalPlatform_15() { return &___oneSignalPlatform_15; }
	inline void set_oneSignalPlatform_15(Il2CppObject * value)
	{
		___oneSignalPlatform_15 = value;
		Il2CppCodeGenWriteBarrier(&___oneSignalPlatform_15, value);
	}

	inline static int32_t get_offset_of_logLevel_16() { return static_cast<int32_t>(offsetof(OneSignal_t3595519118_StaticFields, ___logLevel_16)); }
	inline int32_t get_logLevel_16() const { return ___logLevel_16; }
	inline int32_t* get_address_of_logLevel_16() { return &___logLevel_16; }
	inline void set_logLevel_16(int32_t value)
	{
		___logLevel_16 = value;
	}

	inline static int32_t get_offset_of_visualLogLevel_17() { return static_cast<int32_t>(offsetof(OneSignal_t3595519118_StaticFields, ___visualLogLevel_17)); }
	inline int32_t get_visualLogLevel_17() const { return ___visualLogLevel_17; }
	inline int32_t* get_address_of_visualLogLevel_17() { return &___visualLogLevel_17; }
	inline void set_visualLogLevel_17(int32_t value)
	{
		___visualLogLevel_17 = value;
	}

	inline static int32_t get_offset_of_requiresUserConsent_18() { return static_cast<int32_t>(offsetof(OneSignal_t3595519118_StaticFields, ___requiresUserConsent_18)); }
	inline bool get_requiresUserConsent_18() const { return ___requiresUserConsent_18; }
	inline bool* get_address_of_requiresUserConsent_18() { return &___requiresUserConsent_18; }
	inline void set_requiresUserConsent_18(bool value)
	{
		___requiresUserConsent_18 = value;
	}

	inline static int32_t get_offset_of_postNotificationSuccessDelegate_19() { return static_cast<int32_t>(offsetof(OneSignal_t3595519118_StaticFields, ___postNotificationSuccessDelegate_19)); }
	inline OnPostNotificationSuccess_t2053930136 * get_postNotificationSuccessDelegate_19() const { return ___postNotificationSuccessDelegate_19; }
	inline OnPostNotificationSuccess_t2053930136 ** get_address_of_postNotificationSuccessDelegate_19() { return &___postNotificationSuccessDelegate_19; }
	inline void set_postNotificationSuccessDelegate_19(OnPostNotificationSuccess_t2053930136 * value)
	{
		___postNotificationSuccessDelegate_19 = value;
		Il2CppCodeGenWriteBarrier(&___postNotificationSuccessDelegate_19, value);
	}

	inline static int32_t get_offset_of_postNotificationFailureDelegate_20() { return static_cast<int32_t>(offsetof(OneSignal_t3595519118_StaticFields, ___postNotificationFailureDelegate_20)); }
	inline OnPostNotificationFailure_t2834525727 * get_postNotificationFailureDelegate_20() const { return ___postNotificationFailureDelegate_20; }
	inline OnPostNotificationFailure_t2834525727 ** get_address_of_postNotificationFailureDelegate_20() { return &___postNotificationFailureDelegate_20; }
	inline void set_postNotificationFailureDelegate_20(OnPostNotificationFailure_t2834525727 * value)
	{
		___postNotificationFailureDelegate_20 = value;
		Il2CppCodeGenWriteBarrier(&___postNotificationFailureDelegate_20, value);
	}

	inline static int32_t get_offset_of_setEmailSuccessDelegate_21() { return static_cast<int32_t>(offsetof(OneSignal_t3595519118_StaticFields, ___setEmailSuccessDelegate_21)); }
	inline OnSetEmailSuccess_t2952831753 * get_setEmailSuccessDelegate_21() const { return ___setEmailSuccessDelegate_21; }
	inline OnSetEmailSuccess_t2952831753 ** get_address_of_setEmailSuccessDelegate_21() { return &___setEmailSuccessDelegate_21; }
	inline void set_setEmailSuccessDelegate_21(OnSetEmailSuccess_t2952831753 * value)
	{
		___setEmailSuccessDelegate_21 = value;
		Il2CppCodeGenWriteBarrier(&___setEmailSuccessDelegate_21, value);
	}

	inline static int32_t get_offset_of_setEmailFailureDelegate_22() { return static_cast<int32_t>(offsetof(OneSignal_t3595519118_StaticFields, ___setEmailFailureDelegate_22)); }
	inline OnSetEmailFailure_t3733427344 * get_setEmailFailureDelegate_22() const { return ___setEmailFailureDelegate_22; }
	inline OnSetEmailFailure_t3733427344 ** get_address_of_setEmailFailureDelegate_22() { return &___setEmailFailureDelegate_22; }
	inline void set_setEmailFailureDelegate_22(OnSetEmailFailure_t3733427344 * value)
	{
		___setEmailFailureDelegate_22 = value;
		Il2CppCodeGenWriteBarrier(&___setEmailFailureDelegate_22, value);
	}

	inline static int32_t get_offset_of_logoutEmailSuccessDelegate_23() { return static_cast<int32_t>(offsetof(OneSignal_t3595519118_StaticFields, ___logoutEmailSuccessDelegate_23)); }
	inline OnLogoutEmailSuccess_t3366364849 * get_logoutEmailSuccessDelegate_23() const { return ___logoutEmailSuccessDelegate_23; }
	inline OnLogoutEmailSuccess_t3366364849 ** get_address_of_logoutEmailSuccessDelegate_23() { return &___logoutEmailSuccessDelegate_23; }
	inline void set_logoutEmailSuccessDelegate_23(OnLogoutEmailSuccess_t3366364849 * value)
	{
		___logoutEmailSuccessDelegate_23 = value;
		Il2CppCodeGenWriteBarrier(&___logoutEmailSuccessDelegate_23, value);
	}

	inline static int32_t get_offset_of_logoutEmailFailureDelegate_24() { return static_cast<int32_t>(offsetof(OneSignal_t3595519118_StaticFields, ___logoutEmailFailureDelegate_24)); }
	inline OnLogoutEmailFailure_t4146960440 * get_logoutEmailFailureDelegate_24() const { return ___logoutEmailFailureDelegate_24; }
	inline OnLogoutEmailFailure_t4146960440 ** get_address_of_logoutEmailFailureDelegate_24() { return &___logoutEmailFailureDelegate_24; }
	inline void set_logoutEmailFailureDelegate_24(OnLogoutEmailFailure_t4146960440 * value)
	{
		___logoutEmailFailureDelegate_24 = value;
		Il2CppCodeGenWriteBarrier(&___logoutEmailFailureDelegate_24, value);
	}

	inline static int32_t get_offset_of__inFocusDisplayType_25() { return static_cast<int32_t>(offsetof(OneSignal_t3595519118_StaticFields, ____inFocusDisplayType_25)); }
	inline int32_t get__inFocusDisplayType_25() const { return ____inFocusDisplayType_25; }
	inline int32_t* get_address_of__inFocusDisplayType_25() { return &____inFocusDisplayType_25; }
	inline void set__inFocusDisplayType_25(int32_t value)
	{
		____inFocusDisplayType_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
