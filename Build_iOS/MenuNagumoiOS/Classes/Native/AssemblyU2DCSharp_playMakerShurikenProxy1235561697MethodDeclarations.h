﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// playMakerShurikenProxy
struct playMakerShurikenProxy_t1235561697;
// UnityEngine.ParticleCollisionEvent[]
struct ParticleCollisionEventU5BU5D_t4168492807;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void playMakerShurikenProxy::.ctor()
extern "C"  void playMakerShurikenProxy__ctor_m3002397914 (playMakerShurikenProxy_t1235561697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void playMakerShurikenProxy::Start()
extern "C"  void playMakerShurikenProxy_Start_m1949535706 (playMakerShurikenProxy_t1235561697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleCollisionEvent[] playMakerShurikenProxy::GetCollisionEvents()
extern "C"  ParticleCollisionEventU5BU5D_t4168492807* playMakerShurikenProxy_GetCollisionEvents_m408829237 (playMakerShurikenProxy_t1235561697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void playMakerShurikenProxy::OnParticleCollision(UnityEngine.GameObject)
extern "C"  void playMakerShurikenProxy_OnParticleCollision_m3481794397 (playMakerShurikenProxy_t1235561697 * __this, GameObject_t3674682005 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
