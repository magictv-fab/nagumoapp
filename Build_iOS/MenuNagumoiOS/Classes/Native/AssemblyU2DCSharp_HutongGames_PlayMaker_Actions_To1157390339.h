﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "UnityEngine_UnityEngine_TouchPhase1567063616.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.TouchEvent
struct  TouchEvent_t1157390339  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.TouchEvent::fingerId
	FsmInt_t1596138449 * ___fingerId_9;
	// UnityEngine.TouchPhase HutongGames.PlayMaker.Actions.TouchEvent::touchPhase
	int32_t ___touchPhase_10;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.TouchEvent::sendEvent
	FsmEvent_t2133468028 * ___sendEvent_11;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.TouchEvent::storeFingerId
	FsmInt_t1596138449 * ___storeFingerId_12;

public:
	inline static int32_t get_offset_of_fingerId_9() { return static_cast<int32_t>(offsetof(TouchEvent_t1157390339, ___fingerId_9)); }
	inline FsmInt_t1596138449 * get_fingerId_9() const { return ___fingerId_9; }
	inline FsmInt_t1596138449 ** get_address_of_fingerId_9() { return &___fingerId_9; }
	inline void set_fingerId_9(FsmInt_t1596138449 * value)
	{
		___fingerId_9 = value;
		Il2CppCodeGenWriteBarrier(&___fingerId_9, value);
	}

	inline static int32_t get_offset_of_touchPhase_10() { return static_cast<int32_t>(offsetof(TouchEvent_t1157390339, ___touchPhase_10)); }
	inline int32_t get_touchPhase_10() const { return ___touchPhase_10; }
	inline int32_t* get_address_of_touchPhase_10() { return &___touchPhase_10; }
	inline void set_touchPhase_10(int32_t value)
	{
		___touchPhase_10 = value;
	}

	inline static int32_t get_offset_of_sendEvent_11() { return static_cast<int32_t>(offsetof(TouchEvent_t1157390339, ___sendEvent_11)); }
	inline FsmEvent_t2133468028 * get_sendEvent_11() const { return ___sendEvent_11; }
	inline FsmEvent_t2133468028 ** get_address_of_sendEvent_11() { return &___sendEvent_11; }
	inline void set_sendEvent_11(FsmEvent_t2133468028 * value)
	{
		___sendEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___sendEvent_11, value);
	}

	inline static int32_t get_offset_of_storeFingerId_12() { return static_cast<int32_t>(offsetof(TouchEvent_t1157390339, ___storeFingerId_12)); }
	inline FsmInt_t1596138449 * get_storeFingerId_12() const { return ___storeFingerId_12; }
	inline FsmInt_t1596138449 ** get_address_of_storeFingerId_12() { return &___storeFingerId_12; }
	inline void set_storeFingerId_12(FsmInt_t1596138449 * value)
	{
		___storeFingerId_12 = value;
		Il2CppCodeGenWriteBarrier(&___storeFingerId_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
