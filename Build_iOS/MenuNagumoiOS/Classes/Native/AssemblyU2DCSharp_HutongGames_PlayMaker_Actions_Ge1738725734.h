﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetRoot
struct  GetRoot_t1738725734  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetRoot::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetRoot::storeRoot
	FsmGameObject_t1697147867 * ___storeRoot_10;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(GetRoot_t1738725734, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_storeRoot_10() { return static_cast<int32_t>(offsetof(GetRoot_t1738725734, ___storeRoot_10)); }
	inline FsmGameObject_t1697147867 * get_storeRoot_10() const { return ___storeRoot_10; }
	inline FsmGameObject_t1697147867 ** get_address_of_storeRoot_10() { return &___storeRoot_10; }
	inline void set_storeRoot_10(FsmGameObject_t1697147867 * value)
	{
		___storeRoot_10 = value;
		Il2CppCodeGenWriteBarrier(&___storeRoot_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
