﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Correios.Net.Http.Response
struct Response_t724041629;
// System.String
struct String_t;
// Correios.Net.Address
struct Address_t2296282618;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void Correios.Net.Http.Response::.ctor(System.String)
extern "C"  void Response__ctor_m2682919267 (Response_t724041629 * __this, String_t* ___responseText0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Correios.Net.Http.Response::get_Text()
extern "C"  String_t* Response_get_Text_m760613438 (Response_t724041629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Correios.Net.Http.Response::set_Text(System.String)
extern "C"  void Response_set_Text_m400865171 (Response_t724041629 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Correios.Net.Http.Response::GetValueByTag(System.String)
extern "C"  String_t* Response_GetValueByTag_m2831736928 (Response_t724041629 * __this, String_t* ___tag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Correios.Net.Address Correios.Net.Http.Response::ToAddress()
extern "C"  Address_t2296282618 * Response_ToAddress_m3116673474 (Response_t724041629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Correios.Net.Http.Response::IsInValidResponse()
extern "C"  bool Response_IsInValidResponse_m341125911 (Response_t724041629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
