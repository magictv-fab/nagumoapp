﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.ZipConstants
struct ZipConstants_t422568284;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ICSharpCode.SharpZipLib.Zip.ZipConstants::.ctor()
extern "C"  void ZipConstants__ctor_m285019599 (ZipConstants_t422568284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipConstants::.cctor()
extern "C"  void ZipConstants__cctor_m4058544062 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipConstants::get_DefaultCodePage()
extern "C"  int32_t ZipConstants_get_DefaultCodePage_m3698146739 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipConstants::set_DefaultCodePage(System.Int32)
extern "C"  void ZipConstants_set_DefaultCodePage_m3260659038 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Zip.ZipConstants::ConvertToString(System.Byte[],System.Int32)
extern "C"  String_t* ZipConstants_ConvertToString_m3918944215 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___data0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Zip.ZipConstants::ConvertToString(System.Byte[])
extern "C"  String_t* ZipConstants_ConvertToString_m65594624 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Zip.ZipConstants::ConvertToStringExt(System.Int32,System.Byte[],System.Int32)
extern "C"  String_t* ZipConstants_ConvertToStringExt_m2230691369 (Il2CppObject * __this /* static, unused */, int32_t ___flags0, ByteU5BU5D_t4260760469* ___data1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Zip.ZipConstants::ConvertToStringExt(System.Int32,System.Byte[])
extern "C"  String_t* ZipConstants_ConvertToStringExt_m820951406 (Il2CppObject * __this /* static, unused */, int32_t ___flags0, ByteU5BU5D_t4260760469* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ICSharpCode.SharpZipLib.Zip.ZipConstants::ConvertToArray(System.String)
extern "C"  ByteU5BU5D_t4260760469* ZipConstants_ConvertToArray_m2774944664 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ICSharpCode.SharpZipLib.Zip.ZipConstants::ConvertToArray(System.Int32,System.String)
extern "C"  ByteU5BU5D_t4260760469* ZipConstants_ConvertToArray_m3485053687 (Il2CppObject * __this /* static, unused */, int32_t ___flags0, String_t* ___str1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
