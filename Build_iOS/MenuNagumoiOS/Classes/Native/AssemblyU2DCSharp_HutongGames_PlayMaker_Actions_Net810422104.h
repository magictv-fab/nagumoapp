﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkGetSendRate
struct  NetworkGetSendRate_t810422104  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.NetworkGetSendRate::sendRate
	FsmFloat_t2134102846 * ___sendRate_9;

public:
	inline static int32_t get_offset_of_sendRate_9() { return static_cast<int32_t>(offsetof(NetworkGetSendRate_t810422104, ___sendRate_9)); }
	inline FsmFloat_t2134102846 * get_sendRate_9() const { return ___sendRate_9; }
	inline FsmFloat_t2134102846 ** get_address_of_sendRate_9() { return &___sendRate_9; }
	inline void set_sendRate_9(FsmFloat_t2134102846 * value)
	{
		___sendRate_9 = value;
		Il2CppCodeGenWriteBarrier(&___sendRate_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
