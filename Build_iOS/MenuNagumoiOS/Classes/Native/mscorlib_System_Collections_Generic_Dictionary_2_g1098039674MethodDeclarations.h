﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>
struct Dictionary_2_t1098039674;
// System.Collections.Generic.IEqualityComparer`1<System.Char>
struct IEqualityComparer_1_t3653656942;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<System.Char>
struct ICollection_1_t3757212525;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>[]
struct KeyValuePair_2U5BU5D_t508445813;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>>
struct IEnumerator_1_t2908685429;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Char,ZXing.Aztec.Internal.Decoder/Table>
struct KeyCollection_t2724799125;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Char,ZXing.Aztec.Internal.Decoder/Table>
struct ValueCollection_t4093612683;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_996820380.h"
#include "mscorlib_System_Array1146569071.h"
#include "QRCode_ZXing_Aztec_Internal_Decoder_Table1007478513.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2415363066.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::.ctor()
extern "C"  void Dictionary_2__ctor_m519508538_gshared (Dictionary_2_t1098039674 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m519508538(__this, method) ((  void (*) (Dictionary_2_t1098039674 *, const MethodInfo*))Dictionary_2__ctor_m519508538_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m989844826_gshared (Dictionary_2_t1098039674 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m989844826(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t1098039674 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m989844826_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m1123373876_gshared (Dictionary_2_t1098039674 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m1123373876(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t1098039674 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m1123373876_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m566534436_gshared (Dictionary_2_t1098039674 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m566534436(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1098039674 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m566534436_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m4078041621_gshared (Dictionary_2_t1098039674 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m4078041621(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1098039674 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m4078041621_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m12116961_gshared (Dictionary_2_t1098039674 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m12116961(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1098039674 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m12116961_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m1127052431_gshared (Dictionary_2_t1098039674 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m1127052431(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1098039674 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m1127052431_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m370646810_gshared (Dictionary_2_t1098039674 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m370646810(__this, method) ((  bool (*) (Dictionary_2_t1098039674 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m370646810_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1574343839_gshared (Dictionary_2_t1098039674 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1574343839(__this, method) ((  bool (*) (Dictionary_2_t1098039674 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1574343839_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m3070523835_gshared (Dictionary_2_t1098039674 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m3070523835(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1098039674 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m3070523835_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m2945984938_gshared (Dictionary_2_t1098039674 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m2945984938(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1098039674 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m2945984938_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m3698405991_gshared (Dictionary_2_t1098039674 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m3698405991(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1098039674 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m3698405991_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m1474970475_gshared (Dictionary_2_t1098039674 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m1474970475(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1098039674 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1474970475_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m2056730600_gshared (Dictionary_2_t1098039674 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m2056730600(__this, ___key0, method) ((  void (*) (Dictionary_2_t1098039674 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m2056730600_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3385138121_gshared (Dictionary_2_t1098039674 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3385138121(__this, method) ((  bool (*) (Dictionary_2_t1098039674 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3385138121_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3324256123_gshared (Dictionary_2_t1098039674 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3324256123(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1098039674 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3324256123_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m733943501_gshared (Dictionary_2_t1098039674 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m733943501(__this, method) ((  bool (*) (Dictionary_2_t1098039674 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m733943501_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1452789886_gshared (Dictionary_2_t1098039674 * __this, KeyValuePair_2_t996820380  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1452789886(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t1098039674 *, KeyValuePair_2_t996820380 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1452789886_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1237739848_gshared (Dictionary_2_t1098039674 * __this, KeyValuePair_2_t996820380  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1237739848(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1098039674 *, KeyValuePair_2_t996820380 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1237739848_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m4114406626_gshared (Dictionary_2_t1098039674 * __this, KeyValuePair_2U5BU5D_t508445813* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m4114406626(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1098039674 *, KeyValuePair_2U5BU5D_t508445813*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m4114406626_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m351158893_gshared (Dictionary_2_t1098039674 * __this, KeyValuePair_2_t996820380  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m351158893(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1098039674 *, KeyValuePair_2_t996820380 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m351158893_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m3777180737_gshared (Dictionary_2_t1098039674 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m3777180737(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1098039674 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m3777180737_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m518207888_gshared (Dictionary_2_t1098039674 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m518207888(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1098039674 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m518207888_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2001344711_gshared (Dictionary_2_t1098039674 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2001344711(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1098039674 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2001344711_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2790506324_gshared (Dictionary_2_t1098039674 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2790506324(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1098039674 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2790506324_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m563468035_gshared (Dictionary_2_t1098039674 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m563468035(__this, method) ((  int32_t (*) (Dictionary_2_t1098039674 *, const MethodInfo*))Dictionary_2_get_Count_m563468035_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::get_Item(TKey)
extern "C"  int32_t Dictionary_2_get_Item_m2861047972_gshared (Dictionary_2_t1098039674 * __this, Il2CppChar ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m2861047972(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t1098039674 *, Il2CppChar, const MethodInfo*))Dictionary_2_get_Item_m2861047972_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m2431379171_gshared (Dictionary_2_t1098039674 * __this, Il2CppChar ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m2431379171(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1098039674 *, Il2CppChar, int32_t, const MethodInfo*))Dictionary_2_set_Item_m2431379171_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m889412059_gshared (Dictionary_2_t1098039674 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m889412059(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t1098039674 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m889412059_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m1856549692_gshared (Dictionary_2_t1098039674 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m1856549692(__this, ___size0, method) ((  void (*) (Dictionary_2_t1098039674 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m1856549692_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m4007867960_gshared (Dictionary_2_t1098039674 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m4007867960(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1098039674 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m4007867960_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t996820380  Dictionary_2_make_pair_m3573562572_gshared (Il2CppObject * __this /* static, unused */, Il2CppChar ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m3573562572(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t996820380  (*) (Il2CppObject * /* static, unused */, Il2CppChar, int32_t, const MethodInfo*))Dictionary_2_make_pair_m3573562572_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::pick_key(TKey,TValue)
extern "C"  Il2CppChar Dictionary_2_pick_key_m1904498290_gshared (Il2CppObject * __this /* static, unused */, Il2CppChar ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m1904498290(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppChar (*) (Il2CppObject * /* static, unused */, Il2CppChar, int32_t, const MethodInfo*))Dictionary_2_pick_key_m1904498290_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::pick_value(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_value_m1937653838_gshared (Il2CppObject * __this /* static, unused */, Il2CppChar ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m1937653838(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppChar, int32_t, const MethodInfo*))Dictionary_2_pick_value_m1937653838_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m1089638039_gshared (Dictionary_2_t1098039674 * __this, KeyValuePair_2U5BU5D_t508445813* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m1089638039(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1098039674 *, KeyValuePair_2U5BU5D_t508445813*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1089638039_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::Resize()
extern "C"  void Dictionary_2_Resize_m1346509365_gshared (Dictionary_2_t1098039674 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m1346509365(__this, method) ((  void (*) (Dictionary_2_t1098039674 *, const MethodInfo*))Dictionary_2_Resize_m1346509365_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m1945547692_gshared (Dictionary_2_t1098039674 * __this, Il2CppChar ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m1945547692(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1098039674 *, Il2CppChar, int32_t, const MethodInfo*))Dictionary_2_Add_m1945547692_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::Clear()
extern "C"  void Dictionary_2_Clear_m3405239950_gshared (Dictionary_2_t1098039674 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m3405239950(__this, method) ((  void (*) (Dictionary_2_t1098039674 *, const MethodInfo*))Dictionary_2_Clear_m3405239950_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m2722181048_gshared (Dictionary_2_t1098039674 * __this, Il2CppChar ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m2722181048(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1098039674 *, Il2CppChar, const MethodInfo*))Dictionary_2_ContainsKey_m2722181048_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m781051832_gshared (Dictionary_2_t1098039674 * __this, int32_t ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m781051832(__this, ___value0, method) ((  bool (*) (Dictionary_2_t1098039674 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m781051832_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m2171896961_gshared (Dictionary_2_t1098039674 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m2171896961(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1098039674 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m2171896961_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m1531784323_gshared (Dictionary_2_t1098039674 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m1531784323(__this, ___sender0, method) ((  void (*) (Dictionary_2_t1098039674 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m1531784323_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m1612584504_gshared (Dictionary_2_t1098039674 * __this, Il2CppChar ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m1612584504(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1098039674 *, Il2CppChar, const MethodInfo*))Dictionary_2_Remove_m1612584504_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m39735313_gshared (Dictionary_2_t1098039674 * __this, Il2CppChar ___key0, int32_t* ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m39735313(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t1098039674 *, Il2CppChar, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m39735313_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::get_Keys()
extern "C"  KeyCollection_t2724799125 * Dictionary_2_get_Keys_m87234730_gshared (Dictionary_2_t1098039674 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m87234730(__this, method) ((  KeyCollection_t2724799125 * (*) (Dictionary_2_t1098039674 *, const MethodInfo*))Dictionary_2_get_Keys_m87234730_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::get_Values()
extern "C"  ValueCollection_t4093612683 * Dictionary_2_get_Values_m2224732742_gshared (Dictionary_2_t1098039674 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m2224732742(__this, method) ((  ValueCollection_t4093612683 * (*) (Dictionary_2_t1098039674 *, const MethodInfo*))Dictionary_2_get_Values_m2224732742_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::ToTKey(System.Object)
extern "C"  Il2CppChar Dictionary_2_ToTKey_m1354357197_gshared (Dictionary_2_t1098039674 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m1354357197(__this, ___key0, method) ((  Il2CppChar (*) (Dictionary_2_t1098039674 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m1354357197_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::ToTValue(System.Object)
extern "C"  int32_t Dictionary_2_ToTValue_m249967977_gshared (Dictionary_2_t1098039674 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m249967977(__this, ___value0, method) ((  int32_t (*) (Dictionary_2_t1098039674 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m249967977_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m678528027_gshared (Dictionary_2_t1098039674 * __this, KeyValuePair_2_t996820380  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m678528027(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t1098039674 *, KeyValuePair_2_t996820380 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m678528027_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::GetEnumerator()
extern "C"  Enumerator_t2415363066  Dictionary_2_GetEnumerator_m4172935406_gshared (Dictionary_2_t1098039674 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m4172935406(__this, method) ((  Enumerator_t2415363066  (*) (Dictionary_2_t1098039674 *, const MethodInfo*))Dictionary_2_GetEnumerator_m4172935406_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Char,ZXing.Aztec.Internal.Decoder/Table>::<CopyTo>m__2(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__2_m3741143587_gshared (Il2CppObject * __this /* static, unused */, Il2CppChar ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__2_m3741143587(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, Il2CppChar, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__2_m3741143587_gshared)(__this /* static, unused */, ___key0, ___value1, method)
