﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicApplicationSettings
struct MagicApplicationSettings_t2410043078;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.IO.FileInfo
struct FileInfo_t3233670074;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_FileInfo3233670074.h"
#include "mscorlib_System_String7231557.h"

// System.Void MagicApplicationSettings::.ctor()
extern "C"  void MagicApplicationSettings__ctor_m3374937749 (MagicApplicationSettings_t2410043078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicApplicationSettings::.cctor()
extern "C"  void MagicApplicationSettings__cctor_m1061758904 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicApplicationSettings::Awake()
extern "C"  void MagicApplicationSettings_Awake_m3612542968 (MagicApplicationSettings_t2410043078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MagicApplicationSettings::SetAppReady()
extern "C"  Il2CppObject * MagicApplicationSettings_SetAppReady_m3318921327 (MagicApplicationSettings_t2410043078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicApplicationSettings::ClickExit()
extern "C"  void MagicApplicationSettings_ClickExit_m3880159225 (MagicApplicationSettings_t2410043078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicApplicationSettings::ResetHard()
extern "C"  void MagicApplicationSettings_ResetHard_m1829762829 (MagicApplicationSettings_t2410043078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicApplicationSettings::<ResetHard>m__21(System.IO.FileInfo)
extern "C"  bool MagicApplicationSettings_U3CResetHardU3Em__21_m2822597756 (Il2CppObject * __this /* static, unused */, FileInfo_t3233670074 * ___f0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicApplicationSettings::<ResetHard>m__23(System.String)
extern "C"  bool MagicApplicationSettings_U3CResetHardU3Em__23_m2116489329 (Il2CppObject * __this /* static, unused */, String_t* ___f0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
