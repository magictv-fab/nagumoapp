﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FloatClamp
struct FloatClamp_t1735441703;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FloatClamp::.ctor()
extern "C"  void FloatClamp__ctor_m3898602271 (FloatClamp_t1735441703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatClamp::Reset()
extern "C"  void FloatClamp_Reset_m1545035212 (FloatClamp_t1735441703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatClamp::OnEnter()
extern "C"  void FloatClamp_OnEnter_m737028918 (FloatClamp_t1735441703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatClamp::OnUpdate()
extern "C"  void FloatClamp_OnUpdate_m506619341 (FloatClamp_t1735441703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatClamp::DoClamp()
extern "C"  void FloatClamp_DoClamp_m3332393005 (FloatClamp_t1735441703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
