﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.update.comm.ServerCommunication/<WaitForRequestToken>c__Iterator40
struct U3CWaitForRequestTokenU3Ec__Iterator40_t1531128233;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.update.comm.ServerCommunication/<WaitForRequestToken>c__Iterator40::.ctor()
extern "C"  void U3CWaitForRequestTokenU3Ec__Iterator40__ctor_m4243946770 (U3CWaitForRequestTokenU3Ec__Iterator40_t1531128233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MagicTV.update.comm.ServerCommunication/<WaitForRequestToken>c__Iterator40::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForRequestTokenU3Ec__Iterator40_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m230384074 (U3CWaitForRequestTokenU3Ec__Iterator40_t1531128233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MagicTV.update.comm.ServerCommunication/<WaitForRequestToken>c__Iterator40::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForRequestTokenU3Ec__Iterator40_System_Collections_IEnumerator_get_Current_m2448308574 (U3CWaitForRequestTokenU3Ec__Iterator40_t1531128233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicTV.update.comm.ServerCommunication/<WaitForRequestToken>c__Iterator40::MoveNext()
extern "C"  bool U3CWaitForRequestTokenU3Ec__Iterator40_MoveNext_m2863257226 (U3CWaitForRequestTokenU3Ec__Iterator40_t1531128233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.comm.ServerCommunication/<WaitForRequestToken>c__Iterator40::Dispose()
extern "C"  void U3CWaitForRequestTokenU3Ec__Iterator40_Dispose_m2407165135 (U3CWaitForRequestTokenU3Ec__Iterator40_t1531128233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.comm.ServerCommunication/<WaitForRequestToken>c__Iterator40::Reset()
extern "C"  void U3CWaitForRequestTokenU3Ec__Iterator40_Reset_m1890379711 (U3CWaitForRequestTokenU3Ec__Iterator40_t1531128233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
