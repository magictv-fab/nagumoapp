﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.PDF417.Internal.EC.ModulusGF
struct ModulusGF_t2738856732;
// ZXing.PDF417.Internal.EC.ModulusPoly
struct ModulusPoly_t3133325097;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_PDF417_Internal_EC_ModulusPoly3133325097.h"

// ZXing.PDF417.Internal.EC.ModulusPoly ZXing.PDF417.Internal.EC.ModulusGF::get_Zero()
extern "C"  ModulusPoly_t3133325097 * ModulusGF_get_Zero_m856798858 (ModulusGF_t2738856732 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.EC.ModulusGF::set_Zero(ZXing.PDF417.Internal.EC.ModulusPoly)
extern "C"  void ModulusGF_set_Zero_m3301361729 (ModulusGF_t2738856732 * __this, ModulusPoly_t3133325097 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.PDF417.Internal.EC.ModulusPoly ZXing.PDF417.Internal.EC.ModulusGF::get_One()
extern "C"  ModulusPoly_t3133325097 * ModulusGF_get_One_m3758511334 (ModulusGF_t2738856732 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.EC.ModulusGF::set_One(ZXing.PDF417.Internal.EC.ModulusPoly)
extern "C"  void ModulusGF_set_One_m1808377279 (ModulusGF_t2738856732 * __this, ModulusPoly_t3133325097 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.EC.ModulusGF::.ctor(System.Int32,System.Int32)
extern "C"  void ModulusGF__ctor_m1801701830 (ModulusGF_t2738856732 * __this, int32_t ___modulus0, int32_t ___generator1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.PDF417.Internal.EC.ModulusPoly ZXing.PDF417.Internal.EC.ModulusGF::buildMonomial(System.Int32,System.Int32)
extern "C"  ModulusPoly_t3133325097 * ModulusGF_buildMonomial_m1744067397 (ModulusGF_t2738856732 * __this, int32_t ___degree0, int32_t ___coefficient1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.EC.ModulusGF::add(System.Int32,System.Int32)
extern "C"  int32_t ModulusGF_add_m2644361621 (ModulusGF_t2738856732 * __this, int32_t ___a0, int32_t ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.EC.ModulusGF::subtract(System.Int32,System.Int32)
extern "C"  int32_t ModulusGF_subtract_m2948748992 (ModulusGF_t2738856732 * __this, int32_t ___a0, int32_t ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.EC.ModulusGF::exp(System.Int32)
extern "C"  int32_t ModulusGF_exp_m1559774974 (ModulusGF_t2738856732 * __this, int32_t ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.EC.ModulusGF::log(System.Int32)
extern "C"  int32_t ModulusGF_log_m2059784165 (ModulusGF_t2738856732 * __this, int32_t ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.EC.ModulusGF::inverse(System.Int32)
extern "C"  int32_t ModulusGF_inverse_m835435345 (ModulusGF_t2738856732 * __this, int32_t ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.EC.ModulusGF::multiply(System.Int32,System.Int32)
extern "C"  int32_t ModulusGF_multiply_m2800640432 (ModulusGF_t2738856732 * __this, int32_t ___a0, int32_t ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.EC.ModulusGF::get_Size()
extern "C"  int32_t ModulusGF_get_Size_m1572765180 (ModulusGF_t2738856732 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.EC.ModulusGF::.cctor()
extern "C"  void ModulusGF__cctor_m2838045261 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
