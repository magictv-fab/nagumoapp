﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlWriter4278601340.h"
#include "System_Xml_System_Xml_XmlTextWriter1523325321.h"
#include "System_Xml_System_Xml_XmlTextWriter_XmlNodeInfo1808742809.h"
#include "System_Xml_System_Xml_XmlTextWriter_StringUtil614327009.h"
#include "System_Xml_System_Xml_XmlTextWriter_XmlDeclState205203294.h"
#include "System_Xml_System_Xml_XmlStreamReader837919148.h"
#include "System_Xml_System_Xml_NonBlockingStreamReader2673413431.h"
#include "System_Xml_System_Xml_XmlInputStream2962968081.h"
#include "System_Xml_System_Xml_XmlParserInput3438354706.h"
#include "System_Xml_System_Xml_XmlParserInput_XmlParserInput173147476.h"
#include "System_Xml_Mono_Xml_Schema_XsdWhitespaceFacet1710562669.h"
#include "System_Xml_Mono_Xml_Schema_XsdOrdering1283527491.h"
#include "System_Xml_Mono_Xml_Schema_XsdAnySimpleType730532811.h"
#include "System_Xml_Mono_Xml_Schema_XdtAnyAtomicType1663577061.h"
#include "System_Xml_Mono_Xml_Schema_XdtUntypedAtomic230829008.h"
#include "System_Xml_Mono_Xml_Schema_XsdString3497764096.h"
#include "System_Xml_Mono_Xml_Schema_XsdNormalizedString3069561175.h"
#include "System_Xml_Mono_Xml_Schema_XsdToken2036792492.h"
#include "System_Xml_Mono_Xml_Schema_XsdLanguage2730590407.h"
#include "System_Xml_Mono_Xml_Schema_XsdNMToken280424909.h"
#include "System_Xml_Mono_Xml_Schema_XsdNMTokens613775752.h"
#include "System_Xml_Mono_Xml_Schema_XsdName1434515194.h"
#include "System_Xml_Mono_Xml_Schema_XsdNCName3308285615.h"
#include "System_Xml_Mono_Xml_Schema_XsdID744268010.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDRef2025328715.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDRefs3166186186.h"
#include "System_Xml_Mono_Xml_Schema_XsdEntity3091474642.h"
#include "System_Xml_Mono_Xml_Schema_XsdEntities2242065712.h"
#include "System_Xml_Mono_Xml_Schema_XsdNotation1627605361.h"
#include "System_Xml_Mono_Xml_Schema_XsdDecimal696098180.h"
#include "System_Xml_Mono_Xml_Schema_XsdInteger1111886705.h"
#include "System_Xml_Mono_Xml_Schema_XsdLong1434469099.h"
#include "System_Xml_Mono_Xml_Schema_XsdInt2108011298.h"
#include "System_Xml_Mono_Xml_Schema_XsdShort2035664687.h"
#include "System_Xml_Mono_Xml_Schema_XsdByte1434180983.h"
#include "System_Xml_Mono_Xml_Schema_XsdNonNegativeInteger4252412075.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedLong1954151808.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedInt2817511917.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedShort965959482.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedByte1953863692.h"
#include "System_Xml_Mono_Xml_Schema_XsdPositiveInteger346877240.h"
#include "System_Xml_Mono_Xml_Schema_XsdNonPositiveInteger4018722407.h"
#include "System_Xml_Mono_Xml_Schema_XsdNegativeInteger580566908.h"
#include "System_Xml_Mono_Xml_Schema_XsdFloat2023777551.h"
#include "System_Xml_Mono_Xml_Schema_XsdDouble3063791808.h"
#include "System_Xml_Mono_Xml_Schema_XsdBase64Binary3891592351.h"
#include "System_Xml_Mono_Xml_Schema_XsdHexBinary4085742767.h"
#include "System_Xml_Mono_Xml_Schema_XsdQName2033029455.h"
#include "System_Xml_Mono_Xml_Schema_XsdBoolean3513513563.h"
#include "System_Xml_Mono_Xml_Schema_XsdAnyURI2977086671.h"
#include "System_Xml_Mono_Xml_Schema_XmlSchemaUri2502783536.h"
#include "System_Xml_Mono_Xml_Schema_XsdDuration2352167683.h"
#include "System_Xml_Mono_Xml_Schema_XdtDayTimeDuration621607249.h"
#include "System_Xml_Mono_Xml_Schema_XdtYearMonthDuration36650571.h"
#include "System_Xml_Mono_Xml_Schema_XsdDateTime1841962250.h"
#include "System_Xml_Mono_Xml_Schema_XsdDate1434217501.h"
#include "System_Xml_Mono_Xml_Schema_XsdTime1434701628.h"
#include "System_Xml_Mono_Xml_Schema_XsdGYearMonth1947427211.h"
#include "System_Xml_Mono_Xml_Schema_XsdGMonthDay297523254.h"
#include "System_Xml_Mono_Xml_Schema_XsdGYear2024125431.h"
#include "System_Xml_Mono_Xml_Schema_XsdGMonth3118112584.h"
#include "System_Xml_Mono_Xml_Schema_XsdGDay1434278436.h"
#include "System_Xml_System_Xml_Schema_QNameValueType949579411.h"
#include "System_Xml_System_Xml_Schema_StringValueType3648929520.h"
#include "System_Xml_System_Xml_Schema_UriValueType3320269699.h"
#include "System_Xml_System_Xml_Schema_StringArrayValueType3288337575.h"
#include "System_Xml_System_Xml_Schema_ValidationEventArgs3260433748.h"
#include "System_Xml_System_Xml_Schema_XmlSchema1010706190.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAll4125456685.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAnnotated4226823396.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAnnotation2937014557.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAny4125456760.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAnyAttribute2348706750.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAppInfo238359163.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAttribute2904598248.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAttributeGro2847060657.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAttributeGro1667982748.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaChoice2942714895.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaCollection2750745740.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaCollectionEn1837880752.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaCompilationS1577913490.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaComplexConten699193111.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaComplexConte3934413474.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaComplexConte2765841583.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaComplexType2974990486.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaContent1984162949.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaContentModel3910036446.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaContentProce2425104824.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaContentType2180219871.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDatatype196391954.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDerivationMet998439334.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDocumentatio3797104518.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaElement3664762632.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaEnumerationF1065605342.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaException78171291.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaExternal880573945.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaFacet2982631811.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaFacet_Facet3197530270.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1200 = { sizeof (XmlWriter_t4278601340), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1201 = { sizeof (XmlTextWriter_t1523325321), -1, sizeof(XmlTextWriter_t1523325321_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1201[35] = 
{
	XmlTextWriter_t1523325321_StaticFields::get_offset_of_unmarked_utf8encoding_0(),
	XmlTextWriter_t1523325321_StaticFields::get_offset_of_escaped_text_chars_1(),
	XmlTextWriter_t1523325321_StaticFields::get_offset_of_escaped_attr_chars_2(),
	XmlTextWriter_t1523325321::get_offset_of_base_stream_3(),
	XmlTextWriter_t1523325321::get_offset_of_source_4(),
	XmlTextWriter_t1523325321::get_offset_of_writer_5(),
	XmlTextWriter_t1523325321::get_offset_of_preserver_6(),
	XmlTextWriter_t1523325321::get_offset_of_preserved_name_7(),
	XmlTextWriter_t1523325321::get_offset_of_is_preserved_xmlns_8(),
	XmlTextWriter_t1523325321::get_offset_of_allow_doc_fragment_9(),
	XmlTextWriter_t1523325321::get_offset_of_close_output_stream_10(),
	XmlTextWriter_t1523325321::get_offset_of_ignore_encoding_11(),
	XmlTextWriter_t1523325321::get_offset_of_namespaces_12(),
	XmlTextWriter_t1523325321::get_offset_of_xmldecl_state_13(),
	XmlTextWriter_t1523325321::get_offset_of_check_character_validity_14(),
	XmlTextWriter_t1523325321::get_offset_of_newline_handling_15(),
	XmlTextWriter_t1523325321::get_offset_of_is_document_entity_16(),
	XmlTextWriter_t1523325321::get_offset_of_state_17(),
	XmlTextWriter_t1523325321::get_offset_of_node_state_18(),
	XmlTextWriter_t1523325321::get_offset_of_nsmanager_19(),
	XmlTextWriter_t1523325321::get_offset_of_open_count_20(),
	XmlTextWriter_t1523325321::get_offset_of_elements_21(),
	XmlTextWriter_t1523325321::get_offset_of_new_local_namespaces_22(),
	XmlTextWriter_t1523325321::get_offset_of_explicit_nsdecls_23(),
	XmlTextWriter_t1523325321::get_offset_of_namespace_handling_24(),
	XmlTextWriter_t1523325321::get_offset_of_indent_25(),
	XmlTextWriter_t1523325321::get_offset_of_indent_count_26(),
	XmlTextWriter_t1523325321::get_offset_of_indent_char_27(),
	XmlTextWriter_t1523325321::get_offset_of_indent_string_28(),
	XmlTextWriter_t1523325321::get_offset_of_newline_29(),
	XmlTextWriter_t1523325321::get_offset_of_indent_attributes_30(),
	XmlTextWriter_t1523325321::get_offset_of_quote_char_31(),
	XmlTextWriter_t1523325321::get_offset_of_v2_32(),
	XmlTextWriter_t1523325321_StaticFields::get_offset_of_U3CU3Ef__switchU24map3A_33(),
	XmlTextWriter_t1523325321_StaticFields::get_offset_of_U3CU3Ef__switchU24map3B_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1202 = { sizeof (XmlNodeInfo_t1808742809), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1202[7] = 
{
	XmlNodeInfo_t1808742809::get_offset_of_Prefix_0(),
	XmlNodeInfo_t1808742809::get_offset_of_LocalName_1(),
	XmlNodeInfo_t1808742809::get_offset_of_NS_2(),
	XmlNodeInfo_t1808742809::get_offset_of_HasSimple_3(),
	XmlNodeInfo_t1808742809::get_offset_of_HasElements_4(),
	XmlNodeInfo_t1808742809::get_offset_of_XmlLang_5(),
	XmlNodeInfo_t1808742809::get_offset_of_XmlSpace_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1203 = { sizeof (StringUtil_t614327009), -1, sizeof(StringUtil_t614327009_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1203[2] = 
{
	StringUtil_t614327009_StaticFields::get_offset_of_cul_0(),
	StringUtil_t614327009_StaticFields::get_offset_of_cmp_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1204 = { sizeof (XmlDeclState_t205203294)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1204[5] = 
{
	XmlDeclState_t205203294::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1205 = { sizeof (XmlStreamReader_t837919148), -1, sizeof(XmlStreamReader_t837919148_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1205[2] = 
{
	XmlStreamReader_t837919148::get_offset_of_input_13(),
	XmlStreamReader_t837919148_StaticFields::get_offset_of_invalidDataException_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1206 = { sizeof (NonBlockingStreamReader_t2673413431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1206[11] = 
{
	NonBlockingStreamReader_t2673413431::get_offset_of_input_buffer_2(),
	NonBlockingStreamReader_t2673413431::get_offset_of_decoded_buffer_3(),
	NonBlockingStreamReader_t2673413431::get_offset_of_decoded_count_4(),
	NonBlockingStreamReader_t2673413431::get_offset_of_pos_5(),
	NonBlockingStreamReader_t2673413431::get_offset_of_buffer_size_6(),
	NonBlockingStreamReader_t2673413431::get_offset_of_encoding_7(),
	NonBlockingStreamReader_t2673413431::get_offset_of_decoder_8(),
	NonBlockingStreamReader_t2673413431::get_offset_of_base_stream_9(),
	NonBlockingStreamReader_t2673413431::get_offset_of_mayBlock_10(),
	NonBlockingStreamReader_t2673413431::get_offset_of_line_builder_11(),
	NonBlockingStreamReader_t2673413431::get_offset_of_foundCR_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1207 = { sizeof (XmlInputStream_t2962968081), -1, sizeof(XmlInputStream_t2962968081_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1207[7] = 
{
	XmlInputStream_t2962968081_StaticFields::get_offset_of_StrictUTF8_2(),
	XmlInputStream_t2962968081::get_offset_of_enc_3(),
	XmlInputStream_t2962968081::get_offset_of_stream_4(),
	XmlInputStream_t2962968081::get_offset_of_buffer_5(),
	XmlInputStream_t2962968081::get_offset_of_bufLength_6(),
	XmlInputStream_t2962968081::get_offset_of_bufPos_7(),
	XmlInputStream_t2962968081_StaticFields::get_offset_of_encodingException_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1208 = { sizeof (XmlParserInput_t3438354706), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1208[5] = 
{
	XmlParserInput_t3438354706::get_offset_of_sourceStack_0(),
	XmlParserInput_t3438354706::get_offset_of_source_1(),
	XmlParserInput_t3438354706::get_offset_of_has_peek_2(),
	XmlParserInput_t3438354706::get_offset_of_peek_char_3(),
	XmlParserInput_t3438354706::get_offset_of_allowTextDecl_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1209 = { sizeof (XmlParserInputSource_t173147476), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1209[6] = 
{
	XmlParserInputSource_t173147476::get_offset_of_BaseURI_0(),
	XmlParserInputSource_t173147476::get_offset_of_reader_1(),
	XmlParserInputSource_t173147476::get_offset_of_state_2(),
	XmlParserInputSource_t173147476::get_offset_of_isPE_3(),
	XmlParserInputSource_t173147476::get_offset_of_line_4(),
	XmlParserInputSource_t173147476::get_offset_of_column_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1210 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1211 = { sizeof (XsdWhitespaceFacet_t1710562669)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1211[4] = 
{
	XsdWhitespaceFacet_t1710562669::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1212 = { sizeof (XsdOrdering_t1283527491)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1212[5] = 
{
	XsdOrdering_t1283527491::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1213 = { sizeof (XsdAnySimpleType_t730532811), -1, sizeof(XsdAnySimpleType_t730532811_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1213[6] = 
{
	XsdAnySimpleType_t730532811_StaticFields::get_offset_of_instance_55(),
	XsdAnySimpleType_t730532811_StaticFields::get_offset_of_whitespaceArray_56(),
	XsdAnySimpleType_t730532811_StaticFields::get_offset_of_booleanAllowedFacets_57(),
	XsdAnySimpleType_t730532811_StaticFields::get_offset_of_decimalAllowedFacets_58(),
	XsdAnySimpleType_t730532811_StaticFields::get_offset_of_durationAllowedFacets_59(),
	XsdAnySimpleType_t730532811_StaticFields::get_offset_of_stringAllowedFacets_60(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1214 = { sizeof (XdtAnyAtomicType_t1663577061), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1215 = { sizeof (XdtUntypedAtomic_t230829008), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1216 = { sizeof (XsdString_t3497764096), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1217 = { sizeof (XsdNormalizedString_t3069561175), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1218 = { sizeof (XsdToken_t2036792492), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1219 = { sizeof (XsdLanguage_t2730590407), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1220 = { sizeof (XsdNMToken_t280424909), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1221 = { sizeof (XsdNMTokens_t613775752), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1222 = { sizeof (XsdName_t1434515194), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1223 = { sizeof (XsdNCName_t3308285615), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1224 = { sizeof (XsdID_t744268010), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1225 = { sizeof (XsdIDRef_t2025328715), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1226 = { sizeof (XsdIDRefs_t3166186186), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1227 = { sizeof (XsdEntity_t3091474642), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1228 = { sizeof (XsdEntities_t2242065712), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1229 = { sizeof (XsdNotation_t1627605361), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1230 = { sizeof (XsdDecimal_t696098180), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1231 = { sizeof (XsdInteger_t1111886705), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1232 = { sizeof (XsdLong_t1434469099), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1233 = { sizeof (XsdInt_t2108011298), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1234 = { sizeof (XsdShort_t2035664687), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1235 = { sizeof (XsdByte_t1434180983), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1236 = { sizeof (XsdNonNegativeInteger_t4252412075), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1237 = { sizeof (XsdUnsignedLong_t1954151808), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1238 = { sizeof (XsdUnsignedInt_t2817511917), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1239 = { sizeof (XsdUnsignedShort_t965959482), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1240 = { sizeof (XsdUnsignedByte_t1953863692), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1241 = { sizeof (XsdPositiveInteger_t346877240), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1242 = { sizeof (XsdNonPositiveInteger_t4018722407), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1243 = { sizeof (XsdNegativeInteger_t580566908), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1244 = { sizeof (XsdFloat_t2023777551), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1245 = { sizeof (XsdDouble_t3063791808), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1246 = { sizeof (XsdBase64Binary_t3891592351), -1, sizeof(XsdBase64Binary_t3891592351_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1246[2] = 
{
	XsdBase64Binary_t3891592351_StaticFields::get_offset_of_ALPHABET_61(),
	XsdBase64Binary_t3891592351_StaticFields::get_offset_of_decodeTable_62(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1247 = { sizeof (XsdHexBinary_t4085742767), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1248 = { sizeof (XsdQName_t2033029455), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1249 = { sizeof (XsdBoolean_t3513513563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1250 = { sizeof (XsdAnyURI_t2977086671), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1251 = { sizeof (XmlSchemaUri_t2502783536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1251[1] = 
{
	XmlSchemaUri_t2502783536::get_offset_of_value_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1252 = { sizeof (XsdDuration_t2352167683), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1253 = { sizeof (XdtDayTimeDuration_t621607249), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1254 = { sizeof (XdtYearMonthDuration_t36650571), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1255 = { sizeof (XsdDateTime_t1841962250), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1256 = { sizeof (XsdDate_t1434217501), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1257 = { sizeof (XsdTime_t1434701628), -1, sizeof(XsdTime_t1434701628_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1257[1] = 
{
	XsdTime_t1434701628_StaticFields::get_offset_of_timeFormats_61(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1258 = { sizeof (XsdGYearMonth_t1947427211), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1259 = { sizeof (XsdGMonthDay_t297523254), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1260 = { sizeof (XsdGYear_t2024125431), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1261 = { sizeof (XsdGMonth_t3118112584), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1262 = { sizeof (XsdGDay_t1434278436), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1263 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1264 = { sizeof (QNameValueType_t949579411)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1264[1] = 
{
	QNameValueType_t949579411::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1265 = { sizeof (StringValueType_t3648929520)+ sizeof (Il2CppObject), sizeof(StringValueType_t3648929520_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1265[1] = 
{
	StringValueType_t3648929520::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1266 = { sizeof (UriValueType_t3320269699)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1266[1] = 
{
	UriValueType_t3320269699::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1267 = { sizeof (StringArrayValueType_t3288337575)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1267[1] = 
{
	StringArrayValueType_t3288337575::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1268 = { sizeof (ValidationEventArgs_t3260433748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1268[3] = 
{
	ValidationEventArgs_t3260433748::get_offset_of_exception_1(),
	ValidationEventArgs_t3260433748::get_offset_of_message_2(),
	ValidationEventArgs_t3260433748::get_offset_of_severity_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1269 = { sizeof (XmlSchema_t1010706190), -1, sizeof(XmlSchema_t1010706190_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1269[20] = 
{
	XmlSchema_t1010706190::get_offset_of_attributeFormDefault_13(),
	XmlSchema_t1010706190::get_offset_of_attributeGroups_14(),
	XmlSchema_t1010706190::get_offset_of_attributes_15(),
	XmlSchema_t1010706190::get_offset_of_blockDefault_16(),
	XmlSchema_t1010706190::get_offset_of_elementFormDefault_17(),
	XmlSchema_t1010706190::get_offset_of_elements_18(),
	XmlSchema_t1010706190::get_offset_of_finalDefault_19(),
	XmlSchema_t1010706190::get_offset_of_groups_20(),
	XmlSchema_t1010706190::get_offset_of_id_21(),
	XmlSchema_t1010706190::get_offset_of_includes_22(),
	XmlSchema_t1010706190::get_offset_of_items_23(),
	XmlSchema_t1010706190::get_offset_of_notations_24(),
	XmlSchema_t1010706190::get_offset_of_schemaTypes_25(),
	XmlSchema_t1010706190::get_offset_of_targetNamespace_26(),
	XmlSchema_t1010706190::get_offset_of_version_27(),
	XmlSchema_t1010706190::get_offset_of_schemas_28(),
	XmlSchema_t1010706190::get_offset_of_nameTable_29(),
	XmlSchema_t1010706190::get_offset_of_missedSubComponents_30(),
	XmlSchema_t1010706190::get_offset_of_compilationItems_31(),
	XmlSchema_t1010706190_StaticFields::get_offset_of_U3CU3Ef__switchU24map41_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1270 = { sizeof (XmlSchemaAll_t4125456685), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1270[3] = 
{
	XmlSchemaAll_t4125456685::get_offset_of_schema_28(),
	XmlSchemaAll_t4125456685::get_offset_of_items_29(),
	XmlSchemaAll_t4125456685::get_offset_of_emptiable_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1271 = { sizeof (XmlSchemaAnnotated_t4226823396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1271[3] = 
{
	XmlSchemaAnnotated_t4226823396::get_offset_of_annotation_13(),
	XmlSchemaAnnotated_t4226823396::get_offset_of_id_14(),
	XmlSchemaAnnotated_t4226823396::get_offset_of_unhandledAttributes_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1272 = { sizeof (XmlSchemaAnnotation_t2937014557), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1272[2] = 
{
	XmlSchemaAnnotation_t2937014557::get_offset_of_id_13(),
	XmlSchemaAnnotation_t2937014557::get_offset_of_items_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1273 = { sizeof (XmlSchemaAny_t4125456760), -1, sizeof(XmlSchemaAny_t4125456760_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1273[4] = 
{
	XmlSchemaAny_t4125456760_StaticFields::get_offset_of_anyTypeContent_27(),
	XmlSchemaAny_t4125456760::get_offset_of_nameSpace_28(),
	XmlSchemaAny_t4125456760::get_offset_of_processing_29(),
	XmlSchemaAny_t4125456760::get_offset_of_wildcard_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1274 = { sizeof (XmlSchemaAnyAttribute_t2348706750), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1274[3] = 
{
	XmlSchemaAnyAttribute_t2348706750::get_offset_of_nameSpace_16(),
	XmlSchemaAnyAttribute_t2348706750::get_offset_of_processing_17(),
	XmlSchemaAnyAttribute_t2348706750::get_offset_of_wildcard_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1275 = { sizeof (XmlSchemaAppInfo_t238359163), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1275[2] = 
{
	XmlSchemaAppInfo_t238359163::get_offset_of_markup_13(),
	XmlSchemaAppInfo_t238359163::get_offset_of_source_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1276 = { sizeof (XmlSchemaAttribute_t2904598248), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1276[17] = 
{
	XmlSchemaAttribute_t2904598248::get_offset_of_attributeType_16(),
	XmlSchemaAttribute_t2904598248::get_offset_of_attributeSchemaType_17(),
	XmlSchemaAttribute_t2904598248::get_offset_of_defaultValue_18(),
	XmlSchemaAttribute_t2904598248::get_offset_of_fixedValue_19(),
	XmlSchemaAttribute_t2904598248::get_offset_of_validatedDefaultValue_20(),
	XmlSchemaAttribute_t2904598248::get_offset_of_validatedFixedValue_21(),
	XmlSchemaAttribute_t2904598248::get_offset_of_validatedFixedTypedValue_22(),
	XmlSchemaAttribute_t2904598248::get_offset_of_form_23(),
	XmlSchemaAttribute_t2904598248::get_offset_of_name_24(),
	XmlSchemaAttribute_t2904598248::get_offset_of_targetNamespace_25(),
	XmlSchemaAttribute_t2904598248::get_offset_of_qualifiedName_26(),
	XmlSchemaAttribute_t2904598248::get_offset_of_refName_27(),
	XmlSchemaAttribute_t2904598248::get_offset_of_schemaType_28(),
	XmlSchemaAttribute_t2904598248::get_offset_of_schemaTypeName_29(),
	XmlSchemaAttribute_t2904598248::get_offset_of_use_30(),
	XmlSchemaAttribute_t2904598248::get_offset_of_validatedUse_31(),
	XmlSchemaAttribute_t2904598248::get_offset_of_referencedAttribute_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1277 = { sizeof (XmlSchemaAttributeGroup_t2847060657), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1277[8] = 
{
	XmlSchemaAttributeGroup_t2847060657::get_offset_of_anyAttribute_16(),
	XmlSchemaAttributeGroup_t2847060657::get_offset_of_attributes_17(),
	XmlSchemaAttributeGroup_t2847060657::get_offset_of_name_18(),
	XmlSchemaAttributeGroup_t2847060657::get_offset_of_redefined_19(),
	XmlSchemaAttributeGroup_t2847060657::get_offset_of_qualifiedName_20(),
	XmlSchemaAttributeGroup_t2847060657::get_offset_of_attributeUses_21(),
	XmlSchemaAttributeGroup_t2847060657::get_offset_of_anyAttributeUse_22(),
	XmlSchemaAttributeGroup_t2847060657::get_offset_of_AttributeGroupRecursionCheck_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1278 = { sizeof (XmlSchemaAttributeGroupRef_t1667982748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1278[1] = 
{
	XmlSchemaAttributeGroupRef_t1667982748::get_offset_of_refName_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1279 = { sizeof (XmlSchemaChoice_t2942714895), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1279[2] = 
{
	XmlSchemaChoice_t2942714895::get_offset_of_items_28(),
	XmlSchemaChoice_t2942714895::get_offset_of_minEffectiveTotalRange_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1280 = { sizeof (XmlSchemaCollection_t2750745740), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1280[2] = 
{
	XmlSchemaCollection_t2750745740::get_offset_of_schemaSet_0(),
	XmlSchemaCollection_t2750745740::get_offset_of_ValidationEventHandler_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1281 = { sizeof (XmlSchemaCollectionEnumerator_t1837880752), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1281[1] = 
{
	XmlSchemaCollectionEnumerator_t1837880752::get_offset_of_xenum_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1282 = { sizeof (XmlSchemaCompilationSettings_t1577913490), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1282[1] = 
{
	XmlSchemaCompilationSettings_t1577913490::get_offset_of_enable_upa_check_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1283 = { sizeof (XmlSchemaComplexContent_t699193111), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1283[2] = 
{
	XmlSchemaComplexContent_t699193111::get_offset_of_content_16(),
	XmlSchemaComplexContent_t699193111::get_offset_of_isMixed_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1284 = { sizeof (XmlSchemaComplexContentExtension_t3934413474), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1284[4] = 
{
	XmlSchemaComplexContentExtension_t3934413474::get_offset_of_any_17(),
	XmlSchemaComplexContentExtension_t3934413474::get_offset_of_attributes_18(),
	XmlSchemaComplexContentExtension_t3934413474::get_offset_of_baseTypeName_19(),
	XmlSchemaComplexContentExtension_t3934413474::get_offset_of_particle_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1285 = { sizeof (XmlSchemaComplexContentRestriction_t2765841583), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1285[4] = 
{
	XmlSchemaComplexContentRestriction_t2765841583::get_offset_of_any_17(),
	XmlSchemaComplexContentRestriction_t2765841583::get_offset_of_attributes_18(),
	XmlSchemaComplexContentRestriction_t2765841583::get_offset_of_baseTypeName_19(),
	XmlSchemaComplexContentRestriction_t2765841583::get_offset_of_particle_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1286 = { sizeof (XmlSchemaComplexType_t2974990486), -1, sizeof(XmlSchemaComplexType_t2974990486_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1286[17] = 
{
	XmlSchemaComplexType_t2974990486::get_offset_of_anyAttribute_28(),
	XmlSchemaComplexType_t2974990486::get_offset_of_attributes_29(),
	XmlSchemaComplexType_t2974990486::get_offset_of_attributeUses_30(),
	XmlSchemaComplexType_t2974990486::get_offset_of_attributeWildcard_31(),
	XmlSchemaComplexType_t2974990486::get_offset_of_block_32(),
	XmlSchemaComplexType_t2974990486::get_offset_of_blockResolved_33(),
	XmlSchemaComplexType_t2974990486::get_offset_of_contentModel_34(),
	XmlSchemaComplexType_t2974990486::get_offset_of_validatableParticle_35(),
	XmlSchemaComplexType_t2974990486::get_offset_of_contentTypeParticle_36(),
	XmlSchemaComplexType_t2974990486::get_offset_of_isAbstract_37(),
	XmlSchemaComplexType_t2974990486::get_offset_of_isMixed_38(),
	XmlSchemaComplexType_t2974990486::get_offset_of_particle_39(),
	XmlSchemaComplexType_t2974990486::get_offset_of_resolvedContentType_40(),
	XmlSchemaComplexType_t2974990486::get_offset_of_ValidatedIsAbstract_41(),
	XmlSchemaComplexType_t2974990486_StaticFields::get_offset_of_anyType_42(),
	XmlSchemaComplexType_t2974990486_StaticFields::get_offset_of_AnyTypeName_43(),
	XmlSchemaComplexType_t2974990486::get_offset_of_CollectProcessId_44(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1287 = { sizeof (XmlSchemaContent_t1984162949), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1287[1] = 
{
	XmlSchemaContent_t1984162949::get_offset_of_actualBaseSchemaType_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1288 = { sizeof (XmlSchemaContentModel_t3910036446), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1289 = { sizeof (XmlSchemaContentProcessing_t2425104824)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1289[5] = 
{
	XmlSchemaContentProcessing_t2425104824::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1290 = { sizeof (XmlSchemaContentType_t2180219871)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1290[5] = 
{
	XmlSchemaContentType_t2180219871::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1291 = { sizeof (XmlSchemaDatatype_t196391954), -1, sizeof(XmlSchemaDatatype_t196391954_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1291[55] = 
{
	XmlSchemaDatatype_t196391954::get_offset_of_WhitespaceValue_0(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_wsChars_1(),
	XmlSchemaDatatype_t196391954::get_offset_of_sb_2(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeAnySimpleType_3(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeString_4(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeNormalizedString_5(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeToken_6(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeLanguage_7(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeNMToken_8(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeNMTokens_9(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeName_10(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeNCName_11(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeID_12(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeIDRef_13(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeIDRefs_14(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeEntity_15(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeEntities_16(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeNotation_17(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeDecimal_18(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeInteger_19(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeLong_20(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeInt_21(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeShort_22(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeByte_23(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeNonNegativeInteger_24(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypePositiveInteger_25(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeUnsignedLong_26(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeUnsignedInt_27(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeUnsignedShort_28(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeUnsignedByte_29(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeNonPositiveInteger_30(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeNegativeInteger_31(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeFloat_32(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeDouble_33(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeBase64Binary_34(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeBoolean_35(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeAnyURI_36(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeDuration_37(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeDateTime_38(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeDate_39(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeTime_40(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeHexBinary_41(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeQName_42(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeGYearMonth_43(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeGMonthDay_44(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeGYear_45(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeGMonth_46(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeGDay_47(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeAnyAtomicType_48(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeUntypedAtomic_49(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeDayTimeDuration_50(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeYearMonthDuration_51(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_U3CU3Ef__switchU24map3E_52(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_U3CU3Ef__switchU24map3F_53(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_U3CU3Ef__switchU24map40_54(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1292 = { sizeof (XmlSchemaDerivationMethod_t998439334)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1292[9] = 
{
	XmlSchemaDerivationMethod_t998439334::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1293 = { sizeof (XmlSchemaDocumentation_t3797104518), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1293[3] = 
{
	XmlSchemaDocumentation_t3797104518::get_offset_of_language_13(),
	XmlSchemaDocumentation_t3797104518::get_offset_of_markup_14(),
	XmlSchemaDocumentation_t3797104518::get_offset_of_source_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1294 = { sizeof (XmlSchemaElement_t3664762632), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1294[27] = 
{
	XmlSchemaElement_t3664762632::get_offset_of_block_27(),
	XmlSchemaElement_t3664762632::get_offset_of_constraints_28(),
	XmlSchemaElement_t3664762632::get_offset_of_defaultValue_29(),
	XmlSchemaElement_t3664762632::get_offset_of_elementType_30(),
	XmlSchemaElement_t3664762632::get_offset_of_elementSchemaType_31(),
	XmlSchemaElement_t3664762632::get_offset_of_final_32(),
	XmlSchemaElement_t3664762632::get_offset_of_fixedValue_33(),
	XmlSchemaElement_t3664762632::get_offset_of_form_34(),
	XmlSchemaElement_t3664762632::get_offset_of_isAbstract_35(),
	XmlSchemaElement_t3664762632::get_offset_of_isNillable_36(),
	XmlSchemaElement_t3664762632::get_offset_of_name_37(),
	XmlSchemaElement_t3664762632::get_offset_of_refName_38(),
	XmlSchemaElement_t3664762632::get_offset_of_schemaType_39(),
	XmlSchemaElement_t3664762632::get_offset_of_schemaTypeName_40(),
	XmlSchemaElement_t3664762632::get_offset_of_substitutionGroup_41(),
	XmlSchemaElement_t3664762632::get_offset_of_schema_42(),
	XmlSchemaElement_t3664762632::get_offset_of_parentIsSchema_43(),
	XmlSchemaElement_t3664762632::get_offset_of_qName_44(),
	XmlSchemaElement_t3664762632::get_offset_of_blockResolved_45(),
	XmlSchemaElement_t3664762632::get_offset_of_finalResolved_46(),
	XmlSchemaElement_t3664762632::get_offset_of_referencedElement_47(),
	XmlSchemaElement_t3664762632::get_offset_of_substitutingElements_48(),
	XmlSchemaElement_t3664762632::get_offset_of_substitutionGroupElement_49(),
	XmlSchemaElement_t3664762632::get_offset_of_actualIsAbstract_50(),
	XmlSchemaElement_t3664762632::get_offset_of_actualIsNillable_51(),
	XmlSchemaElement_t3664762632::get_offset_of_validatedDefaultValue_52(),
	XmlSchemaElement_t3664762632::get_offset_of_validatedFixedValue_53(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1295 = { sizeof (XmlSchemaEnumerationFacet_t1065605342), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1296 = { sizeof (XmlSchemaException_t78171291), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1296[5] = 
{
	XmlSchemaException_t78171291::get_offset_of_hasLineInfo_11(),
	XmlSchemaException_t78171291::get_offset_of_lineNumber_12(),
	XmlSchemaException_t78171291::get_offset_of_linePosition_13(),
	XmlSchemaException_t78171291::get_offset_of_sourceObj_14(),
	XmlSchemaException_t78171291::get_offset_of_sourceUri_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1297 = { sizeof (XmlSchemaExternal_t880573945), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1297[3] = 
{
	XmlSchemaExternal_t880573945::get_offset_of_id_13(),
	XmlSchemaExternal_t880573945::get_offset_of_schema_14(),
	XmlSchemaExternal_t880573945::get_offset_of_location_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1298 = { sizeof (XmlSchemaFacet_t2982631811), -1, sizeof(XmlSchemaFacet_t2982631811_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1298[3] = 
{
	XmlSchemaFacet_t2982631811_StaticFields::get_offset_of_AllFacets_16(),
	XmlSchemaFacet_t2982631811::get_offset_of_isFixed_17(),
	XmlSchemaFacet_t2982631811::get_offset_of_val_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1299 = { sizeof (Facet_t3197530270)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1299[14] = 
{
	Facet_t3197530270::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
