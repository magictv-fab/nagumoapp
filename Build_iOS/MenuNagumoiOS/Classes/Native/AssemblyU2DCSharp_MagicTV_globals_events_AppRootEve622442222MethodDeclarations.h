﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.globals.events.AppRootEvents/OnVoidEventHandler
struct OnVoidEventHandler_t622442222;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void MagicTV.globals.events.AppRootEvents/OnVoidEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnVoidEventHandler__ctor_m159672773 (OnVoidEventHandler_t622442222 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.AppRootEvents/OnVoidEventHandler::Invoke()
extern "C"  void OnVoidEventHandler_Invoke_m1710703199 (OnVoidEventHandler_t622442222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult MagicTV.globals.events.AppRootEvents/OnVoidEventHandler::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnVoidEventHandler_BeginInvoke_m1293400140 (OnVoidEventHandler_t622442222 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.AppRootEvents/OnVoidEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnVoidEventHandler_EndInvoke_m355829333 (OnVoidEventHandler_t622442222 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
