﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Action
struct Action_t3771233898;
// System.Object
struct Il2CppObject;
// System.Action`1<System.Boolean>
struct Action_1_t872614854;
// RenderListenerUtility
struct RenderListenerUtility_t2682722690;
// TBEasyWebCam.CallBack.EasyWebCamStartedDelegate
struct EasyWebCamStartedDelegate_t3886828347;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;
// TBEasyWebCam.CallBack.EasyWebCamStopedDelegate
struct EasyWebCamStopedDelegate_t2264447425;
// TBEasyWebCam.CallBack.EasyWebCamUpdateDelegate
struct EasyWebCamUpdateDelegate_t1731309161;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "EasyWebCamLib_U3CModuleU3E86524790.h"
#include "EasyWebCamLib_U3CModuleU3E86524790MethodDeclarations.h"
#include "EasyWebCamLib_RenderListenerUtility2682722690.h"
#include "EasyWebCamLib_RenderListenerUtility2682722690MethodDeclarations.h"
#include "System_Core_System_Action3771233898.h"
#include "mscorlib_System_Void2863195528.h"
#include "mscorlib_System_Delegate3310234105MethodDeclarations.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "mscorlib_System_Threading_Interlocked373807572MethodDeclarations.h"
#include "mscorlib_System_Threading_Interlocked373807572.h"
#include "mscorlib_System_Action_1_gen872614854.h"
#include "UnityEngine_UnityEngine_GameObject3674682005MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_Action_1_gen872614854MethodDeclarations.h"
#include "System_Core_System_Action3771233898MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552MethodDeclarations.h"
#include "EasyWebCamLib_TBEasyWebCam_CallBack_EasyWebCamStar3886828347.h"
#include "EasyWebCamLib_TBEasyWebCam_CallBack_EasyWebCamStar3886828347MethodDeclarations.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"
#include "EasyWebCamLib_TBEasyWebCam_CallBack_EasyWebCamStop2264447425.h"
#include "EasyWebCamLib_TBEasyWebCam_CallBack_EasyWebCamStop2264447425MethodDeclarations.h"
#include "EasyWebCamLib_TBEasyWebCam_CallBack_EasyWebCamUpda1731309161.h"
#include "EasyWebCamLib_TBEasyWebCam_CallBack_EasyWebCamUpda1731309161MethodDeclarations.h"
#include "EasyWebCamLib_TBEasyWebCam_EasyWebCamBase3084723642.h"
#include "EasyWebCamLib_TBEasyWebCam_EasyWebCamBase3084723642MethodDeclarations.h"
#include "EasyWebCamLib_TBEasyWebCam_Setting_FlashMode142630673.h"
#include "EasyWebCamLib_TBEasyWebCam_Setting_FlashMode142630673MethodDeclarations.h"
#include "EasyWebCamLib_TBEasyWebCam_Setting_FocusMode2918518777.h"
#include "EasyWebCamLib_TBEasyWebCam_Setting_FocusMode2918518777MethodDeclarations.h"
#include "EasyWebCamLib_TBEasyWebCam_Setting_ResolutionMode805264687.h"
#include "EasyWebCamLib_TBEasyWebCam_Setting_ResolutionMode805264687MethodDeclarations.h"
#include "EasyWebCamLib_TBEasyWebCam_Setting_TorchMode3286129949.h"
#include "EasyWebCamLib_TBEasyWebCam_Setting_TorchMode3286129949MethodDeclarations.h"
#include "EasyWebCamLib_TBEasyWebCam_Setting_Utilities2464936040.h"
#include "EasyWebCamLib_TBEasyWebCam_Setting_Utilities2464936040MethodDeclarations.h"
#include "mscorlib_System_Int321153838500.h"

// !!0 System.Threading.Interlocked::CompareExchange<System.Object>(!!0&,!!0,!!0)
extern "C"  Il2CppObject * Interlocked_CompareExchange_TisIl2CppObject_m1732656505_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject ** p0, Il2CppObject * p1, Il2CppObject * p2, const MethodInfo* method);
#define Interlocked_CompareExchange_TisIl2CppObject_m1732656505(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject **, Il2CppObject *, Il2CppObject *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m1732656505_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<System.Action>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisAction_t3771233898_m939865506(__this /* static, unused */, p0, p1, p2, method) ((  Action_t3771233898 * (*) (Il2CppObject * /* static, unused */, Action_t3771233898 **, Action_t3771233898 *, Action_t3771233898 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m1732656505_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<System.Action`1<System.Boolean>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisAction_1_t872614854_m4064329240(__this /* static, unused */, p0, p1, p2, method) ((  Action_1_t872614854 * (*) (Il2CppObject * /* static, unused */, Action_1_t872614854 **, Action_1_t872614854 *, Action_1_t872614854 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m1732656505_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m955200616_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m955200616(__this, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m955200616_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<RenderListenerUtility>()
#define GameObject_AddComponent_TisRenderListenerUtility_t2682722690_m2551169348(__this, method) ((  RenderListenerUtility_t2682722690 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m955200616_gshared)(__this, method)
// !!0 System.Threading.Interlocked::CompareExchange<TBEasyWebCam.CallBack.EasyWebCamStartedDelegate>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisEasyWebCamStartedDelegate_t3886828347_m3253195604(__this /* static, unused */, p0, p1, p2, method) ((  EasyWebCamStartedDelegate_t3886828347 * (*) (Il2CppObject * /* static, unused */, EasyWebCamStartedDelegate_t3886828347 **, EasyWebCamStartedDelegate_t3886828347 *, EasyWebCamStartedDelegate_t3886828347 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m1732656505_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<TBEasyWebCam.CallBack.EasyWebCamUpdateDelegate>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisEasyWebCamUpdateDelegate_t1731309161_m3488613306(__this /* static, unused */, p0, p1, p2, method) ((  EasyWebCamUpdateDelegate_t1731309161 * (*) (Il2CppObject * /* static, unused */, EasyWebCamUpdateDelegate_t1731309161 **, EasyWebCamUpdateDelegate_t1731309161 *, EasyWebCamUpdateDelegate_t1731309161 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m1732656505_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<TBEasyWebCam.CallBack.EasyWebCamStopedDelegate>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisEasyWebCamStopedDelegate_t2264447425_m401988962(__this /* static, unused */, p0, p1, p2, method) ((  EasyWebCamStopedDelegate_t2264447425 * (*) (Il2CppObject * /* static, unused */, EasyWebCamStopedDelegate_t2264447425 **, EasyWebCamStopedDelegate_t2264447425 *, EasyWebCamStopedDelegate_t2264447425 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m1732656505_gshared)(__this /* static, unused */, p0, p1, p2, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RenderListenerUtility::add_onQuit(System.Action)
extern Il2CppClass* RenderListenerUtility_t2682722690_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const uint32_t RenderListenerUtility_add_onQuit_m1222160124_MetadataUsageId;
extern "C"  void RenderListenerUtility_add_onQuit_m1222160124 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RenderListenerUtility_add_onQuit_m1222160124_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_t3771233898 * V_0 = NULL;
	Action_t3771233898 * V_1 = NULL;
	Action_t3771233898 * V_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(RenderListenerUtility_t2682722690_il2cpp_TypeInfo_var);
		Action_t3771233898 * L_0 = ((RenderListenerUtility_t2682722690_StaticFields*)RenderListenerUtility_t2682722690_il2cpp_TypeInfo_var->static_fields)->get_onQuit_2();
		V_0 = L_0;
	}

IL_0006:
	{
		Action_t3771233898 * L_1 = V_0;
		V_1 = L_1;
		Action_t3771233898 * L_2 = V_1;
		Action_t3771233898 * L_3 = ___value0;
		Delegate_t3310234105 * L_4 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Action_t3771233898 *)CastclassSealed(L_4, Action_t3771233898_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(RenderListenerUtility_t2682722690_il2cpp_TypeInfo_var);
		Action_t3771233898 * L_5 = V_2;
		Action_t3771233898 * L_6 = V_1;
		Action_t3771233898 * L_7 = InterlockedCompareExchangeImpl<Action_t3771233898 *>((((RenderListenerUtility_t2682722690_StaticFields*)RenderListenerUtility_t2682722690_il2cpp_TypeInfo_var->static_fields)->get_address_of_onQuit_2()), L_5, L_6);
		V_0 = L_7;
		Action_t3771233898 * L_8 = V_0;
		Action_t3771233898 * L_9 = V_1;
		if ((!(((Il2CppObject*)(Action_t3771233898 *)L_8) == ((Il2CppObject*)(Action_t3771233898 *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void RenderListenerUtility::remove_onQuit(System.Action)
extern Il2CppClass* RenderListenerUtility_t2682722690_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const uint32_t RenderListenerUtility_remove_onQuit_m2761031175_MetadataUsageId;
extern "C"  void RenderListenerUtility_remove_onQuit_m2761031175 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RenderListenerUtility_remove_onQuit_m2761031175_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_t3771233898 * V_0 = NULL;
	Action_t3771233898 * V_1 = NULL;
	Action_t3771233898 * V_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(RenderListenerUtility_t2682722690_il2cpp_TypeInfo_var);
		Action_t3771233898 * L_0 = ((RenderListenerUtility_t2682722690_StaticFields*)RenderListenerUtility_t2682722690_il2cpp_TypeInfo_var->static_fields)->get_onQuit_2();
		V_0 = L_0;
	}

IL_0006:
	{
		Action_t3771233898 * L_1 = V_0;
		V_1 = L_1;
		Action_t3771233898 * L_2 = V_1;
		Action_t3771233898 * L_3 = ___value0;
		Delegate_t3310234105 * L_4 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Action_t3771233898 *)CastclassSealed(L_4, Action_t3771233898_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(RenderListenerUtility_t2682722690_il2cpp_TypeInfo_var);
		Action_t3771233898 * L_5 = V_2;
		Action_t3771233898 * L_6 = V_1;
		Action_t3771233898 * L_7 = InterlockedCompareExchangeImpl<Action_t3771233898 *>((((RenderListenerUtility_t2682722690_StaticFields*)RenderListenerUtility_t2682722690_il2cpp_TypeInfo_var->static_fields)->get_address_of_onQuit_2()), L_5, L_6);
		V_0 = L_7;
		Action_t3771233898 * L_8 = V_0;
		Action_t3771233898 * L_9 = V_1;
		if ((!(((Il2CppObject*)(Action_t3771233898 *)L_8) == ((Il2CppObject*)(Action_t3771233898 *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void RenderListenerUtility::add_onPause(System.Action`1<System.Boolean>)
extern Il2CppClass* RenderListenerUtility_t2682722690_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t872614854_il2cpp_TypeInfo_var;
extern const uint32_t RenderListenerUtility_add_onPause_m2565202413_MetadataUsageId;
extern "C"  void RenderListenerUtility_add_onPause_m2565202413 (Il2CppObject * __this /* static, unused */, Action_1_t872614854 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RenderListenerUtility_add_onPause_m2565202413_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t872614854 * V_0 = NULL;
	Action_1_t872614854 * V_1 = NULL;
	Action_1_t872614854 * V_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(RenderListenerUtility_t2682722690_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_0 = ((RenderListenerUtility_t2682722690_StaticFields*)RenderListenerUtility_t2682722690_il2cpp_TypeInfo_var->static_fields)->get_onPause_3();
		V_0 = L_0;
	}

IL_0006:
	{
		Action_1_t872614854 * L_1 = V_0;
		V_1 = L_1;
		Action_1_t872614854 * L_2 = V_1;
		Action_1_t872614854 * L_3 = ___value0;
		Delegate_t3310234105 * L_4 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Action_1_t872614854 *)CastclassSealed(L_4, Action_1_t872614854_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(RenderListenerUtility_t2682722690_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_5 = V_2;
		Action_1_t872614854 * L_6 = V_1;
		Action_1_t872614854 * L_7 = InterlockedCompareExchangeImpl<Action_1_t872614854 *>((((RenderListenerUtility_t2682722690_StaticFields*)RenderListenerUtility_t2682722690_il2cpp_TypeInfo_var->static_fields)->get_address_of_onPause_3()), L_5, L_6);
		V_0 = L_7;
		Action_1_t872614854 * L_8 = V_0;
		Action_1_t872614854 * L_9 = V_1;
		if ((!(((Il2CppObject*)(Action_1_t872614854 *)L_8) == ((Il2CppObject*)(Action_1_t872614854 *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void RenderListenerUtility::remove_onPause(System.Action`1<System.Boolean>)
extern Il2CppClass* RenderListenerUtility_t2682722690_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t872614854_il2cpp_TypeInfo_var;
extern const uint32_t RenderListenerUtility_remove_onPause_m766011650_MetadataUsageId;
extern "C"  void RenderListenerUtility_remove_onPause_m766011650 (Il2CppObject * __this /* static, unused */, Action_1_t872614854 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RenderListenerUtility_remove_onPause_m766011650_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t872614854 * V_0 = NULL;
	Action_1_t872614854 * V_1 = NULL;
	Action_1_t872614854 * V_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(RenderListenerUtility_t2682722690_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_0 = ((RenderListenerUtility_t2682722690_StaticFields*)RenderListenerUtility_t2682722690_il2cpp_TypeInfo_var->static_fields)->get_onPause_3();
		V_0 = L_0;
	}

IL_0006:
	{
		Action_1_t872614854 * L_1 = V_0;
		V_1 = L_1;
		Action_1_t872614854 * L_2 = V_1;
		Action_1_t872614854 * L_3 = ___value0;
		Delegate_t3310234105 * L_4 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Action_1_t872614854 *)CastclassSealed(L_4, Action_1_t872614854_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(RenderListenerUtility_t2682722690_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_5 = V_2;
		Action_1_t872614854 * L_6 = V_1;
		Action_1_t872614854 * L_7 = InterlockedCompareExchangeImpl<Action_1_t872614854 *>((((RenderListenerUtility_t2682722690_StaticFields*)RenderListenerUtility_t2682722690_il2cpp_TypeInfo_var->static_fields)->get_address_of_onPause_3()), L_5, L_6);
		V_0 = L_7;
		Action_1_t872614854 * L_8 = V_0;
		Action_1_t872614854 * L_9 = V_1;
		if ((!(((Il2CppObject*)(Action_1_t872614854 *)L_8) == ((Il2CppObject*)(Action_1_t872614854 *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void RenderListenerUtility::.cctor()
extern Il2CppClass* GameObject_t3674682005_il2cpp_TypeInfo_var;
extern Il2CppClass* RenderListenerUtility_t2682722690_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisRenderListenerUtility_t2682722690_m2551169348_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4087825110;
extern const uint32_t RenderListenerUtility__cctor_m720909886_MetadataUsageId;
extern "C"  void RenderListenerUtility__cctor_m720909886 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RenderListenerUtility__cctor_m720909886_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = (GameObject_t3674682005 *)il2cpp_codegen_object_new(GameObject_t3674682005_il2cpp_TypeInfo_var);
		GameObject__ctor_m3920833606(L_0, _stringLiteral4087825110, /*hidden argument*/NULL);
		NullCheck(L_0);
		RenderListenerUtility_t2682722690 * L_1 = GameObject_AddComponent_TisRenderListenerUtility_t2682722690_m2551169348(L_0, /*hidden argument*/GameObject_AddComponent_TisRenderListenerUtility_t2682722690_m2551169348_MethodInfo_var);
		((RenderListenerUtility_t2682722690_StaticFields*)RenderListenerUtility_t2682722690_il2cpp_TypeInfo_var->static_fields)->set_instance_4(L_1);
		return;
	}
}
// System.Void RenderListenerUtility::Start()
extern "C"  void RenderListenerUtility_Start_m1895438415 (RenderListenerUtility_t2682722690 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void RenderListenerUtility::Update()
extern "C"  void RenderListenerUtility_Update_m2929868190 (RenderListenerUtility_t2682722690 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void RenderListenerUtility::OnApplicationPause(System.Boolean)
extern Il2CppClass* RenderListenerUtility_t2682722690_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const uint32_t RenderListenerUtility_OnApplicationPause_m1365855825_MetadataUsageId;
extern "C"  void RenderListenerUtility_OnApplicationPause_m1365855825 (RenderListenerUtility_t2682722690 * __this, bool ___paused0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RenderListenerUtility_OnApplicationPause_m1365855825_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(RenderListenerUtility_t2682722690_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_0 = ((RenderListenerUtility_t2682722690_StaticFields*)RenderListenerUtility_t2682722690_il2cpp_TypeInfo_var->static_fields)->get_onPause_3();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(RenderListenerUtility_t2682722690_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_1 = ((RenderListenerUtility_t2682722690_StaticFields*)RenderListenerUtility_t2682722690_il2cpp_TypeInfo_var->static_fields)->get_onPause_3();
		bool L_2 = ___paused0;
		NullCheck(L_1);
		Action_1_Invoke_m3594021162(L_1, L_2, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
	}

IL_0012:
	{
		return;
	}
}
// System.Void RenderListenerUtility::OnApplicationQuit()
extern Il2CppClass* RenderListenerUtility_t2682722690_il2cpp_TypeInfo_var;
extern const uint32_t RenderListenerUtility_OnApplicationQuit_m1995206221_MetadataUsageId;
extern "C"  void RenderListenerUtility_OnApplicationQuit_m1995206221 (RenderListenerUtility_t2682722690 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RenderListenerUtility_OnApplicationQuit_m1995206221_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(RenderListenerUtility_t2682722690_il2cpp_TypeInfo_var);
		Action_t3771233898 * L_0 = ((RenderListenerUtility_t2682722690_StaticFields*)RenderListenerUtility_t2682722690_il2cpp_TypeInfo_var->static_fields)->get_onQuit_2();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(RenderListenerUtility_t2682722690_il2cpp_TypeInfo_var);
		Action_t3771233898 * L_1 = ((RenderListenerUtility_t2682722690_StaticFields*)RenderListenerUtility_t2682722690_il2cpp_TypeInfo_var->static_fields)->get_onQuit_2();
		NullCheck(L_1);
		Action_Invoke_m1445970038(L_1, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void RenderListenerUtility::.ctor()
extern "C"  void RenderListenerUtility__ctor_m2948300623 (RenderListenerUtility_t2682722690 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TBEasyWebCam.CallBack.EasyWebCamStartedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void EasyWebCamStartedDelegate__ctor_m2221384916 (EasyWebCamStartedDelegate_t3886828347 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void TBEasyWebCam.CallBack.EasyWebCamStartedDelegate::Invoke()
extern "C"  void EasyWebCamStartedDelegate_Invoke_m3553276462 (EasyWebCamStartedDelegate_t3886828347 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		EasyWebCamStartedDelegate_Invoke_m3553276462((EasyWebCamStartedDelegate_t3886828347 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_EasyWebCamStartedDelegate_t3886828347 (EasyWebCamStartedDelegate_t3886828347 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.IAsyncResult TBEasyWebCam.CallBack.EasyWebCamStartedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * EasyWebCamStartedDelegate_BeginInvoke_m2203446173 (EasyWebCamStartedDelegate_t3886828347 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void TBEasyWebCam.CallBack.EasyWebCamStartedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void EasyWebCamStartedDelegate_EndInvoke_m2455150820 (EasyWebCamStartedDelegate_t3886828347 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void TBEasyWebCam.CallBack.EasyWebCamStopedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void EasyWebCamStopedDelegate__ctor_m4247637174 (EasyWebCamStopedDelegate_t2264447425 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void TBEasyWebCam.CallBack.EasyWebCamStopedDelegate::Invoke()
extern "C"  void EasyWebCamStopedDelegate_Invoke_m4130992272 (EasyWebCamStopedDelegate_t2264447425 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		EasyWebCamStopedDelegate_Invoke_m4130992272((EasyWebCamStopedDelegate_t2264447425 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_EasyWebCamStopedDelegate_t2264447425 (EasyWebCamStopedDelegate_t2264447425 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.IAsyncResult TBEasyWebCam.CallBack.EasyWebCamStopedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * EasyWebCamStopedDelegate_BeginInvoke_m2210574195 (EasyWebCamStopedDelegate_t2264447425 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void TBEasyWebCam.CallBack.EasyWebCamStopedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void EasyWebCamStopedDelegate_EndInvoke_m295700934 (EasyWebCamStopedDelegate_t2264447425 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void TBEasyWebCam.CallBack.EasyWebCamUpdateDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void EasyWebCamUpdateDelegate__ctor_m4246703454 (EasyWebCamUpdateDelegate_t1731309161 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void TBEasyWebCam.CallBack.EasyWebCamUpdateDelegate::Invoke()
extern "C"  void EasyWebCamUpdateDelegate_Invoke_m2699506488 (EasyWebCamUpdateDelegate_t1731309161 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		EasyWebCamUpdateDelegate_Invoke_m2699506488((EasyWebCamUpdateDelegate_t1731309161 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_EasyWebCamUpdateDelegate_t1731309161 (EasyWebCamUpdateDelegate_t1731309161 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.IAsyncResult TBEasyWebCam.CallBack.EasyWebCamUpdateDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * EasyWebCamUpdateDelegate_BeginInvoke_m1144340427 (EasyWebCamUpdateDelegate_t1731309161 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void TBEasyWebCam.CallBack.EasyWebCamUpdateDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void EasyWebCamUpdateDelegate_EndInvoke_m1184652910 (EasyWebCamUpdateDelegate_t1731309161 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void TBEasyWebCam.EasyWebCamBase::add_onEasyWebCamStart(TBEasyWebCam.CallBack.EasyWebCamStartedDelegate)
extern Il2CppClass* EasyWebCamBase_t3084723642_il2cpp_TypeInfo_var;
extern Il2CppClass* EasyWebCamStartedDelegate_t3886828347_il2cpp_TypeInfo_var;
extern const uint32_t EasyWebCamBase_add_onEasyWebCamStart_m3076024489_MetadataUsageId;
extern "C"  void EasyWebCamBase_add_onEasyWebCamStart_m3076024489 (Il2CppObject * __this /* static, unused */, EasyWebCamStartedDelegate_t3886828347 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EasyWebCamBase_add_onEasyWebCamStart_m3076024489_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EasyWebCamStartedDelegate_t3886828347 * V_0 = NULL;
	EasyWebCamStartedDelegate_t3886828347 * V_1 = NULL;
	EasyWebCamStartedDelegate_t3886828347 * V_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(EasyWebCamBase_t3084723642_il2cpp_TypeInfo_var);
		EasyWebCamStartedDelegate_t3886828347 * L_0 = ((EasyWebCamBase_t3084723642_StaticFields*)EasyWebCamBase_t3084723642_il2cpp_TypeInfo_var->static_fields)->get_onEasyWebCamStart_0();
		V_0 = L_0;
	}

IL_0006:
	{
		EasyWebCamStartedDelegate_t3886828347 * L_1 = V_0;
		V_1 = L_1;
		EasyWebCamStartedDelegate_t3886828347 * L_2 = V_1;
		EasyWebCamStartedDelegate_t3886828347 * L_3 = ___value0;
		Delegate_t3310234105 * L_4 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EasyWebCamStartedDelegate_t3886828347 *)CastclassSealed(L_4, EasyWebCamStartedDelegate_t3886828347_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(EasyWebCamBase_t3084723642_il2cpp_TypeInfo_var);
		EasyWebCamStartedDelegate_t3886828347 * L_5 = V_2;
		EasyWebCamStartedDelegate_t3886828347 * L_6 = V_1;
		EasyWebCamStartedDelegate_t3886828347 * L_7 = InterlockedCompareExchangeImpl<EasyWebCamStartedDelegate_t3886828347 *>((((EasyWebCamBase_t3084723642_StaticFields*)EasyWebCamBase_t3084723642_il2cpp_TypeInfo_var->static_fields)->get_address_of_onEasyWebCamStart_0()), L_5, L_6);
		V_0 = L_7;
		EasyWebCamStartedDelegate_t3886828347 * L_8 = V_0;
		EasyWebCamStartedDelegate_t3886828347 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EasyWebCamStartedDelegate_t3886828347 *)L_8) == ((Il2CppObject*)(EasyWebCamStartedDelegate_t3886828347 *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void TBEasyWebCam.EasyWebCamBase::remove_onEasyWebCamStart(TBEasyWebCam.CallBack.EasyWebCamStartedDelegate)
extern Il2CppClass* EasyWebCamBase_t3084723642_il2cpp_TypeInfo_var;
extern Il2CppClass* EasyWebCamStartedDelegate_t3886828347_il2cpp_TypeInfo_var;
extern const uint32_t EasyWebCamBase_remove_onEasyWebCamStart_m1528401280_MetadataUsageId;
extern "C"  void EasyWebCamBase_remove_onEasyWebCamStart_m1528401280 (Il2CppObject * __this /* static, unused */, EasyWebCamStartedDelegate_t3886828347 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EasyWebCamBase_remove_onEasyWebCamStart_m1528401280_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EasyWebCamStartedDelegate_t3886828347 * V_0 = NULL;
	EasyWebCamStartedDelegate_t3886828347 * V_1 = NULL;
	EasyWebCamStartedDelegate_t3886828347 * V_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(EasyWebCamBase_t3084723642_il2cpp_TypeInfo_var);
		EasyWebCamStartedDelegate_t3886828347 * L_0 = ((EasyWebCamBase_t3084723642_StaticFields*)EasyWebCamBase_t3084723642_il2cpp_TypeInfo_var->static_fields)->get_onEasyWebCamStart_0();
		V_0 = L_0;
	}

IL_0006:
	{
		EasyWebCamStartedDelegate_t3886828347 * L_1 = V_0;
		V_1 = L_1;
		EasyWebCamStartedDelegate_t3886828347 * L_2 = V_1;
		EasyWebCamStartedDelegate_t3886828347 * L_3 = ___value0;
		Delegate_t3310234105 * L_4 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EasyWebCamStartedDelegate_t3886828347 *)CastclassSealed(L_4, EasyWebCamStartedDelegate_t3886828347_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(EasyWebCamBase_t3084723642_il2cpp_TypeInfo_var);
		EasyWebCamStartedDelegate_t3886828347 * L_5 = V_2;
		EasyWebCamStartedDelegate_t3886828347 * L_6 = V_1;
		EasyWebCamStartedDelegate_t3886828347 * L_7 = InterlockedCompareExchangeImpl<EasyWebCamStartedDelegate_t3886828347 *>((((EasyWebCamBase_t3084723642_StaticFields*)EasyWebCamBase_t3084723642_il2cpp_TypeInfo_var->static_fields)->get_address_of_onEasyWebCamStart_0()), L_5, L_6);
		V_0 = L_7;
		EasyWebCamStartedDelegate_t3886828347 * L_8 = V_0;
		EasyWebCamStartedDelegate_t3886828347 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EasyWebCamStartedDelegate_t3886828347 *)L_8) == ((Il2CppObject*)(EasyWebCamStartedDelegate_t3886828347 *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void TBEasyWebCam.EasyWebCamBase::add_OnEasyWebCamUpdate(TBEasyWebCam.CallBack.EasyWebCamUpdateDelegate)
extern Il2CppClass* EasyWebCamBase_t3084723642_il2cpp_TypeInfo_var;
extern Il2CppClass* EasyWebCamUpdateDelegate_t1731309161_il2cpp_TypeInfo_var;
extern const uint32_t EasyWebCamBase_add_OnEasyWebCamUpdate_m3667554550_MetadataUsageId;
extern "C"  void EasyWebCamBase_add_OnEasyWebCamUpdate_m3667554550 (Il2CppObject * __this /* static, unused */, EasyWebCamUpdateDelegate_t1731309161 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EasyWebCamBase_add_OnEasyWebCamUpdate_m3667554550_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EasyWebCamUpdateDelegate_t1731309161 * V_0 = NULL;
	EasyWebCamUpdateDelegate_t1731309161 * V_1 = NULL;
	EasyWebCamUpdateDelegate_t1731309161 * V_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(EasyWebCamBase_t3084723642_il2cpp_TypeInfo_var);
		EasyWebCamUpdateDelegate_t1731309161 * L_0 = ((EasyWebCamBase_t3084723642_StaticFields*)EasyWebCamBase_t3084723642_il2cpp_TypeInfo_var->static_fields)->get_OnEasyWebCamUpdate_1();
		V_0 = L_0;
	}

IL_0006:
	{
		EasyWebCamUpdateDelegate_t1731309161 * L_1 = V_0;
		V_1 = L_1;
		EasyWebCamUpdateDelegate_t1731309161 * L_2 = V_1;
		EasyWebCamUpdateDelegate_t1731309161 * L_3 = ___value0;
		Delegate_t3310234105 * L_4 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EasyWebCamUpdateDelegate_t1731309161 *)CastclassSealed(L_4, EasyWebCamUpdateDelegate_t1731309161_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(EasyWebCamBase_t3084723642_il2cpp_TypeInfo_var);
		EasyWebCamUpdateDelegate_t1731309161 * L_5 = V_2;
		EasyWebCamUpdateDelegate_t1731309161 * L_6 = V_1;
		EasyWebCamUpdateDelegate_t1731309161 * L_7 = InterlockedCompareExchangeImpl<EasyWebCamUpdateDelegate_t1731309161 *>((((EasyWebCamBase_t3084723642_StaticFields*)EasyWebCamBase_t3084723642_il2cpp_TypeInfo_var->static_fields)->get_address_of_OnEasyWebCamUpdate_1()), L_5, L_6);
		V_0 = L_7;
		EasyWebCamUpdateDelegate_t1731309161 * L_8 = V_0;
		EasyWebCamUpdateDelegate_t1731309161 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EasyWebCamUpdateDelegate_t1731309161 *)L_8) == ((Il2CppObject*)(EasyWebCamUpdateDelegate_t1731309161 *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void TBEasyWebCam.EasyWebCamBase::remove_OnEasyWebCamUpdate(TBEasyWebCam.CallBack.EasyWebCamUpdateDelegate)
extern Il2CppClass* EasyWebCamBase_t3084723642_il2cpp_TypeInfo_var;
extern Il2CppClass* EasyWebCamUpdateDelegate_t1731309161_il2cpp_TypeInfo_var;
extern const uint32_t EasyWebCamBase_remove_OnEasyWebCamUpdate_m2119931341_MetadataUsageId;
extern "C"  void EasyWebCamBase_remove_OnEasyWebCamUpdate_m2119931341 (Il2CppObject * __this /* static, unused */, EasyWebCamUpdateDelegate_t1731309161 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EasyWebCamBase_remove_OnEasyWebCamUpdate_m2119931341_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EasyWebCamUpdateDelegate_t1731309161 * V_0 = NULL;
	EasyWebCamUpdateDelegate_t1731309161 * V_1 = NULL;
	EasyWebCamUpdateDelegate_t1731309161 * V_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(EasyWebCamBase_t3084723642_il2cpp_TypeInfo_var);
		EasyWebCamUpdateDelegate_t1731309161 * L_0 = ((EasyWebCamBase_t3084723642_StaticFields*)EasyWebCamBase_t3084723642_il2cpp_TypeInfo_var->static_fields)->get_OnEasyWebCamUpdate_1();
		V_0 = L_0;
	}

IL_0006:
	{
		EasyWebCamUpdateDelegate_t1731309161 * L_1 = V_0;
		V_1 = L_1;
		EasyWebCamUpdateDelegate_t1731309161 * L_2 = V_1;
		EasyWebCamUpdateDelegate_t1731309161 * L_3 = ___value0;
		Delegate_t3310234105 * L_4 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EasyWebCamUpdateDelegate_t1731309161 *)CastclassSealed(L_4, EasyWebCamUpdateDelegate_t1731309161_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(EasyWebCamBase_t3084723642_il2cpp_TypeInfo_var);
		EasyWebCamUpdateDelegate_t1731309161 * L_5 = V_2;
		EasyWebCamUpdateDelegate_t1731309161 * L_6 = V_1;
		EasyWebCamUpdateDelegate_t1731309161 * L_7 = InterlockedCompareExchangeImpl<EasyWebCamUpdateDelegate_t1731309161 *>((((EasyWebCamBase_t3084723642_StaticFields*)EasyWebCamBase_t3084723642_il2cpp_TypeInfo_var->static_fields)->get_address_of_OnEasyWebCamUpdate_1()), L_5, L_6);
		V_0 = L_7;
		EasyWebCamUpdateDelegate_t1731309161 * L_8 = V_0;
		EasyWebCamUpdateDelegate_t1731309161 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EasyWebCamUpdateDelegate_t1731309161 *)L_8) == ((Il2CppObject*)(EasyWebCamUpdateDelegate_t1731309161 *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void TBEasyWebCam.EasyWebCamBase::add_OnEasyWebCamStoped(TBEasyWebCam.CallBack.EasyWebCamStopedDelegate)
extern Il2CppClass* EasyWebCamBase_t3084723642_il2cpp_TypeInfo_var;
extern Il2CppClass* EasyWebCamStopedDelegate_t2264447425_il2cpp_TypeInfo_var;
extern const uint32_t EasyWebCamBase_add_OnEasyWebCamStoped_m2036573174_MetadataUsageId;
extern "C"  void EasyWebCamBase_add_OnEasyWebCamStoped_m2036573174 (Il2CppObject * __this /* static, unused */, EasyWebCamStopedDelegate_t2264447425 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EasyWebCamBase_add_OnEasyWebCamStoped_m2036573174_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EasyWebCamStopedDelegate_t2264447425 * V_0 = NULL;
	EasyWebCamStopedDelegate_t2264447425 * V_1 = NULL;
	EasyWebCamStopedDelegate_t2264447425 * V_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(EasyWebCamBase_t3084723642_il2cpp_TypeInfo_var);
		EasyWebCamStopedDelegate_t2264447425 * L_0 = ((EasyWebCamBase_t3084723642_StaticFields*)EasyWebCamBase_t3084723642_il2cpp_TypeInfo_var->static_fields)->get_OnEasyWebCamStoped_2();
		V_0 = L_0;
	}

IL_0006:
	{
		EasyWebCamStopedDelegate_t2264447425 * L_1 = V_0;
		V_1 = L_1;
		EasyWebCamStopedDelegate_t2264447425 * L_2 = V_1;
		EasyWebCamStopedDelegate_t2264447425 * L_3 = ___value0;
		Delegate_t3310234105 * L_4 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EasyWebCamStopedDelegate_t2264447425 *)CastclassSealed(L_4, EasyWebCamStopedDelegate_t2264447425_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(EasyWebCamBase_t3084723642_il2cpp_TypeInfo_var);
		EasyWebCamStopedDelegate_t2264447425 * L_5 = V_2;
		EasyWebCamStopedDelegate_t2264447425 * L_6 = V_1;
		EasyWebCamStopedDelegate_t2264447425 * L_7 = InterlockedCompareExchangeImpl<EasyWebCamStopedDelegate_t2264447425 *>((((EasyWebCamBase_t3084723642_StaticFields*)EasyWebCamBase_t3084723642_il2cpp_TypeInfo_var->static_fields)->get_address_of_OnEasyWebCamStoped_2()), L_5, L_6);
		V_0 = L_7;
		EasyWebCamStopedDelegate_t2264447425 * L_8 = V_0;
		EasyWebCamStopedDelegate_t2264447425 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EasyWebCamStopedDelegate_t2264447425 *)L_8) == ((Il2CppObject*)(EasyWebCamStopedDelegate_t2264447425 *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void TBEasyWebCam.EasyWebCamBase::remove_OnEasyWebCamStoped(TBEasyWebCam.CallBack.EasyWebCamStopedDelegate)
extern Il2CppClass* EasyWebCamBase_t3084723642_il2cpp_TypeInfo_var;
extern Il2CppClass* EasyWebCamStopedDelegate_t2264447425_il2cpp_TypeInfo_var;
extern const uint32_t EasyWebCamBase_remove_OnEasyWebCamStoped_m488949965_MetadataUsageId;
extern "C"  void EasyWebCamBase_remove_OnEasyWebCamStoped_m488949965 (Il2CppObject * __this /* static, unused */, EasyWebCamStopedDelegate_t2264447425 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EasyWebCamBase_remove_OnEasyWebCamStoped_m488949965_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EasyWebCamStopedDelegate_t2264447425 * V_0 = NULL;
	EasyWebCamStopedDelegate_t2264447425 * V_1 = NULL;
	EasyWebCamStopedDelegate_t2264447425 * V_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(EasyWebCamBase_t3084723642_il2cpp_TypeInfo_var);
		EasyWebCamStopedDelegate_t2264447425 * L_0 = ((EasyWebCamBase_t3084723642_StaticFields*)EasyWebCamBase_t3084723642_il2cpp_TypeInfo_var->static_fields)->get_OnEasyWebCamStoped_2();
		V_0 = L_0;
	}

IL_0006:
	{
		EasyWebCamStopedDelegate_t2264447425 * L_1 = V_0;
		V_1 = L_1;
		EasyWebCamStopedDelegate_t2264447425 * L_2 = V_1;
		EasyWebCamStopedDelegate_t2264447425 * L_3 = ___value0;
		Delegate_t3310234105 * L_4 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EasyWebCamStopedDelegate_t2264447425 *)CastclassSealed(L_4, EasyWebCamStopedDelegate_t2264447425_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(EasyWebCamBase_t3084723642_il2cpp_TypeInfo_var);
		EasyWebCamStopedDelegate_t2264447425 * L_5 = V_2;
		EasyWebCamStopedDelegate_t2264447425 * L_6 = V_1;
		EasyWebCamStopedDelegate_t2264447425 * L_7 = InterlockedCompareExchangeImpl<EasyWebCamStopedDelegate_t2264447425 *>((((EasyWebCamBase_t3084723642_StaticFields*)EasyWebCamBase_t3084723642_il2cpp_TypeInfo_var->static_fields)->get_address_of_OnEasyWebCamStoped_2()), L_5, L_6);
		V_0 = L_7;
		EasyWebCamStopedDelegate_t2264447425 * L_8 = V_0;
		EasyWebCamStopedDelegate_t2264447425 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EasyWebCamStopedDelegate_t2264447425 *)L_8) == ((Il2CppObject*)(EasyWebCamStopedDelegate_t2264447425 *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void TBEasyWebCam.EasyWebCamBase::.cctor()
extern Il2CppClass* EasyWebCamBase_t3084723642_il2cpp_TypeInfo_var;
extern const uint32_t EasyWebCamBase__cctor_m691467341_MetadataUsageId;
extern "C"  void EasyWebCamBase__cctor_m691467341 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EasyWebCamBase__cctor_m691467341_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((EasyWebCamBase_t3084723642_StaticFields*)EasyWebCamBase_t3084723642_il2cpp_TypeInfo_var->static_fields)->set_isRunning_3((bool)0);
		return;
	}
}
// System.Void TBEasyWebCam.Setting.Utilities::Dimensions(TBEasyWebCam.Setting.ResolutionMode,System.Int32&,System.Int32&)
extern "C"  void Utilities_Dimensions_m3724800167 (Il2CppObject * __this /* static, unused */, uint8_t ___preset0, int32_t* ___width1, int32_t* ___height2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	uint8_t V_1 = 0;
	{
		int32_t* L_0 = ___width1;
		int32_t* L_1 = ___height2;
		int32_t L_2 = ((int32_t)600);
		V_0 = L_2;
		*((int32_t*)(L_1)) = (int32_t)L_2;
		int32_t L_3 = V_0;
		*((int32_t*)(L_0)) = (int32_t)L_3;
		uint8_t L_4 = ___preset0;
		V_1 = L_4;
		uint8_t L_5 = V_1;
		if (((int32_t)((int32_t)L_5-(int32_t)1)) == 0)
		{
			goto IL_0030;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)1)) == 1)
		{
			goto IL_003f;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)1)) == 2)
		{
			goto IL_0074;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)1)) == 3)
		{
			goto IL_004e;
		}
	}
	{
		uint8_t L_6 = V_1;
		if ((((int32_t)L_6) == ((int32_t)8)))
		{
			goto IL_005d;
		}
	}
	{
		uint8_t L_7 = V_1;
		if ((((int32_t)L_7) == ((int32_t)((int32_t)16))))
		{
			goto IL_006c;
		}
	}
	{
		return;
	}

IL_0030:
	{
		int32_t* L_8 = ___width1;
		*((int32_t*)(L_8)) = (int32_t)((int32_t)1280);
		int32_t* L_9 = ___height2;
		*((int32_t*)(L_9)) = (int32_t)((int32_t)720);
		return;
	}

IL_003f:
	{
		int32_t* L_10 = ___width1;
		*((int32_t*)(L_10)) = (int32_t)((int32_t)1920);
		int32_t* L_11 = ___height2;
		*((int32_t*)(L_11)) = (int32_t)((int32_t)1080);
		return;
	}

IL_004e:
	{
		int32_t* L_12 = ___width1;
		*((int32_t*)(L_12)) = (int32_t)((int32_t)1920);
		int32_t* L_13 = ___height2;
		*((int32_t*)(L_13)) = (int32_t)((int32_t)1080);
		return;
	}

IL_005d:
	{
		int32_t* L_14 = ___width1;
		*((int32_t*)(L_14)) = (int32_t)((int32_t)640);
		int32_t* L_15 = ___height2;
		*((int32_t*)(L_15)) = (int32_t)((int32_t)480);
		return;
	}

IL_006c:
	{
		int32_t* L_16 = ___width1;
		*((int32_t*)(L_16)) = (int32_t)((int32_t)50);
		int32_t* L_17 = ___height2;
		*((int32_t*)(L_17)) = (int32_t)((int32_t)50);
	}

IL_0074:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
