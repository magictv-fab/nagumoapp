﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DownloadImages
struct DownloadImages_t3644489024;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.WWW
struct WWW_t3134621005;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_WWW3134621005.h"
#include "mscorlib_System_String7231557.h"

// System.Void DownloadImages::.ctor()
extern "C"  void DownloadImages__ctor_m1018443611 (DownloadImages_t3644489024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DownloadImages::.cctor()
extern "C"  void DownloadImages__cctor_m1024884658 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DownloadImages::OnEnable()
extern "C"  void DownloadImages_OnEnable_m2876214859 (DownloadImages_t3644489024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator DownloadImages::CheckFilesUpdate()
extern "C"  Il2CppObject * DownloadImages_CheckFilesUpdate_m4008057033 (DownloadImages_t3644489024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DownloadImages::WwwIsDone(UnityEngine.WWW)
extern "C"  bool DownloadImages_WwwIsDone_m1995130026 (Il2CppObject * __this /* static, unused */, WWW_t3134621005 * ___www0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator DownloadImages::Download(System.String,System.String,System.String)
extern "C"  Il2CppObject * DownloadImages_Download_m64692801 (DownloadImages_t3644489024 * __this, String_t* ___url0, String_t* ___folder1, String_t* ___file2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator DownloadImages::LoadFiles()
extern "C"  Il2CppObject * DownloadImages_LoadFiles_m2136445474 (DownloadImages_t3644489024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
