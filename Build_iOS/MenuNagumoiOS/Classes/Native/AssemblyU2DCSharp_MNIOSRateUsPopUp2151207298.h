﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_MNPopup1928680331.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MNIOSRateUsPopUp
struct  MNIOSRateUsPopUp_t2151207298  : public MNPopup_t1928680331
{
public:
	// System.String MNIOSRateUsPopUp::rate
	String_t* ___rate_6;
	// System.String MNIOSRateUsPopUp::remind
	String_t* ___remind_7;
	// System.String MNIOSRateUsPopUp::declined
	String_t* ___declined_8;
	// System.String MNIOSRateUsPopUp::appleId
	String_t* ___appleId_9;

public:
	inline static int32_t get_offset_of_rate_6() { return static_cast<int32_t>(offsetof(MNIOSRateUsPopUp_t2151207298, ___rate_6)); }
	inline String_t* get_rate_6() const { return ___rate_6; }
	inline String_t** get_address_of_rate_6() { return &___rate_6; }
	inline void set_rate_6(String_t* value)
	{
		___rate_6 = value;
		Il2CppCodeGenWriteBarrier(&___rate_6, value);
	}

	inline static int32_t get_offset_of_remind_7() { return static_cast<int32_t>(offsetof(MNIOSRateUsPopUp_t2151207298, ___remind_7)); }
	inline String_t* get_remind_7() const { return ___remind_7; }
	inline String_t** get_address_of_remind_7() { return &___remind_7; }
	inline void set_remind_7(String_t* value)
	{
		___remind_7 = value;
		Il2CppCodeGenWriteBarrier(&___remind_7, value);
	}

	inline static int32_t get_offset_of_declined_8() { return static_cast<int32_t>(offsetof(MNIOSRateUsPopUp_t2151207298, ___declined_8)); }
	inline String_t* get_declined_8() const { return ___declined_8; }
	inline String_t** get_address_of_declined_8() { return &___declined_8; }
	inline void set_declined_8(String_t* value)
	{
		___declined_8 = value;
		Il2CppCodeGenWriteBarrier(&___declined_8, value);
	}

	inline static int32_t get_offset_of_appleId_9() { return static_cast<int32_t>(offsetof(MNIOSRateUsPopUp_t2151207298, ___appleId_9)); }
	inline String_t* get_appleId_9() const { return ___appleId_9; }
	inline String_t** get_address_of_appleId_9() { return &___appleId_9; }
	inline void set_appleId_9(String_t* value)
	{
		___appleId_9 = value;
		Il2CppCodeGenWriteBarrier(&___appleId_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
