﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Flicker
struct Flicker_t1041969670;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Flicker::.ctor()
extern "C"  void Flicker__ctor_m3158791152 (Flicker_t1041969670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Flicker::Reset()
extern "C"  void Flicker_Reset_m805224093 (Flicker_t1041969670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Flicker::OnEnter()
extern "C"  void Flicker_OnEnter_m2743114695 (Flicker_t1041969670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Flicker::OnUpdate()
extern "C"  void Flicker_OnUpdate_m2565736284 (Flicker_t1041969670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
