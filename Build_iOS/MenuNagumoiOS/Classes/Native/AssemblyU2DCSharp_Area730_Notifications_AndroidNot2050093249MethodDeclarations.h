﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Area730.Notifications.AndroidNotifications
struct AndroidNotifications_t2050093249;
// Area730.Notifications.NotificationBuilder
struct NotificationBuilder_t1374367998;
// System.String
struct String_t;
// Area730.Notifications.Notification
struct Notification_t2273303539;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Area730_Notifications_Notificati2273303539.h"

// System.Void Area730.Notifications.AndroidNotifications::.ctor()
extern "C"  void AndroidNotifications__ctor_m3237310677 (AndroidNotifications_t2050093249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Area730.Notifications.AndroidNotifications::.cctor()
extern "C"  void AndroidNotifications__cctor_m1090286968 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Area730.Notifications.NotificationBuilder Area730.Notifications.AndroidNotifications::GetNotificationBuilderByIndex(System.Int32)
extern "C"  NotificationBuilder_t1374367998 * AndroidNotifications_GetNotificationBuilderByIndex_m786626933 (Il2CppObject * __this /* static, unused */, int32_t ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Area730.Notifications.NotificationBuilder Area730.Notifications.AndroidNotifications::GetNotificationBuilderByName(System.String)
extern "C"  NotificationBuilder_t1374367998 * AndroidNotifications_GetNotificationBuilderByName_m1743497479 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Area730.Notifications.AndroidNotifications::getVersion()
extern "C"  float AndroidNotifications_getVersion_m3644565533 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Area730.Notifications.AndroidNotifications::scheduleNotification(Area730.Notifications.Notification)
extern "C"  void AndroidNotifications_scheduleNotification_m3590442393 (Il2CppObject * __this /* static, unused */, Notification_t2273303539 * ___notif0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Area730.Notifications.AndroidNotifications::cancelNotification(Area730.Notifications.Notification)
extern "C"  void AndroidNotifications_cancelNotification_m699820796 (Il2CppObject * __this /* static, unused */, Notification_t2273303539 * ___notif0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Area730.Notifications.AndroidNotifications::cancelAll()
extern "C"  void AndroidNotifications_cancelAll_m3438401562 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Area730.Notifications.AndroidNotifications::cancelNotification(System.Int32)
extern "C"  void AndroidNotifications_cancelNotification_m506877893 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Area730.Notifications.AndroidNotifications::clear(System.Int32)
extern "C"  void AndroidNotifications_clear_m1129880177 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Area730.Notifications.AndroidNotifications::clearAll()
extern "C"  void AndroidNotifications_clearAll_m2726900163 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Area730.Notifications.AndroidNotifications::showToast(System.String)
extern "C"  void AndroidNotifications_showToast_m2421323365 (Il2CppObject * __this /* static, unused */, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
