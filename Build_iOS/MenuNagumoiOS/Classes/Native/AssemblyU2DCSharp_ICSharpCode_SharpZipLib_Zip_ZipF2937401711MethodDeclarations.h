﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.ZipFile
struct ZipFile_t2937401711;
// System.String
struct String_t;
// System.IO.FileStream
struct FileStream_t2141505868;
// System.IO.Stream
struct Stream_t1561764144;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// ICSharpCode.SharpZipLib.Zip.ZipEntry
struct ZipEntry_t3141689087;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// ICSharpCode.SharpZipLib.Zip.ZipTestResultHandler
struct ZipTestResultHandler_t235186184;
// ICSharpCode.SharpZipLib.Core.INameTransform
struct INameTransform_t2173030;
// ICSharpCode.SharpZipLib.Zip.IEntryFactory
struct IEntryFactory_t131367667;
// ICSharpCode.SharpZipLib.Zip.IArchiveStorage
struct IArchiveStorage_t345670484;
// ICSharpCode.SharpZipLib.Zip.IDynamicDataSource
struct IDynamicDataSource_t1093195849;
// ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate
struct ZipUpdate_t3527085946;
// ICSharpCode.SharpZipLib.Zip.IStaticDataSource
struct IStaticDataSource_t3265679022;
// System.Security.Cryptography.CryptoStream
struct CryptoStream_t2166124941;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_IO_FileStream2141505868.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipE3141689087.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Test3010800723.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipTe235186184.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipF3895517839.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_UseZ3006992774.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipF3527085946.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Comp3174865049.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipF2937401711.h"
#include "mscorlib_System_Security_Cryptography_CryptoStream2166124941.h"

// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::.ctor(System.String)
extern "C"  void ZipFile__ctor_m3443316374 (ZipFile_t2937401711 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::.ctor(System.IO.FileStream)
extern "C"  void ZipFile__ctor_m3932371047 (ZipFile_t2937401711 * __this, FileStream_t2141505868 * ___file0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::.ctor(System.IO.Stream)
extern "C"  void ZipFile__ctor_m2842228355 (ZipFile_t2937401711 * __this, Stream_t1561764144 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::.ctor()
extern "C"  void ZipFile__ctor_m1792935916 (ZipFile_t2937401711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::System.IDisposable.Dispose()
extern "C"  void ZipFile_System_IDisposable_Dispose_m42873651 (ZipFile_t2937401711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::OnKeysRequired(System.String)
extern "C"  void ZipFile_OnKeysRequired_m2970939256 (ZipFile_t2937401711 * __this, String_t* ___fileName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ICSharpCode.SharpZipLib.Zip.ZipFile::get_Key()
extern "C"  ByteU5BU5D_t4260760469* ZipFile_get_Key_m22817962 (ZipFile_t2937401711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::set_Key(System.Byte[])
extern "C"  void ZipFile_set_Key_m2212523005 (ZipFile_t2937401711 * __this, ByteU5BU5D_t4260760469* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::set_Password(System.String)
extern "C"  void ZipFile_set_Password_m2288522322 (ZipFile_t2937401711 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile::get_HaveKeys()
extern "C"  bool ZipFile_get_HaveKeys_m2203161257 (ZipFile_t2937401711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::Finalize()
extern "C"  void ZipFile_Finalize_m1243087094 (ZipFile_t2937401711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::Close()
extern "C"  void ZipFile_Close_m3503795458 (ZipFile_t2937401711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Zip.ZipFile ICSharpCode.SharpZipLib.Zip.ZipFile::Create(System.String)
extern "C"  ZipFile_t2937401711 * ZipFile_Create_m1834589826 (Il2CppObject * __this /* static, unused */, String_t* ___fileName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Zip.ZipFile ICSharpCode.SharpZipLib.Zip.ZipFile::Create(System.IO.Stream)
extern "C"  ZipFile_t2937401711 * ZipFile_Create_m514725655 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___outStream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile::get_IsStreamOwner()
extern "C"  bool ZipFile_get_IsStreamOwner_m834639006 (ZipFile_t2937401711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::set_IsStreamOwner(System.Boolean)
extern "C"  void ZipFile_set_IsStreamOwner_m1485452781 (ZipFile_t2937401711 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile::get_IsEmbeddedArchive()
extern "C"  bool ZipFile_get_IsEmbeddedArchive_m4108709411 (ZipFile_t2937401711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile::get_IsNewArchive()
extern "C"  bool ZipFile_get_IsNewArchive_m1732219129 (ZipFile_t2937401711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Zip.ZipFile::get_ZipFileComment()
extern "C"  String_t* ZipFile_get_ZipFileComment_m3433315200 (ZipFile_t2937401711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Zip.ZipFile::get_Name()
extern "C"  String_t* ZipFile_get_Name_m3324743849 (ZipFile_t2937401711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipFile::get_Size()
extern "C"  int32_t ZipFile_get_Size_m3917529428 (ZipFile_t2937401711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile::get_Count()
extern "C"  int64_t ZipFile_get_Count_m2955160959 (ZipFile_t2937401711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Zip.ZipEntry ICSharpCode.SharpZipLib.Zip.ZipFile::get_EntryByIndex(System.Int32)
extern "C"  ZipEntry_t3141689087 * ZipFile_get_EntryByIndex_m2173925229 (ZipFile_t2937401711 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ICSharpCode.SharpZipLib.Zip.ZipFile::GetEnumerator()
extern "C"  Il2CppObject * ZipFile_GetEnumerator_m2098743180 (ZipFile_t2937401711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipFile::FindEntry(System.String,System.Boolean)
extern "C"  int32_t ZipFile_FindEntry_m4272090956 (ZipFile_t2937401711 * __this, String_t* ___name0, bool ___ignoreCase1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Zip.ZipEntry ICSharpCode.SharpZipLib.Zip.ZipFile::GetEntry(System.String)
extern "C"  ZipEntry_t3141689087 * ZipFile_GetEntry_m1709952924 (ZipFile_t2937401711 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream ICSharpCode.SharpZipLib.Zip.ZipFile::GetInputStream(ICSharpCode.SharpZipLib.Zip.ZipEntry)
extern "C"  Stream_t1561764144 * ZipFile_GetInputStream_m2822591337 (ZipFile_t2937401711 * __this, ZipEntry_t3141689087 * ___entry0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream ICSharpCode.SharpZipLib.Zip.ZipFile::GetInputStream(System.Int64)
extern "C"  Stream_t1561764144 * ZipFile_GetInputStream_m3470590410 (ZipFile_t2937401711 * __this, int64_t ___entryIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile::TestArchive(System.Boolean)
extern "C"  bool ZipFile_TestArchive_m1259759813 (ZipFile_t2937401711 * __this, bool ___testData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile::TestArchive(System.Boolean,ICSharpCode.SharpZipLib.Zip.TestStrategy,ICSharpCode.SharpZipLib.Zip.ZipTestResultHandler)
extern "C"  bool ZipFile_TestArchive_m4147149008 (ZipFile_t2937401711 * __this, bool ___testData0, int32_t ___strategy1, ZipTestResultHandler_t235186184 * ___resultHandler2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile::TestLocalHeader(ICSharpCode.SharpZipLib.Zip.ZipEntry,ICSharpCode.SharpZipLib.Zip.ZipFile/HeaderTest)
extern "C"  int64_t ZipFile_TestLocalHeader_m3871398311 (ZipFile_t2937401711 * __this, ZipEntry_t3141689087 * ___entry0, int32_t ___tests1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Core.INameTransform ICSharpCode.SharpZipLib.Zip.ZipFile::get_NameTransform()
extern "C"  Il2CppObject * ZipFile_get_NameTransform_m1646276159 (ZipFile_t2937401711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::set_NameTransform(ICSharpCode.SharpZipLib.Core.INameTransform)
extern "C"  void ZipFile_set_NameTransform_m3125077198 (ZipFile_t2937401711 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Zip.IEntryFactory ICSharpCode.SharpZipLib.Zip.ZipFile::get_EntryFactory()
extern "C"  Il2CppObject * ZipFile_get_EntryFactory_m4235709129 (ZipFile_t2937401711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::set_EntryFactory(ICSharpCode.SharpZipLib.Zip.IEntryFactory)
extern "C"  void ZipFile_set_EntryFactory_m1887066178 (ZipFile_t2937401711 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipFile::get_BufferSize()
extern "C"  int32_t ZipFile_get_BufferSize_m404544564 (ZipFile_t2937401711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::set_BufferSize(System.Int32)
extern "C"  void ZipFile_set_BufferSize_m3818892359 (ZipFile_t2937401711 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile::get_IsUpdating()
extern "C"  bool ZipFile_get_IsUpdating_m2665939389 (ZipFile_t2937401711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Zip.UseZip64 ICSharpCode.SharpZipLib.Zip.ZipFile::get_UseZip64()
extern "C"  int32_t ZipFile_get_UseZip64_m4266159314 (ZipFile_t2937401711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::set_UseZip64(ICSharpCode.SharpZipLib.Zip.UseZip64)
extern "C"  void ZipFile_set_UseZip64_m1815738743 (ZipFile_t2937401711 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::BeginUpdate(ICSharpCode.SharpZipLib.Zip.IArchiveStorage,ICSharpCode.SharpZipLib.Zip.IDynamicDataSource)
extern "C"  void ZipFile_BeginUpdate_m1078086491 (ZipFile_t2937401711 * __this, Il2CppObject * ___archiveStorage0, Il2CppObject * ___dataSource1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::BeginUpdate(ICSharpCode.SharpZipLib.Zip.IArchiveStorage)
extern "C"  void ZipFile_BeginUpdate_m2444051986 (ZipFile_t2937401711 * __this, Il2CppObject * ___archiveStorage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::BeginUpdate()
extern "C"  void ZipFile_BeginUpdate_m303354012 (ZipFile_t2937401711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::CommitUpdate()
extern "C"  void ZipFile_CommitUpdate_m2654847416 (ZipFile_t2937401711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::AbortUpdate()
extern "C"  void ZipFile_AbortUpdate_m3285320739 (ZipFile_t2937401711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::SetComment(System.String)
extern "C"  void ZipFile_SetComment_m1225266733 (ZipFile_t2937401711 * __this, String_t* ___comment0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::AddUpdate(ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate)
extern "C"  void ZipFile_AddUpdate_m1427055396 (ZipFile_t2937401711 * __this, ZipUpdate_t3527085946 * ___update0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::Add(System.String,ICSharpCode.SharpZipLib.Zip.CompressionMethod,System.Boolean)
extern "C"  void ZipFile_Add_m47412419 (ZipFile_t2937401711 * __this, String_t* ___fileName0, int32_t ___compressionMethod1, bool ___useUnicodeText2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::Add(System.String,ICSharpCode.SharpZipLib.Zip.CompressionMethod)
extern "C"  void ZipFile_Add_m699612026 (ZipFile_t2937401711 * __this, String_t* ___fileName0, int32_t ___compressionMethod1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::Add(System.String)
extern "C"  void ZipFile_Add_m2187853111 (ZipFile_t2937401711 * __this, String_t* ___fileName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::Add(System.String,System.String)
extern "C"  void ZipFile_Add_m4173740339 (ZipFile_t2937401711 * __this, String_t* ___fileName0, String_t* ___entryName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::Add(ICSharpCode.SharpZipLib.Zip.IStaticDataSource,System.String)
extern "C"  void ZipFile_Add_m3963084933 (ZipFile_t2937401711 * __this, Il2CppObject * ___dataSource0, String_t* ___entryName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::Add(ICSharpCode.SharpZipLib.Zip.IStaticDataSource,System.String,ICSharpCode.SharpZipLib.Zip.CompressionMethod)
extern "C"  void ZipFile_Add_m3697641032 (ZipFile_t2937401711 * __this, Il2CppObject * ___dataSource0, String_t* ___entryName1, int32_t ___compressionMethod2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::Add(ICSharpCode.SharpZipLib.Zip.IStaticDataSource,System.String,ICSharpCode.SharpZipLib.Zip.CompressionMethod,System.Boolean)
extern "C"  void ZipFile_Add_m56914997 (ZipFile_t2937401711 * __this, Il2CppObject * ___dataSource0, String_t* ___entryName1, int32_t ___compressionMethod2, bool ___useUnicodeText3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::Add(ICSharpCode.SharpZipLib.Zip.ZipEntry)
extern "C"  void ZipFile_Add_m4142264476 (ZipFile_t2937401711 * __this, ZipEntry_t3141689087 * ___entry0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::AddDirectory(System.String)
extern "C"  void ZipFile_AddDirectory_m2562797790 (ZipFile_t2937401711 * __this, String_t* ___directoryName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile::Delete(System.String)
extern "C"  bool ZipFile_Delete_m3635420307 (ZipFile_t2937401711 * __this, String_t* ___fileName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::Delete(ICSharpCode.SharpZipLib.Zip.ZipEntry)
extern "C"  void ZipFile_Delete_m3601062388 (ZipFile_t2937401711 * __this, ZipEntry_t3141689087 * ___entry0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::WriteLEShort(System.Int32)
extern "C"  void ZipFile_WriteLEShort_m1679845613 (ZipFile_t2937401711 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::WriteLEUshort(System.UInt16)
extern "C"  void ZipFile_WriteLEUshort_m1924757355 (ZipFile_t2937401711 * __this, uint16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::WriteLEInt(System.Int32)
extern "C"  void ZipFile_WriteLEInt_m778080992 (ZipFile_t2937401711 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::WriteLEUint(System.UInt32)
extern "C"  void ZipFile_WriteLEUint_m407559070 (ZipFile_t2937401711 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::WriteLeLong(System.Int64)
extern "C"  void ZipFile_WriteLeLong_m1903461744 (ZipFile_t2937401711 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::WriteLEUlong(System.UInt64)
extern "C"  void ZipFile_WriteLEUlong_m2855973834 (ZipFile_t2937401711 * __this, uint64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::WriteLocalEntryHeader(ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate)
extern "C"  void ZipFile_WriteLocalEntryHeader_m1215954715 (ZipFile_t2937401711 * __this, ZipUpdate_t3527085946 * ___update0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipFile::WriteCentralDirectoryHeader(ICSharpCode.SharpZipLib.Zip.ZipEntry)
extern "C"  int32_t ZipFile_WriteCentralDirectoryHeader_m2692847149 (ZipFile_t2937401711 * __this, ZipEntry_t3141689087 * ___entry0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::PostUpdateCleanup()
extern "C"  void ZipFile_PostUpdateCleanup_m1638361509 (ZipFile_t2937401711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Zip.ZipFile::GetTransformedFileName(System.String)
extern "C"  String_t* ZipFile_GetTransformedFileName_m1967723441 (ZipFile_t2937401711 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Zip.ZipFile::GetTransformedDirectoryName(System.String)
extern "C"  String_t* ZipFile_GetTransformedDirectoryName_m2709602130 (ZipFile_t2937401711 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ICSharpCode.SharpZipLib.Zip.ZipFile::GetBuffer()
extern "C"  ByteU5BU5D_t4260760469* ZipFile_GetBuffer_m1752621354 (ZipFile_t2937401711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::CopyDescriptorBytes(ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate,System.IO.Stream,System.IO.Stream)
extern "C"  void ZipFile_CopyDescriptorBytes_m3046623079 (ZipFile_t2937401711 * __this, ZipUpdate_t3527085946 * ___update0, Stream_t1561764144 * ___dest1, Stream_t1561764144 * ___source2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::CopyBytes(ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate,System.IO.Stream,System.IO.Stream,System.Int64,System.Boolean)
extern "C"  void ZipFile_CopyBytes_m3139235901 (ZipFile_t2937401711 * __this, ZipUpdate_t3527085946 * ___update0, Stream_t1561764144 * ___destination1, Stream_t1561764144 * ___source2, int64_t ___bytesToCopy3, bool ___updateCrc4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipFile::GetDescriptorSize(ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate)
extern "C"  int32_t ZipFile_GetDescriptorSize_m1937565466 (ZipFile_t2937401711 * __this, ZipUpdate_t3527085946 * ___update0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::CopyDescriptorBytesDirect(ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate,System.IO.Stream,System.Int64&,System.Int64)
extern "C"  void ZipFile_CopyDescriptorBytesDirect_m2226161797 (ZipFile_t2937401711 * __this, ZipUpdate_t3527085946 * ___update0, Stream_t1561764144 * ___stream1, int64_t* ___destinationPosition2, int64_t ___sourcePosition3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::CopyEntryDataDirect(ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate,System.IO.Stream,System.Boolean,System.Int64&,System.Int64&)
extern "C"  void ZipFile_CopyEntryDataDirect_m4192775174 (ZipFile_t2937401711 * __this, ZipUpdate_t3527085946 * ___update0, Stream_t1561764144 * ___stream1, bool ___updateCrc2, int64_t* ___destinationPosition3, int64_t* ___sourcePosition4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipFile::FindExistingUpdate(ICSharpCode.SharpZipLib.Zip.ZipEntry)
extern "C"  int32_t ZipFile_FindExistingUpdate_m3162775880 (ZipFile_t2937401711 * __this, ZipEntry_t3141689087 * ___entry0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipFile::FindExistingUpdate(System.String)
extern "C"  int32_t ZipFile_FindExistingUpdate_m116399627 (ZipFile_t2937401711 * __this, String_t* ___fileName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream ICSharpCode.SharpZipLib.Zip.ZipFile::GetOutputStream(ICSharpCode.SharpZipLib.Zip.ZipEntry)
extern "C"  Stream_t1561764144 * ZipFile_GetOutputStream_m479299494 (ZipFile_t2937401711 * __this, ZipEntry_t3141689087 * ___entry0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::AddEntry(ICSharpCode.SharpZipLib.Zip.ZipFile,ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate)
extern "C"  void ZipFile_AddEntry_m1548392092 (ZipFile_t2937401711 * __this, ZipFile_t2937401711 * ___workFile0, ZipUpdate_t3527085946 * ___update1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::ModifyEntry(ICSharpCode.SharpZipLib.Zip.ZipFile,ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate)
extern "C"  void ZipFile_ModifyEntry_m3557226499 (ZipFile_t2937401711 * __this, ZipFile_t2937401711 * ___workFile0, ZipUpdate_t3527085946 * ___update1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::CopyEntryDirect(ICSharpCode.SharpZipLib.Zip.ZipFile,ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate,System.Int64&)
extern "C"  void ZipFile_CopyEntryDirect_m510234377 (ZipFile_t2937401711 * __this, ZipFile_t2937401711 * ___workFile0, ZipUpdate_t3527085946 * ___update1, int64_t* ___destinationPosition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::CopyEntry(ICSharpCode.SharpZipLib.Zip.ZipFile,ICSharpCode.SharpZipLib.Zip.ZipFile/ZipUpdate)
extern "C"  void ZipFile_CopyEntry_m1042456446 (ZipFile_t2937401711 * __this, ZipFile_t2937401711 * ___workFile0, ZipUpdate_t3527085946 * ___update1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::Reopen(System.IO.Stream)
extern "C"  void ZipFile_Reopen_m3926307436 (ZipFile_t2937401711 * __this, Stream_t1561764144 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::Reopen()
extern "C"  void ZipFile_Reopen_m3990533589 (ZipFile_t2937401711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::UpdateCommentOnly()
extern "C"  void ZipFile_UpdateCommentOnly_m2554931500 (ZipFile_t2937401711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::RunUpdates()
extern "C"  void ZipFile_RunUpdates_m4114555543 (ZipFile_t2937401711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::CheckUpdating()
extern "C"  void ZipFile_CheckUpdating_m370342136 (ZipFile_t2937401711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::DisposeInternal(System.Boolean)
extern "C"  void ZipFile_DisposeInternal_m693685181 (ZipFile_t2937401711 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::Dispose(System.Boolean)
extern "C"  void ZipFile_Dispose_m232432352 (ZipFile_t2937401711 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 ICSharpCode.SharpZipLib.Zip.ZipFile::ReadLEUshort()
extern "C"  uint16_t ZipFile_ReadLEUshort_m1106495417 (ZipFile_t2937401711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 ICSharpCode.SharpZipLib.Zip.ZipFile::ReadLEUint()
extern "C"  uint32_t ZipFile_ReadLEUint_m2012194598 (ZipFile_t2937401711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 ICSharpCode.SharpZipLib.Zip.ZipFile::ReadLEUlong()
extern "C"  uint64_t ZipFile_ReadLEUlong_m2391890568 (ZipFile_t2937401711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile::LocateBlockWithSignature(System.Int32,System.Int64,System.Int32,System.Int32)
extern "C"  int64_t ZipFile_LocateBlockWithSignature_m1788695079 (ZipFile_t2937401711 * __this, int32_t ___signature0, int64_t ___endLocation1, int32_t ___minimumBlockSize2, int32_t ___maximumVariableData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::ReadEntries()
extern "C"  void ZipFile_ReadEntries_m157831652 (ZipFile_t2937401711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile::LocateEntry(ICSharpCode.SharpZipLib.Zip.ZipEntry)
extern "C"  int64_t ZipFile_LocateEntry_m2956250186 (ZipFile_t2937401711 * __this, ZipEntry_t3141689087 * ___entry0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream ICSharpCode.SharpZipLib.Zip.ZipFile::CreateAndInitDecryptionStream(System.IO.Stream,ICSharpCode.SharpZipLib.Zip.ZipEntry)
extern "C"  Stream_t1561764144 * ZipFile_CreateAndInitDecryptionStream_m4004436892 (ZipFile_t2937401711 * __this, Stream_t1561764144 * ___baseStream0, ZipEntry_t3141689087 * ___entry1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream ICSharpCode.SharpZipLib.Zip.ZipFile::CreateAndInitEncryptionStream(System.IO.Stream,ICSharpCode.SharpZipLib.Zip.ZipEntry)
extern "C"  Stream_t1561764144 * ZipFile_CreateAndInitEncryptionStream_m3518887028 (ZipFile_t2937401711 * __this, Stream_t1561764144 * ___baseStream0, ZipEntry_t3141689087 * ___entry1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::CheckClassicPassword(System.Security.Cryptography.CryptoStream,ICSharpCode.SharpZipLib.Zip.ZipEntry)
extern "C"  void ZipFile_CheckClassicPassword_m3791130454 (Il2CppObject * __this /* static, unused */, CryptoStream_t2166124941 * ___classicCryptoStream0, ZipEntry_t3141689087 * ___entry1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::WriteEncryptionHeader(System.IO.Stream,System.Int64)
extern "C"  void ZipFile_WriteEncryptionHeader_m837549192 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___stream0, int64_t ___crcValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
