﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// UnityEngine.Component
struct Component_t3501516275;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DestroyComponent
struct  DestroyComponent_t1933763019  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.DestroyComponent::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.DestroyComponent::component
	FsmString_t952858651 * ___component_10;
	// UnityEngine.Component HutongGames.PlayMaker.Actions.DestroyComponent::aComponent
	Component_t3501516275 * ___aComponent_11;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(DestroyComponent_t1933763019, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_component_10() { return static_cast<int32_t>(offsetof(DestroyComponent_t1933763019, ___component_10)); }
	inline FsmString_t952858651 * get_component_10() const { return ___component_10; }
	inline FsmString_t952858651 ** get_address_of_component_10() { return &___component_10; }
	inline void set_component_10(FsmString_t952858651 * value)
	{
		___component_10 = value;
		Il2CppCodeGenWriteBarrier(&___component_10, value);
	}

	inline static int32_t get_offset_of_aComponent_11() { return static_cast<int32_t>(offsetof(DestroyComponent_t1933763019, ___aComponent_11)); }
	inline Component_t3501516275 * get_aComponent_11() const { return ___aComponent_11; }
	inline Component_t3501516275 ** get_address_of_aComponent_11() { return &___aComponent_11; }
	inline void set_aComponent_11(Component_t3501516275 * value)
	{
		___aComponent_11 = value;
		Il2CppCodeGenWriteBarrier(&___aComponent_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
