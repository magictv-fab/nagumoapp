﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<PlayMakerFSM>
struct List_1_t873065632;
// HutongGames.PlayMaker.Fsm
struct Fsm_t1527112426;
// FsmTemplate
struct FsmTemplate_t1237263802;
// UnityEngine.GUITexture
struct GUITexture_t4020448292;
// UnityEngine.GUIText
struct GUIText_t3371372606;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerFSM
struct  PlayMakerFSM_t3799847376  : public MonoBehaviour_t667441552
{
public:
	// HutongGames.PlayMaker.Fsm PlayMakerFSM::fsm
	Fsm_t1527112426 * ___fsm_4;
	// FsmTemplate PlayMakerFSM::fsmTemplate
	FsmTemplate_t1237263802 * ___fsmTemplate_5;
	// UnityEngine.GUITexture PlayMakerFSM::<GuiTexture>k__BackingField
	GUITexture_t4020448292 * ___U3CGuiTextureU3Ek__BackingField_6;
	// UnityEngine.GUIText PlayMakerFSM::<GuiText>k__BackingField
	GUIText_t3371372606 * ___U3CGuiTextU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_fsm_4() { return static_cast<int32_t>(offsetof(PlayMakerFSM_t3799847376, ___fsm_4)); }
	inline Fsm_t1527112426 * get_fsm_4() const { return ___fsm_4; }
	inline Fsm_t1527112426 ** get_address_of_fsm_4() { return &___fsm_4; }
	inline void set_fsm_4(Fsm_t1527112426 * value)
	{
		___fsm_4 = value;
		Il2CppCodeGenWriteBarrier(&___fsm_4, value);
	}

	inline static int32_t get_offset_of_fsmTemplate_5() { return static_cast<int32_t>(offsetof(PlayMakerFSM_t3799847376, ___fsmTemplate_5)); }
	inline FsmTemplate_t1237263802 * get_fsmTemplate_5() const { return ___fsmTemplate_5; }
	inline FsmTemplate_t1237263802 ** get_address_of_fsmTemplate_5() { return &___fsmTemplate_5; }
	inline void set_fsmTemplate_5(FsmTemplate_t1237263802 * value)
	{
		___fsmTemplate_5 = value;
		Il2CppCodeGenWriteBarrier(&___fsmTemplate_5, value);
	}

	inline static int32_t get_offset_of_U3CGuiTextureU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PlayMakerFSM_t3799847376, ___U3CGuiTextureU3Ek__BackingField_6)); }
	inline GUITexture_t4020448292 * get_U3CGuiTextureU3Ek__BackingField_6() const { return ___U3CGuiTextureU3Ek__BackingField_6; }
	inline GUITexture_t4020448292 ** get_address_of_U3CGuiTextureU3Ek__BackingField_6() { return &___U3CGuiTextureU3Ek__BackingField_6; }
	inline void set_U3CGuiTextureU3Ek__BackingField_6(GUITexture_t4020448292 * value)
	{
		___U3CGuiTextureU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CGuiTextureU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3CGuiTextU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PlayMakerFSM_t3799847376, ___U3CGuiTextU3Ek__BackingField_7)); }
	inline GUIText_t3371372606 * get_U3CGuiTextU3Ek__BackingField_7() const { return ___U3CGuiTextU3Ek__BackingField_7; }
	inline GUIText_t3371372606 ** get_address_of_U3CGuiTextU3Ek__BackingField_7() { return &___U3CGuiTextU3Ek__BackingField_7; }
	inline void set_U3CGuiTextU3Ek__BackingField_7(GUIText_t3371372606 * value)
	{
		___U3CGuiTextU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CGuiTextU3Ek__BackingField_7, value);
	}
};

struct PlayMakerFSM_t3799847376_StaticFields
{
public:
	// System.Collections.Generic.List`1<PlayMakerFSM> PlayMakerFSM::fsmList
	List_1_t873065632 * ___fsmList_2;
	// System.Boolean PlayMakerFSM::ApplicationIsQuitting
	bool ___ApplicationIsQuitting_3;
	// System.Boolean PlayMakerFSM::<DrawGizmos>k__BackingField
	bool ___U3CDrawGizmosU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_fsmList_2() { return static_cast<int32_t>(offsetof(PlayMakerFSM_t3799847376_StaticFields, ___fsmList_2)); }
	inline List_1_t873065632 * get_fsmList_2() const { return ___fsmList_2; }
	inline List_1_t873065632 ** get_address_of_fsmList_2() { return &___fsmList_2; }
	inline void set_fsmList_2(List_1_t873065632 * value)
	{
		___fsmList_2 = value;
		Il2CppCodeGenWriteBarrier(&___fsmList_2, value);
	}

	inline static int32_t get_offset_of_ApplicationIsQuitting_3() { return static_cast<int32_t>(offsetof(PlayMakerFSM_t3799847376_StaticFields, ___ApplicationIsQuitting_3)); }
	inline bool get_ApplicationIsQuitting_3() const { return ___ApplicationIsQuitting_3; }
	inline bool* get_address_of_ApplicationIsQuitting_3() { return &___ApplicationIsQuitting_3; }
	inline void set_ApplicationIsQuitting_3(bool value)
	{
		___ApplicationIsQuitting_3 = value;
	}

	inline static int32_t get_offset_of_U3CDrawGizmosU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PlayMakerFSM_t3799847376_StaticFields, ___U3CDrawGizmosU3Ek__BackingField_8)); }
	inline bool get_U3CDrawGizmosU3Ek__BackingField_8() const { return ___U3CDrawGizmosU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CDrawGizmosU3Ek__BackingField_8() { return &___U3CDrawGizmosU3Ek__BackingField_8; }
	inline void set_U3CDrawGizmosU3Ek__BackingField_8(bool value)
	{
		___U3CDrawGizmosU3Ek__BackingField_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
