﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IDisposable
struct IDisposable_t1423340799;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3771233898.h"

// System.IDisposable ARM.utils.cron.EasyTimerDisposable::SetInterval(System.Action,System.Int32)
extern "C"  Il2CppObject * EasyTimerDisposable_SetInterval_m2616104085 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___method0, int32_t ___delayInMilliseconds1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable ARM.utils.cron.EasyTimerDisposable::SetTimeout(System.Action,System.Int32)
extern "C"  Il2CppObject * EasyTimerDisposable_SetTimeout_m1766593127 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___method0, int32_t ___delayInMilliseconds1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
