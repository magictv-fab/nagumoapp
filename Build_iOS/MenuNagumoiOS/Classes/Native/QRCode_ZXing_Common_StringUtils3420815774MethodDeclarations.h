﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>
struct IDictionary_2_t759989846;

#include "codegen/il2cpp-codegen.h"

// System.String ZXing.Common.StringUtils::guessEncoding(System.Byte[],System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  String_t* StringUtils_guessEncoding_m133015133 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, Il2CppObject* ___hints1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.StringUtils::.cctor()
extern "C"  void StringUtils__cctor_m1104030969 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
