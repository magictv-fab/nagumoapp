﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Correios.Net.SearchZip
struct SearchZip_t17185055;
// Correios.Net.Address
struct Address_t2296282618;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void Correios.Net.SearchZip::.ctor()
extern "C"  void SearchZip__ctor_m215295085 (SearchZip_t17185055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Correios.Net.Address Correios.Net.SearchZip::GetAddress(System.String,System.Int32)
extern "C"  Address_t2296282618 * SearchZip_GetAddress_m3315145310 (Il2CppObject * __this /* static, unused */, String_t* ___zip0, int32_t ___timeout1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
