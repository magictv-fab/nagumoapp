﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.StereoCameraConfiguration
struct StereoCameraConfiguration_t2173582563;
// UnityEngine.Camera
struct Camera_t2727095145;
// System.Action
struct Action_t3771233898;
// Vuforia.WebCamAbstractBehaviour
struct WebCamAbstractBehaviour_t725705546;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevi134001414.h"
#include "System_Core_System_Action3771233898.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamAbst725705546.h"
#include "UnityEngine_UnityEngine_ScreenOrientation1849668026.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"

// System.Void Vuforia.StereoCameraConfiguration::.ctor(UnityEngine.Camera,UnityEngine.Camera,System.Boolean,System.Single,Vuforia.CameraDevice/CameraDeviceMode,System.Action,System.Int32)
extern "C"  void StereoCameraConfiguration__ctor_m1182147900 (StereoCameraConfiguration_t2173582563 * __this, Camera_t2727095145 * ___leftCamera0, Camera_t2727095145 * ___rightCamera1, bool ___autoStereoSkewing2, float ___cameraOffset3, int32_t ___cameraDeviceMode4, Action_t3771233898 * ___onVideoBackgroundConfigChanged5, int32_t ___eyewearUserCalibrationProfileId6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StereoCameraConfiguration::Init()
extern "C"  void StereoCameraConfiguration_Init_m2814466002 (StereoCameraConfiguration_t2173582563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StereoCameraConfiguration::ConfigureVideoBackground()
extern "C"  void StereoCameraConfiguration_ConfigureVideoBackground_m1317346245 (StereoCameraConfiguration_t2173582563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StereoCameraConfiguration::UpdatePlayModeParameters(Vuforia.WebCamAbstractBehaviour,System.Single)
extern "C"  void StereoCameraConfiguration_UpdatePlayModeParameters_m2529186039 (StereoCameraConfiguration_t2173582563 * __this, WebCamAbstractBehaviour_t725705546 * ___webCamBehaviour0, float ___cameraOffset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StereoCameraConfiguration::UpdateProjection(UnityEngine.ScreenOrientation)
extern "C"  void StereoCameraConfiguration_UpdateProjection_m1190113003 (StereoCameraConfiguration_t2173582563 * __this, int32_t ___orientation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ScreenOrientation Vuforia.StereoCameraConfiguration::CheckForSurfaceChanges()
extern "C"  int32_t StereoCameraConfiguration_CheckForSurfaceChanges_m8192851 (StereoCameraConfiguration_t2173582563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StereoCameraConfiguration::UpdateStereoDepth(UnityEngine.Transform)
extern "C"  void StereoCameraConfiguration_UpdateStereoDepth_m3028024059 (StereoCameraConfiguration_t2173582563 * __this, Transform_t1659122786 * ___trackingReference0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.StereoCameraConfiguration::IsStereo()
extern "C"  bool StereoCameraConfiguration_IsStereo_m1436841176 (StereoCameraConfiguration_t2173582563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StereoCameraConfiguration::ApplyCorrectedProjectionMatrix(UnityEngine.Matrix4x4,System.Boolean)
extern "C"  void StereoCameraConfiguration_ApplyCorrectedProjectionMatrix_m2171401680 (StereoCameraConfiguration_t2173582563 * __this, Matrix4x4_t1651859333  ___projectionMatrix0, bool ___primaryCamera1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StereoCameraConfiguration::ResolveVideoBackgroundBehaviours()
extern "C"  void StereoCameraConfiguration_ResolveVideoBackgroundBehaviours_m2343765557 (StereoCameraConfiguration_t2173582563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.StereoCameraConfiguration::CameraParameterChanged()
extern "C"  bool StereoCameraConfiguration_CameraParameterChanged_m898665190 (StereoCameraConfiguration_t2173582563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StereoCameraConfiguration::ExtractCameraClippingPlanes(UnityEngine.Matrix4x4,System.Single&,System.Single&)
extern "C"  void StereoCameraConfiguration_ExtractCameraClippingPlanes_m3806907835 (Il2CppObject * __this /* static, unused */, Matrix4x4_t1651859333  ___inverseProjMatrix0, float* ___near1, float* ___far2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.StereoCameraConfiguration::ExtractCameraFoV(UnityEngine.Matrix4x4)
extern "C"  float StereoCameraConfiguration_ExtractCameraFoV_m1345670557 (Il2CppObject * __this /* static, unused */, Matrix4x4_t1651859333  ___inverseProjMatrix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vuforia.StereoCameraConfiguration::HomogenizedVec3(UnityEngine.Vector4)
extern "C"  Vector3_t4282066566  StereoCameraConfiguration_HomogenizedVec3_m1125314248 (Il2CppObject * __this /* static, unused */, Vector4_t4282066567  ___vec40, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StereoCameraConfiguration::SetSkewFrustum(System.Boolean)
extern "C"  void StereoCameraConfiguration_SetSkewFrustum_m3107036847 (StereoCameraConfiguration_t2173582563 * __this, bool ___setSkewing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.StereoCameraConfiguration::get_EyewearUserCalibrationProfileId()
extern "C"  int32_t StereoCameraConfiguration_get_EyewearUserCalibrationProfileId_m1504632146 (StereoCameraConfiguration_t2173582563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StereoCameraConfiguration::set_EyewearUserCalibrationProfileId(System.Int32)
extern "C"  void StereoCameraConfiguration_set_EyewearUserCalibrationProfileId_m1702951465 (StereoCameraConfiguration_t2173582563 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StereoCameraConfiguration::.cctor()
extern "C"  void StereoCameraConfiguration__cctor_m2013965739 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
