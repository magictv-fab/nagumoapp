﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DeviceFileInfo
struct DeviceFileInfo_t2242857184;
// System.String
struct String_t;
// MagicTV.vo.ResultRevisionVO
struct ResultRevisionVO_t2445597391;
// MagicTV.vo.UrlInfoVO[]
struct UrlInfoVOU5BU5D_t1681515545;
// ReturnDataVO
struct ReturnDataVO_t544837971;
// MagicTV.vo.UrlInfoVO
struct UrlInfoVO_t1761987528;
// MagicTV.vo.KeyValueVO
struct KeyValueVO_t1780100105;
// MagicTV.vo.BundleVO[]
struct BundleVOU5BU5D_t2162892100;
// System.Collections.Generic.List`1<MagicTV.vo.BundleVO>
struct List_1_t3352703625;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Collections.Generic.IEnumerable`1<MagicTV.vo.MetadataVO>
struct IEnumerable_1_t1517202659;
// MagicTV.vo.BundleVO
struct BundleVO_t1984518073;
// MagicTV.vo.MetadataVO
struct MetadataVO_t2511256998;
// MagicTV.vo.FileVO
struct FileVO_t1935348659;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_MagicTV_vo_UrlInfoVO1761987528.h"
#include "AssemblyU2DCSharp_MagicTV_vo_ResultRevisionVO2445597391.h"
#include "AssemblyU2DCSharp_MagicTV_vo_KeyValueVO1780100105.h"
#include "AssemblyU2DCSharp_MagicTV_vo_BundleVO1984518073.h"
#include "AssemblyU2DCSharp_MagicTV_vo_MetadataVO2511256998.h"
#include "AssemblyU2DCSharp_MagicTV_vo_FileVO1935348659.h"

// System.Void DeviceFileInfo::.ctor()
extern "C"  void DeviceFileInfo__ctor_m1029341115 (DeviceFileInfo_t2242857184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DeviceFileInfo::getLastImageHash()
extern "C"  String_t* DeviceFileInfo_getLastImageHash_m2642619273 (DeviceFileInfo_t2242857184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceFileInfo::setLastImageHash(System.String)
extern "C"  void DeviceFileInfo_setLastImageHash_m593196456 (DeviceFileInfo_t2242857184 * __this, String_t* ___hash0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceFileInfo::OnDestroy()
extern "C"  void DeviceFileInfo_OnDestroy_m2552233780 (DeviceFileInfo_t2242857184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceFileInfo::prepare()
extern "C"  void DeviceFileInfo_prepare_m2281313856 (DeviceFileInfo_t2242857184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceFileInfo::onStateChangeToOpen()
extern "C"  void DeviceFileInfo_onStateChangeToOpen_m3174926208 (DeviceFileInfo_t2242857184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceFileInfo::cacheDbInfo()
extern "C"  void DeviceFileInfo_cacheDbInfo_m221378535 (DeviceFileInfo_t2242857184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.ResultRevisionVO DeviceFileInfo::getRevisionByRevisionId(System.String)
extern "C"  ResultRevisionVO_t2445597391 * DeviceFileInfo_getRevisionByRevisionId_m2121642359 (DeviceFileInfo_t2242857184 * __this, String_t* ___revision_id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.UrlInfoVO[] DeviceFileInfo::getUrlInfoVOToDelete()
extern "C"  UrlInfoVOU5BU5D_t1681515545* DeviceFileInfo_getUrlInfoVOToDelete_m1666997482 (DeviceFileInfo_t2242857184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfo::delete(MagicTV.vo.UrlInfoVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfo_delete_m230912990 (DeviceFileInfo_t2242857184 * __this, UrlInfoVO_t1761987528 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfo::delete(MagicTV.vo.UrlInfoVO[])
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfo_delete_m2864102268 (DeviceFileInfo_t2242857184 * __this, UrlInfoVOU5BU5D_t1681515545* ___objs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.ResultRevisionVO DeviceFileInfo::getCurrentRevision()
extern "C"  ResultRevisionVO_t2445597391 * DeviceFileInfo_getCurrentRevision_m181234771 (DeviceFileInfo_t2242857184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfo::commit(MagicTV.vo.ResultRevisionVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfo_commit_m2325107937 (DeviceFileInfo_t2242857184 * __this, ResultRevisionVO_t2445597391 * ___revisionResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfo::commit(MagicTV.vo.UrlInfoVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfo_commit_m38774378 (DeviceFileInfo_t2242857184 * __this, UrlInfoVO_t1761987528 * ___urlInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfo::commit(MagicTV.vo.KeyValueVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfo_commit_m936109607 (DeviceFileInfo_t2242857184 * __this, KeyValueVO_t1780100105 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.BundleVO[] DeviceFileInfo::getBundles(System.String)
extern "C"  BundleVOU5BU5D_t2162892100* DeviceFileInfo_getBundles_m3352011518 (DeviceFileInfo_t2242857184 * __this, String_t* ___trackingID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceFileInfo::populateIndexedBundles()
extern "C"  void DeviceFileInfo_populateIndexedBundles_m1802056467 (DeviceFileInfo_t2242857184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceFileInfo::addBundle(System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>)
extern "C"  void DeviceFileInfo_addBundle_m1233817045 (DeviceFileInfo_t2242857184 * __this, String_t* ___value0, List_1_t3352703625 * ___list1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceFileInfo::cacheBundle(System.String)
extern "C"  void DeviceFileInfo_cacheBundle_m2239809925 (DeviceFileInfo_t2242857184 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceFileInfo::Update()
extern "C"  void DeviceFileInfo_Update_m3571665586 (DeviceFileInfo_t2242857184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.ResultRevisionVO DeviceFileInfo::getUncachedRevision()
extern "C"  ResultRevisionVO_t2445597391 * DeviceFileInfo_getUncachedRevision_m3010240857 (DeviceFileInfo_t2242857184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceFileInfo::Reset()
extern "C"  void DeviceFileInfo_Reset_m2970741352 (DeviceFileInfo_t2242857184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceFileInfo::setImagesSplash(System.String[])
extern "C"  void DeviceFileInfo_setImagesSplash_m466619174 (DeviceFileInfo_t2242857184 * __this, StringU5BU5D_t4054002952* ___imagesUrl0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceFileInfo::clearImages()
extern "C"  void DeviceFileInfo_clearImages_m3673208030 (DeviceFileInfo_t2242857184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DeviceFileInfo::getTotalImages()
extern "C"  int32_t DeviceFileInfo_getTotalImages_m3576617533 (DeviceFileInfo_t2242857184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DeviceFileInfo::getImageSplash()
extern "C"  String_t* DeviceFileInfo_getImageSplash_m1495600536 (DeviceFileInfo_t2242857184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DeviceFileInfo::getImageByIndex(System.Int32)
extern "C"  String_t* DeviceFileInfo_getImageByIndex_m320909917 (DeviceFileInfo_t2242857184 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.UrlInfoVO[] DeviceFileInfo::getAllUrlInfoVO()
extern "C"  UrlInfoVOU5BU5D_t1681515545* DeviceFileInfo_getAllUrlInfoVO_m2390376105 (DeviceFileInfo_t2242857184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.UrlInfoVO[] DeviceFileInfo::getCachedUrlInfoVO()
extern "C"  UrlInfoVOU5BU5D_t1681515545* DeviceFileInfo_getCachedUrlInfoVO_m1866138882 (DeviceFileInfo_t2242857184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceFileInfo::Save()
extern "C"  void DeviceFileInfo_Save_m259399782 (DeviceFileInfo_t2242857184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<MagicTV.vo.MetadataVO> DeviceFileInfo::<populateIndexedBundles>m__29(MagicTV.vo.BundleVO)
extern "C"  Il2CppObject* DeviceFileInfo_U3CpopulateIndexedBundlesU3Em__29_m1680914902 (Il2CppObject * __this /* static, unused */, BundleVO_t1984518073 * ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DeviceFileInfo::<populateIndexedBundles>m__2A(MagicTV.vo.MetadataVO)
extern "C"  bool DeviceFileInfo_U3CpopulateIndexedBundlesU3Em__2A_m1968396459 (Il2CppObject * __this /* static, unused */, MetadataVO_t2511256998 * ___m0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.BundleVO DeviceFileInfo::<getUncachedRevision>m__2C(MagicTV.vo.BundleVO)
extern "C"  BundleVO_t1984518073 * DeviceFileInfo_U3CgetUncachedRevisionU3Em__2C_m400669478 (Il2CppObject * __this /* static, unused */, BundleVO_t1984518073 * ___bx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.MetadataVO DeviceFileInfo::<getUncachedRevision>m__2D(MagicTV.vo.MetadataVO)
extern "C"  MetadataVO_t2511256998 * DeviceFileInfo_U3CgetUncachedRevisionU3Em__2D_m2305816485 (DeviceFileInfo_t2242857184 * __this, MetadataVO_t2511256998 * ___mx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DeviceFileInfo::<getUncachedRevision>m__2E(MagicTV.vo.BundleVO)
extern "C"  bool DeviceFileInfo_U3CgetUncachedRevisionU3Em__2E_m252641874 (Il2CppObject * __this /* static, unused */, BundleVO_t1984518073 * ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DeviceFileInfo::<getUncachedRevision>m__2F(MagicTV.vo.FileVO)
extern "C"  bool DeviceFileInfo_U3CgetUncachedRevisionU3Em__2F_m3989359223 (Il2CppObject * __this /* static, unused */, FileVO_t1935348659 * ___fx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.FileVO DeviceFileInfo::<getUncachedRevision>m__30(MagicTV.vo.FileVO)
extern "C"  FileVO_t1935348659 * DeviceFileInfo_U3CgetUncachedRevisionU3Em__30_m550854170 (Il2CppObject * __this /* static, unused */, FileVO_t1935348659 * ___fx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DeviceFileInfo::<getUncachedRevision>m__32(MagicTV.vo.FileVO)
extern "C"  bool DeviceFileInfo_U3CgetUncachedRevisionU3Em__32_m3787260044 (Il2CppObject * __this /* static, unused */, FileVO_t1935348659 * ___f0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DeviceFileInfo::<getUncachedRevision>m__33(MagicTV.vo.UrlInfoVO)
extern "C"  bool DeviceFileInfo_U3CgetUncachedRevisionU3Em__33_m2608124924 (Il2CppObject * __this /* static, unused */, UrlInfoVO_t1761987528 * ___ux0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.UrlInfoVO DeviceFileInfo::<getUncachedRevision>m__34(MagicTV.vo.UrlInfoVO)
extern "C"  UrlInfoVO_t1761987528 * DeviceFileInfo_U3CgetUncachedRevisionU3Em__34_m2519847910 (Il2CppObject * __this /* static, unused */, UrlInfoVO_t1761987528 * ___ux0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
