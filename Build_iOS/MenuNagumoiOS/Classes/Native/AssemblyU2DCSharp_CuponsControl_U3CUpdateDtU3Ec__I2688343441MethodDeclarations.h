﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CuponsControl/<UpdateDt>c__Iterator4E
struct U3CUpdateDtU3Ec__Iterator4E_t2688343441;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void CuponsControl/<UpdateDt>c__Iterator4E::.ctor()
extern "C"  void U3CUpdateDtU3Ec__Iterator4E__ctor_m3724345082 (U3CUpdateDtU3Ec__Iterator4E_t2688343441 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CuponsControl/<UpdateDt>c__Iterator4E::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CUpdateDtU3Ec__Iterator4E_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m350740568 (U3CUpdateDtU3Ec__Iterator4E_t2688343441 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CuponsControl/<UpdateDt>c__Iterator4E::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CUpdateDtU3Ec__Iterator4E_System_Collections_IEnumerator_get_Current_m3503657964 (U3CUpdateDtU3Ec__Iterator4E_t2688343441 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CuponsControl/<UpdateDt>c__Iterator4E::MoveNext()
extern "C"  bool U3CUpdateDtU3Ec__Iterator4E_MoveNext_m1703230394 (U3CUpdateDtU3Ec__Iterator4E_t2688343441 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CuponsControl/<UpdateDt>c__Iterator4E::Dispose()
extern "C"  void U3CUpdateDtU3Ec__Iterator4E_Dispose_m1286149303 (U3CUpdateDtU3Ec__Iterator4E_t2688343441 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CuponsControl/<UpdateDt>c__Iterator4E::Reset()
extern "C"  void U3CUpdateDtU3Ec__Iterator4E_Reset_m1370778023 (U3CUpdateDtU3Ec__Iterator4E_t2688343441 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
