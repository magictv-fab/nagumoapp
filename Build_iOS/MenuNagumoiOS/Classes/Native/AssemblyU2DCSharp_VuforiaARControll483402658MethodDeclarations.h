﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VuforiaARControll
struct VuforiaARControll_t483402658;
// Vuforia.ObjectTracker
struct ObjectTracker_t455954211;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VuforiaARControll483402658.h"

// System.Void VuforiaARControll::.ctor()
extern "C"  void VuforiaARControll__ctor_m2247393289 (VuforiaARControll_t483402658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VuforiaARControll VuforiaARControll::get_Instance()
extern "C"  VuforiaARControll_t483402658 * VuforiaARControll_get_Instance_m4236360744 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VuforiaARControll::set_Instance(VuforiaARControll)
extern "C"  void VuforiaARControll_set_Instance_m3546249219 (Il2CppObject * __this /* static, unused */, VuforiaARControll_t483402658 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ObjectTracker VuforiaARControll::get_VuforiaImageTracker()
extern "C"  ObjectTracker_t455954211 * VuforiaARControll_get_VuforiaImageTracker_m3152149219 (VuforiaARControll_t483402658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VuforiaARControll::Enable()
extern "C"  void VuforiaARControll_Enable_m2996173406 (VuforiaARControll_t483402658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VuforiaARControll::Disable()
extern "C"  void VuforiaARControll_Disable_m3127999631 (VuforiaARControll_t483402658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
