﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine
struct DeflaterEngine_t3684739431;
// ICSharpCode.SharpZipLib.Zip.Compression.DeflaterPending
struct DeflaterPending_t1829109954;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Comp1829109954.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Comp1587604368.h"

// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::.ctor(ICSharpCode.SharpZipLib.Zip.Compression.DeflaterPending)
extern "C"  void DeflaterEngine__ctor_m857117020 (DeflaterEngine_t3684739431 * __this, DeflaterPending_t1829109954 * ___pending0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::Deflate(System.Boolean,System.Boolean)
extern "C"  bool DeflaterEngine_Deflate_m2227637829 (DeflaterEngine_t3684739431 * __this, bool ___flush0, bool ___finish1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::SetInput(System.Byte[],System.Int32,System.Int32)
extern "C"  void DeflaterEngine_SetInput_m3904077705 (DeflaterEngine_t3684739431 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::NeedsInput()
extern "C"  bool DeflaterEngine_NeedsInput_m3074806969 (DeflaterEngine_t3684739431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::SetDictionary(System.Byte[],System.Int32,System.Int32)
extern "C"  void DeflaterEngine_SetDictionary_m605399687 (DeflaterEngine_t3684739431 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::Reset()
extern "C"  void DeflaterEngine_Reset_m3380485401 (DeflaterEngine_t3684739431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::ResetAdler()
extern "C"  void DeflaterEngine_ResetAdler_m2982176095 (DeflaterEngine_t3684739431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::get_Adler()
extern "C"  int32_t DeflaterEngine_get_Adler_m1867651049 (DeflaterEngine_t3684739431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::get_TotalIn()
extern "C"  int64_t DeflaterEngine_get_TotalIn_m1212048443 (DeflaterEngine_t3684739431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Zip.Compression.DeflateStrategy ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::get_Strategy()
extern "C"  int32_t DeflaterEngine_get_Strategy_m3468690991 (DeflaterEngine_t3684739431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::set_Strategy(ICSharpCode.SharpZipLib.Zip.Compression.DeflateStrategy)
extern "C"  void DeflaterEngine_set_Strategy_m277066290 (DeflaterEngine_t3684739431 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::SetLevel(System.Int32)
extern "C"  void DeflaterEngine_SetLevel_m2597219307 (DeflaterEngine_t3684739431 * __this, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::FillWindow()
extern "C"  void DeflaterEngine_FillWindow_m2566383275 (DeflaterEngine_t3684739431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::UpdateHash()
extern "C"  void DeflaterEngine_UpdateHash_m3444408271 (DeflaterEngine_t3684739431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::InsertString()
extern "C"  int32_t DeflaterEngine_InsertString_m3776914320 (DeflaterEngine_t3684739431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::SlideWindow()
extern "C"  void DeflaterEngine_SlideWindow_m782519691 (DeflaterEngine_t3684739431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::FindLongestMatch(System.Int32)
extern "C"  bool DeflaterEngine_FindLongestMatch_m1873253265 (DeflaterEngine_t3684739431 * __this, int32_t ___curMatch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::DeflateStored(System.Boolean,System.Boolean)
extern "C"  bool DeflaterEngine_DeflateStored_m991290914 (DeflaterEngine_t3684739431 * __this, bool ___flush0, bool ___finish1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::DeflateFast(System.Boolean,System.Boolean)
extern "C"  bool DeflaterEngine_DeflateFast_m2229531209 (DeflaterEngine_t3684739431 * __this, bool ___flush0, bool ___finish1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::DeflateSlow(System.Boolean,System.Boolean)
extern "C"  bool DeflaterEngine_DeflateSlow_m4285852676 (DeflaterEngine_t3684739431 * __this, bool ___flush0, bool ___finish1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
