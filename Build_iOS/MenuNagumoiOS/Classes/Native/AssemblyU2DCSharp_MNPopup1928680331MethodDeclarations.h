﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MNPopup
struct MNPopup_t1928680331;

#include "codegen/il2cpp-codegen.h"

// System.Void MNPopup::.ctor()
extern "C"  void MNPopup__ctor_m912369728 (MNPopup_t1928680331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
