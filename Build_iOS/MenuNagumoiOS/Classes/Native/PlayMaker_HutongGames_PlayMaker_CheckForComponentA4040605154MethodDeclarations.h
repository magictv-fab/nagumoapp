﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.CheckForComponentAttribute
struct CheckForComponentAttribute_t4040605154;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"

// System.Type HutongGames.PlayMaker.CheckForComponentAttribute::get_Type0()
extern "C"  Type_t * CheckForComponentAttribute_get_Type0_m4138641346 (CheckForComponentAttribute_t4040605154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type HutongGames.PlayMaker.CheckForComponentAttribute::get_Type1()
extern "C"  Type_t * CheckForComponentAttribute_get_Type1_m4138642307 (CheckForComponentAttribute_t4040605154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type HutongGames.PlayMaker.CheckForComponentAttribute::get_Type2()
extern "C"  Type_t * CheckForComponentAttribute_get_Type2_m4138643268 (CheckForComponentAttribute_t4040605154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.CheckForComponentAttribute::.ctor(System.Type)
extern "C"  void CheckForComponentAttribute__ctor_m2406674216 (CheckForComponentAttribute_t4040605154 * __this, Type_t * ___type00, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.CheckForComponentAttribute::.ctor(System.Type,System.Type)
extern "C"  void CheckForComponentAttribute__ctor_m4151116379 (CheckForComponentAttribute_t4040605154 * __this, Type_t * ___type00, Type_t * ___type11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.CheckForComponentAttribute::.ctor(System.Type,System.Type,System.Type)
extern "C"  void CheckForComponentAttribute__ctor_m1979506958 (CheckForComponentAttribute_t4040605154 * __this, Type_t * ___type00, Type_t * ___type11, Type_t * ___type22, const MethodInfo* method) IL2CPP_METHOD_ATTR;
