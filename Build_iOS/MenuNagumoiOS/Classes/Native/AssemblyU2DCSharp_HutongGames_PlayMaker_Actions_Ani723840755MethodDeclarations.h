﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AnimateRect
struct AnimateRect_t723840755;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AnimateRect::.ctor()
extern "C"  void AnimateRect__ctor_m224664995 (AnimateRect_t723840755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateRect::Reset()
extern "C"  void AnimateRect_Reset_m2166065232 (AnimateRect_t723840755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateRect::OnEnter()
extern "C"  void AnimateRect_OnEnter_m546423994 (AnimateRect_t723840755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateRect::OnUpdate()
extern "C"  void AnimateRect_OnUpdate_m3187801289 (AnimateRect_t723840755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
