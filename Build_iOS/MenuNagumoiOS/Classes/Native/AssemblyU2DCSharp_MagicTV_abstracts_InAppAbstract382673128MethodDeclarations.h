﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.abstracts.InAppAbstract
struct InAppAbstract_t382673128;
// System.Action
struct Action_t3771233898;
// MagicTV.vo.BundleVO
struct BundleVO_t1984518073;
// System.String
struct String_t;
// ReturnDataVO
struct ReturnDataVO_t544837971;
// MagicTV.vo.UrlInfoVO
struct UrlInfoVO_t1761987528;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// MagicTV.vo.MetadataVO[]
struct MetadataVOU5BU5D_t4238035203;
// MagicTV.globals.InAppAnalytics
struct InAppAnalytics_t2316734034;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_MagicTV_vo_BundleVO1984518073.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_MagicTV_vo_UrlInfoVO1761987528.h"
#include "AssemblyU2DCSharp_MagicTV_globals_InAppAnalytics2316734034.h"

// System.Void MagicTV.abstracts.InAppAbstract::.ctor()
extern "C"  void InAppAbstract__ctor_m2727038345 (InAppAbstract_t382673128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.InAppAbstract::AddQuitEventHandler(System.Action)
extern "C"  void InAppAbstract_AddQuitEventHandler_m1000232630 (InAppAbstract_t382673128 * __this, Action_t3771233898 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.InAppAbstract::RemoveQuitEventHandler(System.Action)
extern "C"  void InAppAbstract_RemoveQuitEventHandler_m1030317823 (InAppAbstract_t382673128 * __this, Action_t3771233898 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.InAppAbstract::RaiseQuit()
extern "C"  void InAppAbstract_RaiseQuit_m2356653378 (InAppAbstract_t382673128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.InAppAbstract::transformFromVO(MagicTV.vo.BundleVO)
extern "C"  void InAppAbstract_transformFromVO_m1970297739 (InAppAbstract_t382673128 * __this, BundleVO_t1984518073 * ___bundle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.InAppAbstract::captionMetadataConfig(System.String)
extern "C"  void InAppAbstract_captionMetadataConfig_m841235428 (InAppAbstract_t382673128 * __this, String_t* ___legenda0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 MagicTV.abstracts.InAppAbstract::getVectorFromMetaString(System.String,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  InAppAbstract_getVectorFromMetaString_m1674521723 (InAppAbstract_t382673128 * __this, String_t* ___data0, Vector3_t4282066566  ___defaultValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicTV.abstracts.InAppAbstract::checkLocalFile(System.String,System.String)
extern "C"  bool InAppAbstract_checkLocalFile_m428432024 (InAppAbstract_t382673128 * __this, String_t* ___file_destination0, String_t* ___md51, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO MagicTV.abstracts.InAppAbstract::saveFileAndUpdateDatabase(MagicTV.vo.UrlInfoVO&,System.Byte[],System.String)
extern "C"  ReturnDataVO_t544837971 * InAppAbstract_saveFileAndUpdateDatabase_m2592105592 (InAppAbstract_t382673128 * __this, UrlInfoVO_t1761987528 ** ___urlInfoVO0, ByteU5BU5D_t4260760469* ___bytes1, String_t* ___file_type2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.InAppAbstract::SetBundleVO(MagicTV.vo.BundleVO)
extern "C"  void InAppAbstract_SetBundleVO_m2381778589 (InAppAbstract_t382673128 * __this, BundleVO_t1984518073 * ___bundle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.InAppAbstract::debugMetasBundles(MagicTV.vo.MetadataVO[])
extern "C"  void InAppAbstract_debugMetasBundles_m904769717 (InAppAbstract_t382673128 * __this, MetadataVOU5BU5D_t4238035203* ___metadata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.abstracts.InAppAbstract::GetBundleId()
extern "C"  String_t* InAppAbstract_GetBundleId_m1855541501 (InAppAbstract_t382673128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.InAppAbstract::CheckDelayToRun(MagicTV.vo.BundleVO)
extern "C"  void InAppAbstract_CheckDelayToRun_m1375322213 (InAppAbstract_t382673128 * __this, BundleVO_t1984518073 * ___bundle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.InAppAbstract::changeRotationFromGUI(UnityEngine.Vector3)
extern "C"  void InAppAbstract_changeRotationFromGUI_m2695971727 (InAppAbstract_t382673128 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.InAppAbstract::changePositionFromGUI(UnityEngine.Vector3)
extern "C"  void InAppAbstract_changePositionFromGUI_m1775300282 (InAppAbstract_t382673128 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.InAppAbstract::changeScaleFromGUI(UnityEngine.Vector3)
extern "C"  void InAppAbstract_changeScaleFromGUI_m3183910663 (InAppAbstract_t382673128 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.InAppAbstract::Run()
extern "C"  void InAppAbstract_Run_m1784096754 (InAppAbstract_t382673128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.InAppAbstract::StartDelay()
extern "C"  void InAppAbstract_StartDelay_m737072412 (InAppAbstract_t382673128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.InAppAbstract::StopDelay()
extern "C"  void InAppAbstract_StopDelay_m3568811528 (InAppAbstract_t382673128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.InAppAbstract::PauseDelay()
extern "C"  void InAppAbstract_PauseDelay_m2612020488 (InAppAbstract_t382673128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.InAppAbstract::DelayFinished()
extern "C"  void InAppAbstract_DelayFinished_m1436739196 (InAppAbstract_t382673128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.InAppAbstract::Stop()
extern "C"  void InAppAbstract_Stop_m3795196445 (InAppAbstract_t382673128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.abstracts.InAppAbstract::GetAnalyticsCategory()
extern "C"  String_t* InAppAbstract_GetAnalyticsCategory_m3957689222 (InAppAbstract_t382673128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.InAppAbstract::SetAnalytics(MagicTV.globals.InAppAnalytics)
extern "C"  void InAppAbstract_SetAnalytics_m2109735544 (InAppAbstract_t382673128 * __this, InAppAnalytics_t2316734034 * ___analytics0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
