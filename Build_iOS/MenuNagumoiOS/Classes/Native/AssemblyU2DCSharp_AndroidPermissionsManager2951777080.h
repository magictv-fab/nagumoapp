﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t2362096582;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AndroidPermissionsManager
struct  AndroidPermissionsManager_t2951777080  : public Il2CppObject
{
public:

public:
};

struct AndroidPermissionsManager_t2951777080_StaticFields
{
public:
	// UnityEngine.AndroidJavaObject AndroidPermissionsManager::m_Activity
	AndroidJavaObject_t2362096582 * ___m_Activity_0;
	// UnityEngine.AndroidJavaObject AndroidPermissionsManager::m_PermissionService
	AndroidJavaObject_t2362096582 * ___m_PermissionService_1;

public:
	inline static int32_t get_offset_of_m_Activity_0() { return static_cast<int32_t>(offsetof(AndroidPermissionsManager_t2951777080_StaticFields, ___m_Activity_0)); }
	inline AndroidJavaObject_t2362096582 * get_m_Activity_0() const { return ___m_Activity_0; }
	inline AndroidJavaObject_t2362096582 ** get_address_of_m_Activity_0() { return &___m_Activity_0; }
	inline void set_m_Activity_0(AndroidJavaObject_t2362096582 * value)
	{
		___m_Activity_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_Activity_0, value);
	}

	inline static int32_t get_offset_of_m_PermissionService_1() { return static_cast<int32_t>(offsetof(AndroidPermissionsManager_t2951777080_StaticFields, ___m_PermissionService_1)); }
	inline AndroidJavaObject_t2362096582 * get_m_PermissionService_1() const { return ___m_PermissionService_1; }
	inline AndroidJavaObject_t2362096582 ** get_address_of_m_PermissionService_1() { return &___m_PermissionService_1; }
	inline void set_m_PermissionService_1(AndroidJavaObject_t2362096582 * value)
	{
		___m_PermissionService_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_PermissionService_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
