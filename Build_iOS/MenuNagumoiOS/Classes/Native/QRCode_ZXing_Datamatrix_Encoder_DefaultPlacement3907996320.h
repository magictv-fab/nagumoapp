﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Datamatrix.Encoder.DefaultPlacement
struct  DefaultPlacement_t3907996320  : public Il2CppObject
{
public:
	// System.String ZXing.Datamatrix.Encoder.DefaultPlacement::codewords
	String_t* ___codewords_0;
	// System.Int32 ZXing.Datamatrix.Encoder.DefaultPlacement::numrows
	int32_t ___numrows_1;
	// System.Int32 ZXing.Datamatrix.Encoder.DefaultPlacement::numcols
	int32_t ___numcols_2;
	// System.Byte[] ZXing.Datamatrix.Encoder.DefaultPlacement::bits
	ByteU5BU5D_t4260760469* ___bits_3;

public:
	inline static int32_t get_offset_of_codewords_0() { return static_cast<int32_t>(offsetof(DefaultPlacement_t3907996320, ___codewords_0)); }
	inline String_t* get_codewords_0() const { return ___codewords_0; }
	inline String_t** get_address_of_codewords_0() { return &___codewords_0; }
	inline void set_codewords_0(String_t* value)
	{
		___codewords_0 = value;
		Il2CppCodeGenWriteBarrier(&___codewords_0, value);
	}

	inline static int32_t get_offset_of_numrows_1() { return static_cast<int32_t>(offsetof(DefaultPlacement_t3907996320, ___numrows_1)); }
	inline int32_t get_numrows_1() const { return ___numrows_1; }
	inline int32_t* get_address_of_numrows_1() { return &___numrows_1; }
	inline void set_numrows_1(int32_t value)
	{
		___numrows_1 = value;
	}

	inline static int32_t get_offset_of_numcols_2() { return static_cast<int32_t>(offsetof(DefaultPlacement_t3907996320, ___numcols_2)); }
	inline int32_t get_numcols_2() const { return ___numcols_2; }
	inline int32_t* get_address_of_numcols_2() { return &___numcols_2; }
	inline void set_numcols_2(int32_t value)
	{
		___numcols_2 = value;
	}

	inline static int32_t get_offset_of_bits_3() { return static_cast<int32_t>(offsetof(DefaultPlacement_t3907996320, ___bits_3)); }
	inline ByteU5BU5D_t4260760469* get_bits_3() const { return ___bits_3; }
	inline ByteU5BU5D_t4260760469** get_address_of_bits_3() { return &___bits_3; }
	inline void set_bits_3(ByteU5BU5D_t4260760469* value)
	{
		___bits_3 = value;
		Il2CppCodeGenWriteBarrier(&___bits_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
