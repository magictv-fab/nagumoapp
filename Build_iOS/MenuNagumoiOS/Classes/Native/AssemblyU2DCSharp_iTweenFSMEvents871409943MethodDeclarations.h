﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// iTweenFSMEvents
struct iTweenFSMEvents_t871409943;

#include "codegen/il2cpp-codegen.h"

// System.Void iTweenFSMEvents::.ctor()
extern "C"  void iTweenFSMEvents__ctor_m2055104564 (iTweenFSMEvents_t871409943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenFSMEvents::.cctor()
extern "C"  void iTweenFSMEvents__cctor_m3096603129 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenFSMEvents::iTweenOnStart(System.Int32)
extern "C"  void iTweenFSMEvents_iTweenOnStart_m1246944484 (iTweenFSMEvents_t871409943 * __this, int32_t ___aniTweenID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenFSMEvents::iTweenOnComplete(System.Int32)
extern "C"  void iTweenFSMEvents_iTweenOnComplete_m1449795867 (iTweenFSMEvents_t871409943 * __this, int32_t ___aniTweenID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
