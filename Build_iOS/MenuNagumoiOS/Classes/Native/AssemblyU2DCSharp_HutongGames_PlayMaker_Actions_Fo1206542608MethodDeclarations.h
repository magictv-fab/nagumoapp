﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FormatString
struct FormatString_t1206542608;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FormatString::.ctor()
extern "C"  void FormatString__ctor_m994606934 (FormatString_t1206542608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FormatString::Reset()
extern "C"  void FormatString_Reset_m2936007171 (FormatString_t1206542608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FormatString::OnEnter()
extern "C"  void FormatString_OnEnter_m1726252461 (FormatString_t1206542608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FormatString::OnUpdate()
extern "C"  void FormatString_OnUpdate_m1107778102 (FormatString_t1206542608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FormatString::DoFormatString()
extern "C"  void FormatString_DoFormatString_m2819192129 (FormatString_t1206542608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
