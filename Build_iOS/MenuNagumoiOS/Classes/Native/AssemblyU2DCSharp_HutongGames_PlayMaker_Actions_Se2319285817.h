﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "UnityEngine_UnityEngine_Space4209342076.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetPosition
struct  SetPosition_t2319285817  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetPosition::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SetPosition::vector
	FsmVector3_t533912882 * ___vector_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetPosition::x
	FsmFloat_t2134102846 * ___x_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetPosition::y
	FsmFloat_t2134102846 * ___y_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetPosition::z
	FsmFloat_t2134102846 * ___z_13;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.SetPosition::space
	int32_t ___space_14;
	// System.Boolean HutongGames.PlayMaker.Actions.SetPosition::everyFrame
	bool ___everyFrame_15;
	// System.Boolean HutongGames.PlayMaker.Actions.SetPosition::lateUpdate
	bool ___lateUpdate_16;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(SetPosition_t2319285817, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_vector_10() { return static_cast<int32_t>(offsetof(SetPosition_t2319285817, ___vector_10)); }
	inline FsmVector3_t533912882 * get_vector_10() const { return ___vector_10; }
	inline FsmVector3_t533912882 ** get_address_of_vector_10() { return &___vector_10; }
	inline void set_vector_10(FsmVector3_t533912882 * value)
	{
		___vector_10 = value;
		Il2CppCodeGenWriteBarrier(&___vector_10, value);
	}

	inline static int32_t get_offset_of_x_11() { return static_cast<int32_t>(offsetof(SetPosition_t2319285817, ___x_11)); }
	inline FsmFloat_t2134102846 * get_x_11() const { return ___x_11; }
	inline FsmFloat_t2134102846 ** get_address_of_x_11() { return &___x_11; }
	inline void set_x_11(FsmFloat_t2134102846 * value)
	{
		___x_11 = value;
		Il2CppCodeGenWriteBarrier(&___x_11, value);
	}

	inline static int32_t get_offset_of_y_12() { return static_cast<int32_t>(offsetof(SetPosition_t2319285817, ___y_12)); }
	inline FsmFloat_t2134102846 * get_y_12() const { return ___y_12; }
	inline FsmFloat_t2134102846 ** get_address_of_y_12() { return &___y_12; }
	inline void set_y_12(FsmFloat_t2134102846 * value)
	{
		___y_12 = value;
		Il2CppCodeGenWriteBarrier(&___y_12, value);
	}

	inline static int32_t get_offset_of_z_13() { return static_cast<int32_t>(offsetof(SetPosition_t2319285817, ___z_13)); }
	inline FsmFloat_t2134102846 * get_z_13() const { return ___z_13; }
	inline FsmFloat_t2134102846 ** get_address_of_z_13() { return &___z_13; }
	inline void set_z_13(FsmFloat_t2134102846 * value)
	{
		___z_13 = value;
		Il2CppCodeGenWriteBarrier(&___z_13, value);
	}

	inline static int32_t get_offset_of_space_14() { return static_cast<int32_t>(offsetof(SetPosition_t2319285817, ___space_14)); }
	inline int32_t get_space_14() const { return ___space_14; }
	inline int32_t* get_address_of_space_14() { return &___space_14; }
	inline void set_space_14(int32_t value)
	{
		___space_14 = value;
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(SetPosition_t2319285817, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}

	inline static int32_t get_offset_of_lateUpdate_16() { return static_cast<int32_t>(offsetof(SetPosition_t2319285817, ___lateUpdate_16)); }
	inline bool get_lateUpdate_16() const { return ___lateUpdate_16; }
	inline bool* get_address_of_lateUpdate_16() { return &___lateUpdate_16; }
	inline void set_lateUpdate_16(bool value)
	{
		___lateUpdate_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
