﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetScale
struct  SetScale_t1151369904  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetScale::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SetScale::vector
	FsmVector3_t533912882 * ___vector_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetScale::x
	FsmFloat_t2134102846 * ___x_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetScale::y
	FsmFloat_t2134102846 * ___y_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetScale::z
	FsmFloat_t2134102846 * ___z_13;
	// System.Boolean HutongGames.PlayMaker.Actions.SetScale::everyFrame
	bool ___everyFrame_14;
	// System.Boolean HutongGames.PlayMaker.Actions.SetScale::lateUpdate
	bool ___lateUpdate_15;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(SetScale_t1151369904, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_vector_10() { return static_cast<int32_t>(offsetof(SetScale_t1151369904, ___vector_10)); }
	inline FsmVector3_t533912882 * get_vector_10() const { return ___vector_10; }
	inline FsmVector3_t533912882 ** get_address_of_vector_10() { return &___vector_10; }
	inline void set_vector_10(FsmVector3_t533912882 * value)
	{
		___vector_10 = value;
		Il2CppCodeGenWriteBarrier(&___vector_10, value);
	}

	inline static int32_t get_offset_of_x_11() { return static_cast<int32_t>(offsetof(SetScale_t1151369904, ___x_11)); }
	inline FsmFloat_t2134102846 * get_x_11() const { return ___x_11; }
	inline FsmFloat_t2134102846 ** get_address_of_x_11() { return &___x_11; }
	inline void set_x_11(FsmFloat_t2134102846 * value)
	{
		___x_11 = value;
		Il2CppCodeGenWriteBarrier(&___x_11, value);
	}

	inline static int32_t get_offset_of_y_12() { return static_cast<int32_t>(offsetof(SetScale_t1151369904, ___y_12)); }
	inline FsmFloat_t2134102846 * get_y_12() const { return ___y_12; }
	inline FsmFloat_t2134102846 ** get_address_of_y_12() { return &___y_12; }
	inline void set_y_12(FsmFloat_t2134102846 * value)
	{
		___y_12 = value;
		Il2CppCodeGenWriteBarrier(&___y_12, value);
	}

	inline static int32_t get_offset_of_z_13() { return static_cast<int32_t>(offsetof(SetScale_t1151369904, ___z_13)); }
	inline FsmFloat_t2134102846 * get_z_13() const { return ___z_13; }
	inline FsmFloat_t2134102846 ** get_address_of_z_13() { return &___z_13; }
	inline void set_z_13(FsmFloat_t2134102846 * value)
	{
		___z_13 = value;
		Il2CppCodeGenWriteBarrier(&___z_13, value);
	}

	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(SetScale_t1151369904, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}

	inline static int32_t get_offset_of_lateUpdate_15() { return static_cast<int32_t>(offsetof(SetScale_t1151369904, ___lateUpdate_15)); }
	inline bool get_lateUpdate_15() const { return ___lateUpdate_15; }
	inline bool* get_address_of_lateUpdate_15() { return &___lateUpdate_15; }
	inline void set_lateUpdate_15(bool value)
	{
		___lateUpdate_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
