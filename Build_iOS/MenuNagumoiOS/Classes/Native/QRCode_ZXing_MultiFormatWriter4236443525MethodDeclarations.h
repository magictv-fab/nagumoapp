﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.MultiFormatWriter
struct MultiFormatWriter_t4236443525;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// System.String
struct String_t;
// System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>
struct IDictionary_2_t3297010830;
// ZXing.Writer
struct Writer_t2765575657;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "QRCode_ZXing_BarcodeFormat4201805817.h"

// System.Void ZXing.MultiFormatWriter::.cctor()
extern "C"  void MultiFormatWriter__cctor_m95278715 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.BitMatrix ZXing.MultiFormatWriter::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern "C"  BitMatrix_t1058711404 * MultiFormatWriter_encode_m3538411572 (MultiFormatWriter_t4236443525 * __this, String_t* ___contents0, int32_t ___format1, int32_t ___width2, int32_t ___height3, Il2CppObject* ___hints4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.MultiFormatWriter::.ctor()
extern "C"  void MultiFormatWriter__ctor_m434266994 (MultiFormatWriter_t4236443525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Writer ZXing.MultiFormatWriter::<.cctor>b__1()
extern "C"  Il2CppObject * MultiFormatWriter_U3C_cctorU3Eb__1_m185596794 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Writer ZXing.MultiFormatWriter::<.cctor>b__2()
extern "C"  Il2CppObject * MultiFormatWriter_U3C_cctorU3Eb__2_m185597755 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Writer ZXing.MultiFormatWriter::<.cctor>b__3()
extern "C"  Il2CppObject * MultiFormatWriter_U3C_cctorU3Eb__3_m185598716 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Writer ZXing.MultiFormatWriter::<.cctor>b__4()
extern "C"  Il2CppObject * MultiFormatWriter_U3C_cctorU3Eb__4_m185599677 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Writer ZXing.MultiFormatWriter::<.cctor>b__5()
extern "C"  Il2CppObject * MultiFormatWriter_U3C_cctorU3Eb__5_m185600638 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Writer ZXing.MultiFormatWriter::<.cctor>b__6()
extern "C"  Il2CppObject * MultiFormatWriter_U3C_cctorU3Eb__6_m185601599 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Writer ZXing.MultiFormatWriter::<.cctor>b__7()
extern "C"  Il2CppObject * MultiFormatWriter_U3C_cctorU3Eb__7_m185602560 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Writer ZXing.MultiFormatWriter::<.cctor>b__8()
extern "C"  Il2CppObject * MultiFormatWriter_U3C_cctorU3Eb__8_m185603521 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Writer ZXing.MultiFormatWriter::<.cctor>b__9()
extern "C"  Il2CppObject * MultiFormatWriter_U3C_cctorU3Eb__9_m185604482 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Writer ZXing.MultiFormatWriter::<.cctor>b__a()
extern "C"  Il2CppObject * MultiFormatWriter_U3C_cctorU3Eb__a_m185642922 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Writer ZXing.MultiFormatWriter::<.cctor>b__b()
extern "C"  Il2CppObject * MultiFormatWriter_U3C_cctorU3Eb__b_m185643883 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Writer ZXing.MultiFormatWriter::<.cctor>b__c()
extern "C"  Il2CppObject * MultiFormatWriter_U3C_cctorU3Eb__c_m185644844 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Writer ZXing.MultiFormatWriter::<.cctor>b__d()
extern "C"  Il2CppObject * MultiFormatWriter_U3C_cctorU3Eb__d_m185645805 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
