﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>
struct ChangeNotifyDictionary_2_t1676496957;
// System.Action`2<System.Object,System.EventArgs>
struct Action_2_t2663079113;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t770439062;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t2483180780;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t3856534026;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977.h"

// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::add_ValueChanged(System.Action`2<System.Object,System.EventArgs>)
extern "C"  void ChangeNotifyDictionary_2_add_ValueChanged_m2227302673_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, Action_2_t2663079113 * ___value0, const MethodInfo* method);
#define ChangeNotifyDictionary_2_add_ValueChanged_m2227302673(__this, ___value0, method) ((  void (*) (ChangeNotifyDictionary_2_t1676496957 *, Action_2_t2663079113 *, const MethodInfo*))ChangeNotifyDictionary_2_add_ValueChanged_m2227302673_gshared)(__this, ___value0, method)
// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::remove_ValueChanged(System.Action`2<System.Object,System.EventArgs>)
extern "C"  void ChangeNotifyDictionary_2_remove_ValueChanged_m1419530924_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, Action_2_t2663079113 * ___value0, const MethodInfo* method);
#define ChangeNotifyDictionary_2_remove_ValueChanged_m1419530924(__this, ___value0, method) ((  void (*) (ChangeNotifyDictionary_2_t1676496957 *, Action_2_t2663079113 *, const MethodInfo*))ChangeNotifyDictionary_2_remove_ValueChanged_m1419530924_gshared)(__this, ___value0, method)
// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::.ctor()
extern "C"  void ChangeNotifyDictionary_2__ctor_m1576909575_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, const MethodInfo* method);
#define ChangeNotifyDictionary_2__ctor_m1576909575(__this, method) ((  void (*) (ChangeNotifyDictionary_2_t1676496957 *, const MethodInfo*))ChangeNotifyDictionary_2__ctor_m1576909575_gshared)(__this, method)
// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::OnValueChanged()
extern "C"  void ChangeNotifyDictionary_2_OnValueChanged_m2036333919_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, const MethodInfo* method);
#define ChangeNotifyDictionary_2_OnValueChanged_m2036333919(__this, method) ((  void (*) (ChangeNotifyDictionary_2_t1676496957 *, const MethodInfo*))ChangeNotifyDictionary_2_OnValueChanged_m2036333919_gshared)(__this, method)
// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::Add(TKey,TValue)
extern "C"  void ChangeNotifyDictionary_2_Add_m3461214606_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define ChangeNotifyDictionary_2_Add_m3461214606(__this, ___key0, ___value1, method) ((  void (*) (ChangeNotifyDictionary_2_t1676496957 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))ChangeNotifyDictionary_2_Add_m3461214606_gshared)(__this, ___key0, ___value1, method)
// System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::ContainsKey(TKey)
extern "C"  bool ChangeNotifyDictionary_2_ContainsKey_m512756582_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define ChangeNotifyDictionary_2_ContainsKey_m512756582(__this, ___key0, method) ((  bool (*) (ChangeNotifyDictionary_2_t1676496957 *, Il2CppObject *, const MethodInfo*))ChangeNotifyDictionary_2_ContainsKey_m512756582_gshared)(__this, ___key0, method)
// System.Collections.Generic.ICollection`1<TKey> ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::get_Keys()
extern "C"  Il2CppObject* ChangeNotifyDictionary_2_get_Keys_m3793725911_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, const MethodInfo* method);
#define ChangeNotifyDictionary_2_get_Keys_m3793725911(__this, method) ((  Il2CppObject* (*) (ChangeNotifyDictionary_2_t1676496957 *, const MethodInfo*))ChangeNotifyDictionary_2_get_Keys_m3793725911_gshared)(__this, method)
// System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::Remove(TKey)
extern "C"  bool ChangeNotifyDictionary_2_Remove_m1365517770_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define ChangeNotifyDictionary_2_Remove_m1365517770(__this, ___key0, method) ((  bool (*) (ChangeNotifyDictionary_2_t1676496957 *, Il2CppObject *, const MethodInfo*))ChangeNotifyDictionary_2_Remove_m1365517770_gshared)(__this, ___key0, method)
// System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool ChangeNotifyDictionary_2_TryGetValue_m2720374207_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, Il2CppObject * ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define ChangeNotifyDictionary_2_TryGetValue_m2720374207(__this, ___key0, ___value1, method) ((  bool (*) (ChangeNotifyDictionary_2_t1676496957 *, Il2CppObject *, Il2CppObject **, const MethodInfo*))ChangeNotifyDictionary_2_TryGetValue_m2720374207_gshared)(__this, ___key0, ___value1, method)
// TValue ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * ChangeNotifyDictionary_2_get_Item_m1152805902_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define ChangeNotifyDictionary_2_get_Item_m1152805902(__this, ___key0, method) ((  Il2CppObject * (*) (ChangeNotifyDictionary_2_t1676496957 *, Il2CppObject *, const MethodInfo*))ChangeNotifyDictionary_2_get_Item_m1152805902_gshared)(__this, ___key0, method)
// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::set_Item(TKey,TValue)
extern "C"  void ChangeNotifyDictionary_2_set_Item_m986617991_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define ChangeNotifyDictionary_2_set_Item_m986617991(__this, ___key0, ___value1, method) ((  void (*) (ChangeNotifyDictionary_2_t1676496957 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))ChangeNotifyDictionary_2_set_Item_m986617991_gshared)(__this, ___key0, ___value1, method)
// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void ChangeNotifyDictionary_2_Add_m4097002519_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, KeyValuePair_2_t1944668977  ___item0, const MethodInfo* method);
#define ChangeNotifyDictionary_2_Add_m4097002519(__this, ___item0, method) ((  void (*) (ChangeNotifyDictionary_2_t1676496957 *, KeyValuePair_2_t1944668977 , const MethodInfo*))ChangeNotifyDictionary_2_Add_m4097002519_gshared)(__this, ___item0, method)
// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::Clear()
extern "C"  void ChangeNotifyDictionary_2_Clear_m3278010162_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, const MethodInfo* method);
#define ChangeNotifyDictionary_2_Clear_m3278010162(__this, method) ((  void (*) (ChangeNotifyDictionary_2_t1676496957 *, const MethodInfo*))ChangeNotifyDictionary_2_Clear_m3278010162_gshared)(__this, method)
// System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool ChangeNotifyDictionary_2_Contains_m3248197057_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, KeyValuePair_2_t1944668977  ___item0, const MethodInfo* method);
#define ChangeNotifyDictionary_2_Contains_m3248197057(__this, ___item0, method) ((  bool (*) (ChangeNotifyDictionary_2_t1676496957 *, KeyValuePair_2_t1944668977 , const MethodInfo*))ChangeNotifyDictionary_2_Contains_m3248197057_gshared)(__this, ___item0, method)
// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void ChangeNotifyDictionary_2_CopyTo_m1239680059_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, KeyValuePair_2U5BU5D_t2483180780* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define ChangeNotifyDictionary_2_CopyTo_m1239680059(__this, ___array0, ___arrayIndex1, method) ((  void (*) (ChangeNotifyDictionary_2_t1676496957 *, KeyValuePair_2U5BU5D_t2483180780*, int32_t, const MethodInfo*))ChangeNotifyDictionary_2_CopyTo_m1239680059_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Int32 ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::get_Count()
extern "C"  int32_t ChangeNotifyDictionary_2_get_Count_m4278201981_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, const MethodInfo* method);
#define ChangeNotifyDictionary_2_get_Count_m4278201981(__this, method) ((  int32_t (*) (ChangeNotifyDictionary_2_t1676496957 *, const MethodInfo*))ChangeNotifyDictionary_2_get_Count_m4278201981_gshared)(__this, method)
// System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::get_IsReadOnly()
extern "C"  bool ChangeNotifyDictionary_2_get_IsReadOnly_m90599750_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, const MethodInfo* method);
#define ChangeNotifyDictionary_2_get_IsReadOnly_m90599750(__this, method) ((  bool (*) (ChangeNotifyDictionary_2_t1676496957 *, const MethodInfo*))ChangeNotifyDictionary_2_get_IsReadOnly_m90599750_gshared)(__this, method)
// System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool ChangeNotifyDictionary_2_Remove_m2592354598_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, KeyValuePair_2_t1944668977  ___item0, const MethodInfo* method);
#define ChangeNotifyDictionary_2_Remove_m2592354598(__this, ___item0, method) ((  bool (*) (ChangeNotifyDictionary_2_t1676496957 *, KeyValuePair_2_t1944668977 , const MethodInfo*))ChangeNotifyDictionary_2_Remove_m2592354598_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ChangeNotifyDictionary_2_GetEnumerator_m3718556676_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, const MethodInfo* method);
#define ChangeNotifyDictionary_2_GetEnumerator_m3718556676(__this, method) ((  Il2CppObject* (*) (ChangeNotifyDictionary_2_t1676496957 *, const MethodInfo*))ChangeNotifyDictionary_2_GetEnumerator_m3718556676_gshared)(__this, method)
// System.Collections.IEnumerator ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ChangeNotifyDictionary_2_System_Collections_IEnumerable_GetEnumerator_m3495645002_gshared (ChangeNotifyDictionary_2_t1676496957 * __this, const MethodInfo* method);
#define ChangeNotifyDictionary_2_System_Collections_IEnumerable_GetEnumerator_m3495645002(__this, method) ((  Il2CppObject * (*) (ChangeNotifyDictionary_2_t1676496957 *, const MethodInfo*))ChangeNotifyDictionary_2_System_Collections_IEnumerable_GetEnumerator_m3495645002_gshared)(__this, method)
