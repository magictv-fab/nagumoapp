﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UseGUILayout
struct  UseGUILayout_t3603837094  : public FsmStateAction_t2366529033
{
public:
	// System.Boolean HutongGames.PlayMaker.Actions.UseGUILayout::turnOffGUIlayout
	bool ___turnOffGUIlayout_9;

public:
	inline static int32_t get_offset_of_turnOffGUIlayout_9() { return static_cast<int32_t>(offsetof(UseGUILayout_t3603837094, ___turnOffGUIlayout_9)); }
	inline bool get_turnOffGUIlayout_9() const { return ___turnOffGUIlayout_9; }
	inline bool* get_address_of_turnOffGUIlayout_9() { return &___turnOffGUIlayout_9; }
	inline void set_turnOffGUIlayout_9(bool value)
	{
		___turnOffGUIlayout_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
