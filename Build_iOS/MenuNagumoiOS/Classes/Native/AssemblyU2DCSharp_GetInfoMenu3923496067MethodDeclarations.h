﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GetInfoMenu
struct GetInfoMenu_t3923496067;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.Sprite
struct Sprite_t3199167241;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"

// System.Void GetInfoMenu::.ctor()
extern "C"  void GetInfoMenu__ctor_m3614052936 (GetInfoMenu_t3923496067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GetInfoMenu::Start()
extern "C"  void GetInfoMenu_Start_m2561190728 (GetInfoMenu_t3923496067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GetInfoMenu::ILoadImg()
extern "C"  Il2CppObject * GetInfoMenu_ILoadImg_m3112489096 (GetInfoMenu_t3923496067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite GetInfoMenu::CropImage(UnityEngine.Texture2D)
extern "C"  Sprite_t3199167241 * GetInfoMenu_CropImage_m940466762 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___texture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
