﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoadLevelName/<GoLevelAdditive>c__Iterator61
struct U3CGoLevelAdditiveU3Ec__Iterator61_t2168713120;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void LoadLevelName/<GoLevelAdditive>c__Iterator61::.ctor()
extern "C"  void U3CGoLevelAdditiveU3Ec__Iterator61__ctor_m1819883259 (U3CGoLevelAdditiveU3Ec__Iterator61_t2168713120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LoadLevelName/<GoLevelAdditive>c__Iterator61::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGoLevelAdditiveU3Ec__Iterator61_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1449886273 (U3CGoLevelAdditiveU3Ec__Iterator61_t2168713120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LoadLevelName/<GoLevelAdditive>c__Iterator61::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGoLevelAdditiveU3Ec__Iterator61_System_Collections_IEnumerator_get_Current_m3108251605 (U3CGoLevelAdditiveU3Ec__Iterator61_t2168713120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LoadLevelName/<GoLevelAdditive>c__Iterator61::MoveNext()
extern "C"  bool U3CGoLevelAdditiveU3Ec__Iterator61_MoveNext_m201071361 (U3CGoLevelAdditiveU3Ec__Iterator61_t2168713120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadLevelName/<GoLevelAdditive>c__Iterator61::Dispose()
extern "C"  void U3CGoLevelAdditiveU3Ec__Iterator61_Dispose_m754405496 (U3CGoLevelAdditiveU3Ec__Iterator61_t2168713120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadLevelName/<GoLevelAdditive>c__Iterator61::Reset()
extern "C"  void U3CGoLevelAdditiveU3Ec__Iterator61_Reset_m3761283496 (U3CGoLevelAdditiveU3Ec__Iterator61_t2168713120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
