﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.LuminanceSource
struct LuminanceSource_t1231523093;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void ZXing.LuminanceSource::.ctor(System.Int32,System.Int32)
extern "C"  void LuminanceSource__ctor_m4015573892 (LuminanceSource_t1231523093 * __this, int32_t ___width0, int32_t ___height1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.LuminanceSource::get_Width()
extern "C"  int32_t LuminanceSource_get_Width_m3506554795 (LuminanceSource_t1231523093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.LuminanceSource::get_Height()
extern "C"  int32_t LuminanceSource_get_Height_m1832830596 (LuminanceSource_t1231523093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.LuminanceSource::get_RotateSupported()
extern "C"  bool LuminanceSource_get_RotateSupported_m3606339294 (LuminanceSource_t1231523093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.LuminanceSource ZXing.LuminanceSource::rotateCounterClockwise()
extern "C"  LuminanceSource_t1231523093 * LuminanceSource_rotateCounterClockwise_m2337680361 (LuminanceSource_t1231523093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.LuminanceSource::get_InversionSupported()
extern "C"  bool LuminanceSource_get_InversionSupported_m2206387922 (LuminanceSource_t1231523093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.LuminanceSource ZXing.LuminanceSource::invert()
extern "C"  LuminanceSource_t1231523093 * LuminanceSource_invert_m686257646 (LuminanceSource_t1231523093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.LuminanceSource::ToString()
extern "C"  String_t* LuminanceSource_ToString_m2681404491 (LuminanceSource_t1231523093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
