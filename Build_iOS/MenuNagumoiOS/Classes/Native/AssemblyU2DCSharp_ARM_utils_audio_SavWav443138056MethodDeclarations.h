﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// UnityEngine.AudioClip
struct AudioClip_t794140988;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t1365137228;
// System.IO.MemoryStream
struct MemoryStream_t418716369;
// System.IO.FileStream
struct FileStream_t2141505868;
// System.IO.Stream
struct Stream_t1561764144;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_AudioClip794140988.h"
#include "mscorlib_System_IO_Stream1561764144.h"

// System.Boolean ARM.utils.audio.SavWav::Save(System.String,UnityEngine.AudioClip)
extern "C"  bool SavWav_Save_m809415130 (Il2CppObject * __this /* static, unused */, String_t* ___filename0, AudioClip_t794140988 * ___clip1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip ARM.utils.audio.SavWav::TrimSilence(UnityEngine.AudioClip,System.Single)
extern "C"  AudioClip_t794140988 * SavWav_TrimSilence_m3524780639 (Il2CppObject * __this /* static, unused */, AudioClip_t794140988 * ___clip0, float ___min1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip ARM.utils.audio.SavWav::TrimSilence(System.Collections.Generic.List`1<System.Single>,System.Single,System.Int32,System.Int32)
extern "C"  AudioClip_t794140988 * SavWav_TrimSilence_m635790585 (Il2CppObject * __this /* static, unused */, List_1_t1365137228 * ___samples0, float ___min1, int32_t ___channels2, int32_t ___hz3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip ARM.utils.audio.SavWav::TrimSilence(System.Collections.Generic.List`1<System.Single>,System.Single,System.Int32,System.Int32,System.Boolean,System.Boolean)
extern "C"  AudioClip_t794140988 * SavWav_TrimSilence_m1391600025 (Il2CppObject * __this /* static, unused */, List_1_t1365137228 * ___samples0, float ___min1, int32_t ___channels2, int32_t ___hz3, bool ____3D4, bool ___stream5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.MemoryStream ARM.utils.audio.SavWav::CreateEmptyInMemoryStream()
extern "C"  MemoryStream_t418716369 * SavWav_CreateEmptyInMemoryStream_m376870258 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileStream ARM.utils.audio.SavWav::CreateEmpty(System.String)
extern "C"  FileStream_t2141505868 * SavWav_CreateEmpty_m252007675 (Il2CppObject * __this /* static, unused */, String_t* ___filepath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.audio.SavWav::ConvertAndWrite(System.IO.Stream,UnityEngine.AudioClip)
extern "C"  void SavWav_ConvertAndWrite_m2948613501 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___stream0, AudioClip_t794140988 * ___clip1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.audio.SavWav::WriteHeader(System.IO.Stream,UnityEngine.AudioClip)
extern "C"  void SavWav_WriteHeader_m92792622 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___stream0, AudioClip_t794140988 * ___clip1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
