﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Common.CharacterSetECI
struct CharacterSetECI_t2542265168;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.String ZXing.Common.CharacterSetECI::get_EncodingName()
extern "C"  String_t* CharacterSetECI_get_EncodingName_m132210412 (CharacterSetECI_t2542265168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.CharacterSetECI::.cctor()
extern "C"  void CharacterSetECI__cctor_m179319851 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.CharacterSetECI::.ctor(System.Int32,System.String)
extern "C"  void CharacterSetECI__ctor_m3114417423 (CharacterSetECI_t2542265168 * __this, int32_t ___value0, String_t* ___encodingName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.CharacterSetECI::addCharacterSet(System.Int32,System.String)
extern "C"  void CharacterSetECI_addCharacterSet_m2728250055 (Il2CppObject * __this /* static, unused */, int32_t ___value0, String_t* ___encodingName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.CharacterSetECI::addCharacterSet(System.Int32,System.String[])
extern "C"  void CharacterSetECI_addCharacterSet_m1918303269 (Il2CppObject * __this /* static, unused */, int32_t ___value0, StringU5BU5D_t4054002952* ___encodingNames1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.CharacterSetECI ZXing.Common.CharacterSetECI::getCharacterSetECIByValue(System.Int32)
extern "C"  CharacterSetECI_t2542265168 * CharacterSetECI_getCharacterSetECIByValue_m1724378987 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.CharacterSetECI ZXing.Common.CharacterSetECI::getCharacterSetECIByName(System.String)
extern "C"  CharacterSetECI_t2542265168 * CharacterSetECI_getCharacterSetECIByName_m913193118 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
