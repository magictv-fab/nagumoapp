﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>
struct Dictionary_2_t3931530887;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1863364295.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.NetworkReachability,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m159719633_gshared (Enumerator_t1863364295 * __this, Dictionary_2_t3931530887 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m159719633(__this, ___host0, method) ((  void (*) (Enumerator_t1863364295 *, Dictionary_2_t3931530887 *, const MethodInfo*))Enumerator__ctor_m159719633_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.NetworkReachability,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2530983728_gshared (Enumerator_t1863364295 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2530983728(__this, method) ((  Il2CppObject * (*) (Enumerator_t1863364295 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2530983728_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.NetworkReachability,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1456893764_gshared (Enumerator_t1863364295 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1456893764(__this, method) ((  void (*) (Enumerator_t1863364295 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1456893764_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.NetworkReachability,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m141536691_gshared (Enumerator_t1863364295 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m141536691(__this, method) ((  void (*) (Enumerator_t1863364295 *, const MethodInfo*))Enumerator_Dispose_m141536691_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.NetworkReachability,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3624735408_gshared (Enumerator_t1863364295 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3624735408(__this, method) ((  bool (*) (Enumerator_t1863364295 *, const MethodInfo*))Enumerator_MoveNext_m3624735408_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.NetworkReachability,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m3984938706_gshared (Enumerator_t1863364295 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3984938706(__this, method) ((  Il2CppObject * (*) (Enumerator_t1863364295 *, const MethodInfo*))Enumerator_get_Current_m3984938706_gshared)(__this, method)
