﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmMaterial
struct FsmMaterial_t924399665;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RebuildTextures
struct  RebuildTextures_t3727549697  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Actions.RebuildTextures::substanceMaterial
	FsmMaterial_t924399665 * ___substanceMaterial_9;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.RebuildTextures::immediately
	FsmBool_t1075959796 * ___immediately_10;
	// System.Boolean HutongGames.PlayMaker.Actions.RebuildTextures::everyFrame
	bool ___everyFrame_11;

public:
	inline static int32_t get_offset_of_substanceMaterial_9() { return static_cast<int32_t>(offsetof(RebuildTextures_t3727549697, ___substanceMaterial_9)); }
	inline FsmMaterial_t924399665 * get_substanceMaterial_9() const { return ___substanceMaterial_9; }
	inline FsmMaterial_t924399665 ** get_address_of_substanceMaterial_9() { return &___substanceMaterial_9; }
	inline void set_substanceMaterial_9(FsmMaterial_t924399665 * value)
	{
		___substanceMaterial_9 = value;
		Il2CppCodeGenWriteBarrier(&___substanceMaterial_9, value);
	}

	inline static int32_t get_offset_of_immediately_10() { return static_cast<int32_t>(offsetof(RebuildTextures_t3727549697, ___immediately_10)); }
	inline FsmBool_t1075959796 * get_immediately_10() const { return ___immediately_10; }
	inline FsmBool_t1075959796 ** get_address_of_immediately_10() { return &___immediately_10; }
	inline void set_immediately_10(FsmBool_t1075959796 * value)
	{
		___immediately_10 = value;
		Il2CppCodeGenWriteBarrier(&___immediately_10, value);
	}

	inline static int32_t get_offset_of_everyFrame_11() { return static_cast<int32_t>(offsetof(RebuildTextures_t3727549697, ___everyFrame_11)); }
	inline bool get_everyFrame_11() const { return ___everyFrame_11; }
	inline bool* get_address_of_everyFrame_11() { return &___everyFrame_11; }
	inline void set_everyFrame_11(bool value)
	{
		___everyFrame_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
