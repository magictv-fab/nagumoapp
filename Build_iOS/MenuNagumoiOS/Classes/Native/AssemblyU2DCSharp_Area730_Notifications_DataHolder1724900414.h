﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Area730.Notifications.NotificationInstance>
struct List_1_t947156856;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_ScriptableObject2970544072.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Area730.Notifications.DataHolder
struct  DataHolder_t1724900414  : public ScriptableObject_t2970544072
{
public:
	// System.Collections.Generic.List`1<Area730.Notifications.NotificationInstance> Area730.Notifications.DataHolder::notifications
	List_1_t947156856 * ___notifications_2;
	// System.String Area730.Notifications.DataHolder::unityClass
	String_t* ___unityClass_3;

public:
	inline static int32_t get_offset_of_notifications_2() { return static_cast<int32_t>(offsetof(DataHolder_t1724900414, ___notifications_2)); }
	inline List_1_t947156856 * get_notifications_2() const { return ___notifications_2; }
	inline List_1_t947156856 ** get_address_of_notifications_2() { return &___notifications_2; }
	inline void set_notifications_2(List_1_t947156856 * value)
	{
		___notifications_2 = value;
		Il2CppCodeGenWriteBarrier(&___notifications_2, value);
	}

	inline static int32_t get_offset_of_unityClass_3() { return static_cast<int32_t>(offsetof(DataHolder_t1724900414, ___unityClass_3)); }
	inline String_t* get_unityClass_3() const { return ___unityClass_3; }
	inline String_t** get_address_of_unityClass_3() { return &___unityClass_3; }
	inline void set_unityClass_3(String_t* value)
	{
		___unityClass_3 = value;
		Il2CppCodeGenWriteBarrier(&___unityClass_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
