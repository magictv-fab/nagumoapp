﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DrawDebugLine
struct DrawDebugLine_t2835919313;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DrawDebugLine::.ctor()
extern "C"  void DrawDebugLine__ctor_m2302876293 (DrawDebugLine_t2835919313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DrawDebugLine::Reset()
extern "C"  void DrawDebugLine_Reset_m4244276530 (DrawDebugLine_t2835919313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DrawDebugLine::OnUpdate()
extern "C"  void DrawDebugLine_OnUpdate_m3227008167 (DrawDebugLine_t2835919313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
