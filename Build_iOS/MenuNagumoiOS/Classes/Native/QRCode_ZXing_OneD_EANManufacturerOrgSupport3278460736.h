﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.Int32[]>
struct List_1_t304066077;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.EANManufacturerOrgSupport
struct  EANManufacturerOrgSupport_t3278460736  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.Int32[]> ZXing.OneD.EANManufacturerOrgSupport::ranges
	List_1_t304066077 * ___ranges_0;
	// System.Collections.Generic.List`1<System.String> ZXing.OneD.EANManufacturerOrgSupport::countryIdentifiers
	List_1_t1375417109 * ___countryIdentifiers_1;

public:
	inline static int32_t get_offset_of_ranges_0() { return static_cast<int32_t>(offsetof(EANManufacturerOrgSupport_t3278460736, ___ranges_0)); }
	inline List_1_t304066077 * get_ranges_0() const { return ___ranges_0; }
	inline List_1_t304066077 ** get_address_of_ranges_0() { return &___ranges_0; }
	inline void set_ranges_0(List_1_t304066077 * value)
	{
		___ranges_0 = value;
		Il2CppCodeGenWriteBarrier(&___ranges_0, value);
	}

	inline static int32_t get_offset_of_countryIdentifiers_1() { return static_cast<int32_t>(offsetof(EANManufacturerOrgSupport_t3278460736, ___countryIdentifiers_1)); }
	inline List_1_t1375417109 * get_countryIdentifiers_1() const { return ___countryIdentifiers_1; }
	inline List_1_t1375417109 ** get_address_of_countryIdentifiers_1() { return &___countryIdentifiers_1; }
	inline void set_countryIdentifiers_1(List_1_t1375417109 * value)
	{
		___countryIdentifiers_1 = value;
		Il2CppCodeGenWriteBarrier(&___countryIdentifiers_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
