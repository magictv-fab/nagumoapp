﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// <PrivateImplementationDetails>{A1462213-BC44-4626-98E1-2B98D236945C}/__StaticArrayInitTypeSize=10
struct __StaticArrayInitTypeSizeU3D10_t3501907541;
struct __StaticArrayInitTypeSizeU3D10_t3501907541_marshaled_pinvoke;
struct __StaticArrayInitTypeSizeU3D10_t3501907541_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct __StaticArrayInitTypeSizeU3D10_t3501907541;
struct __StaticArrayInitTypeSizeU3D10_t3501907541_marshaled_pinvoke;

extern "C" void __StaticArrayInitTypeSizeU3D10_t3501907541_marshal_pinvoke(const __StaticArrayInitTypeSizeU3D10_t3501907541& unmarshaled, __StaticArrayInitTypeSizeU3D10_t3501907541_marshaled_pinvoke& marshaled);
extern "C" void __StaticArrayInitTypeSizeU3D10_t3501907541_marshal_pinvoke_back(const __StaticArrayInitTypeSizeU3D10_t3501907541_marshaled_pinvoke& marshaled, __StaticArrayInitTypeSizeU3D10_t3501907541& unmarshaled);
extern "C" void __StaticArrayInitTypeSizeU3D10_t3501907541_marshal_pinvoke_cleanup(__StaticArrayInitTypeSizeU3D10_t3501907541_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct __StaticArrayInitTypeSizeU3D10_t3501907541;
struct __StaticArrayInitTypeSizeU3D10_t3501907541_marshaled_com;

extern "C" void __StaticArrayInitTypeSizeU3D10_t3501907541_marshal_com(const __StaticArrayInitTypeSizeU3D10_t3501907541& unmarshaled, __StaticArrayInitTypeSizeU3D10_t3501907541_marshaled_com& marshaled);
extern "C" void __StaticArrayInitTypeSizeU3D10_t3501907541_marshal_com_back(const __StaticArrayInitTypeSizeU3D10_t3501907541_marshaled_com& marshaled, __StaticArrayInitTypeSizeU3D10_t3501907541& unmarshaled);
extern "C" void __StaticArrayInitTypeSizeU3D10_t3501907541_marshal_com_cleanup(__StaticArrayInitTypeSizeU3D10_t3501907541_marshaled_com& marshaled);
