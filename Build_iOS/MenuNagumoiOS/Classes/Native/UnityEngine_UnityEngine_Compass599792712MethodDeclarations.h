﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Compass
struct Compass_t599792712;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine.Compass::.ctor()
extern "C"  void Compass__ctor_m2401581447 (Compass_t599792712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Compass::get_magneticHeading()
extern "C"  float Compass_get_magneticHeading_m2817211142 (Compass_t599792712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Compass::get_rawVector()
extern "C"  Vector3_t4282066566  Compass_get_rawVector_m4065151393 (Compass_t599792712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Compass::INTERNAL_get_rawVector(UnityEngine.Vector3&)
extern "C"  void Compass_INTERNAL_get_rawVector_m2650062384 (Compass_t599792712 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double UnityEngine.Compass::get_timestamp()
extern "C"  double Compass_get_timestamp_m1013803541 (Compass_t599792712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Compass::get_enabled()
extern "C"  bool Compass_get_enabled_m2328786563 (Compass_t599792712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Compass::set_enabled(System.Boolean)
extern "C"  void Compass_set_enabled_m3741384928 (Compass_t599792712 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
