﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Object>
struct Dictionary_2_t3931530887;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En953886983.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23830311593.h"
#include "UnityEngine_UnityEngine_NetworkReachability612403035.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.NetworkReachability,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3574981521_gshared (Enumerator_t953886983 * __this, Dictionary_2_t3931530887 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m3574981521(__this, ___dictionary0, method) ((  void (*) (Enumerator_t953886983 *, Dictionary_2_t3931530887 *, const MethodInfo*))Enumerator__ctor_m3574981521_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.NetworkReachability,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1417087600_gshared (Enumerator_t953886983 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1417087600(__this, method) ((  Il2CppObject * (*) (Enumerator_t953886983 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1417087600_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.NetworkReachability,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3040441476_gshared (Enumerator_t953886983 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3040441476(__this, method) ((  void (*) (Enumerator_t953886983 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3040441476_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.NetworkReachability,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1245982669_gshared (Enumerator_t953886983 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1245982669(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t953886983 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1245982669_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.NetworkReachability,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3486561100_gshared (Enumerator_t953886983 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3486561100(__this, method) ((  Il2CppObject * (*) (Enumerator_t953886983 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3486561100_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.NetworkReachability,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1559162590_gshared (Enumerator_t953886983 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1559162590(__this, method) ((  Il2CppObject * (*) (Enumerator_t953886983 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1559162590_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.NetworkReachability,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3297031152_gshared (Enumerator_t953886983 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3297031152(__this, method) ((  bool (*) (Enumerator_t953886983 *, const MethodInfo*))Enumerator_MoveNext_m3297031152_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.NetworkReachability,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t3830311593  Enumerator_get_Current_m722290112_gshared (Enumerator_t953886983 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m722290112(__this, method) ((  KeyValuePair_2_t3830311593  (*) (Enumerator_t953886983 *, const MethodInfo*))Enumerator_get_Current_m722290112_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.NetworkReachability,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m656613181_gshared (Enumerator_t953886983 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m656613181(__this, method) ((  int32_t (*) (Enumerator_t953886983 *, const MethodInfo*))Enumerator_get_CurrentKey_m656613181_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.NetworkReachability,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m707512353_gshared (Enumerator_t953886983 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m707512353(__this, method) ((  Il2CppObject * (*) (Enumerator_t953886983 *, const MethodInfo*))Enumerator_get_CurrentValue_m707512353_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.NetworkReachability,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m2252190819_gshared (Enumerator_t953886983 * __this, const MethodInfo* method);
#define Enumerator_Reset_m2252190819(__this, method) ((  void (*) (Enumerator_t953886983 *, const MethodInfo*))Enumerator_Reset_m2252190819_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.NetworkReachability,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2906844716_gshared (Enumerator_t953886983 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2906844716(__this, method) ((  void (*) (Enumerator_t953886983 *, const MethodInfo*))Enumerator_VerifyState_m2906844716_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.NetworkReachability,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m198934164_gshared (Enumerator_t953886983 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m198934164(__this, method) ((  void (*) (Enumerator_t953886983 *, const MethodInfo*))Enumerator_VerifyCurrent_m198934164_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.NetworkReachability,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2215288947_gshared (Enumerator_t953886983 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2215288947(__this, method) ((  void (*) (Enumerator_t953886983 *, const MethodInfo*))Enumerator_Dispose_m2215288947_gshared)(__this, method)
