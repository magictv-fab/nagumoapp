﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.abstracts.device.DeviceFileInfoAbstract
struct DeviceFileInfoAbstract_t3788299492;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.abstracts.device.DeviceFileInfoAbstract::.ctor()
extern "C"  void DeviceFileInfoAbstract__ctor_m246481965 (DeviceFileInfoAbstract_t3788299492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.abstracts.device.DeviceFileInfoAbstract MagicTV.abstracts.device.DeviceFileInfoAbstract::GetInstance()
extern "C"  DeviceFileInfoAbstract_t3788299492 * DeviceFileInfoAbstract_GetInstance_m47707947 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
