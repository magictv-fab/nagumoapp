﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PedidosRevisao/<IEnviarPedido>c__Iterator72
struct U3CIEnviarPedidoU3Ec__Iterator72_t764597359;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PedidosRevisao/<IEnviarPedido>c__Iterator72::.ctor()
extern "C"  void U3CIEnviarPedidoU3Ec__Iterator72__ctor_m43619292 (U3CIEnviarPedidoU3Ec__Iterator72_t764597359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PedidosRevisao/<IEnviarPedido>c__Iterator72::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CIEnviarPedidoU3Ec__Iterator72_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4032317558 (U3CIEnviarPedidoU3Ec__Iterator72_t764597359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PedidosRevisao/<IEnviarPedido>c__Iterator72::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CIEnviarPedidoU3Ec__Iterator72_System_Collections_IEnumerator_get_Current_m2108028938 (U3CIEnviarPedidoU3Ec__Iterator72_t764597359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PedidosRevisao/<IEnviarPedido>c__Iterator72::MoveNext()
extern "C"  bool U3CIEnviarPedidoU3Ec__Iterator72_MoveNext_m1465571992 (U3CIEnviarPedidoU3Ec__Iterator72_t764597359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PedidosRevisao/<IEnviarPedido>c__Iterator72::Dispose()
extern "C"  void U3CIEnviarPedidoU3Ec__Iterator72_Dispose_m3161717017 (U3CIEnviarPedidoU3Ec__Iterator72_t764597359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PedidosRevisao/<IEnviarPedido>c__Iterator72::Reset()
extern "C"  void U3CIEnviarPedidoU3Ec__Iterator72_Reset_m1985019529 (U3CIEnviarPedidoU3Ec__Iterator72_t764597359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
