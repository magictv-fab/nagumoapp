﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// UnityEngine.Animator
struct Animator_t2776330603;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AnimatorCrossFade
struct  AnimatorCrossFade_t3506781187  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AnimatorCrossFade::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.AnimatorCrossFade::stateName
	FsmString_t952858651 * ___stateName_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AnimatorCrossFade::transitionDuration
	FsmFloat_t2134102846 * ___transitionDuration_11;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.AnimatorCrossFade::layer
	FsmInt_t1596138449 * ___layer_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AnimatorCrossFade::normalizedTime
	FsmFloat_t2134102846 * ___normalizedTime_13;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.AnimatorCrossFade::_animator
	Animator_t2776330603 * ____animator_14;
	// System.Int32 HutongGames.PlayMaker.Actions.AnimatorCrossFade::_paramID
	int32_t ____paramID_15;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(AnimatorCrossFade_t3506781187, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_stateName_10() { return static_cast<int32_t>(offsetof(AnimatorCrossFade_t3506781187, ___stateName_10)); }
	inline FsmString_t952858651 * get_stateName_10() const { return ___stateName_10; }
	inline FsmString_t952858651 ** get_address_of_stateName_10() { return &___stateName_10; }
	inline void set_stateName_10(FsmString_t952858651 * value)
	{
		___stateName_10 = value;
		Il2CppCodeGenWriteBarrier(&___stateName_10, value);
	}

	inline static int32_t get_offset_of_transitionDuration_11() { return static_cast<int32_t>(offsetof(AnimatorCrossFade_t3506781187, ___transitionDuration_11)); }
	inline FsmFloat_t2134102846 * get_transitionDuration_11() const { return ___transitionDuration_11; }
	inline FsmFloat_t2134102846 ** get_address_of_transitionDuration_11() { return &___transitionDuration_11; }
	inline void set_transitionDuration_11(FsmFloat_t2134102846 * value)
	{
		___transitionDuration_11 = value;
		Il2CppCodeGenWriteBarrier(&___transitionDuration_11, value);
	}

	inline static int32_t get_offset_of_layer_12() { return static_cast<int32_t>(offsetof(AnimatorCrossFade_t3506781187, ___layer_12)); }
	inline FsmInt_t1596138449 * get_layer_12() const { return ___layer_12; }
	inline FsmInt_t1596138449 ** get_address_of_layer_12() { return &___layer_12; }
	inline void set_layer_12(FsmInt_t1596138449 * value)
	{
		___layer_12 = value;
		Il2CppCodeGenWriteBarrier(&___layer_12, value);
	}

	inline static int32_t get_offset_of_normalizedTime_13() { return static_cast<int32_t>(offsetof(AnimatorCrossFade_t3506781187, ___normalizedTime_13)); }
	inline FsmFloat_t2134102846 * get_normalizedTime_13() const { return ___normalizedTime_13; }
	inline FsmFloat_t2134102846 ** get_address_of_normalizedTime_13() { return &___normalizedTime_13; }
	inline void set_normalizedTime_13(FsmFloat_t2134102846 * value)
	{
		___normalizedTime_13 = value;
		Il2CppCodeGenWriteBarrier(&___normalizedTime_13, value);
	}

	inline static int32_t get_offset_of__animator_14() { return static_cast<int32_t>(offsetof(AnimatorCrossFade_t3506781187, ____animator_14)); }
	inline Animator_t2776330603 * get__animator_14() const { return ____animator_14; }
	inline Animator_t2776330603 ** get_address_of__animator_14() { return &____animator_14; }
	inline void set__animator_14(Animator_t2776330603 * value)
	{
		____animator_14 = value;
		Il2CppCodeGenWriteBarrier(&____animator_14, value);
	}

	inline static int32_t get_offset_of__paramID_15() { return static_cast<int32_t>(offsetof(AnimatorCrossFade_t3506781187, ____paramID_15)); }
	inline int32_t get__paramID_15() const { return ____paramID_15; }
	inline int32_t* get_address_of__paramID_15() { return &____paramID_15; }
	inline void set__paramID_15(int32_t value)
	{
		____paramID_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
