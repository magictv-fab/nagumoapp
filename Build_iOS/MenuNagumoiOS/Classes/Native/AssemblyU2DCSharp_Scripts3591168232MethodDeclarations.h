﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Scripts
struct Scripts_t3591168232;

#include "codegen/il2cpp-codegen.h"

// System.Void Scripts::.ctor()
extern "C"  void Scripts__ctor_m2401686403 (Scripts_t3591168232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Scripts::Start()
extern "C"  void Scripts_Start_m1348824195 (Scripts_t3591168232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Scripts::Update()
extern "C"  void Scripts_Update_m3164696554 (Scripts_t3591168232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
