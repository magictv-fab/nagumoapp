﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.EAN8Reader
struct EAN8Reader_t1642208551;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "QRCode_ZXing_BarcodeFormat4201805817.h"

// System.Void ZXing.OneD.EAN8Reader::.ctor()
extern "C"  void EAN8Reader__ctor_m735724252 (EAN8Reader_t1642208551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.EAN8Reader::decodeMiddle(ZXing.Common.BitArray,System.Int32[],System.Text.StringBuilder)
extern "C"  int32_t EAN8Reader_decodeMiddle_m1686356521 (EAN8Reader_t1642208551 * __this, BitArray_t4163851164 * ___row0, Int32U5BU5D_t3230847821* ___startRange1, StringBuilder_t243639308 * ___result2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.BarcodeFormat ZXing.OneD.EAN8Reader::get_BarcodeFormat()
extern "C"  int32_t EAN8Reader_get_BarcodeFormat_m1300580654 (EAN8Reader_t1642208551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
