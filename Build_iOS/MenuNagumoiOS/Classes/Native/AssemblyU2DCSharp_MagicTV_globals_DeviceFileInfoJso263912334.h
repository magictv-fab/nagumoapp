﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MagicTV.vo.ResultRevisionVO
struct ResultRevisionVO_t2445597391;
// MagicTV.globals.DeviceFileInfoJson
struct DeviceFileInfoJson_t3027144048;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.globals.DeviceFileInfoJson/<populateIndexedBundles>c__AnonStorey9B
struct  U3CpopulateIndexedBundlesU3Ec__AnonStorey9B_t263912334  : public Il2CppObject
{
public:
	// MagicTV.vo.ResultRevisionVO MagicTV.globals.DeviceFileInfoJson/<populateIndexedBundles>c__AnonStorey9B::revision
	ResultRevisionVO_t2445597391 * ___revision_0;
	// MagicTV.globals.DeviceFileInfoJson MagicTV.globals.DeviceFileInfoJson/<populateIndexedBundles>c__AnonStorey9B::<>f__this
	DeviceFileInfoJson_t3027144048 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_revision_0() { return static_cast<int32_t>(offsetof(U3CpopulateIndexedBundlesU3Ec__AnonStorey9B_t263912334, ___revision_0)); }
	inline ResultRevisionVO_t2445597391 * get_revision_0() const { return ___revision_0; }
	inline ResultRevisionVO_t2445597391 ** get_address_of_revision_0() { return &___revision_0; }
	inline void set_revision_0(ResultRevisionVO_t2445597391 * value)
	{
		___revision_0 = value;
		Il2CppCodeGenWriteBarrier(&___revision_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CpopulateIndexedBundlesU3Ec__AnonStorey9B_t263912334, ___U3CU3Ef__this_1)); }
	inline DeviceFileInfoJson_t3027144048 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline DeviceFileInfoJson_t3027144048 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(DeviceFileInfoJson_t3027144048 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
