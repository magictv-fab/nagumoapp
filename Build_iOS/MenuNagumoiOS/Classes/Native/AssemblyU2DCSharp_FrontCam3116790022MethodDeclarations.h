﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FrontCam
struct FrontCam_t3116790022;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void FrontCam::.ctor()
extern "C"  void FrontCam__ctor_m304223317 (FrontCam_t3116790022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator FrontCam::Start()
extern "C"  Il2CppObject * FrontCam_Start_m4113008653 (FrontCam_t3116790022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator FrontCam::openCamera()
extern "C"  Il2CppObject * FrontCam_openCamera_m3076749894 (FrontCam_t3116790022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FrontCam::BackButton()
extern "C"  void FrontCam_BackButton_m2161458312 (FrontCam_t3116790022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FrontCam::Close()
extern "C"  void FrontCam_Close_m2015082859 (FrontCam_t3116790022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FrontCam::OnDisable()
extern "C"  void FrontCam_OnDisable_m1571632700 (FrontCam_t3116790022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FrontCam::OnDestroy()
extern "C"  void FrontCam_OnDestroy_m3749164750 (FrontCam_t3116790022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator FrontCam::GoLevel()
extern "C"  Il2CppObject * FrontCam_GoLevel_m3790713383 (FrontCam_t3116790022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FrontCam::hideHUD()
extern "C"  void FrontCam_hideHUD_m3070713896 (FrontCam_t3116790022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FrontCam::showHUD()
extern "C"  void FrontCam_showHUD_m313704269 (FrontCam_t3116790022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator FrontCam::RemoveHUD()
extern "C"  Il2CppObject * FrontCam_RemoveHUD_m821733438 (FrontCam_t3116790022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FrontCam::fakeScreenShot()
extern "C"  void FrontCam_fakeScreenShot_m1142272170 (FrontCam_t3116790022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
