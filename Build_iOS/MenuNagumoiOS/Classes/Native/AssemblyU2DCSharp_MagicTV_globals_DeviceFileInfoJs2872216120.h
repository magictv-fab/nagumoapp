﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MagicTV.vo.MetadataVO
struct MetadataVO_t2511256998;
// MagicTV.globals.DeviceFileInfoJson/<populateIndexedBundles>c__AnonStorey9B
struct U3CpopulateIndexedBundlesU3Ec__AnonStorey9B_t263912334;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.globals.DeviceFileInfoJson/<populateIndexedBundles>c__AnonStorey9B/<populateIndexedBundles>c__AnonStorey9C
struct  U3CpopulateIndexedBundlesU3Ec__AnonStorey9C_t2872216120  : public Il2CppObject
{
public:
	// MagicTV.vo.MetadataVO MagicTV.globals.DeviceFileInfoJson/<populateIndexedBundles>c__AnonStorey9B/<populateIndexedBundles>c__AnonStorey9C::t
	MetadataVO_t2511256998 * ___t_0;
	// MagicTV.globals.DeviceFileInfoJson/<populateIndexedBundles>c__AnonStorey9B MagicTV.globals.DeviceFileInfoJson/<populateIndexedBundles>c__AnonStorey9B/<populateIndexedBundles>c__AnonStorey9C::<>f__ref$155
	U3CpopulateIndexedBundlesU3Ec__AnonStorey9B_t263912334 * ___U3CU3Ef__refU24155_1;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(U3CpopulateIndexedBundlesU3Ec__AnonStorey9C_t2872216120, ___t_0)); }
	inline MetadataVO_t2511256998 * get_t_0() const { return ___t_0; }
	inline MetadataVO_t2511256998 ** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(MetadataVO_t2511256998 * value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier(&___t_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24155_1() { return static_cast<int32_t>(offsetof(U3CpopulateIndexedBundlesU3Ec__AnonStorey9C_t2872216120, ___U3CU3Ef__refU24155_1)); }
	inline U3CpopulateIndexedBundlesU3Ec__AnonStorey9B_t263912334 * get_U3CU3Ef__refU24155_1() const { return ___U3CU3Ef__refU24155_1; }
	inline U3CpopulateIndexedBundlesU3Ec__AnonStorey9B_t263912334 ** get_address_of_U3CU3Ef__refU24155_1() { return &___U3CU3Ef__refU24155_1; }
	inline void set_U3CU3Ef__refU24155_1(U3CpopulateIndexedBundlesU3Ec__AnonStorey9B_t263912334 * value)
	{
		___U3CU3Ef__refU24155_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24155_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
