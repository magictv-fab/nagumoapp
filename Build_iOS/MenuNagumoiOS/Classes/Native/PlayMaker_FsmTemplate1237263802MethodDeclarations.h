﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FsmTemplate
struct FsmTemplate_t1237263802;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.String FsmTemplate::get_Category()
extern "C"  String_t* FsmTemplate_get_Category_m1436780281 (FsmTemplate_t1237263802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FsmTemplate::set_Category(System.String)
extern "C"  void FsmTemplate_set_Category_m511453714 (FsmTemplate_t1237263802 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FsmTemplate::OnEnable()
extern "C"  void FsmTemplate_OnEnable_m3091733303 (FsmTemplate_t1237263802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FsmTemplate::.ctor()
extern "C"  void FsmTemplate__ctor_m2073342447 (FsmTemplate_t1237263802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
