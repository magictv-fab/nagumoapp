﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Core.ProcessDirectoryHandler
struct ProcessDirectoryHandler_t4285218014;
// System.Object
struct Il2CppObject;
// ICSharpCode.SharpZipLib.Core.DirectoryEventArgs
struct DirectoryEventArgs_t2626056408;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Core_Dir2626056408.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ICSharpCode.SharpZipLib.Core.ProcessDirectoryHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void ProcessDirectoryHandler__ctor_m3424854197 (ProcessDirectoryHandler_t4285218014 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.ProcessDirectoryHandler::Invoke(System.Object,ICSharpCode.SharpZipLib.Core.DirectoryEventArgs)
extern "C"  void ProcessDirectoryHandler_Invoke_m1023400649 (ProcessDirectoryHandler_t4285218014 * __this, Il2CppObject * ___sender0, DirectoryEventArgs_t2626056408 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ICSharpCode.SharpZipLib.Core.ProcessDirectoryHandler::BeginInvoke(System.Object,ICSharpCode.SharpZipLib.Core.DirectoryEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ProcessDirectoryHandler_BeginInvoke_m4134043426 (ProcessDirectoryHandler_t4285218014 * __this, Il2CppObject * ___sender0, DirectoryEventArgs_t2626056408 * ___e1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.ProcessDirectoryHandler::EndInvoke(System.IAsyncResult)
extern "C"  void ProcessDirectoryHandler_EndInvoke_m1176843589 (ProcessDirectoryHandler_t4285218014 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
