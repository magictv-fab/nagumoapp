﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream/StackElement
struct StackElement_t2425423099;
struct StackElement_t2425423099_marshaled_pinvoke;
struct StackElement_t2425423099_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct StackElement_t2425423099;
struct StackElement_t2425423099_marshaled_pinvoke;

extern "C" void StackElement_t2425423099_marshal_pinvoke(const StackElement_t2425423099& unmarshaled, StackElement_t2425423099_marshaled_pinvoke& marshaled);
extern "C" void StackElement_t2425423099_marshal_pinvoke_back(const StackElement_t2425423099_marshaled_pinvoke& marshaled, StackElement_t2425423099& unmarshaled);
extern "C" void StackElement_t2425423099_marshal_pinvoke_cleanup(StackElement_t2425423099_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct StackElement_t2425423099;
struct StackElement_t2425423099_marshaled_com;

extern "C" void StackElement_t2425423099_marshal_com(const StackElement_t2425423099& unmarshaled, StackElement_t2425423099_marshaled_com& marshaled);
extern "C" void StackElement_t2425423099_marshal_com_back(const StackElement_t2425423099_marshaled_com& marshaled, StackElement_t2425423099& unmarshaled);
extern "C" void StackElement_t2425423099_marshal_com_cleanup(StackElement_t2425423099_marshaled_com& marshaled);
