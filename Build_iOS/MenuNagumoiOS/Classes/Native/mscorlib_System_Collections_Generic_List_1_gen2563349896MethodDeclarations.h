﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<ZXing.ResultPoint[]>::.ctor()
#define List_1__ctor_m796003562(__this, method) ((  void (*) (List_1_t2563349896 *, const MethodInfo*))List_1__ctor_m574172797_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ZXing.ResultPoint[]>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m443505214(__this, ___collection0, method) ((  void (*) (List_1_t2563349896 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1160795371_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<ZXing.ResultPoint[]>::.ctor(System.Int32)
#define List_1__ctor_m3128607602(__this, ___capacity0, method) ((  void (*) (List_1_t2563349896 *, int32_t, const MethodInfo*))List_1__ctor_m3643386469_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<ZXing.ResultPoint[]>::.ctor(T[],System.Int32)
#define List_1__ctor_m675757820(__this, ___data0, ___size1, method) ((  void (*) (List_1_t2563349896 *, ResultPointU5BU5DU5BU5D_t2883781481*, int32_t, const MethodInfo*))List_1__ctor_m4134761583_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<ZXing.ResultPoint[]>::.cctor()
#define List_1__cctor_m1473398828(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3826137881_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<ZXing.ResultPoint[]>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m455207859(__this, method) ((  Il2CppObject* (*) (List_1_t2563349896 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ZXing.ResultPoint[]>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m2527596739(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2563349896 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<ZXing.ResultPoint[]>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m663345150(__this, method) ((  Il2CppObject * (*) (List_1_t2563349896 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<ZXing.ResultPoint[]>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m1201516083(__this, ___item0, method) ((  int32_t (*) (List_1_t2563349896 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3794749222_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<ZXing.ResultPoint[]>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m700751097(__this, ___item0, method) ((  bool (*) (List_1_t2563349896 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2659633254_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<ZXing.ResultPoint[]>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m1998951179(__this, ___item0, method) ((  int32_t (*) (List_1_t2563349896 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3431692926_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<ZXing.ResultPoint[]>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m3219277238(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2563349896 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2067529129_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<ZXing.ResultPoint[]>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m3664008114(__this, ___item0, method) ((  void (*) (List_1_t2563349896 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1644145887_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<ZXing.ResultPoint[]>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2479843770(__this, method) ((  bool (*) (List_1_t2563349896 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<ZXing.ResultPoint[]>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2529848259(__this, method) ((  bool (*) (List_1_t2563349896 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3867536694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<ZXing.ResultPoint[]>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m4074936879(__this, method) ((  Il2CppObject * (*) (List_1_t2563349896 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<ZXing.ResultPoint[]>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m2329918248(__this, method) ((  bool (*) (List_1_t2563349896 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m432946261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<ZXing.ResultPoint[]>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m3300114129(__this, method) ((  bool (*) (List_1_t2563349896 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2961826820_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<ZXing.ResultPoint[]>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m69778422(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t2563349896 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3985478825_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<ZXing.ResultPoint[]>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m1962925069(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2563349896 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3234554688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<ZXing.ResultPoint[]>::Add(T)
#define List_1_Add_m490364378(__this, ___item0, method) ((  void (*) (List_1_t2563349896 *, ResultPointU5BU5D_t1195164344*, const MethodInfo*))List_1_Add_m268533613_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<ZXing.ResultPoint[]>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m1796862969(__this, ___newCount0, method) ((  void (*) (List_1_t2563349896 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4122600870_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<ZXing.ResultPoint[]>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m3346278990(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t2563349896 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m590371457_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<ZXing.ResultPoint[]>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m575940343(__this, ___collection0, method) ((  void (*) (List_1_t2563349896 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2478449828_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<ZXing.ResultPoint[]>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m4131879863(__this, ___enumerable0, method) ((  void (*) (List_1_t2563349896 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1739422052_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<ZXing.ResultPoint[]>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m900381824(__this, ___collection0, method) ((  void (*) (List_1_t2563349896 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2229151411_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<ZXing.ResultPoint[]>::AsReadOnly()
#define List_1_AsReadOnly_m2172458025(__this, method) ((  ReadOnlyCollection_1_t2752241880 * (*) (List_1_t2563349896 *, const MethodInfo*))List_1_AsReadOnly_m769820182_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ZXing.ResultPoint[]>::Clear()
#define List_1_Clear_m1625633740(__this, method) ((  void (*) (List_1_t2563349896 *, const MethodInfo*))List_1_Clear_m454602559_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<ZXing.ResultPoint[]>::Contains(T)
#define List_1_Contains_m3318383738(__this, ___item0, method) ((  bool (*) (List_1_t2563349896 *, ResultPointU5BU5D_t1195164344*, const MethodInfo*))List_1_Contains_m4186092781_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<ZXing.ResultPoint[]>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m4133422382(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2563349896 *, ResultPointU5BU5DU5BU5D_t2883781481*, int32_t, const MethodInfo*))List_1_CopyTo_m3988356635_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<ZXing.ResultPoint[]>::Find(System.Predicate`1<T>)
#define List_1_Find_m3119906618(__this, ___match0, method) ((  ResultPointU5BU5D_t1195164344* (*) (List_1_t2563349896 *, Predicate_1_t806221227 *, const MethodInfo*))List_1_Find_m3379773421_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<ZXing.ResultPoint[]>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m2267745653(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t806221227 *, const MethodInfo*))List_1_CheckMatch_m3390394152_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<ZXing.ResultPoint[]>::FindAll(System.Predicate`1<T>)
#define List_1_FindAll_m1367457201(__this, ___match0, method) ((  List_1_t2563349896 * (*) (List_1_t2563349896 *, Predicate_1_t806221227 *, const MethodInfo*))List_1_FindAll_m754611998_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<ZXing.ResultPoint[]>::FindAllStackBits(System.Predicate`1<T>)
#define List_1_FindAllStackBits_m3881239177(__this, ___match0, method) ((  List_1_t2563349896 * (*) (List_1_t2563349896 *, Predicate_1_t806221227 *, const MethodInfo*))List_1_FindAllStackBits_m2296615868_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<ZXing.ResultPoint[]>::FindAllList(System.Predicate`1<T>)
#define List_1_FindAllList_m3367156915(__this, ___match0, method) ((  List_1_t2563349896 * (*) (List_1_t2563349896 *, Predicate_1_t806221227 *, const MethodInfo*))List_1_FindAllList_m1562834848_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<ZXing.ResultPoint[]>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m598799002(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t2563349896 *, int32_t, int32_t, Predicate_1_t806221227 *, const MethodInfo*))List_1_GetIndex_m4275988045_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<ZXing.ResultPoint[]>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m2946917545(__this, ___action0, method) ((  void (*) (List_1_t2563349896 *, Action_1_t1590980480 *, const MethodInfo*))List_1_ForEach_m2030348444_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<ZXing.ResultPoint[]>::GetEnumerator()
#define List_1_GetEnumerator_m1224816429(__this, method) ((  Enumerator_t2583022666  (*) (List_1_t2563349896 *, const MethodInfo*))List_1_GetEnumerator_m1919240000_gshared)(__this, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<ZXing.ResultPoint[]>::GetRange(System.Int32,System.Int32)
#define List_1_GetRange_m5176857(__this, ___index0, ___count1, method) ((  List_1_t2563349896 * (*) (List_1_t2563349896 *, int32_t, int32_t, const MethodInfo*))List_1_GetRange_m4200734924_gshared)(__this, ___index0, ___count1, method)
// System.Int32 System.Collections.Generic.List`1<ZXing.ResultPoint[]>::IndexOf(T)
#define List_1_IndexOf_m2622363186(__this, ___item0, method) ((  int32_t (*) (List_1_t2563349896 *, ResultPointU5BU5D_t1195164344*, const MethodInfo*))List_1_IndexOf_m1752303327_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<ZXing.ResultPoint[]>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m2385366405(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t2563349896 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3807054194_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<ZXing.ResultPoint[]>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m3879789566(__this, ___index0, method) ((  void (*) (List_1_t2563349896 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3734723819_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<ZXing.ResultPoint[]>::Insert(System.Int32,T)
#define List_1_Insert_m683183909(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2563349896 *, int32_t, ResultPointU5BU5D_t1195164344*, const MethodInfo*))List_1_Insert_m3427163986_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<ZXing.ResultPoint[]>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m4249524186(__this, ___collection0, method) ((  void (*) (List_1_t2563349896 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2905071175_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<ZXing.ResultPoint[]>::Remove(T)
#define List_1_Remove_m3068795637(__this, ___item0, method) ((  bool (*) (List_1_t2563349896 *, ResultPointU5BU5D_t1195164344*, const MethodInfo*))List_1_Remove_m2747911208_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<ZXing.ResultPoint[]>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m1271155829(__this, ___match0, method) ((  int32_t (*) (List_1_t2563349896 *, Predicate_1_t806221227 *, const MethodInfo*))List_1_RemoveAll_m2933443938_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<ZXing.ResultPoint[]>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m2852004075(__this, ___index0, method) ((  void (*) (List_1_t2563349896 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1301016856_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<ZXing.ResultPoint[]>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m390645518(__this, ___index0, ___count1, method) ((  void (*) (List_1_t2563349896 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m856857915_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<ZXing.ResultPoint[]>::Reverse()
#define List_1_Reverse_m528615329(__this, method) ((  void (*) (List_1_t2563349896 *, const MethodInfo*))List_1_Reverse_m449081940_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ZXing.ResultPoint[]>::Sort()
#define List_1_Sort_m790774689(__this, method) ((  void (*) (List_1_t2563349896 *, const MethodInfo*))List_1_Sort_m1168641486_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ZXing.ResultPoint[]>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m1917897571(__this, ___comparer0, method) ((  void (*) (List_1_t2563349896 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3726677974_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<ZXing.ResultPoint[]>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m2541056308(__this, ___comparison0, method) ((  void (*) (List_1_t2563349896 *, Comparison_1_t4206492827 *, const MethodInfo*))List_1_Sort_m4192185249_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<ZXing.ResultPoint[]>::ToArray()
#define List_1_ToArray_m2366646880(__this, method) ((  ResultPointU5BU5DU5BU5D_t2883781481* (*) (List_1_t2563349896 *, const MethodInfo*))List_1_ToArray_m238588755_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ZXing.ResultPoint[]>::TrimExcess()
#define List_1_TrimExcess_m1008625274(__this, method) ((  void (*) (List_1_t2563349896 *, const MethodInfo*))List_1_TrimExcess_m2451380967_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<ZXing.ResultPoint[]>::TrueForAll(System.Predicate`1<T>)
#define List_1_TrueForAll_m4108029162(__this, ___match0, method) ((  bool (*) (List_1_t2563349896 *, Predicate_1_t806221227 *, const MethodInfo*))List_1_TrueForAll_m2945946909_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<ZXing.ResultPoint[]>::get_Capacity()
#define List_1_get_Capacity_m3727926626(__this, method) ((  int32_t (*) (List_1_t2563349896 *, const MethodInfo*))List_1_get_Capacity_m543520655_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ZXing.ResultPoint[]>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m3302019083(__this, ___value0, method) ((  void (*) (List_1_t2563349896 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1332789688_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<ZXing.ResultPoint[]>::get_Count()
#define List_1_get_Count_m1004951008(__this, method) ((  int32_t (*) (List_1_t2563349896 *, const MethodInfo*))List_1_get_Count_m2594154675_gshared)(__this, method)
// T System.Collections.Generic.List`1<ZXing.ResultPoint[]>::get_Item(System.Int32)
#define List_1_get_Item_m1836867375(__this, ___index0, method) ((  ResultPointU5BU5D_t1195164344* (*) (List_1_t2563349896 *, int32_t, const MethodInfo*))List_1_get_Item_m850128002_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<ZXing.ResultPoint[]>::set_Item(System.Int32,T)
#define List_1_set_Item_m1219336892(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2563349896 *, int32_t, ResultPointU5BU5D_t1195164344*, const MethodInfo*))List_1_set_Item_m1074271145_gshared)(__this, ___index0, ___value1, method)
