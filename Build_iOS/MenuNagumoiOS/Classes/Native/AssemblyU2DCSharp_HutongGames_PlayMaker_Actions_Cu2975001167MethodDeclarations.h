﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.CurveFsmAction
struct CurveFsmAction_t2975001167;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.CurveFsmAction::.ctor()
extern "C"  void CurveFsmAction__ctor_m647055095 (CurveFsmAction_t2975001167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CurveFsmAction::Reset()
extern "C"  void CurveFsmAction_Reset_m2588455332 (CurveFsmAction_t2975001167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CurveFsmAction::OnEnter()
extern "C"  void CurveFsmAction_OnEnter_m2736384270 (CurveFsmAction_t2975001167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CurveFsmAction::Init()
extern "C"  void CurveFsmAction_Init_m3297545341 (CurveFsmAction_t2975001167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CurveFsmAction::OnUpdate()
extern "C"  void CurveFsmAction_OnUpdate_m2357093109 (CurveFsmAction_t2975001167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
