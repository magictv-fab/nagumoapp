﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.EncodeHintType,System.Object>
struct KeyCollection_t1050929640;
// System.Collections.Generic.Dictionary`2<ZXing.EncodeHintType,System.Object>
struct Dictionary_2_t3719137485;
// System.Collections.Generic.IEnumerator`1<ZXing.EncodeHintType>
struct IEnumerator_1_t861460710;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// ZXing.EncodeHintType[]
struct EncodeHintTypeU5BU5D_t2781905696;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_EncodeHintType3244562957.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Key39106243.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.EncodeHintType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m529852539_gshared (KeyCollection_t1050929640 * __this, Dictionary_2_t3719137485 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m529852539(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1050929640 *, Dictionary_2_t3719137485 *, const MethodInfo*))KeyCollection__ctor_m529852539_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.EncodeHintType,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1798950907_gshared (KeyCollection_t1050929640 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1798950907(__this, ___item0, method) ((  void (*) (KeyCollection_t1050929640 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1798950907_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.EncodeHintType,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2316941362_gshared (KeyCollection_t1050929640 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2316941362(__this, method) ((  void (*) (KeyCollection_t1050929640 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2316941362_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.EncodeHintType,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2226256051_gshared (KeyCollection_t1050929640 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2226256051(__this, ___item0, method) ((  bool (*) (KeyCollection_t1050929640 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2226256051_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.EncodeHintType,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3631938264_gshared (KeyCollection_t1050929640 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3631938264(__this, ___item0, method) ((  bool (*) (KeyCollection_t1050929640 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3631938264_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.EncodeHintType,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4097076356_gshared (KeyCollection_t1050929640 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4097076356(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1050929640 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4097076356_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.EncodeHintType,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m2940133796_gshared (KeyCollection_t1050929640 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m2940133796(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1050929640 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m2940133796_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.EncodeHintType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2667546419_gshared (KeyCollection_t1050929640 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2667546419(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1050929640 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2667546419_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.EncodeHintType,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3419410260_gshared (KeyCollection_t1050929640 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3419410260(__this, method) ((  bool (*) (KeyCollection_t1050929640 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3419410260_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.EncodeHintType,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1277784326_gshared (KeyCollection_t1050929640 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1277784326(__this, method) ((  bool (*) (KeyCollection_t1050929640 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1277784326_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.EncodeHintType,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m4580792_gshared (KeyCollection_t1050929640 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m4580792(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1050929640 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m4580792_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.EncodeHintType,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m2318072624_gshared (KeyCollection_t1050929640 * __this, EncodeHintTypeU5BU5D_t2781905696* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m2318072624(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1050929640 *, EncodeHintTypeU5BU5D_t2781905696*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2318072624_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.EncodeHintType,System.Object>::GetEnumerator()
extern "C"  Enumerator_t39106243  KeyCollection_GetEnumerator_m1376463997_gshared (KeyCollection_t1050929640 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m1376463997(__this, method) ((  Enumerator_t39106243  (*) (KeyCollection_t1050929640 *, const MethodInfo*))KeyCollection_GetEnumerator_m1376463997_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.EncodeHintType,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m82071232_gshared (KeyCollection_t1050929640 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m82071232(__this, method) ((  int32_t (*) (KeyCollection_t1050929640 *, const MethodInfo*))KeyCollection_get_Count_m82071232_gshared)(__this, method)
