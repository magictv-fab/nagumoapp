﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.vo.KeyValueVO
struct KeyValueVO_t1780100105;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.vo.KeyValueVO::.ctor()
extern "C"  void KeyValueVO__ctor_m776127082 (KeyValueVO_t1780100105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.vo.KeyValueVO::ToString()
extern "C"  String_t* KeyValueVO_ToString_m3586703043 (KeyValueVO_t1780100105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
