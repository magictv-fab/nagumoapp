﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Core_Pat1707447339.h"
#include "mscorlib_System_DateTime4283661327.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Core.ExtendedPathFilter
struct  ExtendedPathFilter_t3673500740  : public PathFilter_t1707447339
{
public:
	// System.Int64 ICSharpCode.SharpZipLib.Core.ExtendedPathFilter::minSize_
	int64_t ___minSize__1;
	// System.Int64 ICSharpCode.SharpZipLib.Core.ExtendedPathFilter::maxSize_
	int64_t ___maxSize__2;
	// System.DateTime ICSharpCode.SharpZipLib.Core.ExtendedPathFilter::minDate_
	DateTime_t4283661327  ___minDate__3;
	// System.DateTime ICSharpCode.SharpZipLib.Core.ExtendedPathFilter::maxDate_
	DateTime_t4283661327  ___maxDate__4;

public:
	inline static int32_t get_offset_of_minSize__1() { return static_cast<int32_t>(offsetof(ExtendedPathFilter_t3673500740, ___minSize__1)); }
	inline int64_t get_minSize__1() const { return ___minSize__1; }
	inline int64_t* get_address_of_minSize__1() { return &___minSize__1; }
	inline void set_minSize__1(int64_t value)
	{
		___minSize__1 = value;
	}

	inline static int32_t get_offset_of_maxSize__2() { return static_cast<int32_t>(offsetof(ExtendedPathFilter_t3673500740, ___maxSize__2)); }
	inline int64_t get_maxSize__2() const { return ___maxSize__2; }
	inline int64_t* get_address_of_maxSize__2() { return &___maxSize__2; }
	inline void set_maxSize__2(int64_t value)
	{
		___maxSize__2 = value;
	}

	inline static int32_t get_offset_of_minDate__3() { return static_cast<int32_t>(offsetof(ExtendedPathFilter_t3673500740, ___minDate__3)); }
	inline DateTime_t4283661327  get_minDate__3() const { return ___minDate__3; }
	inline DateTime_t4283661327 * get_address_of_minDate__3() { return &___minDate__3; }
	inline void set_minDate__3(DateTime_t4283661327  value)
	{
		___minDate__3 = value;
	}

	inline static int32_t get_offset_of_maxDate__4() { return static_cast<int32_t>(offsetof(ExtendedPathFilter_t3673500740, ___maxDate__4)); }
	inline DateTime_t4283661327  get_maxDate__4() const { return ___maxDate__4; }
	inline DateTime_t4283661327 * get_address_of_maxDate__4() { return &___maxDate__4; }
	inline void set_maxDate__4(DateTime_t4283661327  value)
	{
		___maxDate__4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
