﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.device.AccelerometerEventDispacher
struct AccelerometerEventDispacher_t1386858712;

#include "AssemblyU2DCSharp_ARM_abstracts_ToggleOnOffAbstract832893812.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.device.AccelerometerInvertEventToggleOnOff
struct  AccelerometerInvertEventToggleOnOff_t3204041497  : public ToggleOnOffAbstract_t832893812
{
public:
	// ARM.device.AccelerometerEventDispacher ARM.device.AccelerometerInvertEventToggleOnOff::accelerometerEvent
	AccelerometerEventDispacher_t1386858712 * ___accelerometerEvent_7;

public:
	inline static int32_t get_offset_of_accelerometerEvent_7() { return static_cast<int32_t>(offsetof(AccelerometerInvertEventToggleOnOff_t3204041497, ___accelerometerEvent_7)); }
	inline AccelerometerEventDispacher_t1386858712 * get_accelerometerEvent_7() const { return ___accelerometerEvent_7; }
	inline AccelerometerEventDispacher_t1386858712 ** get_address_of_accelerometerEvent_7() { return &___accelerometerEvent_7; }
	inline void set_accelerometerEvent_7(AccelerometerEventDispacher_t1386858712 * value)
	{
		___accelerometerEvent_7 = value;
		Il2CppCodeGenWriteBarrier(&___accelerometerEvent_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
