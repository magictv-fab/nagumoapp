﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract
struct TransformPositionAngleFilterAbstract_t4158746314;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

// System.Void ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract::.ctor()
extern "C"  void TransformPositionAngleFilterAbstract__ctor_m661583295 (TransformPositionAngleFilterAbstract_t4158746314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract::.cctor()
extern "C"  void TransformPositionAngleFilterAbstract__cctor_m2847116750 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract::Start()
extern "C"  void TransformPositionAngleFilterAbstract_Start_m3903688383 (TransformPositionAngleFilterAbstract_t4158746314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract::_init()
extern "C"  void TransformPositionAngleFilterAbstract__init_m1365647660 (TransformPositionAngleFilterAbstract_t4158746314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract::defineAliasIfNotExists()
extern "C"  void TransformPositionAngleFilterAbstract_defineAliasIfNotExists_m812033826 (TransformPositionAngleFilterAbstract_t4158746314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract::_On()
extern "C"  void TransformPositionAngleFilterAbstract__On_m3187233051 (TransformPositionAngleFilterAbstract_t4158746314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract::_Off()
extern "C"  void TransformPositionAngleFilterAbstract__Off_m19798037 (TransformPositionAngleFilterAbstract_t4158746314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract::filterCurrent(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  TransformPositionAngleFilterAbstract_filterCurrent_m3004890357 (TransformPositionAngleFilterAbstract_t4158746314 * __this, Vector3_t4282066566  ___cur0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract::filterCurrent(UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  TransformPositionAngleFilterAbstract_filterCurrent_m848419553 (TransformPositionAngleFilterAbstract_t4158746314 * __this, Quaternion_t1553702882  ___cur0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract::filter(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  TransformPositionAngleFilterAbstract_filter_m2790742805 (TransformPositionAngleFilterAbstract_t4158746314 * __this, Vector3_t4282066566  ___currentValue0, Vector3_t4282066566  ___nextValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract::filter(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  TransformPositionAngleFilterAbstract_filter_m2579204801 (TransformPositionAngleFilterAbstract_t4158746314 * __this, Quaternion_t1553702882  ___currentValue0, Quaternion_t1553702882  ___nextValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract::getUniqueName()
extern "C"  String_t* TransformPositionAngleFilterAbstract_getUniqueName_m3401045548 (TransformPositionAngleFilterAbstract_t4158746314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract::turnOn()
extern "C"  void TransformPositionAngleFilterAbstract_turnOn_m3802891969 (TransformPositionAngleFilterAbstract_t4158746314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract::turnOff()
extern "C"  void TransformPositionAngleFilterAbstract_turnOff_m1925355311 (TransformPositionAngleFilterAbstract_t4158746314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract::ResetOnOff()
extern "C"  void TransformPositionAngleFilterAbstract_ResetOnOff_m2138804070 (TransformPositionAngleFilterAbstract_t4158746314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract::getGameObjectReference()
extern "C"  GameObject_t3674682005 * TransformPositionAngleFilterAbstract_getGameObjectReference_m2138988222 (TransformPositionAngleFilterAbstract_t4158746314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract::isActive()
extern "C"  bool TransformPositionAngleFilterAbstract_isActive_m4197292297 (TransformPositionAngleFilterAbstract_t4158746314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
