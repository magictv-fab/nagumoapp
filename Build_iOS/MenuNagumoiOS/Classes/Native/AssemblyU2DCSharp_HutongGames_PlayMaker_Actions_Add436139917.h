﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AddMixingTransform
struct  AddMixingTransform_t436139917  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AddMixingTransform::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.AddMixingTransform::animationName
	FsmString_t952858651 * ___animationName_10;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.AddMixingTransform::transform
	FsmString_t952858651 * ___transform_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.AddMixingTransform::recursive
	FsmBool_t1075959796 * ___recursive_12;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(AddMixingTransform_t436139917, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_animationName_10() { return static_cast<int32_t>(offsetof(AddMixingTransform_t436139917, ___animationName_10)); }
	inline FsmString_t952858651 * get_animationName_10() const { return ___animationName_10; }
	inline FsmString_t952858651 ** get_address_of_animationName_10() { return &___animationName_10; }
	inline void set_animationName_10(FsmString_t952858651 * value)
	{
		___animationName_10 = value;
		Il2CppCodeGenWriteBarrier(&___animationName_10, value);
	}

	inline static int32_t get_offset_of_transform_11() { return static_cast<int32_t>(offsetof(AddMixingTransform_t436139917, ___transform_11)); }
	inline FsmString_t952858651 * get_transform_11() const { return ___transform_11; }
	inline FsmString_t952858651 ** get_address_of_transform_11() { return &___transform_11; }
	inline void set_transform_11(FsmString_t952858651 * value)
	{
		___transform_11 = value;
		Il2CppCodeGenWriteBarrier(&___transform_11, value);
	}

	inline static int32_t get_offset_of_recursive_12() { return static_cast<int32_t>(offsetof(AddMixingTransform_t436139917, ___recursive_12)); }
	inline FsmBool_t1075959796 * get_recursive_12() const { return ___recursive_12; }
	inline FsmBool_t1075959796 ** get_address_of_recursive_12() { return &___recursive_12; }
	inline void set_recursive_12(FsmBool_t1075959796 * value)
	{
		___recursive_12 = value;
		Il2CppCodeGenWriteBarrier(&___recursive_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
