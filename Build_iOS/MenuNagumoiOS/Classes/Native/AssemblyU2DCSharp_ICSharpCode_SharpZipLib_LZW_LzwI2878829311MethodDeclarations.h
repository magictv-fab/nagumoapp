﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.LZW.LzwInputStream
struct LzwInputStream_t2878829311;
// System.IO.Stream
struct Stream_t1561764144;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "mscorlib_System_IO_SeekOrigin4120335598.h"
#include "mscorlib_System_AsyncCallback1369114871.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void ICSharpCode.SharpZipLib.LZW.LzwInputStream::.ctor(System.IO.Stream)
extern "C"  void LzwInputStream__ctor_m3331010683 (LzwInputStream_t2878829311 * __this, Stream_t1561764144 * ___baseInputStream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.LZW.LzwInputStream::get_IsStreamOwner()
extern "C"  bool LzwInputStream_get_IsStreamOwner_m3390460590 (LzwInputStream_t2878829311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.LZW.LzwInputStream::set_IsStreamOwner(System.Boolean)
extern "C"  void LzwInputStream_set_IsStreamOwner_m478566885 (LzwInputStream_t2878829311 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.LZW.LzwInputStream::ReadByte()
extern "C"  int32_t LzwInputStream_ReadByte_m2123329324 (LzwInputStream_t2878829311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.LZW.LzwInputStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t LzwInputStream_Read_m1382060517 (LzwInputStream_t2878829311 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.LZW.LzwInputStream::ResetBuf(System.Int32)
extern "C"  int32_t LzwInputStream_ResetBuf_m1735128803 (LzwInputStream_t2878829311 * __this, int32_t ___bitPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.LZW.LzwInputStream::Fill()
extern "C"  void LzwInputStream_Fill_m3165081507 (LzwInputStream_t2878829311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.LZW.LzwInputStream::ParseHeader()
extern "C"  void LzwInputStream_ParseHeader_m1547690338 (LzwInputStream_t2878829311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.LZW.LzwInputStream::get_CanRead()
extern "C"  bool LzwInputStream_get_CanRead_m2263664747 (LzwInputStream_t2878829311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.LZW.LzwInputStream::get_CanSeek()
extern "C"  bool LzwInputStream_get_CanSeek_m2292419789 (LzwInputStream_t2878829311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.LZW.LzwInputStream::get_CanWrite()
extern "C"  bool LzwInputStream_get_CanWrite_m1976783948 (LzwInputStream_t2878829311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.LZW.LzwInputStream::get_Length()
extern "C"  int64_t LzwInputStream_get_Length_m619539550 (LzwInputStream_t2878829311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.LZW.LzwInputStream::get_Position()
extern "C"  int64_t LzwInputStream_get_Position_m2893521953 (LzwInputStream_t2878829311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.LZW.LzwInputStream::set_Position(System.Int64)
extern "C"  void LzwInputStream_set_Position_m2148831896 (LzwInputStream_t2878829311 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.LZW.LzwInputStream::Flush()
extern "C"  void LzwInputStream_Flush_m3722715398 (LzwInputStream_t2878829311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.LZW.LzwInputStream::Seek(System.Int64,System.IO.SeekOrigin)
extern "C"  int64_t LzwInputStream_Seek_m3182892230 (LzwInputStream_t2878829311 * __this, int64_t ___offset0, int32_t ___origin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.LZW.LzwInputStream::SetLength(System.Int64)
extern "C"  void LzwInputStream_SetLength_m1267652668 (LzwInputStream_t2878829311 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.LZW.LzwInputStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C"  void LzwInputStream_Write_m3893327272 (LzwInputStream_t2878829311 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.LZW.LzwInputStream::WriteByte(System.Byte)
extern "C"  void LzwInputStream_WriteByte_m2040217986 (LzwInputStream_t2878829311 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ICSharpCode.SharpZipLib.LZW.LzwInputStream::BeginWrite(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LzwInputStream_BeginWrite_m2895069187 (LzwInputStream_t2878829311 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___state4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.LZW.LzwInputStream::Close()
extern "C"  void LzwInputStream_Close_m1054660346 (LzwInputStream_t2878829311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
