﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PanelManager/<DisablePanelDeleyed>c__Iterator6D
struct U3CDisablePanelDeleyedU3Ec__Iterator6D_t1466402221;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PanelManager/<DisablePanelDeleyed>c__Iterator6D::.ctor()
extern "C"  void U3CDisablePanelDeleyedU3Ec__Iterator6D__ctor_m856798302 (U3CDisablePanelDeleyedU3Ec__Iterator6D_t1466402221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PanelManager/<DisablePanelDeleyed>c__Iterator6D::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDisablePanelDeleyedU3Ec__Iterator6D_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m316475316 (U3CDisablePanelDeleyedU3Ec__Iterator6D_t1466402221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PanelManager/<DisablePanelDeleyed>c__Iterator6D::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDisablePanelDeleyedU3Ec__Iterator6D_System_Collections_IEnumerator_get_Current_m3059544904 (U3CDisablePanelDeleyedU3Ec__Iterator6D_t1466402221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PanelManager/<DisablePanelDeleyed>c__Iterator6D::MoveNext()
extern "C"  bool U3CDisablePanelDeleyedU3Ec__Iterator6D_MoveNext_m4188638422 (U3CDisablePanelDeleyedU3Ec__Iterator6D_t1466402221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelManager/<DisablePanelDeleyed>c__Iterator6D::Dispose()
extern "C"  void U3CDisablePanelDeleyedU3Ec__Iterator6D_Dispose_m2942697755 (U3CDisablePanelDeleyedU3Ec__Iterator6D_t1466402221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelManager/<DisablePanelDeleyed>c__Iterator6D::Reset()
extern "C"  void U3CDisablePanelDeleyedU3Ec__Iterator6D_Reset_m2798198539 (U3CDisablePanelDeleyedU3Ec__Iterator6D_t1466402221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
