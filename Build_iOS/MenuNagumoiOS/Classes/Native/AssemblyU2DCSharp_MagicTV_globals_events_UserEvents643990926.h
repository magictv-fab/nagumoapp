﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MagicTV.globals.events.UserEvents/OnConfirmEventHandler
struct OnConfirmEventHandler_t3783866142;
// MagicTV.globals.events.UserEvents
struct UserEvents_t643990926;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.globals.events.UserEvents
struct  UserEvents_t643990926  : public Il2CppObject
{
public:
	// MagicTV.globals.events.UserEvents/OnConfirmEventHandler MagicTV.globals.events.UserEvents::onConfirm
	OnConfirmEventHandler_t3783866142 * ___onConfirm_0;
	// MagicTV.globals.events.UserEvents/OnConfirmEventHandler MagicTV.globals.events.UserEvents::onConfirmCalled
	OnConfirmEventHandler_t3783866142 * ___onConfirmCalled_1;

public:
	inline static int32_t get_offset_of_onConfirm_0() { return static_cast<int32_t>(offsetof(UserEvents_t643990926, ___onConfirm_0)); }
	inline OnConfirmEventHandler_t3783866142 * get_onConfirm_0() const { return ___onConfirm_0; }
	inline OnConfirmEventHandler_t3783866142 ** get_address_of_onConfirm_0() { return &___onConfirm_0; }
	inline void set_onConfirm_0(OnConfirmEventHandler_t3783866142 * value)
	{
		___onConfirm_0 = value;
		Il2CppCodeGenWriteBarrier(&___onConfirm_0, value);
	}

	inline static int32_t get_offset_of_onConfirmCalled_1() { return static_cast<int32_t>(offsetof(UserEvents_t643990926, ___onConfirmCalled_1)); }
	inline OnConfirmEventHandler_t3783866142 * get_onConfirmCalled_1() const { return ___onConfirmCalled_1; }
	inline OnConfirmEventHandler_t3783866142 ** get_address_of_onConfirmCalled_1() { return &___onConfirmCalled_1; }
	inline void set_onConfirmCalled_1(OnConfirmEventHandler_t3783866142 * value)
	{
		___onConfirmCalled_1 = value;
		Il2CppCodeGenWriteBarrier(&___onConfirmCalled_1, value);
	}
};

struct UserEvents_t643990926_StaticFields
{
public:
	// MagicTV.globals.events.UserEvents MagicTV.globals.events.UserEvents::_instance
	UserEvents_t643990926 * ____instance_2;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(UserEvents_t643990926_StaticFields, ____instance_2)); }
	inline UserEvents_t643990926 * get__instance_2() const { return ____instance_2; }
	inline UserEvents_t643990926 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(UserEvents_t643990926 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier(&____instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
