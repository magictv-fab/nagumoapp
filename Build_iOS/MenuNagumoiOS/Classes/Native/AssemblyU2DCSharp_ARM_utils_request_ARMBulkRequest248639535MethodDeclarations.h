﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.utils.request.ARMBulkRequest
struct ARMBulkRequest_t248639535;

#include "codegen/il2cpp-codegen.h"

// System.Void ARM.utils.request.ARMBulkRequest::.ctor()
extern "C"  void ARMBulkRequest__ctor_m50340012 (ARMBulkRequest_t248639535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
