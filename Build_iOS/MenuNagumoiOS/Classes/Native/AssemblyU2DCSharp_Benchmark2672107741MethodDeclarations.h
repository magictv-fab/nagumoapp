﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Benchmark
struct Benchmark_t2672107741;
// MagicTV.abstracts.device.BenchmarkInfo
struct BenchmarkInfo_t940577717;

#include "codegen/il2cpp-codegen.h"

// System.Void Benchmark::.ctor()
extern "C"  void Benchmark__ctor_m1291885870 (Benchmark_t2672107741 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Benchmark::prepare()
extern "C"  void Benchmark_prepare_m1183752947 (Benchmark_t2672107741 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Benchmark::retestQuality()
extern "C"  void Benchmark_retestQuality_m3343255782 (Benchmark_t2672107741 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Benchmark::testDeviceBenchmark()
extern "C"  void Benchmark_testDeviceBenchmark_m2706477633 (Benchmark_t2672107741 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Benchmark::Play()
extern "C"  void Benchmark_Play_m3793764490 (Benchmark_t2672107741 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Benchmark::Stop()
extern "C"  void Benchmark_Stop_m3887448536 (Benchmark_t2672107741 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Benchmark::Pause()
extern "C"  void Benchmark_Pause_m1346011842 (Benchmark_t2672107741 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.abstracts.device.BenchmarkInfo Benchmark::GetCurrentQuality()
extern "C"  BenchmarkInfo_t940577717 * Benchmark_GetCurrentQuality_m1276679318 (Benchmark_t2672107741 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
