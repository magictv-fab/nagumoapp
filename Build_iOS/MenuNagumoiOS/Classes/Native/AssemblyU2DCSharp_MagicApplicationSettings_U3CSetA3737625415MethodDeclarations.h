﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicApplicationSettings/<SetAppReady>c__Iterator36
struct U3CSetAppReadyU3Ec__Iterator36_t3737625415;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicApplicationSettings/<SetAppReady>c__Iterator36::.ctor()
extern "C"  void U3CSetAppReadyU3Ec__Iterator36__ctor_m3245772804 (U3CSetAppReadyU3Ec__Iterator36_t3737625415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MagicApplicationSettings/<SetAppReady>c__Iterator36::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSetAppReadyU3Ec__Iterator36_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m465861198 (U3CSetAppReadyU3Ec__Iterator36_t3737625415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MagicApplicationSettings/<SetAppReady>c__Iterator36::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSetAppReadyU3Ec__Iterator36_System_Collections_IEnumerator_get_Current_m2174802402 (U3CSetAppReadyU3Ec__Iterator36_t3737625415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicApplicationSettings/<SetAppReady>c__Iterator36::MoveNext()
extern "C"  bool U3CSetAppReadyU3Ec__Iterator36_MoveNext_m1630601584 (U3CSetAppReadyU3Ec__Iterator36_t3737625415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicApplicationSettings/<SetAppReady>c__Iterator36::Dispose()
extern "C"  void U3CSetAppReadyU3Ec__Iterator36_Dispose_m939690817 (U3CSetAppReadyU3Ec__Iterator36_t3737625415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicApplicationSettings/<SetAppReady>c__Iterator36::Reset()
extern "C"  void U3CSetAppReadyU3Ec__Iterator36_Reset_m892205745 (U3CSetAppReadyU3Ec__Iterator36_t3737625415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
