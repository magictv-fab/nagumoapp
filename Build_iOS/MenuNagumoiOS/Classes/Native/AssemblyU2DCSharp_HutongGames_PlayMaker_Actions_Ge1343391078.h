﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetRandomObject
struct  GetRandomObject_t1343391078  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetRandomObject::withTag
	FsmString_t952858651 * ___withTag_9;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetRandomObject::storeResult
	FsmGameObject_t1697147867 * ___storeResult_10;
	// System.Boolean HutongGames.PlayMaker.Actions.GetRandomObject::everyFrame
	bool ___everyFrame_11;

public:
	inline static int32_t get_offset_of_withTag_9() { return static_cast<int32_t>(offsetof(GetRandomObject_t1343391078, ___withTag_9)); }
	inline FsmString_t952858651 * get_withTag_9() const { return ___withTag_9; }
	inline FsmString_t952858651 ** get_address_of_withTag_9() { return &___withTag_9; }
	inline void set_withTag_9(FsmString_t952858651 * value)
	{
		___withTag_9 = value;
		Il2CppCodeGenWriteBarrier(&___withTag_9, value);
	}

	inline static int32_t get_offset_of_storeResult_10() { return static_cast<int32_t>(offsetof(GetRandomObject_t1343391078, ___storeResult_10)); }
	inline FsmGameObject_t1697147867 * get_storeResult_10() const { return ___storeResult_10; }
	inline FsmGameObject_t1697147867 ** get_address_of_storeResult_10() { return &___storeResult_10; }
	inline void set_storeResult_10(FsmGameObject_t1697147867 * value)
	{
		___storeResult_10 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_10, value);
	}

	inline static int32_t get_offset_of_everyFrame_11() { return static_cast<int32_t>(offsetof(GetRandomObject_t1343391078, ___everyFrame_11)); }
	inline bool get_everyFrame_11() const { return ___everyFrame_11; }
	inline bool* get_address_of_everyFrame_11() { return &___everyFrame_11; }
	inline void set_everyFrame_11(bool value)
	{
		___everyFrame_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
