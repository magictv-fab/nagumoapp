﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ICSharpCode.SharpZipLib.Checksums.IChecksum
struct IChecksum_t3967152162;
// System.Boolean[]
struct BooleanU5BU5D_t3456302923;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t1820556512;
// System.IO.Stream
struct Stream_t1561764144;

#include "mscorlib_System_IO_Stream1561764144.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.BZip2.BZip2InputStream
struct  BZip2InputStream_t1830158201  : public Stream_t1561764144
{
public:
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::last
	int32_t ___last_9;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::origPtr
	int32_t ___origPtr_10;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::blockSize100k
	int32_t ___blockSize100k_11;
	// System.Boolean ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::blockRandomised
	bool ___blockRandomised_12;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::bsBuff
	int32_t ___bsBuff_13;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::bsLive
	int32_t ___bsLive_14;
	// ICSharpCode.SharpZipLib.Checksums.IChecksum ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::mCrc
	Il2CppObject * ___mCrc_15;
	// System.Boolean[] ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::inUse
	BooleanU5BU5D_t3456302923* ___inUse_16;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::nInUse
	int32_t ___nInUse_17;
	// System.Byte[] ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::seqToUnseq
	ByteU5BU5D_t4260760469* ___seqToUnseq_18;
	// System.Byte[] ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::unseqToSeq
	ByteU5BU5D_t4260760469* ___unseqToSeq_19;
	// System.Byte[] ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::selector
	ByteU5BU5D_t4260760469* ___selector_20;
	// System.Byte[] ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::selectorMtf
	ByteU5BU5D_t4260760469* ___selectorMtf_21;
	// System.Int32[] ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::tt
	Int32U5BU5D_t3230847821* ___tt_22;
	// System.Byte[] ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::ll8
	ByteU5BU5D_t4260760469* ___ll8_23;
	// System.Int32[] ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::unzftab
	Int32U5BU5D_t3230847821* ___unzftab_24;
	// System.Int32[][] ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::limit
	Int32U5BU5DU5BU5D_t1820556512* ___limit_25;
	// System.Int32[][] ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::baseArray
	Int32U5BU5DU5BU5D_t1820556512* ___baseArray_26;
	// System.Int32[][] ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::perm
	Int32U5BU5DU5BU5D_t1820556512* ___perm_27;
	// System.Int32[] ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::minLens
	Int32U5BU5D_t3230847821* ___minLens_28;
	// System.IO.Stream ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::baseStream
	Stream_t1561764144 * ___baseStream_29;
	// System.Boolean ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::streamEnd
	bool ___streamEnd_30;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::currentChar
	int32_t ___currentChar_31;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::currentState
	int32_t ___currentState_32;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::storedBlockCRC
	int32_t ___storedBlockCRC_33;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::storedCombinedCRC
	int32_t ___storedCombinedCRC_34;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::computedBlockCRC
	int32_t ___computedBlockCRC_35;
	// System.UInt32 ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::computedCombinedCRC
	uint32_t ___computedCombinedCRC_36;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::count
	int32_t ___count_37;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::chPrev
	int32_t ___chPrev_38;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::ch2
	int32_t ___ch2_39;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::tPos
	int32_t ___tPos_40;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::rNToGo
	int32_t ___rNToGo_41;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::rTPos
	int32_t ___rTPos_42;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::i2
	int32_t ___i2_43;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::j2
	int32_t ___j2_44;
	// System.Byte ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::z
	uint8_t ___z_45;
	// System.Boolean ICSharpCode.SharpZipLib.BZip2.BZip2InputStream::isStreamOwner
	bool ___isStreamOwner_46;

public:
	inline static int32_t get_offset_of_last_9() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___last_9)); }
	inline int32_t get_last_9() const { return ___last_9; }
	inline int32_t* get_address_of_last_9() { return &___last_9; }
	inline void set_last_9(int32_t value)
	{
		___last_9 = value;
	}

	inline static int32_t get_offset_of_origPtr_10() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___origPtr_10)); }
	inline int32_t get_origPtr_10() const { return ___origPtr_10; }
	inline int32_t* get_address_of_origPtr_10() { return &___origPtr_10; }
	inline void set_origPtr_10(int32_t value)
	{
		___origPtr_10 = value;
	}

	inline static int32_t get_offset_of_blockSize100k_11() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___blockSize100k_11)); }
	inline int32_t get_blockSize100k_11() const { return ___blockSize100k_11; }
	inline int32_t* get_address_of_blockSize100k_11() { return &___blockSize100k_11; }
	inline void set_blockSize100k_11(int32_t value)
	{
		___blockSize100k_11 = value;
	}

	inline static int32_t get_offset_of_blockRandomised_12() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___blockRandomised_12)); }
	inline bool get_blockRandomised_12() const { return ___blockRandomised_12; }
	inline bool* get_address_of_blockRandomised_12() { return &___blockRandomised_12; }
	inline void set_blockRandomised_12(bool value)
	{
		___blockRandomised_12 = value;
	}

	inline static int32_t get_offset_of_bsBuff_13() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___bsBuff_13)); }
	inline int32_t get_bsBuff_13() const { return ___bsBuff_13; }
	inline int32_t* get_address_of_bsBuff_13() { return &___bsBuff_13; }
	inline void set_bsBuff_13(int32_t value)
	{
		___bsBuff_13 = value;
	}

	inline static int32_t get_offset_of_bsLive_14() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___bsLive_14)); }
	inline int32_t get_bsLive_14() const { return ___bsLive_14; }
	inline int32_t* get_address_of_bsLive_14() { return &___bsLive_14; }
	inline void set_bsLive_14(int32_t value)
	{
		___bsLive_14 = value;
	}

	inline static int32_t get_offset_of_mCrc_15() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___mCrc_15)); }
	inline Il2CppObject * get_mCrc_15() const { return ___mCrc_15; }
	inline Il2CppObject ** get_address_of_mCrc_15() { return &___mCrc_15; }
	inline void set_mCrc_15(Il2CppObject * value)
	{
		___mCrc_15 = value;
		Il2CppCodeGenWriteBarrier(&___mCrc_15, value);
	}

	inline static int32_t get_offset_of_inUse_16() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___inUse_16)); }
	inline BooleanU5BU5D_t3456302923* get_inUse_16() const { return ___inUse_16; }
	inline BooleanU5BU5D_t3456302923** get_address_of_inUse_16() { return &___inUse_16; }
	inline void set_inUse_16(BooleanU5BU5D_t3456302923* value)
	{
		___inUse_16 = value;
		Il2CppCodeGenWriteBarrier(&___inUse_16, value);
	}

	inline static int32_t get_offset_of_nInUse_17() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___nInUse_17)); }
	inline int32_t get_nInUse_17() const { return ___nInUse_17; }
	inline int32_t* get_address_of_nInUse_17() { return &___nInUse_17; }
	inline void set_nInUse_17(int32_t value)
	{
		___nInUse_17 = value;
	}

	inline static int32_t get_offset_of_seqToUnseq_18() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___seqToUnseq_18)); }
	inline ByteU5BU5D_t4260760469* get_seqToUnseq_18() const { return ___seqToUnseq_18; }
	inline ByteU5BU5D_t4260760469** get_address_of_seqToUnseq_18() { return &___seqToUnseq_18; }
	inline void set_seqToUnseq_18(ByteU5BU5D_t4260760469* value)
	{
		___seqToUnseq_18 = value;
		Il2CppCodeGenWriteBarrier(&___seqToUnseq_18, value);
	}

	inline static int32_t get_offset_of_unseqToSeq_19() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___unseqToSeq_19)); }
	inline ByteU5BU5D_t4260760469* get_unseqToSeq_19() const { return ___unseqToSeq_19; }
	inline ByteU5BU5D_t4260760469** get_address_of_unseqToSeq_19() { return &___unseqToSeq_19; }
	inline void set_unseqToSeq_19(ByteU5BU5D_t4260760469* value)
	{
		___unseqToSeq_19 = value;
		Il2CppCodeGenWriteBarrier(&___unseqToSeq_19, value);
	}

	inline static int32_t get_offset_of_selector_20() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___selector_20)); }
	inline ByteU5BU5D_t4260760469* get_selector_20() const { return ___selector_20; }
	inline ByteU5BU5D_t4260760469** get_address_of_selector_20() { return &___selector_20; }
	inline void set_selector_20(ByteU5BU5D_t4260760469* value)
	{
		___selector_20 = value;
		Il2CppCodeGenWriteBarrier(&___selector_20, value);
	}

	inline static int32_t get_offset_of_selectorMtf_21() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___selectorMtf_21)); }
	inline ByteU5BU5D_t4260760469* get_selectorMtf_21() const { return ___selectorMtf_21; }
	inline ByteU5BU5D_t4260760469** get_address_of_selectorMtf_21() { return &___selectorMtf_21; }
	inline void set_selectorMtf_21(ByteU5BU5D_t4260760469* value)
	{
		___selectorMtf_21 = value;
		Il2CppCodeGenWriteBarrier(&___selectorMtf_21, value);
	}

	inline static int32_t get_offset_of_tt_22() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___tt_22)); }
	inline Int32U5BU5D_t3230847821* get_tt_22() const { return ___tt_22; }
	inline Int32U5BU5D_t3230847821** get_address_of_tt_22() { return &___tt_22; }
	inline void set_tt_22(Int32U5BU5D_t3230847821* value)
	{
		___tt_22 = value;
		Il2CppCodeGenWriteBarrier(&___tt_22, value);
	}

	inline static int32_t get_offset_of_ll8_23() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___ll8_23)); }
	inline ByteU5BU5D_t4260760469* get_ll8_23() const { return ___ll8_23; }
	inline ByteU5BU5D_t4260760469** get_address_of_ll8_23() { return &___ll8_23; }
	inline void set_ll8_23(ByteU5BU5D_t4260760469* value)
	{
		___ll8_23 = value;
		Il2CppCodeGenWriteBarrier(&___ll8_23, value);
	}

	inline static int32_t get_offset_of_unzftab_24() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___unzftab_24)); }
	inline Int32U5BU5D_t3230847821* get_unzftab_24() const { return ___unzftab_24; }
	inline Int32U5BU5D_t3230847821** get_address_of_unzftab_24() { return &___unzftab_24; }
	inline void set_unzftab_24(Int32U5BU5D_t3230847821* value)
	{
		___unzftab_24 = value;
		Il2CppCodeGenWriteBarrier(&___unzftab_24, value);
	}

	inline static int32_t get_offset_of_limit_25() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___limit_25)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_limit_25() const { return ___limit_25; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_limit_25() { return &___limit_25; }
	inline void set_limit_25(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___limit_25 = value;
		Il2CppCodeGenWriteBarrier(&___limit_25, value);
	}

	inline static int32_t get_offset_of_baseArray_26() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___baseArray_26)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_baseArray_26() const { return ___baseArray_26; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_baseArray_26() { return &___baseArray_26; }
	inline void set_baseArray_26(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___baseArray_26 = value;
		Il2CppCodeGenWriteBarrier(&___baseArray_26, value);
	}

	inline static int32_t get_offset_of_perm_27() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___perm_27)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_perm_27() const { return ___perm_27; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_perm_27() { return &___perm_27; }
	inline void set_perm_27(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___perm_27 = value;
		Il2CppCodeGenWriteBarrier(&___perm_27, value);
	}

	inline static int32_t get_offset_of_minLens_28() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___minLens_28)); }
	inline Int32U5BU5D_t3230847821* get_minLens_28() const { return ___minLens_28; }
	inline Int32U5BU5D_t3230847821** get_address_of_minLens_28() { return &___minLens_28; }
	inline void set_minLens_28(Int32U5BU5D_t3230847821* value)
	{
		___minLens_28 = value;
		Il2CppCodeGenWriteBarrier(&___minLens_28, value);
	}

	inline static int32_t get_offset_of_baseStream_29() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___baseStream_29)); }
	inline Stream_t1561764144 * get_baseStream_29() const { return ___baseStream_29; }
	inline Stream_t1561764144 ** get_address_of_baseStream_29() { return &___baseStream_29; }
	inline void set_baseStream_29(Stream_t1561764144 * value)
	{
		___baseStream_29 = value;
		Il2CppCodeGenWriteBarrier(&___baseStream_29, value);
	}

	inline static int32_t get_offset_of_streamEnd_30() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___streamEnd_30)); }
	inline bool get_streamEnd_30() const { return ___streamEnd_30; }
	inline bool* get_address_of_streamEnd_30() { return &___streamEnd_30; }
	inline void set_streamEnd_30(bool value)
	{
		___streamEnd_30 = value;
	}

	inline static int32_t get_offset_of_currentChar_31() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___currentChar_31)); }
	inline int32_t get_currentChar_31() const { return ___currentChar_31; }
	inline int32_t* get_address_of_currentChar_31() { return &___currentChar_31; }
	inline void set_currentChar_31(int32_t value)
	{
		___currentChar_31 = value;
	}

	inline static int32_t get_offset_of_currentState_32() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___currentState_32)); }
	inline int32_t get_currentState_32() const { return ___currentState_32; }
	inline int32_t* get_address_of_currentState_32() { return &___currentState_32; }
	inline void set_currentState_32(int32_t value)
	{
		___currentState_32 = value;
	}

	inline static int32_t get_offset_of_storedBlockCRC_33() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___storedBlockCRC_33)); }
	inline int32_t get_storedBlockCRC_33() const { return ___storedBlockCRC_33; }
	inline int32_t* get_address_of_storedBlockCRC_33() { return &___storedBlockCRC_33; }
	inline void set_storedBlockCRC_33(int32_t value)
	{
		___storedBlockCRC_33 = value;
	}

	inline static int32_t get_offset_of_storedCombinedCRC_34() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___storedCombinedCRC_34)); }
	inline int32_t get_storedCombinedCRC_34() const { return ___storedCombinedCRC_34; }
	inline int32_t* get_address_of_storedCombinedCRC_34() { return &___storedCombinedCRC_34; }
	inline void set_storedCombinedCRC_34(int32_t value)
	{
		___storedCombinedCRC_34 = value;
	}

	inline static int32_t get_offset_of_computedBlockCRC_35() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___computedBlockCRC_35)); }
	inline int32_t get_computedBlockCRC_35() const { return ___computedBlockCRC_35; }
	inline int32_t* get_address_of_computedBlockCRC_35() { return &___computedBlockCRC_35; }
	inline void set_computedBlockCRC_35(int32_t value)
	{
		___computedBlockCRC_35 = value;
	}

	inline static int32_t get_offset_of_computedCombinedCRC_36() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___computedCombinedCRC_36)); }
	inline uint32_t get_computedCombinedCRC_36() const { return ___computedCombinedCRC_36; }
	inline uint32_t* get_address_of_computedCombinedCRC_36() { return &___computedCombinedCRC_36; }
	inline void set_computedCombinedCRC_36(uint32_t value)
	{
		___computedCombinedCRC_36 = value;
	}

	inline static int32_t get_offset_of_count_37() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___count_37)); }
	inline int32_t get_count_37() const { return ___count_37; }
	inline int32_t* get_address_of_count_37() { return &___count_37; }
	inline void set_count_37(int32_t value)
	{
		___count_37 = value;
	}

	inline static int32_t get_offset_of_chPrev_38() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___chPrev_38)); }
	inline int32_t get_chPrev_38() const { return ___chPrev_38; }
	inline int32_t* get_address_of_chPrev_38() { return &___chPrev_38; }
	inline void set_chPrev_38(int32_t value)
	{
		___chPrev_38 = value;
	}

	inline static int32_t get_offset_of_ch2_39() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___ch2_39)); }
	inline int32_t get_ch2_39() const { return ___ch2_39; }
	inline int32_t* get_address_of_ch2_39() { return &___ch2_39; }
	inline void set_ch2_39(int32_t value)
	{
		___ch2_39 = value;
	}

	inline static int32_t get_offset_of_tPos_40() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___tPos_40)); }
	inline int32_t get_tPos_40() const { return ___tPos_40; }
	inline int32_t* get_address_of_tPos_40() { return &___tPos_40; }
	inline void set_tPos_40(int32_t value)
	{
		___tPos_40 = value;
	}

	inline static int32_t get_offset_of_rNToGo_41() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___rNToGo_41)); }
	inline int32_t get_rNToGo_41() const { return ___rNToGo_41; }
	inline int32_t* get_address_of_rNToGo_41() { return &___rNToGo_41; }
	inline void set_rNToGo_41(int32_t value)
	{
		___rNToGo_41 = value;
	}

	inline static int32_t get_offset_of_rTPos_42() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___rTPos_42)); }
	inline int32_t get_rTPos_42() const { return ___rTPos_42; }
	inline int32_t* get_address_of_rTPos_42() { return &___rTPos_42; }
	inline void set_rTPos_42(int32_t value)
	{
		___rTPos_42 = value;
	}

	inline static int32_t get_offset_of_i2_43() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___i2_43)); }
	inline int32_t get_i2_43() const { return ___i2_43; }
	inline int32_t* get_address_of_i2_43() { return &___i2_43; }
	inline void set_i2_43(int32_t value)
	{
		___i2_43 = value;
	}

	inline static int32_t get_offset_of_j2_44() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___j2_44)); }
	inline int32_t get_j2_44() const { return ___j2_44; }
	inline int32_t* get_address_of_j2_44() { return &___j2_44; }
	inline void set_j2_44(int32_t value)
	{
		___j2_44 = value;
	}

	inline static int32_t get_offset_of_z_45() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___z_45)); }
	inline uint8_t get_z_45() const { return ___z_45; }
	inline uint8_t* get_address_of_z_45() { return &___z_45; }
	inline void set_z_45(uint8_t value)
	{
		___z_45 = value;
	}

	inline static int32_t get_offset_of_isStreamOwner_46() { return static_cast<int32_t>(offsetof(BZip2InputStream_t1830158201, ___isStreamOwner_46)); }
	inline bool get_isStreamOwner_46() const { return ___isStreamOwner_46; }
	inline bool* get_address_of_isStreamOwner_46() { return &___isStreamOwner_46; }
	inline void set_isStreamOwner_46(bool value)
	{
		___isStreamOwner_46 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
