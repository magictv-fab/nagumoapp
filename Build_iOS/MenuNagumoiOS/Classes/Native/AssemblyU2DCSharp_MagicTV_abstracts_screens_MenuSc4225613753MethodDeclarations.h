﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.abstracts.screens.MenuScreenAbstract
struct MenuScreenAbstract_t4225613753;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.abstracts.screens.MenuScreenAbstract::.ctor()
extern "C"  void MenuScreenAbstract__ctor_m4045878155 (MenuScreenAbstract_t4225613753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.MenuScreenAbstract::RaisePrintScreenCalled()
extern "C"  void MenuScreenAbstract_RaisePrintScreenCalled_m2367575971 (MenuScreenAbstract_t4225613753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.MenuScreenAbstract::RaiseClearCacheCalled()
extern "C"  void MenuScreenAbstract_RaiseClearCacheCalled_m2208126535 (MenuScreenAbstract_t4225613753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.MenuScreenAbstract::RaiseResetCalled()
extern "C"  void MenuScreenAbstract_RaiseResetCalled_m3462897273 (MenuScreenAbstract_t4225613753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
