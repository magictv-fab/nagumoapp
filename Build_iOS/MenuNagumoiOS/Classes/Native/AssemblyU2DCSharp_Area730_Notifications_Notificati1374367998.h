﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Int64[]
struct Int64U5BU5D_t2174042770;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Area730.Notifications.NotificationBuilder
struct  NotificationBuilder_t1374367998  : public Il2CppObject
{
public:
	// System.Int32 Area730.Notifications.NotificationBuilder::_id
	int32_t ____id_4;
	// System.String Area730.Notifications.NotificationBuilder::_smallIcon
	String_t* ____smallIcon_5;
	// System.String Area730.Notifications.NotificationBuilder::_largeIcon
	String_t* ____largeIcon_6;
	// System.Int32 Area730.Notifications.NotificationBuilder::_defaults
	int32_t ____defaults_7;
	// System.Boolean Area730.Notifications.NotificationBuilder::_autoCancel
	bool ____autoCancel_8;
	// System.String Area730.Notifications.NotificationBuilder::_sound
	String_t* ____sound_9;
	// System.String Area730.Notifications.NotificationBuilder::_ticker
	String_t* ____ticker_10;
	// System.String Area730.Notifications.NotificationBuilder::_title
	String_t* ____title_11;
	// System.String Area730.Notifications.NotificationBuilder::_body
	String_t* ____body_12;
	// System.Int64[] Area730.Notifications.NotificationBuilder::_vibratePattern
	Int64U5BU5D_t2174042770* ____vibratePattern_13;
	// System.Int64 Area730.Notifications.NotificationBuilder::_when
	int64_t ____when_14;
	// System.Int64 Area730.Notifications.NotificationBuilder::_delay
	int64_t ____delay_15;
	// System.Boolean Area730.Notifications.NotificationBuilder::_isRepeating
	bool ____isRepeating_16;
	// System.Int64 Area730.Notifications.NotificationBuilder::_interval
	int64_t ____interval_17;
	// System.Int32 Area730.Notifications.NotificationBuilder::_number
	int32_t ____number_18;
	// System.Boolean Area730.Notifications.NotificationBuilder::_alertOnce
	bool ____alertOnce_19;
	// System.String Area730.Notifications.NotificationBuilder::_color
	String_t* ____color_20;
	// System.String Area730.Notifications.NotificationBuilder::_group
	String_t* ____group_21;
	// System.String Area730.Notifications.NotificationBuilder::_sortKey
	String_t* ____sortKey_22;

public:
	inline static int32_t get_offset_of__id_4() { return static_cast<int32_t>(offsetof(NotificationBuilder_t1374367998, ____id_4)); }
	inline int32_t get__id_4() const { return ____id_4; }
	inline int32_t* get_address_of__id_4() { return &____id_4; }
	inline void set__id_4(int32_t value)
	{
		____id_4 = value;
	}

	inline static int32_t get_offset_of__smallIcon_5() { return static_cast<int32_t>(offsetof(NotificationBuilder_t1374367998, ____smallIcon_5)); }
	inline String_t* get__smallIcon_5() const { return ____smallIcon_5; }
	inline String_t** get_address_of__smallIcon_5() { return &____smallIcon_5; }
	inline void set__smallIcon_5(String_t* value)
	{
		____smallIcon_5 = value;
		Il2CppCodeGenWriteBarrier(&____smallIcon_5, value);
	}

	inline static int32_t get_offset_of__largeIcon_6() { return static_cast<int32_t>(offsetof(NotificationBuilder_t1374367998, ____largeIcon_6)); }
	inline String_t* get__largeIcon_6() const { return ____largeIcon_6; }
	inline String_t** get_address_of__largeIcon_6() { return &____largeIcon_6; }
	inline void set__largeIcon_6(String_t* value)
	{
		____largeIcon_6 = value;
		Il2CppCodeGenWriteBarrier(&____largeIcon_6, value);
	}

	inline static int32_t get_offset_of__defaults_7() { return static_cast<int32_t>(offsetof(NotificationBuilder_t1374367998, ____defaults_7)); }
	inline int32_t get__defaults_7() const { return ____defaults_7; }
	inline int32_t* get_address_of__defaults_7() { return &____defaults_7; }
	inline void set__defaults_7(int32_t value)
	{
		____defaults_7 = value;
	}

	inline static int32_t get_offset_of__autoCancel_8() { return static_cast<int32_t>(offsetof(NotificationBuilder_t1374367998, ____autoCancel_8)); }
	inline bool get__autoCancel_8() const { return ____autoCancel_8; }
	inline bool* get_address_of__autoCancel_8() { return &____autoCancel_8; }
	inline void set__autoCancel_8(bool value)
	{
		____autoCancel_8 = value;
	}

	inline static int32_t get_offset_of__sound_9() { return static_cast<int32_t>(offsetof(NotificationBuilder_t1374367998, ____sound_9)); }
	inline String_t* get__sound_9() const { return ____sound_9; }
	inline String_t** get_address_of__sound_9() { return &____sound_9; }
	inline void set__sound_9(String_t* value)
	{
		____sound_9 = value;
		Il2CppCodeGenWriteBarrier(&____sound_9, value);
	}

	inline static int32_t get_offset_of__ticker_10() { return static_cast<int32_t>(offsetof(NotificationBuilder_t1374367998, ____ticker_10)); }
	inline String_t* get__ticker_10() const { return ____ticker_10; }
	inline String_t** get_address_of__ticker_10() { return &____ticker_10; }
	inline void set__ticker_10(String_t* value)
	{
		____ticker_10 = value;
		Il2CppCodeGenWriteBarrier(&____ticker_10, value);
	}

	inline static int32_t get_offset_of__title_11() { return static_cast<int32_t>(offsetof(NotificationBuilder_t1374367998, ____title_11)); }
	inline String_t* get__title_11() const { return ____title_11; }
	inline String_t** get_address_of__title_11() { return &____title_11; }
	inline void set__title_11(String_t* value)
	{
		____title_11 = value;
		Il2CppCodeGenWriteBarrier(&____title_11, value);
	}

	inline static int32_t get_offset_of__body_12() { return static_cast<int32_t>(offsetof(NotificationBuilder_t1374367998, ____body_12)); }
	inline String_t* get__body_12() const { return ____body_12; }
	inline String_t** get_address_of__body_12() { return &____body_12; }
	inline void set__body_12(String_t* value)
	{
		____body_12 = value;
		Il2CppCodeGenWriteBarrier(&____body_12, value);
	}

	inline static int32_t get_offset_of__vibratePattern_13() { return static_cast<int32_t>(offsetof(NotificationBuilder_t1374367998, ____vibratePattern_13)); }
	inline Int64U5BU5D_t2174042770* get__vibratePattern_13() const { return ____vibratePattern_13; }
	inline Int64U5BU5D_t2174042770** get_address_of__vibratePattern_13() { return &____vibratePattern_13; }
	inline void set__vibratePattern_13(Int64U5BU5D_t2174042770* value)
	{
		____vibratePattern_13 = value;
		Il2CppCodeGenWriteBarrier(&____vibratePattern_13, value);
	}

	inline static int32_t get_offset_of__when_14() { return static_cast<int32_t>(offsetof(NotificationBuilder_t1374367998, ____when_14)); }
	inline int64_t get__when_14() const { return ____when_14; }
	inline int64_t* get_address_of__when_14() { return &____when_14; }
	inline void set__when_14(int64_t value)
	{
		____when_14 = value;
	}

	inline static int32_t get_offset_of__delay_15() { return static_cast<int32_t>(offsetof(NotificationBuilder_t1374367998, ____delay_15)); }
	inline int64_t get__delay_15() const { return ____delay_15; }
	inline int64_t* get_address_of__delay_15() { return &____delay_15; }
	inline void set__delay_15(int64_t value)
	{
		____delay_15 = value;
	}

	inline static int32_t get_offset_of__isRepeating_16() { return static_cast<int32_t>(offsetof(NotificationBuilder_t1374367998, ____isRepeating_16)); }
	inline bool get__isRepeating_16() const { return ____isRepeating_16; }
	inline bool* get_address_of__isRepeating_16() { return &____isRepeating_16; }
	inline void set__isRepeating_16(bool value)
	{
		____isRepeating_16 = value;
	}

	inline static int32_t get_offset_of__interval_17() { return static_cast<int32_t>(offsetof(NotificationBuilder_t1374367998, ____interval_17)); }
	inline int64_t get__interval_17() const { return ____interval_17; }
	inline int64_t* get_address_of__interval_17() { return &____interval_17; }
	inline void set__interval_17(int64_t value)
	{
		____interval_17 = value;
	}

	inline static int32_t get_offset_of__number_18() { return static_cast<int32_t>(offsetof(NotificationBuilder_t1374367998, ____number_18)); }
	inline int32_t get__number_18() const { return ____number_18; }
	inline int32_t* get_address_of__number_18() { return &____number_18; }
	inline void set__number_18(int32_t value)
	{
		____number_18 = value;
	}

	inline static int32_t get_offset_of__alertOnce_19() { return static_cast<int32_t>(offsetof(NotificationBuilder_t1374367998, ____alertOnce_19)); }
	inline bool get__alertOnce_19() const { return ____alertOnce_19; }
	inline bool* get_address_of__alertOnce_19() { return &____alertOnce_19; }
	inline void set__alertOnce_19(bool value)
	{
		____alertOnce_19 = value;
	}

	inline static int32_t get_offset_of__color_20() { return static_cast<int32_t>(offsetof(NotificationBuilder_t1374367998, ____color_20)); }
	inline String_t* get__color_20() const { return ____color_20; }
	inline String_t** get_address_of__color_20() { return &____color_20; }
	inline void set__color_20(String_t* value)
	{
		____color_20 = value;
		Il2CppCodeGenWriteBarrier(&____color_20, value);
	}

	inline static int32_t get_offset_of__group_21() { return static_cast<int32_t>(offsetof(NotificationBuilder_t1374367998, ____group_21)); }
	inline String_t* get__group_21() const { return ____group_21; }
	inline String_t** get_address_of__group_21() { return &____group_21; }
	inline void set__group_21(String_t* value)
	{
		____group_21 = value;
		Il2CppCodeGenWriteBarrier(&____group_21, value);
	}

	inline static int32_t get_offset_of__sortKey_22() { return static_cast<int32_t>(offsetof(NotificationBuilder_t1374367998, ____sortKey_22)); }
	inline String_t* get__sortKey_22() const { return ____sortKey_22; }
	inline String_t** get_address_of__sortKey_22() { return &____sortKey_22; }
	inline void set__sortKey_22(String_t* value)
	{
		____sortKey_22 = value;
		Il2CppCodeGenWriteBarrier(&____sortKey_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
