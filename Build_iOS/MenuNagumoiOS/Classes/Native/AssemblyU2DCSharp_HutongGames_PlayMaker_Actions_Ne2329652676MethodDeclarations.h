﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkConnect
struct NetworkConnect_t2329652676;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkConnect::.ctor()
extern "C"  void NetworkConnect__ctor_m482645794 (NetworkConnect_t2329652676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkConnect::Reset()
extern "C"  void NetworkConnect_Reset_m2424046031 (NetworkConnect_t2329652676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkConnect::OnEnter()
extern "C"  void NetworkConnect_OnEnter_m3652835961 (NetworkConnect_t2329652676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
