﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t821476169;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// UnityEngine.AudioSource
struct AudioSource_t1740077639;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AudioPlay
struct  AudioPlay_t298632056  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AudioPlay::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AudioPlay::volume
	FsmFloat_t2134102846 * ___volume_10;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.AudioPlay::oneShotClip
	FsmObject_t821476169 * ___oneShotClip_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.AudioPlay::finishedEvent
	FsmEvent_t2133468028 * ___finishedEvent_12;
	// UnityEngine.AudioSource HutongGames.PlayMaker.Actions.AudioPlay::audio
	AudioSource_t1740077639 * ___audio_13;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(AudioPlay_t298632056, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_volume_10() { return static_cast<int32_t>(offsetof(AudioPlay_t298632056, ___volume_10)); }
	inline FsmFloat_t2134102846 * get_volume_10() const { return ___volume_10; }
	inline FsmFloat_t2134102846 ** get_address_of_volume_10() { return &___volume_10; }
	inline void set_volume_10(FsmFloat_t2134102846 * value)
	{
		___volume_10 = value;
		Il2CppCodeGenWriteBarrier(&___volume_10, value);
	}

	inline static int32_t get_offset_of_oneShotClip_11() { return static_cast<int32_t>(offsetof(AudioPlay_t298632056, ___oneShotClip_11)); }
	inline FsmObject_t821476169 * get_oneShotClip_11() const { return ___oneShotClip_11; }
	inline FsmObject_t821476169 ** get_address_of_oneShotClip_11() { return &___oneShotClip_11; }
	inline void set_oneShotClip_11(FsmObject_t821476169 * value)
	{
		___oneShotClip_11 = value;
		Il2CppCodeGenWriteBarrier(&___oneShotClip_11, value);
	}

	inline static int32_t get_offset_of_finishedEvent_12() { return static_cast<int32_t>(offsetof(AudioPlay_t298632056, ___finishedEvent_12)); }
	inline FsmEvent_t2133468028 * get_finishedEvent_12() const { return ___finishedEvent_12; }
	inline FsmEvent_t2133468028 ** get_address_of_finishedEvent_12() { return &___finishedEvent_12; }
	inline void set_finishedEvent_12(FsmEvent_t2133468028 * value)
	{
		___finishedEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___finishedEvent_12, value);
	}

	inline static int32_t get_offset_of_audio_13() { return static_cast<int32_t>(offsetof(AudioPlay_t298632056, ___audio_13)); }
	inline AudioSource_t1740077639 * get_audio_13() const { return ___audio_13; }
	inline AudioSource_t1740077639 ** get_address_of_audio_13() { return &___audio_13; }
	inline void set_audio_13(AudioSource_t1740077639 * value)
	{
		___audio_13 = value;
		Il2CppCodeGenWriteBarrier(&___audio_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
