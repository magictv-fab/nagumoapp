﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>
struct List_1_t1722560608;
// System.Collections.Generic.IEnumerable`1<Vuforia.Image/PIXEL_FORMAT>
struct IEnumerable_1_t3655288013;
// Vuforia.Image/PIXEL_FORMAT[]
struct PIXEL_FORMATU5BU5D_t882070065;
// System.Collections.Generic.IEnumerator`1<Vuforia.Image/PIXEL_FORMAT>
struct IEnumerator_1_t2266240105;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<Vuforia.Image/PIXEL_FORMAT>
struct ICollection_1_t1248965043;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>
struct ReadOnlyCollection_1_t1911452592;
// System.Predicate`1<Vuforia.Image/PIXEL_FORMAT>
struct Predicate_1_t4260399235;
// System.Action`1<Vuforia.Image/PIXEL_FORMAT>
struct Action_1_t750191192;
// System.Collections.Generic.IComparer`1<Vuforia.Image/PIXEL_FORMAT>
struct IComparer_1_t2929389098;
// System.Comparison`1<Vuforia.Image/PIXEL_FORMAT>
struct Comparison_1_t3365703539;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXE354375056.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1742233378.h"

// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::.ctor()
extern "C"  void List_1__ctor_m2430615487_gshared (List_1_t1722560608 * __this, const MethodInfo* method);
#define List_1__ctor_m2430615487(__this, method) ((  void (*) (List_1_t1722560608 *, const MethodInfo*))List_1__ctor_m2430615487_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m733502843_gshared (List_1_t1722560608 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m733502843(__this, ___collection0, method) ((  void (*) (List_1_t1722560608 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m733502843_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m3131339733_gshared (List_1_t1722560608 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m3131339733(__this, ___capacity0, method) ((  void (*) (List_1_t1722560608 *, int32_t, const MethodInfo*))List_1__ctor_m3131339733_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::.ctor(T[],System.Int32)
extern "C"  void List_1__ctor_m2710308319_gshared (List_1_t1722560608 * __this, PIXEL_FORMATU5BU5D_t882070065* ___data0, int32_t ___size1, const MethodInfo* method);
#define List_1__ctor_m2710308319(__this, ___data0, ___size1, method) ((  void (*) (List_1_t1722560608 *, PIXEL_FORMATU5BU5D_t882070065*, int32_t, const MethodInfo*))List_1__ctor_m2710308319_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::.cctor()
extern "C"  void List_1__cctor_m3312646057_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m3312646057(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3312646057_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2867103886_gshared (List_1_t1722560608 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2867103886(__this, method) ((  Il2CppObject* (*) (List_1_t1722560608 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2867103886_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m800597312_gshared (List_1_t1722560608 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m800597312(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1722560608 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m800597312_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3640704207_gshared (List_1_t1722560608 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3640704207(__this, method) ((  Il2CppObject * (*) (List_1_t1722560608 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3640704207_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m3003977806_gshared (List_1_t1722560608 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m3003977806(__this, ___item0, method) ((  int32_t (*) (List_1_t1722560608 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3003977806_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m2773269298_gshared (List_1_t1722560608 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m2773269298(__this, ___item0, method) ((  bool (*) (List_1_t1722560608 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2773269298_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m4186992550_gshared (List_1_t1722560608 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m4186992550(__this, ___item0, method) ((  int32_t (*) (List_1_t1722560608 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m4186992550_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m579639065_gshared (List_1_t1722560608 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m579639065(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1722560608 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m579639065_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m3733093743_gshared (List_1_t1722560608 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m3733093743(__this, ___item0, method) ((  void (*) (List_1_t1722560608 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3733093743_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m959829171_gshared (List_1_t1722560608 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m959829171(__this, method) ((  bool (*) (List_1_t1722560608 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m959829171_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m469726954_gshared (List_1_t1722560608 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m469726954(__this, method) ((  bool (*) (List_1_t1722560608 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m469726954_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m2249081116_gshared (List_1_t1722560608 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m2249081116(__this, method) ((  Il2CppObject * (*) (List_1_t1722560608 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m2249081116_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m1569011745_gshared (List_1_t1722560608 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m1569011745(__this, method) ((  bool (*) (List_1_t1722560608 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m1569011745_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m1197358776_gshared (List_1_t1722560608 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1197358776(__this, method) ((  bool (*) (List_1_t1722560608 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1197358776_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m2064014243_gshared (List_1_t1722560608 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m2064014243(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1722560608 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m2064014243_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m3596312752_gshared (List_1_t1722560608 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m3596312752(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1722560608 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3596312752_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Add(T)
extern "C"  void List_1_Add_m2124976303_gshared (List_1_t1722560608 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Add_m2124976303(__this, ___item0, method) ((  void (*) (List_1_t1722560608 *, int32_t, const MethodInfo*))List_1_Add_m2124976303_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m2512297526_gshared (List_1_t1722560608 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m2512297526(__this, ___newCount0, method) ((  void (*) (List_1_t1722560608 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m2512297526_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m460695537_gshared (List_1_t1722560608 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m460695537(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t1722560608 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m460695537_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m4231568692_gshared (List_1_t1722560608 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m4231568692(__this, ___collection0, method) ((  void (*) (List_1_t1722560608 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m4231568692_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m3492540916_gshared (List_1_t1722560608 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m3492540916(__this, ___enumerable0, method) ((  void (*) (List_1_t1722560608 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m3492540916_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m3040515107_gshared (List_1_t1722560608 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m3040515107(__this, ___collection0, method) ((  void (*) (List_1_t1722560608 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3040515107_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t1911452592 * List_1_AsReadOnly_m512401410_gshared (List_1_t1722560608 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m512401410(__this, method) ((  ReadOnlyCollection_1_t1911452592 * (*) (List_1_t1722560608 *, const MethodInfo*))List_1_AsReadOnly_m512401410_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Clear()
extern "C"  void List_1_Clear_m853680303_gshared (List_1_t1722560608 * __this, const MethodInfo* method);
#define List_1_Clear_m853680303(__this, method) ((  void (*) (List_1_t1722560608 *, const MethodInfo*))List_1_Clear_m853680303_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Contains(T)
extern "C"  bool List_1_Contains_m680395593_gshared (List_1_t1722560608 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Contains_m680395593(__this, ___item0, method) ((  bool (*) (List_1_t1722560608 *, int32_t, const MethodInfo*))List_1_Contains_m680395593_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2779978411_gshared (List_1_t1722560608 * __this, PIXEL_FORMATU5BU5D_t882070065* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m2779978411(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1722560608 *, PIXEL_FORMATU5BU5D_t882070065*, int32_t, const MethodInfo*))List_1_CopyTo_m2779978411_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Find(System.Predicate`1<T>)
extern "C"  int32_t List_1_Find_m1161076667_gshared (List_1_t1722560608 * __this, Predicate_1_t4260399235 * ___match0, const MethodInfo* method);
#define List_1_Find_m1161076667(__this, ___match0, method) ((  int32_t (*) (List_1_t1722560608 *, Predicate_1_t4260399235 *, const MethodInfo*))List_1_Find_m1161076667_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m612029592_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t4260399235 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m612029592(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t4260399235 *, const MethodInfo*))List_1_CheckMatch_m612029592_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::FindAll(System.Predicate`1<T>)
extern "C"  List_1_t1722560608 * List_1_FindAll_m2894713928_gshared (List_1_t1722560608 * __this, Predicate_1_t4260399235 * ___match0, const MethodInfo* method);
#define List_1_FindAll_m2894713928(__this, ___match0, method) ((  List_1_t1722560608 * (*) (List_1_t1722560608 *, Predicate_1_t4260399235 *, const MethodInfo*))List_1_FindAll_m2894713928_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::FindAllStackBits(System.Predicate`1<T>)
extern "C"  List_1_t1722560608 * List_1_FindAllStackBits_m3696846290_gshared (List_1_t1722560608 * __this, Predicate_1_t4260399235 * ___match0, const MethodInfo* method);
#define List_1_FindAllStackBits_m3696846290(__this, ___match0, method) ((  List_1_t1722560608 * (*) (List_1_t1722560608 *, Predicate_1_t4260399235 *, const MethodInfo*))List_1_FindAllStackBits_m3696846290_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::FindAllList(System.Predicate`1<T>)
extern "C"  List_1_t1722560608 * List_1_FindAllList_m2651828170_gshared (List_1_t1722560608 * __this, Predicate_1_t4260399235 * ___match0, const MethodInfo* method);
#define List_1_FindAllList_m2651828170(__this, ___match0, method) ((  List_1_t1722560608 * (*) (List_1_t1722560608 *, Predicate_1_t4260399235 *, const MethodInfo*))List_1_FindAllList_m2651828170_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m3652258165_gshared (List_1_t1722560608 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t4260399235 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m3652258165(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1722560608 *, int32_t, int32_t, Predicate_1_t4260399235 *, const MethodInfo*))List_1_GetIndex_m3652258165_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::ForEach(System.Action`1<T>)
extern "C"  void List_1_ForEach_m3650552332_gshared (List_1_t1722560608 * __this, Action_1_t750191192 * ___action0, const MethodInfo* method);
#define List_1_ForEach_m3650552332(__this, ___action0, method) ((  void (*) (List_1_t1722560608 *, Action_1_t750191192 *, const MethodInfo*))List_1_ForEach_m3650552332_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::GetEnumerator()
extern "C"  Enumerator_t1742233378  List_1_GetEnumerator_m3833138014_gshared (List_1_t1722560608 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m3833138014(__this, method) ((  Enumerator_t1742233378  (*) (List_1_t1722560608 *, const MethodInfo*))List_1_GetEnumerator_m3833138014_gshared)(__this, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::GetRange(System.Int32,System.Int32)
extern "C"  List_1_t1722560608 * List_1_GetRange_m3599789538_gshared (List_1_t1722560608 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_GetRange_m3599789538(__this, ___index0, ___count1, method) ((  List_1_t1722560608 * (*) (List_1_t1722560608 *, int32_t, int32_t, const MethodInfo*))List_1_GetRange_m3599789538_gshared)(__this, ___index0, ___count1, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m3810515127_gshared (List_1_t1722560608 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m3810515127(__this, ___item0, method) ((  int32_t (*) (List_1_t1722560608 *, int32_t, const MethodInfo*))List_1_IndexOf_m3810515127_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m2886145538_gshared (List_1_t1722560608 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m2886145538(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1722560608 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m2886145538_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m2526345595_gshared (List_1_t1722560608 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m2526345595(__this, ___index0, method) ((  void (*) (List_1_t1722560608 *, int32_t, const MethodInfo*))List_1_CheckIndex_m2526345595_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m471719906_gshared (List_1_t1722560608 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define List_1_Insert_m471719906(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1722560608 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m471719906_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m4025119447_gshared (List_1_t1722560608 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m4025119447(__this, ___collection0, method) ((  void (*) (List_1_t1722560608 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m4025119447_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Remove(T)
extern "C"  bool List_1_Remove_m273312046_gshared (List_1_t1722560608 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Remove_m273312046(__this, ___item0, method) ((  bool (*) (List_1_t1722560608 *, int32_t, const MethodInfo*))List_1_Remove_m273312046_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m1371352378_gshared (List_1_t1722560608 * __this, Predicate_1_t4260399235 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m1371352378(__this, ___match0, method) ((  int32_t (*) (List_1_t1722560608 *, Predicate_1_t4260399235 *, const MethodInfo*))List_1_RemoveAll_m1371352378_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m2640540072_gshared (List_1_t1722560608 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m2640540072(__this, ___index0, method) ((  void (*) (List_1_t1722560608 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2640540072_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m1131871691_gshared (List_1_t1722560608 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m1131871691(__this, ___index0, ___count1, method) ((  void (*) (List_1_t1722560608 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m1131871691_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Reverse()
extern "C"  void List_1_Reverse_m1710704580_gshared (List_1_t1722560608 * __this, const MethodInfo* method);
#define List_1_Reverse_m1710704580(__this, method) ((  void (*) (List_1_t1722560608 *, const MethodInfo*))List_1_Reverse_m1710704580_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Sort()
extern "C"  void List_1_Sort_m1597156958_gshared (List_1_t1722560608 * __this, const MethodInfo* method);
#define List_1_Sort_m1597156958(__this, method) ((  void (*) (List_1_t1722560608 *, const MethodInfo*))List_1_Sort_m1597156958_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m4059552070_gshared (List_1_t1722560608 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m4059552070(__this, ___comparer0, method) ((  void (*) (List_1_t1722560608 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m4059552070_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m2878898225_gshared (List_1_t1722560608 * __this, Comparison_1_t3365703539 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m2878898225(__this, ___comparison0, method) ((  void (*) (List_1_t1722560608 *, Comparison_1_t3365703539 *, const MethodInfo*))List_1_Sort_m2878898225_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::ToArray()
extern "C"  PIXEL_FORMATU5BU5D_t882070065* List_1_ToArray_m3260422429_gshared (List_1_t1722560608 * __this, const MethodInfo* method);
#define List_1_ToArray_m3260422429(__this, method) ((  PIXEL_FORMATU5BU5D_t882070065* (*) (List_1_t1722560608 *, const MethodInfo*))List_1_ToArray_m3260422429_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2192641911_gshared (List_1_t1722560608 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m2192641911(__this, method) ((  void (*) (List_1_t1722560608 *, const MethodInfo*))List_1_TrimExcess_m2192641911_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::TrueForAll(System.Predicate`1<T>)
extern "C"  bool List_1_TrueForAll_m1350296529_gshared (List_1_t1722560608 * __this, Predicate_1_t4260399235 * ___match0, const MethodInfo* method);
#define List_1_TrueForAll_m1350296529(__this, ___match0, method) ((  bool (*) (List_1_t1722560608 *, Predicate_1_t4260399235 *, const MethodInfo*))List_1_TrueForAll_m1350296529_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m161914215_gshared (List_1_t1722560608 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m161914215(__this, method) ((  int32_t (*) (List_1_t1722560608 *, const MethodInfo*))List_1_get_Capacity_m161914215_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m4017453640_gshared (List_1_t1722560608 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m4017453640(__this, ___value0, method) ((  void (*) (List_1_t1722560608 *, int32_t, const MethodInfo*))List_1_set_Capacity_m4017453640_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::get_Count()
extern "C"  int32_t List_1_get_Count_m1979159460_gshared (List_1_t1722560608 * __this, const MethodInfo* method);
#define List_1_get_Count_m1979159460(__this, method) ((  int32_t (*) (List_1_t1722560608 *, const MethodInfo*))List_1_get_Count_m1979159460_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m2219422222_gshared (List_1_t1722560608 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m2219422222(__this, ___index0, method) ((  int32_t (*) (List_1_t1722560608 *, int32_t, const MethodInfo*))List_1_get_Item_m2219422222_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m4160860217_gshared (List_1_t1722560608 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m4160860217(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1722560608 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m4160860217_gshared)(__this, ___index0, ___value1, method)
