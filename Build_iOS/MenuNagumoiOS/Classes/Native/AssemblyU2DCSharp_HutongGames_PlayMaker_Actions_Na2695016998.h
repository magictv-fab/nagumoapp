﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// PlayMakerAnimatorMoveProxy
struct PlayMakerAnimatorMoveProxy_t4175490694;
// UnityEngine.Animator
struct Animator_t2776330603;
// UnityEngine.NavMeshAgent
struct NavMeshAgent_t588466745;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NavMeshAgentAnimatorSynchronizer
struct  NavMeshAgentAnimatorSynchronizer_t2695016998  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.NavMeshAgentAnimatorSynchronizer::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// PlayMakerAnimatorMoveProxy HutongGames.PlayMaker.Actions.NavMeshAgentAnimatorSynchronizer::_animatorProxy
	PlayMakerAnimatorMoveProxy_t4175490694 * ____animatorProxy_10;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.NavMeshAgentAnimatorSynchronizer::_animator
	Animator_t2776330603 * ____animator_11;
	// UnityEngine.NavMeshAgent HutongGames.PlayMaker.Actions.NavMeshAgentAnimatorSynchronizer::_agent
	NavMeshAgent_t588466745 * ____agent_12;
	// UnityEngine.Transform HutongGames.PlayMaker.Actions.NavMeshAgentAnimatorSynchronizer::_trans
	Transform_t1659122786 * ____trans_13;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(NavMeshAgentAnimatorSynchronizer_t2695016998, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of__animatorProxy_10() { return static_cast<int32_t>(offsetof(NavMeshAgentAnimatorSynchronizer_t2695016998, ____animatorProxy_10)); }
	inline PlayMakerAnimatorMoveProxy_t4175490694 * get__animatorProxy_10() const { return ____animatorProxy_10; }
	inline PlayMakerAnimatorMoveProxy_t4175490694 ** get_address_of__animatorProxy_10() { return &____animatorProxy_10; }
	inline void set__animatorProxy_10(PlayMakerAnimatorMoveProxy_t4175490694 * value)
	{
		____animatorProxy_10 = value;
		Il2CppCodeGenWriteBarrier(&____animatorProxy_10, value);
	}

	inline static int32_t get_offset_of__animator_11() { return static_cast<int32_t>(offsetof(NavMeshAgentAnimatorSynchronizer_t2695016998, ____animator_11)); }
	inline Animator_t2776330603 * get__animator_11() const { return ____animator_11; }
	inline Animator_t2776330603 ** get_address_of__animator_11() { return &____animator_11; }
	inline void set__animator_11(Animator_t2776330603 * value)
	{
		____animator_11 = value;
		Il2CppCodeGenWriteBarrier(&____animator_11, value);
	}

	inline static int32_t get_offset_of__agent_12() { return static_cast<int32_t>(offsetof(NavMeshAgentAnimatorSynchronizer_t2695016998, ____agent_12)); }
	inline NavMeshAgent_t588466745 * get__agent_12() const { return ____agent_12; }
	inline NavMeshAgent_t588466745 ** get_address_of__agent_12() { return &____agent_12; }
	inline void set__agent_12(NavMeshAgent_t588466745 * value)
	{
		____agent_12 = value;
		Il2CppCodeGenWriteBarrier(&____agent_12, value);
	}

	inline static int32_t get_offset_of__trans_13() { return static_cast<int32_t>(offsetof(NavMeshAgentAnimatorSynchronizer_t2695016998, ____trans_13)); }
	inline Transform_t1659122786 * get__trans_13() const { return ____trans_13; }
	inline Transform_t1659122786 ** get_address_of__trans_13() { return &____trans_13; }
	inline void set__trans_13(Transform_t1659122786 * value)
	{
		____trans_13 = value;
		Il2CppCodeGenWriteBarrier(&____trans_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
