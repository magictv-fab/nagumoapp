﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GameObjectIsNull
struct  GameObjectIsNull_t2162669226  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GameObjectIsNull::gameObject
	FsmGameObject_t1697147867 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GameObjectIsNull::isNull
	FsmEvent_t2133468028 * ___isNull_10;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GameObjectIsNull::isNotNull
	FsmEvent_t2133468028 * ___isNotNull_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GameObjectIsNull::storeResult
	FsmBool_t1075959796 * ___storeResult_12;
	// System.Boolean HutongGames.PlayMaker.Actions.GameObjectIsNull::everyFrame
	bool ___everyFrame_13;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(GameObjectIsNull_t2162669226, ___gameObject_9)); }
	inline FsmGameObject_t1697147867 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmGameObject_t1697147867 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmGameObject_t1697147867 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_isNull_10() { return static_cast<int32_t>(offsetof(GameObjectIsNull_t2162669226, ___isNull_10)); }
	inline FsmEvent_t2133468028 * get_isNull_10() const { return ___isNull_10; }
	inline FsmEvent_t2133468028 ** get_address_of_isNull_10() { return &___isNull_10; }
	inline void set_isNull_10(FsmEvent_t2133468028 * value)
	{
		___isNull_10 = value;
		Il2CppCodeGenWriteBarrier(&___isNull_10, value);
	}

	inline static int32_t get_offset_of_isNotNull_11() { return static_cast<int32_t>(offsetof(GameObjectIsNull_t2162669226, ___isNotNull_11)); }
	inline FsmEvent_t2133468028 * get_isNotNull_11() const { return ___isNotNull_11; }
	inline FsmEvent_t2133468028 ** get_address_of_isNotNull_11() { return &___isNotNull_11; }
	inline void set_isNotNull_11(FsmEvent_t2133468028 * value)
	{
		___isNotNull_11 = value;
		Il2CppCodeGenWriteBarrier(&___isNotNull_11, value);
	}

	inline static int32_t get_offset_of_storeResult_12() { return static_cast<int32_t>(offsetof(GameObjectIsNull_t2162669226, ___storeResult_12)); }
	inline FsmBool_t1075959796 * get_storeResult_12() const { return ___storeResult_12; }
	inline FsmBool_t1075959796 ** get_address_of_storeResult_12() { return &___storeResult_12; }
	inline void set_storeResult_12(FsmBool_t1075959796 * value)
	{
		___storeResult_12 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_12, value);
	}

	inline static int32_t get_offset_of_everyFrame_13() { return static_cast<int32_t>(offsetof(GameObjectIsNull_t2162669226, ___everyFrame_13)); }
	inline bool get_everyFrame_13() const { return ___everyFrame_13; }
	inline bool* get_address_of_everyFrame_13() { return &___everyFrame_13; }
	inline void set_everyFrame_13(bool value)
	{
		___everyFrame_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
