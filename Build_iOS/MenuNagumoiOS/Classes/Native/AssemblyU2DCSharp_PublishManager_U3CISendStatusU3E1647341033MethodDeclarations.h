﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PublishManager/<ISendStatus>c__Iterator75
struct U3CISendStatusU3Ec__Iterator75_t1647341033;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PublishManager/<ISendStatus>c__Iterator75::.ctor()
extern "C"  void U3CISendStatusU3Ec__Iterator75__ctor_m1933548962 (U3CISendStatusU3Ec__Iterator75_t1647341033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PublishManager/<ISendStatus>c__Iterator75::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CISendStatusU3Ec__Iterator75_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2571734320 (U3CISendStatusU3Ec__Iterator75_t1647341033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PublishManager/<ISendStatus>c__Iterator75::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CISendStatusU3Ec__Iterator75_System_Collections_IEnumerator_get_Current_m2249978052 (U3CISendStatusU3Ec__Iterator75_t1647341033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PublishManager/<ISendStatus>c__Iterator75::MoveNext()
extern "C"  bool U3CISendStatusU3Ec__Iterator75_MoveNext_m1895134738 (U3CISendStatusU3Ec__Iterator75_t1647341033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PublishManager/<ISendStatus>c__Iterator75::Dispose()
extern "C"  void U3CISendStatusU3Ec__Iterator75_Dispose_m2612963679 (U3CISendStatusU3Ec__Iterator75_t1647341033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PublishManager/<ISendStatus>c__Iterator75::Reset()
extern "C"  void U3CISendStatusU3Ec__Iterator75_Reset_m3874949199 (U3CISendStatusU3Ec__Iterator75_t1647341033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
