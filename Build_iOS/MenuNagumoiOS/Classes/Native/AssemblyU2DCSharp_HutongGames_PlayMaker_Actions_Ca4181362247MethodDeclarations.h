﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.CallMethod
struct CallMethod_t4181362247;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.CallMethod::.ctor()
extern "C"  void CallMethod__ctor_m2075553279 (CallMethod_t4181362247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CallMethod::OnEnter()
extern "C"  void CallMethod_OnEnter_m1133604374 (CallMethod_t4181362247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CallMethod::OnUpdate()
extern "C"  void CallMethod_OnUpdate_m4210523885 (CallMethod_t4181362247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CallMethod::DoMethodCall()
extern "C"  void CallMethod_DoMethodCall_m2184409775 (CallMethod_t4181362247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.CallMethod::DoCache()
extern "C"  bool CallMethod_DoCache_m3088570592 (CallMethod_t4181362247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Actions.CallMethod::ErrorCheck()
extern "C"  String_t* CallMethod_ErrorCheck_m3980933672 (CallMethod_t4181362247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
