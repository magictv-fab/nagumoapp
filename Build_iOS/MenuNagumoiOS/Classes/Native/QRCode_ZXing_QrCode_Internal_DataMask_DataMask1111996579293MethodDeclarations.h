﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.QrCode.Internal.DataMask/DataMask111
struct DataMask111_t1996579293;

#include "codegen/il2cpp-codegen.h"

// System.Boolean ZXing.QrCode.Internal.DataMask/DataMask111::isMasked(System.Int32,System.Int32)
extern "C"  bool DataMask111_isMasked_m2646072599 (DataMask111_t1996579293 * __this, int32_t ___i0, int32_t ___j1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.DataMask/DataMask111::.ctor()
extern "C"  void DataMask111__ctor_m1221843966 (DataMask111_t1996579293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
