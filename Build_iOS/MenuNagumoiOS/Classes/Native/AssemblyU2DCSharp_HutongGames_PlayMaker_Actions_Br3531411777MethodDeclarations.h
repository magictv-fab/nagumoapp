﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.BroadcastEvent
struct BroadcastEvent_t3531411777;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.BroadcastEvent::.ctor()
extern "C"  void BroadcastEvent__ctor_m2296303685 (BroadcastEvent_t3531411777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BroadcastEvent::Reset()
extern "C"  void BroadcastEvent_Reset_m4237703922 (BroadcastEvent_t3531411777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BroadcastEvent::OnEnter()
extern "C"  void BroadcastEvent_OnEnter_m2821347036 (BroadcastEvent_t3531411777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
