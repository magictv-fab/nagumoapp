﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ARM_transform_filters_abstracts_4158746314.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.transform.filters.abstracts.histories.HistoryAngleFilterAbstract
struct  HistoryAngleFilterAbstract_t4059889395  : public TransformPositionAngleFilterAbstract_t4158746314
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
