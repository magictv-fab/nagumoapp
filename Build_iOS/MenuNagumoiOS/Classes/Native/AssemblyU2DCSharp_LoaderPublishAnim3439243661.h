﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// LoaderPublishAnim
struct LoaderPublishAnim_t3439243661;
// UnityEngine.UI.Image
struct Image_t538875265;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t1266085011;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoaderPublishAnim
struct  LoaderPublishAnim_t3439243661  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.Image LoaderPublishAnim::image
	Image_t538875265 * ___image_3;
	// System.Single LoaderPublishAnim::yMax
	float ___yMax_4;
	// UnityEngine.GameObject LoaderPublishAnim::container
	GameObject_t3674682005 * ___container_5;
	// System.Single LoaderPublishAnim::multiplier
	float ___multiplier_6;
	// UnityEngine.Events.UnityEvent LoaderPublishAnim::onLoad
	UnityEvent_t1266085011 * ___onLoad_7;
	// System.Boolean LoaderPublishAnim::isLoading
	bool ___isLoading_8;
	// UnityEngine.Vector3 LoaderPublishAnim::startPos
	Vector3_t4282066566  ___startPos_9;
	// UnityEngine.Vector3 LoaderPublishAnim::startDiff
	Vector3_t4282066566  ___startDiff_10;

public:
	inline static int32_t get_offset_of_image_3() { return static_cast<int32_t>(offsetof(LoaderPublishAnim_t3439243661, ___image_3)); }
	inline Image_t538875265 * get_image_3() const { return ___image_3; }
	inline Image_t538875265 ** get_address_of_image_3() { return &___image_3; }
	inline void set_image_3(Image_t538875265 * value)
	{
		___image_3 = value;
		Il2CppCodeGenWriteBarrier(&___image_3, value);
	}

	inline static int32_t get_offset_of_yMax_4() { return static_cast<int32_t>(offsetof(LoaderPublishAnim_t3439243661, ___yMax_4)); }
	inline float get_yMax_4() const { return ___yMax_4; }
	inline float* get_address_of_yMax_4() { return &___yMax_4; }
	inline void set_yMax_4(float value)
	{
		___yMax_4 = value;
	}

	inline static int32_t get_offset_of_container_5() { return static_cast<int32_t>(offsetof(LoaderPublishAnim_t3439243661, ___container_5)); }
	inline GameObject_t3674682005 * get_container_5() const { return ___container_5; }
	inline GameObject_t3674682005 ** get_address_of_container_5() { return &___container_5; }
	inline void set_container_5(GameObject_t3674682005 * value)
	{
		___container_5 = value;
		Il2CppCodeGenWriteBarrier(&___container_5, value);
	}

	inline static int32_t get_offset_of_multiplier_6() { return static_cast<int32_t>(offsetof(LoaderPublishAnim_t3439243661, ___multiplier_6)); }
	inline float get_multiplier_6() const { return ___multiplier_6; }
	inline float* get_address_of_multiplier_6() { return &___multiplier_6; }
	inline void set_multiplier_6(float value)
	{
		___multiplier_6 = value;
	}

	inline static int32_t get_offset_of_onLoad_7() { return static_cast<int32_t>(offsetof(LoaderPublishAnim_t3439243661, ___onLoad_7)); }
	inline UnityEvent_t1266085011 * get_onLoad_7() const { return ___onLoad_7; }
	inline UnityEvent_t1266085011 ** get_address_of_onLoad_7() { return &___onLoad_7; }
	inline void set_onLoad_7(UnityEvent_t1266085011 * value)
	{
		___onLoad_7 = value;
		Il2CppCodeGenWriteBarrier(&___onLoad_7, value);
	}

	inline static int32_t get_offset_of_isLoading_8() { return static_cast<int32_t>(offsetof(LoaderPublishAnim_t3439243661, ___isLoading_8)); }
	inline bool get_isLoading_8() const { return ___isLoading_8; }
	inline bool* get_address_of_isLoading_8() { return &___isLoading_8; }
	inline void set_isLoading_8(bool value)
	{
		___isLoading_8 = value;
	}

	inline static int32_t get_offset_of_startPos_9() { return static_cast<int32_t>(offsetof(LoaderPublishAnim_t3439243661, ___startPos_9)); }
	inline Vector3_t4282066566  get_startPos_9() const { return ___startPos_9; }
	inline Vector3_t4282066566 * get_address_of_startPos_9() { return &___startPos_9; }
	inline void set_startPos_9(Vector3_t4282066566  value)
	{
		___startPos_9 = value;
	}

	inline static int32_t get_offset_of_startDiff_10() { return static_cast<int32_t>(offsetof(LoaderPublishAnim_t3439243661, ___startDiff_10)); }
	inline Vector3_t4282066566  get_startDiff_10() const { return ___startDiff_10; }
	inline Vector3_t4282066566 * get_address_of_startDiff_10() { return &___startDiff_10; }
	inline void set_startDiff_10(Vector3_t4282066566  value)
	{
		___startDiff_10 = value;
	}
};

struct LoaderPublishAnim_t3439243661_StaticFields
{
public:
	// LoaderPublishAnim LoaderPublishAnim::instance
	LoaderPublishAnim_t3439243661 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(LoaderPublishAnim_t3439243661_StaticFields, ___instance_2)); }
	inline LoaderPublishAnim_t3439243661 * get_instance_2() const { return ___instance_2; }
	inline LoaderPublishAnim_t3439243661 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(LoaderPublishAnim_t3439243661 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
