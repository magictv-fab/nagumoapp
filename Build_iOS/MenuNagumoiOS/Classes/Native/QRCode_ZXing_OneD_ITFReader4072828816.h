﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t1820556512;

#include "QRCode_ZXing_OneD_OneDReader3436042911.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.ITFReader
struct  ITFReader_t4072828816  : public OneDReader_t3436042911
{
public:
	// System.Int32 ZXing.OneD.ITFReader::narrowLineWidth
	int32_t ___narrowLineWidth_5;

public:
	inline static int32_t get_offset_of_narrowLineWidth_5() { return static_cast<int32_t>(offsetof(ITFReader_t4072828816, ___narrowLineWidth_5)); }
	inline int32_t get_narrowLineWidth_5() const { return ___narrowLineWidth_5; }
	inline int32_t* get_address_of_narrowLineWidth_5() { return &___narrowLineWidth_5; }
	inline void set_narrowLineWidth_5(int32_t value)
	{
		___narrowLineWidth_5 = value;
	}
};

struct ITFReader_t4072828816_StaticFields
{
public:
	// System.Int32 ZXing.OneD.ITFReader::MAX_AVG_VARIANCE
	int32_t ___MAX_AVG_VARIANCE_2;
	// System.Int32 ZXing.OneD.ITFReader::MAX_INDIVIDUAL_VARIANCE
	int32_t ___MAX_INDIVIDUAL_VARIANCE_3;
	// System.Int32[] ZXing.OneD.ITFReader::DEFAULT_ALLOWED_LENGTHS
	Int32U5BU5D_t3230847821* ___DEFAULT_ALLOWED_LENGTHS_4;
	// System.Int32[] ZXing.OneD.ITFReader::START_PATTERN
	Int32U5BU5D_t3230847821* ___START_PATTERN_6;
	// System.Int32[] ZXing.OneD.ITFReader::END_PATTERN_REVERSED
	Int32U5BU5D_t3230847821* ___END_PATTERN_REVERSED_7;
	// System.Int32[][] ZXing.OneD.ITFReader::PATTERNS
	Int32U5BU5DU5BU5D_t1820556512* ___PATTERNS_8;

public:
	inline static int32_t get_offset_of_MAX_AVG_VARIANCE_2() { return static_cast<int32_t>(offsetof(ITFReader_t4072828816_StaticFields, ___MAX_AVG_VARIANCE_2)); }
	inline int32_t get_MAX_AVG_VARIANCE_2() const { return ___MAX_AVG_VARIANCE_2; }
	inline int32_t* get_address_of_MAX_AVG_VARIANCE_2() { return &___MAX_AVG_VARIANCE_2; }
	inline void set_MAX_AVG_VARIANCE_2(int32_t value)
	{
		___MAX_AVG_VARIANCE_2 = value;
	}

	inline static int32_t get_offset_of_MAX_INDIVIDUAL_VARIANCE_3() { return static_cast<int32_t>(offsetof(ITFReader_t4072828816_StaticFields, ___MAX_INDIVIDUAL_VARIANCE_3)); }
	inline int32_t get_MAX_INDIVIDUAL_VARIANCE_3() const { return ___MAX_INDIVIDUAL_VARIANCE_3; }
	inline int32_t* get_address_of_MAX_INDIVIDUAL_VARIANCE_3() { return &___MAX_INDIVIDUAL_VARIANCE_3; }
	inline void set_MAX_INDIVIDUAL_VARIANCE_3(int32_t value)
	{
		___MAX_INDIVIDUAL_VARIANCE_3 = value;
	}

	inline static int32_t get_offset_of_DEFAULT_ALLOWED_LENGTHS_4() { return static_cast<int32_t>(offsetof(ITFReader_t4072828816_StaticFields, ___DEFAULT_ALLOWED_LENGTHS_4)); }
	inline Int32U5BU5D_t3230847821* get_DEFAULT_ALLOWED_LENGTHS_4() const { return ___DEFAULT_ALLOWED_LENGTHS_4; }
	inline Int32U5BU5D_t3230847821** get_address_of_DEFAULT_ALLOWED_LENGTHS_4() { return &___DEFAULT_ALLOWED_LENGTHS_4; }
	inline void set_DEFAULT_ALLOWED_LENGTHS_4(Int32U5BU5D_t3230847821* value)
	{
		___DEFAULT_ALLOWED_LENGTHS_4 = value;
		Il2CppCodeGenWriteBarrier(&___DEFAULT_ALLOWED_LENGTHS_4, value);
	}

	inline static int32_t get_offset_of_START_PATTERN_6() { return static_cast<int32_t>(offsetof(ITFReader_t4072828816_StaticFields, ___START_PATTERN_6)); }
	inline Int32U5BU5D_t3230847821* get_START_PATTERN_6() const { return ___START_PATTERN_6; }
	inline Int32U5BU5D_t3230847821** get_address_of_START_PATTERN_6() { return &___START_PATTERN_6; }
	inline void set_START_PATTERN_6(Int32U5BU5D_t3230847821* value)
	{
		___START_PATTERN_6 = value;
		Il2CppCodeGenWriteBarrier(&___START_PATTERN_6, value);
	}

	inline static int32_t get_offset_of_END_PATTERN_REVERSED_7() { return static_cast<int32_t>(offsetof(ITFReader_t4072828816_StaticFields, ___END_PATTERN_REVERSED_7)); }
	inline Int32U5BU5D_t3230847821* get_END_PATTERN_REVERSED_7() const { return ___END_PATTERN_REVERSED_7; }
	inline Int32U5BU5D_t3230847821** get_address_of_END_PATTERN_REVERSED_7() { return &___END_PATTERN_REVERSED_7; }
	inline void set_END_PATTERN_REVERSED_7(Int32U5BU5D_t3230847821* value)
	{
		___END_PATTERN_REVERSED_7 = value;
		Il2CppCodeGenWriteBarrier(&___END_PATTERN_REVERSED_7, value);
	}

	inline static int32_t get_offset_of_PATTERNS_8() { return static_cast<int32_t>(offsetof(ITFReader_t4072828816_StaticFields, ___PATTERNS_8)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_PATTERNS_8() const { return ___PATTERNS_8; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_PATTERNS_8() { return &___PATTERNS_8; }
	inline void set_PATTERNS_8(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___PATTERNS_8 = value;
		Il2CppCodeGenWriteBarrier(&___PATTERNS_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
