﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.JsonMapper/<ReadValue>c__AnonStoreyA1
struct  U3CReadValueU3Ec__AnonStoreyA1_t1554182191  : public Il2CppObject
{
public:
	// System.String LitJson.JsonMapper/<ReadValue>c__AnonStoreyA1::property
	String_t* ___property_0;

public:
	inline static int32_t get_offset_of_property_0() { return static_cast<int32_t>(offsetof(U3CReadValueU3Ec__AnonStoreyA1_t1554182191, ___property_0)); }
	inline String_t* get_property_0() const { return ___property_0; }
	inline String_t** get_address_of_property_0() { return &___property_0; }
	inline void set_property_0(String_t* value)
	{
		___property_0 = value;
		Il2CppCodeGenWriteBarrier(&___property_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
