﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTVWindowInAppListControllerScriptWaiting
struct MagicTVWindowInAppListControllerScriptWaiting_t3050884011;
// System.String
struct String_t;
// ARM.utils.request.ARMRequestVO
struct ARMRequestVO_t2431191322;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ARM_utils_request_ARMRequestVO2431191322.h"

// System.Void MagicTVWindowInAppListControllerScriptWaiting::.ctor()
extern "C"  void MagicTVWindowInAppListControllerScriptWaiting__ctor_m3284592160 (MagicTVWindowInAppListControllerScriptWaiting_t3050884011 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowInAppListControllerScriptWaiting::StarClick()
extern "C"  void MagicTVWindowInAppListControllerScriptWaiting_StarClick_m3516766612 (MagicTVWindowInAppListControllerScriptWaiting_t3050884011 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowInAppListControllerScriptWaiting::SetNextImage(System.String)
extern "C"  void MagicTVWindowInAppListControllerScriptWaiting_SetNextImage_m2228191416 (MagicTVWindowInAppListControllerScriptWaiting_t3050884011 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowInAppListControllerScriptWaiting::onImageLoaded(ARM.utils.request.ARMRequestVO)
extern "C"  void MagicTVWindowInAppListControllerScriptWaiting_onImageLoaded_m3672679685 (MagicTVWindowInAppListControllerScriptWaiting_t3050884011 * __this, ARMRequestVO_t2431191322 * ___vo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
