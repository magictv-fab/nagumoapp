﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ConvertBoolToString
struct ConvertBoolToString_t942610519;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToString::.ctor()
extern "C"  void ConvertBoolToString__ctor_m2378226879 (ConvertBoolToString_t942610519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToString::Reset()
extern "C"  void ConvertBoolToString_Reset_m24659820 (ConvertBoolToString_t942610519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToString::OnEnter()
extern "C"  void ConvertBoolToString_OnEnter_m4240125142 (ConvertBoolToString_t942610519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToString::OnUpdate()
extern "C"  void ConvertBoolToString_OnUpdate_m1728419885 (ConvertBoolToString_t942610519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToString::DoConvertBoolToString()
extern "C"  void ConvertBoolToString_DoConvertBoolToString_m3919444123 (ConvertBoolToString_t942610519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
