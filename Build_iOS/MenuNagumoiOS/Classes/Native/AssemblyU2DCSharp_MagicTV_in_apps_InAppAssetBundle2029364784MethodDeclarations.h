﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.in_apps.InAppAssetBundlePresentation
struct InAppAssetBundlePresentation_t2029364784;
// MagicTV.vo.BundleVO
struct BundleVO_t1984518073;
// System.String
struct String_t;
// ARM.utils.request.ARMRequestVO
struct ARMRequestVO_t2431191322;
// UnityEngine.AssetBundle
struct AssetBundle_t2070959688;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_vo_BundleVO1984518073.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ARM_utils_request_ARMRequestVO2431191322.h"
#include "UnityEngine_UnityEngine_AssetBundle2070959688.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Ray3134616544.h"

// System.Void MagicTV.in_apps.InAppAssetBundlePresentation::.ctor()
extern "C"  void InAppAssetBundlePresentation__ctor_m3773394186 (InAppAssetBundlePresentation_t2029364784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAssetBundlePresentation::Start()
extern "C"  void InAppAssetBundlePresentation_Start_m2720531978 (InAppAssetBundlePresentation_t2029364784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAssetBundlePresentation::SetBundleVO(MagicTV.vo.BundleVO)
extern "C"  void InAppAssetBundlePresentation_SetBundleVO_m3572281916 (InAppAssetBundlePresentation_t2029364784 * __this, BundleVO_t1984518073 * ___bundle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAssetBundlePresentation::prepareModel3D(System.String)
extern "C"  void InAppAssetBundlePresentation_prepareModel3D_m3245762805 (InAppAssetBundlePresentation_t2029364784 * __this, String_t* ___assetPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAssetBundlePresentation::onAssetBundleLoaded(ARM.utils.request.ARMRequestVO)
extern "C"  void InAppAssetBundlePresentation_onAssetBundleLoaded_m3876755302 (InAppAssetBundlePresentation_t2029364784 * __this, ARMRequestVO_t2431191322 * ___vo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAssetBundlePresentation::instantiateLoadedAssetBundle(UnityEngine.AssetBundle)
extern "C"  void InAppAssetBundlePresentation_instantiateLoadedAssetBundle_m1256586138 (InAppAssetBundlePresentation_t2029364784 * __this, AssetBundle_t2070959688 * ___model_asset0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAssetBundlePresentation::FSMIsReady()
extern "C"  void InAppAssetBundlePresentation_FSMIsReady_m211196723 (InAppAssetBundlePresentation_t2029364784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAssetBundlePresentation::InternalAppsReady()
extern "C"  void InAppAssetBundlePresentation_InternalAppsReady_m1107300028 (InAppAssetBundlePresentation_t2029364784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAssetBundlePresentation::checkLink(UnityEngine.GameObject)
extern "C"  void InAppAssetBundlePresentation_checkLink_m1511483234 (InAppAssetBundlePresentation_t2029364784 * __this, GameObject_t3674682005 * ___model3d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAssetBundlePresentation::setExternalTo(System.String,UnityEngine.GameObject)
extern "C"  void InAppAssetBundlePresentation_setExternalTo_m2410706220 (InAppAssetBundlePresentation_t2029364784 * __this, String_t* ___value0, GameObject_t3674682005 * ___model3D1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAssetBundlePresentation::openLink()
extern "C"  void InAppAssetBundlePresentation_openLink_m1106002622 (InAppAssetBundlePresentation_t2029364784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAssetBundlePresentation::doOpenLink()
extern "C"  void InAppAssetBundlePresentation_doOpenLink_m2887075689 (InAppAssetBundlePresentation_t2029364784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAssetBundlePresentation::LateUpdate()
extern "C"  void InAppAssetBundlePresentation_LateUpdate_m3537400713 (InAppAssetBundlePresentation_t2029364784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAssetBundlePresentation::UnlockClick()
extern "C"  void InAppAssetBundlePresentation_UnlockClick_m2271502476 (InAppAssetBundlePresentation_t2029364784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAssetBundlePresentation::checkClick()
extern "C"  void InAppAssetBundlePresentation_checkClick_m3425304954 (InAppAssetBundlePresentation_t2029364784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Ray MagicTV.in_apps.InAppAssetBundlePresentation::getRay()
extern "C"  Ray_t3134616544  InAppAssetBundlePresentation_getRay_m4178658670 (InAppAssetBundlePresentation_t2029364784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAssetBundlePresentation::prepare()
extern "C"  void InAppAssetBundlePresentation_prepare_m2206395343 (InAppAssetBundlePresentation_t2029364784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAssetBundlePresentation::DoRun()
extern "C"  void InAppAssetBundlePresentation_DoRun_m2135963688 (InAppAssetBundlePresentation_t2029364784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAssetBundlePresentation::DoStop()
extern "C"  void InAppAssetBundlePresentation_DoStop_m1818169511 (InAppAssetBundlePresentation_t2029364784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAssetBundlePresentation::Dispose()
extern "C"  void InAppAssetBundlePresentation_Dispose_m1177697991 (InAppAssetBundlePresentation_t2029364784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.in_apps.InAppAssetBundlePresentation::GetInAppName()
extern "C"  String_t* InAppAssetBundlePresentation_GetInAppName_m284949006 (InAppAssetBundlePresentation_t2029364784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAssetBundlePresentation::Play()
extern "C"  void InAppAssetBundlePresentation_Play_m271582510 (InAppAssetBundlePresentation_t2029364784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAssetBundlePresentation::Pause()
extern "C"  void InAppAssetBundlePresentation_Pause_m3827520158 (InAppAssetBundlePresentation_t2029364784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAssetBundlePresentation::RewindAndPause()
extern "C"  void InAppAssetBundlePresentation_RewindAndPause_m2335382900 (InAppAssetBundlePresentation_t2029364784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAssetBundlePresentation::RewindAndPlay()
extern "C"  void InAppAssetBundlePresentation_RewindAndPlay_m2301659032 (InAppAssetBundlePresentation_t2029364784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAssetBundlePresentation::Hide()
extern "C"  void InAppAssetBundlePresentation_Hide_m39848892 (InAppAssetBundlePresentation_t2029364784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAssetBundlePresentation::Show()
extern "C"  void InAppAssetBundlePresentation_Show_m354191031 (InAppAssetBundlePresentation_t2029364784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAssetBundlePresentation::configInternalApps()
extern "C"  void InAppAssetBundlePresentation_configInternalApps_m3435804171 (InAppAssetBundlePresentation_t2029364784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAssetBundlePresentation::CustomAction(System.String)
extern "C"  void InAppAssetBundlePresentation_CustomAction_m396479329 (InAppAssetBundlePresentation_t2029364784 * __this, String_t* ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppAssetBundlePresentation::OnDestroy()
extern "C"  void InAppAssetBundlePresentation_OnDestroy_m3569986819 (InAppAssetBundlePresentation_t2029364784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
