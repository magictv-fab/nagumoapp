﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.RSS.Expanded.Decoders.AI01decoder
struct AI01decoder_t1016996677;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"

// System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01decoder::.ctor(ZXing.Common.BitArray)
extern "C"  void AI01decoder__ctor_m4171825025 (AI01decoder_t1016996677 * __this, BitArray_t4163851164 * ___information0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01decoder::encodeCompressedGtin(System.Text.StringBuilder,System.Int32)
extern "C"  void AI01decoder_encodeCompressedGtin_m2345944078 (AI01decoder_t1016996677 * __this, StringBuilder_t243639308 * ___buf0, int32_t ___currentPos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01decoder::encodeCompressedGtinWithoutAI(System.Text.StringBuilder,System.Int32,System.Int32)
extern "C"  void AI01decoder_encodeCompressedGtinWithoutAI_m1172763641 (AI01decoder_t1016996677 * __this, StringBuilder_t243639308 * ___buf0, int32_t ___currentPos1, int32_t ___initialBufferPosition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01decoder::appendCheckDigit(System.Text.StringBuilder,System.Int32)
extern "C"  void AI01decoder_appendCheckDigit_m940995492 (Il2CppObject * __this /* static, unused */, StringBuilder_t243639308 * ___buf0, int32_t ___currentPos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01decoder::.cctor()
extern "C"  void AI01decoder__cctor_m1774942145 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
