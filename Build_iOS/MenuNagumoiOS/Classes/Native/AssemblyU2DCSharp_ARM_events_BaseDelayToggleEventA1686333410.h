﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.Single>
struct List_1_t1365137228;

#include "AssemblyU2DCSharp_ARM_abstracts_ToggleOnOffAbstract832893812.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.events.BaseDelayToggleEventAbstract
struct  BaseDelayToggleEventAbstract_t1686333410  : public ToggleOnOffAbstract_t832893812
{
public:
	// System.Single ARM.events.BaseDelayToggleEventAbstract::delayToOn
	float ___delayToOn_7;
	// System.Single ARM.events.BaseDelayToggleEventAbstract::delayToOff
	float ___delayToOff_8;
	// System.Boolean ARM.events.BaseDelayToggleEventAbstract::_active
	bool ____active_9;
	// System.Single ARM.events.BaseDelayToggleEventAbstract::_totalTimeElapsedOn
	float ____totalTimeElapsedOn_10;
	// System.Boolean ARM.events.BaseDelayToggleEventAbstract::_wantToOn
	bool ____wantToOn_11;
	// System.Boolean ARM.events.BaseDelayToggleEventAbstract::_timerEnabledOn
	bool ____timerEnabledOn_12;
	// System.Single ARM.events.BaseDelayToggleEventAbstract::_totalTimeElapsedOff
	float ____totalTimeElapsedOff_13;
	// System.Boolean ARM.events.BaseDelayToggleEventAbstract::_wantToOff
	bool ____wantToOff_14;
	// System.Boolean ARM.events.BaseDelayToggleEventAbstract::_timerEnabledOff
	bool ____timerEnabledOff_15;
	// System.Collections.Generic.List`1<System.Single> ARM.events.BaseDelayToggleEventAbstract::_inicialConfig
	List_1_t1365137228 * ____inicialConfig_16;

public:
	inline static int32_t get_offset_of_delayToOn_7() { return static_cast<int32_t>(offsetof(BaseDelayToggleEventAbstract_t1686333410, ___delayToOn_7)); }
	inline float get_delayToOn_7() const { return ___delayToOn_7; }
	inline float* get_address_of_delayToOn_7() { return &___delayToOn_7; }
	inline void set_delayToOn_7(float value)
	{
		___delayToOn_7 = value;
	}

	inline static int32_t get_offset_of_delayToOff_8() { return static_cast<int32_t>(offsetof(BaseDelayToggleEventAbstract_t1686333410, ___delayToOff_8)); }
	inline float get_delayToOff_8() const { return ___delayToOff_8; }
	inline float* get_address_of_delayToOff_8() { return &___delayToOff_8; }
	inline void set_delayToOff_8(float value)
	{
		___delayToOff_8 = value;
	}

	inline static int32_t get_offset_of__active_9() { return static_cast<int32_t>(offsetof(BaseDelayToggleEventAbstract_t1686333410, ____active_9)); }
	inline bool get__active_9() const { return ____active_9; }
	inline bool* get_address_of__active_9() { return &____active_9; }
	inline void set__active_9(bool value)
	{
		____active_9 = value;
	}

	inline static int32_t get_offset_of__totalTimeElapsedOn_10() { return static_cast<int32_t>(offsetof(BaseDelayToggleEventAbstract_t1686333410, ____totalTimeElapsedOn_10)); }
	inline float get__totalTimeElapsedOn_10() const { return ____totalTimeElapsedOn_10; }
	inline float* get_address_of__totalTimeElapsedOn_10() { return &____totalTimeElapsedOn_10; }
	inline void set__totalTimeElapsedOn_10(float value)
	{
		____totalTimeElapsedOn_10 = value;
	}

	inline static int32_t get_offset_of__wantToOn_11() { return static_cast<int32_t>(offsetof(BaseDelayToggleEventAbstract_t1686333410, ____wantToOn_11)); }
	inline bool get__wantToOn_11() const { return ____wantToOn_11; }
	inline bool* get_address_of__wantToOn_11() { return &____wantToOn_11; }
	inline void set__wantToOn_11(bool value)
	{
		____wantToOn_11 = value;
	}

	inline static int32_t get_offset_of__timerEnabledOn_12() { return static_cast<int32_t>(offsetof(BaseDelayToggleEventAbstract_t1686333410, ____timerEnabledOn_12)); }
	inline bool get__timerEnabledOn_12() const { return ____timerEnabledOn_12; }
	inline bool* get_address_of__timerEnabledOn_12() { return &____timerEnabledOn_12; }
	inline void set__timerEnabledOn_12(bool value)
	{
		____timerEnabledOn_12 = value;
	}

	inline static int32_t get_offset_of__totalTimeElapsedOff_13() { return static_cast<int32_t>(offsetof(BaseDelayToggleEventAbstract_t1686333410, ____totalTimeElapsedOff_13)); }
	inline float get__totalTimeElapsedOff_13() const { return ____totalTimeElapsedOff_13; }
	inline float* get_address_of__totalTimeElapsedOff_13() { return &____totalTimeElapsedOff_13; }
	inline void set__totalTimeElapsedOff_13(float value)
	{
		____totalTimeElapsedOff_13 = value;
	}

	inline static int32_t get_offset_of__wantToOff_14() { return static_cast<int32_t>(offsetof(BaseDelayToggleEventAbstract_t1686333410, ____wantToOff_14)); }
	inline bool get__wantToOff_14() const { return ____wantToOff_14; }
	inline bool* get_address_of__wantToOff_14() { return &____wantToOff_14; }
	inline void set__wantToOff_14(bool value)
	{
		____wantToOff_14 = value;
	}

	inline static int32_t get_offset_of__timerEnabledOff_15() { return static_cast<int32_t>(offsetof(BaseDelayToggleEventAbstract_t1686333410, ____timerEnabledOff_15)); }
	inline bool get__timerEnabledOff_15() const { return ____timerEnabledOff_15; }
	inline bool* get_address_of__timerEnabledOff_15() { return &____timerEnabledOff_15; }
	inline void set__timerEnabledOff_15(bool value)
	{
		____timerEnabledOff_15 = value;
	}

	inline static int32_t get_offset_of__inicialConfig_16() { return static_cast<int32_t>(offsetof(BaseDelayToggleEventAbstract_t1686333410, ____inicialConfig_16)); }
	inline List_1_t1365137228 * get__inicialConfig_16() const { return ____inicialConfig_16; }
	inline List_1_t1365137228 ** get_address_of__inicialConfig_16() { return &____inicialConfig_16; }
	inline void set__inicialConfig_16(List_1_t1365137228 * value)
	{
		____inicialConfig_16 = value;
		Il2CppCodeGenWriteBarrier(&____inicialConfig_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
