﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DevGUIInAppTransform
struct DevGUIInAppTransform_t3180776118;
// MagicTV.abstracts.InAppAbstract
struct InAppAbstract_t382673128;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_abstracts_InAppAbstract382673128.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void DevGUIInAppTransform::.ctor()
extern "C"  void DevGUIInAppTransform__ctor_m3788192933 (DevGUIInAppTransform_t3180776118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DevGUIInAppTransform::OnInApp(MagicTV.abstracts.InAppAbstract)
extern "C"  void DevGUIInAppTransform_OnInApp_m3154277232 (DevGUIInAppTransform_t3180776118 * __this, InAppAbstract_t382673128 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DevGUIInAppTransform::Start()
extern "C"  void DevGUIInAppTransform_Start_m2735330725 (DevGUIInAppTransform_t3180776118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DevGUIInAppTransform::PauseGUIAction()
extern "C"  void DevGUIInAppTransform_PauseGUIAction_m49495482 (DevGUIInAppTransform_t3180776118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DevGUIInAppTransform::PlayGUIAction()
extern "C"  void DevGUIInAppTransform_PlayGUIAction_m306307840 (DevGUIInAppTransform_t3180776118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DevGUIInAppTransform::SetTransform(System.String)
extern "C"  void DevGUIInAppTransform_SetTransform_m1450398969 (DevGUIInAppTransform_t3180776118 * __this, String_t* ___transform0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DevGUIInAppTransform::SetProperty(System.String)
extern "C"  void DevGUIInAppTransform_SetProperty_m2822915688 (DevGUIInAppTransform_t3180776118 * __this, String_t* ___property0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DevGUIInAppTransform::RaisePlusButtonEvent()
extern "C"  void DevGUIInAppTransform_RaisePlusButtonEvent_m4007530113 (DevGUIInAppTransform_t3180776118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DevGUIInAppTransform::RaiseMinusButtonEvent()
extern "C"  void DevGUIInAppTransform_RaiseMinusButtonEvent_m3995706567 (DevGUIInAppTransform_t3180776118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DevGUIInAppTransform::RaiseTransform(UnityEngine.Vector3)
extern "C"  void DevGUIInAppTransform_RaiseTransform_m1333192218 (DevGUIInAppTransform_t3180776118 * __this, Vector3_t4282066566  ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 DevGUIInAppTransform::getTransformVector(System.Single)
extern "C"  Vector3_t4282066566  DevGUIInAppTransform_getTransformVector_m1744260921 (DevGUIInAppTransform_t3180776118 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DevGUIInAppTransform::getChangeValue()
extern "C"  float DevGUIInAppTransform_getChangeValue_m3735958998 (DevGUIInAppTransform_t3180776118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DevGUIInAppTransform::Update()
extern "C"  void DevGUIInAppTransform_Update_m3196726024 (DevGUIInAppTransform_t3180776118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
