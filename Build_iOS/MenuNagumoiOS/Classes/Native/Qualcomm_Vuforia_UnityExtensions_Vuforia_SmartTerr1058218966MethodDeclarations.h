﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.SmartTerrainTrackerImpl
struct SmartTerrainTrackerImpl_t1058218966;
// Vuforia.SmartTerrainBuilder
struct SmartTerrainBuilder_t3365905913;

#include "codegen/il2cpp-codegen.h"

// System.Single Vuforia.SmartTerrainTrackerImpl::get_ScaleToMillimeter()
extern "C"  float SmartTerrainTrackerImpl_get_ScaleToMillimeter_m143900945 (SmartTerrainTrackerImpl_t1058218966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SmartTerrainTrackerImpl::SetScaleToMillimeter(System.Single)
extern "C"  bool SmartTerrainTrackerImpl_SetScaleToMillimeter_m3537119107 (SmartTerrainTrackerImpl_t1058218966 * __this, float ___scaleFactor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.SmartTerrainBuilder Vuforia.SmartTerrainTrackerImpl::get_SmartTerrainBuilder()
extern "C"  SmartTerrainBuilder_t3365905913 * SmartTerrainTrackerImpl_get_SmartTerrainBuilder_m2439324611 (SmartTerrainTrackerImpl_t1058218966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SmartTerrainTrackerImpl::Start()
extern "C"  bool SmartTerrainTrackerImpl_Start_m3716242139 (SmartTerrainTrackerImpl_t1058218966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerImpl::Stop()
extern "C"  void SmartTerrainTrackerImpl_Stop_m630923063 (SmartTerrainTrackerImpl_t1058218966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerImpl::.ctor()
extern "C"  void SmartTerrainTrackerImpl__ctor_m3418811311 (SmartTerrainTrackerImpl_t1058218966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
