﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWW
struct WWW_t3134621005;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// UnityEngine.Sprite
struct Sprite_t3199167241;
// System.Object
struct Il2CppObject;
// OfertasManagerCRM_Exclusivas
struct OfertasManagerCRM_Exclusivas_t295598117;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OfertasManagerCRM_Exclusivas/<ILoadImgs>c__IteratorA
struct  U3CILoadImgsU3Ec__IteratorA_t2371305409  : public Il2CppObject
{
public:
	// System.Int32 OfertasManagerCRM_Exclusivas/<ILoadImgs>c__IteratorA::<i>__0
	int32_t ___U3CiU3E__0_0;
	// UnityEngine.WWW OfertasManagerCRM_Exclusivas/<ILoadImgs>c__IteratorA::<www>__1
	WWW_t3134621005 * ___U3CwwwU3E__1_1;
	// UnityEngine.Texture2D OfertasManagerCRM_Exclusivas/<ILoadImgs>c__IteratorA::<texture>__2
	Texture2D_t3884108195 * ___U3CtextureU3E__2_2;
	// UnityEngine.Sprite OfertasManagerCRM_Exclusivas/<ILoadImgs>c__IteratorA::<spt>__3
	Sprite_t3199167241 * ___U3CsptU3E__3_3;
	// System.Int32 OfertasManagerCRM_Exclusivas/<ILoadImgs>c__IteratorA::$PC
	int32_t ___U24PC_4;
	// System.Object OfertasManagerCRM_Exclusivas/<ILoadImgs>c__IteratorA::$current
	Il2CppObject * ___U24current_5;
	// OfertasManagerCRM_Exclusivas OfertasManagerCRM_Exclusivas/<ILoadImgs>c__IteratorA::<>f__this
	OfertasManagerCRM_Exclusivas_t295598117 * ___U3CU3Ef__this_6;

public:
	inline static int32_t get_offset_of_U3CiU3E__0_0() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__IteratorA_t2371305409, ___U3CiU3E__0_0)); }
	inline int32_t get_U3CiU3E__0_0() const { return ___U3CiU3E__0_0; }
	inline int32_t* get_address_of_U3CiU3E__0_0() { return &___U3CiU3E__0_0; }
	inline void set_U3CiU3E__0_0(int32_t value)
	{
		___U3CiU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CwwwU3E__1_1() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__IteratorA_t2371305409, ___U3CwwwU3E__1_1)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__1_1() const { return ___U3CwwwU3E__1_1; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__1_1() { return &___U3CwwwU3E__1_1; }
	inline void set_U3CwwwU3E__1_1(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CtextureU3E__2_2() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__IteratorA_t2371305409, ___U3CtextureU3E__2_2)); }
	inline Texture2D_t3884108195 * get_U3CtextureU3E__2_2() const { return ___U3CtextureU3E__2_2; }
	inline Texture2D_t3884108195 ** get_address_of_U3CtextureU3E__2_2() { return &___U3CtextureU3E__2_2; }
	inline void set_U3CtextureU3E__2_2(Texture2D_t3884108195 * value)
	{
		___U3CtextureU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtextureU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CsptU3E__3_3() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__IteratorA_t2371305409, ___U3CsptU3E__3_3)); }
	inline Sprite_t3199167241 * get_U3CsptU3E__3_3() const { return ___U3CsptU3E__3_3; }
	inline Sprite_t3199167241 ** get_address_of_U3CsptU3E__3_3() { return &___U3CsptU3E__3_3; }
	inline void set_U3CsptU3E__3_3(Sprite_t3199167241 * value)
	{
		___U3CsptU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsptU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__IteratorA_t2371305409, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__IteratorA_t2371305409, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_6() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__IteratorA_t2371305409, ___U3CU3Ef__this_6)); }
	inline OfertasManagerCRM_Exclusivas_t295598117 * get_U3CU3Ef__this_6() const { return ___U3CU3Ef__this_6; }
	inline OfertasManagerCRM_Exclusivas_t295598117 ** get_address_of_U3CU3Ef__this_6() { return &___U3CU3Ef__this_6; }
	inline void set_U3CU3Ef__this_6(OfertasManagerCRM_Exclusivas_t295598117 * value)
	{
		___U3CU3Ef__this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
