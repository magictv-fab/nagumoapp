﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen184564025MethodDeclarations.h"

// System.Void System.Func`2<MagicTV.vo.MetadataVO,System.String>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m2566684042(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t3445325228 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3944524044_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<MagicTV.vo.MetadataVO,System.String>::Invoke(T)
#define Func_2_Invoke_m1854614024(__this, ___arg10, method) ((  String_t* (*) (Func_2_t3445325228 *, MetadataVO_t2511256998 *, const MethodInfo*))Func_2_Invoke_m1924616534_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<MagicTV.vo.MetadataVO,System.String>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m576134199(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t3445325228 *, MetadataVO_t2511256998 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m4281703301_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<MagicTV.vo.MetadataVO,System.String>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m167063244(__this, ___result0, method) ((  String_t* (*) (Func_2_t3445325228 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m4118168638_gshared)(__this, ___result0, method)
