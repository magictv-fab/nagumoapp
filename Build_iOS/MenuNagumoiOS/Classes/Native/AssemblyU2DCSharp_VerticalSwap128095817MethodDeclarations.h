﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VerticalSwap
struct VerticalSwap_t128095817;

#include "codegen/il2cpp-codegen.h"

// System.Void VerticalSwap::.ctor()
extern "C"  void VerticalSwap__ctor_m2812239474 (VerticalSwap_t128095817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VerticalSwap::Start()
extern "C"  void VerticalSwap_Start_m1759377266 (VerticalSwap_t128095817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VerticalSwap::Update()
extern "C"  void VerticalSwap_Update_m3006939867 (VerticalSwap_t128095817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VerticalSwap::EndUpdate()
extern "C"  void VerticalSwap_EndUpdate_m24572052 (VerticalSwap_t128095817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
