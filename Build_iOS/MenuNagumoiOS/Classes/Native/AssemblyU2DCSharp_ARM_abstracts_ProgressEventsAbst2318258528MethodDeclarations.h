﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.abstracts.ProgressEventsAbstract/OnCompleteEventHandler
struct OnCompleteEventHandler_t2318258528;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ARM.abstracts.ProgressEventsAbstract/OnCompleteEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnCompleteEventHandler__ctor_m1007019831 (OnCompleteEventHandler_t2318258528 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.ProgressEventsAbstract/OnCompleteEventHandler::Invoke()
extern "C"  void OnCompleteEventHandler_Invoke_m848931409 (OnCompleteEventHandler_t2318258528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ARM.abstracts.ProgressEventsAbstract/OnCompleteEventHandler::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnCompleteEventHandler_BeginInvoke_m1649132058 (OnCompleteEventHandler_t2318258528 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.ProgressEventsAbstract/OnCompleteEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnCompleteEventHandler_EndInvoke_m4091776199 (OnCompleteEventHandler_t2318258528 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
