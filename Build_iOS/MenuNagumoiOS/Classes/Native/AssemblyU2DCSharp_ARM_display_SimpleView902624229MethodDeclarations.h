﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.display.SimpleView
struct SimpleView_t902624229;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Renderer[]
struct RendererU5BU5D_t440051646;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t2697150633;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void ARM.display.SimpleView::.ctor()
extern "C"  void SimpleView__ctor_m1756185350 (SimpleView_t902624229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.display.SimpleView::Show(UnityEngine.GameObject)
extern "C"  void SimpleView_Show_m1842741875 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___presentation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.display.SimpleView::Hide(UnityEngine.GameObject)
extern "C"  void SimpleView_Hide_m4000302520 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___presentation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.display.SimpleView::changeState(UnityEngine.GameObject,UnityEngine.Renderer[],UnityEngine.Collider[],System.Boolean)
extern "C"  void SimpleView_changeState_m2655418001 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___presentation0, RendererU5BU5D_t440051646* ___rendererComponents1, ColliderU5BU5D_t2697150633* ___colliderComponents2, bool ____state3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.display.SimpleView::setChildrenOnOff(System.Boolean,UnityEngine.GameObject)
extern "C"  void SimpleView_setChildrenOnOff_m526411546 (Il2CppObject * __this /* static, unused */, bool ____onOff0, GameObject_t3674682005 * ___presentation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
