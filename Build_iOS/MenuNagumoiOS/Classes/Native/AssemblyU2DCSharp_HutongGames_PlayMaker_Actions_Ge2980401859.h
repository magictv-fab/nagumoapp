﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// PlayMakerAnimatorMoveProxy
struct PlayMakerAnimatorMoveProxy_t4175490694;
// UnityEngine.Animator
struct Animator_t2776330603;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget
struct  GetAnimatorIsMatchingTarget_t2980401859  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// System.Boolean HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::everyFrame
	bool ___everyFrame_10;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::isMatchingActive
	FsmBool_t1075959796 * ___isMatchingActive_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::matchingActivatedEvent
	FsmEvent_t2133468028 * ___matchingActivatedEvent_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::matchingDeactivedEvent
	FsmEvent_t2133468028 * ___matchingDeactivedEvent_13;
	// PlayMakerAnimatorMoveProxy HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::_animatorProxy
	PlayMakerAnimatorMoveProxy_t4175490694 * ____animatorProxy_14;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::_animator
	Animator_t2776330603 * ____animator_15;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(GetAnimatorIsMatchingTarget_t2980401859, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_everyFrame_10() { return static_cast<int32_t>(offsetof(GetAnimatorIsMatchingTarget_t2980401859, ___everyFrame_10)); }
	inline bool get_everyFrame_10() const { return ___everyFrame_10; }
	inline bool* get_address_of_everyFrame_10() { return &___everyFrame_10; }
	inline void set_everyFrame_10(bool value)
	{
		___everyFrame_10 = value;
	}

	inline static int32_t get_offset_of_isMatchingActive_11() { return static_cast<int32_t>(offsetof(GetAnimatorIsMatchingTarget_t2980401859, ___isMatchingActive_11)); }
	inline FsmBool_t1075959796 * get_isMatchingActive_11() const { return ___isMatchingActive_11; }
	inline FsmBool_t1075959796 ** get_address_of_isMatchingActive_11() { return &___isMatchingActive_11; }
	inline void set_isMatchingActive_11(FsmBool_t1075959796 * value)
	{
		___isMatchingActive_11 = value;
		Il2CppCodeGenWriteBarrier(&___isMatchingActive_11, value);
	}

	inline static int32_t get_offset_of_matchingActivatedEvent_12() { return static_cast<int32_t>(offsetof(GetAnimatorIsMatchingTarget_t2980401859, ___matchingActivatedEvent_12)); }
	inline FsmEvent_t2133468028 * get_matchingActivatedEvent_12() const { return ___matchingActivatedEvent_12; }
	inline FsmEvent_t2133468028 ** get_address_of_matchingActivatedEvent_12() { return &___matchingActivatedEvent_12; }
	inline void set_matchingActivatedEvent_12(FsmEvent_t2133468028 * value)
	{
		___matchingActivatedEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___matchingActivatedEvent_12, value);
	}

	inline static int32_t get_offset_of_matchingDeactivedEvent_13() { return static_cast<int32_t>(offsetof(GetAnimatorIsMatchingTarget_t2980401859, ___matchingDeactivedEvent_13)); }
	inline FsmEvent_t2133468028 * get_matchingDeactivedEvent_13() const { return ___matchingDeactivedEvent_13; }
	inline FsmEvent_t2133468028 ** get_address_of_matchingDeactivedEvent_13() { return &___matchingDeactivedEvent_13; }
	inline void set_matchingDeactivedEvent_13(FsmEvent_t2133468028 * value)
	{
		___matchingDeactivedEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___matchingDeactivedEvent_13, value);
	}

	inline static int32_t get_offset_of__animatorProxy_14() { return static_cast<int32_t>(offsetof(GetAnimatorIsMatchingTarget_t2980401859, ____animatorProxy_14)); }
	inline PlayMakerAnimatorMoveProxy_t4175490694 * get__animatorProxy_14() const { return ____animatorProxy_14; }
	inline PlayMakerAnimatorMoveProxy_t4175490694 ** get_address_of__animatorProxy_14() { return &____animatorProxy_14; }
	inline void set__animatorProxy_14(PlayMakerAnimatorMoveProxy_t4175490694 * value)
	{
		____animatorProxy_14 = value;
		Il2CppCodeGenWriteBarrier(&____animatorProxy_14, value);
	}

	inline static int32_t get_offset_of__animator_15() { return static_cast<int32_t>(offsetof(GetAnimatorIsMatchingTarget_t2980401859, ____animator_15)); }
	inline Animator_t2776330603 * get__animator_15() const { return ____animator_15; }
	inline Animator_t2776330603 ** get_address_of__animator_15() { return &____animator_15; }
	inline void set__animator_15(Animator_t2776330603 * value)
	{
		____animator_15 = value;
		Il2CppCodeGenWriteBarrier(&____animator_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
