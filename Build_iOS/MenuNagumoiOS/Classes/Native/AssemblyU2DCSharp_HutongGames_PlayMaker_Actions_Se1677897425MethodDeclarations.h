﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetProceduralBoolean
struct SetProceduralBoolean_t1677897425;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetProceduralBoolean::.ctor()
extern "C"  void SetProceduralBoolean__ctor_m1968145077 (SetProceduralBoolean_t1677897425 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProceduralBoolean::Reset()
extern "C"  void SetProceduralBoolean_Reset_m3909545314 (SetProceduralBoolean_t1677897425 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProceduralBoolean::OnEnter()
extern "C"  void SetProceduralBoolean_OnEnter_m993537356 (SetProceduralBoolean_t1677897425 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProceduralBoolean::OnUpdate()
extern "C"  void SetProceduralBoolean_OnUpdate_m4163413623 (SetProceduralBoolean_t1677897425 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProceduralBoolean::DoSetProceduralFloat()
extern "C"  void SetProceduralBoolean_DoSetProceduralFloat_m532204023 (SetProceduralBoolean_t1677897425 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
