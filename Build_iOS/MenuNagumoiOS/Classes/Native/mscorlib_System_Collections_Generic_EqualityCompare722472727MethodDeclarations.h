﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<InApp.InAppEventDispatcher/EventNames>
struct EqualityComparer_1_t722472727;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.EqualityComparer`1<InApp.InAppEventDispatcher/EventNames>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2067121893_gshared (EqualityComparer_1_t722472727 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m2067121893(__this, method) ((  void (*) (EqualityComparer_1_t722472727 *, const MethodInfo*))EqualityComparer_1__ctor_m2067121893_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<InApp.InAppEventDispatcher/EventNames>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3469140328_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m3469140328(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m3469140328_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<InApp.InAppEventDispatcher/EventNames>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3152812222_gshared (EqualityComparer_1_t722472727 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3152812222(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t722472727 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3152812222_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<InApp.InAppEventDispatcher/EventNames>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m299304972_gshared (EqualityComparer_1_t722472727 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m299304972(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t722472727 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m299304972_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<InApp.InAppEventDispatcher/EventNames>::get_Default()
extern "C"  EqualityComparer_1_t722472727 * EqualityComparer_1_get_Default_m1588286415_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m1588286415(__this /* static, unused */, method) ((  EqualityComparer_1_t722472727 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m1588286415_gshared)(__this /* static, unused */, method)
