﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetMaterialTexture
struct SetMaterialTexture_t65022362;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetMaterialTexture::.ctor()
extern "C"  void SetMaterialTexture__ctor_m3317448716 (SetMaterialTexture_t65022362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMaterialTexture::Reset()
extern "C"  void SetMaterialTexture_Reset_m963881657 (SetMaterialTexture_t65022362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMaterialTexture::OnEnter()
extern "C"  void SetMaterialTexture_OnEnter_m594211043 (SetMaterialTexture_t65022362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMaterialTexture::DoSetMaterialTexture()
extern "C"  void SetMaterialTexture_DoSetMaterialTexture_m1197842325 (SetMaterialTexture_t65022362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
