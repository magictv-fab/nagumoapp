﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.components.ComponentsStarter
struct ComponentsStarter_t1779695339;
// ARM.abstracts.components.GeneralComponentsAbstract
struct GeneralComponentsAbstract_t3900398046;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ARM_abstracts_components_General3900398046.h"

// System.Void ARM.components.ComponentsStarter::.ctor()
extern "C"  void ComponentsStarter__ctor_m721958376 (ComponentsStarter_t1779695339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.components.ComponentsStarter::AddComponent(ARM.abstracts.components.GeneralComponentsAbstract)
extern "C"  void ComponentsStarter_AddComponent_m259132993 (ComponentsStarter_t1779695339 * __this, GeneralComponentsAbstract_t3900398046 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.components.ComponentsStarter::prepare()
extern "C"  void ComponentsStarter_prepare_m3239245101 (ComponentsStarter_t1779695339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.components.ComponentsStarter::checkAllIsComplete()
extern "C"  void ComponentsStarter_checkAllIsComplete_m213791960 (ComponentsStarter_t1779695339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
