﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetMainCamera
struct GetMainCamera_t2582722562;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetMainCamera::.ctor()
extern "C"  void GetMainCamera__ctor_m1352661876 (GetMainCamera_t2582722562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMainCamera::Reset()
extern "C"  void GetMainCamera_Reset_m3294062113 (GetMainCamera_t2582722562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMainCamera::OnEnter()
extern "C"  void GetMainCamera_OnEnter_m2219668043 (GetMainCamera_t2582722562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
