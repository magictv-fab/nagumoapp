﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t4179556250;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.Camera
struct Camera_t2727095145;
// System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>
struct List_1_t639886801;
// System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>
struct List_1_t1083478562;
// System.Action`1<Vuforia.VuforiaUnity/InitError>
struct Action_1_t828034096;
// System.Action
struct Action_t3771233898;
// System.Action`1<System.Boolean>
struct Action_1_t872614854;
// Vuforia.ICameraConfiguration
struct ICameraConfiguration_t2591661610;
// UnityEngine.Material
struct Material_t3870600107;
// System.Collections.Generic.List`1<System.Type>
struct List_1_t4231331326;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevi134001414.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDev3311506418.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRe2868837278.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaAb2257996192.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUni432217960.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaAbstractBehaviour
struct  VuforiaAbstractBehaviour_t1091759131  : public MonoBehaviour_t667441552
{
public:
	// System.String Vuforia.VuforiaAbstractBehaviour::VuforiaLicenseKey
	String_t* ___VuforiaLicenseKey_2;
	// System.Single Vuforia.VuforiaAbstractBehaviour::mCameraOffset
	float ___mCameraOffset_3;
	// System.Boolean Vuforia.VuforiaAbstractBehaviour::mSkewFrustum
	bool ___mSkewFrustum_4;
	// Vuforia.CameraDevice/CameraDeviceMode Vuforia.VuforiaAbstractBehaviour::CameraDeviceModeSetting
	int32_t ___CameraDeviceModeSetting_5;
	// System.Int32 Vuforia.VuforiaAbstractBehaviour::MaxSimultaneousImageTargets
	int32_t ___MaxSimultaneousImageTargets_6;
	// System.Int32 Vuforia.VuforiaAbstractBehaviour::MaxSimultaneousObjectTargets
	int32_t ___MaxSimultaneousObjectTargets_7;
	// System.Boolean Vuforia.VuforiaAbstractBehaviour::UseDelayedLoadingObjectTargets
	bool ___UseDelayedLoadingObjectTargets_8;
	// Vuforia.CameraDevice/CameraDirection Vuforia.VuforiaAbstractBehaviour::CameraDirection
	int32_t ___CameraDirection_9;
	// Vuforia.VuforiaRenderer/VideoBackgroundReflection Vuforia.VuforiaAbstractBehaviour::MirrorVideoBackground
	int32_t ___MirrorVideoBackground_10;
	// Vuforia.VuforiaAbstractBehaviour/WorldCenterMode Vuforia.VuforiaAbstractBehaviour::mWorldCenterMode
	int32_t ___mWorldCenterMode_11;
	// Vuforia.TrackableBehaviour Vuforia.VuforiaAbstractBehaviour::mWorldCenter
	TrackableBehaviour_t4179556250 * ___mWorldCenter_12;
	// UnityEngine.Transform Vuforia.VuforiaAbstractBehaviour::mCentralAnchorPoint
	Transform_t1659122786 * ___mCentralAnchorPoint_13;
	// UnityEngine.Transform Vuforia.VuforiaAbstractBehaviour::mParentAnchorPoint
	Transform_t1659122786 * ___mParentAnchorPoint_14;
	// UnityEngine.Camera Vuforia.VuforiaAbstractBehaviour::mPrimaryCamera
	Camera_t2727095145 * ___mPrimaryCamera_15;
	// UnityEngine.Rect Vuforia.VuforiaAbstractBehaviour::mPrimaryCameraOriginalRect
	Rect_t4241904616  ___mPrimaryCameraOriginalRect_16;
	// UnityEngine.Camera Vuforia.VuforiaAbstractBehaviour::mSecondaryCamera
	Camera_t2727095145 * ___mSecondaryCamera_17;
	// UnityEngine.Rect Vuforia.VuforiaAbstractBehaviour::mSecondaryCameraOriginalRect
	Rect_t4241904616  ___mSecondaryCameraOriginalRect_18;
	// System.Boolean Vuforia.VuforiaAbstractBehaviour::mSecondaryCameraDisabledLocally
	bool ___mSecondaryCameraDisabledLocally_19;
	// System.Boolean Vuforia.VuforiaAbstractBehaviour::mUsingHeadset
	bool ___mUsingHeadset_20;
	// System.String Vuforia.VuforiaAbstractBehaviour::mHeadsetID
	String_t* ___mHeadsetID_21;
	// System.Int32 Vuforia.VuforiaAbstractBehaviour::mEyewearUserCalibrationProfileId
	int32_t ___mEyewearUserCalibrationProfileId_22;
	// System.Boolean Vuforia.VuforiaAbstractBehaviour::mSynchronizePoseUpdates
	bool ___mSynchronizePoseUpdates_23;
	// System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler> Vuforia.VuforiaAbstractBehaviour::mTrackerEventHandlers
	List_1_t639886801 * ___mTrackerEventHandlers_24;
	// System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler> Vuforia.VuforiaAbstractBehaviour::mVideoBgEventHandlers
	List_1_t1083478562 * ___mVideoBgEventHandlers_25;
	// System.Action`1<Vuforia.VuforiaUnity/InitError> Vuforia.VuforiaAbstractBehaviour::mOnVuforiaInitError
	Action_1_t828034096 * ___mOnVuforiaInitError_26;
	// System.Action Vuforia.VuforiaAbstractBehaviour::mOnVuforiaInitialized
	Action_t3771233898 * ___mOnVuforiaInitialized_27;
	// System.Action Vuforia.VuforiaAbstractBehaviour::mOnVuforiaStarted
	Action_t3771233898 * ___mOnVuforiaStarted_28;
	// System.Action Vuforia.VuforiaAbstractBehaviour::mOnTrackablesUpdated
	Action_t3771233898 * ___mOnTrackablesUpdated_29;
	// System.Action Vuforia.VuforiaAbstractBehaviour::mRenderOnUpdate
	Action_t3771233898 * ___mRenderOnUpdate_30;
	// System.Action`1<System.Boolean> Vuforia.VuforiaAbstractBehaviour::mOnPause
	Action_1_t872614854 * ___mOnPause_31;
	// System.Action Vuforia.VuforiaAbstractBehaviour::mOnBackgroundTextureChanged
	Action_t3771233898 * ___mOnBackgroundTextureChanged_32;
	// System.Boolean Vuforia.VuforiaAbstractBehaviour::mStartHasBeenInvoked
	bool ___mStartHasBeenInvoked_33;
	// System.Boolean Vuforia.VuforiaAbstractBehaviour::mHasStarted
	bool ___mHasStarted_34;
	// System.Boolean Vuforia.VuforiaAbstractBehaviour::mFailedToInitialize
	bool ___mFailedToInitialize_35;
	// System.Boolean Vuforia.VuforiaAbstractBehaviour::mBackgroundTextureHasChanged
	bool ___mBackgroundTextureHasChanged_36;
	// Vuforia.VuforiaUnity/InitError Vuforia.VuforiaAbstractBehaviour::mInitError
	int32_t ___mInitError_37;
	// Vuforia.ICameraConfiguration Vuforia.VuforiaAbstractBehaviour::mCameraConfiguration
	Il2CppObject * ___mCameraConfiguration_38;
	// UnityEngine.Material Vuforia.VuforiaAbstractBehaviour::mClearMaterial
	Material_t3870600107 * ___mClearMaterial_39;
	// System.Boolean Vuforia.VuforiaAbstractBehaviour::mHasStartedOnce
	bool ___mHasStartedOnce_40;
	// System.Boolean Vuforia.VuforiaAbstractBehaviour::mWasEnabledBeforePause
	bool ___mWasEnabledBeforePause_41;
	// System.Boolean Vuforia.VuforiaAbstractBehaviour::mObjectTrackerWasActiveBeforePause
	bool ___mObjectTrackerWasActiveBeforePause_42;
	// System.Boolean Vuforia.VuforiaAbstractBehaviour::mObjectTrackerWasActiveBeforeDisabling
	bool ___mObjectTrackerWasActiveBeforeDisabling_43;
	// System.Boolean Vuforia.VuforiaAbstractBehaviour::mMarkerTrackerWasActiveBeforePause
	bool ___mMarkerTrackerWasActiveBeforePause_44;
	// System.Boolean Vuforia.VuforiaAbstractBehaviour::mMarkerTrackerWasActiveBeforeDisabling
	bool ___mMarkerTrackerWasActiveBeforeDisabling_45;
	// System.Int32 Vuforia.VuforiaAbstractBehaviour::mLastUpdatedFrame
	int32_t ___mLastUpdatedFrame_46;
	// System.Collections.Generic.List`1<System.Type> Vuforia.VuforiaAbstractBehaviour::mTrackersRequestedToDeinit
	List_1_t4231331326 * ___mTrackersRequestedToDeinit_47;

public:
	inline static int32_t get_offset_of_VuforiaLicenseKey_2() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___VuforiaLicenseKey_2)); }
	inline String_t* get_VuforiaLicenseKey_2() const { return ___VuforiaLicenseKey_2; }
	inline String_t** get_address_of_VuforiaLicenseKey_2() { return &___VuforiaLicenseKey_2; }
	inline void set_VuforiaLicenseKey_2(String_t* value)
	{
		___VuforiaLicenseKey_2 = value;
		Il2CppCodeGenWriteBarrier(&___VuforiaLicenseKey_2, value);
	}

	inline static int32_t get_offset_of_mCameraOffset_3() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mCameraOffset_3)); }
	inline float get_mCameraOffset_3() const { return ___mCameraOffset_3; }
	inline float* get_address_of_mCameraOffset_3() { return &___mCameraOffset_3; }
	inline void set_mCameraOffset_3(float value)
	{
		___mCameraOffset_3 = value;
	}

	inline static int32_t get_offset_of_mSkewFrustum_4() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mSkewFrustum_4)); }
	inline bool get_mSkewFrustum_4() const { return ___mSkewFrustum_4; }
	inline bool* get_address_of_mSkewFrustum_4() { return &___mSkewFrustum_4; }
	inline void set_mSkewFrustum_4(bool value)
	{
		___mSkewFrustum_4 = value;
	}

	inline static int32_t get_offset_of_CameraDeviceModeSetting_5() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___CameraDeviceModeSetting_5)); }
	inline int32_t get_CameraDeviceModeSetting_5() const { return ___CameraDeviceModeSetting_5; }
	inline int32_t* get_address_of_CameraDeviceModeSetting_5() { return &___CameraDeviceModeSetting_5; }
	inline void set_CameraDeviceModeSetting_5(int32_t value)
	{
		___CameraDeviceModeSetting_5 = value;
	}

	inline static int32_t get_offset_of_MaxSimultaneousImageTargets_6() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___MaxSimultaneousImageTargets_6)); }
	inline int32_t get_MaxSimultaneousImageTargets_6() const { return ___MaxSimultaneousImageTargets_6; }
	inline int32_t* get_address_of_MaxSimultaneousImageTargets_6() { return &___MaxSimultaneousImageTargets_6; }
	inline void set_MaxSimultaneousImageTargets_6(int32_t value)
	{
		___MaxSimultaneousImageTargets_6 = value;
	}

	inline static int32_t get_offset_of_MaxSimultaneousObjectTargets_7() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___MaxSimultaneousObjectTargets_7)); }
	inline int32_t get_MaxSimultaneousObjectTargets_7() const { return ___MaxSimultaneousObjectTargets_7; }
	inline int32_t* get_address_of_MaxSimultaneousObjectTargets_7() { return &___MaxSimultaneousObjectTargets_7; }
	inline void set_MaxSimultaneousObjectTargets_7(int32_t value)
	{
		___MaxSimultaneousObjectTargets_7 = value;
	}

	inline static int32_t get_offset_of_UseDelayedLoadingObjectTargets_8() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___UseDelayedLoadingObjectTargets_8)); }
	inline bool get_UseDelayedLoadingObjectTargets_8() const { return ___UseDelayedLoadingObjectTargets_8; }
	inline bool* get_address_of_UseDelayedLoadingObjectTargets_8() { return &___UseDelayedLoadingObjectTargets_8; }
	inline void set_UseDelayedLoadingObjectTargets_8(bool value)
	{
		___UseDelayedLoadingObjectTargets_8 = value;
	}

	inline static int32_t get_offset_of_CameraDirection_9() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___CameraDirection_9)); }
	inline int32_t get_CameraDirection_9() const { return ___CameraDirection_9; }
	inline int32_t* get_address_of_CameraDirection_9() { return &___CameraDirection_9; }
	inline void set_CameraDirection_9(int32_t value)
	{
		___CameraDirection_9 = value;
	}

	inline static int32_t get_offset_of_MirrorVideoBackground_10() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___MirrorVideoBackground_10)); }
	inline int32_t get_MirrorVideoBackground_10() const { return ___MirrorVideoBackground_10; }
	inline int32_t* get_address_of_MirrorVideoBackground_10() { return &___MirrorVideoBackground_10; }
	inline void set_MirrorVideoBackground_10(int32_t value)
	{
		___MirrorVideoBackground_10 = value;
	}

	inline static int32_t get_offset_of_mWorldCenterMode_11() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mWorldCenterMode_11)); }
	inline int32_t get_mWorldCenterMode_11() const { return ___mWorldCenterMode_11; }
	inline int32_t* get_address_of_mWorldCenterMode_11() { return &___mWorldCenterMode_11; }
	inline void set_mWorldCenterMode_11(int32_t value)
	{
		___mWorldCenterMode_11 = value;
	}

	inline static int32_t get_offset_of_mWorldCenter_12() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mWorldCenter_12)); }
	inline TrackableBehaviour_t4179556250 * get_mWorldCenter_12() const { return ___mWorldCenter_12; }
	inline TrackableBehaviour_t4179556250 ** get_address_of_mWorldCenter_12() { return &___mWorldCenter_12; }
	inline void set_mWorldCenter_12(TrackableBehaviour_t4179556250 * value)
	{
		___mWorldCenter_12 = value;
		Il2CppCodeGenWriteBarrier(&___mWorldCenter_12, value);
	}

	inline static int32_t get_offset_of_mCentralAnchorPoint_13() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mCentralAnchorPoint_13)); }
	inline Transform_t1659122786 * get_mCentralAnchorPoint_13() const { return ___mCentralAnchorPoint_13; }
	inline Transform_t1659122786 ** get_address_of_mCentralAnchorPoint_13() { return &___mCentralAnchorPoint_13; }
	inline void set_mCentralAnchorPoint_13(Transform_t1659122786 * value)
	{
		___mCentralAnchorPoint_13 = value;
		Il2CppCodeGenWriteBarrier(&___mCentralAnchorPoint_13, value);
	}

	inline static int32_t get_offset_of_mParentAnchorPoint_14() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mParentAnchorPoint_14)); }
	inline Transform_t1659122786 * get_mParentAnchorPoint_14() const { return ___mParentAnchorPoint_14; }
	inline Transform_t1659122786 ** get_address_of_mParentAnchorPoint_14() { return &___mParentAnchorPoint_14; }
	inline void set_mParentAnchorPoint_14(Transform_t1659122786 * value)
	{
		___mParentAnchorPoint_14 = value;
		Il2CppCodeGenWriteBarrier(&___mParentAnchorPoint_14, value);
	}

	inline static int32_t get_offset_of_mPrimaryCamera_15() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mPrimaryCamera_15)); }
	inline Camera_t2727095145 * get_mPrimaryCamera_15() const { return ___mPrimaryCamera_15; }
	inline Camera_t2727095145 ** get_address_of_mPrimaryCamera_15() { return &___mPrimaryCamera_15; }
	inline void set_mPrimaryCamera_15(Camera_t2727095145 * value)
	{
		___mPrimaryCamera_15 = value;
		Il2CppCodeGenWriteBarrier(&___mPrimaryCamera_15, value);
	}

	inline static int32_t get_offset_of_mPrimaryCameraOriginalRect_16() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mPrimaryCameraOriginalRect_16)); }
	inline Rect_t4241904616  get_mPrimaryCameraOriginalRect_16() const { return ___mPrimaryCameraOriginalRect_16; }
	inline Rect_t4241904616 * get_address_of_mPrimaryCameraOriginalRect_16() { return &___mPrimaryCameraOriginalRect_16; }
	inline void set_mPrimaryCameraOriginalRect_16(Rect_t4241904616  value)
	{
		___mPrimaryCameraOriginalRect_16 = value;
	}

	inline static int32_t get_offset_of_mSecondaryCamera_17() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mSecondaryCamera_17)); }
	inline Camera_t2727095145 * get_mSecondaryCamera_17() const { return ___mSecondaryCamera_17; }
	inline Camera_t2727095145 ** get_address_of_mSecondaryCamera_17() { return &___mSecondaryCamera_17; }
	inline void set_mSecondaryCamera_17(Camera_t2727095145 * value)
	{
		___mSecondaryCamera_17 = value;
		Il2CppCodeGenWriteBarrier(&___mSecondaryCamera_17, value);
	}

	inline static int32_t get_offset_of_mSecondaryCameraOriginalRect_18() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mSecondaryCameraOriginalRect_18)); }
	inline Rect_t4241904616  get_mSecondaryCameraOriginalRect_18() const { return ___mSecondaryCameraOriginalRect_18; }
	inline Rect_t4241904616 * get_address_of_mSecondaryCameraOriginalRect_18() { return &___mSecondaryCameraOriginalRect_18; }
	inline void set_mSecondaryCameraOriginalRect_18(Rect_t4241904616  value)
	{
		___mSecondaryCameraOriginalRect_18 = value;
	}

	inline static int32_t get_offset_of_mSecondaryCameraDisabledLocally_19() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mSecondaryCameraDisabledLocally_19)); }
	inline bool get_mSecondaryCameraDisabledLocally_19() const { return ___mSecondaryCameraDisabledLocally_19; }
	inline bool* get_address_of_mSecondaryCameraDisabledLocally_19() { return &___mSecondaryCameraDisabledLocally_19; }
	inline void set_mSecondaryCameraDisabledLocally_19(bool value)
	{
		___mSecondaryCameraDisabledLocally_19 = value;
	}

	inline static int32_t get_offset_of_mUsingHeadset_20() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mUsingHeadset_20)); }
	inline bool get_mUsingHeadset_20() const { return ___mUsingHeadset_20; }
	inline bool* get_address_of_mUsingHeadset_20() { return &___mUsingHeadset_20; }
	inline void set_mUsingHeadset_20(bool value)
	{
		___mUsingHeadset_20 = value;
	}

	inline static int32_t get_offset_of_mHeadsetID_21() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mHeadsetID_21)); }
	inline String_t* get_mHeadsetID_21() const { return ___mHeadsetID_21; }
	inline String_t** get_address_of_mHeadsetID_21() { return &___mHeadsetID_21; }
	inline void set_mHeadsetID_21(String_t* value)
	{
		___mHeadsetID_21 = value;
		Il2CppCodeGenWriteBarrier(&___mHeadsetID_21, value);
	}

	inline static int32_t get_offset_of_mEyewearUserCalibrationProfileId_22() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mEyewearUserCalibrationProfileId_22)); }
	inline int32_t get_mEyewearUserCalibrationProfileId_22() const { return ___mEyewearUserCalibrationProfileId_22; }
	inline int32_t* get_address_of_mEyewearUserCalibrationProfileId_22() { return &___mEyewearUserCalibrationProfileId_22; }
	inline void set_mEyewearUserCalibrationProfileId_22(int32_t value)
	{
		___mEyewearUserCalibrationProfileId_22 = value;
	}

	inline static int32_t get_offset_of_mSynchronizePoseUpdates_23() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mSynchronizePoseUpdates_23)); }
	inline bool get_mSynchronizePoseUpdates_23() const { return ___mSynchronizePoseUpdates_23; }
	inline bool* get_address_of_mSynchronizePoseUpdates_23() { return &___mSynchronizePoseUpdates_23; }
	inline void set_mSynchronizePoseUpdates_23(bool value)
	{
		___mSynchronizePoseUpdates_23 = value;
	}

	inline static int32_t get_offset_of_mTrackerEventHandlers_24() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mTrackerEventHandlers_24)); }
	inline List_1_t639886801 * get_mTrackerEventHandlers_24() const { return ___mTrackerEventHandlers_24; }
	inline List_1_t639886801 ** get_address_of_mTrackerEventHandlers_24() { return &___mTrackerEventHandlers_24; }
	inline void set_mTrackerEventHandlers_24(List_1_t639886801 * value)
	{
		___mTrackerEventHandlers_24 = value;
		Il2CppCodeGenWriteBarrier(&___mTrackerEventHandlers_24, value);
	}

	inline static int32_t get_offset_of_mVideoBgEventHandlers_25() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mVideoBgEventHandlers_25)); }
	inline List_1_t1083478562 * get_mVideoBgEventHandlers_25() const { return ___mVideoBgEventHandlers_25; }
	inline List_1_t1083478562 ** get_address_of_mVideoBgEventHandlers_25() { return &___mVideoBgEventHandlers_25; }
	inline void set_mVideoBgEventHandlers_25(List_1_t1083478562 * value)
	{
		___mVideoBgEventHandlers_25 = value;
		Il2CppCodeGenWriteBarrier(&___mVideoBgEventHandlers_25, value);
	}

	inline static int32_t get_offset_of_mOnVuforiaInitError_26() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mOnVuforiaInitError_26)); }
	inline Action_1_t828034096 * get_mOnVuforiaInitError_26() const { return ___mOnVuforiaInitError_26; }
	inline Action_1_t828034096 ** get_address_of_mOnVuforiaInitError_26() { return &___mOnVuforiaInitError_26; }
	inline void set_mOnVuforiaInitError_26(Action_1_t828034096 * value)
	{
		___mOnVuforiaInitError_26 = value;
		Il2CppCodeGenWriteBarrier(&___mOnVuforiaInitError_26, value);
	}

	inline static int32_t get_offset_of_mOnVuforiaInitialized_27() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mOnVuforiaInitialized_27)); }
	inline Action_t3771233898 * get_mOnVuforiaInitialized_27() const { return ___mOnVuforiaInitialized_27; }
	inline Action_t3771233898 ** get_address_of_mOnVuforiaInitialized_27() { return &___mOnVuforiaInitialized_27; }
	inline void set_mOnVuforiaInitialized_27(Action_t3771233898 * value)
	{
		___mOnVuforiaInitialized_27 = value;
		Il2CppCodeGenWriteBarrier(&___mOnVuforiaInitialized_27, value);
	}

	inline static int32_t get_offset_of_mOnVuforiaStarted_28() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mOnVuforiaStarted_28)); }
	inline Action_t3771233898 * get_mOnVuforiaStarted_28() const { return ___mOnVuforiaStarted_28; }
	inline Action_t3771233898 ** get_address_of_mOnVuforiaStarted_28() { return &___mOnVuforiaStarted_28; }
	inline void set_mOnVuforiaStarted_28(Action_t3771233898 * value)
	{
		___mOnVuforiaStarted_28 = value;
		Il2CppCodeGenWriteBarrier(&___mOnVuforiaStarted_28, value);
	}

	inline static int32_t get_offset_of_mOnTrackablesUpdated_29() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mOnTrackablesUpdated_29)); }
	inline Action_t3771233898 * get_mOnTrackablesUpdated_29() const { return ___mOnTrackablesUpdated_29; }
	inline Action_t3771233898 ** get_address_of_mOnTrackablesUpdated_29() { return &___mOnTrackablesUpdated_29; }
	inline void set_mOnTrackablesUpdated_29(Action_t3771233898 * value)
	{
		___mOnTrackablesUpdated_29 = value;
		Il2CppCodeGenWriteBarrier(&___mOnTrackablesUpdated_29, value);
	}

	inline static int32_t get_offset_of_mRenderOnUpdate_30() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mRenderOnUpdate_30)); }
	inline Action_t3771233898 * get_mRenderOnUpdate_30() const { return ___mRenderOnUpdate_30; }
	inline Action_t3771233898 ** get_address_of_mRenderOnUpdate_30() { return &___mRenderOnUpdate_30; }
	inline void set_mRenderOnUpdate_30(Action_t3771233898 * value)
	{
		___mRenderOnUpdate_30 = value;
		Il2CppCodeGenWriteBarrier(&___mRenderOnUpdate_30, value);
	}

	inline static int32_t get_offset_of_mOnPause_31() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mOnPause_31)); }
	inline Action_1_t872614854 * get_mOnPause_31() const { return ___mOnPause_31; }
	inline Action_1_t872614854 ** get_address_of_mOnPause_31() { return &___mOnPause_31; }
	inline void set_mOnPause_31(Action_1_t872614854 * value)
	{
		___mOnPause_31 = value;
		Il2CppCodeGenWriteBarrier(&___mOnPause_31, value);
	}

	inline static int32_t get_offset_of_mOnBackgroundTextureChanged_32() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mOnBackgroundTextureChanged_32)); }
	inline Action_t3771233898 * get_mOnBackgroundTextureChanged_32() const { return ___mOnBackgroundTextureChanged_32; }
	inline Action_t3771233898 ** get_address_of_mOnBackgroundTextureChanged_32() { return &___mOnBackgroundTextureChanged_32; }
	inline void set_mOnBackgroundTextureChanged_32(Action_t3771233898 * value)
	{
		___mOnBackgroundTextureChanged_32 = value;
		Il2CppCodeGenWriteBarrier(&___mOnBackgroundTextureChanged_32, value);
	}

	inline static int32_t get_offset_of_mStartHasBeenInvoked_33() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mStartHasBeenInvoked_33)); }
	inline bool get_mStartHasBeenInvoked_33() const { return ___mStartHasBeenInvoked_33; }
	inline bool* get_address_of_mStartHasBeenInvoked_33() { return &___mStartHasBeenInvoked_33; }
	inline void set_mStartHasBeenInvoked_33(bool value)
	{
		___mStartHasBeenInvoked_33 = value;
	}

	inline static int32_t get_offset_of_mHasStarted_34() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mHasStarted_34)); }
	inline bool get_mHasStarted_34() const { return ___mHasStarted_34; }
	inline bool* get_address_of_mHasStarted_34() { return &___mHasStarted_34; }
	inline void set_mHasStarted_34(bool value)
	{
		___mHasStarted_34 = value;
	}

	inline static int32_t get_offset_of_mFailedToInitialize_35() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mFailedToInitialize_35)); }
	inline bool get_mFailedToInitialize_35() const { return ___mFailedToInitialize_35; }
	inline bool* get_address_of_mFailedToInitialize_35() { return &___mFailedToInitialize_35; }
	inline void set_mFailedToInitialize_35(bool value)
	{
		___mFailedToInitialize_35 = value;
	}

	inline static int32_t get_offset_of_mBackgroundTextureHasChanged_36() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mBackgroundTextureHasChanged_36)); }
	inline bool get_mBackgroundTextureHasChanged_36() const { return ___mBackgroundTextureHasChanged_36; }
	inline bool* get_address_of_mBackgroundTextureHasChanged_36() { return &___mBackgroundTextureHasChanged_36; }
	inline void set_mBackgroundTextureHasChanged_36(bool value)
	{
		___mBackgroundTextureHasChanged_36 = value;
	}

	inline static int32_t get_offset_of_mInitError_37() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mInitError_37)); }
	inline int32_t get_mInitError_37() const { return ___mInitError_37; }
	inline int32_t* get_address_of_mInitError_37() { return &___mInitError_37; }
	inline void set_mInitError_37(int32_t value)
	{
		___mInitError_37 = value;
	}

	inline static int32_t get_offset_of_mCameraConfiguration_38() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mCameraConfiguration_38)); }
	inline Il2CppObject * get_mCameraConfiguration_38() const { return ___mCameraConfiguration_38; }
	inline Il2CppObject ** get_address_of_mCameraConfiguration_38() { return &___mCameraConfiguration_38; }
	inline void set_mCameraConfiguration_38(Il2CppObject * value)
	{
		___mCameraConfiguration_38 = value;
		Il2CppCodeGenWriteBarrier(&___mCameraConfiguration_38, value);
	}

	inline static int32_t get_offset_of_mClearMaterial_39() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mClearMaterial_39)); }
	inline Material_t3870600107 * get_mClearMaterial_39() const { return ___mClearMaterial_39; }
	inline Material_t3870600107 ** get_address_of_mClearMaterial_39() { return &___mClearMaterial_39; }
	inline void set_mClearMaterial_39(Material_t3870600107 * value)
	{
		___mClearMaterial_39 = value;
		Il2CppCodeGenWriteBarrier(&___mClearMaterial_39, value);
	}

	inline static int32_t get_offset_of_mHasStartedOnce_40() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mHasStartedOnce_40)); }
	inline bool get_mHasStartedOnce_40() const { return ___mHasStartedOnce_40; }
	inline bool* get_address_of_mHasStartedOnce_40() { return &___mHasStartedOnce_40; }
	inline void set_mHasStartedOnce_40(bool value)
	{
		___mHasStartedOnce_40 = value;
	}

	inline static int32_t get_offset_of_mWasEnabledBeforePause_41() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mWasEnabledBeforePause_41)); }
	inline bool get_mWasEnabledBeforePause_41() const { return ___mWasEnabledBeforePause_41; }
	inline bool* get_address_of_mWasEnabledBeforePause_41() { return &___mWasEnabledBeforePause_41; }
	inline void set_mWasEnabledBeforePause_41(bool value)
	{
		___mWasEnabledBeforePause_41 = value;
	}

	inline static int32_t get_offset_of_mObjectTrackerWasActiveBeforePause_42() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mObjectTrackerWasActiveBeforePause_42)); }
	inline bool get_mObjectTrackerWasActiveBeforePause_42() const { return ___mObjectTrackerWasActiveBeforePause_42; }
	inline bool* get_address_of_mObjectTrackerWasActiveBeforePause_42() { return &___mObjectTrackerWasActiveBeforePause_42; }
	inline void set_mObjectTrackerWasActiveBeforePause_42(bool value)
	{
		___mObjectTrackerWasActiveBeforePause_42 = value;
	}

	inline static int32_t get_offset_of_mObjectTrackerWasActiveBeforeDisabling_43() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mObjectTrackerWasActiveBeforeDisabling_43)); }
	inline bool get_mObjectTrackerWasActiveBeforeDisabling_43() const { return ___mObjectTrackerWasActiveBeforeDisabling_43; }
	inline bool* get_address_of_mObjectTrackerWasActiveBeforeDisabling_43() { return &___mObjectTrackerWasActiveBeforeDisabling_43; }
	inline void set_mObjectTrackerWasActiveBeforeDisabling_43(bool value)
	{
		___mObjectTrackerWasActiveBeforeDisabling_43 = value;
	}

	inline static int32_t get_offset_of_mMarkerTrackerWasActiveBeforePause_44() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mMarkerTrackerWasActiveBeforePause_44)); }
	inline bool get_mMarkerTrackerWasActiveBeforePause_44() const { return ___mMarkerTrackerWasActiveBeforePause_44; }
	inline bool* get_address_of_mMarkerTrackerWasActiveBeforePause_44() { return &___mMarkerTrackerWasActiveBeforePause_44; }
	inline void set_mMarkerTrackerWasActiveBeforePause_44(bool value)
	{
		___mMarkerTrackerWasActiveBeforePause_44 = value;
	}

	inline static int32_t get_offset_of_mMarkerTrackerWasActiveBeforeDisabling_45() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mMarkerTrackerWasActiveBeforeDisabling_45)); }
	inline bool get_mMarkerTrackerWasActiveBeforeDisabling_45() const { return ___mMarkerTrackerWasActiveBeforeDisabling_45; }
	inline bool* get_address_of_mMarkerTrackerWasActiveBeforeDisabling_45() { return &___mMarkerTrackerWasActiveBeforeDisabling_45; }
	inline void set_mMarkerTrackerWasActiveBeforeDisabling_45(bool value)
	{
		___mMarkerTrackerWasActiveBeforeDisabling_45 = value;
	}

	inline static int32_t get_offset_of_mLastUpdatedFrame_46() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mLastUpdatedFrame_46)); }
	inline int32_t get_mLastUpdatedFrame_46() const { return ___mLastUpdatedFrame_46; }
	inline int32_t* get_address_of_mLastUpdatedFrame_46() { return &___mLastUpdatedFrame_46; }
	inline void set_mLastUpdatedFrame_46(int32_t value)
	{
		___mLastUpdatedFrame_46 = value;
	}

	inline static int32_t get_offset_of_mTrackersRequestedToDeinit_47() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t1091759131, ___mTrackersRequestedToDeinit_47)); }
	inline List_1_t4231331326 * get_mTrackersRequestedToDeinit_47() const { return ___mTrackersRequestedToDeinit_47; }
	inline List_1_t4231331326 ** get_address_of_mTrackersRequestedToDeinit_47() { return &___mTrackersRequestedToDeinit_47; }
	inline void set_mTrackersRequestedToDeinit_47(List_1_t4231331326 * value)
	{
		___mTrackersRequestedToDeinit_47 = value;
		Il2CppCodeGenWriteBarrier(&___mTrackersRequestedToDeinit_47, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
