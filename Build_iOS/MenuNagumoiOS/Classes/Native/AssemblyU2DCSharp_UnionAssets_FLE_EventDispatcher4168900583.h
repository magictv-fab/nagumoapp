﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<UnionAssets.FLE.EventHandlerFunction>>
struct Dictionary_2_t4037634331;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>>
struct Dictionary_2_t1804416613;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnionAssets.FLE.EventDispatcher
struct  EventDispatcher_t4168900583  : public MonoBehaviour_t667441552
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<UnionAssets.FLE.EventHandlerFunction>> UnionAssets.FLE.EventDispatcher::listners
	Dictionary_2_t4037634331 * ___listners_2;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>> UnionAssets.FLE.EventDispatcher::dataListners
	Dictionary_2_t1804416613 * ___dataListners_3;

public:
	inline static int32_t get_offset_of_listners_2() { return static_cast<int32_t>(offsetof(EventDispatcher_t4168900583, ___listners_2)); }
	inline Dictionary_2_t4037634331 * get_listners_2() const { return ___listners_2; }
	inline Dictionary_2_t4037634331 ** get_address_of_listners_2() { return &___listners_2; }
	inline void set_listners_2(Dictionary_2_t4037634331 * value)
	{
		___listners_2 = value;
		Il2CppCodeGenWriteBarrier(&___listners_2, value);
	}

	inline static int32_t get_offset_of_dataListners_3() { return static_cast<int32_t>(offsetof(EventDispatcher_t4168900583, ___dataListners_3)); }
	inline Dictionary_2_t1804416613 * get_dataListners_3() const { return ___dataListners_3; }
	inline Dictionary_2_t1804416613 ** get_address_of_dataListners_3() { return &___dataListners_3; }
	inline void set_dataListners_3(Dictionary_2_t1804416613 * value)
	{
		___dataListners_3 = value;
		Il2CppCodeGenWriteBarrier(&___dataListners_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
