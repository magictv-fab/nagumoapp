﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmTransition
struct FsmTransition_t3771611999;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTransition3771611999.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent2133468028.h"
#include "mscorlib_System_String7231557.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTransition_Cust2484246752.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTransition_Cust4157095950.h"

// System.Void HutongGames.PlayMaker.FsmTransition::.ctor()
extern "C"  void FsmTransition__ctor_m1691138272 (FsmTransition_t3771611999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTransition::.ctor(HutongGames.PlayMaker.FsmTransition)
extern "C"  void FsmTransition__ctor_m1338343305 (FsmTransition_t3771611999 * __this, FsmTransition_t3771611999 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmTransition::get_FsmEvent()
extern "C"  FsmEvent_t2133468028 * FsmTransition_get_FsmEvent_m3103873932 (FsmTransition_t3771611999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTransition::set_FsmEvent(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmTransition_set_FsmEvent_m3006179221 (FsmTransition_t3771611999 * __this, FsmEvent_t2133468028 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmTransition::get_ToState()
extern "C"  String_t* FsmTransition_get_ToState_m548819918 (FsmTransition_t3771611999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTransition::set_ToState(System.String)
extern "C"  void FsmTransition_set_ToState_m3569044075 (FsmTransition_t3771611999 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmTransition/CustomLinkStyle HutongGames.PlayMaker.FsmTransition::get_LinkStyle()
extern "C"  uint8_t FsmTransition_get_LinkStyle_m292862943 (FsmTransition_t3771611999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTransition::set_LinkStyle(HutongGames.PlayMaker.FsmTransition/CustomLinkStyle)
extern "C"  void FsmTransition_set_LinkStyle_m580720218 (FsmTransition_t3771611999 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmTransition/CustomLinkConstraint HutongGames.PlayMaker.FsmTransition::get_LinkConstraint()
extern "C"  uint8_t FsmTransition_get_LinkConstraint_m3705629393 (FsmTransition_t3771611999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTransition::set_LinkConstraint(HutongGames.PlayMaker.FsmTransition/CustomLinkConstraint)
extern "C"  void FsmTransition_set_LinkConstraint_m3373993418 (FsmTransition_t3771611999 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.FsmTransition::get_ColorIndex()
extern "C"  int32_t FsmTransition_get_ColorIndex_m1930422218 (FsmTransition_t3771611999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTransition::set_ColorIndex(System.Int32)
extern "C"  void FsmTransition_set_ColorIndex_m786514465 (FsmTransition_t3771611999 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmTransition::get_EventName()
extern "C"  String_t* FsmTransition_get_EventName_m1790647229 (FsmTransition_t3771611999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmTransition::Equals(HutongGames.PlayMaker.FsmTransition)
extern "C"  bool FsmTransition_Equals_m4204952542 (FsmTransition_t3771611999 * __this, FsmTransition_t3771611999 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
