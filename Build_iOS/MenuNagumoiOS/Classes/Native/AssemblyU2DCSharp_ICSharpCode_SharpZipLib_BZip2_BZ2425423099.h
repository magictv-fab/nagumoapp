﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream/StackElement
struct  StackElement_t2425423099 
{
public:
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream/StackElement::ll
	int32_t ___ll_0;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream/StackElement::hh
	int32_t ___hh_1;
	// System.Int32 ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream/StackElement::dd
	int32_t ___dd_2;

public:
	inline static int32_t get_offset_of_ll_0() { return static_cast<int32_t>(offsetof(StackElement_t2425423099, ___ll_0)); }
	inline int32_t get_ll_0() const { return ___ll_0; }
	inline int32_t* get_address_of_ll_0() { return &___ll_0; }
	inline void set_ll_0(int32_t value)
	{
		___ll_0 = value;
	}

	inline static int32_t get_offset_of_hh_1() { return static_cast<int32_t>(offsetof(StackElement_t2425423099, ___hh_1)); }
	inline int32_t get_hh_1() const { return ___hh_1; }
	inline int32_t* get_address_of_hh_1() { return &___hh_1; }
	inline void set_hh_1(int32_t value)
	{
		___hh_1 = value;
	}

	inline static int32_t get_offset_of_dd_2() { return static_cast<int32_t>(offsetof(StackElement_t2425423099, ___dd_2)); }
	inline int32_t get_dd_2() const { return ___dd_2; }
	inline int32_t* get_address_of_dd_2() { return &___dd_2; }
	inline void set_dd_2(int32_t value)
	{
		___dd_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream/StackElement
struct StackElement_t2425423099_marshaled_pinvoke
{
	int32_t ___ll_0;
	int32_t ___hh_1;
	int32_t ___dd_2;
};
// Native definition for marshalling of: ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream/StackElement
struct StackElement_t2425423099_marshaled_com
{
	int32_t ___ll_0;
	int32_t ___hh_1;
	int32_t ___dd_2;
};
