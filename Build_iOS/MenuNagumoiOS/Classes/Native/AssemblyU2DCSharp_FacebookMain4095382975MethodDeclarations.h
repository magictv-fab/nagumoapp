﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FacebookMain
struct FacebookMain_t4095382975;
// Facebook.Unity.IResult
struct IResult_t2984644868;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// Facebook.Unity.IGraphResult
struct IGraphResult_t873401058;
// Facebook.Unity.IShareResult
struct IShareResult_t3573065907;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"

// System.Void FacebookMain::.ctor()
extern "C"  void FacebookMain__ctor_m2458936252 (FacebookMain_t4095382975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookMain::Start()
extern "C"  void FacebookMain_Start_m1406074044 (FacebookMain_t4095382975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookMain::Update()
extern "C"  void FacebookMain_Update_m644474577 (FacebookMain_t4095382975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookMain::FacebookAccount()
extern "C"  void FacebookMain_FacebookAccount_m2148303969 (FacebookMain_t4095382975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookMain::CallFBLogin()
extern "C"  void FacebookMain_CallFBLogin_m1438928457 (FacebookMain_t4095382975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookMain::CallFBLoginForPublish()
extern "C"  void FacebookMain_CallFBLoginForPublish_m1211638511 (FacebookMain_t4095382975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookMain::CallFBLogout()
extern "C"  void FacebookMain_CallFBLogout_m1662931916 (FacebookMain_t4095382975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookMain::OnInitComplete()
extern "C"  void FacebookMain_OnInitComplete_m1393041712 (FacebookMain_t4095382975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookMain::OnInitCompleteSharer()
extern "C"  void FacebookMain_OnInitCompleteSharer_m2840445379 (FacebookMain_t4095382975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookMain::OnInitCompleteSharerLink()
extern "C"  void FacebookMain_OnInitCompleteSharerLink_m644421213 (FacebookMain_t4095382975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookMain::GetInfo()
extern "C"  void FacebookMain_GetInfo_m964971198 (FacebookMain_t4095382975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookMain::OnHideUnity(System.Boolean)
extern "C"  void FacebookMain_OnHideUnity_m3437206085 (FacebookMain_t4095382975 * __this, bool ___isGameShown0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookMain::HandleResult(Facebook.Unity.IResult)
extern "C"  void FacebookMain_HandleResult_m2712041640 (FacebookMain_t4095382975 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookMain::SharerLink(System.String,System.String,System.String,System.String)
extern "C"  void FacebookMain_SharerLink_m290739713 (FacebookMain_t4095382975 * __this, String_t* ___linkOferta0, String_t* ___linkImg1, String_t* ___title2, String_t* ___info3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator FacebookMain::ISendJsonEnviaSeCompartilhou()
extern "C"  Il2CppObject * FacebookMain_ISendJsonEnviaSeCompartilhou_m945004197 (FacebookMain_t4095382975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookMain::Sharer(UnityEngine.Texture2D)
extern "C"  void FacebookMain_Sharer_m1160160513 (FacebookMain_t4095382975 * __this, Texture2D_t3884108195 * ___texture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookMain::HandleResultSharer(Facebook.Unity.IResult)
extern "C"  void FacebookMain_HandleResultSharer_m1184986939 (FacebookMain_t4095382975 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookMain::HandleResultLoginSharer(Facebook.Unity.IResult)
extern "C"  void FacebookMain_HandleResultLoginSharer_m370234860 (FacebookMain_t4095382975 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookMain::HandleResultLoginSharerLink(Facebook.Unity.IResult)
extern "C"  void FacebookMain_HandleResultLoginSharerLink_m1821840390 (FacebookMain_t4095382975 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookMain::<GetInfo>m__84(Facebook.Unity.IGraphResult)
extern "C"  void FacebookMain_U3CGetInfoU3Em__84_m127863156 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookMain::<SharerLink>m__85(Facebook.Unity.IShareResult)
extern "C"  void FacebookMain_U3CSharerLinkU3Em__85_m4064336143 (FacebookMain_t4095382975 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
