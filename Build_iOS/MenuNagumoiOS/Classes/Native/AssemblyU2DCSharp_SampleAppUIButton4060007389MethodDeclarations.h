﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SampleAppUIButton
struct SampleAppUIButton_t4060007389;
// UnityEngine.GUIStyle
struct GUIStyle_t2990928826;
// UnityEngine.Texture
struct Texture_t2526458961;
// System.String
struct String_t;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_GUIStyle2990928826.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "mscorlib_System_String7231557.h"
#include "System_Core_System_Action3771233898.h"

// System.Void SampleAppUIButton::.ctor(UnityEngine.Rect,UnityEngine.GUIStyle,UnityEngine.Texture)
extern "C"  void SampleAppUIButton__ctor_m1992315470 (SampleAppUIButton_t4060007389 * __this, Rect_t4241904616  ___rect0, GUIStyle_t2990928826 * ___style1, Texture_t2526458961 * ___buttonTexture2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SampleAppUIButton::.ctor(UnityEngine.Rect,UnityEngine.GUIStyle,System.String)
extern "C"  void SampleAppUIButton__ctor_m2205105634 (SampleAppUIButton_t4060007389 * __this, Rect_t4241904616  ___rect0, GUIStyle_t2990928826 * ___style1, String_t* ___title2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SampleAppUIButton::add_TappedOn(System.Action)
extern "C"  void SampleAppUIButton_add_TappedOn_m25973342 (SampleAppUIButton_t4060007389 * __this, Action_t3771233898 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SampleAppUIButton::remove_TappedOn(System.Action)
extern "C"  void SampleAppUIButton_remove_TappedOn_m2539949963 (SampleAppUIButton_t4060007389 * __this, Action_t3771233898 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SampleAppUIButton::Draw()
extern "C"  void SampleAppUIButton_Draw_m3987583386 (SampleAppUIButton_t4060007389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
