﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.vo.result.ReturnResultVO
struct ReturnResultVO_t972950402;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.vo.result.ReturnResultVO::.ctor()
extern "C"  void ReturnResultVO__ctor_m3225690618 (ReturnResultVO_t972950402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
