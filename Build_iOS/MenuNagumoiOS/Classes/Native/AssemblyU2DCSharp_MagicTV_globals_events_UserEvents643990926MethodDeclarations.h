﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.globals.events.UserEvents
struct UserEvents_t643990926;
// MagicTV.globals.events.UserEvents/OnConfirmEventHandler
struct OnConfirmEventHandler_t3783866142;
// MagicTV.vo.result.ConfirmResultVO
struct ConfirmResultVO_t1731700412;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_globals_events_UserEvent3783866142.h"
#include "AssemblyU2DCSharp_MagicTV_vo_result_ConfirmResultV1731700412.h"

// System.Void MagicTV.globals.events.UserEvents::.ctor()
extern "C"  void UserEvents__ctor_m697922991 (UserEvents_t643990926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.UserEvents::AddConfirmEventHandler(MagicTV.globals.events.UserEvents/OnConfirmEventHandler)
extern "C"  void UserEvents_AddConfirmEventHandler_m4111669712 (UserEvents_t643990926 * __this, OnConfirmEventHandler_t3783866142 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.UserEvents::RemoveConfirmEventHandler(MagicTV.globals.events.UserEvents/OnConfirmEventHandler)
extern "C"  void UserEvents_RemoveConfirmEventHandler_m2934476315 (UserEvents_t643990926 * __this, OnConfirmEventHandler_t3783866142 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.UserEvents::RaiseConfirm(MagicTV.vo.result.ConfirmResultVO)
extern "C"  void UserEvents_RaiseConfirm_m2059359438 (UserEvents_t643990926 * __this, ConfirmResultVO_t1731700412 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.UserEvents::AddConfirmCalledEventHandler(MagicTV.globals.events.UserEvents/OnConfirmEventHandler)
extern "C"  void UserEvents_AddConfirmCalledEventHandler_m2732276371 (UserEvents_t643990926 * __this, OnConfirmEventHandler_t3783866142 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.UserEvents::RemoveConfirmCalledEventHandler(MagicTV.globals.events.UserEvents/OnConfirmEventHandler)
extern "C"  void UserEvents_RemoveConfirmCalledEventHandler_m3141339806 (UserEvents_t643990926 * __this, OnConfirmEventHandler_t3783866142 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.UserEvents::RaiseConfirmCalled(MagicTV.vo.result.ConfirmResultVO)
extern "C"  void UserEvents_RaiseConfirmCalled_m3790684689 (UserEvents_t643990926 * __this, ConfirmResultVO_t1731700412 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.globals.events.UserEvents MagicTV.globals.events.UserEvents::GetInstance()
extern "C"  UserEvents_t643990926 * UserEvents_GetInstance_m2684728495 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
