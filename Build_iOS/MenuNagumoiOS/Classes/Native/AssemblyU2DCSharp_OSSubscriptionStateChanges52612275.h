﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OSSubscriptionState
struct OSSubscriptionState_t1688362992;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSSubscriptionStateChanges
struct  OSSubscriptionStateChanges_t52612275  : public Il2CppObject
{
public:
	// OSSubscriptionState OSSubscriptionStateChanges::to
	OSSubscriptionState_t1688362992 * ___to_0;
	// OSSubscriptionState OSSubscriptionStateChanges::from
	OSSubscriptionState_t1688362992 * ___from_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(OSSubscriptionStateChanges_t52612275, ___to_0)); }
	inline OSSubscriptionState_t1688362992 * get_to_0() const { return ___to_0; }
	inline OSSubscriptionState_t1688362992 ** get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(OSSubscriptionState_t1688362992 * value)
	{
		___to_0 = value;
		Il2CppCodeGenWriteBarrier(&___to_0, value);
	}

	inline static int32_t get_offset_of_from_1() { return static_cast<int32_t>(offsetof(OSSubscriptionStateChanges_t52612275, ___from_1)); }
	inline OSSubscriptionState_t1688362992 * get_from_1() const { return ___from_1; }
	inline OSSubscriptionState_t1688362992 ** get_address_of_from_1() { return &___from_1; }
	inline void set_from_1(OSSubscriptionState_t1688362992 * value)
	{
		___from_1 = value;
		Il2CppCodeGenWriteBarrier(&___from_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
