﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.OneDReader
struct  OneDReader_t3436042911  : public Il2CppObject
{
public:

public:
};

struct OneDReader_t3436042911_StaticFields
{
public:
	// System.Int32 ZXing.OneD.OneDReader::INTEGER_MATH_SHIFT
	int32_t ___INTEGER_MATH_SHIFT_0;
	// System.Int32 ZXing.OneD.OneDReader::PATTERN_MATCH_RESULT_SCALE_FACTOR
	int32_t ___PATTERN_MATCH_RESULT_SCALE_FACTOR_1;

public:
	inline static int32_t get_offset_of_INTEGER_MATH_SHIFT_0() { return static_cast<int32_t>(offsetof(OneDReader_t3436042911_StaticFields, ___INTEGER_MATH_SHIFT_0)); }
	inline int32_t get_INTEGER_MATH_SHIFT_0() const { return ___INTEGER_MATH_SHIFT_0; }
	inline int32_t* get_address_of_INTEGER_MATH_SHIFT_0() { return &___INTEGER_MATH_SHIFT_0; }
	inline void set_INTEGER_MATH_SHIFT_0(int32_t value)
	{
		___INTEGER_MATH_SHIFT_0 = value;
	}

	inline static int32_t get_offset_of_PATTERN_MATCH_RESULT_SCALE_FACTOR_1() { return static_cast<int32_t>(offsetof(OneDReader_t3436042911_StaticFields, ___PATTERN_MATCH_RESULT_SCALE_FACTOR_1)); }
	inline int32_t get_PATTERN_MATCH_RESULT_SCALE_FACTOR_1() const { return ___PATTERN_MATCH_RESULT_SCALE_FACTOR_1; }
	inline int32_t* get_address_of_PATTERN_MATCH_RESULT_SCALE_FACTOR_1() { return &___PATTERN_MATCH_RESULT_SCALE_FACTOR_1; }
	inline void set_PATTERN_MATCH_RESULT_SCALE_FACTOR_1(int32_t value)
	{
		___PATTERN_MATCH_RESULT_SCALE_FACTOR_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
