﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetParent
struct  SetParent_t596534010  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetParent::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.SetParent::parent
	FsmGameObject_t1697147867 * ___parent_10;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetParent::resetLocalPosition
	FsmBool_t1075959796 * ___resetLocalPosition_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetParent::resetLocalRotation
	FsmBool_t1075959796 * ___resetLocalRotation_12;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(SetParent_t596534010, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_parent_10() { return static_cast<int32_t>(offsetof(SetParent_t596534010, ___parent_10)); }
	inline FsmGameObject_t1697147867 * get_parent_10() const { return ___parent_10; }
	inline FsmGameObject_t1697147867 ** get_address_of_parent_10() { return &___parent_10; }
	inline void set_parent_10(FsmGameObject_t1697147867 * value)
	{
		___parent_10 = value;
		Il2CppCodeGenWriteBarrier(&___parent_10, value);
	}

	inline static int32_t get_offset_of_resetLocalPosition_11() { return static_cast<int32_t>(offsetof(SetParent_t596534010, ___resetLocalPosition_11)); }
	inline FsmBool_t1075959796 * get_resetLocalPosition_11() const { return ___resetLocalPosition_11; }
	inline FsmBool_t1075959796 ** get_address_of_resetLocalPosition_11() { return &___resetLocalPosition_11; }
	inline void set_resetLocalPosition_11(FsmBool_t1075959796 * value)
	{
		___resetLocalPosition_11 = value;
		Il2CppCodeGenWriteBarrier(&___resetLocalPosition_11, value);
	}

	inline static int32_t get_offset_of_resetLocalRotation_12() { return static_cast<int32_t>(offsetof(SetParent_t596534010, ___resetLocalRotation_12)); }
	inline FsmBool_t1075959796 * get_resetLocalRotation_12() const { return ___resetLocalRotation_12; }
	inline FsmBool_t1075959796 ** get_address_of_resetLocalRotation_12() { return &___resetLocalRotation_12; }
	inline void set_resetLocalRotation_12(FsmBool_t1075959796 * value)
	{
		___resetLocalRotation_12 = value;
		Il2CppCodeGenWriteBarrier(&___resetLocalRotation_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
