﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.DelayedEvent
struct DelayedEvent_t1938906778;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SequenceEvent
struct  SequenceEvent_t2146596135  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SequenceEvent::delay
	FsmFloat_t2134102846 * ___delay_9;
	// HutongGames.PlayMaker.DelayedEvent HutongGames.PlayMaker.Actions.SequenceEvent::delayedEvent
	DelayedEvent_t1938906778 * ___delayedEvent_10;
	// System.Int32 HutongGames.PlayMaker.Actions.SequenceEvent::eventIndex
	int32_t ___eventIndex_11;

public:
	inline static int32_t get_offset_of_delay_9() { return static_cast<int32_t>(offsetof(SequenceEvent_t2146596135, ___delay_9)); }
	inline FsmFloat_t2134102846 * get_delay_9() const { return ___delay_9; }
	inline FsmFloat_t2134102846 ** get_address_of_delay_9() { return &___delay_9; }
	inline void set_delay_9(FsmFloat_t2134102846 * value)
	{
		___delay_9 = value;
		Il2CppCodeGenWriteBarrier(&___delay_9, value);
	}

	inline static int32_t get_offset_of_delayedEvent_10() { return static_cast<int32_t>(offsetof(SequenceEvent_t2146596135, ___delayedEvent_10)); }
	inline DelayedEvent_t1938906778 * get_delayedEvent_10() const { return ___delayedEvent_10; }
	inline DelayedEvent_t1938906778 ** get_address_of_delayedEvent_10() { return &___delayedEvent_10; }
	inline void set_delayedEvent_10(DelayedEvent_t1938906778 * value)
	{
		___delayedEvent_10 = value;
		Il2CppCodeGenWriteBarrier(&___delayedEvent_10, value);
	}

	inline static int32_t get_offset_of_eventIndex_11() { return static_cast<int32_t>(offsetof(SequenceEvent_t2146596135, ___eventIndex_11)); }
	inline int32_t get_eventIndex_11() const { return ___eventIndex_11; }
	inline int32_t* get_address_of_eventIndex_11() { return &___eventIndex_11; }
	inline void set_eventIndex_11(int32_t value)
	{
		___eventIndex_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
