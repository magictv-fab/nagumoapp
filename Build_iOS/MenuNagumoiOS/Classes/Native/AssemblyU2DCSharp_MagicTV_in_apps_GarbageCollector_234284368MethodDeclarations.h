﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.in_apps.GarbageCollector/<getGarbageItem>c__AnonStoreyA0
struct U3CgetGarbageItemU3Ec__AnonStoreyA0_t234284368;
// MagicTV.in_apps.GarbageItemVO
struct GarbageItemVO_t1335202303;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_in_apps_GarbageItemVO1335202303.h"

// System.Void MagicTV.in_apps.GarbageCollector/<getGarbageItem>c__AnonStoreyA0::.ctor()
extern "C"  void U3CgetGarbageItemU3Ec__AnonStoreyA0__ctor_m3728059211 (U3CgetGarbageItemU3Ec__AnonStoreyA0_t234284368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicTV.in_apps.GarbageCollector/<getGarbageItem>c__AnonStoreyA0::<>m__4E(MagicTV.in_apps.GarbageItemVO)
extern "C"  bool U3CgetGarbageItemU3Ec__AnonStoreyA0_U3CU3Em__4E_m243301985 (U3CgetGarbageItemU3Ec__AnonStoreyA0_t234284368 * __this, GarbageItemVO_t1335202303 * ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
