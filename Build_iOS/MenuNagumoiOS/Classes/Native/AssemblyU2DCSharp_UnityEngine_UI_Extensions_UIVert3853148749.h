﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Extensions.UIVerticalScroller
struct UIVerticalScroller_t2397627752;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.UIVerticalScroller/<AddListener>c__AnonStoreyA8
struct  U3CAddListenerU3Ec__AnonStoreyA8_t3853148749  : public Il2CppObject
{
public:
	// System.Int32 UnityEngine.UI.Extensions.UIVerticalScroller/<AddListener>c__AnonStoreyA8::index
	int32_t ___index_0;
	// UnityEngine.UI.Extensions.UIVerticalScroller UnityEngine.UI.Extensions.UIVerticalScroller/<AddListener>c__AnonStoreyA8::<>f__this
	UIVerticalScroller_t2397627752 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(U3CAddListenerU3Ec__AnonStoreyA8_t3853148749, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CAddListenerU3Ec__AnonStoreyA8_t3853148749, ___U3CU3Ef__this_1)); }
	inline UIVerticalScroller_t2397627752 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline UIVerticalScroller_t2397627752 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(UIVerticalScroller_t2397627752 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
