﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DeviceFileInfo/<populateIndexedBundles>c__AnonStorey98
struct U3CpopulateIndexedBundlesU3Ec__AnonStorey98_t1432591743;
// MagicTV.vo.MetadataVO
struct MetadataVO_t2511256998;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_vo_MetadataVO2511256998.h"

// System.Void DeviceFileInfo/<populateIndexedBundles>c__AnonStorey98::.ctor()
extern "C"  void U3CpopulateIndexedBundlesU3Ec__AnonStorey98__ctor_m508687868 (U3CpopulateIndexedBundlesU3Ec__AnonStorey98_t1432591743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceFileInfo/<populateIndexedBundles>c__AnonStorey98::<>m__2B(MagicTV.vo.MetadataVO)
extern "C"  void U3CpopulateIndexedBundlesU3Ec__AnonStorey98_U3CU3Em__2B_m745456063 (U3CpopulateIndexedBundlesU3Ec__AnonStorey98_t1432591743 * __this, MetadataVO_t2511256998 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
