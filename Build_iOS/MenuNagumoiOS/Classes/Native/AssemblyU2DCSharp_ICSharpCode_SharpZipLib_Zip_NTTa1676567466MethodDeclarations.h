﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.NTTaggedData
struct NTTaggedData_t1676567466;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime4283661327.h"

// System.Void ICSharpCode.SharpZipLib.Zip.NTTaggedData::.ctor()
extern "C"  void NTTaggedData__ctor_m3050390337 (NTTaggedData_t1676567466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 ICSharpCode.SharpZipLib.Zip.NTTaggedData::get_TagID()
extern "C"  int16_t NTTaggedData_get_TagID_m2942338147 (NTTaggedData_t1676567466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.NTTaggedData::SetData(System.Byte[],System.Int32,System.Int32)
extern "C"  void NTTaggedData_SetData_m4141262974 (NTTaggedData_t1676567466 * __this, ByteU5BU5D_t4260760469* ___data0, int32_t ___index1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ICSharpCode.SharpZipLib.Zip.NTTaggedData::GetData()
extern "C"  ByteU5BU5D_t4260760469* NTTaggedData_GetData_m344906197 (NTTaggedData_t1676567466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.NTTaggedData::IsValidValue(System.DateTime)
extern "C"  bool NTTaggedData_IsValidValue_m1664433410 (Il2CppObject * __this /* static, unused */, DateTime_t4283661327  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime ICSharpCode.SharpZipLib.Zip.NTTaggedData::get_LastModificationTime()
extern "C"  DateTime_t4283661327  NTTaggedData_get_LastModificationTime_m336196036 (NTTaggedData_t1676567466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.NTTaggedData::set_LastModificationTime(System.DateTime)
extern "C"  void NTTaggedData_set_LastModificationTime_m678075769 (NTTaggedData_t1676567466 * __this, DateTime_t4283661327  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime ICSharpCode.SharpZipLib.Zip.NTTaggedData::get_CreateTime()
extern "C"  DateTime_t4283661327  NTTaggedData_get_CreateTime_m2134780142 (NTTaggedData_t1676567466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.NTTaggedData::set_CreateTime(System.DateTime)
extern "C"  void NTTaggedData_set_CreateTime_m1211375439 (NTTaggedData_t1676567466 * __this, DateTime_t4283661327  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime ICSharpCode.SharpZipLib.Zip.NTTaggedData::get_LastAccessTime()
extern "C"  DateTime_t4283661327  NTTaggedData_get_LastAccessTime_m3844621068 (NTTaggedData_t1676567466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.NTTaggedData::set_LastAccessTime(System.DateTime)
extern "C"  void NTTaggedData_set_LastAccessTime_m1139104497 (NTTaggedData_t1676567466 * __this, DateTime_t4283661327  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
