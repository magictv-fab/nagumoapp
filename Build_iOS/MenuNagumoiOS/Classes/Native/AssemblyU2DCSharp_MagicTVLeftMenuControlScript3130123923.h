﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Animation
struct Animation_t1724966010;
// UnityEngine.AnimationClip
struct AnimationClip_t2007702890;

#include "AssemblyU2DCSharp_LeftMenuControlScript2683180034.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTVLeftMenuControlScript
struct  MagicTVLeftMenuControlScript_t3130123923  : public LeftMenuControlScript_t2683180034
{
public:
	// UnityEngine.Animation MagicTVLeftMenuControlScript::animationController
	Animation_t1724966010 * ___animationController_5;
	// UnityEngine.AnimationClip MagicTVLeftMenuControlScript::openAnimationClip
	AnimationClip_t2007702890 * ___openAnimationClip_6;
	// UnityEngine.AnimationClip MagicTVLeftMenuControlScript::closeAnimationClip
	AnimationClip_t2007702890 * ___closeAnimationClip_7;

public:
	inline static int32_t get_offset_of_animationController_5() { return static_cast<int32_t>(offsetof(MagicTVLeftMenuControlScript_t3130123923, ___animationController_5)); }
	inline Animation_t1724966010 * get_animationController_5() const { return ___animationController_5; }
	inline Animation_t1724966010 ** get_address_of_animationController_5() { return &___animationController_5; }
	inline void set_animationController_5(Animation_t1724966010 * value)
	{
		___animationController_5 = value;
		Il2CppCodeGenWriteBarrier(&___animationController_5, value);
	}

	inline static int32_t get_offset_of_openAnimationClip_6() { return static_cast<int32_t>(offsetof(MagicTVLeftMenuControlScript_t3130123923, ___openAnimationClip_6)); }
	inline AnimationClip_t2007702890 * get_openAnimationClip_6() const { return ___openAnimationClip_6; }
	inline AnimationClip_t2007702890 ** get_address_of_openAnimationClip_6() { return &___openAnimationClip_6; }
	inline void set_openAnimationClip_6(AnimationClip_t2007702890 * value)
	{
		___openAnimationClip_6 = value;
		Il2CppCodeGenWriteBarrier(&___openAnimationClip_6, value);
	}

	inline static int32_t get_offset_of_closeAnimationClip_7() { return static_cast<int32_t>(offsetof(MagicTVLeftMenuControlScript_t3130123923, ___closeAnimationClip_7)); }
	inline AnimationClip_t2007702890 * get_closeAnimationClip_7() const { return ___closeAnimationClip_7; }
	inline AnimationClip_t2007702890 ** get_address_of_closeAnimationClip_7() { return &___closeAnimationClip_7; }
	inline void set_closeAnimationClip_7(AnimationClip_t2007702890 * value)
	{
		___closeAnimationClip_7 = value;
		Il2CppCodeGenWriteBarrier(&___closeAnimationClip_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
