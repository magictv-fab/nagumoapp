﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.events.ChangeEventAbstract/OnChangeEventHandler
struct OnChangeEventHandler_t2629944389;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ARM.events.ChangeEventAbstract/OnChangeEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnChangeEventHandler__ctor_m713204636 (OnChangeEventHandler_t2629944389 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.events.ChangeEventAbstract/OnChangeEventHandler::Invoke(System.Single)
extern "C"  void OnChangeEventHandler_Invoke_m921331317 (OnChangeEventHandler_t2629944389 * __this, float ___percentChange0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ARM.events.ChangeEventAbstract/OnChangeEventHandler::BeginInvoke(System.Single,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnChangeEventHandler_BeginInvoke_m1080278928 (OnChangeEventHandler_t2629944389 * __this, float ___percentChange0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.events.ChangeEventAbstract/OnChangeEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnChangeEventHandler_EndInvoke_m1192248236 (OnChangeEventHandler_t2629944389 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
