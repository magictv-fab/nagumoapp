﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Area730.Notifications.NotificationInstance
struct NotificationInstance_t3873938600;

#include "codegen/il2cpp-codegen.h"

// System.Void Area730.Notifications.NotificationInstance::.ctor()
extern "C"  void NotificationInstance__ctor_m1286944974 (NotificationInstance_t3873938600 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Area730.Notifications.NotificationInstance::Print()
extern "C"  void NotificationInstance_Print_m1816549721 (NotificationInstance_t3873938600 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
