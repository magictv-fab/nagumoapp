﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2013616441.h"
#include "mscorlib_System_Array1146569071.h"
#include "UnityEngine_UnityEngine_NetworkPlayer3231273765.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.NetworkPlayer>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2566756701_gshared (InternalEnumerator_1_t2013616441 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2566756701(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2013616441 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2566756701_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.NetworkPlayer>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1327160611_gshared (InternalEnumerator_1_t2013616441 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1327160611(__this, method) ((  void (*) (InternalEnumerator_1_t2013616441 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1327160611_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.NetworkPlayer>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3324148505_gshared (InternalEnumerator_1_t2013616441 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3324148505(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2013616441 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3324148505_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.NetworkPlayer>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1301070708_gshared (InternalEnumerator_1_t2013616441 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1301070708(__this, method) ((  void (*) (InternalEnumerator_1_t2013616441 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1301070708_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.NetworkPlayer>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m467923411_gshared (InternalEnumerator_1_t2013616441 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m467923411(__this, method) ((  bool (*) (InternalEnumerator_1_t2013616441 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m467923411_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.NetworkPlayer>::get_Current()
extern "C"  NetworkPlayer_t3231273765  InternalEnumerator_1_get_Current_m1630916742_gshared (InternalEnumerator_1_t2013616441 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1630916742(__this, method) ((  NetworkPlayer_t3231273765  (*) (InternalEnumerator_1_t2013616441 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1630916742_gshared)(__this, method)
