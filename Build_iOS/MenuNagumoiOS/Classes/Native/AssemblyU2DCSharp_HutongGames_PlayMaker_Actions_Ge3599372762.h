﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t533912881;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmRect
struct FsmRect_t1076426478;
// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t3871136040;
// HutongGames.PlayMaker.FsmMaterial
struct FsmMaterial_t924399665;
// HutongGames.PlayMaker.FsmTexture
struct FsmTexture_t3073272573;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t2131419205;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t821476169;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetEventInfo
struct  GetEventInfo_t3599372762  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetEventInfo::sentByGameObject
	FsmGameObject_t1697147867 * ___sentByGameObject_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetEventInfo::fsmName
	FsmString_t952858651 * ___fsmName_10;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetEventInfo::getBoolData
	FsmBool_t1075959796 * ___getBoolData_11;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetEventInfo::getIntData
	FsmInt_t1596138449 * ___getIntData_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetEventInfo::getFloatData
	FsmFloat_t2134102846 * ___getFloatData_13;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.GetEventInfo::getVector2Data
	FsmVector2_t533912881 * ___getVector2Data_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetEventInfo::getVector3Data
	FsmVector3_t533912882 * ___getVector3Data_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetEventInfo::getStringData
	FsmString_t952858651 * ___getStringData_16;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetEventInfo::getGameObjectData
	FsmGameObject_t1697147867 * ___getGameObjectData_17;
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.GetEventInfo::getRectData
	FsmRect_t1076426478 * ___getRectData_18;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.GetEventInfo::getQuaternionData
	FsmQuaternion_t3871136040 * ___getQuaternionData_19;
	// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Actions.GetEventInfo::getMaterialData
	FsmMaterial_t924399665 * ___getMaterialData_20;
	// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Actions.GetEventInfo::getTextureData
	FsmTexture_t3073272573 * ___getTextureData_21;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.GetEventInfo::getColorData
	FsmColor_t2131419205 * ___getColorData_22;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.GetEventInfo::getObjectData
	FsmObject_t821476169 * ___getObjectData_23;

public:
	inline static int32_t get_offset_of_sentByGameObject_9() { return static_cast<int32_t>(offsetof(GetEventInfo_t3599372762, ___sentByGameObject_9)); }
	inline FsmGameObject_t1697147867 * get_sentByGameObject_9() const { return ___sentByGameObject_9; }
	inline FsmGameObject_t1697147867 ** get_address_of_sentByGameObject_9() { return &___sentByGameObject_9; }
	inline void set_sentByGameObject_9(FsmGameObject_t1697147867 * value)
	{
		___sentByGameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___sentByGameObject_9, value);
	}

	inline static int32_t get_offset_of_fsmName_10() { return static_cast<int32_t>(offsetof(GetEventInfo_t3599372762, ___fsmName_10)); }
	inline FsmString_t952858651 * get_fsmName_10() const { return ___fsmName_10; }
	inline FsmString_t952858651 ** get_address_of_fsmName_10() { return &___fsmName_10; }
	inline void set_fsmName_10(FsmString_t952858651 * value)
	{
		___fsmName_10 = value;
		Il2CppCodeGenWriteBarrier(&___fsmName_10, value);
	}

	inline static int32_t get_offset_of_getBoolData_11() { return static_cast<int32_t>(offsetof(GetEventInfo_t3599372762, ___getBoolData_11)); }
	inline FsmBool_t1075959796 * get_getBoolData_11() const { return ___getBoolData_11; }
	inline FsmBool_t1075959796 ** get_address_of_getBoolData_11() { return &___getBoolData_11; }
	inline void set_getBoolData_11(FsmBool_t1075959796 * value)
	{
		___getBoolData_11 = value;
		Il2CppCodeGenWriteBarrier(&___getBoolData_11, value);
	}

	inline static int32_t get_offset_of_getIntData_12() { return static_cast<int32_t>(offsetof(GetEventInfo_t3599372762, ___getIntData_12)); }
	inline FsmInt_t1596138449 * get_getIntData_12() const { return ___getIntData_12; }
	inline FsmInt_t1596138449 ** get_address_of_getIntData_12() { return &___getIntData_12; }
	inline void set_getIntData_12(FsmInt_t1596138449 * value)
	{
		___getIntData_12 = value;
		Il2CppCodeGenWriteBarrier(&___getIntData_12, value);
	}

	inline static int32_t get_offset_of_getFloatData_13() { return static_cast<int32_t>(offsetof(GetEventInfo_t3599372762, ___getFloatData_13)); }
	inline FsmFloat_t2134102846 * get_getFloatData_13() const { return ___getFloatData_13; }
	inline FsmFloat_t2134102846 ** get_address_of_getFloatData_13() { return &___getFloatData_13; }
	inline void set_getFloatData_13(FsmFloat_t2134102846 * value)
	{
		___getFloatData_13 = value;
		Il2CppCodeGenWriteBarrier(&___getFloatData_13, value);
	}

	inline static int32_t get_offset_of_getVector2Data_14() { return static_cast<int32_t>(offsetof(GetEventInfo_t3599372762, ___getVector2Data_14)); }
	inline FsmVector2_t533912881 * get_getVector2Data_14() const { return ___getVector2Data_14; }
	inline FsmVector2_t533912881 ** get_address_of_getVector2Data_14() { return &___getVector2Data_14; }
	inline void set_getVector2Data_14(FsmVector2_t533912881 * value)
	{
		___getVector2Data_14 = value;
		Il2CppCodeGenWriteBarrier(&___getVector2Data_14, value);
	}

	inline static int32_t get_offset_of_getVector3Data_15() { return static_cast<int32_t>(offsetof(GetEventInfo_t3599372762, ___getVector3Data_15)); }
	inline FsmVector3_t533912882 * get_getVector3Data_15() const { return ___getVector3Data_15; }
	inline FsmVector3_t533912882 ** get_address_of_getVector3Data_15() { return &___getVector3Data_15; }
	inline void set_getVector3Data_15(FsmVector3_t533912882 * value)
	{
		___getVector3Data_15 = value;
		Il2CppCodeGenWriteBarrier(&___getVector3Data_15, value);
	}

	inline static int32_t get_offset_of_getStringData_16() { return static_cast<int32_t>(offsetof(GetEventInfo_t3599372762, ___getStringData_16)); }
	inline FsmString_t952858651 * get_getStringData_16() const { return ___getStringData_16; }
	inline FsmString_t952858651 ** get_address_of_getStringData_16() { return &___getStringData_16; }
	inline void set_getStringData_16(FsmString_t952858651 * value)
	{
		___getStringData_16 = value;
		Il2CppCodeGenWriteBarrier(&___getStringData_16, value);
	}

	inline static int32_t get_offset_of_getGameObjectData_17() { return static_cast<int32_t>(offsetof(GetEventInfo_t3599372762, ___getGameObjectData_17)); }
	inline FsmGameObject_t1697147867 * get_getGameObjectData_17() const { return ___getGameObjectData_17; }
	inline FsmGameObject_t1697147867 ** get_address_of_getGameObjectData_17() { return &___getGameObjectData_17; }
	inline void set_getGameObjectData_17(FsmGameObject_t1697147867 * value)
	{
		___getGameObjectData_17 = value;
		Il2CppCodeGenWriteBarrier(&___getGameObjectData_17, value);
	}

	inline static int32_t get_offset_of_getRectData_18() { return static_cast<int32_t>(offsetof(GetEventInfo_t3599372762, ___getRectData_18)); }
	inline FsmRect_t1076426478 * get_getRectData_18() const { return ___getRectData_18; }
	inline FsmRect_t1076426478 ** get_address_of_getRectData_18() { return &___getRectData_18; }
	inline void set_getRectData_18(FsmRect_t1076426478 * value)
	{
		___getRectData_18 = value;
		Il2CppCodeGenWriteBarrier(&___getRectData_18, value);
	}

	inline static int32_t get_offset_of_getQuaternionData_19() { return static_cast<int32_t>(offsetof(GetEventInfo_t3599372762, ___getQuaternionData_19)); }
	inline FsmQuaternion_t3871136040 * get_getQuaternionData_19() const { return ___getQuaternionData_19; }
	inline FsmQuaternion_t3871136040 ** get_address_of_getQuaternionData_19() { return &___getQuaternionData_19; }
	inline void set_getQuaternionData_19(FsmQuaternion_t3871136040 * value)
	{
		___getQuaternionData_19 = value;
		Il2CppCodeGenWriteBarrier(&___getQuaternionData_19, value);
	}

	inline static int32_t get_offset_of_getMaterialData_20() { return static_cast<int32_t>(offsetof(GetEventInfo_t3599372762, ___getMaterialData_20)); }
	inline FsmMaterial_t924399665 * get_getMaterialData_20() const { return ___getMaterialData_20; }
	inline FsmMaterial_t924399665 ** get_address_of_getMaterialData_20() { return &___getMaterialData_20; }
	inline void set_getMaterialData_20(FsmMaterial_t924399665 * value)
	{
		___getMaterialData_20 = value;
		Il2CppCodeGenWriteBarrier(&___getMaterialData_20, value);
	}

	inline static int32_t get_offset_of_getTextureData_21() { return static_cast<int32_t>(offsetof(GetEventInfo_t3599372762, ___getTextureData_21)); }
	inline FsmTexture_t3073272573 * get_getTextureData_21() const { return ___getTextureData_21; }
	inline FsmTexture_t3073272573 ** get_address_of_getTextureData_21() { return &___getTextureData_21; }
	inline void set_getTextureData_21(FsmTexture_t3073272573 * value)
	{
		___getTextureData_21 = value;
		Il2CppCodeGenWriteBarrier(&___getTextureData_21, value);
	}

	inline static int32_t get_offset_of_getColorData_22() { return static_cast<int32_t>(offsetof(GetEventInfo_t3599372762, ___getColorData_22)); }
	inline FsmColor_t2131419205 * get_getColorData_22() const { return ___getColorData_22; }
	inline FsmColor_t2131419205 ** get_address_of_getColorData_22() { return &___getColorData_22; }
	inline void set_getColorData_22(FsmColor_t2131419205 * value)
	{
		___getColorData_22 = value;
		Il2CppCodeGenWriteBarrier(&___getColorData_22, value);
	}

	inline static int32_t get_offset_of_getObjectData_23() { return static_cast<int32_t>(offsetof(GetEventInfo_t3599372762, ___getObjectData_23)); }
	inline FsmObject_t821476169 * get_getObjectData_23() const { return ___getObjectData_23; }
	inline FsmObject_t821476169 ** get_address_of_getObjectData_23() { return &___getObjectData_23; }
	inline void set_getObjectData_23(FsmObject_t821476169 * value)
	{
		___getObjectData_23 = value;
		Il2CppCodeGenWriteBarrier(&___getObjectData_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
