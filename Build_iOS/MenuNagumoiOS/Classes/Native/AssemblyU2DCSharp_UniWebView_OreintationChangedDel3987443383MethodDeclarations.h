﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniWebView/OreintationChangedDelegate
struct OreintationChangedDelegate_t3987443383;
// System.Object
struct Il2CppObject;
// UniWebView
struct UniWebView_t424341801;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_UniWebView424341801.h"
#include "UnityEngine_UnityEngine_ScreenOrientation1849668026.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void UniWebView/OreintationChangedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void OreintationChangedDelegate__ctor_m48899854 (OreintationChangedDelegate_t3987443383 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView/OreintationChangedDelegate::Invoke(UniWebView,UnityEngine.ScreenOrientation)
extern "C"  void OreintationChangedDelegate_Invoke_m111252222 (OreintationChangedDelegate_t3987443383 * __this, UniWebView_t424341801 * ___webView0, int32_t ___orientation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult UniWebView/OreintationChangedDelegate::BeginInvoke(UniWebView,UnityEngine.ScreenOrientation,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OreintationChangedDelegate_BeginInvoke_m2254687173 (OreintationChangedDelegate_t3987443383 * __this, UniWebView_t424341801 * ___webView0, int32_t ___orientation1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView/OreintationChangedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void OreintationChangedDelegate_EndInvoke_m194465822 (OreintationChangedDelegate_t3987443383 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
