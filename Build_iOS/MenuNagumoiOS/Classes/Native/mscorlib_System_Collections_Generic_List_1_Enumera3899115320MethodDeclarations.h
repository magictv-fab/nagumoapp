﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<MagicTV.vo.MetadataVO>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m4293279269(__this, ___l0, method) ((  void (*) (Enumerator_t3899115320 *, List_1_t3879442550 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<MagicTV.vo.MetadataVO>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3190412045(__this, method) ((  void (*) (Enumerator_t3899115320 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<MagicTV.vo.MetadataVO>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1529314169(__this, method) ((  Il2CppObject * (*) (Enumerator_t3899115320 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<MagicTV.vo.MetadataVO>::Dispose()
#define Enumerator_Dispose_m3925140810(__this, method) ((  void (*) (Enumerator_t3899115320 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<MagicTV.vo.MetadataVO>::VerifyState()
#define Enumerator_VerifyState_m3628134275(__this, method) ((  void (*) (Enumerator_t3899115320 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<MagicTV.vo.MetadataVO>::MoveNext()
#define Enumerator_MoveNext_m3315296889(__this, method) ((  bool (*) (Enumerator_t3899115320 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<MagicTV.vo.MetadataVO>::get_Current()
#define Enumerator_get_Current_m2638682234(__this, method) ((  MetadataVO_t2511256998 * (*) (Enumerator_t3899115320 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
