﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetRandomChild
struct GetRandomChild_t1715933131;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetRandomChild::.ctor()
extern "C"  void GetRandomChild__ctor_m3037302011 (GetRandomChild_t1715933131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRandomChild::Reset()
extern "C"  void GetRandomChild_Reset_m683734952 (GetRandomChild_t1715933131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRandomChild::OnEnter()
extern "C"  void GetRandomChild_OnEnter_m1956167186 (GetRandomChild_t1715933131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRandomChild::DoGetRandomChild()
extern "C"  void GetRandomChild_DoGetRandomChild_m1065979831 (GetRandomChild_t1715933131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
