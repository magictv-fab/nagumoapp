﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3263875864MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3614831140(__this, ___dictionary0, method) ((  void (*) (Enumerator_t4018270335 *, Dictionary_2_t2700946943 *, const MethodInfo*))Enumerator__ctor_m3385470484_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1736722941(__this, method) ((  Il2CppObject * (*) (Enumerator_t4018270335 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1738102871_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1111494673(__this, method) ((  void (*) (Enumerator_t4018270335 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m433077793_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2467980250(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t4018270335 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m62650648_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1887524121(__this, method) ((  Il2CppObject * (*) (Enumerator_t4018270335 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2097461235_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2482917739(__this, method) ((  Il2CppObject * (*) (Enumerator_t4018270335 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2369021381_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::MoveNext()
#define Enumerator_MoveNext_m803986045(__this, method) ((  bool (*) (Enumerator_t4018270335 *, const MethodInfo*))Enumerator_MoveNext_m3152173905_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::get_Current()
#define Enumerator_get_Current_m889766035(__this, method) ((  KeyValuePair_2_t2599727649  (*) (Enumerator_t4018270335 *, const MethodInfo*))Enumerator_get_Current_m2674279371_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1758958218(__this, method) ((  int32_t (*) (Enumerator_t4018270335 *, const MethodInfo*))Enumerator_get_CurrentKey_m2085094682_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2883287726(__this, method) ((  OnChangeToAnStateEventHandler_t630243546 * (*) (Enumerator_t4018270335 *, const MethodInfo*))Enumerator_get_CurrentValue_m128293082_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::Reset()
#define Enumerator_Reset_m1393530486(__this, method) ((  void (*) (Enumerator_t4018270335 *, const MethodInfo*))Enumerator_Reset_m3310414438_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::VerifyState()
#define Enumerator_VerifyState_m112834047(__this, method) ((  void (*) (Enumerator_t4018270335 *, const MethodInfo*))Enumerator_VerifyState_m1095259631_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3804208551(__this, method) ((  void (*) (Enumerator_t4018270335 *, const MethodInfo*))Enumerator_VerifyCurrent_m3022389655_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>::Dispose()
#define Enumerator_Dispose_m1676429766(__this, method) ((  void (*) (Enumerator_t4018270335 *, const MethodInfo*))Enumerator_Dispose_m1260937654_gshared)(__this, method)
