﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// MagicTV.vo.BundleVO[]
struct BundleVOU5BU5D_t2162892100;
// MagicTV.vo.MetadataVO[]
struct MetadataVOU5BU5D_t4238035203;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.vo.ResultRevisionVO
struct  ResultRevisionVO_t2445597391  : public Il2CppObject
{
public:
	// System.String MagicTV.vo.ResultRevisionVO::revision
	String_t* ___revision_0;
	// System.String MagicTV.vo.ResultRevisionVO::name
	String_t* ___name_1;
	// System.String MagicTV.vo.ResultRevisionVO::short_description
	String_t* ___short_description_2;
	// System.String MagicTV.vo.ResultRevisionVO::long_description
	String_t* ___long_description_3;
	// MagicTV.vo.BundleVO[] MagicTV.vo.ResultRevisionVO::bundles
	BundleVOU5BU5D_t2162892100* ___bundles_4;
	// MagicTV.vo.MetadataVO[] MagicTV.vo.ResultRevisionVO::metadata
	MetadataVOU5BU5D_t4238035203* ___metadata_5;
	// System.String MagicTV.vo.ResultRevisionVO::pin
	String_t* ___pin_6;

public:
	inline static int32_t get_offset_of_revision_0() { return static_cast<int32_t>(offsetof(ResultRevisionVO_t2445597391, ___revision_0)); }
	inline String_t* get_revision_0() const { return ___revision_0; }
	inline String_t** get_address_of_revision_0() { return &___revision_0; }
	inline void set_revision_0(String_t* value)
	{
		___revision_0 = value;
		Il2CppCodeGenWriteBarrier(&___revision_0, value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(ResultRevisionVO_t2445597391, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_short_description_2() { return static_cast<int32_t>(offsetof(ResultRevisionVO_t2445597391, ___short_description_2)); }
	inline String_t* get_short_description_2() const { return ___short_description_2; }
	inline String_t** get_address_of_short_description_2() { return &___short_description_2; }
	inline void set_short_description_2(String_t* value)
	{
		___short_description_2 = value;
		Il2CppCodeGenWriteBarrier(&___short_description_2, value);
	}

	inline static int32_t get_offset_of_long_description_3() { return static_cast<int32_t>(offsetof(ResultRevisionVO_t2445597391, ___long_description_3)); }
	inline String_t* get_long_description_3() const { return ___long_description_3; }
	inline String_t** get_address_of_long_description_3() { return &___long_description_3; }
	inline void set_long_description_3(String_t* value)
	{
		___long_description_3 = value;
		Il2CppCodeGenWriteBarrier(&___long_description_3, value);
	}

	inline static int32_t get_offset_of_bundles_4() { return static_cast<int32_t>(offsetof(ResultRevisionVO_t2445597391, ___bundles_4)); }
	inline BundleVOU5BU5D_t2162892100* get_bundles_4() const { return ___bundles_4; }
	inline BundleVOU5BU5D_t2162892100** get_address_of_bundles_4() { return &___bundles_4; }
	inline void set_bundles_4(BundleVOU5BU5D_t2162892100* value)
	{
		___bundles_4 = value;
		Il2CppCodeGenWriteBarrier(&___bundles_4, value);
	}

	inline static int32_t get_offset_of_metadata_5() { return static_cast<int32_t>(offsetof(ResultRevisionVO_t2445597391, ___metadata_5)); }
	inline MetadataVOU5BU5D_t4238035203* get_metadata_5() const { return ___metadata_5; }
	inline MetadataVOU5BU5D_t4238035203** get_address_of_metadata_5() { return &___metadata_5; }
	inline void set_metadata_5(MetadataVOU5BU5D_t4238035203* value)
	{
		___metadata_5 = value;
		Il2CppCodeGenWriteBarrier(&___metadata_5, value);
	}

	inline static int32_t get_offset_of_pin_6() { return static_cast<int32_t>(offsetof(ResultRevisionVO_t2445597391, ___pin_6)); }
	inline String_t* get_pin_6() const { return ___pin_6; }
	inline String_t** get_address_of_pin_6() { return &___pin_6; }
	inline void set_pin_6(String_t* value)
	{
		___pin_6 = value;
		Il2CppCodeGenWriteBarrier(&___pin_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
