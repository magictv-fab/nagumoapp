﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.OneDReader
struct OneDReader_t3436042911;
// ZXing.Result
struct Result_t2610723219;
// ZXing.BinaryBitmap
struct BinaryBitmap_t2444664454;
// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>
struct IDictionary_2_t759989846;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_BinaryBitmap2444664454.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"

// ZXing.Result ZXing.OneD.OneDReader::decode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  Result_t2610723219 * OneDReader_decode_m1067827375 (OneDReader_t3436042911 * __this, BinaryBitmap_t2444664454 * ___image0, Il2CppObject* ___hints1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.OneDReader::reset()
extern "C"  void OneDReader_reset_m2763980593 (OneDReader_t3436042911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Result ZXing.OneD.OneDReader::doDecode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  Result_t2610723219 * OneDReader_doDecode_m1759284196 (OneDReader_t3436042911 * __this, BinaryBitmap_t2444664454 * ___image0, Il2CppObject* ___hints1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.OneDReader::recordPattern(ZXing.Common.BitArray,System.Int32,System.Int32[])
extern "C"  bool OneDReader_recordPattern_m3135648054 (Il2CppObject * __this /* static, unused */, BitArray_t4163851164 * ___row0, int32_t ___start1, Int32U5BU5D_t3230847821* ___counters2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.OneDReader::recordPattern(ZXing.Common.BitArray,System.Int32,System.Int32[],System.Int32)
extern "C"  bool OneDReader_recordPattern_m2992444257 (Il2CppObject * __this /* static, unused */, BitArray_t4163851164 * ___row0, int32_t ___start1, Int32U5BU5D_t3230847821* ___counters2, int32_t ___numCounters3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.OneDReader::recordPatternInReverse(ZXing.Common.BitArray,System.Int32,System.Int32[])
extern "C"  bool OneDReader_recordPatternInReverse_m4151633505 (Il2CppObject * __this /* static, unused */, BitArray_t4163851164 * ___row0, int32_t ___start1, Int32U5BU5D_t3230847821* ___counters2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.OneDReader::patternMatchVariance(System.Int32[],System.Int32[],System.Int32)
extern "C"  int32_t OneDReader_patternMatchVariance_m2184456265 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___counters0, Int32U5BU5D_t3230847821* ___pattern1, int32_t ___maxIndividualVariance2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.OneDReader::.ctor()
extern "C"  void OneDReader__ctor_m2487233636 (OneDReader_t3436042911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.OneDReader::.cctor()
extern "C"  void OneDReader__cctor_m3607702473 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
