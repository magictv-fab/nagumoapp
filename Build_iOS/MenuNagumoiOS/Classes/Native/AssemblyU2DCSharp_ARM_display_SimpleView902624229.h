﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.display.SimpleView
struct  SimpleView_t902624229  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean ARM.display.SimpleView::_state
	bool ____state_2;
	// System.Boolean ARM.display.SimpleView::_onOff
	bool ____onOff_3;

public:
	inline static int32_t get_offset_of__state_2() { return static_cast<int32_t>(offsetof(SimpleView_t902624229, ____state_2)); }
	inline bool get__state_2() const { return ____state_2; }
	inline bool* get_address_of__state_2() { return &____state_2; }
	inline void set__state_2(bool value)
	{
		____state_2 = value;
	}

	inline static int32_t get_offset_of__onOff_3() { return static_cast<int32_t>(offsetof(SimpleView_t902624229, ____onOff_3)); }
	inline bool get__onOff_3() const { return ____onOff_3; }
	inline bool* get_address_of__onOff_3() { return &____onOff_3; }
	inline void set__onOff_3(bool value)
	{
		____onOff_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
