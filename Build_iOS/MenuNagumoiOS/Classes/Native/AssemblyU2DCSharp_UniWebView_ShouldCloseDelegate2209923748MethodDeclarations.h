﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniWebView/ShouldCloseDelegate
struct ShouldCloseDelegate_t2209923748;
// System.Object
struct Il2CppObject;
// UniWebView
struct UniWebView_t424341801;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_UniWebView424341801.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void UniWebView/ShouldCloseDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ShouldCloseDelegate__ctor_m2612508491 (ShouldCloseDelegate_t2209923748 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniWebView/ShouldCloseDelegate::Invoke(UniWebView)
extern "C"  bool ShouldCloseDelegate_Invoke_m2502172176 (ShouldCloseDelegate_t2209923748 * __this, UniWebView_t424341801 * ___webView0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult UniWebView/ShouldCloseDelegate::BeginInvoke(UniWebView,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ShouldCloseDelegate_BeginInvoke_m2373707657 (ShouldCloseDelegate_t2209923748 * __this, UniWebView_t424341801 * ___webView0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniWebView/ShouldCloseDelegate::EndInvoke(System.IAsyncResult)
extern "C"  bool ShouldCloseDelegate_EndInvoke_m1800860303 (ShouldCloseDelegate_t2209923748 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
