﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.utils.CronWatcher/OnCronEventHandler
struct OnCronEventHandler_t343176941;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ARM.utils.CronWatcher/OnCronEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnCronEventHandler__ctor_m643021076 (OnCronEventHandler_t343176941 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.CronWatcher/OnCronEventHandler::Invoke()
extern "C"  void OnCronEventHandler_Invoke_m3783759470 (OnCronEventHandler_t343176941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ARM.utils.CronWatcher/OnCronEventHandler::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnCronEventHandler_BeginInvoke_m938422997 (OnCronEventHandler_t343176941 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.CronWatcher/OnCronEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnCronEventHandler_EndInvoke_m603378980 (OnCronEventHandler_t343176941 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
