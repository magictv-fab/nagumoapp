﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<MagicTV.globals.Perspective,System.Object>
struct ShimEnumerator_t2226662119;
// System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>
struct Dictionary_2_t2510884092;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<MagicTV.globals.Perspective,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m316360667_gshared (ShimEnumerator_t2226662119 * __this, Dictionary_2_t2510884092 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m316360667(__this, ___host0, method) ((  void (*) (ShimEnumerator_t2226662119 *, Dictionary_2_t2510884092 *, const MethodInfo*))ShimEnumerator__ctor_m316360667_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<MagicTV.globals.Perspective,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3944981094_gshared (ShimEnumerator_t2226662119 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m3944981094(__this, method) ((  bool (*) (ShimEnumerator_t2226662119 *, const MethodInfo*))ShimEnumerator_MoveNext_m3944981094_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<MagicTV.globals.Perspective,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m3853351278_gshared (ShimEnumerator_t2226662119 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m3853351278(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t2226662119 *, const MethodInfo*))ShimEnumerator_get_Entry_m3853351278_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<MagicTV.globals.Perspective,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2477119561_gshared (ShimEnumerator_t2226662119 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m2477119561(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2226662119 *, const MethodInfo*))ShimEnumerator_get_Key_m2477119561_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<MagicTV.globals.Perspective,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m2148452507_gshared (ShimEnumerator_t2226662119 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m2148452507(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2226662119 *, const MethodInfo*))ShimEnumerator_get_Value_m2148452507_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<MagicTV.globals.Perspective,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m208280675_gshared (ShimEnumerator_t2226662119 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m208280675(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2226662119 *, const MethodInfo*))ShimEnumerator_get_Current_m208280675_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<MagicTV.globals.Perspective,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m1790724525_gshared (ShimEnumerator_t2226662119 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m1790724525(__this, method) ((  void (*) (ShimEnumerator_t2226662119 *, const MethodInfo*))ShimEnumerator_Reset_m1790724525_gshared)(__this, method)
