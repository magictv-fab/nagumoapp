﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ButtonAnim
struct ButtonAnim_t2351452291;

#include "codegen/il2cpp-codegen.h"

// System.Void ButtonAnim::.ctor()
extern "C"  void ButtonAnim__ctor_m831662968 (ButtonAnim_t2351452291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonAnim::Start()
extern "C"  void ButtonAnim_Start_m4073768056 (ButtonAnim_t2351452291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonAnim::Update()
extern "C"  void ButtonAnim_Update_m1738610325 (ButtonAnim_t2351452291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
