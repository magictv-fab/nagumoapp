﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.globals.events.VideoDebugEvents/OnFloatEventHandler
struct OnFloatEventHandler_t1243657927;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void MagicTV.globals.events.VideoDebugEvents/OnFloatEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnFloatEventHandler__ctor_m1940940062 (OnFloatEventHandler_t1243657927 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.VideoDebugEvents/OnFloatEventHandler::Invoke(System.Single)
extern "C"  void OnFloatEventHandler_Invoke_m1236687283 (OnFloatEventHandler_t1243657927 * __this, float ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult MagicTV.globals.events.VideoDebugEvents/OnFloatEventHandler::BeginInvoke(System.Single,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnFloatEventHandler_BeginInvoke_m1466849486 (OnFloatEventHandler_t1243657927 * __this, float ___p0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.VideoDebugEvents/OnFloatEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnFloatEventHandler_EndInvoke_m147789358 (OnFloatEventHandler_t1243657927 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
