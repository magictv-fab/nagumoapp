﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CustomECPNManager
struct CustomECPNManager_t1031774400;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void CustomECPNManager::.ctor()
extern "C"  void CustomECPNManager__ctor_m1345344631 (CustomECPNManager_t1031774400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CustomECPNManager::RequestDeviceToken()
extern "C"  void CustomECPNManager_RequestDeviceToken_m3121310497 (CustomECPNManager_t1031774400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CustomECPNManager::RequestUnregisterDevice()
extern "C"  void CustomECPNManager_RequestUnregisterDevice_m2569527990 (CustomECPNManager_t1031774400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CustomECPNManager::GetDevToken()
extern "C"  String_t* CustomECPNManager_GetDevToken_m494469010 (CustomECPNManager_t1031774400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CustomECPNManager::Update()
extern "C"  void CustomECPNManager_Update_m482872694 (CustomECPNManager_t1031774400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CustomECPNManager::RegisterAndroidDevice(System.String)
extern "C"  void CustomECPNManager_RegisterAndroidDevice_m998029195 (CustomECPNManager_t1031774400 * __this, String_t* ___rID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CustomECPNManager::UnregisterDevice(System.String)
extern "C"  void CustomECPNManager_UnregisterDevice_m2193776707 (CustomECPNManager_t1031774400 * __this, String_t* ___rID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CustomECPNManager::RegisterIOSDevice()
extern "C"  void CustomECPNManager_RegisterIOSDevice_m1674902773 (CustomECPNManager_t1031774400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CustomECPNManager::StoreDeviceID(System.String,System.String)
extern "C"  Il2CppObject * CustomECPNManager_StoreDeviceID_m2619003175 (CustomECPNManager_t1031774400 * __this, String_t* ___rID0, String_t* ___os1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
