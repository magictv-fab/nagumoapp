﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3828207484MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3378368935(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2359627532 *, Dictionary_2_t1042304140 *, const MethodInfo*))Enumerator__ctor_m3444471362_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m537375962(__this, method) ((  Il2CppObject * (*) (Enumerator_t2359627532 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2371533151_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1357915310(__this, method) ((  void (*) (Enumerator_t2359627532 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3450565939_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1839702455(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t2359627532 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3609446076_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2047764022(__this, method) ((  Il2CppObject * (*) (Enumerator_t2359627532 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1907045115_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1854639944(__this, method) ((  Il2CppObject * (*) (Enumerator_t2359627532 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m4062723789_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::MoveNext()
#define Enumerator_MoveNext_m2901281946(__this, method) ((  bool (*) (Enumerator_t2359627532 *, const MethodInfo*))Enumerator_MoveNext_m3044872223_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::get_Current()
#define Enumerator_get_Current_m2542697814(__this, method) ((  KeyValuePair_2_t941084846  (*) (Enumerator_t2359627532 *, const MethodInfo*))Enumerator_get_Current_m4212335665_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2449537767(__this, method) ((  int32_t (*) (Enumerator_t2359627532 *, const MethodInfo*))Enumerator_get_CurrentKey_m3803229484_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m810303435(__this, method) ((  OnChangeAnToPerspectiveEventHandler_t2702236419 * (*) (Enumerator_t2359627532 *, const MethodInfo*))Enumerator_get_CurrentValue_m2354261840_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::Reset()
#define Enumerator_Reset_m147251577(__this, method) ((  void (*) (Enumerator_t2359627532 *, const MethodInfo*))Enumerator_Reset_m2959396244_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::VerifyState()
#define Enumerator_VerifyState_m1765765826(__this, method) ((  void (*) (Enumerator_t2359627532 *, const MethodInfo*))Enumerator_VerifyState_m4194606749_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3133748650(__this, method) ((  void (*) (Enumerator_t2359627532 *, const MethodInfo*))Enumerator_VerifyCurrent_m787666629_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::Dispose()
#define Enumerator_Dispose_m2298273801(__this, method) ((  void (*) (Enumerator_t2359627532 *, const MethodInfo*))Enumerator_Dispose_m3234869604_gshared)(__this, method)
