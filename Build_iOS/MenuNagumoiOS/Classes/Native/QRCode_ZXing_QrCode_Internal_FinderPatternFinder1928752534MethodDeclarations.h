﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.QrCode.Internal.FinderPatternFinder
struct FinderPatternFinder_t1928752534;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// ZXing.ResultPointCallback
struct ResultPointCallback_t207829946;
// ZXing.QrCode.Internal.FinderPatternInfo
struct FinderPatternInfo_t3964498398;
// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>
struct IDictionary_2_t759989846;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// ZXing.QrCode.Internal.FinderPattern[]
struct FinderPatternU5BU5D_t1610216241;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitMatrix1058711404.h"
#include "QRCode_ZXing_ResultPointCallback207829946.h"
#include "mscorlib_System_Nullable_1_gen81078199.h"

// System.Void ZXing.QrCode.Internal.FinderPatternFinder::.ctor(ZXing.Common.BitMatrix,ZXing.ResultPointCallback)
extern "C"  void FinderPatternFinder__ctor_m1615327007 (FinderPatternFinder_t1928752534 * __this, BitMatrix_t1058711404 * ___image0, ResultPointCallback_t207829946 * ___resultPointCallback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.QrCode.Internal.FinderPatternInfo ZXing.QrCode.Internal.FinderPatternFinder::find(System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  FinderPatternInfo_t3964498398 * FinderPatternFinder_find_m1819354744 (FinderPatternFinder_t1928752534 * __this, Il2CppObject* ___hints0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Single> ZXing.QrCode.Internal.FinderPatternFinder::centerFromEnd(System.Int32[],System.Int32)
extern "C"  Nullable_1_t81078199  FinderPatternFinder_centerFromEnd_m3294338365 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___stateCount0, int32_t ___end1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.QrCode.Internal.FinderPatternFinder::foundPatternCross(System.Int32[])
extern "C"  bool FinderPatternFinder_foundPatternCross_m1260558793 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___stateCount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.QrCode.Internal.FinderPatternFinder::get_CrossCheckStateCount()
extern "C"  Int32U5BU5D_t3230847821* FinderPatternFinder_get_CrossCheckStateCount_m3306748785 (FinderPatternFinder_t1928752534 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.QrCode.Internal.FinderPatternFinder::crossCheckDiagonal(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  bool FinderPatternFinder_crossCheckDiagonal_m1640046447 (FinderPatternFinder_t1928752534 * __this, int32_t ___startI0, int32_t ___centerJ1, int32_t ___maxCount2, int32_t ___originalStateCountTotal3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Single> ZXing.QrCode.Internal.FinderPatternFinder::crossCheckVertical(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  Nullable_1_t81078199  FinderPatternFinder_crossCheckVertical_m3374776085 (FinderPatternFinder_t1928752534 * __this, int32_t ___startI0, int32_t ___centerJ1, int32_t ___maxCount2, int32_t ___originalStateCountTotal3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Single> ZXing.QrCode.Internal.FinderPatternFinder::crossCheckHorizontal(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  Nullable_1_t81078199  FinderPatternFinder_crossCheckHorizontal_m3484576167 (FinderPatternFinder_t1928752534 * __this, int32_t ___startJ0, int32_t ___centerI1, int32_t ___maxCount2, int32_t ___originalStateCountTotal3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.QrCode.Internal.FinderPatternFinder::handlePossibleCenter(System.Int32[],System.Int32,System.Int32,System.Boolean)
extern "C"  bool FinderPatternFinder_handlePossibleCenter_m4255472550 (FinderPatternFinder_t1928752534 * __this, Int32U5BU5D_t3230847821* ___stateCount0, int32_t ___i1, int32_t ___j2, bool ___pureBarcode3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.QrCode.Internal.FinderPatternFinder::findRowSkip()
extern "C"  int32_t FinderPatternFinder_findRowSkip_m2856205698 (FinderPatternFinder_t1928752534 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.QrCode.Internal.FinderPatternFinder::haveMultiplyConfirmedCenters()
extern "C"  bool FinderPatternFinder_haveMultiplyConfirmedCenters_m3211658917 (FinderPatternFinder_t1928752534 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.QrCode.Internal.FinderPattern[] ZXing.QrCode.Internal.FinderPatternFinder::selectBestPatterns()
extern "C"  FinderPatternU5BU5D_t1610216241* FinderPatternFinder_selectBestPatterns_m3219087951 (FinderPatternFinder_t1928752534 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
