﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "QRCode_ZXing_LuminanceSource1231523093.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.BaseLuminanceSource
struct  BaseLuminanceSource_t803260164  : public LuminanceSource_t1231523093
{
public:
	// System.Byte[] ZXing.BaseLuminanceSource::luminances
	ByteU5BU5D_t4260760469* ___luminances_2;

public:
	inline static int32_t get_offset_of_luminances_2() { return static_cast<int32_t>(offsetof(BaseLuminanceSource_t803260164, ___luminances_2)); }
	inline ByteU5BU5D_t4260760469* get_luminances_2() const { return ___luminances_2; }
	inline ByteU5BU5D_t4260760469** get_address_of_luminances_2() { return &___luminances_2; }
	inline void set_luminances_2(ByteU5BU5D_t4260760469* value)
	{
		___luminances_2 = value;
		Il2CppCodeGenWriteBarrier(&___luminances_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
