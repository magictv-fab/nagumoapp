﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.abstracts.ToggleOnOffAbstract
struct ToggleOnOffAbstract_t832893812;

#include "AssemblyU2DCSharp_ARM_events_BaseDelayToggleEventA1686333410.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.events.DelayToggleEvent
struct  DelayToggleEvent_t90356527  : public BaseDelayToggleEventAbstract_t1686333410
{
public:
	// ARM.abstracts.ToggleOnOffAbstract ARM.events.DelayToggleEvent::toggle
	ToggleOnOffAbstract_t832893812 * ___toggle_17;

public:
	inline static int32_t get_offset_of_toggle_17() { return static_cast<int32_t>(offsetof(DelayToggleEvent_t90356527, ___toggle_17)); }
	inline ToggleOnOffAbstract_t832893812 * get_toggle_17() const { return ___toggle_17; }
	inline ToggleOnOffAbstract_t832893812 ** get_address_of_toggle_17() { return &___toggle_17; }
	inline void set_toggle_17(ToggleOnOffAbstract_t832893812 * value)
	{
		___toggle_17 = value;
		Il2CppCodeGenWriteBarrier(&___toggle_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
