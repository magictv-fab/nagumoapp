﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetMass
struct GetMass_t1738563448;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetMass::.ctor()
extern "C"  void GetMass__ctor_m704194238 (GetMass_t1738563448 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMass::Reset()
extern "C"  void GetMass_Reset_m2645594475 (GetMass_t1738563448 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMass::OnEnter()
extern "C"  void GetMass_OnEnter_m1812525845 (GetMass_t1738563448 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMass::DoGetMass()
extern "C"  void GetMass_DoGetMass_m1944838907 (GetMass_t1738563448 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
