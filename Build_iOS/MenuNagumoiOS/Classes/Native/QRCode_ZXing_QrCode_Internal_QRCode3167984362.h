﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.QrCode.Internal.Mode
struct Mode_t2660577215;
// ZXing.QrCode.Internal.ErrorCorrectionLevel
struct ErrorCorrectionLevel_t1225927610;
// ZXing.QrCode.Internal.Version
struct Version_t1953509534;
// ZXing.QrCode.Internal.ByteMatrix
struct ByteMatrix_t2072255685;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.QRCode
struct  QRCode_t3167984362  : public Il2CppObject
{
public:
	// ZXing.QrCode.Internal.Mode ZXing.QrCode.Internal.QRCode::<Mode>k__BackingField
	Mode_t2660577215 * ___U3CModeU3Ek__BackingField_1;
	// ZXing.QrCode.Internal.ErrorCorrectionLevel ZXing.QrCode.Internal.QRCode::<ECLevel>k__BackingField
	ErrorCorrectionLevel_t1225927610 * ___U3CECLevelU3Ek__BackingField_2;
	// ZXing.QrCode.Internal.Version ZXing.QrCode.Internal.QRCode::<Version>k__BackingField
	Version_t1953509534 * ___U3CVersionU3Ek__BackingField_3;
	// System.Int32 ZXing.QrCode.Internal.QRCode::<MaskPattern>k__BackingField
	int32_t ___U3CMaskPatternU3Ek__BackingField_4;
	// ZXing.QrCode.Internal.ByteMatrix ZXing.QrCode.Internal.QRCode::<Matrix>k__BackingField
	ByteMatrix_t2072255685 * ___U3CMatrixU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CModeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(QRCode_t3167984362, ___U3CModeU3Ek__BackingField_1)); }
	inline Mode_t2660577215 * get_U3CModeU3Ek__BackingField_1() const { return ___U3CModeU3Ek__BackingField_1; }
	inline Mode_t2660577215 ** get_address_of_U3CModeU3Ek__BackingField_1() { return &___U3CModeU3Ek__BackingField_1; }
	inline void set_U3CModeU3Ek__BackingField_1(Mode_t2660577215 * value)
	{
		___U3CModeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CModeU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CECLevelU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(QRCode_t3167984362, ___U3CECLevelU3Ek__BackingField_2)); }
	inline ErrorCorrectionLevel_t1225927610 * get_U3CECLevelU3Ek__BackingField_2() const { return ___U3CECLevelU3Ek__BackingField_2; }
	inline ErrorCorrectionLevel_t1225927610 ** get_address_of_U3CECLevelU3Ek__BackingField_2() { return &___U3CECLevelU3Ek__BackingField_2; }
	inline void set_U3CECLevelU3Ek__BackingField_2(ErrorCorrectionLevel_t1225927610 * value)
	{
		___U3CECLevelU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CECLevelU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CVersionU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(QRCode_t3167984362, ___U3CVersionU3Ek__BackingField_3)); }
	inline Version_t1953509534 * get_U3CVersionU3Ek__BackingField_3() const { return ___U3CVersionU3Ek__BackingField_3; }
	inline Version_t1953509534 ** get_address_of_U3CVersionU3Ek__BackingField_3() { return &___U3CVersionU3Ek__BackingField_3; }
	inline void set_U3CVersionU3Ek__BackingField_3(Version_t1953509534 * value)
	{
		___U3CVersionU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CVersionU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CMaskPatternU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(QRCode_t3167984362, ___U3CMaskPatternU3Ek__BackingField_4)); }
	inline int32_t get_U3CMaskPatternU3Ek__BackingField_4() const { return ___U3CMaskPatternU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CMaskPatternU3Ek__BackingField_4() { return &___U3CMaskPatternU3Ek__BackingField_4; }
	inline void set_U3CMaskPatternU3Ek__BackingField_4(int32_t value)
	{
		___U3CMaskPatternU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CMatrixU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(QRCode_t3167984362, ___U3CMatrixU3Ek__BackingField_5)); }
	inline ByteMatrix_t2072255685 * get_U3CMatrixU3Ek__BackingField_5() const { return ___U3CMatrixU3Ek__BackingField_5; }
	inline ByteMatrix_t2072255685 ** get_address_of_U3CMatrixU3Ek__BackingField_5() { return &___U3CMatrixU3Ek__BackingField_5; }
	inline void set_U3CMatrixU3Ek__BackingField_5(ByteMatrix_t2072255685 * value)
	{
		___U3CMatrixU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CMatrixU3Ek__BackingField_5, value);
	}
};

struct QRCode_t3167984362_StaticFields
{
public:
	// System.Int32 ZXing.QrCode.Internal.QRCode::NUM_MASK_PATTERNS
	int32_t ___NUM_MASK_PATTERNS_0;

public:
	inline static int32_t get_offset_of_NUM_MASK_PATTERNS_0() { return static_cast<int32_t>(offsetof(QRCode_t3167984362_StaticFields, ___NUM_MASK_PATTERNS_0)); }
	inline int32_t get_NUM_MASK_PATTERNS_0() const { return ___NUM_MASK_PATTERNS_0; }
	inline int32_t* get_address_of_NUM_MASK_PATTERNS_0() { return &___NUM_MASK_PATTERNS_0; }
	inline void set_NUM_MASK_PATTERNS_0(int32_t value)
	{
		___NUM_MASK_PATTERNS_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
