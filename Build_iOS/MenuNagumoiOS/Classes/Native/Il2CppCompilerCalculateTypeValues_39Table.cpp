﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_DeviceFileInfo_U3CgetUncachedRev2060657264.h"
#include "AssemblyU2DCSharp_MagicTV_globals_DeviceFileInfoJs3027144048.h"
#include "AssemblyU2DCSharp_MagicTV_globals_DeviceFileInfoJs3904883079.h"
#include "AssemblyU2DCSharp_MagicTV_globals_DeviceFileInfoJso263912334.h"
#include "AssemblyU2DCSharp_MagicTV_globals_DeviceFileInfoJs2872216120.h"
#include "AssemblyU2DCSharp_MagicTV_globals_DeviceFileInfoJs3361971262.h"
#include "AssemblyU2DCSharp_MagicTV_globals_DeviceFileInfoJso261585927.h"
#include "AssemblyU2DCSharp_DeviceInfo2774317124.h"
#include "AssemblyU2DCSharp_FakeDeviceFileInfo2440618901.h"
#include "AssemblyU2DCSharp_FakeDeviceFileInfo_U3CgetBundles2310048435.h"
#include "AssemblyU2DCSharp_MagicTV_globals_InAppAnalytics2316734034.h"
#include "AssemblyU2DCSharp_MagicTV_globals_Perspective644553802.h"
#include "AssemblyU2DCSharp_MagicTV_globals_StateMachine367995870.h"
#include "AssemblyU2DCSharp_MagicTV_globals_AREvents1242196082.h"
#include "AssemblyU2DCSharp_MagicTV_globals_AREvents_OnARTog2139692132.h"
#include "AssemblyU2DCSharp_MagicTV_globals_AREvents_OnStrin3419556962.h"
#include "AssemblyU2DCSharp_MagicTV_globals_AREvents_OnARWan4073481420.h"
#include "AssemblyU2DCSharp_MagicTV_globals_events_AppRootEv2849877622.h"
#include "AssemblyU2DCSharp_MagicTV_globals_events_AppRootEve622442222.h"
#include "AssemblyU2DCSharp_MagicTV_globals_events_ContentEv2493264844.h"
#include "AssemblyU2DCSharp_MagicTV_globals_events_DeviceEve3457131993.h"
#include "AssemblyU2DCSharp_MagicTV_globals_events_DeviceEve1610687155.h"
#include "AssemblyU2DCSharp_MagicTV_globals_events_DeviceEve2272041080.h"
#include "AssemblyU2DCSharp_MagicTV_globals_events_ScreenEve2706497391.h"
#include "AssemblyU2DCSharp_MagicTV_globals_events_ScreenEve2512456021.h"
#include "AssemblyU2DCSharp_MagicTV_globals_events_ScreenEve2329146386.h"
#include "AssemblyU2DCSharp_MagicTV_globals_events_ScreenEven587466104.h"
#include "AssemblyU2DCSharp_MagicTV_globals_events_ScreenEve1587175024.h"
#include "AssemblyU2DCSharp_MagicTV_globals_events_ScreenEve2702236419.h"
#include "AssemblyU2DCSharp_MagicTV_globals_events_StateMagi1499601201.h"
#include "AssemblyU2DCSharp_MagicTV_globals_events_StateMagi4233189127.h"
#include "AssemblyU2DCSharp_MagicTV_globals_events_StateMagin630243546.h"
#include "AssemblyU2DCSharp_MagicTV_globals_events_UserEvents643990926.h"
#include "AssemblyU2DCSharp_MagicTV_globals_events_UserEvent3783866142.h"
#include "AssemblyU2DCSharp_MagicTV_globals_events_VideoDebug779548827.h"
#include "AssemblyU2DCSharp_MagicTV_globals_events_VideoDebu1243657927.h"
#include "AssemblyU2DCSharp_MagicTV_globals_events_VideoDebu1157926502.h"
#include "AssemblyU2DCSharp_MagicTV_globals_events_VideoDebu4203203482.h"
#include "AssemblyU2DCSharp_MagicTV_gui_CenterExitButton719705803.h"
#include "AssemblyU2DCSharp_MagicTV_gui_CenterExitButton_U3C1168321752.h"
#include "AssemblyU2DCSharp_MagicTV_gui_GUILoader3143521200.h"
#include "AssemblyU2DCSharp_MagicTV_gui_screens_HowToScreen4005946933.h"
#include "AssemblyU2DCSharp_MagicTV_gui_screens_InAppGUIScre1999655547.h"
#include "AssemblyU2DCSharp_MagicTV_abstracts_screens_Initia2241678589.h"
#include "AssemblyU2DCSharp_MagicTV_abstracts_screens_LoadSc4135486823.h"
#include "AssemblyU2DCSharp_MagicTV_abstracts_screens_LoadSc3455683822.h"
#include "AssemblyU2DCSharp_MagicTV_abstracts_screens_LoadSc1630568627.h"
#include "AssemblyU2DCSharp_MagicTV_in_apps_controllers_ARM31586749877.h"
#include "AssemblyU2DCSharp_ClickableBehaviour143571995.h"
#include "AssemblyU2DCSharp_MagicTV_in_apps_GarbageCollector1305589272.h"
#include "AssemblyU2DCSharp_MagicTV_in_apps_GarbageCollector_234284368.h"
#include "AssemblyU2DCSharp_MagicTV_in_apps_GarbageItemVO1335202303.h"
#include "AssemblyU2DCSharp_MagicTV_in_apps_InAppAssetBundle2029364784.h"
#include "AssemblyU2DCSharp_MagicTV_in_apps_InAppAudioPresent609990708.h"
#include "AssemblyU2DCSharp_InApp_InAppEventDispatcher3615143467.h"
#include "AssemblyU2DCSharp_InApp_InAppEventDispatcher_Event1614635846.h"
#include "AssemblyU2DCSharp_InApp_InAppEventDispatcher_InApp1910714196.h"
#include "AssemblyU2DCSharp_InApp_InAppEventDispatcher_InApp3474551315.h"
#include "AssemblyU2DCSharp_MagicTV_in_apps_InAppEventsChann1397713486.h"
#include "AssemblyU2DCSharp_MagicTV_in_apps_InAppExemplo1763107804.h"
#include "AssemblyU2DCSharp_MagicTV_in_apps_InAppImagePresen2058331705.h"
#include "AssemblyU2DCSharp_MagicTV_in_apps_InAppManager3917644817.h"
#include "AssemblyU2DCSharp_MagicTV_in_apps_InAppOnDemandDown507396607.h"
#include "AssemblyU2DCSharp_MagicTV_in_apps_InAppProgressInf2775900725.h"
#include "AssemblyU2DCSharp_MagicTV_in_apps_InAppVuforiaVide3711261773.h"
#include "AssemblyU2DCSharp_InApp_DirectorMeta2276429143.h"
#include "AssemblyU2DCSharp_InApp_InAppActorAbstract1901198337.h"
#include "AssemblyU2DCSharp_InApp_DirectorActions480721875.h"
#include "AssemblyU2DCSharp_InApp_InAppDirector1593455082.h"
#include "AssemblyU2DCSharp_InApp_SimpleEvents_BussolaCamera1897102310.h"
#include "AssemblyU2DCSharp_InApp_SimpleEvents_CameraDistanc2003187004.h"
#include "AssemblyU2DCSharp_ConfigurableListenerEventAbstract672359087.h"
#include "AssemblyU2DCSharp_InApp_SimpleEvents_FixedCameraBe4278156937.h"
#include "AssemblyU2DCSharp_InApp_SimpleEvents_GenericConfig3386406039.h"
#include "AssemblyU2DCSharp_InApp_SimpleEvents_GenericOnOff3142219673.h"
#include "AssemblyU2DCSharp_GravityConfig4085554960.h"
#include "AssemblyU2DCSharp_RenderSettiongsConfig1664940846.h"
#include "AssemblyU2DCSharp_InApp_SimpleEvents_TrackingTrans3834212341.h"
#include "AssemblyU2DCSharp_InApp_SimpleEvents_ZoomConfig3244369237.h"
#include "AssemblyU2DCSharp_LitJson_JsonType1715515030.h"
#include "AssemblyU2DCSharp_LitJson_JsonData1715015430.h"
#include "AssemblyU2DCSharp_LitJson_OrderedDictionaryEnumera3526912157.h"
#include "AssemblyU2DCSharp_LitJson_JsonException3617621405.h"
#include "AssemblyU2DCSharp_LitJson_PropertyMetadata4066634616.h"
#include "AssemblyU2DCSharp_LitJson_ArrayMetadata4058342910.h"
#include "AssemblyU2DCSharp_LitJson_ObjectMetadata2009294498.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper863513565.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper_U3CReadValueU1554182191.h"
#include "AssemblyU2DCSharp_JsonProperty2248022781.h"
#include "AssemblyU2DCSharp_JsonPropertyNotSerializable1551128245.h"
#include "AssemblyU2DCSharp_LitJson_JsonToken621800391.h"
#include "AssemblyU2DCSharp_LitJson_JsonReader1009895007.h"
#include "AssemblyU2DCSharp_LitJson_Condition853519089.h"
#include "AssemblyU2DCSharp_LitJson_WriterContext3060158226.h"
#include "AssemblyU2DCSharp_LitJson_JsonWriter1165300239.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3900 = { sizeof (U3CgetUncachedRevisionU3Ec__AnonStorey9A_t2060657264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3900[1] = 
{
	U3CgetUncachedRevisionU3Ec__AnonStorey9A_t2060657264::get_offset_of_bx_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3901 = { sizeof (DeviceFileInfoJson_t3027144048), -1, sizeof(DeviceFileInfoJson_t3027144048_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3901[22] = 
{
	DeviceFileInfoJson_t3027144048::get_offset_of__localFolderOfJson_6(),
	DeviceFileInfoJson_t3027144048::get_offset_of__fileName_7(),
	DeviceFileInfoJson_t3027144048::get_offset_of__indexedBundles_8(),
	DeviceFileInfoJson_t3027144048::get_offset_of__indexedBundlesArray_9(),
	DeviceFileInfoJson_t3027144048::get_offset_of__currentRevisionResult_10(),
	DeviceFileInfoJson_t3027144048::get_offset_of__prepareAllreadyCalled_11(),
	DeviceFileInfoJson_t3027144048::get_offset_of__urlsToDelete_12(),
	DeviceFileInfoJson_t3027144048_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_13(),
	DeviceFileInfoJson_t3027144048_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_14(),
	DeviceFileInfoJson_t3027144048_StaticFields::get_offset_of_U3CU3Ef__amU24cache9_15(),
	DeviceFileInfoJson_t3027144048_StaticFields::get_offset_of_U3CU3Ef__amU24cacheA_16(),
	DeviceFileInfoJson_t3027144048_StaticFields::get_offset_of_U3CU3Ef__amU24cacheB_17(),
	DeviceFileInfoJson_t3027144048_StaticFields::get_offset_of_U3CU3Ef__amU24cacheC_18(),
	DeviceFileInfoJson_t3027144048_StaticFields::get_offset_of_U3CU3Ef__amU24cacheD_19(),
	DeviceFileInfoJson_t3027144048_StaticFields::get_offset_of_U3CU3Ef__amU24cacheE_20(),
	DeviceFileInfoJson_t3027144048_StaticFields::get_offset_of_U3CU3Ef__amU24cacheF_21(),
	DeviceFileInfoJson_t3027144048_StaticFields::get_offset_of_U3CU3Ef__amU24cache10_22(),
	DeviceFileInfoJson_t3027144048_StaticFields::get_offset_of_U3CU3Ef__amU24cache11_23(),
	DeviceFileInfoJson_t3027144048_StaticFields::get_offset_of_U3CU3Ef__amU24cache12_24(),
	DeviceFileInfoJson_t3027144048_StaticFields::get_offset_of_U3CU3Ef__amU24cache13_25(),
	DeviceFileInfoJson_t3027144048_StaticFields::get_offset_of_U3CU3Ef__amU24cache14_26(),
	DeviceFileInfoJson_t3027144048_StaticFields::get_offset_of_U3CU3Ef__amU24cache15_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3902 = { sizeof (U3CDoReadFileU3Ec__Iterator38_t3904883079), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3902[5] = 
{
	U3CDoReadFileU3Ec__Iterator38_t3904883079::get_offset_of_www_0(),
	U3CDoReadFileU3Ec__Iterator38_t3904883079::get_offset_of_U24PC_1(),
	U3CDoReadFileU3Ec__Iterator38_t3904883079::get_offset_of_U24current_2(),
	U3CDoReadFileU3Ec__Iterator38_t3904883079::get_offset_of_U3CU24U3Ewww_3(),
	U3CDoReadFileU3Ec__Iterator38_t3904883079::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3903 = { sizeof (U3CpopulateIndexedBundlesU3Ec__AnonStorey9B_t263912334), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3903[2] = 
{
	U3CpopulateIndexedBundlesU3Ec__AnonStorey9B_t263912334::get_offset_of_revision_0(),
	U3CpopulateIndexedBundlesU3Ec__AnonStorey9B_t263912334::get_offset_of_U3CU3Ef__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3904 = { sizeof (U3CpopulateIndexedBundlesU3Ec__AnonStorey9C_t2872216120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3904[2] = 
{
	U3CpopulateIndexedBundlesU3Ec__AnonStorey9C_t2872216120::get_offset_of_t_0(),
	U3CpopulateIndexedBundlesU3Ec__AnonStorey9C_t2872216120::get_offset_of_U3CU3Ef__refU24155_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3905 = { sizeof (U3CfilterUrlFromIdU3Ec__AnonStorey9D_t3361971262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3905[1] = 
{
	U3CfilterUrlFromIdU3Ec__AnonStorey9D_t3361971262::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3906 = { sizeof (U3CgetFileByIdU3Ec__AnonStorey9E_t261585927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3906[1] = 
{
	U3CgetFileByIdU3Ec__AnonStorey9E_t261585927::get_offset_of_bundle_file_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3907 = { sizeof (DeviceInfo_t2774317124), -1, sizeof(DeviceInfo_t2774317124_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3907[5] = 
{
	0,
	0,
	DeviceInfo_t2774317124_StaticFields::get_offset_of_Instance_6(),
	DeviceInfo_t2774317124::get_offset_of_lastPing_7(),
	DeviceInfo_t2774317124_StaticFields::get_offset_of__lastInfoVersionControlIdToSave_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3908 = { sizeof (FakeDeviceFileInfo_t2440618901), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3908[1] = 
{
	FakeDeviceFileInfo_t2440618901::get_offset_of__ResultRevisionVO_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3909 = { sizeof (U3CgetBundlesU3Ec__AnonStorey9F_t2310048435), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3909[1] = 
{
	U3CgetBundlesU3Ec__AnonStorey9F_t2310048435::get_offset_of_appContext_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3910 = { sizeof (InAppAnalytics_t2316734034), -1, sizeof(InAppAnalytics_t2316734034_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3910[4] = 
{
	InAppAnalytics_t2316734034_StaticFields::get_offset_of_instances_0(),
	InAppAnalytics_t2316734034::get_offset_of__appName_1(),
	InAppAnalytics_t2316734034::get_offset_of__Analytics_2(),
	InAppAnalytics_t2316734034::get_offset_of__lastTrackNameAnalytics_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3911 = { sizeof (Perspective_t644553802)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3911[6] = 
{
	Perspective_t644553802::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3912 = { sizeof (StateMachine_t367995870)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3912[4] = 
{
	StateMachine_t367995870::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3913 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3914 = { sizeof (AREvents_t1242196082), -1, sizeof(AREvents_t1242196082_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3914[6] = 
{
	AREvents_t1242196082::get_offset_of_onARToggle_2(),
	AREvents_t1242196082::get_offset_of_onWantToTurnOn_3(),
	AREvents_t1242196082::get_offset_of_onWantToTurnOff_4(),
	AREvents_t1242196082::get_offset_of_onTrackIn_5(),
	AREvents_t1242196082::get_offset_of_onTrackOut_6(),
	AREvents_t1242196082_StaticFields::get_offset_of__instance_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3915 = { sizeof (OnARToggleOnOff_t2139692132), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3916 = { sizeof (OnStringEvent_t3419556962), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3917 = { sizeof (OnARWantToDoSomething_t4073481420), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3918 = { sizeof (AppRootEvents_t2849877622), -1, sizeof(AppRootEvents_t2849877622_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3918[4] = 
{
	AppRootEvents_t2849877622::get_offset_of_onDownloadAllCalled_2(),
	AppRootEvents_t2849877622::get_offset_of_onRaiseStopPresentation_3(),
	AppRootEvents_t2849877622_StaticFields::get_offset_of__instance_4(),
	AppRootEvents_t2849877622::get_offset_of__channels_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3919 = { sizeof (OnVoidEventHandler_t622442222), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3920 = { sizeof (ContentEvents_t2493264844), -1, sizeof(ContentEvents_t2493264844_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3920[3] = 
{
	ContentEvents_t2493264844::get_offset_of_onUpdate_0(),
	ContentEvents_t2493264844::get_offset_of_onUpdateComplete_1(),
	ContentEvents_t2493264844_StaticFields::get_offset_of__instance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3921 = { sizeof (DeviceEvents_t3457131993), -1, sizeof(DeviceEvents_t3457131993_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3921[14] = 
{
	DeviceEvents_t3457131993::get_offset_of_doVibrate_2(),
	DeviceEvents_t3457131993::get_offset_of__onSendTokenToServer_3(),
	DeviceEvents_t3457131993::get_offset_of__onTokenSavedIntoServer_4(),
	DeviceEvents_t3457131993::get_offset_of_onPinRecieved_5(),
	DeviceEvents_t3457131993::get_offset_of_onReset_6(),
	DeviceEvents_t3457131993::get_offset_of_onClearCacheCompleteNothingToDelete_7(),
	DeviceEvents_t3457131993::get_offset_of_onClearCacheComplete_8(),
	DeviceEvents_t3457131993::get_offset_of_onClearCache_9(),
	DeviceEvents_t3457131993::get_offset_of_onWantToClearCache_10(),
	DeviceEvents_t3457131993::get_offset_of_onResetClicked_11(),
	DeviceEvents_t3457131993::get_offset_of_onResume_12(),
	DeviceEvents_t3457131993::get_offset_of_onPause_13(),
	DeviceEvents_t3457131993::get_offset_of_onAwake_14(),
	DeviceEvents_t3457131993_StaticFields::get_offset_of__instance_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3922 = { sizeof (OnSomething_t1610687155), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3923 = { sizeof (OnString_t2272041080), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3924 = { sizeof (ScreenEvents_t2706497391), -1, sizeof(ScreenEvents_t2706497391_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3924[19] = 
{
	ScreenEvents_t2706497391::get_offset_of_onAlertMessageClose_2(),
	ScreenEvents_t2706497391::get_offset_of_captionText_3(),
	ScreenEvents_t2706497391::get_offset_of_OnSetCaption_4(),
	ScreenEvents_t2706497391::get_offset_of_OnConfigCaption_5(),
	ScreenEvents_t2706497391::get_offset_of_onShowHelpPage_6(),
	ScreenEvents_t2706497391::get_offset_of_onChangeToPerspective_7(),
	ScreenEvents_t2706497391::get_offset_of_onChangeToPerspectiveAskDownload_8(),
	ScreenEvents_t2706497391::get_offset_of_onChangeToPerspectiveBlank_9(),
	ScreenEvents_t2706497391::get_offset_of_onChangeToPerspectiveProgressWithoutCancel_10(),
	ScreenEvents_t2706497391::get_offset_of_onChangeToPerspectiveProgressWithCancel_11(),
	ScreenEvents_t2706497391::get_offset_of_onChangeToPerspectiveProgressBarWithouCancel_12(),
	ScreenEvents_t2706497391::get_offset_of_eventsList_13(),
	ScreenEvents_t2706497391::get_offset_of_onPrintScreenCompleted_14(),
	ScreenEvents_t2706497391::get_offset_of_onRaiseShowCloseButton_15(),
	ScreenEvents_t2706497391::get_offset_of_onRaiseHideCloseButton_16(),
	ScreenEvents_t2706497391::get_offset_of_onPrintScreenCalled_17(),
	ScreenEvents_t2706497391::get_offset_of_onSetPINEventHandler_18(),
	ScreenEvents_t2706497391::get_offset_of_onGoBackEventHandler_19(),
	ScreenEvents_t2706497391_StaticFields::get_offset_of__instance_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3925 = { sizeof (OnVoidEventHandler_t2512456021), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3926 = { sizeof (OnStringEventHandler_t2329146386), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3927 = { sizeof (OnIntBoolEventHandler_t587466104), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3928 = { sizeof (OnChangeToPerspectiveEventHandler_t1587175024), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3929 = { sizeof (OnChangeAnToPerspectiveEventHandler_t2702236419), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3930 = { sizeof (StateMagineEvents_t1499601201), -1, sizeof(StateMagineEvents_t1499601201_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3930[6] = 
{
	StateMagineEvents_t1499601201::get_offset_of_onChangeToState_0(),
	StateMagineEvents_t1499601201::get_offset_of_onChangeToStateInit_1(),
	StateMagineEvents_t1499601201::get_offset_of_onChangeToStateUpdate_2(),
	StateMagineEvents_t1499601201::get_offset_of_onChangeToStateOpen_3(),
	StateMagineEvents_t1499601201::get_offset_of_eventsList_4(),
	StateMagineEvents_t1499601201_StaticFields::get_offset_of__instance_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3931 = { sizeof (OnChangeToStateEventHandler_t4233189127), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3932 = { sizeof (OnChangeToAnStateEventHandler_t630243546), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3933 = { sizeof (UserEvents_t643990926), -1, sizeof(UserEvents_t643990926_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3933[3] = 
{
	UserEvents_t643990926::get_offset_of_onConfirm_0(),
	UserEvents_t643990926::get_offset_of_onConfirmCalled_1(),
	UserEvents_t643990926_StaticFields::get_offset_of__instance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3934 = { sizeof (OnConfirmEventHandler_t3783866142), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3935 = { sizeof (VideoDebugEvents_t779548827), -1, sizeof(VideoDebugEvents_t779548827_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3935[10] = 
{
	VideoDebugEvents_t779548827_StaticFields::get_offset_of_event1_0(),
	VideoDebugEvents_t779548827_StaticFields::get_offset_of_stringEvent_1(),
	VideoDebugEvents_t779548827_StaticFields::get_offset_of_onSaveEvent_2(),
	VideoDebugEvents_t779548827_StaticFields::get_offset_of_event2_3(),
	VideoDebugEvents_t779548827_StaticFields::get_offset_of_event3_4(),
	VideoDebugEvents_t779548827_StaticFields::get_offset_of_event4_5(),
	VideoDebugEvents_t779548827_StaticFields::get_offset_of_change1_6(),
	VideoDebugEvents_t779548827_StaticFields::get_offset_of_change2_7(),
	VideoDebugEvents_t779548827_StaticFields::get_offset_of_change3_8(),
	VideoDebugEvents_t779548827_StaticFields::get_offset_of_change4_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3936 = { sizeof (OnFloatEventHandler_t1243657927), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3937 = { sizeof (OnStringEventHandler_t1157926502), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3938 = { sizeof (OnMetaEventHandler_t4203203482), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3939 = { sizeof (CenterExitButton_t719705803), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3939[3] = 
{
	0,
	CenterExitButton_t719705803::get_offset_of_buttonCenter_3(),
	CenterExitButton_t719705803::get_offset_of_buttonOriginExit_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3940 = { sizeof (U3CDelayU3Ec__Iterator39_t1168321752), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3940[3] = 
{
	U3CDelayU3Ec__Iterator39_t1168321752::get_offset_of_U24PC_0(),
	U3CDelayU3Ec__Iterator39_t1168321752::get_offset_of_U24current_1(),
	U3CDelayU3Ec__Iterator39_t1168321752::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3941 = { sizeof (GUILoader_t3143521200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3941[4] = 
{
	GUILoader_t3143521200::get_offset_of__loadScreen_4(),
	GUILoader_t3143521200::get_offset_of__initialSplashScreen_5(),
	GUILoader_t3143521200::get_offset_of__menuScreen_6(),
	GUILoader_t3143521200::get_offset_of_group_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3942 = { sizeof (HowToScreen_t4005946933), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3943 = { sizeof (InAppGUIScreen_t1999655547), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3944 = { sizeof (InitialSplashScreen_t2241678589), -1, sizeof(InitialSplashScreen_t2241678589_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3944[3] = 
{
	InitialSplashScreen_t2241678589::get_offset_of__currentPerspective_8(),
	InitialSplashScreen_t2241678589_StaticFields::get_offset_of_Instance_9(),
	InitialSplashScreen_t2241678589::get_offset_of__timeoutToHide_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3945 = { sizeof (LoadScreenBar_t4135486823), -1, sizeof(LoadScreenBar_t4135486823_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3945[3] = 
{
	LoadScreenBar_t4135486823::get_offset_of__currentPerspective_8(),
	LoadScreenBar_t4135486823_StaticFields::get_offset_of_Instance_9(),
	LoadScreenBar_t4135486823::get_offset_of__timeoutToHide_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3946 = { sizeof (LoadScreenCircle_t3455683822), -1, sizeof(LoadScreenCircle_t3455683822_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3946[3] = 
{
	LoadScreenCircle_t3455683822::get_offset_of__currentPerspective_8(),
	LoadScreenCircle_t3455683822_StaticFields::get_offset_of_Instance_9(),
	LoadScreenCircle_t3455683822::get_offset_of__timeoutToHide_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3947 = { sizeof (LoadScreenFake_t1630568627), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3947[5] = 
{
	LoadScreenFake_t1630568627::get_offset_of__active_8(),
	LoadScreenFake_t1630568627::get_offset_of__currentPerspective_9(),
	LoadScreenFake_t1630568627::get_offset_of__message_10(),
	LoadScreenFake_t1630568627::get_offset_of_totalInt_11(),
	LoadScreenFake_t1630568627::get_offset_of__showedTotal_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3948 = { sizeof (ARM3DSimpleController_t1586749877), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3949 = { sizeof (ClickableBehaviour_t143571995), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3949[4] = 
{
	ClickableBehaviour_t143571995::get_offset_of__externalLink_2(),
	ClickableBehaviour_t143571995::get_offset_of__unlockClickTimer_3(),
	ClickableBehaviour_t143571995::get_offset_of__hasClick_4(),
	ClickableBehaviour_t143571995::get_offset_of__clickLocked_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3950 = { sizeof (GarbageCollector_t1305589272), -1, sizeof(GarbageCollector_t1305589272_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3950[9] = 
{
	GarbageCollector_t1305589272::get_offset_of__presentations_2(),
	GarbageCollector_t1305589272::get_offset_of_tolerance_3(),
	GarbageCollector_t1305589272::get_offset_of_maxItens_4(),
	GarbageCollector_t1305589272::get_offset_of_maxMemoryUsage_5(),
	GarbageCollector_t1305589272::get_offset_of_debugTotalItens_6(),
	GarbageCollector_t1305589272::get_offset_of_debugFirstItemName_7(),
	GarbageCollector_t1305589272::get_offset_of_debugFirstItemTime_8(),
	GarbageCollector_t1305589272::get_offset_of_trashs_9(),
	GarbageCollector_t1305589272_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3951 = { sizeof (U3CgetGarbageItemU3Ec__AnonStoreyA0_t234284368), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3951[1] = 
{
	U3CgetGarbageItemU3Ec__AnonStoreyA0_t234284368::get_offset_of_trackingName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3952 = { sizeof (GarbageItemVO_t1335202303), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3952[4] = 
{
	GarbageItemVO_t1335202303::get_offset_of_presentation_0(),
	GarbageItemVO_t1335202303::get_offset_of_timerCount_1(),
	GarbageItemVO_t1335202303::get_offset_of_isTrash_2(),
	GarbageItemVO_t1335202303::get_offset_of_isDied_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3953 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3954 = { sizeof (InAppAssetBundlePresentation_t2029364784), -1, sizeof(InAppAssetBundlePresentation_t2029364784_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3954[16] = 
{
	InAppAssetBundlePresentation_t2029364784::get_offset_of__FSM_21(),
	InAppAssetBundlePresentation_t2029364784_StaticFields::get_offset_of__AssetBundleLib_22(),
	InAppAssetBundlePresentation_t2029364784::get_offset_of__requestModel3D_23(),
	InAppAssetBundlePresentation_t2029364784::get_offset_of__localFileSystemAssetPath_24(),
	InAppAssetBundlePresentation_t2029364784::get_offset_of__externalLink_25(),
	InAppAssetBundlePresentation_t2029364784::get_offset_of__delayToOpenLink_26(),
	InAppAssetBundlePresentation_t2029364784::get_offset_of__doPrepareAlreadyCalled_27(),
	InAppAssetBundlePresentation_t2029364784::get_offset_of__loadedModel3d_28(),
	InAppAssetBundlePresentation_t2029364784::get_offset_of__FSMIsReady_29(),
	InAppAssetBundlePresentation_t2029364784::get_offset_of__hasInternalApps_30(),
	InAppAssetBundlePresentation_t2029364784::get_offset_of__InternalAppsAreReady_31(),
	InAppAssetBundlePresentation_t2029364784::get_offset_of__clickGOInstanceID_32(),
	InAppAssetBundlePresentation_t2029364784::get_offset_of__hasClick_33(),
	InAppAssetBundlePresentation_t2029364784::get_offset_of__clickLocked_34(),
	InAppAssetBundlePresentation_t2029364784::get_offset_of_subInternalInApps_35(),
	InAppAssetBundlePresentation_t2029364784::get_offset_of_DebubInternalInApps_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3955 = { sizeof (InAppAudioPresentation_t609990708), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3955[9] = 
{
	0,
	InAppAudioPresentation_t609990708::get_offset_of__audioRequest_22(),
	InAppAudioPresentation_t609990708::get_offset_of__audioSource_23(),
	InAppAudioPresentation_t609990708::get_offset_of__audioComponentIsReady_24(),
	InAppAudioPresentation_t609990708::get_offset_of__audioUrlInfoVO_25(),
	InAppAudioPresentation_t609990708::get_offset_of__doPrepareAlreadyCalled_26(),
	InAppAudioPresentation_t609990708::get_offset_of_debugAudioUrl_27(),
	InAppAudioPresentation_t609990708::get_offset_of_DebugTimeAudio_28(),
	InAppAudioPresentation_t609990708::get_offset_of_DebugCurrentTimeAudio_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3956 = { sizeof (InAppEventDispatcher_t3615143467), -1, sizeof(InAppEventDispatcher_t3615143467_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3956[15] = 
{
	InAppEventDispatcher_t3615143467::get_offset_of_eventDictionary_2(),
	InAppEventDispatcher_t3615143467_StaticFields::get_offset_of__instance_3(),
	InAppEventDispatcher_t3615143467::get_offset_of_actionReferenceGIROSCOPE_INCLINATION_CONFIGURABLE_4(),
	InAppEventDispatcher_t3615143467::get_offset_of_actionReferenceFIXED_CAMERA_5(),
	InAppEventDispatcher_t3615143467::get_offset_of_actionReferenceDETACHED_FROM_TRACKING_6(),
	InAppEventDispatcher_t3615143467::get_offset_of_actionReferenceCAMERA_DISTANCE_CONFIG_7(),
	InAppEventDispatcher_t3615143467::get_offset_of_actionReferenceGENERIC_8(),
	InAppEventDispatcher_t3615143467::get_offset_of_actionReferenceFIXED_GROUP_OF_PRESENTATIONS_9(),
	InAppEventDispatcher_t3615143467::get_offset_of_actionReferenceLIGHT_10(),
	InAppEventDispatcher_t3615143467::get_offset_of_actionReferenceRESET_11(),
	InAppEventDispatcher_t3615143467::get_offset_of_actionReferenceLOCK_SCREEN_ORIENTATION_PORTRAIT_12(),
	InAppEventDispatcher_t3615143467::get_offset_of_actionReferenceLOCK_SCREEN_ORIENTATION_LANDSCAPE_13(),
	InAppEventDispatcher_t3615143467::get_offset_of_actionReferenceNOT_FOUND_14(),
	InAppEventDispatcher_t3615143467::get_offset_of_actionReferenceGIROSCOPE_INCLINATION_15(),
	InAppEventDispatcher_t3615143467_StaticFields::get_offset_of_U3CU3Ef__switchU24map3_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3957 = { sizeof (EventNames_t1614635846)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3957[13] = 
{
	EventNames_t1614635846::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3958 = { sizeof (InAppEventDispatcherWithString_t1910714196), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3959 = { sizeof (InAppEventDispatcherOnOff_t3474551315), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3960 = { sizeof (InAppEventsChannel_t1397713486), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3960[5] = 
{
	InAppEventsChannel_t1397713486::get_offset_of__floatValues_0(),
	InAppEventsChannel_t1397713486::get_offset_of__stringValues_1(),
	InAppEventsChannel_t1397713486::get_offset_of__intValues_2(),
	InAppEventsChannel_t1397713486::get_offset_of__voidEvents_3(),
	InAppEventsChannel_t1397713486::get_offset_of__genericEvents_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3961 = { sizeof (InAppExemplo_t1763107804), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3962 = { sizeof (InAppImagePresentation_t2058331705), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3962[10] = 
{
	0,
	InAppImagePresentation_t2058331705::get_offset_of__doPrepareAlreadyCalled_15(),
	InAppImagePresentation_t2058331705::get_offset_of__urlInfoVO_16(),
	InAppImagePresentation_t2058331705::get_offset_of_resetOnStop_17(),
	InAppImagePresentation_t2058331705::get_offset_of__componentIsReady_18(),
	InAppImagePresentation_t2058331705::get_offset_of__useCache_19(),
	InAppImagePresentation_t2058331705::get_offset_of_externalUrl_20(),
	InAppImagePresentation_t2058331705::get_offset_of__debugLoadNow_21(),
	InAppImagePresentation_t2058331705::get_offset_of__plane_22(),
	InAppImagePresentation_t2058331705::get_offset_of__fileRequest_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3963 = { sizeof (InAppManager_t3917644817), -1, sizeof(InAppManager_t3917644817_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3963[19] = 
{
	InAppManager_t3917644817_StaticFields::get_offset_of_lojaCode_8(),
	InAppManager_t3917644817::get_offset_of__chekStartVuforia_9(),
	InAppManager_t3917644817::get_offset_of_DebugTrackSimulate_10(),
	InAppManager_t3917644817::get_offset_of_DebugEventTrackInSimulate_11(),
	InAppManager_t3917644817::get_offset_of_DebugEventTrackOutSimulate_12(),
	InAppManager_t3917644817_StaticFields::get_offset_of__inAppMagicTVDemo_13(),
	InAppManager_t3917644817::get_offset_of_deviceFileInfo_14(),
	InAppManager_t3917644817::get_offset_of__inAppMemoryCache_15(),
	InAppManager_t3917644817::get_offset_of__inAppMemoryCacheByInAppsOpened_16(),
	InAppManager_t3917644817::get_offset_of_loader_17(),
	InAppManager_t3917644817::get_offset_of_luzes_18(),
	InAppManager_t3917644817::get_offset_of_inAppName_19(),
	InAppManager_t3917644817::get_offset_of_addInAppNow_20(),
	InAppManager_t3917644817::get_offset_of_InAppTypes_21(),
	InAppManager_t3917644817::get_offset_of__inAppTypesList_22(),
	InAppManager_t3917644817::get_offset_of__currentPresentation_23(),
	InAppManager_t3917644817::get_offset_of__currentAnalytics_24(),
	InAppManager_t3917644817::get_offset_of__lastAppName_25(),
	InAppManager_t3917644817::get_offset_of__appNameToRunNow_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3964 = { sizeof (InAppOnDemandDownloadManager_t507396607), -1, sizeof(InAppOnDemandDownloadManager_t507396607_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3964[3] = 
{
	InAppOnDemandDownloadManager_t507396607::get_offset_of__bundles_0(),
	InAppOnDemandDownloadManager_t507396607_StaticFields::get_offset_of_confirm_1(),
	InAppOnDemandDownloadManager_t507396607::get_offset_of__started_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3965 = { sizeof (InAppProgressInfo_t2775900725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3965[6] = 
{
	InAppProgressInfo_t2775900725::get_offset_of__isComplete_0(),
	InAppProgressInfo_t2775900725::get_offset_of__currentProgress_1(),
	InAppProgressInfo_t2775900725::get_offset_of_TotalBytesToDownload_2(),
	InAppProgressInfo_t2775900725::get_offset_of_onComplete_3(),
	InAppProgressInfo_t2775900725::get_offset_of_onError_4(),
	InAppProgressInfo_t2775900725::get_offset_of_onProgress_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3966 = { sizeof (InAppVuforiaVideoPresentation_t3711261773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3966[21] = 
{
	0,
	0,
	0,
	0,
	0,
	InAppVuforiaVideoPresentation_t3711261773::get_offset_of__originalScale_26(),
	InAppVuforiaVideoPresentation_t3711261773::get_offset_of__canPrepare_27(),
	InAppVuforiaVideoPresentation_t3711261773::get_offset_of__needPrepare_28(),
	InAppVuforiaVideoPresentation_t3711261773::get_offset_of__videoController_29(),
	InAppVuforiaVideoPresentation_t3711261773::get_offset_of_VideoPlaneRender_30(),
	InAppVuforiaVideoPresentation_t3711261773::get_offset_of__doPrepareAlreadyCalled_31(),
	InAppVuforiaVideoPresentation_t3711261773::get_offset_of__videoUrlInfoVO_32(),
	InAppVuforiaVideoPresentation_t3711261773::get_offset_of__videoComponentIsReady_33(),
	InAppVuforiaVideoPresentation_t3711261773::get_offset_of_SimpleView_34(),
	InAppVuforiaVideoPresentation_t3711261773::get_offset_of__isDisposed_35(),
	InAppVuforiaVideoPresentation_t3711261773::get_offset_of__loading_36(),
	InAppVuforiaVideoPresentation_t3711261773::get_offset_of_luz_37(),
	InAppVuforiaVideoPresentation_t3711261773::get_offset_of__hideOnStart_38(),
	InAppVuforiaVideoPresentation_t3711261773::get_offset_of__timer_39(),
	InAppVuforiaVideoPresentation_t3711261773::get_offset_of_material_name_40(),
	InAppVuforiaVideoPresentation_t3711261773::get_offset_of__pauseLock_41(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3967 = { sizeof (DirectorMeta_t2276429143), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3967[3] = 
{
	DirectorMeta_t2276429143::get_offset_of_BundleID_0(),
	DirectorMeta_t2276429143::get_offset_of_Action_1(),
	DirectorMeta_t2276429143::get_offset_of_Delay_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3968 = { sizeof (InAppActorAbstract_t1901198337), -1, sizeof(InAppActorAbstract_t1901198337_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3968[7] = 
{
	InAppActorAbstract_t1901198337_StaticFields::get_offset_of_centerExit_14(),
	InAppActorAbstract_t1901198337_StaticFields::get_offset_of_defaultOrientation_15(),
	InAppActorAbstract_t1901198337::get_offset_of__autoPlayOnRun_16(),
	InAppActorAbstract_t1901198337::get_offset_of__autoHideOnStop_17(),
	InAppActorAbstract_t1901198337::get_offset_of__resetOnStop_18(),
	InAppActorAbstract_t1901198337::get_offset_of__loop_19(),
	InAppActorAbstract_t1901198337_StaticFields::get_offset_of_U3CU3Ef__switchU24map2_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3969 = { sizeof (DirectorActions_t480721875)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3969[7] = 
{
	DirectorActions_t480721875::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3970 = { sizeof (InAppDirector_t1593455082), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3971 = { sizeof (BussolaCameraBehavior_t1897102310), -1, sizeof(BussolaCameraBehavior_t1897102310_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3971[5] = 
{
	BussolaCameraBehavior_t1897102310_StaticFields::get_offset_of__instance_2(),
	BussolaCameraBehavior_t1897102310::get_offset_of_objectToMove_3(),
	BussolaCameraBehavior_t1897102310::get_offset_of_DebugIsOn_4(),
	BussolaCameraBehavior_t1897102310::get_offset_of_screenButtonToDebug_5(),
	BussolaCameraBehavior_t1897102310::get_offset_of_filterToOn_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3972 = { sizeof (CameraDistanceConfig_t2003187004), -1, sizeof(CameraDistanceConfig_t2003187004_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3972[4] = 
{
	CameraDistanceConfig_t2003187004_StaticFields::get_offset_of__instance_2(),
	CameraDistanceConfig_t2003187004::get_offset_of_mainCamera_3(),
	CameraDistanceConfig_t2003187004::get_offset_of_cameraNear_4(),
	CameraDistanceConfig_t2003187004::get_offset_of_cameraFar_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3973 = { sizeof (ConfigurableListenerEventAbstract_t672359087), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3974 = { sizeof (FixedCameraBehavior_t4278156937), -1, sizeof(FixedCameraBehavior_t4278156937_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3974[12] = 
{
	FixedCameraBehavior_t4278156937_StaticFields::get_offset_of__instance_2(),
	FixedCameraBehavior_t4278156937::get_offset_of_fixedFilter_3(),
	FixedCameraBehavior_t4278156937::get_offset_of__presentationController_4(),
	FixedCameraBehavior_t4278156937::get_offset_of_cameraFixedPosition_5(),
	FixedCameraBehavior_t4278156937::get_offset_of_cameraFixedRotation_6(),
	FixedCameraBehavior_t4278156937::get_offset_of__isCameraFixed_7(),
	FixedCameraBehavior_t4278156937::get_offset_of__inicialCameraFixedPosition_8(),
	FixedCameraBehavior_t4278156937::get_offset_of__inicialCameraFixedRotation_9(),
	FixedCameraBehavior_t4278156937::get_offset_of__smoothModeFirstTime_10(),
	FixedCameraBehavior_t4278156937::get_offset_of__alwaysLock_11(),
	FixedCameraBehavior_t4278156937::get_offset_of_pluginSmoothOfSwing_12(),
	FixedCameraBehavior_t4278156937::get_offset_of__smoothMode_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3975 = { sizeof (GenericConfig_t3386406039), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3975[4] = 
{
	GenericConfig_t3386406039::get_offset_of_gameObjectsToFind_2(),
	GenericConfig_t3386406039::get_offset_of_configurables_3(),
	GenericConfig_t3386406039::get_offset_of__configurableComponents_4(),
	GenericConfig_t3386406039::get_offset_of_DebugConfigurableComponents_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3976 = { sizeof (GenericOnOff_t3142219673), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3976[4] = 
{
	GenericOnOff_t3142219673::get_offset_of_gameObjectsToFind_2(),
	GenericOnOff_t3142219673::get_offset_of_configurables_3(),
	GenericOnOff_t3142219673::get_offset_of__configurableComponents_4(),
	GenericOnOff_t3142219673::get_offset_of_DebugConfigurableComponents_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3977 = { sizeof (GravityConfig_t4085554960), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3978 = { sizeof (RenderSettiongsConfig_t1664940846), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3978[10] = 
{
	RenderSettiongsConfig_t1664940846::get_offset_of__initialAmbientIntensity_2(),
	RenderSettiongsConfig_t1664940846::get_offset_of__initialAmbientSkyColor_3(),
	RenderSettiongsConfig_t1664940846::get_offset_of__initialAmbientEquatorColor_4(),
	RenderSettiongsConfig_t1664940846::get_offset_of__initialAmbientLight_5(),
	RenderSettiongsConfig_t1664940846::get_offset_of__initialAmbientGroundColor_6(),
	RenderSettiongsConfig_t1664940846::get_offset_of__initialReflectionIntensity_7(),
	RenderSettiongsConfig_t1664940846::get_offset_of__initialReflectionBounces_8(),
	RenderSettiongsConfig_t1664940846::get_offset_of__initialHaloStrength_9(),
	RenderSettiongsConfig_t1664940846::get_offset_of_DebugMetadata_10(),
	RenderSettiongsConfig_t1664940846::get_offset_of_DebugSendNow_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3979 = { sizeof (TrackingTransformConfig_t3834212341), -1, sizeof(TrackingTransformConfig_t3834212341_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3979[8] = 
{
	TrackingTransformConfig_t3834212341_StaticFields::get_offset_of__instance_2(),
	TrackingTransformConfig_t3834212341::get_offset_of__initialPosition_3(),
	TrackingTransformConfig_t3834212341::get_offset_of__initialRotation_4(),
	TrackingTransformConfig_t3834212341::get_offset_of__initialScale_5(),
	TrackingTransformConfig_t3834212341::get_offset_of__position_6(),
	TrackingTransformConfig_t3834212341::get_offset_of__rotation_7(),
	TrackingTransformConfig_t3834212341::get_offset_of__scale_8(),
	TrackingTransformConfig_t3834212341::get_offset_of_trackingContainer_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3980 = { sizeof (ZoomConfig_t3244369237), -1, sizeof(ZoomConfig_t3244369237_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3980[15] = 
{
	ZoomConfig_t3244369237_StaticFields::get_offset_of__instance_2(),
	ZoomConfig_t3244369237::get_offset_of_DebugMode_3(),
	ZoomConfig_t3244369237::get_offset_of_cameraZoomFilter_4(),
	ZoomConfig_t3244369237::get_offset_of_percentZoom_5(),
	ZoomConfig_t3244369237::get_offset_of_scaleDeltaMinimum_6(),
	ZoomConfig_t3244369237::get_offset_of_scaleDeltaMaximum_7(),
	ZoomConfig_t3244369237::get_offset_of_deltaZoomRatio_8(),
	ZoomConfig_t3244369237::get_offset_of_zoomMax_9(),
	ZoomConfig_t3244369237::get_offset_of_zoomMin_10(),
	ZoomConfig_t3244369237::get_offset_of__initialPercentZoom_11(),
	ZoomConfig_t3244369237::get_offset_of__initialScaleDeltaMinimum_12(),
	ZoomConfig_t3244369237::get_offset_of__initialScaleDeltaMaximum_13(),
	ZoomConfig_t3244369237::get_offset_of__initialDeltaZoomRatio_14(),
	ZoomConfig_t3244369237::get_offset_of__initialZoomMax_15(),
	ZoomConfig_t3244369237::get_offset_of__initialZoomMin_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3981 = { sizeof (JsonType_t1715515030)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3981[9] = 
{
	JsonType_t1715515030::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3982 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3983 = { sizeof (JsonData_t1715015430), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3983[10] = 
{
	JsonData_t1715015430::get_offset_of_inst_array_0(),
	JsonData_t1715015430::get_offset_of_inst_boolean_1(),
	JsonData_t1715015430::get_offset_of_inst_double_2(),
	JsonData_t1715015430::get_offset_of_inst_int_3(),
	JsonData_t1715015430::get_offset_of_inst_long_4(),
	JsonData_t1715015430::get_offset_of_inst_object_5(),
	JsonData_t1715015430::get_offset_of_inst_string_6(),
	JsonData_t1715015430::get_offset_of_json_7(),
	JsonData_t1715015430::get_offset_of_type_8(),
	JsonData_t1715015430::get_offset_of_object_list_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3984 = { sizeof (OrderedDictionaryEnumerator_t3526912157), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3984[1] = 
{
	OrderedDictionaryEnumerator_t3526912157::get_offset_of_list_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3985 = { sizeof (JsonException_t3617621405), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3986 = { sizeof (PropertyMetadata_t4066634616)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3986[3] = 
{
	PropertyMetadata_t4066634616::get_offset_of_Info_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropertyMetadata_t4066634616::get_offset_of_IsField_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropertyMetadata_t4066634616::get_offset_of_Type_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3987 = { sizeof (ArrayMetadata_t4058342910)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3987[3] = 
{
	ArrayMetadata_t4058342910::get_offset_of_element_type_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ArrayMetadata_t4058342910::get_offset_of_is_array_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ArrayMetadata_t4058342910::get_offset_of_is_list_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3988 = { sizeof (ObjectMetadata_t2009294498)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3988[3] = 
{
	ObjectMetadata_t2009294498::get_offset_of_element_type_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObjectMetadata_t2009294498::get_offset_of_is_dictionary_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObjectMetadata_t2009294498::get_offset_of_properties_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3989 = { sizeof (JsonMapper_t863513565), -1, sizeof(JsonMapper_t863513565_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3989[49] = 
{
	0,
	JsonMapper_t863513565_StaticFields::get_offset_of_max_nesting_depth_1(),
	JsonMapper_t863513565_StaticFields::get_offset_of_datetime_format_2(),
	JsonMapper_t863513565_StaticFields::get_offset_of_base_exporters_table_3(),
	JsonMapper_t863513565_StaticFields::get_offset_of_custom_exporters_table_4(),
	JsonMapper_t863513565_StaticFields::get_offset_of_base_importers_table_5(),
	JsonMapper_t863513565_StaticFields::get_offset_of_custom_importers_table_6(),
	JsonMapper_t863513565_StaticFields::get_offset_of_array_metadata_7(),
	JsonMapper_t863513565_StaticFields::get_offset_of_array_metadata_lock_8(),
	JsonMapper_t863513565_StaticFields::get_offset_of_conv_ops_9(),
	JsonMapper_t863513565_StaticFields::get_offset_of_conv_ops_lock_10(),
	JsonMapper_t863513565_StaticFields::get_offset_of_object_metadata_11(),
	JsonMapper_t863513565_StaticFields::get_offset_of_object_metadata_lock_12(),
	JsonMapper_t863513565_StaticFields::get_offset_of_type_properties_13(),
	JsonMapper_t863513565_StaticFields::get_offset_of_type_properties_lock_14(),
	JsonMapper_t863513565_StaticFields::get_offset_of_static_writer_15(),
	JsonMapper_t863513565_StaticFields::get_offset_of_static_writer_lock_16(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache10_17(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache11_18(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache12_19(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache13_20(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache14_21(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache15_22(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache16_23(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache17_24(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache18_25(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache19_26(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache1A_27(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache1B_28(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache1C_29(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache1D_30(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache1E_31(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache1F_32(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache20_33(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache21_34(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache22_35(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache23_36(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache24_37(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache25_38(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache26_39(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache27_40(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache28_41(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache29_42(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache2A_43(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache2B_44(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache2C_45(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache2D_46(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache2E_47(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache2F_48(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3990 = { sizeof (U3CReadValueU3Ec__AnonStoreyA1_t1554182191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3990[1] = 
{
	U3CReadValueU3Ec__AnonStoreyA1_t1554182191::get_offset_of_property_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3991 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3991[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3992 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3992[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3993 = { sizeof (JsonProperty_t2248022781), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3993[1] = 
{
	JsonProperty_t2248022781::get_offset_of_PropertyName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3994 = { sizeof (JsonPropertyNotSerializable_t1551128245), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3995 = { sizeof (JsonToken_t621800391)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3995[13] = 
{
	JsonToken_t621800391::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3996 = { sizeof (JsonReader_t1009895007), -1, sizeof(JsonReader_t1009895007_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3996[14] = 
{
	JsonReader_t1009895007_StaticFields::get_offset_of_parse_table_0(),
	JsonReader_t1009895007::get_offset_of_automaton_stack_1(),
	JsonReader_t1009895007::get_offset_of_current_input_2(),
	JsonReader_t1009895007::get_offset_of_current_symbol_3(),
	JsonReader_t1009895007::get_offset_of_end_of_json_4(),
	JsonReader_t1009895007::get_offset_of_end_of_input_5(),
	JsonReader_t1009895007::get_offset_of_lexer_6(),
	JsonReader_t1009895007::get_offset_of_parser_in_string_7(),
	JsonReader_t1009895007::get_offset_of_parser_return_8(),
	JsonReader_t1009895007::get_offset_of_read_started_9(),
	JsonReader_t1009895007::get_offset_of_reader_10(),
	JsonReader_t1009895007::get_offset_of_reader_is_owned_11(),
	JsonReader_t1009895007::get_offset_of_token_value_12(),
	JsonReader_t1009895007::get_offset_of_token_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3997 = { sizeof (Condition_t853519089)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3997[6] = 
{
	Condition_t853519089::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3998 = { sizeof (WriterContext_t3060158226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3998[5] = 
{
	WriterContext_t3060158226::get_offset_of_Count_0(),
	WriterContext_t3060158226::get_offset_of_InArray_1(),
	WriterContext_t3060158226::get_offset_of_InObject_2(),
	WriterContext_t3060158226::get_offset_of_ExpectingValue_3(),
	WriterContext_t3060158226::get_offset_of_Padding_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3999 = { sizeof (JsonWriter_t1165300239), -1, sizeof(JsonWriter_t1165300239_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3999[11] = 
{
	JsonWriter_t1165300239_StaticFields::get_offset_of_number_format_0(),
	JsonWriter_t1165300239::get_offset_of_context_1(),
	JsonWriter_t1165300239::get_offset_of_ctx_stack_2(),
	JsonWriter_t1165300239::get_offset_of_has_reached_end_3(),
	JsonWriter_t1165300239::get_offset_of_hex_seq_4(),
	JsonWriter_t1165300239::get_offset_of_indentation_5(),
	JsonWriter_t1165300239::get_offset_of_indent_value_6(),
	JsonWriter_t1165300239::get_offset_of_inst_string_builder_7(),
	JsonWriter_t1165300239::get_offset_of_pretty_print_8(),
	JsonWriter_t1165300239::get_offset_of_validate_9(),
	JsonWriter_t1165300239::get_offset_of_writer_10(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
