﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AppManager
struct AppManager_t3961229932;
// SampleInitErrorHandler
struct SampleInitErrorHandler_t2792208188;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneViewManager
struct  SceneViewManager_t413752636  : public MonoBehaviour_t667441552
{
public:
	// AppManager SceneViewManager::mAppManager
	AppManager_t3961229932 * ___mAppManager_2;
	// SampleInitErrorHandler SceneViewManager::mPopUpMsg
	SampleInitErrorHandler_t2792208188 * ___mPopUpMsg_3;
	// System.Boolean SceneViewManager::mErrorOccurred
	bool ___mErrorOccurred_4;

public:
	inline static int32_t get_offset_of_mAppManager_2() { return static_cast<int32_t>(offsetof(SceneViewManager_t413752636, ___mAppManager_2)); }
	inline AppManager_t3961229932 * get_mAppManager_2() const { return ___mAppManager_2; }
	inline AppManager_t3961229932 ** get_address_of_mAppManager_2() { return &___mAppManager_2; }
	inline void set_mAppManager_2(AppManager_t3961229932 * value)
	{
		___mAppManager_2 = value;
		Il2CppCodeGenWriteBarrier(&___mAppManager_2, value);
	}

	inline static int32_t get_offset_of_mPopUpMsg_3() { return static_cast<int32_t>(offsetof(SceneViewManager_t413752636, ___mPopUpMsg_3)); }
	inline SampleInitErrorHandler_t2792208188 * get_mPopUpMsg_3() const { return ___mPopUpMsg_3; }
	inline SampleInitErrorHandler_t2792208188 ** get_address_of_mPopUpMsg_3() { return &___mPopUpMsg_3; }
	inline void set_mPopUpMsg_3(SampleInitErrorHandler_t2792208188 * value)
	{
		___mPopUpMsg_3 = value;
		Il2CppCodeGenWriteBarrier(&___mPopUpMsg_3, value);
	}

	inline static int32_t get_offset_of_mErrorOccurred_4() { return static_cast<int32_t>(offsetof(SceneViewManager_t413752636, ___mErrorOccurred_4)); }
	inline bool get_mErrorOccurred_4() const { return ___mErrorOccurred_4; }
	inline bool* get_address_of_mErrorOccurred_4() { return &___mErrorOccurred_4; }
	inline void set_mErrorOccurred_4(bool value)
	{
		___mErrorOccurred_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
