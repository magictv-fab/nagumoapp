﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IO.Stream
struct Stream_t1561764144;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "mscorlib_System_IO_Stream1561764144.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.LZW.LzwInputStream
struct  LzwInputStream_t2878829311  : public Stream_t1561764144
{
public:
	// System.IO.Stream ICSharpCode.SharpZipLib.LZW.LzwInputStream::baseInputStream
	Stream_t1561764144 * ___baseInputStream_5;
	// System.Boolean ICSharpCode.SharpZipLib.LZW.LzwInputStream::isStreamOwner
	bool ___isStreamOwner_6;
	// System.Boolean ICSharpCode.SharpZipLib.LZW.LzwInputStream::isClosed
	bool ___isClosed_7;
	// System.Byte[] ICSharpCode.SharpZipLib.LZW.LzwInputStream::one
	ByteU5BU5D_t4260760469* ___one_8;
	// System.Boolean ICSharpCode.SharpZipLib.LZW.LzwInputStream::headerParsed
	bool ___headerParsed_9;
	// System.Int32[] ICSharpCode.SharpZipLib.LZW.LzwInputStream::tabPrefix
	Int32U5BU5D_t3230847821* ___tabPrefix_10;
	// System.Byte[] ICSharpCode.SharpZipLib.LZW.LzwInputStream::tabSuffix
	ByteU5BU5D_t4260760469* ___tabSuffix_11;
	// System.Int32[] ICSharpCode.SharpZipLib.LZW.LzwInputStream::zeros
	Int32U5BU5D_t3230847821* ___zeros_12;
	// System.Byte[] ICSharpCode.SharpZipLib.LZW.LzwInputStream::stack
	ByteU5BU5D_t4260760469* ___stack_13;
	// System.Boolean ICSharpCode.SharpZipLib.LZW.LzwInputStream::blockMode
	bool ___blockMode_14;
	// System.Int32 ICSharpCode.SharpZipLib.LZW.LzwInputStream::nBits
	int32_t ___nBits_15;
	// System.Int32 ICSharpCode.SharpZipLib.LZW.LzwInputStream::maxBits
	int32_t ___maxBits_16;
	// System.Int32 ICSharpCode.SharpZipLib.LZW.LzwInputStream::maxMaxCode
	int32_t ___maxMaxCode_17;
	// System.Int32 ICSharpCode.SharpZipLib.LZW.LzwInputStream::maxCode
	int32_t ___maxCode_18;
	// System.Int32 ICSharpCode.SharpZipLib.LZW.LzwInputStream::bitMask
	int32_t ___bitMask_19;
	// System.Int32 ICSharpCode.SharpZipLib.LZW.LzwInputStream::oldCode
	int32_t ___oldCode_20;
	// System.Byte ICSharpCode.SharpZipLib.LZW.LzwInputStream::finChar
	uint8_t ___finChar_21;
	// System.Int32 ICSharpCode.SharpZipLib.LZW.LzwInputStream::stackP
	int32_t ___stackP_22;
	// System.Int32 ICSharpCode.SharpZipLib.LZW.LzwInputStream::freeEnt
	int32_t ___freeEnt_23;
	// System.Byte[] ICSharpCode.SharpZipLib.LZW.LzwInputStream::data
	ByteU5BU5D_t4260760469* ___data_24;
	// System.Int32 ICSharpCode.SharpZipLib.LZW.LzwInputStream::bitPos
	int32_t ___bitPos_25;
	// System.Int32 ICSharpCode.SharpZipLib.LZW.LzwInputStream::end
	int32_t ___end_26;
	// System.Int32 ICSharpCode.SharpZipLib.LZW.LzwInputStream::got
	int32_t ___got_27;
	// System.Boolean ICSharpCode.SharpZipLib.LZW.LzwInputStream::eof
	bool ___eof_28;

public:
	inline static int32_t get_offset_of_baseInputStream_5() { return static_cast<int32_t>(offsetof(LzwInputStream_t2878829311, ___baseInputStream_5)); }
	inline Stream_t1561764144 * get_baseInputStream_5() const { return ___baseInputStream_5; }
	inline Stream_t1561764144 ** get_address_of_baseInputStream_5() { return &___baseInputStream_5; }
	inline void set_baseInputStream_5(Stream_t1561764144 * value)
	{
		___baseInputStream_5 = value;
		Il2CppCodeGenWriteBarrier(&___baseInputStream_5, value);
	}

	inline static int32_t get_offset_of_isStreamOwner_6() { return static_cast<int32_t>(offsetof(LzwInputStream_t2878829311, ___isStreamOwner_6)); }
	inline bool get_isStreamOwner_6() const { return ___isStreamOwner_6; }
	inline bool* get_address_of_isStreamOwner_6() { return &___isStreamOwner_6; }
	inline void set_isStreamOwner_6(bool value)
	{
		___isStreamOwner_6 = value;
	}

	inline static int32_t get_offset_of_isClosed_7() { return static_cast<int32_t>(offsetof(LzwInputStream_t2878829311, ___isClosed_7)); }
	inline bool get_isClosed_7() const { return ___isClosed_7; }
	inline bool* get_address_of_isClosed_7() { return &___isClosed_7; }
	inline void set_isClosed_7(bool value)
	{
		___isClosed_7 = value;
	}

	inline static int32_t get_offset_of_one_8() { return static_cast<int32_t>(offsetof(LzwInputStream_t2878829311, ___one_8)); }
	inline ByteU5BU5D_t4260760469* get_one_8() const { return ___one_8; }
	inline ByteU5BU5D_t4260760469** get_address_of_one_8() { return &___one_8; }
	inline void set_one_8(ByteU5BU5D_t4260760469* value)
	{
		___one_8 = value;
		Il2CppCodeGenWriteBarrier(&___one_8, value);
	}

	inline static int32_t get_offset_of_headerParsed_9() { return static_cast<int32_t>(offsetof(LzwInputStream_t2878829311, ___headerParsed_9)); }
	inline bool get_headerParsed_9() const { return ___headerParsed_9; }
	inline bool* get_address_of_headerParsed_9() { return &___headerParsed_9; }
	inline void set_headerParsed_9(bool value)
	{
		___headerParsed_9 = value;
	}

	inline static int32_t get_offset_of_tabPrefix_10() { return static_cast<int32_t>(offsetof(LzwInputStream_t2878829311, ___tabPrefix_10)); }
	inline Int32U5BU5D_t3230847821* get_tabPrefix_10() const { return ___tabPrefix_10; }
	inline Int32U5BU5D_t3230847821** get_address_of_tabPrefix_10() { return &___tabPrefix_10; }
	inline void set_tabPrefix_10(Int32U5BU5D_t3230847821* value)
	{
		___tabPrefix_10 = value;
		Il2CppCodeGenWriteBarrier(&___tabPrefix_10, value);
	}

	inline static int32_t get_offset_of_tabSuffix_11() { return static_cast<int32_t>(offsetof(LzwInputStream_t2878829311, ___tabSuffix_11)); }
	inline ByteU5BU5D_t4260760469* get_tabSuffix_11() const { return ___tabSuffix_11; }
	inline ByteU5BU5D_t4260760469** get_address_of_tabSuffix_11() { return &___tabSuffix_11; }
	inline void set_tabSuffix_11(ByteU5BU5D_t4260760469* value)
	{
		___tabSuffix_11 = value;
		Il2CppCodeGenWriteBarrier(&___tabSuffix_11, value);
	}

	inline static int32_t get_offset_of_zeros_12() { return static_cast<int32_t>(offsetof(LzwInputStream_t2878829311, ___zeros_12)); }
	inline Int32U5BU5D_t3230847821* get_zeros_12() const { return ___zeros_12; }
	inline Int32U5BU5D_t3230847821** get_address_of_zeros_12() { return &___zeros_12; }
	inline void set_zeros_12(Int32U5BU5D_t3230847821* value)
	{
		___zeros_12 = value;
		Il2CppCodeGenWriteBarrier(&___zeros_12, value);
	}

	inline static int32_t get_offset_of_stack_13() { return static_cast<int32_t>(offsetof(LzwInputStream_t2878829311, ___stack_13)); }
	inline ByteU5BU5D_t4260760469* get_stack_13() const { return ___stack_13; }
	inline ByteU5BU5D_t4260760469** get_address_of_stack_13() { return &___stack_13; }
	inline void set_stack_13(ByteU5BU5D_t4260760469* value)
	{
		___stack_13 = value;
		Il2CppCodeGenWriteBarrier(&___stack_13, value);
	}

	inline static int32_t get_offset_of_blockMode_14() { return static_cast<int32_t>(offsetof(LzwInputStream_t2878829311, ___blockMode_14)); }
	inline bool get_blockMode_14() const { return ___blockMode_14; }
	inline bool* get_address_of_blockMode_14() { return &___blockMode_14; }
	inline void set_blockMode_14(bool value)
	{
		___blockMode_14 = value;
	}

	inline static int32_t get_offset_of_nBits_15() { return static_cast<int32_t>(offsetof(LzwInputStream_t2878829311, ___nBits_15)); }
	inline int32_t get_nBits_15() const { return ___nBits_15; }
	inline int32_t* get_address_of_nBits_15() { return &___nBits_15; }
	inline void set_nBits_15(int32_t value)
	{
		___nBits_15 = value;
	}

	inline static int32_t get_offset_of_maxBits_16() { return static_cast<int32_t>(offsetof(LzwInputStream_t2878829311, ___maxBits_16)); }
	inline int32_t get_maxBits_16() const { return ___maxBits_16; }
	inline int32_t* get_address_of_maxBits_16() { return &___maxBits_16; }
	inline void set_maxBits_16(int32_t value)
	{
		___maxBits_16 = value;
	}

	inline static int32_t get_offset_of_maxMaxCode_17() { return static_cast<int32_t>(offsetof(LzwInputStream_t2878829311, ___maxMaxCode_17)); }
	inline int32_t get_maxMaxCode_17() const { return ___maxMaxCode_17; }
	inline int32_t* get_address_of_maxMaxCode_17() { return &___maxMaxCode_17; }
	inline void set_maxMaxCode_17(int32_t value)
	{
		___maxMaxCode_17 = value;
	}

	inline static int32_t get_offset_of_maxCode_18() { return static_cast<int32_t>(offsetof(LzwInputStream_t2878829311, ___maxCode_18)); }
	inline int32_t get_maxCode_18() const { return ___maxCode_18; }
	inline int32_t* get_address_of_maxCode_18() { return &___maxCode_18; }
	inline void set_maxCode_18(int32_t value)
	{
		___maxCode_18 = value;
	}

	inline static int32_t get_offset_of_bitMask_19() { return static_cast<int32_t>(offsetof(LzwInputStream_t2878829311, ___bitMask_19)); }
	inline int32_t get_bitMask_19() const { return ___bitMask_19; }
	inline int32_t* get_address_of_bitMask_19() { return &___bitMask_19; }
	inline void set_bitMask_19(int32_t value)
	{
		___bitMask_19 = value;
	}

	inline static int32_t get_offset_of_oldCode_20() { return static_cast<int32_t>(offsetof(LzwInputStream_t2878829311, ___oldCode_20)); }
	inline int32_t get_oldCode_20() const { return ___oldCode_20; }
	inline int32_t* get_address_of_oldCode_20() { return &___oldCode_20; }
	inline void set_oldCode_20(int32_t value)
	{
		___oldCode_20 = value;
	}

	inline static int32_t get_offset_of_finChar_21() { return static_cast<int32_t>(offsetof(LzwInputStream_t2878829311, ___finChar_21)); }
	inline uint8_t get_finChar_21() const { return ___finChar_21; }
	inline uint8_t* get_address_of_finChar_21() { return &___finChar_21; }
	inline void set_finChar_21(uint8_t value)
	{
		___finChar_21 = value;
	}

	inline static int32_t get_offset_of_stackP_22() { return static_cast<int32_t>(offsetof(LzwInputStream_t2878829311, ___stackP_22)); }
	inline int32_t get_stackP_22() const { return ___stackP_22; }
	inline int32_t* get_address_of_stackP_22() { return &___stackP_22; }
	inline void set_stackP_22(int32_t value)
	{
		___stackP_22 = value;
	}

	inline static int32_t get_offset_of_freeEnt_23() { return static_cast<int32_t>(offsetof(LzwInputStream_t2878829311, ___freeEnt_23)); }
	inline int32_t get_freeEnt_23() const { return ___freeEnt_23; }
	inline int32_t* get_address_of_freeEnt_23() { return &___freeEnt_23; }
	inline void set_freeEnt_23(int32_t value)
	{
		___freeEnt_23 = value;
	}

	inline static int32_t get_offset_of_data_24() { return static_cast<int32_t>(offsetof(LzwInputStream_t2878829311, ___data_24)); }
	inline ByteU5BU5D_t4260760469* get_data_24() const { return ___data_24; }
	inline ByteU5BU5D_t4260760469** get_address_of_data_24() { return &___data_24; }
	inline void set_data_24(ByteU5BU5D_t4260760469* value)
	{
		___data_24 = value;
		Il2CppCodeGenWriteBarrier(&___data_24, value);
	}

	inline static int32_t get_offset_of_bitPos_25() { return static_cast<int32_t>(offsetof(LzwInputStream_t2878829311, ___bitPos_25)); }
	inline int32_t get_bitPos_25() const { return ___bitPos_25; }
	inline int32_t* get_address_of_bitPos_25() { return &___bitPos_25; }
	inline void set_bitPos_25(int32_t value)
	{
		___bitPos_25 = value;
	}

	inline static int32_t get_offset_of_end_26() { return static_cast<int32_t>(offsetof(LzwInputStream_t2878829311, ___end_26)); }
	inline int32_t get_end_26() const { return ___end_26; }
	inline int32_t* get_address_of_end_26() { return &___end_26; }
	inline void set_end_26(int32_t value)
	{
		___end_26 = value;
	}

	inline static int32_t get_offset_of_got_27() { return static_cast<int32_t>(offsetof(LzwInputStream_t2878829311, ___got_27)); }
	inline int32_t get_got_27() const { return ___got_27; }
	inline int32_t* get_address_of_got_27() { return &___got_27; }
	inline void set_got_27(int32_t value)
	{
		___got_27 = value;
	}

	inline static int32_t get_offset_of_eof_28() { return static_cast<int32_t>(offsetof(LzwInputStream_t2878829311, ___eof_28)); }
	inline bool get_eof_28() const { return ___eof_28; }
	inline bool* get_address_of_eof_28() { return &___eof_28; }
	inline void set_eof_28(bool value)
	{
		___eof_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
