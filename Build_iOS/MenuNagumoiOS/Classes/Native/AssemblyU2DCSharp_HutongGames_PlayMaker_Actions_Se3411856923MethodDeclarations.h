﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAnimatorFloat
struct SetAnimatorFloat_t3411856923;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAnimatorFloat::.ctor()
extern "C"  void SetAnimatorFloat__ctor_m820167339 (SetAnimatorFloat_t3411856923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorFloat::Reset()
extern "C"  void SetAnimatorFloat_Reset_m2761567576 (SetAnimatorFloat_t3411856923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorFloat::OnEnter()
extern "C"  void SetAnimatorFloat_OnEnter_m1593526210 (SetAnimatorFloat_t3411856923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorFloat::OnAnimatorMoveEvent()
extern "C"  void SetAnimatorFloat_OnAnimatorMoveEvent_m127027980 (SetAnimatorFloat_t3411856923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorFloat::OnUpdate()
extern "C"  void SetAnimatorFloat_OnUpdate_m1288231617 (SetAnimatorFloat_t3411856923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorFloat::SetParameter()
extern "C"  void SetAnimatorFloat_SetParameter_m2210846944 (SetAnimatorFloat_t3411856923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorFloat::OnExit()
extern "C"  void SetAnimatorFloat_OnExit_m1168702358 (SetAnimatorFloat_t3411856923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
