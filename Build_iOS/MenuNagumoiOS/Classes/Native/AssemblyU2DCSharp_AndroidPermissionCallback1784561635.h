﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.String>
struct Action_1_t403047693;

#include "UnityEngine_UnityEngine_AndroidJavaProxy1828457281.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AndroidPermissionCallback
struct  AndroidPermissionCallback_t1784561635  : public AndroidJavaProxy_t1828457281
{
public:
	// System.Action`1<System.String> AndroidPermissionCallback::OnPermissionGrantedAction
	Action_1_t403047693 * ___OnPermissionGrantedAction_1;
	// System.Action`1<System.String> AndroidPermissionCallback::OnPermissionDeniedAction
	Action_1_t403047693 * ___OnPermissionDeniedAction_2;

public:
	inline static int32_t get_offset_of_OnPermissionGrantedAction_1() { return static_cast<int32_t>(offsetof(AndroidPermissionCallback_t1784561635, ___OnPermissionGrantedAction_1)); }
	inline Action_1_t403047693 * get_OnPermissionGrantedAction_1() const { return ___OnPermissionGrantedAction_1; }
	inline Action_1_t403047693 ** get_address_of_OnPermissionGrantedAction_1() { return &___OnPermissionGrantedAction_1; }
	inline void set_OnPermissionGrantedAction_1(Action_1_t403047693 * value)
	{
		___OnPermissionGrantedAction_1 = value;
		Il2CppCodeGenWriteBarrier(&___OnPermissionGrantedAction_1, value);
	}

	inline static int32_t get_offset_of_OnPermissionDeniedAction_2() { return static_cast<int32_t>(offsetof(AndroidPermissionCallback_t1784561635, ___OnPermissionDeniedAction_2)); }
	inline Action_1_t403047693 * get_OnPermissionDeniedAction_2() const { return ___OnPermissionDeniedAction_2; }
	inline Action_1_t403047693 ** get_address_of_OnPermissionDeniedAction_2() { return &___OnPermissionDeniedAction_2; }
	inline void set_OnPermissionDeniedAction_2(Action_1_t403047693 * value)
	{
		___OnPermissionDeniedAction_2 = value;
		Il2CppCodeGenWriteBarrier(&___OnPermissionDeniedAction_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
