﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_EventArgs2540831021.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Core.ProgressEventArgs
struct  ProgressEventArgs_t2021439644  : public EventArgs_t2540831021
{
public:
	// System.String ICSharpCode.SharpZipLib.Core.ProgressEventArgs::name_
	String_t* ___name__1;
	// System.Int64 ICSharpCode.SharpZipLib.Core.ProgressEventArgs::processed_
	int64_t ___processed__2;
	// System.Int64 ICSharpCode.SharpZipLib.Core.ProgressEventArgs::target_
	int64_t ___target__3;
	// System.Boolean ICSharpCode.SharpZipLib.Core.ProgressEventArgs::continueRunning_
	bool ___continueRunning__4;

public:
	inline static int32_t get_offset_of_name__1() { return static_cast<int32_t>(offsetof(ProgressEventArgs_t2021439644, ___name__1)); }
	inline String_t* get_name__1() const { return ___name__1; }
	inline String_t** get_address_of_name__1() { return &___name__1; }
	inline void set_name__1(String_t* value)
	{
		___name__1 = value;
		Il2CppCodeGenWriteBarrier(&___name__1, value);
	}

	inline static int32_t get_offset_of_processed__2() { return static_cast<int32_t>(offsetof(ProgressEventArgs_t2021439644, ___processed__2)); }
	inline int64_t get_processed__2() const { return ___processed__2; }
	inline int64_t* get_address_of_processed__2() { return &___processed__2; }
	inline void set_processed__2(int64_t value)
	{
		___processed__2 = value;
	}

	inline static int32_t get_offset_of_target__3() { return static_cast<int32_t>(offsetof(ProgressEventArgs_t2021439644, ___target__3)); }
	inline int64_t get_target__3() const { return ___target__3; }
	inline int64_t* get_address_of_target__3() { return &___target__3; }
	inline void set_target__3(int64_t value)
	{
		___target__3 = value;
	}

	inline static int32_t get_offset_of_continueRunning__4() { return static_cast<int32_t>(offsetof(ProgressEventArgs_t2021439644, ___continueRunning__4)); }
	inline bool get_continueRunning__4() const { return ___continueRunning__4; }
	inline bool* get_address_of_continueRunning__4() { return &___continueRunning__4; }
	inline void set_continueRunning__4(bool value)
	{
		___continueRunning__4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
