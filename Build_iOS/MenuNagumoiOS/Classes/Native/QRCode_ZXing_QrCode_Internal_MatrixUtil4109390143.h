﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[][]
struct Int32U5BU5DU5BU5D_t1820556512;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.MatrixUtil
struct  MatrixUtil_t4109390143  : public Il2CppObject
{
public:

public:
};

struct MatrixUtil_t4109390143_StaticFields
{
public:
	// System.Int32[][] ZXing.QrCode.Internal.MatrixUtil::POSITION_DETECTION_PATTERN
	Int32U5BU5DU5BU5D_t1820556512* ___POSITION_DETECTION_PATTERN_0;
	// System.Int32[][] ZXing.QrCode.Internal.MatrixUtil::POSITION_ADJUSTMENT_PATTERN
	Int32U5BU5DU5BU5D_t1820556512* ___POSITION_ADJUSTMENT_PATTERN_1;
	// System.Int32[][] ZXing.QrCode.Internal.MatrixUtil::POSITION_ADJUSTMENT_PATTERN_COORDINATE_TABLE
	Int32U5BU5DU5BU5D_t1820556512* ___POSITION_ADJUSTMENT_PATTERN_COORDINATE_TABLE_2;
	// System.Int32[][] ZXing.QrCode.Internal.MatrixUtil::TYPE_INFO_COORDINATES
	Int32U5BU5DU5BU5D_t1820556512* ___TYPE_INFO_COORDINATES_3;

public:
	inline static int32_t get_offset_of_POSITION_DETECTION_PATTERN_0() { return static_cast<int32_t>(offsetof(MatrixUtil_t4109390143_StaticFields, ___POSITION_DETECTION_PATTERN_0)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_POSITION_DETECTION_PATTERN_0() const { return ___POSITION_DETECTION_PATTERN_0; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_POSITION_DETECTION_PATTERN_0() { return &___POSITION_DETECTION_PATTERN_0; }
	inline void set_POSITION_DETECTION_PATTERN_0(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___POSITION_DETECTION_PATTERN_0 = value;
		Il2CppCodeGenWriteBarrier(&___POSITION_DETECTION_PATTERN_0, value);
	}

	inline static int32_t get_offset_of_POSITION_ADJUSTMENT_PATTERN_1() { return static_cast<int32_t>(offsetof(MatrixUtil_t4109390143_StaticFields, ___POSITION_ADJUSTMENT_PATTERN_1)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_POSITION_ADJUSTMENT_PATTERN_1() const { return ___POSITION_ADJUSTMENT_PATTERN_1; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_POSITION_ADJUSTMENT_PATTERN_1() { return &___POSITION_ADJUSTMENT_PATTERN_1; }
	inline void set_POSITION_ADJUSTMENT_PATTERN_1(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___POSITION_ADJUSTMENT_PATTERN_1 = value;
		Il2CppCodeGenWriteBarrier(&___POSITION_ADJUSTMENT_PATTERN_1, value);
	}

	inline static int32_t get_offset_of_POSITION_ADJUSTMENT_PATTERN_COORDINATE_TABLE_2() { return static_cast<int32_t>(offsetof(MatrixUtil_t4109390143_StaticFields, ___POSITION_ADJUSTMENT_PATTERN_COORDINATE_TABLE_2)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_POSITION_ADJUSTMENT_PATTERN_COORDINATE_TABLE_2() const { return ___POSITION_ADJUSTMENT_PATTERN_COORDINATE_TABLE_2; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_POSITION_ADJUSTMENT_PATTERN_COORDINATE_TABLE_2() { return &___POSITION_ADJUSTMENT_PATTERN_COORDINATE_TABLE_2; }
	inline void set_POSITION_ADJUSTMENT_PATTERN_COORDINATE_TABLE_2(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___POSITION_ADJUSTMENT_PATTERN_COORDINATE_TABLE_2 = value;
		Il2CppCodeGenWriteBarrier(&___POSITION_ADJUSTMENT_PATTERN_COORDINATE_TABLE_2, value);
	}

	inline static int32_t get_offset_of_TYPE_INFO_COORDINATES_3() { return static_cast<int32_t>(offsetof(MatrixUtil_t4109390143_StaticFields, ___TYPE_INFO_COORDINATES_3)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_TYPE_INFO_COORDINATES_3() const { return ___TYPE_INFO_COORDINATES_3; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_TYPE_INFO_COORDINATES_3() { return &___TYPE_INFO_COORDINATES_3; }
	inline void set_TYPE_INFO_COORDINATES_3(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___TYPE_INFO_COORDINATES_3 = value;
		Il2CppCodeGenWriteBarrier(&___TYPE_INFO_COORDINATES_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
