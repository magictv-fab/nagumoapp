﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameMain/<Start>c__Iterator5B
struct U3CStartU3Ec__Iterator5B_t2168360362;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GameMain/<Start>c__Iterator5B::.ctor()
extern "C"  void U3CStartU3Ec__Iterator5B__ctor_m2257634049 (U3CStartU3Ec__Iterator5B_t2168360362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameMain/<Start>c__Iterator5B::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator5B_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2357882289 (U3CStartU3Ec__Iterator5B_t2168360362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameMain/<Start>c__Iterator5B::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator5B_System_Collections_IEnumerator_get_Current_m3624399685 (U3CStartU3Ec__Iterator5B_t2168360362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMain/<Start>c__Iterator5B::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator5B_MoveNext_m1862316627 (U3CStartU3Ec__Iterator5B_t2168360362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMain/<Start>c__Iterator5B::Dispose()
extern "C"  void U3CStartU3Ec__Iterator5B_Dispose_m526119678 (U3CStartU3Ec__Iterator5B_t2168360362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMain/<Start>c__Iterator5B::Reset()
extern "C"  void U3CStartU3Ec__Iterator5B_Reset_m4199034286 (U3CStartU3Ec__Iterator5B_t2168360362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
