﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.DecodeHintType,System.Object>
struct ValueCollection_t4177689510;
// System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>
struct Dictionary_2_t1182116501;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3408917205.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.DecodeHintType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m3014512577_gshared (ValueCollection_t4177689510 * __this, Dictionary_2_t1182116501 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m3014512577(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t4177689510 *, Dictionary_2_t1182116501 *, const MethodInfo*))ValueCollection__ctor_m3014512577_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.DecodeHintType,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2322002001_gshared (ValueCollection_t4177689510 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2322002001(__this, ___item0, method) ((  void (*) (ValueCollection_t4177689510 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2322002001_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.DecodeHintType,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4097551770_gshared (ValueCollection_t4177689510 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4097551770(__this, method) ((  void (*) (ValueCollection_t4177689510 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4097551770_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.DecodeHintType,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2352879673_gshared (ValueCollection_t4177689510 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2352879673(__this, ___item0, method) ((  bool (*) (ValueCollection_t4177689510 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2352879673_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.DecodeHintType,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1040313118_gshared (ValueCollection_t4177689510 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1040313118(__this, ___item0, method) ((  bool (*) (ValueCollection_t4177689510 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1040313118_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.DecodeHintType,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3322288474_gshared (ValueCollection_t4177689510 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3322288474(__this, method) ((  Il2CppObject* (*) (ValueCollection_t4177689510 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3322288474_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.DecodeHintType,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m2433848862_gshared (ValueCollection_t4177689510 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m2433848862(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t4177689510 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2433848862_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.DecodeHintType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2016469933_gshared (ValueCollection_t4177689510 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2016469933(__this, method) ((  Il2CppObject * (*) (ValueCollection_t4177689510 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2016469933_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.DecodeHintType,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m628055532_gshared (ValueCollection_t4177689510 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m628055532(__this, method) ((  bool (*) (ValueCollection_t4177689510 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m628055532_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.DecodeHintType,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2332173772_gshared (ValueCollection_t4177689510 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2332173772(__this, method) ((  bool (*) (ValueCollection_t4177689510 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2332173772_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.DecodeHintType,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m116774462_gshared (ValueCollection_t4177689510 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m116774462(__this, method) ((  Il2CppObject * (*) (ValueCollection_t4177689510 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m116774462_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.DecodeHintType,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m652078024_gshared (ValueCollection_t4177689510 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m652078024(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t4177689510 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m652078024_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.DecodeHintType,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3408917205  ValueCollection_GetEnumerator_m2218985265_gshared (ValueCollection_t4177689510 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m2218985265(__this, method) ((  Enumerator_t3408917205  (*) (ValueCollection_t4177689510 *, const MethodInfo*))ValueCollection_GetEnumerator_m2218985265_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.DecodeHintType,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m4167680774_gshared (ValueCollection_t4177689510 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m4167680774(__this, method) ((  int32_t (*) (ValueCollection_t4177689510 *, const MethodInfo*))ValueCollection_get_Count_m4167680774_gshared)(__this, method)
