﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.SByte[]
struct SByteU5BU5D_t2505034988;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.BarcodeRow
struct  BarcodeRow_t3616570866  : public Il2CppObject
{
public:
	// System.SByte[] ZXing.PDF417.Internal.BarcodeRow::row
	SByteU5BU5D_t2505034988* ___row_0;
	// System.Int32 ZXing.PDF417.Internal.BarcodeRow::currentLocation
	int32_t ___currentLocation_1;

public:
	inline static int32_t get_offset_of_row_0() { return static_cast<int32_t>(offsetof(BarcodeRow_t3616570866, ___row_0)); }
	inline SByteU5BU5D_t2505034988* get_row_0() const { return ___row_0; }
	inline SByteU5BU5D_t2505034988** get_address_of_row_0() { return &___row_0; }
	inline void set_row_0(SByteU5BU5D_t2505034988* value)
	{
		___row_0 = value;
		Il2CppCodeGenWriteBarrier(&___row_0, value);
	}

	inline static int32_t get_offset_of_currentLocation_1() { return static_cast<int32_t>(offsetof(BarcodeRow_t3616570866, ___currentLocation_1)); }
	inline int32_t get_currentLocation_1() const { return ___currentLocation_1; }
	inline int32_t* get_address_of_currentLocation_1() { return &___currentLocation_1; }
	inline void set_currentLocation_1(int32_t value)
	{
		___currentLocation_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
