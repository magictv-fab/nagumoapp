﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iTw410382178.h"
#include "AssemblyU2DCSharp_iTween_EaseType2734598229.h"
#include "AssemblyU2DCSharp_iTween_LoopType1485160459.h"
#include "UnityEngine_UnityEngine_Space4209342076.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.iTweenRotateBy
struct  iTweenRotateBy_t1141196668  : public iTweenFsmAction_t410382178
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.iTweenRotateBy::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.iTweenRotateBy::id
	FsmString_t952858651 * ___id_18;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.iTweenRotateBy::vector
	FsmVector3_t533912882 * ___vector_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.iTweenRotateBy::time
	FsmFloat_t2134102846 * ___time_20;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.iTweenRotateBy::delay
	FsmFloat_t2134102846 * ___delay_21;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.iTweenRotateBy::speed
	FsmFloat_t2134102846 * ___speed_22;
	// iTween/EaseType HutongGames.PlayMaker.Actions.iTweenRotateBy::easeType
	int32_t ___easeType_23;
	// iTween/LoopType HutongGames.PlayMaker.Actions.iTweenRotateBy::loopType
	int32_t ___loopType_24;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.iTweenRotateBy::space
	int32_t ___space_25;

public:
	inline static int32_t get_offset_of_gameObject_17() { return static_cast<int32_t>(offsetof(iTweenRotateBy_t1141196668, ___gameObject_17)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_17() const { return ___gameObject_17; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_17() { return &___gameObject_17; }
	inline void set_gameObject_17(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_17 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_17, value);
	}

	inline static int32_t get_offset_of_id_18() { return static_cast<int32_t>(offsetof(iTweenRotateBy_t1141196668, ___id_18)); }
	inline FsmString_t952858651 * get_id_18() const { return ___id_18; }
	inline FsmString_t952858651 ** get_address_of_id_18() { return &___id_18; }
	inline void set_id_18(FsmString_t952858651 * value)
	{
		___id_18 = value;
		Il2CppCodeGenWriteBarrier(&___id_18, value);
	}

	inline static int32_t get_offset_of_vector_19() { return static_cast<int32_t>(offsetof(iTweenRotateBy_t1141196668, ___vector_19)); }
	inline FsmVector3_t533912882 * get_vector_19() const { return ___vector_19; }
	inline FsmVector3_t533912882 ** get_address_of_vector_19() { return &___vector_19; }
	inline void set_vector_19(FsmVector3_t533912882 * value)
	{
		___vector_19 = value;
		Il2CppCodeGenWriteBarrier(&___vector_19, value);
	}

	inline static int32_t get_offset_of_time_20() { return static_cast<int32_t>(offsetof(iTweenRotateBy_t1141196668, ___time_20)); }
	inline FsmFloat_t2134102846 * get_time_20() const { return ___time_20; }
	inline FsmFloat_t2134102846 ** get_address_of_time_20() { return &___time_20; }
	inline void set_time_20(FsmFloat_t2134102846 * value)
	{
		___time_20 = value;
		Il2CppCodeGenWriteBarrier(&___time_20, value);
	}

	inline static int32_t get_offset_of_delay_21() { return static_cast<int32_t>(offsetof(iTweenRotateBy_t1141196668, ___delay_21)); }
	inline FsmFloat_t2134102846 * get_delay_21() const { return ___delay_21; }
	inline FsmFloat_t2134102846 ** get_address_of_delay_21() { return &___delay_21; }
	inline void set_delay_21(FsmFloat_t2134102846 * value)
	{
		___delay_21 = value;
		Il2CppCodeGenWriteBarrier(&___delay_21, value);
	}

	inline static int32_t get_offset_of_speed_22() { return static_cast<int32_t>(offsetof(iTweenRotateBy_t1141196668, ___speed_22)); }
	inline FsmFloat_t2134102846 * get_speed_22() const { return ___speed_22; }
	inline FsmFloat_t2134102846 ** get_address_of_speed_22() { return &___speed_22; }
	inline void set_speed_22(FsmFloat_t2134102846 * value)
	{
		___speed_22 = value;
		Il2CppCodeGenWriteBarrier(&___speed_22, value);
	}

	inline static int32_t get_offset_of_easeType_23() { return static_cast<int32_t>(offsetof(iTweenRotateBy_t1141196668, ___easeType_23)); }
	inline int32_t get_easeType_23() const { return ___easeType_23; }
	inline int32_t* get_address_of_easeType_23() { return &___easeType_23; }
	inline void set_easeType_23(int32_t value)
	{
		___easeType_23 = value;
	}

	inline static int32_t get_offset_of_loopType_24() { return static_cast<int32_t>(offsetof(iTweenRotateBy_t1141196668, ___loopType_24)); }
	inline int32_t get_loopType_24() const { return ___loopType_24; }
	inline int32_t* get_address_of_loopType_24() { return &___loopType_24; }
	inline void set_loopType_24(int32_t value)
	{
		___loopType_24 = value;
	}

	inline static int32_t get_offset_of_space_25() { return static_cast<int32_t>(offsetof(iTweenRotateBy_t1141196668, ___space_25)); }
	inline int32_t get_space_25() const { return ___space_25; }
	inline int32_t* get_address_of_space_25() { return &___space_25; }
	inline void set_space_25(int32_t value)
	{
		___space_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
