﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.events.InvertFilterActiveToggleOnOff
struct InvertFilterActiveToggleOnOff_t1330757774;

#include "codegen/il2cpp-codegen.h"

// System.Void ARM.events.InvertFilterActiveToggleOnOff::.ctor()
extern "C"  void InvertFilterActiveToggleOnOff__ctor_m2983400982 (InvertFilterActiveToggleOnOff_t1330757774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.events.InvertFilterActiveToggleOnOff::Start()
extern "C"  void InvertFilterActiveToggleOnOff_Start_m1930538774 (InvertFilterActiveToggleOnOff_t1330757774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.events.InvertFilterActiveToggleOnOff::Update()
extern "C"  void InvertFilterActiveToggleOnOff_Update_m4017979319 (InvertFilterActiveToggleOnOff_t1330757774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
