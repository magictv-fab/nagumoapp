﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TransformEvents/OnTransformEventHandler
struct OnTransformEventHandler_t2999119763;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void TransformEvents/OnTransformEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnTransformEventHandler__ctor_m1493181418 (OnTransformEventHandler_t2999119763 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransformEvents/OnTransformEventHandler::Invoke(UnityEngine.Vector3)
extern "C"  void OnTransformEventHandler_Invoke_m2540859541 (OnTransformEventHandler_t2999119763 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult TransformEvents/OnTransformEventHandler::BeginInvoke(UnityEngine.Vector3,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnTransformEventHandler_BeginInvoke_m1309091668 (OnTransformEventHandler_t2999119763 * __this, Vector3_t4282066566  ___value0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransformEvents/OnTransformEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnTransformEventHandler_EndInvoke_m503716602 (OnTransformEventHandler_t2999119763 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
