﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector3Lerp
struct Vector3Lerp_t122321781;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector3Lerp::.ctor()
extern "C"  void Vector3Lerp__ctor_m1107788641 (Vector3Lerp_t122321781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Lerp::Reset()
extern "C"  void Vector3Lerp_Reset_m3049188878 (Vector3Lerp_t122321781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Lerp::OnEnter()
extern "C"  void Vector3Lerp_OnEnter_m3119690488 (Vector3Lerp_t122321781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Lerp::OnUpdate()
extern "C"  void Vector3Lerp_OnUpdate_m1354683979 (Vector3Lerp_t122321781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Lerp::DoVector3Lerp()
extern "C"  void Vector3Lerp_DoVector3Lerp_m929971931 (Vector3Lerp_t122321781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
