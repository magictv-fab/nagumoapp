﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoaderPublishAnim
struct LoaderPublishAnim_t3439243661;

#include "codegen/il2cpp-codegen.h"

// System.Void LoaderPublishAnim::.ctor()
extern "C"  void LoaderPublishAnim__ctor_m3695143550 (LoaderPublishAnim_t3439243661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoaderPublishAnim::Start()
extern "C"  void LoaderPublishAnim_Start_m2642281342 (LoaderPublishAnim_t3439243661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoaderPublishAnim::Update()
extern "C"  void LoaderPublishAnim_Update_m312195151 (LoaderPublishAnim_t3439243661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoaderPublishAnim::OnLoad()
extern "C"  void LoaderPublishAnim_OnLoad_m290490347 (LoaderPublishAnim_t3439243661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoaderPublishAnim::EndLoad()
extern "C"  void LoaderPublishAnim_EndLoad_m146070781 (LoaderPublishAnim_t3439243661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
