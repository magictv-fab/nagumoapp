﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.INamedVariable
struct INamedVariable_t1024128046;
// System.String
struct String_t;
// UnityEngine.Object
struct Object_t3071478659;

#include "mscorlib_System_Object4170816371.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType3118725144.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmVar
struct  FsmVar_t1596150537  : public Il2CppObject
{
public:
	// HutongGames.PlayMaker.INamedVariable HutongGames.PlayMaker.FsmVar::namedVar
	Il2CppObject * ___namedVar_0;
	// System.String HutongGames.PlayMaker.FsmVar::variableName
	String_t* ___variableName_1;
	// System.Boolean HutongGames.PlayMaker.FsmVar::useVariable
	bool ___useVariable_2;
	// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmVar::type
	int32_t ___type_3;
	// System.Single HutongGames.PlayMaker.FsmVar::floatValue
	float ___floatValue_4;
	// System.Int32 HutongGames.PlayMaker.FsmVar::intValue
	int32_t ___intValue_5;
	// System.Boolean HutongGames.PlayMaker.FsmVar::boolValue
	bool ___boolValue_6;
	// System.String HutongGames.PlayMaker.FsmVar::stringValue
	String_t* ___stringValue_7;
	// UnityEngine.Vector4 HutongGames.PlayMaker.FsmVar::vector4Value
	Vector4_t4282066567  ___vector4Value_8;
	// UnityEngine.Object HutongGames.PlayMaker.FsmVar::objectReference
	Object_t3071478659 * ___objectReference_9;

public:
	inline static int32_t get_offset_of_namedVar_0() { return static_cast<int32_t>(offsetof(FsmVar_t1596150537, ___namedVar_0)); }
	inline Il2CppObject * get_namedVar_0() const { return ___namedVar_0; }
	inline Il2CppObject ** get_address_of_namedVar_0() { return &___namedVar_0; }
	inline void set_namedVar_0(Il2CppObject * value)
	{
		___namedVar_0 = value;
		Il2CppCodeGenWriteBarrier(&___namedVar_0, value);
	}

	inline static int32_t get_offset_of_variableName_1() { return static_cast<int32_t>(offsetof(FsmVar_t1596150537, ___variableName_1)); }
	inline String_t* get_variableName_1() const { return ___variableName_1; }
	inline String_t** get_address_of_variableName_1() { return &___variableName_1; }
	inline void set_variableName_1(String_t* value)
	{
		___variableName_1 = value;
		Il2CppCodeGenWriteBarrier(&___variableName_1, value);
	}

	inline static int32_t get_offset_of_useVariable_2() { return static_cast<int32_t>(offsetof(FsmVar_t1596150537, ___useVariable_2)); }
	inline bool get_useVariable_2() const { return ___useVariable_2; }
	inline bool* get_address_of_useVariable_2() { return &___useVariable_2; }
	inline void set_useVariable_2(bool value)
	{
		___useVariable_2 = value;
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(FsmVar_t1596150537, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_floatValue_4() { return static_cast<int32_t>(offsetof(FsmVar_t1596150537, ___floatValue_4)); }
	inline float get_floatValue_4() const { return ___floatValue_4; }
	inline float* get_address_of_floatValue_4() { return &___floatValue_4; }
	inline void set_floatValue_4(float value)
	{
		___floatValue_4 = value;
	}

	inline static int32_t get_offset_of_intValue_5() { return static_cast<int32_t>(offsetof(FsmVar_t1596150537, ___intValue_5)); }
	inline int32_t get_intValue_5() const { return ___intValue_5; }
	inline int32_t* get_address_of_intValue_5() { return &___intValue_5; }
	inline void set_intValue_5(int32_t value)
	{
		___intValue_5 = value;
	}

	inline static int32_t get_offset_of_boolValue_6() { return static_cast<int32_t>(offsetof(FsmVar_t1596150537, ___boolValue_6)); }
	inline bool get_boolValue_6() const { return ___boolValue_6; }
	inline bool* get_address_of_boolValue_6() { return &___boolValue_6; }
	inline void set_boolValue_6(bool value)
	{
		___boolValue_6 = value;
	}

	inline static int32_t get_offset_of_stringValue_7() { return static_cast<int32_t>(offsetof(FsmVar_t1596150537, ___stringValue_7)); }
	inline String_t* get_stringValue_7() const { return ___stringValue_7; }
	inline String_t** get_address_of_stringValue_7() { return &___stringValue_7; }
	inline void set_stringValue_7(String_t* value)
	{
		___stringValue_7 = value;
		Il2CppCodeGenWriteBarrier(&___stringValue_7, value);
	}

	inline static int32_t get_offset_of_vector4Value_8() { return static_cast<int32_t>(offsetof(FsmVar_t1596150537, ___vector4Value_8)); }
	inline Vector4_t4282066567  get_vector4Value_8() const { return ___vector4Value_8; }
	inline Vector4_t4282066567 * get_address_of_vector4Value_8() { return &___vector4Value_8; }
	inline void set_vector4Value_8(Vector4_t4282066567  value)
	{
		___vector4Value_8 = value;
	}

	inline static int32_t get_offset_of_objectReference_9() { return static_cast<int32_t>(offsetof(FsmVar_t1596150537, ___objectReference_9)); }
	inline Object_t3071478659 * get_objectReference_9() const { return ___objectReference_9; }
	inline Object_t3071478659 ** get_address_of_objectReference_9() { return &___objectReference_9; }
	inline void set_objectReference_9(Object_t3071478659 * value)
	{
		___objectReference_9 = value;
		Il2CppCodeGenWriteBarrier(&___objectReference_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
