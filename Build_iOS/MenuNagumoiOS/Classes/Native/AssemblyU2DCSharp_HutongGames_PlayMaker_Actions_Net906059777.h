﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkGetMessagePlayerProperties
struct  NetworkGetMessagePlayerProperties_t906059777  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.NetworkGetMessagePlayerProperties::IpAddress
	FsmString_t952858651 * ___IpAddress_9;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.NetworkGetMessagePlayerProperties::port
	FsmInt_t1596138449 * ___port_10;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.NetworkGetMessagePlayerProperties::guid
	FsmString_t952858651 * ___guid_11;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.NetworkGetMessagePlayerProperties::externalIPAddress
	FsmString_t952858651 * ___externalIPAddress_12;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.NetworkGetMessagePlayerProperties::externalPort
	FsmInt_t1596138449 * ___externalPort_13;

public:
	inline static int32_t get_offset_of_IpAddress_9() { return static_cast<int32_t>(offsetof(NetworkGetMessagePlayerProperties_t906059777, ___IpAddress_9)); }
	inline FsmString_t952858651 * get_IpAddress_9() const { return ___IpAddress_9; }
	inline FsmString_t952858651 ** get_address_of_IpAddress_9() { return &___IpAddress_9; }
	inline void set_IpAddress_9(FsmString_t952858651 * value)
	{
		___IpAddress_9 = value;
		Il2CppCodeGenWriteBarrier(&___IpAddress_9, value);
	}

	inline static int32_t get_offset_of_port_10() { return static_cast<int32_t>(offsetof(NetworkGetMessagePlayerProperties_t906059777, ___port_10)); }
	inline FsmInt_t1596138449 * get_port_10() const { return ___port_10; }
	inline FsmInt_t1596138449 ** get_address_of_port_10() { return &___port_10; }
	inline void set_port_10(FsmInt_t1596138449 * value)
	{
		___port_10 = value;
		Il2CppCodeGenWriteBarrier(&___port_10, value);
	}

	inline static int32_t get_offset_of_guid_11() { return static_cast<int32_t>(offsetof(NetworkGetMessagePlayerProperties_t906059777, ___guid_11)); }
	inline FsmString_t952858651 * get_guid_11() const { return ___guid_11; }
	inline FsmString_t952858651 ** get_address_of_guid_11() { return &___guid_11; }
	inline void set_guid_11(FsmString_t952858651 * value)
	{
		___guid_11 = value;
		Il2CppCodeGenWriteBarrier(&___guid_11, value);
	}

	inline static int32_t get_offset_of_externalIPAddress_12() { return static_cast<int32_t>(offsetof(NetworkGetMessagePlayerProperties_t906059777, ___externalIPAddress_12)); }
	inline FsmString_t952858651 * get_externalIPAddress_12() const { return ___externalIPAddress_12; }
	inline FsmString_t952858651 ** get_address_of_externalIPAddress_12() { return &___externalIPAddress_12; }
	inline void set_externalIPAddress_12(FsmString_t952858651 * value)
	{
		___externalIPAddress_12 = value;
		Il2CppCodeGenWriteBarrier(&___externalIPAddress_12, value);
	}

	inline static int32_t get_offset_of_externalPort_13() { return static_cast<int32_t>(offsetof(NetworkGetMessagePlayerProperties_t906059777, ___externalPort_13)); }
	inline FsmInt_t1596138449 * get_externalPort_13() const { return ___externalPort_13; }
	inline FsmInt_t1596138449 ** get_address_of_externalPort_13() { return &___externalPort_13; }
	inline void set_externalPort_13(FsmInt_t1596138449 * value)
	{
		___externalPort_13 = value;
		Il2CppCodeGenWriteBarrier(&___externalPort_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
