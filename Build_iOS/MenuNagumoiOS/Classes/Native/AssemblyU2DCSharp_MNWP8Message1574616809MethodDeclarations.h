﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MNWP8Message
struct MNWP8Message_t1574616809;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MNWP8Message::.ctor()
extern "C"  void MNWP8Message__ctor_m4160416210 (MNWP8Message_t1574616809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MNWP8Message MNWP8Message::Create(System.String,System.String)
extern "C"  MNWP8Message_t1574616809 * MNWP8Message_Create_m2877452828 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNWP8Message::init()
extern "C"  void MNWP8Message_init_m3772823074 (MNWP8Message_t1574616809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNWP8Message::onPopUpCallBack()
extern "C"  void MNWP8Message_onPopUpCallBack_m459196802 (MNWP8Message_t1574616809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
