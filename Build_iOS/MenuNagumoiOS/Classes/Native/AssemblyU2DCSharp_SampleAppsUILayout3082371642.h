﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<ISampleAppUIElement>
struct List_1_t3548236426;
// UnityEngine.GUIStyle
struct GUIStyle_t2990928826;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SampleAppsUILayout
struct  SampleAppsUILayout_t3082371642  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<ISampleAppUIElement> SampleAppsUILayout::GUIElements
	List_1_t3548236426 * ___GUIElements_0;
	// System.Single SampleAppsUILayout::mIndex
	float ___mIndex_1;
	// UnityEngine.GUIStyle SampleAppsUILayout::mStyleHeader
	GUIStyle_t2990928826 * ___mStyleHeader_2;
	// UnityEngine.GUIStyle SampleAppsUILayout::mStyleAboutButton
	GUIStyle_t2990928826 * ___mStyleAboutButton_3;
	// UnityEngine.GUIStyle SampleAppsUILayout::mStyleSlider
	GUIStyle_t2990928826 * ___mStyleSlider_4;
	// UnityEngine.GUIStyle SampleAppsUILayout::mStyleToggle
	GUIStyle_t2990928826 * ___mStyleToggle_5;
	// UnityEngine.GUIStyle SampleAppsUILayout::mStyleCloseButton
	GUIStyle_t2990928826 * ___mStyleCloseButton_6;
	// UnityEngine.GUIStyle SampleAppsUILayout::mStyleGroupLabel
	GUIStyle_t2990928826 * ___mStyleGroupLabel_7;

public:
	inline static int32_t get_offset_of_GUIElements_0() { return static_cast<int32_t>(offsetof(SampleAppsUILayout_t3082371642, ___GUIElements_0)); }
	inline List_1_t3548236426 * get_GUIElements_0() const { return ___GUIElements_0; }
	inline List_1_t3548236426 ** get_address_of_GUIElements_0() { return &___GUIElements_0; }
	inline void set_GUIElements_0(List_1_t3548236426 * value)
	{
		___GUIElements_0 = value;
		Il2CppCodeGenWriteBarrier(&___GUIElements_0, value);
	}

	inline static int32_t get_offset_of_mIndex_1() { return static_cast<int32_t>(offsetof(SampleAppsUILayout_t3082371642, ___mIndex_1)); }
	inline float get_mIndex_1() const { return ___mIndex_1; }
	inline float* get_address_of_mIndex_1() { return &___mIndex_1; }
	inline void set_mIndex_1(float value)
	{
		___mIndex_1 = value;
	}

	inline static int32_t get_offset_of_mStyleHeader_2() { return static_cast<int32_t>(offsetof(SampleAppsUILayout_t3082371642, ___mStyleHeader_2)); }
	inline GUIStyle_t2990928826 * get_mStyleHeader_2() const { return ___mStyleHeader_2; }
	inline GUIStyle_t2990928826 ** get_address_of_mStyleHeader_2() { return &___mStyleHeader_2; }
	inline void set_mStyleHeader_2(GUIStyle_t2990928826 * value)
	{
		___mStyleHeader_2 = value;
		Il2CppCodeGenWriteBarrier(&___mStyleHeader_2, value);
	}

	inline static int32_t get_offset_of_mStyleAboutButton_3() { return static_cast<int32_t>(offsetof(SampleAppsUILayout_t3082371642, ___mStyleAboutButton_3)); }
	inline GUIStyle_t2990928826 * get_mStyleAboutButton_3() const { return ___mStyleAboutButton_3; }
	inline GUIStyle_t2990928826 ** get_address_of_mStyleAboutButton_3() { return &___mStyleAboutButton_3; }
	inline void set_mStyleAboutButton_3(GUIStyle_t2990928826 * value)
	{
		___mStyleAboutButton_3 = value;
		Il2CppCodeGenWriteBarrier(&___mStyleAboutButton_3, value);
	}

	inline static int32_t get_offset_of_mStyleSlider_4() { return static_cast<int32_t>(offsetof(SampleAppsUILayout_t3082371642, ___mStyleSlider_4)); }
	inline GUIStyle_t2990928826 * get_mStyleSlider_4() const { return ___mStyleSlider_4; }
	inline GUIStyle_t2990928826 ** get_address_of_mStyleSlider_4() { return &___mStyleSlider_4; }
	inline void set_mStyleSlider_4(GUIStyle_t2990928826 * value)
	{
		___mStyleSlider_4 = value;
		Il2CppCodeGenWriteBarrier(&___mStyleSlider_4, value);
	}

	inline static int32_t get_offset_of_mStyleToggle_5() { return static_cast<int32_t>(offsetof(SampleAppsUILayout_t3082371642, ___mStyleToggle_5)); }
	inline GUIStyle_t2990928826 * get_mStyleToggle_5() const { return ___mStyleToggle_5; }
	inline GUIStyle_t2990928826 ** get_address_of_mStyleToggle_5() { return &___mStyleToggle_5; }
	inline void set_mStyleToggle_5(GUIStyle_t2990928826 * value)
	{
		___mStyleToggle_5 = value;
		Il2CppCodeGenWriteBarrier(&___mStyleToggle_5, value);
	}

	inline static int32_t get_offset_of_mStyleCloseButton_6() { return static_cast<int32_t>(offsetof(SampleAppsUILayout_t3082371642, ___mStyleCloseButton_6)); }
	inline GUIStyle_t2990928826 * get_mStyleCloseButton_6() const { return ___mStyleCloseButton_6; }
	inline GUIStyle_t2990928826 ** get_address_of_mStyleCloseButton_6() { return &___mStyleCloseButton_6; }
	inline void set_mStyleCloseButton_6(GUIStyle_t2990928826 * value)
	{
		___mStyleCloseButton_6 = value;
		Il2CppCodeGenWriteBarrier(&___mStyleCloseButton_6, value);
	}

	inline static int32_t get_offset_of_mStyleGroupLabel_7() { return static_cast<int32_t>(offsetof(SampleAppsUILayout_t3082371642, ___mStyleGroupLabel_7)); }
	inline GUIStyle_t2990928826 * get_mStyleGroupLabel_7() const { return ___mStyleGroupLabel_7; }
	inline GUIStyle_t2990928826 ** get_address_of_mStyleGroupLabel_7() { return &___mStyleGroupLabel_7; }
	inline void set_mStyleGroupLabel_7(GUIStyle_t2990928826 * value)
	{
		___mStyleGroupLabel_7 = value;
		Il2CppCodeGenWriteBarrier(&___mStyleGroupLabel_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
