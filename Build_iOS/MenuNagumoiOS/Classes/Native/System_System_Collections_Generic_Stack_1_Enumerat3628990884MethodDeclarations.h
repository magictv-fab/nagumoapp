﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_Enumerat2532196025MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1/Enumerator<UnityEngine.RectTransform>::.ctor(System.Collections.Generic.Stack`1<T>)
#define Enumerator__ctor_m2324189786(__this, ___t0, method) ((  void (*) (Enumerator_t3628990884 *, Stack_1_t4071204858 *, const MethodInfo*))Enumerator__ctor_m1003414509_gshared)(__this, ___t0, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<UnityEngine.RectTransform>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2926263838(__this, method) ((  void (*) (Enumerator_t3628990884 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3054975473_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<UnityEngine.RectTransform>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1772220116(__this, method) ((  Il2CppObject * (*) (Enumerator_t3628990884 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2470832103_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<UnityEngine.RectTransform>::Dispose()
#define Enumerator_Dispose_m26759321(__this, method) ((  void (*) (Enumerator_t3628990884 *, const MethodInfo*))Enumerator_Dispose_m1634653158_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<UnityEngine.RectTransform>::MoveNext()
#define Enumerator_MoveNext_m1527508450(__this, method) ((  bool (*) (Enumerator_t3628990884 *, const MethodInfo*))Enumerator_MoveNext_m3012756789_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<UnityEngine.RectTransform>::get_Current()
#define Enumerator_get_Current_m2142336555(__this, method) ((  RectTransform_t972643934 * (*) (Enumerator_t3628990884 *, const MethodInfo*))Enumerator_get_Current_m2483819640_gshared)(__this, method)
