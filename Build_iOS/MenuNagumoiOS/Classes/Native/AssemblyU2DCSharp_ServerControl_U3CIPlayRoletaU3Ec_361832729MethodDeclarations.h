﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ServerControl/<IPlayRoleta>c__Iterator7D
struct U3CIPlayRoletaU3Ec__Iterator7D_t361832729;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ServerControl/<IPlayRoleta>c__Iterator7D::.ctor()
extern "C"  void U3CIPlayRoletaU3Ec__Iterator7D__ctor_m1857186210 (U3CIPlayRoletaU3Ec__Iterator7D_t361832729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ServerControl/<IPlayRoleta>c__Iterator7D::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CIPlayRoletaU3Ec__Iterator7D_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3085085818 (U3CIPlayRoletaU3Ec__Iterator7D_t361832729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ServerControl/<IPlayRoleta>c__Iterator7D::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CIPlayRoletaU3Ec__Iterator7D_System_Collections_IEnumerator_get_Current_m501277710 (U3CIPlayRoletaU3Ec__Iterator7D_t361832729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ServerControl/<IPlayRoleta>c__Iterator7D::MoveNext()
extern "C"  bool U3CIPlayRoletaU3Ec__Iterator7D_MoveNext_m2625572090 (U3CIPlayRoletaU3Ec__Iterator7D_t361832729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ServerControl/<IPlayRoleta>c__Iterator7D::Dispose()
extern "C"  void U3CIPlayRoletaU3Ec__Iterator7D_Dispose_m2242803039 (U3CIPlayRoletaU3Ec__Iterator7D_t361832729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ServerControl/<IPlayRoleta>c__Iterator7D::Reset()
extern "C"  void U3CIPlayRoletaU3Ec__Iterator7D_Reset_m3798586447 (U3CIPlayRoletaU3Ec__Iterator7D_t361832729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
