﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.MasterServerSetProperties
struct  MasterServerSetProperties_t3940320318  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.MasterServerSetProperties::ipAddress
	FsmString_t952858651 * ___ipAddress_9;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.MasterServerSetProperties::port
	FsmInt_t1596138449 * ___port_10;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.MasterServerSetProperties::updateRate
	FsmInt_t1596138449 * ___updateRate_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.MasterServerSetProperties::dedicatedServer
	FsmBool_t1075959796 * ___dedicatedServer_12;

public:
	inline static int32_t get_offset_of_ipAddress_9() { return static_cast<int32_t>(offsetof(MasterServerSetProperties_t3940320318, ___ipAddress_9)); }
	inline FsmString_t952858651 * get_ipAddress_9() const { return ___ipAddress_9; }
	inline FsmString_t952858651 ** get_address_of_ipAddress_9() { return &___ipAddress_9; }
	inline void set_ipAddress_9(FsmString_t952858651 * value)
	{
		___ipAddress_9 = value;
		Il2CppCodeGenWriteBarrier(&___ipAddress_9, value);
	}

	inline static int32_t get_offset_of_port_10() { return static_cast<int32_t>(offsetof(MasterServerSetProperties_t3940320318, ___port_10)); }
	inline FsmInt_t1596138449 * get_port_10() const { return ___port_10; }
	inline FsmInt_t1596138449 ** get_address_of_port_10() { return &___port_10; }
	inline void set_port_10(FsmInt_t1596138449 * value)
	{
		___port_10 = value;
		Il2CppCodeGenWriteBarrier(&___port_10, value);
	}

	inline static int32_t get_offset_of_updateRate_11() { return static_cast<int32_t>(offsetof(MasterServerSetProperties_t3940320318, ___updateRate_11)); }
	inline FsmInt_t1596138449 * get_updateRate_11() const { return ___updateRate_11; }
	inline FsmInt_t1596138449 ** get_address_of_updateRate_11() { return &___updateRate_11; }
	inline void set_updateRate_11(FsmInt_t1596138449 * value)
	{
		___updateRate_11 = value;
		Il2CppCodeGenWriteBarrier(&___updateRate_11, value);
	}

	inline static int32_t get_offset_of_dedicatedServer_12() { return static_cast<int32_t>(offsetof(MasterServerSetProperties_t3940320318, ___dedicatedServer_12)); }
	inline FsmBool_t1075959796 * get_dedicatedServer_12() const { return ___dedicatedServer_12; }
	inline FsmBool_t1075959796 ** get_address_of_dedicatedServer_12() { return &___dedicatedServer_12; }
	inline void set_dedicatedServer_12(FsmBool_t1075959796 * value)
	{
		___dedicatedServer_12 = value;
		Il2CppCodeGenWriteBarrier(&___dedicatedServer_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
