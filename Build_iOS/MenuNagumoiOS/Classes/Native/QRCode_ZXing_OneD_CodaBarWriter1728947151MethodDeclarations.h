﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.CodaBarWriter
struct CodaBarWriter_t1728947151;
// System.Boolean[]
struct BooleanU5BU5D_t3456302923;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Boolean[] ZXing.OneD.CodaBarWriter::encode(System.String)
extern "C"  BooleanU5BU5D_t3456302923* CodaBarWriter_encode_m4107407578 (CodaBarWriter_t1728947151 * __this, String_t* ___contents0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.CodaBarWriter::.ctor()
extern "C"  void CodaBarWriter__ctor_m131027876 (CodaBarWriter_t1728947151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.CodaBarWriter::.cctor()
extern "C"  void CodaBarWriter__cctor_m3579767945 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
