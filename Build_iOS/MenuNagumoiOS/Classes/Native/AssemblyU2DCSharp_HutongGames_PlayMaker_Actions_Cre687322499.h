﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.CreateObject
struct  CreateObject_t687322499  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.CreateObject::gameObject
	FsmGameObject_t1697147867 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.CreateObject::spawnPoint
	FsmGameObject_t1697147867 * ___spawnPoint_10;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.CreateObject::position
	FsmVector3_t533912882 * ___position_11;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.CreateObject::rotation
	FsmVector3_t533912882 * ___rotation_12;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.CreateObject::storeObject
	FsmGameObject_t1697147867 * ___storeObject_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.CreateObject::networkInstantiate
	FsmBool_t1075959796 * ___networkInstantiate_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.CreateObject::networkGroup
	FsmInt_t1596138449 * ___networkGroup_15;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(CreateObject_t687322499, ___gameObject_9)); }
	inline FsmGameObject_t1697147867 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmGameObject_t1697147867 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmGameObject_t1697147867 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_spawnPoint_10() { return static_cast<int32_t>(offsetof(CreateObject_t687322499, ___spawnPoint_10)); }
	inline FsmGameObject_t1697147867 * get_spawnPoint_10() const { return ___spawnPoint_10; }
	inline FsmGameObject_t1697147867 ** get_address_of_spawnPoint_10() { return &___spawnPoint_10; }
	inline void set_spawnPoint_10(FsmGameObject_t1697147867 * value)
	{
		___spawnPoint_10 = value;
		Il2CppCodeGenWriteBarrier(&___spawnPoint_10, value);
	}

	inline static int32_t get_offset_of_position_11() { return static_cast<int32_t>(offsetof(CreateObject_t687322499, ___position_11)); }
	inline FsmVector3_t533912882 * get_position_11() const { return ___position_11; }
	inline FsmVector3_t533912882 ** get_address_of_position_11() { return &___position_11; }
	inline void set_position_11(FsmVector3_t533912882 * value)
	{
		___position_11 = value;
		Il2CppCodeGenWriteBarrier(&___position_11, value);
	}

	inline static int32_t get_offset_of_rotation_12() { return static_cast<int32_t>(offsetof(CreateObject_t687322499, ___rotation_12)); }
	inline FsmVector3_t533912882 * get_rotation_12() const { return ___rotation_12; }
	inline FsmVector3_t533912882 ** get_address_of_rotation_12() { return &___rotation_12; }
	inline void set_rotation_12(FsmVector3_t533912882 * value)
	{
		___rotation_12 = value;
		Il2CppCodeGenWriteBarrier(&___rotation_12, value);
	}

	inline static int32_t get_offset_of_storeObject_13() { return static_cast<int32_t>(offsetof(CreateObject_t687322499, ___storeObject_13)); }
	inline FsmGameObject_t1697147867 * get_storeObject_13() const { return ___storeObject_13; }
	inline FsmGameObject_t1697147867 ** get_address_of_storeObject_13() { return &___storeObject_13; }
	inline void set_storeObject_13(FsmGameObject_t1697147867 * value)
	{
		___storeObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___storeObject_13, value);
	}

	inline static int32_t get_offset_of_networkInstantiate_14() { return static_cast<int32_t>(offsetof(CreateObject_t687322499, ___networkInstantiate_14)); }
	inline FsmBool_t1075959796 * get_networkInstantiate_14() const { return ___networkInstantiate_14; }
	inline FsmBool_t1075959796 ** get_address_of_networkInstantiate_14() { return &___networkInstantiate_14; }
	inline void set_networkInstantiate_14(FsmBool_t1075959796 * value)
	{
		___networkInstantiate_14 = value;
		Il2CppCodeGenWriteBarrier(&___networkInstantiate_14, value);
	}

	inline static int32_t get_offset_of_networkGroup_15() { return static_cast<int32_t>(offsetof(CreateObject_t687322499, ___networkGroup_15)); }
	inline FsmInt_t1596138449 * get_networkGroup_15() const { return ___networkGroup_15; }
	inline FsmInt_t1596138449 ** get_address_of_networkGroup_15() { return &___networkGroup_15; }
	inline void set_networkGroup_15(FsmInt_t1596138449 * value)
	{
		___networkGroup_15 = value;
		Il2CppCodeGenWriteBarrier(&___networkGroup_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
