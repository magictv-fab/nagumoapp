﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Core.NameFilter
struct NameFilter_t123192849;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ICSharpCode.SharpZipLib.Core.NameFilter::.ctor(System.String)
extern "C"  void NameFilter__ctor_m259055352 (NameFilter_t123192849 * __this, String_t* ___filter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Core.NameFilter::IsValidExpression(System.String)
extern "C"  bool NameFilter_IsValidExpression_m71805564 (Il2CppObject * __this /* static, unused */, String_t* ___expression0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Core.NameFilter::IsValidFilterExpression(System.String)
extern "C"  bool NameFilter_IsValidFilterExpression_m1557017508 (Il2CppObject * __this /* static, unused */, String_t* ___toTest0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] ICSharpCode.SharpZipLib.Core.NameFilter::SplitQuoted(System.String)
extern "C"  StringU5BU5D_t4054002952* NameFilter_SplitQuoted_m3077234327 (Il2CppObject * __this /* static, unused */, String_t* ___original0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Core.NameFilter::ToString()
extern "C"  String_t* NameFilter_ToString_m2667902883 (NameFilter_t123192849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Core.NameFilter::IsIncluded(System.String)
extern "C"  bool NameFilter_IsIncluded_m1359936822 (NameFilter_t123192849 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Core.NameFilter::IsExcluded(System.String)
extern "C"  bool NameFilter_IsExcluded_m1770119784 (NameFilter_t123192849 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Core.NameFilter::IsMatch(System.String)
extern "C"  bool NameFilter_IsMatch_m592328459 (NameFilter_t123192849 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.NameFilter::Compile()
extern "C"  void NameFilter_Compile_m2188282139 (NameFilter_t123192849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
