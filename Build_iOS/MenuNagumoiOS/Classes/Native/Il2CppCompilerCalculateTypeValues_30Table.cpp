﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_StateMana2944561054.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFin1331156121.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFind138063285.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFin3822354588.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFin2468213607.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFin3609410114.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFin1985818969.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFin3256513230.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFin2230003487.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableS179597514.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Trackable3166588170.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextureRe3929117224.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackerMa2355365335.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackerMana55743383.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualBut704206407.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualBu1019632289.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualBu1005202439.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamImp1072092957.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamPro3136319864.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamPro1961690790.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamPro3794639002.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTarg2347335889.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerAbst865486027.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MaskOutAb3409682331.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MultiTarg3565607731.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUn1557074901.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUni432217960.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUn1795183225.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUn2677974245.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaAb1091759131.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaAb2257996192.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMac337691955.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRu1069467038.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRu2616862271.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceUti743372383.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextRecoAb137142681.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SimpleTar3516126639.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TurnOffAb1299549867.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UserDefin1531824314.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoBack2475465524.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoText4076028962.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualBu1191238304.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamAbst725705546.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordAbstra545947899.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordFilter661135463.h"
#include "Qualcomm_Vuforia_UnityExtensions_U3CPrivateImpleme3248040453.h"
#include "Qualcomm_Vuforia_UnityExtensions_U3CPrivateImpleme3454134163.h"
#include "UnityEngine_UI_U3CModuleU3E86524790.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventHandl1938870832.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste2276120119.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigge672607302.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigger95600550.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigge849715470.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg3843052064.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve2704060668.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_MoveDirect2840182460.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycasterM2104732159.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastRes3762661364.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviou2511441271.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AxisEventD3355659985.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AbstractEv1322796528.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD2054899105.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1848751023.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEven384979233.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve2820857440.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInputMod15847059.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp3771003169.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp2039702646.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp3613479957.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp4082623702.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_Standalone1096194655.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_Standalone1573608962.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_TouchInputM894675487.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseRaycas2327671059.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_Physics2DRa935388869.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PhysicsRay1170055767.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorT723277650.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3000 = { sizeof (StateManagerImpl_t2944561054), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3000[6] = 
{
	StateManagerImpl_t2944561054::get_offset_of_mTrackableBehaviours_0(),
	StateManagerImpl_t2944561054::get_offset_of_mAutomaticallyCreatedBehaviours_1(),
	StateManagerImpl_t2944561054::get_offset_of_mBehavioursMarkedForDeletion_2(),
	StateManagerImpl_t2944561054::get_offset_of_mActiveTrackableBehaviours_3(),
	StateManagerImpl_t2944561054::get_offset_of_mWordManager_4(),
	StateManagerImpl_t2944561054::get_offset_of_mCameraPositioningHelper_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3001 = { sizeof (TargetFinder_t1331156121), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3002 = { sizeof (InitState_t138063285)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3002[6] = 
{
	InitState_t138063285::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3003 = { sizeof (UpdateState_t3822354588)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3003[12] = 
{
	UpdateState_t3822354588::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3004 = { sizeof (FilterMode_t2468213607)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3004[3] = 
{
	FilterMode_t2468213607::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3005 = { sizeof (TargetSearchResult_t3609410114)+ sizeof (Il2CppObject), sizeof(TargetSearchResult_t3609410114_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3005[6] = 
{
	TargetSearchResult_t3609410114::get_offset_of_TargetName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t3609410114::get_offset_of_UniqueTargetId_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t3609410114::get_offset_of_TargetSize_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t3609410114::get_offset_of_MetaData_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t3609410114::get_offset_of_TrackingRating_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t3609410114::get_offset_of_TargetSearchResultPtr_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3006 = { sizeof (TargetFinderImpl_t1985818969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3006[4] = 
{
	TargetFinderImpl_t1985818969::get_offset_of_mTargetFinderStatePtr_0(),
	TargetFinderImpl_t1985818969::get_offset_of_mTargetFinderState_1(),
	TargetFinderImpl_t1985818969::get_offset_of_mNewResults_2(),
	TargetFinderImpl_t1985818969::get_offset_of_mImageTargets_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3007 = { sizeof (TargetFinderState_t3256513230)+ sizeof (Il2CppObject), sizeof(TargetFinderState_t3256513230_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3007[4] = 
{
	TargetFinderState_t3256513230::get_offset_of_IsRequesting_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetFinderState_t3256513230::get_offset_of_UpdateState_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetFinderState_t3256513230::get_offset_of_ResultCount_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetFinderState_t3256513230::get_offset_of_unused_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3008 = { sizeof (InternalTargetSearchResult_t2230003487)+ sizeof (Il2CppObject), sizeof(InternalTargetSearchResult_t2230003487_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3008[6] = 
{
	InternalTargetSearchResult_t2230003487::get_offset_of_TargetNamePtr_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t2230003487::get_offset_of_UniqueTargetIdPtr_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t2230003487::get_offset_of_MetaDataPtr_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t2230003487::get_offset_of_TargetSearchResultPtr_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t2230003487::get_offset_of_TargetSize_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t2230003487::get_offset_of_TrackingRating_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3009 = { sizeof (TrackableSource_t179597514), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3010 = { sizeof (TrackableSourceImpl_t3166588170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3010[1] = 
{
	TrackableSourceImpl_t3166588170::get_offset_of_U3CTrackableSourcePtrU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3011 = { sizeof (TextureRenderer_t3929117224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3011[3] = 
{
	TextureRenderer_t3929117224::get_offset_of_mTextureBufferCamera_0(),
	TextureRenderer_t3929117224::get_offset_of_mTextureWidth_1(),
	TextureRenderer_t3929117224::get_offset_of_mTextureHeight_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3012 = { sizeof (TrackerManager_t2355365335), -1, sizeof(TrackerManager_t2355365335_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3012[1] = 
{
	TrackerManager_t2355365335_StaticFields::get_offset_of_mInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3013 = { sizeof (TrackerManagerImpl_t55743383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3013[5] = 
{
	TrackerManagerImpl_t55743383::get_offset_of_mObjectTracker_1(),
	TrackerManagerImpl_t55743383::get_offset_of_mMarkerTracker_2(),
	TrackerManagerImpl_t55743383::get_offset_of_mTextTracker_3(),
	TrackerManagerImpl_t55743383::get_offset_of_mSmartTerrainTracker_4(),
	TrackerManagerImpl_t55743383::get_offset_of_mStateManager_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3014 = { sizeof (VirtualButton_t704206407), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3014[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3015 = { sizeof (Sensitivity_t1019632289)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3015[4] = 
{
	Sensitivity_t1019632289::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3016 = { sizeof (VirtualButtonImpl_t1005202439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3016[6] = 
{
	VirtualButtonImpl_t1005202439::get_offset_of_mName_1(),
	VirtualButtonImpl_t1005202439::get_offset_of_mID_2(),
	VirtualButtonImpl_t1005202439::get_offset_of_mArea_3(),
	VirtualButtonImpl_t1005202439::get_offset_of_mIsEnabled_4(),
	VirtualButtonImpl_t1005202439::get_offset_of_mParentImageTarget_5(),
	VirtualButtonImpl_t1005202439::get_offset_of_mParentDataSet_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3017 = { sizeof (WebCamImpl_t1072092957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3017[15] = 
{
	WebCamImpl_t1072092957::get_offset_of_mARCameras_0(),
	WebCamImpl_t1072092957::get_offset_of_mOriginalCameraCullMask_1(),
	WebCamImpl_t1072092957::get_offset_of_mWebCamTexture_2(),
	WebCamImpl_t1072092957::get_offset_of_mVideoModeData_3(),
	WebCamImpl_t1072092957::get_offset_of_mVideoTextureInfo_4(),
	WebCamImpl_t1072092957::get_offset_of_mTextureRenderer_5(),
	WebCamImpl_t1072092957::get_offset_of_mBufferReadTexture_6(),
	WebCamImpl_t1072092957::get_offset_of_mReadPixelsRect_7(),
	WebCamImpl_t1072092957::get_offset_of_mWebCamProfile_8(),
	WebCamImpl_t1072092957::get_offset_of_mFlipHorizontally_9(),
	WebCamImpl_t1072092957::get_offset_of_mIsDirty_10(),
	WebCamImpl_t1072092957::get_offset_of_mLastFrameIdx_11(),
	WebCamImpl_t1072092957::get_offset_of_mRenderTextureLayer_12(),
	WebCamImpl_t1072092957::get_offset_of_mWebcamPlaying_13(),
	WebCamImpl_t1072092957::get_offset_of_U3CIsTextureSizeAvailableU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3018 = { sizeof (WebCamProfile_t3136319864), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3018[1] = 
{
	WebCamProfile_t3136319864::get_offset_of_mProfileCollection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3019 = { sizeof (ProfileData_t1961690790)+ sizeof (Il2CppObject), sizeof(ProfileData_t1961690790_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3019[3] = 
{
	ProfileData_t1961690790::get_offset_of_RequestedTextureSize_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ProfileData_t1961690790::get_offset_of_ResampledTextureSize_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ProfileData_t1961690790::get_offset_of_RequestedFPS_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3020 = { sizeof (ProfileCollection_t3794639002)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3020[2] = 
{
	ProfileCollection_t3794639002::get_offset_of_DefaultProfile_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ProfileCollection_t3794639002::get_offset_of_Profiles_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3021 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3022 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3023 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3024 = { sizeof (ImageTargetAbstractBehaviour_t2347335889), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3024[4] = 
{
	ImageTargetAbstractBehaviour_t2347335889::get_offset_of_mAspectRatio_20(),
	ImageTargetAbstractBehaviour_t2347335889::get_offset_of_mImageTargetType_21(),
	ImageTargetAbstractBehaviour_t2347335889::get_offset_of_mImageTarget_22(),
	ImageTargetAbstractBehaviour_t2347335889::get_offset_of_mVirtualButtonBehaviours_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3025 = { sizeof (MarkerAbstractBehaviour_t865486027), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3025[2] = 
{
	MarkerAbstractBehaviour_t865486027::get_offset_of_mMarkerID_9(),
	MarkerAbstractBehaviour_t865486027::get_offset_of_mMarker_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3026 = { sizeof (MaskOutAbstractBehaviour_t3409682331), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3026[1] = 
{
	MaskOutAbstractBehaviour_t3409682331::get_offset_of_maskMaterial_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3027 = { sizeof (MultiTargetAbstractBehaviour_t3565607731), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3027[1] = 
{
	MultiTargetAbstractBehaviour_t3565607731::get_offset_of_mMultiTarget_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3028 = { sizeof (VuforiaUnity_t1557074901), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3029 = { sizeof (InitError_t432217960)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3029[12] = 
{
	InitError_t432217960::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3030 = { sizeof (VuforiaHint_t1795183225)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3030[4] = 
{
	VuforiaHint_t1795183225::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3031 = { sizeof (StorageType_t2677974245)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3031[4] = 
{
	StorageType_t2677974245::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3032 = { sizeof (VuforiaAbstractBehaviour_t1091759131), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3032[46] = 
{
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_VuforiaLicenseKey_2(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mCameraOffset_3(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mSkewFrustum_4(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_CameraDeviceModeSetting_5(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_MaxSimultaneousImageTargets_6(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_MaxSimultaneousObjectTargets_7(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_UseDelayedLoadingObjectTargets_8(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_CameraDirection_9(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_MirrorVideoBackground_10(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mWorldCenterMode_11(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mWorldCenter_12(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mCentralAnchorPoint_13(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mParentAnchorPoint_14(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mPrimaryCamera_15(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mPrimaryCameraOriginalRect_16(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mSecondaryCamera_17(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mSecondaryCameraOriginalRect_18(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mSecondaryCameraDisabledLocally_19(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mUsingHeadset_20(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mHeadsetID_21(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mEyewearUserCalibrationProfileId_22(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mSynchronizePoseUpdates_23(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mTrackerEventHandlers_24(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mVideoBgEventHandlers_25(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mOnVuforiaInitError_26(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mOnVuforiaInitialized_27(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mOnVuforiaStarted_28(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mOnTrackablesUpdated_29(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mRenderOnUpdate_30(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mOnPause_31(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mOnBackgroundTextureChanged_32(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mStartHasBeenInvoked_33(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mHasStarted_34(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mFailedToInitialize_35(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mBackgroundTextureHasChanged_36(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mInitError_37(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mCameraConfiguration_38(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mClearMaterial_39(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mHasStartedOnce_40(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mWasEnabledBeforePause_41(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mObjectTrackerWasActiveBeforePause_42(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mObjectTrackerWasActiveBeforeDisabling_43(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mMarkerTrackerWasActiveBeforePause_44(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mMarkerTrackerWasActiveBeforeDisabling_45(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mLastUpdatedFrame_46(),
	VuforiaAbstractBehaviour_t1091759131::get_offset_of_mTrackersRequestedToDeinit_47(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3033 = { sizeof (WorldCenterMode_t2257996192)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3033[4] = 
{
	WorldCenterMode_t2257996192::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3034 = { sizeof (VuforiaMacros_t337691955)+ sizeof (Il2CppObject), sizeof(VuforiaMacros_t337691955_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3034[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3035 = { sizeof (VuforiaRuntimeUtilities_t1069467038), -1, sizeof(VuforiaRuntimeUtilities_t1069467038_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3035[1] = 
{
	VuforiaRuntimeUtilities_t1069467038_StaticFields::get_offset_of_sWebCamUsed_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3036 = { sizeof (WebCamUsed_t2616862271)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3036[4] = 
{
	WebCamUsed_t2616862271::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3037 = { sizeof (SurfaceUtilities_t743372383), -1, sizeof(SurfaceUtilities_t743372383_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3037[1] = 
{
	SurfaceUtilities_t743372383_StaticFields::get_offset_of_mScreenOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3038 = { sizeof (TextRecoAbstractBehaviour_t137142681), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3038[12] = 
{
	TextRecoAbstractBehaviour_t137142681::get_offset_of_mHasInitialized_2(),
	TextRecoAbstractBehaviour_t137142681::get_offset_of_mTrackerWasActiveBeforePause_3(),
	TextRecoAbstractBehaviour_t137142681::get_offset_of_mTrackerWasActiveBeforeDisabling_4(),
	TextRecoAbstractBehaviour_t137142681::get_offset_of_mWordListFile_5(),
	TextRecoAbstractBehaviour_t137142681::get_offset_of_mCustomWordListFile_6(),
	TextRecoAbstractBehaviour_t137142681::get_offset_of_mAdditionalCustomWords_7(),
	TextRecoAbstractBehaviour_t137142681::get_offset_of_mFilterMode_8(),
	TextRecoAbstractBehaviour_t137142681::get_offset_of_mFilterListFile_9(),
	TextRecoAbstractBehaviour_t137142681::get_offset_of_mAdditionalFilterWords_10(),
	TextRecoAbstractBehaviour_t137142681::get_offset_of_mWordPrefabCreationMode_11(),
	TextRecoAbstractBehaviour_t137142681::get_offset_of_mMaximumWordInstances_12(),
	TextRecoAbstractBehaviour_t137142681::get_offset_of_mTextRecoEventHandlers_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3039 = { sizeof (SimpleTargetData_t3516126639)+ sizeof (Il2CppObject), sizeof(SimpleTargetData_t3516126639_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3039[2] = 
{
	SimpleTargetData_t3516126639::get_offset_of_id_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SimpleTargetData_t3516126639::get_offset_of_unused_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3040 = { sizeof (TurnOffAbstractBehaviour_t1299549867), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3041 = { sizeof (UserDefinedTargetBuildingAbstractBehaviour_t1531824314), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3041[11] = 
{
	UserDefinedTargetBuildingAbstractBehaviour_t1531824314::get_offset_of_mObjectTracker_2(),
	UserDefinedTargetBuildingAbstractBehaviour_t1531824314::get_offset_of_mLastFrameQuality_3(),
	UserDefinedTargetBuildingAbstractBehaviour_t1531824314::get_offset_of_mCurrentlyScanning_4(),
	UserDefinedTargetBuildingAbstractBehaviour_t1531824314::get_offset_of_mWasScanningBeforeDisable_5(),
	UserDefinedTargetBuildingAbstractBehaviour_t1531824314::get_offset_of_mCurrentlyBuilding_6(),
	UserDefinedTargetBuildingAbstractBehaviour_t1531824314::get_offset_of_mWasBuildingBeforeDisable_7(),
	UserDefinedTargetBuildingAbstractBehaviour_t1531824314::get_offset_of_mOnInitializedCalled_8(),
	UserDefinedTargetBuildingAbstractBehaviour_t1531824314::get_offset_of_mHandlers_9(),
	UserDefinedTargetBuildingAbstractBehaviour_t1531824314::get_offset_of_StopTrackerWhileScanning_10(),
	UserDefinedTargetBuildingAbstractBehaviour_t1531824314::get_offset_of_StartScanningAutomatically_11(),
	UserDefinedTargetBuildingAbstractBehaviour_t1531824314::get_offset_of_StopScanningWhenFinshedBuilding_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3042 = { sizeof (VideoBackgroundAbstractBehaviour_t2475465524), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3042[7] = 
{
	VideoBackgroundAbstractBehaviour_t2475465524::get_offset_of_mClearBuffers_2(),
	VideoBackgroundAbstractBehaviour_t2475465524::get_offset_of_mSkipStateUpdates_3(),
	VideoBackgroundAbstractBehaviour_t2475465524::get_offset_of_mVuforiaAbstractBehaviour_4(),
	VideoBackgroundAbstractBehaviour_t2475465524::get_offset_of_mCamera_5(),
	VideoBackgroundAbstractBehaviour_t2475465524::get_offset_of_mBackgroundBehaviour_6(),
	VideoBackgroundAbstractBehaviour_t2475465524::get_offset_of_mStereoDepth_7(),
	VideoBackgroundAbstractBehaviour_t2475465524::get_offset_of_mDisabledMeshRenderers_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3043 = { sizeof (VideoTextureRendererAbstractBehaviour_t4076028962), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3043[3] = 
{
	VideoTextureRendererAbstractBehaviour_t4076028962::get_offset_of_mTexture_2(),
	VideoTextureRendererAbstractBehaviour_t4076028962::get_offset_of_mVideoBgConfigChanged_3(),
	VideoTextureRendererAbstractBehaviour_t4076028962::get_offset_of_mNativeTextureID_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3044 = { sizeof (VirtualButtonAbstractBehaviour_t1191238304), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3044[14] = 
{
	0,
	VirtualButtonAbstractBehaviour_t1191238304::get_offset_of_mName_3(),
	VirtualButtonAbstractBehaviour_t1191238304::get_offset_of_mSensitivity_4(),
	VirtualButtonAbstractBehaviour_t1191238304::get_offset_of_mHasUpdatedPose_5(),
	VirtualButtonAbstractBehaviour_t1191238304::get_offset_of_mPrevTransform_6(),
	VirtualButtonAbstractBehaviour_t1191238304::get_offset_of_mPrevParent_7(),
	VirtualButtonAbstractBehaviour_t1191238304::get_offset_of_mSensitivityDirty_8(),
	VirtualButtonAbstractBehaviour_t1191238304::get_offset_of_mPreviouslyEnabled_9(),
	VirtualButtonAbstractBehaviour_t1191238304::get_offset_of_mPressed_10(),
	VirtualButtonAbstractBehaviour_t1191238304::get_offset_of_mHandlers_11(),
	VirtualButtonAbstractBehaviour_t1191238304::get_offset_of_mLeftTop_12(),
	VirtualButtonAbstractBehaviour_t1191238304::get_offset_of_mRightBottom_13(),
	VirtualButtonAbstractBehaviour_t1191238304::get_offset_of_mUnregisterOnDestroy_14(),
	VirtualButtonAbstractBehaviour_t1191238304::get_offset_of_mVirtualButton_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3045 = { sizeof (WebCamAbstractBehaviour_t725705546), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3045[7] = 
{
	WebCamAbstractBehaviour_t725705546::get_offset_of_RenderTextureLayer_2(),
	WebCamAbstractBehaviour_t725705546::get_offset_of_mPlayModeRenderVideo_3(),
	WebCamAbstractBehaviour_t725705546::get_offset_of_mDeviceNameSetInEditor_4(),
	WebCamAbstractBehaviour_t725705546::get_offset_of_mFlipHorizontally_5(),
	WebCamAbstractBehaviour_t725705546::get_offset_of_mTurnOffWebCam_6(),
	WebCamAbstractBehaviour_t725705546::get_offset_of_mWebCamImpl_7(),
	WebCamAbstractBehaviour_t725705546::get_offset_of_mBackgroundCameraInstance_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3046 = { sizeof (WordAbstractBehaviour_t545947899), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3046[3] = 
{
	WordAbstractBehaviour_t545947899::get_offset_of_mMode_9(),
	WordAbstractBehaviour_t545947899::get_offset_of_mSpecificWord_10(),
	WordAbstractBehaviour_t545947899::get_offset_of_mWord_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3047 = { sizeof (WordFilterMode_t661135463)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3047[4] = 
{
	WordFilterMode_t661135463::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3048 = { sizeof (U3CPrivateImplementationDetailsU3EU7B7F9F85B4U2DC683U2D4C34U2DB525U2D19C32F1AC11BU7D_t3248040453), -1, sizeof(U3CPrivateImplementationDetailsU3EU7B7F9F85B4U2DC683U2D4C34U2DB525U2D19C32F1AC11BU7D_t3248040453_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3048[1] = 
{
	U3CPrivateImplementationDetailsU3EU7B7F9F85B4U2DC683U2D4C34U2DB525U2D19C32F1AC11BU7D_t3248040453_StaticFields::get_offset_of_U24U24method0x60008eeU2D1_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3049 = { sizeof (__StaticArrayInitTypeSizeU3D24_t3454134163)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D24_t3454134163_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3050 = { sizeof (U3CModuleU3E_t86524805), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3051 = { sizeof (EventHandle_t1938870832)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3051[3] = 
{
	EventHandle_t1938870832::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3052 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3053 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3054 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3055 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3056 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3057 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3058 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3059 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3060 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3061 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3062 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3063 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3064 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3065 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3066 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3067 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3068 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3069 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3070 = { sizeof (EventSystem_t2276120119), -1, sizeof(EventSystem_t2276120119_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3070[10] = 
{
	EventSystem_t2276120119::get_offset_of_m_SystemInputModules_2(),
	EventSystem_t2276120119::get_offset_of_m_CurrentInputModule_3(),
	EventSystem_t2276120119::get_offset_of_m_FirstSelected_4(),
	EventSystem_t2276120119::get_offset_of_m_sendNavigationEvents_5(),
	EventSystem_t2276120119::get_offset_of_m_DragThreshold_6(),
	EventSystem_t2276120119::get_offset_of_m_CurrentSelected_7(),
	EventSystem_t2276120119::get_offset_of_m_SelectionGuard_8(),
	EventSystem_t2276120119::get_offset_of_m_DummyData_9(),
	EventSystem_t2276120119_StaticFields::get_offset_of_s_RaycastComparer_10(),
	EventSystem_t2276120119_StaticFields::get_offset_of_U3CcurrentU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3071 = { sizeof (EventTrigger_t672607302), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3071[2] = 
{
	EventTrigger_t672607302::get_offset_of_m_Delegates_2(),
	EventTrigger_t672607302::get_offset_of_delegates_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3072 = { sizeof (TriggerEvent_t95600550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3073 = { sizeof (Entry_t849715470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3073[2] = 
{
	Entry_t849715470::get_offset_of_eventID_0(),
	Entry_t849715470::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3074 = { sizeof (EventTriggerType_t3843052064)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3074[18] = 
{
	EventTriggerType_t3843052064::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3075 = { sizeof (ExecuteEvents_t2704060668), -1, sizeof(ExecuteEvents_t2704060668_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3075[20] = 
{
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_PointerEnterHandler_0(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_PointerExitHandler_1(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_PointerDownHandler_2(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_PointerUpHandler_3(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_PointerClickHandler_4(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_InitializePotentialDragHandler_5(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_BeginDragHandler_6(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_DragHandler_7(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_EndDragHandler_8(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_DropHandler_9(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_ScrollHandler_10(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_UpdateSelectedHandler_11(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_SelectHandler_12(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_DeselectHandler_13(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_MoveHandler_14(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_SubmitHandler_15(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_CancelHandler_16(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_HandlerListPool_17(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_InternalTransformList_18(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_U3CU3Ef__amU24cache13_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3076 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3077 = { sizeof (MoveDirection_t2840182460)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3077[6] = 
{
	MoveDirection_t2840182460::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3078 = { sizeof (RaycasterManager_t2104732159), -1, sizeof(RaycasterManager_t2104732159_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3078[1] = 
{
	RaycasterManager_t2104732159_StaticFields::get_offset_of_s_Raycasters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3079 = { sizeof (RaycastResult_t3762661364)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3079[10] = 
{
	RaycastResult_t3762661364::get_offset_of_m_GameObject_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_module_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_distance_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_index_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_depth_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_sortingLayer_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_sortingOrder_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_worldPosition_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_worldNormal_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_screenPosition_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3080 = { sizeof (UIBehaviour_t2511441271), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3081 = { sizeof (AxisEventData_t3355659985), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3081[2] = 
{
	AxisEventData_t3355659985::get_offset_of_U3CmoveVectorU3Ek__BackingField_2(),
	AxisEventData_t3355659985::get_offset_of_U3CmoveDirU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3082 = { sizeof (AbstractEventData_t1322796528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3082[1] = 
{
	AbstractEventData_t1322796528::get_offset_of_m_Used_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3083 = { sizeof (BaseEventData_t2054899105), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3083[1] = 
{
	BaseEventData_t2054899105::get_offset_of_m_EventSystem_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3084 = { sizeof (PointerEventData_t1848751023), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3084[21] = 
{
	PointerEventData_t1848751023::get_offset_of_m_PointerPress_2(),
	PointerEventData_t1848751023::get_offset_of_hovered_3(),
	PointerEventData_t1848751023::get_offset_of_U3CpointerEnterU3Ek__BackingField_4(),
	PointerEventData_t1848751023::get_offset_of_U3ClastPressU3Ek__BackingField_5(),
	PointerEventData_t1848751023::get_offset_of_U3CrawPointerPressU3Ek__BackingField_6(),
	PointerEventData_t1848751023::get_offset_of_U3CpointerDragU3Ek__BackingField_7(),
	PointerEventData_t1848751023::get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_8(),
	PointerEventData_t1848751023::get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_9(),
	PointerEventData_t1848751023::get_offset_of_U3CeligibleForClickU3Ek__BackingField_10(),
	PointerEventData_t1848751023::get_offset_of_U3CpointerIdU3Ek__BackingField_11(),
	PointerEventData_t1848751023::get_offset_of_U3CpositionU3Ek__BackingField_12(),
	PointerEventData_t1848751023::get_offset_of_U3CdeltaU3Ek__BackingField_13(),
	PointerEventData_t1848751023::get_offset_of_U3CpressPositionU3Ek__BackingField_14(),
	PointerEventData_t1848751023::get_offset_of_U3CworldPositionU3Ek__BackingField_15(),
	PointerEventData_t1848751023::get_offset_of_U3CworldNormalU3Ek__BackingField_16(),
	PointerEventData_t1848751023::get_offset_of_U3CclickTimeU3Ek__BackingField_17(),
	PointerEventData_t1848751023::get_offset_of_U3CclickCountU3Ek__BackingField_18(),
	PointerEventData_t1848751023::get_offset_of_U3CscrollDeltaU3Ek__BackingField_19(),
	PointerEventData_t1848751023::get_offset_of_U3CuseDragThresholdU3Ek__BackingField_20(),
	PointerEventData_t1848751023::get_offset_of_U3CdraggingU3Ek__BackingField_21(),
	PointerEventData_t1848751023::get_offset_of_U3CbuttonU3Ek__BackingField_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3085 = { sizeof (InputButton_t384979233)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3085[4] = 
{
	InputButton_t384979233::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3086 = { sizeof (FramePressState_t2820857440)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3086[5] = 
{
	FramePressState_t2820857440::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3087 = { sizeof (BaseInputModule_t15847059), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3087[4] = 
{
	BaseInputModule_t15847059::get_offset_of_m_RaycastResultCache_2(),
	BaseInputModule_t15847059::get_offset_of_m_AxisEventData_3(),
	BaseInputModule_t15847059::get_offset_of_m_EventSystem_4(),
	BaseInputModule_t15847059::get_offset_of_m_BaseEventData_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3088 = { sizeof (PointerInputModule_t3771003169), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3088[6] = 
{
	0,
	0,
	0,
	0,
	PointerInputModule_t3771003169::get_offset_of_m_PointerData_10(),
	PointerInputModule_t3771003169::get_offset_of_m_MouseState_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3089 = { sizeof (ButtonState_t2039702646), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3089[2] = 
{
	ButtonState_t2039702646::get_offset_of_m_Button_0(),
	ButtonState_t2039702646::get_offset_of_m_EventData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3090 = { sizeof (MouseState_t3613479957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3090[1] = 
{
	MouseState_t3613479957::get_offset_of_m_TrackedButtons_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3091 = { sizeof (MouseButtonEventData_t4082623702), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3091[2] = 
{
	MouseButtonEventData_t4082623702::get_offset_of_buttonState_0(),
	MouseButtonEventData_t4082623702::get_offset_of_buttonData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3092 = { sizeof (StandaloneInputModule_t1096194655), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3092[12] = 
{
	StandaloneInputModule_t1096194655::get_offset_of_m_PrevActionTime_12(),
	StandaloneInputModule_t1096194655::get_offset_of_m_LastMoveVector_13(),
	StandaloneInputModule_t1096194655::get_offset_of_m_ConsecutiveMoveCount_14(),
	StandaloneInputModule_t1096194655::get_offset_of_m_LastMousePosition_15(),
	StandaloneInputModule_t1096194655::get_offset_of_m_MousePosition_16(),
	StandaloneInputModule_t1096194655::get_offset_of_m_HorizontalAxis_17(),
	StandaloneInputModule_t1096194655::get_offset_of_m_VerticalAxis_18(),
	StandaloneInputModule_t1096194655::get_offset_of_m_SubmitButton_19(),
	StandaloneInputModule_t1096194655::get_offset_of_m_CancelButton_20(),
	StandaloneInputModule_t1096194655::get_offset_of_m_InputActionsPerSecond_21(),
	StandaloneInputModule_t1096194655::get_offset_of_m_RepeatDelay_22(),
	StandaloneInputModule_t1096194655::get_offset_of_m_ForceModuleActive_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3093 = { sizeof (InputMode_t1573608962)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3093[3] = 
{
	InputMode_t1573608962::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3094 = { sizeof (TouchInputModule_t894675487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3094[3] = 
{
	TouchInputModule_t894675487::get_offset_of_m_LastMousePosition_12(),
	TouchInputModule_t894675487::get_offset_of_m_MousePosition_13(),
	TouchInputModule_t894675487::get_offset_of_m_ForceModuleActive_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3095 = { sizeof (BaseRaycaster_t2327671059), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3096 = { sizeof (Physics2DRaycaster_t935388869), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3097 = { sizeof (PhysicsRaycaster_t1170055767), -1, sizeof(PhysicsRaycaster_t1170055767_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3097[4] = 
{
	0,
	PhysicsRaycaster_t1170055767::get_offset_of_m_EventCamera_3(),
	PhysicsRaycaster_t1170055767::get_offset_of_m_EventMask_4(),
	PhysicsRaycaster_t1170055767_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3098 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3099 = { sizeof (ColorTween_t723277650)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3099[6] = 
{
	ColorTween_t723277650::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t723277650::get_offset_of_m_StartColor_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t723277650::get_offset_of_m_TargetColor_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t723277650::get_offset_of_m_TweenMode_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t723277650::get_offset_of_m_Duration_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t723277650::get_offset_of_m_IgnoreTimeScale_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
