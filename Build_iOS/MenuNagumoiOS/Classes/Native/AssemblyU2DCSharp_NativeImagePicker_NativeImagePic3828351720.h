﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// NativeImagePicker/CallbackImagePicked
struct CallbackImagePicked_t554520473;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeImagePicker/NativeImagePickerBehaviour
struct  NativeImagePickerBehaviour_t3828351720  : public MonoBehaviour_t667441552
{
public:
	// NativeImagePicker/CallbackImagePicked NativeImagePicker/NativeImagePickerBehaviour::Callback
	CallbackImagePicked_t554520473 * ___Callback_2;

public:
	inline static int32_t get_offset_of_Callback_2() { return static_cast<int32_t>(offsetof(NativeImagePickerBehaviour_t3828351720, ___Callback_2)); }
	inline CallbackImagePicked_t554520473 * get_Callback_2() const { return ___Callback_2; }
	inline CallbackImagePicked_t554520473 ** get_address_of_Callback_2() { return &___Callback_2; }
	inline void set_Callback_2(CallbackImagePicked_t554520473 * value)
	{
		___Callback_2 = value;
		Il2CppCodeGenWriteBarrier(&___Callback_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
