﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>
struct Dictionary_2_t1182116501;
// System.Collections.Generic.IEqualityComparer`1<ZXing.DecodeHintType>
struct IEqualityComparer_1_t2886815753;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<ZXing.DecodeHintType>
struct ICollection_1_t2990371336;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<ZXing.DecodeHintType,System.Object>[]
struct KeyValuePair_2U5BU5D_t3618423950;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<ZXing.DecodeHintType,System.Object>>
struct IEnumerator_1_t2992762256;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.DecodeHintType,System.Object>
struct KeyCollection_t2808875952;
// System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.DecodeHintType,System.Object>
struct ValueCollection_t4177689510;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21080897207.h"
#include "mscorlib_System_Array1146569071.h"
#include "QRCode_ZXing_DecodeHintType2095781349.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2499439893.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m1479402237_gshared (Dictionary_2_t1182116501 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m1479402237(__this, method) ((  void (*) (Dictionary_2_t1182116501 *, const MethodInfo*))Dictionary_2__ctor_m1479402237_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3325055005_gshared (Dictionary_2_t1182116501 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m3325055005(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t1182116501 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3325055005_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m1024593143_gshared (Dictionary_2_t1182116501 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m1024593143(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t1182116501 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m1024593143_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m796122343_gshared (Dictionary_2_t1182116501 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m796122343(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1182116501 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m796122343_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m727612402_gshared (Dictionary_2_t1182116501 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m727612402(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1182116501 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m727612402_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m4019237732_gshared (Dictionary_2_t1182116501 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m4019237732(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1182116501 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m4019237732_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m3679416146_gshared (Dictionary_2_t1182116501 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m3679416146(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1182116501 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m3679416146_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m800369975_gshared (Dictionary_2_t1182116501 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m800369975(__this, method) ((  bool (*) (Dictionary_2_t1182116501 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m800369975_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3112226530_gshared (Dictionary_2_t1182116501 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3112226530(__this, method) ((  bool (*) (Dictionary_2_t1182116501 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3112226530_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m3379747224_gshared (Dictionary_2_t1182116501 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m3379747224(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1182116501 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m3379747224_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m3460438599_gshared (Dictionary_2_t1182116501 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m3460438599(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1182116501 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m3460438599_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m294687658_gshared (Dictionary_2_t1182116501 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m294687658(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1182116501 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m294687658_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m2635696072_gshared (Dictionary_2_t1182116501 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m2635696072(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1182116501 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m2635696072_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m3085435525_gshared (Dictionary_2_t1182116501 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m3085435525(__this, ___key0, method) ((  void (*) (Dictionary_2_t1182116501 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m3085435525_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1970437260_gshared (Dictionary_2_t1182116501 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1970437260(__this, method) ((  bool (*) (Dictionary_2_t1182116501 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1970437260_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3694789374_gshared (Dictionary_2_t1182116501 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3694789374(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1182116501 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3694789374_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1550806480_gshared (Dictionary_2_t1182116501 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1550806480(__this, method) ((  bool (*) (Dictionary_2_t1182116501 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1550806480_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m828958875_gshared (Dictionary_2_t1182116501 * __this, KeyValuePair_2_t1080897207  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m828958875(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t1182116501 *, KeyValuePair_2_t1080897207 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m828958875_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m476679755_gshared (Dictionary_2_t1182116501 * __this, KeyValuePair_2_t1080897207  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m476679755(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1182116501 *, KeyValuePair_2_t1080897207 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m476679755_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1665468351_gshared (Dictionary_2_t1182116501 * __this, KeyValuePair_2U5BU5D_t3618423950* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1665468351(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1182116501 *, KeyValuePair_2U5BU5D_t3618423950*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1665468351_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1963772976_gshared (Dictionary_2_t1182116501 * __this, KeyValuePair_2_t1080897207  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1963772976(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1182116501 *, KeyValuePair_2_t1080897207 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1963772976_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m1341127518_gshared (Dictionary_2_t1182116501 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m1341127518(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1182116501 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m1341127518_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2612964589_gshared (Dictionary_2_t1182116501 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2612964589(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1182116501 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2612964589_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3303191268_gshared (Dictionary_2_t1182116501 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3303191268(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1182116501 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3303191268_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1174827953_gshared (Dictionary_2_t1182116501 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1174827953(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1182116501 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1174827953_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m606424518_gshared (Dictionary_2_t1182116501 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m606424518(__this, method) ((  int32_t (*) (Dictionary_2_t1182116501 *, const MethodInfo*))Dictionary_2_get_Count_m606424518_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m2099528705_gshared (Dictionary_2_t1182116501 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m2099528705(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1182116501 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m2099528705_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m1992375270_gshared (Dictionary_2_t1182116501 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m1992375270(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1182116501 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m1992375270_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m2535114782_gshared (Dictionary_2_t1182116501 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m2535114782(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t1182116501 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m2535114782_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m1666751321_gshared (Dictionary_2_t1182116501 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m1666751321(__this, ___size0, method) ((  void (*) (Dictionary_2_t1182116501 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m1666751321_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m2339907477_gshared (Dictionary_2_t1182116501 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m2339907477(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1182116501 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2339907477_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t1080897207  Dictionary_2_make_pair_m3042623017_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m3042623017(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t1080897207  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m3042623017_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::pick_key(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_key_m3025668981_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m3025668981(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_key_m3025668981_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m2575799697_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m2575799697(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m2575799697_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m998309530_gshared (Dictionary_2_t1182116501 * __this, KeyValuePair_2U5BU5D_t3618423950* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m998309530(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1182116501 *, KeyValuePair_2U5BU5D_t3618423950*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m998309530_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m1903680594_gshared (Dictionary_2_t1182116501 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m1903680594(__this, method) ((  void (*) (Dictionary_2_t1182116501 *, const MethodInfo*))Dictionary_2_Resize_m1903680594_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m1305243343_gshared (Dictionary_2_t1182116501 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m1305243343(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1182116501 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m1305243343_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m790813905_gshared (Dictionary_2_t1182116501 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m790813905(__this, method) ((  void (*) (Dictionary_2_t1182116501 *, const MethodInfo*))Dictionary_2_Clear_m790813905_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m3978604859_gshared (Dictionary_2_t1182116501 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m3978604859(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1182116501 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m3978604859_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m1895755707_gshared (Dictionary_2_t1182116501 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m1895755707(__this, ___value0, method) ((  bool (*) (Dictionary_2_t1182116501 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m1895755707_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m634998084_gshared (Dictionary_2_t1182116501 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m634998084(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1182116501 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m634998084_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m22390688_gshared (Dictionary_2_t1182116501 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m22390688(__this, ___sender0, method) ((  void (*) (Dictionary_2_t1182116501 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m22390688_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m2300210901_gshared (Dictionary_2_t1182116501 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m2300210901(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1182116501 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m2300210901_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m355835540_gshared (Dictionary_2_t1182116501 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m355835540(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t1182116501 *, int32_t, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m355835540_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::get_Keys()
extern "C"  KeyCollection_t2808875952 * Dictionary_2_get_Keys_m2046863751_gshared (Dictionary_2_t1182116501 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m2046863751(__this, method) ((  KeyCollection_t2808875952 * (*) (Dictionary_2_t1182116501 *, const MethodInfo*))Dictionary_2_get_Keys_m2046863751_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::get_Values()
extern "C"  ValueCollection_t4177689510 * Dictionary_2_get_Values_m3976879331_gshared (Dictionary_2_t1182116501 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m3976879331(__this, method) ((  ValueCollection_t4177689510 * (*) (Dictionary_2_t1182116501 *, const MethodInfo*))Dictionary_2_get_Values_m3976879331_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m2475527888_gshared (Dictionary_2_t1182116501 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m2475527888(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t1182116501 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m2475527888_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m888113836_gshared (Dictionary_2_t1182116501 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m888113836(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t1182116501 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m888113836_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m4045042872_gshared (Dictionary_2_t1182116501 * __this, KeyValuePair_2_t1080897207  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m4045042872(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t1182116501 *, KeyValuePair_2_t1080897207 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m4045042872_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::GetEnumerator()
extern "C"  Enumerator_t2499439893  Dictionary_2_GetEnumerator_m3828644017_gshared (Dictionary_2_t1182116501 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m3828644017(__this, method) ((  Enumerator_t2499439893  (*) (Dictionary_2_t1182116501 *, const MethodInfo*))Dictionary_2_GetEnumerator_m3828644017_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>::<CopyTo>m__2(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__2_m3882704934_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__2_m3882704934(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__2_m3882704934_gshared)(__this /* static, unused */, ___key0, ___value1, method)
