﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream
struct PartialInputStream_t2132028793;
// ICSharpCode.SharpZipLib.Zip.ZipFile
struct ZipFile_t2937401711;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipF2937401711.h"
#include "mscorlib_System_IO_SeekOrigin4120335598.h"

// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::.ctor(ICSharpCode.SharpZipLib.Zip.ZipFile,System.Int64,System.Int64)
extern "C"  void PartialInputStream__ctor_m1778748337 (PartialInputStream_t2132028793 * __this, ZipFile_t2937401711 * ___zipFile0, int64_t ___start1, int64_t ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::ReadByte()
extern "C"  int32_t PartialInputStream_ReadByte_m1717606414 (PartialInputStream_t2132028793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::Close()
extern "C"  void PartialInputStream_Close_m3599000664 (PartialInputStream_t2132028793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t PartialInputStream_Read_m2442014147 (PartialInputStream_t2132028793 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C"  void PartialInputStream_Write_m221362826 (PartialInputStream_t2132028793 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::SetLength(System.Int64)
extern "C"  void PartialInputStream_SetLength_m3806063002 (PartialInputStream_t2132028793 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::Seek(System.Int64,System.IO.SeekOrigin)
extern "C"  int64_t PartialInputStream_Seek_m1527126180 (PartialInputStream_t2132028793 * __this, int64_t ___offset0, int32_t ___origin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::Flush()
extern "C"  void PartialInputStream_Flush_m1972088420 (PartialInputStream_t2132028793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::get_Position()
extern "C"  int64_t PartialInputStream_get_Position_m2721460867 (PartialInputStream_t2132028793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::set_Position(System.Int64)
extern "C"  void PartialInputStream_set_Position_m2441911418 (PartialInputStream_t2132028793 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::get_Length()
extern "C"  int64_t PartialInputStream_get_Length_m744500032 (PartialInputStream_t2132028793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::get_CanWrite()
extern "C"  bool PartialInputStream_get_CanWrite_m3831673646 (PartialInputStream_t2132028793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::get_CanSeek()
extern "C"  bool PartialInputStream_get_CanSeek_m689686955 (PartialInputStream_t2132028793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::get_CanRead()
extern "C"  bool PartialInputStream_get_CanRead_m660931913 (PartialInputStream_t2132028793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::get_CanTimeout()
extern "C"  bool PartialInputStream_get_CanTimeout_m2910586032 (PartialInputStream_t2132028793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
