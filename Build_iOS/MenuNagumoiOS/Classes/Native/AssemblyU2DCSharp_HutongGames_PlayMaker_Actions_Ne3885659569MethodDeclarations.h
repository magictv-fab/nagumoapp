﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkSetMaximumConnections
struct NetworkSetMaximumConnections_t3885659569;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkSetMaximumConnections::.ctor()
extern "C"  void NetworkSetMaximumConnections__ctor_m86761429 (NetworkSetMaximumConnections_t3885659569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkSetMaximumConnections::Reset()
extern "C"  void NetworkSetMaximumConnections_Reset_m2028161666 (NetworkSetMaximumConnections_t3885659569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkSetMaximumConnections::OnEnter()
extern "C"  void NetworkSetMaximumConnections_OnEnter_m1165083244 (NetworkSetMaximumConnections_t3885659569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Actions.NetworkSetMaximumConnections::ErrorCheck()
extern "C"  String_t* NetworkSetMaximumConnections_ErrorCheck_m981265234 (NetworkSetMaximumConnections_t3885659569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
