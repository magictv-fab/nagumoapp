﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ConvertBoolToInt
struct ConvertBoolToInt_t3932551647;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToInt::.ctor()
extern "C"  void ConvertBoolToInt__ctor_m1244959591 (ConvertBoolToInt_t3932551647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToInt::Reset()
extern "C"  void ConvertBoolToInt_Reset_m3186359828 (ConvertBoolToInt_t3932551647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToInt::OnEnter()
extern "C"  void ConvertBoolToInt_OnEnter_m1796987262 (ConvertBoolToInt_t3932551647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToInt::OnUpdate()
extern "C"  void ConvertBoolToInt_OnUpdate_m3300556933 (ConvertBoolToInt_t3932551647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToInt::DoConvertBoolToInt()
extern "C"  void ConvertBoolToInt_DoConvertBoolToInt_m391389279 (ConvertBoolToInt_t3932551647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
