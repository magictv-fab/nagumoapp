﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo1432926611MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.OneD.RSS.Expanded.ExpandedPair>::.ctor(System.Collections.Generic.IList`1<T>)
#define ReadOnlyCollection_1__ctor_m3928107914(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t675476927 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1366664402_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.OneD.RSS.Expanded.ExpandedPair>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1687259252(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t675476927 *, ExpandedPair_t3413366687 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2541166012_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.OneD.RSS.Expanded.ExpandedPair>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2772120278(__this, method) ((  void (*) (ReadOnlyCollection_1_t675476927 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3473426062_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.OneD.RSS.Expanded.ExpandedPair>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m996477531(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t675476927 *, int32_t, ExpandedPair_t3413366687 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3496388003_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.OneD.RSS.Expanded.ExpandedPair>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1267515843(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t675476927 *, ExpandedPair_t3413366687 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m348744375_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.OneD.RSS.Expanded.ExpandedPair>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3165297697(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t675476927 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1370240873_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.OneD.RSS.Expanded.ExpandedPair>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m317412487(__this, ___index0, method) ((  ExpandedPair_t3413366687 * (*) (ReadOnlyCollection_1_t675476927 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3534609325_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.OneD.RSS.Expanded.ExpandedPair>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1646796914(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t675476927 *, int32_t, ExpandedPair_t3413366687 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3174042042_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.OneD.RSS.Expanded.ExpandedPair>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m793053804(__this, method) ((  bool (*) (ReadOnlyCollection_1_t675476927 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2459576056_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.OneD.RSS.Expanded.ExpandedPair>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m487904569(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t675476927 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1945557633_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.OneD.RSS.Expanded.ExpandedPair>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1109239304(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t675476927 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3330065468_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.OneD.RSS.Expanded.ExpandedPair>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m3602887861(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t675476927 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1628967861_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.OneD.RSS.Expanded.ExpandedPair>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m4137486407(__this, method) ((  void (*) (ReadOnlyCollection_1_t675476927 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m514207119_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.OneD.RSS.Expanded.ExpandedPair>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m3577222187(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t675476927 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m736178103_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.OneD.RSS.Expanded.ExpandedPair>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m16550029(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t675476927 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3658311565_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.OneD.RSS.Expanded.ExpandedPair>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m2635992960(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t675476927 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2823806264_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.OneD.RSS.Expanded.ExpandedPair>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m3029494184(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t675476927 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2498539760_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.OneD.RSS.Expanded.ExpandedPair>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1010394896(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t675476927 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1730676936_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.OneD.RSS.Expanded.ExpandedPair>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2292600657(__this, method) ((  bool (*) (ReadOnlyCollection_1_t675476927 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1373829189_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.OneD.RSS.Expanded.ExpandedPair>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2586712131(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t675476927 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m918746289_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.OneD.RSS.Expanded.ExpandedPair>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3544252378(__this, method) ((  bool (*) (ReadOnlyCollection_1_t675476927 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m932754534_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.OneD.RSS.Expanded.ExpandedPair>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m429792223(__this, method) ((  bool (*) (ReadOnlyCollection_1_t675476927 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2423760339_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.OneD.RSS.Expanded.ExpandedPair>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m109905162(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t675476927 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3512499704_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.OneD.RSS.Expanded.ExpandedPair>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m4067449687(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t675476927 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m4167408399_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.OneD.RSS.Expanded.ExpandedPair>::Contains(T)
#define ReadOnlyCollection_1_Contains_m1783336456(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t675476927 *, ExpandedPair_t3413366687 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m687553276_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.OneD.RSS.Expanded.ExpandedPair>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m2622617508(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t675476927 *, ExpandedPairU5BU5D_t1339167494*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m475587820_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.OneD.RSS.Expanded.ExpandedPair>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m4294714591(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t675476927 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m809369055_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.OneD.RSS.Expanded.ExpandedPair>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m3755257712(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t675476927 *, ExpandedPair_t3413366687 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m817393776_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.OneD.RSS.Expanded.ExpandedPair>::get_Count()
#define ReadOnlyCollection_1_get_Count_m266179595(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t675476927 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3681678091_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<ZXing.OneD.RSS.Expanded.ExpandedPair>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m3432328647(__this, ___index0, method) ((  ExpandedPair_t3413366687 * (*) (ReadOnlyCollection_1_t675476927 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2421641197_gshared)(__this, ___index0, method)
