﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerProxyBase
struct PlayMakerProxyBase_t3469687535;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayMakerProxyBase::Awake()
extern "C"  void PlayMakerProxyBase_Awake_m1062924081 (PlayMakerProxyBase_t3469687535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerProxyBase::Reset()
extern "C"  void PlayMakerProxyBase_Reset_m2766719099 (PlayMakerProxyBase_t3469687535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerProxyBase::.ctor()
extern "C"  void PlayMakerProxyBase__ctor_m825318862 (PlayMakerProxyBase_t3469687535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
