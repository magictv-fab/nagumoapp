﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OSEmailSubscriptionState
struct OSEmailSubscriptionState_t2956802108;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSEmailSubscriptionStateChanges
struct  OSEmailSubscriptionStateChanges_t721307367  : public Il2CppObject
{
public:
	// OSEmailSubscriptionState OSEmailSubscriptionStateChanges::to
	OSEmailSubscriptionState_t2956802108 * ___to_0;
	// OSEmailSubscriptionState OSEmailSubscriptionStateChanges::from
	OSEmailSubscriptionState_t2956802108 * ___from_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(OSEmailSubscriptionStateChanges_t721307367, ___to_0)); }
	inline OSEmailSubscriptionState_t2956802108 * get_to_0() const { return ___to_0; }
	inline OSEmailSubscriptionState_t2956802108 ** get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(OSEmailSubscriptionState_t2956802108 * value)
	{
		___to_0 = value;
		Il2CppCodeGenWriteBarrier(&___to_0, value);
	}

	inline static int32_t get_offset_of_from_1() { return static_cast<int32_t>(offsetof(OSEmailSubscriptionStateChanges_t721307367, ___from_1)); }
	inline OSEmailSubscriptionState_t2956802108 * get_from_1() const { return ___from_1; }
	inline OSEmailSubscriptionState_t2956802108 ** get_address_of_from_1() { return &___from_1; }
	inline void set_from_1(OSEmailSubscriptionState_t2956802108 * value)
	{
		___from_1 = value;
		Il2CppCodeGenWriteBarrier(&___from_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
