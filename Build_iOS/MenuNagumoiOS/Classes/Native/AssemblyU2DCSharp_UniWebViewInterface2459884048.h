﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewInterface
struct  UniWebViewInterface_t2459884048  : public Il2CppObject
{
public:

public:
};

struct UniWebViewInterface_t2459884048_StaticFields
{
public:
	// System.Boolean UniWebViewInterface::correctPlatform
	bool ___correctPlatform_1;

public:
	inline static int32_t get_offset_of_correctPlatform_1() { return static_cast<int32_t>(offsetof(UniWebViewInterface_t2459884048_StaticFields, ___correctPlatform_1)); }
	inline bool get_correctPlatform_1() const { return ___correctPlatform_1; }
	inline bool* get_address_of_correctPlatform_1() { return &___correctPlatform_1; }
	inline void set_correctPlatform_1(bool value)
	{
		___correctPlatform_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
