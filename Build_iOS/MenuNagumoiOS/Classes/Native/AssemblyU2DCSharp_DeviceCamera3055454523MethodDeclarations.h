﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DeviceCamera
struct DeviceCamera_t3055454523;
// UnityEngine.Texture
struct Texture_t2526458961;
// UnityEngine.Color[]
struct ColorU5BU5D_t2441545636;
// UnityEngine.Color32[]
struct Color32U5BU5D_t2960766953;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void DeviceCamera::.ctor(System.Boolean)
extern "C"  void DeviceCamera__ctor_m3861627191 (DeviceCamera_t3055454523 * __this, bool ___isUseEWC0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceCamera::.ctor(System.Int32,System.Int32,System.Boolean)
extern "C"  void DeviceCamera__ctor_m4151339863 (DeviceCamera_t3055454523 * __this, int32_t ___width0, int32_t ___height1, bool ___isUseEWC2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture DeviceCamera::get_preview()
extern "C"  Texture_t2526458961 * DeviceCamera_get_preview_m2916306158 (DeviceCamera_t3055454523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceCamera::Play()
extern "C"  void DeviceCamera_Play_m2564630456 (DeviceCamera_t3055454523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceCamera::Stop()
extern "C"  void DeviceCamera_Stop_m2658314502 (DeviceCamera_t3055454523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 DeviceCamera::getSize()
extern "C"  Vector2_t4282066565  DeviceCamera_getSize_m2326501562 (DeviceCamera_t3055454523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DeviceCamera::Width()
extern "C"  int32_t DeviceCamera_Width_m3725862870 (DeviceCamera_t3055454523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DeviceCamera::Height()
extern "C"  int32_t DeviceCamera_Height_m41446329 (DeviceCamera_t3055454523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DeviceCamera::isPlaying()
extern "C"  bool DeviceCamera_isPlaying_m1155671278 (DeviceCamera_t3055454523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color[] DeviceCamera::GetPixels()
extern "C"  ColorU5BU5D_t2441545636* DeviceCamera_GetPixels_m3826898300 (DeviceCamera_t3055454523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color[] DeviceCamera::GetPixels(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  ColorU5BU5D_t2441545636* DeviceCamera_GetPixels_m196851210 (DeviceCamera_t3055454523 * __this, int32_t ___x0, int32_t ___y1, int32_t ___targetWidth2, int32_t ___targetHeight3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32[] DeviceCamera::GetPixels32()
extern "C"  Color32U5BU5D_t2960766953* DeviceCamera_GetPixels32_m3870675770 (DeviceCamera_t3055454523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
