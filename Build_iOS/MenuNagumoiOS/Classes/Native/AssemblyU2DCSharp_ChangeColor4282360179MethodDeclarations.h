﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChangeColor
struct ChangeColor_t4282360179;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1848751023;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1848751023.h"

// System.Void ChangeColor::.ctor()
extern "C"  void ChangeColor__ctor_m4159621976 (ChangeColor_t4282360179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeColor::OnEnable()
extern "C"  void ChangeColor_OnEnable_m2973441326 (ChangeColor_t4282360179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeColor::SetRed(System.Single)
extern "C"  void ChangeColor_SetRed_m412170320 (ChangeColor_t4282360179 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeColor::SetGreen(System.Single)
extern "C"  void ChangeColor_SetGreen_m62610910 (ChangeColor_t4282360179 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeColor::SetBlue(System.Single)
extern "C"  void ChangeColor_SetBlue_m2265329369 (ChangeColor_t4282360179 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeColor::OnValueChanged(System.Single,System.Int32)
extern "C"  void ChangeColor_OnValueChanged_m684204986 (ChangeColor_t4282360179 * __this, float ___value0, int32_t ___channel1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeColor::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ChangeColor_OnPointerClick_m302730952 (ChangeColor_t4282360179 * __this, PointerEventData_t1848751023 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
