﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// MagicTV.abstracts.device.DeviceFileInfoAbstract
struct DeviceFileInfoAbstract_t3788299492;
// System.Collections.Generic.Dictionary`2<System.String,ARM.components.GroupInAppAbstractComponentsManager>
struct Dictionary_2_t1559753164;
// MagicTV.abstracts.InAppAbstract[]
struct InAppAbstractU5BU5D_t412350073;
// System.Collections.Generic.Dictionary`2<System.String,MagicTV.abstracts.InAppAbstract>
struct Dictionary_2_t1203091498;
// ARM.components.GroupInAppAbstractComponentsManager
struct GroupInAppAbstractComponentsManager_t739334794;
// MagicTV.globals.InAppAnalytics
struct InAppAnalytics_t2316734034;

#include "AssemblyU2DCSharp_MagicTV_abstracts_InAppManagerAb3506247605.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.in_apps.InAppManager
struct  InAppManager_t3917644817  : public InAppManagerAbstract_t3506247605
{
public:
	// System.Boolean MagicTV.in_apps.InAppManager::_chekStartVuforia
	bool ____chekStartVuforia_9;
	// System.String MagicTV.in_apps.InAppManager::DebugTrackSimulate
	String_t* ___DebugTrackSimulate_10;
	// System.Boolean MagicTV.in_apps.InAppManager::DebugEventTrackInSimulate
	bool ___DebugEventTrackInSimulate_11;
	// System.Boolean MagicTV.in_apps.InAppManager::DebugEventTrackOutSimulate
	bool ___DebugEventTrackOutSimulate_12;
	// MagicTV.abstracts.device.DeviceFileInfoAbstract MagicTV.in_apps.InAppManager::deviceFileInfo
	DeviceFileInfoAbstract_t3788299492 * ___deviceFileInfo_14;
	// System.Collections.Generic.Dictionary`2<System.String,ARM.components.GroupInAppAbstractComponentsManager> MagicTV.in_apps.InAppManager::_inAppMemoryCache
	Dictionary_2_t1559753164 * ____inAppMemoryCache_15;
	// System.Collections.Generic.Dictionary`2<System.String,ARM.components.GroupInAppAbstractComponentsManager> MagicTV.in_apps.InAppManager::_inAppMemoryCacheByInAppsOpened
	Dictionary_2_t1559753164 * ____inAppMemoryCacheByInAppsOpened_16;
	// UnityEngine.GameObject MagicTV.in_apps.InAppManager::loader
	GameObject_t3674682005 * ___loader_17;
	// UnityEngine.GameObject MagicTV.in_apps.InAppManager::luzes
	GameObject_t3674682005 * ___luzes_18;
	// System.String MagicTV.in_apps.InAppManager::inAppName
	String_t* ___inAppName_19;
	// System.Boolean MagicTV.in_apps.InAppManager::addInAppNow
	bool ___addInAppNow_20;
	// MagicTV.abstracts.InAppAbstract[] MagicTV.in_apps.InAppManager::InAppTypes
	InAppAbstractU5BU5D_t412350073* ___InAppTypes_21;
	// System.Collections.Generic.Dictionary`2<System.String,MagicTV.abstracts.InAppAbstract> MagicTV.in_apps.InAppManager::_inAppTypesList
	Dictionary_2_t1203091498 * ____inAppTypesList_22;
	// ARM.components.GroupInAppAbstractComponentsManager MagicTV.in_apps.InAppManager::_currentPresentation
	GroupInAppAbstractComponentsManager_t739334794 * ____currentPresentation_23;
	// MagicTV.globals.InAppAnalytics MagicTV.in_apps.InAppManager::_currentAnalytics
	InAppAnalytics_t2316734034 * ____currentAnalytics_24;
	// System.String MagicTV.in_apps.InAppManager::_lastAppName
	String_t* ____lastAppName_25;
	// System.String MagicTV.in_apps.InAppManager::_appNameToRunNow
	String_t* ____appNameToRunNow_26;

public:
	inline static int32_t get_offset_of__chekStartVuforia_9() { return static_cast<int32_t>(offsetof(InAppManager_t3917644817, ____chekStartVuforia_9)); }
	inline bool get__chekStartVuforia_9() const { return ____chekStartVuforia_9; }
	inline bool* get_address_of__chekStartVuforia_9() { return &____chekStartVuforia_9; }
	inline void set__chekStartVuforia_9(bool value)
	{
		____chekStartVuforia_9 = value;
	}

	inline static int32_t get_offset_of_DebugTrackSimulate_10() { return static_cast<int32_t>(offsetof(InAppManager_t3917644817, ___DebugTrackSimulate_10)); }
	inline String_t* get_DebugTrackSimulate_10() const { return ___DebugTrackSimulate_10; }
	inline String_t** get_address_of_DebugTrackSimulate_10() { return &___DebugTrackSimulate_10; }
	inline void set_DebugTrackSimulate_10(String_t* value)
	{
		___DebugTrackSimulate_10 = value;
		Il2CppCodeGenWriteBarrier(&___DebugTrackSimulate_10, value);
	}

	inline static int32_t get_offset_of_DebugEventTrackInSimulate_11() { return static_cast<int32_t>(offsetof(InAppManager_t3917644817, ___DebugEventTrackInSimulate_11)); }
	inline bool get_DebugEventTrackInSimulate_11() const { return ___DebugEventTrackInSimulate_11; }
	inline bool* get_address_of_DebugEventTrackInSimulate_11() { return &___DebugEventTrackInSimulate_11; }
	inline void set_DebugEventTrackInSimulate_11(bool value)
	{
		___DebugEventTrackInSimulate_11 = value;
	}

	inline static int32_t get_offset_of_DebugEventTrackOutSimulate_12() { return static_cast<int32_t>(offsetof(InAppManager_t3917644817, ___DebugEventTrackOutSimulate_12)); }
	inline bool get_DebugEventTrackOutSimulate_12() const { return ___DebugEventTrackOutSimulate_12; }
	inline bool* get_address_of_DebugEventTrackOutSimulate_12() { return &___DebugEventTrackOutSimulate_12; }
	inline void set_DebugEventTrackOutSimulate_12(bool value)
	{
		___DebugEventTrackOutSimulate_12 = value;
	}

	inline static int32_t get_offset_of_deviceFileInfo_14() { return static_cast<int32_t>(offsetof(InAppManager_t3917644817, ___deviceFileInfo_14)); }
	inline DeviceFileInfoAbstract_t3788299492 * get_deviceFileInfo_14() const { return ___deviceFileInfo_14; }
	inline DeviceFileInfoAbstract_t3788299492 ** get_address_of_deviceFileInfo_14() { return &___deviceFileInfo_14; }
	inline void set_deviceFileInfo_14(DeviceFileInfoAbstract_t3788299492 * value)
	{
		___deviceFileInfo_14 = value;
		Il2CppCodeGenWriteBarrier(&___deviceFileInfo_14, value);
	}

	inline static int32_t get_offset_of__inAppMemoryCache_15() { return static_cast<int32_t>(offsetof(InAppManager_t3917644817, ____inAppMemoryCache_15)); }
	inline Dictionary_2_t1559753164 * get__inAppMemoryCache_15() const { return ____inAppMemoryCache_15; }
	inline Dictionary_2_t1559753164 ** get_address_of__inAppMemoryCache_15() { return &____inAppMemoryCache_15; }
	inline void set__inAppMemoryCache_15(Dictionary_2_t1559753164 * value)
	{
		____inAppMemoryCache_15 = value;
		Il2CppCodeGenWriteBarrier(&____inAppMemoryCache_15, value);
	}

	inline static int32_t get_offset_of__inAppMemoryCacheByInAppsOpened_16() { return static_cast<int32_t>(offsetof(InAppManager_t3917644817, ____inAppMemoryCacheByInAppsOpened_16)); }
	inline Dictionary_2_t1559753164 * get__inAppMemoryCacheByInAppsOpened_16() const { return ____inAppMemoryCacheByInAppsOpened_16; }
	inline Dictionary_2_t1559753164 ** get_address_of__inAppMemoryCacheByInAppsOpened_16() { return &____inAppMemoryCacheByInAppsOpened_16; }
	inline void set__inAppMemoryCacheByInAppsOpened_16(Dictionary_2_t1559753164 * value)
	{
		____inAppMemoryCacheByInAppsOpened_16 = value;
		Il2CppCodeGenWriteBarrier(&____inAppMemoryCacheByInAppsOpened_16, value);
	}

	inline static int32_t get_offset_of_loader_17() { return static_cast<int32_t>(offsetof(InAppManager_t3917644817, ___loader_17)); }
	inline GameObject_t3674682005 * get_loader_17() const { return ___loader_17; }
	inline GameObject_t3674682005 ** get_address_of_loader_17() { return &___loader_17; }
	inline void set_loader_17(GameObject_t3674682005 * value)
	{
		___loader_17 = value;
		Il2CppCodeGenWriteBarrier(&___loader_17, value);
	}

	inline static int32_t get_offset_of_luzes_18() { return static_cast<int32_t>(offsetof(InAppManager_t3917644817, ___luzes_18)); }
	inline GameObject_t3674682005 * get_luzes_18() const { return ___luzes_18; }
	inline GameObject_t3674682005 ** get_address_of_luzes_18() { return &___luzes_18; }
	inline void set_luzes_18(GameObject_t3674682005 * value)
	{
		___luzes_18 = value;
		Il2CppCodeGenWriteBarrier(&___luzes_18, value);
	}

	inline static int32_t get_offset_of_inAppName_19() { return static_cast<int32_t>(offsetof(InAppManager_t3917644817, ___inAppName_19)); }
	inline String_t* get_inAppName_19() const { return ___inAppName_19; }
	inline String_t** get_address_of_inAppName_19() { return &___inAppName_19; }
	inline void set_inAppName_19(String_t* value)
	{
		___inAppName_19 = value;
		Il2CppCodeGenWriteBarrier(&___inAppName_19, value);
	}

	inline static int32_t get_offset_of_addInAppNow_20() { return static_cast<int32_t>(offsetof(InAppManager_t3917644817, ___addInAppNow_20)); }
	inline bool get_addInAppNow_20() const { return ___addInAppNow_20; }
	inline bool* get_address_of_addInAppNow_20() { return &___addInAppNow_20; }
	inline void set_addInAppNow_20(bool value)
	{
		___addInAppNow_20 = value;
	}

	inline static int32_t get_offset_of_InAppTypes_21() { return static_cast<int32_t>(offsetof(InAppManager_t3917644817, ___InAppTypes_21)); }
	inline InAppAbstractU5BU5D_t412350073* get_InAppTypes_21() const { return ___InAppTypes_21; }
	inline InAppAbstractU5BU5D_t412350073** get_address_of_InAppTypes_21() { return &___InAppTypes_21; }
	inline void set_InAppTypes_21(InAppAbstractU5BU5D_t412350073* value)
	{
		___InAppTypes_21 = value;
		Il2CppCodeGenWriteBarrier(&___InAppTypes_21, value);
	}

	inline static int32_t get_offset_of__inAppTypesList_22() { return static_cast<int32_t>(offsetof(InAppManager_t3917644817, ____inAppTypesList_22)); }
	inline Dictionary_2_t1203091498 * get__inAppTypesList_22() const { return ____inAppTypesList_22; }
	inline Dictionary_2_t1203091498 ** get_address_of__inAppTypesList_22() { return &____inAppTypesList_22; }
	inline void set__inAppTypesList_22(Dictionary_2_t1203091498 * value)
	{
		____inAppTypesList_22 = value;
		Il2CppCodeGenWriteBarrier(&____inAppTypesList_22, value);
	}

	inline static int32_t get_offset_of__currentPresentation_23() { return static_cast<int32_t>(offsetof(InAppManager_t3917644817, ____currentPresentation_23)); }
	inline GroupInAppAbstractComponentsManager_t739334794 * get__currentPresentation_23() const { return ____currentPresentation_23; }
	inline GroupInAppAbstractComponentsManager_t739334794 ** get_address_of__currentPresentation_23() { return &____currentPresentation_23; }
	inline void set__currentPresentation_23(GroupInAppAbstractComponentsManager_t739334794 * value)
	{
		____currentPresentation_23 = value;
		Il2CppCodeGenWriteBarrier(&____currentPresentation_23, value);
	}

	inline static int32_t get_offset_of__currentAnalytics_24() { return static_cast<int32_t>(offsetof(InAppManager_t3917644817, ____currentAnalytics_24)); }
	inline InAppAnalytics_t2316734034 * get__currentAnalytics_24() const { return ____currentAnalytics_24; }
	inline InAppAnalytics_t2316734034 ** get_address_of__currentAnalytics_24() { return &____currentAnalytics_24; }
	inline void set__currentAnalytics_24(InAppAnalytics_t2316734034 * value)
	{
		____currentAnalytics_24 = value;
		Il2CppCodeGenWriteBarrier(&____currentAnalytics_24, value);
	}

	inline static int32_t get_offset_of__lastAppName_25() { return static_cast<int32_t>(offsetof(InAppManager_t3917644817, ____lastAppName_25)); }
	inline String_t* get__lastAppName_25() const { return ____lastAppName_25; }
	inline String_t** get_address_of__lastAppName_25() { return &____lastAppName_25; }
	inline void set__lastAppName_25(String_t* value)
	{
		____lastAppName_25 = value;
		Il2CppCodeGenWriteBarrier(&____lastAppName_25, value);
	}

	inline static int32_t get_offset_of__appNameToRunNow_26() { return static_cast<int32_t>(offsetof(InAppManager_t3917644817, ____appNameToRunNow_26)); }
	inline String_t* get__appNameToRunNow_26() const { return ____appNameToRunNow_26; }
	inline String_t** get_address_of__appNameToRunNow_26() { return &____appNameToRunNow_26; }
	inline void set__appNameToRunNow_26(String_t* value)
	{
		____appNameToRunNow_26 = value;
		Il2CppCodeGenWriteBarrier(&____appNameToRunNow_26, value);
	}
};

struct InAppManager_t3917644817_StaticFields
{
public:
	// System.Int32 MagicTV.in_apps.InAppManager::lojaCode
	int32_t ___lojaCode_8;
	// UnityEngine.GameObject MagicTV.in_apps.InAppManager::_inAppMagicTVDemo
	GameObject_t3674682005 * ____inAppMagicTVDemo_13;

public:
	inline static int32_t get_offset_of_lojaCode_8() { return static_cast<int32_t>(offsetof(InAppManager_t3917644817_StaticFields, ___lojaCode_8)); }
	inline int32_t get_lojaCode_8() const { return ___lojaCode_8; }
	inline int32_t* get_address_of_lojaCode_8() { return &___lojaCode_8; }
	inline void set_lojaCode_8(int32_t value)
	{
		___lojaCode_8 = value;
	}

	inline static int32_t get_offset_of__inAppMagicTVDemo_13() { return static_cast<int32_t>(offsetof(InAppManager_t3917644817_StaticFields, ____inAppMagicTVDemo_13)); }
	inline GameObject_t3674682005 * get__inAppMagicTVDemo_13() const { return ____inAppMagicTVDemo_13; }
	inline GameObject_t3674682005 ** get_address_of__inAppMagicTVDemo_13() { return &____inAppMagicTVDemo_13; }
	inline void set__inAppMagicTVDemo_13(GameObject_t3674682005 * value)
	{
		____inAppMagicTVDemo_13 = value;
		Il2CppCodeGenWriteBarrier(&____inAppMagicTVDemo_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
