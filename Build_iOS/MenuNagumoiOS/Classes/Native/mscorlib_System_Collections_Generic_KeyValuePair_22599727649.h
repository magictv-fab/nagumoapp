﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler
struct OnChangeToAnStateEventHandler_t630243546;

#include "mscorlib_System_ValueType1744280289.h"
#include "AssemblyU2DCSharp_MagicTV_globals_StateMachine367995870.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>
struct  KeyValuePair_2_t2599727649 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	OnChangeToAnStateEventHandler_t630243546 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2599727649, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2599727649, ___value_1)); }
	inline OnChangeToAnStateEventHandler_t630243546 * get_value_1() const { return ___value_1; }
	inline OnChangeToAnStateEventHandler_t630243546 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(OnChangeToAnStateEventHandler_t630243546 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier(&___value_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
