﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.abstracts.BenchmarkAbstract
struct BenchmarkAbstract_t3666376745;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.abstracts.BenchmarkAbstract::.ctor()
extern "C"  void BenchmarkAbstract__ctor_m2364005224 (BenchmarkAbstract_t3666376745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
