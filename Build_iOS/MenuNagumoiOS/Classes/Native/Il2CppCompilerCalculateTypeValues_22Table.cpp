﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit2416790841.h"
#include "UnityEngine_UnityEngine_Collision2494107688.h"
#include "UnityEngine_UnityEngine_CollisionFlags490137529.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteraction577895768.h"
#include "UnityEngine_UnityEngine_Physics3358180733.h"
#include "UnityEngine_UnityEngine_ContactPoint243083348.h"
#include "UnityEngine_UnityEngine_Rigidbody3346577219.h"
#include "UnityEngine_UnityEngine_Joint4201008640.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"
#include "UnityEngine_UnityEngine_BoxCollider2538127765.h"
#include "UnityEngine_UnityEngine_SphereCollider111527973.h"
#include "UnityEngine_UnityEngine_MeshCollider2667653125.h"
#include "UnityEngine_UnityEngine_CapsuleCollider318617463.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"
#include "UnityEngine_UnityEngine_PhysicMaterial211873335.h"
#include "UnityEngine_UnityEngine_CharacterController1618060635.h"
#include "UnityEngine_UnityEngine_Cloth4194460560.h"
#include "UnityEngine_UnityEngine_Physics2D9846735.h"
#include "UnityEngine_UnityEngine_RaycastHit2D1374744384.h"
#include "UnityEngine_UnityEngine_Rigidbody2D1743771669.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098.h"
#include "UnityEngine_UnityEngine_ContactPoint2D4288432358.h"
#include "UnityEngine_UnityEngine_Collision2D2859305914.h"
#include "UnityEngine_UnityEngine_NavMeshAgent588466745.h"
#include "UnityEngine_UnityEngine_AudioSettings3774206607.h"
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigu1377657005.h"
#include "UnityEngine_UnityEngine_AudioType794660134.h"
#include "UnityEngine_UnityEngine_AudioClip794140988.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallback83861602.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCa4244274966.h"
#include "UnityEngine_UnityEngine_AudioListener3685735200.h"
#include "UnityEngine_UnityEngine_AudioSource1740077639.h"
#include "UnityEngine_UnityEngine_WebCamDevice3274004757.h"
#include "UnityEngine_UnityEngine_WebCamTexture1290350902.h"
#include "UnityEngine_UnityEngine_WrapMode1491636113.h"
#include "UnityEngine_UnityEngine_AnimationEventSource2152433973.h"
#include "UnityEngine_UnityEngine_AnimationEvent3669457594.h"
#include "UnityEngine_UnityEngine_AnimationClip2007702890.h"
#include "UnityEngine_UnityEngine_PlayMode1155122555.h"
#include "UnityEngine_UnityEngine_AnimationBlendMode23503924.h"
#include "UnityEngine_UnityEngine_Animation1724966010.h"
#include "UnityEngine_UnityEngine_Animation_Enumerator1374492422.h"
#include "UnityEngine_UnityEngine_AnimationState3682323633.h"
#include "UnityEngine_UnityEngine_AvatarTarget2373143374.h"
#include "UnityEngine_UnityEngine_AvatarIKGoal2036631794.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo2746035113.h"
#include "UnityEngine_UnityEngine_AnimatorCullingMode3217251138.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo323110318.h"
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo2817229998.h"
#include "UnityEngine_UnityEngine_MatchTargetWeightMask258413904.h"
#include "UnityEngine_UnityEngine_Animator2776330603.h"
#include "UnityEngine_UnityEngine_SkeletonBone421858229.h"
#include "UnityEngine_UnityEngine_HumanLimit3300934482.h"
#include "UnityEngine_UnityEngine_HumanBone194476679.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController274649809.h"
#include "UnityEngine_UnityEngine_HumanBodyBones1606609988.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim1390812184.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim3906681469.h"
#include "UnityEngine_UnityEngine_TextGenerationSettings1923005356.h"
#include "UnityEngine_UnityEngine_TextGenerator538854556.h"
#include "UnityEngine_UnityEngine_TextAnchor213922566.h"
#include "UnityEngine_UnityEngine_HorizontalWrapMode2918974229.h"
#include "UnityEngine_UnityEngine_VerticalWrapMode1147493927.h"
#include "UnityEngine_UnityEngine_GUIText3371372606.h"
#include "UnityEngine_UnityEngine_TextMesh2567681854.h"
#include "UnityEngine_UnityEngine_Font4241557075.h"
#include "UnityEngine_UnityEngine_Font_FontTextureRebuildCal4168056797.h"
#include "UnityEngine_UnityEngine_UICharInfo65807484.h"
#include "UnityEngine_UnityEngine_UILineInfo4113875482.h"
#include "UnityEngine_UnityEngine_UIVertex4244065212.h"
#include "UnityEngine_UnityEngine_RenderMode77252893.h"
#include "UnityEngine_UnityEngine_Canvas2727140764.h"
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvases4247149838.h"
#include "UnityEngine_UnityEngine_CanvasGroup3702418109.h"
#include "UnityEngine_UnityEngine_CanvasRenderer3950887807.h"
#include "UnityEngine_UnityEngine_RectTransformUtility3025555048.h"
#include "UnityEngine_UnityEngine_Event4196595728.h"
#include "UnityEngine_UnityEngine_KeyCode3128317986.h"
#include "UnityEngine_UnityEngine_EventType637886954.h"
#include "UnityEngine_UnityEngine_EventModifiers4195406918.h"
#include "UnityEngine_UnityEngine_ScaleMode3023293187.h"
#include "UnityEngine_UnityEngine_GUI3134605553.h"
#include "UnityEngine_UnityEngine_GUI_ScrollViewState2687015188.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction2749288659.h"
#include "UnityEngine_UnityEngine_GUIContent2094828418.h"
#include "UnityEngine_UnityEngine_GUILayout3864601915.h"
#include "UnityEngine_UnityEngine_GUILayoutUtility87000299.h"
#include "UnityEngine_UnityEngine_GUILayoutUtility_LayoutCach879908455.h"
#include "UnityEngine_UnityEngine_GUILayoutEntry1336615025.h"
#include "UnityEngine_UnityEngine_GUILayoutGroup1338576510.h"
#include "UnityEngine_UnityEngine_GUIScrollGroup1392439483.h"
#include "UnityEngine_UnityEngine_GUIGridSizer3080214772.h"
#include "UnityEngine_UnityEngine_GUIWordWrapSizer1645739142.h"
#include "UnityEngine_UnityEngine_GUILayoutOption331591504.h"
#include "UnityEngine_UnityEngine_GUILayoutOption_Type957706982.h"
#include "UnityEngine_UnityEngine_GUISettings4075193652.h"
#include "UnityEngine_UnityEngine_GUISkin3371348110.h"
#include "UnityEngine_UnityEngine_GUISkin_SkinChangedDelegat4002872878.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (ControllerColliderHit_t2416790841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2200[7] = 
{
	ControllerColliderHit_t2416790841::get_offset_of_m_Controller_0(),
	ControllerColliderHit_t2416790841::get_offset_of_m_Collider_1(),
	ControllerColliderHit_t2416790841::get_offset_of_m_Point_2(),
	ControllerColliderHit_t2416790841::get_offset_of_m_Normal_3(),
	ControllerColliderHit_t2416790841::get_offset_of_m_MoveDirection_4(),
	ControllerColliderHit_t2416790841::get_offset_of_m_MoveLength_5(),
	ControllerColliderHit_t2416790841::get_offset_of_m_Push_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (Collision_t2494107688), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2201[5] = 
{
	Collision_t2494107688::get_offset_of_m_Impulse_0(),
	Collision_t2494107688::get_offset_of_m_RelativeVelocity_1(),
	Collision_t2494107688::get_offset_of_m_Rigidbody_2(),
	Collision_t2494107688::get_offset_of_m_Collider_3(),
	Collision_t2494107688::get_offset_of_m_Contacts_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (CollisionFlags_t490137529)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2202[8] = 
{
	CollisionFlags_t490137529::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (QueryTriggerInteraction_t577895768)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2203[4] = 
{
	QueryTriggerInteraction_t577895768::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (Physics_t3358180733), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (ContactPoint_t243083348)+ sizeof (Il2CppObject), sizeof(ContactPoint_t243083348_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2205[4] = 
{
	ContactPoint_t243083348::get_offset_of_m_Point_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t243083348::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t243083348::get_offset_of_m_ThisColliderInstanceID_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t243083348::get_offset_of_m_OtherColliderInstanceID_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (Rigidbody_t3346577219), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (Joint_t4201008640), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (Collider_t2939674232), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (BoxCollider_t2538127765), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (SphereCollider_t111527973), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (MeshCollider_t2667653125), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (CapsuleCollider_t318617463), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (RaycastHit_t4003175726)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2213[6] = 
{
	RaycastHit_t4003175726::get_offset_of_m_Point_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t4003175726::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t4003175726::get_offset_of_m_FaceID_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t4003175726::get_offset_of_m_Distance_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t4003175726::get_offset_of_m_UV_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t4003175726::get_offset_of_m_Collider_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (PhysicMaterial_t211873335), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (CharacterController_t1618060635), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (Cloth_t4194460560), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { sizeof (Physics2D_t9846735), -1, sizeof(Physics2D_t9846735_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2217[1] = 
{
	Physics2D_t9846735_StaticFields::get_offset_of_m_LastDisabledRigidbody2D_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { sizeof (RaycastHit2D_t1374744384)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2218[6] = 
{
	RaycastHit2D_t1374744384::get_offset_of_m_Centroid_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t1374744384::get_offset_of_m_Point_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t1374744384::get_offset_of_m_Normal_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t1374744384::get_offset_of_m_Distance_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t1374744384::get_offset_of_m_Fraction_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t1374744384::get_offset_of_m_Collider_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (Rigidbody2D_t1743771669), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (Collider2D_t1552025098), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (ContactPoint2D_t4288432358)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2221[4] = 
{
	ContactPoint2D_t4288432358::get_offset_of_m_Point_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t4288432358::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t4288432358::get_offset_of_m_Collider_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t4288432358::get_offset_of_m_OtherCollider_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (Collision2D_t2859305914), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2222[5] = 
{
	Collision2D_t2859305914::get_offset_of_m_Rigidbody_0(),
	Collision2D_t2859305914::get_offset_of_m_Collider_1(),
	Collision2D_t2859305914::get_offset_of_m_Contacts_2(),
	Collision2D_t2859305914::get_offset_of_m_RelativeVelocity_3(),
	Collision2D_t2859305914::get_offset_of_m_Enabled_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (NavMeshAgent_t588466745), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (AudioSettings_t3774206607), -1, sizeof(AudioSettings_t3774206607_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2224[1] = 
{
	AudioSettings_t3774206607_StaticFields::get_offset_of_OnAudioConfigurationChanged_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (AudioConfigurationChangeHandler_t1377657005), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (AudioType_t794660134)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2226[14] = 
{
	AudioType_t794660134::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (AudioClip_t794140988), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2227[2] = 
{
	AudioClip_t794140988::get_offset_of_m_PCMReaderCallback_2(),
	AudioClip_t794140988::get_offset_of_m_PCMSetPositionCallback_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (PCMReaderCallback_t83861602), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (PCMSetPositionCallback_t4244274966), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (AudioListener_t3685735200), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { sizeof (AudioSource_t1740077639), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (WebCamDevice_t3274004757)+ sizeof (Il2CppObject), sizeof(WebCamDevice_t3274004757_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2232[2] = 
{
	WebCamDevice_t3274004757::get_offset_of_m_Name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WebCamDevice_t3274004757::get_offset_of_m_Flags_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (WebCamTexture_t1290350902), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (WrapMode_t1491636113)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2234[7] = 
{
	WrapMode_t1491636113::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (AnimationEventSource_t2152433973)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2235[4] = 
{
	AnimationEventSource_t2152433973::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (AnimationEvent_t3669457594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2236[11] = 
{
	AnimationEvent_t3669457594::get_offset_of_m_Time_0(),
	AnimationEvent_t3669457594::get_offset_of_m_FunctionName_1(),
	AnimationEvent_t3669457594::get_offset_of_m_StringParameter_2(),
	AnimationEvent_t3669457594::get_offset_of_m_ObjectReferenceParameter_3(),
	AnimationEvent_t3669457594::get_offset_of_m_FloatParameter_4(),
	AnimationEvent_t3669457594::get_offset_of_m_IntParameter_5(),
	AnimationEvent_t3669457594::get_offset_of_m_MessageOptions_6(),
	AnimationEvent_t3669457594::get_offset_of_m_Source_7(),
	AnimationEvent_t3669457594::get_offset_of_m_StateSender_8(),
	AnimationEvent_t3669457594::get_offset_of_m_AnimatorStateInfo_9(),
	AnimationEvent_t3669457594::get_offset_of_m_AnimatorClipInfo_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { sizeof (AnimationClip_t2007702890), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { sizeof (PlayMode_t1155122555)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2238[3] = 
{
	PlayMode_t1155122555::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { sizeof (AnimationBlendMode_t23503924)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2239[3] = 
{
	AnimationBlendMode_t23503924::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (Animation_t1724966010), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (Enumerator_t1374492422), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2241[2] = 
{
	Enumerator_t1374492422::get_offset_of_m_Outer_0(),
	Enumerator_t1374492422::get_offset_of_m_CurrentIndex_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { sizeof (AnimationState_t3682323633), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (AvatarTarget_t2373143374)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2243[7] = 
{
	AvatarTarget_t2373143374::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (AvatarIKGoal_t2036631794)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2244[5] = 
{
	AvatarIKGoal_t2036631794::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (AnimatorClipInfo_t2746035113)+ sizeof (Il2CppObject), sizeof(AnimatorClipInfo_t2746035113_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2245[2] = 
{
	AnimatorClipInfo_t2746035113::get_offset_of_m_ClipInstanceID_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorClipInfo_t2746035113::get_offset_of_m_Weight_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (AnimatorCullingMode_t3217251138)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2246[4] = 
{
	AnimatorCullingMode_t3217251138::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (AnimatorStateInfo_t323110318)+ sizeof (Il2CppObject), sizeof(AnimatorStateInfo_t323110318_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2247[9] = 
{
	AnimatorStateInfo_t323110318::get_offset_of_m_Name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t323110318::get_offset_of_m_Path_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t323110318::get_offset_of_m_FullPath_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t323110318::get_offset_of_m_NormalizedTime_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t323110318::get_offset_of_m_Length_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t323110318::get_offset_of_m_Speed_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t323110318::get_offset_of_m_SpeedMultiplier_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t323110318::get_offset_of_m_Tag_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t323110318::get_offset_of_m_Loop_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (AnimatorTransitionInfo_t2817229998)+ sizeof (Il2CppObject), sizeof(AnimatorTransitionInfo_t2817229998_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2248[6] = 
{
	AnimatorTransitionInfo_t2817229998::get_offset_of_m_FullPath_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2817229998::get_offset_of_m_UserName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2817229998::get_offset_of_m_Name_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2817229998::get_offset_of_m_NormalizedTime_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2817229998::get_offset_of_m_AnyState_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2817229998::get_offset_of_m_TransitionType_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (MatchTargetWeightMask_t258413904)+ sizeof (Il2CppObject), sizeof(MatchTargetWeightMask_t258413904_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2249[2] = 
{
	MatchTargetWeightMask_t258413904::get_offset_of_m_PositionXYZWeight_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MatchTargetWeightMask_t258413904::get_offset_of_m_RotationWeight_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { sizeof (Animator_t2776330603), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { sizeof (SkeletonBone_t421858229)+ sizeof (Il2CppObject), sizeof(SkeletonBone_t421858229_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2251[5] = 
{
	SkeletonBone_t421858229::get_offset_of_name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t421858229::get_offset_of_position_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t421858229::get_offset_of_rotation_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t421858229::get_offset_of_scale_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t421858229::get_offset_of_transformModified_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { sizeof (HumanLimit_t3300934482)+ sizeof (Il2CppObject), sizeof(HumanLimit_t3300934482_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2252[5] = 
{
	HumanLimit_t3300934482::get_offset_of_m_Min_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t3300934482::get_offset_of_m_Max_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t3300934482::get_offset_of_m_Center_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t3300934482::get_offset_of_m_AxisLength_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t3300934482::get_offset_of_m_UseDefaultValues_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (HumanBone_t194476679)+ sizeof (Il2CppObject), sizeof(HumanBone_t194476679_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2253[3] = 
{
	HumanBone_t194476679::get_offset_of_m_BoneName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanBone_t194476679::get_offset_of_m_HumanName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanBone_t194476679::get_offset_of_limit_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (RuntimeAnimatorController_t274649809), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (HumanBodyBones_t1606609988)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2255[56] = 
{
	HumanBodyBones_t1606609988::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (AnimationPlayable_t1390812184), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (AnimatorControllerPlayable_t3906681469), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (TextGenerationSettings_t1923005356)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2259[18] = 
{
	TextGenerationSettings_t1923005356::get_offset_of_font_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_color_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_fontSize_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_lineSpacing_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_richText_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_scaleFactor_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_fontStyle_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_textAnchor_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_alignByGeometry_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_resizeTextForBestFit_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_resizeTextMinSize_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_resizeTextMaxSize_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_updateBounds_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_verticalOverflow_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_horizontalOverflow_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_generationExtents_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_pivot_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_generateOutOfBounds_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (TextGenerator_t538854556), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2260[11] = 
{
	TextGenerator_t538854556::get_offset_of_m_Ptr_0(),
	TextGenerator_t538854556::get_offset_of_m_LastString_1(),
	TextGenerator_t538854556::get_offset_of_m_LastSettings_2(),
	TextGenerator_t538854556::get_offset_of_m_HasGenerated_3(),
	TextGenerator_t538854556::get_offset_of_m_LastValid_4(),
	TextGenerator_t538854556::get_offset_of_m_Verts_5(),
	TextGenerator_t538854556::get_offset_of_m_Characters_6(),
	TextGenerator_t538854556::get_offset_of_m_Lines_7(),
	TextGenerator_t538854556::get_offset_of_m_CachedVerts_8(),
	TextGenerator_t538854556::get_offset_of_m_CachedCharacters_9(),
	TextGenerator_t538854556::get_offset_of_m_CachedLines_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (TextAnchor_t213922566)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2261[10] = 
{
	TextAnchor_t213922566::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (HorizontalWrapMode_t2918974229)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2262[3] = 
{
	HorizontalWrapMode_t2918974229::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (VerticalWrapMode_t1147493927)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2263[3] = 
{
	VerticalWrapMode_t1147493927::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { sizeof (GUIText_t3371372606), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { sizeof (TextMesh_t2567681854), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { sizeof (Font_t4241557075), -1, sizeof(Font_t4241557075_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2266[2] = 
{
	Font_t4241557075_StaticFields::get_offset_of_textureRebuilt_2(),
	Font_t4241557075::get_offset_of_m_FontTextureRebuildCallback_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (FontTextureRebuildCallback_t4168056797), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { sizeof (UICharInfo_t65807484)+ sizeof (Il2CppObject), sizeof(UICharInfo_t65807484_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2268[2] = 
{
	UICharInfo_t65807484::get_offset_of_cursorPos_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UICharInfo_t65807484::get_offset_of_charWidth_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (UILineInfo_t4113875482)+ sizeof (Il2CppObject), sizeof(UILineInfo_t4113875482_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2269[3] = 
{
	UILineInfo_t4113875482::get_offset_of_startCharIdx_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UILineInfo_t4113875482::get_offset_of_height_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UILineInfo_t4113875482::get_offset_of_topY_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { sizeof (UIVertex_t4244065212)+ sizeof (Il2CppObject), sizeof(UIVertex_t4244065212_marshaled_pinvoke), sizeof(UIVertex_t4244065212_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2270[9] = 
{
	UIVertex_t4244065212::get_offset_of_position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4244065212::get_offset_of_normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4244065212::get_offset_of_color_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4244065212::get_offset_of_uv0_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4244065212::get_offset_of_uv1_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4244065212::get_offset_of_tangent_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4244065212_StaticFields::get_offset_of_s_DefaultColor_6(),
	UIVertex_t4244065212_StaticFields::get_offset_of_s_DefaultTangent_7(),
	UIVertex_t4244065212_StaticFields::get_offset_of_simpleVert_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (RenderMode_t77252893)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2271[4] = 
{
	RenderMode_t77252893::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (Canvas_t2727140764), -1, sizeof(Canvas_t2727140764_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2272[1] = 
{
	Canvas_t2727140764_StaticFields::get_offset_of_willRenderCanvases_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { sizeof (WillRenderCanvases_t4247149838), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (CanvasGroup_t3702418109), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (CanvasRenderer_t3950887807), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { sizeof (RectTransformUtility_t3025555048), -1, sizeof(RectTransformUtility_t3025555048_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2277[1] = 
{
	RectTransformUtility_t3025555048_StaticFields::get_offset_of_s_Corners_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (Event_t4196595728), sizeof(Event_t4196595728_marshaled_pinvoke), sizeof(Event_t4196595728_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2278[4] = 
{
	Event_t4196595728::get_offset_of_m_Ptr_0(),
	Event_t4196595728_StaticFields::get_offset_of_s_Current_1(),
	Event_t4196595728_StaticFields::get_offset_of_s_MasterEvent_2(),
	Event_t4196595728_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (KeyCode_t3128317986)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2279[322] = 
{
	KeyCode_t3128317986::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (EventType_t637886954)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2280[31] = 
{
	EventType_t637886954::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { sizeof (EventModifiers_t4195406918)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2281[9] = 
{
	EventModifiers_t4195406918::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (ScaleMode_t3023293187)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2282[4] = 
{
	ScaleMode_t3023293187::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { sizeof (GUI_t3134605553), -1, sizeof(GUI_t3134605553_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2283[14] = 
{
	GUI_t3134605553_StaticFields::get_offset_of_s_ScrollStepSize_0(),
	GUI_t3134605553_StaticFields::get_offset_of_s_ScrollControlId_1(),
	GUI_t3134605553_StaticFields::get_offset_of_s_HotTextField_2(),
	GUI_t3134605553_StaticFields::get_offset_of_s_BoxHash_3(),
	GUI_t3134605553_StaticFields::get_offset_of_s_RepeatButtonHash_4(),
	GUI_t3134605553_StaticFields::get_offset_of_s_ToggleHash_5(),
	GUI_t3134605553_StaticFields::get_offset_of_s_ButtonGridHash_6(),
	GUI_t3134605553_StaticFields::get_offset_of_s_SliderHash_7(),
	GUI_t3134605553_StaticFields::get_offset_of_s_BeginGroupHash_8(),
	GUI_t3134605553_StaticFields::get_offset_of_s_ScrollviewHash_9(),
	GUI_t3134605553_StaticFields::get_offset_of_s_Skin_10(),
	GUI_t3134605553_StaticFields::get_offset_of_s_ScrollViewStates_11(),
	GUI_t3134605553_StaticFields::get_offset_of_U3CnextScrollStepTimeU3Ek__BackingField_12(),
	GUI_t3134605553_StaticFields::get_offset_of_U3CscrollTroughSideU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (ScrollViewState_t2687015188), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2284[5] = 
{
	ScrollViewState_t2687015188::get_offset_of_position_0(),
	ScrollViewState_t2687015188::get_offset_of_visibleRect_1(),
	ScrollViewState_t2687015188::get_offset_of_viewRect_2(),
	ScrollViewState_t2687015188::get_offset_of_scrollPosition_3(),
	ScrollViewState_t2687015188::get_offset_of_apply_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { sizeof (WindowFunction_t2749288659), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { sizeof (GUIContent_t2094828418), -1, sizeof(GUIContent_t2094828418_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2286[7] = 
{
	GUIContent_t2094828418::get_offset_of_m_Text_0(),
	GUIContent_t2094828418::get_offset_of_m_Image_1(),
	GUIContent_t2094828418::get_offset_of_m_Tooltip_2(),
	GUIContent_t2094828418_StaticFields::get_offset_of_s_Text_3(),
	GUIContent_t2094828418_StaticFields::get_offset_of_s_Image_4(),
	GUIContent_t2094828418_StaticFields::get_offset_of_s_TextImage_5(),
	GUIContent_t2094828418_StaticFields::get_offset_of_none_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287 = { sizeof (GUILayout_t3864601915), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288 = { sizeof (GUILayoutUtility_t87000299), -1, sizeof(GUILayoutUtility_t87000299_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2288[5] = 
{
	GUILayoutUtility_t87000299_StaticFields::get_offset_of_s_StoredLayouts_0(),
	GUILayoutUtility_t87000299_StaticFields::get_offset_of_s_StoredWindows_1(),
	GUILayoutUtility_t87000299_StaticFields::get_offset_of_current_2(),
	GUILayoutUtility_t87000299_StaticFields::get_offset_of_kDummyRect_3(),
	GUILayoutUtility_t87000299_StaticFields::get_offset_of_s_SpaceStyle_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289 = { sizeof (LayoutCache_t879908455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2289[3] = 
{
	LayoutCache_t879908455::get_offset_of_topLevel_0(),
	LayoutCache_t879908455::get_offset_of_layoutGroups_1(),
	LayoutCache_t879908455::get_offset_of_windows_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290 = { sizeof (GUILayoutEntry_t1336615025), -1, sizeof(GUILayoutEntry_t1336615025_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2290[10] = 
{
	GUILayoutEntry_t1336615025::get_offset_of_minWidth_0(),
	GUILayoutEntry_t1336615025::get_offset_of_maxWidth_1(),
	GUILayoutEntry_t1336615025::get_offset_of_minHeight_2(),
	GUILayoutEntry_t1336615025::get_offset_of_maxHeight_3(),
	GUILayoutEntry_t1336615025::get_offset_of_rect_4(),
	GUILayoutEntry_t1336615025::get_offset_of_stretchWidth_5(),
	GUILayoutEntry_t1336615025::get_offset_of_stretchHeight_6(),
	GUILayoutEntry_t1336615025::get_offset_of_m_Style_7(),
	GUILayoutEntry_t1336615025_StaticFields::get_offset_of_kDummyRect_8(),
	GUILayoutEntry_t1336615025_StaticFields::get_offset_of_indent_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291 = { sizeof (GUILayoutGroup_t1338576510), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2291[17] = 
{
	GUILayoutGroup_t1338576510::get_offset_of_entries_10(),
	GUILayoutGroup_t1338576510::get_offset_of_isVertical_11(),
	GUILayoutGroup_t1338576510::get_offset_of_resetCoords_12(),
	GUILayoutGroup_t1338576510::get_offset_of_spacing_13(),
	GUILayoutGroup_t1338576510::get_offset_of_sameSize_14(),
	GUILayoutGroup_t1338576510::get_offset_of_isWindow_15(),
	GUILayoutGroup_t1338576510::get_offset_of_windowID_16(),
	GUILayoutGroup_t1338576510::get_offset_of_m_Cursor_17(),
	GUILayoutGroup_t1338576510::get_offset_of_m_StretchableCountX_18(),
	GUILayoutGroup_t1338576510::get_offset_of_m_StretchableCountY_19(),
	GUILayoutGroup_t1338576510::get_offset_of_m_UserSpecifiedWidth_20(),
	GUILayoutGroup_t1338576510::get_offset_of_m_UserSpecifiedHeight_21(),
	GUILayoutGroup_t1338576510::get_offset_of_m_ChildMinWidth_22(),
	GUILayoutGroup_t1338576510::get_offset_of_m_ChildMaxWidth_23(),
	GUILayoutGroup_t1338576510::get_offset_of_m_ChildMinHeight_24(),
	GUILayoutGroup_t1338576510::get_offset_of_m_ChildMaxHeight_25(),
	GUILayoutGroup_t1338576510::get_offset_of_m_Margin_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292 = { sizeof (GUIScrollGroup_t1392439483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2292[12] = 
{
	GUIScrollGroup_t1392439483::get_offset_of_calcMinWidth_27(),
	GUIScrollGroup_t1392439483::get_offset_of_calcMaxWidth_28(),
	GUIScrollGroup_t1392439483::get_offset_of_calcMinHeight_29(),
	GUIScrollGroup_t1392439483::get_offset_of_calcMaxHeight_30(),
	GUIScrollGroup_t1392439483::get_offset_of_clientWidth_31(),
	GUIScrollGroup_t1392439483::get_offset_of_clientHeight_32(),
	GUIScrollGroup_t1392439483::get_offset_of_allowHorizontalScroll_33(),
	GUIScrollGroup_t1392439483::get_offset_of_allowVerticalScroll_34(),
	GUIScrollGroup_t1392439483::get_offset_of_needsHorizontalScrollbar_35(),
	GUIScrollGroup_t1392439483::get_offset_of_needsVerticalScrollbar_36(),
	GUIScrollGroup_t1392439483::get_offset_of_horizontalScrollbar_37(),
	GUIScrollGroup_t1392439483::get_offset_of_verticalScrollbar_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293 = { sizeof (GUIGridSizer_t3080214772), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2293[6] = 
{
	GUIGridSizer_t3080214772::get_offset_of_m_Count_10(),
	GUIGridSizer_t3080214772::get_offset_of_m_XCount_11(),
	GUIGridSizer_t3080214772::get_offset_of_m_MinButtonWidth_12(),
	GUIGridSizer_t3080214772::get_offset_of_m_MaxButtonWidth_13(),
	GUIGridSizer_t3080214772::get_offset_of_m_MinButtonHeight_14(),
	GUIGridSizer_t3080214772::get_offset_of_m_MaxButtonHeight_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294 = { sizeof (GUIWordWrapSizer_t1645739142), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2294[3] = 
{
	GUIWordWrapSizer_t1645739142::get_offset_of_m_Content_10(),
	GUIWordWrapSizer_t1645739142::get_offset_of_m_ForcedMinHeight_11(),
	GUIWordWrapSizer_t1645739142::get_offset_of_m_ForcedMaxHeight_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295 = { sizeof (GUILayoutOption_t331591504), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2295[2] = 
{
	GUILayoutOption_t331591504::get_offset_of_type_0(),
	GUILayoutOption_t331591504::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296 = { sizeof (Type_t957706982)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2296[15] = 
{
	Type_t957706982::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297 = { sizeof (GUISettings_t4075193652), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2297[5] = 
{
	GUISettings_t4075193652::get_offset_of_m_DoubleClickSelectsWord_0(),
	GUISettings_t4075193652::get_offset_of_m_TripleClickSelectsLine_1(),
	GUISettings_t4075193652::get_offset_of_m_CursorColor_2(),
	GUISettings_t4075193652::get_offset_of_m_CursorFlashSpeed_3(),
	GUISettings_t4075193652::get_offset_of_m_SelectionColor_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2298 = { sizeof (GUISkin_t3371348110), -1, sizeof(GUISkin_t3371348110_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2298[27] = 
{
	GUISkin_t3371348110::get_offset_of_m_Font_2(),
	GUISkin_t3371348110::get_offset_of_m_box_3(),
	GUISkin_t3371348110::get_offset_of_m_button_4(),
	GUISkin_t3371348110::get_offset_of_m_toggle_5(),
	GUISkin_t3371348110::get_offset_of_m_label_6(),
	GUISkin_t3371348110::get_offset_of_m_textField_7(),
	GUISkin_t3371348110::get_offset_of_m_textArea_8(),
	GUISkin_t3371348110::get_offset_of_m_window_9(),
	GUISkin_t3371348110::get_offset_of_m_horizontalSlider_10(),
	GUISkin_t3371348110::get_offset_of_m_horizontalSliderThumb_11(),
	GUISkin_t3371348110::get_offset_of_m_verticalSlider_12(),
	GUISkin_t3371348110::get_offset_of_m_verticalSliderThumb_13(),
	GUISkin_t3371348110::get_offset_of_m_horizontalScrollbar_14(),
	GUISkin_t3371348110::get_offset_of_m_horizontalScrollbarThumb_15(),
	GUISkin_t3371348110::get_offset_of_m_horizontalScrollbarLeftButton_16(),
	GUISkin_t3371348110::get_offset_of_m_horizontalScrollbarRightButton_17(),
	GUISkin_t3371348110::get_offset_of_m_verticalScrollbar_18(),
	GUISkin_t3371348110::get_offset_of_m_verticalScrollbarThumb_19(),
	GUISkin_t3371348110::get_offset_of_m_verticalScrollbarUpButton_20(),
	GUISkin_t3371348110::get_offset_of_m_verticalScrollbarDownButton_21(),
	GUISkin_t3371348110::get_offset_of_m_ScrollView_22(),
	GUISkin_t3371348110::get_offset_of_m_CustomStyles_23(),
	GUISkin_t3371348110::get_offset_of_m_Settings_24(),
	GUISkin_t3371348110_StaticFields::get_offset_of_ms_Error_25(),
	GUISkin_t3371348110::get_offset_of_m_Styles_26(),
	GUISkin_t3371348110_StaticFields::get_offset_of_m_SkinChanged_27(),
	GUISkin_t3371348110_StaticFields::get_offset_of_current_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2299 = { sizeof (SkinChangedDelegate_t4002872878), sizeof(Il2CppMethodPointer), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
