﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Camera
struct Camera_t2727095145;

#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MonoCamer3733412120.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.StereoCameraConfiguration
struct  StereoCameraConfiguration_t2173582563  : public MonoCameraConfiguration_t3733412120
{
public:
	// UnityEngine.Camera Vuforia.StereoCameraConfiguration::mSecondaryCamera
	Camera_t2727095145 * ___mSecondaryCamera_17;
	// System.Boolean Vuforia.StereoCameraConfiguration::mSkewFrustum
	bool ___mSkewFrustum_18;
	// System.Boolean Vuforia.StereoCameraConfiguration::mNeedToCheckStereo
	bool ___mNeedToCheckStereo_19;
	// System.Single Vuforia.StereoCameraConfiguration::mLastAppliedNearClipPlane
	float ___mLastAppliedNearClipPlane_20;
	// System.Single Vuforia.StereoCameraConfiguration::mLastAppliedFarClipPlane
	float ___mLastAppliedFarClipPlane_21;
	// System.Single Vuforia.StereoCameraConfiguration::mLastAppliedVirtualFoV
	float ___mLastAppliedVirtualFoV_22;
	// System.Single Vuforia.StereoCameraConfiguration::mNewNearClipPlane
	float ___mNewNearClipPlane_23;
	// System.Single Vuforia.StereoCameraConfiguration::mNewFarClipPlane
	float ___mNewFarClipPlane_24;
	// System.Single Vuforia.StereoCameraConfiguration::mNewVirtualFoV
	float ___mNewVirtualFoV_25;
	// System.Single Vuforia.StereoCameraConfiguration::mCameraOffset
	float ___mCameraOffset_26;
	// System.Int32 Vuforia.StereoCameraConfiguration::mEyewearUserCalibrationProfileId
	int32_t ___mEyewearUserCalibrationProfileId_27;

public:
	inline static int32_t get_offset_of_mSecondaryCamera_17() { return static_cast<int32_t>(offsetof(StereoCameraConfiguration_t2173582563, ___mSecondaryCamera_17)); }
	inline Camera_t2727095145 * get_mSecondaryCamera_17() const { return ___mSecondaryCamera_17; }
	inline Camera_t2727095145 ** get_address_of_mSecondaryCamera_17() { return &___mSecondaryCamera_17; }
	inline void set_mSecondaryCamera_17(Camera_t2727095145 * value)
	{
		___mSecondaryCamera_17 = value;
		Il2CppCodeGenWriteBarrier(&___mSecondaryCamera_17, value);
	}

	inline static int32_t get_offset_of_mSkewFrustum_18() { return static_cast<int32_t>(offsetof(StereoCameraConfiguration_t2173582563, ___mSkewFrustum_18)); }
	inline bool get_mSkewFrustum_18() const { return ___mSkewFrustum_18; }
	inline bool* get_address_of_mSkewFrustum_18() { return &___mSkewFrustum_18; }
	inline void set_mSkewFrustum_18(bool value)
	{
		___mSkewFrustum_18 = value;
	}

	inline static int32_t get_offset_of_mNeedToCheckStereo_19() { return static_cast<int32_t>(offsetof(StereoCameraConfiguration_t2173582563, ___mNeedToCheckStereo_19)); }
	inline bool get_mNeedToCheckStereo_19() const { return ___mNeedToCheckStereo_19; }
	inline bool* get_address_of_mNeedToCheckStereo_19() { return &___mNeedToCheckStereo_19; }
	inline void set_mNeedToCheckStereo_19(bool value)
	{
		___mNeedToCheckStereo_19 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedNearClipPlane_20() { return static_cast<int32_t>(offsetof(StereoCameraConfiguration_t2173582563, ___mLastAppliedNearClipPlane_20)); }
	inline float get_mLastAppliedNearClipPlane_20() const { return ___mLastAppliedNearClipPlane_20; }
	inline float* get_address_of_mLastAppliedNearClipPlane_20() { return &___mLastAppliedNearClipPlane_20; }
	inline void set_mLastAppliedNearClipPlane_20(float value)
	{
		___mLastAppliedNearClipPlane_20 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedFarClipPlane_21() { return static_cast<int32_t>(offsetof(StereoCameraConfiguration_t2173582563, ___mLastAppliedFarClipPlane_21)); }
	inline float get_mLastAppliedFarClipPlane_21() const { return ___mLastAppliedFarClipPlane_21; }
	inline float* get_address_of_mLastAppliedFarClipPlane_21() { return &___mLastAppliedFarClipPlane_21; }
	inline void set_mLastAppliedFarClipPlane_21(float value)
	{
		___mLastAppliedFarClipPlane_21 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedVirtualFoV_22() { return static_cast<int32_t>(offsetof(StereoCameraConfiguration_t2173582563, ___mLastAppliedVirtualFoV_22)); }
	inline float get_mLastAppliedVirtualFoV_22() const { return ___mLastAppliedVirtualFoV_22; }
	inline float* get_address_of_mLastAppliedVirtualFoV_22() { return &___mLastAppliedVirtualFoV_22; }
	inline void set_mLastAppliedVirtualFoV_22(float value)
	{
		___mLastAppliedVirtualFoV_22 = value;
	}

	inline static int32_t get_offset_of_mNewNearClipPlane_23() { return static_cast<int32_t>(offsetof(StereoCameraConfiguration_t2173582563, ___mNewNearClipPlane_23)); }
	inline float get_mNewNearClipPlane_23() const { return ___mNewNearClipPlane_23; }
	inline float* get_address_of_mNewNearClipPlane_23() { return &___mNewNearClipPlane_23; }
	inline void set_mNewNearClipPlane_23(float value)
	{
		___mNewNearClipPlane_23 = value;
	}

	inline static int32_t get_offset_of_mNewFarClipPlane_24() { return static_cast<int32_t>(offsetof(StereoCameraConfiguration_t2173582563, ___mNewFarClipPlane_24)); }
	inline float get_mNewFarClipPlane_24() const { return ___mNewFarClipPlane_24; }
	inline float* get_address_of_mNewFarClipPlane_24() { return &___mNewFarClipPlane_24; }
	inline void set_mNewFarClipPlane_24(float value)
	{
		___mNewFarClipPlane_24 = value;
	}

	inline static int32_t get_offset_of_mNewVirtualFoV_25() { return static_cast<int32_t>(offsetof(StereoCameraConfiguration_t2173582563, ___mNewVirtualFoV_25)); }
	inline float get_mNewVirtualFoV_25() const { return ___mNewVirtualFoV_25; }
	inline float* get_address_of_mNewVirtualFoV_25() { return &___mNewVirtualFoV_25; }
	inline void set_mNewVirtualFoV_25(float value)
	{
		___mNewVirtualFoV_25 = value;
	}

	inline static int32_t get_offset_of_mCameraOffset_26() { return static_cast<int32_t>(offsetof(StereoCameraConfiguration_t2173582563, ___mCameraOffset_26)); }
	inline float get_mCameraOffset_26() const { return ___mCameraOffset_26; }
	inline float* get_address_of_mCameraOffset_26() { return &___mCameraOffset_26; }
	inline void set_mCameraOffset_26(float value)
	{
		___mCameraOffset_26 = value;
	}

	inline static int32_t get_offset_of_mEyewearUserCalibrationProfileId_27() { return static_cast<int32_t>(offsetof(StereoCameraConfiguration_t2173582563, ___mEyewearUserCalibrationProfileId_27)); }
	inline int32_t get_mEyewearUserCalibrationProfileId_27() const { return ___mEyewearUserCalibrationProfileId_27; }
	inline int32_t* get_address_of_mEyewearUserCalibrationProfileId_27() { return &___mEyewearUserCalibrationProfileId_27; }
	inline void set_mEyewearUserCalibrationProfileId_27(int32_t value)
	{
		___mEyewearUserCalibrationProfileId_27 = value;
	}
};

struct StereoCameraConfiguration_t2173582563_StaticFields
{
public:
	// UnityEngine.Vector4 Vuforia.StereoCameraConfiguration::MIN_CENTER
	Vector4_t4282066567  ___MIN_CENTER_13;
	// UnityEngine.Vector4 Vuforia.StereoCameraConfiguration::MAX_CENTER
	Vector4_t4282066567  ___MAX_CENTER_14;
	// UnityEngine.Vector4 Vuforia.StereoCameraConfiguration::MAX_BOTTOM
	Vector4_t4282066567  ___MAX_BOTTOM_15;
	// UnityEngine.Vector4 Vuforia.StereoCameraConfiguration::MAX_TOP
	Vector4_t4282066567  ___MAX_TOP_16;

public:
	inline static int32_t get_offset_of_MIN_CENTER_13() { return static_cast<int32_t>(offsetof(StereoCameraConfiguration_t2173582563_StaticFields, ___MIN_CENTER_13)); }
	inline Vector4_t4282066567  get_MIN_CENTER_13() const { return ___MIN_CENTER_13; }
	inline Vector4_t4282066567 * get_address_of_MIN_CENTER_13() { return &___MIN_CENTER_13; }
	inline void set_MIN_CENTER_13(Vector4_t4282066567  value)
	{
		___MIN_CENTER_13 = value;
	}

	inline static int32_t get_offset_of_MAX_CENTER_14() { return static_cast<int32_t>(offsetof(StereoCameraConfiguration_t2173582563_StaticFields, ___MAX_CENTER_14)); }
	inline Vector4_t4282066567  get_MAX_CENTER_14() const { return ___MAX_CENTER_14; }
	inline Vector4_t4282066567 * get_address_of_MAX_CENTER_14() { return &___MAX_CENTER_14; }
	inline void set_MAX_CENTER_14(Vector4_t4282066567  value)
	{
		___MAX_CENTER_14 = value;
	}

	inline static int32_t get_offset_of_MAX_BOTTOM_15() { return static_cast<int32_t>(offsetof(StereoCameraConfiguration_t2173582563_StaticFields, ___MAX_BOTTOM_15)); }
	inline Vector4_t4282066567  get_MAX_BOTTOM_15() const { return ___MAX_BOTTOM_15; }
	inline Vector4_t4282066567 * get_address_of_MAX_BOTTOM_15() { return &___MAX_BOTTOM_15; }
	inline void set_MAX_BOTTOM_15(Vector4_t4282066567  value)
	{
		___MAX_BOTTOM_15 = value;
	}

	inline static int32_t get_offset_of_MAX_TOP_16() { return static_cast<int32_t>(offsetof(StereoCameraConfiguration_t2173582563_StaticFields, ___MAX_TOP_16)); }
	inline Vector4_t4282066567  get_MAX_TOP_16() const { return ___MAX_TOP_16; }
	inline Vector4_t4282066567 * get_address_of_MAX_TOP_16() { return &___MAX_TOP_16; }
	inline void set_MAX_TOP_16(Vector4_t4282066567  value)
	{
		___MAX_TOP_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
