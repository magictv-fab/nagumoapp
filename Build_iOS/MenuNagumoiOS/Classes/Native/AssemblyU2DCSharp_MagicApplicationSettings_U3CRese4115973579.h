﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IO.FileInfo
struct FileInfo_t3233670074;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicApplicationSettings/<ResetHard>c__AnonStorey94
struct  U3CResetHardU3Ec__AnonStorey94_t4115973579  : public Il2CppObject
{
public:
	// System.IO.FileInfo MagicApplicationSettings/<ResetHard>c__AnonStorey94::f
	FileInfo_t3233670074 * ___f_0;

public:
	inline static int32_t get_offset_of_f_0() { return static_cast<int32_t>(offsetof(U3CResetHardU3Ec__AnonStorey94_t4115973579, ___f_0)); }
	inline FileInfo_t3233670074 * get_f_0() const { return ___f_0; }
	inline FileInfo_t3233670074 ** get_address_of_f_0() { return &___f_0; }
	inline void set_f_0(FileInfo_t3233670074 * value)
	{
		___f_0 = value;
		Il2CppCodeGenWriteBarrier(&___f_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
