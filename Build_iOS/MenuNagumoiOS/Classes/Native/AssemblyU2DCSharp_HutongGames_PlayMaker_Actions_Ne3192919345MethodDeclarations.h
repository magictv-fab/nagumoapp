﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkGetIsMessageQueueRunning
struct NetworkGetIsMessageQueueRunning_t3192919345;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkGetIsMessageQueueRunning::.ctor()
extern "C"  void NetworkGetIsMessageQueueRunning__ctor_m1126013221 (NetworkGetIsMessageQueueRunning_t3192919345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetIsMessageQueueRunning::Reset()
extern "C"  void NetworkGetIsMessageQueueRunning_Reset_m3067413458 (NetworkGetIsMessageQueueRunning_t3192919345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetIsMessageQueueRunning::OnEnter()
extern "C"  void NetworkGetIsMessageQueueRunning_OnEnter_m3453642684 (NetworkGetIsMessageQueueRunning_t3192919345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
