﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DownloadImages/<CheckFilesUpdate>c__Iterator19
struct U3CCheckFilesUpdateU3Ec__Iterator19_t2563746816;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void DownloadImages/<CheckFilesUpdate>c__Iterator19::.ctor()
extern "C"  void U3CCheckFilesUpdateU3Ec__Iterator19__ctor_m1280393371 (U3CCheckFilesUpdateU3Ec__Iterator19_t2563746816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DownloadImages/<CheckFilesUpdate>c__Iterator19::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckFilesUpdateU3Ec__Iterator19_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2097116001 (U3CCheckFilesUpdateU3Ec__Iterator19_t2563746816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DownloadImages/<CheckFilesUpdate>c__Iterator19::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckFilesUpdateU3Ec__Iterator19_System_Collections_IEnumerator_get_Current_m991844597 (U3CCheckFilesUpdateU3Ec__Iterator19_t2563746816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DownloadImages/<CheckFilesUpdate>c__Iterator19::MoveNext()
extern "C"  bool U3CCheckFilesUpdateU3Ec__Iterator19_MoveNext_m1358844001 (U3CCheckFilesUpdateU3Ec__Iterator19_t2563746816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DownloadImages/<CheckFilesUpdate>c__Iterator19::Dispose()
extern "C"  void U3CCheckFilesUpdateU3Ec__Iterator19_Dispose_m1995665944 (U3CCheckFilesUpdateU3Ec__Iterator19_t2563746816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DownloadImages/<CheckFilesUpdate>c__Iterator19::Reset()
extern "C"  void U3CCheckFilesUpdateU3Ec__Iterator19_Reset_m3221793608 (U3CCheckFilesUpdateU3Ec__Iterator19_t2563746816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
