﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FunctionCall
struct FunctionCall_t3279845016;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FunctionCall3279845016.h"
#include "mscorlib_System_String7231557.h"

// System.Void HutongGames.PlayMaker.FunctionCall::.ctor()
extern "C"  void FunctionCall__ctor_m598581563 (FunctionCall_t3279845016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FunctionCall::.ctor(HutongGames.PlayMaker.FunctionCall)
extern "C"  void FunctionCall__ctor_m3052794905 (FunctionCall_t3279845016 * __this, FunctionCall_t3279845016 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FunctionCall::ResetParameters()
extern "C"  void FunctionCall_ResetParameters_m3253541554 (FunctionCall_t3279845016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FunctionCall::get_ParameterType()
extern "C"  String_t* FunctionCall_get_ParameterType_m540713776 (FunctionCall_t3279845016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FunctionCall::set_ParameterType(System.String)
extern "C"  void FunctionCall_set_ParameterType_m2447054627 (FunctionCall_t3279845016 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
