﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SampleInitErrorHandler/ErrorData
struct ErrorData_t135901855;
struct ErrorData_t135901855_marshaled_pinvoke;
struct ErrorData_t135901855_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct ErrorData_t135901855;
struct ErrorData_t135901855_marshaled_pinvoke;

extern "C" void ErrorData_t135901855_marshal_pinvoke(const ErrorData_t135901855& unmarshaled, ErrorData_t135901855_marshaled_pinvoke& marshaled);
extern "C" void ErrorData_t135901855_marshal_pinvoke_back(const ErrorData_t135901855_marshaled_pinvoke& marshaled, ErrorData_t135901855& unmarshaled);
extern "C" void ErrorData_t135901855_marshal_pinvoke_cleanup(ErrorData_t135901855_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct ErrorData_t135901855;
struct ErrorData_t135901855_marshaled_com;

extern "C" void ErrorData_t135901855_marshal_com(const ErrorData_t135901855& unmarshaled, ErrorData_t135901855_marshaled_com& marshaled);
extern "C" void ErrorData_t135901855_marshal_com_back(const ErrorData_t135901855_marshaled_com& marshaled, ErrorData_t135901855& unmarshaled);
extern "C" void ErrorData_t135901855_marshal_com_cleanup(ErrorData_t135901855_marshaled_com& marshaled);
