﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmFloat2134102846.h"

// System.Single HutongGames.PlayMaker.FsmFloat::get_Value()
extern "C"  float FsmFloat_get_Value_m4137923823 (FsmFloat_t2134102846 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmFloat::set_Value(System.Single)
extern "C"  void FsmFloat_set_Value_m1568963140 (FsmFloat_t2134102846 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmFloat::.ctor()
extern "C"  void FsmFloat__ctor_m3007896661 (FsmFloat_t2134102846 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmFloat::.ctor(System.String)
extern "C"  void FsmFloat__ctor_m1342321869 (FsmFloat_t2134102846 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmFloat::.ctor(HutongGames.PlayMaker.FsmFloat)
extern "C"  void FsmFloat__ctor_m1443600077 (FsmFloat_t2134102846 * __this, FsmFloat_t2134102846 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmFloat::ToString()
extern "C"  String_t* FsmFloat_ToString_m3208952222 (FsmFloat_t2134102846 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.FsmFloat::op_Implicit(System.Single)
extern "C"  FsmFloat_t2134102846 * FsmFloat_op_Implicit_m3142426606 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
