﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUIVerticalSlider
struct GUIVerticalSlider_t4042007136;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUIVerticalSlider::.ctor()
extern "C"  void GUIVerticalSlider__ctor_m2718967510 (GUIVerticalSlider_t4042007136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUIVerticalSlider::Reset()
extern "C"  void GUIVerticalSlider_Reset_m365400451 (GUIVerticalSlider_t4042007136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUIVerticalSlider::OnGUI()
extern "C"  void GUIVerticalSlider_OnGUI_m2214366160 (GUIVerticalSlider_t4042007136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
