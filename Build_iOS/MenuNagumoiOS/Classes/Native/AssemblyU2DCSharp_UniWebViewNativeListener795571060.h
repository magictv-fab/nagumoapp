﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>
struct Dictionary_2_t1615989430;
// UniWebView
struct UniWebView_t424341801;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewNativeListener
struct  UniWebViewNativeListener_t795571060  : public MonoBehaviour_t667441552
{
public:
	// UniWebView UniWebViewNativeListener::webView
	UniWebView_t424341801 * ___webView_3;

public:
	inline static int32_t get_offset_of_webView_3() { return static_cast<int32_t>(offsetof(UniWebViewNativeListener_t795571060, ___webView_3)); }
	inline UniWebView_t424341801 * get_webView_3() const { return ___webView_3; }
	inline UniWebView_t424341801 ** get_address_of_webView_3() { return &___webView_3; }
	inline void set_webView_3(UniWebView_t424341801 * value)
	{
		___webView_3 = value;
		Il2CppCodeGenWriteBarrier(&___webView_3, value);
	}
};

struct UniWebViewNativeListener_t795571060_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener> UniWebViewNativeListener::listeners
	Dictionary_2_t1615989430 * ___listeners_2;

public:
	inline static int32_t get_offset_of_listeners_2() { return static_cast<int32_t>(offsetof(UniWebViewNativeListener_t795571060_StaticFields, ___listeners_2)); }
	inline Dictionary_2_t1615989430 * get_listeners_2() const { return ___listeners_2; }
	inline Dictionary_2_t1615989430 ** get_address_of_listeners_2() { return &___listeners_2; }
	inline void set_listeners_2(Dictionary_2_t1615989430 * value)
	{
		___listeners_2 = value;
		Il2CppCodeGenWriteBarrier(&___listeners_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
