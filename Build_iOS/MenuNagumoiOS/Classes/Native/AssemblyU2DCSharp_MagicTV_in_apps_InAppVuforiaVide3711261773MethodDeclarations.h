﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.in_apps.InAppVuforiaVideoPresentation
struct InAppVuforiaVideoPresentation_t3711261773;
// MagicTV.vo.BundleVO
struct BundleVO_t1984518073;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_vo_BundleVO1984518073.h"
#include "mscorlib_System_String7231557.h"

// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::.ctor()
extern "C"  void InAppVuforiaVideoPresentation__ctor_m3209393757 (InAppVuforiaVideoPresentation_t3711261773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::Awake()
extern "C"  void InAppVuforiaVideoPresentation_Awake_m3446998976 (InAppVuforiaVideoPresentation_t3711261773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::getVideoComponent()
extern "C"  void InAppVuforiaVideoPresentation_getVideoComponent_m2866736723 (InAppVuforiaVideoPresentation_t3711261773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::Start()
extern "C"  void InAppVuforiaVideoPresentation_Start_m2156531549 (InAppVuforiaVideoPresentation_t3711261773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::DoRun()
extern "C"  void InAppVuforiaVideoPresentation_DoRun_m1571963259 (InAppVuforiaVideoPresentation_t3711261773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::SetBundleVO(MagicTV.vo.BundleVO)
extern "C"  void InAppVuforiaVideoPresentation_SetBundleVO_m3324820553 (InAppVuforiaVideoPresentation_t3711261773 * __this, BundleVO_t1984518073 * ___bundle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::lightConfig()
extern "C"  void InAppVuforiaVideoPresentation_lightConfig_m1006978579 (InAppVuforiaVideoPresentation_t3711261773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::prepare()
extern "C"  void InAppVuforiaVideoPresentation_prepare_m1367862370 (InAppVuforiaVideoPresentation_t3711261773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::doPrepare()
extern "C"  void InAppVuforiaVideoPresentation_doPrepare_m2917682583 (InAppVuforiaVideoPresentation_t3711261773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::prepareVideo()
extern "C"  void InAppVuforiaVideoPresentation_prepareVideo_m3725331419 (InAppVuforiaVideoPresentation_t3711261773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::initVideoComponent()
extern "C"  void InAppVuforiaVideoPresentation_initVideoComponent_m536785945 (InAppVuforiaVideoPresentation_t3711261773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::setVideoComponentActivated()
extern "C"  void InAppVuforiaVideoPresentation_setVideoComponentActivated_m792138196 (InAppVuforiaVideoPresentation_t3711261773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::debugFloat1Change(System.Single)
extern "C"  void InAppVuforiaVideoPresentation_debugFloat1Change_m1613640344 (InAppVuforiaVideoPresentation_t3711261773 * __this, float ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::RaiseFloat1()
extern "C"  void InAppVuforiaVideoPresentation_RaiseFloat1_m1484496924 (InAppVuforiaVideoPresentation_t3711261773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::debugFloat2Change(System.Single)
extern "C"  void InAppVuforiaVideoPresentation_debugFloat2Change_m3870551351 (InAppVuforiaVideoPresentation_t3711261773 * __this, float ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::RaiseFloat2()
extern "C"  void InAppVuforiaVideoPresentation_RaiseFloat2_m1484497885 (InAppVuforiaVideoPresentation_t3711261773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::debugFloat3Change(System.Single)
extern "C"  void InAppVuforiaVideoPresentation_debugFloat3Change_m1832495062 (InAppVuforiaVideoPresentation_t3711261773 * __this, float ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::RaiseFloat3()
extern "C"  void InAppVuforiaVideoPresentation_RaiseFloat3_m1484498846 (InAppVuforiaVideoPresentation_t3711261773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::debugFloat4Change(System.Single)
extern "C"  void InAppVuforiaVideoPresentation_debugFloat4Change_m4089406069 (InAppVuforiaVideoPresentation_t3711261773 * __this, float ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::RaiseFloat4()
extern "C"  void InAppVuforiaVideoPresentation_RaiseFloat4_m1484499807 (InAppVuforiaVideoPresentation_t3711261773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::applyChromakey()
extern "C"  void InAppVuforiaVideoPresentation_applyChromakey_m1441050946 (InAppVuforiaVideoPresentation_t3711261773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::checkComponentsPrepared()
extern "C"  void InAppVuforiaVideoPresentation_checkComponentsPrepared_m1623935734 (InAppVuforiaVideoPresentation_t3711261773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::DoStop()
extern "C"  void InAppVuforiaVideoPresentation_DoStop_m1514025396 (InAppVuforiaVideoPresentation_t3711261773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::Dispose()
extern "C"  void InAppVuforiaVideoPresentation_Dispose_m339165018 (InAppVuforiaVideoPresentation_t3711261773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.in_apps.InAppVuforiaVideoPresentation::GetInAppName()
extern "C"  String_t* InAppVuforiaVideoPresentation_GetInAppName_m4157962869 (InAppVuforiaVideoPresentation_t3711261773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::onFinishHandler()
extern "C"  void InAppVuforiaVideoPresentation_onFinishHandler_m2551206067 (InAppVuforiaVideoPresentation_t3711261773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::Play()
extern "C"  void InAppVuforiaVideoPresentation_Play_m3717072251 (InAppVuforiaVideoPresentation_t3711261773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::Pause()
extern "C"  void InAppVuforiaVideoPresentation_Pause_m3263519729 (InAppVuforiaVideoPresentation_t3711261773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::RewindAndPause()
extern "C"  void InAppVuforiaVideoPresentation_RewindAndPause_m2212159873 (InAppVuforiaVideoPresentation_t3711261773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::RewindAndPlay()
extern "C"  void InAppVuforiaVideoPresentation_RewindAndPlay_m1604947435 (InAppVuforiaVideoPresentation_t3711261773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::Hide()
extern "C"  void InAppVuforiaVideoPresentation_Hide_m3485338633 (InAppVuforiaVideoPresentation_t3711261773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::Show()
extern "C"  void InAppVuforiaVideoPresentation_Show_m3799680772 (InAppVuforiaVideoPresentation_t3711261773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::CustomAction(System.String)
extern "C"  void InAppVuforiaVideoPresentation_CustomAction_m1732046132 (InAppVuforiaVideoPresentation_t3711261773 * __this, String_t* ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::AnaltyticsOnFinish()
extern "C"  void InAppVuforiaVideoPresentation_AnaltyticsOnFinish_m2065060347 (InAppVuforiaVideoPresentation_t3711261773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::AnaltyticsOnPause()
extern "C"  void InAppVuforiaVideoPresentation_AnaltyticsOnPause_m1653451632 (InAppVuforiaVideoPresentation_t3711261773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppVuforiaVideoPresentation::AnaltyticsOnPlay()
extern "C"  void InAppVuforiaVideoPresentation_AnaltyticsOnPlay_m1032735260 (InAppVuforiaVideoPresentation_t3711261773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.in_apps.InAppVuforiaVideoPresentation::GetAnalyticsCategory()
extern "C"  String_t* InAppVuforiaVideoPresentation_GetAnalyticsCategory_m2845409650 (InAppVuforiaVideoPresentation_t3711261773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
