﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.RSS.AbstractRSSReader
struct AbstractRSSReader_t1625773429;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Single[]
struct SingleU5BU5D_t2316563989;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t1820556512;

#include "codegen/il2cpp-codegen.h"

// System.Void ZXing.OneD.RSS.AbstractRSSReader::.ctor()
extern "C"  void AbstractRSSReader__ctor_m2437858922 (AbstractRSSReader_t1625773429 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.OneD.RSS.AbstractRSSReader::getDecodeFinderCounters()
extern "C"  Int32U5BU5D_t3230847821* AbstractRSSReader_getDecodeFinderCounters_m51722173 (AbstractRSSReader_t1625773429 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.OneD.RSS.AbstractRSSReader::getDataCharacterCounters()
extern "C"  Int32U5BU5D_t3230847821* AbstractRSSReader_getDataCharacterCounters_m1637956518 (AbstractRSSReader_t1625773429 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] ZXing.OneD.RSS.AbstractRSSReader::getOddRoundingErrors()
extern "C"  SingleU5BU5D_t2316563989* AbstractRSSReader_getOddRoundingErrors_m2416827036 (AbstractRSSReader_t1625773429 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] ZXing.OneD.RSS.AbstractRSSReader::getEvenRoundingErrors()
extern "C"  SingleU5BU5D_t2316563989* AbstractRSSReader_getEvenRoundingErrors_m1518955085 (AbstractRSSReader_t1625773429 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.OneD.RSS.AbstractRSSReader::getOddCounts()
extern "C"  Int32U5BU5D_t3230847821* AbstractRSSReader_getOddCounts_m2795968643 (AbstractRSSReader_t1625773429 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.OneD.RSS.AbstractRSSReader::getEvenCounts()
extern "C"  Int32U5BU5D_t3230847821* AbstractRSSReader_getEvenCounts_m682491248 (AbstractRSSReader_t1625773429 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.RSS.AbstractRSSReader::parseFinderValue(System.Int32[],System.Int32[][],System.Int32&)
extern "C"  bool AbstractRSSReader_parseFinderValue_m2780614707 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___counters0, Int32U5BU5DU5BU5D_t1820556512* ___finderPatterns1, int32_t* ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.RSS.AbstractRSSReader::count(System.Int32[])
extern "C"  int32_t AbstractRSSReader_count_m1437904344 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.AbstractRSSReader::increment(System.Int32[],System.Single[])
extern "C"  void AbstractRSSReader_increment_m1146395785 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___array0, SingleU5BU5D_t2316563989* ___errors1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.AbstractRSSReader::decrement(System.Int32[],System.Single[])
extern "C"  void AbstractRSSReader_decrement_m1965444525 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___array0, SingleU5BU5D_t2316563989* ___errors1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.RSS.AbstractRSSReader::isFinderPattern(System.Int32[])
extern "C"  bool AbstractRSSReader_isFinderPattern_m3679415075 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___counters0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.AbstractRSSReader::.cctor()
extern "C"  void AbstractRSSReader__cctor_m2077086339 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
