﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutEndHorizontal
struct GUILayoutEndHorizontal_t2025169154;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutEndHorizontal::.ctor()
extern "C"  void GUILayoutEndHorizontal__ctor_m1190515108 (GUILayoutEndHorizontal_t2025169154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutEndHorizontal::Reset()
extern "C"  void GUILayoutEndHorizontal_Reset_m3131915345 (GUILayoutEndHorizontal_t2025169154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutEndHorizontal::OnGUI()
extern "C"  void GUILayoutEndHorizontal_OnGUI_m685913758 (GUILayoutEndHorizontal_t2025169154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
