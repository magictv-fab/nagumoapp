﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MobileNativeDialog
struct MobileNativeDialog_t811616801;
// System.String
struct String_t;
// UnionAssets.FLE.CEvent
struct CEvent_t44106931;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_UnionAssets_FLE_CEvent44106931.h"
#include "AssemblyU2DCSharp_MNDialogResult3125317574.h"

// System.Void MobileNativeDialog::.ctor(System.String,System.String)
extern "C"  void MobileNativeDialog__ctor_m1050566628 (MobileNativeDialog_t811616801 * __this, String_t* ___title0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MobileNativeDialog::.ctor(System.String,System.String,System.String,System.String)
extern "C"  void MobileNativeDialog__ctor_m1741330780 (MobileNativeDialog_t811616801 * __this, String_t* ___title0, String_t* ___message1, String_t* ___yes2, String_t* ___no3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MobileNativeDialog::init(System.String,System.String,System.String,System.String)
extern "C"  void MobileNativeDialog_init_m2433003932 (MobileNativeDialog_t811616801 * __this, String_t* ___title0, String_t* ___message1, String_t* ___yes2, String_t* ___no3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MobileNativeDialog::OnCompleteListener(UnionAssets.FLE.CEvent)
extern "C"  void MobileNativeDialog_OnCompleteListener_m3323750452 (MobileNativeDialog_t811616801 * __this, CEvent_t44106931 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MobileNativeDialog::<OnComplete>m__9(MNDialogResult)
extern "C"  void MobileNativeDialog_U3COnCompleteU3Em__9_m205011194 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
