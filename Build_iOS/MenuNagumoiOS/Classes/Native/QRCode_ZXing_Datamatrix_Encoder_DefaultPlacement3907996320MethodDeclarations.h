﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Datamatrix.Encoder.DefaultPlacement
struct DefaultPlacement_t3907996320;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ZXing.Datamatrix.Encoder.DefaultPlacement::.ctor(System.String,System.Int32,System.Int32)
extern "C"  void DefaultPlacement__ctor_m3379509414 (DefaultPlacement_t3907996320 * __this, String_t* ___codewords0, int32_t ___numcols1, int32_t ___numrows2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Datamatrix.Encoder.DefaultPlacement::getBit(System.Int32,System.Int32)
extern "C"  bool DefaultPlacement_getBit_m1420824507 (DefaultPlacement_t3907996320 * __this, int32_t ___col0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.DefaultPlacement::setBit(System.Int32,System.Int32,System.Boolean)
extern "C"  void DefaultPlacement_setBit_m2853824842 (DefaultPlacement_t3907996320 * __this, int32_t ___col0, int32_t ___row1, bool ___bit2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Datamatrix.Encoder.DefaultPlacement::hasBit(System.Int32,System.Int32)
extern "C"  bool DefaultPlacement_hasBit_m2631795711 (DefaultPlacement_t3907996320 * __this, int32_t ___col0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.DefaultPlacement::place()
extern "C"  void DefaultPlacement_place_m1324671553 (DefaultPlacement_t3907996320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.DefaultPlacement::module(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void DefaultPlacement_module_m942485234 (DefaultPlacement_t3907996320 * __this, int32_t ___row0, int32_t ___col1, int32_t ___pos2, int32_t ___bit3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.DefaultPlacement::utah(System.Int32,System.Int32,System.Int32)
extern "C"  void DefaultPlacement_utah_m2922746463 (DefaultPlacement_t3907996320 * __this, int32_t ___row0, int32_t ___col1, int32_t ___pos2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.DefaultPlacement::corner1(System.Int32)
extern "C"  void DefaultPlacement_corner1_m1566855815 (DefaultPlacement_t3907996320 * __this, int32_t ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.DefaultPlacement::corner2(System.Int32)
extern "C"  void DefaultPlacement_corner2_m3074407624 (DefaultPlacement_t3907996320 * __this, int32_t ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.DefaultPlacement::corner3(System.Int32)
extern "C"  void DefaultPlacement_corner3_m286992137 (DefaultPlacement_t3907996320 * __this, int32_t ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.DefaultPlacement::corner4(System.Int32)
extern "C"  void DefaultPlacement_corner4_m1794543946 (DefaultPlacement_t3907996320 * __this, int32_t ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
