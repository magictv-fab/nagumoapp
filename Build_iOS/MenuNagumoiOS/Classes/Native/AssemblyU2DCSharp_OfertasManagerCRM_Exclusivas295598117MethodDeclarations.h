﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertasManagerCRM_Exclusivas
struct OfertasManagerCRM_Exclusivas_t295598117;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void OfertasManagerCRM_Exclusivas::.ctor()
extern "C"  void OfertasManagerCRM_Exclusivas__ctor_m2942739478 (OfertasManagerCRM_Exclusivas_t295598117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Exclusivas::.cctor()
extern "C"  void OfertasManagerCRM_Exclusivas__cctor_m548514391 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Exclusivas::Start()
extern "C"  void OfertasManagerCRM_Exclusivas_Start_m1889877270 (OfertasManagerCRM_Exclusivas_t295598117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Exclusivas::ShowNumberSelected(System.Int32)
extern "C"  void OfertasManagerCRM_Exclusivas_ShowNumberSelected_m2933772192 (OfertasManagerCRM_Exclusivas_t295598117 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Exclusivas::InstantiateNextItem()
extern "C"  void OfertasManagerCRM_Exclusivas_InstantiateNextItem_m1572187556 (OfertasManagerCRM_Exclusivas_t295598117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Exclusivas::SendStatus(System.Int32)
extern "C"  void OfertasManagerCRM_Exclusivas_SendStatus_m2314320153 (OfertasManagerCRM_Exclusivas_t295598117 * __this, int32_t ___status0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Exclusivas::BtnAccept()
extern "C"  void OfertasManagerCRM_Exclusivas_BtnAccept_m2584416568 (OfertasManagerCRM_Exclusivas_t295598117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Exclusivas::BtnRecuse()
extern "C"  void OfertasManagerCRM_Exclusivas_BtnRecuse_m3937279847 (OfertasManagerCRM_Exclusivas_t295598117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OfertasManagerCRM_Exclusivas::ISendStatus(System.String,System.Int32)
extern "C"  Il2CppObject * OfertasManagerCRM_Exclusivas_ISendStatus_m2358689316 (OfertasManagerCRM_Exclusivas_t295598117 * __this, String_t* ___id0, int32_t ___status1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OfertasManagerCRM_Exclusivas::ILoadOfertas()
extern "C"  Il2CppObject * OfertasManagerCRM_Exclusivas_ILoadOfertas_m1167346537 (OfertasManagerCRM_Exclusivas_t295598117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Exclusivas::Update()
extern "C"  void OfertasManagerCRM_Exclusivas_Update_m2757472695 (OfertasManagerCRM_Exclusivas_t295598117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Exclusivas::LoadMoreItens()
extern "C"  void OfertasManagerCRM_Exclusivas_LoadMoreItens_m4096486520 (OfertasManagerCRM_Exclusivas_t295598117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OfertasManagerCRM_Exclusivas::GetDateString(System.String)
extern "C"  String_t* OfertasManagerCRM_Exclusivas_GetDateString_m1107228604 (Il2CppObject * __this /* static, unused */, String_t* ___date0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OfertasManagerCRM_Exclusivas::GetDateTimeString(System.String)
extern "C"  String_t* OfertasManagerCRM_Exclusivas_GetDateTimeString_m3643449583 (Il2CppObject * __this /* static, unused */, String_t* ___date0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OfertasManagerCRM_Exclusivas::ILoadImgs()
extern "C"  Il2CppObject * OfertasManagerCRM_Exclusivas_ILoadImgs_m3997262155 (OfertasManagerCRM_Exclusivas_t295598117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OfertasManagerCRM_Exclusivas::LoadLoadeds()
extern "C"  Il2CppObject * OfertasManagerCRM_Exclusivas_LoadLoadeds_m4178280180 (OfertasManagerCRM_Exclusivas_t295598117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Exclusivas::PopUp(System.String)
extern "C"  void OfertasManagerCRM_Exclusivas_PopUp_m3150187522 (OfertasManagerCRM_Exclusivas_t295598117 * __this, String_t* ___txt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
