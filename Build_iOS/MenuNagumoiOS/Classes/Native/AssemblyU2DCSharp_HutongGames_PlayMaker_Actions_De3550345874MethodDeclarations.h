﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DebugDrawShape
struct DebugDrawShape_t3550345874;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DebugDrawShape::.ctor()
extern "C"  void DebugDrawShape__ctor_m4008759828 (DebugDrawShape_t3550345874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DebugDrawShape::Reset()
extern "C"  void DebugDrawShape_Reset_m1655192769 (DebugDrawShape_t3550345874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DebugDrawShape::OnDrawGizmos()
extern "C"  void DebugDrawShape_OnDrawGizmos_m528515212 (DebugDrawShape_t3550345874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
