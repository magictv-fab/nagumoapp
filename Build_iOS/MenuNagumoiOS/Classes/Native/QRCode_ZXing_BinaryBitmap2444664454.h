﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.Binarizer
struct Binarizer_t1492033400;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.BinaryBitmap
struct  BinaryBitmap_t2444664454  : public Il2CppObject
{
public:
	// ZXing.Binarizer ZXing.BinaryBitmap::binarizer
	Binarizer_t1492033400 * ___binarizer_0;
	// ZXing.Common.BitMatrix ZXing.BinaryBitmap::matrix
	BitMatrix_t1058711404 * ___matrix_1;

public:
	inline static int32_t get_offset_of_binarizer_0() { return static_cast<int32_t>(offsetof(BinaryBitmap_t2444664454, ___binarizer_0)); }
	inline Binarizer_t1492033400 * get_binarizer_0() const { return ___binarizer_0; }
	inline Binarizer_t1492033400 ** get_address_of_binarizer_0() { return &___binarizer_0; }
	inline void set_binarizer_0(Binarizer_t1492033400 * value)
	{
		___binarizer_0 = value;
		Il2CppCodeGenWriteBarrier(&___binarizer_0, value);
	}

	inline static int32_t get_offset_of_matrix_1() { return static_cast<int32_t>(offsetof(BinaryBitmap_t2444664454, ___matrix_1)); }
	inline BitMatrix_t1058711404 * get_matrix_1() const { return ___matrix_1; }
	inline BitMatrix_t1058711404 ** get_address_of_matrix_1() { return &___matrix_1; }
	inline void set_matrix_1(BitMatrix_t1058711404 * value)
	{
		___matrix_1 = value;
		Il2CppCodeGenWriteBarrier(&___matrix_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
