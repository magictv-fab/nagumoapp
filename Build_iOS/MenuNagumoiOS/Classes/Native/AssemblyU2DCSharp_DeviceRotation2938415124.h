﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeviceRotation
struct  DeviceRotation_t2938415124  : public Il2CppObject
{
public:

public:
};

struct DeviceRotation_t2938415124_StaticFields
{
public:
	// System.Boolean DeviceRotation::gyroInitialized
	bool ___gyroInitialized_0;

public:
	inline static int32_t get_offset_of_gyroInitialized_0() { return static_cast<int32_t>(offsetof(DeviceRotation_t2938415124_StaticFields, ___gyroInitialized_0)); }
	inline bool get_gyroInitialized_0() const { return ___gyroInitialized_0; }
	inline bool* get_address_of_gyroInitialized_0() { return &___gyroInitialized_0; }
	inline void set_gyroInitialized_0(bool value)
	{
		___gyroInitialized_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
