﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ServerControl
struct ServerControl_t2725829754;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// LightsControl
struct LightsControl_t2444629088;
// GameMain
struct GameMain_t2590236907;
// ServerControl/OnPlayDelegate
struct OnPlayDelegate_t3097072301;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ServerControl
struct  ServerControl_t2725829754  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject ServerControl::popup
	GameObject_t3674682005 * ___popup_11;
	// LightsControl ServerControl::lightControl
	LightsControl_t2444629088 * ___lightControl_12;
	// GameMain ServerControl::gameMain
	GameMain_t2590236907 * ___gameMain_13;
	// System.String ServerControl::alertMessage
	String_t* ___alertMessage_14;
	// ServerControl/OnPlayDelegate ServerControl::OnPlay
	OnPlayDelegate_t3097072301 * ___OnPlay_15;

public:
	inline static int32_t get_offset_of_popup_11() { return static_cast<int32_t>(offsetof(ServerControl_t2725829754, ___popup_11)); }
	inline GameObject_t3674682005 * get_popup_11() const { return ___popup_11; }
	inline GameObject_t3674682005 ** get_address_of_popup_11() { return &___popup_11; }
	inline void set_popup_11(GameObject_t3674682005 * value)
	{
		___popup_11 = value;
		Il2CppCodeGenWriteBarrier(&___popup_11, value);
	}

	inline static int32_t get_offset_of_lightControl_12() { return static_cast<int32_t>(offsetof(ServerControl_t2725829754, ___lightControl_12)); }
	inline LightsControl_t2444629088 * get_lightControl_12() const { return ___lightControl_12; }
	inline LightsControl_t2444629088 ** get_address_of_lightControl_12() { return &___lightControl_12; }
	inline void set_lightControl_12(LightsControl_t2444629088 * value)
	{
		___lightControl_12 = value;
		Il2CppCodeGenWriteBarrier(&___lightControl_12, value);
	}

	inline static int32_t get_offset_of_gameMain_13() { return static_cast<int32_t>(offsetof(ServerControl_t2725829754, ___gameMain_13)); }
	inline GameMain_t2590236907 * get_gameMain_13() const { return ___gameMain_13; }
	inline GameMain_t2590236907 ** get_address_of_gameMain_13() { return &___gameMain_13; }
	inline void set_gameMain_13(GameMain_t2590236907 * value)
	{
		___gameMain_13 = value;
		Il2CppCodeGenWriteBarrier(&___gameMain_13, value);
	}

	inline static int32_t get_offset_of_alertMessage_14() { return static_cast<int32_t>(offsetof(ServerControl_t2725829754, ___alertMessage_14)); }
	inline String_t* get_alertMessage_14() const { return ___alertMessage_14; }
	inline String_t** get_address_of_alertMessage_14() { return &___alertMessage_14; }
	inline void set_alertMessage_14(String_t* value)
	{
		___alertMessage_14 = value;
		Il2CppCodeGenWriteBarrier(&___alertMessage_14, value);
	}

	inline static int32_t get_offset_of_OnPlay_15() { return static_cast<int32_t>(offsetof(ServerControl_t2725829754, ___OnPlay_15)); }
	inline OnPlayDelegate_t3097072301 * get_OnPlay_15() const { return ___OnPlay_15; }
	inline OnPlayDelegate_t3097072301 ** get_address_of_OnPlay_15() { return &___OnPlay_15; }
	inline void set_OnPlay_15(OnPlayDelegate_t3097072301 * value)
	{
		___OnPlay_15 = value;
		Il2CppCodeGenWriteBarrier(&___OnPlay_15, value);
	}
};

struct ServerControl_t2725829754_StaticFields
{
public:
	// ServerControl ServerControl::instance
	ServerControl_t2725829754 * ___instance_2;
	// System.String ServerControl::url
	String_t* ___url_3;
	// System.String ServerControl::urlCRM
	String_t* ___urlCRM_4;
	// System.String ServerControl::urlIMGOfertas
	String_t* ___urlIMGOfertas_5;
	// System.String ServerControl::urlIMGPublicacoes
	String_t* ___urlIMGPublicacoes_6;
	// System.String ServerControl::urlIMGTabloides
	String_t* ___urlIMGTabloides_7;
	// System.String ServerControl::urlIMGProfile
	String_t* ___urlIMGProfile_8;
	// System.String ServerControl::urlAcougue
	String_t* ___urlAcougue_9;
	// System.String ServerControl::urlIMGAcougue
	String_t* ___urlIMGAcougue_10;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(ServerControl_t2725829754_StaticFields, ___instance_2)); }
	inline ServerControl_t2725829754 * get_instance_2() const { return ___instance_2; }
	inline ServerControl_t2725829754 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(ServerControl_t2725829754 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}

	inline static int32_t get_offset_of_url_3() { return static_cast<int32_t>(offsetof(ServerControl_t2725829754_StaticFields, ___url_3)); }
	inline String_t* get_url_3() const { return ___url_3; }
	inline String_t** get_address_of_url_3() { return &___url_3; }
	inline void set_url_3(String_t* value)
	{
		___url_3 = value;
		Il2CppCodeGenWriteBarrier(&___url_3, value);
	}

	inline static int32_t get_offset_of_urlCRM_4() { return static_cast<int32_t>(offsetof(ServerControl_t2725829754_StaticFields, ___urlCRM_4)); }
	inline String_t* get_urlCRM_4() const { return ___urlCRM_4; }
	inline String_t** get_address_of_urlCRM_4() { return &___urlCRM_4; }
	inline void set_urlCRM_4(String_t* value)
	{
		___urlCRM_4 = value;
		Il2CppCodeGenWriteBarrier(&___urlCRM_4, value);
	}

	inline static int32_t get_offset_of_urlIMGOfertas_5() { return static_cast<int32_t>(offsetof(ServerControl_t2725829754_StaticFields, ___urlIMGOfertas_5)); }
	inline String_t* get_urlIMGOfertas_5() const { return ___urlIMGOfertas_5; }
	inline String_t** get_address_of_urlIMGOfertas_5() { return &___urlIMGOfertas_5; }
	inline void set_urlIMGOfertas_5(String_t* value)
	{
		___urlIMGOfertas_5 = value;
		Il2CppCodeGenWriteBarrier(&___urlIMGOfertas_5, value);
	}

	inline static int32_t get_offset_of_urlIMGPublicacoes_6() { return static_cast<int32_t>(offsetof(ServerControl_t2725829754_StaticFields, ___urlIMGPublicacoes_6)); }
	inline String_t* get_urlIMGPublicacoes_6() const { return ___urlIMGPublicacoes_6; }
	inline String_t** get_address_of_urlIMGPublicacoes_6() { return &___urlIMGPublicacoes_6; }
	inline void set_urlIMGPublicacoes_6(String_t* value)
	{
		___urlIMGPublicacoes_6 = value;
		Il2CppCodeGenWriteBarrier(&___urlIMGPublicacoes_6, value);
	}

	inline static int32_t get_offset_of_urlIMGTabloides_7() { return static_cast<int32_t>(offsetof(ServerControl_t2725829754_StaticFields, ___urlIMGTabloides_7)); }
	inline String_t* get_urlIMGTabloides_7() const { return ___urlIMGTabloides_7; }
	inline String_t** get_address_of_urlIMGTabloides_7() { return &___urlIMGTabloides_7; }
	inline void set_urlIMGTabloides_7(String_t* value)
	{
		___urlIMGTabloides_7 = value;
		Il2CppCodeGenWriteBarrier(&___urlIMGTabloides_7, value);
	}

	inline static int32_t get_offset_of_urlIMGProfile_8() { return static_cast<int32_t>(offsetof(ServerControl_t2725829754_StaticFields, ___urlIMGProfile_8)); }
	inline String_t* get_urlIMGProfile_8() const { return ___urlIMGProfile_8; }
	inline String_t** get_address_of_urlIMGProfile_8() { return &___urlIMGProfile_8; }
	inline void set_urlIMGProfile_8(String_t* value)
	{
		___urlIMGProfile_8 = value;
		Il2CppCodeGenWriteBarrier(&___urlIMGProfile_8, value);
	}

	inline static int32_t get_offset_of_urlAcougue_9() { return static_cast<int32_t>(offsetof(ServerControl_t2725829754_StaticFields, ___urlAcougue_9)); }
	inline String_t* get_urlAcougue_9() const { return ___urlAcougue_9; }
	inline String_t** get_address_of_urlAcougue_9() { return &___urlAcougue_9; }
	inline void set_urlAcougue_9(String_t* value)
	{
		___urlAcougue_9 = value;
		Il2CppCodeGenWriteBarrier(&___urlAcougue_9, value);
	}

	inline static int32_t get_offset_of_urlIMGAcougue_10() { return static_cast<int32_t>(offsetof(ServerControl_t2725829754_StaticFields, ___urlIMGAcougue_10)); }
	inline String_t* get_urlIMGAcougue_10() const { return ___urlIMGAcougue_10; }
	inline String_t** get_address_of_urlIMGAcougue_10() { return &___urlIMGAcougue_10; }
	inline void set_urlIMGAcougue_10(String_t* value)
	{
		___urlIMGAcougue_10 = value;
		Il2CppCodeGenWriteBarrier(&___urlIMGAcougue_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
