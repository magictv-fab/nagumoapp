﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ButtonAlertExample
struct ButtonAlertExample_t917118176;

#include "codegen/il2cpp-codegen.h"

// System.Void ButtonAlertExample::.ctor()
extern "C"  void ButtonAlertExample__ctor_m1230921659 (ButtonAlertExample_t917118176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonAlertExample::Click()
extern "C"  void ButtonAlertExample_Click_m2935769185 (ButtonAlertExample_t917118176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonAlertExample::OkClick()
extern "C"  void ButtonAlertExample_OkClick_m2710449381 (ButtonAlertExample_t917118176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
