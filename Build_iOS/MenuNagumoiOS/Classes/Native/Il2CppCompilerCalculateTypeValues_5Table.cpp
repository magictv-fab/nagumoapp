﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_Emit_LocalBuilder194563060.h"
#include "mscorlib_System_Reflection_Emit_MethodBuilder302405488.h"
#include "mscorlib_System_Reflection_Emit_MethodToken23137230.h"
#include "mscorlib_System_Reflection_Emit_ModuleBuilder595214213.h"
#include "mscorlib_System_Reflection_Emit_ModuleBuilderToken3788085887.h"
#include "mscorlib_System_Reflection_Emit_OpCodeNames3688280752.h"
#include "mscorlib_System_Reflection_Emit_OpCode3389331186.h"
#include "mscorlib_System_Reflection_Emit_OpCodes3677614459.h"
#include "mscorlib_System_Reflection_Emit_OperandType912928217.h"
#include "mscorlib_System_Reflection_Emit_PackingSize581671168.h"
#include "mscorlib_System_Reflection_Emit_ParameterBuilder3159962230.h"
#include "mscorlib_System_Reflection_Emit_PEFileKinds1559682852.h"
#include "mscorlib_System_Reflection_Emit_PropertyBuilder2012258748.h"
#include "mscorlib_System_Reflection_Emit_StackBehaviour1381517209.h"
#include "mscorlib_System_Reflection_Emit_TypeBuilder1918497079.h"
#include "mscorlib_System_Reflection_Emit_UnmanagedMarshal224089244.h"
#include "mscorlib_System_Resources_NeutralResourcesLanguage1239221824.h"
#include "mscorlib_System_Resources_ResourceManager1323731545.h"
#include "mscorlib_System_Resources_PredefinedResourceType2553580280.h"
#include "mscorlib_System_Resources_ResourceReader2295508955.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceI4013605874.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceC2113902833.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceEn177058824.h"
#include "mscorlib_System_Resources_ResourceSet3047644174.h"
#include "mscorlib_System_Resources_RuntimeResourceSet565351398.h"
#include "mscorlib_System_Resources_SatelliteContractVersion3298646731.h"
#include "mscorlib_System_Runtime_CompilerServices_Compilati1666434957.h"
#include "mscorlib_System_Runtime_CompilerServices_Compilati3111187931.h"
#include "mscorlib_System_Runtime_CompilerServices_DefaultDe1215592498.h"
#include "mscorlib_System_Runtime_CompilerServices_IsVolatil1428470152.h"
#include "mscorlib_System_Runtime_CompilerServices_LoadHint2899402799.h"
#include "mscorlib_System_Runtime_CompilerServices_StringFre2193595201.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_Criti3073722122.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_Cer1552592646.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_Consi3698611150.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_Reliab323877396.h"
#include "mscorlib_System_Runtime_Hosting_ActivationArgument2163479198.h"
#include "mscorlib_System_Runtime_InteropServices_CallingCon1635440495.h"
#include "mscorlib_System_Runtime_InteropServices_CharSet4282055494.h"
#include "mscorlib_System_Runtime_InteropServices_ClassInter3547252501.h"
#include "mscorlib_System_Runtime_InteropServices_ClassInterf777794597.h"
#include "mscorlib_System_Runtime_InteropServices_ComCompatibl49511955.h"
#include "mscorlib_System_Runtime_InteropServices_ComDefaultI513079213.h"
#include "mscorlib_System_Runtime_InteropServices_COMExcepti2569906296.h"
#include "mscorlib_System_Runtime_InteropServices_ComInterfa1050936508.h"
#include "mscorlib_System_Runtime_InteropServices_DispIdAttr3541616345.h"
#include "mscorlib_System_Runtime_InteropServices_ErrorWrapp3505718805.h"
#include "mscorlib_System_Runtime_InteropServices_ExternalEx3159215326.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandle1812538030.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandleTy1621876744.h"
#include "mscorlib_System_Runtime_InteropServices_InterfaceTy986726003.h"
#include "mscorlib_System_Runtime_InteropServices_Marshal87536056.h"
#include "mscorlib_System_Runtime_InteropServices_MarshalDir1970812184.h"
#include "mscorlib_System_Runtime_InteropServices_PreserveSi1347954145.h"
#include "mscorlib_System_Runtime_InteropServices_RuntimeEnvi725914053.h"
#include "mscorlib_System_Runtime_InteropServices_SafeHandle3340575423.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibImp4035097262.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibVer2572937673.h"
#include "mscorlib_System_Runtime_InteropServices_UnmanagedF1462594039.h"
#include "mscorlib_System_Runtime_InteropServices_UnmanagedTyp96484186.h"
#include "mscorlib_System_Runtime_Remoting_ActivatedClientTy2438336762.h"
#include "mscorlib_System_Runtime_Remoting_ActivatedServiceTy667738406.h"
#include "mscorlib_System_Runtime_Remoting_EnvoyInfo4176714743.h"
#include "mscorlib_System_Runtime_Remoting_Identity3943038460.h"
#include "mscorlib_System_Runtime_Remoting_ClientIdentity2516200039.h"
#include "mscorlib_System_Runtime_Remoting_InternalRemotingS2780704540.h"
#include "mscorlib_System_Runtime_Remoting_ObjRef2484604634.h"
#include "mscorlib_System_Runtime_Remoting_RemotingConfigurat770821701.h"
#include "mscorlib_System_Runtime_Remoting_ConfigHandler913489850.h"
#include "mscorlib_System_Runtime_Remoting_ChannelData140186591.h"
#include "mscorlib_System_Runtime_Remoting_ProviderData816409465.h"
#include "mscorlib_System_Runtime_Remoting_FormatterData3328950694.h"
#include "mscorlib_System_Runtime_Remoting_RemotingException3362817310.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize500 = { sizeof (LocalBuilder_t194563060), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable500[1] = 
{
	LocalBuilder_t194563060::get_offset_of_ilgen_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize501 = { sizeof (MethodBuilder_t302405488), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable501[18] = 
{
	MethodBuilder_t302405488::get_offset_of_rtype_0(),
	MethodBuilder_t302405488::get_offset_of_parameters_1(),
	MethodBuilder_t302405488::get_offset_of_attrs_2(),
	MethodBuilder_t302405488::get_offset_of_iattrs_3(),
	MethodBuilder_t302405488::get_offset_of_name_4(),
	MethodBuilder_t302405488::get_offset_of_table_idx_5(),
	MethodBuilder_t302405488::get_offset_of_code_6(),
	MethodBuilder_t302405488::get_offset_of_ilgen_7(),
	MethodBuilder_t302405488::get_offset_of_type_8(),
	MethodBuilder_t302405488::get_offset_of_pinfo_9(),
	MethodBuilder_t302405488::get_offset_of_override_method_10(),
	MethodBuilder_t302405488::get_offset_of_call_conv_11(),
	MethodBuilder_t302405488::get_offset_of_init_locals_12(),
	MethodBuilder_t302405488::get_offset_of_generic_params_13(),
	MethodBuilder_t302405488::get_offset_of_returnModReq_14(),
	MethodBuilder_t302405488::get_offset_of_returnModOpt_15(),
	MethodBuilder_t302405488::get_offset_of_paramModReq_16(),
	MethodBuilder_t302405488::get_offset_of_paramModOpt_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize502 = { sizeof (MethodToken_t23137230)+ sizeof (Il2CppObject), sizeof(MethodToken_t23137230_marshaled_pinvoke), sizeof(MethodToken_t23137230_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable502[2] = 
{
	MethodToken_t23137230::get_offset_of_tokValue_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MethodToken_t23137230_StaticFields::get_offset_of_Empty_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize503 = { sizeof (ModuleBuilder_t595214213), -1, sizeof(ModuleBuilder_t595214213_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable503[13] = 
{
	ModuleBuilder_t595214213::get_offset_of_num_types_10(),
	ModuleBuilder_t595214213::get_offset_of_types_11(),
	ModuleBuilder_t595214213::get_offset_of_guid_12(),
	ModuleBuilder_t595214213::get_offset_of_table_idx_13(),
	ModuleBuilder_t595214213::get_offset_of_assemblyb_14(),
	ModuleBuilder_t595214213::get_offset_of_global_type_15(),
	ModuleBuilder_t595214213::get_offset_of_name_cache_16(),
	ModuleBuilder_t595214213::get_offset_of_us_string_cache_17(),
	ModuleBuilder_t595214213::get_offset_of_table_indexes_18(),
	ModuleBuilder_t595214213::get_offset_of_transient_19(),
	ModuleBuilder_t595214213::get_offset_of_token_gen_20(),
	ModuleBuilder_t595214213::get_offset_of_symbolWriter_21(),
	ModuleBuilder_t595214213_StaticFields::get_offset_of_type_modifiers_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize504 = { sizeof (ModuleBuilderTokenGenerator_t3788085887), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable504[1] = 
{
	ModuleBuilderTokenGenerator_t3788085887::get_offset_of_mb_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize505 = { sizeof (OpCodeNames_t3688280752), -1, sizeof(OpCodeNames_t3688280752_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable505[1] = 
{
	OpCodeNames_t3688280752_StaticFields::get_offset_of_names_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize506 = { sizeof (OpCode_t3389331186)+ sizeof (Il2CppObject), sizeof(OpCode_t3389331186_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable506[8] = 
{
	OpCode_t3389331186::get_offset_of_op1_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OpCode_t3389331186::get_offset_of_op2_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OpCode_t3389331186::get_offset_of_push_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OpCode_t3389331186::get_offset_of_pop_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OpCode_t3389331186::get_offset_of_size_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OpCode_t3389331186::get_offset_of_type_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OpCode_t3389331186::get_offset_of_args_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OpCode_t3389331186::get_offset_of_flow_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize507 = { sizeof (OpCodes_t3677614459), -1, sizeof(OpCodes_t3677614459_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable507[226] = 
{
	OpCodes_t3677614459_StaticFields::get_offset_of_Nop_0(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Break_1(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldarg_0_2(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldarg_1_3(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldarg_2_4(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldarg_3_5(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldloc_0_6(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldloc_1_7(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldloc_2_8(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldloc_3_9(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Stloc_0_10(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Stloc_1_11(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Stloc_2_12(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Stloc_3_13(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldarg_S_14(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldarga_S_15(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Starg_S_16(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldloc_S_17(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldloca_S_18(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Stloc_S_19(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldnull_20(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldc_I4_M1_21(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldc_I4_0_22(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldc_I4_1_23(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldc_I4_2_24(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldc_I4_3_25(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldc_I4_4_26(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldc_I4_5_27(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldc_I4_6_28(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldc_I4_7_29(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldc_I4_8_30(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldc_I4_S_31(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldc_I4_32(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldc_I8_33(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldc_R4_34(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldc_R8_35(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Dup_36(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Pop_37(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Jmp_38(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Call_39(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Calli_40(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ret_41(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Br_S_42(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Brfalse_S_43(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Brtrue_S_44(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Beq_S_45(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Bge_S_46(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Bgt_S_47(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ble_S_48(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Blt_S_49(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Bne_Un_S_50(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Bge_Un_S_51(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Bgt_Un_S_52(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ble_Un_S_53(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Blt_Un_S_54(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Br_55(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Brfalse_56(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Brtrue_57(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Beq_58(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Bge_59(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Bgt_60(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ble_61(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Blt_62(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Bne_Un_63(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Bge_Un_64(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Bgt_Un_65(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ble_Un_66(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Blt_Un_67(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Switch_68(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldind_I1_69(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldind_U1_70(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldind_I2_71(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldind_U2_72(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldind_I4_73(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldind_U4_74(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldind_I8_75(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldind_I_76(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldind_R4_77(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldind_R8_78(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldind_Ref_79(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Stind_Ref_80(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Stind_I1_81(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Stind_I2_82(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Stind_I4_83(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Stind_I8_84(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Stind_R4_85(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Stind_R8_86(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Add_87(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Sub_88(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Mul_89(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Div_90(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Div_Un_91(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Rem_92(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Rem_Un_93(),
	OpCodes_t3677614459_StaticFields::get_offset_of_And_94(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Or_95(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Xor_96(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Shl_97(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Shr_98(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Shr_Un_99(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Neg_100(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Not_101(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Conv_I1_102(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Conv_I2_103(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Conv_I4_104(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Conv_I8_105(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Conv_R4_106(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Conv_R8_107(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Conv_U4_108(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Conv_U8_109(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Callvirt_110(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Cpobj_111(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldobj_112(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldstr_113(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Newobj_114(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Castclass_115(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Isinst_116(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Conv_R_Un_117(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Unbox_118(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Throw_119(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldfld_120(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldflda_121(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Stfld_122(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldsfld_123(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldsflda_124(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Stsfld_125(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Stobj_126(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Conv_Ovf_I1_Un_127(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Conv_Ovf_I2_Un_128(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Conv_Ovf_I4_Un_129(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Conv_Ovf_I8_Un_130(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Conv_Ovf_U1_Un_131(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Conv_Ovf_U2_Un_132(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Conv_Ovf_U4_Un_133(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Conv_Ovf_U8_Un_134(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Conv_Ovf_I_Un_135(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Conv_Ovf_U_Un_136(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Box_137(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Newarr_138(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldlen_139(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldelema_140(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldelem_I1_141(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldelem_U1_142(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldelem_I2_143(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldelem_U2_144(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldelem_I4_145(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldelem_U4_146(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldelem_I8_147(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldelem_I_148(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldelem_R4_149(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldelem_R8_150(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldelem_Ref_151(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Stelem_I_152(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Stelem_I1_153(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Stelem_I2_154(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Stelem_I4_155(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Stelem_I8_156(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Stelem_R4_157(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Stelem_R8_158(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Stelem_Ref_159(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldelem_160(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Stelem_161(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Unbox_Any_162(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Conv_Ovf_I1_163(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Conv_Ovf_U1_164(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Conv_Ovf_I2_165(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Conv_Ovf_U2_166(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Conv_Ovf_I4_167(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Conv_Ovf_U4_168(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Conv_Ovf_I8_169(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Conv_Ovf_U8_170(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Refanyval_171(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ckfinite_172(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Mkrefany_173(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldtoken_174(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Conv_U2_175(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Conv_U1_176(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Conv_I_177(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Conv_Ovf_I_178(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Conv_Ovf_U_179(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Add_Ovf_180(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Add_Ovf_Un_181(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Mul_Ovf_182(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Mul_Ovf_Un_183(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Sub_Ovf_184(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Sub_Ovf_Un_185(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Endfinally_186(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Leave_187(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Leave_S_188(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Stind_I_189(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Conv_U_190(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Prefix7_191(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Prefix6_192(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Prefix5_193(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Prefix4_194(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Prefix3_195(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Prefix2_196(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Prefix1_197(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Prefixref_198(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Arglist_199(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ceq_200(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Cgt_201(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Cgt_Un_202(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Clt_203(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Clt_Un_204(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldftn_205(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldvirtftn_206(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldarg_207(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldarga_208(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Starg_209(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldloc_210(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Ldloca_211(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Stloc_212(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Localloc_213(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Endfilter_214(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Unaligned_215(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Volatile_216(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Tailcall_217(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Initobj_218(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Constrained_219(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Cpblk_220(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Initblk_221(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Rethrow_222(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Sizeof_223(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Refanytype_224(),
	OpCodes_t3677614459_StaticFields::get_offset_of_Readonly_225(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize508 = { sizeof (OperandType_t912928217)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable508[19] = 
{
	OperandType_t912928217::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize509 = { sizeof (PackingSize_t581671168)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable509[10] = 
{
	PackingSize_t581671168::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize510 = { sizeof (ParameterBuilder_t3159962230), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable510[3] = 
{
	ParameterBuilder_t3159962230::get_offset_of_name_0(),
	ParameterBuilder_t3159962230::get_offset_of_attrs_1(),
	ParameterBuilder_t3159962230::get_offset_of_position_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize511 = { sizeof (PEFileKinds_t1559682852)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable511[4] = 
{
	PEFileKinds_t1559682852::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize512 = { sizeof (PropertyBuilder_t2012258748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable512[6] = 
{
	PropertyBuilder_t2012258748::get_offset_of_attrs_0(),
	PropertyBuilder_t2012258748::get_offset_of_name_1(),
	PropertyBuilder_t2012258748::get_offset_of_type_2(),
	PropertyBuilder_t2012258748::get_offset_of_set_method_3(),
	PropertyBuilder_t2012258748::get_offset_of_get_method_4(),
	PropertyBuilder_t2012258748::get_offset_of_typeb_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize513 = { sizeof (StackBehaviour_t1381517209)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable513[30] = 
{
	StackBehaviour_t1381517209::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize514 = { sizeof (TypeBuilder_t1918497079), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable514[21] = 
{
	TypeBuilder_t1918497079::get_offset_of_tname_8(),
	TypeBuilder_t1918497079::get_offset_of_nspace_9(),
	TypeBuilder_t1918497079::get_offset_of_parent_10(),
	TypeBuilder_t1918497079::get_offset_of_nesting_type_11(),
	TypeBuilder_t1918497079::get_offset_of_interfaces_12(),
	TypeBuilder_t1918497079::get_offset_of_num_methods_13(),
	TypeBuilder_t1918497079::get_offset_of_methods_14(),
	TypeBuilder_t1918497079::get_offset_of_ctors_15(),
	TypeBuilder_t1918497079::get_offset_of_properties_16(),
	TypeBuilder_t1918497079::get_offset_of_fields_17(),
	TypeBuilder_t1918497079::get_offset_of_subtypes_18(),
	TypeBuilder_t1918497079::get_offset_of_attrs_19(),
	TypeBuilder_t1918497079::get_offset_of_table_idx_20(),
	TypeBuilder_t1918497079::get_offset_of_pmodule_21(),
	TypeBuilder_t1918497079::get_offset_of_class_size_22(),
	TypeBuilder_t1918497079::get_offset_of_packing_size_23(),
	TypeBuilder_t1918497079::get_offset_of_generic_params_24(),
	TypeBuilder_t1918497079::get_offset_of_created_25(),
	TypeBuilder_t1918497079::get_offset_of_fullname_26(),
	TypeBuilder_t1918497079::get_offset_of_createTypeCalled_27(),
	TypeBuilder_t1918497079::get_offset_of_underlying_type_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize515 = { sizeof (UnmanagedMarshal_t224089244), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable515[9] = 
{
	UnmanagedMarshal_t224089244::get_offset_of_count_0(),
	UnmanagedMarshal_t224089244::get_offset_of_t_1(),
	UnmanagedMarshal_t224089244::get_offset_of_tbase_2(),
	UnmanagedMarshal_t224089244::get_offset_of_guid_3(),
	UnmanagedMarshal_t224089244::get_offset_of_mcookie_4(),
	UnmanagedMarshal_t224089244::get_offset_of_marshaltype_5(),
	UnmanagedMarshal_t224089244::get_offset_of_marshaltyperef_6(),
	UnmanagedMarshal_t224089244::get_offset_of_param_num_7(),
	UnmanagedMarshal_t224089244::get_offset_of_has_size_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize516 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize517 = { sizeof (NeutralResourcesLanguageAttribute_t1239221824), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable517[1] = 
{
	NeutralResourcesLanguageAttribute_t1239221824::get_offset_of_culture_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize518 = { sizeof (ResourceManager_t1323731545), -1, sizeof(ResourceManager_t1323731545_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable518[5] = 
{
	ResourceManager_t1323731545_StaticFields::get_offset_of_ResourceCache_0(),
	ResourceManager_t1323731545_StaticFields::get_offset_of_NonExistent_1(),
	ResourceManager_t1323731545_StaticFields::get_offset_of_HeaderVersionNumber_2(),
	ResourceManager_t1323731545_StaticFields::get_offset_of_MagicNumber_3(),
	ResourceManager_t1323731545::get_offset_of_resourceSetType_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize519 = { sizeof (PredefinedResourceType_t2553580280)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable519[21] = 
{
	PredefinedResourceType_t2553580280::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize520 = { sizeof (ResourceReader_t2295508955), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable520[13] = 
{
	ResourceReader_t2295508955::get_offset_of_reader_0(),
	ResourceReader_t2295508955::get_offset_of_readerLock_1(),
	ResourceReader_t2295508955::get_offset_of_formatter_2(),
	ResourceReader_t2295508955::get_offset_of_resourceCount_3(),
	ResourceReader_t2295508955::get_offset_of_typeCount_4(),
	ResourceReader_t2295508955::get_offset_of_typeNames_5(),
	ResourceReader_t2295508955::get_offset_of_hashes_6(),
	ResourceReader_t2295508955::get_offset_of_infos_7(),
	ResourceReader_t2295508955::get_offset_of_dataSectionOffset_8(),
	ResourceReader_t2295508955::get_offset_of_nameSectionOffset_9(),
	ResourceReader_t2295508955::get_offset_of_resource_ver_10(),
	ResourceReader_t2295508955::get_offset_of_cache_11(),
	ResourceReader_t2295508955::get_offset_of_cache_lock_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize521 = { sizeof (ResourceInfo_t4013605874)+ sizeof (Il2CppObject), sizeof(ResourceInfo_t4013605874_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable521[3] = 
{
	ResourceInfo_t4013605874::get_offset_of_ValuePosition_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ResourceInfo_t4013605874::get_offset_of_ResourceName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ResourceInfo_t4013605874::get_offset_of_TypeIndex_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize522 = { sizeof (ResourceCacheItem_t2113902833)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable522[2] = 
{
	ResourceCacheItem_t2113902833::get_offset_of_ResourceName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ResourceCacheItem_t2113902833::get_offset_of_ResourceValue_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize523 = { sizeof (ResourceEnumerator_t177058824), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable523[3] = 
{
	ResourceEnumerator_t177058824::get_offset_of_reader_0(),
	ResourceEnumerator_t177058824::get_offset_of_index_1(),
	ResourceEnumerator_t177058824::get_offset_of_finished_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize524 = { sizeof (ResourceSet_t3047644174), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable524[4] = 
{
	ResourceSet_t3047644174::get_offset_of_Reader_0(),
	ResourceSet_t3047644174::get_offset_of_Table_1(),
	ResourceSet_t3047644174::get_offset_of_resources_read_2(),
	ResourceSet_t3047644174::get_offset_of_disposed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize525 = { sizeof (RuntimeResourceSet_t565351398), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize526 = { sizeof (SatelliteContractVersionAttribute_t3298646731), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable526[1] = 
{
	SatelliteContractVersionAttribute_t3298646731::get_offset_of_ver_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize527 = { sizeof (CompilationRelaxations_t1666434957)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable527[2] = 
{
	CompilationRelaxations_t1666434957::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize528 = { sizeof (CompilationRelaxationsAttribute_t3111187931), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable528[1] = 
{
	CompilationRelaxationsAttribute_t3111187931::get_offset_of_relax_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize529 = { sizeof (DefaultDependencyAttribute_t1215592498), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable529[1] = 
{
	DefaultDependencyAttribute_t1215592498::get_offset_of_hint_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize530 = { sizeof (IsVolatile_t1428470152), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize531 = { sizeof (LoadHint_t2899402799)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable531[4] = 
{
	LoadHint_t2899402799::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize532 = { sizeof (StringFreezingAttribute_t2193595201), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize533 = { sizeof (CriticalFinalizerObject_t3073722122), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize534 = { sizeof (Cer_t1552592646)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable534[4] = 
{
	Cer_t1552592646::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize535 = { sizeof (Consistency_t3698611150)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable535[5] = 
{
	Consistency_t3698611150::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize536 = { sizeof (ReliabilityContractAttribute_t323877396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable536[2] = 
{
	ReliabilityContractAttribute_t323877396::get_offset_of_consistency_0(),
	ReliabilityContractAttribute_t323877396::get_offset_of_cer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize537 = { sizeof (ActivationArguments_t2163479198), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize538 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize539 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize540 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize541 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize542 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize543 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize544 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize545 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize546 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize547 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize548 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize549 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize550 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize551 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize552 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize553 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize554 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize555 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize556 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize557 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize558 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize559 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize560 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize561 = { sizeof (CallingConvention_t1635440495)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable561[6] = 
{
	CallingConvention_t1635440495::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize562 = { sizeof (CharSet_t4282055494)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable562[5] = 
{
	CharSet_t4282055494::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize563 = { sizeof (ClassInterfaceAttribute_t3547252501), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable563[1] = 
{
	ClassInterfaceAttribute_t3547252501::get_offset_of_ciType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize564 = { sizeof (ClassInterfaceType_t777794597)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable564[4] = 
{
	ClassInterfaceType_t777794597::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize565 = { sizeof (ComCompatibleVersionAttribute_t49511955), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable565[4] = 
{
	ComCompatibleVersionAttribute_t49511955::get_offset_of_major_0(),
	ComCompatibleVersionAttribute_t49511955::get_offset_of_minor_1(),
	ComCompatibleVersionAttribute_t49511955::get_offset_of_build_2(),
	ComCompatibleVersionAttribute_t49511955::get_offset_of_revision_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize566 = { sizeof (ComDefaultInterfaceAttribute_t513079213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable566[1] = 
{
	ComDefaultInterfaceAttribute_t513079213::get_offset_of__type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize567 = { sizeof (COMException_t2569906296), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize568 = { sizeof (ComInterfaceType_t1050936508)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable568[4] = 
{
	ComInterfaceType_t1050936508::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize569 = { sizeof (DispIdAttribute_t3541616345), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable569[1] = 
{
	DispIdAttribute_t3541616345::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize570 = { sizeof (ErrorWrapper_t3505718805), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable570[1] = 
{
	ErrorWrapper_t3505718805::get_offset_of_errorCode_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize571 = { sizeof (ExternalException_t3159215326), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize572 = { sizeof (GCHandle_t1812538030)+ sizeof (Il2CppObject), sizeof(GCHandle_t1812538030_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable572[1] = 
{
	GCHandle_t1812538030::get_offset_of_handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize573 = { sizeof (GCHandleType_t1621876744)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable573[5] = 
{
	GCHandleType_t1621876744::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize574 = { sizeof (InterfaceTypeAttribute_t986726003), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable574[1] = 
{
	InterfaceTypeAttribute_t986726003::get_offset_of_intType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize575 = { sizeof (Marshal_t87536056), -1, sizeof(Marshal_t87536056_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable575[2] = 
{
	Marshal_t87536056_StaticFields::get_offset_of_SystemMaxDBCSCharSize_0(),
	Marshal_t87536056_StaticFields::get_offset_of_SystemDefaultCharSize_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize576 = { sizeof (MarshalDirectiveException_t1970812184), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable576[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize577 = { sizeof (PreserveSigAttribute_t1347954145), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize578 = { sizeof (RuntimeEnvironment_t725914053), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize579 = { sizeof (SafeHandle_t3340575423), sizeof(void*), 0, 0 };
extern const int32_t g_FieldOffsetTable579[4] = 
{
	SafeHandle_t3340575423::get_offset_of_handle_0(),
	SafeHandle_t3340575423::get_offset_of_invalid_handle_value_1(),
	SafeHandle_t3340575423::get_offset_of_refcount_2(),
	SafeHandle_t3340575423::get_offset_of_owns_handle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize580 = { sizeof (TypeLibImportClassAttribute_t4035097262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable580[1] = 
{
	TypeLibImportClassAttribute_t4035097262::get_offset_of__importClass_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize581 = { sizeof (TypeLibVersionAttribute_t2572937673), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable581[2] = 
{
	TypeLibVersionAttribute_t2572937673::get_offset_of_major_0(),
	TypeLibVersionAttribute_t2572937673::get_offset_of_minor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize582 = { sizeof (UnmanagedFunctionPointerAttribute_t1462594039), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable582[1] = 
{
	UnmanagedFunctionPointerAttribute_t1462594039::get_offset_of_call_conv_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize583 = { sizeof (UnmanagedType_t96484186)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable583[36] = 
{
	UnmanagedType_t96484186::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize584 = { sizeof (ActivatedClientTypeEntry_t2438336762), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable584[2] = 
{
	ActivatedClientTypeEntry_t2438336762::get_offset_of_applicationUrl_2(),
	ActivatedClientTypeEntry_t2438336762::get_offset_of_obj_type_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize585 = { sizeof (ActivatedServiceTypeEntry_t667738406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable585[1] = 
{
	ActivatedServiceTypeEntry_t667738406::get_offset_of_obj_type_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize586 = { sizeof (EnvoyInfo_t4176714743), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable586[1] = 
{
	EnvoyInfo_t4176714743::get_offset_of_envoySinks_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize587 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize588 = { sizeof (Identity_t3943038460), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable588[7] = 
{
	Identity_t3943038460::get_offset_of__objectUri_0(),
	Identity_t3943038460::get_offset_of__channelSink_1(),
	Identity_t3943038460::get_offset_of__envoySink_2(),
	Identity_t3943038460::get_offset_of__clientDynamicProperties_3(),
	Identity_t3943038460::get_offset_of__serverDynamicProperties_4(),
	Identity_t3943038460::get_offset_of__objRef_5(),
	Identity_t3943038460::get_offset_of__disposed_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize589 = { sizeof (ClientIdentity_t2516200039), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable589[1] = 
{
	ClientIdentity_t2516200039::get_offset_of__proxyReference_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize590 = { sizeof (InternalRemotingServices_t2780704540), -1, sizeof(InternalRemotingServices_t2780704540_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable590[1] = 
{
	InternalRemotingServices_t2780704540_StaticFields::get_offset_of__soapAttributes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize591 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize592 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize593 = { sizeof (ObjRef_t2484604634), -1, sizeof(ObjRef_t2484604634_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable593[9] = 
{
	ObjRef_t2484604634::get_offset_of_channel_info_0(),
	ObjRef_t2484604634::get_offset_of_uri_1(),
	ObjRef_t2484604634::get_offset_of_typeInfo_2(),
	ObjRef_t2484604634::get_offset_of_envoyInfo_3(),
	ObjRef_t2484604634::get_offset_of_flags_4(),
	ObjRef_t2484604634::get_offset_of__serverType_5(),
	ObjRef_t2484604634_StaticFields::get_offset_of_MarshalledObjectRef_6(),
	ObjRef_t2484604634_StaticFields::get_offset_of_WellKnowObjectRef_7(),
	ObjRef_t2484604634_StaticFields::get_offset_of_U3CU3Ef__switchU24map21_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize594 = { sizeof (RemotingConfiguration_t770821701), -1, sizeof(RemotingConfiguration_t770821701_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable594[13] = 
{
	RemotingConfiguration_t770821701_StaticFields::get_offset_of_applicationID_0(),
	RemotingConfiguration_t770821701_StaticFields::get_offset_of_applicationName_1(),
	RemotingConfiguration_t770821701_StaticFields::get_offset_of_processGuid_2(),
	RemotingConfiguration_t770821701_StaticFields::get_offset_of_defaultConfigRead_3(),
	RemotingConfiguration_t770821701_StaticFields::get_offset_of_defaultDelayedConfigRead_4(),
	RemotingConfiguration_t770821701_StaticFields::get_offset_of__errorMode_5(),
	RemotingConfiguration_t770821701_StaticFields::get_offset_of_wellKnownClientEntries_6(),
	RemotingConfiguration_t770821701_StaticFields::get_offset_of_activatedClientEntries_7(),
	RemotingConfiguration_t770821701_StaticFields::get_offset_of_wellKnownServiceEntries_8(),
	RemotingConfiguration_t770821701_StaticFields::get_offset_of_activatedServiceEntries_9(),
	RemotingConfiguration_t770821701_StaticFields::get_offset_of_channelTemplates_10(),
	RemotingConfiguration_t770821701_StaticFields::get_offset_of_clientProviderTemplates_11(),
	RemotingConfiguration_t770821701_StaticFields::get_offset_of_serverProviderTemplates_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize595 = { sizeof (ConfigHandler_t913489850), -1, sizeof(ConfigHandler_t913489850_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable595[10] = 
{
	ConfigHandler_t913489850::get_offset_of_typeEntries_0(),
	ConfigHandler_t913489850::get_offset_of_channelInstances_1(),
	ConfigHandler_t913489850::get_offset_of_currentChannel_2(),
	ConfigHandler_t913489850::get_offset_of_currentProviderData_3(),
	ConfigHandler_t913489850::get_offset_of_currentClientUrl_4(),
	ConfigHandler_t913489850::get_offset_of_appName_5(),
	ConfigHandler_t913489850::get_offset_of_currentXmlPath_6(),
	ConfigHandler_t913489850::get_offset_of_onlyDelayedChannels_7(),
	ConfigHandler_t913489850_StaticFields::get_offset_of_U3CU3Ef__switchU24map22_8(),
	ConfigHandler_t913489850_StaticFields::get_offset_of_U3CU3Ef__switchU24map23_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize596 = { sizeof (ChannelData_t140186591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable596[7] = 
{
	ChannelData_t140186591::get_offset_of_Ref_0(),
	ChannelData_t140186591::get_offset_of_Type_1(),
	ChannelData_t140186591::get_offset_of_Id_2(),
	ChannelData_t140186591::get_offset_of_DelayLoadAsClientChannel_3(),
	ChannelData_t140186591::get_offset_of__serverProviders_4(),
	ChannelData_t140186591::get_offset_of__clientProviders_5(),
	ChannelData_t140186591::get_offset_of__customProperties_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize597 = { sizeof (ProviderData_t816409465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable597[5] = 
{
	ProviderData_t816409465::get_offset_of_Ref_0(),
	ProviderData_t816409465::get_offset_of_Type_1(),
	ProviderData_t816409465::get_offset_of_Id_2(),
	ProviderData_t816409465::get_offset_of_CustomProperties_3(),
	ProviderData_t816409465::get_offset_of_CustomData_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize598 = { sizeof (FormatterData_t3328950694), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize599 = { sizeof (RemotingException_t3362817310), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
