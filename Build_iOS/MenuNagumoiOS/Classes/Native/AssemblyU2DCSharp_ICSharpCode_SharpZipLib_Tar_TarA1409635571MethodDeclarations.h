﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Tar.TarArchive
struct TarArchive_t1409635571;
// ICSharpCode.SharpZipLib.Tar.TarInputStream
struct TarInputStream_t1386636699;
// ICSharpCode.SharpZipLib.Tar.TarOutputStream
struct TarOutputStream_t1551862984;
// ICSharpCode.SharpZipLib.Tar.ProgressMessageHandler
struct ProgressMessageHandler_t76602726;
// ICSharpCode.SharpZipLib.Tar.TarEntry
struct TarEntry_t762185187;
// System.String
struct String_t;
// System.IO.Stream
struct Stream_t1561764144;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Tar_TarI1386636699.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Tar_TarO1551862984.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Tar_Progre76602726.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Tar_TarEn762185187.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_IO_Stream1561764144.h"

// System.Void ICSharpCode.SharpZipLib.Tar.TarArchive::.ctor()
extern "C"  void TarArchive__ctor_m2977857924 (TarArchive_t1409635571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarArchive::.ctor(ICSharpCode.SharpZipLib.Tar.TarInputStream)
extern "C"  void TarArchive__ctor_m603593221 (TarArchive_t1409635571 * __this, TarInputStream_t1386636699 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarArchive::.ctor(ICSharpCode.SharpZipLib.Tar.TarOutputStream)
extern "C"  void TarArchive__ctor_m3776613538 (TarArchive_t1409635571 * __this, TarOutputStream_t1551862984 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarArchive::add_ProgressMessageEvent(ICSharpCode.SharpZipLib.Tar.ProgressMessageHandler)
extern "C"  void TarArchive_add_ProgressMessageEvent_m3645289812 (TarArchive_t1409635571 * __this, ProgressMessageHandler_t76602726 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarArchive::remove_ProgressMessageEvent(ICSharpCode.SharpZipLib.Tar.ProgressMessageHandler)
extern "C"  void TarArchive_remove_ProgressMessageEvent_m2174041779 (TarArchive_t1409635571 * __this, ProgressMessageHandler_t76602726 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarArchive::OnProgressMessageEvent(ICSharpCode.SharpZipLib.Tar.TarEntry,System.String)
extern "C"  void TarArchive_OnProgressMessageEvent_m82575892 (TarArchive_t1409635571 * __this, TarEntry_t762185187 * ___entry0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Tar.TarArchive ICSharpCode.SharpZipLib.Tar.TarArchive::CreateInputTarArchive(System.IO.Stream)
extern "C"  TarArchive_t1409635571 * TarArchive_CreateInputTarArchive_m851816958 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___inputStream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Tar.TarArchive ICSharpCode.SharpZipLib.Tar.TarArchive::CreateInputTarArchive(System.IO.Stream,System.Int32)
extern "C"  TarArchive_t1409635571 * TarArchive_CreateInputTarArchive_m2532513689 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___inputStream0, int32_t ___blockFactor1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Tar.TarArchive ICSharpCode.SharpZipLib.Tar.TarArchive::CreateOutputTarArchive(System.IO.Stream)
extern "C"  TarArchive_t1409635571 * TarArchive_CreateOutputTarArchive_m4082526167 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___outputStream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Tar.TarArchive ICSharpCode.SharpZipLib.Tar.TarArchive::CreateOutputTarArchive(System.IO.Stream,System.Int32)
extern "C"  TarArchive_t1409635571 * TarArchive_CreateOutputTarArchive_m2230348384 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___outputStream0, int32_t ___blockFactor1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarArchive::SetKeepOldFiles(System.Boolean)
extern "C"  void TarArchive_SetKeepOldFiles_m3049851408 (TarArchive_t1409635571 * __this, bool ___keepExistingFiles0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Tar.TarArchive::get_AsciiTranslate()
extern "C"  bool TarArchive_get_AsciiTranslate_m1174328634 (TarArchive_t1409635571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarArchive::set_AsciiTranslate(System.Boolean)
extern "C"  void TarArchive_set_AsciiTranslate_m534352113 (TarArchive_t1409635571 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarArchive::SetAsciiTranslation(System.Boolean)
extern "C"  void TarArchive_SetAsciiTranslation_m569496059 (TarArchive_t1409635571 * __this, bool ___translateAsciiFiles0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Tar.TarArchive::get_PathPrefix()
extern "C"  String_t* TarArchive_get_PathPrefix_m527469891 (TarArchive_t1409635571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarArchive::set_PathPrefix(System.String)
extern "C"  void TarArchive_set_PathPrefix_m3785158894 (TarArchive_t1409635571 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Tar.TarArchive::get_RootPath()
extern "C"  String_t* TarArchive_get_RootPath_m4125543699 (TarArchive_t1409635571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarArchive::set_RootPath(System.String)
extern "C"  void TarArchive_set_RootPath_m3179358942 (TarArchive_t1409635571 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarArchive::SetUserInfo(System.Int32,System.String,System.Int32,System.String)
extern "C"  void TarArchive_SetUserInfo_m1176556617 (TarArchive_t1409635571 * __this, int32_t ___userId0, String_t* ___userName1, int32_t ___groupId2, String_t* ___groupName3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Tar.TarArchive::get_ApplyUserInfoOverrides()
extern "C"  bool TarArchive_get_ApplyUserInfoOverrides_m1432637629 (TarArchive_t1409635571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarArchive::set_ApplyUserInfoOverrides(System.Boolean)
extern "C"  void TarArchive_set_ApplyUserInfoOverrides_m415460148 (TarArchive_t1409635571 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarArchive::get_UserId()
extern "C"  int32_t TarArchive_get_UserId_m1287855773 (TarArchive_t1409635571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Tar.TarArchive::get_UserName()
extern "C"  String_t* TarArchive_get_UserName_m2815408674 (TarArchive_t1409635571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarArchive::get_GroupId()
extern "C"  int32_t TarArchive_get_GroupId_m3761799269 (TarArchive_t1409635571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Tar.TarArchive::get_GroupName()
extern "C"  String_t* TarArchive_get_GroupName_m556754912 (TarArchive_t1409635571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarArchive::get_RecordSize()
extern "C"  int32_t TarArchive_get_RecordSize_m4058057289 (TarArchive_t1409635571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarArchive::set_IsStreamOwner(System.Boolean)
extern "C"  void TarArchive_set_IsStreamOwner_m4030161285 (TarArchive_t1409635571 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarArchive::CloseArchive()
extern "C"  void TarArchive_CloseArchive_m1107979626 (TarArchive_t1409635571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarArchive::ListContents()
extern "C"  void TarArchive_ListContents_m745543704 (TarArchive_t1409635571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarArchive::ExtractContents(System.String)
extern "C"  void TarArchive_ExtractContents_m760652997 (TarArchive_t1409635571 * __this, String_t* ___destinationDirectory0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarArchive::ExtractEntry(System.String,ICSharpCode.SharpZipLib.Tar.TarEntry)
extern "C"  void TarArchive_ExtractEntry_m2699861358 (TarArchive_t1409635571 * __this, String_t* ___destDir0, TarEntry_t762185187 * ___entry1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarArchive::WriteEntry(ICSharpCode.SharpZipLib.Tar.TarEntry,System.Boolean)
extern "C"  void TarArchive_WriteEntry_m3530111217 (TarArchive_t1409635571 * __this, TarEntry_t762185187 * ___sourceEntry0, bool ___recurse1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarArchive::WriteEntryCore(ICSharpCode.SharpZipLib.Tar.TarEntry,System.Boolean)
extern "C"  void TarArchive_WriteEntryCore_m269903762 (TarArchive_t1409635571 * __this, TarEntry_t762185187 * ___sourceEntry0, bool ___recurse1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarArchive::Dispose()
extern "C"  void TarArchive_Dispose_m1171528897 (TarArchive_t1409635571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarArchive::Dispose(System.Boolean)
extern "C"  void TarArchive_Dispose_m1128590456 (TarArchive_t1409635571 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarArchive::Close()
extern "C"  void TarArchive_Close_m393750170 (TarArchive_t1409635571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarArchive::Finalize()
extern "C"  void TarArchive_Finalize_m918421598 (TarArchive_t1409635571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarArchive::EnsureDirectoryExists(System.String)
extern "C"  void TarArchive_EnsureDirectoryExists_m1078757685 (Il2CppObject * __this /* static, unused */, String_t* ___directoryName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Tar.TarArchive::IsBinary(System.String)
extern "C"  bool TarArchive_IsBinary_m2269208803 (Il2CppObject * __this /* static, unused */, String_t* ___filename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
