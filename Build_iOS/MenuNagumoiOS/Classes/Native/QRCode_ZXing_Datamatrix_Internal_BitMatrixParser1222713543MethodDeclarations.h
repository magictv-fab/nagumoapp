﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Datamatrix.Internal.BitMatrixParser
struct BitMatrixParser_t1222713543;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// ZXing.Datamatrix.Internal.Version
struct Version_t2313761970;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitMatrix1058711404.h"

// System.Void ZXing.Datamatrix.Internal.BitMatrixParser::.ctor(ZXing.Common.BitMatrix)
extern "C"  void BitMatrixParser__ctor_m2575116385 (BitMatrixParser_t1222713543 * __this, BitMatrix_t1058711404 * ___bitMatrix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Datamatrix.Internal.Version ZXing.Datamatrix.Internal.BitMatrixParser::get_Version()
extern "C"  Version_t2313761970 * BitMatrixParser_get_Version_m652059254 (BitMatrixParser_t1222713543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Datamatrix.Internal.Version ZXing.Datamatrix.Internal.BitMatrixParser::readVersion(ZXing.Common.BitMatrix)
extern "C"  Version_t2313761970 * BitMatrixParser_readVersion_m3236426292 (Il2CppObject * __this /* static, unused */, BitMatrix_t1058711404 * ___bitMatrix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ZXing.Datamatrix.Internal.BitMatrixParser::readCodewords()
extern "C"  ByteU5BU5D_t4260760469* BitMatrixParser_readCodewords_m1347873860 (BitMatrixParser_t1222713543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Datamatrix.Internal.BitMatrixParser::readModule(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  bool BitMatrixParser_readModule_m528355178 (BitMatrixParser_t1222713543 * __this, int32_t ___row0, int32_t ___column1, int32_t ___numRows2, int32_t ___numColumns3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Internal.BitMatrixParser::readUtah(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t BitMatrixParser_readUtah_m694117546 (BitMatrixParser_t1222713543 * __this, int32_t ___row0, int32_t ___column1, int32_t ___numRows2, int32_t ___numColumns3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Internal.BitMatrixParser::readCorner1(System.Int32,System.Int32)
extern "C"  int32_t BitMatrixParser_readCorner1_m1231976990 (BitMatrixParser_t1222713543 * __this, int32_t ___numRows0, int32_t ___numColumns1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Internal.BitMatrixParser::readCorner2(System.Int32,System.Int32)
extern "C"  int32_t BitMatrixParser_readCorner2_m977240445 (BitMatrixParser_t1222713543 * __this, int32_t ___numRows0, int32_t ___numColumns1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Internal.BitMatrixParser::readCorner3(System.Int32,System.Int32)
extern "C"  int32_t BitMatrixParser_readCorner3_m722503900 (BitMatrixParser_t1222713543 * __this, int32_t ___numRows0, int32_t ___numColumns1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Internal.BitMatrixParser::readCorner4(System.Int32,System.Int32)
extern "C"  int32_t BitMatrixParser_readCorner4_m467767355 (BitMatrixParser_t1222713543 * __this, int32_t ___numRows0, int32_t ___numColumns1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.BitMatrix ZXing.Datamatrix.Internal.BitMatrixParser::extractDataRegion(ZXing.Common.BitMatrix)
extern "C"  BitMatrix_t1058711404 * BitMatrixParser_extractDataRegion_m2397821030 (BitMatrixParser_t1222713543 * __this, BitMatrix_t1058711404 * ___bitMatrix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
