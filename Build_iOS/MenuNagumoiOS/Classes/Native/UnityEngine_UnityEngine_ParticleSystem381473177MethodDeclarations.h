﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ParticleSystem
struct ParticleSystem_t381473177;

#include "codegen/il2cpp-codegen.h"

// System.Boolean UnityEngine.ParticleSystem::get_isStopped()
extern "C"  bool ParticleSystem_get_isStopped_m3698458602 (ParticleSystem_t381473177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
