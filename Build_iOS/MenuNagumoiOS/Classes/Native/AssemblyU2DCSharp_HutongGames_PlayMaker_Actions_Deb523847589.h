﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "PlayMaker_HutongGames_PlayMaker_LogLevel284580066.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DebugVector3
struct  DebugVector3_t523847589  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.LogLevel HutongGames.PlayMaker.Actions.DebugVector3::logLevel
	int32_t ___logLevel_9;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.DebugVector3::vector3Variable
	FsmVector3_t533912882 * ___vector3Variable_10;

public:
	inline static int32_t get_offset_of_logLevel_9() { return static_cast<int32_t>(offsetof(DebugVector3_t523847589, ___logLevel_9)); }
	inline int32_t get_logLevel_9() const { return ___logLevel_9; }
	inline int32_t* get_address_of_logLevel_9() { return &___logLevel_9; }
	inline void set_logLevel_9(int32_t value)
	{
		___logLevel_9 = value;
	}

	inline static int32_t get_offset_of_vector3Variable_10() { return static_cast<int32_t>(offsetof(DebugVector3_t523847589, ___vector3Variable_10)); }
	inline FsmVector3_t533912882 * get_vector3Variable_10() const { return ___vector3Variable_10; }
	inline FsmVector3_t533912882 ** get_address_of_vector3Variable_10() { return &___vector3Variable_10; }
	inline void set_vector3Variable_10(FsmVector3_t533912882 * value)
	{
		___vector3Variable_10 = value;
		Il2CppCodeGenWriteBarrier(&___vector3Variable_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
