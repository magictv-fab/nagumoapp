﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Common.DecoderResult
struct DecoderResult_t3752650303;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.String
struct String_t;
// System.Text.Encoding
struct Encoding_t2012439129;
// ZXing.PDF417.PDF417ResultMetadata
struct PDF417ResultMetadata_t2693798038;
// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "QRCode_ZXing_PDF417_PDF417ResultMetadata2693798038.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "mscorlib_System_Text_Encoding2012439129.h"

// System.Void ZXing.PDF417.Internal.DecodedBitStreamParser::.cctor()
extern "C"  void DecodedBitStreamParser__cctor_m3922641245 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.DecoderResult ZXing.PDF417.Internal.DecodedBitStreamParser::decode(System.Int32[],System.String)
extern "C"  DecoderResult_t3752650303 * DecodedBitStreamParser_decode_m1691566498 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___codewords0, String_t* ___ecLevel1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding ZXing.PDF417.Internal.DecodedBitStreamParser::getEncoding(System.String)
extern "C"  Encoding_t2012439129 * DecodedBitStreamParser_getEncoding_m773711251 (Il2CppObject * __this /* static, unused */, String_t* ___encodingName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.DecodedBitStreamParser::decodeMacroBlock(System.Int32[],System.Int32,ZXing.PDF417.PDF417ResultMetadata)
extern "C"  int32_t DecodedBitStreamParser_decodeMacroBlock_m3117341651 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___codewords0, int32_t ___codeIndex1, PDF417ResultMetadata_t2693798038 * ___resultMetadata2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.DecodedBitStreamParser::textCompaction(System.Int32[],System.Int32,System.Text.StringBuilder)
extern "C"  int32_t DecodedBitStreamParser_textCompaction_m552823096 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___codewords0, int32_t ___codeIndex1, StringBuilder_t243639308 * ___result2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.DecodedBitStreamParser::decodeTextCompaction(System.Int32[],System.Int32[],System.Int32,System.Text.StringBuilder)
extern "C"  void DecodedBitStreamParser_decodeTextCompaction_m1434578633 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___textCompactionData0, Int32U5BU5D_t3230847821* ___byteCompactionData1, int32_t ___length2, StringBuilder_t243639308 * ___result3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.DecodedBitStreamParser::byteCompaction(System.Int32,System.Int32[],System.Text.Encoding,System.Int32,System.Text.StringBuilder)
extern "C"  int32_t DecodedBitStreamParser_byteCompaction_m993118751 (Il2CppObject * __this /* static, unused */, int32_t ___mode0, Int32U5BU5D_t3230847821* ___codewords1, Encoding_t2012439129 * ___encoding2, int32_t ___codeIndex3, StringBuilder_t243639308 * ___result4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.DecodedBitStreamParser::numericCompaction(System.Int32[],System.Int32,System.Text.StringBuilder)
extern "C"  int32_t DecodedBitStreamParser_numericCompaction_m3672336250 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___codewords0, int32_t ___codeIndex1, StringBuilder_t243639308 * ___result2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.PDF417.Internal.DecodedBitStreamParser::decodeBase900toBase10(System.Int32[],System.Int32)
extern "C"  String_t* DecodedBitStreamParser_decodeBase900toBase10_m2303797816 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___codewords0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
