﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t1820556512;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "QRCode_ZXing_OneD_OneDimensionalCodeWriter4068326409.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.PlesseyWriter
struct  PlesseyWriter_t2711109794  : public OneDimensionalCodeWriter_t4068326409
{
public:

public:
};

struct PlesseyWriter_t2711109794_StaticFields
{
public:
	// System.Int32[] ZXing.OneD.PlesseyWriter::startWidths
	Int32U5BU5D_t3230847821* ___startWidths_0;
	// System.Int32[] ZXing.OneD.PlesseyWriter::terminationWidths
	Int32U5BU5D_t3230847821* ___terminationWidths_1;
	// System.Int32[] ZXing.OneD.PlesseyWriter::endWidths
	Int32U5BU5D_t3230847821* ___endWidths_2;
	// System.Int32[][] ZXing.OneD.PlesseyWriter::numberWidths
	Int32U5BU5DU5BU5D_t1820556512* ___numberWidths_3;
	// System.Byte[] ZXing.OneD.PlesseyWriter::crcGrid
	ByteU5BU5D_t4260760469* ___crcGrid_4;
	// System.Int32[] ZXing.OneD.PlesseyWriter::crc0Widths
	Int32U5BU5D_t3230847821* ___crc0Widths_5;
	// System.Int32[] ZXing.OneD.PlesseyWriter::crc1Widths
	Int32U5BU5D_t3230847821* ___crc1Widths_6;

public:
	inline static int32_t get_offset_of_startWidths_0() { return static_cast<int32_t>(offsetof(PlesseyWriter_t2711109794_StaticFields, ___startWidths_0)); }
	inline Int32U5BU5D_t3230847821* get_startWidths_0() const { return ___startWidths_0; }
	inline Int32U5BU5D_t3230847821** get_address_of_startWidths_0() { return &___startWidths_0; }
	inline void set_startWidths_0(Int32U5BU5D_t3230847821* value)
	{
		___startWidths_0 = value;
		Il2CppCodeGenWriteBarrier(&___startWidths_0, value);
	}

	inline static int32_t get_offset_of_terminationWidths_1() { return static_cast<int32_t>(offsetof(PlesseyWriter_t2711109794_StaticFields, ___terminationWidths_1)); }
	inline Int32U5BU5D_t3230847821* get_terminationWidths_1() const { return ___terminationWidths_1; }
	inline Int32U5BU5D_t3230847821** get_address_of_terminationWidths_1() { return &___terminationWidths_1; }
	inline void set_terminationWidths_1(Int32U5BU5D_t3230847821* value)
	{
		___terminationWidths_1 = value;
		Il2CppCodeGenWriteBarrier(&___terminationWidths_1, value);
	}

	inline static int32_t get_offset_of_endWidths_2() { return static_cast<int32_t>(offsetof(PlesseyWriter_t2711109794_StaticFields, ___endWidths_2)); }
	inline Int32U5BU5D_t3230847821* get_endWidths_2() const { return ___endWidths_2; }
	inline Int32U5BU5D_t3230847821** get_address_of_endWidths_2() { return &___endWidths_2; }
	inline void set_endWidths_2(Int32U5BU5D_t3230847821* value)
	{
		___endWidths_2 = value;
		Il2CppCodeGenWriteBarrier(&___endWidths_2, value);
	}

	inline static int32_t get_offset_of_numberWidths_3() { return static_cast<int32_t>(offsetof(PlesseyWriter_t2711109794_StaticFields, ___numberWidths_3)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_numberWidths_3() const { return ___numberWidths_3; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_numberWidths_3() { return &___numberWidths_3; }
	inline void set_numberWidths_3(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___numberWidths_3 = value;
		Il2CppCodeGenWriteBarrier(&___numberWidths_3, value);
	}

	inline static int32_t get_offset_of_crcGrid_4() { return static_cast<int32_t>(offsetof(PlesseyWriter_t2711109794_StaticFields, ___crcGrid_4)); }
	inline ByteU5BU5D_t4260760469* get_crcGrid_4() const { return ___crcGrid_4; }
	inline ByteU5BU5D_t4260760469** get_address_of_crcGrid_4() { return &___crcGrid_4; }
	inline void set_crcGrid_4(ByteU5BU5D_t4260760469* value)
	{
		___crcGrid_4 = value;
		Il2CppCodeGenWriteBarrier(&___crcGrid_4, value);
	}

	inline static int32_t get_offset_of_crc0Widths_5() { return static_cast<int32_t>(offsetof(PlesseyWriter_t2711109794_StaticFields, ___crc0Widths_5)); }
	inline Int32U5BU5D_t3230847821* get_crc0Widths_5() const { return ___crc0Widths_5; }
	inline Int32U5BU5D_t3230847821** get_address_of_crc0Widths_5() { return &___crc0Widths_5; }
	inline void set_crc0Widths_5(Int32U5BU5D_t3230847821* value)
	{
		___crc0Widths_5 = value;
		Il2CppCodeGenWriteBarrier(&___crc0Widths_5, value);
	}

	inline static int32_t get_offset_of_crc1Widths_6() { return static_cast<int32_t>(offsetof(PlesseyWriter_t2711109794_StaticFields, ___crc1Widths_6)); }
	inline Int32U5BU5D_t3230847821* get_crc1Widths_6() const { return ___crc1Widths_6; }
	inline Int32U5BU5D_t3230847821** get_address_of_crc1Widths_6() { return &___crc1Widths_6; }
	inline void set_crc1Widths_6(Int32U5BU5D_t3230847821* value)
	{
		___crc1Widths_6 = value;
		Il2CppCodeGenWriteBarrier(&___crc1Widths_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
