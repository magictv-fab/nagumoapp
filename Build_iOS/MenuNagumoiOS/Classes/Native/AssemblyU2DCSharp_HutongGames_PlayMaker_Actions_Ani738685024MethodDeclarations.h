﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AnimatorFollow
struct AnimatorFollow_t738685024;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AnimatorFollow::.ctor()
extern "C"  void AnimatorFollow__ctor_m544474118 (AnimatorFollow_t738685024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorFollow::Reset()
extern "C"  void AnimatorFollow_Reset_m2485874355 (AnimatorFollow_t738685024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorFollow::OnEnter()
extern "C"  void AnimatorFollow_OnEnter_m2940313181 (AnimatorFollow_t738685024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorFollow::OnUpdate()
extern "C"  void AnimatorFollow_OnUpdate_m88954758 (AnimatorFollow_t738685024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorFollow::OnExit()
extern "C"  void AnimatorFollow_OnExit_m1212147099 (AnimatorFollow_t738685024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorFollow::OnAnimatorMoveEvent()
extern "C"  void AnimatorFollow_OnAnimatorMoveEvent_m2354876711 (AnimatorFollow_t738685024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
