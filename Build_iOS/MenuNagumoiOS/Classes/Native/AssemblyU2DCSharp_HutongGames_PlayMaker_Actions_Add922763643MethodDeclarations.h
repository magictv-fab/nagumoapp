﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AddAnimationClip
struct AddAnimationClip_t922763643;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AddAnimationClip::.ctor()
extern "C"  void AddAnimationClip__ctor_m3321568075 (AddAnimationClip_t922763643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddAnimationClip::Reset()
extern "C"  void AddAnimationClip_Reset_m968001016 (AddAnimationClip_t922763643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddAnimationClip::OnEnter()
extern "C"  void AddAnimationClip_OnEnter_m257947746 (AddAnimationClip_t922763643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddAnimationClip::DoAddAnimationClip()
extern "C"  void AddAnimationClip_DoAddAnimationClip_m458185623 (AddAnimationClip_t922763643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
