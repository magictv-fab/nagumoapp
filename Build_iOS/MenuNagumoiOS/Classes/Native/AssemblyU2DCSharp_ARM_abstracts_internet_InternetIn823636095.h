﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.abstracts.internet.InternetInfoAbstract/OnWifiInfoEventHandler
struct OnWifiInfoEventHandler_t558195666;

#include "AssemblyU2DCSharp_ARM_abstracts_components_General3900398046.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.abstracts.internet.InternetInfoAbstract
struct  InternetInfoAbstract_t823636095  : public GeneralComponentsAbstract_t3900398046
{
public:
	// ARM.abstracts.internet.InternetInfoAbstract/OnWifiInfoEventHandler ARM.abstracts.internet.InternetInfoAbstract::onWifiInfo
	OnWifiInfoEventHandler_t558195666 * ___onWifiInfo_5;
	// System.Boolean ARM.abstracts.internet.InternetInfoAbstract::_hasWifi
	bool ____hasWifi_6;

public:
	inline static int32_t get_offset_of_onWifiInfo_5() { return static_cast<int32_t>(offsetof(InternetInfoAbstract_t823636095, ___onWifiInfo_5)); }
	inline OnWifiInfoEventHandler_t558195666 * get_onWifiInfo_5() const { return ___onWifiInfo_5; }
	inline OnWifiInfoEventHandler_t558195666 ** get_address_of_onWifiInfo_5() { return &___onWifiInfo_5; }
	inline void set_onWifiInfo_5(OnWifiInfoEventHandler_t558195666 * value)
	{
		___onWifiInfo_5 = value;
		Il2CppCodeGenWriteBarrier(&___onWifiInfo_5, value);
	}

	inline static int32_t get_offset_of__hasWifi_6() { return static_cast<int32_t>(offsetof(InternetInfoAbstract_t823636095, ____hasWifi_6)); }
	inline bool get__hasWifi_6() const { return ____hasWifi_6; }
	inline bool* get_address_of__hasWifi_6() { return &____hasWifi_6; }
	inline void set__hasWifi_6(bool value)
	{
		____hasWifi_6 = value;
	}
};

struct InternetInfoAbstract_t823636095_StaticFields
{
public:
	// System.Boolean ARM.abstracts.internet.InternetInfoAbstract::isWifi
	bool ___isWifi_4;

public:
	inline static int32_t get_offset_of_isWifi_4() { return static_cast<int32_t>(offsetof(InternetInfoAbstract_t823636095_StaticFields, ___isWifi_4)); }
	inline bool get_isWifi_4() const { return ___isWifi_4; }
	inline bool* get_address_of_isWifi_4() { return &___isWifi_4; }
	inline void set_isWifi_4(bool value)
	{
		___isWifi_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
