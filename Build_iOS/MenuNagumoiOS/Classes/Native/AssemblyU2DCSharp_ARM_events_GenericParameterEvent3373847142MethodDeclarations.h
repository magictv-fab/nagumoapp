﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.events.GenericParameterEventDispatcher/GenericEvent
struct GenericEvent_t3373847142;
// System.Object
struct Il2CppObject;
// ARM.events.GenericParameterVO
struct GenericParameterVO_t1159220759;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_ARM_events_GenericParameterVO1159220759.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ARM.events.GenericParameterEventDispatcher/GenericEvent::.ctor(System.Object,System.IntPtr)
extern "C"  void GenericEvent__ctor_m3288148285 (GenericEvent_t3373847142 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.events.GenericParameterEventDispatcher/GenericEvent::Invoke(ARM.events.GenericParameterVO)
extern "C"  void GenericEvent_Invoke_m197272909 (GenericEvent_t3373847142 * __this, GenericParameterVO_t1159220759 * ___parameter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ARM.events.GenericParameterEventDispatcher/GenericEvent::BeginInvoke(ARM.events.GenericParameterVO,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GenericEvent_BeginInvoke_m3266905238 (GenericEvent_t3373847142 * __this, GenericParameterVO_t1159220759 * ___parameter0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.events.GenericParameterEventDispatcher/GenericEvent::EndInvoke(System.IAsyncResult)
extern "C"  void GenericEvent_EndInvoke_m3140010445 (GenericEvent_t3373847142 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
