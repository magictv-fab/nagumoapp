﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OfertaData[]
struct OfertaDataU5BU5D_t382376346;
// OfertaData
struct OfertaData_t3417615771;
// UnityEngine.WWW
struct WWW_t3134621005;
// UnityEngine.Sprite
struct Sprite_t3199167241;
// System.Object
struct Il2CppObject;
// MinhasOfertasManager
struct MinhasOfertasManager_t1225294387;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MinhasOfertasManager/<ILoadImgs>c__Iterator1F
struct  U3CILoadImgsU3Ec__Iterator1F_t3364386919  : public Il2CppObject
{
public:
	// System.Int32 MinhasOfertasManager/<ILoadImgs>c__Iterator1F::<i>__0
	int32_t ___U3CiU3E__0_0;
	// OfertaData[] MinhasOfertasManager/<ILoadImgs>c__Iterator1F::<$s_32>__1
	OfertaDataU5BU5D_t382376346* ___U3CU24s_32U3E__1_1;
	// System.Int32 MinhasOfertasManager/<ILoadImgs>c__Iterator1F::<$s_33>__2
	int32_t ___U3CU24s_33U3E__2_2;
	// OfertaData MinhasOfertasManager/<ILoadImgs>c__Iterator1F::<ofertaData>__3
	OfertaData_t3417615771 * ___U3CofertaDataU3E__3_3;
	// UnityEngine.WWW MinhasOfertasManager/<ILoadImgs>c__Iterator1F::<www>__4
	WWW_t3134621005 * ___U3CwwwU3E__4_4;
	// UnityEngine.Sprite MinhasOfertasManager/<ILoadImgs>c__Iterator1F::<spt>__5
	Sprite_t3199167241 * ___U3CsptU3E__5_5;
	// System.Int32 MinhasOfertasManager/<ILoadImgs>c__Iterator1F::$PC
	int32_t ___U24PC_6;
	// System.Object MinhasOfertasManager/<ILoadImgs>c__Iterator1F::$current
	Il2CppObject * ___U24current_7;
	// MinhasOfertasManager MinhasOfertasManager/<ILoadImgs>c__Iterator1F::<>f__this
	MinhasOfertasManager_t1225294387 * ___U3CU3Ef__this_8;

public:
	inline static int32_t get_offset_of_U3CiU3E__0_0() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator1F_t3364386919, ___U3CiU3E__0_0)); }
	inline int32_t get_U3CiU3E__0_0() const { return ___U3CiU3E__0_0; }
	inline int32_t* get_address_of_U3CiU3E__0_0() { return &___U3CiU3E__0_0; }
	inline void set_U3CiU3E__0_0(int32_t value)
	{
		___U3CiU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CU24s_32U3E__1_1() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator1F_t3364386919, ___U3CU24s_32U3E__1_1)); }
	inline OfertaDataU5BU5D_t382376346* get_U3CU24s_32U3E__1_1() const { return ___U3CU24s_32U3E__1_1; }
	inline OfertaDataU5BU5D_t382376346** get_address_of_U3CU24s_32U3E__1_1() { return &___U3CU24s_32U3E__1_1; }
	inline void set_U3CU24s_32U3E__1_1(OfertaDataU5BU5D_t382376346* value)
	{
		___U3CU24s_32U3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_32U3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CU24s_33U3E__2_2() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator1F_t3364386919, ___U3CU24s_33U3E__2_2)); }
	inline int32_t get_U3CU24s_33U3E__2_2() const { return ___U3CU24s_33U3E__2_2; }
	inline int32_t* get_address_of_U3CU24s_33U3E__2_2() { return &___U3CU24s_33U3E__2_2; }
	inline void set_U3CU24s_33U3E__2_2(int32_t value)
	{
		___U3CU24s_33U3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CofertaDataU3E__3_3() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator1F_t3364386919, ___U3CofertaDataU3E__3_3)); }
	inline OfertaData_t3417615771 * get_U3CofertaDataU3E__3_3() const { return ___U3CofertaDataU3E__3_3; }
	inline OfertaData_t3417615771 ** get_address_of_U3CofertaDataU3E__3_3() { return &___U3CofertaDataU3E__3_3; }
	inline void set_U3CofertaDataU3E__3_3(OfertaData_t3417615771 * value)
	{
		___U3CofertaDataU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CofertaDataU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__4_4() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator1F_t3364386919, ___U3CwwwU3E__4_4)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__4_4() const { return ___U3CwwwU3E__4_4; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__4_4() { return &___U3CwwwU3E__4_4; }
	inline void set_U3CwwwU3E__4_4(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U3CsptU3E__5_5() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator1F_t3364386919, ___U3CsptU3E__5_5)); }
	inline Sprite_t3199167241 * get_U3CsptU3E__5_5() const { return ___U3CsptU3E__5_5; }
	inline Sprite_t3199167241 ** get_address_of_U3CsptU3E__5_5() { return &___U3CsptU3E__5_5; }
	inline void set_U3CsptU3E__5_5(Sprite_t3199167241 * value)
	{
		___U3CsptU3E__5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsptU3E__5_5, value);
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator1F_t3364386919, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator1F_t3364386919, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_8() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator1F_t3364386919, ___U3CU3Ef__this_8)); }
	inline MinhasOfertasManager_t1225294387 * get_U3CU3Ef__this_8() const { return ___U3CU3Ef__this_8; }
	inline MinhasOfertasManager_t1225294387 ** get_address_of_U3CU3Ef__this_8() { return &___U3CU3Ef__this_8; }
	inline void set_U3CU3Ef__this_8(MinhasOfertasManager_t1225294387 * value)
	{
		___U3CU3Ef__this_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
