﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.PDF417.Internal.BarcodeMetadata
struct BarcodeMetadata_t443526333;

#include "codegen/il2cpp-codegen.h"

// System.Int32 ZXing.PDF417.Internal.BarcodeMetadata::get_ColumnCount()
extern "C"  int32_t BarcodeMetadata_get_ColumnCount_m1824216685 (BarcodeMetadata_t443526333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.BarcodeMetadata::set_ColumnCount(System.Int32)
extern "C"  void BarcodeMetadata_set_ColumnCount_m4066823676 (BarcodeMetadata_t443526333 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.BarcodeMetadata::get_ErrorCorrectionLevel()
extern "C"  int32_t BarcodeMetadata_get_ErrorCorrectionLevel_m1603452460 (BarcodeMetadata_t443526333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.BarcodeMetadata::set_ErrorCorrectionLevel(System.Int32)
extern "C"  void BarcodeMetadata_set_ErrorCorrectionLevel_m3332516223 (BarcodeMetadata_t443526333 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.BarcodeMetadata::get_RowCountUpper()
extern "C"  int32_t BarcodeMetadata_get_RowCountUpper_m1530946465 (BarcodeMetadata_t443526333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.BarcodeMetadata::set_RowCountUpper(System.Int32)
extern "C"  void BarcodeMetadata_set_RowCountUpper_m2249536944 (BarcodeMetadata_t443526333 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.BarcodeMetadata::get_RowCountLower()
extern "C"  int32_t BarcodeMetadata_get_RowCountLower_m2111183424 (BarcodeMetadata_t443526333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.BarcodeMetadata::set_RowCountLower(System.Int32)
extern "C"  void BarcodeMetadata_set_RowCountLower_m3140154831 (BarcodeMetadata_t443526333 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.PDF417.Internal.BarcodeMetadata::get_RowCount()
extern "C"  int32_t BarcodeMetadata_get_RowCount_m4268149667 (BarcodeMetadata_t443526333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.BarcodeMetadata::set_RowCount(System.Int32)
extern "C"  void BarcodeMetadata_set_RowCount_m3110416246 (BarcodeMetadata_t443526333 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.BarcodeMetadata::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void BarcodeMetadata__ctor_m3580019669 (BarcodeMetadata_t443526333 * __this, int32_t ___columnCount0, int32_t ___rowCountUpperPart1, int32_t ___rowCountLowerPart2, int32_t ___errorCorrectionLevel3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
