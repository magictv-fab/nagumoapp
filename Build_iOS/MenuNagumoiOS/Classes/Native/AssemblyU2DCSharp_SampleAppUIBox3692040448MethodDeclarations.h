﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SampleAppUIBox
struct SampleAppUIBox_t3692040448;

#include "codegen/il2cpp-codegen.h"

// System.Void SampleAppUIBox::.ctor()
extern "C"  void SampleAppUIBox__ctor_m4236827547 (SampleAppUIBox_t3692040448 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SampleAppUIBox::Draw()
extern "C"  void SampleAppUIBox_Draw_m3966393997 (SampleAppUIBox_t3692040448 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
