﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetRotation
struct GetRotation_t1619760002;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetRotation::.ctor()
extern "C"  void GetRotation__ctor_m205269492 (GetRotation_t1619760002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRotation::Reset()
extern "C"  void GetRotation_Reset_m2146669729 (GetRotation_t1619760002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRotation::OnEnter()
extern "C"  void GetRotation_OnEnter_m3382182091 (GetRotation_t1619760002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRotation::OnUpdate()
extern "C"  void GetRotation_OnUpdate_m901989080 (GetRotation_t1619760002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRotation::DoGetRotation()
extern "C"  void GetRotation_DoGetRotation_m507767611 (GetRotation_t1619760002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
