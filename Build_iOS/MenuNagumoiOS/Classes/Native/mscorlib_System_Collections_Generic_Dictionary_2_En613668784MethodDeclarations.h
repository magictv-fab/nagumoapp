﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1309933840MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2520873352(__this, ___dictionary0, method) ((  void (*) (Enumerator_t613668784 *, Dictionary_2_t3591312688 *, const MethodInfo*))Enumerator__ctor_m661894921_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3199694179(__this, method) ((  Il2CppObject * (*) (Enumerator_t613668784 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m186784184_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m597570093(__this, method) ((  void (*) (Enumerator_t613668784 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2125948428_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4211875364(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t613668784 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1611766933_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m354671615(__this, method) ((  Il2CppObject * (*) (Enumerator_t613668784 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4168428692_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2585442001(__this, method) ((  Il2CppObject * (*) (Enumerator_t613668784 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3998889510_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::MoveNext()
#define Enumerator_MoveNext_m1332923741(__this, method) ((  bool (*) (Enumerator_t613668784 *, const MethodInfo*))Enumerator_MoveNext_m2347244280_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::get_Current()
#define Enumerator_get_Current_m54649407(__this, method) ((  KeyValuePair_2_t3490093394  (*) (Enumerator_t613668784 *, const MethodInfo*))Enumerator_get_Current_m534300216_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1244334630(__this, method) ((  int32_t (*) (Enumerator_t613668784 *, const MethodInfo*))Enumerator_get_CurrentKey_m791811781_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1406154470(__this, method) ((  InAppEventDispatcherOnOff_t3474551315 * (*) (Enumerator_t613668784 *, const MethodInfo*))Enumerator_get_CurrentValue_m1176045609_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::Reset()
#define Enumerator_Reset_m3694413786(__this, method) ((  void (*) (Enumerator_t613668784 *, const MethodInfo*))Enumerator_Reset_m1067045851_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::VerifyState()
#define Enumerator_VerifyState_m4257533027(__this, method) ((  void (*) (Enumerator_t613668784 *, const MethodInfo*))Enumerator_VerifyState_m2662831012_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1130277643(__this, method) ((  void (*) (Enumerator_t613668784 *, const MethodInfo*))Enumerator_VerifyCurrent_m1924965900_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::Dispose()
#define Enumerator_Dispose_m917123626(__this, method) ((  void (*) (Enumerator_t613668784 *, const MethodInfo*))Enumerator_Dispose_m1457308139_gshared)(__this, method)
