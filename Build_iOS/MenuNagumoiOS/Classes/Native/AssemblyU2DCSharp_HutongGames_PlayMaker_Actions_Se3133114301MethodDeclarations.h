﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetLightColor
struct SetLightColor_t3133114301;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetLightColor::.ctor()
extern "C"  void SetLightColor__ctor_m1394617881 (SetLightColor_t3133114301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightColor::Reset()
extern "C"  void SetLightColor_Reset_m3336018118 (SetLightColor_t3133114301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightColor::OnEnter()
extern "C"  void SetLightColor_OnEnter_m3884683184 (SetLightColor_t3133114301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightColor::OnUpdate()
extern "C"  void SetLightColor_OnUpdate_m3594621075 (SetLightColor_t3133114301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightColor::DoSetLightColor()
extern "C"  void SetLightColor_DoSetLightColor_m91388379 (SetLightColor_t3133114301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
