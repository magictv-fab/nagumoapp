﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ve4195528751.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector3Operator
struct  Vector3Operator_t542499106  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3Operator::vector1
	FsmVector3_t533912882 * ___vector1_9;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3Operator::vector2
	FsmVector3_t533912882 * ___vector2_10;
	// HutongGames.PlayMaker.Actions.Vector3Operator/Vector3Operation HutongGames.PlayMaker.Actions.Vector3Operator::operation
	int32_t ___operation_11;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3Operator::storeVector3Result
	FsmVector3_t533912882 * ___storeVector3Result_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector3Operator::storeFloatResult
	FsmFloat_t2134102846 * ___storeFloatResult_13;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector3Operator::everyFrame
	bool ___everyFrame_14;

public:
	inline static int32_t get_offset_of_vector1_9() { return static_cast<int32_t>(offsetof(Vector3Operator_t542499106, ___vector1_9)); }
	inline FsmVector3_t533912882 * get_vector1_9() const { return ___vector1_9; }
	inline FsmVector3_t533912882 ** get_address_of_vector1_9() { return &___vector1_9; }
	inline void set_vector1_9(FsmVector3_t533912882 * value)
	{
		___vector1_9 = value;
		Il2CppCodeGenWriteBarrier(&___vector1_9, value);
	}

	inline static int32_t get_offset_of_vector2_10() { return static_cast<int32_t>(offsetof(Vector3Operator_t542499106, ___vector2_10)); }
	inline FsmVector3_t533912882 * get_vector2_10() const { return ___vector2_10; }
	inline FsmVector3_t533912882 ** get_address_of_vector2_10() { return &___vector2_10; }
	inline void set_vector2_10(FsmVector3_t533912882 * value)
	{
		___vector2_10 = value;
		Il2CppCodeGenWriteBarrier(&___vector2_10, value);
	}

	inline static int32_t get_offset_of_operation_11() { return static_cast<int32_t>(offsetof(Vector3Operator_t542499106, ___operation_11)); }
	inline int32_t get_operation_11() const { return ___operation_11; }
	inline int32_t* get_address_of_operation_11() { return &___operation_11; }
	inline void set_operation_11(int32_t value)
	{
		___operation_11 = value;
	}

	inline static int32_t get_offset_of_storeVector3Result_12() { return static_cast<int32_t>(offsetof(Vector3Operator_t542499106, ___storeVector3Result_12)); }
	inline FsmVector3_t533912882 * get_storeVector3Result_12() const { return ___storeVector3Result_12; }
	inline FsmVector3_t533912882 ** get_address_of_storeVector3Result_12() { return &___storeVector3Result_12; }
	inline void set_storeVector3Result_12(FsmVector3_t533912882 * value)
	{
		___storeVector3Result_12 = value;
		Il2CppCodeGenWriteBarrier(&___storeVector3Result_12, value);
	}

	inline static int32_t get_offset_of_storeFloatResult_13() { return static_cast<int32_t>(offsetof(Vector3Operator_t542499106, ___storeFloatResult_13)); }
	inline FsmFloat_t2134102846 * get_storeFloatResult_13() const { return ___storeFloatResult_13; }
	inline FsmFloat_t2134102846 ** get_address_of_storeFloatResult_13() { return &___storeFloatResult_13; }
	inline void set_storeFloatResult_13(FsmFloat_t2134102846 * value)
	{
		___storeFloatResult_13 = value;
		Il2CppCodeGenWriteBarrier(&___storeFloatResult_13, value);
	}

	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(Vector3Operator_t542499106, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
