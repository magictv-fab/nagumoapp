﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertasData
struct OfertasData_t2909397772;

#include "codegen/il2cpp-codegen.h"

// System.Void OfertasData::.ctor()
extern "C"  void OfertasData__ctor_m1050822111 (OfertasData_t2909397772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
