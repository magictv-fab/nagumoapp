﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// UnityEngine.Animator
struct Animator_t2776330603;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorLayerName
struct  GetAnimatorLayerName_t4020332743  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorLayerName::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorLayerName::layerIndex
	FsmInt_t1596138449 * ___layerIndex_10;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetAnimatorLayerName::layerName
	FsmString_t952858651 * ___layerName_11;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorLayerName::_animator
	Animator_t2776330603 * ____animator_12;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(GetAnimatorLayerName_t4020332743, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_layerIndex_10() { return static_cast<int32_t>(offsetof(GetAnimatorLayerName_t4020332743, ___layerIndex_10)); }
	inline FsmInt_t1596138449 * get_layerIndex_10() const { return ___layerIndex_10; }
	inline FsmInt_t1596138449 ** get_address_of_layerIndex_10() { return &___layerIndex_10; }
	inline void set_layerIndex_10(FsmInt_t1596138449 * value)
	{
		___layerIndex_10 = value;
		Il2CppCodeGenWriteBarrier(&___layerIndex_10, value);
	}

	inline static int32_t get_offset_of_layerName_11() { return static_cast<int32_t>(offsetof(GetAnimatorLayerName_t4020332743, ___layerName_11)); }
	inline FsmString_t952858651 * get_layerName_11() const { return ___layerName_11; }
	inline FsmString_t952858651 ** get_address_of_layerName_11() { return &___layerName_11; }
	inline void set_layerName_11(FsmString_t952858651 * value)
	{
		___layerName_11 = value;
		Il2CppCodeGenWriteBarrier(&___layerName_11, value);
	}

	inline static int32_t get_offset_of__animator_12() { return static_cast<int32_t>(offsetof(GetAnimatorLayerName_t4020332743, ____animator_12)); }
	inline Animator_t2776330603 * get__animator_12() const { return ____animator_12; }
	inline Animator_t2776330603 ** get_address_of__animator_12() { return &____animator_12; }
	inline void set__animator_12(Animator_t2776330603 * value)
	{
		____animator_12 = value;
		Il2CppCodeGenWriteBarrier(&____animator_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
