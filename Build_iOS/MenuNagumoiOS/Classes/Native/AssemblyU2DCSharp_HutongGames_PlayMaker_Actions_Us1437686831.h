﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co2322045418.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UseGravity
struct  UseGravity_t1437686831  : public ComponentAction_1_t2322045418
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UseGravity::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UseGravity::useGravity
	FsmBool_t1075959796 * ___useGravity_12;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(UseGravity_t1437686831, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_useGravity_12() { return static_cast<int32_t>(offsetof(UseGravity_t1437686831, ___useGravity_12)); }
	inline FsmBool_t1075959796 * get_useGravity_12() const { return ___useGravity_12; }
	inline FsmBool_t1075959796 ** get_address_of_useGravity_12() { return &___useGravity_12; }
	inline void set_useGravity_12(FsmBool_t1075959796 * value)
	{
		___useGravity_12 = value;
		Il2CppCodeGenWriteBarrier(&___useGravity_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
