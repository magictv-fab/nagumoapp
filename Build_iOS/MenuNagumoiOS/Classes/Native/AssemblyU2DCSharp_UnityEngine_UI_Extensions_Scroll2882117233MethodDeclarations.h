﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Extensions.ScrollSnapBase/SelectionPageChangedEvent
struct SelectionPageChangedEvent_t2882117233;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.Extensions.ScrollSnapBase/SelectionPageChangedEvent::.ctor()
extern "C"  void SelectionPageChangedEvent__ctor_m2043642186 (SelectionPageChangedEvent_t2882117233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
