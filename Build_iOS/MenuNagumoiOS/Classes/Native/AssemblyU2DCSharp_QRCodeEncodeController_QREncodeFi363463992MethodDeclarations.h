﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// QRCodeEncodeController/QREncodeFinished
struct QREncodeFinished_t363463992;
// System.Object
struct Il2CppObject;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void QRCodeEncodeController/QREncodeFinished::.ctor(System.Object,System.IntPtr)
extern "C"  void QREncodeFinished__ctor_m65800975 (QREncodeFinished_t363463992 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QRCodeEncodeController/QREncodeFinished::Invoke(UnityEngine.Texture2D)
extern "C"  void QREncodeFinished_Invoke_m3898726803 (QREncodeFinished_t363463992 * __this, Texture2D_t3884108195 * ___tex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult QRCodeEncodeController/QREncodeFinished::BeginInvoke(UnityEngine.Texture2D,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * QREncodeFinished_BeginInvoke_m3968465452 (QREncodeFinished_t363463992 * __this, Texture2D_t3884108195 * ___tex0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QRCodeEncodeController/QREncodeFinished::EndInvoke(System.IAsyncResult)
extern "C"  void QREncodeFinished_EndInvoke_m4083506335 (QREncodeFinished_t363463992 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
