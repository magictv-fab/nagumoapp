﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.NetworkReachability>
struct DefaultComparer_t3355146998;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_NetworkReachability612403035.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.NetworkReachability>::.ctor()
extern "C"  void DefaultComparer__ctor_m2316726988_gshared (DefaultComparer_t3355146998 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2316726988(__this, method) ((  void (*) (DefaultComparer_t3355146998 *, const MethodInfo*))DefaultComparer__ctor_m2316726988_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.NetworkReachability>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3672027487_gshared (DefaultComparer_t3355146998 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3672027487(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3355146998 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m3672027487_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.NetworkReachability>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m273507485_gshared (DefaultComparer_t3355146998 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m273507485(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3355146998 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m273507485_gshared)(__this, ___x0, ___y1, method)
