﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TransformEvents/OnBundleIDEventHandler
struct OnBundleIDEventHandler_t3610444662;
// TransformEvents/OnInAppEventHandler
struct OnInAppEventHandler_t2177803875;
// TransformEvents/OnTransformEventHandler
struct OnTransformEventHandler_t2999119763;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TransformEvents
struct  TransformEvents_t2233642885  : public Il2CppObject
{
public:

public:
};

struct TransformEvents_t2233642885_StaticFields
{
public:
	// TransformEvents/OnBundleIDEventHandler TransformEvents::onBundleID
	OnBundleIDEventHandler_t3610444662 * ___onBundleID_0;
	// TransformEvents/OnInAppEventHandler TransformEvents::onInApp
	OnInAppEventHandler_t2177803875 * ___onInApp_1;
	// TransformEvents/OnTransformEventHandler TransformEvents::onPosition
	OnTransformEventHandler_t2999119763 * ___onPosition_2;
	// TransformEvents/OnTransformEventHandler TransformEvents::onRotation
	OnTransformEventHandler_t2999119763 * ___onRotation_3;
	// TransformEvents/OnTransformEventHandler TransformEvents::onScale
	OnTransformEventHandler_t2999119763 * ___onScale_4;

public:
	inline static int32_t get_offset_of_onBundleID_0() { return static_cast<int32_t>(offsetof(TransformEvents_t2233642885_StaticFields, ___onBundleID_0)); }
	inline OnBundleIDEventHandler_t3610444662 * get_onBundleID_0() const { return ___onBundleID_0; }
	inline OnBundleIDEventHandler_t3610444662 ** get_address_of_onBundleID_0() { return &___onBundleID_0; }
	inline void set_onBundleID_0(OnBundleIDEventHandler_t3610444662 * value)
	{
		___onBundleID_0 = value;
		Il2CppCodeGenWriteBarrier(&___onBundleID_0, value);
	}

	inline static int32_t get_offset_of_onInApp_1() { return static_cast<int32_t>(offsetof(TransformEvents_t2233642885_StaticFields, ___onInApp_1)); }
	inline OnInAppEventHandler_t2177803875 * get_onInApp_1() const { return ___onInApp_1; }
	inline OnInAppEventHandler_t2177803875 ** get_address_of_onInApp_1() { return &___onInApp_1; }
	inline void set_onInApp_1(OnInAppEventHandler_t2177803875 * value)
	{
		___onInApp_1 = value;
		Il2CppCodeGenWriteBarrier(&___onInApp_1, value);
	}

	inline static int32_t get_offset_of_onPosition_2() { return static_cast<int32_t>(offsetof(TransformEvents_t2233642885_StaticFields, ___onPosition_2)); }
	inline OnTransformEventHandler_t2999119763 * get_onPosition_2() const { return ___onPosition_2; }
	inline OnTransformEventHandler_t2999119763 ** get_address_of_onPosition_2() { return &___onPosition_2; }
	inline void set_onPosition_2(OnTransformEventHandler_t2999119763 * value)
	{
		___onPosition_2 = value;
		Il2CppCodeGenWriteBarrier(&___onPosition_2, value);
	}

	inline static int32_t get_offset_of_onRotation_3() { return static_cast<int32_t>(offsetof(TransformEvents_t2233642885_StaticFields, ___onRotation_3)); }
	inline OnTransformEventHandler_t2999119763 * get_onRotation_3() const { return ___onRotation_3; }
	inline OnTransformEventHandler_t2999119763 ** get_address_of_onRotation_3() { return &___onRotation_3; }
	inline void set_onRotation_3(OnTransformEventHandler_t2999119763 * value)
	{
		___onRotation_3 = value;
		Il2CppCodeGenWriteBarrier(&___onRotation_3, value);
	}

	inline static int32_t get_offset_of_onScale_4() { return static_cast<int32_t>(offsetof(TransformEvents_t2233642885_StaticFields, ___onScale_4)); }
	inline OnTransformEventHandler_t2999119763 * get_onScale_4() const { return ___onScale_4; }
	inline OnTransformEventHandler_t2999119763 ** get_address_of_onScale_4() { return &___onScale_4; }
	inline void set_onScale_4(OnTransformEventHandler_t2999119763 * value)
	{
		___onScale_4 = value;
		Il2CppCodeGenWriteBarrier(&___onScale_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
