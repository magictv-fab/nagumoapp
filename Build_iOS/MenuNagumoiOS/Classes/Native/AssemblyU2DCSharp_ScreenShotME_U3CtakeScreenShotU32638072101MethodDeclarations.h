﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScreenShotME/<takeScreenShot>c__Iterator0
struct U3CtakeScreenShotU3Ec__Iterator0_t2638072101;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ScreenShotME/<takeScreenShot>c__Iterator0::.ctor()
extern "C"  void U3CtakeScreenShotU3Ec__Iterator0__ctor_m3007260134 (U3CtakeScreenShotU3Ec__Iterator0_t2638072101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScreenShotME/<takeScreenShot>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CtakeScreenShotU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1698473836 (U3CtakeScreenShotU3Ec__Iterator0_t2638072101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScreenShotME/<takeScreenShot>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CtakeScreenShotU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1721959168 (U3CtakeScreenShotU3Ec__Iterator0_t2638072101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ScreenShotME/<takeScreenShot>c__Iterator0::MoveNext()
extern "C"  bool U3CtakeScreenShotU3Ec__Iterator0_MoveNext_m4203206478 (U3CtakeScreenShotU3Ec__Iterator0_t2638072101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenShotME/<takeScreenShot>c__Iterator0::Dispose()
extern "C"  void U3CtakeScreenShotU3Ec__Iterator0_Dispose_m3657248931 (U3CtakeScreenShotU3Ec__Iterator0_t2638072101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenShotME/<takeScreenShot>c__Iterator0::Reset()
extern "C"  void U3CtakeScreenShotU3Ec__Iterator0_Reset_m653693075 (U3CtakeScreenShotU3Ec__Iterator0_t2638072101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
