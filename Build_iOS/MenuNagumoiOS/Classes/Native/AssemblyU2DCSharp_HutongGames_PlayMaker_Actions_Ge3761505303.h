﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetControllerHitInfo
struct  GetControllerHitInfo_t3761505303  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetControllerHitInfo::gameObjectHit
	FsmGameObject_t1697147867 * ___gameObjectHit_9;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetControllerHitInfo::contactPoint
	FsmVector3_t533912882 * ___contactPoint_10;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetControllerHitInfo::contactNormal
	FsmVector3_t533912882 * ___contactNormal_11;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetControllerHitInfo::moveDirection
	FsmVector3_t533912882 * ___moveDirection_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetControllerHitInfo::moveLength
	FsmFloat_t2134102846 * ___moveLength_13;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetControllerHitInfo::physicsMaterialName
	FsmString_t952858651 * ___physicsMaterialName_14;

public:
	inline static int32_t get_offset_of_gameObjectHit_9() { return static_cast<int32_t>(offsetof(GetControllerHitInfo_t3761505303, ___gameObjectHit_9)); }
	inline FsmGameObject_t1697147867 * get_gameObjectHit_9() const { return ___gameObjectHit_9; }
	inline FsmGameObject_t1697147867 ** get_address_of_gameObjectHit_9() { return &___gameObjectHit_9; }
	inline void set_gameObjectHit_9(FsmGameObject_t1697147867 * value)
	{
		___gameObjectHit_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObjectHit_9, value);
	}

	inline static int32_t get_offset_of_contactPoint_10() { return static_cast<int32_t>(offsetof(GetControllerHitInfo_t3761505303, ___contactPoint_10)); }
	inline FsmVector3_t533912882 * get_contactPoint_10() const { return ___contactPoint_10; }
	inline FsmVector3_t533912882 ** get_address_of_contactPoint_10() { return &___contactPoint_10; }
	inline void set_contactPoint_10(FsmVector3_t533912882 * value)
	{
		___contactPoint_10 = value;
		Il2CppCodeGenWriteBarrier(&___contactPoint_10, value);
	}

	inline static int32_t get_offset_of_contactNormal_11() { return static_cast<int32_t>(offsetof(GetControllerHitInfo_t3761505303, ___contactNormal_11)); }
	inline FsmVector3_t533912882 * get_contactNormal_11() const { return ___contactNormal_11; }
	inline FsmVector3_t533912882 ** get_address_of_contactNormal_11() { return &___contactNormal_11; }
	inline void set_contactNormal_11(FsmVector3_t533912882 * value)
	{
		___contactNormal_11 = value;
		Il2CppCodeGenWriteBarrier(&___contactNormal_11, value);
	}

	inline static int32_t get_offset_of_moveDirection_12() { return static_cast<int32_t>(offsetof(GetControllerHitInfo_t3761505303, ___moveDirection_12)); }
	inline FsmVector3_t533912882 * get_moveDirection_12() const { return ___moveDirection_12; }
	inline FsmVector3_t533912882 ** get_address_of_moveDirection_12() { return &___moveDirection_12; }
	inline void set_moveDirection_12(FsmVector3_t533912882 * value)
	{
		___moveDirection_12 = value;
		Il2CppCodeGenWriteBarrier(&___moveDirection_12, value);
	}

	inline static int32_t get_offset_of_moveLength_13() { return static_cast<int32_t>(offsetof(GetControllerHitInfo_t3761505303, ___moveLength_13)); }
	inline FsmFloat_t2134102846 * get_moveLength_13() const { return ___moveLength_13; }
	inline FsmFloat_t2134102846 ** get_address_of_moveLength_13() { return &___moveLength_13; }
	inline void set_moveLength_13(FsmFloat_t2134102846 * value)
	{
		___moveLength_13 = value;
		Il2CppCodeGenWriteBarrier(&___moveLength_13, value);
	}

	inline static int32_t get_offset_of_physicsMaterialName_14() { return static_cast<int32_t>(offsetof(GetControllerHitInfo_t3761505303, ___physicsMaterialName_14)); }
	inline FsmString_t952858651 * get_physicsMaterialName_14() const { return ___physicsMaterialName_14; }
	inline FsmString_t952858651 ** get_address_of_physicsMaterialName_14() { return &___physicsMaterialName_14; }
	inline void set_physicsMaterialName_14(FsmString_t952858651 * value)
	{
		___physicsMaterialName_14 = value;
		Il2CppCodeGenWriteBarrier(&___physicsMaterialName_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
