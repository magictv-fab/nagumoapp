﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.ZipEntry
struct ZipEntry_t3141689087;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// ICSharpCode.SharpZipLib.Zip.ZipExtraData
struct ZipExtraData_t591052775;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Comp3174865049.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipE3141689087.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipEx591052775.h"

// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::.ctor(System.String)
extern "C"  void ZipEntry__ctor_m2695872246 (ZipEntry_t3141689087 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::.ctor(System.String,System.Int32)
extern "C"  void ZipEntry__ctor_m3382173089 (ZipEntry_t3141689087 * __this, String_t* ___name0, int32_t ___versionRequiredToExtract1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::.ctor(System.String,System.Int32,System.Int32,ICSharpCode.SharpZipLib.Zip.CompressionMethod)
extern "C"  void ZipEntry__ctor_m1495592153 (ZipEntry_t3141689087 * __this, String_t* ___name0, int32_t ___versionRequiredToExtract1, int32_t ___madeByInfo2, int32_t ___method3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::.ctor(ICSharpCode.SharpZipLib.Zip.ZipEntry)
extern "C"  void ZipEntry__ctor_m3936266237 (ZipEntry_t3141689087 * __this, ZipEntry_t3141689087 * ___entry0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::get_HasCrc()
extern "C"  bool ZipEntry_get_HasCrc_m1536219599 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::get_IsCrypted()
extern "C"  bool ZipEntry_get_IsCrypted_m835325776 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_IsCrypted(System.Boolean)
extern "C"  void ZipEntry_set_IsCrypted_m3650464263 (ZipEntry_t3141689087 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::get_IsUnicodeText()
extern "C"  bool ZipEntry_get_IsUnicodeText_m994427437 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_IsUnicodeText(System.Boolean)
extern "C"  void ZipEntry_set_IsUnicodeText_m1698821668 (ZipEntry_t3141689087 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte ICSharpCode.SharpZipLib.Zip.ZipEntry::get_CryptoCheckValue()
extern "C"  uint8_t ZipEntry_get_CryptoCheckValue_m1762044215 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_CryptoCheckValue(System.Byte)
extern "C"  void ZipEntry_set_CryptoCheckValue_m1228782188 (ZipEntry_t3141689087 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_Flags()
extern "C"  int32_t ZipEntry_get_Flags_m3243342010 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_Flags(System.Int32)
extern "C"  void ZipEntry_set_Flags_m1104600421 (ZipEntry_t3141689087 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_ZipFileIndex()
extern "C"  int64_t ZipEntry_get_ZipFileIndex_m3339220901 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_ZipFileIndex(System.Int64)
extern "C"  void ZipEntry_set_ZipFileIndex_m182132316 (ZipEntry_t3141689087 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_Offset()
extern "C"  int64_t ZipEntry_get_Offset_m3740282211 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_Offset(System.Int64)
extern "C"  void ZipEntry_set_Offset_m3996341978 (ZipEntry_t3141689087 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_ExternalFileAttributes()
extern "C"  int32_t ZipEntry_get_ExternalFileAttributes_m2511883629 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_ExternalFileAttributes(System.Int32)
extern "C"  void ZipEntry_set_ExternalFileAttributes_m190812196 (ZipEntry_t3141689087 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_VersionMadeBy()
extern "C"  int32_t ZipEntry_get_VersionMadeBy_m837509175 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::get_IsDOSEntry()
extern "C"  bool ZipEntry_get_IsDOSEntry_m1592482761 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::HasDosAttributes(System.Int32)
extern "C"  bool ZipEntry_HasDosAttributes_m3797516098 (ZipEntry_t3141689087 * __this, int32_t ___attributes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_HostSystem()
extern "C"  int32_t ZipEntry_get_HostSystem_m880792966 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_HostSystem(System.Int32)
extern "C"  void ZipEntry_set_HostSystem_m554722237 (ZipEntry_t3141689087 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_Version()
extern "C"  int32_t ZipEntry_get_Version_m3075301963 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::get_CanDecompress()
extern "C"  bool ZipEntry_get_CanDecompress_m312149536 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::ForceZip64()
extern "C"  void ZipEntry_ForceZip64_m2846961516 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::IsZip64Forced()
extern "C"  bool ZipEntry_IsZip64Forced_m4159918020 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::get_LocalHeaderRequiresZip64()
extern "C"  bool ZipEntry_get_LocalHeaderRequiresZip64_m1314000846 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::get_CentralHeaderRequiresZip64()
extern "C"  bool ZipEntry_get_CentralHeaderRequiresZip64_m1416068868 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_DosTime()
extern "C"  int64_t ZipEntry_get_DosTime_m3255839047 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_DosTime(System.Int64)
extern "C"  void ZipEntry_set_DosTime_m1137080084 (ZipEntry_t3141689087 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime ICSharpCode.SharpZipLib.Zip.ZipEntry::get_DateTime()
extern "C"  DateTime_t4283661327  ZipEntry_get_DateTime_m2673386741 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_DateTime(System.DateTime)
extern "C"  void ZipEntry_set_DateTime_m2005648936 (ZipEntry_t3141689087 * __this, DateTime_t4283661327  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Zip.ZipEntry::get_Name()
extern "C"  String_t* ZipEntry_get_Name_m65147311 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_Size()
extern "C"  int64_t ZipEntry_get_Size_m3947453713 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_Size(System.Int64)
extern "C"  void ZipEntry_set_Size_m71136968 (ZipEntry_t3141689087 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_CompressedSize()
extern "C"  int64_t ZipEntry_get_CompressedSize_m2510665010 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_CompressedSize(System.Int64)
extern "C"  void ZipEntry_set_CompressedSize_m3933711145 (ZipEntry_t3141689087 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_Crc()
extern "C"  int64_t ZipEntry_get_Crc_m944088998 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_Crc(System.Int64)
extern "C"  void ZipEntry_set_Crc_m3216462963 (ZipEntry_t3141689087 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Zip.CompressionMethod ICSharpCode.SharpZipLib.Zip.ZipEntry::get_CompressionMethod()
extern "C"  int32_t ZipEntry_get_CompressionMethod_m409292862 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_CompressionMethod(ICSharpCode.SharpZipLib.Zip.CompressionMethod)
extern "C"  void ZipEntry_set_CompressionMethod_m3681808981 (ZipEntry_t3141689087 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Zip.CompressionMethod ICSharpCode.SharpZipLib.Zip.ZipEntry::get_CompressionMethodForHeader()
extern "C"  int32_t ZipEntry_get_CompressionMethodForHeader_m2904179066 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ICSharpCode.SharpZipLib.Zip.ZipEntry::get_ExtraData()
extern "C"  ByteU5BU5D_t4260760469* ZipEntry_get_ExtraData_m144965873 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_ExtraData(System.Byte[])
extern "C"  void ZipEntry_set_ExtraData_m2953997954 (ZipEntry_t3141689087 * __this, ByteU5BU5D_t4260760469* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_AESKeySize()
extern "C"  int32_t ZipEntry_get_AESKeySize_m1604554816 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_AESKeySize(System.Int32)
extern "C"  void ZipEntry_set_AESKeySize_m765450103 (ZipEntry_t3141689087 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte ICSharpCode.SharpZipLib.Zip.ZipEntry::get_AESEncryptionStrength()
extern "C"  uint8_t ZipEntry_get_AESEncryptionStrength_m141218312 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_AESSaltLen()
extern "C"  int32_t ZipEntry_get_AESSaltLen_m3913231199 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_AESOverheadSize()
extern "C"  int32_t ZipEntry_get_AESOverheadSize_m3615403447 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::ProcessExtraData(System.Boolean)
extern "C"  void ZipEntry_ProcessExtraData_m938527066 (ZipEntry_t3141689087 * __this, bool ___localHeader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::ProcessAESExtraData(ICSharpCode.SharpZipLib.Zip.ZipExtraData)
extern "C"  void ZipEntry_ProcessAESExtraData_m2541008205 (ZipEntry_t3141689087 * __this, ZipExtraData_t591052775 * ___extraData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Zip.ZipEntry::get_Comment()
extern "C"  String_t* ZipEntry_get_Comment_m1097531357 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_Comment(System.String)
extern "C"  void ZipEntry_set_Comment_m3883979158 (ZipEntry_t3141689087 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::get_IsDirectory()
extern "C"  bool ZipEntry_get_IsDirectory_m4166244016 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::get_IsFile()
extern "C"  bool ZipEntry_get_IsFile_m820832187 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::IsCompressionMethodSupported()
extern "C"  bool ZipEntry_IsCompressionMethodSupported_m3458152189 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ICSharpCode.SharpZipLib.Zip.ZipEntry::Clone()
extern "C"  Il2CppObject * ZipEntry_Clone_m3428460594 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Zip.ZipEntry::ToString()
extern "C"  String_t* ZipEntry_ToString_m3721839687 (ZipEntry_t3141689087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::IsCompressionMethodSupported(ICSharpCode.SharpZipLib.Zip.CompressionMethod)
extern "C"  bool ZipEntry_IsCompressionMethodSupported_m1429415276 (Il2CppObject * __this /* static, unused */, int32_t ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Zip.ZipEntry::CleanName(System.String)
extern "C"  String_t* ZipEntry_CleanName_m3318236935 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
