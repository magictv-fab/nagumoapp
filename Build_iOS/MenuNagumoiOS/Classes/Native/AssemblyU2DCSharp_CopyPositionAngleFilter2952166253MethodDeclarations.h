﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CopyPositionAngleFilter
struct CopyPositionAngleFilter_t2952166253;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void CopyPositionAngleFilter::.ctor()
extern "C"  void CopyPositionAngleFilter__ctor_m3883363998 (CopyPositionAngleFilter_t2952166253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion CopyPositionAngleFilter::_doFilter(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  CopyPositionAngleFilter__doFilter_m1197229888 (CopyPositionAngleFilter_t2952166253 * __this, Quaternion_t1553702882  ___currentValue0, Quaternion_t1553702882  ___nextValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 CopyPositionAngleFilter::_doFilter(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  CopyPositionAngleFilter__doFilter_m1883169876 (CopyPositionAngleFilter_t2952166253 * __this, Vector3_t4282066566  ___currentValue0, Vector3_t4282066566  ___nextValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CopyPositionAngleFilter::Update()
extern "C"  void CopyPositionAngleFilter_Update_m1852061743 (CopyPositionAngleFilter_t2952166253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CopyPositionAngleFilter::LateUpdate()
extern "C"  void CopyPositionAngleFilter_LateUpdate_m1718636149 (CopyPositionAngleFilter_t2952166253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CopyPositionAngleFilter::doFollow()
extern "C"  void CopyPositionAngleFilter_doFollow_m3444663650 (CopyPositionAngleFilter_t2952166253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion CopyPositionAngleFilter::doCopyRotation(UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  CopyPositionAngleFilter_doCopyRotation_m3272084487 (CopyPositionAngleFilter_t2952166253 * __this, Quaternion_t1553702882  ___rot0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 CopyPositionAngleFilter::doCopyPosition(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  CopyPositionAngleFilter_doCopyPosition_m727871428 (CopyPositionAngleFilter_t2952166253 * __this, Vector3_t4282066566  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
