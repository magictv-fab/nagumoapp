﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Binarizer
struct Binarizer_t1492033400;
// ZXing.LuminanceSource
struct LuminanceSource_t1231523093;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_LuminanceSource1231523093.h"

// System.Void ZXing.Binarizer::.ctor(ZXing.LuminanceSource)
extern "C"  void Binarizer__ctor_m3756677450 (Binarizer_t1492033400 * __this, LuminanceSource_t1231523093 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.LuminanceSource ZXing.Binarizer::get_LuminanceSource()
extern "C"  LuminanceSource_t1231523093 * Binarizer_get_LuminanceSource_m3644050737 (Binarizer_t1492033400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Binarizer::get_Width()
extern "C"  int32_t Binarizer_get_Width_m3030267560 (Binarizer_t1492033400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Binarizer::get_Height()
extern "C"  int32_t Binarizer_get_Height_m4247795495 (Binarizer_t1492033400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
