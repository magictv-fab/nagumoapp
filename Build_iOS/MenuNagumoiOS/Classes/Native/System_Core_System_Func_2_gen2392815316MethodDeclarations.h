﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen184564025MethodDeclarations.h"

// System.Void System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3958203315(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t2392815316 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3944524044_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer>::Invoke(T)
#define Func_2_Invoke_m3874378575(__this, ___arg10, method) ((  Binarizer_t1492033400 * (*) (Func_2_t2392815316 *, LuminanceSource_t1231523093 *, const MethodInfo*))Func_2_Invoke_m1924616534_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m1806422782(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t2392815316 *, LuminanceSource_t1231523093 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m4281703301_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m2003593189(__this, ___result0, method) ((  Binarizer_t1492033400 * (*) (Func_2_t2392815316 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m4118168638_gshared)(__this, ___result0, method)
