﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GameObject>
struct Dictionary_2_t3671945244;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.utils.cron.EasyTimer
struct  EasyTimer_t3067488385  : public MonoBehaviour_t667441552
{
public:

public:
};

struct EasyTimer_t3067488385_StaticFields
{
public:
	// System.Int32 ARM.utils.cron.EasyTimer::_id
	int32_t ____id_2;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GameObject> ARM.utils.cron.EasyTimer::timers
	Dictionary_2_t3671945244 * ___timers_3;

public:
	inline static int32_t get_offset_of__id_2() { return static_cast<int32_t>(offsetof(EasyTimer_t3067488385_StaticFields, ____id_2)); }
	inline int32_t get__id_2() const { return ____id_2; }
	inline int32_t* get_address_of__id_2() { return &____id_2; }
	inline void set__id_2(int32_t value)
	{
		____id_2 = value;
	}

	inline static int32_t get_offset_of_timers_3() { return static_cast<int32_t>(offsetof(EasyTimer_t3067488385_StaticFields, ___timers_3)); }
	inline Dictionary_2_t3671945244 * get_timers_3() const { return ___timers_3; }
	inline Dictionary_2_t3671945244 ** get_address_of_timers_3() { return &___timers_3; }
	inline void set_timers_3(Dictionary_2_t3671945244 * value)
	{
		___timers_3 = value;
		Il2CppCodeGenWriteBarrier(&___timers_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
