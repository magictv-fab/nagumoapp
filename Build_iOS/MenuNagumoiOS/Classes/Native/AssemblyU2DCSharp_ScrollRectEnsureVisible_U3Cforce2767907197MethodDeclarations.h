﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScrollRectEnsureVisible/<forceSnap>c__Iterator16
struct U3CforceSnapU3Ec__Iterator16_t2767907197;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ScrollRectEnsureVisible/<forceSnap>c__Iterator16::.ctor()
extern "C"  void U3CforceSnapU3Ec__Iterator16__ctor_m648952254 (U3CforceSnapU3Ec__Iterator16_t2767907197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScrollRectEnsureVisible/<forceSnap>c__Iterator16::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CforceSnapU3Ec__Iterator16_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2909073630 (U3CforceSnapU3Ec__Iterator16_t2767907197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScrollRectEnsureVisible/<forceSnap>c__Iterator16::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CforceSnapU3Ec__Iterator16_System_Collections_IEnumerator_get_Current_m161509490 (U3CforceSnapU3Ec__Iterator16_t2767907197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ScrollRectEnsureVisible/<forceSnap>c__Iterator16::MoveNext()
extern "C"  bool U3CforceSnapU3Ec__Iterator16_MoveNext_m3554673502 (U3CforceSnapU3Ec__Iterator16_t2767907197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollRectEnsureVisible/<forceSnap>c__Iterator16::Dispose()
extern "C"  void U3CforceSnapU3Ec__Iterator16_Dispose_m771141243 (U3CforceSnapU3Ec__Iterator16_t2767907197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollRectEnsureVisible/<forceSnap>c__Iterator16::Reset()
extern "C"  void U3CforceSnapU3Ec__Iterator16_Reset_m2590352491 (U3CforceSnapU3Ec__Iterator16_t2767907197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
