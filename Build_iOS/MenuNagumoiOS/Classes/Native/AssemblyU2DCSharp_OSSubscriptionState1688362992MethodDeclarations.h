﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OSSubscriptionState
struct OSSubscriptionState_t1688362992;

#include "codegen/il2cpp-codegen.h"

// System.Void OSSubscriptionState::.ctor()
extern "C"  void OSSubscriptionState__ctor_m2576392571 (OSSubscriptionState_t1688362992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
