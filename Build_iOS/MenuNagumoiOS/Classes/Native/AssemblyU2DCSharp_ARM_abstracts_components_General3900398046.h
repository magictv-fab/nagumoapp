﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OnEvent
struct OnEvent_t314892251;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.abstracts.components.GeneralComponentsAbstract
struct  GeneralComponentsAbstract_t3900398046  : public MonoBehaviour_t667441552
{
public:
	// OnEvent ARM.abstracts.components.GeneralComponentsAbstract::onCompoentIsReady
	OnEvent_t314892251 * ___onCompoentIsReady_2;
	// System.Boolean ARM.abstracts.components.GeneralComponentsAbstract::_isComponentReady
	bool ____isComponentReady_3;

public:
	inline static int32_t get_offset_of_onCompoentIsReady_2() { return static_cast<int32_t>(offsetof(GeneralComponentsAbstract_t3900398046, ___onCompoentIsReady_2)); }
	inline OnEvent_t314892251 * get_onCompoentIsReady_2() const { return ___onCompoentIsReady_2; }
	inline OnEvent_t314892251 ** get_address_of_onCompoentIsReady_2() { return &___onCompoentIsReady_2; }
	inline void set_onCompoentIsReady_2(OnEvent_t314892251 * value)
	{
		___onCompoentIsReady_2 = value;
		Il2CppCodeGenWriteBarrier(&___onCompoentIsReady_2, value);
	}

	inline static int32_t get_offset_of__isComponentReady_3() { return static_cast<int32_t>(offsetof(GeneralComponentsAbstract_t3900398046, ____isComponentReady_3)); }
	inline bool get__isComponentReady_3() const { return ____isComponentReady_3; }
	inline bool* get_address_of__isComponentReady_3() { return &____isComponentReady_3; }
	inline void set__isComponentReady_3(bool value)
	{
		____isComponentReady_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
