﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.QrCode.Internal.DataMask/DataMask011
struct DataMask011_t1996578332;

#include "codegen/il2cpp-codegen.h"

// System.Boolean ZXing.QrCode.Internal.DataMask/DataMask011::isMasked(System.Int32,System.Int32)
extern "C"  bool DataMask011_isMasked_m1184492600 (DataMask011_t1996578332 * __this, int32_t ___i0, int32_t ___j1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.DataMask/DataMask011::.ctor()
extern "C"  void DataMask011__ctor_m1092761247 (DataMask011_t1996578332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
