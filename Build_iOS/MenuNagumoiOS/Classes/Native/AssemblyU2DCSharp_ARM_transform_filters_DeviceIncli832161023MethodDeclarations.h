﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.transform.filters.DeviceInclinationCameraFilter
struct DeviceInclinationCameraFilter_t832161023;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "mscorlib_System_String7231557.h"

// System.Void ARM.transform.filters.DeviceInclinationCameraFilter::.ctor()
extern "C"  void DeviceInclinationCameraFilter__ctor_m3973076841 (DeviceInclinationCameraFilter_t832161023 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.DeviceInclinationCameraFilter::Start()
extern "C"  void DeviceInclinationCameraFilter_Start_m2920214633 (DeviceInclinationCameraFilter_t832161023 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.transform.filters.DeviceInclinationCameraFilter::MovAverage(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  DeviceInclinationCameraFilter_MovAverage_m1802217071 (DeviceInclinationCameraFilter_t832161023 * __this, Vector3_t4282066566  ___sample0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.transform.filters.DeviceInclinationCameraFilter::_doFilter(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  DeviceInclinationCameraFilter__doFilter_m1658979753 (DeviceInclinationCameraFilter_t832161023 * __this, Vector3_t4282066566  ___currentValue0, Vector3_t4282066566  ___nextValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion ARM.transform.filters.DeviceInclinationCameraFilter::_doFilter(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  DeviceInclinationCameraFilter__doFilter_m1600976341 (DeviceInclinationCameraFilter_t832161023 * __this, Quaternion_t1553702882  ___last0, Quaternion_t1553702882  ___nextValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.DeviceInclinationCameraFilter::configByString(System.String)
extern "C"  void DeviceInclinationCameraFilter_configByString_m340072669 (DeviceInclinationCameraFilter_t832161023 * __this, String_t* ___metadata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.DeviceInclinationCameraFilter::Reset()
extern "C"  void DeviceInclinationCameraFilter_Reset_m1619509782 (DeviceInclinationCameraFilter_t832161023 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.DeviceInclinationCameraFilter::OnGUI()
extern "C"  void DeviceInclinationCameraFilter_OnGUI_m3468475491 (DeviceInclinationCameraFilter_t832161023 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
