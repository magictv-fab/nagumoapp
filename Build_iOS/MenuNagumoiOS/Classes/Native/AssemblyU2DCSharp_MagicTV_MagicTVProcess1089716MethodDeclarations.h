﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.MagicTVProcess
struct MagicTVProcess_t1089716;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_MNDialogResult3125317574.h"

// System.Void MagicTV.MagicTVProcess::.ctor()
extern "C"  void MagicTVProcess__ctor_m3653157340 (MagicTVProcess_t1089716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.MagicTVProcess::Awake()
extern "C"  void MagicTVProcess_Awake_m3890762559 (MagicTVProcess_t1089716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.MagicTVProcess::checkTrackoutDebug(System.String)
extern "C"  void MagicTVProcess_checkTrackoutDebug_m2440126898 (MagicTVProcess_t1089716 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.MagicTVProcess::checkTrackinDebug(System.String)
extern "C"  void MagicTVProcess_checkTrackinDebug_m2383569565 (MagicTVProcess_t1089716 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.MagicTVProcess::DebugClearCache()
extern "C"  void MagicTVProcess_DebugClearCache_m731660706 (MagicTVProcess_t1089716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.MagicTVProcess::OnGUI()
extern "C"  void MagicTVProcess_OnGUI_m3148555990 (MagicTVProcess_t1089716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.MagicTVProcess::ClearCacheProcess()
extern "C"  void MagicTVProcess_ClearCacheProcess_m2990535284 (MagicTVProcess_t1089716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.MagicTVProcess::OnDialogCloseCache(MNDialogResult)
extern "C"  void MagicTVProcess_OnDialogCloseCache_m2754227251 (MagicTVProcess_t1089716 * __this, int32_t ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.MagicTVProcess::nothingToClear()
extern "C"  void MagicTVProcess_nothingToClear_m2164729293 (MagicTVProcess_t1089716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.MagicTVProcess::clearCacheCompleted()
extern "C"  void MagicTVProcess_clearCacheCompleted_m3240841840 (MagicTVProcess_t1089716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.MagicTVProcess::ResetProcess()
extern "C"  void MagicTVProcess_ResetProcess_m1970352904 (MagicTVProcess_t1089716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.MagicTVProcess::OnDialogClose(MNDialogResult)
extern "C"  void MagicTVProcess_OnDialogClose_m2454161765 (MagicTVProcess_t1089716 * __this, int32_t ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.MagicTVProcess::doResetAll()
extern "C"  void MagicTVProcess_doResetAll_m1073699525 (MagicTVProcess_t1089716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.MagicTVProcess::doRestartLevel(System.Boolean)
extern "C"  void MagicTVProcess_doRestartLevel_m811283807 (MagicTVProcess_t1089716 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.MagicTVProcess::OnVuforiaStarted()
extern "C"  void MagicTVProcess_OnVuforiaStarted_m2747917190 (MagicTVProcess_t1089716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.MagicTVProcess::prepare()
extern "C"  void MagicTVProcess_prepare_m2622903329 (MagicTVProcess_t1089716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.MagicTVProcess::Play()
extern "C"  void MagicTVProcess_Play_m2345913884 (MagicTVProcess_t1089716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.MagicTVProcess::initProcessComplete()
extern "C"  void MagicTVProcess_initProcessComplete_m2764951282 (MagicTVProcess_t1089716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.MagicTVProcess::updateProcessStepComplete()
extern "C"  void MagicTVProcess_updateProcessStepComplete_m3786620549 (MagicTVProcess_t1089716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.MagicTVProcess::magicPresentationIsDone()
extern "C"  void MagicTVProcess_magicPresentationIsDone_m1071367725 (MagicTVProcess_t1089716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.MagicTVProcess::Stop()
extern "C"  void MagicTVProcess_Stop_m2439597930 (MagicTVProcess_t1089716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.MagicTVProcess::Pause()
extern "C"  void MagicTVProcess_Pause_m3707283312 (MagicTVProcess_t1089716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
