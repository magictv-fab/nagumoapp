﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.processes.UpdateProcessComponent
struct UpdateProcessComponent_t50846353;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.processes.UpdateProcessComponent::.ctor()
extern "C"  void UpdateProcessComponent__ctor_m1943760180 (UpdateProcessComponent_t50846353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.processes.UpdateProcessComponent::prepare()
extern "C"  void UpdateProcessComponent_prepare_m569739641 (UpdateProcessComponent_t50846353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.processes.UpdateProcessComponent::Play()
extern "C"  void UpdateProcessComponent_Play_m2152224708 (UpdateProcessComponent_t50846353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.processes.UpdateProcessComponent::downloadCompleted()
extern "C"  void UpdateProcessComponent_downloadCompleted_m2613240533 (UpdateProcessComponent_t50846353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.processes.UpdateProcessComponent::Stop()
extern "C"  void UpdateProcessComponent_Stop_m2245908754 (UpdateProcessComponent_t50846353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.processes.UpdateProcessComponent::Pause()
extern "C"  void UpdateProcessComponent_Pause_m1997886152 (UpdateProcessComponent_t50846353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
