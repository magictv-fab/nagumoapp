﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.BarcodeReaderGeneric`1<System.Object>
struct BarcodeReaderGeneric_1_t3481712763;
// ZXing.Reader
struct Reader_t2610170425;
// System.Func`4<System.Object,System.Int32,System.Int32,ZXing.LuminanceSource>
struct Func_4_t3774219921;
// System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer>
struct Func_2_t2392815316;
// System.Func`5<System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat,ZXing.LuminanceSource>
struct Func_5_t533482363;
// ZXing.Result
struct Result_t2610723219;
// ZXing.LuminanceSource
struct LuminanceSource_t1231523093;
// System.Object
struct Il2CppObject;
// ZXing.Common.DecodingOptions
struct DecodingOptions_t3608870897;
// ZXing.Binarizer
struct Binarizer_t1492033400;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_LuminanceSource1231523093.h"
#include "mscorlib_System_Object4170816371.h"
#include "QRCode_ZXing_Result2610723219.h"
#include "QRCode_ZXing_RGBLuminanceSource_BitmapFormat3665922245.h"

// System.Void ZXing.BarcodeReaderGeneric`1<System.Object>::.cctor()
extern "C"  void BarcodeReaderGeneric_1__cctor_m4288126543_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define BarcodeReaderGeneric_1__cctor_m4288126543(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))BarcodeReaderGeneric_1__cctor_m4288126543_gshared)(__this /* static, unused */, method)
// System.Void ZXing.BarcodeReaderGeneric`1<System.Object>::.ctor(ZXing.Reader,System.Func`4<T,System.Int32,System.Int32,ZXing.LuminanceSource>,System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer>)
extern "C"  void BarcodeReaderGeneric_1__ctor_m3825891079_gshared (BarcodeReaderGeneric_1_t3481712763 * __this, Il2CppObject * ___reader0, Func_4_t3774219921 * ___createLuminanceSource1, Func_2_t2392815316 * ___createBinarizer2, const MethodInfo* method);
#define BarcodeReaderGeneric_1__ctor_m3825891079(__this, ___reader0, ___createLuminanceSource1, ___createBinarizer2, method) ((  void (*) (BarcodeReaderGeneric_1_t3481712763 *, Il2CppObject *, Func_4_t3774219921 *, Func_2_t2392815316 *, const MethodInfo*))BarcodeReaderGeneric_1__ctor_m3825891079_gshared)(__this, ___reader0, ___createLuminanceSource1, ___createBinarizer2, method)
// System.Void ZXing.BarcodeReaderGeneric`1<System.Object>::.ctor(ZXing.Reader,System.Func`4<T,System.Int32,System.Int32,ZXing.LuminanceSource>,System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer>,System.Func`5<System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat,ZXing.LuminanceSource>)
extern "C"  void BarcodeReaderGeneric_1__ctor_m2970014716_gshared (BarcodeReaderGeneric_1_t3481712763 * __this, Il2CppObject * ___reader0, Func_4_t3774219921 * ___createLuminanceSource1, Func_2_t2392815316 * ___createBinarizer2, Func_5_t533482363 * ___createRGBLuminanceSource3, const MethodInfo* method);
#define BarcodeReaderGeneric_1__ctor_m2970014716(__this, ___reader0, ___createLuminanceSource1, ___createBinarizer2, ___createRGBLuminanceSource3, method) ((  void (*) (BarcodeReaderGeneric_1_t3481712763 *, Il2CppObject *, Func_4_t3774219921 *, Func_2_t2392815316 *, Func_5_t533482363 *, const MethodInfo*))BarcodeReaderGeneric_1__ctor_m2970014716_gshared)(__this, ___reader0, ___createLuminanceSource1, ___createBinarizer2, ___createRGBLuminanceSource3, method)
// ZXing.Result ZXing.BarcodeReaderGeneric`1<System.Object>::Decode(ZXing.LuminanceSource)
extern "C"  Result_t2610723219 * BarcodeReaderGeneric_1_Decode_m1780868875_gshared (BarcodeReaderGeneric_1_t3481712763 * __this, LuminanceSource_t1231523093 * ___luminanceSource0, const MethodInfo* method);
#define BarcodeReaderGeneric_1_Decode_m1780868875(__this, ___luminanceSource0, method) ((  Result_t2610723219 * (*) (BarcodeReaderGeneric_1_t3481712763 *, LuminanceSource_t1231523093 *, const MethodInfo*))BarcodeReaderGeneric_1_Decode_m1780868875_gshared)(__this, ___luminanceSource0, method)
// ZXing.Result ZXing.BarcodeReaderGeneric`1<System.Object>::Decode(T,System.Int32,System.Int32)
extern "C"  Result_t2610723219 * BarcodeReaderGeneric_1_Decode_m693721600_gshared (BarcodeReaderGeneric_1_t3481712763 * __this, Il2CppObject * ___rawRGB0, int32_t ___width1, int32_t ___height2, const MethodInfo* method);
#define BarcodeReaderGeneric_1_Decode_m693721600(__this, ___rawRGB0, ___width1, ___height2, method) ((  Result_t2610723219 * (*) (BarcodeReaderGeneric_1_t3481712763 *, Il2CppObject *, int32_t, int32_t, const MethodInfo*))BarcodeReaderGeneric_1_Decode_m693721600_gshared)(__this, ___rawRGB0, ___width1, ___height2, method)
// System.Void ZXing.BarcodeReaderGeneric`1<System.Object>::OnResultFound(ZXing.Result)
extern "C"  void BarcodeReaderGeneric_1_OnResultFound_m1942802843_gshared (BarcodeReaderGeneric_1_t3481712763 * __this, Result_t2610723219 * ___result0, const MethodInfo* method);
#define BarcodeReaderGeneric_1_OnResultFound_m1942802843(__this, ___result0, method) ((  void (*) (BarcodeReaderGeneric_1_t3481712763 *, Result_t2610723219 *, const MethodInfo*))BarcodeReaderGeneric_1_OnResultFound_m1942802843_gshared)(__this, ___result0, method)
// System.Boolean ZXing.BarcodeReaderGeneric`1<System.Object>::get_AutoRotate()
extern "C"  bool BarcodeReaderGeneric_1_get_AutoRotate_m3605742181_gshared (BarcodeReaderGeneric_1_t3481712763 * __this, const MethodInfo* method);
#define BarcodeReaderGeneric_1_get_AutoRotate_m3605742181(__this, method) ((  bool (*) (BarcodeReaderGeneric_1_t3481712763 *, const MethodInfo*))BarcodeReaderGeneric_1_get_AutoRotate_m3605742181_gshared)(__this, method)
// System.Void ZXing.BarcodeReaderGeneric`1<System.Object>::set_AutoRotate(System.Boolean)
extern "C"  void BarcodeReaderGeneric_1_set_AutoRotate_m3182146308_gshared (BarcodeReaderGeneric_1_t3481712763 * __this, bool ___value0, const MethodInfo* method);
#define BarcodeReaderGeneric_1_set_AutoRotate_m3182146308(__this, ___value0, method) ((  void (*) (BarcodeReaderGeneric_1_t3481712763 *, bool, const MethodInfo*))BarcodeReaderGeneric_1_set_AutoRotate_m3182146308_gshared)(__this, ___value0, method)
// System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer> ZXing.BarcodeReaderGeneric`1<System.Object>::get_CreateBinarizer()
extern "C"  Func_2_t2392815316 * BarcodeReaderGeneric_1_get_CreateBinarizer_m3095086350_gshared (BarcodeReaderGeneric_1_t3481712763 * __this, const MethodInfo* method);
#define BarcodeReaderGeneric_1_get_CreateBinarizer_m3095086350(__this, method) ((  Func_2_t2392815316 * (*) (BarcodeReaderGeneric_1_t3481712763 *, const MethodInfo*))BarcodeReaderGeneric_1_get_CreateBinarizer_m3095086350_gshared)(__this, method)
// System.Func`4<T,System.Int32,System.Int32,ZXing.LuminanceSource> ZXing.BarcodeReaderGeneric`1<System.Object>::get_CreateLuminanceSource()
extern "C"  Func_4_t3774219921 * BarcodeReaderGeneric_1_get_CreateLuminanceSource_m3272449141_gshared (BarcodeReaderGeneric_1_t3481712763 * __this, const MethodInfo* method);
#define BarcodeReaderGeneric_1_get_CreateLuminanceSource_m3272449141(__this, method) ((  Func_4_t3774219921 * (*) (BarcodeReaderGeneric_1_t3481712763 *, const MethodInfo*))BarcodeReaderGeneric_1_get_CreateLuminanceSource_m3272449141_gshared)(__this, method)
// ZXing.Common.DecodingOptions ZXing.BarcodeReaderGeneric`1<System.Object>::get_Options()
extern "C"  DecodingOptions_t3608870897 * BarcodeReaderGeneric_1_get_Options_m4237311540_gshared (BarcodeReaderGeneric_1_t3481712763 * __this, const MethodInfo* method);
#define BarcodeReaderGeneric_1_get_Options_m4237311540(__this, method) ((  DecodingOptions_t3608870897 * (*) (BarcodeReaderGeneric_1_t3481712763 *, const MethodInfo*))BarcodeReaderGeneric_1_get_Options_m4237311540_gshared)(__this, method)
// ZXing.Reader ZXing.BarcodeReaderGeneric`1<System.Object>::get_Reader()
extern "C"  Il2CppObject * BarcodeReaderGeneric_1_get_Reader_m3072341634_gshared (BarcodeReaderGeneric_1_t3481712763 * __this, const MethodInfo* method);
#define BarcodeReaderGeneric_1_get_Reader_m3072341634(__this, method) ((  Il2CppObject * (*) (BarcodeReaderGeneric_1_t3481712763 *, const MethodInfo*))BarcodeReaderGeneric_1_get_Reader_m3072341634_gshared)(__this, method)
// System.Boolean ZXing.BarcodeReaderGeneric`1<System.Object>::get_TryInverted()
extern "C"  bool BarcodeReaderGeneric_1_get_TryInverted_m1524396567_gshared (BarcodeReaderGeneric_1_t3481712763 * __this, const MethodInfo* method);
#define BarcodeReaderGeneric_1_get_TryInverted_m1524396567(__this, method) ((  bool (*) (BarcodeReaderGeneric_1_t3481712763 *, const MethodInfo*))BarcodeReaderGeneric_1_get_TryInverted_m1524396567_gshared)(__this, method)
// System.Void ZXing.BarcodeReaderGeneric`1<System.Object>::set_TryInverted(System.Boolean)
extern "C"  void BarcodeReaderGeneric_1_set_TryInverted_m4063223910_gshared (BarcodeReaderGeneric_1_t3481712763 * __this, bool ___value0, const MethodInfo* method);
#define BarcodeReaderGeneric_1_set_TryInverted_m4063223910(__this, ___value0, method) ((  void (*) (BarcodeReaderGeneric_1_t3481712763 *, bool, const MethodInfo*))BarcodeReaderGeneric_1_set_TryInverted_m4063223910_gshared)(__this, ___value0, method)
// ZXing.Binarizer ZXing.BarcodeReaderGeneric`1<System.Object>::<.cctor>b__0(ZXing.LuminanceSource)
extern "C"  Binarizer_t1492033400 * BarcodeReaderGeneric_1_U3C_cctorU3Eb__0_m2217776963_gshared (Il2CppObject * __this /* static, unused */, LuminanceSource_t1231523093 * ___luminanceSource0, const MethodInfo* method);
#define BarcodeReaderGeneric_1_U3C_cctorU3Eb__0_m2217776963(__this /* static, unused */, ___luminanceSource0, method) ((  Binarizer_t1492033400 * (*) (Il2CppObject * /* static, unused */, LuminanceSource_t1231523093 *, const MethodInfo*))BarcodeReaderGeneric_1_U3C_cctorU3Eb__0_m2217776963_gshared)(__this /* static, unused */, ___luminanceSource0, method)
// ZXing.LuminanceSource ZXing.BarcodeReaderGeneric`1<System.Object>::<.cctor>b__1(System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat)
extern "C"  LuminanceSource_t1231523093 * BarcodeReaderGeneric_1_U3C_cctorU3Eb__1_m4225005580_gshared (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___rawBytes0, int32_t ___width1, int32_t ___height2, int32_t ___format3, const MethodInfo* method);
#define BarcodeReaderGeneric_1_U3C_cctorU3Eb__1_m4225005580(__this /* static, unused */, ___rawBytes0, ___width1, ___height2, ___format3, method) ((  LuminanceSource_t1231523093 * (*) (Il2CppObject * /* static, unused */, ByteU5BU5D_t4260760469*, int32_t, int32_t, int32_t, const MethodInfo*))BarcodeReaderGeneric_1_U3C_cctorU3Eb__1_m4225005580_gshared)(__this /* static, unused */, ___rawBytes0, ___width1, ___height2, ___format3, method)
