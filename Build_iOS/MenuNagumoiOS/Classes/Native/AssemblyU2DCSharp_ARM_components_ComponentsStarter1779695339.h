﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<ARM.abstracts.components.GeneralComponentsAbstract>
struct List_1_t973616302;
// ARM.abstracts.components.GeneralComponentsAbstract[]
struct GeneralComponentsAbstractU5BU5D_t4214250731;

#include "AssemblyU2DCSharp_ARM_abstracts_components_General3900398046.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.components.ComponentsStarter
struct  ComponentsStarter_t1779695339  : public GeneralComponentsAbstract_t3900398046
{
public:
	// System.Collections.Generic.List`1<ARM.abstracts.components.GeneralComponentsAbstract> ARM.components.ComponentsStarter::componentsToInit
	List_1_t973616302 * ___componentsToInit_4;
	// ARM.abstracts.components.GeneralComponentsAbstract[] ARM.components.ComponentsStarter::components
	GeneralComponentsAbstractU5BU5D_t4214250731* ___components_5;

public:
	inline static int32_t get_offset_of_componentsToInit_4() { return static_cast<int32_t>(offsetof(ComponentsStarter_t1779695339, ___componentsToInit_4)); }
	inline List_1_t973616302 * get_componentsToInit_4() const { return ___componentsToInit_4; }
	inline List_1_t973616302 ** get_address_of_componentsToInit_4() { return &___componentsToInit_4; }
	inline void set_componentsToInit_4(List_1_t973616302 * value)
	{
		___componentsToInit_4 = value;
		Il2CppCodeGenWriteBarrier(&___componentsToInit_4, value);
	}

	inline static int32_t get_offset_of_components_5() { return static_cast<int32_t>(offsetof(ComponentsStarter_t1779695339, ___components_5)); }
	inline GeneralComponentsAbstractU5BU5D_t4214250731* get_components_5() const { return ___components_5; }
	inline GeneralComponentsAbstractU5BU5D_t4214250731** get_address_of_components_5() { return &___components_5; }
	inline void set_components_5(GeneralComponentsAbstractU5BU5D_t4214250731* value)
	{
		___components_5 = value;
		Il2CppCodeGenWriteBarrier(&___components_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
