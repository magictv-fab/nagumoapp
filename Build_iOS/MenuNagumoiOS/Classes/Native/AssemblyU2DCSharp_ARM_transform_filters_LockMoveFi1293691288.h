﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ARM_transform_filters_abstracts_4158746314.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.transform.filters.LockMoveFilter
struct  LockMoveFilter_t1293691288  : public TransformPositionAngleFilterAbstract_t4158746314
{
public:
	// System.Boolean ARM.transform.filters.LockMoveFilter::_lastAngleHasValue
	bool ____lastAngleHasValue_15;
	// UnityEngine.Quaternion ARM.transform.filters.LockMoveFilter::_lastAngleValue
	Quaternion_t1553702882  ____lastAngleValue_16;
	// System.Boolean ARM.transform.filters.LockMoveFilter::_lastPositionHasValue
	bool ____lastPositionHasValue_17;
	// UnityEngine.Vector3 ARM.transform.filters.LockMoveFilter::_lastPositionValue
	Vector3_t4282066566  ____lastPositionValue_18;
	// System.Boolean ARM.transform.filters.LockMoveFilter::isWorking
	bool ___isWorking_19;

public:
	inline static int32_t get_offset_of__lastAngleHasValue_15() { return static_cast<int32_t>(offsetof(LockMoveFilter_t1293691288, ____lastAngleHasValue_15)); }
	inline bool get__lastAngleHasValue_15() const { return ____lastAngleHasValue_15; }
	inline bool* get_address_of__lastAngleHasValue_15() { return &____lastAngleHasValue_15; }
	inline void set__lastAngleHasValue_15(bool value)
	{
		____lastAngleHasValue_15 = value;
	}

	inline static int32_t get_offset_of__lastAngleValue_16() { return static_cast<int32_t>(offsetof(LockMoveFilter_t1293691288, ____lastAngleValue_16)); }
	inline Quaternion_t1553702882  get__lastAngleValue_16() const { return ____lastAngleValue_16; }
	inline Quaternion_t1553702882 * get_address_of__lastAngleValue_16() { return &____lastAngleValue_16; }
	inline void set__lastAngleValue_16(Quaternion_t1553702882  value)
	{
		____lastAngleValue_16 = value;
	}

	inline static int32_t get_offset_of__lastPositionHasValue_17() { return static_cast<int32_t>(offsetof(LockMoveFilter_t1293691288, ____lastPositionHasValue_17)); }
	inline bool get__lastPositionHasValue_17() const { return ____lastPositionHasValue_17; }
	inline bool* get_address_of__lastPositionHasValue_17() { return &____lastPositionHasValue_17; }
	inline void set__lastPositionHasValue_17(bool value)
	{
		____lastPositionHasValue_17 = value;
	}

	inline static int32_t get_offset_of__lastPositionValue_18() { return static_cast<int32_t>(offsetof(LockMoveFilter_t1293691288, ____lastPositionValue_18)); }
	inline Vector3_t4282066566  get__lastPositionValue_18() const { return ____lastPositionValue_18; }
	inline Vector3_t4282066566 * get_address_of__lastPositionValue_18() { return &____lastPositionValue_18; }
	inline void set__lastPositionValue_18(Vector3_t4282066566  value)
	{
		____lastPositionValue_18 = value;
	}

	inline static int32_t get_offset_of_isWorking_19() { return static_cast<int32_t>(offsetof(LockMoveFilter_t1293691288, ___isWorking_19)); }
	inline bool get_isWorking_19() const { return ___isWorking_19; }
	inline bool* get_address_of_isWorking_19() { return &___isWorking_19; }
	inline void set_isWorking_19(bool value)
	{
		___isWorking_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
