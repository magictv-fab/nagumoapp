﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LightsControl/<ITurn>c__Iterator5E
struct U3CITurnU3Ec__Iterator5E_t1739934332;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void LightsControl/<ITurn>c__Iterator5E::.ctor()
extern "C"  void U3CITurnU3Ec__Iterator5E__ctor_m877891999 (U3CITurnU3Ec__Iterator5E_t1739934332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LightsControl/<ITurn>c__Iterator5E::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CITurnU3Ec__Iterator5E_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1174938717 (U3CITurnU3Ec__Iterator5E_t1739934332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LightsControl/<ITurn>c__Iterator5E::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CITurnU3Ec__Iterator5E_System_Collections_IEnumerator_get_Current_m2520809969 (U3CITurnU3Ec__Iterator5E_t1739934332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LightsControl/<ITurn>c__Iterator5E::MoveNext()
extern "C"  bool U3CITurnU3Ec__Iterator5E_MoveNext_m1028923101 (U3CITurnU3Ec__Iterator5E_t1739934332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightsControl/<ITurn>c__Iterator5E::Dispose()
extern "C"  void U3CITurnU3Ec__Iterator5E_Dispose_m1738904092 (U3CITurnU3Ec__Iterator5E_t1739934332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightsControl/<ITurn>c__Iterator5E::Reset()
extern "C"  void U3CITurnU3Ec__Iterator5E_Reset_m2819292236 (U3CITurnU3Ec__Iterator5E_t1739934332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
