﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ConvertFloatToInt
struct ConvertFloatToInt_t1062086873;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ConvertFloatToInt::.ctor()
extern "C"  void ConvertFloatToInt__ctor_m2437634173 (ConvertFloatToInt_t1062086873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertFloatToInt::Reset()
extern "C"  void ConvertFloatToInt_Reset_m84067114 (ConvertFloatToInt_t1062086873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertFloatToInt::OnEnter()
extern "C"  void ConvertFloatToInt_OnEnter_m1200992532 (ConvertFloatToInt_t1062086873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertFloatToInt::OnUpdate()
extern "C"  void ConvertFloatToInt_OnUpdate_m2004589487 (ConvertFloatToInt_t1062086873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertFloatToInt::DoConvertFloatToInt()
extern "C"  void ConvertFloatToInt_DoConvertFloatToInt_m892816219 (ConvertFloatToInt_t1062086873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
