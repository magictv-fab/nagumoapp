﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.StaticDiskDataSource
struct  StaticDiskDataSource_t3533306910  : public Il2CppObject
{
public:
	// System.String ICSharpCode.SharpZipLib.Zip.StaticDiskDataSource::fileName_
	String_t* ___fileName__0;

public:
	inline static int32_t get_offset_of_fileName__0() { return static_cast<int32_t>(offsetof(StaticDiskDataSource_t3533306910, ___fileName__0)); }
	inline String_t* get_fileName__0() const { return ___fileName__0; }
	inline String_t** get_address_of_fileName__0() { return &___fileName__0; }
	inline void set_fileName__0(String_t* value)
	{
		___fileName__0 = value;
		Il2CppCodeGenWriteBarrier(&___fileName__0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
