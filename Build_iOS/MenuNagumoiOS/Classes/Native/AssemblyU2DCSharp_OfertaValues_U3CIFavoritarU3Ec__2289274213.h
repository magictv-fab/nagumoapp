﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// UnityEngine.WWW
struct WWW_t3134621005;
// System.Object
struct Il2CppObject;
// OfertaValues
struct OfertaValues_t3488850643;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OfertaValues/<IFavoritar>c__Iterator23
struct  U3CIFavoritarU3Ec__Iterator23_t2289274213  : public Il2CppObject
{
public:
	// System.String OfertaValues/<IFavoritar>c__Iterator23::id
	String_t* ___id_0;
	// System.String OfertaValues/<IFavoritar>c__Iterator23::<json>__0
	String_t* ___U3CjsonU3E__0_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> OfertaValues/<IFavoritar>c__Iterator23::<headers>__1
	Dictionary_2_t827649927 * ___U3CheadersU3E__1_2;
	// System.Byte[] OfertaValues/<IFavoritar>c__Iterator23::<pData>__2
	ByteU5BU5D_t4260760469* ___U3CpDataU3E__2_3;
	// UnityEngine.WWW OfertaValues/<IFavoritar>c__Iterator23::<www>__3
	WWW_t3134621005 * ___U3CwwwU3E__3_4;
	// System.Int32 OfertaValues/<IFavoritar>c__Iterator23::$PC
	int32_t ___U24PC_5;
	// System.Object OfertaValues/<IFavoritar>c__Iterator23::$current
	Il2CppObject * ___U24current_6;
	// System.String OfertaValues/<IFavoritar>c__Iterator23::<$>id
	String_t* ___U3CU24U3Eid_7;
	// OfertaValues OfertaValues/<IFavoritar>c__Iterator23::<>f__this
	OfertaValues_t3488850643 * ___U3CU3Ef__this_8;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CIFavoritarU3Ec__Iterator23_t2289274213, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier(&___id_0, value);
	}

	inline static int32_t get_offset_of_U3CjsonU3E__0_1() { return static_cast<int32_t>(offsetof(U3CIFavoritarU3Ec__Iterator23_t2289274213, ___U3CjsonU3E__0_1)); }
	inline String_t* get_U3CjsonU3E__0_1() const { return ___U3CjsonU3E__0_1; }
	inline String_t** get_address_of_U3CjsonU3E__0_1() { return &___U3CjsonU3E__0_1; }
	inline void set_U3CjsonU3E__0_1(String_t* value)
	{
		___U3CjsonU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjsonU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CheadersU3E__1_2() { return static_cast<int32_t>(offsetof(U3CIFavoritarU3Ec__Iterator23_t2289274213, ___U3CheadersU3E__1_2)); }
	inline Dictionary_2_t827649927 * get_U3CheadersU3E__1_2() const { return ___U3CheadersU3E__1_2; }
	inline Dictionary_2_t827649927 ** get_address_of_U3CheadersU3E__1_2() { return &___U3CheadersU3E__1_2; }
	inline void set_U3CheadersU3E__1_2(Dictionary_2_t827649927 * value)
	{
		___U3CheadersU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CheadersU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U3CpDataU3E__2_3() { return static_cast<int32_t>(offsetof(U3CIFavoritarU3Ec__Iterator23_t2289274213, ___U3CpDataU3E__2_3)); }
	inline ByteU5BU5D_t4260760469* get_U3CpDataU3E__2_3() const { return ___U3CpDataU3E__2_3; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CpDataU3E__2_3() { return &___U3CpDataU3E__2_3; }
	inline void set_U3CpDataU3E__2_3(ByteU5BU5D_t4260760469* value)
	{
		___U3CpDataU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpDataU3E__2_3, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__3_4() { return static_cast<int32_t>(offsetof(U3CIFavoritarU3Ec__Iterator23_t2289274213, ___U3CwwwU3E__3_4)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__3_4() const { return ___U3CwwwU3E__3_4; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__3_4() { return &___U3CwwwU3E__3_4; }
	inline void set_U3CwwwU3E__3_4(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__3_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__3_4, value);
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CIFavoritarU3Ec__Iterator23_t2289274213, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CIFavoritarU3Ec__Iterator23_t2289274213, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eid_7() { return static_cast<int32_t>(offsetof(U3CIFavoritarU3Ec__Iterator23_t2289274213, ___U3CU24U3Eid_7)); }
	inline String_t* get_U3CU24U3Eid_7() const { return ___U3CU24U3Eid_7; }
	inline String_t** get_address_of_U3CU24U3Eid_7() { return &___U3CU24U3Eid_7; }
	inline void set_U3CU24U3Eid_7(String_t* value)
	{
		___U3CU24U3Eid_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eid_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_8() { return static_cast<int32_t>(offsetof(U3CIFavoritarU3Ec__Iterator23_t2289274213, ___U3CU3Ef__this_8)); }
	inline OfertaValues_t3488850643 * get_U3CU3Ef__this_8() const { return ___U3CU3Ef__this_8; }
	inline OfertaValues_t3488850643 ** get_address_of_U3CU3Ef__this_8() { return &___U3CU3Ef__this_8; }
	inline void set_U3CU3Ef__this_8(OfertaValues_t3488850643 * value)
	{
		___U3CU3Ef__this_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
