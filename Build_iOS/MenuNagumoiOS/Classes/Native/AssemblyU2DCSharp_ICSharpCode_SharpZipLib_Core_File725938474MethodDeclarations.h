﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Core.FileFailureHandler
struct FileFailureHandler_t725938474;
// System.Object
struct Il2CppObject;
// ICSharpCode.SharpZipLib.Core.ScanFailureEventArgs
struct ScanFailureEventArgs_t2731106168;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Core_Sca2731106168.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ICSharpCode.SharpZipLib.Core.FileFailureHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void FileFailureHandler__ctor_m2894590001 (FileFailureHandler_t725938474 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.FileFailureHandler::Invoke(System.Object,ICSharpCode.SharpZipLib.Core.ScanFailureEventArgs)
extern "C"  void FileFailureHandler_Invoke_m2438505517 (FileFailureHandler_t725938474 * __this, Il2CppObject * ___sender0, ScanFailureEventArgs_t2731106168 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ICSharpCode.SharpZipLib.Core.FileFailureHandler::BeginInvoke(System.Object,ICSharpCode.SharpZipLib.Core.ScanFailureEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * FileFailureHandler_BeginInvoke_m3420380174 (FileFailureHandler_t725938474 * __this, Il2CppObject * ___sender0, ScanFailureEventArgs_t2731106168 * ___e1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.FileFailureHandler::EndInvoke(System.IAsyncResult)
extern "C"  void FileFailureHandler_EndInvoke_m1400185537 (FileFailureHandler_t725938474 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
