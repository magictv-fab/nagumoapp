﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t3191267369;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Camera
struct Camera_t2727095145;
// Vuforia.VuforiaAbstractBehaviour
struct VuforiaAbstractBehaviour_t1091759131;
// Vuforia.HideExcessAreaAbstractBehaviour[]
struct HideExcessAreaAbstractBehaviourU5BU5D_t3294375964;
// System.Collections.Generic.List`1<Vuforia.HideExcessAreaAbstractBehaviour>
struct List_1_t1833232017;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.HideExcessAreaAbstractBehaviour
struct  HideExcessAreaAbstractBehaviour_t465046465  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Shader Vuforia.HideExcessAreaAbstractBehaviour::matteShader
	Shader_t3191267369 * ___matteShader_2;
	// System.Boolean Vuforia.HideExcessAreaAbstractBehaviour::disableMattes
	bool ___disableMattes_3;
	// UnityEngine.GameObject Vuforia.HideExcessAreaAbstractBehaviour::mBgPlane
	GameObject_t3674682005 * ___mBgPlane_4;
	// UnityEngine.GameObject Vuforia.HideExcessAreaAbstractBehaviour::mLeftPlane
	GameObject_t3674682005 * ___mLeftPlane_5;
	// UnityEngine.GameObject Vuforia.HideExcessAreaAbstractBehaviour::mRightPlane
	GameObject_t3674682005 * ___mRightPlane_6;
	// UnityEngine.GameObject Vuforia.HideExcessAreaAbstractBehaviour::mTopPlane
	GameObject_t3674682005 * ___mTopPlane_7;
	// UnityEngine.GameObject Vuforia.HideExcessAreaAbstractBehaviour::mBottomPlane
	GameObject_t3674682005 * ___mBottomPlane_8;
	// UnityEngine.Camera Vuforia.HideExcessAreaAbstractBehaviour::mCamera
	Camera_t2727095145 * ___mCamera_9;
	// System.Boolean Vuforia.HideExcessAreaAbstractBehaviour::mSceneIsScaledDown
	bool ___mSceneIsScaledDown_10;
	// UnityEngine.Vector3 Vuforia.HideExcessAreaAbstractBehaviour::mBgPlaneLocalPos
	Vector3_t4282066566  ___mBgPlaneLocalPos_11;
	// UnityEngine.Vector3 Vuforia.HideExcessAreaAbstractBehaviour::mBgPlaneLocalScale
	Vector3_t4282066566  ___mBgPlaneLocalScale_12;
	// System.Single Vuforia.HideExcessAreaAbstractBehaviour::mCameraNearPlane
	float ___mCameraNearPlane_13;
	// UnityEngine.Rect Vuforia.HideExcessAreaAbstractBehaviour::mCameraPixelRect
	Rect_t4241904616  ___mCameraPixelRect_14;
	// System.Single Vuforia.HideExcessAreaAbstractBehaviour::mCameraFieldOFView
	float ___mCameraFieldOFView_15;
	// Vuforia.VuforiaAbstractBehaviour Vuforia.HideExcessAreaAbstractBehaviour::mVuforiaBehaviour
	VuforiaAbstractBehaviour_t1091759131 * ___mVuforiaBehaviour_16;
	// Vuforia.HideExcessAreaAbstractBehaviour[] Vuforia.HideExcessAreaAbstractBehaviour::mHideBehaviours
	HideExcessAreaAbstractBehaviourU5BU5D_t3294375964* ___mHideBehaviours_17;
	// System.Collections.Generic.List`1<Vuforia.HideExcessAreaAbstractBehaviour> Vuforia.HideExcessAreaAbstractBehaviour::mDeactivatedHideBehaviours
	List_1_t1833232017 * ___mDeactivatedHideBehaviours_18;
	// System.Boolean Vuforia.HideExcessAreaAbstractBehaviour::mPlanesActivated
	bool ___mPlanesActivated_19;
	// UnityEngine.Vector3 Vuforia.HideExcessAreaAbstractBehaviour::mLeftPlaneCachedScale
	Vector3_t4282066566  ___mLeftPlaneCachedScale_20;
	// UnityEngine.Vector3 Vuforia.HideExcessAreaAbstractBehaviour::mRightPlaneCachedScale
	Vector3_t4282066566  ___mRightPlaneCachedScale_21;
	// UnityEngine.Vector3 Vuforia.HideExcessAreaAbstractBehaviour::mBottomPlaneCachedScale
	Vector3_t4282066566  ___mBottomPlaneCachedScale_22;
	// UnityEngine.Vector3 Vuforia.HideExcessAreaAbstractBehaviour::mTopPlaneCachedScale
	Vector3_t4282066566  ___mTopPlaneCachedScale_23;

public:
	inline static int32_t get_offset_of_matteShader_2() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t465046465, ___matteShader_2)); }
	inline Shader_t3191267369 * get_matteShader_2() const { return ___matteShader_2; }
	inline Shader_t3191267369 ** get_address_of_matteShader_2() { return &___matteShader_2; }
	inline void set_matteShader_2(Shader_t3191267369 * value)
	{
		___matteShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___matteShader_2, value);
	}

	inline static int32_t get_offset_of_disableMattes_3() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t465046465, ___disableMattes_3)); }
	inline bool get_disableMattes_3() const { return ___disableMattes_3; }
	inline bool* get_address_of_disableMattes_3() { return &___disableMattes_3; }
	inline void set_disableMattes_3(bool value)
	{
		___disableMattes_3 = value;
	}

	inline static int32_t get_offset_of_mBgPlane_4() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t465046465, ___mBgPlane_4)); }
	inline GameObject_t3674682005 * get_mBgPlane_4() const { return ___mBgPlane_4; }
	inline GameObject_t3674682005 ** get_address_of_mBgPlane_4() { return &___mBgPlane_4; }
	inline void set_mBgPlane_4(GameObject_t3674682005 * value)
	{
		___mBgPlane_4 = value;
		Il2CppCodeGenWriteBarrier(&___mBgPlane_4, value);
	}

	inline static int32_t get_offset_of_mLeftPlane_5() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t465046465, ___mLeftPlane_5)); }
	inline GameObject_t3674682005 * get_mLeftPlane_5() const { return ___mLeftPlane_5; }
	inline GameObject_t3674682005 ** get_address_of_mLeftPlane_5() { return &___mLeftPlane_5; }
	inline void set_mLeftPlane_5(GameObject_t3674682005 * value)
	{
		___mLeftPlane_5 = value;
		Il2CppCodeGenWriteBarrier(&___mLeftPlane_5, value);
	}

	inline static int32_t get_offset_of_mRightPlane_6() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t465046465, ___mRightPlane_6)); }
	inline GameObject_t3674682005 * get_mRightPlane_6() const { return ___mRightPlane_6; }
	inline GameObject_t3674682005 ** get_address_of_mRightPlane_6() { return &___mRightPlane_6; }
	inline void set_mRightPlane_6(GameObject_t3674682005 * value)
	{
		___mRightPlane_6 = value;
		Il2CppCodeGenWriteBarrier(&___mRightPlane_6, value);
	}

	inline static int32_t get_offset_of_mTopPlane_7() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t465046465, ___mTopPlane_7)); }
	inline GameObject_t3674682005 * get_mTopPlane_7() const { return ___mTopPlane_7; }
	inline GameObject_t3674682005 ** get_address_of_mTopPlane_7() { return &___mTopPlane_7; }
	inline void set_mTopPlane_7(GameObject_t3674682005 * value)
	{
		___mTopPlane_7 = value;
		Il2CppCodeGenWriteBarrier(&___mTopPlane_7, value);
	}

	inline static int32_t get_offset_of_mBottomPlane_8() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t465046465, ___mBottomPlane_8)); }
	inline GameObject_t3674682005 * get_mBottomPlane_8() const { return ___mBottomPlane_8; }
	inline GameObject_t3674682005 ** get_address_of_mBottomPlane_8() { return &___mBottomPlane_8; }
	inline void set_mBottomPlane_8(GameObject_t3674682005 * value)
	{
		___mBottomPlane_8 = value;
		Il2CppCodeGenWriteBarrier(&___mBottomPlane_8, value);
	}

	inline static int32_t get_offset_of_mCamera_9() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t465046465, ___mCamera_9)); }
	inline Camera_t2727095145 * get_mCamera_9() const { return ___mCamera_9; }
	inline Camera_t2727095145 ** get_address_of_mCamera_9() { return &___mCamera_9; }
	inline void set_mCamera_9(Camera_t2727095145 * value)
	{
		___mCamera_9 = value;
		Il2CppCodeGenWriteBarrier(&___mCamera_9, value);
	}

	inline static int32_t get_offset_of_mSceneIsScaledDown_10() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t465046465, ___mSceneIsScaledDown_10)); }
	inline bool get_mSceneIsScaledDown_10() const { return ___mSceneIsScaledDown_10; }
	inline bool* get_address_of_mSceneIsScaledDown_10() { return &___mSceneIsScaledDown_10; }
	inline void set_mSceneIsScaledDown_10(bool value)
	{
		___mSceneIsScaledDown_10 = value;
	}

	inline static int32_t get_offset_of_mBgPlaneLocalPos_11() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t465046465, ___mBgPlaneLocalPos_11)); }
	inline Vector3_t4282066566  get_mBgPlaneLocalPos_11() const { return ___mBgPlaneLocalPos_11; }
	inline Vector3_t4282066566 * get_address_of_mBgPlaneLocalPos_11() { return &___mBgPlaneLocalPos_11; }
	inline void set_mBgPlaneLocalPos_11(Vector3_t4282066566  value)
	{
		___mBgPlaneLocalPos_11 = value;
	}

	inline static int32_t get_offset_of_mBgPlaneLocalScale_12() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t465046465, ___mBgPlaneLocalScale_12)); }
	inline Vector3_t4282066566  get_mBgPlaneLocalScale_12() const { return ___mBgPlaneLocalScale_12; }
	inline Vector3_t4282066566 * get_address_of_mBgPlaneLocalScale_12() { return &___mBgPlaneLocalScale_12; }
	inline void set_mBgPlaneLocalScale_12(Vector3_t4282066566  value)
	{
		___mBgPlaneLocalScale_12 = value;
	}

	inline static int32_t get_offset_of_mCameraNearPlane_13() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t465046465, ___mCameraNearPlane_13)); }
	inline float get_mCameraNearPlane_13() const { return ___mCameraNearPlane_13; }
	inline float* get_address_of_mCameraNearPlane_13() { return &___mCameraNearPlane_13; }
	inline void set_mCameraNearPlane_13(float value)
	{
		___mCameraNearPlane_13 = value;
	}

	inline static int32_t get_offset_of_mCameraPixelRect_14() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t465046465, ___mCameraPixelRect_14)); }
	inline Rect_t4241904616  get_mCameraPixelRect_14() const { return ___mCameraPixelRect_14; }
	inline Rect_t4241904616 * get_address_of_mCameraPixelRect_14() { return &___mCameraPixelRect_14; }
	inline void set_mCameraPixelRect_14(Rect_t4241904616  value)
	{
		___mCameraPixelRect_14 = value;
	}

	inline static int32_t get_offset_of_mCameraFieldOFView_15() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t465046465, ___mCameraFieldOFView_15)); }
	inline float get_mCameraFieldOFView_15() const { return ___mCameraFieldOFView_15; }
	inline float* get_address_of_mCameraFieldOFView_15() { return &___mCameraFieldOFView_15; }
	inline void set_mCameraFieldOFView_15(float value)
	{
		___mCameraFieldOFView_15 = value;
	}

	inline static int32_t get_offset_of_mVuforiaBehaviour_16() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t465046465, ___mVuforiaBehaviour_16)); }
	inline VuforiaAbstractBehaviour_t1091759131 * get_mVuforiaBehaviour_16() const { return ___mVuforiaBehaviour_16; }
	inline VuforiaAbstractBehaviour_t1091759131 ** get_address_of_mVuforiaBehaviour_16() { return &___mVuforiaBehaviour_16; }
	inline void set_mVuforiaBehaviour_16(VuforiaAbstractBehaviour_t1091759131 * value)
	{
		___mVuforiaBehaviour_16 = value;
		Il2CppCodeGenWriteBarrier(&___mVuforiaBehaviour_16, value);
	}

	inline static int32_t get_offset_of_mHideBehaviours_17() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t465046465, ___mHideBehaviours_17)); }
	inline HideExcessAreaAbstractBehaviourU5BU5D_t3294375964* get_mHideBehaviours_17() const { return ___mHideBehaviours_17; }
	inline HideExcessAreaAbstractBehaviourU5BU5D_t3294375964** get_address_of_mHideBehaviours_17() { return &___mHideBehaviours_17; }
	inline void set_mHideBehaviours_17(HideExcessAreaAbstractBehaviourU5BU5D_t3294375964* value)
	{
		___mHideBehaviours_17 = value;
		Il2CppCodeGenWriteBarrier(&___mHideBehaviours_17, value);
	}

	inline static int32_t get_offset_of_mDeactivatedHideBehaviours_18() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t465046465, ___mDeactivatedHideBehaviours_18)); }
	inline List_1_t1833232017 * get_mDeactivatedHideBehaviours_18() const { return ___mDeactivatedHideBehaviours_18; }
	inline List_1_t1833232017 ** get_address_of_mDeactivatedHideBehaviours_18() { return &___mDeactivatedHideBehaviours_18; }
	inline void set_mDeactivatedHideBehaviours_18(List_1_t1833232017 * value)
	{
		___mDeactivatedHideBehaviours_18 = value;
		Il2CppCodeGenWriteBarrier(&___mDeactivatedHideBehaviours_18, value);
	}

	inline static int32_t get_offset_of_mPlanesActivated_19() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t465046465, ___mPlanesActivated_19)); }
	inline bool get_mPlanesActivated_19() const { return ___mPlanesActivated_19; }
	inline bool* get_address_of_mPlanesActivated_19() { return &___mPlanesActivated_19; }
	inline void set_mPlanesActivated_19(bool value)
	{
		___mPlanesActivated_19 = value;
	}

	inline static int32_t get_offset_of_mLeftPlaneCachedScale_20() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t465046465, ___mLeftPlaneCachedScale_20)); }
	inline Vector3_t4282066566  get_mLeftPlaneCachedScale_20() const { return ___mLeftPlaneCachedScale_20; }
	inline Vector3_t4282066566 * get_address_of_mLeftPlaneCachedScale_20() { return &___mLeftPlaneCachedScale_20; }
	inline void set_mLeftPlaneCachedScale_20(Vector3_t4282066566  value)
	{
		___mLeftPlaneCachedScale_20 = value;
	}

	inline static int32_t get_offset_of_mRightPlaneCachedScale_21() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t465046465, ___mRightPlaneCachedScale_21)); }
	inline Vector3_t4282066566  get_mRightPlaneCachedScale_21() const { return ___mRightPlaneCachedScale_21; }
	inline Vector3_t4282066566 * get_address_of_mRightPlaneCachedScale_21() { return &___mRightPlaneCachedScale_21; }
	inline void set_mRightPlaneCachedScale_21(Vector3_t4282066566  value)
	{
		___mRightPlaneCachedScale_21 = value;
	}

	inline static int32_t get_offset_of_mBottomPlaneCachedScale_22() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t465046465, ___mBottomPlaneCachedScale_22)); }
	inline Vector3_t4282066566  get_mBottomPlaneCachedScale_22() const { return ___mBottomPlaneCachedScale_22; }
	inline Vector3_t4282066566 * get_address_of_mBottomPlaneCachedScale_22() { return &___mBottomPlaneCachedScale_22; }
	inline void set_mBottomPlaneCachedScale_22(Vector3_t4282066566  value)
	{
		___mBottomPlaneCachedScale_22 = value;
	}

	inline static int32_t get_offset_of_mTopPlaneCachedScale_23() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t465046465, ___mTopPlaneCachedScale_23)); }
	inline Vector3_t4282066566  get_mTopPlaneCachedScale_23() const { return ___mTopPlaneCachedScale_23; }
	inline Vector3_t4282066566 * get_address_of_mTopPlaneCachedScale_23() { return &___mTopPlaneCachedScale_23; }
	inline void set_mTopPlaneCachedScale_23(Vector3_t4282066566  value)
	{
		___mTopPlaneCachedScale_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
