﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WebCamTexture
struct WebCamTexture_t1290350902;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadMainScene/<FirstStart>c__Iterator3A
struct  U3CFirstStartU3Ec__Iterator3A_t648989837  : public Il2CppObject
{
public:
	// UnityEngine.WebCamTexture LoadMainScene/<FirstStart>c__Iterator3A::<cam>__0
	WebCamTexture_t1290350902 * ___U3CcamU3E__0_0;
	// System.Int32 LoadMainScene/<FirstStart>c__Iterator3A::$PC
	int32_t ___U24PC_1;
	// System.Object LoadMainScene/<FirstStart>c__Iterator3A::$current
	Il2CppObject * ___U24current_2;

public:
	inline static int32_t get_offset_of_U3CcamU3E__0_0() { return static_cast<int32_t>(offsetof(U3CFirstStartU3Ec__Iterator3A_t648989837, ___U3CcamU3E__0_0)); }
	inline WebCamTexture_t1290350902 * get_U3CcamU3E__0_0() const { return ___U3CcamU3E__0_0; }
	inline WebCamTexture_t1290350902 ** get_address_of_U3CcamU3E__0_0() { return &___U3CcamU3E__0_0; }
	inline void set_U3CcamU3E__0_0(WebCamTexture_t1290350902 * value)
	{
		___U3CcamU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcamU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CFirstStartU3Ec__Iterator3A_t648989837, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CFirstStartU3Ec__Iterator3A_t648989837, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
