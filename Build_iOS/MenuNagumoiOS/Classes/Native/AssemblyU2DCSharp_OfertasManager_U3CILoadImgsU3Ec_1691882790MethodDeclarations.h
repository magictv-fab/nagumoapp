﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertasManager/<ILoadImgs>c__Iterator6B
struct U3CILoadImgsU3Ec__Iterator6B_t1691882790;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OfertasManager/<ILoadImgs>c__Iterator6B::.ctor()
extern "C"  void U3CILoadImgsU3Ec__Iterator6B__ctor_m15328517 (U3CILoadImgsU3Ec__Iterator6B_t1691882790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManager/<ILoadImgs>c__Iterator6B::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CILoadImgsU3Ec__Iterator6B_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2921335917 (U3CILoadImgsU3Ec__Iterator6B_t1691882790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManager/<ILoadImgs>c__Iterator6B::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CILoadImgsU3Ec__Iterator6B_System_Collections_IEnumerator_get_Current_m4174023169 (U3CILoadImgsU3Ec__Iterator6B_t1691882790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OfertasManager/<ILoadImgs>c__Iterator6B::MoveNext()
extern "C"  bool U3CILoadImgsU3Ec__Iterator6B_MoveNext_m1275253455 (U3CILoadImgsU3Ec__Iterator6B_t1691882790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManager/<ILoadImgs>c__Iterator6B::Dispose()
extern "C"  void U3CILoadImgsU3Ec__Iterator6B_Dispose_m1744086018 (U3CILoadImgsU3Ec__Iterator6B_t1691882790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManager/<ILoadImgs>c__Iterator6B::Reset()
extern "C"  void U3CILoadImgsU3Ec__Iterator6B_Reset_m1956728754 (U3CILoadImgsU3Ec__Iterator6B_t1691882790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
