﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ICSharpCode.SharpZipLib.Core.ProcessDirectoryHandler
struct ProcessDirectoryHandler_t4285218014;
// ICSharpCode.SharpZipLib.Core.ProcessFileHandler
struct ProcessFileHandler_t83050093;
// ICSharpCode.SharpZipLib.Core.CompletedFileHandler
struct CompletedFileHandler_t164988305;
// ICSharpCode.SharpZipLib.Core.DirectoryFailureHandler
struct DirectoryFailureHandler_t1659888223;
// ICSharpCode.SharpZipLib.Core.FileFailureHandler
struct FileFailureHandler_t725938474;
// ICSharpCode.SharpZipLib.Core.IScanFilter
struct IScanFilter_t337021136;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Core.FileSystemScanner
struct  FileSystemScanner_t1416526437  : public Il2CppObject
{
public:
	// ICSharpCode.SharpZipLib.Core.ProcessDirectoryHandler ICSharpCode.SharpZipLib.Core.FileSystemScanner::ProcessDirectory
	ProcessDirectoryHandler_t4285218014 * ___ProcessDirectory_0;
	// ICSharpCode.SharpZipLib.Core.ProcessFileHandler ICSharpCode.SharpZipLib.Core.FileSystemScanner::ProcessFile
	ProcessFileHandler_t83050093 * ___ProcessFile_1;
	// ICSharpCode.SharpZipLib.Core.CompletedFileHandler ICSharpCode.SharpZipLib.Core.FileSystemScanner::CompletedFile
	CompletedFileHandler_t164988305 * ___CompletedFile_2;
	// ICSharpCode.SharpZipLib.Core.DirectoryFailureHandler ICSharpCode.SharpZipLib.Core.FileSystemScanner::DirectoryFailure
	DirectoryFailureHandler_t1659888223 * ___DirectoryFailure_3;
	// ICSharpCode.SharpZipLib.Core.FileFailureHandler ICSharpCode.SharpZipLib.Core.FileSystemScanner::FileFailure
	FileFailureHandler_t725938474 * ___FileFailure_4;
	// ICSharpCode.SharpZipLib.Core.IScanFilter ICSharpCode.SharpZipLib.Core.FileSystemScanner::fileFilter_
	Il2CppObject * ___fileFilter__5;
	// ICSharpCode.SharpZipLib.Core.IScanFilter ICSharpCode.SharpZipLib.Core.FileSystemScanner::directoryFilter_
	Il2CppObject * ___directoryFilter__6;
	// System.Boolean ICSharpCode.SharpZipLib.Core.FileSystemScanner::alive_
	bool ___alive__7;

public:
	inline static int32_t get_offset_of_ProcessDirectory_0() { return static_cast<int32_t>(offsetof(FileSystemScanner_t1416526437, ___ProcessDirectory_0)); }
	inline ProcessDirectoryHandler_t4285218014 * get_ProcessDirectory_0() const { return ___ProcessDirectory_0; }
	inline ProcessDirectoryHandler_t4285218014 ** get_address_of_ProcessDirectory_0() { return &___ProcessDirectory_0; }
	inline void set_ProcessDirectory_0(ProcessDirectoryHandler_t4285218014 * value)
	{
		___ProcessDirectory_0 = value;
		Il2CppCodeGenWriteBarrier(&___ProcessDirectory_0, value);
	}

	inline static int32_t get_offset_of_ProcessFile_1() { return static_cast<int32_t>(offsetof(FileSystemScanner_t1416526437, ___ProcessFile_1)); }
	inline ProcessFileHandler_t83050093 * get_ProcessFile_1() const { return ___ProcessFile_1; }
	inline ProcessFileHandler_t83050093 ** get_address_of_ProcessFile_1() { return &___ProcessFile_1; }
	inline void set_ProcessFile_1(ProcessFileHandler_t83050093 * value)
	{
		___ProcessFile_1 = value;
		Il2CppCodeGenWriteBarrier(&___ProcessFile_1, value);
	}

	inline static int32_t get_offset_of_CompletedFile_2() { return static_cast<int32_t>(offsetof(FileSystemScanner_t1416526437, ___CompletedFile_2)); }
	inline CompletedFileHandler_t164988305 * get_CompletedFile_2() const { return ___CompletedFile_2; }
	inline CompletedFileHandler_t164988305 ** get_address_of_CompletedFile_2() { return &___CompletedFile_2; }
	inline void set_CompletedFile_2(CompletedFileHandler_t164988305 * value)
	{
		___CompletedFile_2 = value;
		Il2CppCodeGenWriteBarrier(&___CompletedFile_2, value);
	}

	inline static int32_t get_offset_of_DirectoryFailure_3() { return static_cast<int32_t>(offsetof(FileSystemScanner_t1416526437, ___DirectoryFailure_3)); }
	inline DirectoryFailureHandler_t1659888223 * get_DirectoryFailure_3() const { return ___DirectoryFailure_3; }
	inline DirectoryFailureHandler_t1659888223 ** get_address_of_DirectoryFailure_3() { return &___DirectoryFailure_3; }
	inline void set_DirectoryFailure_3(DirectoryFailureHandler_t1659888223 * value)
	{
		___DirectoryFailure_3 = value;
		Il2CppCodeGenWriteBarrier(&___DirectoryFailure_3, value);
	}

	inline static int32_t get_offset_of_FileFailure_4() { return static_cast<int32_t>(offsetof(FileSystemScanner_t1416526437, ___FileFailure_4)); }
	inline FileFailureHandler_t725938474 * get_FileFailure_4() const { return ___FileFailure_4; }
	inline FileFailureHandler_t725938474 ** get_address_of_FileFailure_4() { return &___FileFailure_4; }
	inline void set_FileFailure_4(FileFailureHandler_t725938474 * value)
	{
		___FileFailure_4 = value;
		Il2CppCodeGenWriteBarrier(&___FileFailure_4, value);
	}

	inline static int32_t get_offset_of_fileFilter__5() { return static_cast<int32_t>(offsetof(FileSystemScanner_t1416526437, ___fileFilter__5)); }
	inline Il2CppObject * get_fileFilter__5() const { return ___fileFilter__5; }
	inline Il2CppObject ** get_address_of_fileFilter__5() { return &___fileFilter__5; }
	inline void set_fileFilter__5(Il2CppObject * value)
	{
		___fileFilter__5 = value;
		Il2CppCodeGenWriteBarrier(&___fileFilter__5, value);
	}

	inline static int32_t get_offset_of_directoryFilter__6() { return static_cast<int32_t>(offsetof(FileSystemScanner_t1416526437, ___directoryFilter__6)); }
	inline Il2CppObject * get_directoryFilter__6() const { return ___directoryFilter__6; }
	inline Il2CppObject ** get_address_of_directoryFilter__6() { return &___directoryFilter__6; }
	inline void set_directoryFilter__6(Il2CppObject * value)
	{
		___directoryFilter__6 = value;
		Il2CppCodeGenWriteBarrier(&___directoryFilter__6, value);
	}

	inline static int32_t get_offset_of_alive__7() { return static_cast<int32_t>(offsetof(FileSystemScanner_t1416526437, ___alive__7)); }
	inline bool get_alive__7() const { return ___alive__7; }
	inline bool* get_address_of_alive__7() { return &___alive__7; }
	inline void set_alive__7(bool value)
	{
		___alive__7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
