﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2953159047MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.FsmInt>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m802833388(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t378481125 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2616641763_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.FsmInt>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m243315252(__this, method) ((  void (*) (InternalEnumerator_1_t378481125 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2224260061_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<HutongGames.PlayMaker.FsmInt>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m733867744(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t378481125 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m390763987_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.FsmInt>::Dispose()
#define InternalEnumerator_1_Dispose_m2526250179(__this, method) ((  void (*) (InternalEnumerator_1_t378481125 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2760671866_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<HutongGames.PlayMaker.FsmInt>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3391146272(__this, method) ((  bool (*) (InternalEnumerator_1_t378481125 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3716548237_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<HutongGames.PlayMaker.FsmInt>::get_Current()
#define InternalEnumerator_1_get_Current_m2457447155(__this, method) ((  FsmInt_t1596138449 * (*) (InternalEnumerator_1_t378481125 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2178852364_gshared)(__this, method)
