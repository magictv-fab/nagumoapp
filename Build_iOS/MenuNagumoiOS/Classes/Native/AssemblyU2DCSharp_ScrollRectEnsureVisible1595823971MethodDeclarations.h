﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScrollRectEnsureVisible
struct ScrollRectEnsureVisible_t1595823971;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.RectTransform
struct RectTransform_t972643934;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RectTransform972643934.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void ScrollRectEnsureVisible::.ctor()
extern "C"  void ScrollRectEnsureVisible__ctor_m3867715432 (ScrollRectEnsureVisible_t1595823971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollRectEnsureVisible::Start()
extern "C"  void ScrollRectEnsureVisible_Start_m2814853224 (ScrollRectEnsureVisible_t1595823971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ScrollRectEnsureVisible::forceSnap()
extern "C"  Il2CppObject * ScrollRectEnsureVisible_forceSnap_m2398786691 (ScrollRectEnsureVisible_t1595823971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollRectEnsureVisible::getIndexScroll()
extern "C"  void ScrollRectEnsureVisible_getIndexScroll_m316087749 (ScrollRectEnsureVisible_t1595823971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollRectEnsureVisible::CenterOnItem(UnityEngine.RectTransform)
extern "C"  void ScrollRectEnsureVisible_CenterOnItem_m1809845054 (ScrollRectEnsureVisible_t1595823971 * __this, RectTransform_t972643934 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollRectEnsureVisible::Awake()
extern "C"  void ScrollRectEnsureVisible_Awake_m4105320651 (ScrollRectEnsureVisible_t1595823971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollRectEnsureVisible::Reset()
extern "C"  void ScrollRectEnsureVisible_Reset_m1514148373 (ScrollRectEnsureVisible_t1595823971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ScrollRectEnsureVisible::GetWidgetWorldPoint(UnityEngine.RectTransform)
extern "C"  Vector3_t4282066566  ScrollRectEnsureVisible_GetWidgetWorldPoint_m2334422921 (ScrollRectEnsureVisible_t1595823971 * __this, RectTransform_t972643934 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ScrollRectEnsureVisible::GetWorldPointInWidget(UnityEngine.RectTransform,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  ScrollRectEnsureVisible_GetWorldPointInWidget_m1459081399 (ScrollRectEnsureVisible_t1595823971 * __this, RectTransform_t972643934 * ___target0, Vector3_t4282066566  ___worldPoint1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
