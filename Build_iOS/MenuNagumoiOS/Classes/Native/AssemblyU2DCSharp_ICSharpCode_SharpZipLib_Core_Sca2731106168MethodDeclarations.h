﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Core.ScanFailureEventArgs
struct ScanFailureEventArgs_t2731106168;
// System.String
struct String_t;
// System.Exception
struct Exception_t3991598821;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Exception3991598821.h"

// System.Void ICSharpCode.SharpZipLib.Core.ScanFailureEventArgs::.ctor(System.String,System.Exception)
extern "C"  void ScanFailureEventArgs__ctor_m2774524919 (ScanFailureEventArgs_t2731106168 * __this, String_t* ___name0, Exception_t3991598821 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Core.ScanFailureEventArgs::get_Name()
extern "C"  String_t* ScanFailureEventArgs_get_Name_m932013490 (ScanFailureEventArgs_t2731106168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception ICSharpCode.SharpZipLib.Core.ScanFailureEventArgs::get_Exception()
extern "C"  Exception_t3991598821 * ScanFailureEventArgs_get_Exception_m1128612404 (ScanFailureEventArgs_t2731106168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Core.ScanFailureEventArgs::get_ContinueRunning()
extern "C"  bool ScanFailureEventArgs_get_ContinueRunning_m3980175620 (ScanFailureEventArgs_t2731106168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.ScanFailureEventArgs::set_ContinueRunning(System.Boolean)
extern "C"  void ScanFailureEventArgs_set_ContinueRunning_m2808788179 (ScanFailureEventArgs_t2731106168 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
