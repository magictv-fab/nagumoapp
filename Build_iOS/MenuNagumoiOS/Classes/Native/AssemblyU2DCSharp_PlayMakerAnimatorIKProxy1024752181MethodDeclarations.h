﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerAnimatorIKProxy
struct PlayMakerAnimatorIKProxy_t1024752181;
// System.Action`1<System.Int32>
struct Action_1_t1549654636;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayMakerAnimatorIKProxy::.ctor()
extern "C"  void PlayMakerAnimatorIKProxy__ctor_m1305133062 (PlayMakerAnimatorIKProxy_t1024752181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerAnimatorIKProxy::add_OnAnimatorIKEvent(System.Action`1<System.Int32>)
extern "C"  void PlayMakerAnimatorIKProxy_add_OnAnimatorIKEvent_m2290104737 (PlayMakerAnimatorIKProxy_t1024752181 * __this, Action_1_t1549654636 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerAnimatorIKProxy::remove_OnAnimatorIKEvent(System.Action`1<System.Int32>)
extern "C"  void PlayMakerAnimatorIKProxy_remove_OnAnimatorIKEvent_m1260795332 (PlayMakerAnimatorIKProxy_t1024752181 * __this, Action_1_t1549654636 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerAnimatorIKProxy::OnAnimatorIK(System.Int32)
extern "C"  void PlayMakerAnimatorIKProxy_OnAnimatorIK_m3711407959 (PlayMakerAnimatorIKProxy_t1024752181 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
