﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IDictionary`2<System.Int32,ZXing.Common.CharacterSetECI>
struct IDictionary_2_t2117401752;
// System.Collections.Generic.IDictionary`2<System.String,ZXing.Common.CharacterSetECI>
struct IDictionary_2_t2940556883;
// System.String
struct String_t;

#include "QRCode_ZXing_Common_ECI1296865545.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.CharacterSetECI
struct  CharacterSetECI_t2542265168  : public ECI_t1296865545
{
public:
	// System.String ZXing.Common.CharacterSetECI::encodingName
	String_t* ___encodingName_3;

public:
	inline static int32_t get_offset_of_encodingName_3() { return static_cast<int32_t>(offsetof(CharacterSetECI_t2542265168, ___encodingName_3)); }
	inline String_t* get_encodingName_3() const { return ___encodingName_3; }
	inline String_t** get_address_of_encodingName_3() { return &___encodingName_3; }
	inline void set_encodingName_3(String_t* value)
	{
		___encodingName_3 = value;
		Il2CppCodeGenWriteBarrier(&___encodingName_3, value);
	}
};

struct CharacterSetECI_t2542265168_StaticFields
{
public:
	// System.Collections.Generic.IDictionary`2<System.Int32,ZXing.Common.CharacterSetECI> ZXing.Common.CharacterSetECI::VALUE_TO_ECI
	Il2CppObject* ___VALUE_TO_ECI_1;
	// System.Collections.Generic.IDictionary`2<System.String,ZXing.Common.CharacterSetECI> ZXing.Common.CharacterSetECI::NAME_TO_ECI
	Il2CppObject* ___NAME_TO_ECI_2;

public:
	inline static int32_t get_offset_of_VALUE_TO_ECI_1() { return static_cast<int32_t>(offsetof(CharacterSetECI_t2542265168_StaticFields, ___VALUE_TO_ECI_1)); }
	inline Il2CppObject* get_VALUE_TO_ECI_1() const { return ___VALUE_TO_ECI_1; }
	inline Il2CppObject** get_address_of_VALUE_TO_ECI_1() { return &___VALUE_TO_ECI_1; }
	inline void set_VALUE_TO_ECI_1(Il2CppObject* value)
	{
		___VALUE_TO_ECI_1 = value;
		Il2CppCodeGenWriteBarrier(&___VALUE_TO_ECI_1, value);
	}

	inline static int32_t get_offset_of_NAME_TO_ECI_2() { return static_cast<int32_t>(offsetof(CharacterSetECI_t2542265168_StaticFields, ___NAME_TO_ECI_2)); }
	inline Il2CppObject* get_NAME_TO_ECI_2() const { return ___NAME_TO_ECI_2; }
	inline Il2CppObject** get_address_of_NAME_TO_ECI_2() { return &___NAME_TO_ECI_2; }
	inline void set_NAME_TO_ECI_2(Il2CppObject* value)
	{
		___NAME_TO_ECI_2 = value;
		Il2CppCodeGenWriteBarrier(&___NAME_TO_ECI_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
