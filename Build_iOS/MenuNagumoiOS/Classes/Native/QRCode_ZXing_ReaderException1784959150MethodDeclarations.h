﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.ReaderException
struct ReaderException_t1784959150;

#include "codegen/il2cpp-codegen.h"

// System.Void ZXing.ReaderException::.ctor()
extern "C"  void ReaderException__ctor_m1397899881 (ReaderException_t1784959150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
