﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.LuminanceSource
struct LuminanceSource_t1231523093;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "QRCode_ZXing_LuminanceSource1231523093.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.InvertedLuminanceSource
struct  InvertedLuminanceSource_t1075320672  : public LuminanceSource_t1231523093
{
public:
	// ZXing.LuminanceSource ZXing.InvertedLuminanceSource::delegate
	LuminanceSource_t1231523093 * ___delegate_2;
	// System.Byte[] ZXing.InvertedLuminanceSource::invertedMatrix
	ByteU5BU5D_t4260760469* ___invertedMatrix_3;

public:
	inline static int32_t get_offset_of_delegate_2() { return static_cast<int32_t>(offsetof(InvertedLuminanceSource_t1075320672, ___delegate_2)); }
	inline LuminanceSource_t1231523093 * get_delegate_2() const { return ___delegate_2; }
	inline LuminanceSource_t1231523093 ** get_address_of_delegate_2() { return &___delegate_2; }
	inline void set_delegate_2(LuminanceSource_t1231523093 * value)
	{
		___delegate_2 = value;
		Il2CppCodeGenWriteBarrier(&___delegate_2, value);
	}

	inline static int32_t get_offset_of_invertedMatrix_3() { return static_cast<int32_t>(offsetof(InvertedLuminanceSource_t1075320672, ___invertedMatrix_3)); }
	inline ByteU5BU5D_t4260760469* get_invertedMatrix_3() const { return ___invertedMatrix_3; }
	inline ByteU5BU5D_t4260760469** get_address_of_invertedMatrix_3() { return &___invertedMatrix_3; }
	inline void set_invertedMatrix_3(ByteU5BU5D_t4260760469* value)
	{
		___invertedMatrix_3 = value;
		Il2CppCodeGenWriteBarrier(&___invertedMatrix_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
