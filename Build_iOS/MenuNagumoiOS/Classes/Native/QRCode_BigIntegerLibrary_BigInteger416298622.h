﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// BigIntegerLibrary.BigInteger
struct BigInteger_t416298622;
// BigIntegerLibrary.BigInteger/DigitContainer
struct DigitContainer_t1134993364;

#include "mscorlib_System_Object4170816371.h"
#include "QRCode_BigIntegerLibrary_Sign1848424669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BigIntegerLibrary.BigInteger
struct  BigInteger_t416298622  : public Il2CppObject
{
public:
	// BigIntegerLibrary.BigInteger/DigitContainer BigIntegerLibrary.BigInteger::digits
	DigitContainer_t1134993364 * ___digits_4;
	// System.Int32 BigIntegerLibrary.BigInteger::size
	int32_t ___size_5;
	// BigIntegerLibrary.Sign BigIntegerLibrary.BigInteger::sign
	int32_t ___sign_6;

public:
	inline static int32_t get_offset_of_digits_4() { return static_cast<int32_t>(offsetof(BigInteger_t416298622, ___digits_4)); }
	inline DigitContainer_t1134993364 * get_digits_4() const { return ___digits_4; }
	inline DigitContainer_t1134993364 ** get_address_of_digits_4() { return &___digits_4; }
	inline void set_digits_4(DigitContainer_t1134993364 * value)
	{
		___digits_4 = value;
		Il2CppCodeGenWriteBarrier(&___digits_4, value);
	}

	inline static int32_t get_offset_of_size_5() { return static_cast<int32_t>(offsetof(BigInteger_t416298622, ___size_5)); }
	inline int32_t get_size_5() const { return ___size_5; }
	inline int32_t* get_address_of_size_5() { return &___size_5; }
	inline void set_size_5(int32_t value)
	{
		___size_5 = value;
	}

	inline static int32_t get_offset_of_sign_6() { return static_cast<int32_t>(offsetof(BigInteger_t416298622, ___sign_6)); }
	inline int32_t get_sign_6() const { return ___sign_6; }
	inline int32_t* get_address_of_sign_6() { return &___sign_6; }
	inline void set_sign_6(int32_t value)
	{
		___sign_6 = value;
	}
};

struct BigInteger_t416298622_StaticFields
{
public:
	// BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Zero
	BigInteger_t416298622 * ___Zero_0;
	// BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::One
	BigInteger_t416298622 * ___One_1;
	// BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Two
	BigInteger_t416298622 * ___Two_2;
	// BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Ten
	BigInteger_t416298622 * ___Ten_3;

public:
	inline static int32_t get_offset_of_Zero_0() { return static_cast<int32_t>(offsetof(BigInteger_t416298622_StaticFields, ___Zero_0)); }
	inline BigInteger_t416298622 * get_Zero_0() const { return ___Zero_0; }
	inline BigInteger_t416298622 ** get_address_of_Zero_0() { return &___Zero_0; }
	inline void set_Zero_0(BigInteger_t416298622 * value)
	{
		___Zero_0 = value;
		Il2CppCodeGenWriteBarrier(&___Zero_0, value);
	}

	inline static int32_t get_offset_of_One_1() { return static_cast<int32_t>(offsetof(BigInteger_t416298622_StaticFields, ___One_1)); }
	inline BigInteger_t416298622 * get_One_1() const { return ___One_1; }
	inline BigInteger_t416298622 ** get_address_of_One_1() { return &___One_1; }
	inline void set_One_1(BigInteger_t416298622 * value)
	{
		___One_1 = value;
		Il2CppCodeGenWriteBarrier(&___One_1, value);
	}

	inline static int32_t get_offset_of_Two_2() { return static_cast<int32_t>(offsetof(BigInteger_t416298622_StaticFields, ___Two_2)); }
	inline BigInteger_t416298622 * get_Two_2() const { return ___Two_2; }
	inline BigInteger_t416298622 ** get_address_of_Two_2() { return &___Two_2; }
	inline void set_Two_2(BigInteger_t416298622 * value)
	{
		___Two_2 = value;
		Il2CppCodeGenWriteBarrier(&___Two_2, value);
	}

	inline static int32_t get_offset_of_Ten_3() { return static_cast<int32_t>(offsetof(BigInteger_t416298622_StaticFields, ___Ten_3)); }
	inline BigInteger_t416298622 * get_Ten_3() const { return ___Ten_3; }
	inline BigInteger_t416298622 ** get_address_of_Ten_3() { return &___Ten_3; }
	inline void set_Ten_3(BigInteger_t416298622 * value)
	{
		___Ten_3 = value;
		Il2CppCodeGenWriteBarrier(&___Ten_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
