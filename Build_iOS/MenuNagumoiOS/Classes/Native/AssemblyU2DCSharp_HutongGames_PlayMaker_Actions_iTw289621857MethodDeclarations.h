﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenScaleAdd
struct iTweenScaleAdd_t289621857;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenScaleAdd::.ctor()
extern "C"  void iTweenScaleAdd__ctor_m2414050853 (iTweenScaleAdd_t289621857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleAdd::Reset()
extern "C"  void iTweenScaleAdd_Reset_m60483794 (iTweenScaleAdd_t289621857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleAdd::OnEnter()
extern "C"  void iTweenScaleAdd_OnEnter_m12258492 (iTweenScaleAdd_t289621857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleAdd::OnExit()
extern "C"  void iTweenScaleAdd_OnExit_m3334451036 (iTweenScaleAdd_t289621857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleAdd::DoiTween()
extern "C"  void iTweenScaleAdd_DoiTween_m1831145484 (iTweenScaleAdd_t289621857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
