﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.utils.download.GenericDownloadManager
struct GenericDownloadManager_t3298305714;
// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t3134621005;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ARM.utils.download.GenericDownloadManager::.ctor()
extern "C"  void GenericDownloadManager__ctor_m4116848390 (GenericDownloadManager_t3298305714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.download.GenericDownloadManager::.cctor()
extern "C"  void GenericDownloadManager__cctor_m2586152295 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARM.utils.download.GenericDownloadManager::get__instanceID()
extern "C"  int32_t GenericDownloadManager_get__instanceID_m338070200 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.download.GenericDownloadManager::Load(System.String,System.Int32)
extern "C"  void GenericDownloadManager_Load_m163976441 (GenericDownloadManager_t3298305714 * __this, String_t* ___url0, int32_t ___extraAttempts1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.download.GenericDownloadManager::Dispose()
extern "C"  void GenericDownloadManager_Dispose_m524706243 (GenericDownloadManager_t3298305714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARM.utils.download.GenericDownloadManager::IsComplete()
extern "C"  bool GenericDownloadManager_IsComplete_m1915805261 (GenericDownloadManager_t3298305714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWW ARM.utils.download.GenericDownloadManager::GetRequest()
extern "C"  WWW_t3134621005 * GenericDownloadManager_GetRequest_m2384703844 (GenericDownloadManager_t3298305714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.download.GenericDownloadManager::prepare()
extern "C"  void GenericDownloadManager_prepare_m1553403595 (GenericDownloadManager_t3298305714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
