﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t4179556250;

#include "AssemblyU2DCSharp_ARM_camera_abstracts_TrackEventD3123780554.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.camera.ra.VuforiaTrackEvent
struct  VuforiaTrackEvent_t3472797067  : public TrackEventDispatcherAbstract_t3123780554
{
public:
	// Vuforia.TrackableBehaviour ARM.camera.ra.VuforiaTrackEvent::mTrackableBehaviour
	TrackableBehaviour_t4179556250 * ___mTrackableBehaviour_4;

public:
	inline static int32_t get_offset_of_mTrackableBehaviour_4() { return static_cast<int32_t>(offsetof(VuforiaTrackEvent_t3472797067, ___mTrackableBehaviour_4)); }
	inline TrackableBehaviour_t4179556250 * get_mTrackableBehaviour_4() const { return ___mTrackableBehaviour_4; }
	inline TrackableBehaviour_t4179556250 ** get_address_of_mTrackableBehaviour_4() { return &___mTrackableBehaviour_4; }
	inline void set_mTrackableBehaviour_4(TrackableBehaviour_t4179556250 * value)
	{
		___mTrackableBehaviour_4 = value;
		Il2CppCodeGenWriteBarrier(&___mTrackableBehaviour_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
