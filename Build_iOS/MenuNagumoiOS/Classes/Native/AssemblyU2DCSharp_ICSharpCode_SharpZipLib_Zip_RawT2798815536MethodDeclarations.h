﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.RawTaggedData
struct RawTaggedData_t2798815536;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"

// System.Void ICSharpCode.SharpZipLib.Zip.RawTaggedData::.ctor(System.Int16)
extern "C"  void RawTaggedData__ctor_m2147164438 (RawTaggedData_t2798815536 * __this, int16_t ___tag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 ICSharpCode.SharpZipLib.Zip.RawTaggedData::get_TagID()
extern "C"  int16_t RawTaggedData_get_TagID_m1284178557 (RawTaggedData_t2798815536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.RawTaggedData::set_TagID(System.Int16)
extern "C"  void RawTaggedData_set_TagID_m3348550476 (RawTaggedData_t2798815536 * __this, int16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.RawTaggedData::SetData(System.Byte[],System.Int32,System.Int32)
extern "C"  void RawTaggedData_SetData_m117044148 (RawTaggedData_t2798815536 * __this, ByteU5BU5D_t4260760469* ___data0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ICSharpCode.SharpZipLib.Zip.RawTaggedData::GetData()
extern "C"  ByteU5BU5D_t4260760469* RawTaggedData_GetData_m1506631795 (RawTaggedData_t2798815536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ICSharpCode.SharpZipLib.Zip.RawTaggedData::get_Data()
extern "C"  ByteU5BU5D_t4260760469* RawTaggedData_get_Data_m3403039266 (RawTaggedData_t2798815536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.RawTaggedData::set_Data(System.Byte[])
extern "C"  void RawTaggedData_set_Data_m1263287177 (RawTaggedData_t2798815536 * __this, ByteU5BU5D_t4260760469* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
