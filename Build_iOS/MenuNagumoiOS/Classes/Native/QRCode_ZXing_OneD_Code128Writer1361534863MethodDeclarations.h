﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.Code128Writer
struct Code128Writer_t1361534863;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// System.String
struct String_t;
// System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>
struct IDictionary_2_t3297010830;
// System.Boolean[]
struct BooleanU5BU5D_t3456302923;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "QRCode_ZXing_BarcodeFormat4201805817.h"

// ZXing.Common.BitMatrix ZXing.OneD.Code128Writer::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern "C"  BitMatrix_t1058711404 * Code128Writer_encode_m497859922 (Code128Writer_t1361534863 * __this, String_t* ___contents0, int32_t ___format1, int32_t ___width2, int32_t ___height3, Il2CppObject* ___hints4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean[] ZXing.OneD.Code128Writer::encode(System.String)
extern "C"  BooleanU5BU5D_t3456302923* Code128Writer_encode_m3810598170 (Code128Writer_t1361534863 * __this, String_t* ___contents0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.Code128Writer::isDigits(System.String,System.Int32,System.Int32)
extern "C"  bool Code128Writer_isDigits_m1668309886 (Il2CppObject * __this /* static, unused */, String_t* ___value0, int32_t ___start1, int32_t ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.Code128Writer::.ctor()
extern "C"  void Code128Writer__ctor_m1183633380 (Code128Writer_t1361534863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
