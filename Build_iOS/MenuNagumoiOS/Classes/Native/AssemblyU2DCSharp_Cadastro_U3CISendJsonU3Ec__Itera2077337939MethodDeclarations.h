﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Cadastro/<ISendJson>c__Iterator4A
struct U3CISendJsonU3Ec__Iterator4A_t2077337939;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Cadastro/<ISendJson>c__Iterator4A::.ctor()
extern "C"  void U3CISendJsonU3Ec__Iterator4A__ctor_m2445869432 (U3CISendJsonU3Ec__Iterator4A_t2077337939 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Cadastro/<ISendJson>c__Iterator4A::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CISendJsonU3Ec__Iterator4A_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m315735578 (U3CISendJsonU3Ec__Iterator4A_t2077337939 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Cadastro/<ISendJson>c__Iterator4A::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CISendJsonU3Ec__Iterator4A_System_Collections_IEnumerator_get_Current_m459748782 (U3CISendJsonU3Ec__Iterator4A_t2077337939 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Cadastro/<ISendJson>c__Iterator4A::MoveNext()
extern "C"  bool U3CISendJsonU3Ec__Iterator4A_MoveNext_m148571772 (U3CISendJsonU3Ec__Iterator4A_t2077337939 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cadastro/<ISendJson>c__Iterator4A::Dispose()
extern "C"  void U3CISendJsonU3Ec__Iterator4A_Dispose_m1031696309 (U3CISendJsonU3Ec__Iterator4A_t2077337939 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cadastro/<ISendJson>c__Iterator4A::Reset()
extern "C"  void U3CISendJsonU3Ec__Iterator4A_Reset_m92302373 (U3CISendJsonU3Ec__Iterator4A_t2077337939 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
