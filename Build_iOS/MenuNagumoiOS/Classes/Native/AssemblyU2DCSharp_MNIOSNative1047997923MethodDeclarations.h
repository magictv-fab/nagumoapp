﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MNIOSNative
struct MNIOSNative_t1047997923;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MNIOSNative::.ctor()
extern "C"  void MNIOSNative__ctor_m4036377832 (MNIOSNative_t1047997923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSNative::_MNP_ShowRateUsPopUp(System.String,System.String,System.String,System.String,System.String)
extern "C"  void MNIOSNative__MNP_ShowRateUsPopUp_m3505421748 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, String_t* ___rate2, String_t* ___remind3, String_t* ___declined4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSNative::_MNP_ShowDialog(System.String,System.String,System.String,System.String)
extern "C"  void MNIOSNative__MNP_ShowDialog_m816346556 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, String_t* ___yes2, String_t* ___no3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSNative::_MNP_ShowMessage(System.String,System.String,System.String)
extern "C"  void MNIOSNative__MNP_ShowMessage_m388980451 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, String_t* ___ok2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSNative::_MNP_DismissCurrentAlert()
extern "C"  void MNIOSNative__MNP_DismissCurrentAlert_m2017866362 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSNative::_MNP_RedirectToAppStoreRatingPage(System.String)
extern "C"  void MNIOSNative__MNP_RedirectToAppStoreRatingPage_m299313386 (Il2CppObject * __this /* static, unused */, String_t* ___appId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSNative::_MNP_ShowPreloader()
extern "C"  void MNIOSNative__MNP_ShowPreloader_m3250282342 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSNative::_MNP_HidePreloader()
extern "C"  void MNIOSNative__MNP_HidePreloader_m4245467137 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSNative::dismissCurrentAlert()
extern "C"  void MNIOSNative_dismissCurrentAlert_m890633523 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSNative::showRateUsPopUP(System.String,System.String,System.String,System.String,System.String)
extern "C"  void MNIOSNative_showRateUsPopUP_m2590815803 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, String_t* ___rate2, String_t* ___remind3, String_t* ___declined4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSNative::showDialog(System.String,System.String)
extern "C"  void MNIOSNative_showDialog_m227703485 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSNative::showDialog(System.String,System.String,System.String,System.String)
extern "C"  void MNIOSNative_showDialog_m581723317 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, String_t* ___yes2, String_t* ___no3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSNative::showMessage(System.String,System.String)
extern "C"  void MNIOSNative_showMessage_m3373512590 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSNative::showMessage(System.String,System.String,System.String)
extern "C"  void MNIOSNative_showMessage_m2901774154 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, String_t* ___ok2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSNative::RedirectToAppStoreRatingPage(System.String)
extern "C"  void MNIOSNative_RedirectToAppStoreRatingPage_m3523041987 (Il2CppObject * __this /* static, unused */, String_t* ___appleId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSNative::ShowPreloader()
extern "C"  void MNIOSNative_ShowPreloader_m1654038719 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSNative::HidePreloader()
extern "C"  void MNIOSNative_HidePreloader_m2649223514 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
