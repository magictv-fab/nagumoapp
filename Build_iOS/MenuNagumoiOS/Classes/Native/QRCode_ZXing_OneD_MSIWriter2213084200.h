﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t1820556512;

#include "QRCode_ZXing_OneD_OneDimensionalCodeWriter4068326409.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.MSIWriter
struct  MSIWriter_t2213084200  : public OneDimensionalCodeWriter_t4068326409
{
public:

public:
};

struct MSIWriter_t2213084200_StaticFields
{
public:
	// System.Int32[] ZXing.OneD.MSIWriter::startWidths
	Int32U5BU5D_t3230847821* ___startWidths_0;
	// System.Int32[] ZXing.OneD.MSIWriter::endWidths
	Int32U5BU5D_t3230847821* ___endWidths_1;
	// System.Int32[][] ZXing.OneD.MSIWriter::numberWidths
	Int32U5BU5DU5BU5D_t1820556512* ___numberWidths_2;

public:
	inline static int32_t get_offset_of_startWidths_0() { return static_cast<int32_t>(offsetof(MSIWriter_t2213084200_StaticFields, ___startWidths_0)); }
	inline Int32U5BU5D_t3230847821* get_startWidths_0() const { return ___startWidths_0; }
	inline Int32U5BU5D_t3230847821** get_address_of_startWidths_0() { return &___startWidths_0; }
	inline void set_startWidths_0(Int32U5BU5D_t3230847821* value)
	{
		___startWidths_0 = value;
		Il2CppCodeGenWriteBarrier(&___startWidths_0, value);
	}

	inline static int32_t get_offset_of_endWidths_1() { return static_cast<int32_t>(offsetof(MSIWriter_t2213084200_StaticFields, ___endWidths_1)); }
	inline Int32U5BU5D_t3230847821* get_endWidths_1() const { return ___endWidths_1; }
	inline Int32U5BU5D_t3230847821** get_address_of_endWidths_1() { return &___endWidths_1; }
	inline void set_endWidths_1(Int32U5BU5D_t3230847821* value)
	{
		___endWidths_1 = value;
		Il2CppCodeGenWriteBarrier(&___endWidths_1, value);
	}

	inline static int32_t get_offset_of_numberWidths_2() { return static_cast<int32_t>(offsetof(MSIWriter_t2213084200_StaticFields, ___numberWidths_2)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_numberWidths_2() const { return ___numberWidths_2; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_numberWidths_2() { return &___numberWidths_2; }
	inline void set_numberWidths_2(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___numberWidths_2 = value;
		Il2CppCodeGenWriteBarrier(&___numberWidths_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
