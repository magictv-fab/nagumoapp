﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// MagicTV.abstracts.device.BenchmarkInfo
struct BenchmarkInfo_t940577717;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;

#include "AssemblyU2DCSharp_MagicTV_abstracts_BenchmarkAbstr3666376745.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Benchmark
struct  Benchmark_t2672107741  : public BenchmarkAbstract_t3666376745
{
public:
	// System.String Benchmark::_userDeviceQuality
	String_t* ____userDeviceQuality_9;
	// System.String Benchmark::_deviceModel
	String_t* ____deviceModel_10;
	// System.Int32 Benchmark::_graphicsMemorySize
	int32_t ____graphicsMemorySize_11;
	// System.Int32 Benchmark::_processorCount
	int32_t ____processorCount_12;
	// System.Boolean Benchmark::_supportShadows
	bool ____supportShadows_13;
	// System.Int32 Benchmark::_systemMemorySize
	int32_t ____systemMemorySize_14;
	// MagicTV.abstracts.device.BenchmarkInfo Benchmark::_benchmarkInfo
	BenchmarkInfo_t940577717 * ____benchmarkInfo_15;
	// System.Collections.Generic.List`1<System.String> Benchmark::_lowPerformanceDevice
	List_1_t1375417109 * ____lowPerformanceDevice_16;
	// System.Collections.Generic.List`1<System.String> Benchmark::_midPerformanceDevice
	List_1_t1375417109 * ____midPerformanceDevice_17;

public:
	inline static int32_t get_offset_of__userDeviceQuality_9() { return static_cast<int32_t>(offsetof(Benchmark_t2672107741, ____userDeviceQuality_9)); }
	inline String_t* get__userDeviceQuality_9() const { return ____userDeviceQuality_9; }
	inline String_t** get_address_of__userDeviceQuality_9() { return &____userDeviceQuality_9; }
	inline void set__userDeviceQuality_9(String_t* value)
	{
		____userDeviceQuality_9 = value;
		Il2CppCodeGenWriteBarrier(&____userDeviceQuality_9, value);
	}

	inline static int32_t get_offset_of__deviceModel_10() { return static_cast<int32_t>(offsetof(Benchmark_t2672107741, ____deviceModel_10)); }
	inline String_t* get__deviceModel_10() const { return ____deviceModel_10; }
	inline String_t** get_address_of__deviceModel_10() { return &____deviceModel_10; }
	inline void set__deviceModel_10(String_t* value)
	{
		____deviceModel_10 = value;
		Il2CppCodeGenWriteBarrier(&____deviceModel_10, value);
	}

	inline static int32_t get_offset_of__graphicsMemorySize_11() { return static_cast<int32_t>(offsetof(Benchmark_t2672107741, ____graphicsMemorySize_11)); }
	inline int32_t get__graphicsMemorySize_11() const { return ____graphicsMemorySize_11; }
	inline int32_t* get_address_of__graphicsMemorySize_11() { return &____graphicsMemorySize_11; }
	inline void set__graphicsMemorySize_11(int32_t value)
	{
		____graphicsMemorySize_11 = value;
	}

	inline static int32_t get_offset_of__processorCount_12() { return static_cast<int32_t>(offsetof(Benchmark_t2672107741, ____processorCount_12)); }
	inline int32_t get__processorCount_12() const { return ____processorCount_12; }
	inline int32_t* get_address_of__processorCount_12() { return &____processorCount_12; }
	inline void set__processorCount_12(int32_t value)
	{
		____processorCount_12 = value;
	}

	inline static int32_t get_offset_of__supportShadows_13() { return static_cast<int32_t>(offsetof(Benchmark_t2672107741, ____supportShadows_13)); }
	inline bool get__supportShadows_13() const { return ____supportShadows_13; }
	inline bool* get_address_of__supportShadows_13() { return &____supportShadows_13; }
	inline void set__supportShadows_13(bool value)
	{
		____supportShadows_13 = value;
	}

	inline static int32_t get_offset_of__systemMemorySize_14() { return static_cast<int32_t>(offsetof(Benchmark_t2672107741, ____systemMemorySize_14)); }
	inline int32_t get__systemMemorySize_14() const { return ____systemMemorySize_14; }
	inline int32_t* get_address_of__systemMemorySize_14() { return &____systemMemorySize_14; }
	inline void set__systemMemorySize_14(int32_t value)
	{
		____systemMemorySize_14 = value;
	}

	inline static int32_t get_offset_of__benchmarkInfo_15() { return static_cast<int32_t>(offsetof(Benchmark_t2672107741, ____benchmarkInfo_15)); }
	inline BenchmarkInfo_t940577717 * get__benchmarkInfo_15() const { return ____benchmarkInfo_15; }
	inline BenchmarkInfo_t940577717 ** get_address_of__benchmarkInfo_15() { return &____benchmarkInfo_15; }
	inline void set__benchmarkInfo_15(BenchmarkInfo_t940577717 * value)
	{
		____benchmarkInfo_15 = value;
		Il2CppCodeGenWriteBarrier(&____benchmarkInfo_15, value);
	}

	inline static int32_t get_offset_of__lowPerformanceDevice_16() { return static_cast<int32_t>(offsetof(Benchmark_t2672107741, ____lowPerformanceDevice_16)); }
	inline List_1_t1375417109 * get__lowPerformanceDevice_16() const { return ____lowPerformanceDevice_16; }
	inline List_1_t1375417109 ** get_address_of__lowPerformanceDevice_16() { return &____lowPerformanceDevice_16; }
	inline void set__lowPerformanceDevice_16(List_1_t1375417109 * value)
	{
		____lowPerformanceDevice_16 = value;
		Il2CppCodeGenWriteBarrier(&____lowPerformanceDevice_16, value);
	}

	inline static int32_t get_offset_of__midPerformanceDevice_17() { return static_cast<int32_t>(offsetof(Benchmark_t2672107741, ____midPerformanceDevice_17)); }
	inline List_1_t1375417109 * get__midPerformanceDevice_17() const { return ____midPerformanceDevice_17; }
	inline List_1_t1375417109 ** get_address_of__midPerformanceDevice_17() { return &____midPerformanceDevice_17; }
	inline void set__midPerformanceDevice_17(List_1_t1375417109 * value)
	{
		____midPerformanceDevice_17 = value;
		Il2CppCodeGenWriteBarrier(&____midPerformanceDevice_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
