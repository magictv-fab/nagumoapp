﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenScaleBy
struct iTweenScaleBy_t1692844877;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenScaleBy::.ctor()
extern "C"  void iTweenScaleBy__ctor_m2067461257 (iTweenScaleBy_t1692844877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleBy::Reset()
extern "C"  void iTweenScaleBy_Reset_m4008861494 (iTweenScaleBy_t1692844877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleBy::OnEnter()
extern "C"  void iTweenScaleBy_OnEnter_m1947105824 (iTweenScaleBy_t1692844877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleBy::OnExit()
extern "C"  void iTweenScaleBy_OnExit_m1180108152 (iTweenScaleBy_t1692844877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleBy::DoiTween()
extern "C"  void iTweenScaleBy_DoiTween_m1681870632 (iTweenScaleBy_t1692844877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
