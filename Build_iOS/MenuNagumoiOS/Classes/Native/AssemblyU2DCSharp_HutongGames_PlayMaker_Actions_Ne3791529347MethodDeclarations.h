﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkGetPlayerPing
struct NetworkGetPlayerPing_t3791529347;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkGetPlayerPing::.ctor()
extern "C"  void NetworkGetPlayerPing__ctor_m921269315 (NetworkGetPlayerPing_t3791529347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetPlayerPing::Reset()
extern "C"  void NetworkGetPlayerPing_Reset_m2862669552 (NetworkGetPlayerPing_t3791529347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetPlayerPing::OnEnter()
extern "C"  void NetworkGetPlayerPing_OnEnter_m4263244634 (NetworkGetPlayerPing_t3791529347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetPlayerPing::OnUpdate()
extern "C"  void NetworkGetPlayerPing_OnUpdate_m2445124137 (NetworkGetPlayerPing_t3791529347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetPlayerPing::GetAveragePing()
extern "C"  void NetworkGetPlayerPing_GetAveragePing_m1043187290 (NetworkGetPlayerPing_t3791529347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
