﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.EntryPatchData
struct EntryPatchData_t4202945134;

#include "codegen/il2cpp-codegen.h"

// System.Void ICSharpCode.SharpZipLib.Zip.EntryPatchData::.ctor()
extern "C"  void EntryPatchData__ctor_m983433981 (EntryPatchData_t4202945134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.EntryPatchData::get_SizePatchOffset()
extern "C"  int64_t EntryPatchData_get_SizePatchOffset_m2367442941 (EntryPatchData_t4202945134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.EntryPatchData::set_SizePatchOffset(System.Int64)
extern "C"  void EntryPatchData_set_SizePatchOffset_m508773002 (EntryPatchData_t4202945134 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.EntryPatchData::get_CrcPatchOffset()
extern "C"  int64_t EntryPatchData_get_CrcPatchOffset_m2642049606 (EntryPatchData_t4202945134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.EntryPatchData::set_CrcPatchOffset(System.Int64)
extern "C"  void EntryPatchData_set_CrcPatchOffset_m3172968829 (EntryPatchData_t4202945134 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
