﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SendJson
struct SendJson_t1311805616;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void SendJson::.ctor()
extern "C"  void SendJson__ctor_m4067423211 (SendJson_t1311805616 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SendJson::Start()
extern "C"  void SendJson_Start_m3014561003 (SendJson_t1311805616 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SendJson::Update()
extern "C"  void SendJson_Update_m3262930050 (SendJson_t1311805616 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SendJson::SendJsonBtn()
extern "C"  void SendJson_SendJsonBtn_m1130219445 (SendJson_t1311805616 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SendJson::ISendJson()
extern "C"  Il2CppObject * SendJson_ISendJson_m443632026 (SendJson_t1311805616 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
