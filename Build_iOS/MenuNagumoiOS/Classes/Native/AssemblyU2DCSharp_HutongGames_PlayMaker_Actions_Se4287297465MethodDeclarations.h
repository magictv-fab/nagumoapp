﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAnimatorBody
struct SetAnimatorBody_t4287297465;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAnimatorBody::.ctor()
extern "C"  void SetAnimatorBody__ctor_m1099161501 (SetAnimatorBody_t4287297465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorBody::Reset()
extern "C"  void SetAnimatorBody_Reset_m3040561738 (SetAnimatorBody_t4287297465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorBody::OnEnter()
extern "C"  void SetAnimatorBody_OnEnter_m3418943540 (SetAnimatorBody_t4287297465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorBody::OnAnimatorIKEvent(System.Int32)
extern "C"  void SetAnimatorBody_OnAnimatorIKEvent_m2390189310 (SetAnimatorBody_t4287297465 * __this, int32_t ___layer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorBody::OnUpdate()
extern "C"  void SetAnimatorBody_OnUpdate_m2041593999 (SetAnimatorBody_t4287297465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorBody::DoSetBody()
extern "C"  void SetAnimatorBody_DoSetBody_m3301934996 (SetAnimatorBody_t4287297465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorBody::OnExit()
extern "C"  void SetAnimatorBody_OnExit_m1227586788 (SetAnimatorBody_t4287297465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
