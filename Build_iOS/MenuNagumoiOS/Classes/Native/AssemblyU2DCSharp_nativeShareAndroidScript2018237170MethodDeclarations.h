﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// nativeShareAndroidScript
struct nativeShareAndroidScript_t2018237170;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void nativeShareAndroidScript::.ctor()
extern "C"  void nativeShareAndroidScript__ctor_m2769806313 (nativeShareAndroidScript_t2018237170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void nativeShareAndroidScript::ShareBtnPress()
extern "C"  void nativeShareAndroidScript_ShareBtnPress_m2870681037 (nativeShareAndroidScript_t2018237170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator nativeShareAndroidScript::ShareScreenshot()
extern "C"  Il2CppObject * nativeShareAndroidScript_ShareScreenshot_m2435280804 (nativeShareAndroidScript_t2018237170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void nativeShareAndroidScript::OnApplicationFocus(System.Boolean)
extern "C"  void nativeShareAndroidScript_OnApplicationFocus_m276825177 (nativeShareAndroidScript_t2018237170 * __this, bool ___focus0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator nativeShareAndroidScript::ShowPanelOk()
extern "C"  Il2CppObject * nativeShareAndroidScript_ShowPanelOk_m3139592642 (nativeShareAndroidScript_t2018237170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
