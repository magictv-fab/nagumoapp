﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3363211663MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.vo.BundleVO[]>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2916129736(__this, ___dictionary0, method) ((  void (*) (Enumerator_t5666566 *, Dictionary_2_t2983310470 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.vo.BundleVO[]>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1341318361(__this, method) ((  Il2CppObject * (*) (Enumerator_t5666566 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.vo.BundleVO[]>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1414932461(__this, method) ((  void (*) (Enumerator_t5666566 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.vo.BundleVO[]>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1823658678(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t5666566 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.vo.BundleVO[]>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3287243509(__this, method) ((  Il2CppObject * (*) (Enumerator_t5666566 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.vo.BundleVO[]>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3288485959(__this, method) ((  Il2CppObject * (*) (Enumerator_t5666566 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.vo.BundleVO[]>::MoveNext()
#define Enumerator_MoveNext_m2405827673(__this, method) ((  bool (*) (Enumerator_t5666566 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.vo.BundleVO[]>::get_Current()
#define Enumerator_get_Current_m3217803575(__this, method) ((  KeyValuePair_2_t2882091176  (*) (Enumerator_t5666566 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.vo.BundleVO[]>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m47118182(__this, method) ((  String_t* (*) (Enumerator_t5666566 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.vo.BundleVO[]>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2293672074(__this, method) ((  BundleVOU5BU5D_t2162892100* (*) (Enumerator_t5666566 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.vo.BundleVO[]>::Reset()
#define Enumerator_Reset_m202639898(__this, method) ((  void (*) (Enumerator_t5666566 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.vo.BundleVO[]>::VerifyState()
#define Enumerator_VerifyState_m1020911267(__this, method) ((  void (*) (Enumerator_t5666566 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.vo.BundleVO[]>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m293088587(__this, method) ((  void (*) (Enumerator_t5666566 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,MagicTV.vo.BundleVO[]>::Dispose()
#define Enumerator_Dispose_m3986842730(__this, method) ((  void (*) (Enumerator_t5666566 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
