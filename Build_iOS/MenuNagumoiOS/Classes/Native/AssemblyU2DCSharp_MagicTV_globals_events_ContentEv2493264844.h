﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t3771233898;
// MagicTV.globals.events.ContentEvents
struct ContentEvents_t2493264844;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.globals.events.ContentEvents
struct  ContentEvents_t2493264844  : public Il2CppObject
{
public:
	// System.Action MagicTV.globals.events.ContentEvents::onUpdate
	Action_t3771233898 * ___onUpdate_0;
	// System.Action MagicTV.globals.events.ContentEvents::onUpdateComplete
	Action_t3771233898 * ___onUpdateComplete_1;

public:
	inline static int32_t get_offset_of_onUpdate_0() { return static_cast<int32_t>(offsetof(ContentEvents_t2493264844, ___onUpdate_0)); }
	inline Action_t3771233898 * get_onUpdate_0() const { return ___onUpdate_0; }
	inline Action_t3771233898 ** get_address_of_onUpdate_0() { return &___onUpdate_0; }
	inline void set_onUpdate_0(Action_t3771233898 * value)
	{
		___onUpdate_0 = value;
		Il2CppCodeGenWriteBarrier(&___onUpdate_0, value);
	}

	inline static int32_t get_offset_of_onUpdateComplete_1() { return static_cast<int32_t>(offsetof(ContentEvents_t2493264844, ___onUpdateComplete_1)); }
	inline Action_t3771233898 * get_onUpdateComplete_1() const { return ___onUpdateComplete_1; }
	inline Action_t3771233898 ** get_address_of_onUpdateComplete_1() { return &___onUpdateComplete_1; }
	inline void set_onUpdateComplete_1(Action_t3771233898 * value)
	{
		___onUpdateComplete_1 = value;
		Il2CppCodeGenWriteBarrier(&___onUpdateComplete_1, value);
	}
};

struct ContentEvents_t2493264844_StaticFields
{
public:
	// MagicTV.globals.events.ContentEvents MagicTV.globals.events.ContentEvents::_instance
	ContentEvents_t2493264844 * ____instance_2;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(ContentEvents_t2493264844_StaticFields, ____instance_2)); }
	inline ContentEvents_t2493264844 * get__instance_2() const { return ____instance_2; }
	inline ContentEvents_t2493264844 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(ContentEvents_t2493264844 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier(&____instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
