﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttachedAudio
struct AttachedAudio_t711869554;

#include "codegen/il2cpp-codegen.h"

// System.Void AttachedAudio::.ctor()
extern "C"  void AttachedAudio__ctor_m3805084985 (AttachedAudio_t711869554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttachedAudio::Play()
extern "C"  void AttachedAudio_Play_m1519530783 (AttachedAudio_t711869554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttachedAudio::Mute()
extern "C"  void AttachedAudio_Mute_m1442501828 (AttachedAudio_t711869554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttachedAudio::UnMute()
extern "C"  void AttachedAudio_UnMute_m1305372221 (AttachedAudio_t711869554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttachedAudio::Stop()
extern "C"  void AttachedAudio_Stop_m1613214829 (AttachedAudio_t711869554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttachedAudio::Toggle()
extern "C"  void AttachedAudio_Toggle_m1181255871 (AttachedAudio_t711869554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttachedAudio::Sync()
extern "C"  void AttachedAudio_Sync_m1617790150 (AttachedAudio_t711869554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
