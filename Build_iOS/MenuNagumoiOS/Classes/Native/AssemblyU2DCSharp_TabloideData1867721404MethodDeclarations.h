﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TabloideData
struct TabloideData_t1867721404;

#include "codegen/il2cpp-codegen.h"

// System.Void TabloideData::.ctor()
extern "C"  void TabloideData__ctor_m408099679 (TabloideData_t1867721404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
