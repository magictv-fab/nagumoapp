﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// System.Object
struct Il2CppObject;
// CaptureAndSave
struct CaptureAndSave_t700313070;

#include "mscorlib_System_Object4170816371.h"
#include "CaptureAndSaveLib_ImageType1125820181.h"
#include "UnityEngine_UnityEngine_TextureFormat4189619560.h"
#include "CaptureAndSaveLib_WatermarkAlignment1574127103.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CaptureAndSave/<GetScreenPixels>c__Iterator1
struct  U3CGetScreenPixelsU3Ec__Iterator1_t3827175978  : public Il2CppObject
{
public:
	// ImageType CaptureAndSave/<GetScreenPixels>c__Iterator1::imgType
	int32_t ___imgType_0;
	// UnityEngine.TextureFormat CaptureAndSave/<GetScreenPixels>c__Iterator1::<tFormat>__0
	int32_t ___U3CtFormatU3E__0_1;
	// System.Int32 CaptureAndSave/<GetScreenPixels>c__Iterator1::width
	int32_t ___width_2;
	// System.Int32 CaptureAndSave/<GetScreenPixels>c__Iterator1::height
	int32_t ___height_3;
	// System.Int32 CaptureAndSave/<GetScreenPixels>c__Iterator1::x
	int32_t ___x_4;
	// System.Int32 CaptureAndSave/<GetScreenPixels>c__Iterator1::y
	int32_t ___y_5;
	// UnityEngine.Texture2D CaptureAndSave/<GetScreenPixels>c__Iterator1::watermark
	Texture2D_t3884108195 * ___watermark_6;
	// UnityEngine.Texture2D CaptureAndSave/<GetScreenPixels>c__Iterator1::<temp>__1
	Texture2D_t3884108195 * ___U3CtempU3E__1_7;
	// WatermarkAlignment CaptureAndSave/<GetScreenPixels>c__Iterator1::align
	int32_t ___align_8;
	// System.Int32 CaptureAndSave/<GetScreenPixels>c__Iterator1::$PC
	int32_t ___U24PC_9;
	// System.Object CaptureAndSave/<GetScreenPixels>c__Iterator1::$current
	Il2CppObject * ___U24current_10;
	// ImageType CaptureAndSave/<GetScreenPixels>c__Iterator1::<$>imgType
	int32_t ___U3CU24U3EimgType_11;
	// System.Int32 CaptureAndSave/<GetScreenPixels>c__Iterator1::<$>width
	int32_t ___U3CU24U3Ewidth_12;
	// System.Int32 CaptureAndSave/<GetScreenPixels>c__Iterator1::<$>height
	int32_t ___U3CU24U3Eheight_13;
	// System.Int32 CaptureAndSave/<GetScreenPixels>c__Iterator1::<$>x
	int32_t ___U3CU24U3Ex_14;
	// System.Int32 CaptureAndSave/<GetScreenPixels>c__Iterator1::<$>y
	int32_t ___U3CU24U3Ey_15;
	// UnityEngine.Texture2D CaptureAndSave/<GetScreenPixels>c__Iterator1::<$>watermark
	Texture2D_t3884108195 * ___U3CU24U3Ewatermark_16;
	// WatermarkAlignment CaptureAndSave/<GetScreenPixels>c__Iterator1::<$>align
	int32_t ___U3CU24U3Ealign_17;
	// CaptureAndSave CaptureAndSave/<GetScreenPixels>c__Iterator1::<>f__this
	CaptureAndSave_t700313070 * ___U3CU3Ef__this_18;

public:
	inline static int32_t get_offset_of_imgType_0() { return static_cast<int32_t>(offsetof(U3CGetScreenPixelsU3Ec__Iterator1_t3827175978, ___imgType_0)); }
	inline int32_t get_imgType_0() const { return ___imgType_0; }
	inline int32_t* get_address_of_imgType_0() { return &___imgType_0; }
	inline void set_imgType_0(int32_t value)
	{
		___imgType_0 = value;
	}

	inline static int32_t get_offset_of_U3CtFormatU3E__0_1() { return static_cast<int32_t>(offsetof(U3CGetScreenPixelsU3Ec__Iterator1_t3827175978, ___U3CtFormatU3E__0_1)); }
	inline int32_t get_U3CtFormatU3E__0_1() const { return ___U3CtFormatU3E__0_1; }
	inline int32_t* get_address_of_U3CtFormatU3E__0_1() { return &___U3CtFormatU3E__0_1; }
	inline void set_U3CtFormatU3E__0_1(int32_t value)
	{
		___U3CtFormatU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_width_2() { return static_cast<int32_t>(offsetof(U3CGetScreenPixelsU3Ec__Iterator1_t3827175978, ___width_2)); }
	inline int32_t get_width_2() const { return ___width_2; }
	inline int32_t* get_address_of_width_2() { return &___width_2; }
	inline void set_width_2(int32_t value)
	{
		___width_2 = value;
	}

	inline static int32_t get_offset_of_height_3() { return static_cast<int32_t>(offsetof(U3CGetScreenPixelsU3Ec__Iterator1_t3827175978, ___height_3)); }
	inline int32_t get_height_3() const { return ___height_3; }
	inline int32_t* get_address_of_height_3() { return &___height_3; }
	inline void set_height_3(int32_t value)
	{
		___height_3 = value;
	}

	inline static int32_t get_offset_of_x_4() { return static_cast<int32_t>(offsetof(U3CGetScreenPixelsU3Ec__Iterator1_t3827175978, ___x_4)); }
	inline int32_t get_x_4() const { return ___x_4; }
	inline int32_t* get_address_of_x_4() { return &___x_4; }
	inline void set_x_4(int32_t value)
	{
		___x_4 = value;
	}

	inline static int32_t get_offset_of_y_5() { return static_cast<int32_t>(offsetof(U3CGetScreenPixelsU3Ec__Iterator1_t3827175978, ___y_5)); }
	inline int32_t get_y_5() const { return ___y_5; }
	inline int32_t* get_address_of_y_5() { return &___y_5; }
	inline void set_y_5(int32_t value)
	{
		___y_5 = value;
	}

	inline static int32_t get_offset_of_watermark_6() { return static_cast<int32_t>(offsetof(U3CGetScreenPixelsU3Ec__Iterator1_t3827175978, ___watermark_6)); }
	inline Texture2D_t3884108195 * get_watermark_6() const { return ___watermark_6; }
	inline Texture2D_t3884108195 ** get_address_of_watermark_6() { return &___watermark_6; }
	inline void set_watermark_6(Texture2D_t3884108195 * value)
	{
		___watermark_6 = value;
		Il2CppCodeGenWriteBarrier(&___watermark_6, value);
	}

	inline static int32_t get_offset_of_U3CtempU3E__1_7() { return static_cast<int32_t>(offsetof(U3CGetScreenPixelsU3Ec__Iterator1_t3827175978, ___U3CtempU3E__1_7)); }
	inline Texture2D_t3884108195 * get_U3CtempU3E__1_7() const { return ___U3CtempU3E__1_7; }
	inline Texture2D_t3884108195 ** get_address_of_U3CtempU3E__1_7() { return &___U3CtempU3E__1_7; }
	inline void set_U3CtempU3E__1_7(Texture2D_t3884108195 * value)
	{
		___U3CtempU3E__1_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtempU3E__1_7, value);
	}

	inline static int32_t get_offset_of_align_8() { return static_cast<int32_t>(offsetof(U3CGetScreenPixelsU3Ec__Iterator1_t3827175978, ___align_8)); }
	inline int32_t get_align_8() const { return ___align_8; }
	inline int32_t* get_address_of_align_8() { return &___align_8; }
	inline void set_align_8(int32_t value)
	{
		___align_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CGetScreenPixelsU3Ec__Iterator1_t3827175978, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CGetScreenPixelsU3Ec__Iterator1_t3827175978, ___U24current_10)); }
	inline Il2CppObject * get_U24current_10() const { return ___U24current_10; }
	inline Il2CppObject ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(Il2CppObject * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_10, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EimgType_11() { return static_cast<int32_t>(offsetof(U3CGetScreenPixelsU3Ec__Iterator1_t3827175978, ___U3CU24U3EimgType_11)); }
	inline int32_t get_U3CU24U3EimgType_11() const { return ___U3CU24U3EimgType_11; }
	inline int32_t* get_address_of_U3CU24U3EimgType_11() { return &___U3CU24U3EimgType_11; }
	inline void set_U3CU24U3EimgType_11(int32_t value)
	{
		___U3CU24U3EimgType_11 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Ewidth_12() { return static_cast<int32_t>(offsetof(U3CGetScreenPixelsU3Ec__Iterator1_t3827175978, ___U3CU24U3Ewidth_12)); }
	inline int32_t get_U3CU24U3Ewidth_12() const { return ___U3CU24U3Ewidth_12; }
	inline int32_t* get_address_of_U3CU24U3Ewidth_12() { return &___U3CU24U3Ewidth_12; }
	inline void set_U3CU24U3Ewidth_12(int32_t value)
	{
		___U3CU24U3Ewidth_12 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Eheight_13() { return static_cast<int32_t>(offsetof(U3CGetScreenPixelsU3Ec__Iterator1_t3827175978, ___U3CU24U3Eheight_13)); }
	inline int32_t get_U3CU24U3Eheight_13() const { return ___U3CU24U3Eheight_13; }
	inline int32_t* get_address_of_U3CU24U3Eheight_13() { return &___U3CU24U3Eheight_13; }
	inline void set_U3CU24U3Eheight_13(int32_t value)
	{
		___U3CU24U3Eheight_13 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Ex_14() { return static_cast<int32_t>(offsetof(U3CGetScreenPixelsU3Ec__Iterator1_t3827175978, ___U3CU24U3Ex_14)); }
	inline int32_t get_U3CU24U3Ex_14() const { return ___U3CU24U3Ex_14; }
	inline int32_t* get_address_of_U3CU24U3Ex_14() { return &___U3CU24U3Ex_14; }
	inline void set_U3CU24U3Ex_14(int32_t value)
	{
		___U3CU24U3Ex_14 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Ey_15() { return static_cast<int32_t>(offsetof(U3CGetScreenPixelsU3Ec__Iterator1_t3827175978, ___U3CU24U3Ey_15)); }
	inline int32_t get_U3CU24U3Ey_15() const { return ___U3CU24U3Ey_15; }
	inline int32_t* get_address_of_U3CU24U3Ey_15() { return &___U3CU24U3Ey_15; }
	inline void set_U3CU24U3Ey_15(int32_t value)
	{
		___U3CU24U3Ey_15 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Ewatermark_16() { return static_cast<int32_t>(offsetof(U3CGetScreenPixelsU3Ec__Iterator1_t3827175978, ___U3CU24U3Ewatermark_16)); }
	inline Texture2D_t3884108195 * get_U3CU24U3Ewatermark_16() const { return ___U3CU24U3Ewatermark_16; }
	inline Texture2D_t3884108195 ** get_address_of_U3CU24U3Ewatermark_16() { return &___U3CU24U3Ewatermark_16; }
	inline void set_U3CU24U3Ewatermark_16(Texture2D_t3884108195 * value)
	{
		___U3CU24U3Ewatermark_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ewatermark_16, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ealign_17() { return static_cast<int32_t>(offsetof(U3CGetScreenPixelsU3Ec__Iterator1_t3827175978, ___U3CU24U3Ealign_17)); }
	inline int32_t get_U3CU24U3Ealign_17() const { return ___U3CU24U3Ealign_17; }
	inline int32_t* get_address_of_U3CU24U3Ealign_17() { return &___U3CU24U3Ealign_17; }
	inline void set_U3CU24U3Ealign_17(int32_t value)
	{
		___U3CU24U3Ealign_17 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_18() { return static_cast<int32_t>(offsetof(U3CGetScreenPixelsU3Ec__Iterator1_t3827175978, ___U3CU3Ef__this_18)); }
	inline CaptureAndSave_t700313070 * get_U3CU3Ef__this_18() const { return ___U3CU3Ef__this_18; }
	inline CaptureAndSave_t700313070 ** get_address_of_U3CU3Ef__this_18() { return &___U3CU3Ef__this_18; }
	inline void set_U3CU3Ef__this_18(CaptureAndSave_t700313070 * value)
	{
		___U3CU3Ef__this_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
