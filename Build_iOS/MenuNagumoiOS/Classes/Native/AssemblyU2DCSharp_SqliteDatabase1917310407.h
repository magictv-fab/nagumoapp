﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SqliteDatabase
struct  SqliteDatabase_t1917310407  : public Il2CppObject
{
public:
	// System.Boolean SqliteDatabase::showDebug
	bool ___showDebug_8;
	// System.Boolean SqliteDatabase::CanExQuery
	bool ___CanExQuery_9;
	// System.IntPtr SqliteDatabase::_connection
	IntPtr_t ____connection_10;
	// System.String SqliteDatabase::pathDB
	String_t* ___pathDB_11;
	// System.Boolean SqliteDatabase::<IsConnectionOpen>k__BackingField
	bool ___U3CIsConnectionOpenU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_showDebug_8() { return static_cast<int32_t>(offsetof(SqliteDatabase_t1917310407, ___showDebug_8)); }
	inline bool get_showDebug_8() const { return ___showDebug_8; }
	inline bool* get_address_of_showDebug_8() { return &___showDebug_8; }
	inline void set_showDebug_8(bool value)
	{
		___showDebug_8 = value;
	}

	inline static int32_t get_offset_of_CanExQuery_9() { return static_cast<int32_t>(offsetof(SqliteDatabase_t1917310407, ___CanExQuery_9)); }
	inline bool get_CanExQuery_9() const { return ___CanExQuery_9; }
	inline bool* get_address_of_CanExQuery_9() { return &___CanExQuery_9; }
	inline void set_CanExQuery_9(bool value)
	{
		___CanExQuery_9 = value;
	}

	inline static int32_t get_offset_of__connection_10() { return static_cast<int32_t>(offsetof(SqliteDatabase_t1917310407, ____connection_10)); }
	inline IntPtr_t get__connection_10() const { return ____connection_10; }
	inline IntPtr_t* get_address_of__connection_10() { return &____connection_10; }
	inline void set__connection_10(IntPtr_t value)
	{
		____connection_10 = value;
	}

	inline static int32_t get_offset_of_pathDB_11() { return static_cast<int32_t>(offsetof(SqliteDatabase_t1917310407, ___pathDB_11)); }
	inline String_t* get_pathDB_11() const { return ___pathDB_11; }
	inline String_t** get_address_of_pathDB_11() { return &___pathDB_11; }
	inline void set_pathDB_11(String_t* value)
	{
		___pathDB_11 = value;
		Il2CppCodeGenWriteBarrier(&___pathDB_11, value);
	}

	inline static int32_t get_offset_of_U3CIsConnectionOpenU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(SqliteDatabase_t1917310407, ___U3CIsConnectionOpenU3Ek__BackingField_12)); }
	inline bool get_U3CIsConnectionOpenU3Ek__BackingField_12() const { return ___U3CIsConnectionOpenU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CIsConnectionOpenU3Ek__BackingField_12() { return &___U3CIsConnectionOpenU3Ek__BackingField_12; }
	inline void set_U3CIsConnectionOpenU3Ek__BackingField_12(bool value)
	{
		___U3CIsConnectionOpenU3Ek__BackingField_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
