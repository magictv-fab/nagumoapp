﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Datamatrix.Encoder.SymbolInfo
struct SymbolInfo_t553159586;
// ZXing.Dimension
struct Dimension_t1395692488;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Datamatrix_Encoder_SymbolShapeHint2380532246.h"
#include "QRCode_ZXing_Dimension1395692488.h"

// System.Void ZXing.Datamatrix.Encoder.SymbolInfo::.ctor(System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void SymbolInfo__ctor_m1188403014 (SymbolInfo_t553159586 * __this, bool ___rectangular0, int32_t ___dataCapacity1, int32_t ___errorCodewords2, int32_t ___matrixWidth3, int32_t ___matrixHeight4, int32_t ___dataRegions5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.SymbolInfo::.ctor(System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void SymbolInfo__ctor_m399720742 (SymbolInfo_t553159586 * __this, bool ___rectangular0, int32_t ___dataCapacity1, int32_t ___errorCodewords2, int32_t ___matrixWidth3, int32_t ___matrixHeight4, int32_t ___dataRegions5, int32_t ___rsBlockData6, int32_t ___rsBlockError7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Datamatrix.Encoder.SymbolInfo ZXing.Datamatrix.Encoder.SymbolInfo::lookup(System.Int32,ZXing.Datamatrix.Encoder.SymbolShapeHint,ZXing.Dimension,ZXing.Dimension,System.Boolean)
extern "C"  SymbolInfo_t553159586 * SymbolInfo_lookup_m233650991 (Il2CppObject * __this /* static, unused */, int32_t ___dataCodewords0, int32_t ___shape1, Dimension_t1395692488 * ___minSize2, Dimension_t1395692488 * ___maxSize3, bool ___fail4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::getHorizontalDataRegions()
extern "C"  int32_t SymbolInfo_getHorizontalDataRegions_m1555516183 (SymbolInfo_t553159586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::getVerticalDataRegions()
extern "C"  int32_t SymbolInfo_getVerticalDataRegions_m2214308421 (SymbolInfo_t553159586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::getSymbolDataWidth()
extern "C"  int32_t SymbolInfo_getSymbolDataWidth_m2390283626 (SymbolInfo_t553159586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::getSymbolDataHeight()
extern "C"  int32_t SymbolInfo_getSymbolDataHeight_m1588162725 (SymbolInfo_t553159586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::getSymbolWidth()
extern "C"  int32_t SymbolInfo_getSymbolWidth_m2370232052 (SymbolInfo_t553159586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::getSymbolHeight()
extern "C"  int32_t SymbolInfo_getSymbolHeight_m966563931 (SymbolInfo_t553159586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::getInterleavedBlockCount()
extern "C"  int32_t SymbolInfo_getInterleavedBlockCount_m4142404657 (SymbolInfo_t553159586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::getDataLengthForInterleavedBlock(System.Int32)
extern "C"  int32_t SymbolInfo_getDataLengthForInterleavedBlock_m2517766388 (SymbolInfo_t553159586 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::getErrorLengthForInterleavedBlock(System.Int32)
extern "C"  int32_t SymbolInfo_getErrorLengthForInterleavedBlock_m471701036 (SymbolInfo_t553159586 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.Datamatrix.Encoder.SymbolInfo::ToString()
extern "C"  String_t* SymbolInfo_ToString_m2257807219 (SymbolInfo_t553159586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.SymbolInfo::.cctor()
extern "C"  void SymbolInfo__cctor_m3285447155 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
