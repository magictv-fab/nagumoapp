﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScrollDetailTexture
struct ScrollDetailTexture_t3150826909;

#include "codegen/il2cpp-codegen.h"

// System.Void ScrollDetailTexture::.ctor()
extern "C"  void ScrollDetailTexture__ctor_m2106672750 (ScrollDetailTexture_t3150826909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollDetailTexture::OnEnable()
extern "C"  void ScrollDetailTexture_OnEnable_m3897344600 (ScrollDetailTexture_t3150826909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollDetailTexture::OnDisable()
extern "C"  void ScrollDetailTexture_OnDisable_m999535573 (ScrollDetailTexture_t3150826909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollDetailTexture::Update()
extern "C"  void ScrollDetailTexture_Update_m2609207903 (ScrollDetailTexture_t3150826909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
