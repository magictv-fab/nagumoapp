﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UpdateTransformMetadata
struct  UpdateTransformMetadata_t2275182802  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 UpdateTransformMetadata::bundle_id
	int32_t ___bundle_id_2;
	// UnityEngine.Rect UpdateTransformMetadata::buttonSize
	Rect_t4241904616  ___buttonSize_3;
	// UnityEngine.GameObject UpdateTransformMetadata::objetoToSendTransform
	GameObject_t3674682005 * ___objetoToSendTransform_4;
	// System.String UpdateTransformMetadata::result
	String_t* ___result_5;
	// System.Boolean UpdateTransformMetadata::_enviando
	bool ____enviando_6;

public:
	inline static int32_t get_offset_of_bundle_id_2() { return static_cast<int32_t>(offsetof(UpdateTransformMetadata_t2275182802, ___bundle_id_2)); }
	inline int32_t get_bundle_id_2() const { return ___bundle_id_2; }
	inline int32_t* get_address_of_bundle_id_2() { return &___bundle_id_2; }
	inline void set_bundle_id_2(int32_t value)
	{
		___bundle_id_2 = value;
	}

	inline static int32_t get_offset_of_buttonSize_3() { return static_cast<int32_t>(offsetof(UpdateTransformMetadata_t2275182802, ___buttonSize_3)); }
	inline Rect_t4241904616  get_buttonSize_3() const { return ___buttonSize_3; }
	inline Rect_t4241904616 * get_address_of_buttonSize_3() { return &___buttonSize_3; }
	inline void set_buttonSize_3(Rect_t4241904616  value)
	{
		___buttonSize_3 = value;
	}

	inline static int32_t get_offset_of_objetoToSendTransform_4() { return static_cast<int32_t>(offsetof(UpdateTransformMetadata_t2275182802, ___objetoToSendTransform_4)); }
	inline GameObject_t3674682005 * get_objetoToSendTransform_4() const { return ___objetoToSendTransform_4; }
	inline GameObject_t3674682005 ** get_address_of_objetoToSendTransform_4() { return &___objetoToSendTransform_4; }
	inline void set_objetoToSendTransform_4(GameObject_t3674682005 * value)
	{
		___objetoToSendTransform_4 = value;
		Il2CppCodeGenWriteBarrier(&___objetoToSendTransform_4, value);
	}

	inline static int32_t get_offset_of_result_5() { return static_cast<int32_t>(offsetof(UpdateTransformMetadata_t2275182802, ___result_5)); }
	inline String_t* get_result_5() const { return ___result_5; }
	inline String_t** get_address_of_result_5() { return &___result_5; }
	inline void set_result_5(String_t* value)
	{
		___result_5 = value;
		Il2CppCodeGenWriteBarrier(&___result_5, value);
	}

	inline static int32_t get_offset_of__enviando_6() { return static_cast<int32_t>(offsetof(UpdateTransformMetadata_t2275182802, ____enviando_6)); }
	inline bool get__enviando_6() const { return ____enviando_6; }
	inline bool* get_address_of__enviando_6() { return &____enviando_6; }
	inline void set__enviando_6(bool value)
	{
		____enviando_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
