﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer
struct InflaterInputBuffer_t441930877;
// System.IO.Stream
struct Stream_t1561764144;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// ICSharpCode.SharpZipLib.Zip.Compression.Inflater
struct Inflater_t1975778921;
// System.Security.Cryptography.ICryptoTransform
struct ICryptoTransform_t153068654;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Comp1975778921.h"

// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::.ctor(System.IO.Stream)
extern "C"  void InflaterInputBuffer__ctor_m3319100804 (InflaterInputBuffer_t441930877 * __this, Stream_t1561764144 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::.ctor(System.IO.Stream,System.Int32)
extern "C"  void InflaterInputBuffer__ctor_m3740253651 (InflaterInputBuffer_t441930877 * __this, Stream_t1561764144 * ___stream0, int32_t ___bufferSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::get_RawLength()
extern "C"  int32_t InflaterInputBuffer_get_RawLength_m143695198 (InflaterInputBuffer_t441930877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::get_RawData()
extern "C"  ByteU5BU5D_t4260760469* InflaterInputBuffer_get_RawData_m164928702 (InflaterInputBuffer_t441930877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::get_ClearTextLength()
extern "C"  int32_t InflaterInputBuffer_get_ClearTextLength_m3672790288 (InflaterInputBuffer_t441930877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::get_ClearText()
extern "C"  ByteU5BU5D_t4260760469* InflaterInputBuffer_get_ClearText_m1369490726 (InflaterInputBuffer_t441930877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::get_Available()
extern "C"  int32_t InflaterInputBuffer_get_Available_m2446986297 (InflaterInputBuffer_t441930877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::set_Available(System.Int32)
extern "C"  void InflaterInputBuffer_set_Available_m3015639816 (InflaterInputBuffer_t441930877 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::SetInflaterInput(ICSharpCode.SharpZipLib.Zip.Compression.Inflater)
extern "C"  void InflaterInputBuffer_SetInflaterInput_m621699203 (InflaterInputBuffer_t441930877 * __this, Inflater_t1975778921 * ___inflater0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::Fill()
extern "C"  void InflaterInputBuffer_Fill_m1269322618 (InflaterInputBuffer_t441930877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadRawBuffer(System.Byte[])
extern "C"  int32_t InflaterInputBuffer_ReadRawBuffer_m1105775998 (InflaterInputBuffer_t441930877 * __this, ByteU5BU5D_t4260760469* ___buffer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadRawBuffer(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t InflaterInputBuffer_ReadRawBuffer_m879403358 (InflaterInputBuffer_t441930877 * __this, ByteU5BU5D_t4260760469* ___outBuffer0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadClearTextBuffer(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t InflaterInputBuffer_ReadClearTextBuffer_m3683356588 (InflaterInputBuffer_t441930877 * __this, ByteU5BU5D_t4260760469* ___outBuffer0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadLeByte()
extern "C"  int32_t InflaterInputBuffer_ReadLeByte_m626278272 (InflaterInputBuffer_t441930877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadLeShort()
extern "C"  int32_t InflaterInputBuffer_ReadLeShort_m3946565094 (InflaterInputBuffer_t441930877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadLeInt()
extern "C"  int32_t InflaterInputBuffer_ReadLeInt_m1550358233 (InflaterInputBuffer_t441930877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadLeLong()
extern "C"  int64_t InflaterInputBuffer_ReadLeLong_m1556801171 (InflaterInputBuffer_t441930877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::set_CryptoTransform(System.Security.Cryptography.ICryptoTransform)
extern "C"  void InflaterInputBuffer_set_CryptoTransform_m2363594468 (InflaterInputBuffer_t441930877 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
