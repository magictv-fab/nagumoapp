﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// UnityEngine.WWW
struct WWW_t3134621005;
// PublicationData[]
struct PublicationDataU5BU5D_t1155489811;
// PublicationData
struct PublicationData_t473548758;
// System.Exception
struct Exception_t3991598821;
// System.Object
struct Il2CppObject;
// PublishManager
struct PublishManager_t4010203070;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PublishManager/<ILoadPublish>c__Iterator77
struct  U3CILoadPublishU3Ec__Iterator77_t1612164062  : public Il2CppObject
{
public:
	// System.String PublishManager/<ILoadPublish>c__Iterator77::<json>__0
	String_t* ___U3CjsonU3E__0_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> PublishManager/<ILoadPublish>c__Iterator77::<headers>__1
	Dictionary_2_t827649927 * ___U3CheadersU3E__1_1;
	// System.Byte[] PublishManager/<ILoadPublish>c__Iterator77::<pData>__2
	ByteU5BU5D_t4260760469* ___U3CpDataU3E__2_2;
	// UnityEngine.WWW PublishManager/<ILoadPublish>c__Iterator77::<www>__3
	WWW_t3134621005 * ___U3CwwwU3E__3_3;
	// System.String PublishManager/<ILoadPublish>c__Iterator77::<message>__4
	String_t* ___U3CmessageU3E__4_4;
	// PublicationData[] PublishManager/<ILoadPublish>c__Iterator77::<$s_340>__5
	PublicationDataU5BU5D_t1155489811* ___U3CU24s_340U3E__5_5;
	// System.Int32 PublishManager/<ILoadPublish>c__Iterator77::<$s_341>__6
	int32_t ___U3CU24s_341U3E__6_6;
	// PublicationData PublishManager/<ILoadPublish>c__Iterator77::<publicationData>__7
	PublicationData_t473548758 * ___U3CpublicationDataU3E__7_7;
	// System.Exception PublishManager/<ILoadPublish>c__Iterator77::<ex>__8
	Exception_t3991598821 * ___U3CexU3E__8_8;
	// System.Int32 PublishManager/<ILoadPublish>c__Iterator77::$PC
	int32_t ___U24PC_9;
	// System.Object PublishManager/<ILoadPublish>c__Iterator77::$current
	Il2CppObject * ___U24current_10;
	// PublishManager PublishManager/<ILoadPublish>c__Iterator77::<>f__this
	PublishManager_t4010203070 * ___U3CU3Ef__this_11;

public:
	inline static int32_t get_offset_of_U3CjsonU3E__0_0() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator77_t1612164062, ___U3CjsonU3E__0_0)); }
	inline String_t* get_U3CjsonU3E__0_0() const { return ___U3CjsonU3E__0_0; }
	inline String_t** get_address_of_U3CjsonU3E__0_0() { return &___U3CjsonU3E__0_0; }
	inline void set_U3CjsonU3E__0_0(String_t* value)
	{
		___U3CjsonU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjsonU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CheadersU3E__1_1() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator77_t1612164062, ___U3CheadersU3E__1_1)); }
	inline Dictionary_2_t827649927 * get_U3CheadersU3E__1_1() const { return ___U3CheadersU3E__1_1; }
	inline Dictionary_2_t827649927 ** get_address_of_U3CheadersU3E__1_1() { return &___U3CheadersU3E__1_1; }
	inline void set_U3CheadersU3E__1_1(Dictionary_2_t827649927 * value)
	{
		___U3CheadersU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CheadersU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CpDataU3E__2_2() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator77_t1612164062, ___U3CpDataU3E__2_2)); }
	inline ByteU5BU5D_t4260760469* get_U3CpDataU3E__2_2() const { return ___U3CpDataU3E__2_2; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CpDataU3E__2_2() { return &___U3CpDataU3E__2_2; }
	inline void set_U3CpDataU3E__2_2(ByteU5BU5D_t4260760469* value)
	{
		___U3CpDataU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpDataU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__3_3() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator77_t1612164062, ___U3CwwwU3E__3_3)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__3_3() const { return ___U3CwwwU3E__3_3; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__3_3() { return &___U3CwwwU3E__3_3; }
	inline void set_U3CwwwU3E__3_3(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CmessageU3E__4_4() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator77_t1612164062, ___U3CmessageU3E__4_4)); }
	inline String_t* get_U3CmessageU3E__4_4() const { return ___U3CmessageU3E__4_4; }
	inline String_t** get_address_of_U3CmessageU3E__4_4() { return &___U3CmessageU3E__4_4; }
	inline void set_U3CmessageU3E__4_4(String_t* value)
	{
		___U3CmessageU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmessageU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U3CU24s_340U3E__5_5() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator77_t1612164062, ___U3CU24s_340U3E__5_5)); }
	inline PublicationDataU5BU5D_t1155489811* get_U3CU24s_340U3E__5_5() const { return ___U3CU24s_340U3E__5_5; }
	inline PublicationDataU5BU5D_t1155489811** get_address_of_U3CU24s_340U3E__5_5() { return &___U3CU24s_340U3E__5_5; }
	inline void set_U3CU24s_340U3E__5_5(PublicationDataU5BU5D_t1155489811* value)
	{
		___U3CU24s_340U3E__5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_340U3E__5_5, value);
	}

	inline static int32_t get_offset_of_U3CU24s_341U3E__6_6() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator77_t1612164062, ___U3CU24s_341U3E__6_6)); }
	inline int32_t get_U3CU24s_341U3E__6_6() const { return ___U3CU24s_341U3E__6_6; }
	inline int32_t* get_address_of_U3CU24s_341U3E__6_6() { return &___U3CU24s_341U3E__6_6; }
	inline void set_U3CU24s_341U3E__6_6(int32_t value)
	{
		___U3CU24s_341U3E__6_6 = value;
	}

	inline static int32_t get_offset_of_U3CpublicationDataU3E__7_7() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator77_t1612164062, ___U3CpublicationDataU3E__7_7)); }
	inline PublicationData_t473548758 * get_U3CpublicationDataU3E__7_7() const { return ___U3CpublicationDataU3E__7_7; }
	inline PublicationData_t473548758 ** get_address_of_U3CpublicationDataU3E__7_7() { return &___U3CpublicationDataU3E__7_7; }
	inline void set_U3CpublicationDataU3E__7_7(PublicationData_t473548758 * value)
	{
		___U3CpublicationDataU3E__7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpublicationDataU3E__7_7, value);
	}

	inline static int32_t get_offset_of_U3CexU3E__8_8() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator77_t1612164062, ___U3CexU3E__8_8)); }
	inline Exception_t3991598821 * get_U3CexU3E__8_8() const { return ___U3CexU3E__8_8; }
	inline Exception_t3991598821 ** get_address_of_U3CexU3E__8_8() { return &___U3CexU3E__8_8; }
	inline void set_U3CexU3E__8_8(Exception_t3991598821 * value)
	{
		___U3CexU3E__8_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CexU3E__8_8, value);
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator77_t1612164062, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator77_t1612164062, ___U24current_10)); }
	inline Il2CppObject * get_U24current_10() const { return ___U24current_10; }
	inline Il2CppObject ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(Il2CppObject * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_11() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator77_t1612164062, ___U3CU3Ef__this_11)); }
	inline PublishManager_t4010203070 * get_U3CU3Ef__this_11() const { return ___U3CU3Ef__this_11; }
	inline PublishManager_t4010203070 ** get_address_of_U3CU3Ef__this_11() { return &___U3CU3Ef__this_11; }
	inline void set_U3CU3Ef__this_11(PublishManager_t4010203070 * value)
	{
		___U3CU3Ef__this_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
