﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.QrCode.Internal.ErrorCorrectionLevel
struct ErrorCorrectionLevel_t1225927610;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ZXing.QrCode.Internal.ErrorCorrectionLevel::.ctor(System.Int32,System.Int32,System.String)
extern "C"  void ErrorCorrectionLevel__ctor_m2827675936 (ErrorCorrectionLevel_t1225927610 * __this, int32_t ___ordinal0, int32_t ___bits1, String_t* ___name2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.QrCode.Internal.ErrorCorrectionLevel::get_Bits()
extern "C"  int32_t ErrorCorrectionLevel_get_Bits_m574482527 (ErrorCorrectionLevel_t1225927610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.QrCode.Internal.ErrorCorrectionLevel::ordinal()
extern "C"  int32_t ErrorCorrectionLevel_ordinal_m3901815459 (ErrorCorrectionLevel_t1225927610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.QrCode.Internal.ErrorCorrectionLevel::ToString()
extern "C"  String_t* ErrorCorrectionLevel_ToString_m381154385 (ErrorCorrectionLevel_t1225927610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.QrCode.Internal.ErrorCorrectionLevel ZXing.QrCode.Internal.ErrorCorrectionLevel::forBits(System.Int32)
extern "C"  ErrorCorrectionLevel_t1225927610 * ErrorCorrectionLevel_forBits_m1438271484 (Il2CppObject * __this /* static, unused */, int32_t ___bits0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.ErrorCorrectionLevel::.cctor()
extern "C"  void ErrorCorrectionLevel__cctor_m3032855147 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
