﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ServerControl/<ConectandoLetreiro>c__Iterator7E
struct U3CConectandoLetreiroU3Ec__Iterator7E_t539930572;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ServerControl/<ConectandoLetreiro>c__Iterator7E::.ctor()
extern "C"  void U3CConectandoLetreiroU3Ec__Iterator7E__ctor_m4221467935 (U3CConectandoLetreiroU3Ec__Iterator7E_t539930572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ServerControl/<ConectandoLetreiro>c__Iterator7E::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CConectandoLetreiroU3Ec__Iterator7E_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3010966931 (U3CConectandoLetreiroU3Ec__Iterator7E_t539930572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ServerControl/<ConectandoLetreiro>c__Iterator7E::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CConectandoLetreiroU3Ec__Iterator7E_System_Collections_IEnumerator_get_Current_m2130001703 (U3CConectandoLetreiroU3Ec__Iterator7E_t539930572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ServerControl/<ConectandoLetreiro>c__Iterator7E::MoveNext()
extern "C"  bool U3CConectandoLetreiroU3Ec__Iterator7E_MoveNext_m819953781 (U3CConectandoLetreiroU3Ec__Iterator7E_t539930572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ServerControl/<ConectandoLetreiro>c__Iterator7E::Dispose()
extern "C"  void U3CConectandoLetreiroU3Ec__Iterator7E_Dispose_m2279841180 (U3CConectandoLetreiroU3Ec__Iterator7E_t539930572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ServerControl/<ConectandoLetreiro>c__Iterator7E::Reset()
extern "C"  void U3CConectandoLetreiroU3Ec__Iterator7E_Reset_m1867900876 (U3CConectandoLetreiroU3Ec__Iterator7E_t539930572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
