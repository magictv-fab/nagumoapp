﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmMaterial
struct FsmMaterial_t924399665;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t2131419205;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetProceduralColor
struct  SetProceduralColor_t3246619020  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Actions.SetProceduralColor::substanceMaterial
	FsmMaterial_t924399665 * ___substanceMaterial_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetProceduralColor::colorProperty
	FsmString_t952858651 * ___colorProperty_10;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.SetProceduralColor::colorValue
	FsmColor_t2131419205 * ___colorValue_11;
	// System.Boolean HutongGames.PlayMaker.Actions.SetProceduralColor::everyFrame
	bool ___everyFrame_12;

public:
	inline static int32_t get_offset_of_substanceMaterial_9() { return static_cast<int32_t>(offsetof(SetProceduralColor_t3246619020, ___substanceMaterial_9)); }
	inline FsmMaterial_t924399665 * get_substanceMaterial_9() const { return ___substanceMaterial_9; }
	inline FsmMaterial_t924399665 ** get_address_of_substanceMaterial_9() { return &___substanceMaterial_9; }
	inline void set_substanceMaterial_9(FsmMaterial_t924399665 * value)
	{
		___substanceMaterial_9 = value;
		Il2CppCodeGenWriteBarrier(&___substanceMaterial_9, value);
	}

	inline static int32_t get_offset_of_colorProperty_10() { return static_cast<int32_t>(offsetof(SetProceduralColor_t3246619020, ___colorProperty_10)); }
	inline FsmString_t952858651 * get_colorProperty_10() const { return ___colorProperty_10; }
	inline FsmString_t952858651 ** get_address_of_colorProperty_10() { return &___colorProperty_10; }
	inline void set_colorProperty_10(FsmString_t952858651 * value)
	{
		___colorProperty_10 = value;
		Il2CppCodeGenWriteBarrier(&___colorProperty_10, value);
	}

	inline static int32_t get_offset_of_colorValue_11() { return static_cast<int32_t>(offsetof(SetProceduralColor_t3246619020, ___colorValue_11)); }
	inline FsmColor_t2131419205 * get_colorValue_11() const { return ___colorValue_11; }
	inline FsmColor_t2131419205 ** get_address_of_colorValue_11() { return &___colorValue_11; }
	inline void set_colorValue_11(FsmColor_t2131419205 * value)
	{
		___colorValue_11 = value;
		Il2CppCodeGenWriteBarrier(&___colorValue_11, value);
	}

	inline static int32_t get_offset_of_everyFrame_12() { return static_cast<int32_t>(offsetof(SetProceduralColor_t3246619020, ___everyFrame_12)); }
	inline bool get_everyFrame_12() const { return ___everyFrame_12; }
	inline bool* get_address_of_everyFrame_12() { return &___everyFrame_12; }
	inline void set_everyFrame_12(bool value)
	{
		___everyFrame_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
