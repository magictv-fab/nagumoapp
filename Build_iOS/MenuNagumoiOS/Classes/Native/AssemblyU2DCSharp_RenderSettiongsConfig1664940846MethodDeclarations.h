﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RenderSettiongsConfig
struct RenderSettiongsConfig_t1664940846;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

// System.Void RenderSettiongsConfig::.ctor()
extern "C"  void RenderSettiongsConfig__ctor_m2865148413 (RenderSettiongsConfig_t1664940846 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RenderSettiongsConfig::Start()
extern "C"  void RenderSettiongsConfig_Start_m1812286205 (RenderSettiongsConfig_t1664940846 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RenderSettiongsConfig::Update()
extern "C"  void RenderSettiongsConfig_Update_m352149680 (RenderSettiongsConfig_t1664940846 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String RenderSettiongsConfig::getUniqueName()
extern "C"  String_t* RenderSettiongsConfig_getUniqueName_m4249082512 (RenderSettiongsConfig_t1664940846 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RenderSettiongsConfig::configByString(System.String)
extern "C"  void RenderSettiongsConfig_configByString_m2856881265 (RenderSettiongsConfig_t1664940846 * __this, String_t* ___metadata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RenderSettiongsConfig::Config_ambientSkyColor(System.String)
extern "C"  void RenderSettiongsConfig_Config_ambientSkyColor_m1551343646 (RenderSettiongsConfig_t1664940846 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RenderSettiongsConfig::Config_ambientEquatorColor(System.String)
extern "C"  void RenderSettiongsConfig_Config_ambientEquatorColor_m2527540892 (RenderSettiongsConfig_t1664940846 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RenderSettiongsConfig::Config_ambientLight(System.String)
extern "C"  void RenderSettiongsConfig_Config_ambientLight_m3118383276 (RenderSettiongsConfig_t1664940846 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RenderSettiongsConfig::Config_ambientGroundColor(System.String)
extern "C"  void RenderSettiongsConfig_Config_ambientGroundColor_m2292668966 (RenderSettiongsConfig_t1664940846 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RenderSettiongsConfig::Config_ambientIntensity(System.String)
extern "C"  void RenderSettiongsConfig_Config_ambientIntensity_m352667503 (RenderSettiongsConfig_t1664940846 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RenderSettiongsConfig::Config_reflectionIntensity(System.String)
extern "C"  void RenderSettiongsConfig_Config_reflectionIntensity_m3983407120 (RenderSettiongsConfig_t1664940846 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RenderSettiongsConfig::Config_haloStrength(System.String)
extern "C"  void RenderSettiongsConfig_Config_haloStrength_m4119344013 (RenderSettiongsConfig_t1664940846 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RenderSettiongsConfig::Config_reflectionBounces(System.String)
extern "C"  void RenderSettiongsConfig_Config_reflectionBounces_m2793522904 (RenderSettiongsConfig_t1664940846 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RenderSettiongsConfig::Reset()
extern "C"  void RenderSettiongsConfig_Reset_m511581354 (RenderSettiongsConfig_t1664940846 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject RenderSettiongsConfig::getGameObjectReference()
extern "C"  GameObject_t3674682005 * RenderSettiongsConfig_getGameObjectReference_m2164455158 (RenderSettiongsConfig_t1664940846 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single RenderSettiongsConfig::getMaxMinFloat(System.String,System.Single,System.Single)
extern "C"  float RenderSettiongsConfig_getMaxMinFloat_m2025709369 (RenderSettiongsConfig_t1664940846 * __this, String_t* ___value0, float ___min1, float ___max2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color RenderSettiongsConfig::getColorByString(System.String,UnityEngine.Color)
extern "C"  Color_t4194546905  RenderSettiongsConfig_getColorByString_m4083478861 (RenderSettiongsConfig_t1664940846 * __this, String_t* ___colorString0, Color_t4194546905  ___defaultColor1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
