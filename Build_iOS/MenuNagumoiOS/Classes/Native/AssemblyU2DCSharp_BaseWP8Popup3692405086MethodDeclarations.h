﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseWP8Popup
struct BaseWP8Popup_t3692405086;

#include "codegen/il2cpp-codegen.h"

// System.Void BaseWP8Popup::.ctor()
extern "C"  void BaseWP8Popup__ctor_m1450113789 (BaseWP8Popup_t3692405086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
