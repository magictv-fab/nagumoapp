﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.vo.request.ServerRequestParamsUpdateConstants
struct ServerRequestParamsUpdateConstants_t3388834476;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.vo.request.ServerRequestParamsUpdateConstants::.ctor()
extern "C"  void ServerRequestParamsUpdateConstants__ctor_m3301083778 (ServerRequestParamsUpdateConstants_t3388834476 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
