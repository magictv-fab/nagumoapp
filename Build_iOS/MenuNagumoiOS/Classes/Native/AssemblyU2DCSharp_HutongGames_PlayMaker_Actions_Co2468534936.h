﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ConvertStringToInt
struct  ConvertStringToInt_t2468534936  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ConvertStringToInt::stringVariable
	FsmString_t952858651 * ___stringVariable_9;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ConvertStringToInt::intVariable
	FsmInt_t1596138449 * ___intVariable_10;
	// System.Boolean HutongGames.PlayMaker.Actions.ConvertStringToInt::everyFrame
	bool ___everyFrame_11;

public:
	inline static int32_t get_offset_of_stringVariable_9() { return static_cast<int32_t>(offsetof(ConvertStringToInt_t2468534936, ___stringVariable_9)); }
	inline FsmString_t952858651 * get_stringVariable_9() const { return ___stringVariable_9; }
	inline FsmString_t952858651 ** get_address_of_stringVariable_9() { return &___stringVariable_9; }
	inline void set_stringVariable_9(FsmString_t952858651 * value)
	{
		___stringVariable_9 = value;
		Il2CppCodeGenWriteBarrier(&___stringVariable_9, value);
	}

	inline static int32_t get_offset_of_intVariable_10() { return static_cast<int32_t>(offsetof(ConvertStringToInt_t2468534936, ___intVariable_10)); }
	inline FsmInt_t1596138449 * get_intVariable_10() const { return ___intVariable_10; }
	inline FsmInt_t1596138449 ** get_address_of_intVariable_10() { return &___intVariable_10; }
	inline void set_intVariable_10(FsmInt_t1596138449 * value)
	{
		___intVariable_10 = value;
		Il2CppCodeGenWriteBarrier(&___intVariable_10, value);
	}

	inline static int32_t get_offset_of_everyFrame_11() { return static_cast<int32_t>(offsetof(ConvertStringToInt_t2468534936, ___everyFrame_11)); }
	inline bool get_everyFrame_11() const { return ___everyFrame_11; }
	inline bool* get_address_of_everyFrame_11() { return &___everyFrame_11; }
	inline void set_everyFrame_11(bool value)
	{
		___everyFrame_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
