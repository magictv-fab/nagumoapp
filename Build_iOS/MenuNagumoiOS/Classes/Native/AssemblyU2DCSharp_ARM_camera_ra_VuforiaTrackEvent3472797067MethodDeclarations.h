﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.camera.ra.VuforiaTrackEvent
struct VuforiaTrackEvent_t3472797067;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableB835151357.h"

// System.Void ARM.camera.ra.VuforiaTrackEvent::.ctor()
extern "C"  void VuforiaTrackEvent__ctor_m3641216834 (VuforiaTrackEvent_t3472797067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.camera.ra.VuforiaTrackEvent::Start()
extern "C"  void VuforiaTrackEvent_Start_m2588354626 (VuforiaTrackEvent_t3472797067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.camera.ra.VuforiaTrackEvent::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C"  void VuforiaTrackEvent_OnTrackableStateChanged_m1348336027 (VuforiaTrackEvent_t3472797067 * __this, int32_t ___previousStatus0, int32_t ___newStatus1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
