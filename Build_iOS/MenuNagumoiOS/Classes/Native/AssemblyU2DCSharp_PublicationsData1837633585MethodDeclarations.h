﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PublicationsData
struct PublicationsData_t1837633585;

#include "codegen/il2cpp-codegen.h"

// System.Void PublicationsData::.ctor()
extern "C"  void PublicationsData__ctor_m2364390794 (PublicationsData_t1837633585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
