﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmColor
struct FsmColor_t2131419205;
// HutongGames.PlayMaker.FsmAnimationCurve
struct FsmAnimationCurve_t2685995989;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_An4201352541.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_An2191327052.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AnimateColor
struct  AnimateColor_t301720362  : public AnimateFsmAction_t4201352541
{
public:
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.AnimateColor::colorVariable
	FsmColor_t2131419205 * ___colorVariable_32;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.AnimateColor::curveR
	FsmAnimationCurve_t2685995989 * ___curveR_33;
	// HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation HutongGames.PlayMaker.Actions.AnimateColor::calculationR
	int32_t ___calculationR_34;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.AnimateColor::curveG
	FsmAnimationCurve_t2685995989 * ___curveG_35;
	// HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation HutongGames.PlayMaker.Actions.AnimateColor::calculationG
	int32_t ___calculationG_36;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.AnimateColor::curveB
	FsmAnimationCurve_t2685995989 * ___curveB_37;
	// HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation HutongGames.PlayMaker.Actions.AnimateColor::calculationB
	int32_t ___calculationB_38;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.AnimateColor::curveA
	FsmAnimationCurve_t2685995989 * ___curveA_39;
	// HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation HutongGames.PlayMaker.Actions.AnimateColor::calculationA
	int32_t ___calculationA_40;
	// System.Boolean HutongGames.PlayMaker.Actions.AnimateColor::finishInNextStep
	bool ___finishInNextStep_41;
	// UnityEngine.Color HutongGames.PlayMaker.Actions.AnimateColor::clr
	Color_t4194546905  ___clr_42;

public:
	inline static int32_t get_offset_of_colorVariable_32() { return static_cast<int32_t>(offsetof(AnimateColor_t301720362, ___colorVariable_32)); }
	inline FsmColor_t2131419205 * get_colorVariable_32() const { return ___colorVariable_32; }
	inline FsmColor_t2131419205 ** get_address_of_colorVariable_32() { return &___colorVariable_32; }
	inline void set_colorVariable_32(FsmColor_t2131419205 * value)
	{
		___colorVariable_32 = value;
		Il2CppCodeGenWriteBarrier(&___colorVariable_32, value);
	}

	inline static int32_t get_offset_of_curveR_33() { return static_cast<int32_t>(offsetof(AnimateColor_t301720362, ___curveR_33)); }
	inline FsmAnimationCurve_t2685995989 * get_curveR_33() const { return ___curveR_33; }
	inline FsmAnimationCurve_t2685995989 ** get_address_of_curveR_33() { return &___curveR_33; }
	inline void set_curveR_33(FsmAnimationCurve_t2685995989 * value)
	{
		___curveR_33 = value;
		Il2CppCodeGenWriteBarrier(&___curveR_33, value);
	}

	inline static int32_t get_offset_of_calculationR_34() { return static_cast<int32_t>(offsetof(AnimateColor_t301720362, ___calculationR_34)); }
	inline int32_t get_calculationR_34() const { return ___calculationR_34; }
	inline int32_t* get_address_of_calculationR_34() { return &___calculationR_34; }
	inline void set_calculationR_34(int32_t value)
	{
		___calculationR_34 = value;
	}

	inline static int32_t get_offset_of_curveG_35() { return static_cast<int32_t>(offsetof(AnimateColor_t301720362, ___curveG_35)); }
	inline FsmAnimationCurve_t2685995989 * get_curveG_35() const { return ___curveG_35; }
	inline FsmAnimationCurve_t2685995989 ** get_address_of_curveG_35() { return &___curveG_35; }
	inline void set_curveG_35(FsmAnimationCurve_t2685995989 * value)
	{
		___curveG_35 = value;
		Il2CppCodeGenWriteBarrier(&___curveG_35, value);
	}

	inline static int32_t get_offset_of_calculationG_36() { return static_cast<int32_t>(offsetof(AnimateColor_t301720362, ___calculationG_36)); }
	inline int32_t get_calculationG_36() const { return ___calculationG_36; }
	inline int32_t* get_address_of_calculationG_36() { return &___calculationG_36; }
	inline void set_calculationG_36(int32_t value)
	{
		___calculationG_36 = value;
	}

	inline static int32_t get_offset_of_curveB_37() { return static_cast<int32_t>(offsetof(AnimateColor_t301720362, ___curveB_37)); }
	inline FsmAnimationCurve_t2685995989 * get_curveB_37() const { return ___curveB_37; }
	inline FsmAnimationCurve_t2685995989 ** get_address_of_curveB_37() { return &___curveB_37; }
	inline void set_curveB_37(FsmAnimationCurve_t2685995989 * value)
	{
		___curveB_37 = value;
		Il2CppCodeGenWriteBarrier(&___curveB_37, value);
	}

	inline static int32_t get_offset_of_calculationB_38() { return static_cast<int32_t>(offsetof(AnimateColor_t301720362, ___calculationB_38)); }
	inline int32_t get_calculationB_38() const { return ___calculationB_38; }
	inline int32_t* get_address_of_calculationB_38() { return &___calculationB_38; }
	inline void set_calculationB_38(int32_t value)
	{
		___calculationB_38 = value;
	}

	inline static int32_t get_offset_of_curveA_39() { return static_cast<int32_t>(offsetof(AnimateColor_t301720362, ___curveA_39)); }
	inline FsmAnimationCurve_t2685995989 * get_curveA_39() const { return ___curveA_39; }
	inline FsmAnimationCurve_t2685995989 ** get_address_of_curveA_39() { return &___curveA_39; }
	inline void set_curveA_39(FsmAnimationCurve_t2685995989 * value)
	{
		___curveA_39 = value;
		Il2CppCodeGenWriteBarrier(&___curveA_39, value);
	}

	inline static int32_t get_offset_of_calculationA_40() { return static_cast<int32_t>(offsetof(AnimateColor_t301720362, ___calculationA_40)); }
	inline int32_t get_calculationA_40() const { return ___calculationA_40; }
	inline int32_t* get_address_of_calculationA_40() { return &___calculationA_40; }
	inline void set_calculationA_40(int32_t value)
	{
		___calculationA_40 = value;
	}

	inline static int32_t get_offset_of_finishInNextStep_41() { return static_cast<int32_t>(offsetof(AnimateColor_t301720362, ___finishInNextStep_41)); }
	inline bool get_finishInNextStep_41() const { return ___finishInNextStep_41; }
	inline bool* get_address_of_finishInNextStep_41() { return &___finishInNextStep_41; }
	inline void set_finishInNextStep_41(bool value)
	{
		___finishInNextStep_41 = value;
	}

	inline static int32_t get_offset_of_clr_42() { return static_cast<int32_t>(offsetof(AnimateColor_t301720362, ___clr_42)); }
	inline Color_t4194546905  get_clr_42() const { return ___clr_42; }
	inline Color_t4194546905 * get_address_of_clr_42() { return &___clr_42; }
	inline void set_clr_42(Color_t4194546905  value)
	{
		___clr_42 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
