﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<System.Int64>
struct Collection_1_t339296753;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Int64[]
struct Int64U5BU5D_t2174042770;
// System.Collections.Generic.IEnumerator`1<System.Int64>
struct IEnumerator_1_t3065703644;
// System.Collections.Generic.IList`1<System.Int64>
struct IList_1_t3848485798;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.Int64>::.ctor()
extern "C"  void Collection_1__ctor_m126137845_gshared (Collection_1_t339296753 * __this, const MethodInfo* method);
#define Collection_1__ctor_m126137845(__this, method) ((  void (*) (Collection_1_t339296753 *, const MethodInfo*))Collection_1__ctor_m126137845_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int64>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m202397346_gshared (Collection_1_t339296753 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m202397346(__this, method) ((  bool (*) (Collection_1_t339296753 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m202397346_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int64>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m277424367_gshared (Collection_1_t339296753 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m277424367(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t339296753 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m277424367_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Int64>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m770687806_gshared (Collection_1_t339296753 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m770687806(__this, method) ((  Il2CppObject * (*) (Collection_1_t339296753 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m770687806_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int64>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m3695520575_gshared (Collection_1_t339296753 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m3695520575(__this, ___value0, method) ((  int32_t (*) (Collection_1_t339296753 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m3695520575_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int64>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m3925150689_gshared (Collection_1_t339296753 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m3925150689(__this, ___value0, method) ((  bool (*) (Collection_1_t339296753 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m3925150689_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int64>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m1114614295_gshared (Collection_1_t339296753 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m1114614295(__this, ___value0, method) ((  int32_t (*) (Collection_1_t339296753 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1114614295_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int64>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m539345418_gshared (Collection_1_t339296753 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m539345418(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t339296753 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m539345418_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int64>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m3892928222_gshared (Collection_1_t339296753 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m3892928222(__this, ___value0, method) ((  void (*) (Collection_1_t339296753 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m3892928222_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int64>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m3674518491_gshared (Collection_1_t339296753 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m3674518491(__this, method) ((  bool (*) (Collection_1_t339296753 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m3674518491_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Int64>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m874756173_gshared (Collection_1_t339296753 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m874756173(__this, method) ((  Il2CppObject * (*) (Collection_1_t339296753 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m874756173_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int64>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m2552723472_gshared (Collection_1_t339296753 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m2552723472(__this, method) ((  bool (*) (Collection_1_t339296753 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2552723472_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int64>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m1921828073_gshared (Collection_1_t339296753 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1921828073(__this, method) ((  bool (*) (Collection_1_t339296753 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1921828073_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Int64>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m4187671188_gshared (Collection_1_t339296753 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m4187671188(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t339296753 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m4187671188_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int64>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m3528823649_gshared (Collection_1_t339296753 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m3528823649(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t339296753 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m3528823649_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int64>::Add(T)
extern "C"  void Collection_1_Add_m3319347178_gshared (Collection_1_t339296753 * __this, int64_t ___item0, const MethodInfo* method);
#define Collection_1_Add_m3319347178(__this, ___item0, method) ((  void (*) (Collection_1_t339296753 *, int64_t, const MethodInfo*))Collection_1_Add_m3319347178_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int64>::Clear()
extern "C"  void Collection_1_Clear_m1827238432_gshared (Collection_1_t339296753 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1827238432(__this, method) ((  void (*) (Collection_1_t339296753 *, const MethodInfo*))Collection_1_Clear_m1827238432_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int64>::ClearItems()
extern "C"  void Collection_1_ClearItems_m1395950306_gshared (Collection_1_t339296753 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1395950306(__this, method) ((  void (*) (Collection_1_t339296753 *, const MethodInfo*))Collection_1_ClearItems_m1395950306_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int64>::Contains(T)
extern "C"  bool Collection_1_Contains_m4265657490_gshared (Collection_1_t339296753 * __this, int64_t ___item0, const MethodInfo* method);
#define Collection_1_Contains_m4265657490(__this, ___item0, method) ((  bool (*) (Collection_1_t339296753 *, int64_t, const MethodInfo*))Collection_1_Contains_m4265657490_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int64>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m3346839898_gshared (Collection_1_t339296753 * __this, Int64U5BU5D_t2174042770* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m3346839898(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t339296753 *, Int64U5BU5D_t2174042770*, int32_t, const MethodInfo*))Collection_1_CopyTo_m3346839898_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Int64>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m974384745_gshared (Collection_1_t339296753 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m974384745(__this, method) ((  Il2CppObject* (*) (Collection_1_t339296753 *, const MethodInfo*))Collection_1_GetEnumerator_m974384745_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int64>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m1979684774_gshared (Collection_1_t339296753 * __this, int64_t ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m1979684774(__this, ___item0, method) ((  int32_t (*) (Collection_1_t339296753 *, int64_t, const MethodInfo*))Collection_1_IndexOf_m1979684774_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int64>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m1808621137_gshared (Collection_1_t339296753 * __this, int32_t ___index0, int64_t ___item1, const MethodInfo* method);
#define Collection_1_Insert_m1808621137(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t339296753 *, int32_t, int64_t, const MethodInfo*))Collection_1_Insert_m1808621137_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int64>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m1343945732_gshared (Collection_1_t339296753 * __this, int32_t ___index0, int64_t ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m1343945732(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t339296753 *, int32_t, int64_t, const MethodInfo*))Collection_1_InsertItem_m1343945732_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int64>::Remove(T)
extern "C"  bool Collection_1_Remove_m1067548941_gshared (Collection_1_t339296753 * __this, int64_t ___item0, const MethodInfo* method);
#define Collection_1_Remove_m1067548941(__this, ___item0, method) ((  bool (*) (Collection_1_t339296753 *, int64_t, const MethodInfo*))Collection_1_Remove_m1067548941_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int64>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m3977441303_gshared (Collection_1_t339296753 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m3977441303(__this, ___index0, method) ((  void (*) (Collection_1_t339296753 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m3977441303_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int64>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m2699823735_gshared (Collection_1_t339296753 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m2699823735(__this, ___index0, method) ((  void (*) (Collection_1_t339296753 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m2699823735_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int64>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m1057993365_gshared (Collection_1_t339296753 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1057993365(__this, method) ((  int32_t (*) (Collection_1_t339296753 *, const MethodInfo*))Collection_1_get_Count_m1057993365_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Int64>::get_Item(System.Int32)
extern "C"  int64_t Collection_1_get_Item_m1110288637_gshared (Collection_1_t339296753 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m1110288637(__this, ___index0, method) ((  int64_t (*) (Collection_1_t339296753 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1110288637_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int64>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m432754408_gshared (Collection_1_t339296753 * __this, int32_t ___index0, int64_t ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m432754408(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t339296753 *, int32_t, int64_t, const MethodInfo*))Collection_1_set_Item_m432754408_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int64>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m1549270161_gshared (Collection_1_t339296753 * __this, int32_t ___index0, int64_t ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m1549270161(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t339296753 *, int32_t, int64_t, const MethodInfo*))Collection_1_SetItem_m1549270161_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int64>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m3222255450_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m3222255450(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m3222255450_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<System.Int64>::ConvertItem(System.Object)
extern "C"  int64_t Collection_1_ConvertItem_m71340380_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m71340380(__this /* static, unused */, ___item0, method) ((  int64_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m71340380_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int64>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m3545208858_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m3545208858(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m3545208858_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int64>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m1266238250_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1266238250(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m1266238250_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int64>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m1199432629_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1199432629(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m1199432629_gshared)(__this /* static, unused */, ___list0, method)
