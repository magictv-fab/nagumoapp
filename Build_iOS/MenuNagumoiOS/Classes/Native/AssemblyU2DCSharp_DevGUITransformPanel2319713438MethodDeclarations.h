﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DevGUITransformPanel
struct DevGUITransformPanel_t2319713438;
// MagicTV.abstracts.InAppAbstract
struct InAppAbstract_t382673128;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_abstracts_InAppAbstract382673128.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void DevGUITransformPanel::.ctor()
extern "C"  void DevGUITransformPanel__ctor_m3296503741 (DevGUITransformPanel_t2319713438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DevGUITransformPanel::Start()
extern "C"  void DevGUITransformPanel_Start_m2243641533 (DevGUITransformPanel_t2319713438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DevGUITransformPanel::OnInApp(MagicTV.abstracts.InAppAbstract)
extern "C"  void DevGUITransformPanel_OnInApp_m3043281752 (DevGUITransformPanel_t2319713438 * __this, InAppAbstract_t382673128 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DevGUITransformPanel::Update()
extern "C"  void DevGUITransformPanel_Update_m839262960 (DevGUITransformPanel_t2319713438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DevGUITransformPanel::OnTransform(System.Single)
extern "C"  void DevGUITransformPanel_OnTransform_m111157315 (DevGUITransformPanel_t2319713438 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DevGUITransformPanel::SetProperty(System.String)
extern "C"  void DevGUITransformPanel_SetProperty_m431428688 (DevGUITransformPanel_t2319713438 * __this, String_t* ___property0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DevGUITransformPanel::SetTransform(System.String)
extern "C"  void DevGUITransformPanel_SetTransform_m328746001 (DevGUITransformPanel_t2319713438 * __this, String_t* ___transform0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 DevGUITransformPanel::getTransformVector(System.Single)
extern "C"  Vector3_t4282066566  DevGUITransformPanel_getTransformVector_m2701298769 (DevGUITransformPanel_t2319713438 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DevGUITransformPanel::RaiseTransform(UnityEngine.Vector3)
extern "C"  void DevGUITransformPanel_RaiseTransform_m1923562802 (DevGUITransformPanel_t2319713438 * __this, Vector3_t4282066566  ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
