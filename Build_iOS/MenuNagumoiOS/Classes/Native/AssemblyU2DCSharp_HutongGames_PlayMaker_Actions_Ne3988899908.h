﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkInstantiate
struct  NetworkInstantiate_t3988899908  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.NetworkInstantiate::prefab
	FsmGameObject_t1697147867 * ___prefab_9;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.NetworkInstantiate::spawnPoint
	FsmGameObject_t1697147867 * ___spawnPoint_10;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.NetworkInstantiate::position
	FsmVector3_t533912882 * ___position_11;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.NetworkInstantiate::rotation
	FsmVector3_t533912882 * ___rotation_12;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.NetworkInstantiate::storeObject
	FsmGameObject_t1697147867 * ___storeObject_13;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.NetworkInstantiate::networkGroup
	FsmInt_t1596138449 * ___networkGroup_14;

public:
	inline static int32_t get_offset_of_prefab_9() { return static_cast<int32_t>(offsetof(NetworkInstantiate_t3988899908, ___prefab_9)); }
	inline FsmGameObject_t1697147867 * get_prefab_9() const { return ___prefab_9; }
	inline FsmGameObject_t1697147867 ** get_address_of_prefab_9() { return &___prefab_9; }
	inline void set_prefab_9(FsmGameObject_t1697147867 * value)
	{
		___prefab_9 = value;
		Il2CppCodeGenWriteBarrier(&___prefab_9, value);
	}

	inline static int32_t get_offset_of_spawnPoint_10() { return static_cast<int32_t>(offsetof(NetworkInstantiate_t3988899908, ___spawnPoint_10)); }
	inline FsmGameObject_t1697147867 * get_spawnPoint_10() const { return ___spawnPoint_10; }
	inline FsmGameObject_t1697147867 ** get_address_of_spawnPoint_10() { return &___spawnPoint_10; }
	inline void set_spawnPoint_10(FsmGameObject_t1697147867 * value)
	{
		___spawnPoint_10 = value;
		Il2CppCodeGenWriteBarrier(&___spawnPoint_10, value);
	}

	inline static int32_t get_offset_of_position_11() { return static_cast<int32_t>(offsetof(NetworkInstantiate_t3988899908, ___position_11)); }
	inline FsmVector3_t533912882 * get_position_11() const { return ___position_11; }
	inline FsmVector3_t533912882 ** get_address_of_position_11() { return &___position_11; }
	inline void set_position_11(FsmVector3_t533912882 * value)
	{
		___position_11 = value;
		Il2CppCodeGenWriteBarrier(&___position_11, value);
	}

	inline static int32_t get_offset_of_rotation_12() { return static_cast<int32_t>(offsetof(NetworkInstantiate_t3988899908, ___rotation_12)); }
	inline FsmVector3_t533912882 * get_rotation_12() const { return ___rotation_12; }
	inline FsmVector3_t533912882 ** get_address_of_rotation_12() { return &___rotation_12; }
	inline void set_rotation_12(FsmVector3_t533912882 * value)
	{
		___rotation_12 = value;
		Il2CppCodeGenWriteBarrier(&___rotation_12, value);
	}

	inline static int32_t get_offset_of_storeObject_13() { return static_cast<int32_t>(offsetof(NetworkInstantiate_t3988899908, ___storeObject_13)); }
	inline FsmGameObject_t1697147867 * get_storeObject_13() const { return ___storeObject_13; }
	inline FsmGameObject_t1697147867 ** get_address_of_storeObject_13() { return &___storeObject_13; }
	inline void set_storeObject_13(FsmGameObject_t1697147867 * value)
	{
		___storeObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___storeObject_13, value);
	}

	inline static int32_t get_offset_of_networkGroup_14() { return static_cast<int32_t>(offsetof(NetworkInstantiate_t3988899908, ___networkGroup_14)); }
	inline FsmInt_t1596138449 * get_networkGroup_14() const { return ___networkGroup_14; }
	inline FsmInt_t1596138449 ** get_address_of_networkGroup_14() { return &___networkGroup_14; }
	inline void set_networkGroup_14(FsmInt_t1596138449 * value)
	{
		___networkGroup_14 = value;
		Il2CppCodeGenWriteBarrier(&___networkGroup_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
