﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SmoothLookAtDirection
struct  SmoothLookAtDirection_t4235776941  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SmoothLookAtDirection::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SmoothLookAtDirection::targetDirection
	FsmVector3_t533912882 * ___targetDirection_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SmoothLookAtDirection::minMagnitude
	FsmFloat_t2134102846 * ___minMagnitude_11;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SmoothLookAtDirection::upVector
	FsmVector3_t533912882 * ___upVector_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SmoothLookAtDirection::keepVertical
	FsmBool_t1075959796 * ___keepVertical_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SmoothLookAtDirection::speed
	FsmFloat_t2134102846 * ___speed_14;
	// System.Boolean HutongGames.PlayMaker.Actions.SmoothLookAtDirection::lateUpdate
	bool ___lateUpdate_15;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.SmoothLookAtDirection::previousGo
	GameObject_t3674682005 * ___previousGo_16;
	// UnityEngine.Quaternion HutongGames.PlayMaker.Actions.SmoothLookAtDirection::lastRotation
	Quaternion_t1553702882  ___lastRotation_17;
	// UnityEngine.Quaternion HutongGames.PlayMaker.Actions.SmoothLookAtDirection::desiredRotation
	Quaternion_t1553702882  ___desiredRotation_18;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(SmoothLookAtDirection_t4235776941, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_targetDirection_10() { return static_cast<int32_t>(offsetof(SmoothLookAtDirection_t4235776941, ___targetDirection_10)); }
	inline FsmVector3_t533912882 * get_targetDirection_10() const { return ___targetDirection_10; }
	inline FsmVector3_t533912882 ** get_address_of_targetDirection_10() { return &___targetDirection_10; }
	inline void set_targetDirection_10(FsmVector3_t533912882 * value)
	{
		___targetDirection_10 = value;
		Il2CppCodeGenWriteBarrier(&___targetDirection_10, value);
	}

	inline static int32_t get_offset_of_minMagnitude_11() { return static_cast<int32_t>(offsetof(SmoothLookAtDirection_t4235776941, ___minMagnitude_11)); }
	inline FsmFloat_t2134102846 * get_minMagnitude_11() const { return ___minMagnitude_11; }
	inline FsmFloat_t2134102846 ** get_address_of_minMagnitude_11() { return &___minMagnitude_11; }
	inline void set_minMagnitude_11(FsmFloat_t2134102846 * value)
	{
		___minMagnitude_11 = value;
		Il2CppCodeGenWriteBarrier(&___minMagnitude_11, value);
	}

	inline static int32_t get_offset_of_upVector_12() { return static_cast<int32_t>(offsetof(SmoothLookAtDirection_t4235776941, ___upVector_12)); }
	inline FsmVector3_t533912882 * get_upVector_12() const { return ___upVector_12; }
	inline FsmVector3_t533912882 ** get_address_of_upVector_12() { return &___upVector_12; }
	inline void set_upVector_12(FsmVector3_t533912882 * value)
	{
		___upVector_12 = value;
		Il2CppCodeGenWriteBarrier(&___upVector_12, value);
	}

	inline static int32_t get_offset_of_keepVertical_13() { return static_cast<int32_t>(offsetof(SmoothLookAtDirection_t4235776941, ___keepVertical_13)); }
	inline FsmBool_t1075959796 * get_keepVertical_13() const { return ___keepVertical_13; }
	inline FsmBool_t1075959796 ** get_address_of_keepVertical_13() { return &___keepVertical_13; }
	inline void set_keepVertical_13(FsmBool_t1075959796 * value)
	{
		___keepVertical_13 = value;
		Il2CppCodeGenWriteBarrier(&___keepVertical_13, value);
	}

	inline static int32_t get_offset_of_speed_14() { return static_cast<int32_t>(offsetof(SmoothLookAtDirection_t4235776941, ___speed_14)); }
	inline FsmFloat_t2134102846 * get_speed_14() const { return ___speed_14; }
	inline FsmFloat_t2134102846 ** get_address_of_speed_14() { return &___speed_14; }
	inline void set_speed_14(FsmFloat_t2134102846 * value)
	{
		___speed_14 = value;
		Il2CppCodeGenWriteBarrier(&___speed_14, value);
	}

	inline static int32_t get_offset_of_lateUpdate_15() { return static_cast<int32_t>(offsetof(SmoothLookAtDirection_t4235776941, ___lateUpdate_15)); }
	inline bool get_lateUpdate_15() const { return ___lateUpdate_15; }
	inline bool* get_address_of_lateUpdate_15() { return &___lateUpdate_15; }
	inline void set_lateUpdate_15(bool value)
	{
		___lateUpdate_15 = value;
	}

	inline static int32_t get_offset_of_previousGo_16() { return static_cast<int32_t>(offsetof(SmoothLookAtDirection_t4235776941, ___previousGo_16)); }
	inline GameObject_t3674682005 * get_previousGo_16() const { return ___previousGo_16; }
	inline GameObject_t3674682005 ** get_address_of_previousGo_16() { return &___previousGo_16; }
	inline void set_previousGo_16(GameObject_t3674682005 * value)
	{
		___previousGo_16 = value;
		Il2CppCodeGenWriteBarrier(&___previousGo_16, value);
	}

	inline static int32_t get_offset_of_lastRotation_17() { return static_cast<int32_t>(offsetof(SmoothLookAtDirection_t4235776941, ___lastRotation_17)); }
	inline Quaternion_t1553702882  get_lastRotation_17() const { return ___lastRotation_17; }
	inline Quaternion_t1553702882 * get_address_of_lastRotation_17() { return &___lastRotation_17; }
	inline void set_lastRotation_17(Quaternion_t1553702882  value)
	{
		___lastRotation_17 = value;
	}

	inline static int32_t get_offset_of_desiredRotation_18() { return static_cast<int32_t>(offsetof(SmoothLookAtDirection_t4235776941, ___desiredRotation_18)); }
	inline Quaternion_t1553702882  get_desiredRotation_18() const { return ___desiredRotation_18; }
	inline Quaternion_t1553702882 * get_address_of_desiredRotation_18() { return &___desiredRotation_18; }
	inline void set_desiredRotation_18(Quaternion_t1553702882  value)
	{
		___desiredRotation_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
