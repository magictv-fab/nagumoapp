﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmMaterial
struct FsmMaterial_t924399665;
// UnityEngine.Material
struct Material_t3870600107;
// System.String
struct String_t;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t821476169;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "mscorlib_System_String7231557.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmObject821476169.h"

// UnityEngine.Material HutongGames.PlayMaker.FsmMaterial::get_Value()
extern "C"  Material_t3870600107 * FsmMaterial_get_Value_m1376213207 (FsmMaterial_t924399665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmMaterial::set_Value(UnityEngine.Material)
extern "C"  void FsmMaterial_set_Value_m3341549218 (FsmMaterial_t924399665 * __this, Material_t3870600107 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmMaterial::.ctor()
extern "C"  void FsmMaterial__ctor_m4051814862 (FsmMaterial_t924399665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmMaterial::.ctor(System.String)
extern "C"  void FsmMaterial__ctor_m38188020 (FsmMaterial_t924399665 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmMaterial::.ctor(HutongGames.PlayMaker.FsmObject)
extern "C"  void FsmMaterial__ctor_m987867825 (FsmMaterial_t924399665 * __this, FsmObject_t821476169 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
