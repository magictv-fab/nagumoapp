﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Single[]
struct SingleU5BU5D_t2316563989;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.utils.PoolFloat
struct  PoolFloat_t382107510  : public Il2CppObject
{
public:
	// System.Single[] ARM.utils.PoolFloat::itens
	SingleU5BU5D_t2316563989* ___itens_0;
	// System.Int32 ARM.utils.PoolFloat::_currentIndex
	int32_t ____currentIndex_1;
	// System.Int32 ARM.utils.PoolFloat::_currentSize
	int32_t ____currentSize_2;
	// System.UInt32 ARM.utils.PoolFloat::_maxSize
	uint32_t ____maxSize_3;

public:
	inline static int32_t get_offset_of_itens_0() { return static_cast<int32_t>(offsetof(PoolFloat_t382107510, ___itens_0)); }
	inline SingleU5BU5D_t2316563989* get_itens_0() const { return ___itens_0; }
	inline SingleU5BU5D_t2316563989** get_address_of_itens_0() { return &___itens_0; }
	inline void set_itens_0(SingleU5BU5D_t2316563989* value)
	{
		___itens_0 = value;
		Il2CppCodeGenWriteBarrier(&___itens_0, value);
	}

	inline static int32_t get_offset_of__currentIndex_1() { return static_cast<int32_t>(offsetof(PoolFloat_t382107510, ____currentIndex_1)); }
	inline int32_t get__currentIndex_1() const { return ____currentIndex_1; }
	inline int32_t* get_address_of__currentIndex_1() { return &____currentIndex_1; }
	inline void set__currentIndex_1(int32_t value)
	{
		____currentIndex_1 = value;
	}

	inline static int32_t get_offset_of__currentSize_2() { return static_cast<int32_t>(offsetof(PoolFloat_t382107510, ____currentSize_2)); }
	inline int32_t get__currentSize_2() const { return ____currentSize_2; }
	inline int32_t* get_address_of__currentSize_2() { return &____currentSize_2; }
	inline void set__currentSize_2(int32_t value)
	{
		____currentSize_2 = value;
	}

	inline static int32_t get_offset_of__maxSize_3() { return static_cast<int32_t>(offsetof(PoolFloat_t382107510, ____maxSize_3)); }
	inline uint32_t get__maxSize_3() const { return ____maxSize_3; }
	inline uint32_t* get_address_of__maxSize_3() { return &____maxSize_3; }
	inline void set__maxSize_3(uint32_t value)
	{
		____maxSize_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
