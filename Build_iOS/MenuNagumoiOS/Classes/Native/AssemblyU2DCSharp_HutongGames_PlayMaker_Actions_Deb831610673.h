﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmVar
struct FsmVar_t1596150537;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "PlayMaker_HutongGames_PlayMaker_LogLevel284580066.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DebugFsmVariable
struct  DebugFsmVariable_t831610673  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.LogLevel HutongGames.PlayMaker.Actions.DebugFsmVariable::logLevel
	int32_t ___logLevel_9;
	// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.Actions.DebugFsmVariable::fsmVar
	FsmVar_t1596150537 * ___fsmVar_10;

public:
	inline static int32_t get_offset_of_logLevel_9() { return static_cast<int32_t>(offsetof(DebugFsmVariable_t831610673, ___logLevel_9)); }
	inline int32_t get_logLevel_9() const { return ___logLevel_9; }
	inline int32_t* get_address_of_logLevel_9() { return &___logLevel_9; }
	inline void set_logLevel_9(int32_t value)
	{
		___logLevel_9 = value;
	}

	inline static int32_t get_offset_of_fsmVar_10() { return static_cast<int32_t>(offsetof(DebugFsmVariable_t831610673, ___fsmVar_10)); }
	inline FsmVar_t1596150537 * get_fsmVar_10() const { return ___fsmVar_10; }
	inline FsmVar_t1596150537 ** get_address_of_fsmVar_10() { return &___fsmVar_10; }
	inline void set_fsmVar_10(FsmVar_t1596150537 * value)
	{
		___fsmVar_10 = value;
		Il2CppCodeGenWriteBarrier(&___fsmVar_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
