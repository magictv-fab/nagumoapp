﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.transform.filters.AngleMeanFilter
struct AngleMeanFilter_t3394023366;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void ARM.transform.filters.AngleMeanFilter::.ctor()
extern "C"  void AngleMeanFilter__ctor_m3428536962 (AngleMeanFilter_t3394023366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.AngleMeanFilter::Start()
extern "C"  void AngleMeanFilter_Start_m2375674754 (AngleMeanFilter_t3394023366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.AngleMeanFilter::reset()
extern "C"  void AngleMeanFilter_reset_m3705283919 (AngleMeanFilter_t3394023366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion ARM.transform.filters.AngleMeanFilter::_doFilter(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  AngleMeanFilter__doFilter_m2717771228 (AngleMeanFilter_t3394023366 * __this, Quaternion_t1553702882  ___currentPosition0, Quaternion_t1553702882  ___newValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.transform.filters.AngleMeanFilter::_doFilter(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  AngleMeanFilter__doFilter_m1497270128 (AngleMeanFilter_t3394023366 * __this, Vector3_t4282066566  ___currentValue0, Vector3_t4282066566  ___nextValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.AngleMeanFilter::save(UnityEngine.Quaternion)
extern "C"  void AngleMeanFilter_save_m3171698474 (AngleMeanFilter_t3394023366 * __this, Quaternion_t1553702882  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
