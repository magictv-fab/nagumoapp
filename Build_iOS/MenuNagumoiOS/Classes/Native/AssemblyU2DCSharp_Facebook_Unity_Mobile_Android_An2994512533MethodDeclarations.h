﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Mobile_Android_An2110299454MethodDeclarations.h"

// System.Void Facebook.Unity.Mobile.Android.AndroidFacebook/JavaMethodCall`1<Facebook.Unity.IAccessTokenRefreshResult>::.ctor(Facebook.Unity.Mobile.Android.AndroidFacebook,System.String)
#define JavaMethodCall_1__ctor_m1802046741(__this, ___androidImpl0, ___methodName1, method) ((  void (*) (JavaMethodCall_1_t2994512533 *, AndroidFacebook_t2774136887 *, String_t*, const MethodInfo*))JavaMethodCall_1__ctor_m3083954302_gshared)(__this, ___androidImpl0, ___methodName1, method)
// System.Void Facebook.Unity.Mobile.Android.AndroidFacebook/JavaMethodCall`1<Facebook.Unity.IAccessTokenRefreshResult>::Call(Facebook.Unity.MethodArguments)
#define JavaMethodCall_1_Call_m1795250656(__this, ___args0, method) ((  void (*) (JavaMethodCall_1_t2994512533 *, MethodArguments_t3236074899 *, const MethodInfo*))JavaMethodCall_1_Call_m1503093513_gshared)(__this, ___args0, method)
