﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.events.GenericParameterVO
struct  GenericParameterVO_t1159220759  : public Il2CppObject
{
public:
	// System.Int32 ARM.events.GenericParameterVO::id
	int32_t ___id_0;
	// System.String ARM.events.GenericParameterVO::type
	String_t* ___type_1;
	// System.Int32 ARM.events.GenericParameterVO::intValue
	int32_t ___intValue_2;
	// System.String ARM.events.GenericParameterVO::stringValue
	String_t* ___stringValue_3;
	// System.Single ARM.events.GenericParameterVO::floatValue
	float ___floatValue_4;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(GenericParameterVO_t1159220759, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(GenericParameterVO_t1159220759, ___type_1)); }
	inline String_t* get_type_1() const { return ___type_1; }
	inline String_t** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(String_t* value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier(&___type_1, value);
	}

	inline static int32_t get_offset_of_intValue_2() { return static_cast<int32_t>(offsetof(GenericParameterVO_t1159220759, ___intValue_2)); }
	inline int32_t get_intValue_2() const { return ___intValue_2; }
	inline int32_t* get_address_of_intValue_2() { return &___intValue_2; }
	inline void set_intValue_2(int32_t value)
	{
		___intValue_2 = value;
	}

	inline static int32_t get_offset_of_stringValue_3() { return static_cast<int32_t>(offsetof(GenericParameterVO_t1159220759, ___stringValue_3)); }
	inline String_t* get_stringValue_3() const { return ___stringValue_3; }
	inline String_t** get_address_of_stringValue_3() { return &___stringValue_3; }
	inline void set_stringValue_3(String_t* value)
	{
		___stringValue_3 = value;
		Il2CppCodeGenWriteBarrier(&___stringValue_3, value);
	}

	inline static int32_t get_offset_of_floatValue_4() { return static_cast<int32_t>(offsetof(GenericParameterVO_t1159220759, ___floatValue_4)); }
	inline float get_floatValue_4() const { return ___floatValue_4; }
	inline float* get_address_of_floatValue_4() { return &___floatValue_4; }
	inline void set_floatValue_4(float value)
	{
		___floatValue_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
