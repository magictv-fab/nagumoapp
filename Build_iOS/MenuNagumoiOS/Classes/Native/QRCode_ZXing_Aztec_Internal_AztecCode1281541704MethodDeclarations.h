﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Aztec.Internal.AztecCode
struct AztecCode_t1281541704;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitMatrix1058711404.h"

// System.Void ZXing.Aztec.Internal.AztecCode::set_isCompact(System.Boolean)
extern "C"  void AztecCode_set_isCompact_m4269369880 (AztecCode_t1281541704 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Aztec.Internal.AztecCode::set_Size(System.Int32)
extern "C"  void AztecCode_set_Size_m3208459340 (AztecCode_t1281541704 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Aztec.Internal.AztecCode::set_Layers(System.Int32)
extern "C"  void AztecCode_set_Layers_m1967257997 (AztecCode_t1281541704 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Aztec.Internal.AztecCode::set_CodeWords(System.Int32)
extern "C"  void AztecCode_set_CodeWords_m118530805 (AztecCode_t1281541704 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.BitMatrix ZXing.Aztec.Internal.AztecCode::get_Matrix()
extern "C"  BitMatrix_t1058711404 * AztecCode_get_Matrix_m1591174319 (AztecCode_t1281541704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Aztec.Internal.AztecCode::set_Matrix(ZXing.Common.BitMatrix)
extern "C"  void AztecCode_set_Matrix_m1736491622 (AztecCode_t1281541704 * __this, BitMatrix_t1058711404 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Aztec.Internal.AztecCode::.ctor()
extern "C"  void AztecCode__ctor_m1318252839 (AztecCode_t1281541704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
