﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.QrCode.Internal.DataMask/DataMask000
struct DataMask000_t1996578300;

#include "codegen/il2cpp-codegen.h"

// System.Boolean ZXing.QrCode.Internal.DataMask/DataMask000::isMasked(System.Int32,System.Int32)
extern "C"  bool DataMask000_isMasked_m474372184 (DataMask000_t1996578300 * __this, int32_t ___i0, int32_t ___j1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.DataMask/DataMask000::.ctor()
extern "C"  void DataMask000__ctor_m3086226111 (DataMask000_t1996578300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
