﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.BarcodeFormat,System.Object>
struct ShimEnumerator_t1005475740;
// System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>
struct Dictionary_2_t1289697713;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.BarcodeFormat,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m772629001_gshared (ShimEnumerator_t1005475740 * __this, Dictionary_2_t1289697713 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m772629001(__this, ___host0, method) ((  void (*) (ShimEnumerator_t1005475740 *, Dictionary_2_t1289697713 *, const MethodInfo*))ShimEnumerator__ctor_m772629001_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.BarcodeFormat,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3488666744_gshared (ShimEnumerator_t1005475740 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m3488666744(__this, method) ((  bool (*) (ShimEnumerator_t1005475740 *, const MethodInfo*))ShimEnumerator_MoveNext_m3488666744_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.BarcodeFormat,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m1581620892_gshared (ShimEnumerator_t1005475740 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m1581620892(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t1005475740 *, const MethodInfo*))ShimEnumerator_get_Entry_m1581620892_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.BarcodeFormat,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2760163063_gshared (ShimEnumerator_t1005475740 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m2760163063(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1005475740 *, const MethodInfo*))ShimEnumerator_get_Key_m2760163063_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.BarcodeFormat,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m3570318281_gshared (ShimEnumerator_t1005475740 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m3570318281(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1005475740 *, const MethodInfo*))ShimEnumerator_get_Value_m3570318281_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.BarcodeFormat,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m821689361_gshared (ShimEnumerator_t1005475740 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m821689361(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1005475740 *, const MethodInfo*))ShimEnumerator_get_Current_m821689361_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.BarcodeFormat,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m1289944795_gshared (ShimEnumerator_t1005475740 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m1289944795(__this, method) ((  void (*) (ShimEnumerator_t1005475740 *, const MethodInfo*))ShimEnumerator_Reset_m1289944795_gshared)(__this, method)
