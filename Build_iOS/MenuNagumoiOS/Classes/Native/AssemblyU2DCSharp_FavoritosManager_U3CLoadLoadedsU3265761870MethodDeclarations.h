﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FavoritosManager/<LoadLoadeds>c__Iterator55
struct U3CLoadLoadedsU3Ec__Iterator55_t3265761870;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void FavoritosManager/<LoadLoadeds>c__Iterator55::.ctor()
extern "C"  void U3CLoadLoadedsU3Ec__Iterator55__ctor_m3900899549 (U3CLoadLoadedsU3Ec__Iterator55_t3265761870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FavoritosManager/<LoadLoadeds>c__Iterator55::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadLoadedsU3Ec__Iterator55_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m957643285 (U3CLoadLoadedsU3Ec__Iterator55_t3265761870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FavoritosManager/<LoadLoadeds>c__Iterator55::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadLoadedsU3Ec__Iterator55_System_Collections_IEnumerator_get_Current_m3546236841 (U3CLoadLoadedsU3Ec__Iterator55_t3265761870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FavoritosManager/<LoadLoadeds>c__Iterator55::MoveNext()
extern "C"  bool U3CLoadLoadedsU3Ec__Iterator55_MoveNext_m1851703799 (U3CLoadLoadedsU3Ec__Iterator55_t3265761870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FavoritosManager/<LoadLoadeds>c__Iterator55::Dispose()
extern "C"  void U3CLoadLoadedsU3Ec__Iterator55_Dispose_m3451267546 (U3CLoadLoadedsU3Ec__Iterator55_t3265761870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FavoritosManager/<LoadLoadeds>c__Iterator55::Reset()
extern "C"  void U3CLoadLoadedsU3Ec__Iterator55_Reset_m1547332490 (U3CLoadLoadedsU3Ec__Iterator55_t3265761870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
