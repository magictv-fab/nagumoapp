﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ConvertIntToString
struct  ConvertIntToString_t3222633520  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ConvertIntToString::intVariable
	FsmInt_t1596138449 * ___intVariable_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ConvertIntToString::stringVariable
	FsmString_t952858651 * ___stringVariable_10;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ConvertIntToString::format
	FsmString_t952858651 * ___format_11;
	// System.Boolean HutongGames.PlayMaker.Actions.ConvertIntToString::everyFrame
	bool ___everyFrame_12;

public:
	inline static int32_t get_offset_of_intVariable_9() { return static_cast<int32_t>(offsetof(ConvertIntToString_t3222633520, ___intVariable_9)); }
	inline FsmInt_t1596138449 * get_intVariable_9() const { return ___intVariable_9; }
	inline FsmInt_t1596138449 ** get_address_of_intVariable_9() { return &___intVariable_9; }
	inline void set_intVariable_9(FsmInt_t1596138449 * value)
	{
		___intVariable_9 = value;
		Il2CppCodeGenWriteBarrier(&___intVariable_9, value);
	}

	inline static int32_t get_offset_of_stringVariable_10() { return static_cast<int32_t>(offsetof(ConvertIntToString_t3222633520, ___stringVariable_10)); }
	inline FsmString_t952858651 * get_stringVariable_10() const { return ___stringVariable_10; }
	inline FsmString_t952858651 ** get_address_of_stringVariable_10() { return &___stringVariable_10; }
	inline void set_stringVariable_10(FsmString_t952858651 * value)
	{
		___stringVariable_10 = value;
		Il2CppCodeGenWriteBarrier(&___stringVariable_10, value);
	}

	inline static int32_t get_offset_of_format_11() { return static_cast<int32_t>(offsetof(ConvertIntToString_t3222633520, ___format_11)); }
	inline FsmString_t952858651 * get_format_11() const { return ___format_11; }
	inline FsmString_t952858651 ** get_address_of_format_11() { return &___format_11; }
	inline void set_format_11(FsmString_t952858651 * value)
	{
		___format_11 = value;
		Il2CppCodeGenWriteBarrier(&___format_11, value);
	}

	inline static int32_t get_offset_of_everyFrame_12() { return static_cast<int32_t>(offsetof(ConvertIntToString_t3222633520, ___everyFrame_12)); }
	inline bool get_everyFrame_12() const { return ___everyFrame_12; }
	inline bool* get_address_of_everyFrame_12() { return &___everyFrame_12; }
	inline void set_everyFrame_12(bool value)
	{
		___everyFrame_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
