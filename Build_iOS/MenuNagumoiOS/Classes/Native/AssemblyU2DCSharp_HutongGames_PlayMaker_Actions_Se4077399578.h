﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetFogDensity
struct  SetFogDensity_t4077399578  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetFogDensity::fogDensity
	FsmFloat_t2134102846 * ___fogDensity_9;
	// System.Boolean HutongGames.PlayMaker.Actions.SetFogDensity::everyFrame
	bool ___everyFrame_10;

public:
	inline static int32_t get_offset_of_fogDensity_9() { return static_cast<int32_t>(offsetof(SetFogDensity_t4077399578, ___fogDensity_9)); }
	inline FsmFloat_t2134102846 * get_fogDensity_9() const { return ___fogDensity_9; }
	inline FsmFloat_t2134102846 ** get_address_of_fogDensity_9() { return &___fogDensity_9; }
	inline void set_fogDensity_9(FsmFloat_t2134102846 * value)
	{
		___fogDensity_9 = value;
		Il2CppCodeGenWriteBarrier(&___fogDensity_9, value);
	}

	inline static int32_t get_offset_of_everyFrame_10() { return static_cast<int32_t>(offsetof(SetFogDensity_t4077399578, ___everyFrame_10)); }
	inline bool get_everyFrame_10() const { return ___everyFrame_10; }
	inline bool* get_address_of_everyFrame_10() { return &___everyFrame_10; }
	inline void set_everyFrame_10(bool value)
	{
		___everyFrame_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
