﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Correios.Net.Address
struct Address_t2296282618;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void Correios.Net.Address::.ctor()
extern "C"  void Address__ctor_m3439938546 (Address_t2296282618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Correios.Net.Address::get_Zip()
extern "C"  String_t* Address_get_Zip_m3874473861 (Address_t2296282618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Correios.Net.Address::set_Zip(System.String)
extern "C"  void Address_set_Zip_m2188010862 (Address_t2296282618 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Correios.Net.Address::get_Street()
extern "C"  String_t* Address_get_Street_m3473009089 (Address_t2296282618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Correios.Net.Address::set_Street(System.String)
extern "C"  void Address_set_Street_m169855856 (Address_t2296282618 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Correios.Net.Address::get_District()
extern "C"  String_t* Address_get_District_m4261325644 (Address_t2296282618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Correios.Net.Address::set_District(System.String)
extern "C"  void Address_set_District_m1959148549 (Address_t2296282618 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Correios.Net.Address::get_City()
extern "C"  String_t* Address_get_City_m3486299241 (Address_t2296282618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Correios.Net.Address::set_City(System.String)
extern "C"  void Address_set_City_m2778425736 (Address_t2296282618 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Correios.Net.Address::get_State()
extern "C"  String_t* Address_get_State_m2313534517 (Address_t2296282618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Correios.Net.Address::set_State(System.String)
extern "C"  void Address_set_State_m2803326590 (Address_t2296282618 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Correios.Net.Address::get_UniqueZip()
extern "C"  bool Address_get_UniqueZip_m3334570499 (Address_t2296282618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Correios.Net.Address::set_UniqueZip(System.Boolean)
extern "C"  void Address_set_UniqueZip_m3761098618 (Address_t2296282618 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
