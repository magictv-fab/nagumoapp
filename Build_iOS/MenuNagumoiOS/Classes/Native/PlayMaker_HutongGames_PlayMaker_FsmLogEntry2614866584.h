﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// HutongGames.PlayMaker.FsmState
struct FsmState_t2146334067;
// HutongGames.PlayMaker.FsmStateAction
struct FsmStateAction_t2366529033;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmTransition
struct FsmTransition_t3771611999;
// HutongGames.PlayMaker.FsmEventTarget
struct FsmEventTarget_t1823904941;
// HutongGames.PlayMaker.FsmVariables
struct FsmVariables_t963491929;

#include "mscorlib_System_Object4170816371.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmLogType537852544.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmLogEntry
struct  FsmLogEntry_t2614866584  : public Il2CppObject
{
public:
	// System.String HutongGames.PlayMaker.FsmLogEntry::textWithTimecode
	String_t* ___textWithTimecode_0;
	// HutongGames.PlayMaker.FsmLogType HutongGames.PlayMaker.FsmLogEntry::<LogType>k__BackingField
	int32_t ___U3CLogTypeU3Ek__BackingField_1;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.FsmLogEntry::<State>k__BackingField
	FsmState_t2146334067 * ___U3CStateU3Ek__BackingField_2;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.FsmLogEntry::<SentByState>k__BackingField
	FsmState_t2146334067 * ___U3CSentByStateU3Ek__BackingField_3;
	// HutongGames.PlayMaker.FsmStateAction HutongGames.PlayMaker.FsmLogEntry::<Action>k__BackingField
	FsmStateAction_t2366529033 * ___U3CActionU3Ek__BackingField_4;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmLogEntry::<Event>k__BackingField
	FsmEvent_t2133468028 * ___U3CEventU3Ek__BackingField_5;
	// HutongGames.PlayMaker.FsmTransition HutongGames.PlayMaker.FsmLogEntry::<Transition>k__BackingField
	FsmTransition_t3771611999 * ___U3CTransitionU3Ek__BackingField_6;
	// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.FsmLogEntry::<EventTarget>k__BackingField
	FsmEventTarget_t1823904941 * ___U3CEventTargetU3Ek__BackingField_7;
	// System.Single HutongGames.PlayMaker.FsmLogEntry::<Time>k__BackingField
	float ___U3CTimeU3Ek__BackingField_8;
	// System.String HutongGames.PlayMaker.FsmLogEntry::<Text>k__BackingField
	String_t* ___U3CTextU3Ek__BackingField_9;
	// System.String HutongGames.PlayMaker.FsmLogEntry::<Text2>k__BackingField
	String_t* ___U3CText2U3Ek__BackingField_10;
	// System.Int32 HutongGames.PlayMaker.FsmLogEntry::<FrameCount>k__BackingField
	int32_t ___U3CFrameCountU3Ek__BackingField_11;
	// HutongGames.PlayMaker.FsmVariables HutongGames.PlayMaker.FsmLogEntry::<FsmVariablesCopy>k__BackingField
	FsmVariables_t963491929 * ___U3CFsmVariablesCopyU3Ek__BackingField_12;
	// HutongGames.PlayMaker.FsmVariables HutongGames.PlayMaker.FsmLogEntry::<GlobalVariablesCopy>k__BackingField
	FsmVariables_t963491929 * ___U3CGlobalVariablesCopyU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_textWithTimecode_0() { return static_cast<int32_t>(offsetof(FsmLogEntry_t2614866584, ___textWithTimecode_0)); }
	inline String_t* get_textWithTimecode_0() const { return ___textWithTimecode_0; }
	inline String_t** get_address_of_textWithTimecode_0() { return &___textWithTimecode_0; }
	inline void set_textWithTimecode_0(String_t* value)
	{
		___textWithTimecode_0 = value;
		Il2CppCodeGenWriteBarrier(&___textWithTimecode_0, value);
	}

	inline static int32_t get_offset_of_U3CLogTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FsmLogEntry_t2614866584, ___U3CLogTypeU3Ek__BackingField_1)); }
	inline int32_t get_U3CLogTypeU3Ek__BackingField_1() const { return ___U3CLogTypeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CLogTypeU3Ek__BackingField_1() { return &___U3CLogTypeU3Ek__BackingField_1; }
	inline void set_U3CLogTypeU3Ek__BackingField_1(int32_t value)
	{
		___U3CLogTypeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FsmLogEntry_t2614866584, ___U3CStateU3Ek__BackingField_2)); }
	inline FsmState_t2146334067 * get_U3CStateU3Ek__BackingField_2() const { return ___U3CStateU3Ek__BackingField_2; }
	inline FsmState_t2146334067 ** get_address_of_U3CStateU3Ek__BackingField_2() { return &___U3CStateU3Ek__BackingField_2; }
	inline void set_U3CStateU3Ek__BackingField_2(FsmState_t2146334067 * value)
	{
		___U3CStateU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CStateU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CSentByStateU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(FsmLogEntry_t2614866584, ___U3CSentByStateU3Ek__BackingField_3)); }
	inline FsmState_t2146334067 * get_U3CSentByStateU3Ek__BackingField_3() const { return ___U3CSentByStateU3Ek__BackingField_3; }
	inline FsmState_t2146334067 ** get_address_of_U3CSentByStateU3Ek__BackingField_3() { return &___U3CSentByStateU3Ek__BackingField_3; }
	inline void set_U3CSentByStateU3Ek__BackingField_3(FsmState_t2146334067 * value)
	{
		___U3CSentByStateU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSentByStateU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CActionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(FsmLogEntry_t2614866584, ___U3CActionU3Ek__BackingField_4)); }
	inline FsmStateAction_t2366529033 * get_U3CActionU3Ek__BackingField_4() const { return ___U3CActionU3Ek__BackingField_4; }
	inline FsmStateAction_t2366529033 ** get_address_of_U3CActionU3Ek__BackingField_4() { return &___U3CActionU3Ek__BackingField_4; }
	inline void set_U3CActionU3Ek__BackingField_4(FsmStateAction_t2366529033 * value)
	{
		___U3CActionU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CActionU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CEventU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(FsmLogEntry_t2614866584, ___U3CEventU3Ek__BackingField_5)); }
	inline FsmEvent_t2133468028 * get_U3CEventU3Ek__BackingField_5() const { return ___U3CEventU3Ek__BackingField_5; }
	inline FsmEvent_t2133468028 ** get_address_of_U3CEventU3Ek__BackingField_5() { return &___U3CEventU3Ek__BackingField_5; }
	inline void set_U3CEventU3Ek__BackingField_5(FsmEvent_t2133468028 * value)
	{
		___U3CEventU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CEventU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CTransitionU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(FsmLogEntry_t2614866584, ___U3CTransitionU3Ek__BackingField_6)); }
	inline FsmTransition_t3771611999 * get_U3CTransitionU3Ek__BackingField_6() const { return ___U3CTransitionU3Ek__BackingField_6; }
	inline FsmTransition_t3771611999 ** get_address_of_U3CTransitionU3Ek__BackingField_6() { return &___U3CTransitionU3Ek__BackingField_6; }
	inline void set_U3CTransitionU3Ek__BackingField_6(FsmTransition_t3771611999 * value)
	{
		___U3CTransitionU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTransitionU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3CEventTargetU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(FsmLogEntry_t2614866584, ___U3CEventTargetU3Ek__BackingField_7)); }
	inline FsmEventTarget_t1823904941 * get_U3CEventTargetU3Ek__BackingField_7() const { return ___U3CEventTargetU3Ek__BackingField_7; }
	inline FsmEventTarget_t1823904941 ** get_address_of_U3CEventTargetU3Ek__BackingField_7() { return &___U3CEventTargetU3Ek__BackingField_7; }
	inline void set_U3CEventTargetU3Ek__BackingField_7(FsmEventTarget_t1823904941 * value)
	{
		___U3CEventTargetU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CEventTargetU3Ek__BackingField_7, value);
	}

	inline static int32_t get_offset_of_U3CTimeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(FsmLogEntry_t2614866584, ___U3CTimeU3Ek__BackingField_8)); }
	inline float get_U3CTimeU3Ek__BackingField_8() const { return ___U3CTimeU3Ek__BackingField_8; }
	inline float* get_address_of_U3CTimeU3Ek__BackingField_8() { return &___U3CTimeU3Ek__BackingField_8; }
	inline void set_U3CTimeU3Ek__BackingField_8(float value)
	{
		___U3CTimeU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CTextU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(FsmLogEntry_t2614866584, ___U3CTextU3Ek__BackingField_9)); }
	inline String_t* get_U3CTextU3Ek__BackingField_9() const { return ___U3CTextU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CTextU3Ek__BackingField_9() { return &___U3CTextU3Ek__BackingField_9; }
	inline void set_U3CTextU3Ek__BackingField_9(String_t* value)
	{
		___U3CTextU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTextU3Ek__BackingField_9, value);
	}

	inline static int32_t get_offset_of_U3CText2U3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(FsmLogEntry_t2614866584, ___U3CText2U3Ek__BackingField_10)); }
	inline String_t* get_U3CText2U3Ek__BackingField_10() const { return ___U3CText2U3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CText2U3Ek__BackingField_10() { return &___U3CText2U3Ek__BackingField_10; }
	inline void set_U3CText2U3Ek__BackingField_10(String_t* value)
	{
		___U3CText2U3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CText2U3Ek__BackingField_10, value);
	}

	inline static int32_t get_offset_of_U3CFrameCountU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(FsmLogEntry_t2614866584, ___U3CFrameCountU3Ek__BackingField_11)); }
	inline int32_t get_U3CFrameCountU3Ek__BackingField_11() const { return ___U3CFrameCountU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CFrameCountU3Ek__BackingField_11() { return &___U3CFrameCountU3Ek__BackingField_11; }
	inline void set_U3CFrameCountU3Ek__BackingField_11(int32_t value)
	{
		___U3CFrameCountU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CFsmVariablesCopyU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(FsmLogEntry_t2614866584, ___U3CFsmVariablesCopyU3Ek__BackingField_12)); }
	inline FsmVariables_t963491929 * get_U3CFsmVariablesCopyU3Ek__BackingField_12() const { return ___U3CFsmVariablesCopyU3Ek__BackingField_12; }
	inline FsmVariables_t963491929 ** get_address_of_U3CFsmVariablesCopyU3Ek__BackingField_12() { return &___U3CFsmVariablesCopyU3Ek__BackingField_12; }
	inline void set_U3CFsmVariablesCopyU3Ek__BackingField_12(FsmVariables_t963491929 * value)
	{
		___U3CFsmVariablesCopyU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CFsmVariablesCopyU3Ek__BackingField_12, value);
	}

	inline static int32_t get_offset_of_U3CGlobalVariablesCopyU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(FsmLogEntry_t2614866584, ___U3CGlobalVariablesCopyU3Ek__BackingField_13)); }
	inline FsmVariables_t963491929 * get_U3CGlobalVariablesCopyU3Ek__BackingField_13() const { return ___U3CGlobalVariablesCopyU3Ek__BackingField_13; }
	inline FsmVariables_t963491929 ** get_address_of_U3CGlobalVariablesCopyU3Ek__BackingField_13() { return &___U3CGlobalVariablesCopyU3Ek__BackingField_13; }
	inline void set_U3CGlobalVariablesCopyU3Ek__BackingField_13(FsmVariables_t963491929 * value)
	{
		___U3CGlobalVariablesCopyU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CGlobalVariablesCopyU3Ek__BackingField_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
