﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState
struct CurrentParsingState_t868552452;

#include "codegen/il2cpp-codegen.h"

// System.Void ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState::.ctor()
extern "C"  void CurrentParsingState__ctor_m460792973 (CurrentParsingState_t868552452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState::getPosition()
extern "C"  int32_t CurrentParsingState_getPosition_m1031037724 (CurrentParsingState_t868552452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState::setPosition(System.Int32)
extern "C"  void CurrentParsingState_setPosition_m2665103239 (CurrentParsingState_t868552452 * __this, int32_t ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState::incrementPosition(System.Int32)
extern "C"  void CurrentParsingState_incrementPosition_m4114167636 (CurrentParsingState_t868552452 * __this, int32_t ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState::isAlpha()
extern "C"  bool CurrentParsingState_isAlpha_m2382736939 (CurrentParsingState_t868552452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState::isIsoIec646()
extern "C"  bool CurrentParsingState_isIsoIec646_m4287753923 (CurrentParsingState_t868552452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState::setNumeric()
extern "C"  void CurrentParsingState_setNumeric_m3697230850 (CurrentParsingState_t868552452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState::setAlpha()
extern "C"  void CurrentParsingState_setAlpha_m2794702611 (CurrentParsingState_t868552452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState::setIsoIec646()
extern "C"  void CurrentParsingState_setIsoIec646_m2149143467 (CurrentParsingState_t868552452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
