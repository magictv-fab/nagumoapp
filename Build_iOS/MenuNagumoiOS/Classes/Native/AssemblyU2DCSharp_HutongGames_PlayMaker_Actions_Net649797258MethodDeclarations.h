﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkGetConnectionsCount
struct NetworkGetConnectionsCount_t649797258;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkGetConnectionsCount::.ctor()
extern "C"  void NetworkGetConnectionsCount__ctor_m3390718236 (NetworkGetConnectionsCount_t649797258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetConnectionsCount::Reset()
extern "C"  void NetworkGetConnectionsCount_Reset_m1037151177 (NetworkGetConnectionsCount_t649797258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetConnectionsCount::OnEnter()
extern "C"  void NetworkGetConnectionsCount_OnEnter_m2286743027 (NetworkGetConnectionsCount_t649797258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetConnectionsCount::OnUpdate()
extern "C"  void NetworkGetConnectionsCount_OnUpdate_m1303116464 (NetworkGetConnectionsCount_t649797258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
