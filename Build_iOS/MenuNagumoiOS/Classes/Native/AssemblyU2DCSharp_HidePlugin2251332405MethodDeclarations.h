﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HidePlugin
struct HidePlugin_t2251332405;

#include "codegen/il2cpp-codegen.h"

// System.Void HidePlugin::.ctor()
extern "C"  void HidePlugin__ctor_m374196486 (HidePlugin_t2251332405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HidePlugin::HideUnity()
extern "C"  void HidePlugin_HideUnity_m999350071 (HidePlugin_t2251332405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
