﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ToggleCamera
struct ToggleCamera_t809234361;

#include "codegen/il2cpp-codegen.h"

// System.Void ToggleCamera::.ctor()
extern "C"  void ToggleCamera__ctor_m2208015106 (ToggleCamera_t809234361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToggleCamera::cameraOn()
extern "C"  void ToggleCamera_cameraOn_m3797519430 (ToggleCamera_t809234361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToggleCamera::cameraOff()
extern "C"  void ToggleCamera_cameraOff_m1758806602 (ToggleCamera_t809234361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
