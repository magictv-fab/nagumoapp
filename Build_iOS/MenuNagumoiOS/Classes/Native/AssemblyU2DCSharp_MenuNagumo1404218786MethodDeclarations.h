﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MenuNagumo
struct MenuNagumo_t1404218786;

#include "codegen/il2cpp-codegen.h"

// System.Void MenuNagumo::.ctor()
extern "C"  void MenuNagumo__ctor_m2286231865 (MenuNagumo_t1404218786 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuNagumo::Start()
extern "C"  void MenuNagumo_Start_m1233369657 (MenuNagumo_t1404218786 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuNagumo::Update()
extern "C"  void MenuNagumo_Update_m3880573172 (MenuNagumo_t1404218786 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
