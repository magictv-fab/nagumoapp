﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicApplicationSettings/<ResetHard>c__AnonStorey95
struct  U3CResetHardU3Ec__AnonStorey95_t4115973580  : public Il2CppObject
{
public:
	// System.String MagicApplicationSettings/<ResetHard>c__AnonStorey95::folderOfThisFile
	String_t* ___folderOfThisFile_0;

public:
	inline static int32_t get_offset_of_folderOfThisFile_0() { return static_cast<int32_t>(offsetof(U3CResetHardU3Ec__AnonStorey95_t4115973580, ___folderOfThisFile_0)); }
	inline String_t* get_folderOfThisFile_0() const { return ___folderOfThisFile_0; }
	inline String_t** get_address_of_folderOfThisFile_0() { return &___folderOfThisFile_0; }
	inline void set_folderOfThisFile_0(String_t* value)
	{
		___folderOfThisFile_0 = value;
		Il2CppCodeGenWriteBarrier(&___folderOfThisFile_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
