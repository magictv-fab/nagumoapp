﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.utils.bytes.ByteListAbstract
struct ByteListAbstract_t980263662;
// ARM.utils.bytes.ByteListAbstract/OnReadyEventHandler
struct OnReadyEventHandler_t1972392543;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ARM_utils_bytes_ByteListAbstract1972392543.h"

// System.Void ARM.utils.bytes.ByteListAbstract::.ctor()
extern "C"  void ByteListAbstract__ctor_m2500246593 (ByteListAbstract_t980263662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 ARM.utils.bytes.ByteListAbstract::get_maxByteValue()
extern "C"  uint32_t ByteListAbstract_get_maxByteValue_m937764204 (ByteListAbstract_t980263662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.bytes.ByteListAbstract::addOnReadyEventListener(ARM.utils.bytes.ByteListAbstract/OnReadyEventHandler)
extern "C"  void ByteListAbstract_addOnReadyEventListener_m750675275 (ByteListAbstract_t980263662 * __this, OnReadyEventHandler_t1972392543 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.bytes.ByteListAbstract::removeOnReadyEventListener(ARM.utils.bytes.ByteListAbstract/OnReadyEventHandler)
extern "C"  void ByteListAbstract_removeOnReadyEventListener_m103409842 (ByteListAbstract_t980263662 * __this, OnReadyEventHandler_t1972392543 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.bytes.ByteListAbstract::raiseOnReadyEvent()
extern "C"  void ByteListAbstract_raiseOnReadyEvent_m661410817 (ByteListAbstract_t980263662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
