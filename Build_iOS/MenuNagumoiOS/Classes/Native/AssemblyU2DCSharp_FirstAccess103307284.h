﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FirstAccess
struct  FirstAccess_t103307284  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject FirstAccess::screenFirst
	GameObject_t3674682005 * ___screenFirst_2;
	// UnityEngine.GameObject FirstAccess::loadScreen
	GameObject_t3674682005 * ___loadScreen_3;
	// System.String FirstAccess::url
	String_t* ___url_4;

public:
	inline static int32_t get_offset_of_screenFirst_2() { return static_cast<int32_t>(offsetof(FirstAccess_t103307284, ___screenFirst_2)); }
	inline GameObject_t3674682005 * get_screenFirst_2() const { return ___screenFirst_2; }
	inline GameObject_t3674682005 ** get_address_of_screenFirst_2() { return &___screenFirst_2; }
	inline void set_screenFirst_2(GameObject_t3674682005 * value)
	{
		___screenFirst_2 = value;
		Il2CppCodeGenWriteBarrier(&___screenFirst_2, value);
	}

	inline static int32_t get_offset_of_loadScreen_3() { return static_cast<int32_t>(offsetof(FirstAccess_t103307284, ___loadScreen_3)); }
	inline GameObject_t3674682005 * get_loadScreen_3() const { return ___loadScreen_3; }
	inline GameObject_t3674682005 ** get_address_of_loadScreen_3() { return &___loadScreen_3; }
	inline void set_loadScreen_3(GameObject_t3674682005 * value)
	{
		___loadScreen_3 = value;
		Il2CppCodeGenWriteBarrier(&___loadScreen_3, value);
	}

	inline static int32_t get_offset_of_url_4() { return static_cast<int32_t>(offsetof(FirstAccess_t103307284, ___url_4)); }
	inline String_t* get_url_4() const { return ___url_4; }
	inline String_t** get_address_of_url_4() { return &___url_4; }
	inline void set_url_4(String_t* value)
	{
		___url_4 = value;
		Il2CppCodeGenWriteBarrier(&___url_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
