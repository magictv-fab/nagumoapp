﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.Mobile.Android.FBJavaClass/AndroidJavaClass
struct AndroidJavaClass_t1906676742;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void Facebook.Unity.Mobile.Android.FBJavaClass/AndroidJavaClass::.ctor(System.String)
extern "C"  void AndroidJavaClass__ctor_m4170044877 (AndroidJavaClass_t1906676742 * __this, String_t* ___mock0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Mobile.Android.FBJavaClass/AndroidJavaClass::CallStatic(System.String,System.Object[])
extern "C"  void AndroidJavaClass_CallStatic_m486019859 (AndroidJavaClass_t1906676742 * __this, String_t* ___method0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
