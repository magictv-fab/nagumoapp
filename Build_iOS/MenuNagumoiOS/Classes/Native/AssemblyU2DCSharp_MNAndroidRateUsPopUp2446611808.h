﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_MNPopup1928680331.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MNAndroidRateUsPopUp
struct  MNAndroidRateUsPopUp_t2446611808  : public MNPopup_t1928680331
{
public:
	// System.String MNAndroidRateUsPopUp::yes
	String_t* ___yes_6;
	// System.String MNAndroidRateUsPopUp::later
	String_t* ___later_7;
	// System.String MNAndroidRateUsPopUp::no
	String_t* ___no_8;
	// System.String MNAndroidRateUsPopUp::url
	String_t* ___url_9;

public:
	inline static int32_t get_offset_of_yes_6() { return static_cast<int32_t>(offsetof(MNAndroidRateUsPopUp_t2446611808, ___yes_6)); }
	inline String_t* get_yes_6() const { return ___yes_6; }
	inline String_t** get_address_of_yes_6() { return &___yes_6; }
	inline void set_yes_6(String_t* value)
	{
		___yes_6 = value;
		Il2CppCodeGenWriteBarrier(&___yes_6, value);
	}

	inline static int32_t get_offset_of_later_7() { return static_cast<int32_t>(offsetof(MNAndroidRateUsPopUp_t2446611808, ___later_7)); }
	inline String_t* get_later_7() const { return ___later_7; }
	inline String_t** get_address_of_later_7() { return &___later_7; }
	inline void set_later_7(String_t* value)
	{
		___later_7 = value;
		Il2CppCodeGenWriteBarrier(&___later_7, value);
	}

	inline static int32_t get_offset_of_no_8() { return static_cast<int32_t>(offsetof(MNAndroidRateUsPopUp_t2446611808, ___no_8)); }
	inline String_t* get_no_8() const { return ___no_8; }
	inline String_t** get_address_of_no_8() { return &___no_8; }
	inline void set_no_8(String_t* value)
	{
		___no_8 = value;
		Il2CppCodeGenWriteBarrier(&___no_8, value);
	}

	inline static int32_t get_offset_of_url_9() { return static_cast<int32_t>(offsetof(MNAndroidRateUsPopUp_t2446611808, ___url_9)); }
	inline String_t* get_url_9() const { return ___url_9; }
	inline String_t** get_address_of_url_9() { return &___url_9; }
	inline void set_url_9(String_t* value)
	{
		___url_9 = value;
		Il2CppCodeGenWriteBarrier(&___url_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
