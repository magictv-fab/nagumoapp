﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.vo.FileVO
struct FileVO_t1935348659;
// System.String
struct String_t;
// MagicTV.vo.UrlInfoVO[]
struct UrlInfoVOU5BU5D_t1681515545;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MagicTV.vo.FileVO::.ctor()
extern "C"  void FileVO__ctor_m1598524928 (FileVO_t1935348659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.vo.FileVO::ToString()
extern "C"  String_t* FileVO_ToString_m1824975341 (FileVO_t1935348659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.UrlInfoVO[] MagicTV.vo.FileVO::getByExtension(System.String)
extern "C"  UrlInfoVOU5BU5D_t1681515545* FileVO_getByExtension_m983321191 (FileVO_t1935348659 * __this, String_t* ___ext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
