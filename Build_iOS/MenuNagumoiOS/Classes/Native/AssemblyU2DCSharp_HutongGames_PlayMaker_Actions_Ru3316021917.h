﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmTemplateControl
struct FsmTemplateControl_t2786508133;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.Fsm
struct Fsm_t1527112426;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RunFSM
struct  RunFSM_t3316021917  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmTemplateControl HutongGames.PlayMaker.Actions.RunFSM::fsmTemplateControl
	FsmTemplateControl_t2786508133 * ___fsmTemplateControl_9;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.RunFSM::storeID
	FsmInt_t1596138449 * ___storeID_10;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.RunFSM::finishEvent
	FsmEvent_t2133468028 * ___finishEvent_11;
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.Actions.RunFSM::runFsm
	Fsm_t1527112426 * ___runFsm_12;

public:
	inline static int32_t get_offset_of_fsmTemplateControl_9() { return static_cast<int32_t>(offsetof(RunFSM_t3316021917, ___fsmTemplateControl_9)); }
	inline FsmTemplateControl_t2786508133 * get_fsmTemplateControl_9() const { return ___fsmTemplateControl_9; }
	inline FsmTemplateControl_t2786508133 ** get_address_of_fsmTemplateControl_9() { return &___fsmTemplateControl_9; }
	inline void set_fsmTemplateControl_9(FsmTemplateControl_t2786508133 * value)
	{
		___fsmTemplateControl_9 = value;
		Il2CppCodeGenWriteBarrier(&___fsmTemplateControl_9, value);
	}

	inline static int32_t get_offset_of_storeID_10() { return static_cast<int32_t>(offsetof(RunFSM_t3316021917, ___storeID_10)); }
	inline FsmInt_t1596138449 * get_storeID_10() const { return ___storeID_10; }
	inline FsmInt_t1596138449 ** get_address_of_storeID_10() { return &___storeID_10; }
	inline void set_storeID_10(FsmInt_t1596138449 * value)
	{
		___storeID_10 = value;
		Il2CppCodeGenWriteBarrier(&___storeID_10, value);
	}

	inline static int32_t get_offset_of_finishEvent_11() { return static_cast<int32_t>(offsetof(RunFSM_t3316021917, ___finishEvent_11)); }
	inline FsmEvent_t2133468028 * get_finishEvent_11() const { return ___finishEvent_11; }
	inline FsmEvent_t2133468028 ** get_address_of_finishEvent_11() { return &___finishEvent_11; }
	inline void set_finishEvent_11(FsmEvent_t2133468028 * value)
	{
		___finishEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___finishEvent_11, value);
	}

	inline static int32_t get_offset_of_runFsm_12() { return static_cast<int32_t>(offsetof(RunFSM_t3316021917, ___runFsm_12)); }
	inline Fsm_t1527112426 * get_runFsm_12() const { return ___runFsm_12; }
	inline Fsm_t1527112426 ** get_address_of_runFsm_12() { return &___runFsm_12; }
	inline void set_runFsm_12(Fsm_t1527112426 * value)
	{
		___runFsm_12 = value;
		Il2CppCodeGenWriteBarrier(&___runFsm_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
