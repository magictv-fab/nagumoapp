﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Eas595986710.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EaseVector3
struct  EaseVector3_t1125521936  : public EaseFsmAction_t595986710
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.EaseVector3::fromValue
	FsmVector3_t533912882 * ___fromValue_30;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.EaseVector3::toValue
	FsmVector3_t533912882 * ___toValue_31;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.EaseVector3::vector3Variable
	FsmVector3_t533912882 * ___vector3Variable_32;
	// System.Boolean HutongGames.PlayMaker.Actions.EaseVector3::finishInNextStep
	bool ___finishInNextStep_33;

public:
	inline static int32_t get_offset_of_fromValue_30() { return static_cast<int32_t>(offsetof(EaseVector3_t1125521936, ___fromValue_30)); }
	inline FsmVector3_t533912882 * get_fromValue_30() const { return ___fromValue_30; }
	inline FsmVector3_t533912882 ** get_address_of_fromValue_30() { return &___fromValue_30; }
	inline void set_fromValue_30(FsmVector3_t533912882 * value)
	{
		___fromValue_30 = value;
		Il2CppCodeGenWriteBarrier(&___fromValue_30, value);
	}

	inline static int32_t get_offset_of_toValue_31() { return static_cast<int32_t>(offsetof(EaseVector3_t1125521936, ___toValue_31)); }
	inline FsmVector3_t533912882 * get_toValue_31() const { return ___toValue_31; }
	inline FsmVector3_t533912882 ** get_address_of_toValue_31() { return &___toValue_31; }
	inline void set_toValue_31(FsmVector3_t533912882 * value)
	{
		___toValue_31 = value;
		Il2CppCodeGenWriteBarrier(&___toValue_31, value);
	}

	inline static int32_t get_offset_of_vector3Variable_32() { return static_cast<int32_t>(offsetof(EaseVector3_t1125521936, ___vector3Variable_32)); }
	inline FsmVector3_t533912882 * get_vector3Variable_32() const { return ___vector3Variable_32; }
	inline FsmVector3_t533912882 ** get_address_of_vector3Variable_32() { return &___vector3Variable_32; }
	inline void set_vector3Variable_32(FsmVector3_t533912882 * value)
	{
		___vector3Variable_32 = value;
		Il2CppCodeGenWriteBarrier(&___vector3Variable_32, value);
	}

	inline static int32_t get_offset_of_finishInNextStep_33() { return static_cast<int32_t>(offsetof(EaseVector3_t1125521936, ___finishInNextStep_33)); }
	inline bool get_finishInNextStep_33() const { return ___finishInNextStep_33; }
	inline bool* get_address_of_finishInNextStep_33() { return &___finishInNextStep_33; }
	inline void set_finishInNextStep_33(bool value)
	{
		___finishInNextStep_33 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
