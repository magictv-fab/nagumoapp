﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.TitleAttribute
struct TitleAttribute_t3939375206;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.String HutongGames.PlayMaker.TitleAttribute::get_Text()
extern "C"  String_t* TitleAttribute_get_Text_m2008702864 (TitleAttribute_t3939375206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.TitleAttribute::.ctor(System.String)
extern "C"  void TitleAttribute__ctor_m14952309 (TitleAttribute_t3939375206 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
