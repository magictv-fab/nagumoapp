﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.globals.DeviceFileInfoJson/<filterUrlFromId>c__AnonStorey9D
struct U3CfilterUrlFromIdU3Ec__AnonStorey9D_t3361971262;
// MagicTV.vo.UrlInfoVO
struct UrlInfoVO_t1761987528;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_vo_UrlInfoVO1761987528.h"

// System.Void MagicTV.globals.DeviceFileInfoJson/<filterUrlFromId>c__AnonStorey9D::.ctor()
extern "C"  void U3CfilterUrlFromIdU3Ec__AnonStorey9D__ctor_m4085968109 (U3CfilterUrlFromIdU3Ec__AnonStorey9D_t3361971262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicTV.globals.DeviceFileInfoJson/<filterUrlFromId>c__AnonStorey9D::<>m__3A(MagicTV.vo.UrlInfoVO)
extern "C"  bool U3CfilterUrlFromIdU3Ec__AnonStorey9D_U3CU3Em__3A_m1278589368 (U3CfilterUrlFromIdU3Ec__AnonStorey9D_t3361971262 * __this, UrlInfoVO_t1761987528 * ___u0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
