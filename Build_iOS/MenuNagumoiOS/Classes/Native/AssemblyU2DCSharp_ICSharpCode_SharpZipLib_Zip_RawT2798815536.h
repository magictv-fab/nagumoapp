﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.RawTaggedData
struct  RawTaggedData_t2798815536  : public Il2CppObject
{
public:
	// System.Int16 ICSharpCode.SharpZipLib.Zip.RawTaggedData::_tag
	int16_t ____tag_0;
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.RawTaggedData::_data
	ByteU5BU5D_t4260760469* ____data_1;

public:
	inline static int32_t get_offset_of__tag_0() { return static_cast<int32_t>(offsetof(RawTaggedData_t2798815536, ____tag_0)); }
	inline int16_t get__tag_0() const { return ____tag_0; }
	inline int16_t* get_address_of__tag_0() { return &____tag_0; }
	inline void set__tag_0(int16_t value)
	{
		____tag_0 = value;
	}

	inline static int32_t get_offset_of__data_1() { return static_cast<int32_t>(offsetof(RawTaggedData_t2798815536, ____data_1)); }
	inline ByteU5BU5D_t4260760469* get__data_1() const { return ____data_1; }
	inline ByteU5BU5D_t4260760469** get_address_of__data_1() { return &____data_1; }
	inline void set__data_1(ByteU5BU5D_t4260760469* value)
	{
		____data_1 = value;
		Il2CppCodeGenWriteBarrier(&____data_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
