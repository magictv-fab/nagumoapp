﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Datamatrix.Encoder.X12Encoder
struct X12Encoder_t1108078463;
// ZXing.Datamatrix.Encoder.EncoderContext
struct EncoderContext_t1774722223;
// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Datamatrix_Encoder_EncoderContext1774722223.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"

// System.Int32 ZXing.Datamatrix.Encoder.X12Encoder::get_EncodingMode()
extern "C"  int32_t X12Encoder_get_EncodingMode_m2979667960 (X12Encoder_t1108078463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.X12Encoder::encode(ZXing.Datamatrix.Encoder.EncoderContext)
extern "C"  void X12Encoder_encode_m1161321655 (X12Encoder_t1108078463 * __this, EncoderContext_t1774722223 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Encoder.X12Encoder::encodeChar(System.Char,System.Text.StringBuilder)
extern "C"  int32_t X12Encoder_encodeChar_m141422748 (X12Encoder_t1108078463 * __this, Il2CppChar ___c0, StringBuilder_t243639308 * ___sb1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.X12Encoder::handleEOD(ZXing.Datamatrix.Encoder.EncoderContext,System.Text.StringBuilder)
extern "C"  void X12Encoder_handleEOD_m3125071307 (X12Encoder_t1108078463 * __this, EncoderContext_t1774722223 * ___context0, StringBuilder_t243639308 * ___buffer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.X12Encoder::.ctor()
extern "C"  void X12Encoder__ctor_m4156661181 (X12Encoder_t1108078463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
