﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.globals.events.StateMagineEvents
struct StateMagineEvents_t1499601201;
// MagicTV.globals.events.StateMagineEvents/OnChangeToStateEventHandler
struct OnChangeToStateEventHandler_t4233189127;
// MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler
struct OnChangeToAnStateEventHandler_t630243546;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_globals_events_StateMagi4233189127.h"
#include "AssemblyU2DCSharp_MagicTV_globals_events_StateMagin630243546.h"
#include "AssemblyU2DCSharp_MagicTV_globals_StateMachine367995870.h"

// System.Void MagicTV.globals.events.StateMagineEvents::.ctor()
extern "C"  void StateMagineEvents__ctor_m2006436508 (StateMagineEvents_t1499601201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.StateMagineEvents::AddChangeToStateEventHandler(MagicTV.globals.events.StateMagineEvents/OnChangeToStateEventHandler)
extern "C"  void StateMagineEvents_AddChangeToStateEventHandler_m4053022358 (StateMagineEvents_t1499601201 * __this, OnChangeToStateEventHandler_t4233189127 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.StateMagineEvents::RemoveChangeToStateEventHandler(MagicTV.globals.events.StateMagineEvents/OnChangeToStateEventHandler)
extern "C"  void StateMagineEvents_RemoveChangeToStateEventHandler_m432601509 (StateMagineEvents_t1499601201 * __this, OnChangeToStateEventHandler_t4233189127 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.StateMagineEvents::AddChangeToStateEventHandler(MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler,MagicTV.globals.StateMachine)
extern "C"  void StateMagineEvents_AddChangeToStateEventHandler_m99738576 (StateMagineEvents_t1499601201 * __this, OnChangeToAnStateEventHandler_t630243546 * ___action0, int32_t ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.StateMagineEvents::RemoveChangeToStateEventHandler(MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler,MagicTV.globals.StateMachine)
extern "C"  void StateMagineEvents_RemoveChangeToStateEventHandler_m2461564641 (StateMagineEvents_t1499601201 * __this, OnChangeToAnStateEventHandler_t630243546 * ___action0, int32_t ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.StateMagineEvents::RaiseChangeToState(MagicTV.globals.StateMachine)
extern "C"  void StateMagineEvents_RaiseChangeToState_m757593455 (StateMagineEvents_t1499601201 * __this, int32_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.globals.events.StateMagineEvents MagicTV.globals.events.StateMagineEvents::GetInstance()
extern "C"  StateMagineEvents_t1499601201 * StateMagineEvents_GetInstance_m2831648967 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
