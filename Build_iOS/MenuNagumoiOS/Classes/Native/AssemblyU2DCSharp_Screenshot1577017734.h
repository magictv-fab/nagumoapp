﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// Screenshot
struct Screenshot_t1577017734;
// Screenshot/OnCompleteEventHandler
struct OnCompleteEventHandler_t2690410097;
// System.Func`2<System.String,System.Boolean>
struct Func_2_t3730860138;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Screenshot
struct  Screenshot_t1577017734  : public MonoBehaviour_t667441552
{
public:
	// Screenshot/OnCompleteEventHandler Screenshot::onComplete
	OnCompleteEventHandler_t2690410097 * ___onComplete_6;
	// System.String Screenshot::_path
	String_t* ____path_7;

public:
	inline static int32_t get_offset_of_onComplete_6() { return static_cast<int32_t>(offsetof(Screenshot_t1577017734, ___onComplete_6)); }
	inline OnCompleteEventHandler_t2690410097 * get_onComplete_6() const { return ___onComplete_6; }
	inline OnCompleteEventHandler_t2690410097 ** get_address_of_onComplete_6() { return &___onComplete_6; }
	inline void set_onComplete_6(OnCompleteEventHandler_t2690410097 * value)
	{
		___onComplete_6 = value;
		Il2CppCodeGenWriteBarrier(&___onComplete_6, value);
	}

	inline static int32_t get_offset_of__path_7() { return static_cast<int32_t>(offsetof(Screenshot_t1577017734, ____path_7)); }
	inline String_t* get__path_7() const { return ____path_7; }
	inline String_t** get_address_of__path_7() { return &____path_7; }
	inline void set__path_7(String_t* value)
	{
		____path_7 = value;
		Il2CppCodeGenWriteBarrier(&____path_7, value);
	}
};

struct Screenshot_t1577017734_StaticFields
{
public:
	// UnityEngine.GameObject Screenshot::_selfGameObject
	GameObject_t3674682005 * ____selfGameObject_3;
	// System.String Screenshot::fotoPath
	String_t* ___fotoPath_4;
	// Screenshot Screenshot::_selfInstance
	Screenshot_t1577017734 * ____selfInstance_5;
	// System.Func`2<System.String,System.Boolean> Screenshot::<>f__am$cache5
	Func_2_t3730860138 * ___U3CU3Ef__amU24cache5_8;

public:
	inline static int32_t get_offset_of__selfGameObject_3() { return static_cast<int32_t>(offsetof(Screenshot_t1577017734_StaticFields, ____selfGameObject_3)); }
	inline GameObject_t3674682005 * get__selfGameObject_3() const { return ____selfGameObject_3; }
	inline GameObject_t3674682005 ** get_address_of__selfGameObject_3() { return &____selfGameObject_3; }
	inline void set__selfGameObject_3(GameObject_t3674682005 * value)
	{
		____selfGameObject_3 = value;
		Il2CppCodeGenWriteBarrier(&____selfGameObject_3, value);
	}

	inline static int32_t get_offset_of_fotoPath_4() { return static_cast<int32_t>(offsetof(Screenshot_t1577017734_StaticFields, ___fotoPath_4)); }
	inline String_t* get_fotoPath_4() const { return ___fotoPath_4; }
	inline String_t** get_address_of_fotoPath_4() { return &___fotoPath_4; }
	inline void set_fotoPath_4(String_t* value)
	{
		___fotoPath_4 = value;
		Il2CppCodeGenWriteBarrier(&___fotoPath_4, value);
	}

	inline static int32_t get_offset_of__selfInstance_5() { return static_cast<int32_t>(offsetof(Screenshot_t1577017734_StaticFields, ____selfInstance_5)); }
	inline Screenshot_t1577017734 * get__selfInstance_5() const { return ____selfInstance_5; }
	inline Screenshot_t1577017734 ** get_address_of__selfInstance_5() { return &____selfInstance_5; }
	inline void set__selfInstance_5(Screenshot_t1577017734 * value)
	{
		____selfInstance_5 = value;
		Il2CppCodeGenWriteBarrier(&____selfInstance_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_8() { return static_cast<int32_t>(offsetof(Screenshot_t1577017734_StaticFields, ___U3CU3Ef__amU24cache5_8)); }
	inline Func_2_t3730860138 * get_U3CU3Ef__amU24cache5_8() const { return ___U3CU3Ef__amU24cache5_8; }
	inline Func_2_t3730860138 ** get_address_of_U3CU3Ef__amU24cache5_8() { return &___U3CU3Ef__amU24cache5_8; }
	inline void set_U3CU3Ef__amU24cache5_8(Func_2_t3730860138 * value)
	{
		___U3CU3Ef__amU24cache5_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
