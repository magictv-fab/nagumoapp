﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetStringValue
struct SetStringValue_t3445720326;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetStringValue::.ctor()
extern "C"  void SetStringValue__ctor_m2295319072 (SetStringValue_t3445720326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetStringValue::Reset()
extern "C"  void SetStringValue_Reset_m4236719309 (SetStringValue_t3445720326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetStringValue::OnEnter()
extern "C"  void SetStringValue_OnEnter_m1875133943 (SetStringValue_t3445720326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetStringValue::OnUpdate()
extern "C"  void SetStringValue_OnUpdate_m1428136748 (SetStringValue_t3445720326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetStringValue::DoSetStringValue()
extern "C"  void SetStringValue_DoSetStringValue_m4016712685 (SetStringValue_t3445720326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
