﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmObject
struct FsmObject_t821476169;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "PlayMaker_HutongGames_PlayMaker_LogLevel284580066.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DebugObject
struct  DebugObject_t2190156288  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.LogLevel HutongGames.PlayMaker.Actions.DebugObject::logLevel
	int32_t ___logLevel_9;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.DebugObject::fsmObject
	FsmObject_t821476169 * ___fsmObject_10;

public:
	inline static int32_t get_offset_of_logLevel_9() { return static_cast<int32_t>(offsetof(DebugObject_t2190156288, ___logLevel_9)); }
	inline int32_t get_logLevel_9() const { return ___logLevel_9; }
	inline int32_t* get_address_of_logLevel_9() { return &___logLevel_9; }
	inline void set_logLevel_9(int32_t value)
	{
		___logLevel_9 = value;
	}

	inline static int32_t get_offset_of_fsmObject_10() { return static_cast<int32_t>(offsetof(DebugObject_t2190156288, ___fsmObject_10)); }
	inline FsmObject_t821476169 * get_fsmObject_10() const { return ___fsmObject_10; }
	inline FsmObject_t821476169 ** get_address_of_fsmObject_10() { return &___fsmObject_10; }
	inline void set_fsmObject_10(FsmObject_t821476169 * value)
	{
		___fsmObject_10 = value;
		Il2CppCodeGenWriteBarrier(&___fsmObject_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
