﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.EnableAnimation
struct EnableAnimation_t3592025679;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void HutongGames.PlayMaker.Actions.EnableAnimation::.ctor()
extern "C"  void EnableAnimation__ctor_m3959089607 (EnableAnimation_t3592025679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnableAnimation::Reset()
extern "C"  void EnableAnimation_Reset_m1605522548 (EnableAnimation_t3592025679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnableAnimation::OnEnter()
extern "C"  void EnableAnimation_OnEnter_m3030783966 (EnableAnimation_t3592025679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnableAnimation::DoEnableAnimation(UnityEngine.GameObject)
extern "C"  void EnableAnimation_DoEnableAnimation_m3112802771 (EnableAnimation_t3592025679 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnableAnimation::OnExit()
extern "C"  void EnableAnimation_OnExit_m3986012154 (EnableAnimation_t3592025679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
