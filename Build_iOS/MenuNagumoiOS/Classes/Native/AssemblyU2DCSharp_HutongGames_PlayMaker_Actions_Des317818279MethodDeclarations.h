﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DestroyObject
struct DestroyObject_t317818279;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DestroyObject::.ctor()
extern "C"  void DestroyObject__ctor_m2180790127 (DestroyObject_t317818279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DestroyObject::Reset()
extern "C"  void DestroyObject_Reset_m4122190364 (DestroyObject_t317818279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DestroyObject::OnEnter()
extern "C"  void DestroyObject_OnEnter_m3481967494 (DestroyObject_t317818279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DestroyObject::OnUpdate()
extern "C"  void DestroyObject_OnUpdate_m3995336573 (DestroyObject_t317818279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
