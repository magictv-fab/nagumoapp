﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Com700434209.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RemoveMixingTransform
struct  RemoveMixingTransform_t2995927984  : public ComponentAction_1_t700434209
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RemoveMixingTransform::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.RemoveMixingTransform::animationName
	FsmString_t952858651 * ___animationName_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.RemoveMixingTransform::transfrom
	FsmString_t952858651 * ___transfrom_13;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(RemoveMixingTransform_t2995927984, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_animationName_12() { return static_cast<int32_t>(offsetof(RemoveMixingTransform_t2995927984, ___animationName_12)); }
	inline FsmString_t952858651 * get_animationName_12() const { return ___animationName_12; }
	inline FsmString_t952858651 ** get_address_of_animationName_12() { return &___animationName_12; }
	inline void set_animationName_12(FsmString_t952858651 * value)
	{
		___animationName_12 = value;
		Il2CppCodeGenWriteBarrier(&___animationName_12, value);
	}

	inline static int32_t get_offset_of_transfrom_13() { return static_cast<int32_t>(offsetof(RemoveMixingTransform_t2995927984, ___transfrom_13)); }
	inline FsmString_t952858651 * get_transfrom_13() const { return ___transfrom_13; }
	inline FsmString_t952858651 ** get_address_of_transfrom_13() { return &___transfrom_13; }
	inline void set_transfrom_13(FsmString_t952858651 * value)
	{
		___transfrom_13 = value;
		Il2CppCodeGenWriteBarrier(&___transfrom_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
