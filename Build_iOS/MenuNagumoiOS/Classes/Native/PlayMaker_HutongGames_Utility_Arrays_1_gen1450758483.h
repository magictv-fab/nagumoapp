﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmVector2[]
struct FsmVector2U5BU5D_t120994540;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.Utility.Arrays`1<HutongGames.PlayMaker.FsmVector2>
struct  Arrays_1_t1450758483  : public Il2CppObject
{
public:

public:
};

struct Arrays_1_t1450758483_StaticFields
{
public:
	// T[] HutongGames.Utility.Arrays`1::Empty
	FsmVector2U5BU5D_t120994540* ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Arrays_1_t1450758483_StaticFields, ___Empty_0)); }
	inline FsmVector2U5BU5D_t120994540* get_Empty_0() const { return ___Empty_0; }
	inline FsmVector2U5BU5D_t120994540** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(FsmVector2U5BU5D_t120994540* value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier(&___Empty_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
