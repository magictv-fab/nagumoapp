﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VideoTexture_Material
struct VideoTexture_Material_t3546220134;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UVT_PlayDirection3288864031.h"
#include "AssemblyU2DCSharp_UVT_PlayState1348117041.h"

// System.Void VideoTexture_Material::.ctor()
extern "C"  void VideoTexture_Material__ctor_m3637626309 (VideoTexture_Material_t3546220134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoTexture_Material::Awake()
extern "C"  void VideoTexture_Material_Awake_m3875231528 (VideoTexture_Material_t3546220134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoTexture_Material::Start()
extern "C"  void VideoTexture_Material_Start_m2584764101 (VideoTexture_Material_t3546220134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoTexture_Material::Update()
extern "C"  void VideoTexture_Material_Update_m2824127976 (VideoTexture_Material_t3546220134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoTexture_Material::HandleControls()
extern "C"  void VideoTexture_Material_HandleControls_m1988593373 (VideoTexture_Material_t3546220134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoTexture_Material::EnableControls(System.Boolean)
extern "C"  void VideoTexture_Material_EnableControls_m3598683119 (VideoTexture_Material_t3546220134 * __this, bool ___EnableControls0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoTexture_Material::ForceAudioSync()
extern "C"  void VideoTexture_Material_ForceAudioSync_m595376325 (VideoTexture_Material_t3546220134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoTexture_Material::UpdatePlayFactor()
extern "C"  void VideoTexture_Material_UpdatePlayFactor_m776127019 (VideoTexture_Material_t3546220134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoTexture_Material::UpdateAudio()
extern "C"  void VideoTexture_Material_UpdateAudio_m3567342256 (VideoTexture_Material_t3546220134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoTexture_Material::Sync()
extern "C"  void VideoTexture_Material_Sync_m2443672250 (VideoTexture_Material_t3546220134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoTexture_Material::Play()
extern "C"  void VideoTexture_Material_Play_m2345412883 (VideoTexture_Material_t3546220134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoTexture_Material::Stop()
extern "C"  void VideoTexture_Material_Stop_m2439096929 (VideoTexture_Material_t3546220134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoTexture_Material::TogglePlay()
extern "C"  void VideoTexture_Material_TogglePlay_m103195399 (VideoTexture_Material_t3546220134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoTexture_Material::ChangeDirection(UVT_PlayDirection)
extern "C"  void VideoTexture_Material_ChangeDirection_m1888273377 (VideoTexture_Material_t3546220134 * __this, int32_t ___newUVT_PlayDirection0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UVT_PlayState VideoTexture_Material::CurrentUVT_PlayState()
extern "C"  int32_t VideoTexture_Material_CurrentUVT_PlayState_m979294389 (VideoTexture_Material_t3546220134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UVT_PlayDirection VideoTexture_Material::CurrentUVT_PlayDirection()
extern "C"  int32_t VideoTexture_Material_CurrentUVT_PlayDirection_m2140069777 (VideoTexture_Material_t3546220134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VideoTexture_Material::CurrentPosition()
extern "C"  float VideoTexture_Material_CurrentPosition_m4218217073 (VideoTexture_Material_t3546220134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
