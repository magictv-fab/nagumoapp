﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_AppEvents3255826952.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_AppInvite3019343425.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_AppLinks3515718768.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_AppReques4212052403.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_DialogSha3869684773.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_GameGroups209060222.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_GraphRequ4038948153.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_GraphRequ3592004826.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_MainMenu2357105936.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_Pay3762569302.h"
#include "AssemblyU2DCSharp_Facebook_Unity_AccessToken3236047507.h"
#include "AssemblyU2DCSharp_Facebook_Unity_AppEventName2956053284.h"
#include "AssemblyU2DCSharp_Facebook_Unity_AppEventParameter3701649977.h"
#include "AssemblyU2DCSharp_Facebook_Unity_CallbackManager1300627398.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Canvas_CanvasFace3651918198.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Canvas_CanvasFace2832912295.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Canvas_CanvasFace3297174377.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Canvas_CanvasJSWr2718380960.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Canvas_JsBridge2292005674.h"
#include "AssemblyU2DCSharp_Facebook_Unity_ComponentFactory293624013.h"
#include "AssemblyU2DCSharp_Facebook_Unity_ComponentFactory_3988679170.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Constants3283635373.h"
#include "AssemblyU2DCSharp_Facebook_Unity_FB3725712860.h"
#include "AssemblyU2DCSharp_Facebook_Unity_FB_Canvas1344918474.h"
#include "AssemblyU2DCSharp_Facebook_Unity_FB_Mobile1643769620.h"
#include "AssemblyU2DCSharp_Facebook_Unity_FB_Android1625597501.h"
#include "AssemblyU2DCSharp_Facebook_Unity_FB_CompiledFacebo3689884636.h"
#include "AssemblyU2DCSharp_Facebook_Unity_FB_OnDLLLoaded970356664.h"
#include "AssemblyU2DCSharp_Facebook_Unity_FB_U3CInitU3Ec__A3153034682.h"
#include "AssemblyU2DCSharp_Facebook_Unity_FacebookBase850267831.h"
#include "AssemblyU2DCSharp_Facebook_Unity_FacebookGameObjec1988871287.h"
#include "AssemblyU2DCSharp_Facebook_Unity_FacebookSdkVersio3824863716.h"
#include "AssemblyU2DCSharp_Facebook_Unity_FacebookSettings3725355145.h"
#include "AssemblyU2DCSharp_Facebook_Unity_FacebookSettings_3482710948.h"
#include "AssemblyU2DCSharp_Facebook_Unity_FacebookUnityPlat1192368832.h"
#include "AssemblyU2DCSharp_Facebook_Unity_MethodArguments3236074899.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Mobile_Android_An2774136887.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Mobile_Android_An2212214888.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Mobile_Android_And148490282.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Mobile_Android_FB2461275164.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Mobile_Android_FB1185743683.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Mobile_Android_FB1906676742.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Mobile_IOS_IOSFac3828157341.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Mobile_IOS_IOSFac2882038999.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Mobile_IOS_IOSFac2627293877.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Mobile_IOS_IOSFac4031893710.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Mobile_IOS_IOSFace562470544.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Mobile_IOS_IOSWra4260454664.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Mobile_MobileFace2252988036.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Mobile_MobileFaceb194045109.h"
#include "AssemblyU2DCSharp_Facebook_Unity_ShareDialogMode56248424.h"
#include "AssemblyU2DCSharp_Facebook_Unity_OGActionType3473630824.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Editor_EditorFace1070344141.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Editor_EditorFace3558123006.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Editor_EditorFace2301290944.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Editor_EditorFace3317586847.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Editor_EditorFace3814375362.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Editor_Dialogs_Em1261978245.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Editor_Dialogs_Moc760013837.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Editor_Dialogs_Mo4051134152.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Editor_Dialogs_Mo3804468227.h"
#include "AssemblyU2DCSharp_Facebook_Unity_AccessTokenRefres2095557731.h"
#include "AssemblyU2DCSharp_Facebook_Unity_AppInviteResult2203762021.h"
#include "AssemblyU2DCSharp_Facebook_Unity_AppLinkResult2993735062.h"
#include "AssemblyU2DCSharp_Facebook_Unity_AppRequestResult3663988331.h"
#include "AssemblyU2DCSharp_Facebook_Unity_GraphResult1600985769.h"
#include "AssemblyU2DCSharp_Facebook_Unity_GroupCreateResult2234513590.h"
#include "AssemblyU2DCSharp_Facebook_Unity_GroupJoinResult3530549732.h"
#include "AssemblyU2DCSharp_Facebook_Unity_LoginResult1256196228.h"
#include "AssemblyU2DCSharp_Facebook_Unity_PayResult2846556739.h"
#include "AssemblyU2DCSharp_Facebook_Unity_ResultBase64408782.h"
#include "AssemblyU2DCSharp_Facebook_Unity_ShareResult5683322.h"
#include "AssemblyU2DCSharp_Facebook_Unity_AsyncRequestStrin2872237476.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3400 = { sizeof (AppEvents_t3255826952), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3401 = { sizeof (AppInvites_t3019343425), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3402 = { sizeof (AppLinks_t3515718768), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3403 = { sizeof (AppRequests_t4212052403), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3403[10] = 
{
	AppRequests_t4212052403::get_offset_of_requestMessage_18(),
	AppRequests_t4212052403::get_offset_of_requestTo_19(),
	AppRequests_t4212052403::get_offset_of_requestFilter_20(),
	AppRequests_t4212052403::get_offset_of_requestExcludes_21(),
	AppRequests_t4212052403::get_offset_of_requestMax_22(),
	AppRequests_t4212052403::get_offset_of_requestData_23(),
	AppRequests_t4212052403::get_offset_of_requestTitle_24(),
	AppRequests_t4212052403::get_offset_of_requestObjectID_25(),
	AppRequests_t4212052403::get_offset_of_selectedAction_26(),
	AppRequests_t4212052403::get_offset_of_actionTypeStrings_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3404 = { sizeof (DialogShare_t3869684773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3404[11] = 
{
	DialogShare_t3869684773::get_offset_of_shareLink_18(),
	DialogShare_t3869684773::get_offset_of_shareTitle_19(),
	DialogShare_t3869684773::get_offset_of_shareDescription_20(),
	DialogShare_t3869684773::get_offset_of_shareImage_21(),
	DialogShare_t3869684773::get_offset_of_feedTo_22(),
	DialogShare_t3869684773::get_offset_of_feedLink_23(),
	DialogShare_t3869684773::get_offset_of_feedTitle_24(),
	DialogShare_t3869684773::get_offset_of_feedCaption_25(),
	DialogShare_t3869684773::get_offset_of_feedDescription_26(),
	DialogShare_t3869684773::get_offset_of_feedImage_27(),
	DialogShare_t3869684773::get_offset_of_feedMediaSource_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3405 = { sizeof (GameGroups_t209060222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3405[4] = 
{
	GameGroups_t209060222::get_offset_of_gamerGroupName_18(),
	GameGroups_t209060222::get_offset_of_gamerGroupDesc_19(),
	GameGroups_t209060222::get_offset_of_gamerGroupPrivacy_20(),
	GameGroups_t209060222::get_offset_of_gamerGroupCurrentGroup_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3406 = { sizeof (GraphRequest_t4038948153), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3406[2] = 
{
	GraphRequest_t4038948153::get_offset_of_apiQuery_18(),
	GraphRequest_t4038948153::get_offset_of_profilePic_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3407 = { sizeof (U3CTakeScreenshotU3Ec__Iterator29_t3592004826), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3407[8] = 
{
	U3CTakeScreenshotU3Ec__Iterator29_t3592004826::get_offset_of_U3CwidthU3E__0_0(),
	U3CTakeScreenshotU3Ec__Iterator29_t3592004826::get_offset_of_U3CheightU3E__1_1(),
	U3CTakeScreenshotU3Ec__Iterator29_t3592004826::get_offset_of_U3CtexU3E__2_2(),
	U3CTakeScreenshotU3Ec__Iterator29_t3592004826::get_offset_of_U3CscreenshotU3E__3_3(),
	U3CTakeScreenshotU3Ec__Iterator29_t3592004826::get_offset_of_U3CwwwFormU3E__4_4(),
	U3CTakeScreenshotU3Ec__Iterator29_t3592004826::get_offset_of_U24PC_5(),
	U3CTakeScreenshotU3Ec__Iterator29_t3592004826::get_offset_of_U24current_6(),
	U3CTakeScreenshotU3Ec__Iterator29_t3592004826::get_offset_of_U3CU3Ef__this_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3408 = { sizeof (MainMenu_t2357105936), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3409 = { sizeof (Pay_t3762569302), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3409[1] = 
{
	Pay_t3762569302::get_offset_of_payProduct_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3410 = { sizeof (AccessToken_t3236047507), -1, sizeof(AccessToken_t3236047507_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3410[6] = 
{
	AccessToken_t3236047507_StaticFields::get_offset_of_U3CCurrentAccessTokenU3Ek__BackingField_0(),
	AccessToken_t3236047507::get_offset_of_U3CTokenStringU3Ek__BackingField_1(),
	AccessToken_t3236047507::get_offset_of_U3CExpirationTimeU3Ek__BackingField_2(),
	AccessToken_t3236047507::get_offset_of_U3CPermissionsU3Ek__BackingField_3(),
	AccessToken_t3236047507::get_offset_of_U3CUserIdU3Ek__BackingField_4(),
	AccessToken_t3236047507::get_offset_of_U3CLastRefreshU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3411 = { sizeof (AppEventName_t2956053284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3411[14] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3412 = { sizeof (AppEventParameterName_t3701649977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3412[11] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3413 = { sizeof (CallbackManager_t1300627398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3413[2] = 
{
	CallbackManager_t1300627398::get_offset_of_facebookDelegates_0(),
	CallbackManager_t1300627398::get_offset_of_nextAsyncId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3414 = { sizeof (CanvasFacebook_t3651918198), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3414[13] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	CanvasFacebook_t3651918198::get_offset_of_appId_13(),
	CanvasFacebook_t3651918198::get_offset_of_appLinkUrl_14(),
	CanvasFacebook_t3651918198::get_offset_of_canvasJSWrapper_15(),
	CanvasFacebook_t3651918198::get_offset_of_U3CLimitEventUsageU3Ek__BackingField_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3415 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3415[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3416 = { sizeof (CanvasFacebookGameObject_t2832912295), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3417 = { sizeof (CanvasFacebookLoader_t3297174377), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3418 = { sizeof (CanvasJSWrapper_t2718380960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3418[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3419 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3420 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3421 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3422 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3423 = { sizeof (JsBridge_t2292005674), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3423[1] = 
{
	JsBridge_t2292005674::get_offset_of_facebook_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3424 = { sizeof (ComponentFactory_t293624013), -1, sizeof(ComponentFactory_t293624013_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3424[2] = 
{
	0,
	ComponentFactory_t293624013_StaticFields::get_offset_of_facebookGameObject_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3425 = { sizeof (IfNotExist_t3988679170)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3425[3] = 
{
	IfNotExist_t3988679170::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3426 = { sizeof (Constants_t3283635373), -1, sizeof(Constants_t3283635373_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3426[20] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	Constants_t3283635373_StaticFields::get_offset_of_currentPlatform_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3427 = { sizeof (FB_t3725712860), -1, sizeof(FB_t3725712860_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3427[7] = 
{
	0,
	FB_t3725712860_StaticFields::get_offset_of_facebook_3(),
	FB_t3725712860_StaticFields::get_offset_of_isInitCalled_4(),
	FB_t3725712860_StaticFields::get_offset_of_facebookDomain_5(),
	FB_t3725712860_StaticFields::get_offset_of_graphApiVersion_6(),
	FB_t3725712860_StaticFields::get_offset_of_U3CAppIdU3Ek__BackingField_7(),
	FB_t3725712860_StaticFields::get_offset_of_U3COnDLLLoadedDelegateU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3428 = { sizeof (Canvas_t1344918474), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3429 = { sizeof (Mobile_t1643769620), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3430 = { sizeof (Android_t1625597501), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3431 = { sizeof (CompiledFacebookLoader_t3689884636), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3432 = { sizeof (OnDLLLoaded_t970356664), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3433 = { sizeof (U3CInitU3Ec__AnonStorey90_t3153034682), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3433[10] = 
{
	U3CInitU3Ec__AnonStorey90_t3153034682::get_offset_of_onHideUnity_0(),
	U3CInitU3Ec__AnonStorey90_t3153034682::get_offset_of_onInitComplete_1(),
	U3CInitU3Ec__AnonStorey90_t3153034682::get_offset_of_appId_2(),
	U3CInitU3Ec__AnonStorey90_t3153034682::get_offset_of_cookie_3(),
	U3CInitU3Ec__AnonStorey90_t3153034682::get_offset_of_logging_4(),
	U3CInitU3Ec__AnonStorey90_t3153034682::get_offset_of_status_5(),
	U3CInitU3Ec__AnonStorey90_t3153034682::get_offset_of_xfbml_6(),
	U3CInitU3Ec__AnonStorey90_t3153034682::get_offset_of_authResponse_7(),
	U3CInitU3Ec__AnonStorey90_t3153034682::get_offset_of_frictionlessRequests_8(),
	U3CInitU3Ec__AnonStorey90_t3153034682::get_offset_of_jsSDKLocale_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3434 = { sizeof (FacebookBase_t850267831), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3434[4] = 
{
	FacebookBase_t850267831::get_offset_of_onInitCompleteDelegate_0(),
	FacebookBase_t850267831::get_offset_of_onHideUnityDelegate_1(),
	FacebookBase_t850267831::get_offset_of_U3CInitializedU3Ek__BackingField_2(),
	FacebookBase_t850267831::get_offset_of_U3CCallbackManagerU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3435 = { sizeof (FacebookGameObject_t1988871287), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3435[1] = 
{
	FacebookGameObject_t1988871287::get_offset_of_U3CFacebookU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3436 = { sizeof (FacebookSdkVersion_t3824863716), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3437 = { sizeof (FacebookSettings_t3725355145), -1, sizeof(FacebookSettings_t3725355145_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3437[14] = 
{
	0,
	0,
	0,
	FacebookSettings_t3725355145_StaticFields::get_offset_of_instance_5(),
	FacebookSettings_t3725355145::get_offset_of_selectedAppIndex_6(),
	FacebookSettings_t3725355145::get_offset_of_appIds_7(),
	FacebookSettings_t3725355145::get_offset_of_appLabels_8(),
	FacebookSettings_t3725355145::get_offset_of_cookie_9(),
	FacebookSettings_t3725355145::get_offset_of_logging_10(),
	FacebookSettings_t3725355145::get_offset_of_status_11(),
	FacebookSettings_t3725355145::get_offset_of_xfbml_12(),
	FacebookSettings_t3725355145::get_offset_of_frictionlessRequests_13(),
	FacebookSettings_t3725355145::get_offset_of_iosURLSuffix_14(),
	FacebookSettings_t3725355145::get_offset_of_appLinkSchemes_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3438 = { sizeof (UrlSchemes_t3482710948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3438[1] = 
{
	UrlSchemes_t3482710948::get_offset_of_list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3439 = { sizeof (FacebookUnityPlatform_t1192368832)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3439[6] = 
{
	FacebookUnityPlatform_t1192368832::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3440 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3441 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3442 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3443 = { sizeof (MethodArguments_t3236074899), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3443[1] = 
{
	MethodArguments_t3236074899::get_offset_of_arguments_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3444 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3444[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3445 = { sizeof (AndroidFacebook_t2774136887), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3445[4] = 
{
	0,
	AndroidFacebook_t2774136887::get_offset_of_limitEventUsage_7(),
	AndroidFacebook_t2774136887::get_offset_of_facebookJava_8(),
	AndroidFacebook_t2774136887::get_offset_of_U3CKeyHashU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3446 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3446[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3447 = { sizeof (AndroidFacebookGameObject_t2212214888), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3448 = { sizeof (AndroidFacebookLoader_t148490282), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3449 = { sizeof (FBJavaClass_t2461275164), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3449[2] = 
{
	0,
	FBJavaClass_t2461275164::get_offset_of_facebookJavaClass_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3450 = { sizeof (AndroidJNIHelper_t1185743683), -1, sizeof(AndroidJNIHelper_t1185743683_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3450[1] = 
{
	AndroidJNIHelper_t1185743683_StaticFields::get_offset_of_U3CDebugU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3451 = { sizeof (AndroidJavaClass_t1906676742), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3452 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3453 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3454 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3455 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3456 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3457 = { sizeof (IOSFacebook_t3828157341), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3457[3] = 
{
	0,
	IOSFacebook_t3828157341::get_offset_of_limitEventUsage_7(),
	IOSFacebook_t3828157341::get_offset_of_iosWrapper_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3458 = { sizeof (FBInsightsFlushBehavior_t2882038999)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3458[3] = 
{
	FBInsightsFlushBehavior_t2882038999::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3459 = { sizeof (NativeDict_t2627293877), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3459[3] = 
{
	NativeDict_t2627293877::get_offset_of_U3CNumEntriesU3Ek__BackingField_0(),
	NativeDict_t2627293877::get_offset_of_U3CKeysU3Ek__BackingField_1(),
	NativeDict_t2627293877::get_offset_of_U3CValuesU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3460 = { sizeof (IOSFacebookGameObject_t4031893710), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3461 = { sizeof (IOSFacebookLoader_t562470544), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3462 = { sizeof (IOSWrapper_t4260454664), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3463 = { sizeof (MobileFacebook_t2252988036), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3463[2] = 
{
	0,
	MobileFacebook_t2252988036::get_offset_of_shareDialogMode_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3464 = { sizeof (MobileFacebookGameObject_t194045109), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3465 = { sizeof (ShareDialogMode_t56248424)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3465[5] = 
{
	ShareDialogMode_t56248424::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3466 = { sizeof (OGActionType_t3473630824)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3466[4] = 
{
	OGActionType_t3473630824::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3467 = { sizeof (EditorFacebook_t1070344141), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3467[4] = 
{
	0,
	0,
	EditorFacebook_t1070344141::get_offset_of_U3CLimitEventUsageU3Ek__BackingField_6(),
	EditorFacebook_t1070344141::get_offset_of_U3CShareDialogModeU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3468 = { sizeof (EditorFacebookGameObject_t3558123006), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3469 = { sizeof (EditorFacebookLoader_t2301290944), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3470 = { sizeof (EditorFacebookMockDialog_t3317586847), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3470[4] = 
{
	EditorFacebookMockDialog_t3317586847::get_offset_of_modalRect_2(),
	EditorFacebookMockDialog_t3317586847::get_offset_of_modalStyle_3(),
	EditorFacebookMockDialog_t3317586847::get_offset_of_U3CCallbackU3Ek__BackingField_4(),
	EditorFacebookMockDialog_t3317586847::get_offset_of_U3CCallbackIDU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3471 = { sizeof (OnComplete_t3814375362), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3472 = { sizeof (EmptyMockDialog_t1261978245), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3472[1] = 
{
	EmptyMockDialog_t1261978245::get_offset_of_U3CEmptyDialogTitleU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3473 = { sizeof (MockLoginDialog_t760013837), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3473[1] = 
{
	MockLoginDialog_t760013837::get_offset_of_accessToken_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3474 = { sizeof (U3CSendSuccessResultU3Ec__AnonStorey91_t4051134152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3474[2] = 
{
	U3CSendSuccessResultU3Ec__AnonStorey91_t4051134152::get_offset_of_facebookID_0(),
	U3CSendSuccessResultU3Ec__AnonStorey91_t4051134152::get_offset_of_U3CU3Ef__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3475 = { sizeof (MockShareDialog_t3804468227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3475[1] = 
{
	MockShareDialog_t3804468227::get_offset_of_U3CSubTitleU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3476 = { sizeof (AccessTokenRefreshResult_t2095557731), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3476[1] = 
{
	AccessTokenRefreshResult_t2095557731::get_offset_of_U3CAccessTokenU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3477 = { sizeof (AppInviteResult_t2203762021), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3478 = { sizeof (AppLinkResult_t2993735062), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3478[4] = 
{
	AppLinkResult_t2993735062::get_offset_of_U3CUrlU3Ek__BackingField_5(),
	AppLinkResult_t2993735062::get_offset_of_U3CTargetUrlU3Ek__BackingField_6(),
	AppLinkResult_t2993735062::get_offset_of_U3CRefU3Ek__BackingField_7(),
	AppLinkResult_t2993735062::get_offset_of_U3CExtrasU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3479 = { sizeof (AppRequestResult_t3663988331), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3479[4] = 
{
	0,
	0,
	AppRequestResult_t3663988331::get_offset_of_U3CRequestIDU3Ek__BackingField_7(),
	AppRequestResult_t3663988331::get_offset_of_U3CToU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3480 = { sizeof (GraphResult_t1600985769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3480[2] = 
{
	GraphResult_t1600985769::get_offset_of_U3CResultListU3Ek__BackingField_5(),
	GraphResult_t1600985769::get_offset_of_U3CTextureU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3481 = { sizeof (GroupCreateResult_t2234513590), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3481[2] = 
{
	0,
	GroupCreateResult_t2234513590::get_offset_of_U3CGroupIdU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3482 = { sizeof (GroupJoinResult_t3530549732), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3483 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3484 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3485 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3486 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3487 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3488 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3489 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3490 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3491 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3492 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3493 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3494 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3495 = { sizeof (LoginResult_t1256196228), -1, sizeof(LoginResult_t1256196228_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3495[6] = 
{
	0,
	LoginResult_t1256196228_StaticFields::get_offset_of_UserIdKey_6(),
	LoginResult_t1256196228_StaticFields::get_offset_of_ExpirationTimestampKey_7(),
	LoginResult_t1256196228_StaticFields::get_offset_of_PermissionsKey_8(),
	LoginResult_t1256196228_StaticFields::get_offset_of_AccessTokenKey_9(),
	LoginResult_t1256196228::get_offset_of_U3CAccessTokenU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3496 = { sizeof (PayResult_t2846556739), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3497 = { sizeof (ResultBase_t64408782), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3497[5] = 
{
	ResultBase_t64408782::get_offset_of_U3CErrorU3Ek__BackingField_0(),
	ResultBase_t64408782::get_offset_of_U3CResultDictionaryU3Ek__BackingField_1(),
	ResultBase_t64408782::get_offset_of_U3CRawResultU3Ek__BackingField_2(),
	ResultBase_t64408782::get_offset_of_U3CCancelledU3Ek__BackingField_3(),
	ResultBase_t64408782::get_offset_of_U3CCallbackIdU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3498 = { sizeof (ShareResult_t5683322), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3498[1] = 
{
	ShareResult_t5683322::get_offset_of_U3CPostIdU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3499 = { sizeof (AsyncRequestString_t2872237476), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3499[5] = 
{
	AsyncRequestString_t2872237476::get_offset_of_url_2(),
	AsyncRequestString_t2872237476::get_offset_of_method_3(),
	AsyncRequestString_t2872237476::get_offset_of_formData_4(),
	AsyncRequestString_t2872237476::get_offset_of_query_5(),
	AsyncRequestString_t2872237476::get_offset_of_callback_6(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
