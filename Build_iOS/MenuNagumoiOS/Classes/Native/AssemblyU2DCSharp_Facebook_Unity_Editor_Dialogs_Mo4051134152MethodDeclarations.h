﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.Editor.Dialogs.MockLoginDialog/<SendSuccessResult>c__AnonStorey91
struct U3CSendSuccessResultU3Ec__AnonStorey91_t4051134152;
// Facebook.Unity.IGraphResult
struct IGraphResult_t873401058;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.Editor.Dialogs.MockLoginDialog/<SendSuccessResult>c__AnonStorey91::.ctor()
extern "C"  void U3CSendSuccessResultU3Ec__AnonStorey91__ctor_m4013170899 (U3CSendSuccessResultU3Ec__AnonStorey91_t4051134152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.Dialogs.MockLoginDialog/<SendSuccessResult>c__AnonStorey91::<>m__11(Facebook.Unity.IGraphResult)
extern "C"  void U3CSendSuccessResultU3Ec__AnonStorey91_U3CU3Em__11_m2656534227 (U3CSendSuccessResultU3Ec__AnonStorey91_t4051134152 * __this, Il2CppObject * ___permResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
