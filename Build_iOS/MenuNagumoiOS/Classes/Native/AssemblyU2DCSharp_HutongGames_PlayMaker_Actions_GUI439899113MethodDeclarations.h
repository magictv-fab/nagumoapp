﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject
struct GUILayoutBeginAreaFollowObject_t439899113;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject::.ctor()
extern "C"  void GUILayoutBeginAreaFollowObject__ctor_m2004408989 (GUILayoutBeginAreaFollowObject_t439899113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject::Reset()
extern "C"  void GUILayoutBeginAreaFollowObject_Reset_m3945809226 (GUILayoutBeginAreaFollowObject_t439899113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject::OnGUI()
extern "C"  void GUILayoutBeginAreaFollowObject_OnGUI_m1499807639 (GUILayoutBeginAreaFollowObject_t439899113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject::DummyBeginArea()
extern "C"  void GUILayoutBeginAreaFollowObject_DummyBeginArea_m2118469909 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
