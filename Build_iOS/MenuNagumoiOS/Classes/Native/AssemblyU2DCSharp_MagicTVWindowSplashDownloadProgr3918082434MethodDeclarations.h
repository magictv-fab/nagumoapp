﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTVWindowSplashDownloadProgressControllerScript
struct MagicTVWindowSplashDownloadProgressControllerScript_t3918082434;
// UnityEngine.UI.Slider
struct Slider_t79469677;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider79469677.h"

// System.Void MagicTVWindowSplashDownloadProgressControllerScript::.ctor()
extern "C"  void MagicTVWindowSplashDownloadProgressControllerScript__ctor_m1233245225 (MagicTVWindowSplashDownloadProgressControllerScript_t3918082434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowSplashDownloadProgressControllerScript::UpdateText(System.Single)
extern "C"  void MagicTVWindowSplashDownloadProgressControllerScript_UpdateText_m2841432250 (MagicTVWindowSplashDownloadProgressControllerScript_t3918082434 * __this, float ___progress0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowSplashDownloadProgressControllerScript::CancelDownLoadClick()
extern "C"  void MagicTVWindowSplashDownloadProgressControllerScript_CancelDownLoadClick_m1025810093 (MagicTVWindowSplashDownloadProgressControllerScript_t3918082434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MagicTVWindowSplashDownloadProgressControllerScript::get_Progress()
extern "C"  float MagicTVWindowSplashDownloadProgressControllerScript_get_Progress_m3494249765 (MagicTVWindowSplashDownloadProgressControllerScript_t3918082434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowSplashDownloadProgressControllerScript::set_Progress(System.Single)
extern "C"  void MagicTVWindowSplashDownloadProgressControllerScript_set_Progress_m1069049062 (MagicTVWindowSplashDownloadProgressControllerScript_t3918082434 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowSplashDownloadProgressControllerScript::CanCancelDownload()
extern "C"  void MagicTVWindowSplashDownloadProgressControllerScript_CanCancelDownload_m265921753 (MagicTVWindowSplashDownloadProgressControllerScript_t3918082434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowSplashDownloadProgressControllerScript::CanNotCancelDownload()
extern "C"  void MagicTVWindowSplashDownloadProgressControllerScript_CanNotCancelDownload_m4245650336 (MagicTVWindowSplashDownloadProgressControllerScript_t3918082434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowSplashDownloadProgressControllerScript::<set_Progress>m__14(UnityEngine.UI.Slider)
extern "C"  void MagicTVWindowSplashDownloadProgressControllerScript_U3Cset_ProgressU3Em__14_m2501811599 (MagicTVWindowSplashDownloadProgressControllerScript_t3918082434 * __this, Slider_t79469677 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
