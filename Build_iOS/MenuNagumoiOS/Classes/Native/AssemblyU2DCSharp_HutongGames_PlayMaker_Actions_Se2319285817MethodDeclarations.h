﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetPosition
struct SetPosition_t2319285817;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetPosition::.ctor()
extern "C"  void SetPosition__ctor_m4220607773 (SetPosition_t2319285817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetPosition::Reset()
extern "C"  void SetPosition_Reset_m1867040714 (SetPosition_t2319285817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetPosition::OnEnter()
extern "C"  void SetPosition_OnEnter_m946671028 (SetPosition_t2319285817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetPosition::OnUpdate()
extern "C"  void SetPosition_OnUpdate_m2710557455 (SetPosition_t2319285817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetPosition::OnLateUpdate()
extern "C"  void SetPosition_OnLateUpdate_m2459138389 (SetPosition_t2319285817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetPosition::DoSetPosition()
extern "C"  void SetPosition_DoSetPosition_m3465412187 (SetPosition_t2319285817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
