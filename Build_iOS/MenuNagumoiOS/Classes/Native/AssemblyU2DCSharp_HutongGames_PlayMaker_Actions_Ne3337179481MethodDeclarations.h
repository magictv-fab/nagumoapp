﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkSetMinimumAllocatableViewIDs
struct NetworkSetMinimumAllocatableViewIDs_t3337179481;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkSetMinimumAllocatableViewIDs::.ctor()
extern "C"  void NetworkSetMinimumAllocatableViewIDs__ctor_m3446855165 (NetworkSetMinimumAllocatableViewIDs_t3337179481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkSetMinimumAllocatableViewIDs::Reset()
extern "C"  void NetworkSetMinimumAllocatableViewIDs_Reset_m1093288106 (NetworkSetMinimumAllocatableViewIDs_t3337179481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkSetMinimumAllocatableViewIDs::OnEnter()
extern "C"  void NetworkSetMinimumAllocatableViewIDs_OnEnter_m399756948 (NetworkSetMinimumAllocatableViewIDs_t3337179481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
