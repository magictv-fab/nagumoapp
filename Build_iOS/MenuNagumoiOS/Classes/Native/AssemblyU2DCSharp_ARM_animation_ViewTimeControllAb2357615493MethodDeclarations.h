﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.animation.ViewTimeControllAbstract
struct ViewTimeControllAbstract_t2357615493;
// ARM.animation.ViewTimeControllAbstract/OnReadyEventHandlerandler
struct OnReadyEventHandlerandler_t2819726214;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ARM_animation_ViewTimeControllAb2819726214.h"

// System.Void ARM.animation.ViewTimeControllAbstract::.ctor()
extern "C"  void ViewTimeControllAbstract__ctor_m347984924 (ViewTimeControllAbstract_t2357615493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.animation.ViewTimeControllAbstract::AddOnReady(ARM.animation.ViewTimeControllAbstract/OnReadyEventHandlerandler)
extern "C"  void ViewTimeControllAbstract_AddOnReady_m1212357637 (ViewTimeControllAbstract_t2357615493 * __this, OnReadyEventHandlerandler_t2819726214 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.animation.ViewTimeControllAbstract::RaiseOnReady()
extern "C"  void ViewTimeControllAbstract_RaiseOnReady_m428437024 (ViewTimeControllAbstract_t2357615493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARM.animation.ViewTimeControllAbstract::get_isReady()
extern "C"  bool ViewTimeControllAbstract_get_isReady_m1518415638 (ViewTimeControllAbstract_t2357615493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARM.animation.ViewTimeControllAbstract::isPlaying()
extern "C"  bool ViewTimeControllAbstract_isPlaying_m2523883594 (ViewTimeControllAbstract_t2357615493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.animation.ViewTimeControllAbstract::Play()
extern "C"  void ViewTimeControllAbstract_Play_m3347674076 (ViewTimeControllAbstract_t2357615493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.animation.ViewTimeControllAbstract::Stop()
extern "C"  void ViewTimeControllAbstract_Stop_m3441358122 (ViewTimeControllAbstract_t2357615493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.animation.ViewTimeControllAbstract::Pause()
extern "C"  void ViewTimeControllAbstract_Pause_m402110896 (ViewTimeControllAbstract_t2357615493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.animation.ViewTimeControllAbstract::_onFinished()
extern "C"  void ViewTimeControllAbstract__onFinished_m3764744106 (ViewTimeControllAbstract_t2357615493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
