﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutBeginCentered
struct GUILayoutBeginCentered_t363266336;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginCentered::.ctor()
extern "C"  void GUILayoutBeginCentered__ctor_m3559633734 (GUILayoutBeginCentered_t363266336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginCentered::Reset()
extern "C"  void GUILayoutBeginCentered_Reset_m1206066675 (GUILayoutBeginCentered_t363266336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginCentered::OnGUI()
extern "C"  void GUILayoutBeginCentered_OnGUI_m3055032384 (GUILayoutBeginCentered_t363266336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
