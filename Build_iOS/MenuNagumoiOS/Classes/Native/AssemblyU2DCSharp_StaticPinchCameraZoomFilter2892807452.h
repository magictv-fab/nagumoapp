﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// StaticPinchCameraZoomFilter
struct StaticPinchCameraZoomFilter_t2892807452;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StaticPinchCameraZoomFilter
struct  StaticPinchCameraZoomFilter_t2892807452  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject StaticPinchCameraZoomFilter::CameraToPinchControl
	GameObject_t3674682005 * ___CameraToPinchControl_3;
	// System.Single StaticPinchCameraZoomFilter::ZoomTotal
	float ___ZoomTotal_4;
	// System.Single StaticPinchCameraZoomFilter::ZoomPinch
	float ___ZoomPinch_5;
	// System.Single StaticPinchCameraZoomFilter::ZoomBonus
	float ___ZoomBonus_6;
	// System.Single StaticPinchCameraZoomFilter::InitalCamerasDistance
	float ___InitalCamerasDistance_7;

public:
	inline static int32_t get_offset_of_CameraToPinchControl_3() { return static_cast<int32_t>(offsetof(StaticPinchCameraZoomFilter_t2892807452, ___CameraToPinchControl_3)); }
	inline GameObject_t3674682005 * get_CameraToPinchControl_3() const { return ___CameraToPinchControl_3; }
	inline GameObject_t3674682005 ** get_address_of_CameraToPinchControl_3() { return &___CameraToPinchControl_3; }
	inline void set_CameraToPinchControl_3(GameObject_t3674682005 * value)
	{
		___CameraToPinchControl_3 = value;
		Il2CppCodeGenWriteBarrier(&___CameraToPinchControl_3, value);
	}

	inline static int32_t get_offset_of_ZoomTotal_4() { return static_cast<int32_t>(offsetof(StaticPinchCameraZoomFilter_t2892807452, ___ZoomTotal_4)); }
	inline float get_ZoomTotal_4() const { return ___ZoomTotal_4; }
	inline float* get_address_of_ZoomTotal_4() { return &___ZoomTotal_4; }
	inline void set_ZoomTotal_4(float value)
	{
		___ZoomTotal_4 = value;
	}

	inline static int32_t get_offset_of_ZoomPinch_5() { return static_cast<int32_t>(offsetof(StaticPinchCameraZoomFilter_t2892807452, ___ZoomPinch_5)); }
	inline float get_ZoomPinch_5() const { return ___ZoomPinch_5; }
	inline float* get_address_of_ZoomPinch_5() { return &___ZoomPinch_5; }
	inline void set_ZoomPinch_5(float value)
	{
		___ZoomPinch_5 = value;
	}

	inline static int32_t get_offset_of_ZoomBonus_6() { return static_cast<int32_t>(offsetof(StaticPinchCameraZoomFilter_t2892807452, ___ZoomBonus_6)); }
	inline float get_ZoomBonus_6() const { return ___ZoomBonus_6; }
	inline float* get_address_of_ZoomBonus_6() { return &___ZoomBonus_6; }
	inline void set_ZoomBonus_6(float value)
	{
		___ZoomBonus_6 = value;
	}

	inline static int32_t get_offset_of_InitalCamerasDistance_7() { return static_cast<int32_t>(offsetof(StaticPinchCameraZoomFilter_t2892807452, ___InitalCamerasDistance_7)); }
	inline float get_InitalCamerasDistance_7() const { return ___InitalCamerasDistance_7; }
	inline float* get_address_of_InitalCamerasDistance_7() { return &___InitalCamerasDistance_7; }
	inline void set_InitalCamerasDistance_7(float value)
	{
		___InitalCamerasDistance_7 = value;
	}
};

struct StaticPinchCameraZoomFilter_t2892807452_StaticFields
{
public:
	// StaticPinchCameraZoomFilter StaticPinchCameraZoomFilter::Instance
	StaticPinchCameraZoomFilter_t2892807452 * ___Instance_2;

public:
	inline static int32_t get_offset_of_Instance_2() { return static_cast<int32_t>(offsetof(StaticPinchCameraZoomFilter_t2892807452_StaticFields, ___Instance_2)); }
	inline StaticPinchCameraZoomFilter_t2892807452 * get_Instance_2() const { return ___Instance_2; }
	inline StaticPinchCameraZoomFilter_t2892807452 ** get_address_of_Instance_2() { return &___Instance_2; }
	inline void set_Instance_2(StaticPinchCameraZoomFilter_t2892807452 * value)
	{
		___Instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
