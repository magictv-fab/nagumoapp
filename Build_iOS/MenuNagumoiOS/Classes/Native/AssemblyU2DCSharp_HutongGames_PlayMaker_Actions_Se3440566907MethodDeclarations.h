﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetBackgroundColor
struct SetBackgroundColor_t3440566907;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetBackgroundColor::.ctor()
extern "C"  void SetBackgroundColor__ctor_m2997154891 (SetBackgroundColor_t3440566907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetBackgroundColor::Reset()
extern "C"  void SetBackgroundColor_Reset_m643587832 (SetBackgroundColor_t3440566907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetBackgroundColor::OnEnter()
extern "C"  void SetBackgroundColor_OnEnter_m2029490530 (SetBackgroundColor_t3440566907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetBackgroundColor::OnUpdate()
extern "C"  void SetBackgroundColor_OnUpdate_m1918223649 (SetBackgroundColor_t3440566907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetBackgroundColor::DoSetBackgroundColor()
extern "C"  void SetBackgroundColor_DoSetBackgroundColor_m1204554263 (SetBackgroundColor_t3440566907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
