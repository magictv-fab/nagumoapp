﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.ResultPoint
struct ResultPoint_t1538592853;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions
struct  ResultPointsAndTransitions_t1206419768  : public Il2CppObject
{
public:
	// ZXing.ResultPoint ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::<From>k__BackingField
	ResultPoint_t1538592853 * ___U3CFromU3Ek__BackingField_0;
	// ZXing.ResultPoint ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::<To>k__BackingField
	ResultPoint_t1538592853 * ___U3CToU3Ek__BackingField_1;
	// System.Int32 ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions::<Transitions>k__BackingField
	int32_t ___U3CTransitionsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CFromU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ResultPointsAndTransitions_t1206419768, ___U3CFromU3Ek__BackingField_0)); }
	inline ResultPoint_t1538592853 * get_U3CFromU3Ek__BackingField_0() const { return ___U3CFromU3Ek__BackingField_0; }
	inline ResultPoint_t1538592853 ** get_address_of_U3CFromU3Ek__BackingField_0() { return &___U3CFromU3Ek__BackingField_0; }
	inline void set_U3CFromU3Ek__BackingField_0(ResultPoint_t1538592853 * value)
	{
		___U3CFromU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CFromU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CToU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ResultPointsAndTransitions_t1206419768, ___U3CToU3Ek__BackingField_1)); }
	inline ResultPoint_t1538592853 * get_U3CToU3Ek__BackingField_1() const { return ___U3CToU3Ek__BackingField_1; }
	inline ResultPoint_t1538592853 ** get_address_of_U3CToU3Ek__BackingField_1() { return &___U3CToU3Ek__BackingField_1; }
	inline void set_U3CToU3Ek__BackingField_1(ResultPoint_t1538592853 * value)
	{
		___U3CToU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CToU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CTransitionsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ResultPointsAndTransitions_t1206419768, ___U3CTransitionsU3Ek__BackingField_2)); }
	inline int32_t get_U3CTransitionsU3Ek__BackingField_2() const { return ___U3CTransitionsU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CTransitionsU3Ek__BackingField_2() { return &___U3CTransitionsU3Ek__BackingField_2; }
	inline void set_U3CTransitionsU3Ek__BackingField_2(int32_t value)
	{
		___U3CTransitionsU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
