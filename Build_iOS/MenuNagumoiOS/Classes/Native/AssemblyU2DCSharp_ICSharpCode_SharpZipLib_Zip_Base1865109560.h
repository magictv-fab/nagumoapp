﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_File4232823638.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.BaseArchiveStorage
struct  BaseArchiveStorage_t1865109560  : public Il2CppObject
{
public:
	// ICSharpCode.SharpZipLib.Zip.FileUpdateMode ICSharpCode.SharpZipLib.Zip.BaseArchiveStorage::updateMode_
	int32_t ___updateMode__0;

public:
	inline static int32_t get_offset_of_updateMode__0() { return static_cast<int32_t>(offsetof(BaseArchiveStorage_t1865109560, ___updateMode__0)); }
	inline int32_t get_updateMode__0() const { return ___updateMode__0; }
	inline int32_t* get_address_of_updateMode__0() { return &___updateMode__0; }
	inline void set_updateMode__0(int32_t value)
	{
		___updateMode__0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
