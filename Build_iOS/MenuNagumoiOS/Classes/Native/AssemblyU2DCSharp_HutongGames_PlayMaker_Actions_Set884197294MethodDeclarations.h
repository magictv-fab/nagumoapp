﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetGUIText
struct SetGUIText_t884197294;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetGUIText::.ctor()
extern "C"  void SetGUIText__ctor_m128110712 (SetGUIText_t884197294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUIText::Reset()
extern "C"  void SetGUIText_Reset_m2069510949 (SetGUIText_t884197294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUIText::OnEnter()
extern "C"  void SetGUIText_OnEnter_m2247038543 (SetGUIText_t884197294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUIText::OnUpdate()
extern "C"  void SetGUIText_OnUpdate_m72277460 (SetGUIText_t884197294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUIText::DoSetGUIText()
extern "C"  void SetGUIText_DoSetGUIText_m1322523581 (SetGUIText_t884197294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
