﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkPeerTypeSwitch
struct NetworkPeerTypeSwitch_t2696412844;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkPeerTypeSwitch::.ctor()
extern "C"  void NetworkPeerTypeSwitch__ctor_m2909776906 (NetworkPeerTypeSwitch_t2696412844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkPeerTypeSwitch::Reset()
extern "C"  void NetworkPeerTypeSwitch_Reset_m556209847 (NetworkPeerTypeSwitch_t2696412844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkPeerTypeSwitch::OnEnter()
extern "C"  void NetworkPeerTypeSwitch_OnEnter_m3958592865 (NetworkPeerTypeSwitch_t2696412844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkPeerTypeSwitch::OnUpdate()
extern "C"  void NetworkPeerTypeSwitch_OnUpdate_m1590853890 (NetworkPeerTypeSwitch_t2696412844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkPeerTypeSwitch::DoNetworkPeerTypeSwitch()
extern "C"  void NetworkPeerTypeSwitch_DoNetworkPeerTypeSwitch_m3533761723 (NetworkPeerTypeSwitch_t2696412844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
