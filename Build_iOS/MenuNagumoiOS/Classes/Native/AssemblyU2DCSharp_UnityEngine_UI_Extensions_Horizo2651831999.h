﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_UnityEngine_UI_Extensions_Scroll1686927404.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.HorizontalScrollSnap
struct  HorizontalScrollSnap_t2651831999  : public ScrollSnapBase_t1686927404
{
public:
	// System.Single UnityEngine.UI.Extensions.HorizontalScrollSnap::sizeContentPlus
	float ___sizeContentPlus_38;
	// System.Single UnityEngine.UI.Extensions.HorizontalScrollSnap::sizeCellX
	float ___sizeCellX_39;
	// System.Boolean UnityEngine.UI.Extensions.HorizontalScrollSnap::updateCounter
	bool ___updateCounter_40;

public:
	inline static int32_t get_offset_of_sizeContentPlus_38() { return static_cast<int32_t>(offsetof(HorizontalScrollSnap_t2651831999, ___sizeContentPlus_38)); }
	inline float get_sizeContentPlus_38() const { return ___sizeContentPlus_38; }
	inline float* get_address_of_sizeContentPlus_38() { return &___sizeContentPlus_38; }
	inline void set_sizeContentPlus_38(float value)
	{
		___sizeContentPlus_38 = value;
	}

	inline static int32_t get_offset_of_sizeCellX_39() { return static_cast<int32_t>(offsetof(HorizontalScrollSnap_t2651831999, ___sizeCellX_39)); }
	inline float get_sizeCellX_39() const { return ___sizeCellX_39; }
	inline float* get_address_of_sizeCellX_39() { return &___sizeCellX_39; }
	inline void set_sizeCellX_39(float value)
	{
		___sizeCellX_39 = value;
	}

	inline static int32_t get_offset_of_updateCounter_40() { return static_cast<int32_t>(offsetof(HorizontalScrollSnap_t2651831999, ___updateCounter_40)); }
	inline bool get_updateCounter_40() const { return ___updateCounter_40; }
	inline bool* get_address_of_updateCounter_40() { return &___updateCounter_40; }
	inline void set_updateCounter_40(bool value)
	{
		___updateCounter_40 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
