﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.ShareResult
struct ShareResult_t5683322;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void Facebook.Unity.ShareResult::.ctor(System.String)
extern "C"  void ShareResult__ctor_m214718114 (ShareResult_t5683322 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.ShareResult::get_PostId()
extern "C"  String_t* ShareResult_get_PostId_m2293539755 (ShareResult_t5683322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.ShareResult::set_PostId(System.String)
extern "C"  void ShareResult_set_PostId_m99241862 (ShareResult_t5683322 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
