﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.BlockPair
struct  BlockPair_t178469773  : public Il2CppObject
{
public:
	// System.Byte[] ZXing.QrCode.Internal.BlockPair::dataBytes
	ByteU5BU5D_t4260760469* ___dataBytes_0;
	// System.Byte[] ZXing.QrCode.Internal.BlockPair::errorCorrectionBytes
	ByteU5BU5D_t4260760469* ___errorCorrectionBytes_1;

public:
	inline static int32_t get_offset_of_dataBytes_0() { return static_cast<int32_t>(offsetof(BlockPair_t178469773, ___dataBytes_0)); }
	inline ByteU5BU5D_t4260760469* get_dataBytes_0() const { return ___dataBytes_0; }
	inline ByteU5BU5D_t4260760469** get_address_of_dataBytes_0() { return &___dataBytes_0; }
	inline void set_dataBytes_0(ByteU5BU5D_t4260760469* value)
	{
		___dataBytes_0 = value;
		Il2CppCodeGenWriteBarrier(&___dataBytes_0, value);
	}

	inline static int32_t get_offset_of_errorCorrectionBytes_1() { return static_cast<int32_t>(offsetof(BlockPair_t178469773, ___errorCorrectionBytes_1)); }
	inline ByteU5BU5D_t4260760469* get_errorCorrectionBytes_1() const { return ___errorCorrectionBytes_1; }
	inline ByteU5BU5D_t4260760469** get_address_of_errorCorrectionBytes_1() { return &___errorCorrectionBytes_1; }
	inline void set_errorCorrectionBytes_1(ByteU5BU5D_t4260760469* value)
	{
		___errorCorrectionBytes_1 = value;
		Il2CppCodeGenWriteBarrier(&___errorCorrectionBytes_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
