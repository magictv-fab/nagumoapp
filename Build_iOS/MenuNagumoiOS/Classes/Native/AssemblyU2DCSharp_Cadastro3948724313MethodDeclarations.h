﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Cadastro
struct Cadastro_t3948724313;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void Cadastro::.ctor()
extern "C"  void Cadastro__ctor_m654594658 (Cadastro_t3948724313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cadastro::Start()
extern "C"  void Cadastro_Start_m3896699746 (Cadastro_t3948724313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cadastro::EstadoSelect(System.String)
extern "C"  void Cadastro_EstadoSelect_m1152894558 (Cadastro_t3948724313 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Cadastro::ConvertUSToBrFormat(System.String)
extern "C"  String_t* Cadastro_ConvertUSToBrFormat_m1038178450 (Il2CppObject * __this /* static, unused */, String_t* ___dt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Cadastro::ConvertBRToUSFormat(System.String)
extern "C"  String_t* Cadastro_ConvertBRToUSFormat_m847285682 (Il2CppObject * __this /* static, unused */, String_t* ___dt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cadastro::Logout()
extern "C"  void Cadastro_Logout_m1250393452 (Cadastro_t3948724313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cadastro::Update()
extern "C"  void Cadastro_Update_m544460011 (Cadastro_t3948724313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cadastro::OnValidateInput(System.String)
extern "C"  void Cadastro_OnValidateInput_m3740852461 (Cadastro_t3948724313 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cadastro::FeedForm()
extern "C"  void Cadastro_FeedForm_m3694812452 (Cadastro_t3948724313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cadastro::SearchCep(System.String)
extern "C"  void Cadastro_SearchCep_m3308282044 (Cadastro_t3948724313 * __this, String_t* ___cep0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Cadastro::IBuscaCEPAPI(System.String)
extern "C"  Il2CppObject * Cadastro_IBuscaCEPAPI_m1994889719 (Cadastro_t3948724313 * __this, String_t* ___cep0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Cadastro::IBuscaCep(System.String)
extern "C"  Il2CppObject * Cadastro_IBuscaCep_m505324049 (Cadastro_t3948724313 * __this, String_t* ___cep0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Cadastro::ValidadeDate(System.String)
extern "C"  bool Cadastro_ValidadeDate_m4104786936 (Cadastro_t3948724313 * __this, String_t* ___date0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cadastro::SendJson()
extern "C"  void Cadastro_SendJson_m4058650290 (Cadastro_t3948724313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cadastro::onChangeSexo(System.Int32)
extern "C"  void Cadastro_onChangeSexo_m3440759211 (Cadastro_t3948724313 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Cadastro::CheckData()
extern "C"  bool Cadastro_CheckData_m47120926 (Cadastro_t3948724313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Cadastro::IsValid(System.String)
extern "C"  bool Cadastro_IsValid_m2524168900 (Cadastro_t3948724313 * __this, String_t* ___emailaddress0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Cadastro::ISendJson(System.String)
extern "C"  Il2CppObject * Cadastro_ISendJson_m272481809 (Cadastro_t3948724313 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cadastro::ClosePopUp()
extern "C"  void Cadastro_ClosePopUp_m346621590 (Cadastro_t3948724313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cadastro::PopUp(System.String)
extern "C"  void Cadastro_PopUp_m405338422 (Cadastro_t3948724313 * __this, String_t* ___txt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cadastro::ButtonCupons()
extern "C"  void Cadastro_ButtonCupons_m292594922 (Cadastro_t3948724313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cadastro::Close()
extern "C"  void Cadastro_Close_m2365454200 (Cadastro_t3948724313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Cadastro::GoLevel()
extern "C"  Il2CppObject * Cadastro_GoLevel_m1195155700 (Cadastro_t3948724313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Cadastro::Base64Encode(System.String)
extern "C"  String_t* Cadastro_Base64Encode_m2703191416 (Il2CppObject * __this /* static, unused */, String_t* ___plainText0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Cadastro::GetMacAddress()
extern "C"  String_t* Cadastro_GetMacAddress_m2861936792 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Cadastro::IsCnpj(System.String)
extern "C"  bool Cadastro_IsCnpj_m4109132509 (Il2CppObject * __this /* static, unused */, String_t* ___cnpj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Cadastro::IsCpf(System.String)
extern "C"  bool Cadastro_IsCpf_m4223852359 (Il2CppObject * __this /* static, unused */, String_t* ___cpf0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
