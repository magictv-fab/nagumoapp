﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector3Lerp
struct  Vector3Lerp_t122321781  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3Lerp::fromVector
	FsmVector3_t533912882 * ___fromVector_9;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3Lerp::toVector
	FsmVector3_t533912882 * ___toVector_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector3Lerp::amount
	FsmFloat_t2134102846 * ___amount_11;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3Lerp::storeResult
	FsmVector3_t533912882 * ___storeResult_12;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector3Lerp::everyFrame
	bool ___everyFrame_13;

public:
	inline static int32_t get_offset_of_fromVector_9() { return static_cast<int32_t>(offsetof(Vector3Lerp_t122321781, ___fromVector_9)); }
	inline FsmVector3_t533912882 * get_fromVector_9() const { return ___fromVector_9; }
	inline FsmVector3_t533912882 ** get_address_of_fromVector_9() { return &___fromVector_9; }
	inline void set_fromVector_9(FsmVector3_t533912882 * value)
	{
		___fromVector_9 = value;
		Il2CppCodeGenWriteBarrier(&___fromVector_9, value);
	}

	inline static int32_t get_offset_of_toVector_10() { return static_cast<int32_t>(offsetof(Vector3Lerp_t122321781, ___toVector_10)); }
	inline FsmVector3_t533912882 * get_toVector_10() const { return ___toVector_10; }
	inline FsmVector3_t533912882 ** get_address_of_toVector_10() { return &___toVector_10; }
	inline void set_toVector_10(FsmVector3_t533912882 * value)
	{
		___toVector_10 = value;
		Il2CppCodeGenWriteBarrier(&___toVector_10, value);
	}

	inline static int32_t get_offset_of_amount_11() { return static_cast<int32_t>(offsetof(Vector3Lerp_t122321781, ___amount_11)); }
	inline FsmFloat_t2134102846 * get_amount_11() const { return ___amount_11; }
	inline FsmFloat_t2134102846 ** get_address_of_amount_11() { return &___amount_11; }
	inline void set_amount_11(FsmFloat_t2134102846 * value)
	{
		___amount_11 = value;
		Il2CppCodeGenWriteBarrier(&___amount_11, value);
	}

	inline static int32_t get_offset_of_storeResult_12() { return static_cast<int32_t>(offsetof(Vector3Lerp_t122321781, ___storeResult_12)); }
	inline FsmVector3_t533912882 * get_storeResult_12() const { return ___storeResult_12; }
	inline FsmVector3_t533912882 ** get_address_of_storeResult_12() { return &___storeResult_12; }
	inline void set_storeResult_12(FsmVector3_t533912882 * value)
	{
		___storeResult_12 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_12, value);
	}

	inline static int32_t get_offset_of_everyFrame_13() { return static_cast<int32_t>(offsetof(Vector3Lerp_t122321781, ___everyFrame_13)); }
	inline bool get_everyFrame_13() const { return ___everyFrame_13; }
	inline bool* get_address_of_everyFrame_13() { return &___everyFrame_13; }
	inline void set_everyFrame_13(bool value)
	{
		___everyFrame_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
