﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.MatchTargetWeightMask
struct MatchTargetWeightMask_t258413904;
struct MatchTargetWeightMask_t258413904_marshaled_pinvoke;
struct MatchTargetWeightMask_t258413904_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_MatchTargetWeightMask258413904.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine.MatchTargetWeightMask::.ctor(UnityEngine.Vector3,System.Single)
extern "C"  void MatchTargetWeightMask__ctor_m2493400991 (MatchTargetWeightMask_t258413904 * __this, Vector3_t4282066566  ___positionXYZWeight0, float ___rotationWeight1, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct MatchTargetWeightMask_t258413904;
struct MatchTargetWeightMask_t258413904_marshaled_pinvoke;

extern "C" void MatchTargetWeightMask_t258413904_marshal_pinvoke(const MatchTargetWeightMask_t258413904& unmarshaled, MatchTargetWeightMask_t258413904_marshaled_pinvoke& marshaled);
extern "C" void MatchTargetWeightMask_t258413904_marshal_pinvoke_back(const MatchTargetWeightMask_t258413904_marshaled_pinvoke& marshaled, MatchTargetWeightMask_t258413904& unmarshaled);
extern "C" void MatchTargetWeightMask_t258413904_marshal_pinvoke_cleanup(MatchTargetWeightMask_t258413904_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct MatchTargetWeightMask_t258413904;
struct MatchTargetWeightMask_t258413904_marshaled_com;

extern "C" void MatchTargetWeightMask_t258413904_marshal_com(const MatchTargetWeightMask_t258413904& unmarshaled, MatchTargetWeightMask_t258413904_marshaled_com& marshaled);
extern "C" void MatchTargetWeightMask_t258413904_marshal_com_back(const MatchTargetWeightMask_t258413904_marshaled_com& marshaled, MatchTargetWeightMask_t258413904& unmarshaled);
extern "C" void MatchTargetWeightMask_t258413904_marshal_com_cleanup(MatchTargetWeightMask_t258413904_marshaled_com& marshaled);
