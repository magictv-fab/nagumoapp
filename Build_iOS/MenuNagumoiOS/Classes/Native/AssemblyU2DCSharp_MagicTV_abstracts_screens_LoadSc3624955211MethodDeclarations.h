﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.abstracts.screens.LoadScreenAbstract/OnLoadAnswerEventHandler
struct OnLoadAnswerEventHandler_t3624955211;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void MagicTV.abstracts.screens.LoadScreenAbstract/OnLoadAnswerEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnLoadAnswerEventHandler__ctor_m1447029666 (OnLoadAnswerEventHandler_t3624955211 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.LoadScreenAbstract/OnLoadAnswerEventHandler::Invoke()
extern "C"  void OnLoadAnswerEventHandler_Invoke_m219078268 (OnLoadAnswerEventHandler_t3624955211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult MagicTV.abstracts.screens.LoadScreenAbstract/OnLoadAnswerEventHandler::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnLoadAnswerEventHandler_BeginInvoke_m2652139151 (OnLoadAnswerEventHandler_t3624955211 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.LoadScreenAbstract/OnLoadAnswerEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnLoadAnswerEventHandler_EndInvoke_m3792600754 (OnLoadAnswerEventHandler_t3624955211 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
