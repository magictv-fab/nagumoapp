﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Common.DetectorResult
struct DetectorResult_t3846928147;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// ZXing.ResultPoint[]
struct ResultPointU5BU5D_t1195164344;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitMatrix1058711404.h"

// ZXing.Common.BitMatrix ZXing.Common.DetectorResult::get_Bits()
extern "C"  BitMatrix_t1058711404 * DetectorResult_get_Bits_m152465884 (DetectorResult_t3846928147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.DetectorResult::set_Bits(ZXing.Common.BitMatrix)
extern "C"  void DetectorResult_set_Bits_m2139224611 (DetectorResult_t3846928147 * __this, BitMatrix_t1058711404 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.ResultPoint[] ZXing.Common.DetectorResult::get_Points()
extern "C"  ResultPointU5BU5D_t1195164344* DetectorResult_get_Points_m2415034585 (DetectorResult_t3846928147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.DetectorResult::set_Points(ZXing.ResultPoint[])
extern "C"  void DetectorResult_set_Points_m1094512626 (DetectorResult_t3846928147 * __this, ResultPointU5BU5D_t1195164344* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.DetectorResult::.ctor(ZXing.Common.BitMatrix,ZXing.ResultPoint[])
extern "C"  void DetectorResult__ctor_m3216309499 (DetectorResult_t3846928147 * __this, BitMatrix_t1058711404 * ___bits0, ResultPointU5BU5D_t1195164344* ___points1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
