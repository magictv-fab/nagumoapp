﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IO.MemoryStream
struct MemoryStream_t418716369;

#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Base1865109560.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.MemoryArchiveStorage
struct  MemoryArchiveStorage_t3679306472  : public BaseArchiveStorage_t1865109560
{
public:
	// System.IO.MemoryStream ICSharpCode.SharpZipLib.Zip.MemoryArchiveStorage::temporaryStream_
	MemoryStream_t418716369 * ___temporaryStream__1;
	// System.IO.MemoryStream ICSharpCode.SharpZipLib.Zip.MemoryArchiveStorage::finalStream_
	MemoryStream_t418716369 * ___finalStream__2;

public:
	inline static int32_t get_offset_of_temporaryStream__1() { return static_cast<int32_t>(offsetof(MemoryArchiveStorage_t3679306472, ___temporaryStream__1)); }
	inline MemoryStream_t418716369 * get_temporaryStream__1() const { return ___temporaryStream__1; }
	inline MemoryStream_t418716369 ** get_address_of_temporaryStream__1() { return &___temporaryStream__1; }
	inline void set_temporaryStream__1(MemoryStream_t418716369 * value)
	{
		___temporaryStream__1 = value;
		Il2CppCodeGenWriteBarrier(&___temporaryStream__1, value);
	}

	inline static int32_t get_offset_of_finalStream__2() { return static_cast<int32_t>(offsetof(MemoryArchiveStorage_t3679306472, ___finalStream__2)); }
	inline MemoryStream_t418716369 * get_finalStream__2() const { return ___finalStream__2; }
	inline MemoryStream_t418716369 ** get_address_of_finalStream__2() { return &___finalStream__2; }
	inline void set_finalStream__2(MemoryStream_t418716369 * value)
	{
		___finalStream__2 = value;
		Il2CppCodeGenWriteBarrier(&___finalStream__2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
