﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.transform.filters.HistoryDelaySituationCameraFilter
struct HistoryDelaySituationCameraFilter_t914159810;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void ARM.transform.filters.HistoryDelaySituationCameraFilter::.ctor()
extern "C"  void HistoryDelaySituationCameraFilter__ctor_m3431463686 (HistoryDelaySituationCameraFilter_t914159810 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.HistoryDelaySituationCameraFilter::Start()
extern "C"  void HistoryDelaySituationCameraFilter_Start_m2378601478 (HistoryDelaySituationCameraFilter_t914159810 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.HistoryDelaySituationCameraFilter::trackIn()
extern "C"  void HistoryDelaySituationCameraFilter_trackIn_m1769612116 (HistoryDelaySituationCameraFilter_t914159810 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.HistoryDelaySituationCameraFilter::trackOut()
extern "C"  void HistoryDelaySituationCameraFilter_trackOut_m3324190753 (HistoryDelaySituationCameraFilter_t914159810 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.HistoryDelaySituationCameraFilter::Update()
extern "C"  void HistoryDelaySituationCameraFilter_Update_m728053959 (HistoryDelaySituationCameraFilter_t914159810 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion ARM.transform.filters.HistoryDelaySituationCameraFilter::_doFilter(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  HistoryDelaySituationCameraFilter__doFilter_m3983202264 (HistoryDelaySituationCameraFilter_t914159810 * __this, Quaternion_t1553702882  ___current0, Quaternion_t1553702882  ___next1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion ARM.transform.filters.HistoryDelaySituationCameraFilter::filter(UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  HistoryDelaySituationCameraFilter_filter_m2662182617 (HistoryDelaySituationCameraFilter_t914159810 * __this, Quaternion_t1553702882  ___supoustCameraAngle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.transform.filters.HistoryDelaySituationCameraFilter::filter(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  HistoryDelaySituationCameraFilter_filter_m1047894269 (HistoryDelaySituationCameraFilter_t914159810 * __this, Vector3_t4282066566  ___supoustCameraPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.transform.filters.HistoryDelaySituationCameraFilter::_doFilter(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  HistoryDelaySituationCameraFilter__doFilter_m1731137260 (HistoryDelaySituationCameraFilter_t914159810 * __this, Vector3_t4282066566  ___currentPosition0, Vector3_t4282066566  ___newPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.HistoryDelaySituationCameraFilter::save(UnityEngine.Quaternion)
extern "C"  void HistoryDelaySituationCameraFilter_save_m4023179558 (HistoryDelaySituationCameraFilter_t914159810 * __this, Quaternion_t1553702882  ___angle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.HistoryDelaySituationCameraFilter::save(UnityEngine.Vector3)
extern "C"  void HistoryDelaySituationCameraFilter_save_m1497462878 (HistoryDelaySituationCameraFilter_t914159810 * __this, Vector3_t4282066566  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
