﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22656869371MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<ZXing.Aztec.Internal.Decoder/Table,System.String[]>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1165061272(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2540055952 *, int32_t, StringU5BU5D_t4054002952*, const MethodInfo*))KeyValuePair_2__ctor_m4068774668_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<ZXing.Aztec.Internal.Decoder/Table,System.String[]>::get_Key()
#define KeyValuePair_2_get_Key_m3723125840(__this, method) ((  int32_t (*) (KeyValuePair_2_t2540055952 *, const MethodInfo*))KeyValuePair_2_get_Key_m548895580_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<ZXing.Aztec.Internal.Decoder/Table,System.String[]>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3097652881(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2540055952 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m3592818589_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<ZXing.Aztec.Internal.Decoder/Table,System.String[]>::get_Value()
#define KeyValuePair_2_get_Value_m2337968272(__this, method) ((  StringU5BU5D_t4054002952* (*) (KeyValuePair_2_t2540055952 *, const MethodInfo*))KeyValuePair_2_get_Value_m2031527964_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<ZXing.Aztec.Internal.Decoder/Table,System.String[]>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2719093265(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2540055952 *, StringU5BU5D_t4054002952*, const MethodInfo*))KeyValuePair_2_set_Value_m596004125_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<ZXing.Aztec.Internal.Decoder/Table,System.String[]>::ToString()
#define KeyValuePair_2_ToString_m2416497777(__this, method) ((  String_t* (*) (KeyValuePair_2_t2540055952 *, const MethodInfo*))KeyValuePair_2_ToString_m637364773_gshared)(__this, method)
