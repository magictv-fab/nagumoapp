﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetKey
struct GetKey_t2986509073;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetKey::.ctor()
extern "C"  void GetKey__ctor_m653000821 (GetKey_t2986509073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetKey::Reset()
extern "C"  void GetKey_Reset_m2594401058 (GetKey_t2986509073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetKey::OnEnter()
extern "C"  void GetKey_OnEnter_m4155259660 (GetKey_t2986509073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetKey::OnUpdate()
extern "C"  void GetKey_OnUpdate_m3392557239 (GetKey_t2986509073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetKey::DoGetKey()
extern "C"  void GetKey_DoGetKey_m86958531 (GetKey_t2986509073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
