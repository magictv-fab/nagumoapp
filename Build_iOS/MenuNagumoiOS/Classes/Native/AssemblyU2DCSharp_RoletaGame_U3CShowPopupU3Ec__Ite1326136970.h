﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Hashtable
struct Hashtable_t1407064410;
// System.Object
struct Il2CppObject;
// RoletaGame
struct RoletaGame_t108100629;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RoletaGame/<ShowPopup>c__Iterator7A
struct  U3CShowPopupU3Ec__Iterator7A_t1326136970  : public Il2CppObject
{
public:
	// System.Collections.Hashtable RoletaGame/<ShowPopup>c__Iterator7A::<ht>__0
	Hashtable_t1407064410 * ___U3ChtU3E__0_0;
	// System.Int32 RoletaGame/<ShowPopup>c__Iterator7A::$PC
	int32_t ___U24PC_1;
	// System.Object RoletaGame/<ShowPopup>c__Iterator7A::$current
	Il2CppObject * ___U24current_2;
	// RoletaGame RoletaGame/<ShowPopup>c__Iterator7A::<>f__this
	RoletaGame_t108100629 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_U3ChtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CShowPopupU3Ec__Iterator7A_t1326136970, ___U3ChtU3E__0_0)); }
	inline Hashtable_t1407064410 * get_U3ChtU3E__0_0() const { return ___U3ChtU3E__0_0; }
	inline Hashtable_t1407064410 ** get_address_of_U3ChtU3E__0_0() { return &___U3ChtU3E__0_0; }
	inline void set_U3ChtU3E__0_0(Hashtable_t1407064410 * value)
	{
		___U3ChtU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3ChtU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CShowPopupU3Ec__Iterator7A_t1326136970, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CShowPopupU3Ec__Iterator7A_t1326136970, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3CShowPopupU3Ec__Iterator7A_t1326136970, ___U3CU3Ef__this_3)); }
	inline RoletaGame_t108100629 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline RoletaGame_t108100629 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(RoletaGame_t108100629 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
