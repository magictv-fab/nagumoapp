﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ScreenshotManager_SaveStatus3474830263.h"
#include "AssemblyU2DCSharp_ScreenshotManager_ImageType30728781.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScreenshotManager/<Save>c__Iterator31
struct  U3CSaveU3Ec__Iterator31_t1819521282  : public Il2CppObject
{
public:
	// System.Int32 ScreenshotManager/<Save>c__Iterator31::<count>__0
	int32_t ___U3CcountU3E__0_0;
	// ScreenshotManager/SaveStatus ScreenshotManager/<Save>c__Iterator31::<saved>__1
	int32_t ___U3CsavedU3E__1_1;
	// System.String ScreenshotManager/<Save>c__Iterator31::path
	String_t* ___path_2;
	// System.Byte[] ScreenshotManager/<Save>c__Iterator31::bytes
	ByteU5BU5D_t4260760469* ___bytes_3;
	// ScreenshotManager/ImageType ScreenshotManager/<Save>c__Iterator31::imageType
	int32_t ___imageType_4;
	// System.Int32 ScreenshotManager/<Save>c__Iterator31::$PC
	int32_t ___U24PC_5;
	// System.Object ScreenshotManager/<Save>c__Iterator31::$current
	Il2CppObject * ___U24current_6;
	// System.String ScreenshotManager/<Save>c__Iterator31::<$>path
	String_t* ___U3CU24U3Epath_7;
	// System.Byte[] ScreenshotManager/<Save>c__Iterator31::<$>bytes
	ByteU5BU5D_t4260760469* ___U3CU24U3Ebytes_8;
	// ScreenshotManager/ImageType ScreenshotManager/<Save>c__Iterator31::<$>imageType
	int32_t ___U3CU24U3EimageType_9;

public:
	inline static int32_t get_offset_of_U3CcountU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSaveU3Ec__Iterator31_t1819521282, ___U3CcountU3E__0_0)); }
	inline int32_t get_U3CcountU3E__0_0() const { return ___U3CcountU3E__0_0; }
	inline int32_t* get_address_of_U3CcountU3E__0_0() { return &___U3CcountU3E__0_0; }
	inline void set_U3CcountU3E__0_0(int32_t value)
	{
		___U3CcountU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CsavedU3E__1_1() { return static_cast<int32_t>(offsetof(U3CSaveU3Ec__Iterator31_t1819521282, ___U3CsavedU3E__1_1)); }
	inline int32_t get_U3CsavedU3E__1_1() const { return ___U3CsavedU3E__1_1; }
	inline int32_t* get_address_of_U3CsavedU3E__1_1() { return &___U3CsavedU3E__1_1; }
	inline void set_U3CsavedU3E__1_1(int32_t value)
	{
		___U3CsavedU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_path_2() { return static_cast<int32_t>(offsetof(U3CSaveU3Ec__Iterator31_t1819521282, ___path_2)); }
	inline String_t* get_path_2() const { return ___path_2; }
	inline String_t** get_address_of_path_2() { return &___path_2; }
	inline void set_path_2(String_t* value)
	{
		___path_2 = value;
		Il2CppCodeGenWriteBarrier(&___path_2, value);
	}

	inline static int32_t get_offset_of_bytes_3() { return static_cast<int32_t>(offsetof(U3CSaveU3Ec__Iterator31_t1819521282, ___bytes_3)); }
	inline ByteU5BU5D_t4260760469* get_bytes_3() const { return ___bytes_3; }
	inline ByteU5BU5D_t4260760469** get_address_of_bytes_3() { return &___bytes_3; }
	inline void set_bytes_3(ByteU5BU5D_t4260760469* value)
	{
		___bytes_3 = value;
		Il2CppCodeGenWriteBarrier(&___bytes_3, value);
	}

	inline static int32_t get_offset_of_imageType_4() { return static_cast<int32_t>(offsetof(U3CSaveU3Ec__Iterator31_t1819521282, ___imageType_4)); }
	inline int32_t get_imageType_4() const { return ___imageType_4; }
	inline int32_t* get_address_of_imageType_4() { return &___imageType_4; }
	inline void set_imageType_4(int32_t value)
	{
		___imageType_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CSaveU3Ec__Iterator31_t1819521282, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CSaveU3Ec__Iterator31_t1819521282, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Epath_7() { return static_cast<int32_t>(offsetof(U3CSaveU3Ec__Iterator31_t1819521282, ___U3CU24U3Epath_7)); }
	inline String_t* get_U3CU24U3Epath_7() const { return ___U3CU24U3Epath_7; }
	inline String_t** get_address_of_U3CU24U3Epath_7() { return &___U3CU24U3Epath_7; }
	inline void set_U3CU24U3Epath_7(String_t* value)
	{
		___U3CU24U3Epath_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Epath_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ebytes_8() { return static_cast<int32_t>(offsetof(U3CSaveU3Ec__Iterator31_t1819521282, ___U3CU24U3Ebytes_8)); }
	inline ByteU5BU5D_t4260760469* get_U3CU24U3Ebytes_8() const { return ___U3CU24U3Ebytes_8; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CU24U3Ebytes_8() { return &___U3CU24U3Ebytes_8; }
	inline void set_U3CU24U3Ebytes_8(ByteU5BU5D_t4260760469* value)
	{
		___U3CU24U3Ebytes_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ebytes_8, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EimageType_9() { return static_cast<int32_t>(offsetof(U3CSaveU3Ec__Iterator31_t1819521282, ___U3CU24U3EimageType_9)); }
	inline int32_t get_U3CU24U3EimageType_9() const { return ___U3CU24U3EimageType_9; }
	inline int32_t* get_address_of_U3CU24U3EimageType_9() { return &___U3CU24U3EimageType_9; }
	inline void set_U3CU24U3EimageType_9(int32_t value)
	{
		___U3CU24U3EimageType_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
