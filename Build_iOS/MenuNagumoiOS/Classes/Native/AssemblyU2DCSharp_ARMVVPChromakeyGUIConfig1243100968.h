﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.UI.InputField
struct InputField_t609046876;
// ARMVVPChromakeyGUIConfig
struct ARMVVPChromakeyGUIConfig_t1243100968;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARMVVPChromakeyGUIConfig
struct  ARMVVPChromakeyGUIConfig_t1243100968  : public MonoBehaviour_t667441552
{
public:
	// System.Single ARMVVPChromakeyGUIConfig::sensibilityChanger
	float ___sensibilityChanger_2;
	// System.Single ARMVVPChromakeyGUIConfig::recorteChanger
	float ___recorteChanger_3;
	// System.Single ARMVVPChromakeyGUIConfig::smoothChanger
	float ___smoothChanger_4;
	// System.Single ARMVVPChromakeyGUIConfig::smoothSensibilityChanger
	float ___smoothSensibilityChanger_5;
	// System.Single ARMVVPChromakeyGUIConfig::sensibility
	float ___sensibility_6;
	// System.Single ARMVVPChromakeyGUIConfig::recorte
	float ___recorte_7;
	// System.Single ARMVVPChromakeyGUIConfig::smoothSensibility
	float ___smoothSensibility_8;
	// System.Single ARMVVPChromakeyGUIConfig::smooth
	float ___smooth_9;
	// System.String ARMVVPChromakeyGUIConfig::shaderChoiced
	String_t* ___shaderChoiced_10;
	// UnityEngine.UI.Text ARMVVPChromakeyGUIConfig::GUIMaterialName
	Text_t9039225 * ___GUIMaterialName_11;
	// UnityEngine.UI.Text ARMVVPChromakeyGUIConfig::GUISensibility
	Text_t9039225 * ___GUISensibility_12;
	// UnityEngine.UI.Text ARMVVPChromakeyGUIConfig::GUIRecorte
	Text_t9039225 * ___GUIRecorte_13;
	// UnityEngine.UI.InputField ARMVVPChromakeyGUIConfig::GUIFloatValSmoothInput
	InputField_t609046876 * ___GUIFloatValSmoothInput_14;
	// UnityEngine.UI.Text ARMVVPChromakeyGUIConfig::GUISmoothLabel
	Text_t9039225 * ___GUISmoothLabel_15;
	// UnityEngine.UI.InputField ARMVVPChromakeyGUIConfig::GUIFloatValSmoothSensibilityInput
	InputField_t609046876 * ___GUIFloatValSmoothSensibilityInput_16;
	// UnityEngine.UI.Text ARMVVPChromakeyGUIConfig::GUISmoothSensibilityLabel
	Text_t9039225 * ___GUISmoothSensibilityLabel_17;
	// UnityEngine.UI.InputField ARMVVPChromakeyGUIConfig::GUIFloatValSensi
	InputField_t609046876 * ___GUIFloatValSensi_18;
	// UnityEngine.UI.InputField ARMVVPChromakeyGUIConfig::GUIFloatrecorte
	InputField_t609046876 * ___GUIFloatrecorte_19;
	// UnityEngine.UI.InputField ARMVVPChromakeyGUIConfig::GUISaveString
	InputField_t609046876 * ___GUISaveString_20;
	// System.String ARMVVPChromakeyGUIConfig::bundleID
	String_t* ___bundleID_21;

public:
	inline static int32_t get_offset_of_sensibilityChanger_2() { return static_cast<int32_t>(offsetof(ARMVVPChromakeyGUIConfig_t1243100968, ___sensibilityChanger_2)); }
	inline float get_sensibilityChanger_2() const { return ___sensibilityChanger_2; }
	inline float* get_address_of_sensibilityChanger_2() { return &___sensibilityChanger_2; }
	inline void set_sensibilityChanger_2(float value)
	{
		___sensibilityChanger_2 = value;
	}

	inline static int32_t get_offset_of_recorteChanger_3() { return static_cast<int32_t>(offsetof(ARMVVPChromakeyGUIConfig_t1243100968, ___recorteChanger_3)); }
	inline float get_recorteChanger_3() const { return ___recorteChanger_3; }
	inline float* get_address_of_recorteChanger_3() { return &___recorteChanger_3; }
	inline void set_recorteChanger_3(float value)
	{
		___recorteChanger_3 = value;
	}

	inline static int32_t get_offset_of_smoothChanger_4() { return static_cast<int32_t>(offsetof(ARMVVPChromakeyGUIConfig_t1243100968, ___smoothChanger_4)); }
	inline float get_smoothChanger_4() const { return ___smoothChanger_4; }
	inline float* get_address_of_smoothChanger_4() { return &___smoothChanger_4; }
	inline void set_smoothChanger_4(float value)
	{
		___smoothChanger_4 = value;
	}

	inline static int32_t get_offset_of_smoothSensibilityChanger_5() { return static_cast<int32_t>(offsetof(ARMVVPChromakeyGUIConfig_t1243100968, ___smoothSensibilityChanger_5)); }
	inline float get_smoothSensibilityChanger_5() const { return ___smoothSensibilityChanger_5; }
	inline float* get_address_of_smoothSensibilityChanger_5() { return &___smoothSensibilityChanger_5; }
	inline void set_smoothSensibilityChanger_5(float value)
	{
		___smoothSensibilityChanger_5 = value;
	}

	inline static int32_t get_offset_of_sensibility_6() { return static_cast<int32_t>(offsetof(ARMVVPChromakeyGUIConfig_t1243100968, ___sensibility_6)); }
	inline float get_sensibility_6() const { return ___sensibility_6; }
	inline float* get_address_of_sensibility_6() { return &___sensibility_6; }
	inline void set_sensibility_6(float value)
	{
		___sensibility_6 = value;
	}

	inline static int32_t get_offset_of_recorte_7() { return static_cast<int32_t>(offsetof(ARMVVPChromakeyGUIConfig_t1243100968, ___recorte_7)); }
	inline float get_recorte_7() const { return ___recorte_7; }
	inline float* get_address_of_recorte_7() { return &___recorte_7; }
	inline void set_recorte_7(float value)
	{
		___recorte_7 = value;
	}

	inline static int32_t get_offset_of_smoothSensibility_8() { return static_cast<int32_t>(offsetof(ARMVVPChromakeyGUIConfig_t1243100968, ___smoothSensibility_8)); }
	inline float get_smoothSensibility_8() const { return ___smoothSensibility_8; }
	inline float* get_address_of_smoothSensibility_8() { return &___smoothSensibility_8; }
	inline void set_smoothSensibility_8(float value)
	{
		___smoothSensibility_8 = value;
	}

	inline static int32_t get_offset_of_smooth_9() { return static_cast<int32_t>(offsetof(ARMVVPChromakeyGUIConfig_t1243100968, ___smooth_9)); }
	inline float get_smooth_9() const { return ___smooth_9; }
	inline float* get_address_of_smooth_9() { return &___smooth_9; }
	inline void set_smooth_9(float value)
	{
		___smooth_9 = value;
	}

	inline static int32_t get_offset_of_shaderChoiced_10() { return static_cast<int32_t>(offsetof(ARMVVPChromakeyGUIConfig_t1243100968, ___shaderChoiced_10)); }
	inline String_t* get_shaderChoiced_10() const { return ___shaderChoiced_10; }
	inline String_t** get_address_of_shaderChoiced_10() { return &___shaderChoiced_10; }
	inline void set_shaderChoiced_10(String_t* value)
	{
		___shaderChoiced_10 = value;
		Il2CppCodeGenWriteBarrier(&___shaderChoiced_10, value);
	}

	inline static int32_t get_offset_of_GUIMaterialName_11() { return static_cast<int32_t>(offsetof(ARMVVPChromakeyGUIConfig_t1243100968, ___GUIMaterialName_11)); }
	inline Text_t9039225 * get_GUIMaterialName_11() const { return ___GUIMaterialName_11; }
	inline Text_t9039225 ** get_address_of_GUIMaterialName_11() { return &___GUIMaterialName_11; }
	inline void set_GUIMaterialName_11(Text_t9039225 * value)
	{
		___GUIMaterialName_11 = value;
		Il2CppCodeGenWriteBarrier(&___GUIMaterialName_11, value);
	}

	inline static int32_t get_offset_of_GUISensibility_12() { return static_cast<int32_t>(offsetof(ARMVVPChromakeyGUIConfig_t1243100968, ___GUISensibility_12)); }
	inline Text_t9039225 * get_GUISensibility_12() const { return ___GUISensibility_12; }
	inline Text_t9039225 ** get_address_of_GUISensibility_12() { return &___GUISensibility_12; }
	inline void set_GUISensibility_12(Text_t9039225 * value)
	{
		___GUISensibility_12 = value;
		Il2CppCodeGenWriteBarrier(&___GUISensibility_12, value);
	}

	inline static int32_t get_offset_of_GUIRecorte_13() { return static_cast<int32_t>(offsetof(ARMVVPChromakeyGUIConfig_t1243100968, ___GUIRecorte_13)); }
	inline Text_t9039225 * get_GUIRecorte_13() const { return ___GUIRecorte_13; }
	inline Text_t9039225 ** get_address_of_GUIRecorte_13() { return &___GUIRecorte_13; }
	inline void set_GUIRecorte_13(Text_t9039225 * value)
	{
		___GUIRecorte_13 = value;
		Il2CppCodeGenWriteBarrier(&___GUIRecorte_13, value);
	}

	inline static int32_t get_offset_of_GUIFloatValSmoothInput_14() { return static_cast<int32_t>(offsetof(ARMVVPChromakeyGUIConfig_t1243100968, ___GUIFloatValSmoothInput_14)); }
	inline InputField_t609046876 * get_GUIFloatValSmoothInput_14() const { return ___GUIFloatValSmoothInput_14; }
	inline InputField_t609046876 ** get_address_of_GUIFloatValSmoothInput_14() { return &___GUIFloatValSmoothInput_14; }
	inline void set_GUIFloatValSmoothInput_14(InputField_t609046876 * value)
	{
		___GUIFloatValSmoothInput_14 = value;
		Il2CppCodeGenWriteBarrier(&___GUIFloatValSmoothInput_14, value);
	}

	inline static int32_t get_offset_of_GUISmoothLabel_15() { return static_cast<int32_t>(offsetof(ARMVVPChromakeyGUIConfig_t1243100968, ___GUISmoothLabel_15)); }
	inline Text_t9039225 * get_GUISmoothLabel_15() const { return ___GUISmoothLabel_15; }
	inline Text_t9039225 ** get_address_of_GUISmoothLabel_15() { return &___GUISmoothLabel_15; }
	inline void set_GUISmoothLabel_15(Text_t9039225 * value)
	{
		___GUISmoothLabel_15 = value;
		Il2CppCodeGenWriteBarrier(&___GUISmoothLabel_15, value);
	}

	inline static int32_t get_offset_of_GUIFloatValSmoothSensibilityInput_16() { return static_cast<int32_t>(offsetof(ARMVVPChromakeyGUIConfig_t1243100968, ___GUIFloatValSmoothSensibilityInput_16)); }
	inline InputField_t609046876 * get_GUIFloatValSmoothSensibilityInput_16() const { return ___GUIFloatValSmoothSensibilityInput_16; }
	inline InputField_t609046876 ** get_address_of_GUIFloatValSmoothSensibilityInput_16() { return &___GUIFloatValSmoothSensibilityInput_16; }
	inline void set_GUIFloatValSmoothSensibilityInput_16(InputField_t609046876 * value)
	{
		___GUIFloatValSmoothSensibilityInput_16 = value;
		Il2CppCodeGenWriteBarrier(&___GUIFloatValSmoothSensibilityInput_16, value);
	}

	inline static int32_t get_offset_of_GUISmoothSensibilityLabel_17() { return static_cast<int32_t>(offsetof(ARMVVPChromakeyGUIConfig_t1243100968, ___GUISmoothSensibilityLabel_17)); }
	inline Text_t9039225 * get_GUISmoothSensibilityLabel_17() const { return ___GUISmoothSensibilityLabel_17; }
	inline Text_t9039225 ** get_address_of_GUISmoothSensibilityLabel_17() { return &___GUISmoothSensibilityLabel_17; }
	inline void set_GUISmoothSensibilityLabel_17(Text_t9039225 * value)
	{
		___GUISmoothSensibilityLabel_17 = value;
		Il2CppCodeGenWriteBarrier(&___GUISmoothSensibilityLabel_17, value);
	}

	inline static int32_t get_offset_of_GUIFloatValSensi_18() { return static_cast<int32_t>(offsetof(ARMVVPChromakeyGUIConfig_t1243100968, ___GUIFloatValSensi_18)); }
	inline InputField_t609046876 * get_GUIFloatValSensi_18() const { return ___GUIFloatValSensi_18; }
	inline InputField_t609046876 ** get_address_of_GUIFloatValSensi_18() { return &___GUIFloatValSensi_18; }
	inline void set_GUIFloatValSensi_18(InputField_t609046876 * value)
	{
		___GUIFloatValSensi_18 = value;
		Il2CppCodeGenWriteBarrier(&___GUIFloatValSensi_18, value);
	}

	inline static int32_t get_offset_of_GUIFloatrecorte_19() { return static_cast<int32_t>(offsetof(ARMVVPChromakeyGUIConfig_t1243100968, ___GUIFloatrecorte_19)); }
	inline InputField_t609046876 * get_GUIFloatrecorte_19() const { return ___GUIFloatrecorte_19; }
	inline InputField_t609046876 ** get_address_of_GUIFloatrecorte_19() { return &___GUIFloatrecorte_19; }
	inline void set_GUIFloatrecorte_19(InputField_t609046876 * value)
	{
		___GUIFloatrecorte_19 = value;
		Il2CppCodeGenWriteBarrier(&___GUIFloatrecorte_19, value);
	}

	inline static int32_t get_offset_of_GUISaveString_20() { return static_cast<int32_t>(offsetof(ARMVVPChromakeyGUIConfig_t1243100968, ___GUISaveString_20)); }
	inline InputField_t609046876 * get_GUISaveString_20() const { return ___GUISaveString_20; }
	inline InputField_t609046876 ** get_address_of_GUISaveString_20() { return &___GUISaveString_20; }
	inline void set_GUISaveString_20(InputField_t609046876 * value)
	{
		___GUISaveString_20 = value;
		Il2CppCodeGenWriteBarrier(&___GUISaveString_20, value);
	}

	inline static int32_t get_offset_of_bundleID_21() { return static_cast<int32_t>(offsetof(ARMVVPChromakeyGUIConfig_t1243100968, ___bundleID_21)); }
	inline String_t* get_bundleID_21() const { return ___bundleID_21; }
	inline String_t** get_address_of_bundleID_21() { return &___bundleID_21; }
	inline void set_bundleID_21(String_t* value)
	{
		___bundleID_21 = value;
		Il2CppCodeGenWriteBarrier(&___bundleID_21, value);
	}
};

struct ARMVVPChromakeyGUIConfig_t1243100968_StaticFields
{
public:
	// ARMVVPChromakeyGUIConfig ARMVVPChromakeyGUIConfig::Instance
	ARMVVPChromakeyGUIConfig_t1243100968 * ___Instance_22;

public:
	inline static int32_t get_offset_of_Instance_22() { return static_cast<int32_t>(offsetof(ARMVVPChromakeyGUIConfig_t1243100968_StaticFields, ___Instance_22)); }
	inline ARMVVPChromakeyGUIConfig_t1243100968 * get_Instance_22() const { return ___Instance_22; }
	inline ARMVVPChromakeyGUIConfig_t1243100968 ** get_address_of_Instance_22() { return &___Instance_22; }
	inline void set_Instance_22(ARMVVPChromakeyGUIConfig_t1243100968 * value)
	{
		___Instance_22 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
