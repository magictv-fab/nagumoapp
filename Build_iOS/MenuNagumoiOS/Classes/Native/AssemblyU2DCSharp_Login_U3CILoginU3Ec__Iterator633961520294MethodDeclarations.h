﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Login/<ILogin>c__Iterator63
struct U3CILoginU3Ec__Iterator63_t3961520294;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Login/<ILogin>c__Iterator63::.ctor()
extern "C"  void U3CILoginU3Ec__Iterator63__ctor_m2692220293 (U3CILoginU3Ec__Iterator63_t3961520294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Login/<ILogin>c__Iterator63::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CILoginU3Ec__Iterator63_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1970685037 (U3CILoginU3Ec__Iterator63_t3961520294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Login/<ILogin>c__Iterator63::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CILoginU3Ec__Iterator63_System_Collections_IEnumerator_get_Current_m3817914881 (U3CILoginU3Ec__Iterator63_t3961520294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Login/<ILogin>c__Iterator63::MoveNext()
extern "C"  bool U3CILoginU3Ec__Iterator63_MoveNext_m2988880975 (U3CILoginU3Ec__Iterator63_t3961520294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login/<ILogin>c__Iterator63::Dispose()
extern "C"  void U3CILoginU3Ec__Iterator63_Dispose_m1551672450 (U3CILoginU3Ec__Iterator63_t3961520294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Login/<ILogin>c__Iterator63::Reset()
extern "C"  void U3CILoginU3Ec__Iterator63_Reset_m338653234 (U3CILoginU3Ec__Iterator63_t3961520294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
