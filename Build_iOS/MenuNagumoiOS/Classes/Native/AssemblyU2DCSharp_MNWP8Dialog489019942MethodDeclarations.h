﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MNWP8Dialog
struct MNWP8Dialog_t489019942;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MNWP8Dialog::.ctor()
extern "C"  void MNWP8Dialog__ctor_m4242648069 (MNWP8Dialog_t489019942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MNWP8Dialog MNWP8Dialog::Create(System.String,System.String)
extern "C"  MNWP8Dialog_t489019942 * MNWP8Dialog_Create_m1525115888 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNWP8Dialog::init()
extern "C"  void MNWP8Dialog_init_m4191117711 (MNWP8Dialog_t489019942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNWP8Dialog::OnOkDel()
extern "C"  void MNWP8Dialog_OnOkDel_m807438995 (MNWP8Dialog_t489019942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNWP8Dialog::OnCancelDel()
extern "C"  void MNWP8Dialog_OnCancelDel_m3257795381 (MNWP8Dialog_t489019942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
