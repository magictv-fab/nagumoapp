﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AnimateFsmAction
struct AnimateFsmAction_t4201352541;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AnimateFsmAction::.ctor()
extern "C"  void AnimateFsmAction__ctor_m3622440361 (AnimateFsmAction_t4201352541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateFsmAction::Reset()
extern "C"  void AnimateFsmAction_Reset_m1268873302 (AnimateFsmAction_t4201352541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateFsmAction::OnEnter()
extern "C"  void AnimateFsmAction_OnEnter_m1633405760 (AnimateFsmAction_t4201352541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateFsmAction::Init()
extern "C"  void AnimateFsmAction_Init_m3532072843 (AnimateFsmAction_t4201352541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateFsmAction::OnUpdate()
extern "C"  void AnimateFsmAction_OnUpdate_m2524497667 (AnimateFsmAction_t4201352541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
