﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertasManagerCRM_PraVoce/<LoadLoadeds>c__IteratorF
struct U3CLoadLoadedsU3Ec__IteratorF_t1761641062;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OfertasManagerCRM_PraVoce/<LoadLoadeds>c__IteratorF::.ctor()
extern "C"  void U3CLoadLoadedsU3Ec__IteratorF__ctor_m3299689413 (U3CLoadLoadedsU3Ec__IteratorF_t1761641062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManagerCRM_PraVoce/<LoadLoadeds>c__IteratorF::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadLoadedsU3Ec__IteratorF_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2526158125 (U3CLoadLoadedsU3Ec__IteratorF_t1761641062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManagerCRM_PraVoce/<LoadLoadeds>c__IteratorF::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadLoadedsU3Ec__IteratorF_System_Collections_IEnumerator_get_Current_m2331384513 (U3CLoadLoadedsU3Ec__IteratorF_t1761641062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OfertasManagerCRM_PraVoce/<LoadLoadeds>c__IteratorF::MoveNext()
extern "C"  bool U3CLoadLoadedsU3Ec__IteratorF_MoveNext_m1542531599 (U3CLoadLoadedsU3Ec__IteratorF_t1761641062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_PraVoce/<LoadLoadeds>c__IteratorF::Dispose()
extern "C"  void U3CLoadLoadedsU3Ec__IteratorF_Dispose_m1213944514 (U3CLoadLoadedsU3Ec__IteratorF_t1761641062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_PraVoce/<LoadLoadeds>c__IteratorF::Reset()
extern "C"  void U3CLoadLoadedsU3Ec__IteratorF_Reset_m946122354 (U3CLoadLoadedsU3Ec__IteratorF_t1761641062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
