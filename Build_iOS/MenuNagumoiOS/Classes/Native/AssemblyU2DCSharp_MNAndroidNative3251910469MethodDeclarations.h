﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MNAndroidNative
struct MNAndroidNative_t3251910469;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MNAndroidNative::.ctor()
extern "C"  void MNAndroidNative__ctor_m2112679366 (MNAndroidNative_t3251910469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNAndroidNative::CallActivityFunction(System.String,System.Object[])
extern "C"  void MNAndroidNative_CallActivityFunction_m1801903691 (Il2CppObject * __this /* static, unused */, String_t* ___methodName0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNAndroidNative::showDialog(System.String,System.String)
extern "C"  void MNAndroidNative_showDialog_m893268891 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNAndroidNative::showDialog(System.String,System.String,System.String,System.String)
extern "C"  void MNAndroidNative_showDialog_m2874950803 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, String_t* ___yes2, String_t* ___no3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNAndroidNative::showMessage(System.String,System.String)
extern "C"  void MNAndroidNative_showMessage_m2531203696 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNAndroidNative::showMessage(System.String,System.String,System.String)
extern "C"  void MNAndroidNative_showMessage_m1306607276 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, String_t* ___ok2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNAndroidNative::showRateDialog(System.String,System.String,System.String,System.String,System.String)
extern "C"  void MNAndroidNative_showRateDialog_m1543089039 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, String_t* ___yes2, String_t* ___laiter3, String_t* ___no4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNAndroidNative::ShowPreloader(System.String,System.String)
extern "C"  void MNAndroidNative_ShowPreloader_m747073025 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNAndroidNative::HidePreloader()
extern "C"  void MNAndroidNative_HidePreloader_m350676024 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNAndroidNative::RedirectStoreRatingPage(System.String)
extern "C"  void MNAndroidNative_RedirectStoreRatingPage_m1463371565 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
