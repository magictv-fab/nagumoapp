﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// CaptureAndSave
struct CaptureAndSave_t700313070;

#include "mscorlib_System_Object4170816371.h"
#include "CaptureAndSaveLib_ImageType1125820181.h"
#include "CaptureAndSaveLib_WatermarkAlignment1574127103.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CaptureAndSave/<SaveToAlbum>c__Iterator0
struct  U3CSaveToAlbumU3Ec__Iterator0_t371193057  : public Il2CppObject
{
public:
	// System.Int32 CaptureAndSave/<SaveToAlbum>c__Iterator0::x
	int32_t ___x_0;
	// System.Int32 CaptureAndSave/<SaveToAlbum>c__Iterator0::y
	int32_t ___y_1;
	// System.Int32 CaptureAndSave/<SaveToAlbum>c__Iterator0::width
	int32_t ___width_2;
	// System.Int32 CaptureAndSave/<SaveToAlbum>c__Iterator0::height
	int32_t ___height_3;
	// ImageType CaptureAndSave/<SaveToAlbum>c__Iterator0::imgType
	int32_t ___imgType_4;
	// UnityEngine.Texture2D CaptureAndSave/<SaveToAlbum>c__Iterator0::<t>__0
	Texture2D_t3884108195 * ___U3CtU3E__0_5;
	// System.String CaptureAndSave/<SaveToAlbum>c__Iterator0::path
	String_t* ___path_6;
	// System.Boolean CaptureAndSave/<SaveToAlbum>c__Iterator0::saveToGallery
	bool ___saveToGallery_7;
	// UnityEngine.Texture2D CaptureAndSave/<SaveToAlbum>c__Iterator0::watermark
	Texture2D_t3884108195 * ___watermark_8;
	// WatermarkAlignment CaptureAndSave/<SaveToAlbum>c__Iterator0::align
	int32_t ___align_9;
	// System.Int32 CaptureAndSave/<SaveToAlbum>c__Iterator0::$PC
	int32_t ___U24PC_10;
	// System.Object CaptureAndSave/<SaveToAlbum>c__Iterator0::$current
	Il2CppObject * ___U24current_11;
	// System.Int32 CaptureAndSave/<SaveToAlbum>c__Iterator0::<$>x
	int32_t ___U3CU24U3Ex_12;
	// System.Int32 CaptureAndSave/<SaveToAlbum>c__Iterator0::<$>y
	int32_t ___U3CU24U3Ey_13;
	// System.Int32 CaptureAndSave/<SaveToAlbum>c__Iterator0::<$>width
	int32_t ___U3CU24U3Ewidth_14;
	// System.Int32 CaptureAndSave/<SaveToAlbum>c__Iterator0::<$>height
	int32_t ___U3CU24U3Eheight_15;
	// ImageType CaptureAndSave/<SaveToAlbum>c__Iterator0::<$>imgType
	int32_t ___U3CU24U3EimgType_16;
	// System.String CaptureAndSave/<SaveToAlbum>c__Iterator0::<$>path
	String_t* ___U3CU24U3Epath_17;
	// System.Boolean CaptureAndSave/<SaveToAlbum>c__Iterator0::<$>saveToGallery
	bool ___U3CU24U3EsaveToGallery_18;
	// UnityEngine.Texture2D CaptureAndSave/<SaveToAlbum>c__Iterator0::<$>watermark
	Texture2D_t3884108195 * ___U3CU24U3Ewatermark_19;
	// WatermarkAlignment CaptureAndSave/<SaveToAlbum>c__Iterator0::<$>align
	int32_t ___U3CU24U3Ealign_20;
	// CaptureAndSave CaptureAndSave/<SaveToAlbum>c__Iterator0::<>f__this
	CaptureAndSave_t700313070 * ___U3CU3Ef__this_21;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(U3CSaveToAlbumU3Ec__Iterator0_t371193057, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(U3CSaveToAlbumU3Ec__Iterator0_t371193057, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_width_2() { return static_cast<int32_t>(offsetof(U3CSaveToAlbumU3Ec__Iterator0_t371193057, ___width_2)); }
	inline int32_t get_width_2() const { return ___width_2; }
	inline int32_t* get_address_of_width_2() { return &___width_2; }
	inline void set_width_2(int32_t value)
	{
		___width_2 = value;
	}

	inline static int32_t get_offset_of_height_3() { return static_cast<int32_t>(offsetof(U3CSaveToAlbumU3Ec__Iterator0_t371193057, ___height_3)); }
	inline int32_t get_height_3() const { return ___height_3; }
	inline int32_t* get_address_of_height_3() { return &___height_3; }
	inline void set_height_3(int32_t value)
	{
		___height_3 = value;
	}

	inline static int32_t get_offset_of_imgType_4() { return static_cast<int32_t>(offsetof(U3CSaveToAlbumU3Ec__Iterator0_t371193057, ___imgType_4)); }
	inline int32_t get_imgType_4() const { return ___imgType_4; }
	inline int32_t* get_address_of_imgType_4() { return &___imgType_4; }
	inline void set_imgType_4(int32_t value)
	{
		___imgType_4 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E__0_5() { return static_cast<int32_t>(offsetof(U3CSaveToAlbumU3Ec__Iterator0_t371193057, ___U3CtU3E__0_5)); }
	inline Texture2D_t3884108195 * get_U3CtU3E__0_5() const { return ___U3CtU3E__0_5; }
	inline Texture2D_t3884108195 ** get_address_of_U3CtU3E__0_5() { return &___U3CtU3E__0_5; }
	inline void set_U3CtU3E__0_5(Texture2D_t3884108195 * value)
	{
		___U3CtU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtU3E__0_5, value);
	}

	inline static int32_t get_offset_of_path_6() { return static_cast<int32_t>(offsetof(U3CSaveToAlbumU3Ec__Iterator0_t371193057, ___path_6)); }
	inline String_t* get_path_6() const { return ___path_6; }
	inline String_t** get_address_of_path_6() { return &___path_6; }
	inline void set_path_6(String_t* value)
	{
		___path_6 = value;
		Il2CppCodeGenWriteBarrier(&___path_6, value);
	}

	inline static int32_t get_offset_of_saveToGallery_7() { return static_cast<int32_t>(offsetof(U3CSaveToAlbumU3Ec__Iterator0_t371193057, ___saveToGallery_7)); }
	inline bool get_saveToGallery_7() const { return ___saveToGallery_7; }
	inline bool* get_address_of_saveToGallery_7() { return &___saveToGallery_7; }
	inline void set_saveToGallery_7(bool value)
	{
		___saveToGallery_7 = value;
	}

	inline static int32_t get_offset_of_watermark_8() { return static_cast<int32_t>(offsetof(U3CSaveToAlbumU3Ec__Iterator0_t371193057, ___watermark_8)); }
	inline Texture2D_t3884108195 * get_watermark_8() const { return ___watermark_8; }
	inline Texture2D_t3884108195 ** get_address_of_watermark_8() { return &___watermark_8; }
	inline void set_watermark_8(Texture2D_t3884108195 * value)
	{
		___watermark_8 = value;
		Il2CppCodeGenWriteBarrier(&___watermark_8, value);
	}

	inline static int32_t get_offset_of_align_9() { return static_cast<int32_t>(offsetof(U3CSaveToAlbumU3Ec__Iterator0_t371193057, ___align_9)); }
	inline int32_t get_align_9() const { return ___align_9; }
	inline int32_t* get_address_of_align_9() { return &___align_9; }
	inline void set_align_9(int32_t value)
	{
		___align_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CSaveToAlbumU3Ec__Iterator0_t371193057, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}

	inline static int32_t get_offset_of_U24current_11() { return static_cast<int32_t>(offsetof(U3CSaveToAlbumU3Ec__Iterator0_t371193057, ___U24current_11)); }
	inline Il2CppObject * get_U24current_11() const { return ___U24current_11; }
	inline Il2CppObject ** get_address_of_U24current_11() { return &___U24current_11; }
	inline void set_U24current_11(Il2CppObject * value)
	{
		___U24current_11 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_11, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ex_12() { return static_cast<int32_t>(offsetof(U3CSaveToAlbumU3Ec__Iterator0_t371193057, ___U3CU24U3Ex_12)); }
	inline int32_t get_U3CU24U3Ex_12() const { return ___U3CU24U3Ex_12; }
	inline int32_t* get_address_of_U3CU24U3Ex_12() { return &___U3CU24U3Ex_12; }
	inline void set_U3CU24U3Ex_12(int32_t value)
	{
		___U3CU24U3Ex_12 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Ey_13() { return static_cast<int32_t>(offsetof(U3CSaveToAlbumU3Ec__Iterator0_t371193057, ___U3CU24U3Ey_13)); }
	inline int32_t get_U3CU24U3Ey_13() const { return ___U3CU24U3Ey_13; }
	inline int32_t* get_address_of_U3CU24U3Ey_13() { return &___U3CU24U3Ey_13; }
	inline void set_U3CU24U3Ey_13(int32_t value)
	{
		___U3CU24U3Ey_13 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Ewidth_14() { return static_cast<int32_t>(offsetof(U3CSaveToAlbumU3Ec__Iterator0_t371193057, ___U3CU24U3Ewidth_14)); }
	inline int32_t get_U3CU24U3Ewidth_14() const { return ___U3CU24U3Ewidth_14; }
	inline int32_t* get_address_of_U3CU24U3Ewidth_14() { return &___U3CU24U3Ewidth_14; }
	inline void set_U3CU24U3Ewidth_14(int32_t value)
	{
		___U3CU24U3Ewidth_14 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Eheight_15() { return static_cast<int32_t>(offsetof(U3CSaveToAlbumU3Ec__Iterator0_t371193057, ___U3CU24U3Eheight_15)); }
	inline int32_t get_U3CU24U3Eheight_15() const { return ___U3CU24U3Eheight_15; }
	inline int32_t* get_address_of_U3CU24U3Eheight_15() { return &___U3CU24U3Eheight_15; }
	inline void set_U3CU24U3Eheight_15(int32_t value)
	{
		___U3CU24U3Eheight_15 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3EimgType_16() { return static_cast<int32_t>(offsetof(U3CSaveToAlbumU3Ec__Iterator0_t371193057, ___U3CU24U3EimgType_16)); }
	inline int32_t get_U3CU24U3EimgType_16() const { return ___U3CU24U3EimgType_16; }
	inline int32_t* get_address_of_U3CU24U3EimgType_16() { return &___U3CU24U3EimgType_16; }
	inline void set_U3CU24U3EimgType_16(int32_t value)
	{
		___U3CU24U3EimgType_16 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Epath_17() { return static_cast<int32_t>(offsetof(U3CSaveToAlbumU3Ec__Iterator0_t371193057, ___U3CU24U3Epath_17)); }
	inline String_t* get_U3CU24U3Epath_17() const { return ___U3CU24U3Epath_17; }
	inline String_t** get_address_of_U3CU24U3Epath_17() { return &___U3CU24U3Epath_17; }
	inline void set_U3CU24U3Epath_17(String_t* value)
	{
		___U3CU24U3Epath_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Epath_17, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EsaveToGallery_18() { return static_cast<int32_t>(offsetof(U3CSaveToAlbumU3Ec__Iterator0_t371193057, ___U3CU24U3EsaveToGallery_18)); }
	inline bool get_U3CU24U3EsaveToGallery_18() const { return ___U3CU24U3EsaveToGallery_18; }
	inline bool* get_address_of_U3CU24U3EsaveToGallery_18() { return &___U3CU24U3EsaveToGallery_18; }
	inline void set_U3CU24U3EsaveToGallery_18(bool value)
	{
		___U3CU24U3EsaveToGallery_18 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Ewatermark_19() { return static_cast<int32_t>(offsetof(U3CSaveToAlbumU3Ec__Iterator0_t371193057, ___U3CU24U3Ewatermark_19)); }
	inline Texture2D_t3884108195 * get_U3CU24U3Ewatermark_19() const { return ___U3CU24U3Ewatermark_19; }
	inline Texture2D_t3884108195 ** get_address_of_U3CU24U3Ewatermark_19() { return &___U3CU24U3Ewatermark_19; }
	inline void set_U3CU24U3Ewatermark_19(Texture2D_t3884108195 * value)
	{
		___U3CU24U3Ewatermark_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ewatermark_19, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ealign_20() { return static_cast<int32_t>(offsetof(U3CSaveToAlbumU3Ec__Iterator0_t371193057, ___U3CU24U3Ealign_20)); }
	inline int32_t get_U3CU24U3Ealign_20() const { return ___U3CU24U3Ealign_20; }
	inline int32_t* get_address_of_U3CU24U3Ealign_20() { return &___U3CU24U3Ealign_20; }
	inline void set_U3CU24U3Ealign_20(int32_t value)
	{
		___U3CU24U3Ealign_20 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_21() { return static_cast<int32_t>(offsetof(U3CSaveToAlbumU3Ec__Iterator0_t371193057, ___U3CU3Ef__this_21)); }
	inline CaptureAndSave_t700313070 * get_U3CU3Ef__this_21() const { return ___U3CU3Ef__this_21; }
	inline CaptureAndSave_t700313070 ** get_address_of_U3CU3Ef__this_21() { return &___U3CU3Ef__this_21; }
	inline void set_U3CU3Ef__this_21(CaptureAndSave_t700313070 * value)
	{
		___U3CU3Ef__this_21 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
