﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Remoting_Proxies_RemotingP3846099589.h"
#include "mscorlib_System_Runtime_Remoting_Proxies_ProxyAttr4283279896.h"
#include "mscorlib_System_Runtime_Remoting_Services_Tracking1684096907.h"
#include "mscorlib_System_Runtime_Serialization_FormatterCon3096250402.h"
#include "mscorlib_System_Runtime_Serialization_FormatterServi11855534.h"
#include "mscorlib_System_Runtime_Serialization_ObjectIDGener228858559.h"
#include "mscorlib_System_Runtime_Serialization_ObjectIDGenerat5241131.h"
#include "mscorlib_System_Runtime_Serialization_ObjectManage2228689236.h"
#include "mscorlib_System_Runtime_Serialization_BaseFixupReco293418230.h"
#include "mscorlib_System_Runtime_Serialization_ArrayFixupRe4157922996.h"
#include "mscorlib_System_Runtime_Serialization_MultiArrayFix403281415.h"
#include "mscorlib_System_Runtime_Serialization_FixupRecord3910479623.h"
#include "mscorlib_System_Runtime_Serialization_DelayedFixup4036501323.h"
#include "mscorlib_System_Runtime_Serialization_ObjectRecord2197013678.h"
#include "mscorlib_System_Runtime_Serialization_ObjectRecord402472668.h"
#include "mscorlib_System_Runtime_Serialization_OnDeserializ4217150558.h"
#include "mscorlib_System_Runtime_Serialization_OnDeserializ1050530859.h"
#include "mscorlib_System_Runtime_Serialization_OnSerialized2575355391.h"
#include "mscorlib_System_Runtime_Serialization_OnSerializin1694488234.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2137423328.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2310767978.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio1474775431.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio1918496398.h"
#include "mscorlib_System_Runtime_Serialization_Serialization541289707.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2904781384.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2198782602.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio3814713513.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2774397435.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_F3005881063.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_For38666771.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_T1936244052.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1638665103.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B4003669736.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3504091841.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B2420703430.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3415425644.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3874862518.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1447641708.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3131639422.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3015042459.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Bi715263678.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Bi777805349.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3077458456.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B2491933764.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B2041888159.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B3287044654.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B2286703290.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B2456665247.h"
#include "mscorlib_System_Security_AllowPartiallyTrustedCall3409392348.h"
#include "mscorlib_System_Security_CodeAccessPermission2127220.h"
#include "mscorlib_System_Security_HostSecurityManager514696603.h"
#include "mscorlib_System_Security_HostSecurityManagerOption2658022701.h"
#include "mscorlib_System_Security_NamedPermissionSet3755049006.h"
#include "mscorlib_System_Security_PermissionBuilder2532014882.h"
#include "mscorlib_System_Security_PermissionSet1199249641.h"
#include "mscorlib_System_Security_PolicyLevelType2708160610.h"
#include "mscorlib_System_Security_SecurityContext444694245.h"
#include "mscorlib_System_Security_SecurityCriticalAttribute3643889075.h"
#include "mscorlib_System_Security_SecurityCriticalScope3655139947.h"
#include "mscorlib_System_Security_SecurityElement2125293618.h"
#include "mscorlib_System_Security_SecurityElement_SecurityA1645524282.h"
#include "mscorlib_System_Security_RuntimeDeclSecurityEntry3185381156.h"
#include "mscorlib_System_Security_RuntimeSecurityFrame2355870761.h"
#include "mscorlib_System_Security_SecurityFrame1903782627.h"
#include "mscorlib_System_Security_SecurityException3044716869.h"
#include "mscorlib_System_Security_RuntimeDeclSecurityAction2571204719.h"
#include "mscorlib_System_Security_SecurityManager328423651.h"
#include "mscorlib_System_Security_SecuritySafeCriticalAttri2604671270.h"
#include "mscorlib_System_Security_SecurityZone3089400096.h"
#include "mscorlib_System_Security_SuppressUnmanagedCodeSecu2984551154.h"
#include "mscorlib_System_Security_UnverifiableCodeAttribute2154663753.h"
#include "mscorlib_System_Security_XmlSyntaxException2231940457.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricAl1241690687.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricKey684750218.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricSi2683552211.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricSi1609524596.h"
#include "mscorlib_System_Security_Cryptography_Base64Consta1709966438.h"
#include "mscorlib_System_Security_Cryptography_CipherMode2662187258.h"
#include "mscorlib_System_Security_Cryptography_CryptoConfig1703322959.h"
#include "mscorlib_System_Security_Cryptography_CryptoConfig2385149168.h"
#include "mscorlib_System_Security_Cryptography_Cryptographi3687180084.h"
#include "mscorlib_System_Security_Cryptography_Cryptographic363661542.h"
#include "mscorlib_System_Security_Cryptography_CryptoStream2166124941.h"
#include "mscorlib_System_Security_Cryptography_CryptoStream3444626768.h"
#include "mscorlib_System_Security_Cryptography_CspParameters309313264.h"
#include "mscorlib_System_Security_Cryptography_CspProviderF3394239842.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize700 = { sizeof (RemotingProxy_t3846099589), -1, sizeof(RemotingProxy_t3846099589_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable700[5] = 
{
	RemotingProxy_t3846099589_StaticFields::get_offset_of__cache_GetTypeMethod_5(),
	RemotingProxy_t3846099589_StaticFields::get_offset_of__cache_GetHashCodeMethod_6(),
	RemotingProxy_t3846099589::get_offset_of__sink_7(),
	RemotingProxy_t3846099589::get_offset_of__hasEnvoySink_8(),
	RemotingProxy_t3846099589::get_offset_of__ctorCall_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize701 = { sizeof (ProxyAttribute_t4283279896), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize702 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize703 = { sizeof (TrackingServices_t1684096907), -1, sizeof(TrackingServices_t1684096907_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable703[1] = 
{
	TrackingServices_t1684096907_StaticFields::get_offset_of__handlers_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize704 = { sizeof (FormatterConverter_t3096250402), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize705 = { sizeof (FormatterServices_t11855534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize706 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize707 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize708 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize709 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize710 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize711 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize712 = { sizeof (ObjectIDGenerator_t228858559), -1, sizeof(ObjectIDGenerator_t228858559_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable712[3] = 
{
	ObjectIDGenerator_t228858559::get_offset_of_table_0(),
	ObjectIDGenerator_t228858559::get_offset_of_current_1(),
	ObjectIDGenerator_t228858559_StaticFields::get_offset_of_comparer_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize713 = { sizeof (InstanceComparer_t5241131), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize714 = { sizeof (ObjectManager_t2228689236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable714[9] = 
{
	ObjectManager_t2228689236::get_offset_of__objectRecordChain_0(),
	ObjectManager_t2228689236::get_offset_of__lastObjectRecord_1(),
	ObjectManager_t2228689236::get_offset_of__deserializedRecords_2(),
	ObjectManager_t2228689236::get_offset_of__onDeserializedCallbackRecords_3(),
	ObjectManager_t2228689236::get_offset_of__objectRecords_4(),
	ObjectManager_t2228689236::get_offset_of__finalFixup_5(),
	ObjectManager_t2228689236::get_offset_of__selector_6(),
	ObjectManager_t2228689236::get_offset_of__context_7(),
	ObjectManager_t2228689236::get_offset_of__registeredObjectsCount_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize715 = { sizeof (BaseFixupRecord_t293418230), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable715[4] = 
{
	BaseFixupRecord_t293418230::get_offset_of_ObjectToBeFixed_0(),
	BaseFixupRecord_t293418230::get_offset_of_ObjectRequired_1(),
	BaseFixupRecord_t293418230::get_offset_of_NextSameContainer_2(),
	BaseFixupRecord_t293418230::get_offset_of_NextSameRequired_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize716 = { sizeof (ArrayFixupRecord_t4157922996), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable716[1] = 
{
	ArrayFixupRecord_t4157922996::get_offset_of__index_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize717 = { sizeof (MultiArrayFixupRecord_t403281415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable717[1] = 
{
	MultiArrayFixupRecord_t403281415::get_offset_of__indices_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize718 = { sizeof (FixupRecord_t3910479623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable718[1] = 
{
	FixupRecord_t3910479623::get_offset_of__member_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize719 = { sizeof (DelayedFixupRecord_t4036501323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable719[1] = 
{
	DelayedFixupRecord_t4036501323::get_offset_of__memberName_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize720 = { sizeof (ObjectRecordStatus_t2197013678)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable720[5] = 
{
	ObjectRecordStatus_t2197013678::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize721 = { sizeof (ObjectRecord_t402472668), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable721[13] = 
{
	ObjectRecord_t402472668::get_offset_of_Status_0(),
	ObjectRecord_t402472668::get_offset_of_OriginalObject_1(),
	ObjectRecord_t402472668::get_offset_of_ObjectInstance_2(),
	ObjectRecord_t402472668::get_offset_of_ObjectID_3(),
	ObjectRecord_t402472668::get_offset_of_Info_4(),
	ObjectRecord_t402472668::get_offset_of_IdOfContainingObj_5(),
	ObjectRecord_t402472668::get_offset_of_Surrogate_6(),
	ObjectRecord_t402472668::get_offset_of_SurrogateSelector_7(),
	ObjectRecord_t402472668::get_offset_of_Member_8(),
	ObjectRecord_t402472668::get_offset_of_ArrayIndex_9(),
	ObjectRecord_t402472668::get_offset_of_FixupChainAsContainer_10(),
	ObjectRecord_t402472668::get_offset_of_FixupChainAsRequired_11(),
	ObjectRecord_t402472668::get_offset_of_Next_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize722 = { sizeof (OnDeserializedAttribute_t4217150558), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize723 = { sizeof (OnDeserializingAttribute_t1050530859), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize724 = { sizeof (OnSerializedAttribute_t2575355391), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize725 = { sizeof (OnSerializingAttribute_t1694488234), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize726 = { sizeof (SerializationBinder_t2137423328), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize727 = { sizeof (SerializationCallbacks_t2310767978), -1, sizeof(SerializationCallbacks_t2310767978_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable727[6] = 
{
	SerializationCallbacks_t2310767978::get_offset_of_onSerializingList_0(),
	SerializationCallbacks_t2310767978::get_offset_of_onSerializedList_1(),
	SerializationCallbacks_t2310767978::get_offset_of_onDeserializingList_2(),
	SerializationCallbacks_t2310767978::get_offset_of_onDeserializedList_3(),
	SerializationCallbacks_t2310767978_StaticFields::get_offset_of_cache_4(),
	SerializationCallbacks_t2310767978_StaticFields::get_offset_of_cache_lock_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize728 = { sizeof (CallbackHandler_t1474775431), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize729 = { sizeof (SerializationEntry_t1918496398)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable729[3] = 
{
	SerializationEntry_t1918496398::get_offset_of_name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SerializationEntry_t1918496398::get_offset_of_objectType_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SerializationEntry_t1918496398::get_offset_of_value_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize730 = { sizeof (SerializationException_t541289707), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize731 = { sizeof (SerializationInfo_t2185721892), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable731[5] = 
{
	SerializationInfo_t2185721892::get_offset_of_serialized_0(),
	SerializationInfo_t2185721892::get_offset_of_values_1(),
	SerializationInfo_t2185721892::get_offset_of_assemblyName_2(),
	SerializationInfo_t2185721892::get_offset_of_fullTypeName_3(),
	SerializationInfo_t2185721892::get_offset_of_converter_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize732 = { sizeof (SerializationInfoEnumerator_t2904781384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable732[1] = 
{
	SerializationInfoEnumerator_t2904781384::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize733 = { sizeof (SerializationObjectManager_t2198782602), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable733[3] = 
{
	SerializationObjectManager_t2198782602::get_offset_of_context_0(),
	SerializationObjectManager_t2198782602::get_offset_of_seen_1(),
	SerializationObjectManager_t2198782602::get_offset_of_callbacks_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize734 = { sizeof (U3CRegisterObjectU3Ec__AnonStorey2_t3814713513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable734[2] = 
{
	U3CRegisterObjectU3Ec__AnonStorey2_t3814713513::get_offset_of_sc_0(),
	U3CRegisterObjectU3Ec__AnonStorey2_t3814713513::get_offset_of_obj_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize735 = { sizeof (StreamingContext_t2761351129)+ sizeof (Il2CppObject), sizeof(StreamingContext_t2761351129_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable735[2] = 
{
	StreamingContext_t2761351129::get_offset_of_state_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	StreamingContext_t2761351129::get_offset_of_additional_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize736 = { sizeof (StreamingContextStates_t2774397435)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable736[10] = 
{
	StreamingContextStates_t2774397435::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize737 = { sizeof (FormatterAssemblyStyle_t3005881063)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable737[3] = 
{
	FormatterAssemblyStyle_t3005881063::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize738 = { sizeof (FormatterTypeStyle_t38666771)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable738[4] = 
{
	FormatterTypeStyle_t38666771::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize739 = { sizeof (TypeFilterLevel_t1936244052)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable739[3] = 
{
	TypeFilterLevel_t1936244052::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize740 = { sizeof (BinaryFormatter_t1638665103), -1, sizeof(BinaryFormatter_t1638665103_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable740[7] = 
{
	BinaryFormatter_t1638665103::get_offset_of_assembly_format_0(),
	BinaryFormatter_t1638665103::get_offset_of_binder_1(),
	BinaryFormatter_t1638665103::get_offset_of_context_2(),
	BinaryFormatter_t1638665103::get_offset_of_surrogate_selector_3(),
	BinaryFormatter_t1638665103::get_offset_of_type_format_4(),
	BinaryFormatter_t1638665103::get_offset_of_filter_level_5(),
	BinaryFormatter_t1638665103_StaticFields::get_offset_of_U3CDefaultSurrogateSelectorU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize741 = { sizeof (BinaryCommon_t4003669736), -1, sizeof(BinaryCommon_t4003669736_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable741[4] = 
{
	BinaryCommon_t4003669736_StaticFields::get_offset_of_BinaryHeader_0(),
	BinaryCommon_t4003669736_StaticFields::get_offset_of__typeCodesToType_1(),
	BinaryCommon_t4003669736_StaticFields::get_offset_of__typeCodeMap_2(),
	BinaryCommon_t4003669736_StaticFields::get_offset_of_UseReflectionSerialization_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize742 = { sizeof (BinaryElement_t3504091841)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable742[24] = 
{
	BinaryElement_t3504091841::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize743 = { sizeof (TypeTag_t2420703430)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable743[9] = 
{
	TypeTag_t2420703430::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize744 = { sizeof (MethodFlags_t3415425644)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable744[11] = 
{
	MethodFlags_t3415425644::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize745 = { sizeof (ReturnTypeTag_t3874862518)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable745[5] = 
{
	ReturnTypeTag_t3874862518::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize746 = { sizeof (CodeGenerator_t1447641708), -1, sizeof(CodeGenerator_t1447641708_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable746[2] = 
{
	CodeGenerator_t1447641708_StaticFields::get_offset_of_monitor_0(),
	CodeGenerator_t1447641708_StaticFields::get_offset_of__module_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize747 = { sizeof (ObjectReader_t3131639422), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable747[12] = 
{
	ObjectReader_t3131639422::get_offset_of__surrogateSelector_0(),
	ObjectReader_t3131639422::get_offset_of__context_1(),
	ObjectReader_t3131639422::get_offset_of__binder_2(),
	ObjectReader_t3131639422::get_offset_of__filterLevel_3(),
	ObjectReader_t3131639422::get_offset_of__manager_4(),
	ObjectReader_t3131639422::get_offset_of__registeredAssemblies_5(),
	ObjectReader_t3131639422::get_offset_of__typeMetadataCache_6(),
	ObjectReader_t3131639422::get_offset_of__lastObject_7(),
	ObjectReader_t3131639422::get_offset_of__lastObjectID_8(),
	ObjectReader_t3131639422::get_offset_of__rootObjectID_9(),
	ObjectReader_t3131639422::get_offset_of_arrayBuffer_10(),
	ObjectReader_t3131639422::get_offset_of_ArrayBufferLength_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize748 = { sizeof (TypeMetadata_t3015042459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable748[6] = 
{
	TypeMetadata_t3015042459::get_offset_of_Type_0(),
	TypeMetadata_t3015042459::get_offset_of_MemberTypes_1(),
	TypeMetadata_t3015042459::get_offset_of_MemberNames_2(),
	TypeMetadata_t3015042459::get_offset_of_MemberInfos_3(),
	TypeMetadata_t3015042459::get_offset_of_FieldCount_4(),
	TypeMetadata_t3015042459::get_offset_of_NeedsSerializationInfo_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize749 = { sizeof (ArrayNullFiller_t715263678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable749[1] = 
{
	ArrayNullFiller_t715263678::get_offset_of_NullCount_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize750 = { sizeof (TypeMetadata_t777805349), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable750[2] = 
{
	TypeMetadata_t777805349::get_offset_of_TypeAssemblyName_0(),
	TypeMetadata_t777805349::get_offset_of_InstanceTypeName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize751 = { sizeof (ClrTypeMetadata_t3077458456), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable751[1] = 
{
	ClrTypeMetadata_t3077458456::get_offset_of_InstanceType_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize752 = { sizeof (SerializableTypeMetadata_t2491933764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable752[2] = 
{
	SerializableTypeMetadata_t2491933764::get_offset_of_types_2(),
	SerializableTypeMetadata_t2491933764::get_offset_of_names_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize753 = { sizeof (MemberTypeMetadata_t2041888159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable753[1] = 
{
	MemberTypeMetadata_t2041888159::get_offset_of_members_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize754 = { sizeof (ObjectWriter_t3287044654), -1, sizeof(ObjectWriter_t3287044654_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable754[14] = 
{
	ObjectWriter_t3287044654::get_offset_of__idGenerator_0(),
	ObjectWriter_t3287044654::get_offset_of__cachedMetadata_1(),
	ObjectWriter_t3287044654::get_offset_of__pendingObjects_2(),
	ObjectWriter_t3287044654::get_offset_of__assemblyCache_3(),
	ObjectWriter_t3287044654_StaticFields::get_offset_of__cachedTypes_4(),
	ObjectWriter_t3287044654_StaticFields::get_offset_of_CorlibAssembly_5(),
	ObjectWriter_t3287044654_StaticFields::get_offset_of_CorlibAssemblyName_6(),
	ObjectWriter_t3287044654::get_offset_of__surrogateSelector_7(),
	ObjectWriter_t3287044654::get_offset_of__context_8(),
	ObjectWriter_t3287044654::get_offset_of__assemblyFormat_9(),
	ObjectWriter_t3287044654::get_offset_of__typeFormat_10(),
	ObjectWriter_t3287044654::get_offset_of_arrayBuffer_11(),
	ObjectWriter_t3287044654::get_offset_of_ArrayBufferLength_12(),
	ObjectWriter_t3287044654::get_offset_of__manager_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize755 = { sizeof (MetadataReference_t2286703290), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable755[2] = 
{
	MetadataReference_t2286703290::get_offset_of_Metadata_0(),
	MetadataReference_t2286703290::get_offset_of_ObjectID_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize756 = { sizeof (MessageFormatter_t2456665247), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize757 = { sizeof (AllowPartiallyTrustedCallersAttribute_t3409392348), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize758 = { sizeof (CodeAccessPermission_t2127220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize759 = { sizeof (HostSecurityManager_t514696603), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize760 = { sizeof (HostSecurityManagerOptions_t2658022701)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable760[8] = 
{
	HostSecurityManagerOptions_t2658022701::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize761 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize762 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize763 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize764 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize765 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize766 = { sizeof (NamedPermissionSet_t3755049006), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable766[2] = 
{
	NamedPermissionSet_t3755049006::get_offset_of_name_12(),
	NamedPermissionSet_t3755049006::get_offset_of_description_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize767 = { sizeof (PermissionBuilder_t2532014882), -1, sizeof(PermissionBuilder_t2532014882_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable767[1] = 
{
	PermissionBuilder_t2532014882_StaticFields::get_offset_of_psNone_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize768 = { sizeof (PermissionSet_t1199249641), -1, sizeof(PermissionSet_t1199249641_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable768[12] = 
{
	0,
	0,
	PermissionSet_t1199249641_StaticFields::get_offset_of_psUnrestricted_2(),
	PermissionSet_t1199249641::get_offset_of_state_3(),
	PermissionSet_t1199249641::get_offset_of_list_4(),
	PermissionSet_t1199249641::get_offset_of__policyLevel_5(),
	PermissionSet_t1199249641::get_offset_of__declsec_6(),
	PermissionSet_t1199249641::get_offset_of__readOnly_7(),
	PermissionSet_t1199249641::get_offset_of__ignored_8(),
	PermissionSet_t1199249641_StaticFields::get_offset_of_action_9(),
	PermissionSet_t1199249641_StaticFields::get_offset_of_U3CU3Ef__switchU24map2B_10(),
	PermissionSet_t1199249641_StaticFields::get_offset_of_U3CU3Ef__switchU24map2C_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize769 = { sizeof (PolicyLevelType_t2708160610)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable769[5] = 
{
	PolicyLevelType_t2708160610::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize770 = { sizeof (SecurityContext_t444694245), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable770[4] = 
{
	SecurityContext_t444694245::get_offset_of__capture_0(),
	SecurityContext_t444694245::get_offset_of__winid_1(),
	SecurityContext_t444694245::get_offset_of__stack_2(),
	SecurityContext_t444694245::get_offset_of__suppressFlow_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize771 = { sizeof (SecurityCriticalAttribute_t3643889075), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable771[1] = 
{
	SecurityCriticalAttribute_t3643889075::get_offset_of__scope_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize772 = { sizeof (SecurityCriticalScope_t3655139947)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable772[3] = 
{
	SecurityCriticalScope_t3655139947::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize773 = { sizeof (SecurityElement_t2125293618), -1, sizeof(SecurityElement_t2125293618_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable773[9] = 
{
	SecurityElement_t2125293618::get_offset_of_text_0(),
	SecurityElement_t2125293618::get_offset_of_tag_1(),
	SecurityElement_t2125293618::get_offset_of_attributes_2(),
	SecurityElement_t2125293618::get_offset_of_children_3(),
	SecurityElement_t2125293618_StaticFields::get_offset_of_invalid_tag_chars_4(),
	SecurityElement_t2125293618_StaticFields::get_offset_of_invalid_text_chars_5(),
	SecurityElement_t2125293618_StaticFields::get_offset_of_invalid_attr_name_chars_6(),
	SecurityElement_t2125293618_StaticFields::get_offset_of_invalid_attr_value_chars_7(),
	SecurityElement_t2125293618_StaticFields::get_offset_of_invalid_chars_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize774 = { sizeof (SecurityAttribute_t1645524282), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable774[2] = 
{
	SecurityAttribute_t1645524282::get_offset_of__name_0(),
	SecurityAttribute_t1645524282::get_offset_of__value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize775 = { sizeof (RuntimeDeclSecurityEntry_t3185381156)+ sizeof (Il2CppObject), sizeof(RuntimeDeclSecurityEntry_t3185381156_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable775[3] = 
{
	RuntimeDeclSecurityEntry_t3185381156::get_offset_of_blob_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RuntimeDeclSecurityEntry_t3185381156::get_offset_of_size_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RuntimeDeclSecurityEntry_t3185381156::get_offset_of_index_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize776 = { sizeof (RuntimeSecurityFrame_t2355870761), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable776[5] = 
{
	RuntimeSecurityFrame_t2355870761::get_offset_of_domain_0(),
	RuntimeSecurityFrame_t2355870761::get_offset_of_method_1(),
	RuntimeSecurityFrame_t2355870761::get_offset_of_assert_2(),
	RuntimeSecurityFrame_t2355870761::get_offset_of_deny_3(),
	RuntimeSecurityFrame_t2355870761::get_offset_of_permitonly_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize777 = { sizeof (SecurityFrame_t1903782627)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable777[5] = 
{
	SecurityFrame_t1903782627::get_offset_of__domain_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SecurityFrame_t1903782627::get_offset_of__method_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SecurityFrame_t1903782627::get_offset_of__assert_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SecurityFrame_t1903782627::get_offset_of__deny_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SecurityFrame_t1903782627::get_offset_of__permitonly_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize778 = { sizeof (SecurityException_t3044716869), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable778[8] = 
{
	SecurityException_t3044716869::get_offset_of_permissionState_11(),
	SecurityException_t3044716869::get_offset_of_permissionType_12(),
	SecurityException_t3044716869::get_offset_of__granted_13(),
	SecurityException_t3044716869::get_offset_of__refused_14(),
	SecurityException_t3044716869::get_offset_of__demanded_15(),
	SecurityException_t3044716869::get_offset_of__firstperm_16(),
	SecurityException_t3044716869::get_offset_of__method_17(),
	SecurityException_t3044716869::get_offset_of__evidence_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize779 = { sizeof (RuntimeDeclSecurityActions_t2571204719)+ sizeof (Il2CppObject), sizeof(RuntimeDeclSecurityActions_t2571204719_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable779[3] = 
{
	RuntimeDeclSecurityActions_t2571204719::get_offset_of_cas_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RuntimeDeclSecurityActions_t2571204719::get_offset_of_noncas_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RuntimeDeclSecurityActions_t2571204719::get_offset_of_choice_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize780 = { sizeof (SecurityManager_t328423651), -1, sizeof(SecurityManager_t328423651_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable780[5] = 
{
	SecurityManager_t328423651_StaticFields::get_offset_of__lockObject_0(),
	SecurityManager_t328423651_StaticFields::get_offset_of__hierarchy_1(),
	SecurityManager_t328423651_StaticFields::get_offset_of__declsecCache_2(),
	SecurityManager_t328423651_StaticFields::get_offset_of__level_3(),
	SecurityManager_t328423651_StaticFields::get_offset_of__execution_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize781 = { sizeof (SecuritySafeCriticalAttribute_t2604671270), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize782 = { sizeof (SecurityZone_t3089400096)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable782[7] = 
{
	SecurityZone_t3089400096::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize783 = { sizeof (SuppressUnmanagedCodeSecurityAttribute_t2984551154), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize784 = { sizeof (UnverifiableCodeAttribute_t2154663753), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize785 = { sizeof (XmlSyntaxException_t2231940457), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize786 = { sizeof (AsymmetricAlgorithm_t1241690687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable786[2] = 
{
	AsymmetricAlgorithm_t1241690687::get_offset_of_KeySizeValue_0(),
	AsymmetricAlgorithm_t1241690687::get_offset_of_LegalKeySizesValue_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize787 = { sizeof (AsymmetricKeyExchangeFormatter_t684750218), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize788 = { sizeof (AsymmetricSignatureDeformatter_t2683552211), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize789 = { sizeof (AsymmetricSignatureFormatter_t1609524596), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize790 = { sizeof (Base64Constants_t1709966438), -1, sizeof(Base64Constants_t1709966438_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable790[2] = 
{
	Base64Constants_t1709966438_StaticFields::get_offset_of_EncodeTable_0(),
	Base64Constants_t1709966438_StaticFields::get_offset_of_DecodeTable_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize791 = { sizeof (CipherMode_t2662187258)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable791[6] = 
{
	CipherMode_t2662187258::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize792 = { sizeof (CryptoConfig_t1703322959), -1, sizeof(CryptoConfig_t1703322959_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable792[3] = 
{
	CryptoConfig_t1703322959_StaticFields::get_offset_of_lockObject_0(),
	CryptoConfig_t1703322959_StaticFields::get_offset_of_algorithms_1(),
	CryptoConfig_t1703322959_StaticFields::get_offset_of_oid_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize793 = { sizeof (CryptoHandler_t2385149168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable793[5] = 
{
	CryptoHandler_t2385149168::get_offset_of_algorithms_0(),
	CryptoHandler_t2385149168::get_offset_of_oid_1(),
	CryptoHandler_t2385149168::get_offset_of_names_2(),
	CryptoHandler_t2385149168::get_offset_of_classnames_3(),
	CryptoHandler_t2385149168::get_offset_of_level_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize794 = { sizeof (CryptographicException_t3687180084), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize795 = { sizeof (CryptographicUnexpectedOperationException_t363661542), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize796 = { sizeof (CryptoStream_t2166124941), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable796[15] = 
{
	CryptoStream_t2166124941::get_offset_of__stream_2(),
	CryptoStream_t2166124941::get_offset_of__transform_3(),
	CryptoStream_t2166124941::get_offset_of__mode_4(),
	CryptoStream_t2166124941::get_offset_of__currentBlock_5(),
	CryptoStream_t2166124941::get_offset_of__disposed_6(),
	CryptoStream_t2166124941::get_offset_of__flushedFinalBlock_7(),
	CryptoStream_t2166124941::get_offset_of__partialCount_8(),
	CryptoStream_t2166124941::get_offset_of__endOfStream_9(),
	CryptoStream_t2166124941::get_offset_of__waitingBlock_10(),
	CryptoStream_t2166124941::get_offset_of__waitingCount_11(),
	CryptoStream_t2166124941::get_offset_of__transformedBlock_12(),
	CryptoStream_t2166124941::get_offset_of__transformedPos_13(),
	CryptoStream_t2166124941::get_offset_of__transformedCount_14(),
	CryptoStream_t2166124941::get_offset_of__workingBlock_15(),
	CryptoStream_t2166124941::get_offset_of__workingCount_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize797 = { sizeof (CryptoStreamMode_t3444626768)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable797[3] = 
{
	CryptoStreamMode_t3444626768::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize798 = { sizeof (CspParameters_t309313264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable798[5] = 
{
	CspParameters_t309313264::get_offset_of__Flags_0(),
	CspParameters_t309313264::get_offset_of_KeyContainerName_1(),
	CspParameters_t309313264::get_offset_of_KeyNumber_2(),
	CspParameters_t309313264::get_offset_of_ProviderName_3(),
	CspParameters_t309313264::get_offset_of_ProviderType_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize799 = { sizeof (CspProviderFlags_t3394239842)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable799[9] = 
{
	CspProviderFlags_t3394239842::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
