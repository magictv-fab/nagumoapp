﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// UnityEngine.AndroidJavaClass
struct AndroidJavaClass_t1816259147;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t2362096582;
// System.Object
struct Il2CppObject;
// nativeShareAndroidScript
struct nativeShareAndroidScript_t2018237170;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// nativeShareAndroidScript/<ShareScreenshot>c__Iterator25
struct  U3CShareScreenshotU3Ec__Iterator25_t3076450652  : public Il2CppObject
{
public:
	// UnityEngine.Texture2D nativeShareAndroidScript/<ShareScreenshot>c__Iterator25::<tex>__0
	Texture2D_t3884108195 * ___U3CtexU3E__0_0;
	// System.String nativeShareAndroidScript/<ShareScreenshot>c__Iterator25::<destination>__1
	String_t* ___U3CdestinationU3E__1_1;
	// System.Byte[] nativeShareAndroidScript/<ShareScreenshot>c__Iterator25::<bytes>__2
	ByteU5BU5D_t4260760469* ___U3CbytesU3E__2_2;
	// UnityEngine.AndroidJavaClass nativeShareAndroidScript/<ShareScreenshot>c__Iterator25::<intentClass>__3
	AndroidJavaClass_t1816259147 * ___U3CintentClassU3E__3_3;
	// UnityEngine.AndroidJavaObject nativeShareAndroidScript/<ShareScreenshot>c__Iterator25::<intentObject>__4
	AndroidJavaObject_t2362096582 * ___U3CintentObjectU3E__4_4;
	// UnityEngine.AndroidJavaClass nativeShareAndroidScript/<ShareScreenshot>c__Iterator25::<uriClass>__5
	AndroidJavaClass_t1816259147 * ___U3CuriClassU3E__5_5;
	// UnityEngine.AndroidJavaObject nativeShareAndroidScript/<ShareScreenshot>c__Iterator25::<uriObject>__6
	AndroidJavaObject_t2362096582 * ___U3CuriObjectU3E__6_6;
	// UnityEngine.AndroidJavaClass nativeShareAndroidScript/<ShareScreenshot>c__Iterator25::<unity>__7
	AndroidJavaClass_t1816259147 * ___U3CunityU3E__7_7;
	// UnityEngine.AndroidJavaObject nativeShareAndroidScript/<ShareScreenshot>c__Iterator25::<currentActivity>__8
	AndroidJavaObject_t2362096582 * ___U3CcurrentActivityU3E__8_8;
	// UnityEngine.AndroidJavaObject nativeShareAndroidScript/<ShareScreenshot>c__Iterator25::<chooser>__9
	AndroidJavaObject_t2362096582 * ___U3CchooserU3E__9_9;
	// System.Int32 nativeShareAndroidScript/<ShareScreenshot>c__Iterator25::$PC
	int32_t ___U24PC_10;
	// System.Object nativeShareAndroidScript/<ShareScreenshot>c__Iterator25::$current
	Il2CppObject * ___U24current_11;
	// nativeShareAndroidScript nativeShareAndroidScript/<ShareScreenshot>c__Iterator25::<>f__this
	nativeShareAndroidScript_t2018237170 * ___U3CU3Ef__this_12;

public:
	inline static int32_t get_offset_of_U3CtexU3E__0_0() { return static_cast<int32_t>(offsetof(U3CShareScreenshotU3Ec__Iterator25_t3076450652, ___U3CtexU3E__0_0)); }
	inline Texture2D_t3884108195 * get_U3CtexU3E__0_0() const { return ___U3CtexU3E__0_0; }
	inline Texture2D_t3884108195 ** get_address_of_U3CtexU3E__0_0() { return &___U3CtexU3E__0_0; }
	inline void set_U3CtexU3E__0_0(Texture2D_t3884108195 * value)
	{
		___U3CtexU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtexU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CdestinationU3E__1_1() { return static_cast<int32_t>(offsetof(U3CShareScreenshotU3Ec__Iterator25_t3076450652, ___U3CdestinationU3E__1_1)); }
	inline String_t* get_U3CdestinationU3E__1_1() const { return ___U3CdestinationU3E__1_1; }
	inline String_t** get_address_of_U3CdestinationU3E__1_1() { return &___U3CdestinationU3E__1_1; }
	inline void set_U3CdestinationU3E__1_1(String_t* value)
	{
		___U3CdestinationU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdestinationU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CbytesU3E__2_2() { return static_cast<int32_t>(offsetof(U3CShareScreenshotU3Ec__Iterator25_t3076450652, ___U3CbytesU3E__2_2)); }
	inline ByteU5BU5D_t4260760469* get_U3CbytesU3E__2_2() const { return ___U3CbytesU3E__2_2; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CbytesU3E__2_2() { return &___U3CbytesU3E__2_2; }
	inline void set_U3CbytesU3E__2_2(ByteU5BU5D_t4260760469* value)
	{
		___U3CbytesU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbytesU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CintentClassU3E__3_3() { return static_cast<int32_t>(offsetof(U3CShareScreenshotU3Ec__Iterator25_t3076450652, ___U3CintentClassU3E__3_3)); }
	inline AndroidJavaClass_t1816259147 * get_U3CintentClassU3E__3_3() const { return ___U3CintentClassU3E__3_3; }
	inline AndroidJavaClass_t1816259147 ** get_address_of_U3CintentClassU3E__3_3() { return &___U3CintentClassU3E__3_3; }
	inline void set_U3CintentClassU3E__3_3(AndroidJavaClass_t1816259147 * value)
	{
		___U3CintentClassU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CintentClassU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CintentObjectU3E__4_4() { return static_cast<int32_t>(offsetof(U3CShareScreenshotU3Ec__Iterator25_t3076450652, ___U3CintentObjectU3E__4_4)); }
	inline AndroidJavaObject_t2362096582 * get_U3CintentObjectU3E__4_4() const { return ___U3CintentObjectU3E__4_4; }
	inline AndroidJavaObject_t2362096582 ** get_address_of_U3CintentObjectU3E__4_4() { return &___U3CintentObjectU3E__4_4; }
	inline void set_U3CintentObjectU3E__4_4(AndroidJavaObject_t2362096582 * value)
	{
		___U3CintentObjectU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CintentObjectU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U3CuriClassU3E__5_5() { return static_cast<int32_t>(offsetof(U3CShareScreenshotU3Ec__Iterator25_t3076450652, ___U3CuriClassU3E__5_5)); }
	inline AndroidJavaClass_t1816259147 * get_U3CuriClassU3E__5_5() const { return ___U3CuriClassU3E__5_5; }
	inline AndroidJavaClass_t1816259147 ** get_address_of_U3CuriClassU3E__5_5() { return &___U3CuriClassU3E__5_5; }
	inline void set_U3CuriClassU3E__5_5(AndroidJavaClass_t1816259147 * value)
	{
		___U3CuriClassU3E__5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CuriClassU3E__5_5, value);
	}

	inline static int32_t get_offset_of_U3CuriObjectU3E__6_6() { return static_cast<int32_t>(offsetof(U3CShareScreenshotU3Ec__Iterator25_t3076450652, ___U3CuriObjectU3E__6_6)); }
	inline AndroidJavaObject_t2362096582 * get_U3CuriObjectU3E__6_6() const { return ___U3CuriObjectU3E__6_6; }
	inline AndroidJavaObject_t2362096582 ** get_address_of_U3CuriObjectU3E__6_6() { return &___U3CuriObjectU3E__6_6; }
	inline void set_U3CuriObjectU3E__6_6(AndroidJavaObject_t2362096582 * value)
	{
		___U3CuriObjectU3E__6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CuriObjectU3E__6_6, value);
	}

	inline static int32_t get_offset_of_U3CunityU3E__7_7() { return static_cast<int32_t>(offsetof(U3CShareScreenshotU3Ec__Iterator25_t3076450652, ___U3CunityU3E__7_7)); }
	inline AndroidJavaClass_t1816259147 * get_U3CunityU3E__7_7() const { return ___U3CunityU3E__7_7; }
	inline AndroidJavaClass_t1816259147 ** get_address_of_U3CunityU3E__7_7() { return &___U3CunityU3E__7_7; }
	inline void set_U3CunityU3E__7_7(AndroidJavaClass_t1816259147 * value)
	{
		___U3CunityU3E__7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CunityU3E__7_7, value);
	}

	inline static int32_t get_offset_of_U3CcurrentActivityU3E__8_8() { return static_cast<int32_t>(offsetof(U3CShareScreenshotU3Ec__Iterator25_t3076450652, ___U3CcurrentActivityU3E__8_8)); }
	inline AndroidJavaObject_t2362096582 * get_U3CcurrentActivityU3E__8_8() const { return ___U3CcurrentActivityU3E__8_8; }
	inline AndroidJavaObject_t2362096582 ** get_address_of_U3CcurrentActivityU3E__8_8() { return &___U3CcurrentActivityU3E__8_8; }
	inline void set_U3CcurrentActivityU3E__8_8(AndroidJavaObject_t2362096582 * value)
	{
		___U3CcurrentActivityU3E__8_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcurrentActivityU3E__8_8, value);
	}

	inline static int32_t get_offset_of_U3CchooserU3E__9_9() { return static_cast<int32_t>(offsetof(U3CShareScreenshotU3Ec__Iterator25_t3076450652, ___U3CchooserU3E__9_9)); }
	inline AndroidJavaObject_t2362096582 * get_U3CchooserU3E__9_9() const { return ___U3CchooserU3E__9_9; }
	inline AndroidJavaObject_t2362096582 ** get_address_of_U3CchooserU3E__9_9() { return &___U3CchooserU3E__9_9; }
	inline void set_U3CchooserU3E__9_9(AndroidJavaObject_t2362096582 * value)
	{
		___U3CchooserU3E__9_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CchooserU3E__9_9, value);
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CShareScreenshotU3Ec__Iterator25_t3076450652, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}

	inline static int32_t get_offset_of_U24current_11() { return static_cast<int32_t>(offsetof(U3CShareScreenshotU3Ec__Iterator25_t3076450652, ___U24current_11)); }
	inline Il2CppObject * get_U24current_11() const { return ___U24current_11; }
	inline Il2CppObject ** get_address_of_U24current_11() { return &___U24current_11; }
	inline void set_U24current_11(Il2CppObject * value)
	{
		___U24current_11 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_12() { return static_cast<int32_t>(offsetof(U3CShareScreenshotU3Ec__Iterator25_t3076450652, ___U3CU3Ef__this_12)); }
	inline nativeShareAndroidScript_t2018237170 * get_U3CU3Ef__this_12() const { return ___U3CU3Ef__this_12; }
	inline nativeShareAndroidScript_t2018237170 ** get_address_of_U3CU3Ef__this_12() { return &___U3CU3Ef__this_12; }
	inline void set_U3CU3Ef__this_12(nativeShareAndroidScript_t2018237170 * value)
	{
		___U3CU3Ef__this_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
