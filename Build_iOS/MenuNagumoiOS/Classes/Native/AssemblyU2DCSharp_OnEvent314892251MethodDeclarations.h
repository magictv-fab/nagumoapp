﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnEvent
struct OnEvent_t314892251;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void OnEvent::.ctor(System.Object,System.IntPtr)
extern "C"  void OnEvent__ctor_m3504599090 (OnEvent_t314892251 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnEvent::Invoke()
extern "C"  void OnEvent_Invoke_m3592910604 (OnEvent_t314892251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult OnEvent::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnEvent_BeginInvoke_m468673279 (OnEvent_t314892251 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnEvent::EndInvoke(System.IAsyncResult)
extern "C"  void OnEvent_EndInvoke_m2014006082 (OnEvent_t314892251 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
