﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTVLeftMenuControlScript
struct MagicTVLeftMenuControlScript_t3130123923;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTVLeftMenuControlScript::.ctor()
extern "C"  void MagicTVLeftMenuControlScript__ctor_m3992818024 (MagicTVLeftMenuControlScript_t3130123923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVLeftMenuControlScript::Show()
extern "C"  void MagicTVLeftMenuControlScript_Show_m2162384537 (MagicTVLeftMenuControlScript_t3130123923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVLeftMenuControlScript::Hide()
extern "C"  void MagicTVLeftMenuControlScript_Hide_m1848042398 (MagicTVLeftMenuControlScript_t3130123923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVLeftMenuControlScript::OpenCloseClik()
extern "C"  void MagicTVLeftMenuControlScript_OpenCloseClik_m3313933695 (MagicTVLeftMenuControlScript_t3130123923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
