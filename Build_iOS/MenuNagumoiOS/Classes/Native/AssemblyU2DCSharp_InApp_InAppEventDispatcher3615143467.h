﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>
struct Dictionary_2_t3591312688;
// InApp.InAppEventDispatcher
struct InAppEventDispatcher_t3615143467;
// InApp.InAppEventDispatcher/InAppEventDispatcherWithString
struct InAppEventDispatcherWithString_t1910714196;
// InApp.InAppEventDispatcher/InAppEventDispatcherOnOff
struct InAppEventDispatcherOnOff_t3474551315;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1974256870;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InApp.InAppEventDispatcher
struct  InAppEventDispatcher_t3615143467  : public MonoBehaviour_t667441552
{
public:
	// System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff> InApp.InAppEventDispatcher::eventDictionary
	Dictionary_2_t3591312688 * ___eventDictionary_2;
	// InApp.InAppEventDispatcher/InAppEventDispatcherWithString InApp.InAppEventDispatcher::actionReferenceGIROSCOPE_INCLINATION_CONFIGURABLE
	InAppEventDispatcherWithString_t1910714196 * ___actionReferenceGIROSCOPE_INCLINATION_CONFIGURABLE_4;
	// InApp.InAppEventDispatcher/InAppEventDispatcherWithString InApp.InAppEventDispatcher::actionReferenceFIXED_CAMERA
	InAppEventDispatcherWithString_t1910714196 * ___actionReferenceFIXED_CAMERA_5;
	// InApp.InAppEventDispatcher/InAppEventDispatcherWithString InApp.InAppEventDispatcher::actionReferenceDETACHED_FROM_TRACKING
	InAppEventDispatcherWithString_t1910714196 * ___actionReferenceDETACHED_FROM_TRACKING_6;
	// InApp.InAppEventDispatcher/InAppEventDispatcherWithString InApp.InAppEventDispatcher::actionReferenceCAMERA_DISTANCE_CONFIG
	InAppEventDispatcherWithString_t1910714196 * ___actionReferenceCAMERA_DISTANCE_CONFIG_7;
	// InApp.InAppEventDispatcher/InAppEventDispatcherWithString InApp.InAppEventDispatcher::actionReferenceGENERIC
	InAppEventDispatcherWithString_t1910714196 * ___actionReferenceGENERIC_8;
	// InApp.InAppEventDispatcher/InAppEventDispatcherOnOff InApp.InAppEventDispatcher::actionReferenceFIXED_GROUP_OF_PRESENTATIONS
	InAppEventDispatcherOnOff_t3474551315 * ___actionReferenceFIXED_GROUP_OF_PRESENTATIONS_9;
	// InApp.InAppEventDispatcher/InAppEventDispatcherOnOff InApp.InAppEventDispatcher::actionReferenceLIGHT
	InAppEventDispatcherOnOff_t3474551315 * ___actionReferenceLIGHT_10;
	// InApp.InAppEventDispatcher/InAppEventDispatcherOnOff InApp.InAppEventDispatcher::actionReferenceRESET
	InAppEventDispatcherOnOff_t3474551315 * ___actionReferenceRESET_11;
	// InApp.InAppEventDispatcher/InAppEventDispatcherOnOff InApp.InAppEventDispatcher::actionReferenceLOCK_SCREEN_ORIENTATION_PORTRAIT
	InAppEventDispatcherOnOff_t3474551315 * ___actionReferenceLOCK_SCREEN_ORIENTATION_PORTRAIT_12;
	// InApp.InAppEventDispatcher/InAppEventDispatcherOnOff InApp.InAppEventDispatcher::actionReferenceLOCK_SCREEN_ORIENTATION_LANDSCAPE
	InAppEventDispatcherOnOff_t3474551315 * ___actionReferenceLOCK_SCREEN_ORIENTATION_LANDSCAPE_13;
	// InApp.InAppEventDispatcher/InAppEventDispatcherOnOff InApp.InAppEventDispatcher::actionReferenceNOT_FOUND
	InAppEventDispatcherOnOff_t3474551315 * ___actionReferenceNOT_FOUND_14;
	// InApp.InAppEventDispatcher/InAppEventDispatcherOnOff InApp.InAppEventDispatcher::actionReferenceGIROSCOPE_INCLINATION
	InAppEventDispatcherOnOff_t3474551315 * ___actionReferenceGIROSCOPE_INCLINATION_15;

public:
	inline static int32_t get_offset_of_eventDictionary_2() { return static_cast<int32_t>(offsetof(InAppEventDispatcher_t3615143467, ___eventDictionary_2)); }
	inline Dictionary_2_t3591312688 * get_eventDictionary_2() const { return ___eventDictionary_2; }
	inline Dictionary_2_t3591312688 ** get_address_of_eventDictionary_2() { return &___eventDictionary_2; }
	inline void set_eventDictionary_2(Dictionary_2_t3591312688 * value)
	{
		___eventDictionary_2 = value;
		Il2CppCodeGenWriteBarrier(&___eventDictionary_2, value);
	}

	inline static int32_t get_offset_of_actionReferenceGIROSCOPE_INCLINATION_CONFIGURABLE_4() { return static_cast<int32_t>(offsetof(InAppEventDispatcher_t3615143467, ___actionReferenceGIROSCOPE_INCLINATION_CONFIGURABLE_4)); }
	inline InAppEventDispatcherWithString_t1910714196 * get_actionReferenceGIROSCOPE_INCLINATION_CONFIGURABLE_4() const { return ___actionReferenceGIROSCOPE_INCLINATION_CONFIGURABLE_4; }
	inline InAppEventDispatcherWithString_t1910714196 ** get_address_of_actionReferenceGIROSCOPE_INCLINATION_CONFIGURABLE_4() { return &___actionReferenceGIROSCOPE_INCLINATION_CONFIGURABLE_4; }
	inline void set_actionReferenceGIROSCOPE_INCLINATION_CONFIGURABLE_4(InAppEventDispatcherWithString_t1910714196 * value)
	{
		___actionReferenceGIROSCOPE_INCLINATION_CONFIGURABLE_4 = value;
		Il2CppCodeGenWriteBarrier(&___actionReferenceGIROSCOPE_INCLINATION_CONFIGURABLE_4, value);
	}

	inline static int32_t get_offset_of_actionReferenceFIXED_CAMERA_5() { return static_cast<int32_t>(offsetof(InAppEventDispatcher_t3615143467, ___actionReferenceFIXED_CAMERA_5)); }
	inline InAppEventDispatcherWithString_t1910714196 * get_actionReferenceFIXED_CAMERA_5() const { return ___actionReferenceFIXED_CAMERA_5; }
	inline InAppEventDispatcherWithString_t1910714196 ** get_address_of_actionReferenceFIXED_CAMERA_5() { return &___actionReferenceFIXED_CAMERA_5; }
	inline void set_actionReferenceFIXED_CAMERA_5(InAppEventDispatcherWithString_t1910714196 * value)
	{
		___actionReferenceFIXED_CAMERA_5 = value;
		Il2CppCodeGenWriteBarrier(&___actionReferenceFIXED_CAMERA_5, value);
	}

	inline static int32_t get_offset_of_actionReferenceDETACHED_FROM_TRACKING_6() { return static_cast<int32_t>(offsetof(InAppEventDispatcher_t3615143467, ___actionReferenceDETACHED_FROM_TRACKING_6)); }
	inline InAppEventDispatcherWithString_t1910714196 * get_actionReferenceDETACHED_FROM_TRACKING_6() const { return ___actionReferenceDETACHED_FROM_TRACKING_6; }
	inline InAppEventDispatcherWithString_t1910714196 ** get_address_of_actionReferenceDETACHED_FROM_TRACKING_6() { return &___actionReferenceDETACHED_FROM_TRACKING_6; }
	inline void set_actionReferenceDETACHED_FROM_TRACKING_6(InAppEventDispatcherWithString_t1910714196 * value)
	{
		___actionReferenceDETACHED_FROM_TRACKING_6 = value;
		Il2CppCodeGenWriteBarrier(&___actionReferenceDETACHED_FROM_TRACKING_6, value);
	}

	inline static int32_t get_offset_of_actionReferenceCAMERA_DISTANCE_CONFIG_7() { return static_cast<int32_t>(offsetof(InAppEventDispatcher_t3615143467, ___actionReferenceCAMERA_DISTANCE_CONFIG_7)); }
	inline InAppEventDispatcherWithString_t1910714196 * get_actionReferenceCAMERA_DISTANCE_CONFIG_7() const { return ___actionReferenceCAMERA_DISTANCE_CONFIG_7; }
	inline InAppEventDispatcherWithString_t1910714196 ** get_address_of_actionReferenceCAMERA_DISTANCE_CONFIG_7() { return &___actionReferenceCAMERA_DISTANCE_CONFIG_7; }
	inline void set_actionReferenceCAMERA_DISTANCE_CONFIG_7(InAppEventDispatcherWithString_t1910714196 * value)
	{
		___actionReferenceCAMERA_DISTANCE_CONFIG_7 = value;
		Il2CppCodeGenWriteBarrier(&___actionReferenceCAMERA_DISTANCE_CONFIG_7, value);
	}

	inline static int32_t get_offset_of_actionReferenceGENERIC_8() { return static_cast<int32_t>(offsetof(InAppEventDispatcher_t3615143467, ___actionReferenceGENERIC_8)); }
	inline InAppEventDispatcherWithString_t1910714196 * get_actionReferenceGENERIC_8() const { return ___actionReferenceGENERIC_8; }
	inline InAppEventDispatcherWithString_t1910714196 ** get_address_of_actionReferenceGENERIC_8() { return &___actionReferenceGENERIC_8; }
	inline void set_actionReferenceGENERIC_8(InAppEventDispatcherWithString_t1910714196 * value)
	{
		___actionReferenceGENERIC_8 = value;
		Il2CppCodeGenWriteBarrier(&___actionReferenceGENERIC_8, value);
	}

	inline static int32_t get_offset_of_actionReferenceFIXED_GROUP_OF_PRESENTATIONS_9() { return static_cast<int32_t>(offsetof(InAppEventDispatcher_t3615143467, ___actionReferenceFIXED_GROUP_OF_PRESENTATIONS_9)); }
	inline InAppEventDispatcherOnOff_t3474551315 * get_actionReferenceFIXED_GROUP_OF_PRESENTATIONS_9() const { return ___actionReferenceFIXED_GROUP_OF_PRESENTATIONS_9; }
	inline InAppEventDispatcherOnOff_t3474551315 ** get_address_of_actionReferenceFIXED_GROUP_OF_PRESENTATIONS_9() { return &___actionReferenceFIXED_GROUP_OF_PRESENTATIONS_9; }
	inline void set_actionReferenceFIXED_GROUP_OF_PRESENTATIONS_9(InAppEventDispatcherOnOff_t3474551315 * value)
	{
		___actionReferenceFIXED_GROUP_OF_PRESENTATIONS_9 = value;
		Il2CppCodeGenWriteBarrier(&___actionReferenceFIXED_GROUP_OF_PRESENTATIONS_9, value);
	}

	inline static int32_t get_offset_of_actionReferenceLIGHT_10() { return static_cast<int32_t>(offsetof(InAppEventDispatcher_t3615143467, ___actionReferenceLIGHT_10)); }
	inline InAppEventDispatcherOnOff_t3474551315 * get_actionReferenceLIGHT_10() const { return ___actionReferenceLIGHT_10; }
	inline InAppEventDispatcherOnOff_t3474551315 ** get_address_of_actionReferenceLIGHT_10() { return &___actionReferenceLIGHT_10; }
	inline void set_actionReferenceLIGHT_10(InAppEventDispatcherOnOff_t3474551315 * value)
	{
		___actionReferenceLIGHT_10 = value;
		Il2CppCodeGenWriteBarrier(&___actionReferenceLIGHT_10, value);
	}

	inline static int32_t get_offset_of_actionReferenceRESET_11() { return static_cast<int32_t>(offsetof(InAppEventDispatcher_t3615143467, ___actionReferenceRESET_11)); }
	inline InAppEventDispatcherOnOff_t3474551315 * get_actionReferenceRESET_11() const { return ___actionReferenceRESET_11; }
	inline InAppEventDispatcherOnOff_t3474551315 ** get_address_of_actionReferenceRESET_11() { return &___actionReferenceRESET_11; }
	inline void set_actionReferenceRESET_11(InAppEventDispatcherOnOff_t3474551315 * value)
	{
		___actionReferenceRESET_11 = value;
		Il2CppCodeGenWriteBarrier(&___actionReferenceRESET_11, value);
	}

	inline static int32_t get_offset_of_actionReferenceLOCK_SCREEN_ORIENTATION_PORTRAIT_12() { return static_cast<int32_t>(offsetof(InAppEventDispatcher_t3615143467, ___actionReferenceLOCK_SCREEN_ORIENTATION_PORTRAIT_12)); }
	inline InAppEventDispatcherOnOff_t3474551315 * get_actionReferenceLOCK_SCREEN_ORIENTATION_PORTRAIT_12() const { return ___actionReferenceLOCK_SCREEN_ORIENTATION_PORTRAIT_12; }
	inline InAppEventDispatcherOnOff_t3474551315 ** get_address_of_actionReferenceLOCK_SCREEN_ORIENTATION_PORTRAIT_12() { return &___actionReferenceLOCK_SCREEN_ORIENTATION_PORTRAIT_12; }
	inline void set_actionReferenceLOCK_SCREEN_ORIENTATION_PORTRAIT_12(InAppEventDispatcherOnOff_t3474551315 * value)
	{
		___actionReferenceLOCK_SCREEN_ORIENTATION_PORTRAIT_12 = value;
		Il2CppCodeGenWriteBarrier(&___actionReferenceLOCK_SCREEN_ORIENTATION_PORTRAIT_12, value);
	}

	inline static int32_t get_offset_of_actionReferenceLOCK_SCREEN_ORIENTATION_LANDSCAPE_13() { return static_cast<int32_t>(offsetof(InAppEventDispatcher_t3615143467, ___actionReferenceLOCK_SCREEN_ORIENTATION_LANDSCAPE_13)); }
	inline InAppEventDispatcherOnOff_t3474551315 * get_actionReferenceLOCK_SCREEN_ORIENTATION_LANDSCAPE_13() const { return ___actionReferenceLOCK_SCREEN_ORIENTATION_LANDSCAPE_13; }
	inline InAppEventDispatcherOnOff_t3474551315 ** get_address_of_actionReferenceLOCK_SCREEN_ORIENTATION_LANDSCAPE_13() { return &___actionReferenceLOCK_SCREEN_ORIENTATION_LANDSCAPE_13; }
	inline void set_actionReferenceLOCK_SCREEN_ORIENTATION_LANDSCAPE_13(InAppEventDispatcherOnOff_t3474551315 * value)
	{
		___actionReferenceLOCK_SCREEN_ORIENTATION_LANDSCAPE_13 = value;
		Il2CppCodeGenWriteBarrier(&___actionReferenceLOCK_SCREEN_ORIENTATION_LANDSCAPE_13, value);
	}

	inline static int32_t get_offset_of_actionReferenceNOT_FOUND_14() { return static_cast<int32_t>(offsetof(InAppEventDispatcher_t3615143467, ___actionReferenceNOT_FOUND_14)); }
	inline InAppEventDispatcherOnOff_t3474551315 * get_actionReferenceNOT_FOUND_14() const { return ___actionReferenceNOT_FOUND_14; }
	inline InAppEventDispatcherOnOff_t3474551315 ** get_address_of_actionReferenceNOT_FOUND_14() { return &___actionReferenceNOT_FOUND_14; }
	inline void set_actionReferenceNOT_FOUND_14(InAppEventDispatcherOnOff_t3474551315 * value)
	{
		___actionReferenceNOT_FOUND_14 = value;
		Il2CppCodeGenWriteBarrier(&___actionReferenceNOT_FOUND_14, value);
	}

	inline static int32_t get_offset_of_actionReferenceGIROSCOPE_INCLINATION_15() { return static_cast<int32_t>(offsetof(InAppEventDispatcher_t3615143467, ___actionReferenceGIROSCOPE_INCLINATION_15)); }
	inline InAppEventDispatcherOnOff_t3474551315 * get_actionReferenceGIROSCOPE_INCLINATION_15() const { return ___actionReferenceGIROSCOPE_INCLINATION_15; }
	inline InAppEventDispatcherOnOff_t3474551315 ** get_address_of_actionReferenceGIROSCOPE_INCLINATION_15() { return &___actionReferenceGIROSCOPE_INCLINATION_15; }
	inline void set_actionReferenceGIROSCOPE_INCLINATION_15(InAppEventDispatcherOnOff_t3474551315 * value)
	{
		___actionReferenceGIROSCOPE_INCLINATION_15 = value;
		Il2CppCodeGenWriteBarrier(&___actionReferenceGIROSCOPE_INCLINATION_15, value);
	}
};

struct InAppEventDispatcher_t3615143467_StaticFields
{
public:
	// InApp.InAppEventDispatcher InApp.InAppEventDispatcher::_instance
	InAppEventDispatcher_t3615143467 * ____instance_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> InApp.InAppEventDispatcher::<>f__switch$map3
	Dictionary_2_t1974256870 * ___U3CU3Ef__switchU24map3_16;

public:
	inline static int32_t get_offset_of__instance_3() { return static_cast<int32_t>(offsetof(InAppEventDispatcher_t3615143467_StaticFields, ____instance_3)); }
	inline InAppEventDispatcher_t3615143467 * get__instance_3() const { return ____instance_3; }
	inline InAppEventDispatcher_t3615143467 ** get_address_of__instance_3() { return &____instance_3; }
	inline void set__instance_3(InAppEventDispatcher_t3615143467 * value)
	{
		____instance_3 = value;
		Il2CppCodeGenWriteBarrier(&____instance_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map3_16() { return static_cast<int32_t>(offsetof(InAppEventDispatcher_t3615143467_StaticFields, ___U3CU3Ef__switchU24map3_16)); }
	inline Dictionary_2_t1974256870 * get_U3CU3Ef__switchU24map3_16() const { return ___U3CU3Ef__switchU24map3_16; }
	inline Dictionary_2_t1974256870 ** get_address_of_U3CU3Ef__switchU24map3_16() { return &___U3CU3Ef__switchU24map3_16; }
	inline void set_U3CU3Ef__switchU24map3_16(Dictionary_2_t1974256870 * value)
	{
		___U3CU3Ef__switchU24map3_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map3_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
