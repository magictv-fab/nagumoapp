﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.MultiFormatReader
struct MultiFormatReader_t4081038293;
// ZXing.Result
struct Result_t2610723219;
// ZXing.BinaryBitmap
struct BinaryBitmap_t2444664454;
// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>
struct IDictionary_2_t759989846;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_BinaryBitmap2444664454.h"

// ZXing.Result ZXing.MultiFormatReader::decode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  Result_t2610723219 * MultiFormatReader_decode_m2019882989 (MultiFormatReader_t4081038293 * __this, BinaryBitmap_t2444664454 * ___image0, Il2CppObject* ___hints1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Result ZXing.MultiFormatReader::decodeWithState(ZXing.BinaryBitmap)
extern "C"  Result_t2610723219 * MultiFormatReader_decodeWithState_m2378665753 (MultiFormatReader_t4081038293 * __this, BinaryBitmap_t2444664454 * ___image0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.MultiFormatReader::set_Hints(System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  void MultiFormatReader_set_Hints_m3501229558 (MultiFormatReader_t4081038293 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.MultiFormatReader::reset()
extern "C"  void MultiFormatReader_reset_m27417583 (MultiFormatReader_t4081038293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Result ZXing.MultiFormatReader::decodeInternal(ZXing.BinaryBitmap)
extern "C"  Result_t2610723219 * MultiFormatReader_decodeInternal_m639984061 (MultiFormatReader_t4081038293 * __this, BinaryBitmap_t2444664454 * ___image0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.MultiFormatReader::.ctor()
extern "C"  void MultiFormatReader__ctor_m4045637922 (MultiFormatReader_t4081038293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
