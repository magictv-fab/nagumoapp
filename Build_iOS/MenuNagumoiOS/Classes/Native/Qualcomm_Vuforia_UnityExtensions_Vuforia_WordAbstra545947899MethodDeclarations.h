﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t545947899;
// Vuforia.Word
struct Word_t2165514892;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordTempl3973358313.h"

// System.Void Vuforia.WordAbstractBehaviour::InternalUnregisterTrackable()
extern "C"  void WordAbstractBehaviour_InternalUnregisterTrackable_m1432179796 (WordAbstractBehaviour_t545947899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.Word Vuforia.WordAbstractBehaviour::get_Word()
extern "C"  Il2CppObject * WordAbstractBehaviour_get_Word_m1599644366 (WordAbstractBehaviour_t545947899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_SpecificWord()
extern "C"  String_t* WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_SpecificWord_m3742082101 (WordAbstractBehaviour_t545947899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.SetSpecificWord(System.String)
extern "C"  void WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m1503861743 (WordAbstractBehaviour_t545947899 * __this, String_t* ___word0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WordTemplateMode Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_Mode()
extern "C"  int32_t WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_Mode_m3646611483 (WordAbstractBehaviour_t545947899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_IsTemplateMode()
extern "C"  bool WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsTemplateMode_m2607006377 (WordAbstractBehaviour_t545947899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_IsSpecificWordMode()
extern "C"  bool WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsSpecificWordMode_m3406246539 (WordAbstractBehaviour_t545947899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.SetMode(Vuforia.WordTemplateMode)
extern "C"  void WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m686020103 (WordAbstractBehaviour_t545947899 * __this, int32_t ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.InitializeWord(Vuforia.Word)
extern "C"  void WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m4000620401 (WordAbstractBehaviour_t545947899 * __this, Il2CppObject * ___word0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::.ctor()
extern "C"  void WordAbstractBehaviour__ctor_m3910734634 (WordAbstractBehaviour_t545947899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
extern "C"  bool WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m2012144384 (WordAbstractBehaviour_t545947899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
extern "C"  void WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m1365625175 (WordAbstractBehaviour_t545947899 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
extern "C"  Transform_t1659122786 * WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m908147229 (WordAbstractBehaviour_t545947899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
extern "C"  GameObject_t3674682005 * WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m4223599859 (WordAbstractBehaviour_t545947899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
