﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.update.UpdateProcessController
struct UpdateProcessController_t827871604;
// System.String
struct String_t;
// MagicTV.vo.RevisionInfoVO
struct RevisionInfoVO_t1983749152;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_MagicTV_vo_RevisionInfoVO1983749152.h"

// System.Void MagicTV.update.UpdateProcessController::.ctor()
extern "C"  void UpdateProcessController__ctor_m3368090559 (UpdateProcessController_t827871604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.UpdateProcessController::prepare()
extern "C"  void UpdateProcessController_prepare_m3551633732 (UpdateProcessController_t827871604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.UpdateProcessController::hideProgress()
extern "C"  void UpdateProcessController_hideProgress_m2165344084 (UpdateProcessController_t827871604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.UpdateProcessController::showProgress(System.Single)
extern "C"  void UpdateProcessController_showProgress_m1765655228 (UpdateProcessController_t827871604 * __this, float ___progress0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.UpdateProcessController::prepareInitialData()
extern "C"  void UpdateProcessController_prepareInitialData_m4090809932 (UpdateProcessController_t827871604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.UpdateProcessController::Play()
extern "C"  void UpdateProcessController_Play_m1921076185 (UpdateProcessController_t827871604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.UpdateProcessController::sendTokenToServer(System.String)
extern "C"  void UpdateProcessController_sendTokenToServer_m1492410166 (UpdateProcessController_t827871604 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.UpdateProcessController::doSendTokenToServer()
extern "C"  void UpdateProcessController_doSendTokenToServer_m374476225 (UpdateProcessController_t827871604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.UpdateProcessController::tokenSaved(System.Boolean)
extern "C"  void UpdateProcessController_tokenSaved_m2695430986 (UpdateProcessController_t827871604 * __this, bool ___success0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.UpdateProcessController::UpdateRequestEventHandler(MagicTV.vo.RevisionInfoVO)
extern "C"  void UpdateProcessController_UpdateRequestEventHandler_m3954824455 (UpdateProcessController_t827871604 * __this, RevisionInfoVO_t1983749152 * ___resultInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.UpdateProcessController::StartMobileProcess(MagicTV.vo.RevisionInfoVO)
extern "C"  void UpdateProcessController_StartMobileProcess_m2857162474 (UpdateProcessController_t827871604 * __this, RevisionInfoVO_t1983749152 * ___resultInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.UpdateProcessController::commitPin(System.String)
extern "C"  void UpdateProcessController_commitPin_m1279176327 (UpdateProcessController_t827871604 * __this, String_t* ___pin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.UpdateProcessController::deleteFiles()
extern "C"  void UpdateProcessController_deleteFiles_m976568457 (UpdateProcessController_t827871604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.UpdateProcessController::StartDownloadProcess()
extern "C"  void UpdateProcessController_StartDownloadProcess_m1305686122 (UpdateProcessController_t827871604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.UpdateProcessController::doStartCacheProcess()
extern "C"  void UpdateProcessController_doStartCacheProcess_m855703489 (UpdateProcessController_t827871604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.UpdateProcessController::cacheCompleteEventHandler()
extern "C"  void UpdateProcessController_cacheCompleteEventHandler_m1502834184 (UpdateProcessController_t827871604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.UpdateProcessController::cacheProgressEventHandler(System.Single)
extern "C"  void UpdateProcessController_cacheProgressEventHandler_m3459389231 (UpdateProcessController_t827871604 * __this, float ___subTotal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.UpdateProcessController::UpdateErrorEventHandler(System.String)
extern "C"  void UpdateProcessController_UpdateErrorEventHandler_m771313846 (UpdateProcessController_t827871604 * __this, String_t* ___errorMessage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.UpdateProcessController::EndProcess()
extern "C"  void UpdateProcessController_EndProcess_m940789433 (UpdateProcessController_t827871604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
