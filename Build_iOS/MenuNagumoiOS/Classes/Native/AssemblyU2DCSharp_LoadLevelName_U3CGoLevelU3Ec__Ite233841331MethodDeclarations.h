﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoadLevelName/<GoLevel>c__Iterator60
struct U3CGoLevelU3Ec__Iterator60_t233841331;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void LoadLevelName/<GoLevel>c__Iterator60::.ctor()
extern "C"  void U3CGoLevelU3Ec__Iterator60__ctor_m362897736 (U3CGoLevelU3Ec__Iterator60_t233841331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LoadLevelName/<GoLevel>c__Iterator60::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGoLevelU3Ec__Iterator60_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2856252180 (U3CGoLevelU3Ec__Iterator60_t233841331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LoadLevelName/<GoLevel>c__Iterator60::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGoLevelU3Ec__Iterator60_System_Collections_IEnumerator_get_Current_m3737231016 (U3CGoLevelU3Ec__Iterator60_t233841331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LoadLevelName/<GoLevel>c__Iterator60::MoveNext()
extern "C"  bool U3CGoLevelU3Ec__Iterator60_MoveNext_m3548816788 (U3CGoLevelU3Ec__Iterator60_t233841331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadLevelName/<GoLevel>c__Iterator60::Dispose()
extern "C"  void U3CGoLevelU3Ec__Iterator60_Dispose_m750656389 (U3CGoLevelU3Ec__Iterator60_t233841331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadLevelName/<GoLevel>c__Iterator60::Reset()
extern "C"  void U3CGoLevelU3Ec__Iterator60_Reset_m2304297973 (U3CGoLevelU3Ec__Iterator60_t233841331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
