﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WindowControlListScript
struct WindowControlListScript_t2639301142;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Type
struct Type_t;
// MagicTVWindowAlertControllerScript
struct MagicTVWindowAlertControllerScript_t1344644260;
// MagicTVWindowConfirmControllerScript
struct MagicTVWindowConfirmControllerScript_t165107144;
// MagicTVWindowPromptControllerScript
struct MagicTVWindowPromptControllerScript_t1472770442;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void WindowControlListScript::.ctor()
extern "C"  void WindowControlListScript__ctor_m2819149333 (WindowControlListScript_t2639301142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WindowControlListScript::clearTable()
extern "C"  void WindowControlListScript_clearTable_m2879173424 (WindowControlListScript_t2639301142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WindowControlListScript::Start()
extern "C"  void WindowControlListScript_Start_m1766287125 (WindowControlListScript_t2639301142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WindowControlListScript::Update()
extern "C"  void WindowControlListScript_Update_m3221145496 (WindowControlListScript_t2639301142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WindowControlListScript::popUpClose()
extern "C"  void WindowControlListScript_popUpClose_m448151419 (WindowControlListScript_t2639301142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject WindowControlListScript::_instantiatePopUp(System.Type)
extern "C"  GameObject_t3674682005 * WindowControlListScript__instantiatePopUp_m3356897840 (WindowControlListScript_t2639301142 * __this, Type_t * ___popUpType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTVWindowAlertControllerScript WindowControlListScript::InstantiateAlertControllerScript()
extern "C"  MagicTVWindowAlertControllerScript_t1344644260 * WindowControlListScript_InstantiateAlertControllerScript_m4148213135 (WindowControlListScript_t2639301142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTVWindowConfirmControllerScript WindowControlListScript::InstantiateConfirmControllerScript()
extern "C"  MagicTVWindowConfirmControllerScript_t165107144 * WindowControlListScript_InstantiateConfirmControllerScript_m2905168535 (WindowControlListScript_t2639301142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTVWindowPromptControllerScript WindowControlListScript::InstantiatePromptControllerScript()
extern "C"  MagicTVWindowPromptControllerScript_t1472770442 * WindowControlListScript_InstantiatePromptControllerScript_m747484753 (WindowControlListScript_t2639301142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WindowControlListScript::AddPopUpToQueue(UnityEngine.GameObject)
extern "C"  void WindowControlListScript_AddPopUpToQueue_m1920688918 (WindowControlListScript_t2639301142 * __this, GameObject_t3674682005 * ___popUp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
