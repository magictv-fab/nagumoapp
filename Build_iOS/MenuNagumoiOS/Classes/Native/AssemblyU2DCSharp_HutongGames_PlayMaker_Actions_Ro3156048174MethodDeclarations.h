﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RotateGUI
struct RotateGUI_t3156048174;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RotateGUI::.ctor()
extern "C"  void RotateGUI__ctor_m1568276424 (RotateGUI_t3156048174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RotateGUI::Reset()
extern "C"  void RotateGUI_Reset_m3509676661 (RotateGUI_t3156048174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RotateGUI::OnGUI()
extern "C"  void RotateGUI_OnGUI_m1063675074 (RotateGUI_t3156048174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RotateGUI::OnUpdate()
extern "C"  void RotateGUI_OnUpdate_m1620683908 (RotateGUI_t3156048174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
