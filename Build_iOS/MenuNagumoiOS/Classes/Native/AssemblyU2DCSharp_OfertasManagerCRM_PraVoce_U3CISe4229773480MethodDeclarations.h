﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertasManagerCRM_PraVoce/<ISendStatus>c__IteratorC
struct U3CISendStatusU3Ec__IteratorC_t4229773480;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OfertasManagerCRM_PraVoce/<ISendStatus>c__IteratorC::.ctor()
extern "C"  void U3CISendStatusU3Ec__IteratorC__ctor_m577533891 (U3CISendStatusU3Ec__IteratorC_t4229773480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManagerCRM_PraVoce/<ISendStatus>c__IteratorC::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CISendStatusU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m867462127 (U3CISendStatusU3Ec__IteratorC_t4229773480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManagerCRM_PraVoce/<ISendStatus>c__IteratorC::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CISendStatusU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m767700355 (U3CISendStatusU3Ec__IteratorC_t4229773480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OfertasManagerCRM_PraVoce/<ISendStatus>c__IteratorC::MoveNext()
extern "C"  bool U3CISendStatusU3Ec__IteratorC_MoveNext_m3379858769 (U3CISendStatusU3Ec__IteratorC_t4229773480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_PraVoce/<ISendStatus>c__IteratorC::Dispose()
extern "C"  void U3CISendStatusU3Ec__IteratorC_Dispose_m857571136 (U3CISendStatusU3Ec__IteratorC_t4229773480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_PraVoce/<ISendStatus>c__IteratorC::Reset()
extern "C"  void U3CISendStatusU3Ec__IteratorC_Reset_m2518934128 (U3CISendStatusU3Ec__IteratorC_t4229773480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
