﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.Type>
struct Dictionary_2_t3683564144;
// System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>
struct Dictionary_2_t2672984508;
// System.Collections.Generic.Dictionary`2<System.Type,System.Int32>
struct Dictionary_2_t1259260985;
// HutongGames.PlayMaker.Fsm
struct Fsm_t1527112426;
// HutongGames.PlayMaker.FsmState
struct FsmState_t2146334067;
// HutongGames.PlayMaker.FsmStateAction
struct FsmStateAction_t2366529033;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// System.Collections.Generic.List`1<System.Reflection.FieldInfo>
struct List_1_t1046271522;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Collections.Generic.List`1<System.Boolean>
struct List_1_t1844984270;
// System.Collections.Generic.List`1<UnityEngine.Object>
struct List_1_t144696915;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>
struct List_1_t3065333419;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmOwnerDefault>
struct List_1_t1620082664;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmAnimationCurve>
struct List_1_t4054181541;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>
struct List_1_t353063272;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>
struct List_1_t4154693685;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEventTarget>
struct List_1_t3192090493;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmProperty>
struct List_1_t1000377263;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.LayoutOption>
struct List_1_t2333180753;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmString>
struct List_1_t2321044203;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmObject>
struct List_1_t2189661721;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmVar>
struct List_1_t2964336089;
// System.Collections.Generic.List`1<System.Byte>
struct List_1_t4230795212;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>
struct List_1_t4040850731;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.ActionData
struct  ActionData_t3958426178  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.String> HutongGames.PlayMaker.ActionData::actionNames
	List_1_t1375417109 * ___actionNames_11;
	// System.Collections.Generic.List`1<System.String> HutongGames.PlayMaker.ActionData::customNames
	List_1_t1375417109 * ___customNames_12;
	// System.Collections.Generic.List`1<System.Boolean> HutongGames.PlayMaker.ActionData::actionEnabled
	List_1_t1844984270 * ___actionEnabled_13;
	// System.Collections.Generic.List`1<System.Boolean> HutongGames.PlayMaker.ActionData::actionIsOpen
	List_1_t1844984270 * ___actionIsOpen_14;
	// System.Collections.Generic.List`1<System.Int32> HutongGames.PlayMaker.ActionData::actionStartIndex
	List_1_t2522024052 * ___actionStartIndex_15;
	// System.Collections.Generic.List`1<System.Int32> HutongGames.PlayMaker.ActionData::actionHashCodes
	List_1_t2522024052 * ___actionHashCodes_16;
	// System.Collections.Generic.List`1<UnityEngine.Object> HutongGames.PlayMaker.ActionData::unityObjectParams
	List_1_t144696915 * ___unityObjectParams_17;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject> HutongGames.PlayMaker.ActionData::fsmGameObjectParams
	List_1_t3065333419 * ___fsmGameObjectParams_18;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmOwnerDefault> HutongGames.PlayMaker.ActionData::fsmOwnerDefaultParams
	List_1_t1620082664 * ___fsmOwnerDefaultParams_19;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmAnimationCurve> HutongGames.PlayMaker.ActionData::animationCurveParams
	List_1_t4054181541 * ___animationCurveParams_20;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall> HutongGames.PlayMaker.ActionData::functionCallParams
	List_1_t353063272 * ___functionCallParams_21;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl> HutongGames.PlayMaker.ActionData::fsmTemplateControlParams
	List_1_t4154693685 * ___fsmTemplateControlParams_22;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEventTarget> HutongGames.PlayMaker.ActionData::fsmEventTargetParams
	List_1_t3192090493 * ___fsmEventTargetParams_23;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmProperty> HutongGames.PlayMaker.ActionData::fsmPropertyParams
	List_1_t1000377263 * ___fsmPropertyParams_24;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.LayoutOption> HutongGames.PlayMaker.ActionData::layoutOptionParams
	List_1_t2333180753 * ___layoutOptionParams_25;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmString> HutongGames.PlayMaker.ActionData::fsmStringParams
	List_1_t2321044203 * ___fsmStringParams_26;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmObject> HutongGames.PlayMaker.ActionData::fsmObjectParams
	List_1_t2189661721 * ___fsmObjectParams_27;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmVar> HutongGames.PlayMaker.ActionData::fsmVarParams
	List_1_t2964336089 * ___fsmVarParams_28;
	// System.Collections.Generic.List`1<System.Byte> HutongGames.PlayMaker.ActionData::byteData
	List_1_t4230795212 * ___byteData_29;
	// System.Byte[] HutongGames.PlayMaker.ActionData::byteDataAsArray
	ByteU5BU5D_t4260760469* ___byteDataAsArray_30;
	// System.Collections.Generic.List`1<System.Int32> HutongGames.PlayMaker.ActionData::arrayParamSizes
	List_1_t2522024052 * ___arrayParamSizes_31;
	// System.Collections.Generic.List`1<System.String> HutongGames.PlayMaker.ActionData::arrayParamTypes
	List_1_t1375417109 * ___arrayParamTypes_32;
	// System.Collections.Generic.List`1<System.Int32> HutongGames.PlayMaker.ActionData::customTypeSizes
	List_1_t2522024052 * ___customTypeSizes_33;
	// System.Collections.Generic.List`1<System.String> HutongGames.PlayMaker.ActionData::customTypeNames
	List_1_t1375417109 * ___customTypeNames_34;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType> HutongGames.PlayMaker.ActionData::paramDataType
	List_1_t4040850731 * ___paramDataType_35;
	// System.Collections.Generic.List`1<System.String> HutongGames.PlayMaker.ActionData::paramName
	List_1_t1375417109 * ___paramName_36;
	// System.Collections.Generic.List`1<System.Int32> HutongGames.PlayMaker.ActionData::paramDataPos
	List_1_t2522024052 * ___paramDataPos_37;
	// System.Collections.Generic.List`1<System.Int32> HutongGames.PlayMaker.ActionData::paramByteDataSize
	List_1_t2522024052 * ___paramByteDataSize_38;
	// System.Int32 HutongGames.PlayMaker.ActionData::nextParamIndex
	int32_t ___nextParamIndex_39;

public:
	inline static int32_t get_offset_of_actionNames_11() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___actionNames_11)); }
	inline List_1_t1375417109 * get_actionNames_11() const { return ___actionNames_11; }
	inline List_1_t1375417109 ** get_address_of_actionNames_11() { return &___actionNames_11; }
	inline void set_actionNames_11(List_1_t1375417109 * value)
	{
		___actionNames_11 = value;
		Il2CppCodeGenWriteBarrier(&___actionNames_11, value);
	}

	inline static int32_t get_offset_of_customNames_12() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___customNames_12)); }
	inline List_1_t1375417109 * get_customNames_12() const { return ___customNames_12; }
	inline List_1_t1375417109 ** get_address_of_customNames_12() { return &___customNames_12; }
	inline void set_customNames_12(List_1_t1375417109 * value)
	{
		___customNames_12 = value;
		Il2CppCodeGenWriteBarrier(&___customNames_12, value);
	}

	inline static int32_t get_offset_of_actionEnabled_13() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___actionEnabled_13)); }
	inline List_1_t1844984270 * get_actionEnabled_13() const { return ___actionEnabled_13; }
	inline List_1_t1844984270 ** get_address_of_actionEnabled_13() { return &___actionEnabled_13; }
	inline void set_actionEnabled_13(List_1_t1844984270 * value)
	{
		___actionEnabled_13 = value;
		Il2CppCodeGenWriteBarrier(&___actionEnabled_13, value);
	}

	inline static int32_t get_offset_of_actionIsOpen_14() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___actionIsOpen_14)); }
	inline List_1_t1844984270 * get_actionIsOpen_14() const { return ___actionIsOpen_14; }
	inline List_1_t1844984270 ** get_address_of_actionIsOpen_14() { return &___actionIsOpen_14; }
	inline void set_actionIsOpen_14(List_1_t1844984270 * value)
	{
		___actionIsOpen_14 = value;
		Il2CppCodeGenWriteBarrier(&___actionIsOpen_14, value);
	}

	inline static int32_t get_offset_of_actionStartIndex_15() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___actionStartIndex_15)); }
	inline List_1_t2522024052 * get_actionStartIndex_15() const { return ___actionStartIndex_15; }
	inline List_1_t2522024052 ** get_address_of_actionStartIndex_15() { return &___actionStartIndex_15; }
	inline void set_actionStartIndex_15(List_1_t2522024052 * value)
	{
		___actionStartIndex_15 = value;
		Il2CppCodeGenWriteBarrier(&___actionStartIndex_15, value);
	}

	inline static int32_t get_offset_of_actionHashCodes_16() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___actionHashCodes_16)); }
	inline List_1_t2522024052 * get_actionHashCodes_16() const { return ___actionHashCodes_16; }
	inline List_1_t2522024052 ** get_address_of_actionHashCodes_16() { return &___actionHashCodes_16; }
	inline void set_actionHashCodes_16(List_1_t2522024052 * value)
	{
		___actionHashCodes_16 = value;
		Il2CppCodeGenWriteBarrier(&___actionHashCodes_16, value);
	}

	inline static int32_t get_offset_of_unityObjectParams_17() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___unityObjectParams_17)); }
	inline List_1_t144696915 * get_unityObjectParams_17() const { return ___unityObjectParams_17; }
	inline List_1_t144696915 ** get_address_of_unityObjectParams_17() { return &___unityObjectParams_17; }
	inline void set_unityObjectParams_17(List_1_t144696915 * value)
	{
		___unityObjectParams_17 = value;
		Il2CppCodeGenWriteBarrier(&___unityObjectParams_17, value);
	}

	inline static int32_t get_offset_of_fsmGameObjectParams_18() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___fsmGameObjectParams_18)); }
	inline List_1_t3065333419 * get_fsmGameObjectParams_18() const { return ___fsmGameObjectParams_18; }
	inline List_1_t3065333419 ** get_address_of_fsmGameObjectParams_18() { return &___fsmGameObjectParams_18; }
	inline void set_fsmGameObjectParams_18(List_1_t3065333419 * value)
	{
		___fsmGameObjectParams_18 = value;
		Il2CppCodeGenWriteBarrier(&___fsmGameObjectParams_18, value);
	}

	inline static int32_t get_offset_of_fsmOwnerDefaultParams_19() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___fsmOwnerDefaultParams_19)); }
	inline List_1_t1620082664 * get_fsmOwnerDefaultParams_19() const { return ___fsmOwnerDefaultParams_19; }
	inline List_1_t1620082664 ** get_address_of_fsmOwnerDefaultParams_19() { return &___fsmOwnerDefaultParams_19; }
	inline void set_fsmOwnerDefaultParams_19(List_1_t1620082664 * value)
	{
		___fsmOwnerDefaultParams_19 = value;
		Il2CppCodeGenWriteBarrier(&___fsmOwnerDefaultParams_19, value);
	}

	inline static int32_t get_offset_of_animationCurveParams_20() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___animationCurveParams_20)); }
	inline List_1_t4054181541 * get_animationCurveParams_20() const { return ___animationCurveParams_20; }
	inline List_1_t4054181541 ** get_address_of_animationCurveParams_20() { return &___animationCurveParams_20; }
	inline void set_animationCurveParams_20(List_1_t4054181541 * value)
	{
		___animationCurveParams_20 = value;
		Il2CppCodeGenWriteBarrier(&___animationCurveParams_20, value);
	}

	inline static int32_t get_offset_of_functionCallParams_21() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___functionCallParams_21)); }
	inline List_1_t353063272 * get_functionCallParams_21() const { return ___functionCallParams_21; }
	inline List_1_t353063272 ** get_address_of_functionCallParams_21() { return &___functionCallParams_21; }
	inline void set_functionCallParams_21(List_1_t353063272 * value)
	{
		___functionCallParams_21 = value;
		Il2CppCodeGenWriteBarrier(&___functionCallParams_21, value);
	}

	inline static int32_t get_offset_of_fsmTemplateControlParams_22() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___fsmTemplateControlParams_22)); }
	inline List_1_t4154693685 * get_fsmTemplateControlParams_22() const { return ___fsmTemplateControlParams_22; }
	inline List_1_t4154693685 ** get_address_of_fsmTemplateControlParams_22() { return &___fsmTemplateControlParams_22; }
	inline void set_fsmTemplateControlParams_22(List_1_t4154693685 * value)
	{
		___fsmTemplateControlParams_22 = value;
		Il2CppCodeGenWriteBarrier(&___fsmTemplateControlParams_22, value);
	}

	inline static int32_t get_offset_of_fsmEventTargetParams_23() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___fsmEventTargetParams_23)); }
	inline List_1_t3192090493 * get_fsmEventTargetParams_23() const { return ___fsmEventTargetParams_23; }
	inline List_1_t3192090493 ** get_address_of_fsmEventTargetParams_23() { return &___fsmEventTargetParams_23; }
	inline void set_fsmEventTargetParams_23(List_1_t3192090493 * value)
	{
		___fsmEventTargetParams_23 = value;
		Il2CppCodeGenWriteBarrier(&___fsmEventTargetParams_23, value);
	}

	inline static int32_t get_offset_of_fsmPropertyParams_24() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___fsmPropertyParams_24)); }
	inline List_1_t1000377263 * get_fsmPropertyParams_24() const { return ___fsmPropertyParams_24; }
	inline List_1_t1000377263 ** get_address_of_fsmPropertyParams_24() { return &___fsmPropertyParams_24; }
	inline void set_fsmPropertyParams_24(List_1_t1000377263 * value)
	{
		___fsmPropertyParams_24 = value;
		Il2CppCodeGenWriteBarrier(&___fsmPropertyParams_24, value);
	}

	inline static int32_t get_offset_of_layoutOptionParams_25() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___layoutOptionParams_25)); }
	inline List_1_t2333180753 * get_layoutOptionParams_25() const { return ___layoutOptionParams_25; }
	inline List_1_t2333180753 ** get_address_of_layoutOptionParams_25() { return &___layoutOptionParams_25; }
	inline void set_layoutOptionParams_25(List_1_t2333180753 * value)
	{
		___layoutOptionParams_25 = value;
		Il2CppCodeGenWriteBarrier(&___layoutOptionParams_25, value);
	}

	inline static int32_t get_offset_of_fsmStringParams_26() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___fsmStringParams_26)); }
	inline List_1_t2321044203 * get_fsmStringParams_26() const { return ___fsmStringParams_26; }
	inline List_1_t2321044203 ** get_address_of_fsmStringParams_26() { return &___fsmStringParams_26; }
	inline void set_fsmStringParams_26(List_1_t2321044203 * value)
	{
		___fsmStringParams_26 = value;
		Il2CppCodeGenWriteBarrier(&___fsmStringParams_26, value);
	}

	inline static int32_t get_offset_of_fsmObjectParams_27() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___fsmObjectParams_27)); }
	inline List_1_t2189661721 * get_fsmObjectParams_27() const { return ___fsmObjectParams_27; }
	inline List_1_t2189661721 ** get_address_of_fsmObjectParams_27() { return &___fsmObjectParams_27; }
	inline void set_fsmObjectParams_27(List_1_t2189661721 * value)
	{
		___fsmObjectParams_27 = value;
		Il2CppCodeGenWriteBarrier(&___fsmObjectParams_27, value);
	}

	inline static int32_t get_offset_of_fsmVarParams_28() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___fsmVarParams_28)); }
	inline List_1_t2964336089 * get_fsmVarParams_28() const { return ___fsmVarParams_28; }
	inline List_1_t2964336089 ** get_address_of_fsmVarParams_28() { return &___fsmVarParams_28; }
	inline void set_fsmVarParams_28(List_1_t2964336089 * value)
	{
		___fsmVarParams_28 = value;
		Il2CppCodeGenWriteBarrier(&___fsmVarParams_28, value);
	}

	inline static int32_t get_offset_of_byteData_29() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___byteData_29)); }
	inline List_1_t4230795212 * get_byteData_29() const { return ___byteData_29; }
	inline List_1_t4230795212 ** get_address_of_byteData_29() { return &___byteData_29; }
	inline void set_byteData_29(List_1_t4230795212 * value)
	{
		___byteData_29 = value;
		Il2CppCodeGenWriteBarrier(&___byteData_29, value);
	}

	inline static int32_t get_offset_of_byteDataAsArray_30() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___byteDataAsArray_30)); }
	inline ByteU5BU5D_t4260760469* get_byteDataAsArray_30() const { return ___byteDataAsArray_30; }
	inline ByteU5BU5D_t4260760469** get_address_of_byteDataAsArray_30() { return &___byteDataAsArray_30; }
	inline void set_byteDataAsArray_30(ByteU5BU5D_t4260760469* value)
	{
		___byteDataAsArray_30 = value;
		Il2CppCodeGenWriteBarrier(&___byteDataAsArray_30, value);
	}

	inline static int32_t get_offset_of_arrayParamSizes_31() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___arrayParamSizes_31)); }
	inline List_1_t2522024052 * get_arrayParamSizes_31() const { return ___arrayParamSizes_31; }
	inline List_1_t2522024052 ** get_address_of_arrayParamSizes_31() { return &___arrayParamSizes_31; }
	inline void set_arrayParamSizes_31(List_1_t2522024052 * value)
	{
		___arrayParamSizes_31 = value;
		Il2CppCodeGenWriteBarrier(&___arrayParamSizes_31, value);
	}

	inline static int32_t get_offset_of_arrayParamTypes_32() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___arrayParamTypes_32)); }
	inline List_1_t1375417109 * get_arrayParamTypes_32() const { return ___arrayParamTypes_32; }
	inline List_1_t1375417109 ** get_address_of_arrayParamTypes_32() { return &___arrayParamTypes_32; }
	inline void set_arrayParamTypes_32(List_1_t1375417109 * value)
	{
		___arrayParamTypes_32 = value;
		Il2CppCodeGenWriteBarrier(&___arrayParamTypes_32, value);
	}

	inline static int32_t get_offset_of_customTypeSizes_33() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___customTypeSizes_33)); }
	inline List_1_t2522024052 * get_customTypeSizes_33() const { return ___customTypeSizes_33; }
	inline List_1_t2522024052 ** get_address_of_customTypeSizes_33() { return &___customTypeSizes_33; }
	inline void set_customTypeSizes_33(List_1_t2522024052 * value)
	{
		___customTypeSizes_33 = value;
		Il2CppCodeGenWriteBarrier(&___customTypeSizes_33, value);
	}

	inline static int32_t get_offset_of_customTypeNames_34() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___customTypeNames_34)); }
	inline List_1_t1375417109 * get_customTypeNames_34() const { return ___customTypeNames_34; }
	inline List_1_t1375417109 ** get_address_of_customTypeNames_34() { return &___customTypeNames_34; }
	inline void set_customTypeNames_34(List_1_t1375417109 * value)
	{
		___customTypeNames_34 = value;
		Il2CppCodeGenWriteBarrier(&___customTypeNames_34, value);
	}

	inline static int32_t get_offset_of_paramDataType_35() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___paramDataType_35)); }
	inline List_1_t4040850731 * get_paramDataType_35() const { return ___paramDataType_35; }
	inline List_1_t4040850731 ** get_address_of_paramDataType_35() { return &___paramDataType_35; }
	inline void set_paramDataType_35(List_1_t4040850731 * value)
	{
		___paramDataType_35 = value;
		Il2CppCodeGenWriteBarrier(&___paramDataType_35, value);
	}

	inline static int32_t get_offset_of_paramName_36() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___paramName_36)); }
	inline List_1_t1375417109 * get_paramName_36() const { return ___paramName_36; }
	inline List_1_t1375417109 ** get_address_of_paramName_36() { return &___paramName_36; }
	inline void set_paramName_36(List_1_t1375417109 * value)
	{
		___paramName_36 = value;
		Il2CppCodeGenWriteBarrier(&___paramName_36, value);
	}

	inline static int32_t get_offset_of_paramDataPos_37() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___paramDataPos_37)); }
	inline List_1_t2522024052 * get_paramDataPos_37() const { return ___paramDataPos_37; }
	inline List_1_t2522024052 ** get_address_of_paramDataPos_37() { return &___paramDataPos_37; }
	inline void set_paramDataPos_37(List_1_t2522024052 * value)
	{
		___paramDataPos_37 = value;
		Il2CppCodeGenWriteBarrier(&___paramDataPos_37, value);
	}

	inline static int32_t get_offset_of_paramByteDataSize_38() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___paramByteDataSize_38)); }
	inline List_1_t2522024052 * get_paramByteDataSize_38() const { return ___paramByteDataSize_38; }
	inline List_1_t2522024052 ** get_address_of_paramByteDataSize_38() { return &___paramByteDataSize_38; }
	inline void set_paramByteDataSize_38(List_1_t2522024052 * value)
	{
		___paramByteDataSize_38 = value;
		Il2CppCodeGenWriteBarrier(&___paramByteDataSize_38, value);
	}

	inline static int32_t get_offset_of_nextParamIndex_39() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___nextParamIndex_39)); }
	inline int32_t get_nextParamIndex_39() const { return ___nextParamIndex_39; }
	inline int32_t* get_address_of_nextParamIndex_39() { return &___nextParamIndex_39; }
	inline void set_nextParamIndex_39(int32_t value)
	{
		___nextParamIndex_39 = value;
	}
};

struct ActionData_t3958426178_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Type> HutongGames.PlayMaker.ActionData::ActionTypeLookup
	Dictionary_2_t3683564144 * ___ActionTypeLookup_0;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]> HutongGames.PlayMaker.ActionData::ActionFieldsLookup
	Dictionary_2_t2672984508 * ___ActionFieldsLookup_1;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Int32> HutongGames.PlayMaker.ActionData::ActionHashCodeLookup
	Dictionary_2_t1259260985 * ___ActionHashCodeLookup_2;
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.ActionData::currentFsm
	Fsm_t1527112426 * ___currentFsm_3;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.ActionData::currentState
	FsmState_t2146334067 * ___currentState_4;
	// HutongGames.PlayMaker.FsmStateAction HutongGames.PlayMaker.ActionData::currentAction
	FsmStateAction_t2366529033 * ___currentAction_5;
	// System.Int32 HutongGames.PlayMaker.ActionData::currentActionIndex
	int32_t ___currentActionIndex_6;
	// System.String HutongGames.PlayMaker.ActionData::currentParameter
	String_t* ___currentParameter_7;
	// System.Boolean HutongGames.PlayMaker.ActionData::resaveActionData
	bool ___resaveActionData_8;
	// System.Collections.Generic.List`1<System.Int32> HutongGames.PlayMaker.ActionData::UsedIndices
	List_1_t2522024052 * ___UsedIndices_9;
	// System.Collections.Generic.List`1<System.Reflection.FieldInfo> HutongGames.PlayMaker.ActionData::InitFields
	List_1_t1046271522 * ___InitFields_10;

public:
	inline static int32_t get_offset_of_ActionTypeLookup_0() { return static_cast<int32_t>(offsetof(ActionData_t3958426178_StaticFields, ___ActionTypeLookup_0)); }
	inline Dictionary_2_t3683564144 * get_ActionTypeLookup_0() const { return ___ActionTypeLookup_0; }
	inline Dictionary_2_t3683564144 ** get_address_of_ActionTypeLookup_0() { return &___ActionTypeLookup_0; }
	inline void set_ActionTypeLookup_0(Dictionary_2_t3683564144 * value)
	{
		___ActionTypeLookup_0 = value;
		Il2CppCodeGenWriteBarrier(&___ActionTypeLookup_0, value);
	}

	inline static int32_t get_offset_of_ActionFieldsLookup_1() { return static_cast<int32_t>(offsetof(ActionData_t3958426178_StaticFields, ___ActionFieldsLookup_1)); }
	inline Dictionary_2_t2672984508 * get_ActionFieldsLookup_1() const { return ___ActionFieldsLookup_1; }
	inline Dictionary_2_t2672984508 ** get_address_of_ActionFieldsLookup_1() { return &___ActionFieldsLookup_1; }
	inline void set_ActionFieldsLookup_1(Dictionary_2_t2672984508 * value)
	{
		___ActionFieldsLookup_1 = value;
		Il2CppCodeGenWriteBarrier(&___ActionFieldsLookup_1, value);
	}

	inline static int32_t get_offset_of_ActionHashCodeLookup_2() { return static_cast<int32_t>(offsetof(ActionData_t3958426178_StaticFields, ___ActionHashCodeLookup_2)); }
	inline Dictionary_2_t1259260985 * get_ActionHashCodeLookup_2() const { return ___ActionHashCodeLookup_2; }
	inline Dictionary_2_t1259260985 ** get_address_of_ActionHashCodeLookup_2() { return &___ActionHashCodeLookup_2; }
	inline void set_ActionHashCodeLookup_2(Dictionary_2_t1259260985 * value)
	{
		___ActionHashCodeLookup_2 = value;
		Il2CppCodeGenWriteBarrier(&___ActionHashCodeLookup_2, value);
	}

	inline static int32_t get_offset_of_currentFsm_3() { return static_cast<int32_t>(offsetof(ActionData_t3958426178_StaticFields, ___currentFsm_3)); }
	inline Fsm_t1527112426 * get_currentFsm_3() const { return ___currentFsm_3; }
	inline Fsm_t1527112426 ** get_address_of_currentFsm_3() { return &___currentFsm_3; }
	inline void set_currentFsm_3(Fsm_t1527112426 * value)
	{
		___currentFsm_3 = value;
		Il2CppCodeGenWriteBarrier(&___currentFsm_3, value);
	}

	inline static int32_t get_offset_of_currentState_4() { return static_cast<int32_t>(offsetof(ActionData_t3958426178_StaticFields, ___currentState_4)); }
	inline FsmState_t2146334067 * get_currentState_4() const { return ___currentState_4; }
	inline FsmState_t2146334067 ** get_address_of_currentState_4() { return &___currentState_4; }
	inline void set_currentState_4(FsmState_t2146334067 * value)
	{
		___currentState_4 = value;
		Il2CppCodeGenWriteBarrier(&___currentState_4, value);
	}

	inline static int32_t get_offset_of_currentAction_5() { return static_cast<int32_t>(offsetof(ActionData_t3958426178_StaticFields, ___currentAction_5)); }
	inline FsmStateAction_t2366529033 * get_currentAction_5() const { return ___currentAction_5; }
	inline FsmStateAction_t2366529033 ** get_address_of_currentAction_5() { return &___currentAction_5; }
	inline void set_currentAction_5(FsmStateAction_t2366529033 * value)
	{
		___currentAction_5 = value;
		Il2CppCodeGenWriteBarrier(&___currentAction_5, value);
	}

	inline static int32_t get_offset_of_currentActionIndex_6() { return static_cast<int32_t>(offsetof(ActionData_t3958426178_StaticFields, ___currentActionIndex_6)); }
	inline int32_t get_currentActionIndex_6() const { return ___currentActionIndex_6; }
	inline int32_t* get_address_of_currentActionIndex_6() { return &___currentActionIndex_6; }
	inline void set_currentActionIndex_6(int32_t value)
	{
		___currentActionIndex_6 = value;
	}

	inline static int32_t get_offset_of_currentParameter_7() { return static_cast<int32_t>(offsetof(ActionData_t3958426178_StaticFields, ___currentParameter_7)); }
	inline String_t* get_currentParameter_7() const { return ___currentParameter_7; }
	inline String_t** get_address_of_currentParameter_7() { return &___currentParameter_7; }
	inline void set_currentParameter_7(String_t* value)
	{
		___currentParameter_7 = value;
		Il2CppCodeGenWriteBarrier(&___currentParameter_7, value);
	}

	inline static int32_t get_offset_of_resaveActionData_8() { return static_cast<int32_t>(offsetof(ActionData_t3958426178_StaticFields, ___resaveActionData_8)); }
	inline bool get_resaveActionData_8() const { return ___resaveActionData_8; }
	inline bool* get_address_of_resaveActionData_8() { return &___resaveActionData_8; }
	inline void set_resaveActionData_8(bool value)
	{
		___resaveActionData_8 = value;
	}

	inline static int32_t get_offset_of_UsedIndices_9() { return static_cast<int32_t>(offsetof(ActionData_t3958426178_StaticFields, ___UsedIndices_9)); }
	inline List_1_t2522024052 * get_UsedIndices_9() const { return ___UsedIndices_9; }
	inline List_1_t2522024052 ** get_address_of_UsedIndices_9() { return &___UsedIndices_9; }
	inline void set_UsedIndices_9(List_1_t2522024052 * value)
	{
		___UsedIndices_9 = value;
		Il2CppCodeGenWriteBarrier(&___UsedIndices_9, value);
	}

	inline static int32_t get_offset_of_InitFields_10() { return static_cast<int32_t>(offsetof(ActionData_t3958426178_StaticFields, ___InitFields_10)); }
	inline List_1_t1046271522 * get_InitFields_10() const { return ___InitFields_10; }
	inline List_1_t1046271522 ** get_address_of_InitFields_10() { return &___InitFields_10; }
	inline void set_InitFields_10(List_1_t1046271522 * value)
	{
		___InitFields_10 = value;
		Il2CppCodeGenWriteBarrier(&___InitFields_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
