﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenMoveBy
struct iTweenMoveBy_t469656626;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenMoveBy::.ctor()
extern "C"  void iTweenMoveBy__ctor_m197714548 (iTweenMoveBy_t469656626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveBy::Reset()
extern "C"  void iTweenMoveBy_Reset_m2139114785 (iTweenMoveBy_t469656626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveBy::OnEnter()
extern "C"  void iTweenMoveBy_OnEnter_m416848203 (iTweenMoveBy_t469656626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveBy::OnExit()
extern "C"  void iTweenMoveBy_OnExit_m3347502317 (iTweenMoveBy_t469656626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveBy::DoiTween()
extern "C"  void iTweenMoveBy_DoiTween_m1488524637 (iTweenMoveBy_t469656626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
