﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutAction
struct GUILayoutAction_t2615417833;
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t2977405297;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutAction::.ctor()
extern "C"  void GUILayoutAction__ctor_m2599973229 (GUILayoutAction_t2615417833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUILayoutOption[] HutongGames.PlayMaker.Actions.GUILayoutAction::get_LayoutOptions()
extern "C"  GUILayoutOptionU5BU5D_t2977405297* GUILayoutAction_get_LayoutOptions_m1855901188 (GUILayoutAction_t2615417833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutAction::Reset()
extern "C"  void GUILayoutAction_Reset_m246406170 (GUILayoutAction_t2615417833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
