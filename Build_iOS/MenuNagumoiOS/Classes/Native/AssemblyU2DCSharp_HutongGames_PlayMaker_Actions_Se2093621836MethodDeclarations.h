﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAnimatorIKGoal
struct SetAnimatorIKGoal_t2093621836;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAnimatorIKGoal::.ctor()
extern "C"  void SetAnimatorIKGoal__ctor_m2050314346 (SetAnimatorIKGoal_t2093621836 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorIKGoal::Reset()
extern "C"  void SetAnimatorIKGoal_Reset_m3991714583 (SetAnimatorIKGoal_t2093621836 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorIKGoal::OnEnter()
extern "C"  void SetAnimatorIKGoal_OnEnter_m2648793537 (SetAnimatorIKGoal_t2093621836 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorIKGoal::OnAnimatorIKEvent(System.Int32)
extern "C"  void SetAnimatorIKGoal_OnAnimatorIKEvent_m8814283 (SetAnimatorIKGoal_t2093621836 * __this, int32_t ___layer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorIKGoal::DoSetIKGoal()
extern "C"  void SetAnimatorIKGoal_DoSetIKGoal_m1243623220 (SetAnimatorIKGoal_t2093621836 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorIKGoal::OnExit()
extern "C"  void SetAnimatorIKGoal_OnExit_m648553911 (SetAnimatorIKGoal_t2093621836 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
