﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniWebViewAndroidStaticListener
struct UniWebViewAndroidStaticListener_t2819366888;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void UniWebViewAndroidStaticListener::.ctor()
extern "C"  void UniWebViewAndroidStaticListener__ctor_m3801234563 (UniWebViewAndroidStaticListener_t2819366888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewAndroidStaticListener::Awake()
extern "C"  void UniWebViewAndroidStaticListener_Awake_m4038839782 (UniWebViewAndroidStaticListener_t2819366888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewAndroidStaticListener::OnJavaMessage(System.String)
extern "C"  void UniWebViewAndroidStaticListener_OnJavaMessage_m3100397083 (UniWebViewAndroidStaticListener_t2819366888 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
