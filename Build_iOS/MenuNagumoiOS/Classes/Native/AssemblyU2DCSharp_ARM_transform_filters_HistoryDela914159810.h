﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.camera.abstracts.TrackEventDispatcherAbstract
struct TrackEventDispatcherAbstract_t3123780554;

#include "AssemblyU2DCSharp_ARM_transform_filters_abstracts_4158746314.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.transform.filters.HistoryDelaySituationCameraFilter
struct  HistoryDelaySituationCameraFilter_t914159810  : public TransformPositionAngleFilterAbstract_t4158746314
{
public:
	// UnityEngine.Quaternion ARM.transform.filters.HistoryDelaySituationCameraFilter::_lastTargetAngle
	Quaternion_t1553702882  ____lastTargetAngle_15;
	// UnityEngine.Vector3 ARM.transform.filters.HistoryDelaySituationCameraFilter::_lastTargetPosition
	Vector3_t4282066566  ____lastTargetPosition_16;
	// ARM.camera.abstracts.TrackEventDispatcherAbstract ARM.transform.filters.HistoryDelaySituationCameraFilter::trackerEventPlugin
	TrackEventDispatcherAbstract_t3123780554 * ___trackerEventPlugin_17;
	// System.Boolean ARM.transform.filters.HistoryDelaySituationCameraFilter::_isActive
	bool ____isActive_18;
	// System.Single ARM.transform.filters.HistoryDelaySituationCameraFilter::delayTime
	float ___delayTime_19;
	// System.Single ARM.transform.filters.HistoryDelaySituationCameraFilter::_totalTimeElapsed
	float ____totalTimeElapsed_20;
	// System.Boolean ARM.transform.filters.HistoryDelaySituationCameraFilter::_timer
	bool ____timer_21;
	// System.Boolean ARM.transform.filters.HistoryDelaySituationCameraFilter::_timerEnabled
	bool ____timerEnabled_22;

public:
	inline static int32_t get_offset_of__lastTargetAngle_15() { return static_cast<int32_t>(offsetof(HistoryDelaySituationCameraFilter_t914159810, ____lastTargetAngle_15)); }
	inline Quaternion_t1553702882  get__lastTargetAngle_15() const { return ____lastTargetAngle_15; }
	inline Quaternion_t1553702882 * get_address_of__lastTargetAngle_15() { return &____lastTargetAngle_15; }
	inline void set__lastTargetAngle_15(Quaternion_t1553702882  value)
	{
		____lastTargetAngle_15 = value;
	}

	inline static int32_t get_offset_of__lastTargetPosition_16() { return static_cast<int32_t>(offsetof(HistoryDelaySituationCameraFilter_t914159810, ____lastTargetPosition_16)); }
	inline Vector3_t4282066566  get__lastTargetPosition_16() const { return ____lastTargetPosition_16; }
	inline Vector3_t4282066566 * get_address_of__lastTargetPosition_16() { return &____lastTargetPosition_16; }
	inline void set__lastTargetPosition_16(Vector3_t4282066566  value)
	{
		____lastTargetPosition_16 = value;
	}

	inline static int32_t get_offset_of_trackerEventPlugin_17() { return static_cast<int32_t>(offsetof(HistoryDelaySituationCameraFilter_t914159810, ___trackerEventPlugin_17)); }
	inline TrackEventDispatcherAbstract_t3123780554 * get_trackerEventPlugin_17() const { return ___trackerEventPlugin_17; }
	inline TrackEventDispatcherAbstract_t3123780554 ** get_address_of_trackerEventPlugin_17() { return &___trackerEventPlugin_17; }
	inline void set_trackerEventPlugin_17(TrackEventDispatcherAbstract_t3123780554 * value)
	{
		___trackerEventPlugin_17 = value;
		Il2CppCodeGenWriteBarrier(&___trackerEventPlugin_17, value);
	}

	inline static int32_t get_offset_of__isActive_18() { return static_cast<int32_t>(offsetof(HistoryDelaySituationCameraFilter_t914159810, ____isActive_18)); }
	inline bool get__isActive_18() const { return ____isActive_18; }
	inline bool* get_address_of__isActive_18() { return &____isActive_18; }
	inline void set__isActive_18(bool value)
	{
		____isActive_18 = value;
	}

	inline static int32_t get_offset_of_delayTime_19() { return static_cast<int32_t>(offsetof(HistoryDelaySituationCameraFilter_t914159810, ___delayTime_19)); }
	inline float get_delayTime_19() const { return ___delayTime_19; }
	inline float* get_address_of_delayTime_19() { return &___delayTime_19; }
	inline void set_delayTime_19(float value)
	{
		___delayTime_19 = value;
	}

	inline static int32_t get_offset_of__totalTimeElapsed_20() { return static_cast<int32_t>(offsetof(HistoryDelaySituationCameraFilter_t914159810, ____totalTimeElapsed_20)); }
	inline float get__totalTimeElapsed_20() const { return ____totalTimeElapsed_20; }
	inline float* get_address_of__totalTimeElapsed_20() { return &____totalTimeElapsed_20; }
	inline void set__totalTimeElapsed_20(float value)
	{
		____totalTimeElapsed_20 = value;
	}

	inline static int32_t get_offset_of__timer_21() { return static_cast<int32_t>(offsetof(HistoryDelaySituationCameraFilter_t914159810, ____timer_21)); }
	inline bool get__timer_21() const { return ____timer_21; }
	inline bool* get_address_of__timer_21() { return &____timer_21; }
	inline void set__timer_21(bool value)
	{
		____timer_21 = value;
	}

	inline static int32_t get_offset_of__timerEnabled_22() { return static_cast<int32_t>(offsetof(HistoryDelaySituationCameraFilter_t914159810, ____timerEnabled_22)); }
	inline bool get__timerEnabled_22() const { return ____timerEnabled_22; }
	inline bool* get_address_of__timerEnabled_22() { return &____timerEnabled_22; }
	inline void set__timerEnabled_22(bool value)
	{
		____timerEnabled_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
