﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.globals.events.AppRootEvents
struct AppRootEvents_t2849877622;
// System.String
struct String_t;
// MagicTV.globals.AREvents
struct AREvents_t1242196082;
// MagicTV.globals.events.ScreenEvents
struct ScreenEvents_t2706497391;
// MagicTV.globals.events.DeviceEvents
struct DeviceEvents_t3457131993;
// MagicTV.in_apps.InAppEventsChannel
struct InAppEventsChannel_t1397713486;
// MagicTV.globals.events.StateMagineEvents
struct StateMagineEvents_t1499601201;
// MagicTV.MagicTVProcess
struct MagicTVProcess_t1089716;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_MagicTV_MagicTVProcess1089716.h"

// System.Void MagicTV.globals.events.AppRootEvents::.ctor()
extern "C"  void AppRootEvents__ctor_m2129534711 (AppRootEvents_t2849877622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.AppRootEvents::RaiseDownloadAll()
extern "C"  void AppRootEvents_RaiseDownloadAll_m2937997978 (AppRootEvents_t2849877622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.AppRootEvents::LogError(System.String,System.String)
extern "C"  void AppRootEvents_LogError_m724253581 (Il2CppObject * __this /* static, unused */, String_t* ___context0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.AppRootEvents::RaiseStopPresentation()
extern "C"  void AppRootEvents_RaiseStopPresentation_m514090589 (AppRootEvents_t2849877622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.globals.events.AppRootEvents MagicTV.globals.events.AppRootEvents::GetInstance()
extern "C"  AppRootEvents_t2849877622 * AppRootEvents_GetInstance_m1652543527 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.globals.AREvents MagicTV.globals.events.AppRootEvents::GetAR()
extern "C"  AREvents_t1242196082 * AppRootEvents_GetAR_m1659220838 (AppRootEvents_t2849877622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.globals.events.ScreenEvents MagicTV.globals.events.AppRootEvents::GetScreen()
extern "C"  ScreenEvents_t2706497391 * AppRootEvents_GetScreen_m824998561 (AppRootEvents_t2849877622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.globals.events.DeviceEvents MagicTV.globals.events.AppRootEvents::GetDevice()
extern "C"  DeviceEvents_t3457131993 * AppRootEvents_GetDevice_m1072471541 (AppRootEvents_t2849877622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.in_apps.InAppEventsChannel MagicTV.globals.events.AppRootEvents::GetEventChannel(System.String)
extern "C"  InAppEventsChannel_t1397713486 * AppRootEvents_GetEventChannel_m231423804 (AppRootEvents_t2849877622 * __this, String_t* ___channelName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.AppRootEvents::RemoveEventChannel(System.String)
extern "C"  void AppRootEvents_RemoveEventChannel_m3126434408 (AppRootEvents_t2849877622 * __this, String_t* ___channelName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.globals.events.StateMagineEvents MagicTV.globals.events.AppRootEvents::GetStateMachine()
extern "C"  StateMagineEvents_t1499601201 * AppRootEvents_GetStateMachine_m2121355651 (AppRootEvents_t2849877622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.AppRootEvents::prepare(MagicTV.MagicTVProcess)
extern "C"  void AppRootEvents_prepare_m1718838397 (Il2CppObject * __this /* static, unused */, MagicTVProcess_t1089716 * ___main0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
