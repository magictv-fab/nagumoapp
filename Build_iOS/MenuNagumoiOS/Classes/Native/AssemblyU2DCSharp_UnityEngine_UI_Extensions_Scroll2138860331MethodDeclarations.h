﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Extensions.ScrollRectEx/<OnScroll>c__AnonStorey8F
struct U3COnScrollU3Ec__AnonStorey8F_t2138860331;
// UnityEngine.EventSystems.IScrollHandler
struct IScrollHandler_t3315383580;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.Extensions.ScrollRectEx/<OnScroll>c__AnonStorey8F::.ctor()
extern "C"  void U3COnScrollU3Ec__AnonStorey8F__ctor_m1961462736 (U3COnScrollU3Ec__AnonStorey8F_t2138860331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollRectEx/<OnScroll>c__AnonStorey8F::<>m__4(UnityEngine.EventSystems.IScrollHandler)
extern "C"  void U3COnScrollU3Ec__AnonStorey8F_U3CU3Em__4_m2693712308 (U3COnScrollU3Ec__AnonStorey8F_t2138860331 * __this, Il2CppObject * ___parent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
