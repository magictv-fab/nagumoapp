﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PedidoData
struct PedidoData_t3443010735;
// System.String
struct String_t;
// UnityEngine.Sprite
struct Sprite_t3199167241;
// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.UI.Image
struct Image_t538875265;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PedidoValues
struct  PedidoValues_t2123607271  : public MonoBehaviour_t667441552
{
public:
	// PedidoData PedidoValues::pedido
	PedidoData_t3443010735 * ___pedido_2;
	// System.String PedidoValues::nomeCarne
	String_t* ___nomeCarne_3;
	// System.String PedidoValues::peso
	String_t* ___peso_4;
	// System.String PedidoValues::tipoCarne
	String_t* ___tipoCarne_5;
	// System.String PedidoValues::unidade
	String_t* ___unidade_6;
	// System.String PedidoValues::horarioData
	String_t* ___horarioData_7;
	// System.String PedidoValues::info
	String_t* ___info_8;
	// System.String PedidoValues::codePedido
	String_t* ___codePedido_9;
	// System.Int32 PedidoValues::quantidade
	int32_t ___quantidade_10;
	// UnityEngine.Sprite PedidoValues::sprite
	Sprite_t3199167241 * ___sprite_11;
	// UnityEngine.UI.Text PedidoValues::infoText
	Text_t9039225 * ___infoText_12;
	// UnityEngine.UI.Text PedidoValues::titleText
	Text_t9039225 * ___titleText_13;
	// UnityEngine.UI.Image PedidoValues::image
	Image_t538875265 * ___image_14;

public:
	inline static int32_t get_offset_of_pedido_2() { return static_cast<int32_t>(offsetof(PedidoValues_t2123607271, ___pedido_2)); }
	inline PedidoData_t3443010735 * get_pedido_2() const { return ___pedido_2; }
	inline PedidoData_t3443010735 ** get_address_of_pedido_2() { return &___pedido_2; }
	inline void set_pedido_2(PedidoData_t3443010735 * value)
	{
		___pedido_2 = value;
		Il2CppCodeGenWriteBarrier(&___pedido_2, value);
	}

	inline static int32_t get_offset_of_nomeCarne_3() { return static_cast<int32_t>(offsetof(PedidoValues_t2123607271, ___nomeCarne_3)); }
	inline String_t* get_nomeCarne_3() const { return ___nomeCarne_3; }
	inline String_t** get_address_of_nomeCarne_3() { return &___nomeCarne_3; }
	inline void set_nomeCarne_3(String_t* value)
	{
		___nomeCarne_3 = value;
		Il2CppCodeGenWriteBarrier(&___nomeCarne_3, value);
	}

	inline static int32_t get_offset_of_peso_4() { return static_cast<int32_t>(offsetof(PedidoValues_t2123607271, ___peso_4)); }
	inline String_t* get_peso_4() const { return ___peso_4; }
	inline String_t** get_address_of_peso_4() { return &___peso_4; }
	inline void set_peso_4(String_t* value)
	{
		___peso_4 = value;
		Il2CppCodeGenWriteBarrier(&___peso_4, value);
	}

	inline static int32_t get_offset_of_tipoCarne_5() { return static_cast<int32_t>(offsetof(PedidoValues_t2123607271, ___tipoCarne_5)); }
	inline String_t* get_tipoCarne_5() const { return ___tipoCarne_5; }
	inline String_t** get_address_of_tipoCarne_5() { return &___tipoCarne_5; }
	inline void set_tipoCarne_5(String_t* value)
	{
		___tipoCarne_5 = value;
		Il2CppCodeGenWriteBarrier(&___tipoCarne_5, value);
	}

	inline static int32_t get_offset_of_unidade_6() { return static_cast<int32_t>(offsetof(PedidoValues_t2123607271, ___unidade_6)); }
	inline String_t* get_unidade_6() const { return ___unidade_6; }
	inline String_t** get_address_of_unidade_6() { return &___unidade_6; }
	inline void set_unidade_6(String_t* value)
	{
		___unidade_6 = value;
		Il2CppCodeGenWriteBarrier(&___unidade_6, value);
	}

	inline static int32_t get_offset_of_horarioData_7() { return static_cast<int32_t>(offsetof(PedidoValues_t2123607271, ___horarioData_7)); }
	inline String_t* get_horarioData_7() const { return ___horarioData_7; }
	inline String_t** get_address_of_horarioData_7() { return &___horarioData_7; }
	inline void set_horarioData_7(String_t* value)
	{
		___horarioData_7 = value;
		Il2CppCodeGenWriteBarrier(&___horarioData_7, value);
	}

	inline static int32_t get_offset_of_info_8() { return static_cast<int32_t>(offsetof(PedidoValues_t2123607271, ___info_8)); }
	inline String_t* get_info_8() const { return ___info_8; }
	inline String_t** get_address_of_info_8() { return &___info_8; }
	inline void set_info_8(String_t* value)
	{
		___info_8 = value;
		Il2CppCodeGenWriteBarrier(&___info_8, value);
	}

	inline static int32_t get_offset_of_codePedido_9() { return static_cast<int32_t>(offsetof(PedidoValues_t2123607271, ___codePedido_9)); }
	inline String_t* get_codePedido_9() const { return ___codePedido_9; }
	inline String_t** get_address_of_codePedido_9() { return &___codePedido_9; }
	inline void set_codePedido_9(String_t* value)
	{
		___codePedido_9 = value;
		Il2CppCodeGenWriteBarrier(&___codePedido_9, value);
	}

	inline static int32_t get_offset_of_quantidade_10() { return static_cast<int32_t>(offsetof(PedidoValues_t2123607271, ___quantidade_10)); }
	inline int32_t get_quantidade_10() const { return ___quantidade_10; }
	inline int32_t* get_address_of_quantidade_10() { return &___quantidade_10; }
	inline void set_quantidade_10(int32_t value)
	{
		___quantidade_10 = value;
	}

	inline static int32_t get_offset_of_sprite_11() { return static_cast<int32_t>(offsetof(PedidoValues_t2123607271, ___sprite_11)); }
	inline Sprite_t3199167241 * get_sprite_11() const { return ___sprite_11; }
	inline Sprite_t3199167241 ** get_address_of_sprite_11() { return &___sprite_11; }
	inline void set_sprite_11(Sprite_t3199167241 * value)
	{
		___sprite_11 = value;
		Il2CppCodeGenWriteBarrier(&___sprite_11, value);
	}

	inline static int32_t get_offset_of_infoText_12() { return static_cast<int32_t>(offsetof(PedidoValues_t2123607271, ___infoText_12)); }
	inline Text_t9039225 * get_infoText_12() const { return ___infoText_12; }
	inline Text_t9039225 ** get_address_of_infoText_12() { return &___infoText_12; }
	inline void set_infoText_12(Text_t9039225 * value)
	{
		___infoText_12 = value;
		Il2CppCodeGenWriteBarrier(&___infoText_12, value);
	}

	inline static int32_t get_offset_of_titleText_13() { return static_cast<int32_t>(offsetof(PedidoValues_t2123607271, ___titleText_13)); }
	inline Text_t9039225 * get_titleText_13() const { return ___titleText_13; }
	inline Text_t9039225 ** get_address_of_titleText_13() { return &___titleText_13; }
	inline void set_titleText_13(Text_t9039225 * value)
	{
		___titleText_13 = value;
		Il2CppCodeGenWriteBarrier(&___titleText_13, value);
	}

	inline static int32_t get_offset_of_image_14() { return static_cast<int32_t>(offsetof(PedidoValues_t2123607271, ___image_14)); }
	inline Image_t538875265 * get_image_14() const { return ___image_14; }
	inline Image_t538875265 ** get_address_of_image_14() { return &___image_14; }
	inline void set_image_14(Image_t538875265 * value)
	{
		___image_14 = value;
		Il2CppCodeGenWriteBarrier(&___image_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
