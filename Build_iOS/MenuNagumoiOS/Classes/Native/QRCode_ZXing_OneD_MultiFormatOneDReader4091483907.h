﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IList`1<ZXing.OneD.OneDReader>
struct IList_1_t1835722818;

#include "QRCode_ZXing_OneD_OneDReader3436042911.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.MultiFormatOneDReader
struct  MultiFormatOneDReader_t4091483907  : public OneDReader_t3436042911
{
public:
	// System.Collections.Generic.IList`1<ZXing.OneD.OneDReader> ZXing.OneD.MultiFormatOneDReader::readers
	Il2CppObject* ___readers_2;

public:
	inline static int32_t get_offset_of_readers_2() { return static_cast<int32_t>(offsetof(MultiFormatOneDReader_t4091483907, ___readers_2)); }
	inline Il2CppObject* get_readers_2() const { return ___readers_2; }
	inline Il2CppObject** get_address_of_readers_2() { return &___readers_2; }
	inline void set_readers_2(Il2CppObject* value)
	{
		___readers_2 = value;
		Il2CppCodeGenWriteBarrier(&___readers_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
