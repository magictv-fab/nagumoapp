﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.in_apps.InAppProgressInfo
struct InAppProgressInfo_t2775900725;
// MagicTV.in_apps.OnInAppCommonEventHandler
struct OnInAppCommonEventHandler_t1653412278;
// MagicTV.vo.BundleVO
struct BundleVO_t1984518073;
// MagicTV.in_apps.OnInAppProgressEventHandler
struct OnInAppProgressEventHandler_t3139301368;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_in_apps_OnInAppCommonEve1653412278.h"
#include "AssemblyU2DCSharp_MagicTV_vo_BundleVO1984518073.h"
#include "AssemblyU2DCSharp_MagicTV_in_apps_OnInAppProgressE3139301368.h"

// System.Void MagicTV.in_apps.InAppProgressInfo::.ctor()
extern "C"  void InAppProgressInfo__ctor_m4267716981 (InAppProgressInfo_t2775900725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicTV.in_apps.InAppProgressInfo::isComplete()
extern "C"  bool InAppProgressInfo_isComplete_m193343774 (InAppProgressInfo_t2775900725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppProgressInfo::AddCompleteEventhandler(MagicTV.in_apps.OnInAppCommonEventHandler)
extern "C"  void InAppProgressInfo_AddCompleteEventhandler_m4076623966 (InAppProgressInfo_t2775900725 * __this, OnInAppCommonEventHandler_t1653412278 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppProgressInfo::RemoveCompleteEventhandler(MagicTV.in_apps.OnInAppCommonEventHandler)
extern "C"  void InAppProgressInfo_RemoveCompleteEventhandler_m3119700735 (InAppProgressInfo_t2775900725 * __this, OnInAppCommonEventHandler_t1653412278 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppProgressInfo::RaiseComplete(MagicTV.vo.BundleVO)
extern "C"  void InAppProgressInfo_RaiseComplete_m3271671209 (InAppProgressInfo_t2775900725 * __this, BundleVO_t1984518073 * ___var0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppProgressInfo::AddProgressEventhandler(MagicTV.in_apps.OnInAppProgressEventHandler)
extern "C"  void InAppProgressInfo_AddProgressEventhandler_m374573288 (InAppProgressInfo_t2775900725 * __this, OnInAppProgressEventHandler_t3139301368 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppProgressInfo::RemoveProgressEventhandler(MagicTV.in_apps.OnInAppProgressEventHandler)
extern "C"  void InAppProgressInfo_RemoveProgressEventhandler_m4189316937 (InAppProgressInfo_t2775900725 * __this, OnInAppProgressEventHandler_t3139301368 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppProgressInfo::RaiseProgress(System.Single)
extern "C"  void InAppProgressInfo_RaiseProgress_m3075860639 (InAppProgressInfo_t2775900725 * __this, float ___progress0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MagicTV.in_apps.InAppProgressInfo::GetCurrentProgress()
extern "C"  float InAppProgressInfo_GetCurrentProgress_m1798293235 (InAppProgressInfo_t2775900725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppProgressInfo::AddErrorEventhandler(MagicTV.in_apps.OnInAppCommonEventHandler)
extern "C"  void InAppProgressInfo_AddErrorEventhandler_m2705535285 (InAppProgressInfo_t2775900725 * __this, OnInAppCommonEventHandler_t1653412278 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppProgressInfo::RemoveErrorEventhandler(MagicTV.in_apps.OnInAppCommonEventHandler)
extern "C"  void InAppProgressInfo_RemoveErrorEventhandler_m3012008500 (InAppProgressInfo_t2775900725 * __this, OnInAppCommonEventHandler_t1653412278 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppProgressInfo::RaiseError(MagicTV.vo.BundleVO)
extern "C"  void InAppProgressInfo_RaiseError_m233050678 (InAppProgressInfo_t2775900725 * __this, BundleVO_t1984518073 * ___var0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
