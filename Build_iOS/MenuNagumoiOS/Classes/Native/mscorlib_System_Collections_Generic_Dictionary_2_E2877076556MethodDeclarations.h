﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3363211663MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,ARM.components.GroupInAppAbstractComponentsManager>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1012615933(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2877076556 *, Dictionary_2_t1559753164 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,ARM.components.GroupInAppAbstractComponentsManager>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1619546318(__this, method) ((  Il2CppObject * (*) (Enumerator_t2877076556 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,ARM.components.GroupInAppAbstractComponentsManager>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4243112344(__this, method) ((  void (*) (Enumerator_t2877076556 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,ARM.components.GroupInAppAbstractComponentsManager>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1041887503(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t2877076556 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,ARM.components.GroupInAppAbstractComponentsManager>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m736049962(__this, method) ((  Il2CppObject * (*) (Enumerator_t2877076556 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,ARM.components.GroupInAppAbstractComponentsManager>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m4017813308(__this, method) ((  Il2CppObject * (*) (Enumerator_t2877076556 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,ARM.components.GroupInAppAbstractComponentsManager>::MoveNext()
#define Enumerator_MoveNext_m149600712(__this, method) ((  bool (*) (Enumerator_t2877076556 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,ARM.components.GroupInAppAbstractComponentsManager>::get_Current()
#define Enumerator_get_Current_m2070535540(__this, method) ((  KeyValuePair_2_t1458533870  (*) (Enumerator_t2877076556 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,ARM.components.GroupInAppAbstractComponentsManager>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m612551633(__this, method) ((  String_t* (*) (Enumerator_t2877076556 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,ARM.components.GroupInAppAbstractComponentsManager>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m335509329(__this, method) ((  GroupInAppAbstractComponentsManager_t739334794 * (*) (Enumerator_t2877076556 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,ARM.components.GroupInAppAbstractComponentsManager>::Reset()
#define Enumerator_Reset_m3536370127(__this, method) ((  void (*) (Enumerator_t2877076556 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,ARM.components.GroupInAppAbstractComponentsManager>::VerifyState()
#define Enumerator_VerifyState_m4124521112(__this, method) ((  void (*) (Enumerator_t2877076556 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,ARM.components.GroupInAppAbstractComponentsManager>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2154846208(__this, method) ((  void (*) (Enumerator_t2877076556 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,ARM.components.GroupInAppAbstractComponentsManager>::Dispose()
#define Enumerator_Dispose_m3655989983(__this, method) ((  void (*) (Enumerator_t2877076556 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
