﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sombra
struct  Sombra_t2482075392  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject Sombra::target
	GameObject_t3674682005 * ___target_2;
	// System.Single Sombra::bias
	float ___bias_3;
	// System.Boolean Sombra::ok
	bool ___ok_4;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(Sombra_t2482075392, ___target_2)); }
	inline GameObject_t3674682005 * get_target_2() const { return ___target_2; }
	inline GameObject_t3674682005 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(GameObject_t3674682005 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier(&___target_2, value);
	}

	inline static int32_t get_offset_of_bias_3() { return static_cast<int32_t>(offsetof(Sombra_t2482075392, ___bias_3)); }
	inline float get_bias_3() const { return ___bias_3; }
	inline float* get_address_of_bias_3() { return &___bias_3; }
	inline void set_bias_3(float value)
	{
		___bias_3 = value;
	}

	inline static int32_t get_offset_of_ok_4() { return static_cast<int32_t>(offsetof(Sombra_t2482075392, ___ok_4)); }
	inline bool get_ok_4() const { return ___ok_4; }
	inline bool* get_address_of_ok_4() { return &___ok_4; }
	inline void set_ok_4(bool value)
	{
		___ok_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
