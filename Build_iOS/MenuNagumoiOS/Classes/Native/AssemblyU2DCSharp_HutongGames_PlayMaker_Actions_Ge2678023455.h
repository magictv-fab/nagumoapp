﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// UnityEngine.ParticleCollisionEvent[]
struct ParticleCollisionEventU5BU5D_t4168492807;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetNextCollisionEvent
struct  GetNextCollisionEvent_t2678023455  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetNextCollisionEvent::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetNextCollisionEvent::collisionNormal
	FsmVector3_t533912882 * ___collisionNormal_10;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetNextCollisionEvent::collisionVelocity
	FsmVector3_t533912882 * ___collisionVelocity_11;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetNextCollisionEvent::collisionIntersection
	FsmVector3_t533912882 * ___collisionIntersection_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetNextCollisionEvent::loopEvent
	FsmEvent_t2133468028 * ___loopEvent_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetNextCollisionEvent::finishedEvent
	FsmEvent_t2133468028 * ___finishedEvent_14;
	// UnityEngine.ParticleCollisionEvent[] HutongGames.PlayMaker.Actions.GetNextCollisionEvent::collisionEvents
	ParticleCollisionEventU5BU5D_t4168492807* ___collisionEvents_15;
	// System.Int32 HutongGames.PlayMaker.Actions.GetNextCollisionEvent::nextIndex
	int32_t ___nextIndex_16;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(GetNextCollisionEvent_t2678023455, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_collisionNormal_10() { return static_cast<int32_t>(offsetof(GetNextCollisionEvent_t2678023455, ___collisionNormal_10)); }
	inline FsmVector3_t533912882 * get_collisionNormal_10() const { return ___collisionNormal_10; }
	inline FsmVector3_t533912882 ** get_address_of_collisionNormal_10() { return &___collisionNormal_10; }
	inline void set_collisionNormal_10(FsmVector3_t533912882 * value)
	{
		___collisionNormal_10 = value;
		Il2CppCodeGenWriteBarrier(&___collisionNormal_10, value);
	}

	inline static int32_t get_offset_of_collisionVelocity_11() { return static_cast<int32_t>(offsetof(GetNextCollisionEvent_t2678023455, ___collisionVelocity_11)); }
	inline FsmVector3_t533912882 * get_collisionVelocity_11() const { return ___collisionVelocity_11; }
	inline FsmVector3_t533912882 ** get_address_of_collisionVelocity_11() { return &___collisionVelocity_11; }
	inline void set_collisionVelocity_11(FsmVector3_t533912882 * value)
	{
		___collisionVelocity_11 = value;
		Il2CppCodeGenWriteBarrier(&___collisionVelocity_11, value);
	}

	inline static int32_t get_offset_of_collisionIntersection_12() { return static_cast<int32_t>(offsetof(GetNextCollisionEvent_t2678023455, ___collisionIntersection_12)); }
	inline FsmVector3_t533912882 * get_collisionIntersection_12() const { return ___collisionIntersection_12; }
	inline FsmVector3_t533912882 ** get_address_of_collisionIntersection_12() { return &___collisionIntersection_12; }
	inline void set_collisionIntersection_12(FsmVector3_t533912882 * value)
	{
		___collisionIntersection_12 = value;
		Il2CppCodeGenWriteBarrier(&___collisionIntersection_12, value);
	}

	inline static int32_t get_offset_of_loopEvent_13() { return static_cast<int32_t>(offsetof(GetNextCollisionEvent_t2678023455, ___loopEvent_13)); }
	inline FsmEvent_t2133468028 * get_loopEvent_13() const { return ___loopEvent_13; }
	inline FsmEvent_t2133468028 ** get_address_of_loopEvent_13() { return &___loopEvent_13; }
	inline void set_loopEvent_13(FsmEvent_t2133468028 * value)
	{
		___loopEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___loopEvent_13, value);
	}

	inline static int32_t get_offset_of_finishedEvent_14() { return static_cast<int32_t>(offsetof(GetNextCollisionEvent_t2678023455, ___finishedEvent_14)); }
	inline FsmEvent_t2133468028 * get_finishedEvent_14() const { return ___finishedEvent_14; }
	inline FsmEvent_t2133468028 ** get_address_of_finishedEvent_14() { return &___finishedEvent_14; }
	inline void set_finishedEvent_14(FsmEvent_t2133468028 * value)
	{
		___finishedEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___finishedEvent_14, value);
	}

	inline static int32_t get_offset_of_collisionEvents_15() { return static_cast<int32_t>(offsetof(GetNextCollisionEvent_t2678023455, ___collisionEvents_15)); }
	inline ParticleCollisionEventU5BU5D_t4168492807* get_collisionEvents_15() const { return ___collisionEvents_15; }
	inline ParticleCollisionEventU5BU5D_t4168492807** get_address_of_collisionEvents_15() { return &___collisionEvents_15; }
	inline void set_collisionEvents_15(ParticleCollisionEventU5BU5D_t4168492807* value)
	{
		___collisionEvents_15 = value;
		Il2CppCodeGenWriteBarrier(&___collisionEvents_15, value);
	}

	inline static int32_t get_offset_of_nextIndex_16() { return static_cast<int32_t>(offsetof(GetNextCollisionEvent_t2678023455, ___nextIndex_16)); }
	inline int32_t get_nextIndex_16() const { return ___nextIndex_16; }
	inline int32_t* get_address_of_nextIndex_16() { return &___nextIndex_16; }
	inline void set_nextIndex_16(int32_t value)
	{
		___nextIndex_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
