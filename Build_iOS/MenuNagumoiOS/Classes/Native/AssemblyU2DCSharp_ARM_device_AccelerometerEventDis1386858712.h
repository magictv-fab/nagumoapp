﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.utils.PoolVector3
struct PoolVector3_t628120362;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t1365137228;

#include "AssemblyU2DCSharp_ARM_device_abstracts_DeviceAccel1009313746.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.device.AccelerometerEventDispacher
struct  AccelerometerEventDispacher_t1386858712  : public DeviceAccelerometerEventDispetcherAbstract_t1009313746
{
public:
	// System.Boolean ARM.device.AccelerometerEventDispacher::onGUIDebug
	bool ___onGUIDebug_12;
	// System.Single ARM.device.AccelerometerEventDispacher::shakeTolerance
	float ___shakeTolerance_13;
	// System.Single ARM.device.AccelerometerEventDispacher::movementToleranceX
	float ___movementToleranceX_14;
	// System.Single ARM.device.AccelerometerEventDispacher::movementToleranceY
	float ___movementToleranceY_15;
	// System.Single ARM.device.AccelerometerEventDispacher::movementToleranceZ
	float ___movementToleranceZ_16;
	// System.UInt32 ARM.device.AccelerometerEventDispacher::_historyRange
	uint32_t ____historyRange_17;
	// System.Single ARM.device.AccelerometerEventDispacher::_time
	float ____time_18;
	// System.Single ARM.device.AccelerometerEventDispacher::_clockTimer
	float ____clockTimer_19;
	// ARM.utils.PoolVector3 ARM.device.AccelerometerEventDispacher::_poolVector3
	PoolVector3_t628120362 * ____poolVector3_20;
	// System.Boolean ARM.device.AccelerometerEventDispacher::_isShaking
	bool ____isShaking_21;
	// System.Boolean ARM.device.AccelerometerEventDispacher::_isMoving
	bool ____isMoving_22;
	// System.Boolean ARM.device.AccelerometerEventDispacher::_active
	bool ____active_23;
	// System.Collections.Generic.List`1<System.Single> ARM.device.AccelerometerEventDispacher::_inicialConfig
	List_1_t1365137228 * ____inicialConfig_24;
	// System.Single ARM.device.AccelerometerEventDispacher::xDiff
	float ___xDiff_25;
	// System.Single ARM.device.AccelerometerEventDispacher::yDiff
	float ___yDiff_26;
	// System.Single ARM.device.AccelerometerEventDispacher::zDiff
	float ___zDiff_27;
	// System.Single ARM.device.AccelerometerEventDispacher::inicialX
	float ___inicialX_28;
	// System.Single ARM.device.AccelerometerEventDispacher::inicialY
	float ___inicialY_29;
	// System.Single ARM.device.AccelerometerEventDispacher::larguraBotao
	float ___larguraBotao_30;
	// System.Single ARM.device.AccelerometerEventDispacher::alturaBotao
	float ___alturaBotao_31;

public:
	inline static int32_t get_offset_of_onGUIDebug_12() { return static_cast<int32_t>(offsetof(AccelerometerEventDispacher_t1386858712, ___onGUIDebug_12)); }
	inline bool get_onGUIDebug_12() const { return ___onGUIDebug_12; }
	inline bool* get_address_of_onGUIDebug_12() { return &___onGUIDebug_12; }
	inline void set_onGUIDebug_12(bool value)
	{
		___onGUIDebug_12 = value;
	}

	inline static int32_t get_offset_of_shakeTolerance_13() { return static_cast<int32_t>(offsetof(AccelerometerEventDispacher_t1386858712, ___shakeTolerance_13)); }
	inline float get_shakeTolerance_13() const { return ___shakeTolerance_13; }
	inline float* get_address_of_shakeTolerance_13() { return &___shakeTolerance_13; }
	inline void set_shakeTolerance_13(float value)
	{
		___shakeTolerance_13 = value;
	}

	inline static int32_t get_offset_of_movementToleranceX_14() { return static_cast<int32_t>(offsetof(AccelerometerEventDispacher_t1386858712, ___movementToleranceX_14)); }
	inline float get_movementToleranceX_14() const { return ___movementToleranceX_14; }
	inline float* get_address_of_movementToleranceX_14() { return &___movementToleranceX_14; }
	inline void set_movementToleranceX_14(float value)
	{
		___movementToleranceX_14 = value;
	}

	inline static int32_t get_offset_of_movementToleranceY_15() { return static_cast<int32_t>(offsetof(AccelerometerEventDispacher_t1386858712, ___movementToleranceY_15)); }
	inline float get_movementToleranceY_15() const { return ___movementToleranceY_15; }
	inline float* get_address_of_movementToleranceY_15() { return &___movementToleranceY_15; }
	inline void set_movementToleranceY_15(float value)
	{
		___movementToleranceY_15 = value;
	}

	inline static int32_t get_offset_of_movementToleranceZ_16() { return static_cast<int32_t>(offsetof(AccelerometerEventDispacher_t1386858712, ___movementToleranceZ_16)); }
	inline float get_movementToleranceZ_16() const { return ___movementToleranceZ_16; }
	inline float* get_address_of_movementToleranceZ_16() { return &___movementToleranceZ_16; }
	inline void set_movementToleranceZ_16(float value)
	{
		___movementToleranceZ_16 = value;
	}

	inline static int32_t get_offset_of__historyRange_17() { return static_cast<int32_t>(offsetof(AccelerometerEventDispacher_t1386858712, ____historyRange_17)); }
	inline uint32_t get__historyRange_17() const { return ____historyRange_17; }
	inline uint32_t* get_address_of__historyRange_17() { return &____historyRange_17; }
	inline void set__historyRange_17(uint32_t value)
	{
		____historyRange_17 = value;
	}

	inline static int32_t get_offset_of__time_18() { return static_cast<int32_t>(offsetof(AccelerometerEventDispacher_t1386858712, ____time_18)); }
	inline float get__time_18() const { return ____time_18; }
	inline float* get_address_of__time_18() { return &____time_18; }
	inline void set__time_18(float value)
	{
		____time_18 = value;
	}

	inline static int32_t get_offset_of__clockTimer_19() { return static_cast<int32_t>(offsetof(AccelerometerEventDispacher_t1386858712, ____clockTimer_19)); }
	inline float get__clockTimer_19() const { return ____clockTimer_19; }
	inline float* get_address_of__clockTimer_19() { return &____clockTimer_19; }
	inline void set__clockTimer_19(float value)
	{
		____clockTimer_19 = value;
	}

	inline static int32_t get_offset_of__poolVector3_20() { return static_cast<int32_t>(offsetof(AccelerometerEventDispacher_t1386858712, ____poolVector3_20)); }
	inline PoolVector3_t628120362 * get__poolVector3_20() const { return ____poolVector3_20; }
	inline PoolVector3_t628120362 ** get_address_of__poolVector3_20() { return &____poolVector3_20; }
	inline void set__poolVector3_20(PoolVector3_t628120362 * value)
	{
		____poolVector3_20 = value;
		Il2CppCodeGenWriteBarrier(&____poolVector3_20, value);
	}

	inline static int32_t get_offset_of__isShaking_21() { return static_cast<int32_t>(offsetof(AccelerometerEventDispacher_t1386858712, ____isShaking_21)); }
	inline bool get__isShaking_21() const { return ____isShaking_21; }
	inline bool* get_address_of__isShaking_21() { return &____isShaking_21; }
	inline void set__isShaking_21(bool value)
	{
		____isShaking_21 = value;
	}

	inline static int32_t get_offset_of__isMoving_22() { return static_cast<int32_t>(offsetof(AccelerometerEventDispacher_t1386858712, ____isMoving_22)); }
	inline bool get__isMoving_22() const { return ____isMoving_22; }
	inline bool* get_address_of__isMoving_22() { return &____isMoving_22; }
	inline void set__isMoving_22(bool value)
	{
		____isMoving_22 = value;
	}

	inline static int32_t get_offset_of__active_23() { return static_cast<int32_t>(offsetof(AccelerometerEventDispacher_t1386858712, ____active_23)); }
	inline bool get__active_23() const { return ____active_23; }
	inline bool* get_address_of__active_23() { return &____active_23; }
	inline void set__active_23(bool value)
	{
		____active_23 = value;
	}

	inline static int32_t get_offset_of__inicialConfig_24() { return static_cast<int32_t>(offsetof(AccelerometerEventDispacher_t1386858712, ____inicialConfig_24)); }
	inline List_1_t1365137228 * get__inicialConfig_24() const { return ____inicialConfig_24; }
	inline List_1_t1365137228 ** get_address_of__inicialConfig_24() { return &____inicialConfig_24; }
	inline void set__inicialConfig_24(List_1_t1365137228 * value)
	{
		____inicialConfig_24 = value;
		Il2CppCodeGenWriteBarrier(&____inicialConfig_24, value);
	}

	inline static int32_t get_offset_of_xDiff_25() { return static_cast<int32_t>(offsetof(AccelerometerEventDispacher_t1386858712, ___xDiff_25)); }
	inline float get_xDiff_25() const { return ___xDiff_25; }
	inline float* get_address_of_xDiff_25() { return &___xDiff_25; }
	inline void set_xDiff_25(float value)
	{
		___xDiff_25 = value;
	}

	inline static int32_t get_offset_of_yDiff_26() { return static_cast<int32_t>(offsetof(AccelerometerEventDispacher_t1386858712, ___yDiff_26)); }
	inline float get_yDiff_26() const { return ___yDiff_26; }
	inline float* get_address_of_yDiff_26() { return &___yDiff_26; }
	inline void set_yDiff_26(float value)
	{
		___yDiff_26 = value;
	}

	inline static int32_t get_offset_of_zDiff_27() { return static_cast<int32_t>(offsetof(AccelerometerEventDispacher_t1386858712, ___zDiff_27)); }
	inline float get_zDiff_27() const { return ___zDiff_27; }
	inline float* get_address_of_zDiff_27() { return &___zDiff_27; }
	inline void set_zDiff_27(float value)
	{
		___zDiff_27 = value;
	}

	inline static int32_t get_offset_of_inicialX_28() { return static_cast<int32_t>(offsetof(AccelerometerEventDispacher_t1386858712, ___inicialX_28)); }
	inline float get_inicialX_28() const { return ___inicialX_28; }
	inline float* get_address_of_inicialX_28() { return &___inicialX_28; }
	inline void set_inicialX_28(float value)
	{
		___inicialX_28 = value;
	}

	inline static int32_t get_offset_of_inicialY_29() { return static_cast<int32_t>(offsetof(AccelerometerEventDispacher_t1386858712, ___inicialY_29)); }
	inline float get_inicialY_29() const { return ___inicialY_29; }
	inline float* get_address_of_inicialY_29() { return &___inicialY_29; }
	inline void set_inicialY_29(float value)
	{
		___inicialY_29 = value;
	}

	inline static int32_t get_offset_of_larguraBotao_30() { return static_cast<int32_t>(offsetof(AccelerometerEventDispacher_t1386858712, ___larguraBotao_30)); }
	inline float get_larguraBotao_30() const { return ___larguraBotao_30; }
	inline float* get_address_of_larguraBotao_30() { return &___larguraBotao_30; }
	inline void set_larguraBotao_30(float value)
	{
		___larguraBotao_30 = value;
	}

	inline static int32_t get_offset_of_alturaBotao_31() { return static_cast<int32_t>(offsetof(AccelerometerEventDispacher_t1386858712, ___alturaBotao_31)); }
	inline float get_alturaBotao_31() const { return ___alturaBotao_31; }
	inline float* get_address_of_alturaBotao_31() { return &___alturaBotao_31; }
	inline void set_alturaBotao_31(float value)
	{
		___alturaBotao_31 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
