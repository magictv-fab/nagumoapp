﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.ResultPoint[]
struct ResultPointU5BU5D_t1195164344;
// ZXing.Maxicode.Internal.Decoder
struct Decoder_t3124105626;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Maxicode.MaxiCodeReader
struct  MaxiCodeReader_t3092722715  : public Il2CppObject
{
public:
	// ZXing.Maxicode.Internal.Decoder ZXing.Maxicode.MaxiCodeReader::decoder
	Decoder_t3124105626 * ___decoder_1;

public:
	inline static int32_t get_offset_of_decoder_1() { return static_cast<int32_t>(offsetof(MaxiCodeReader_t3092722715, ___decoder_1)); }
	inline Decoder_t3124105626 * get_decoder_1() const { return ___decoder_1; }
	inline Decoder_t3124105626 ** get_address_of_decoder_1() { return &___decoder_1; }
	inline void set_decoder_1(Decoder_t3124105626 * value)
	{
		___decoder_1 = value;
		Il2CppCodeGenWriteBarrier(&___decoder_1, value);
	}
};

struct MaxiCodeReader_t3092722715_StaticFields
{
public:
	// ZXing.ResultPoint[] ZXing.Maxicode.MaxiCodeReader::NO_POINTS
	ResultPointU5BU5D_t1195164344* ___NO_POINTS_0;

public:
	inline static int32_t get_offset_of_NO_POINTS_0() { return static_cast<int32_t>(offsetof(MaxiCodeReader_t3092722715_StaticFields, ___NO_POINTS_0)); }
	inline ResultPointU5BU5D_t1195164344* get_NO_POINTS_0() const { return ___NO_POINTS_0; }
	inline ResultPointU5BU5D_t1195164344** get_address_of_NO_POINTS_0() { return &___NO_POINTS_0; }
	inline void set_NO_POINTS_0(ResultPointU5BU5D_t1195164344* value)
	{
		___NO_POINTS_0 = value;
		Il2CppCodeGenWriteBarrier(&___NO_POINTS_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
