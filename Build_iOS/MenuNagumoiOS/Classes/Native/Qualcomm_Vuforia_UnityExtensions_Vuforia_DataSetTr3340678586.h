﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Vuforia.ReconstructionFromTargetAbstractBehaviour
struct ReconstructionFromTargetAbstractBehaviour_t147246886;

#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Trackable4179556250.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DataSetTrackableBehaviour
struct  DataSetTrackableBehaviour_t3340678586  : public TrackableBehaviour_t4179556250
{
public:
	// System.String Vuforia.DataSetTrackableBehaviour::mDataSetPath
	String_t* ___mDataSetPath_9;
	// System.Boolean Vuforia.DataSetTrackableBehaviour::mExtendedTracking
	bool ___mExtendedTracking_10;
	// System.Boolean Vuforia.DataSetTrackableBehaviour::mInitializeSmartTerrain
	bool ___mInitializeSmartTerrain_11;
	// Vuforia.ReconstructionFromTargetAbstractBehaviour Vuforia.DataSetTrackableBehaviour::mReconstructionToInitialize
	ReconstructionFromTargetAbstractBehaviour_t147246886 * ___mReconstructionToInitialize_12;
	// UnityEngine.Vector3 Vuforia.DataSetTrackableBehaviour::mSmartTerrainOccluderBoundsMin
	Vector3_t4282066566  ___mSmartTerrainOccluderBoundsMin_13;
	// UnityEngine.Vector3 Vuforia.DataSetTrackableBehaviour::mSmartTerrainOccluderBoundsMax
	Vector3_t4282066566  ___mSmartTerrainOccluderBoundsMax_14;
	// System.Boolean Vuforia.DataSetTrackableBehaviour::mIsSmartTerrainOccluderOffset
	bool ___mIsSmartTerrainOccluderOffset_15;
	// UnityEngine.Vector3 Vuforia.DataSetTrackableBehaviour::mSmartTerrainOccluderOffset
	Vector3_t4282066566  ___mSmartTerrainOccluderOffset_16;
	// UnityEngine.Quaternion Vuforia.DataSetTrackableBehaviour::mSmartTerrainOccluderRotation
	Quaternion_t1553702882  ___mSmartTerrainOccluderRotation_17;
	// System.Boolean Vuforia.DataSetTrackableBehaviour::mSmartTerrainOccluderLockedInPlace
	bool ___mSmartTerrainOccluderLockedInPlace_18;
	// System.Boolean Vuforia.DataSetTrackableBehaviour::mAutoSetOccluderFromTargetSize
	bool ___mAutoSetOccluderFromTargetSize_19;

public:
	inline static int32_t get_offset_of_mDataSetPath_9() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3340678586, ___mDataSetPath_9)); }
	inline String_t* get_mDataSetPath_9() const { return ___mDataSetPath_9; }
	inline String_t** get_address_of_mDataSetPath_9() { return &___mDataSetPath_9; }
	inline void set_mDataSetPath_9(String_t* value)
	{
		___mDataSetPath_9 = value;
		Il2CppCodeGenWriteBarrier(&___mDataSetPath_9, value);
	}

	inline static int32_t get_offset_of_mExtendedTracking_10() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3340678586, ___mExtendedTracking_10)); }
	inline bool get_mExtendedTracking_10() const { return ___mExtendedTracking_10; }
	inline bool* get_address_of_mExtendedTracking_10() { return &___mExtendedTracking_10; }
	inline void set_mExtendedTracking_10(bool value)
	{
		___mExtendedTracking_10 = value;
	}

	inline static int32_t get_offset_of_mInitializeSmartTerrain_11() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3340678586, ___mInitializeSmartTerrain_11)); }
	inline bool get_mInitializeSmartTerrain_11() const { return ___mInitializeSmartTerrain_11; }
	inline bool* get_address_of_mInitializeSmartTerrain_11() { return &___mInitializeSmartTerrain_11; }
	inline void set_mInitializeSmartTerrain_11(bool value)
	{
		___mInitializeSmartTerrain_11 = value;
	}

	inline static int32_t get_offset_of_mReconstructionToInitialize_12() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3340678586, ___mReconstructionToInitialize_12)); }
	inline ReconstructionFromTargetAbstractBehaviour_t147246886 * get_mReconstructionToInitialize_12() const { return ___mReconstructionToInitialize_12; }
	inline ReconstructionFromTargetAbstractBehaviour_t147246886 ** get_address_of_mReconstructionToInitialize_12() { return &___mReconstructionToInitialize_12; }
	inline void set_mReconstructionToInitialize_12(ReconstructionFromTargetAbstractBehaviour_t147246886 * value)
	{
		___mReconstructionToInitialize_12 = value;
		Il2CppCodeGenWriteBarrier(&___mReconstructionToInitialize_12, value);
	}

	inline static int32_t get_offset_of_mSmartTerrainOccluderBoundsMin_13() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3340678586, ___mSmartTerrainOccluderBoundsMin_13)); }
	inline Vector3_t4282066566  get_mSmartTerrainOccluderBoundsMin_13() const { return ___mSmartTerrainOccluderBoundsMin_13; }
	inline Vector3_t4282066566 * get_address_of_mSmartTerrainOccluderBoundsMin_13() { return &___mSmartTerrainOccluderBoundsMin_13; }
	inline void set_mSmartTerrainOccluderBoundsMin_13(Vector3_t4282066566  value)
	{
		___mSmartTerrainOccluderBoundsMin_13 = value;
	}

	inline static int32_t get_offset_of_mSmartTerrainOccluderBoundsMax_14() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3340678586, ___mSmartTerrainOccluderBoundsMax_14)); }
	inline Vector3_t4282066566  get_mSmartTerrainOccluderBoundsMax_14() const { return ___mSmartTerrainOccluderBoundsMax_14; }
	inline Vector3_t4282066566 * get_address_of_mSmartTerrainOccluderBoundsMax_14() { return &___mSmartTerrainOccluderBoundsMax_14; }
	inline void set_mSmartTerrainOccluderBoundsMax_14(Vector3_t4282066566  value)
	{
		___mSmartTerrainOccluderBoundsMax_14 = value;
	}

	inline static int32_t get_offset_of_mIsSmartTerrainOccluderOffset_15() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3340678586, ___mIsSmartTerrainOccluderOffset_15)); }
	inline bool get_mIsSmartTerrainOccluderOffset_15() const { return ___mIsSmartTerrainOccluderOffset_15; }
	inline bool* get_address_of_mIsSmartTerrainOccluderOffset_15() { return &___mIsSmartTerrainOccluderOffset_15; }
	inline void set_mIsSmartTerrainOccluderOffset_15(bool value)
	{
		___mIsSmartTerrainOccluderOffset_15 = value;
	}

	inline static int32_t get_offset_of_mSmartTerrainOccluderOffset_16() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3340678586, ___mSmartTerrainOccluderOffset_16)); }
	inline Vector3_t4282066566  get_mSmartTerrainOccluderOffset_16() const { return ___mSmartTerrainOccluderOffset_16; }
	inline Vector3_t4282066566 * get_address_of_mSmartTerrainOccluderOffset_16() { return &___mSmartTerrainOccluderOffset_16; }
	inline void set_mSmartTerrainOccluderOffset_16(Vector3_t4282066566  value)
	{
		___mSmartTerrainOccluderOffset_16 = value;
	}

	inline static int32_t get_offset_of_mSmartTerrainOccluderRotation_17() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3340678586, ___mSmartTerrainOccluderRotation_17)); }
	inline Quaternion_t1553702882  get_mSmartTerrainOccluderRotation_17() const { return ___mSmartTerrainOccluderRotation_17; }
	inline Quaternion_t1553702882 * get_address_of_mSmartTerrainOccluderRotation_17() { return &___mSmartTerrainOccluderRotation_17; }
	inline void set_mSmartTerrainOccluderRotation_17(Quaternion_t1553702882  value)
	{
		___mSmartTerrainOccluderRotation_17 = value;
	}

	inline static int32_t get_offset_of_mSmartTerrainOccluderLockedInPlace_18() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3340678586, ___mSmartTerrainOccluderLockedInPlace_18)); }
	inline bool get_mSmartTerrainOccluderLockedInPlace_18() const { return ___mSmartTerrainOccluderLockedInPlace_18; }
	inline bool* get_address_of_mSmartTerrainOccluderLockedInPlace_18() { return &___mSmartTerrainOccluderLockedInPlace_18; }
	inline void set_mSmartTerrainOccluderLockedInPlace_18(bool value)
	{
		___mSmartTerrainOccluderLockedInPlace_18 = value;
	}

	inline static int32_t get_offset_of_mAutoSetOccluderFromTargetSize_19() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3340678586, ___mAutoSetOccluderFromTargetSize_19)); }
	inline bool get_mAutoSetOccluderFromTargetSize_19() const { return ___mAutoSetOccluderFromTargetSize_19; }
	inline bool* get_address_of_mAutoSetOccluderFromTargetSize_19() { return &___mAutoSetOccluderFromTargetSize_19; }
	inline void set_mAutoSetOccluderFromTargetSize_19(bool value)
	{
		___mAutoSetOccluderFromTargetSize_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
