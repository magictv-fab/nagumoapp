﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.MemoryArchiveStorage
struct MemoryArchiveStorage_t3679306472;
// System.IO.MemoryStream
struct MemoryStream_t418716369;
// System.IO.Stream
struct Stream_t1561764144;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_File4232823638.h"
#include "mscorlib_System_IO_Stream1561764144.h"

// System.Void ICSharpCode.SharpZipLib.Zip.MemoryArchiveStorage::.ctor()
extern "C"  void MemoryArchiveStorage__ctor_m3091894723 (MemoryArchiveStorage_t3679306472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.MemoryArchiveStorage::.ctor(ICSharpCode.SharpZipLib.Zip.FileUpdateMode)
extern "C"  void MemoryArchiveStorage__ctor_m3318313405 (MemoryArchiveStorage_t3679306472 * __this, int32_t ___updateMode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.MemoryStream ICSharpCode.SharpZipLib.Zip.MemoryArchiveStorage::get_FinalStream()
extern "C"  MemoryStream_t418716369 * MemoryArchiveStorage_get_FinalStream_m3162623195 (MemoryArchiveStorage_t3679306472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream ICSharpCode.SharpZipLib.Zip.MemoryArchiveStorage::GetTemporaryOutput()
extern "C"  Stream_t1561764144 * MemoryArchiveStorage_GetTemporaryOutput_m2753528305 (MemoryArchiveStorage_t3679306472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream ICSharpCode.SharpZipLib.Zip.MemoryArchiveStorage::ConvertTemporaryToFinal()
extern "C"  Stream_t1561764144 * MemoryArchiveStorage_ConvertTemporaryToFinal_m2715757258 (MemoryArchiveStorage_t3679306472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream ICSharpCode.SharpZipLib.Zip.MemoryArchiveStorage::MakeTemporaryCopy(System.IO.Stream)
extern "C"  Stream_t1561764144 * MemoryArchiveStorage_MakeTemporaryCopy_m3894817724 (MemoryArchiveStorage_t3679306472 * __this, Stream_t1561764144 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream ICSharpCode.SharpZipLib.Zip.MemoryArchiveStorage::OpenForDirectUpdate(System.IO.Stream)
extern "C"  Stream_t1561764144 * MemoryArchiveStorage_OpenForDirectUpdate_m4190458933 (MemoryArchiveStorage_t3679306472 * __this, Stream_t1561764144 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.MemoryArchiveStorage::Dispose()
extern "C"  void MemoryArchiveStorage_Dispose_m3386710336 (MemoryArchiveStorage_t3679306472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
