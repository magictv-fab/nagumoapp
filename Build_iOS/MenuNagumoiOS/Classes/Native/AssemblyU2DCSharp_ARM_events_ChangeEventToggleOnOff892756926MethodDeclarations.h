﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.events.ChangeEventToggleOnOff
struct ChangeEventToggleOnOff_t892756926;

#include "codegen/il2cpp-codegen.h"

// System.Void ARM.events.ChangeEventToggleOnOff::.ctor()
extern "C"  void ChangeEventToggleOnOff__ctor_m3788867510 (ChangeEventToggleOnOff_t892756926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.events.ChangeEventToggleOnOff::Start()
extern "C"  void ChangeEventToggleOnOff_Start_m2736005302 (ChangeEventToggleOnOff_t892756926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.events.ChangeEventToggleOnOff::changedEventHandler(System.Single)
extern "C"  void ChangeEventToggleOnOff_changedEventHandler_m1345938899 (ChangeEventToggleOnOff_t892756926 * __this, float ___percentChanged0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
