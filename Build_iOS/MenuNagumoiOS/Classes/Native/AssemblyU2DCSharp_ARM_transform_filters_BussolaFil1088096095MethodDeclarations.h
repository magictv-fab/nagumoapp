﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.transform.filters.BussolaFilter
struct BussolaFilter_t1088096095;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_String7231557.h"

// System.Void ARM.transform.filters.BussolaFilter::.ctor()
extern "C"  void BussolaFilter__ctor_m472289545 (BussolaFilter_t1088096095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.BussolaFilter::Start()
extern "C"  void BussolaFilter_Start_m3714394633 (BussolaFilter_t1088096095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.BussolaFilter::Update()
extern "C"  void BussolaFilter_Update_m3482936100 (BussolaFilter_t1088096095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion ARM.transform.filters.BussolaFilter::_doFilter(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  BussolaFilter__doFilter_m691445813 (BussolaFilter_t1088096095 * __this, Quaternion_t1553702882  ___last0, Quaternion_t1553702882  ___nextValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.transform.filters.BussolaFilter::_doFilter(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  BussolaFilter__doFilter_m290673673 (BussolaFilter_t1088096095 * __this, Vector3_t4282066566  ___last0, Vector3_t4282066566  ___nextValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion ARM.transform.filters.BussolaFilter::getCameraInclination()
extern "C"  Quaternion_t1553702882  BussolaFilter_getCameraInclination_m83778810 (BussolaFilter_t1088096095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.BussolaFilter::OnGUI()
extern "C"  void BussolaFilter_OnGUI_m4262655491 (BussolaFilter_t1088096095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.BussolaFilter::configByString(System.String)
extern "C"  void BussolaFilter_configByString_m3912915581 (BussolaFilter_t1088096095 * __this, String_t* ___metadata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.BussolaFilter::Reset()
extern "C"  void BussolaFilter_Reset_m2413689782 (BussolaFilter_t1088096095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
