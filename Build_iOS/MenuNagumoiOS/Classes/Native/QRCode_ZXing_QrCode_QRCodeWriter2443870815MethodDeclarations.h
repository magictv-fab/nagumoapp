﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.QrCode.QRCodeWriter
struct QRCodeWriter_t2443870815;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// System.String
struct String_t;
// System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>
struct IDictionary_2_t3297010830;
// ZXing.QrCode.Internal.QRCode
struct QRCode_t3167984362;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "QRCode_ZXing_BarcodeFormat4201805817.h"
#include "QRCode_ZXing_QrCode_Internal_QRCode3167984362.h"

// ZXing.Common.BitMatrix ZXing.QrCode.QRCodeWriter::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern "C"  BitMatrix_t1058711404 * QRCodeWriter_encode_m3856882706 (QRCodeWriter_t2443870815 * __this, String_t* ___contents0, int32_t ___format1, int32_t ___width2, int32_t ___height3, Il2CppObject* ___hints4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.BitMatrix ZXing.QrCode.QRCodeWriter::renderResult(ZXing.QrCode.Internal.QRCode,System.Int32,System.Int32,System.Int32)
extern "C"  BitMatrix_t1058711404 * QRCodeWriter_renderResult_m1497174757 (Il2CppObject * __this /* static, unused */, QRCode_t3167984362 * ___code0, int32_t ___width1, int32_t ___height2, int32_t ___quietZone3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.QRCodeWriter::.ctor()
extern "C"  void QRCodeWriter__ctor_m3670081492 (QRCodeWriter_t2443870815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
