﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicApplicationSettings/<ResetHard>c__AnonStorey97
struct  U3CResetHardU3Ec__AnonStorey97_t4115973582  : public Il2CppObject
{
public:
	// System.String MagicApplicationSettings/<ResetHard>c__AnonStorey97::pathTemp
	String_t* ___pathTemp_0;

public:
	inline static int32_t get_offset_of_pathTemp_0() { return static_cast<int32_t>(offsetof(U3CResetHardU3Ec__AnonStorey97_t4115973582, ___pathTemp_0)); }
	inline String_t* get_pathTemp_0() const { return ___pathTemp_0; }
	inline String_t** get_address_of_pathTemp_0() { return &___pathTemp_0; }
	inline void set_pathTemp_0(String_t* value)
	{
		___pathTemp_0 = value;
		Il2CppCodeGenWriteBarrier(&___pathTemp_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
