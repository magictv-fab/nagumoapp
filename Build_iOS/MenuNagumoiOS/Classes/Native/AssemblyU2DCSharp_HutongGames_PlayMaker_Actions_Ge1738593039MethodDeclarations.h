﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetName
struct GetName_t1738593039;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetName::.ctor()
extern "C"  void GetName__ctor_m1058786567 (GetName_t1738593039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetName::Reset()
extern "C"  void GetName_Reset_m3000186804 (GetName_t1738593039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetName::OnEnter()
extern "C"  void GetName_OnEnter_m3273337630 (GetName_t1738593039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetName::OnUpdate()
extern "C"  void GetName_OnUpdate_m1822778085 (GetName_t1738593039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetName::DoGetGameObjectName()
extern "C"  void GetName_DoGetGameObjectName_m200644876 (GetName_t1738593039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
