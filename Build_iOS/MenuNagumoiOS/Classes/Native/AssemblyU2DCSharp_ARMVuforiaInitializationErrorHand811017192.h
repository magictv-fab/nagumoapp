﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2662109048;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARMVuforiaInitializationErrorHandler
struct  ARMVuforiaInitializationErrorHandler_t811017192  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject[] ARMVuforiaInitializationErrorHandler::DisableList
	GameObjectU5BU5D_t2662109048* ___DisableList_3;
	// UnityEngine.GameObject[] ARMVuforiaInitializationErrorHandler::EnaableList
	GameObjectU5BU5D_t2662109048* ___EnaableList_4;
	// System.String ARMVuforiaInitializationErrorHandler::mErrorText
	String_t* ___mErrorText_5;
	// System.Boolean ARMVuforiaInitializationErrorHandler::mErrorOccurred
	bool ___mErrorOccurred_6;
	// System.Boolean ARMVuforiaInitializationErrorHandler::_restart_when_return
	bool ____restart_when_return_7;

public:
	inline static int32_t get_offset_of_DisableList_3() { return static_cast<int32_t>(offsetof(ARMVuforiaInitializationErrorHandler_t811017192, ___DisableList_3)); }
	inline GameObjectU5BU5D_t2662109048* get_DisableList_3() const { return ___DisableList_3; }
	inline GameObjectU5BU5D_t2662109048** get_address_of_DisableList_3() { return &___DisableList_3; }
	inline void set_DisableList_3(GameObjectU5BU5D_t2662109048* value)
	{
		___DisableList_3 = value;
		Il2CppCodeGenWriteBarrier(&___DisableList_3, value);
	}

	inline static int32_t get_offset_of_EnaableList_4() { return static_cast<int32_t>(offsetof(ARMVuforiaInitializationErrorHandler_t811017192, ___EnaableList_4)); }
	inline GameObjectU5BU5D_t2662109048* get_EnaableList_4() const { return ___EnaableList_4; }
	inline GameObjectU5BU5D_t2662109048** get_address_of_EnaableList_4() { return &___EnaableList_4; }
	inline void set_EnaableList_4(GameObjectU5BU5D_t2662109048* value)
	{
		___EnaableList_4 = value;
		Il2CppCodeGenWriteBarrier(&___EnaableList_4, value);
	}

	inline static int32_t get_offset_of_mErrorText_5() { return static_cast<int32_t>(offsetof(ARMVuforiaInitializationErrorHandler_t811017192, ___mErrorText_5)); }
	inline String_t* get_mErrorText_5() const { return ___mErrorText_5; }
	inline String_t** get_address_of_mErrorText_5() { return &___mErrorText_5; }
	inline void set_mErrorText_5(String_t* value)
	{
		___mErrorText_5 = value;
		Il2CppCodeGenWriteBarrier(&___mErrorText_5, value);
	}

	inline static int32_t get_offset_of_mErrorOccurred_6() { return static_cast<int32_t>(offsetof(ARMVuforiaInitializationErrorHandler_t811017192, ___mErrorOccurred_6)); }
	inline bool get_mErrorOccurred_6() const { return ___mErrorOccurred_6; }
	inline bool* get_address_of_mErrorOccurred_6() { return &___mErrorOccurred_6; }
	inline void set_mErrorOccurred_6(bool value)
	{
		___mErrorOccurred_6 = value;
	}

	inline static int32_t get_offset_of__restart_when_return_7() { return static_cast<int32_t>(offsetof(ARMVuforiaInitializationErrorHandler_t811017192, ____restart_when_return_7)); }
	inline bool get__restart_when_return_7() const { return ____restart_when_return_7; }
	inline bool* get_address_of__restart_when_return_7() { return &____restart_when_return_7; }
	inline void set__restart_when_return_7(bool value)
	{
		____restart_when_return_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
