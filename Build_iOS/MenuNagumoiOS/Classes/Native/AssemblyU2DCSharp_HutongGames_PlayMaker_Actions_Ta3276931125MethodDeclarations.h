﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.TakeScreenshot
struct TakeScreenshot_t3276931125;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.TakeScreenshot::.ctor()
extern "C"  void TakeScreenshot__ctor_m1672836049 (TakeScreenshot_t3276931125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TakeScreenshot::Reset()
extern "C"  void TakeScreenshot_Reset_m3614236286 (TakeScreenshot_t3276931125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TakeScreenshot::OnEnter()
extern "C"  void TakeScreenshot_OnEnter_m669402984 (TakeScreenshot_t3276931125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
