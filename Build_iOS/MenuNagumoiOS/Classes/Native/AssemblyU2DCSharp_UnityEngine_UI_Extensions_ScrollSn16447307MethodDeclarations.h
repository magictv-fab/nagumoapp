﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Extensions.ScrollSnapBase/SelectionChangeEndEvent
struct SelectionChangeEndEvent_t16447307;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.Extensions.ScrollSnapBase/SelectionChangeEndEvent::.ctor()
extern "C"  void SelectionChangeEndEvent__ctor_m1949737904 (SelectionChangeEndEvent_t16447307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
