﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.in_apps.GarbageItemVO
struct GarbageItemVO_t1335202303;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.in_apps.GarbageItemVO::.ctor()
extern "C"  void GarbageItemVO__ctor_m3783678187 (GarbageItemVO_t1335202303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
