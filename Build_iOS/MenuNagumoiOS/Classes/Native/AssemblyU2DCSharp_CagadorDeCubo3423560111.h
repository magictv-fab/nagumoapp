﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// CagadorDeCubo
struct CagadorDeCubo_t3423560111;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CagadorDeCubo
struct  CagadorDeCubo_t3423560111  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject CagadorDeCubo::Cubo
	GameObject_t3674682005 * ___Cubo_2;

public:
	inline static int32_t get_offset_of_Cubo_2() { return static_cast<int32_t>(offsetof(CagadorDeCubo_t3423560111, ___Cubo_2)); }
	inline GameObject_t3674682005 * get_Cubo_2() const { return ___Cubo_2; }
	inline GameObject_t3674682005 ** get_address_of_Cubo_2() { return &___Cubo_2; }
	inline void set_Cubo_2(GameObject_t3674682005 * value)
	{
		___Cubo_2 = value;
		Il2CppCodeGenWriteBarrier(&___Cubo_2, value);
	}
};

struct CagadorDeCubo_t3423560111_StaticFields
{
public:
	// CagadorDeCubo CagadorDeCubo::Instance
	CagadorDeCubo_t3423560111 * ___Instance_3;

public:
	inline static int32_t get_offset_of_Instance_3() { return static_cast<int32_t>(offsetof(CagadorDeCubo_t3423560111_StaticFields, ___Instance_3)); }
	inline CagadorDeCubo_t3423560111 * get_Instance_3() const { return ___Instance_3; }
	inline CagadorDeCubo_t3423560111 ** get_address_of_Instance_3() { return &___Instance_3; }
	inline void set_Instance_3(CagadorDeCubo_t3423560111 * value)
	{
		___Instance_3 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
