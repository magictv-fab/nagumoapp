﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ScreenshotManager
struct ScreenshotManager_t4227900231;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Action`1<UnityEngine.Texture2D>
struct Action_1_t4279924331;
// System.Action`1<System.String>
struct Action_1_t403047693;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScreenshotManager
struct  ScreenshotManager_t4227900231  : public MonoBehaviour_t667441552
{
public:

public:
};

struct ScreenshotManager_t4227900231_StaticFields
{
public:
	// ScreenshotManager ScreenshotManager::instance
	ScreenshotManager_t4227900231 * ___instance_2;
	// UnityEngine.GameObject ScreenshotManager::go
	GameObject_t3674682005 * ___go_3;
	// System.Action`1<UnityEngine.Texture2D> ScreenshotManager::OnScreenshotTaken
	Action_1_t4279924331 * ___OnScreenshotTaken_4;
	// System.Action`1<System.String> ScreenshotManager::OnScreenshotSaved
	Action_1_t403047693 * ___OnScreenshotSaved_5;
	// System.Action`1<System.String> ScreenshotManager::OnImageSaved
	Action_1_t403047693 * ___OnImageSaved_6;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(ScreenshotManager_t4227900231_StaticFields, ___instance_2)); }
	inline ScreenshotManager_t4227900231 * get_instance_2() const { return ___instance_2; }
	inline ScreenshotManager_t4227900231 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(ScreenshotManager_t4227900231 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}

	inline static int32_t get_offset_of_go_3() { return static_cast<int32_t>(offsetof(ScreenshotManager_t4227900231_StaticFields, ___go_3)); }
	inline GameObject_t3674682005 * get_go_3() const { return ___go_3; }
	inline GameObject_t3674682005 ** get_address_of_go_3() { return &___go_3; }
	inline void set_go_3(GameObject_t3674682005 * value)
	{
		___go_3 = value;
		Il2CppCodeGenWriteBarrier(&___go_3, value);
	}

	inline static int32_t get_offset_of_OnScreenshotTaken_4() { return static_cast<int32_t>(offsetof(ScreenshotManager_t4227900231_StaticFields, ___OnScreenshotTaken_4)); }
	inline Action_1_t4279924331 * get_OnScreenshotTaken_4() const { return ___OnScreenshotTaken_4; }
	inline Action_1_t4279924331 ** get_address_of_OnScreenshotTaken_4() { return &___OnScreenshotTaken_4; }
	inline void set_OnScreenshotTaken_4(Action_1_t4279924331 * value)
	{
		___OnScreenshotTaken_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnScreenshotTaken_4, value);
	}

	inline static int32_t get_offset_of_OnScreenshotSaved_5() { return static_cast<int32_t>(offsetof(ScreenshotManager_t4227900231_StaticFields, ___OnScreenshotSaved_5)); }
	inline Action_1_t403047693 * get_OnScreenshotSaved_5() const { return ___OnScreenshotSaved_5; }
	inline Action_1_t403047693 ** get_address_of_OnScreenshotSaved_5() { return &___OnScreenshotSaved_5; }
	inline void set_OnScreenshotSaved_5(Action_1_t403047693 * value)
	{
		___OnScreenshotSaved_5 = value;
		Il2CppCodeGenWriteBarrier(&___OnScreenshotSaved_5, value);
	}

	inline static int32_t get_offset_of_OnImageSaved_6() { return static_cast<int32_t>(offsetof(ScreenshotManager_t4227900231_StaticFields, ___OnImageSaved_6)); }
	inline Action_1_t403047693 * get_OnImageSaved_6() const { return ___OnImageSaved_6; }
	inline Action_1_t403047693 ** get_address_of_OnImageSaved_6() { return &___OnImageSaved_6; }
	inline void set_OnImageSaved_6(Action_1_t403047693 * value)
	{
		___OnImageSaved_6 = value;
		Il2CppCodeGenWriteBarrier(&___OnImageSaved_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
