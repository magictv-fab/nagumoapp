﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2612654269.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23830311593.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.NetworkReachability,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3297746558_gshared (InternalEnumerator_1_t2612654269 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3297746558(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2612654269 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3297746558_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.NetworkReachability,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2337626722_gshared (InternalEnumerator_1_t2612654269 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2337626722(__this, method) ((  void (*) (InternalEnumerator_1_t2612654269 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2337626722_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.NetworkReachability,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1392964110_gshared (InternalEnumerator_1_t2612654269 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1392964110(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2612654269 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1392964110_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.NetworkReachability,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m921406933_gshared (InternalEnumerator_1_t2612654269 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m921406933(__this, method) ((  void (*) (InternalEnumerator_1_t2612654269 *, const MethodInfo*))InternalEnumerator_1_Dispose_m921406933_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.NetworkReachability,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2151973198_gshared (InternalEnumerator_1_t2612654269 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2151973198(__this, method) ((  bool (*) (InternalEnumerator_1_t2612654269 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2151973198_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.NetworkReachability,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t3830311593  InternalEnumerator_1_get_Current_m1095596677_gshared (InternalEnumerator_1_t2612654269 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1095596677(__this, method) ((  KeyValuePair_2_t3830311593  (*) (InternalEnumerator_1_t2612654269 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1095596677_gshared)(__this, method)
