﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Collections.Generic.List`1<DataRow>
struct List_1_t181054592;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataTable
struct  DataTable_t1629861412  : public Il2CppObject
{
public:
	// System.Int32 DataTable::last_insert_id
	int32_t ___last_insert_id_0;
	// System.Collections.Generic.List`1<System.String> DataTable::<Columns>k__BackingField
	List_1_t1375417109 * ___U3CColumnsU3Ek__BackingField_1;
	// System.Collections.Generic.List`1<DataRow> DataTable::<Rows>k__BackingField
	List_1_t181054592 * ___U3CRowsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_last_insert_id_0() { return static_cast<int32_t>(offsetof(DataTable_t1629861412, ___last_insert_id_0)); }
	inline int32_t get_last_insert_id_0() const { return ___last_insert_id_0; }
	inline int32_t* get_address_of_last_insert_id_0() { return &___last_insert_id_0; }
	inline void set_last_insert_id_0(int32_t value)
	{
		___last_insert_id_0 = value;
	}

	inline static int32_t get_offset_of_U3CColumnsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DataTable_t1629861412, ___U3CColumnsU3Ek__BackingField_1)); }
	inline List_1_t1375417109 * get_U3CColumnsU3Ek__BackingField_1() const { return ___U3CColumnsU3Ek__BackingField_1; }
	inline List_1_t1375417109 ** get_address_of_U3CColumnsU3Ek__BackingField_1() { return &___U3CColumnsU3Ek__BackingField_1; }
	inline void set_U3CColumnsU3Ek__BackingField_1(List_1_t1375417109 * value)
	{
		___U3CColumnsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CColumnsU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CRowsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DataTable_t1629861412, ___U3CRowsU3Ek__BackingField_2)); }
	inline List_1_t181054592 * get_U3CRowsU3Ek__BackingField_2() const { return ___U3CRowsU3Ek__BackingField_2; }
	inline List_1_t181054592 ** get_address_of_U3CRowsU3Ek__BackingField_2() { return &___U3CRowsU3Ek__BackingField_2; }
	inline void set_U3CRowsU3Ek__BackingField_2(List_1_t181054592 * value)
	{
		___U3CRowsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRowsU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
