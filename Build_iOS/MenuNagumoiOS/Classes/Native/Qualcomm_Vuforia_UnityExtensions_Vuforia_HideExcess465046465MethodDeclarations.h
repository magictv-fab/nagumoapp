﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.HideExcessAreaAbstractBehaviour
struct HideExcessAreaAbstractBehaviour_t465046465;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

// UnityEngine.GameObject Vuforia.HideExcessAreaAbstractBehaviour::CreateQuad(UnityEngine.GameObject,System.String,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3,System.Int32)
extern "C"  GameObject_t3674682005 * HideExcessAreaAbstractBehaviour_CreateQuad_m3568086448 (HideExcessAreaAbstractBehaviour_t465046465 * __this, GameObject_t3674682005 * ___parent0, String_t* ___name1, Vector3_t4282066566  ___position2, Quaternion_t1553702882  ___rotation3, Vector3_t4282066566  ___scale4, int32_t ___layer5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::SetPlanesActive(System.Boolean)
extern "C"  void HideExcessAreaAbstractBehaviour_SetPlanesActive_m2234589432 (HideExcessAreaAbstractBehaviour_t465046465 * __this, bool ___activeflag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::SetPlanesRenderingActive(System.Boolean)
extern "C"  void HideExcessAreaAbstractBehaviour_SetPlanesRenderingActive_m2412753264 (HideExcessAreaAbstractBehaviour_t465046465 * __this, bool ___activeflag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.HideExcessAreaAbstractBehaviour::isPlanesRenderingActive()
extern "C"  bool HideExcessAreaAbstractBehaviour_isPlanesRenderingActive_m3721059615 (HideExcessAreaAbstractBehaviour_t465046465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::ForceUpdateHideBehaviours()
extern "C"  void HideExcessAreaAbstractBehaviour_ForceUpdateHideBehaviours_m646142638 (HideExcessAreaAbstractBehaviour_t465046465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnVuforiaStarted()
extern "C"  void HideExcessAreaAbstractBehaviour_OnVuforiaStarted_m2079086654 (HideExcessAreaAbstractBehaviour_t465046465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.HideExcessAreaAbstractBehaviour::HasCalculationDataChanged()
extern "C"  bool HideExcessAreaAbstractBehaviour_HasCalculationDataChanged_m2839413897 (HideExcessAreaAbstractBehaviour_t465046465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnPreCull()
extern "C"  void HideExcessAreaAbstractBehaviour_OnPreCull_m3379802904 (HideExcessAreaAbstractBehaviour_t465046465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnPostRender()
extern "C"  void HideExcessAreaAbstractBehaviour_OnPostRender_m2386892277 (HideExcessAreaAbstractBehaviour_t465046465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::Start()
extern "C"  void HideExcessAreaAbstractBehaviour_Start_m3503597860 (HideExcessAreaAbstractBehaviour_t465046465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnDisable()
extern "C"  void HideExcessAreaAbstractBehaviour_OnDisable_m1175499403 (HideExcessAreaAbstractBehaviour_t465046465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnDestroy()
extern "C"  void HideExcessAreaAbstractBehaviour_OnDestroy_m3353031453 (HideExcessAreaAbstractBehaviour_t465046465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::Update()
extern "C"  void HideExcessAreaAbstractBehaviour_Update_m1243203433 (HideExcessAreaAbstractBehaviour_t465046465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::.ctor()
extern "C"  void HideExcessAreaAbstractBehaviour__ctor_m261492772 (HideExcessAreaAbstractBehaviour_t465046465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
