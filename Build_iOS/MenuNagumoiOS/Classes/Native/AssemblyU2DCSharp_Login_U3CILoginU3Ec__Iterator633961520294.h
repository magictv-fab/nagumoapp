﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// UnityEngine.WWW
struct WWW_t3134621005;
// System.Object
struct Il2CppObject;
// Login
struct Login_t73596745;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Login/<ILogin>c__Iterator63
struct  U3CILoginU3Ec__Iterator63_t3961520294  : public Il2CppObject
{
public:
	// System.String Login/<ILogin>c__Iterator63::<json>__0
	String_t* ___U3CjsonU3E__0_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Login/<ILogin>c__Iterator63::<headers>__1
	Dictionary_2_t827649927 * ___U3CheadersU3E__1_1;
	// System.Byte[] Login/<ILogin>c__Iterator63::<pData>__2
	ByteU5BU5D_t4260760469* ___U3CpDataU3E__2_2;
	// UnityEngine.WWW Login/<ILogin>c__Iterator63::<www>__3
	WWW_t3134621005 * ___U3CwwwU3E__3_3;
	// System.String Login/<ILogin>c__Iterator63::<code>__4
	String_t* ___U3CcodeU3E__4_4;
	// System.String Login/<ILogin>c__Iterator63::<message>__5
	String_t* ___U3CmessageU3E__5_5;
	// System.Int32 Login/<ILogin>c__Iterator63::$PC
	int32_t ___U24PC_6;
	// System.Object Login/<ILogin>c__Iterator63::$current
	Il2CppObject * ___U24current_7;
	// Login Login/<ILogin>c__Iterator63::<>f__this
	Login_t73596745 * ___U3CU3Ef__this_8;

public:
	inline static int32_t get_offset_of_U3CjsonU3E__0_0() { return static_cast<int32_t>(offsetof(U3CILoginU3Ec__Iterator63_t3961520294, ___U3CjsonU3E__0_0)); }
	inline String_t* get_U3CjsonU3E__0_0() const { return ___U3CjsonU3E__0_0; }
	inline String_t** get_address_of_U3CjsonU3E__0_0() { return &___U3CjsonU3E__0_0; }
	inline void set_U3CjsonU3E__0_0(String_t* value)
	{
		___U3CjsonU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjsonU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CheadersU3E__1_1() { return static_cast<int32_t>(offsetof(U3CILoginU3Ec__Iterator63_t3961520294, ___U3CheadersU3E__1_1)); }
	inline Dictionary_2_t827649927 * get_U3CheadersU3E__1_1() const { return ___U3CheadersU3E__1_1; }
	inline Dictionary_2_t827649927 ** get_address_of_U3CheadersU3E__1_1() { return &___U3CheadersU3E__1_1; }
	inline void set_U3CheadersU3E__1_1(Dictionary_2_t827649927 * value)
	{
		___U3CheadersU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CheadersU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CpDataU3E__2_2() { return static_cast<int32_t>(offsetof(U3CILoginU3Ec__Iterator63_t3961520294, ___U3CpDataU3E__2_2)); }
	inline ByteU5BU5D_t4260760469* get_U3CpDataU3E__2_2() const { return ___U3CpDataU3E__2_2; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CpDataU3E__2_2() { return &___U3CpDataU3E__2_2; }
	inline void set_U3CpDataU3E__2_2(ByteU5BU5D_t4260760469* value)
	{
		___U3CpDataU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpDataU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__3_3() { return static_cast<int32_t>(offsetof(U3CILoginU3Ec__Iterator63_t3961520294, ___U3CwwwU3E__3_3)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__3_3() const { return ___U3CwwwU3E__3_3; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__3_3() { return &___U3CwwwU3E__3_3; }
	inline void set_U3CwwwU3E__3_3(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CcodeU3E__4_4() { return static_cast<int32_t>(offsetof(U3CILoginU3Ec__Iterator63_t3961520294, ___U3CcodeU3E__4_4)); }
	inline String_t* get_U3CcodeU3E__4_4() const { return ___U3CcodeU3E__4_4; }
	inline String_t** get_address_of_U3CcodeU3E__4_4() { return &___U3CcodeU3E__4_4; }
	inline void set_U3CcodeU3E__4_4(String_t* value)
	{
		___U3CcodeU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcodeU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U3CmessageU3E__5_5() { return static_cast<int32_t>(offsetof(U3CILoginU3Ec__Iterator63_t3961520294, ___U3CmessageU3E__5_5)); }
	inline String_t* get_U3CmessageU3E__5_5() const { return ___U3CmessageU3E__5_5; }
	inline String_t** get_address_of_U3CmessageU3E__5_5() { return &___U3CmessageU3E__5_5; }
	inline void set_U3CmessageU3E__5_5(String_t* value)
	{
		___U3CmessageU3E__5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmessageU3E__5_5, value);
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CILoginU3Ec__Iterator63_t3961520294, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CILoginU3Ec__Iterator63_t3961520294, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_8() { return static_cast<int32_t>(offsetof(U3CILoginU3Ec__Iterator63_t3961520294, ___U3CU3Ef__this_8)); }
	inline Login_t73596745 * get_U3CU3Ef__this_8() const { return ___U3CU3Ef__this_8; }
	inline Login_t73596745 ** get_address_of_U3CU3Ef__this_8() { return &___U3CU3Ef__this_8; }
	inline void set_U3CU3Ef__this_8(Login_t73596745 * value)
	{
		___U3CU3Ef__this_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
