﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MinhasOfertasManager
struct MinhasOfertasManager_t1225294387;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MinhasOfertasManager::.ctor()
extern "C"  void MinhasOfertasManager__ctor_m1832100296 (MinhasOfertasManager_t1225294387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MinhasOfertasManager::.cctor()
extern "C"  void MinhasOfertasManager__cctor_m478438117 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MinhasOfertasManager::Start()
extern "C"  void MinhasOfertasManager_Start_m779238088 (MinhasOfertasManager_t1225294387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MinhasOfertasManager::Update()
extern "C"  void MinhasOfertasManager_Update_m2687396421 (MinhasOfertasManager_t1225294387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MinhasOfertasManager::InstantiateOfertas()
extern "C"  void MinhasOfertasManager_InstantiateOfertas_m144214868 (MinhasOfertasManager_t1225294387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MinhasOfertasManager::MinhaOfertaRemove(System.String)
extern "C"  void MinhasOfertasManager_MinhaOfertaRemove_m1327554780 (MinhasOfertasManager_t1225294387 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MinhasOfertasManager::IMinhaOfertaRemove(System.String)
extern "C"  Il2CppObject * MinhasOfertasManager_IMinhaOfertaRemove_m2204259847 (MinhasOfertasManager_t1225294387 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MinhasOfertasManager::ILoadOfertas()
extern "C"  Il2CppObject * MinhasOfertasManager_ILoadOfertas_m3196889463 (MinhasOfertasManager_t1225294387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MinhasOfertasManager::ILoadImgs()
extern "C"  Il2CppObject * MinhasOfertasManager_ILoadImgs_m2146332157 (MinhasOfertasManager_t1225294387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MinhasOfertasManager::LoadLoadeds()
extern "C"  Il2CppObject * MinhasOfertasManager_LoadLoadeds_m3551012646 (MinhasOfertasManager_t1225294387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MinhasOfertasManager::PopUp(System.String)
extern "C"  void MinhasOfertasManager_PopUp_m99639184 (MinhasOfertasManager_t1225294387 * __this, String_t* ___txt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
