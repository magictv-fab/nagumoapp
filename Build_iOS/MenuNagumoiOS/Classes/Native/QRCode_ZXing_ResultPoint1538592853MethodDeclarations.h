﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.ResultPoint
struct ResultPoint_t1538592853;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// ZXing.ResultPoint[]
struct ResultPointU5BU5D_t1195164344;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "QRCode_ZXing_ResultPoint1538592853.h"

// System.Void ZXing.ResultPoint::.ctor(System.Single,System.Single)
extern "C"  void ResultPoint__ctor_m1352572270 (ResultPoint_t1538592853 * __this, float ___x0, float ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ZXing.ResultPoint::get_X()
extern "C"  float ResultPoint_get_X_m3421039515 (ResultPoint_t1538592853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ZXing.ResultPoint::get_Y()
extern "C"  float ResultPoint_get_Y_m3421040476 (ResultPoint_t1538592853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.ResultPoint::Equals(System.Object)
extern "C"  bool ResultPoint_Equals_m2045323527 (ResultPoint_t1538592853 * __this, Il2CppObject * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.ResultPoint::GetHashCode()
extern "C"  int32_t ResultPoint_GetHashCode_m4021501215 (ResultPoint_t1538592853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.ResultPoint::ToString()
extern "C"  String_t* ResultPoint_ToString_m2550817291 (ResultPoint_t1538592853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.ResultPoint::orderBestPatterns(ZXing.ResultPoint[])
extern "C"  void ResultPoint_orderBestPatterns_m2255383890 (Il2CppObject * __this /* static, unused */, ResultPointU5BU5D_t1195164344* ___patterns0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ZXing.ResultPoint::distance(ZXing.ResultPoint,ZXing.ResultPoint)
extern "C"  float ResultPoint_distance_m4219569409 (Il2CppObject * __this /* static, unused */, ResultPoint_t1538592853 * ___pattern10, ResultPoint_t1538592853 * ___pattern21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ZXing.ResultPoint::crossProductZ(ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint)
extern "C"  float ResultPoint_crossProductZ_m2198801944 (Il2CppObject * __this /* static, unused */, ResultPoint_t1538592853 * ___pointA0, ResultPoint_t1538592853 * ___pointB1, ResultPoint_t1538592853 * ___pointC2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
