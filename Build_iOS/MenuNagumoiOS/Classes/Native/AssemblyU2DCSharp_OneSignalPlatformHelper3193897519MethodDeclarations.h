﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OneSignalPlatformHelper
struct OneSignalPlatformHelper_t3193897519;
// OSPermissionSubscriptionState
struct OSPermissionSubscriptionState_t4252985057;
// OneSignalPlatform
struct OneSignalPlatform_t4133004065;
// System.String
struct String_t;
// OSPermissionStateChanges
struct OSPermissionStateChanges_t1134260069;
// OSSubscriptionStateChanges
struct OSSubscriptionStateChanges_t52612275;
// OSEmailSubscriptionStateChanges
struct OSEmailSubscriptionStateChanges_t721307367;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void OneSignalPlatformHelper::.ctor()
extern "C"  void OneSignalPlatformHelper__ctor_m518873628 (OneSignalPlatformHelper_t3193897519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OSPermissionSubscriptionState OneSignalPlatformHelper::parsePermissionSubscriptionState(OneSignalPlatform,System.String)
extern "C"  OSPermissionSubscriptionState_t4252985057 * OneSignalPlatformHelper_parsePermissionSubscriptionState_m936893381 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___platform0, String_t* ___jsonStr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OSPermissionStateChanges OneSignalPlatformHelper::parseOSPermissionStateChanges(OneSignalPlatform,System.String)
extern "C"  OSPermissionStateChanges_t1134260069 * OneSignalPlatformHelper_parseOSPermissionStateChanges_m221869059 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___platform0, String_t* ___stateChangesJSONString1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OSSubscriptionStateChanges OneSignalPlatformHelper::parseOSSubscriptionStateChanges(OneSignalPlatform,System.String)
extern "C"  OSSubscriptionStateChanges_t52612275 * OneSignalPlatformHelper_parseOSSubscriptionStateChanges_m3365947011 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___platform0, String_t* ___stateChangesJSONString1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OSEmailSubscriptionStateChanges OneSignalPlatformHelper::parseOSEmailSubscriptionStateChanges(OneSignalPlatform,System.String)
extern "C"  OSEmailSubscriptionStateChanges_t721307367 * OneSignalPlatformHelper_parseOSEmailSubscriptionStateChanges_m2158073309 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___platform0, String_t* ___stateChangesJSONString1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
