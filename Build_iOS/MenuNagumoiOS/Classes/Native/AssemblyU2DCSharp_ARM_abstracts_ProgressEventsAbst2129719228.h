﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.abstracts.ProgressEventsAbstract/OnProgressEventHandler
struct OnProgressEventHandler_t3695678548;
// ARM.abstracts.ProgressEventsAbstract/OnErrorEventHandler
struct OnErrorEventHandler_t1722022561;
// ARM.abstracts.ProgressEventsAbstract/OnCompleteEventHandler
struct OnCompleteEventHandler_t2318258528;

#include "AssemblyU2DCSharp_ARM_animation_GenericStartableCo1794600275.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.abstracts.ProgressEventsAbstract
struct  ProgressEventsAbstract_t2129719228  : public GenericStartableComponentControllAbstract_t1794600275
{
public:
	// ARM.abstracts.ProgressEventsAbstract/OnProgressEventHandler ARM.abstracts.ProgressEventsAbstract::onProgress
	OnProgressEventHandler_t3695678548 * ___onProgress_8;
	// ARM.abstracts.ProgressEventsAbstract/OnErrorEventHandler ARM.abstracts.ProgressEventsAbstract::onError
	OnErrorEventHandler_t1722022561 * ___onError_9;
	// System.Boolean ARM.abstracts.ProgressEventsAbstract::_isComplete
	bool ____isComplete_10;
	// ARM.abstracts.ProgressEventsAbstract/OnCompleteEventHandler ARM.abstracts.ProgressEventsAbstract::onComplete
	OnCompleteEventHandler_t2318258528 * ___onComplete_11;
	// System.Single ARM.abstracts.ProgressEventsAbstract::weight
	float ___weight_12;
	// System.Single ARM.abstracts.ProgressEventsAbstract::_currentProgress
	float ____currentProgress_13;

public:
	inline static int32_t get_offset_of_onProgress_8() { return static_cast<int32_t>(offsetof(ProgressEventsAbstract_t2129719228, ___onProgress_8)); }
	inline OnProgressEventHandler_t3695678548 * get_onProgress_8() const { return ___onProgress_8; }
	inline OnProgressEventHandler_t3695678548 ** get_address_of_onProgress_8() { return &___onProgress_8; }
	inline void set_onProgress_8(OnProgressEventHandler_t3695678548 * value)
	{
		___onProgress_8 = value;
		Il2CppCodeGenWriteBarrier(&___onProgress_8, value);
	}

	inline static int32_t get_offset_of_onError_9() { return static_cast<int32_t>(offsetof(ProgressEventsAbstract_t2129719228, ___onError_9)); }
	inline OnErrorEventHandler_t1722022561 * get_onError_9() const { return ___onError_9; }
	inline OnErrorEventHandler_t1722022561 ** get_address_of_onError_9() { return &___onError_9; }
	inline void set_onError_9(OnErrorEventHandler_t1722022561 * value)
	{
		___onError_9 = value;
		Il2CppCodeGenWriteBarrier(&___onError_9, value);
	}

	inline static int32_t get_offset_of__isComplete_10() { return static_cast<int32_t>(offsetof(ProgressEventsAbstract_t2129719228, ____isComplete_10)); }
	inline bool get__isComplete_10() const { return ____isComplete_10; }
	inline bool* get_address_of__isComplete_10() { return &____isComplete_10; }
	inline void set__isComplete_10(bool value)
	{
		____isComplete_10 = value;
	}

	inline static int32_t get_offset_of_onComplete_11() { return static_cast<int32_t>(offsetof(ProgressEventsAbstract_t2129719228, ___onComplete_11)); }
	inline OnCompleteEventHandler_t2318258528 * get_onComplete_11() const { return ___onComplete_11; }
	inline OnCompleteEventHandler_t2318258528 ** get_address_of_onComplete_11() { return &___onComplete_11; }
	inline void set_onComplete_11(OnCompleteEventHandler_t2318258528 * value)
	{
		___onComplete_11 = value;
		Il2CppCodeGenWriteBarrier(&___onComplete_11, value);
	}

	inline static int32_t get_offset_of_weight_12() { return static_cast<int32_t>(offsetof(ProgressEventsAbstract_t2129719228, ___weight_12)); }
	inline float get_weight_12() const { return ___weight_12; }
	inline float* get_address_of_weight_12() { return &___weight_12; }
	inline void set_weight_12(float value)
	{
		___weight_12 = value;
	}

	inline static int32_t get_offset_of__currentProgress_13() { return static_cast<int32_t>(offsetof(ProgressEventsAbstract_t2129719228, ____currentProgress_13)); }
	inline float get__currentProgress_13() const { return ____currentProgress_13; }
	inline float* get_address_of__currentProgress_13() { return &____currentProgress_13; }
	inline void set__currentProgress_13(float value)
	{
		____currentProgress_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
