﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// UnityEngine.Animator
struct Animator_t2776330603;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorLayersAffectMassCenter
struct  GetAnimatorLayersAffectMassCenter_t2244244939  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorLayersAffectMassCenter::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetAnimatorLayersAffectMassCenter::affectMassCenter
	FsmBool_t1075959796 * ___affectMassCenter_10;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorLayersAffectMassCenter::affectMassCenterEvent
	FsmEvent_t2133468028 * ___affectMassCenterEvent_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorLayersAffectMassCenter::doNotAffectMassCenterEvent
	FsmEvent_t2133468028 * ___doNotAffectMassCenterEvent_12;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorLayersAffectMassCenter::_animator
	Animator_t2776330603 * ____animator_13;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(GetAnimatorLayersAffectMassCenter_t2244244939, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_affectMassCenter_10() { return static_cast<int32_t>(offsetof(GetAnimatorLayersAffectMassCenter_t2244244939, ___affectMassCenter_10)); }
	inline FsmBool_t1075959796 * get_affectMassCenter_10() const { return ___affectMassCenter_10; }
	inline FsmBool_t1075959796 ** get_address_of_affectMassCenter_10() { return &___affectMassCenter_10; }
	inline void set_affectMassCenter_10(FsmBool_t1075959796 * value)
	{
		___affectMassCenter_10 = value;
		Il2CppCodeGenWriteBarrier(&___affectMassCenter_10, value);
	}

	inline static int32_t get_offset_of_affectMassCenterEvent_11() { return static_cast<int32_t>(offsetof(GetAnimatorLayersAffectMassCenter_t2244244939, ___affectMassCenterEvent_11)); }
	inline FsmEvent_t2133468028 * get_affectMassCenterEvent_11() const { return ___affectMassCenterEvent_11; }
	inline FsmEvent_t2133468028 ** get_address_of_affectMassCenterEvent_11() { return &___affectMassCenterEvent_11; }
	inline void set_affectMassCenterEvent_11(FsmEvent_t2133468028 * value)
	{
		___affectMassCenterEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___affectMassCenterEvent_11, value);
	}

	inline static int32_t get_offset_of_doNotAffectMassCenterEvent_12() { return static_cast<int32_t>(offsetof(GetAnimatorLayersAffectMassCenter_t2244244939, ___doNotAffectMassCenterEvent_12)); }
	inline FsmEvent_t2133468028 * get_doNotAffectMassCenterEvent_12() const { return ___doNotAffectMassCenterEvent_12; }
	inline FsmEvent_t2133468028 ** get_address_of_doNotAffectMassCenterEvent_12() { return &___doNotAffectMassCenterEvent_12; }
	inline void set_doNotAffectMassCenterEvent_12(FsmEvent_t2133468028 * value)
	{
		___doNotAffectMassCenterEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___doNotAffectMassCenterEvent_12, value);
	}

	inline static int32_t get_offset_of__animator_13() { return static_cast<int32_t>(offsetof(GetAnimatorLayersAffectMassCenter_t2244244939, ____animator_13)); }
	inline Animator_t2776330603 * get__animator_13() const { return ____animator_13; }
	inline Animator_t2776330603 ** get_address_of__animator_13() { return &____animator_13; }
	inline void set__animator_13(Animator_t2776330603 * value)
	{
		____animator_13 = value;
		Il2CppCodeGenWriteBarrier(&____animator_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
