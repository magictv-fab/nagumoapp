﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NotificationObj
struct NotificationObj_t1964455916;

#include "codegen/il2cpp-codegen.h"

// System.Void NotificationObj::.ctor()
extern "C"  void NotificationObj__ctor_m569555199 (NotificationObj_t1964455916 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationObj::Start()
extern "C"  void NotificationObj_Start_m3811660287 (NotificationObj_t1964455916 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationObj::Update()
extern "C"  void NotificationObj_Update_m2203204078 (NotificationObj_t1964455916 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
