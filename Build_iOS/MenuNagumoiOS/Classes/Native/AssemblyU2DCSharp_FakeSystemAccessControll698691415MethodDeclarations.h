﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FakeSystemAccessControll
struct FakeSystemAccessControll_t698691415;

#include "codegen/il2cpp-codegen.h"

// System.Void FakeSystemAccessControll::.ctor()
extern "C"  void FakeSystemAccessControll__ctor_m3791040804 (FakeSystemAccessControll_t698691415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FakeSystemAccessControll::prepare()
extern "C"  void FakeSystemAccessControll_prepare_m1984926057 (FakeSystemAccessControll_t698691415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FakeSystemAccessControll::Play()
extern "C"  void FakeSystemAccessControll_Play_m549246420 (FakeSystemAccessControll_t698691415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FakeSystemAccessControll::Stop()
extern "C"  void FakeSystemAccessControll_Stop_m642930466 (FakeSystemAccessControll_t698691415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FakeSystemAccessControll::Pause()
extern "C"  void FakeSystemAccessControll_Pause_m3845166776 (FakeSystemAccessControll_t698691415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
