﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen396978522.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_InApp_InAppEventDispatcher_Event1614635846.h"

// System.Void System.Array/InternalEnumerator`1<InApp.InAppEventDispatcher/EventNames>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1791103051_gshared (InternalEnumerator_1_t396978522 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1791103051(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t396978522 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1791103051_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<InApp.InAppEventDispatcher/EventNames>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4028363637_gshared (InternalEnumerator_1_t396978522 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4028363637(__this, method) ((  void (*) (InternalEnumerator_1_t396978522 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4028363637_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<InApp.InAppEventDispatcher/EventNames>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3738985579_gshared (InternalEnumerator_1_t396978522 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3738985579(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t396978522 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3738985579_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<InApp.InAppEventDispatcher/EventNames>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3908987874_gshared (InternalEnumerator_1_t396978522 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3908987874(__this, method) ((  void (*) (InternalEnumerator_1_t396978522 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3908987874_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<InApp.InAppEventDispatcher/EventNames>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m206693413_gshared (InternalEnumerator_1_t396978522 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m206693413(__this, method) ((  bool (*) (InternalEnumerator_1_t396978522 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m206693413_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<InApp.InAppEventDispatcher/EventNames>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m4187474036_gshared (InternalEnumerator_1_t396978522 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m4187474036(__this, method) ((  int32_t (*) (InternalEnumerator_1_t396978522 *, const MethodInfo*))InternalEnumerator_1_get_Current_m4187474036_gshared)(__this, method)
