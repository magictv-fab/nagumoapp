﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MagicTV.globals.events.StateMagineEvents/OnChangeToStateEventHandler
struct OnChangeToStateEventHandler_t4233189127;
// MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler
struct OnChangeToAnStateEventHandler_t630243546;
// System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler>
struct Dictionary_2_t2700946943;
// MagicTV.globals.events.StateMagineEvents
struct StateMagineEvents_t1499601201;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.globals.events.StateMagineEvents
struct  StateMagineEvents_t1499601201  : public Il2CppObject
{
public:
	// MagicTV.globals.events.StateMagineEvents/OnChangeToStateEventHandler MagicTV.globals.events.StateMagineEvents::onChangeToState
	OnChangeToStateEventHandler_t4233189127 * ___onChangeToState_0;
	// MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler MagicTV.globals.events.StateMagineEvents::onChangeToStateInit
	OnChangeToAnStateEventHandler_t630243546 * ___onChangeToStateInit_1;
	// MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler MagicTV.globals.events.StateMagineEvents::onChangeToStateUpdate
	OnChangeToAnStateEventHandler_t630243546 * ___onChangeToStateUpdate_2;
	// MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler MagicTV.globals.events.StateMagineEvents::onChangeToStateOpen
	OnChangeToAnStateEventHandler_t630243546 * ___onChangeToStateOpen_3;
	// System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler> MagicTV.globals.events.StateMagineEvents::eventsList
	Dictionary_2_t2700946943 * ___eventsList_4;

public:
	inline static int32_t get_offset_of_onChangeToState_0() { return static_cast<int32_t>(offsetof(StateMagineEvents_t1499601201, ___onChangeToState_0)); }
	inline OnChangeToStateEventHandler_t4233189127 * get_onChangeToState_0() const { return ___onChangeToState_0; }
	inline OnChangeToStateEventHandler_t4233189127 ** get_address_of_onChangeToState_0() { return &___onChangeToState_0; }
	inline void set_onChangeToState_0(OnChangeToStateEventHandler_t4233189127 * value)
	{
		___onChangeToState_0 = value;
		Il2CppCodeGenWriteBarrier(&___onChangeToState_0, value);
	}

	inline static int32_t get_offset_of_onChangeToStateInit_1() { return static_cast<int32_t>(offsetof(StateMagineEvents_t1499601201, ___onChangeToStateInit_1)); }
	inline OnChangeToAnStateEventHandler_t630243546 * get_onChangeToStateInit_1() const { return ___onChangeToStateInit_1; }
	inline OnChangeToAnStateEventHandler_t630243546 ** get_address_of_onChangeToStateInit_1() { return &___onChangeToStateInit_1; }
	inline void set_onChangeToStateInit_1(OnChangeToAnStateEventHandler_t630243546 * value)
	{
		___onChangeToStateInit_1 = value;
		Il2CppCodeGenWriteBarrier(&___onChangeToStateInit_1, value);
	}

	inline static int32_t get_offset_of_onChangeToStateUpdate_2() { return static_cast<int32_t>(offsetof(StateMagineEvents_t1499601201, ___onChangeToStateUpdate_2)); }
	inline OnChangeToAnStateEventHandler_t630243546 * get_onChangeToStateUpdate_2() const { return ___onChangeToStateUpdate_2; }
	inline OnChangeToAnStateEventHandler_t630243546 ** get_address_of_onChangeToStateUpdate_2() { return &___onChangeToStateUpdate_2; }
	inline void set_onChangeToStateUpdate_2(OnChangeToAnStateEventHandler_t630243546 * value)
	{
		___onChangeToStateUpdate_2 = value;
		Il2CppCodeGenWriteBarrier(&___onChangeToStateUpdate_2, value);
	}

	inline static int32_t get_offset_of_onChangeToStateOpen_3() { return static_cast<int32_t>(offsetof(StateMagineEvents_t1499601201, ___onChangeToStateOpen_3)); }
	inline OnChangeToAnStateEventHandler_t630243546 * get_onChangeToStateOpen_3() const { return ___onChangeToStateOpen_3; }
	inline OnChangeToAnStateEventHandler_t630243546 ** get_address_of_onChangeToStateOpen_3() { return &___onChangeToStateOpen_3; }
	inline void set_onChangeToStateOpen_3(OnChangeToAnStateEventHandler_t630243546 * value)
	{
		___onChangeToStateOpen_3 = value;
		Il2CppCodeGenWriteBarrier(&___onChangeToStateOpen_3, value);
	}

	inline static int32_t get_offset_of_eventsList_4() { return static_cast<int32_t>(offsetof(StateMagineEvents_t1499601201, ___eventsList_4)); }
	inline Dictionary_2_t2700946943 * get_eventsList_4() const { return ___eventsList_4; }
	inline Dictionary_2_t2700946943 ** get_address_of_eventsList_4() { return &___eventsList_4; }
	inline void set_eventsList_4(Dictionary_2_t2700946943 * value)
	{
		___eventsList_4 = value;
		Il2CppCodeGenWriteBarrier(&___eventsList_4, value);
	}
};

struct StateMagineEvents_t1499601201_StaticFields
{
public:
	// MagicTV.globals.events.StateMagineEvents MagicTV.globals.events.StateMagineEvents::_instance
	StateMagineEvents_t1499601201 * ____instance_5;

public:
	inline static int32_t get_offset_of__instance_5() { return static_cast<int32_t>(offsetof(StateMagineEvents_t1499601201_StaticFields, ____instance_5)); }
	inline StateMagineEvents_t1499601201 * get__instance_5() const { return ____instance_5; }
	inline StateMagineEvents_t1499601201 ** get_address_of__instance_5() { return &____instance_5; }
	inline void set__instance_5(StateMagineEvents_t1499601201 * value)
	{
		____instance_5 = value;
		Il2CppCodeGenWriteBarrier(&____instance_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
