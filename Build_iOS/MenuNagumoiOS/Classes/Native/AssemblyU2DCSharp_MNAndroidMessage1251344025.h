﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_MNPopup1928680331.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MNAndroidMessage
struct  MNAndroidMessage_t1251344025  : public MNPopup_t1928680331
{
public:
	// System.String MNAndroidMessage::ok
	String_t* ___ok_6;

public:
	inline static int32_t get_offset_of_ok_6() { return static_cast<int32_t>(offsetof(MNAndroidMessage_t1251344025, ___ok_6)); }
	inline String_t* get_ok_6() const { return ___ok_6; }
	inline String_t** get_address_of_ok_6() { return &___ok_6; }
	inline void set_ok_6(String_t* value)
	{
		___ok_6 = value;
		Il2CppCodeGenWriteBarrier(&___ok_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
