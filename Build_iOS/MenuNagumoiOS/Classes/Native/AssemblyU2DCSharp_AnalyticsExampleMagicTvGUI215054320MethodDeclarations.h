﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnalyticsExampleMagicTvGUI
struct AnalyticsExampleMagicTvGUI_t215054320;

#include "codegen/il2cpp-codegen.h"

// System.Void AnalyticsExampleMagicTvGUI::.ctor()
extern "C"  void AnalyticsExampleMagicTvGUI__ctor_m3730235051 (AnalyticsExampleMagicTvGUI_t215054320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsExampleMagicTvGUI::Awake()
extern "C"  void AnalyticsExampleMagicTvGUI_Awake_m3967840270 (AnalyticsExampleMagicTvGUI_t215054320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsExampleMagicTvGUI::Start()
extern "C"  void AnalyticsExampleMagicTvGUI_Start_m2677372843 (AnalyticsExampleMagicTvGUI_t215054320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsExampleMagicTvGUI::WindowInAppGUIonClickCamera()
extern "C"  void AnalyticsExampleMagicTvGUI_WindowInAppGUIonClickCamera_m3723772136 (AnalyticsExampleMagicTvGUI_t215054320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsExampleMagicTvGUI::WindowInstructionsOnShow()
extern "C"  void AnalyticsExampleMagicTvGUI_WindowInstructionsOnShow_m3622941834 (AnalyticsExampleMagicTvGUI_t215054320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsExampleMagicTvGUI::WindowHelpOnShow()
extern "C"  void AnalyticsExampleMagicTvGUI_WindowHelpOnShow_m1173703206 (AnalyticsExampleMagicTvGUI_t215054320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsExampleMagicTvGUI::WindowHelpOnClickPrint()
extern "C"  void AnalyticsExampleMagicTvGUI_WindowHelpOnClickPrint_m846156078 (AnalyticsExampleMagicTvGUI_t215054320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsExampleMagicTvGUI::WindowHelpOnClickYouTubeVideo()
extern "C"  void AnalyticsExampleMagicTvGUI_WindowHelpOnClickYouTubeVideo_m2977748281 (AnalyticsExampleMagicTvGUI_t215054320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsExampleMagicTvGUI::WindowAboutOnShow()
extern "C"  void AnalyticsExampleMagicTvGUI_WindowAboutOnShow_m2849252066 (AnalyticsExampleMagicTvGUI_t215054320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsExampleMagicTvGUI::WindowAboutOnClickEmail()
extern "C"  void AnalyticsExampleMagicTvGUI_WindowAboutOnClickEmail_m2214577689 (AnalyticsExampleMagicTvGUI_t215054320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsExampleMagicTvGUI::WindowAboutOnClickFacebook()
extern "C"  void AnalyticsExampleMagicTvGUI_WindowAboutOnClickFacebook_m3521443915 (AnalyticsExampleMagicTvGUI_t215054320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsExampleMagicTvGUI::WindowAboutOnClickSite()
extern "C"  void AnalyticsExampleMagicTvGUI_WindowAboutOnClickSite_m4071342732 (AnalyticsExampleMagicTvGUI_t215054320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsExampleMagicTvGUI::WindowAboutOnClickTwitter()
extern "C"  void AnalyticsExampleMagicTvGUI_WindowAboutOnClickTwitter_m4235828272 (AnalyticsExampleMagicTvGUI_t215054320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsExampleMagicTvGUI::WindowAboutOnClickYouTubeChannel()
extern "C"  void AnalyticsExampleMagicTvGUI_WindowAboutOnClickYouTubeChannel_m2886969413 (AnalyticsExampleMagicTvGUI_t215054320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
