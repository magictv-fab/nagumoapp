﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RunFSM
struct RunFSM_t3316021917;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// UnityEngine.Collider
struct Collider_t2939674232;
// UnityEngine.Collision
struct Collision_t2494107688;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t2416790841;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent2133468028.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"
#include "UnityEngine_UnityEngine_Collision2494107688.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit2416790841.h"

// System.Void HutongGames.PlayMaker.Actions.RunFSM::.ctor()
extern "C"  void RunFSM__ctor_m4009215593 (RunFSM_t3316021917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSM::Reset()
extern "C"  void RunFSM_Reset_m1655648534 (RunFSM_t3316021917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSM::Awake()
extern "C"  void RunFSM_Awake_m4246820812 (RunFSM_t3316021917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.RunFSM::Event(HutongGames.PlayMaker.FsmEvent)
extern "C"  bool RunFSM_Event_m3927129447 (RunFSM_t3316021917 * __this, FsmEvent_t2133468028 * ___fsmEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSM::OnEnter()
extern "C"  void RunFSM_OnEnter_m3957216256 (RunFSM_t3316021917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSM::OnUpdate()
extern "C"  void RunFSM_OnUpdate_m1548179011 (RunFSM_t3316021917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSM::OnFixedUpdate()
extern "C"  void RunFSM_OnFixedUpdate_m927984645 (RunFSM_t3316021917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSM::OnLateUpdate()
extern "C"  void RunFSM_OnLateUpdate_m1387152009 (RunFSM_t3316021917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSM::DoTriggerEnter(UnityEngine.Collider)
extern "C"  void RunFSM_DoTriggerEnter_m3384122715 (RunFSM_t3316021917 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSM::DoTriggerStay(UnityEngine.Collider)
extern "C"  void RunFSM_DoTriggerStay_m4178292706 (RunFSM_t3316021917 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSM::DoTriggerExit(UnityEngine.Collider)
extern "C"  void RunFSM_DoTriggerExit_m1560203495 (RunFSM_t3316021917 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSM::DoCollisionEnter(UnityEngine.Collision)
extern "C"  void RunFSM_DoCollisionEnter_m1309450507 (RunFSM_t3316021917 * __this, Collision_t2494107688 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSM::DoCollisionStay(UnityEngine.Collision)
extern "C"  void RunFSM_DoCollisionStay_m2060302512 (RunFSM_t3316021917 * __this, Collision_t2494107688 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSM::DoCollisionExit(UnityEngine.Collision)
extern "C"  void RunFSM_DoCollisionExit_m2503915595 (RunFSM_t3316021917 * __this, Collision_t2494107688 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSM::DoControllerColliderHit(UnityEngine.ControllerColliderHit)
extern "C"  void RunFSM_DoControllerColliderHit_m4291607975 (RunFSM_t3316021917 * __this, ControllerColliderHit_t2416790841 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSM::OnGUI()
extern "C"  void RunFSM_OnGUI_m3504614243 (RunFSM_t3316021917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSM::OnExit()
extern "C"  void RunFSM_OnExit_m1244950424 (RunFSM_t3316021917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSM::CheckIfFinished()
extern "C"  void RunFSM_CheckIfFinished_m3021569566 (RunFSM_t3316021917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
