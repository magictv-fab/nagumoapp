﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.IEnumerable`1<System.Double>
struct IEnumerable_1_t2874172226;
// System.Collections.Generic.IEnumerable`1<System.Single>
struct IEnumerable_1_t3297864633;

#include "codegen/il2cpp-codegen.h"

// System.Double System.Linq.Enumerable::Average(System.Collections.Generic.IEnumerable`1<System.Double>)
extern "C"  double Enumerable_Average_m1248688079 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single System.Linq.Enumerable::Max(System.Collections.Generic.IEnumerable`1<System.Single>)
extern "C"  float Enumerable_Max_m3316738568 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single System.Linq.Enumerable::<Max>m__F(System.Single,System.Single)
extern "C"  float Enumerable_U3CMaxU3Em__F_m1025678683 (Il2CppObject * __this /* static, unused */, float ___a0, float ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
