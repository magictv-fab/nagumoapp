﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertasManagerCRM_Preco
struct OfertasManagerCRM_Preco_t455772099;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void OfertasManagerCRM_Preco::.ctor()
extern "C"  void OfertasManagerCRM_Preco__ctor_m1488773896 (OfertasManagerCRM_Preco_t455772099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Preco::.cctor()
extern "C"  void OfertasManagerCRM_Preco__cctor_m2720221605 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Preco::Start()
extern "C"  void OfertasManagerCRM_Preco_Start_m435911688 (OfertasManagerCRM_Preco_t455772099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Preco::ShowNumberSelected(System.Int32)
extern "C"  void OfertasManagerCRM_Preco_ShowNumberSelected_m1943659758 (OfertasManagerCRM_Preco_t455772099 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Preco::InstantiateNextItem()
extern "C"  void OfertasManagerCRM_Preco_InstantiateNextItem_m229760278 (OfertasManagerCRM_Preco_t455772099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Preco::SendStatus(System.Int32)
extern "C"  void OfertasManagerCRM_Preco_SendStatus_m503774311 (OfertasManagerCRM_Preco_t455772099 * __this, int32_t ___status0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Preco::BtnAccept()
extern "C"  void OfertasManagerCRM_Preco_BtnAccept_m526681898 (OfertasManagerCRM_Preco_t455772099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Preco::BtnRecuse()
extern "C"  void OfertasManagerCRM_Preco_BtnRecuse_m1879545177 (OfertasManagerCRM_Preco_t455772099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OfertasManagerCRM_Preco::ISendStatus(System.String,System.Int32)
extern "C"  Il2CppObject * OfertasManagerCRM_Preco_ISendStatus_m876446886 (OfertasManagerCRM_Preco_t455772099 * __this, String_t* ___id0, int32_t ___status1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OfertasManagerCRM_Preco::ILoadOfertas()
extern "C"  Il2CppObject * OfertasManagerCRM_Preco_ILoadOfertas_m3148351143 (OfertasManagerCRM_Preco_t455772099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Preco::Update()
extern "C"  void OfertasManagerCRM_Preco_Update_m634212613 (OfertasManagerCRM_Preco_t455772099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Preco::LoadMoreItens()
extern "C"  void OfertasManagerCRM_Preco_LoadMoreItens_m2736036202 (OfertasManagerCRM_Preco_t455772099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OfertasManagerCRM_Preco::GetDateString(System.String)
extern "C"  String_t* OfertasManagerCRM_Preco_GetDateString_m534675812 (Il2CppObject * __this /* static, unused */, String_t* ___date0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OfertasManagerCRM_Preco::GetDateTimeString(System.String)
extern "C"  String_t* OfertasManagerCRM_Preco_GetDateTimeString_m1130174103 (Il2CppObject * __this /* static, unused */, String_t* ___date0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OfertasManagerCRM_Preco::ILoadImgs()
extern "C"  Il2CppObject * OfertasManagerCRM_Preco_ILoadImgs_m4175666893 (OfertasManagerCRM_Preco_t455772099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OfertasManagerCRM_Preco::LoadLoadeds()
extern "C"  Il2CppObject * OfertasManagerCRM_Preco_LoadLoadeds_m3826541558 (OfertasManagerCRM_Preco_t455772099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Preco::PopUp(System.String)
extern "C"  void OfertasManagerCRM_Preco_PopUp_m2829788752 (OfertasManagerCRM_Preco_t455772099 * __this, String_t* ___txt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
