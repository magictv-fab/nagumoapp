﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PublishManager/<ILoadPublishUpdate>c__Iterator78
struct U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PublishManager/<ILoadPublishUpdate>c__Iterator78::.ctor()
extern "C"  void U3CILoadPublishUpdateU3Ec__Iterator78__ctor_m2924236499 (U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PublishManager/<ILoadPublishUpdate>c__Iterator78::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CILoadPublishUpdateU3Ec__Iterator78_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m304832233 (U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PublishManager/<ILoadPublishUpdate>c__Iterator78::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CILoadPublishUpdateU3Ec__Iterator78_System_Collections_IEnumerator_get_Current_m1331844221 (U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PublishManager/<ILoadPublishUpdate>c__Iterator78::MoveNext()
extern "C"  bool U3CILoadPublishUpdateU3Ec__Iterator78_MoveNext_m3373750825 (U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PublishManager/<ILoadPublishUpdate>c__Iterator78::Dispose()
extern "C"  void U3CILoadPublishUpdateU3Ec__Iterator78_Dispose_m1180947024 (U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PublishManager/<ILoadPublishUpdate>c__Iterator78::Reset()
extern "C"  void U3CILoadPublishUpdateU3Ec__Iterator78_Reset_m570669440 (U3CILoadPublishUpdateU3Ec__Iterator78_t2042484936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
