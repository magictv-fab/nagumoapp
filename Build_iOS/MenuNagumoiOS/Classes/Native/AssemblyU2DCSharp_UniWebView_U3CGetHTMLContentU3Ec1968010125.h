﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.String>
struct Action_1_t403047693;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView/<GetHTMLContent>c__AnonStoreyA7
struct  U3CGetHTMLContentU3Ec__AnonStoreyA7_t1968010125  : public Il2CppObject
{
public:
	// System.Action`1<System.String> UniWebView/<GetHTMLContent>c__AnonStoreyA7::handler
	Action_1_t403047693 * ___handler_0;

public:
	inline static int32_t get_offset_of_handler_0() { return static_cast<int32_t>(offsetof(U3CGetHTMLContentU3Ec__AnonStoreyA7_t1968010125, ___handler_0)); }
	inline Action_1_t403047693 * get_handler_0() const { return ___handler_0; }
	inline Action_1_t403047693 ** get_address_of_handler_0() { return &___handler_0; }
	inline void set_handler_0(Action_1_t403047693 * value)
	{
		___handler_0 = value;
		Il2CppCodeGenWriteBarrier(&___handler_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
