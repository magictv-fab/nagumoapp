﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// HutongGames.PlayMaker.Actions.DebugFloat
struct DebugFloat_t914859313;
// HutongGames.PlayMaker.Actions.DebugFsmVariable
struct DebugFsmVariable_t831610673;
// HutongGames.PlayMaker.Actions.DebugGameObject
struct DebugGameObject_t2633884050;
// HutongGames.PlayMaker.Actions.DebugInt
struct DebugInt_t295189828;
// HutongGames.PlayMaker.Actions.DebugLog
struct DebugLog_t295192729;
// HutongGames.PlayMaker.Actions.DebugObject
struct DebugObject_t2190156288;
// HutongGames.PlayMaker.Actions.DebugVector3
struct DebugVector3_t523847589;
// HutongGames.PlayMaker.Actions.DestroyComponent
struct DestroyComponent_t1933763019;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// HutongGames.PlayMaker.Actions.DestroyObject
struct DestroyObject_t317818279;
// HutongGames.PlayMaker.Actions.DestroySelf
struct DestroySelf_t1894685940;
// HutongGames.PlayMaker.Actions.DetachChildren
struct DetachChildren_t237419930;
// HutongGames.PlayMaker.Actions.DeviceOrientationEvent
struct DeviceOrientationEvent_t1490992904;
// HutongGames.PlayMaker.Actions.DevicePlayFullScreenMovie
struct DevicePlayFullScreenMovie_t2639184217;
// HutongGames.PlayMaker.Actions.DeviceShakeEvent
struct DeviceShakeEvent_t2461145554;
// HutongGames.PlayMaker.Actions.DeviceVibrate
struct DeviceVibrate_t2224094215;
// HutongGames.PlayMaker.Actions.DontDestroyOnLoad
struct DontDestroyOnLoad_t722253884;
// HutongGames.PlayMaker.Actions.DrawDebugLine
struct DrawDebugLine_t2835919313;
// HutongGames.PlayMaker.Actions.DrawDebugRay
struct DrawDebugRay_t3299009635;
// HutongGames.PlayMaker.Actions.DrawFullscreenColor
struct DrawFullscreenColor_t161869330;
// HutongGames.PlayMaker.Actions.DrawStateLabel
struct DrawStateLabel_t2977589295;
// HutongGames.PlayMaker.Actions.DrawTexture
struct DrawTexture_t1601360325;
// HutongGames.PlayMaker.Actions.EaseColor
struct EaseColor_t1435706211;
// HutongGames.PlayMaker.Actions.EaseFloat
struct EaseFloat_t1438389852;
// HutongGames.PlayMaker.Actions.EaseFsmAction
struct EaseFsmAction_t595986710;
// HutongGames.PlayMaker.Actions.EaseFsmAction/EasingFunction
struct EasingFunction_t524263911;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;
// HutongGames.PlayMaker.Actions.EaseRect
struct EaseRect_t3254272922;
// HutongGames.PlayMaker.Actions.EaseVector3
struct EaseVector3_t1125521936;
// HutongGames.PlayMaker.Actions.EnableAnimation
struct EnableAnimation_t3592025679;
// UnityEngine.Animation
struct Animation_t1724966010;
// HutongGames.PlayMaker.Actions.EnableBehaviour
struct EnableBehaviour_t2067166088;
// System.String
struct String_t;
// HutongGames.PlayMaker.Actions.EnableFog
struct EnableFog_t3773849481;
// HutongGames.PlayMaker.Actions.EnableFSM
struct EnableFSM_t3773848587;
// PlayMakerFSM[]
struct PlayMakerFSMU5BU5D_t191094001;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// PlayMakerFSM
struct PlayMakerFSM_t3799847376;
// HutongGames.PlayMaker.Actions.EnableGUI
struct EnableGUI_t3773849606;
// HutongGames.PlayMaker.Actions.Explosion
struct Explosion_t444282467;
// UnityEngine.Rigidbody
struct Rigidbody_t3346577219;
// HutongGames.PlayMaker.Actions.FindChild
struct FindChild_t514440753;
// HutongGames.PlayMaker.Actions.FindClosest
struct FindClosest_t1284703470;
// HutongGames.PlayMaker.Actions.FindGameObject
struct FindGameObject_t2514584402;
// HutongGames.PlayMaker.Actions.FinishFSM
struct FinishFSM_t701881083;
// HutongGames.PlayMaker.Actions.Flicker
struct Flicker_t1041969670;
// HutongGames.PlayMaker.Actions.FloatAbs
struct FloatAbs_t1757489438;
// HutongGames.PlayMaker.Actions.FloatAdd
struct FloatAdd_t1757489485;
// HutongGames.PlayMaker.Actions.FloatAddMutiple
struct FloatAddMutiple_t949141333;
// HutongGames.PlayMaker.Actions.FloatChanged
struct FloatChanged_t1905781472;
// HutongGames.PlayMaker.Actions.FloatClamp
struct FloatClamp_t1735441703;
// HutongGames.PlayMaker.Actions.FloatCompare
struct FloatCompare_t2117322001;
// HutongGames.PlayMaker.Actions.FloatDivide
struct FloatDivide_t1636605059;
// HutongGames.PlayMaker.Actions.FloatInterpolate
struct FloatInterpolate_t1095399597;
// HutongGames.PlayMaker.Actions.FloatMultiply
struct FloatMultiply_t1817102958;
// HutongGames.PlayMaker.Actions.FloatOperator
struct FloatOperator_t662719726;
// HutongGames.PlayMaker.Actions.FloatSignTest
struct FloatSignTest_t1474597945;
// HutongGames.PlayMaker.Actions.FloatSubtract
struct FloatSubtract_t3397992286;
// HutongGames.PlayMaker.Actions.FloatSwitch
struct FloatSwitch_t2078594878;
// HutongGames.PlayMaker.Actions.FormatString
struct FormatString_t1206542608;
// HutongGames.PlayMaker.Actions.ForwardAllEvents
struct ForwardAllEvents_t4134244477;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.Actions.ForwardEvent
struct ForwardEvent_t3275232509;
// HutongGames.PlayMaker.Actions.FsmEventOptions
struct FsmEventOptions_t3746547986;
// HutongGames.PlayMaker.Actions.FsmStateSwitch
struct FsmStateSwitch_t3425333421;
// HutongGames.PlayMaker.Actions.FsmStateTest
struct FsmStateTest_t3341384075;
// HutongGames.PlayMaker.Actions.GameObjectChanged
struct GameObjectChanged_t641634289;
// HutongGames.PlayMaker.Actions.GameObjectCompare
struct GameObjectCompare_t853174818;
// HutongGames.PlayMaker.Actions.GameObjectCompareTag
struct GameObjectCompareTag_t3437146702;
// HutongGames.PlayMaker.Actions.GameObjectHasChildren
struct GameObjectHasChildren_t3474747126;
// HutongGames.PlayMaker.Actions.GameObjectIsChildOf
struct GameObjectIsChildOf_t1849565254;
// HutongGames.PlayMaker.Actions.GameObjectIsNull
struct GameObjectIsNull_t2162669226;
// HutongGames.PlayMaker.Actions.GameObjectIsVisible
struct GameObjectIsVisible_t1570039973;
// HutongGames.PlayMaker.Actions.GameObjectTagSwitch
struct GameObjectTagSwitch_t3433592875;
// HutongGames.PlayMaker.Actions.GetAngleToTarget
struct GetAngleToTarget_t1034773969;
// HutongGames.PlayMaker.Actions.GetAnimatorApplyRootMotion
struct GetAnimatorApplyRootMotion_t212715153;
// UnityEngine.Animator
struct Animator_t2776330603;
// HutongGames.PlayMaker.Actions.GetAnimatorBody
struct GetAnimatorBody_t3376544941;
// PlayMakerAnimatorMoveProxy
struct PlayMakerAnimatorMoveProxy_t4175490694;
// HutongGames.PlayMaker.Actions.GetAnimatorBoneGameObject
struct GetAnimatorBoneGameObject_t14629568;
// HutongGames.PlayMaker.Actions.GetAnimatorBool
struct GetAnimatorBool_t3376545269;
// HutongGames.PlayMaker.Actions.GetAnimatorCullingMode
struct GetAnimatorCullingMode_t994674750;
// HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo
struct GetAnimatorCurrentStateInfo_t2823805553;
// HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsName
struct GetAnimatorCurrentStateInfoIsName_t2852409574;
// HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag
struct GetAnimatorCurrentStateInfoIsTag_t2329710485;
// HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo
struct GetAnimatorCurrentTransitionInfo_t114989767;
// HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName
struct GetAnimatorCurrentTransitionInfoIsName_t3939956924;
// HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsUserName
struct GetAnimatorCurrentTransitionInfoIsUserName_t1580140583;
// HutongGames.PlayMaker.Actions.GetAnimatorDelta
struct GetAnimatorDelta_t946274563;
// HutongGames.PlayMaker.Actions.GetAnimatorFeetPivotActive
struct GetAnimatorFeetPivotActive_t705651877;
// HutongGames.PlayMaker.Actions.GetAnimatorFloat
struct GetAnimatorFloat_t948332455;
// HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight
struct GetAnimatorGravityWeight_t3107738545;
// HutongGames.PlayMaker.Actions.GetAnimatorHumanScale
struct GetAnimatorHumanScale_t3915188008;
// HutongGames.PlayMaker.Actions.GetAnimatorIKGoal
struct GetAnimatorIKGoal_t3033774656;
// HutongGames.PlayMaker.Actions.GetAnimatorInt
struct GetAnimatorInt_t3593545018;
// HutongGames.PlayMaker.Actions.GetAnimatorIsControlled
struct GetAnimatorIsControlled_t34751331;
// HutongGames.PlayMaker.Actions.GetAnimatorIsHuman
struct GetAnimatorIsHuman_t55053166;
// HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition
struct GetAnimatorIsLayerInTransition_t3666075020;
// HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget
struct GetAnimatorIsMatchingTarget_t2980401859;
// HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve
struct GetAnimatorIsParameterControlledByCurve_t1394563734;
// HutongGames.PlayMaker.Actions.GetAnimatorLayerCount
struct GetAnimatorLayerCount_t3712536905;
// HutongGames.PlayMaker.Actions.GetAnimatorLayerName
struct GetAnimatorLayerName_t4020332743;
// HutongGames.PlayMaker.Actions.GetAnimatorLayersAffectMassCenter
struct GetAnimatorLayersAffectMassCenter_t2244244939;
// HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight
struct GetAnimatorLayerWeight_t3333517428;
// HutongGames.PlayMaker.Actions.GetAnimatorLeftFootBottomHeight
struct GetAnimatorLeftFootBottomHeight_t4090212338;
// HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo
struct GetAnimatorNextStateInfo_t3502000247;
// HutongGames.PlayMaker.Actions.GetAnimatorPivot
struct GetAnimatorPivot_t957485453;
// HutongGames.PlayMaker.Actions.GetAnimatorPlayBackSpeed
struct GetAnimatorPlayBackSpeed_t331618327;
// HutongGames.PlayMaker.Actions.GetAnimatorPlayBackTime
struct GetAnimatorPlayBackTime_t2941148851;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Deb914859313.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Deb914859313MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_LogLevel284580066.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmFloat2134102846.h"
#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3211770239MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmFloat2134102846MethodDeclarations.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionHelpers1898294297MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Boolean476798718.h"
#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3211770239.h"
#include "mscorlib_System_Single4291918972.h"
#include "mscorlib_System_Object4170816371.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm1527112426.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Deb831610673.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Deb831610673MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVar1596150537.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVar1596150537MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_De2633884050.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_De2633884050MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmGameObject1697147867.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Deb295189828.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Deb295189828MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmInt1596138449.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmInt1596138449MethodDeclarations.h"
#include "mscorlib_System_Int321153838500.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Deb295192729.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Deb295192729MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmString952858651MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmString952858651.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_De2190156288.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_De2190156288MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmObject821476169.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Deb523847589.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Deb523847589MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector3533912882.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector3533912882MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_De1933763019.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_De1933763019MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3501516275.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmOwnerDefault251897112.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmOwnerDefault251897112MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmGameObject1697147867MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_OwnerDefaultOption1934292325.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_GameObject3674682005MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3071478659MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Des317818279.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Des317818279MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmBool1075959796MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform1659122786MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmBool1075959796.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_De1894685940.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_De1894685940MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Det237419930.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Det237419930MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm1527112426MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_De1490992904.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_De1490992904MethodDeclarations.h"
#include "UnityEngine_UnityEngine_DeviceOrientation1141857680.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent2133468028.h"
#include "UnityEngine_UnityEngine_Input4200062272MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_De2639184217.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_De2639184217MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color4194546905MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmColor2131419205MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmColor2131419205.h"
#include "UnityEngine_UnityEngine_FullScreenMovieControlMode3302654991.h"
#include "UnityEngine_UnityEngine_FullScreenMovieScalingMode4213044537.h"
#include "UnityEngine_UnityEngine_Handheld3573483176MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_De2461145554.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_De2461145554MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_De2224094215.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_De2224094215MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Don722253884.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Don722253884MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3501516275MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Dr2835919313.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Dr2835919313MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug4195163081MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Dr3299009635.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Dr3299009635MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Dra161869330.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Dra161869330MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI3134605553MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen3187157168MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Rect4241904616MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Dr2977589295.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Dr2977589295MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Dr1601360325.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Dr1601360325MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTexture3073272573.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmRect1076426478.h"
#include "UnityEngine_UnityEngine_ScaleMode3023293187.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTexture3073272573MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmRect1076426478MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ea1435706211.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ea1435706211MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Eas595986710MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Eas595986710.h"
#include "mscorlib_ArrayTypes.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ea1438389852.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ea1438389852MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Eas397396748.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Eas524263911.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTime1076490199MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time4241968337MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Eas524263911MethodDeclarations.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "UnityEngine_UnityEngine_Mathf4203372500MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Eas397396748MethodDeclarations.h"
#include "mscorlib_System_AsyncCallback1369114871.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ea3254272922.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ea3254272922MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ea1125521936.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ea1125521936MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_En3592025679.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_En3592025679MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animation1724966010MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TrackedReference2089686725MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationState3682323633MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animation1724966010.h"
#include "UnityEngine_UnityEngine_AnimationState3682323633.h"
#include "UnityEngine_UnityEngine_TrackedReference2089686725.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_En2067166088.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_En2067166088MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour200106419MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour200106419.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_En3773849481.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_En3773849481MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderSettings425127197MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_En3773848587.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_En3773848587MethodDeclarations.h"
#include "PlayMaker_PlayMakerFSM3799847376MethodDeclarations.h"
#include "PlayMaker_ArrayTypes.h"
#include "PlayMaker_PlayMakerFSM3799847376.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_En3773849606.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_En3773849606MethodDeclarations.h"
#include "PlayMaker_PlayMakerGUI3799848395MethodDeclarations.h"
#include "PlayMaker_PlayMakerGUI3799848395.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Exp444282467.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Exp444282467MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ForceMode2134283300.h"
#include "UnityEngine_UnityEngine_Physics3358180733MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody3346577219MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"
#include "UnityEngine_UnityEngine_Rigidbody3346577219.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fin514440753.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fin514440753MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fi1284703470.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fi1284703470MethodDeclarations.h"
#include "mscorlib_System_Type2863145774MethodDeclarations.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_RuntimeTypeHandle2669177232.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fi2514584402.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fi2514584402MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fin701881083.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fin701881083MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1041969670.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1041969670MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co2052155886MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random3156561159MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer3076687687MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer3076687687.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1757489438.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1757489438MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1757489485.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1757489485MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Flo949141333.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Flo949141333MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1905781472.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1905781472MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1735441703.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1735441703MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl2117322001.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl2117322001MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent2133468028MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1636605059.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1636605059MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1095399597.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1095399597MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_InterpolationType930981288.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1817102958.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1817102958MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Flo662719726.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Flo662719726MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1085110523.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1085110523MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1474597945.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1474597945MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl3397992286.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl3397992286MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl2078594878.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl2078594878MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fo1206542608.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fo1206542608MethodDeclarations.h"
#include "mscorlib_System_FormatException3455918062.h"
#include "mscorlib_System_Exception3991598821MethodDeclarations.h"
#include "mscorlib_System_Exception3991598821.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fo4134244477.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fo4134244477MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventTarget1823904941MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventTarget1823904941.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventTarget_Eve3998278035.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fo3275232509.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fo3275232509MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs3746547986.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs3746547986MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs3425333421.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs3425333421MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs3341384075.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs3341384075MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Gam641634289.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Gam641634289MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Gam853174818.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Gam853174818MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ga3437146702.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ga3437146702MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ga3474747126.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ga3474747126MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ga1849565254.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ga1849565254MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ga2162669226.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ga2162669226MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ga1570039973.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ga1570039973MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ga3433592875.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ga3433592875MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1034773969.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1034773969MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get212715153.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get212715153MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animator2776330603.h"
#include "UnityEngine_UnityEngine_Animator2776330603MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3376544941.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3376544941MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmQuaternion3871136040.h"
#include "System_Core_System_Action3771233898MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayMakerAnimatorMoveProxy4175490694MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayMakerAnimatorMoveProxy4175490694.h"
#include "System_Core_System_Action3771233898.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmQuaternion3871136040MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GetA14629568.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GetA14629568MethodDeclarations.h"
#include "UnityEngine_UnityEngine_HumanBodyBones1606609988.h"
#include "mscorlib_System_Enum2862688501MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3376545269.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3376545269MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get994674750.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get994674750MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorCullingMode3217251138.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2823805553.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2823805553MethodDeclarations.h"
#include "mscorlib_System_Math2862914300MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo323110318.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo323110318MethodDeclarations.h"
#include "mscorlib_System_Double3868226565.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2852409574.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2852409574MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2329710485.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2329710485MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get114989767.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get114989767MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo2817229998.h"
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo2817229998MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3939956924.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3939956924MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1580140583.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1580140583MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get946274563.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get946274563MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get705651877.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get705651877MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get948332455.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get948332455MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3107738545.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3107738545MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3915188008.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3915188008MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3033774656.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3033774656MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AvatarIKGoal2036631794.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3593545018.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3593545018MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GetA34751331.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GetA34751331MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GetA55053166.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GetA55053166MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3666075020.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3666075020MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2980401859.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2980401859MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1394563734.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1394563734MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3712536905.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3712536905MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge4020332743.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge4020332743MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2244244939.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2244244939MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3333517428.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3333517428MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge4090212338.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge4090212338MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3502000247.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3502000247MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get957485453.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get957485453MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get331618327.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get331618327MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2941148851.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2941148851MethodDeclarations.h"

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2925956997_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2925956997(__this, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2925956997_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animation>()
#define GameObject_GetComponent_TisAnimation_t1724966010_m2530801684(__this, method) ((  Animation_t1724966010 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2925956997_gshared)(__this, method)
// !!0[] UnityEngine.GameObject::GetComponents<System.Object>()
extern "C"  ObjectU5BU5D_t1108656482* GameObject_GetComponents_TisIl2CppObject_m1118944320_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_GetComponents_TisIl2CppObject_m1118944320(__this, method) ((  ObjectU5BU5D_t1108656482* (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponents_TisIl2CppObject_m1118944320_gshared)(__this, method)
// !!0[] UnityEngine.GameObject::GetComponents<PlayMakerFSM>()
#define GameObject_GetComponents_TisPlayMakerFSM_t3799847376_m3603576560(__this, method) ((  PlayMakerFSMU5BU5D_t191094001* (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponents_TisIl2CppObject_m1118944320_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<PlayMakerFSM>()
#define GameObject_GetComponent_TisPlayMakerFSM_t3799847376_m3107677963(__this, method) ((  PlayMakerFSM_t3799847376 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2925956997_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Rigidbody>()
#define GameObject_GetComponent_TisRigidbody_t3346577219_m1334743405(__this, method) ((  Rigidbody_t3346577219 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2925956997_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animator>()
#define GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(__this, method) ((  Animator_t2776330603 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2925956997_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<PlayMakerAnimatorMoveProxy>()
#define GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623(__this, method) ((  PlayMakerAnimatorMoveProxy_t4175490694 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2925956997_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void HutongGames.PlayMaker.Actions.DebugFloat::.ctor()
extern "C"  void DebugFloat__ctor_m3412883029 (DebugFloat_t914859313 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DebugFloat::Reset()
extern "C"  void DebugFloat_Reset_m1059315970 (DebugFloat_t914859313 * __this, const MethodInfo* method)
{
	{
		__this->set_logLevel_9(0);
		__this->set_floatVariable_10((FsmFloat_t2134102846 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DebugFloat::OnEnter()
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2433880;
extern Il2CppCodeGenString* _stringLiteral1830;
extern const uint32_t DebugFloat_OnEnter_m2112272620_MetadataUsageId;
extern "C"  void DebugFloat_OnEnter_m2112272620 (DebugFloat_t914859313 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DebugFloat_OnEnter_m2112272620_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		V_0 = _stringLiteral2433880;
		FsmFloat_t2134102846 * L_0 = __this->get_floatVariable_10();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_003c;
		}
	}
	{
		FsmFloat_t2134102846 * L_2 = __this->get_floatVariable_10();
		NullCheck(L_2);
		String_t* L_3 = NamedVariable_get_Name_m3214577877(L_2, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_4 = __this->get_floatVariable_10();
		NullCheck(L_4);
		float L_5 = FsmFloat_get_Value_m4137923823(L_4, /*hidden argument*/NULL);
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m2809334143(NULL /*static, unused*/, L_3, _stringLiteral1830, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_003c:
	{
		Fsm_t1527112426 * L_9 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		int32_t L_10 = __this->get_logLevel_9();
		String_t* L_11 = V_0;
		ActionHelpers_DebugLog_m3557515559(NULL /*static, unused*/, L_9, L_10, L_11, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DebugFsmVariable::.ctor()
extern "C"  void DebugFsmVariable__ctor_m479267413 (DebugFsmVariable_t831610673 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DebugFsmVariable::Reset()
extern "C"  void DebugFsmVariable_Reset_m2420667650 (DebugFsmVariable_t831610673 * __this, const MethodInfo* method)
{
	{
		__this->set_logLevel_9(0);
		__this->set_fsmVar_10((FsmVar_t1596150537 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DebugFsmVariable::OnEnter()
extern "C"  void DebugFsmVariable_OnEnter_m406211820 (DebugFsmVariable_t831610673 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_logLevel_9();
		FsmVar_t1596150537 * L_2 = __this->get_fsmVar_10();
		NullCheck(L_2);
		String_t* L_3 = FsmVar_DebugString_m3885228041(L_2, /*hidden argument*/NULL);
		ActionHelpers_DebugLog_m3557515559(NULL /*static, unused*/, L_0, L_1, L_3, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DebugGameObject::.ctor()
extern "C"  void DebugGameObject__ctor_m2391276004 (DebugGameObject_t2633884050 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DebugGameObject::Reset()
extern "C"  void DebugGameObject_Reset_m37708945 (DebugGameObject_t2633884050 * __this, const MethodInfo* method)
{
	{
		__this->set_logLevel_9(0);
		__this->set_gameObject_10((FsmGameObject_t1697147867 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DebugGameObject::OnEnter()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2433880;
extern Il2CppCodeGenString* _stringLiteral1830;
extern const uint32_t DebugGameObject_OnEnter_m3895432379_MetadataUsageId;
extern "C"  void DebugGameObject_OnEnter_m3895432379 (DebugGameObject_t2633884050 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DebugGameObject_OnEnter_m3895432379_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		V_0 = _stringLiteral2433880;
		FsmGameObject_t1697147867 * L_0 = __this->get_gameObject_10();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0032;
		}
	}
	{
		FsmGameObject_t1697147867 * L_2 = __this->get_gameObject_10();
		NullCheck(L_2);
		String_t* L_3 = NamedVariable_get_Name_m3214577877(L_2, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_4 = __this->get_gameObject_10();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m2809334143(NULL /*static, unused*/, L_3, _stringLiteral1830, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
	}

IL_0032:
	{
		Fsm_t1527112426 * L_6 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		int32_t L_7 = __this->get_logLevel_9();
		String_t* L_8 = V_0;
		ActionHelpers_DebugLog_m3557515559(NULL /*static, unused*/, L_6, L_7, L_8, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DebugInt::.ctor()
extern "C"  void DebugInt__ctor_m2930305442 (DebugInt_t295189828 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DebugInt::Reset()
extern "C"  void DebugInt_Reset_m576738383 (DebugInt_t295189828 * __this, const MethodInfo* method)
{
	{
		__this->set_logLevel_9(0);
		__this->set_intVariable_10((FsmInt_t1596138449 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DebugInt::OnEnter()
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2433880;
extern Il2CppCodeGenString* _stringLiteral1830;
extern const uint32_t DebugInt_OnEnter_m2211679481_MetadataUsageId;
extern "C"  void DebugInt_OnEnter_m2211679481 (DebugInt_t295189828 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DebugInt_OnEnter_m2211679481_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		V_0 = _stringLiteral2433880;
		FsmInt_t1596138449 * L_0 = __this->get_intVariable_10();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_003c;
		}
	}
	{
		FsmInt_t1596138449 * L_2 = __this->get_intVariable_10();
		NullCheck(L_2);
		String_t* L_3 = NamedVariable_get_Name_m3214577877(L_2, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_4 = __this->get_intVariable_10();
		NullCheck(L_4);
		int32_t L_5 = FsmInt_get_Value_m27059446(L_4, /*hidden argument*/NULL);
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m2809334143(NULL /*static, unused*/, L_3, _stringLiteral1830, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_003c:
	{
		Fsm_t1527112426 * L_9 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		int32_t L_10 = __this->get_logLevel_9();
		String_t* L_11 = V_0;
		ActionHelpers_DebugLog_m3557515559(NULL /*static, unused*/, L_9, L_10, L_11, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DebugLog::.ctor()
extern "C"  void DebugLog__ctor_m4075277805 (DebugLog_t295192729 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DebugLog::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t DebugLog_Reset_m1721710746_MetadataUsageId;
extern "C"  void DebugLog_Reset_m1721710746 (DebugLog_t295192729 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DebugLog_Reset_m1721710746_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_logLevel_9(0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_text_10(L_1);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DebugLog::OnEnter()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t DebugLog_OnEnter_m3018492548_MetadataUsageId;
extern "C"  void DebugLog_OnEnter_m3018492548 (DebugLog_t295192729 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DebugLog_OnEnter_m3018492548_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmString_t952858651 * L_0 = __this->get_text_10();
		NullCheck(L_0);
		String_t* L_1 = FsmString_get_Value_m872383149(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		Fsm_t1527112426 * L_3 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_logLevel_9();
		FsmString_t952858651 * L_5 = __this->get_text_10();
		NullCheck(L_5);
		String_t* L_6 = FsmString_get_Value_m872383149(L_5, /*hidden argument*/NULL);
		ActionHelpers_DebugLog_m3557515559(NULL /*static, unused*/, L_3, L_4, L_6, /*hidden argument*/NULL);
	}

IL_0031:
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DebugObject::.ctor()
extern "C"  void DebugObject__ctor_m2986477878 (DebugObject_t2190156288 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DebugObject::Reset()
extern "C"  void DebugObject_Reset_m632910819 (DebugObject_t2190156288 * __this, const MethodInfo* method)
{
	{
		__this->set_logLevel_9(0);
		__this->set_fsmObject_10((FsmObject_t821476169 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DebugObject::OnEnter()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2433880;
extern Il2CppCodeGenString* _stringLiteral1830;
extern const uint32_t DebugObject_OnEnter_m358815629_MetadataUsageId;
extern "C"  void DebugObject_OnEnter_m358815629 (DebugObject_t2190156288 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DebugObject_OnEnter_m358815629_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		V_0 = _stringLiteral2433880;
		FsmObject_t821476169 * L_0 = __this->get_fsmObject_10();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0032;
		}
	}
	{
		FsmObject_t821476169 * L_2 = __this->get_fsmObject_10();
		NullCheck(L_2);
		String_t* L_3 = NamedVariable_get_Name_m3214577877(L_2, /*hidden argument*/NULL);
		FsmObject_t821476169 * L_4 = __this->get_fsmObject_10();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m2809334143(NULL /*static, unused*/, L_3, _stringLiteral1830, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
	}

IL_0032:
	{
		Fsm_t1527112426 * L_6 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		int32_t L_7 = __this->get_logLevel_9();
		String_t* L_8 = V_0;
		ActionHelpers_DebugLog_m3557515559(NULL /*static, unused*/, L_6, L_7, L_8, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DebugVector3::.ctor()
extern "C"  void DebugVector3__ctor_m3860541537 (DebugVector3_t523847589 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DebugVector3::Reset()
extern "C"  void DebugVector3_Reset_m1506974478 (DebugVector3_t523847589 * __this, const MethodInfo* method)
{
	{
		__this->set_logLevel_9(0);
		__this->set_vector3Variable_10((FsmVector3_t533912882 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DebugVector3::OnEnter()
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2433880;
extern Il2CppCodeGenString* _stringLiteral1830;
extern const uint32_t DebugVector3_OnEnter_m2815369208_MetadataUsageId;
extern "C"  void DebugVector3_OnEnter_m2815369208 (DebugVector3_t523847589 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DebugVector3_OnEnter_m2815369208_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		V_0 = _stringLiteral2433880;
		FsmVector3_t533912882 * L_0 = __this->get_vector3Variable_10();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_003c;
		}
	}
	{
		FsmVector3_t533912882 * L_2 = __this->get_vector3Variable_10();
		NullCheck(L_2);
		String_t* L_3 = NamedVariable_get_Name_m3214577877(L_2, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_4 = __this->get_vector3Variable_10();
		NullCheck(L_4);
		Vector3_t4282066566  L_5 = FsmVector3_get_Value_m2779135117(L_4, /*hidden argument*/NULL);
		Vector3_t4282066566  L_6 = L_5;
		Il2CppObject * L_7 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m2809334143(NULL /*static, unused*/, L_3, _stringLiteral1830, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_003c:
	{
		Fsm_t1527112426 * L_9 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		int32_t L_10 = __this->get_logLevel_9();
		String_t* L_11 = V_0;
		ActionHelpers_DebugLog_m3557515559(NULL /*static, unused*/, L_9, L_10, L_11, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DestroyComponent::.ctor()
extern "C"  void DestroyComponent__ctor_m1906300155 (DestroyComponent_t1933763019 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DestroyComponent::Reset()
extern "C"  void DestroyComponent_Reset_m3847700392 (DestroyComponent_t1933763019 * __this, const MethodInfo* method)
{
	{
		__this->set_aComponent_11((Component_t3501516275 *)NULL);
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_component_10((FsmString_t952858651 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DestroyComponent::OnEnter()
extern "C"  void DestroyComponent_OnEnter_m1690109458 (DestroyComponent_t1933763019 * __this, const MethodInfo* method)
{
	DestroyComponent_t1933763019 * G_B2_0 = NULL;
	DestroyComponent_t1933763019 * G_B1_0 = NULL;
	GameObject_t3674682005 * G_B3_0 = NULL;
	DestroyComponent_t1933763019 * G_B3_1 = NULL;
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_9();
		NullCheck(L_0);
		int32_t L_1 = FsmOwnerDefault_get_OwnerOption_m3357910390(L_0, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if (L_1)
		{
			G_B2_0 = __this;
			goto IL_001c;
		}
	}
	{
		GameObject_t3674682005 * L_2 = FsmStateAction_get_Owner_m1855633209(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_002c;
	}

IL_001c:
	{
		FsmOwnerDefault_t251897112 * L_3 = __this->get_gameObject_9();
		NullCheck(L_3);
		FsmGameObject_t1697147867 * L_4 = FsmOwnerDefault_get_GameObject_m3249227945(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_t3674682005 * L_5 = FsmGameObject_get_Value_m673294275(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
	}

IL_002c:
	{
		NullCheck(G_B3_1);
		DestroyComponent_DoDestroyComponent_m4082829423(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DestroyComponent::DoDestroyComponent(UnityEngine.GameObject)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1503981801;
extern const uint32_t DestroyComponent_DoDestroyComponent_m4082829423_MetadataUsageId;
extern "C"  void DestroyComponent_DoDestroyComponent_m4082829423 (DestroyComponent_t1933763019 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DestroyComponent_DoDestroyComponent_m4082829423_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = ___go0;
		FsmString_t952858651 * L_1 = __this->get_component_10();
		NullCheck(L_1);
		String_t* L_2 = FsmString_get_Value_m872383149(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t3501516275 * L_3 = GameObject_GetComponent_m2525409030(L_0, L_2, /*hidden argument*/NULL);
		__this->set_aComponent_11(L_3);
		Component_t3501516275 * L_4 = __this->get_aComponent_11();
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0048;
		}
	}
	{
		FsmString_t952858651 * L_6 = __this->get_component_10();
		NullCheck(L_6);
		String_t* L_7 = FsmString_get_Value_m872383149(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1503981801, L_7, /*hidden argument*/NULL);
		FsmStateAction_LogError_m3478223492(__this, L_8, /*hidden argument*/NULL);
		goto IL_0053;
	}

IL_0048:
	{
		Component_t3501516275 * L_9 = __this->get_aComponent_11();
		Object_Destroy_m176400816(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
	}

IL_0053:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DestroyObject::.ctor()
extern "C"  void DestroyObject__ctor_m2180790127 (DestroyObject_t317818279 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DestroyObject::Reset()
extern "C"  void DestroyObject_Reset_m4122190364 (DestroyObject_t317818279 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmGameObject_t1697147867 *)NULL);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_delay_10(L_0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DestroyObject::OnEnter()
extern "C"  void DestroyObject_OnEnter_m3481967494 (DestroyObject_t317818279 * __this, const MethodInfo* method)
{
	GameObject_t3674682005 * V_0 = NULL;
	{
		FsmGameObject_t1697147867 * L_0 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_1 = FsmGameObject_get_Value_m673294275(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t3674682005 * L_2 = V_0;
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0064;
		}
	}
	{
		FsmFloat_t2134102846 * L_4 = __this->get_delay_10();
		NullCheck(L_4);
		float L_5 = FsmFloat_get_Value_m4137923823(L_4, /*hidden argument*/NULL);
		if ((!(((float)L_5) <= ((float)(0.0f)))))
		{
			goto IL_0038;
		}
	}
	{
		GameObject_t3674682005 * L_6 = V_0;
		Object_Destroy_m176400816(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		goto IL_0049;
	}

IL_0038:
	{
		GameObject_t3674682005 * L_7 = V_0;
		FsmFloat_t2134102846 * L_8 = __this->get_delay_10();
		NullCheck(L_8);
		float L_9 = FsmFloat_get_Value_m4137923823(L_8, /*hidden argument*/NULL);
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_7, L_9, /*hidden argument*/NULL);
	}

IL_0049:
	{
		FsmBool_t1075959796 * L_10 = __this->get_detachChildren_11();
		NullCheck(L_10);
		bool L_11 = FsmBool_get_Value_m3101329097(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0064;
		}
	}
	{
		GameObject_t3674682005 * L_12 = V_0;
		NullCheck(L_12);
		Transform_t1659122786 * L_13 = GameObject_get_transform_m1278640159(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_DetachChildren_m1928114025(L_13, /*hidden argument*/NULL);
	}

IL_0064:
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DestroyObject::OnUpdate()
extern "C"  void DestroyObject_OnUpdate_m3995336573 (DestroyObject_t317818279 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DestroySelf::.ctor()
extern "C"  void DestroySelf__ctor_m417833154 (DestroySelf_t1894685940 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DestroySelf::Reset()
extern "C"  void DestroySelf_Reset_m2359233391 (DestroySelf_t1894685940 * __this, const MethodInfo* method)
{
	{
		FsmBool_t1075959796 * L_0 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_detachChildren_9(L_0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DestroySelf::OnEnter()
extern "C"  void DestroySelf_OnEnter_m1497431065 (DestroySelf_t1894685940 * __this, const MethodInfo* method)
{
	{
		GameObject_t3674682005 * L_0 = FsmStateAction_get_Owner_m1855633209(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003c;
		}
	}
	{
		FsmBool_t1075959796 * L_2 = __this->get_detachChildren_9();
		NullCheck(L_2);
		bool L_3 = FsmBool_get_Value_m3101329097(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0031;
		}
	}
	{
		GameObject_t3674682005 * L_4 = FsmStateAction_get_Owner_m1855633209(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t1659122786 * L_5 = GameObject_get_transform_m1278640159(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_DetachChildren_m1928114025(L_5, /*hidden argument*/NULL);
	}

IL_0031:
	{
		GameObject_t3674682005 * L_6 = FsmStateAction_get_Owner_m1855633209(__this, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
	}

IL_003c:
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DetachChildren::.ctor()
extern "C"  void DetachChildren__ctor_m2698967052 (DetachChildren_t237419930 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DetachChildren::Reset()
extern "C"  void DetachChildren_Reset_m345399993 (DetachChildren_t237419930 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DetachChildren::OnEnter()
extern "C"  void DetachChildren_OnEnter_m3233786083 (DetachChildren_t237419930 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		DetachChildren_DoDetachChildren_m2713073613(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DetachChildren::DoDetachChildren(UnityEngine.GameObject)
extern "C"  void DetachChildren_DoDetachChildren_m2713073613 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method)
{
	{
		GameObject_t3674682005 * L_0 = ___go0;
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		GameObject_t3674682005 * L_2 = ___go0;
		NullCheck(L_2);
		Transform_t1659122786 * L_3 = GameObject_get_transform_m1278640159(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_DetachChildren_m1928114025(L_3, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DeviceOrientationEvent::.ctor()
extern "C"  void DeviceOrientationEvent__ctor_m3655801438 (DeviceOrientationEvent_t1490992904 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DeviceOrientationEvent::Reset()
extern "C"  void DeviceOrientationEvent_Reset_m1302234379 (DeviceOrientationEvent_t1490992904 * __this, const MethodInfo* method)
{
	{
		__this->set_orientation_9(1);
		__this->set_sendEvent_10((FsmEvent_t2133468028 *)NULL);
		__this->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DeviceOrientationEvent::OnEnter()
extern "C"  void DeviceOrientationEvent_OnEnter_m3628629685 (DeviceOrientationEvent_t1490992904 * __this, const MethodInfo* method)
{
	{
		DeviceOrientationEvent_DoDetectDeviceOrientation_m3036551464(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_11();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DeviceOrientationEvent::OnUpdate()
extern "C"  void DeviceOrientationEvent_OnUpdate_m4246897198 (DeviceOrientationEvent_t1490992904 * __this, const MethodInfo* method)
{
	{
		DeviceOrientationEvent_DoDetectDeviceOrientation_m3036551464(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DeviceOrientationEvent::DoDetectDeviceOrientation()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t DeviceOrientationEvent_DoDetectDeviceOrientation_m3036551464_MetadataUsageId;
extern "C"  void DeviceOrientationEvent_DoDetectDeviceOrientation_m3036551464 (DeviceOrientationEvent_t1490992904 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeviceOrientationEvent_DoDetectDeviceOrientation_m3036551464_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		int32_t L_0 = Input_get_deviceOrientation_m4123423598(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_orientation_9();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0021;
		}
	}
	{
		Fsm_t1527112426 * L_2 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_3 = __this->get_sendEvent_10();
		NullCheck(L_2);
		Fsm_Event_m625948263(L_2, L_3, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DevicePlayFullScreenMovie::.ctor()
extern "C"  void DevicePlayFullScreenMovie__ctor_m2528438269 (DevicePlayFullScreenMovie_t2639184217 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DevicePlayFullScreenMovie::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t DevicePlayFullScreenMovie_Reset_m174871210_MetadataUsageId;
extern "C"  void DevicePlayFullScreenMovie_Reset_m174871210 (DevicePlayFullScreenMovie_t2639184217 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DevicePlayFullScreenMovie_Reset_m174871210_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_moviePath_9(L_1);
		Color_t4194546905  L_2 = Color_get_black_m1687201969(NULL /*static, unused*/, /*hidden argument*/NULL);
		FsmColor_t2131419205 * L_3 = FsmColor_op_Implicit_m2192961033(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_fadeColor_10(L_3);
		__this->set_movieControlMode_11(0);
		__this->set_movieScalingMode_12(1);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DevicePlayFullScreenMovie::OnEnter()
extern "C"  void DevicePlayFullScreenMovie_OnEnter_m2564382868 (DevicePlayFullScreenMovie_t2639184217 * __this, const MethodInfo* method)
{
	{
		FsmString_t952858651 * L_0 = __this->get_moviePath_9();
		NullCheck(L_0);
		String_t* L_1 = FsmString_get_Value_m872383149(L_0, /*hidden argument*/NULL);
		FsmColor_t2131419205 * L_2 = __this->get_fadeColor_10();
		NullCheck(L_2);
		Color_t4194546905  L_3 = FsmColor_get_Value_m1679829997(L_2, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_movieControlMode_11();
		int32_t L_5 = __this->get_movieScalingMode_12();
		Handheld_PlayFullScreenMovie_m1459407990(NULL /*static, unused*/, L_1, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DeviceShakeEvent::.ctor()
extern "C"  void DeviceShakeEvent__ctor_m4258378964 (DeviceShakeEvent_t2461145554 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DeviceShakeEvent::Reset()
extern "C"  void DeviceShakeEvent_Reset_m1904811905 (DeviceShakeEvent_t2461145554 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (3.0f), /*hidden argument*/NULL);
		__this->set_shakeThreshold_9(L_0);
		__this->set_sendEvent_10((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DeviceShakeEvent::OnUpdate()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t DeviceShakeEvent_OnUpdate_m2670676984_MetadataUsageId;
extern "C"  void DeviceShakeEvent_OnUpdate_m2670676984 (DeviceShakeEvent_t2461145554 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeviceShakeEvent_OnUpdate_m2670676984_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		Vector3_t4282066566  L_0 = Input_get_acceleration_m3697865796(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = Vector3_get_sqrMagnitude_m1207423764((&V_0), /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_2 = __this->get_shakeThreshold_9();
		NullCheck(L_2);
		float L_3 = FsmFloat_get_Value_m4137923823(L_2, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_4 = __this->get_shakeThreshold_9();
		NullCheck(L_4);
		float L_5 = FsmFloat_get_Value_m4137923823(L_4, /*hidden argument*/NULL);
		if ((!(((float)L_1) > ((float)((float)((float)L_3*(float)L_5))))))
		{
			goto IL_003a;
		}
	}
	{
		Fsm_t1527112426 * L_6 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_7 = __this->get_sendEvent_10();
		NullCheck(L_6);
		Fsm_Event_m625948263(L_6, L_7, /*hidden argument*/NULL);
	}

IL_003a:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DeviceVibrate::.ctor()
extern "C"  void DeviceVibrate__ctor_m3907775759 (DeviceVibrate_t2224094215 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DeviceVibrate::Reset()
extern "C"  void DeviceVibrate_Reset_m1554208700 (DeviceVibrate_t2224094215 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DeviceVibrate::OnEnter()
extern "C"  void DeviceVibrate_OnEnter_m962816294 (DeviceVibrate_t2224094215 * __this, const MethodInfo* method)
{
	{
		Handheld_Vibrate_m2690509590(NULL /*static, unused*/, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DontDestroyOnLoad::.ctor()
extern "C"  void DontDestroyOnLoad__ctor_m2045012090 (DontDestroyOnLoad_t722253884 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DontDestroyOnLoad::Reset()
extern "C"  void DontDestroyOnLoad_Reset_m3986412327 (DontDestroyOnLoad_t722253884 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DontDestroyOnLoad::OnEnter()
extern "C"  void DontDestroyOnLoad_OnEnter_m1848292817 (DontDestroyOnLoad_t722253884 * __this, const MethodInfo* method)
{
	{
		GameObject_t3674682005 * L_0 = FsmStateAction_get_Owner_m1855633209(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t1659122786 * L_1 = GameObject_get_transform_m1278640159(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t1659122786 * L_2 = Transform_get_root_m1064615716(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_t3674682005 * L_3 = Component_get_gameObject_m1170635899(L_2, /*hidden argument*/NULL);
		Object_DontDestroyOnLoad_m4064482788(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DrawDebugLine::.ctor()
extern "C"  void DrawDebugLine__ctor_m2302876293 (DrawDebugLine_t2835919313 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DrawDebugLine::Reset()
extern Il2CppClass* FsmGameObject_t1697147867_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern const uint32_t DrawDebugLine_Reset_m4244276530_MetadataUsageId;
extern "C"  void DrawDebugLine_Reset_m4244276530 (DrawDebugLine_t2835919313 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DrawDebugLine_Reset_m4244276530_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmGameObject_t1697147867 * V_0 = NULL;
	FsmVector3_t533912882 * V_1 = NULL;
	{
		FsmGameObject_t1697147867 * L_0 = (FsmGameObject_t1697147867 *)il2cpp_codegen_object_new(FsmGameObject_t1697147867_il2cpp_TypeInfo_var);
		FsmGameObject__ctor_m1048269796(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmGameObject_t1697147867 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_2 = V_0;
		__this->set_fromObject_9(L_2);
		FsmVector3_t533912882 * L_3 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_3, /*hidden argument*/NULL);
		V_1 = L_3;
		FsmVector3_t533912882 * L_4 = V_1;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_5 = V_1;
		__this->set_fromPosition_10(L_5);
		FsmGameObject_t1697147867 * L_6 = (FsmGameObject_t1697147867 *)il2cpp_codegen_object_new(FsmGameObject_t1697147867_il2cpp_TypeInfo_var);
		FsmGameObject__ctor_m1048269796(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		FsmGameObject_t1697147867 * L_7 = V_0;
		NullCheck(L_7);
		NamedVariable_set_UseVariable_m4266138971(L_7, (bool)1, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_8 = V_0;
		__this->set_toObject_11(L_8);
		FsmVector3_t533912882 * L_9 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_9, /*hidden argument*/NULL);
		V_1 = L_9;
		FsmVector3_t533912882 * L_10 = V_1;
		NullCheck(L_10);
		NamedVariable_set_UseVariable_m4266138971(L_10, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_11 = V_1;
		__this->set_toPosition_12(L_11);
		Color_t4194546905  L_12 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		FsmColor_t2131419205 * L_13 = FsmColor_op_Implicit_m2192961033(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		__this->set_color_13(L_13);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DrawDebugLine::OnUpdate()
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const uint32_t DrawDebugLine_OnUpdate_m3227008167_MetadataUsageId;
extern "C"  void DrawDebugLine_OnUpdate_m3227008167 (DrawDebugLine_t2835919313 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DrawDebugLine_OnUpdate_m3227008167_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		FsmGameObject_t1697147867 * L_0 = __this->get_fromObject_9();
		FsmVector3_t533912882 * L_1 = __this->get_fromPosition_10();
		Vector3_t4282066566  L_2 = ActionHelpers_GetPosition_m819515482(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		FsmGameObject_t1697147867 * L_3 = __this->get_toObject_11();
		FsmVector3_t533912882 * L_4 = __this->get_toPosition_12();
		Vector3_t4282066566  L_5 = ActionHelpers_GetPosition_m819515482(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		Vector3_t4282066566  L_6 = V_0;
		Vector3_t4282066566  L_7 = V_1;
		FsmColor_t2131419205 * L_8 = __this->get_color_13();
		NullCheck(L_8);
		Color_t4194546905  L_9 = FsmColor_get_Value_m1679829997(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_DrawLine_m4257273494(NULL /*static, unused*/, L_6, L_7, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DrawDebugRay::.ctor()
extern "C"  void DrawDebugRay__ctor_m1447233891 (DrawDebugRay_t3299009635 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DrawDebugRay::Reset()
extern Il2CppClass* FsmGameObject_t1697147867_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern const uint32_t DrawDebugRay_Reset_m3388634128_MetadataUsageId;
extern "C"  void DrawDebugRay_Reset_m3388634128 (DrawDebugRay_t3299009635 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DrawDebugRay_Reset_m3388634128_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmGameObject_t1697147867 * V_0 = NULL;
	FsmVector3_t533912882 * V_1 = NULL;
	{
		FsmGameObject_t1697147867 * L_0 = (FsmGameObject_t1697147867 *)il2cpp_codegen_object_new(FsmGameObject_t1697147867_il2cpp_TypeInfo_var);
		FsmGameObject__ctor_m1048269796(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmGameObject_t1697147867 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_2 = V_0;
		__this->set_fromObject_9(L_2);
		FsmVector3_t533912882 * L_3 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_3, /*hidden argument*/NULL);
		V_1 = L_3;
		FsmVector3_t533912882 * L_4 = V_1;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_5 = V_1;
		__this->set_fromPosition_10(L_5);
		FsmVector3_t533912882 * L_6 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_6, /*hidden argument*/NULL);
		V_1 = L_6;
		FsmVector3_t533912882 * L_7 = V_1;
		NullCheck(L_7);
		NamedVariable_set_UseVariable_m4266138971(L_7, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_8 = V_1;
		__this->set_direction_11(L_8);
		Color_t4194546905  L_9 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		FsmColor_t2131419205 * L_10 = FsmColor_op_Implicit_m2192961033(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		__this->set_color_12(L_10);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DrawDebugRay::OnUpdate()
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const uint32_t DrawDebugRay_OnUpdate_m3415111945_MetadataUsageId;
extern "C"  void DrawDebugRay_OnUpdate_m3415111945 (DrawDebugRay_t3299009635 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DrawDebugRay_OnUpdate_m3415111945_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		FsmGameObject_t1697147867 * L_0 = __this->get_fromObject_9();
		FsmVector3_t533912882 * L_1 = __this->get_fromPosition_10();
		Vector3_t4282066566  L_2 = ActionHelpers_GetPosition_m819515482(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t4282066566  L_3 = V_0;
		FsmVector3_t533912882 * L_4 = __this->get_direction_11();
		NullCheck(L_4);
		Vector3_t4282066566  L_5 = FsmVector3_get_Value_m2779135117(L_4, /*hidden argument*/NULL);
		FsmColor_t2131419205 * L_6 = __this->get_color_12();
		NullCheck(L_6);
		Color_t4194546905  L_7 = FsmColor_get_Value_m1679829997(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_DrawRay_m123146114(NULL /*static, unused*/, L_3, L_5, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DrawFullscreenColor::.ctor()
extern "C"  void DrawFullscreenColor__ctor_m2379172196 (DrawFullscreenColor_t161869330 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DrawFullscreenColor::Reset()
extern "C"  void DrawFullscreenColor_Reset_m25605137 (DrawFullscreenColor_t161869330 * __this, const MethodInfo* method)
{
	{
		Color_t4194546905  L_0 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		FsmColor_t2131419205 * L_1 = FsmColor_op_Implicit_m2192961033(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_color_9(L_1);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DrawFullscreenColor::OnGUI()
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern const uint32_t DrawFullscreenColor_OnGUI_m1874570846_MetadataUsageId;
extern "C"  void DrawFullscreenColor_OnGUI_m1874570846 (DrawFullscreenColor_t161869330 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DrawFullscreenColor_OnGUI_m1874570846_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Color_t4194546905  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		Color_t4194546905  L_0 = GUI_get_color_m1489208189(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmColor_t2131419205 * L_1 = __this->get_color_9();
		NullCheck(L_1);
		Color_t4194546905  L_2 = FsmColor_get_Value_m1679829997(L_1, /*hidden argument*/NULL);
		GUI_set_color_m2304110692(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		int32_t L_3 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_4 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t4241904616  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Rect__ctor_m3291325233(&L_5, (0.0f), (0.0f), (((float)((float)L_3))), (((float)((float)L_4))), /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_6 = ActionHelpers_get_WhiteTexture_m2707937660(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUI_DrawTexture_m418809280(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		Color_t4194546905  L_7 = V_0;
		GUI_set_color_m2304110692(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DrawStateLabel::.ctor()
extern "C"  void DrawStateLabel__ctor_m4274611479 (DrawStateLabel_t2977589295 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DrawStateLabel::Reset()
extern "C"  void DrawStateLabel_Reset_m1921044420 (DrawStateLabel_t2977589295 * __this, const MethodInfo* method)
{
	{
		FsmBool_t1075959796 * L_0 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_showLabel_9(L_0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DrawStateLabel::OnEnter()
extern "C"  void DrawStateLabel_OnEnter_m1304624942 (DrawStateLabel_t2977589295 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_1 = __this->get_showLabel_9();
		NullCheck(L_1);
		bool L_2 = FsmBool_get_Value_m3101329097(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Fsm_set_ShowStateLabel_m3525398371(L_0, L_2, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DrawTexture::.ctor()
extern "C"  void DrawTexture__ctor_m2580760337 (DrawTexture_t1601360325 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DrawTexture::Reset()
extern "C"  void DrawTexture_Reset_m227193278 (DrawTexture_t1601360325 * __this, const MethodInfo* method)
{
	{
		__this->set_texture_9((FsmTexture_t3073272573 *)NULL);
		__this->set_screenRect_10((FsmRect_t1076426478 *)NULL);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_left_11(L_0);
		FsmFloat_t2134102846 * L_1 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_top_12(L_1);
		FsmFloat_t2134102846 * L_2 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_width_13(L_2);
		FsmFloat_t2134102846 * L_3 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_height_14(L_3);
		__this->set_scaleMode_15(0);
		FsmBool_t1075959796 * L_4 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_alphaBlend_16(L_4);
		FsmFloat_t2134102846 * L_5 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_imageAspect_17(L_5);
		FsmBool_t1075959796 * L_6 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_normalized_18(L_6);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.DrawTexture::OnGUI()
extern Il2CppClass* Rect_t4241904616_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern const uint32_t DrawTexture_OnGUI_m2076158987_MetadataUsageId;
extern "C"  void DrawTexture_OnGUI_m2076158987 (DrawTexture_t1601360325 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DrawTexture_OnGUI_m2076158987_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	DrawTexture_t1601360325 * G_B4_0 = NULL;
	DrawTexture_t1601360325 * G_B3_0 = NULL;
	Rect_t4241904616  G_B5_0;
	memset(&G_B5_0, 0, sizeof(G_B5_0));
	DrawTexture_t1601360325 * G_B5_1 = NULL;
	{
		FsmTexture_t3073272573 * L_0 = __this->get_texture_9();
		NullCheck(L_0);
		Texture_t2526458961 * L_1 = FsmTexture_get_Value_m3156202285(L_0, /*hidden argument*/NULL);
		bool L_2 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		return;
	}

IL_0017:
	{
		FsmRect_t1076426478 * L_3 = __this->get_screenRect_10();
		NullCheck(L_3);
		bool L_4 = NamedVariable_get_IsNone_m281035543(L_3, /*hidden argument*/NULL);
		G_B3_0 = __this;
		if (L_4)
		{
			G_B4_0 = __this;
			goto IL_0038;
		}
	}
	{
		FsmRect_t1076426478 * L_5 = __this->get_screenRect_10();
		NullCheck(L_5);
		Rect_t4241904616  L_6 = FsmRect_get_Value_m1002500317(L_5, /*hidden argument*/NULL);
		G_B5_0 = L_6;
		G_B5_1 = G_B3_0;
		goto IL_0041;
	}

IL_0038:
	{
		Initobj (Rect_t4241904616_il2cpp_TypeInfo_var, (&V_0));
		Rect_t4241904616  L_7 = V_0;
		G_B5_0 = L_7;
		G_B5_1 = G_B4_0;
	}

IL_0041:
	{
		NullCheck(G_B5_1);
		G_B5_1->set_rect_19(G_B5_0);
		FsmFloat_t2134102846 * L_8 = __this->get_left_11();
		NullCheck(L_8);
		bool L_9 = NamedVariable_get_IsNone_m281035543(L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_006c;
		}
	}
	{
		Rect_t4241904616 * L_10 = __this->get_address_of_rect_19();
		FsmFloat_t2134102846 * L_11 = __this->get_left_11();
		NullCheck(L_11);
		float L_12 = FsmFloat_get_Value_m4137923823(L_11, /*hidden argument*/NULL);
		Rect_set_x_m577970569(L_10, L_12, /*hidden argument*/NULL);
	}

IL_006c:
	{
		FsmFloat_t2134102846 * L_13 = __this->get_top_12();
		NullCheck(L_13);
		bool L_14 = NamedVariable_get_IsNone_m281035543(L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_0092;
		}
	}
	{
		Rect_t4241904616 * L_15 = __this->get_address_of_rect_19();
		FsmFloat_t2134102846 * L_16 = __this->get_top_12();
		NullCheck(L_16);
		float L_17 = FsmFloat_get_Value_m4137923823(L_16, /*hidden argument*/NULL);
		Rect_set_y_m67436392(L_15, L_17, /*hidden argument*/NULL);
	}

IL_0092:
	{
		FsmFloat_t2134102846 * L_18 = __this->get_width_13();
		NullCheck(L_18);
		bool L_19 = NamedVariable_get_IsNone_m281035543(L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_00b8;
		}
	}
	{
		Rect_t4241904616 * L_20 = __this->get_address_of_rect_19();
		FsmFloat_t2134102846 * L_21 = __this->get_width_13();
		NullCheck(L_21);
		float L_22 = FsmFloat_get_Value_m4137923823(L_21, /*hidden argument*/NULL);
		Rect_set_width_m3771513595(L_20, L_22, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		FsmFloat_t2134102846 * L_23 = __this->get_height_14();
		NullCheck(L_23);
		bool L_24 = NamedVariable_get_IsNone_m281035543(L_23, /*hidden argument*/NULL);
		if (L_24)
		{
			goto IL_00de;
		}
	}
	{
		Rect_t4241904616 * L_25 = __this->get_address_of_rect_19();
		FsmFloat_t2134102846 * L_26 = __this->get_height_14();
		NullCheck(L_26);
		float L_27 = FsmFloat_get_Value_m4137923823(L_26, /*hidden argument*/NULL);
		Rect_set_height_m3398820332(L_25, L_27, /*hidden argument*/NULL);
	}

IL_00de:
	{
		FsmBool_t1075959796 * L_28 = __this->get_normalized_18();
		NullCheck(L_28);
		bool L_29 = FsmBool_get_Value_m3101329097(L_28, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_014e;
		}
	}
	{
		Rect_t4241904616 * L_30 = __this->get_address_of_rect_19();
		Rect_t4241904616 * L_31 = L_30;
		float L_32 = Rect_get_x_m982385354(L_31, /*hidden argument*/NULL);
		int32_t L_33 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_x_m577970569(L_31, ((float)((float)L_32*(float)(((float)((float)L_33))))), /*hidden argument*/NULL);
		Rect_t4241904616 * L_34 = __this->get_address_of_rect_19();
		Rect_t4241904616 * L_35 = L_34;
		float L_36 = Rect_get_width_m2824209432(L_35, /*hidden argument*/NULL);
		int32_t L_37 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_width_m3771513595(L_35, ((float)((float)L_36*(float)(((float)((float)L_37))))), /*hidden argument*/NULL);
		Rect_t4241904616 * L_38 = __this->get_address_of_rect_19();
		Rect_t4241904616 * L_39 = L_38;
		float L_40 = Rect_get_y_m982386315(L_39, /*hidden argument*/NULL);
		int32_t L_41 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_y_m67436392(L_39, ((float)((float)L_40*(float)(((float)((float)L_41))))), /*hidden argument*/NULL);
		Rect_t4241904616 * L_42 = __this->get_address_of_rect_19();
		Rect_t4241904616 * L_43 = L_42;
		float L_44 = Rect_get_height_m2154960823(L_43, /*hidden argument*/NULL);
		int32_t L_45 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_height_m3398820332(L_43, ((float)((float)L_44*(float)(((float)((float)L_45))))), /*hidden argument*/NULL);
	}

IL_014e:
	{
		Rect_t4241904616  L_46 = __this->get_rect_19();
		FsmTexture_t3073272573 * L_47 = __this->get_texture_9();
		NullCheck(L_47);
		Texture_t2526458961 * L_48 = FsmTexture_get_Value_m3156202285(L_47, /*hidden argument*/NULL);
		int32_t L_49 = __this->get_scaleMode_15();
		FsmBool_t1075959796 * L_50 = __this->get_alphaBlend_16();
		NullCheck(L_50);
		bool L_51 = FsmBool_get_Value_m3101329097(L_50, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_52 = __this->get_imageAspect_17();
		NullCheck(L_52);
		float L_53 = FsmFloat_get_Value_m4137923823(L_52, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_DrawTexture_m1839804844(NULL /*static, unused*/, L_46, L_48, L_49, L_51, L_53, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EaseColor::.ctor()
extern "C"  void EaseColor__ctor_m4026007859 (EaseColor_t1435706211 * __this, const MethodInfo* method)
{
	{
		EaseFsmAction__ctor_m1487085792(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EaseColor::Reset()
extern "C"  void EaseColor_Reset_m1672440800 (EaseColor_t1435706211 * __this, const MethodInfo* method)
{
	{
		EaseFsmAction_Reset_m3428486029(__this, /*hidden argument*/NULL);
		__this->set_colorVariable_32((FsmColor_t2131419205 *)NULL);
		__this->set_fromValue_30((FsmColor_t2131419205 *)NULL);
		__this->set_toValue_31((FsmColor_t2131419205 *)NULL);
		__this->set_finishInNextStep_33((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EaseColor::OnEnter()
extern Il2CppClass* SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var;
extern const uint32_t EaseColor_OnEnter_m2914714698_MetadataUsageId;
extern "C"  void EaseColor_OnEnter_m2914714698 (EaseColor_t1435706211 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EaseColor_OnEnter_m2914714698_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Color_t4194546905  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Color_t4194546905  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Color_t4194546905  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Color_t4194546905  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Color_t4194546905  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Color_t4194546905  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Color_t4194546905  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Color_t4194546905  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		EaseFsmAction_OnEnter_m2552032439(__this, /*hidden argument*/NULL);
		((EaseFsmAction_t595986710 *)__this)->set_fromFloats_23(((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)4)));
		SingleU5BU5D_t2316563989* L_0 = ((EaseFsmAction_t595986710 *)__this)->get_fromFloats_23();
		FsmColor_t2131419205 * L_1 = __this->get_fromValue_30();
		NullCheck(L_1);
		Color_t4194546905  L_2 = FsmColor_get_Value_m1679829997(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = (&V_0)->get_r_0();
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)L_3);
		SingleU5BU5D_t2316563989* L_4 = ((EaseFsmAction_t595986710 *)__this)->get_fromFloats_23();
		FsmColor_t2131419205 * L_5 = __this->get_fromValue_30();
		NullCheck(L_5);
		Color_t4194546905  L_6 = FsmColor_get_Value_m1679829997(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		float L_7 = (&V_1)->get_g_1();
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (float)L_7);
		SingleU5BU5D_t2316563989* L_8 = ((EaseFsmAction_t595986710 *)__this)->get_fromFloats_23();
		FsmColor_t2131419205 * L_9 = __this->get_fromValue_30();
		NullCheck(L_9);
		Color_t4194546905  L_10 = FsmColor_get_Value_m1679829997(L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		float L_11 = (&V_2)->get_b_2();
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (float)L_11);
		SingleU5BU5D_t2316563989* L_12 = ((EaseFsmAction_t595986710 *)__this)->get_fromFloats_23();
		FsmColor_t2131419205 * L_13 = __this->get_fromValue_30();
		NullCheck(L_13);
		Color_t4194546905  L_14 = FsmColor_get_Value_m1679829997(L_13, /*hidden argument*/NULL);
		V_3 = L_14;
		float L_15 = (&V_3)->get_a_3();
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (float)L_15);
		((EaseFsmAction_t595986710 *)__this)->set_toFloats_24(((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)4)));
		SingleU5BU5D_t2316563989* L_16 = ((EaseFsmAction_t595986710 *)__this)->get_toFloats_24();
		FsmColor_t2131419205 * L_17 = __this->get_toValue_31();
		NullCheck(L_17);
		Color_t4194546905  L_18 = FsmColor_get_Value_m1679829997(L_17, /*hidden argument*/NULL);
		V_4 = L_18;
		float L_19 = (&V_4)->get_r_0();
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 0);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)L_19);
		SingleU5BU5D_t2316563989* L_20 = ((EaseFsmAction_t595986710 *)__this)->get_toFloats_24();
		FsmColor_t2131419205 * L_21 = __this->get_toValue_31();
		NullCheck(L_21);
		Color_t4194546905  L_22 = FsmColor_get_Value_m1679829997(L_21, /*hidden argument*/NULL);
		V_5 = L_22;
		float L_23 = (&V_5)->get_g_1();
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 1);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(1), (float)L_23);
		SingleU5BU5D_t2316563989* L_24 = ((EaseFsmAction_t595986710 *)__this)->get_toFloats_24();
		FsmColor_t2131419205 * L_25 = __this->get_toValue_31();
		NullCheck(L_25);
		Color_t4194546905  L_26 = FsmColor_get_Value_m1679829997(L_25, /*hidden argument*/NULL);
		V_6 = L_26;
		float L_27 = (&V_6)->get_b_2();
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 2);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(2), (float)L_27);
		SingleU5BU5D_t2316563989* L_28 = ((EaseFsmAction_t595986710 *)__this)->get_toFloats_24();
		FsmColor_t2131419205 * L_29 = __this->get_toValue_31();
		NullCheck(L_29);
		Color_t4194546905  L_30 = FsmColor_get_Value_m1679829997(L_29, /*hidden argument*/NULL);
		V_7 = L_30;
		float L_31 = (&V_7)->get_a_3();
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 3);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(3), (float)L_31);
		((EaseFsmAction_t595986710 *)__this)->set_resultFloats_25(((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)4)));
		__this->set_finishInNextStep_33((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EaseColor::OnExit()
extern "C"  void EaseColor_OnExit_m1765510670 (EaseColor_t1435706211 * __this, const MethodInfo* method)
{
	{
		EaseFsmAction_OnExit_m368337921(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EaseColor::OnUpdate()
extern "C"  void EaseColor_OnUpdate_m3590369081 (EaseColor_t1435706211 * __this, const MethodInfo* method)
{
	Color_t4194546905  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Color_t4194546905  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Color_t4194546905  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Color_t4194546905  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Color_t4194546905  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Color_t4194546905  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Color_t4194546905  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Color_t4194546905  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Color_t4194546905  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Color_t4194546905  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Color_t4194546905  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Color_t4194546905  V_11;
	memset(&V_11, 0, sizeof(V_11));
	FsmColor_t2131419205 * G_B11_0 = NULL;
	FsmColor_t2131419205 * G_B10_0 = NULL;
	float G_B14_0 = 0.0f;
	FsmColor_t2131419205 * G_B14_1 = NULL;
	FsmColor_t2131419205 * G_B13_0 = NULL;
	FsmColor_t2131419205 * G_B12_0 = NULL;
	float G_B16_0 = 0.0f;
	FsmColor_t2131419205 * G_B16_1 = NULL;
	float G_B15_0 = 0.0f;
	FsmColor_t2131419205 * G_B15_1 = NULL;
	float G_B19_0 = 0.0f;
	float G_B19_1 = 0.0f;
	FsmColor_t2131419205 * G_B19_2 = NULL;
	float G_B18_0 = 0.0f;
	FsmColor_t2131419205 * G_B18_1 = NULL;
	float G_B17_0 = 0.0f;
	FsmColor_t2131419205 * G_B17_1 = NULL;
	float G_B21_0 = 0.0f;
	float G_B21_1 = 0.0f;
	FsmColor_t2131419205 * G_B21_2 = NULL;
	float G_B20_0 = 0.0f;
	float G_B20_1 = 0.0f;
	FsmColor_t2131419205 * G_B20_2 = NULL;
	float G_B24_0 = 0.0f;
	float G_B24_1 = 0.0f;
	float G_B24_2 = 0.0f;
	FsmColor_t2131419205 * G_B24_3 = NULL;
	float G_B23_0 = 0.0f;
	float G_B23_1 = 0.0f;
	FsmColor_t2131419205 * G_B23_2 = NULL;
	float G_B22_0 = 0.0f;
	float G_B22_1 = 0.0f;
	FsmColor_t2131419205 * G_B22_2 = NULL;
	float G_B26_0 = 0.0f;
	float G_B26_1 = 0.0f;
	float G_B26_2 = 0.0f;
	FsmColor_t2131419205 * G_B26_3 = NULL;
	float G_B25_0 = 0.0f;
	float G_B25_1 = 0.0f;
	float G_B25_2 = 0.0f;
	FsmColor_t2131419205 * G_B25_3 = NULL;
	float G_B29_0 = 0.0f;
	float G_B29_1 = 0.0f;
	float G_B29_2 = 0.0f;
	float G_B29_3 = 0.0f;
	FsmColor_t2131419205 * G_B29_4 = NULL;
	float G_B28_0 = 0.0f;
	float G_B28_1 = 0.0f;
	float G_B28_2 = 0.0f;
	FsmColor_t2131419205 * G_B28_3 = NULL;
	float G_B27_0 = 0.0f;
	float G_B27_1 = 0.0f;
	float G_B27_2 = 0.0f;
	FsmColor_t2131419205 * G_B27_3 = NULL;
	{
		EaseFsmAction_OnUpdate_m937153644(__this, /*hidden argument*/NULL);
		FsmColor_t2131419205 * L_0 = __this->get_colorVariable_32();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0051;
		}
	}
	{
		bool L_2 = ((EaseFsmAction_t595986710 *)__this)->get_isRunning_29();
		if (!L_2)
		{
			goto IL_0051;
		}
	}
	{
		FsmColor_t2131419205 * L_3 = __this->get_colorVariable_32();
		SingleU5BU5D_t2316563989* L_4 = ((EaseFsmAction_t595986710 *)__this)->get_resultFloats_25();
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		int32_t L_5 = 0;
		float L_6 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		SingleU5BU5D_t2316563989* L_7 = ((EaseFsmAction_t595986710 *)__this)->get_resultFloats_25();
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 1);
		int32_t L_8 = 1;
		float L_9 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		SingleU5BU5D_t2316563989* L_10 = ((EaseFsmAction_t595986710 *)__this)->get_resultFloats_25();
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		int32_t L_11 = 2;
		float L_12 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		SingleU5BU5D_t2316563989* L_13 = ((EaseFsmAction_t595986710 *)__this)->get_resultFloats_25();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 3);
		int32_t L_14 = 3;
		float L_15 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		Color_t4194546905  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Color__ctor_m2252924356(&L_16, L_6, L_9, L_12, L_15, /*hidden argument*/NULL);
		NullCheck(L_3);
		FsmColor_set_Value_m1684002054(L_3, L_16, /*hidden argument*/NULL);
	}

IL_0051:
	{
		bool L_17 = __this->get_finishInNextStep_33();
		if (!L_17)
		{
			goto IL_007e;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_18 = ((EaseFsmAction_t595986710 *)__this)->get_finishEvent_14();
		if (!L_18)
		{
			goto IL_007e;
		}
	}
	{
		Fsm_t1527112426 * L_19 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_20 = ((EaseFsmAction_t595986710 *)__this)->get_finishEvent_14();
		NullCheck(L_19);
		Fsm_Event_m625948263(L_19, L_20, /*hidden argument*/NULL);
	}

IL_007e:
	{
		bool L_21 = ((EaseFsmAction_t595986710 *)__this)->get_finishAction_26();
		if (!L_21)
		{
			goto IL_024f;
		}
	}
	{
		bool L_22 = __this->get_finishInNextStep_33();
		if (L_22)
		{
			goto IL_024f;
		}
	}
	{
		FsmColor_t2131419205 * L_23 = __this->get_colorVariable_32();
		NullCheck(L_23);
		bool L_24 = NamedVariable_get_IsNone_m281035543(L_23, /*hidden argument*/NULL);
		if (L_24)
		{
			goto IL_0248;
		}
	}
	{
		FsmColor_t2131419205 * L_25 = __this->get_colorVariable_32();
		FsmBool_t1075959796 * L_26 = ((EaseFsmAction_t595986710 *)__this)->get_reverse_13();
		NullCheck(L_26);
		bool L_27 = NamedVariable_get_IsNone_m281035543(L_26, /*hidden argument*/NULL);
		G_B10_0 = L_25;
		if (!L_27)
		{
			G_B11_0 = L_25;
			goto IL_00d2;
		}
	}
	{
		FsmColor_t2131419205 * L_28 = __this->get_toValue_31();
		NullCheck(L_28);
		Color_t4194546905  L_29 = FsmColor_get_Value_m1679829997(L_28, /*hidden argument*/NULL);
		V_0 = L_29;
		float L_30 = (&V_0)->get_r_0();
		G_B14_0 = L_30;
		G_B14_1 = G_B10_0;
		goto IL_010d;
	}

IL_00d2:
	{
		FsmBool_t1075959796 * L_31 = ((EaseFsmAction_t595986710 *)__this)->get_reverse_13();
		NullCheck(L_31);
		bool L_32 = FsmBool_get_Value_m3101329097(L_31, /*hidden argument*/NULL);
		G_B12_0 = G_B11_0;
		if (!L_32)
		{
			G_B13_0 = G_B11_0;
			goto IL_00fa;
		}
	}
	{
		FsmColor_t2131419205 * L_33 = __this->get_fromValue_30();
		NullCheck(L_33);
		Color_t4194546905  L_34 = FsmColor_get_Value_m1679829997(L_33, /*hidden argument*/NULL);
		V_1 = L_34;
		float L_35 = (&V_1)->get_r_0();
		G_B14_0 = L_35;
		G_B14_1 = G_B12_0;
		goto IL_010d;
	}

IL_00fa:
	{
		FsmColor_t2131419205 * L_36 = __this->get_toValue_31();
		NullCheck(L_36);
		Color_t4194546905  L_37 = FsmColor_get_Value_m1679829997(L_36, /*hidden argument*/NULL);
		V_2 = L_37;
		float L_38 = (&V_2)->get_r_0();
		G_B14_0 = L_38;
		G_B14_1 = G_B13_0;
	}

IL_010d:
	{
		FsmBool_t1075959796 * L_39 = ((EaseFsmAction_t595986710 *)__this)->get_reverse_13();
		NullCheck(L_39);
		bool L_40 = NamedVariable_get_IsNone_m281035543(L_39, /*hidden argument*/NULL);
		G_B15_0 = G_B14_0;
		G_B15_1 = G_B14_1;
		if (!L_40)
		{
			G_B16_0 = G_B14_0;
			G_B16_1 = G_B14_1;
			goto IL_0135;
		}
	}
	{
		FsmColor_t2131419205 * L_41 = __this->get_toValue_31();
		NullCheck(L_41);
		Color_t4194546905  L_42 = FsmColor_get_Value_m1679829997(L_41, /*hidden argument*/NULL);
		V_3 = L_42;
		float L_43 = (&V_3)->get_g_1();
		G_B19_0 = L_43;
		G_B19_1 = G_B15_0;
		G_B19_2 = G_B15_1;
		goto IL_0172;
	}

IL_0135:
	{
		FsmBool_t1075959796 * L_44 = ((EaseFsmAction_t595986710 *)__this)->get_reverse_13();
		NullCheck(L_44);
		bool L_45 = FsmBool_get_Value_m3101329097(L_44, /*hidden argument*/NULL);
		G_B17_0 = G_B16_0;
		G_B17_1 = G_B16_1;
		if (!L_45)
		{
			G_B18_0 = G_B16_0;
			G_B18_1 = G_B16_1;
			goto IL_015e;
		}
	}
	{
		FsmColor_t2131419205 * L_46 = __this->get_fromValue_30();
		NullCheck(L_46);
		Color_t4194546905  L_47 = FsmColor_get_Value_m1679829997(L_46, /*hidden argument*/NULL);
		V_4 = L_47;
		float L_48 = (&V_4)->get_g_1();
		G_B19_0 = L_48;
		G_B19_1 = G_B17_0;
		G_B19_2 = G_B17_1;
		goto IL_0172;
	}

IL_015e:
	{
		FsmColor_t2131419205 * L_49 = __this->get_toValue_31();
		NullCheck(L_49);
		Color_t4194546905  L_50 = FsmColor_get_Value_m1679829997(L_49, /*hidden argument*/NULL);
		V_5 = L_50;
		float L_51 = (&V_5)->get_g_1();
		G_B19_0 = L_51;
		G_B19_1 = G_B18_0;
		G_B19_2 = G_B18_1;
	}

IL_0172:
	{
		FsmBool_t1075959796 * L_52 = ((EaseFsmAction_t595986710 *)__this)->get_reverse_13();
		NullCheck(L_52);
		bool L_53 = NamedVariable_get_IsNone_m281035543(L_52, /*hidden argument*/NULL);
		G_B20_0 = G_B19_0;
		G_B20_1 = G_B19_1;
		G_B20_2 = G_B19_2;
		if (!L_53)
		{
			G_B21_0 = G_B19_0;
			G_B21_1 = G_B19_1;
			G_B21_2 = G_B19_2;
			goto IL_019b;
		}
	}
	{
		FsmColor_t2131419205 * L_54 = __this->get_toValue_31();
		NullCheck(L_54);
		Color_t4194546905  L_55 = FsmColor_get_Value_m1679829997(L_54, /*hidden argument*/NULL);
		V_6 = L_55;
		float L_56 = (&V_6)->get_b_2();
		G_B24_0 = L_56;
		G_B24_1 = G_B20_0;
		G_B24_2 = G_B20_1;
		G_B24_3 = G_B20_2;
		goto IL_01d8;
	}

IL_019b:
	{
		FsmBool_t1075959796 * L_57 = ((EaseFsmAction_t595986710 *)__this)->get_reverse_13();
		NullCheck(L_57);
		bool L_58 = FsmBool_get_Value_m3101329097(L_57, /*hidden argument*/NULL);
		G_B22_0 = G_B21_0;
		G_B22_1 = G_B21_1;
		G_B22_2 = G_B21_2;
		if (!L_58)
		{
			G_B23_0 = G_B21_0;
			G_B23_1 = G_B21_1;
			G_B23_2 = G_B21_2;
			goto IL_01c4;
		}
	}
	{
		FsmColor_t2131419205 * L_59 = __this->get_fromValue_30();
		NullCheck(L_59);
		Color_t4194546905  L_60 = FsmColor_get_Value_m1679829997(L_59, /*hidden argument*/NULL);
		V_7 = L_60;
		float L_61 = (&V_7)->get_b_2();
		G_B24_0 = L_61;
		G_B24_1 = G_B22_0;
		G_B24_2 = G_B22_1;
		G_B24_3 = G_B22_2;
		goto IL_01d8;
	}

IL_01c4:
	{
		FsmColor_t2131419205 * L_62 = __this->get_toValue_31();
		NullCheck(L_62);
		Color_t4194546905  L_63 = FsmColor_get_Value_m1679829997(L_62, /*hidden argument*/NULL);
		V_8 = L_63;
		float L_64 = (&V_8)->get_b_2();
		G_B24_0 = L_64;
		G_B24_1 = G_B23_0;
		G_B24_2 = G_B23_1;
		G_B24_3 = G_B23_2;
	}

IL_01d8:
	{
		FsmBool_t1075959796 * L_65 = ((EaseFsmAction_t595986710 *)__this)->get_reverse_13();
		NullCheck(L_65);
		bool L_66 = NamedVariable_get_IsNone_m281035543(L_65, /*hidden argument*/NULL);
		G_B25_0 = G_B24_0;
		G_B25_1 = G_B24_1;
		G_B25_2 = G_B24_2;
		G_B25_3 = G_B24_3;
		if (!L_66)
		{
			G_B26_0 = G_B24_0;
			G_B26_1 = G_B24_1;
			G_B26_2 = G_B24_2;
			G_B26_3 = G_B24_3;
			goto IL_0201;
		}
	}
	{
		FsmColor_t2131419205 * L_67 = __this->get_toValue_31();
		NullCheck(L_67);
		Color_t4194546905  L_68 = FsmColor_get_Value_m1679829997(L_67, /*hidden argument*/NULL);
		V_9 = L_68;
		float L_69 = (&V_9)->get_a_3();
		G_B29_0 = L_69;
		G_B29_1 = G_B25_0;
		G_B29_2 = G_B25_1;
		G_B29_3 = G_B25_2;
		G_B29_4 = G_B25_3;
		goto IL_023e;
	}

IL_0201:
	{
		FsmBool_t1075959796 * L_70 = ((EaseFsmAction_t595986710 *)__this)->get_reverse_13();
		NullCheck(L_70);
		bool L_71 = FsmBool_get_Value_m3101329097(L_70, /*hidden argument*/NULL);
		G_B27_0 = G_B26_0;
		G_B27_1 = G_B26_1;
		G_B27_2 = G_B26_2;
		G_B27_3 = G_B26_3;
		if (!L_71)
		{
			G_B28_0 = G_B26_0;
			G_B28_1 = G_B26_1;
			G_B28_2 = G_B26_2;
			G_B28_3 = G_B26_3;
			goto IL_022a;
		}
	}
	{
		FsmColor_t2131419205 * L_72 = __this->get_fromValue_30();
		NullCheck(L_72);
		Color_t4194546905  L_73 = FsmColor_get_Value_m1679829997(L_72, /*hidden argument*/NULL);
		V_10 = L_73;
		float L_74 = (&V_10)->get_a_3();
		G_B29_0 = L_74;
		G_B29_1 = G_B27_0;
		G_B29_2 = G_B27_1;
		G_B29_3 = G_B27_2;
		G_B29_4 = G_B27_3;
		goto IL_023e;
	}

IL_022a:
	{
		FsmColor_t2131419205 * L_75 = __this->get_toValue_31();
		NullCheck(L_75);
		Color_t4194546905  L_76 = FsmColor_get_Value_m1679829997(L_75, /*hidden argument*/NULL);
		V_11 = L_76;
		float L_77 = (&V_11)->get_a_3();
		G_B29_0 = L_77;
		G_B29_1 = G_B28_0;
		G_B29_2 = G_B28_1;
		G_B29_3 = G_B28_2;
		G_B29_4 = G_B28_3;
	}

IL_023e:
	{
		Color_t4194546905  L_78;
		memset(&L_78, 0, sizeof(L_78));
		Color__ctor_m2252924356(&L_78, G_B29_3, G_B29_2, G_B29_1, G_B29_0, /*hidden argument*/NULL);
		NullCheck(G_B29_4);
		FsmColor_set_Value_m1684002054(G_B29_4, L_78, /*hidden argument*/NULL);
	}

IL_0248:
	{
		__this->set_finishInNextStep_33((bool)1);
	}

IL_024f:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EaseFloat::.ctor()
extern "C"  void EaseFloat__ctor_m2771277402 (EaseFloat_t1438389852 * __this, const MethodInfo* method)
{
	{
		EaseFsmAction__ctor_m1487085792(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EaseFloat::Reset()
extern "C"  void EaseFloat_Reset_m417710343 (EaseFloat_t1438389852 * __this, const MethodInfo* method)
{
	{
		EaseFsmAction_Reset_m3428486029(__this, /*hidden argument*/NULL);
		__this->set_floatVariable_32((FsmFloat_t2134102846 *)NULL);
		__this->set_fromValue_30((FsmFloat_t2134102846 *)NULL);
		__this->set_toValue_31((FsmFloat_t2134102846 *)NULL);
		__this->set_finishInNextStep_33((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EaseFloat::OnEnter()
extern Il2CppClass* SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var;
extern const uint32_t EaseFloat_OnEnter_m4004555697_MetadataUsageId;
extern "C"  void EaseFloat_OnEnter_m4004555697 (EaseFloat_t1438389852 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EaseFloat_OnEnter_m4004555697_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EaseFsmAction_OnEnter_m2552032439(__this, /*hidden argument*/NULL);
		((EaseFsmAction_t595986710 *)__this)->set_fromFloats_23(((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)1)));
		SingleU5BU5D_t2316563989* L_0 = ((EaseFsmAction_t595986710 *)__this)->get_fromFloats_23();
		FsmFloat_t2134102846 * L_1 = __this->get_fromValue_30();
		NullCheck(L_1);
		float L_2 = FsmFloat_get_Value_m4137923823(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)L_2);
		((EaseFsmAction_t595986710 *)__this)->set_toFloats_24(((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)1)));
		SingleU5BU5D_t2316563989* L_3 = ((EaseFsmAction_t595986710 *)__this)->get_toFloats_24();
		FsmFloat_t2134102846 * L_4 = __this->get_toValue_31();
		NullCheck(L_4);
		float L_5 = FsmFloat_get_Value_m4137923823(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)L_5);
		((EaseFsmAction_t595986710 *)__this)->set_resultFloats_25(((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)1)));
		__this->set_finishInNextStep_33((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EaseFloat::OnExit()
extern "C"  void EaseFloat_OnExit_m1523572167 (EaseFloat_t1438389852 * __this, const MethodInfo* method)
{
	{
		EaseFsmAction_OnExit_m368337921(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EaseFloat::OnUpdate()
extern "C"  void EaseFloat_OnUpdate_m3015701682 (EaseFloat_t1438389852 * __this, const MethodInfo* method)
{
	FsmFloat_t2134102846 * G_B11_0 = NULL;
	FsmFloat_t2134102846 * G_B10_0 = NULL;
	float G_B14_0 = 0.0f;
	FsmFloat_t2134102846 * G_B14_1 = NULL;
	FsmFloat_t2134102846 * G_B13_0 = NULL;
	FsmFloat_t2134102846 * G_B12_0 = NULL;
	{
		EaseFsmAction_OnUpdate_m937153644(__this, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_0 = __this->get_floatVariable_32();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0034;
		}
	}
	{
		bool L_2 = ((EaseFsmAction_t595986710 *)__this)->get_isRunning_29();
		if (!L_2)
		{
			goto IL_0034;
		}
	}
	{
		FsmFloat_t2134102846 * L_3 = __this->get_floatVariable_32();
		SingleU5BU5D_t2316563989* L_4 = ((EaseFsmAction_t595986710 *)__this)->get_resultFloats_25();
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		int32_t L_5 = 0;
		float L_6 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_3);
		FsmFloat_set_Value_m1568963140(L_3, L_6, /*hidden argument*/NULL);
	}

IL_0034:
	{
		bool L_7 = __this->get_finishInNextStep_33();
		if (!L_7)
		{
			goto IL_0061;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_8 = ((EaseFsmAction_t595986710 *)__this)->get_finishEvent_14();
		if (!L_8)
		{
			goto IL_0061;
		}
	}
	{
		Fsm_t1527112426 * L_9 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_10 = ((EaseFsmAction_t595986710 *)__this)->get_finishEvent_14();
		NullCheck(L_9);
		Fsm_Event_m625948263(L_9, L_10, /*hidden argument*/NULL);
	}

IL_0061:
	{
		bool L_11 = ((EaseFsmAction_t595986710 *)__this)->get_finishAction_26();
		if (!L_11)
		{
			goto IL_00e4;
		}
	}
	{
		bool L_12 = __this->get_finishInNextStep_33();
		if (L_12)
		{
			goto IL_00e4;
		}
	}
	{
		FsmFloat_t2134102846 * L_13 = __this->get_floatVariable_32();
		NullCheck(L_13);
		bool L_14 = NamedVariable_get_IsNone_m281035543(L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_00dd;
		}
	}
	{
		FsmFloat_t2134102846 * L_15 = __this->get_floatVariable_32();
		FsmBool_t1075959796 * L_16 = ((EaseFsmAction_t595986710 *)__this)->get_reverse_13();
		NullCheck(L_16);
		bool L_17 = NamedVariable_get_IsNone_m281035543(L_16, /*hidden argument*/NULL);
		G_B10_0 = L_15;
		if (!L_17)
		{
			G_B11_0 = L_15;
			goto IL_00ad;
		}
	}
	{
		FsmFloat_t2134102846 * L_18 = __this->get_toValue_31();
		NullCheck(L_18);
		float L_19 = FsmFloat_get_Value_m4137923823(L_18, /*hidden argument*/NULL);
		G_B14_0 = L_19;
		G_B14_1 = G_B10_0;
		goto IL_00d8;
	}

IL_00ad:
	{
		FsmBool_t1075959796 * L_20 = ((EaseFsmAction_t595986710 *)__this)->get_reverse_13();
		NullCheck(L_20);
		bool L_21 = FsmBool_get_Value_m3101329097(L_20, /*hidden argument*/NULL);
		G_B12_0 = G_B11_0;
		if (!L_21)
		{
			G_B13_0 = G_B11_0;
			goto IL_00cd;
		}
	}
	{
		FsmFloat_t2134102846 * L_22 = __this->get_fromValue_30();
		NullCheck(L_22);
		float L_23 = FsmFloat_get_Value_m4137923823(L_22, /*hidden argument*/NULL);
		G_B14_0 = L_23;
		G_B14_1 = G_B12_0;
		goto IL_00d8;
	}

IL_00cd:
	{
		FsmFloat_t2134102846 * L_24 = __this->get_toValue_31();
		NullCheck(L_24);
		float L_25 = FsmFloat_get_Value_m4137923823(L_24, /*hidden argument*/NULL);
		G_B14_0 = L_25;
		G_B14_1 = G_B13_0;
	}

IL_00d8:
	{
		NullCheck(G_B14_1);
		FsmFloat_set_Value_m1568963140(G_B14_1, G_B14_0, /*hidden argument*/NULL);
	}

IL_00dd:
	{
		__this->set_finishInNextStep_33((bool)1);
	}

IL_00e4:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EaseFsmAction::.ctor()
extern Il2CppClass* SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var;
extern const uint32_t EaseFsmAction__ctor_m1487085792_MetadataUsageId;
extern "C"  void EaseFsmAction__ctor_m1487085792 (EaseFsmAction_t595986710 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EaseFsmAction__ctor_m1487085792_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_easeType_12(((int32_t)21));
		__this->set_fromFloats_23(((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)0)));
		__this->set_toFloats_24(((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)0)));
		__this->set_resultFloats_25(((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)0)));
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EaseFsmAction::Reset()
extern Il2CppClass* FsmFloat_t2134102846_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmBool_t1075959796_il2cpp_TypeInfo_var;
extern Il2CppClass* SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var;
extern const uint32_t EaseFsmAction_Reset_m3428486029_MetadataUsageId;
extern "C"  void EaseFsmAction_Reset_m3428486029 (EaseFsmAction_t595986710 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EaseFsmAction_Reset_m3428486029_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmFloat_t2134102846 * V_0 = NULL;
	FsmBool_t1075959796 * V_1 = NULL;
	{
		__this->set_easeType_12(((int32_t)21));
		FsmFloat_t2134102846 * L_0 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmFloat_t2134102846 * L_1 = V_0;
		NullCheck(L_1);
		FsmFloat_set_Value_m1568963140(L_1, (1.0f), /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_2 = V_0;
		__this->set_time_9(L_2);
		FsmFloat_t2134102846 * L_3 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_3, /*hidden argument*/NULL);
		V_0 = L_3;
		FsmFloat_t2134102846 * L_4 = V_0;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_5 = V_0;
		__this->set_delay_11(L_5);
		FsmFloat_t2134102846 * L_6 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		FsmFloat_t2134102846 * L_7 = V_0;
		NullCheck(L_7);
		NamedVariable_set_UseVariable_m4266138971(L_7, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_8 = V_0;
		__this->set_speed_10(L_8);
		FsmBool_t1075959796 * L_9 = (FsmBool_t1075959796 *)il2cpp_codegen_object_new(FsmBool_t1075959796_il2cpp_TypeInfo_var);
		FsmBool__ctor_m1553455211(L_9, /*hidden argument*/NULL);
		V_1 = L_9;
		FsmBool_t1075959796 * L_10 = V_1;
		NullCheck(L_10);
		FsmBool_set_Value_m1126216340(L_10, (bool)0, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_11 = V_1;
		__this->set_reverse_13(L_11);
		__this->set_realTime_15((bool)0);
		__this->set_finishEvent_14((FsmEvent_t2133468028 *)NULL);
		__this->set_ease_16((EasingFunction_t524263911 *)NULL);
		__this->set_runningTime_17((0.0f));
		__this->set_lastTime_18((0.0f));
		__this->set_percentage_22((0.0f));
		__this->set_fromFloats_23(((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)0)));
		__this->set_toFloats_24(((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)0)));
		__this->set_resultFloats_25(((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)0)));
		__this->set_finishAction_26((bool)0);
		__this->set_start_27((bool)0);
		__this->set_finished_28((bool)0);
		__this->set_isRunning_29((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EaseFsmAction::OnEnter()
extern "C"  void EaseFsmAction_OnEnter_m2552032439 (EaseFsmAction_t595986710 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	EaseFsmAction_t595986710 * G_B2_0 = NULL;
	EaseFsmAction_t595986710 * G_B1_0 = NULL;
	float G_B5_0 = 0.0f;
	EaseFsmAction_t595986710 * G_B5_1 = NULL;
	EaseFsmAction_t595986710 * G_B4_0 = NULL;
	EaseFsmAction_t595986710 * G_B3_0 = NULL;
	EaseFsmAction_t595986710 * G_B7_0 = NULL;
	EaseFsmAction_t595986710 * G_B6_0 = NULL;
	float G_B8_0 = 0.0f;
	EaseFsmAction_t595986710 * G_B8_1 = NULL;
	{
		__this->set_finished_28((bool)0);
		__this->set_isRunning_29((bool)0);
		EaseFsmAction_SetEasingFunction_m1325706339(__this, /*hidden argument*/NULL);
		__this->set_runningTime_17((0.0f));
		FsmBool_t1075959796 * L_0 = __this->get_reverse_13();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if (!L_1)
		{
			G_B2_0 = __this;
			goto IL_003a;
		}
	}
	{
		G_B5_0 = (0.0f);
		G_B5_1 = G_B1_0;
		goto IL_0059;
	}

IL_003a:
	{
		FsmBool_t1075959796 * L_2 = __this->get_reverse_13();
		NullCheck(L_2);
		bool L_3 = FsmBool_get_Value_m3101329097(L_2, /*hidden argument*/NULL);
		G_B3_0 = G_B2_0;
		if (!L_3)
		{
			G_B4_0 = G_B2_0;
			goto IL_0054;
		}
	}
	{
		G_B5_0 = (1.0f);
		G_B5_1 = G_B3_0;
		goto IL_0059;
	}

IL_0054:
	{
		G_B5_0 = (0.0f);
		G_B5_1 = G_B4_0;
	}

IL_0059:
	{
		NullCheck(G_B5_1);
		G_B5_1->set_percentage_22(G_B5_0);
		__this->set_finishAction_26((bool)0);
		float L_4 = FsmTime_get_RealtimeSinceStartup_m372847495(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_startTime_19(L_4);
		float L_5 = FsmTime_get_RealtimeSinceStartup_m372847495(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_6 = __this->get_startTime_19();
		__this->set_lastTime_18(((float)((float)L_5-(float)L_6)));
		FsmFloat_t2134102846 * L_7 = __this->get_delay_11();
		NullCheck(L_7);
		bool L_8 = NamedVariable_get_IsNone_m281035543(L_7, /*hidden argument*/NULL);
		G_B6_0 = __this;
		if (!L_8)
		{
			G_B7_0 = __this;
			goto IL_009d;
		}
	}
	{
		G_B8_0 = (0.0f);
		G_B8_1 = G_B6_0;
		goto IL_00b1;
	}

IL_009d:
	{
		FsmFloat_t2134102846 * L_9 = __this->get_delay_11();
		NullCheck(L_9);
		float L_10 = FsmFloat_get_Value_m4137923823(L_9, /*hidden argument*/NULL);
		float L_11 = L_10;
		V_0 = L_11;
		__this->set_delayTime_21(L_11);
		float L_12 = V_0;
		G_B8_0 = L_12;
		G_B8_1 = G_B7_0;
	}

IL_00b1:
	{
		NullCheck(G_B8_1);
		G_B8_1->set_delayTime_21(G_B8_0);
		__this->set_start_27((bool)1);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EaseFsmAction::OnExit()
extern "C"  void EaseFsmAction_OnExit_m368337921 (EaseFsmAction_t595986710 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EaseFsmAction::OnUpdate()
extern "C"  void EaseFsmAction_OnUpdate_m937153644 (EaseFsmAction_t595986710 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t G_B13_0 = 0;
	{
		bool L_0 = __this->get_start_27();
		if (!L_0)
		{
			goto IL_00b6;
		}
	}
	{
		bool L_1 = __this->get_isRunning_29();
		if (L_1)
		{
			goto IL_00b6;
		}
	}
	{
		float L_2 = __this->get_delayTime_21();
		if ((!(((float)L_2) >= ((float)(0.0f)))))
		{
			goto IL_008b;
		}
	}
	{
		bool L_3 = __this->get_realTime_15();
		if (!L_3)
		{
			goto IL_0074;
		}
	}
	{
		float L_4 = FsmTime_get_RealtimeSinceStartup_m372847495(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = __this->get_startTime_19();
		float L_6 = __this->get_lastTime_18();
		__this->set_deltaTime_20(((float)((float)((float)((float)L_4-(float)L_5))-(float)L_6)));
		float L_7 = FsmTime_get_RealtimeSinceStartup_m372847495(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_8 = __this->get_startTime_19();
		__this->set_lastTime_18(((float)((float)L_7-(float)L_8)));
		float L_9 = __this->get_delayTime_21();
		float L_10 = __this->get_deltaTime_20();
		__this->set_delayTime_21(((float)((float)L_9-(float)L_10)));
		goto IL_0086;
	}

IL_0074:
	{
		float L_11 = __this->get_delayTime_21();
		float L_12 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_delayTime_21(((float)((float)L_11-(float)L_12)));
	}

IL_0086:
	{
		goto IL_00b6;
	}

IL_008b:
	{
		__this->set_isRunning_29((bool)1);
		__this->set_start_27((bool)0);
		float L_13 = FsmTime_get_RealtimeSinceStartup_m372847495(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_startTime_19(L_13);
		float L_14 = FsmTime_get_RealtimeSinceStartup_m372847495(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_15 = __this->get_startTime_19();
		__this->set_lastTime_18(((float)((float)L_14-(float)L_15)));
	}

IL_00b6:
	{
		bool L_16 = __this->get_isRunning_29();
		if (!L_16)
		{
			goto IL_01db;
		}
	}
	{
		bool L_17 = __this->get_finished_28();
		if (L_17)
		{
			goto IL_01db;
		}
	}
	{
		FsmBool_t1075959796 * L_18 = __this->get_reverse_13();
		NullCheck(L_18);
		bool L_19 = NamedVariable_get_IsNone_m281035543(L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00e2;
		}
	}
	{
		G_B13_0 = 0;
		goto IL_00ed;
	}

IL_00e2:
	{
		FsmBool_t1075959796 * L_20 = __this->get_reverse_13();
		NullCheck(L_20);
		bool L_21 = FsmBool_get_Value_m3101329097(L_20, /*hidden argument*/NULL);
		G_B13_0 = ((int32_t)(L_21));
	}

IL_00ed:
	{
		if (G_B13_0)
		{
			goto IL_0169;
		}
	}
	{
		EaseFsmAction_UpdatePercentage_m268259175(__this, /*hidden argument*/NULL);
		float L_22 = __this->get_percentage_22();
		if ((!(((float)L_22) < ((float)(1.0f)))))
		{
			goto IL_014f;
		}
	}
	{
		V_0 = 0;
		goto IL_013c;
	}

IL_010f:
	{
		SingleU5BU5D_t2316563989* L_23 = __this->get_resultFloats_25();
		int32_t L_24 = V_0;
		EasingFunction_t524263911 * L_25 = __this->get_ease_16();
		SingleU5BU5D_t2316563989* L_26 = __this->get_fromFloats_23();
		int32_t L_27 = V_0;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		int32_t L_28 = L_27;
		float L_29 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		SingleU5BU5D_t2316563989* L_30 = __this->get_toFloats_24();
		int32_t L_31 = V_0;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, L_31);
		int32_t L_32 = L_31;
		float L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		float L_34 = __this->get_percentage_22();
		NullCheck(L_25);
		float L_35 = EasingFunction_Invoke_m2080563457(L_25, L_29, L_33, L_34, /*hidden argument*/NULL);
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(L_24), (float)L_35);
		int32_t L_36 = V_0;
		V_0 = ((int32_t)((int32_t)L_36+(int32_t)1));
	}

IL_013c:
	{
		int32_t L_37 = V_0;
		SingleU5BU5D_t2316563989* L_38 = __this->get_fromFloats_23();
		NullCheck(L_38);
		if ((((int32_t)L_37) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_38)->max_length)))))))
		{
			goto IL_010f;
		}
	}
	{
		goto IL_0164;
	}

IL_014f:
	{
		__this->set_finishAction_26((bool)1);
		__this->set_finished_28((bool)1);
		__this->set_isRunning_29((bool)0);
	}

IL_0164:
	{
		goto IL_01db;
	}

IL_0169:
	{
		EaseFsmAction_UpdatePercentage_m268259175(__this, /*hidden argument*/NULL);
		float L_39 = __this->get_percentage_22();
		if ((!(((float)L_39) > ((float)(0.0f)))))
		{
			goto IL_01c6;
		}
	}
	{
		V_1 = 0;
		goto IL_01b3;
	}

IL_0186:
	{
		SingleU5BU5D_t2316563989* L_40 = __this->get_resultFloats_25();
		int32_t L_41 = V_1;
		EasingFunction_t524263911 * L_42 = __this->get_ease_16();
		SingleU5BU5D_t2316563989* L_43 = __this->get_fromFloats_23();
		int32_t L_44 = V_1;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, L_44);
		int32_t L_45 = L_44;
		float L_46 = (L_43)->GetAt(static_cast<il2cpp_array_size_t>(L_45));
		SingleU5BU5D_t2316563989* L_47 = __this->get_toFloats_24();
		int32_t L_48 = V_1;
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, L_48);
		int32_t L_49 = L_48;
		float L_50 = (L_47)->GetAt(static_cast<il2cpp_array_size_t>(L_49));
		float L_51 = __this->get_percentage_22();
		NullCheck(L_42);
		float L_52 = EasingFunction_Invoke_m2080563457(L_42, L_46, L_50, L_51, /*hidden argument*/NULL);
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, L_41);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(L_41), (float)L_52);
		int32_t L_53 = V_1;
		V_1 = ((int32_t)((int32_t)L_53+(int32_t)1));
	}

IL_01b3:
	{
		int32_t L_54 = V_1;
		SingleU5BU5D_t2316563989* L_55 = __this->get_fromFloats_23();
		NullCheck(L_55);
		if ((((int32_t)L_54) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_55)->max_length)))))))
		{
			goto IL_0186;
		}
	}
	{
		goto IL_01db;
	}

IL_01c6:
	{
		__this->set_finishAction_26((bool)1);
		__this->set_finished_28((bool)1);
		__this->set_isRunning_29((bool)0);
	}

IL_01db:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EaseFsmAction::UpdatePercentage()
extern "C"  void EaseFsmAction_UpdatePercentage_m268259175 (EaseFsmAction_t595986710 * __this, const MethodInfo* method)
{
	int32_t G_B11_0 = 0;
	{
		bool L_0 = __this->get_realTime_15();
		if (!L_0)
		{
			goto IL_0082;
		}
	}
	{
		float L_1 = FsmTime_get_RealtimeSinceStartup_m372847495(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_2 = __this->get_startTime_19();
		float L_3 = __this->get_lastTime_18();
		__this->set_deltaTime_20(((float)((float)((float)((float)L_1-(float)L_2))-(float)L_3)));
		float L_4 = FsmTime_get_RealtimeSinceStartup_m372847495(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = __this->get_startTime_19();
		__this->set_lastTime_18(((float)((float)L_4-(float)L_5)));
		FsmFloat_t2134102846 * L_6 = __this->get_speed_10();
		NullCheck(L_6);
		bool L_7 = NamedVariable_get_IsNone_m281035543(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_006a;
		}
	}
	{
		float L_8 = __this->get_runningTime_17();
		float L_9 = __this->get_deltaTime_20();
		FsmFloat_t2134102846 * L_10 = __this->get_speed_10();
		NullCheck(L_10);
		float L_11 = FsmFloat_get_Value_m4137923823(L_10, /*hidden argument*/NULL);
		__this->set_runningTime_17(((float)((float)L_8+(float)((float)((float)L_9*(float)L_11)))));
		goto IL_007d;
	}

IL_006a:
	{
		float L_12 = __this->get_runningTime_17();
		float L_13 = __this->get_deltaTime_20();
		__this->set_runningTime_17(((float)((float)L_12+(float)L_13)));
	}

IL_007d:
	{
		goto IL_00c7;
	}

IL_0082:
	{
		FsmFloat_t2134102846 * L_14 = __this->get_speed_10();
		NullCheck(L_14);
		bool L_15 = NamedVariable_get_IsNone_m281035543(L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_00b5;
		}
	}
	{
		float L_16 = __this->get_runningTime_17();
		float L_17 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_18 = __this->get_speed_10();
		NullCheck(L_18);
		float L_19 = FsmFloat_get_Value_m4137923823(L_18, /*hidden argument*/NULL);
		__this->set_runningTime_17(((float)((float)L_16+(float)((float)((float)L_17*(float)L_19)))));
		goto IL_00c7;
	}

IL_00b5:
	{
		float L_20 = __this->get_runningTime_17();
		float L_21 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_runningTime_17(((float)((float)L_20+(float)L_21)));
	}

IL_00c7:
	{
		FsmBool_t1075959796 * L_22 = __this->get_reverse_13();
		NullCheck(L_22);
		bool L_23 = NamedVariable_get_IsNone_m281035543(L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00dd;
		}
	}
	{
		G_B11_0 = 0;
		goto IL_00e8;
	}

IL_00dd:
	{
		FsmBool_t1075959796 * L_24 = __this->get_reverse_13();
		NullCheck(L_24);
		bool L_25 = FsmBool_get_Value_m3101329097(L_24, /*hidden argument*/NULL);
		G_B11_0 = ((int32_t)(L_25));
	}

IL_00e8:
	{
		if (!G_B11_0)
		{
			goto IL_0110;
		}
	}
	{
		float L_26 = __this->get_runningTime_17();
		FsmFloat_t2134102846 * L_27 = __this->get_time_9();
		NullCheck(L_27);
		float L_28 = FsmFloat_get_Value_m4137923823(L_27, /*hidden argument*/NULL);
		__this->set_percentage_22(((float)((float)(1.0f)-(float)((float)((float)L_26/(float)L_28)))));
		goto IL_0128;
	}

IL_0110:
	{
		float L_29 = __this->get_runningTime_17();
		FsmFloat_t2134102846 * L_30 = __this->get_time_9();
		NullCheck(L_30);
		float L_31 = FsmFloat_get_Value_m4137923823(L_30, /*hidden argument*/NULL);
		__this->set_percentage_22(((float)((float)L_29/(float)L_31)));
	}

IL_0128:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EaseFsmAction::SetEasingFunction()
extern Il2CppClass* EasingFunction_t524263911_il2cpp_TypeInfo_var;
extern const MethodInfo* EaseFsmAction_easeInQuad_m558969507_MethodInfo_var;
extern const MethodInfo* EaseFsmAction_easeOutQuad_m4049571076_MethodInfo_var;
extern const MethodInfo* EaseFsmAction_easeInOutQuad_m2004719049_MethodInfo_var;
extern const MethodInfo* EaseFsmAction_easeInCubic_m239140756_MethodInfo_var;
extern const MethodInfo* EaseFsmAction_easeOutCubic_m1073606995_MethodInfo_var;
extern const MethodInfo* EaseFsmAction_easeInOutCubic_m2107703598_MethodInfo_var;
extern const MethodInfo* EaseFsmAction_easeInQuart_m3744997887_MethodInfo_var;
extern const MethodInfo* EaseFsmAction_easeOutQuart_m284496830_MethodInfo_var;
extern const MethodInfo* EaseFsmAction_easeInOutQuart_m1318593433_MethodInfo_var;
extern const MethodInfo* EaseFsmAction_easeInQuint_m1081181427_MethodInfo_var;
extern const MethodInfo* EaseFsmAction_easeOutQuint_m1915647666_MethodInfo_var;
extern const MethodInfo* EaseFsmAction_easeInOutQuint_m2949744269_MethodInfo_var;
extern const MethodInfo* EaseFsmAction_easeInSine_m4156630429_MethodInfo_var;
extern const MethodInfo* EaseFsmAction_easeOutSine_m3352264702_MethodInfo_var;
extern const MethodInfo* EaseFsmAction_easeInOutSine_m1307412675_MethodInfo_var;
extern const MethodInfo* EaseFsmAction_easeInExpo_m999423256_MethodInfo_var;
extern const MethodInfo* EaseFsmAction_easeOutExpo_m195057529_MethodInfo_var;
extern const MethodInfo* EaseFsmAction_easeInOutExpo_m2445172798_MethodInfo_var;
extern const MethodInfo* EaseFsmAction_easeInCirc_m2227763411_MethodInfo_var;
extern const MethodInfo* EaseFsmAction_easeOutCirc_m1423397684_MethodInfo_var;
extern const MethodInfo* EaseFsmAction_easeInOutCirc_m3673512953_MethodInfo_var;
extern const MethodInfo* EaseFsmAction_linear_m2867094712_MethodInfo_var;
extern const MethodInfo* EaseFsmAction_spring_m1912221904_MethodInfo_var;
extern const MethodInfo* EaseFsmAction_bounce_m353328501_MethodInfo_var;
extern const MethodInfo* EaseFsmAction_easeInBack_m1267354371_MethodInfo_var;
extern const MethodInfo* EaseFsmAction_easeOutBack_m462988644_MethodInfo_var;
extern const MethodInfo* EaseFsmAction_easeInOutBack_m2713103913_MethodInfo_var;
extern const MethodInfo* EaseFsmAction_elastic_m1445158614_MethodInfo_var;
extern const uint32_t EaseFsmAction_SetEasingFunction_m1325706339_MetadataUsageId;
extern "C"  void EaseFsmAction_SetEasingFunction_m1325706339 (EaseFsmAction_t595986710 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EaseFsmAction_SetEasingFunction_m1325706339_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_easeType_12();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0082;
		}
		if (L_1 == 1)
		{
			goto IL_0099;
		}
		if (L_1 == 2)
		{
			goto IL_00b0;
		}
		if (L_1 == 3)
		{
			goto IL_00c7;
		}
		if (L_1 == 4)
		{
			goto IL_00de;
		}
		if (L_1 == 5)
		{
			goto IL_00f5;
		}
		if (L_1 == 6)
		{
			goto IL_010c;
		}
		if (L_1 == 7)
		{
			goto IL_0123;
		}
		if (L_1 == 8)
		{
			goto IL_013a;
		}
		if (L_1 == 9)
		{
			goto IL_0151;
		}
		if (L_1 == 10)
		{
			goto IL_0168;
		}
		if (L_1 == 11)
		{
			goto IL_017f;
		}
		if (L_1 == 12)
		{
			goto IL_0196;
		}
		if (L_1 == 13)
		{
			goto IL_01ad;
		}
		if (L_1 == 14)
		{
			goto IL_01c4;
		}
		if (L_1 == 15)
		{
			goto IL_01db;
		}
		if (L_1 == 16)
		{
			goto IL_01f2;
		}
		if (L_1 == 17)
		{
			goto IL_0209;
		}
		if (L_1 == 18)
		{
			goto IL_0220;
		}
		if (L_1 == 19)
		{
			goto IL_0237;
		}
		if (L_1 == 20)
		{
			goto IL_024e;
		}
		if (L_1 == 21)
		{
			goto IL_0265;
		}
		if (L_1 == 22)
		{
			goto IL_027c;
		}
		if (L_1 == 23)
		{
			goto IL_0293;
		}
		if (L_1 == 24)
		{
			goto IL_02aa;
		}
		if (L_1 == 25)
		{
			goto IL_02c1;
		}
		if (L_1 == 26)
		{
			goto IL_02d8;
		}
		if (L_1 == 27)
		{
			goto IL_02ef;
		}
	}
	{
		goto IL_0306;
	}

IL_0082:
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)EaseFsmAction_easeInQuad_m558969507_MethodInfo_var);
		EasingFunction_t524263911 * L_3 = (EasingFunction_t524263911 *)il2cpp_codegen_object_new(EasingFunction_t524263911_il2cpp_TypeInfo_var);
		EasingFunction__ctor_m768378126(L_3, __this, L_2, /*hidden argument*/NULL);
		__this->set_ease_16(L_3);
		goto IL_0306;
	}

IL_0099:
	{
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)EaseFsmAction_easeOutQuad_m4049571076_MethodInfo_var);
		EasingFunction_t524263911 * L_5 = (EasingFunction_t524263911 *)il2cpp_codegen_object_new(EasingFunction_t524263911_il2cpp_TypeInfo_var);
		EasingFunction__ctor_m768378126(L_5, __this, L_4, /*hidden argument*/NULL);
		__this->set_ease_16(L_5);
		goto IL_0306;
	}

IL_00b0:
	{
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)EaseFsmAction_easeInOutQuad_m2004719049_MethodInfo_var);
		EasingFunction_t524263911 * L_7 = (EasingFunction_t524263911 *)il2cpp_codegen_object_new(EasingFunction_t524263911_il2cpp_TypeInfo_var);
		EasingFunction__ctor_m768378126(L_7, __this, L_6, /*hidden argument*/NULL);
		__this->set_ease_16(L_7);
		goto IL_0306;
	}

IL_00c7:
	{
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)EaseFsmAction_easeInCubic_m239140756_MethodInfo_var);
		EasingFunction_t524263911 * L_9 = (EasingFunction_t524263911 *)il2cpp_codegen_object_new(EasingFunction_t524263911_il2cpp_TypeInfo_var);
		EasingFunction__ctor_m768378126(L_9, __this, L_8, /*hidden argument*/NULL);
		__this->set_ease_16(L_9);
		goto IL_0306;
	}

IL_00de:
	{
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)EaseFsmAction_easeOutCubic_m1073606995_MethodInfo_var);
		EasingFunction_t524263911 * L_11 = (EasingFunction_t524263911 *)il2cpp_codegen_object_new(EasingFunction_t524263911_il2cpp_TypeInfo_var);
		EasingFunction__ctor_m768378126(L_11, __this, L_10, /*hidden argument*/NULL);
		__this->set_ease_16(L_11);
		goto IL_0306;
	}

IL_00f5:
	{
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)(void*)EaseFsmAction_easeInOutCubic_m2107703598_MethodInfo_var);
		EasingFunction_t524263911 * L_13 = (EasingFunction_t524263911 *)il2cpp_codegen_object_new(EasingFunction_t524263911_il2cpp_TypeInfo_var);
		EasingFunction__ctor_m768378126(L_13, __this, L_12, /*hidden argument*/NULL);
		__this->set_ease_16(L_13);
		goto IL_0306;
	}

IL_010c:
	{
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)EaseFsmAction_easeInQuart_m3744997887_MethodInfo_var);
		EasingFunction_t524263911 * L_15 = (EasingFunction_t524263911 *)il2cpp_codegen_object_new(EasingFunction_t524263911_il2cpp_TypeInfo_var);
		EasingFunction__ctor_m768378126(L_15, __this, L_14, /*hidden argument*/NULL);
		__this->set_ease_16(L_15);
		goto IL_0306;
	}

IL_0123:
	{
		IntPtr_t L_16;
		L_16.set_m_value_0((void*)(void*)EaseFsmAction_easeOutQuart_m284496830_MethodInfo_var);
		EasingFunction_t524263911 * L_17 = (EasingFunction_t524263911 *)il2cpp_codegen_object_new(EasingFunction_t524263911_il2cpp_TypeInfo_var);
		EasingFunction__ctor_m768378126(L_17, __this, L_16, /*hidden argument*/NULL);
		__this->set_ease_16(L_17);
		goto IL_0306;
	}

IL_013a:
	{
		IntPtr_t L_18;
		L_18.set_m_value_0((void*)(void*)EaseFsmAction_easeInOutQuart_m1318593433_MethodInfo_var);
		EasingFunction_t524263911 * L_19 = (EasingFunction_t524263911 *)il2cpp_codegen_object_new(EasingFunction_t524263911_il2cpp_TypeInfo_var);
		EasingFunction__ctor_m768378126(L_19, __this, L_18, /*hidden argument*/NULL);
		__this->set_ease_16(L_19);
		goto IL_0306;
	}

IL_0151:
	{
		IntPtr_t L_20;
		L_20.set_m_value_0((void*)(void*)EaseFsmAction_easeInQuint_m1081181427_MethodInfo_var);
		EasingFunction_t524263911 * L_21 = (EasingFunction_t524263911 *)il2cpp_codegen_object_new(EasingFunction_t524263911_il2cpp_TypeInfo_var);
		EasingFunction__ctor_m768378126(L_21, __this, L_20, /*hidden argument*/NULL);
		__this->set_ease_16(L_21);
		goto IL_0306;
	}

IL_0168:
	{
		IntPtr_t L_22;
		L_22.set_m_value_0((void*)(void*)EaseFsmAction_easeOutQuint_m1915647666_MethodInfo_var);
		EasingFunction_t524263911 * L_23 = (EasingFunction_t524263911 *)il2cpp_codegen_object_new(EasingFunction_t524263911_il2cpp_TypeInfo_var);
		EasingFunction__ctor_m768378126(L_23, __this, L_22, /*hidden argument*/NULL);
		__this->set_ease_16(L_23);
		goto IL_0306;
	}

IL_017f:
	{
		IntPtr_t L_24;
		L_24.set_m_value_0((void*)(void*)EaseFsmAction_easeInOutQuint_m2949744269_MethodInfo_var);
		EasingFunction_t524263911 * L_25 = (EasingFunction_t524263911 *)il2cpp_codegen_object_new(EasingFunction_t524263911_il2cpp_TypeInfo_var);
		EasingFunction__ctor_m768378126(L_25, __this, L_24, /*hidden argument*/NULL);
		__this->set_ease_16(L_25);
		goto IL_0306;
	}

IL_0196:
	{
		IntPtr_t L_26;
		L_26.set_m_value_0((void*)(void*)EaseFsmAction_easeInSine_m4156630429_MethodInfo_var);
		EasingFunction_t524263911 * L_27 = (EasingFunction_t524263911 *)il2cpp_codegen_object_new(EasingFunction_t524263911_il2cpp_TypeInfo_var);
		EasingFunction__ctor_m768378126(L_27, __this, L_26, /*hidden argument*/NULL);
		__this->set_ease_16(L_27);
		goto IL_0306;
	}

IL_01ad:
	{
		IntPtr_t L_28;
		L_28.set_m_value_0((void*)(void*)EaseFsmAction_easeOutSine_m3352264702_MethodInfo_var);
		EasingFunction_t524263911 * L_29 = (EasingFunction_t524263911 *)il2cpp_codegen_object_new(EasingFunction_t524263911_il2cpp_TypeInfo_var);
		EasingFunction__ctor_m768378126(L_29, __this, L_28, /*hidden argument*/NULL);
		__this->set_ease_16(L_29);
		goto IL_0306;
	}

IL_01c4:
	{
		IntPtr_t L_30;
		L_30.set_m_value_0((void*)(void*)EaseFsmAction_easeInOutSine_m1307412675_MethodInfo_var);
		EasingFunction_t524263911 * L_31 = (EasingFunction_t524263911 *)il2cpp_codegen_object_new(EasingFunction_t524263911_il2cpp_TypeInfo_var);
		EasingFunction__ctor_m768378126(L_31, __this, L_30, /*hidden argument*/NULL);
		__this->set_ease_16(L_31);
		goto IL_0306;
	}

IL_01db:
	{
		IntPtr_t L_32;
		L_32.set_m_value_0((void*)(void*)EaseFsmAction_easeInExpo_m999423256_MethodInfo_var);
		EasingFunction_t524263911 * L_33 = (EasingFunction_t524263911 *)il2cpp_codegen_object_new(EasingFunction_t524263911_il2cpp_TypeInfo_var);
		EasingFunction__ctor_m768378126(L_33, __this, L_32, /*hidden argument*/NULL);
		__this->set_ease_16(L_33);
		goto IL_0306;
	}

IL_01f2:
	{
		IntPtr_t L_34;
		L_34.set_m_value_0((void*)(void*)EaseFsmAction_easeOutExpo_m195057529_MethodInfo_var);
		EasingFunction_t524263911 * L_35 = (EasingFunction_t524263911 *)il2cpp_codegen_object_new(EasingFunction_t524263911_il2cpp_TypeInfo_var);
		EasingFunction__ctor_m768378126(L_35, __this, L_34, /*hidden argument*/NULL);
		__this->set_ease_16(L_35);
		goto IL_0306;
	}

IL_0209:
	{
		IntPtr_t L_36;
		L_36.set_m_value_0((void*)(void*)EaseFsmAction_easeInOutExpo_m2445172798_MethodInfo_var);
		EasingFunction_t524263911 * L_37 = (EasingFunction_t524263911 *)il2cpp_codegen_object_new(EasingFunction_t524263911_il2cpp_TypeInfo_var);
		EasingFunction__ctor_m768378126(L_37, __this, L_36, /*hidden argument*/NULL);
		__this->set_ease_16(L_37);
		goto IL_0306;
	}

IL_0220:
	{
		IntPtr_t L_38;
		L_38.set_m_value_0((void*)(void*)EaseFsmAction_easeInCirc_m2227763411_MethodInfo_var);
		EasingFunction_t524263911 * L_39 = (EasingFunction_t524263911 *)il2cpp_codegen_object_new(EasingFunction_t524263911_il2cpp_TypeInfo_var);
		EasingFunction__ctor_m768378126(L_39, __this, L_38, /*hidden argument*/NULL);
		__this->set_ease_16(L_39);
		goto IL_0306;
	}

IL_0237:
	{
		IntPtr_t L_40;
		L_40.set_m_value_0((void*)(void*)EaseFsmAction_easeOutCirc_m1423397684_MethodInfo_var);
		EasingFunction_t524263911 * L_41 = (EasingFunction_t524263911 *)il2cpp_codegen_object_new(EasingFunction_t524263911_il2cpp_TypeInfo_var);
		EasingFunction__ctor_m768378126(L_41, __this, L_40, /*hidden argument*/NULL);
		__this->set_ease_16(L_41);
		goto IL_0306;
	}

IL_024e:
	{
		IntPtr_t L_42;
		L_42.set_m_value_0((void*)(void*)EaseFsmAction_easeInOutCirc_m3673512953_MethodInfo_var);
		EasingFunction_t524263911 * L_43 = (EasingFunction_t524263911 *)il2cpp_codegen_object_new(EasingFunction_t524263911_il2cpp_TypeInfo_var);
		EasingFunction__ctor_m768378126(L_43, __this, L_42, /*hidden argument*/NULL);
		__this->set_ease_16(L_43);
		goto IL_0306;
	}

IL_0265:
	{
		IntPtr_t L_44;
		L_44.set_m_value_0((void*)(void*)EaseFsmAction_linear_m2867094712_MethodInfo_var);
		EasingFunction_t524263911 * L_45 = (EasingFunction_t524263911 *)il2cpp_codegen_object_new(EasingFunction_t524263911_il2cpp_TypeInfo_var);
		EasingFunction__ctor_m768378126(L_45, __this, L_44, /*hidden argument*/NULL);
		__this->set_ease_16(L_45);
		goto IL_0306;
	}

IL_027c:
	{
		IntPtr_t L_46;
		L_46.set_m_value_0((void*)(void*)EaseFsmAction_spring_m1912221904_MethodInfo_var);
		EasingFunction_t524263911 * L_47 = (EasingFunction_t524263911 *)il2cpp_codegen_object_new(EasingFunction_t524263911_il2cpp_TypeInfo_var);
		EasingFunction__ctor_m768378126(L_47, __this, L_46, /*hidden argument*/NULL);
		__this->set_ease_16(L_47);
		goto IL_0306;
	}

IL_0293:
	{
		IntPtr_t L_48;
		L_48.set_m_value_0((void*)(void*)EaseFsmAction_bounce_m353328501_MethodInfo_var);
		EasingFunction_t524263911 * L_49 = (EasingFunction_t524263911 *)il2cpp_codegen_object_new(EasingFunction_t524263911_il2cpp_TypeInfo_var);
		EasingFunction__ctor_m768378126(L_49, __this, L_48, /*hidden argument*/NULL);
		__this->set_ease_16(L_49);
		goto IL_0306;
	}

IL_02aa:
	{
		IntPtr_t L_50;
		L_50.set_m_value_0((void*)(void*)EaseFsmAction_easeInBack_m1267354371_MethodInfo_var);
		EasingFunction_t524263911 * L_51 = (EasingFunction_t524263911 *)il2cpp_codegen_object_new(EasingFunction_t524263911_il2cpp_TypeInfo_var);
		EasingFunction__ctor_m768378126(L_51, __this, L_50, /*hidden argument*/NULL);
		__this->set_ease_16(L_51);
		goto IL_0306;
	}

IL_02c1:
	{
		IntPtr_t L_52;
		L_52.set_m_value_0((void*)(void*)EaseFsmAction_easeOutBack_m462988644_MethodInfo_var);
		EasingFunction_t524263911 * L_53 = (EasingFunction_t524263911 *)il2cpp_codegen_object_new(EasingFunction_t524263911_il2cpp_TypeInfo_var);
		EasingFunction__ctor_m768378126(L_53, __this, L_52, /*hidden argument*/NULL);
		__this->set_ease_16(L_53);
		goto IL_0306;
	}

IL_02d8:
	{
		IntPtr_t L_54;
		L_54.set_m_value_0((void*)(void*)EaseFsmAction_easeInOutBack_m2713103913_MethodInfo_var);
		EasingFunction_t524263911 * L_55 = (EasingFunction_t524263911 *)il2cpp_codegen_object_new(EasingFunction_t524263911_il2cpp_TypeInfo_var);
		EasingFunction__ctor_m768378126(L_55, __this, L_54, /*hidden argument*/NULL);
		__this->set_ease_16(L_55);
		goto IL_0306;
	}

IL_02ef:
	{
		IntPtr_t L_56;
		L_56.set_m_value_0((void*)(void*)EaseFsmAction_elastic_m1445158614_MethodInfo_var);
		EasingFunction_t524263911 * L_57 = (EasingFunction_t524263911 *)il2cpp_codegen_object_new(EasingFunction_t524263911_il2cpp_TypeInfo_var);
		EasingFunction__ctor_m768378126(L_57, __this, L_56, /*hidden argument*/NULL);
		__this->set_ease_16(L_57);
		goto IL_0306;
	}

IL_0306:
	{
		return;
	}
}
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::linear(System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t EaseFsmAction_linear_m2867094712_MetadataUsageId;
extern "C"  float EaseFsmAction_linear_m2867094712 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EaseFsmAction_linear_m2867094712_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___start0;
		float L_1 = ___end1;
		float L_2 = ___value2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::clerp(System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t EaseFsmAction_clerp_m3165956593_MetadataUsageId;
extern "C"  float EaseFsmAction_clerp_m3165956593 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EaseFsmAction_clerp_m3165956593_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		V_0 = (0.0f);
		V_1 = (360.0f);
		float L_0 = V_1;
		float L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_2 = fabsf(((float)((float)((float)((float)L_0-(float)L_1))/(float)(2.0f))));
		V_2 = L_2;
		V_3 = (0.0f);
		V_4 = (0.0f);
		float L_3 = ___end1;
		float L_4 = ___start0;
		float L_5 = V_2;
		if ((!(((float)((float)((float)L_3-(float)L_4))) < ((float)((-L_5))))))
		{
			goto IL_0045;
		}
	}
	{
		float L_6 = V_1;
		float L_7 = ___start0;
		float L_8 = ___end1;
		float L_9 = ___value2;
		V_4 = ((float)((float)((float)((float)((float)((float)L_6-(float)L_7))+(float)L_8))*(float)L_9));
		float L_10 = ___start0;
		float L_11 = V_4;
		V_3 = ((float)((float)L_10+(float)L_11));
		goto IL_006a;
	}

IL_0045:
	{
		float L_12 = ___end1;
		float L_13 = ___start0;
		float L_14 = V_2;
		if ((!(((float)((float)((float)L_12-(float)L_13))) > ((float)L_14))))
		{
			goto IL_0062;
		}
	}
	{
		float L_15 = V_1;
		float L_16 = ___end1;
		float L_17 = ___start0;
		float L_18 = ___value2;
		V_4 = ((float)((float)((-((float)((float)((float)((float)L_15-(float)L_16))+(float)L_17))))*(float)L_18));
		float L_19 = ___start0;
		float L_20 = V_4;
		V_3 = ((float)((float)L_19+(float)L_20));
		goto IL_006a;
	}

IL_0062:
	{
		float L_21 = ___start0;
		float L_22 = ___end1;
		float L_23 = ___start0;
		float L_24 = ___value2;
		V_3 = ((float)((float)L_21+(float)((float)((float)((float)((float)L_22-(float)L_23))*(float)L_24))));
	}

IL_006a:
	{
		float L_25 = V_3;
		return L_25;
	}
}
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::spring(System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t EaseFsmAction_spring_m1912221904_MetadataUsageId;
extern "C"  float EaseFsmAction_spring_m1912221904 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EaseFsmAction_spring_m1912221904_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___value2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m2272733930(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		___value2 = L_1;
		float L_2 = ___value2;
		float L_3 = ___value2;
		float L_4 = ___value2;
		float L_5 = ___value2;
		float L_6 = sinf(((float)((float)((float)((float)L_2*(float)(3.14159274f)))*(float)((float)((float)(0.2f)+(float)((float)((float)((float)((float)((float)((float)(2.5f)*(float)L_3))*(float)L_4))*(float)L_5)))))));
		float L_7 = ___value2;
		float L_8 = powf(((float)((float)(1.0f)-(float)L_7)), (2.2f));
		float L_9 = ___value2;
		float L_10 = ___value2;
		___value2 = ((float)((float)((float)((float)((float)((float)L_6*(float)L_8))+(float)L_9))*(float)((float)((float)(1.0f)+(float)((float)((float)(1.2f)*(float)((float)((float)(1.0f)-(float)L_10))))))));
		float L_11 = ___start0;
		float L_12 = ___end1;
		float L_13 = ___start0;
		float L_14 = ___value2;
		return ((float)((float)L_11+(float)((float)((float)((float)((float)L_12-(float)L_13))*(float)L_14))));
	}
}
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInQuad(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInQuad_m558969507 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method)
{
	{
		float L_0 = ___end1;
		float L_1 = ___start0;
		___end1 = ((float)((float)L_0-(float)L_1));
		float L_2 = ___end1;
		float L_3 = ___value2;
		float L_4 = ___value2;
		float L_5 = ___start0;
		return ((float)((float)((float)((float)((float)((float)L_2*(float)L_3))*(float)L_4))+(float)L_5));
	}
}
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeOutQuad(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeOutQuad_m4049571076 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method)
{
	{
		float L_0 = ___end1;
		float L_1 = ___start0;
		___end1 = ((float)((float)L_0-(float)L_1));
		float L_2 = ___end1;
		float L_3 = ___value2;
		float L_4 = ___value2;
		float L_5 = ___start0;
		return ((float)((float)((float)((float)((float)((float)((-L_2))*(float)L_3))*(float)((float)((float)L_4-(float)(2.0f)))))+(float)L_5));
	}
}
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInOutQuad(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInOutQuad_m2004719049 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method)
{
	{
		float L_0 = ___value2;
		___value2 = ((float)((float)L_0/(float)(0.5f)));
		float L_1 = ___end1;
		float L_2 = ___start0;
		___end1 = ((float)((float)L_1-(float)L_2));
		float L_3 = ___value2;
		if ((!(((float)L_3) < ((float)(1.0f)))))
		{
			goto IL_0027;
		}
	}
	{
		float L_4 = ___end1;
		float L_5 = ___value2;
		float L_6 = ___value2;
		float L_7 = ___start0;
		return ((float)((float)((float)((float)((float)((float)((float)((float)L_4/(float)(2.0f)))*(float)L_5))*(float)L_6))+(float)L_7));
	}

IL_0027:
	{
		float L_8 = ___value2;
		___value2 = ((float)((float)L_8-(float)(1.0f)));
		float L_9 = ___end1;
		float L_10 = ___value2;
		float L_11 = ___value2;
		float L_12 = ___start0;
		return ((float)((float)((float)((float)((float)((float)((-L_9))/(float)(2.0f)))*(float)((float)((float)((float)((float)L_10*(float)((float)((float)L_11-(float)(2.0f)))))-(float)(1.0f)))))+(float)L_12));
	}
}
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInCubic(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInCubic_m239140756 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method)
{
	{
		float L_0 = ___end1;
		float L_1 = ___start0;
		___end1 = ((float)((float)L_0-(float)L_1));
		float L_2 = ___end1;
		float L_3 = ___value2;
		float L_4 = ___value2;
		float L_5 = ___value2;
		float L_6 = ___start0;
		return ((float)((float)((float)((float)((float)((float)((float)((float)L_2*(float)L_3))*(float)L_4))*(float)L_5))+(float)L_6));
	}
}
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeOutCubic(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeOutCubic_m1073606995 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method)
{
	{
		float L_0 = ___value2;
		___value2 = ((float)((float)L_0-(float)(1.0f)));
		float L_1 = ___end1;
		float L_2 = ___start0;
		___end1 = ((float)((float)L_1-(float)L_2));
		float L_3 = ___end1;
		float L_4 = ___value2;
		float L_5 = ___value2;
		float L_6 = ___value2;
		float L_7 = ___start0;
		return ((float)((float)((float)((float)L_3*(float)((float)((float)((float)((float)((float)((float)L_4*(float)L_5))*(float)L_6))+(float)(1.0f)))))+(float)L_7));
	}
}
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInOutCubic(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInOutCubic_m2107703598 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method)
{
	{
		float L_0 = ___value2;
		___value2 = ((float)((float)L_0/(float)(0.5f)));
		float L_1 = ___end1;
		float L_2 = ___start0;
		___end1 = ((float)((float)L_1-(float)L_2));
		float L_3 = ___value2;
		if ((!(((float)L_3) < ((float)(1.0f)))))
		{
			goto IL_0029;
		}
	}
	{
		float L_4 = ___end1;
		float L_5 = ___value2;
		float L_6 = ___value2;
		float L_7 = ___value2;
		float L_8 = ___start0;
		return ((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)L_4/(float)(2.0f)))*(float)L_5))*(float)L_6))*(float)L_7))+(float)L_8));
	}

IL_0029:
	{
		float L_9 = ___value2;
		___value2 = ((float)((float)L_9-(float)(2.0f)));
		float L_10 = ___end1;
		float L_11 = ___value2;
		float L_12 = ___value2;
		float L_13 = ___value2;
		float L_14 = ___start0;
		return ((float)((float)((float)((float)((float)((float)L_10/(float)(2.0f)))*(float)((float)((float)((float)((float)((float)((float)L_11*(float)L_12))*(float)L_13))+(float)(2.0f)))))+(float)L_14));
	}
}
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInQuart(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInQuart_m3744997887 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method)
{
	{
		float L_0 = ___end1;
		float L_1 = ___start0;
		___end1 = ((float)((float)L_0-(float)L_1));
		float L_2 = ___end1;
		float L_3 = ___value2;
		float L_4 = ___value2;
		float L_5 = ___value2;
		float L_6 = ___value2;
		float L_7 = ___start0;
		return ((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)L_2*(float)L_3))*(float)L_4))*(float)L_5))*(float)L_6))+(float)L_7));
	}
}
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeOutQuart(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeOutQuart_m284496830 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method)
{
	{
		float L_0 = ___value2;
		___value2 = ((float)((float)L_0-(float)(1.0f)));
		float L_1 = ___end1;
		float L_2 = ___start0;
		___end1 = ((float)((float)L_1-(float)L_2));
		float L_3 = ___end1;
		float L_4 = ___value2;
		float L_5 = ___value2;
		float L_6 = ___value2;
		float L_7 = ___value2;
		float L_8 = ___start0;
		return ((float)((float)((float)((float)((-L_3))*(float)((float)((float)((float)((float)((float)((float)((float)((float)L_4*(float)L_5))*(float)L_6))*(float)L_7))-(float)(1.0f)))))+(float)L_8));
	}
}
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInOutQuart(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInOutQuart_m1318593433 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method)
{
	{
		float L_0 = ___value2;
		___value2 = ((float)((float)L_0/(float)(0.5f)));
		float L_1 = ___end1;
		float L_2 = ___start0;
		___end1 = ((float)((float)L_1-(float)L_2));
		float L_3 = ___value2;
		if ((!(((float)L_3) < ((float)(1.0f)))))
		{
			goto IL_002b;
		}
	}
	{
		float L_4 = ___end1;
		float L_5 = ___value2;
		float L_6 = ___value2;
		float L_7 = ___value2;
		float L_8 = ___value2;
		float L_9 = ___start0;
		return ((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)L_4/(float)(2.0f)))*(float)L_5))*(float)L_6))*(float)L_7))*(float)L_8))+(float)L_9));
	}

IL_002b:
	{
		float L_10 = ___value2;
		___value2 = ((float)((float)L_10-(float)(2.0f)));
		float L_11 = ___end1;
		float L_12 = ___value2;
		float L_13 = ___value2;
		float L_14 = ___value2;
		float L_15 = ___value2;
		float L_16 = ___start0;
		return ((float)((float)((float)((float)((float)((float)((-L_11))/(float)(2.0f)))*(float)((float)((float)((float)((float)((float)((float)((float)((float)L_12*(float)L_13))*(float)L_14))*(float)L_15))-(float)(2.0f)))))+(float)L_16));
	}
}
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInQuint(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInQuint_m1081181427 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method)
{
	{
		float L_0 = ___end1;
		float L_1 = ___start0;
		___end1 = ((float)((float)L_0-(float)L_1));
		float L_2 = ___end1;
		float L_3 = ___value2;
		float L_4 = ___value2;
		float L_5 = ___value2;
		float L_6 = ___value2;
		float L_7 = ___value2;
		float L_8 = ___start0;
		return ((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)L_2*(float)L_3))*(float)L_4))*(float)L_5))*(float)L_6))*(float)L_7))+(float)L_8));
	}
}
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeOutQuint(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeOutQuint_m1915647666 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method)
{
	{
		float L_0 = ___value2;
		___value2 = ((float)((float)L_0-(float)(1.0f)));
		float L_1 = ___end1;
		float L_2 = ___start0;
		___end1 = ((float)((float)L_1-(float)L_2));
		float L_3 = ___end1;
		float L_4 = ___value2;
		float L_5 = ___value2;
		float L_6 = ___value2;
		float L_7 = ___value2;
		float L_8 = ___value2;
		float L_9 = ___start0;
		return ((float)((float)((float)((float)L_3*(float)((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)L_4*(float)L_5))*(float)L_6))*(float)L_7))*(float)L_8))+(float)(1.0f)))))+(float)L_9));
	}
}
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInOutQuint(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInOutQuint_m2949744269 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method)
{
	{
		float L_0 = ___value2;
		___value2 = ((float)((float)L_0/(float)(0.5f)));
		float L_1 = ___end1;
		float L_2 = ___start0;
		___end1 = ((float)((float)L_1-(float)L_2));
		float L_3 = ___value2;
		if ((!(((float)L_3) < ((float)(1.0f)))))
		{
			goto IL_002d;
		}
	}
	{
		float L_4 = ___end1;
		float L_5 = ___value2;
		float L_6 = ___value2;
		float L_7 = ___value2;
		float L_8 = ___value2;
		float L_9 = ___value2;
		float L_10 = ___start0;
		return ((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)L_4/(float)(2.0f)))*(float)L_5))*(float)L_6))*(float)L_7))*(float)L_8))*(float)L_9))+(float)L_10));
	}

IL_002d:
	{
		float L_11 = ___value2;
		___value2 = ((float)((float)L_11-(float)(2.0f)));
		float L_12 = ___end1;
		float L_13 = ___value2;
		float L_14 = ___value2;
		float L_15 = ___value2;
		float L_16 = ___value2;
		float L_17 = ___value2;
		float L_18 = ___start0;
		return ((float)((float)((float)((float)((float)((float)L_12/(float)(2.0f)))*(float)((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)L_13*(float)L_14))*(float)L_15))*(float)L_16))*(float)L_17))+(float)(2.0f)))))+(float)L_18));
	}
}
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInSine(System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t EaseFsmAction_easeInSine_m4156630429_MetadataUsageId;
extern "C"  float EaseFsmAction_easeInSine_m4156630429 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EaseFsmAction_easeInSine_m4156630429_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___end1;
		float L_1 = ___start0;
		___end1 = ((float)((float)L_0-(float)L_1));
		float L_2 = ___end1;
		float L_3 = ___value2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_4 = cosf(((float)((float)((float)((float)L_3/(float)(1.0f)))*(float)(1.57079637f))));
		float L_5 = ___end1;
		float L_6 = ___start0;
		return ((float)((float)((float)((float)((float)((float)((-L_2))*(float)L_4))+(float)L_5))+(float)L_6));
	}
}
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeOutSine(System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t EaseFsmAction_easeOutSine_m3352264702_MetadataUsageId;
extern "C"  float EaseFsmAction_easeOutSine_m3352264702 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EaseFsmAction_easeOutSine_m3352264702_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___end1;
		float L_1 = ___start0;
		___end1 = ((float)((float)L_0-(float)L_1));
		float L_2 = ___end1;
		float L_3 = ___value2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_4 = sinf(((float)((float)((float)((float)L_3/(float)(1.0f)))*(float)(1.57079637f))));
		float L_5 = ___start0;
		return ((float)((float)((float)((float)L_2*(float)L_4))+(float)L_5));
	}
}
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInOutSine(System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t EaseFsmAction_easeInOutSine_m1307412675_MetadataUsageId;
extern "C"  float EaseFsmAction_easeInOutSine_m1307412675 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EaseFsmAction_easeInOutSine_m1307412675_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___end1;
		float L_1 = ___start0;
		___end1 = ((float)((float)L_0-(float)L_1));
		float L_2 = ___end1;
		float L_3 = ___value2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_4 = cosf(((float)((float)((float)((float)(3.14159274f)*(float)L_3))/(float)(1.0f))));
		float L_5 = ___start0;
		return ((float)((float)((float)((float)((float)((float)((-L_2))/(float)(2.0f)))*(float)((float)((float)L_4-(float)(1.0f)))))+(float)L_5));
	}
}
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInExpo(System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t EaseFsmAction_easeInExpo_m999423256_MetadataUsageId;
extern "C"  float EaseFsmAction_easeInExpo_m999423256 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EaseFsmAction_easeInExpo_m999423256_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___end1;
		float L_1 = ___start0;
		___end1 = ((float)((float)L_0-(float)L_1));
		float L_2 = ___end1;
		float L_3 = ___value2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_4 = powf((2.0f), ((float)((float)(10.0f)*(float)((float)((float)((float)((float)L_3/(float)(1.0f)))-(float)(1.0f))))));
		float L_5 = ___start0;
		return ((float)((float)((float)((float)L_2*(float)L_4))+(float)L_5));
	}
}
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeOutExpo(System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t EaseFsmAction_easeOutExpo_m195057529_MetadataUsageId;
extern "C"  float EaseFsmAction_easeOutExpo_m195057529 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EaseFsmAction_easeOutExpo_m195057529_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___end1;
		float L_1 = ___start0;
		___end1 = ((float)((float)L_0-(float)L_1));
		float L_2 = ___end1;
		float L_3 = ___value2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_4 = powf((2.0f), ((float)((float)((float)((float)(-10.0f)*(float)L_3))/(float)(1.0f))));
		float L_5 = ___start0;
		return ((float)((float)((float)((float)L_2*(float)((float)((float)((-L_4))+(float)(1.0f)))))+(float)L_5));
	}
}
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInOutExpo(System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t EaseFsmAction_easeInOutExpo_m2445172798_MetadataUsageId;
extern "C"  float EaseFsmAction_easeInOutExpo_m2445172798 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EaseFsmAction_easeInOutExpo_m2445172798_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___value2;
		___value2 = ((float)((float)L_0/(float)(0.5f)));
		float L_1 = ___end1;
		float L_2 = ___start0;
		___end1 = ((float)((float)L_1-(float)L_2));
		float L_3 = ___value2;
		if ((!(((float)L_3) < ((float)(1.0f)))))
		{
			goto IL_003b;
		}
	}
	{
		float L_4 = ___end1;
		float L_5 = ___value2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_6 = powf((2.0f), ((float)((float)(10.0f)*(float)((float)((float)L_5-(float)(1.0f))))));
		float L_7 = ___start0;
		return ((float)((float)((float)((float)((float)((float)L_4/(float)(2.0f)))*(float)L_6))+(float)L_7));
	}

IL_003b:
	{
		float L_8 = ___value2;
		___value2 = ((float)((float)L_8-(float)(1.0f)));
		float L_9 = ___end1;
		float L_10 = ___value2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_11 = powf((2.0f), ((float)((float)(-10.0f)*(float)L_10)));
		float L_12 = ___start0;
		return ((float)((float)((float)((float)((float)((float)L_9/(float)(2.0f)))*(float)((float)((float)((-L_11))+(float)(2.0f)))))+(float)L_12));
	}
}
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInCirc(System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t EaseFsmAction_easeInCirc_m2227763411_MetadataUsageId;
extern "C"  float EaseFsmAction_easeInCirc_m2227763411 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EaseFsmAction_easeInCirc_m2227763411_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___end1;
		float L_1 = ___start0;
		___end1 = ((float)((float)L_0-(float)L_1));
		float L_2 = ___end1;
		float L_3 = ___value2;
		float L_4 = ___value2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_5 = sqrtf(((float)((float)(1.0f)-(float)((float)((float)L_3*(float)L_4)))));
		float L_6 = ___start0;
		return ((float)((float)((float)((float)((-L_2))*(float)((float)((float)L_5-(float)(1.0f)))))+(float)L_6));
	}
}
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeOutCirc(System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t EaseFsmAction_easeOutCirc_m1423397684_MetadataUsageId;
extern "C"  float EaseFsmAction_easeOutCirc_m1423397684 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EaseFsmAction_easeOutCirc_m1423397684_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___value2;
		___value2 = ((float)((float)L_0-(float)(1.0f)));
		float L_1 = ___end1;
		float L_2 = ___start0;
		___end1 = ((float)((float)L_1-(float)L_2));
		float L_3 = ___end1;
		float L_4 = ___value2;
		float L_5 = ___value2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_6 = sqrtf(((float)((float)(1.0f)-(float)((float)((float)L_4*(float)L_5)))));
		float L_7 = ___start0;
		return ((float)((float)((float)((float)L_3*(float)L_6))+(float)L_7));
	}
}
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInOutCirc(System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t EaseFsmAction_easeInOutCirc_m3673512953_MetadataUsageId;
extern "C"  float EaseFsmAction_easeInOutCirc_m3673512953 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EaseFsmAction_easeInOutCirc_m3673512953_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___value2;
		___value2 = ((float)((float)L_0/(float)(0.5f)));
		float L_1 = ___end1;
		float L_2 = ___start0;
		___end1 = ((float)((float)L_1-(float)L_2));
		float L_3 = ___value2;
		if ((!(((float)L_3) < ((float)(1.0f)))))
		{
			goto IL_0039;
		}
	}
	{
		float L_4 = ___end1;
		float L_5 = ___value2;
		float L_6 = ___value2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_7 = sqrtf(((float)((float)(1.0f)-(float)((float)((float)L_5*(float)L_6)))));
		float L_8 = ___start0;
		return ((float)((float)((float)((float)((float)((float)((-L_4))/(float)(2.0f)))*(float)((float)((float)L_7-(float)(1.0f)))))+(float)L_8));
	}

IL_0039:
	{
		float L_9 = ___value2;
		___value2 = ((float)((float)L_9-(float)(2.0f)));
		float L_10 = ___end1;
		float L_11 = ___value2;
		float L_12 = ___value2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_13 = sqrtf(((float)((float)(1.0f)-(float)((float)((float)L_11*(float)L_12)))));
		float L_14 = ___start0;
		return ((float)((float)((float)((float)((float)((float)L_10/(float)(2.0f)))*(float)((float)((float)L_13+(float)(1.0f)))))+(float)L_14));
	}
}
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::bounce(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_bounce_m353328501 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method)
{
	{
		float L_0 = ___value2;
		___value2 = ((float)((float)L_0/(float)(1.0f)));
		float L_1 = ___end1;
		float L_2 = ___start0;
		___end1 = ((float)((float)L_1-(float)L_2));
		float L_3 = ___value2;
		if ((!(((float)L_3) < ((float)(0.363636374f)))))
		{
			goto IL_0027;
		}
	}
	{
		float L_4 = ___end1;
		float L_5 = ___value2;
		float L_6 = ___value2;
		float L_7 = ___start0;
		return ((float)((float)((float)((float)L_4*(float)((float)((float)((float)((float)(7.5625f)*(float)L_5))*(float)L_6))))+(float)L_7));
	}

IL_0027:
	{
		float L_8 = ___value2;
		if ((!(((float)L_8) < ((float)(0.727272749f)))))
		{
			goto IL_004f;
		}
	}
	{
		float L_9 = ___value2;
		___value2 = ((float)((float)L_9-(float)(0.545454562f)));
		float L_10 = ___end1;
		float L_11 = ___value2;
		float L_12 = ___value2;
		float L_13 = ___start0;
		return ((float)((float)((float)((float)L_10*(float)((float)((float)((float)((float)((float)((float)(7.5625f)*(float)L_11))*(float)L_12))+(float)(0.75f)))))+(float)L_13));
	}

IL_004f:
	{
		float L_14 = ___value2;
		if ((!(((double)(((double)((double)L_14)))) < ((double)(0.90909090909090906)))))
		{
			goto IL_007c;
		}
	}
	{
		float L_15 = ___value2;
		___value2 = ((float)((float)L_15-(float)(0.8181818f)));
		float L_16 = ___end1;
		float L_17 = ___value2;
		float L_18 = ___value2;
		float L_19 = ___start0;
		return ((float)((float)((float)((float)L_16*(float)((float)((float)((float)((float)((float)((float)(7.5625f)*(float)L_17))*(float)L_18))+(float)(0.9375f)))))+(float)L_19));
	}

IL_007c:
	{
		float L_20 = ___value2;
		___value2 = ((float)((float)L_20-(float)(0.954545438f)));
		float L_21 = ___end1;
		float L_22 = ___value2;
		float L_23 = ___value2;
		float L_24 = ___start0;
		return ((float)((float)((float)((float)L_21*(float)((float)((float)((float)((float)((float)((float)(7.5625f)*(float)L_22))*(float)L_23))+(float)(0.984375f)))))+(float)L_24));
	}
}
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInBack(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInBack_m1267354371 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = ___end1;
		float L_1 = ___start0;
		___end1 = ((float)((float)L_0-(float)L_1));
		float L_2 = ___value2;
		___value2 = ((float)((float)L_2/(float)(1.0f)));
		V_0 = (1.70158f);
		float L_3 = ___end1;
		float L_4 = ___value2;
		float L_5 = ___value2;
		float L_6 = V_0;
		float L_7 = ___value2;
		float L_8 = V_0;
		float L_9 = ___start0;
		return ((float)((float)((float)((float)((float)((float)((float)((float)L_3*(float)L_4))*(float)L_5))*(float)((float)((float)((float)((float)((float)((float)L_6+(float)(1.0f)))*(float)L_7))-(float)L_8))))+(float)L_9));
	}
}
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeOutBack(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeOutBack_m462988644 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (1.70158f);
		float L_0 = ___end1;
		float L_1 = ___start0;
		___end1 = ((float)((float)L_0-(float)L_1));
		float L_2 = ___value2;
		___value2 = ((float)((float)((float)((float)L_2/(float)(1.0f)))-(float)(1.0f)));
		float L_3 = ___end1;
		float L_4 = ___value2;
		float L_5 = ___value2;
		float L_6 = V_0;
		float L_7 = ___value2;
		float L_8 = V_0;
		float L_9 = ___start0;
		return ((float)((float)((float)((float)L_3*(float)((float)((float)((float)((float)((float)((float)L_4*(float)L_5))*(float)((float)((float)((float)((float)((float)((float)L_6+(float)(1.0f)))*(float)L_7))+(float)L_8))))+(float)(1.0f)))))+(float)L_9));
	}
}
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInOutBack(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInOutBack_m2713103913 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (1.70158f);
		float L_0 = ___end1;
		float L_1 = ___start0;
		___end1 = ((float)((float)L_0-(float)L_1));
		float L_2 = ___value2;
		___value2 = ((float)((float)L_2/(float)(0.5f)));
		float L_3 = ___value2;
		if ((!(((float)L_3) < ((float)(1.0f)))))
		{
			goto IL_0041;
		}
	}
	{
		float L_4 = V_0;
		V_0 = ((float)((float)L_4*(float)(1.525f)));
		float L_5 = ___end1;
		float L_6 = ___value2;
		float L_7 = ___value2;
		float L_8 = V_0;
		float L_9 = ___value2;
		float L_10 = V_0;
		float L_11 = ___start0;
		return ((float)((float)((float)((float)((float)((float)L_5/(float)(2.0f)))*(float)((float)((float)((float)((float)L_6*(float)L_7))*(float)((float)((float)((float)((float)((float)((float)L_8+(float)(1.0f)))*(float)L_9))-(float)L_10))))))+(float)L_11));
	}

IL_0041:
	{
		float L_12 = ___value2;
		___value2 = ((float)((float)L_12-(float)(2.0f)));
		float L_13 = V_0;
		V_0 = ((float)((float)L_13*(float)(1.525f)));
		float L_14 = ___end1;
		float L_15 = ___value2;
		float L_16 = ___value2;
		float L_17 = V_0;
		float L_18 = ___value2;
		float L_19 = V_0;
		float L_20 = ___start0;
		return ((float)((float)((float)((float)((float)((float)L_14/(float)(2.0f)))*(float)((float)((float)((float)((float)((float)((float)L_15*(float)L_16))*(float)((float)((float)((float)((float)((float)((float)L_17+(float)(1.0f)))*(float)L_18))+(float)L_19))))+(float)(2.0f)))))+(float)L_20));
	}
}
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::punch(System.Single,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t EaseFsmAction_punch_m1071373240_MetadataUsageId;
extern "C"  float EaseFsmAction_punch_m1071373240 (EaseFsmAction_t595986710 * __this, float ___amplitude0, float ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EaseFsmAction_punch_m1071373240_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (9.0f);
		float L_0 = ___value1;
		if ((!(((float)L_0) == ((float)(0.0f)))))
		{
			goto IL_0017;
		}
	}
	{
		return (0.0f);
	}

IL_0017:
	{
		float L_1 = ___value1;
		if ((!(((float)L_1) == ((float)(1.0f)))))
		{
			goto IL_0028;
		}
	}
	{
		return (0.0f);
	}

IL_0028:
	{
		V_1 = (0.3f);
		float L_2 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_3 = asinf((0.0f));
		V_0 = ((float)((float)((float)((float)L_2/(float)(6.28318548f)))*(float)L_3));
		float L_4 = ___amplitude0;
		float L_5 = ___value1;
		float L_6 = powf((2.0f), ((float)((float)(-10.0f)*(float)L_5)));
		float L_7 = ___value1;
		float L_8 = V_0;
		float L_9 = V_1;
		float L_10 = sinf(((float)((float)((float)((float)((float)((float)((float)((float)L_7*(float)(1.0f)))-(float)L_8))*(float)(6.28318548f)))/(float)L_9)));
		return ((float)((float)((float)((float)L_4*(float)L_6))*(float)L_10));
	}
}
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::elastic(System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t EaseFsmAction_elastic_m1445158614_MetadataUsageId;
extern "C"  float EaseFsmAction_elastic_m1445158614 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EaseFsmAction_elastic_m1445158614_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		float L_0 = ___end1;
		float L_1 = ___start0;
		___end1 = ((float)((float)L_0-(float)L_1));
		V_0 = (1.0f);
		float L_2 = V_0;
		V_1 = ((float)((float)L_2*(float)(0.3f)));
		V_2 = (0.0f);
		V_3 = (0.0f);
		float L_3 = ___value2;
		if ((!(((float)L_3) == ((float)(0.0f)))))
		{
			goto IL_002c;
		}
	}
	{
		float L_4 = ___start0;
		return L_4;
	}

IL_002c:
	{
		float L_5 = ___value2;
		float L_6 = V_0;
		float L_7 = ((float)((float)L_5/(float)L_6));
		___value2 = L_7;
		if ((!(((float)L_7) == ((float)(1.0f)))))
		{
			goto IL_0040;
		}
	}
	{
		float L_8 = ___start0;
		float L_9 = ___end1;
		return ((float)((float)L_8+(float)L_9));
	}

IL_0040:
	{
		float L_10 = V_3;
		if ((((float)L_10) == ((float)(0.0f))))
		{
			goto IL_0057;
		}
	}
	{
		float L_11 = V_3;
		float L_12 = ___end1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_13 = fabsf(L_12);
		if ((!(((float)L_11) < ((float)L_13))))
		{
			goto IL_0066;
		}
	}

IL_0057:
	{
		float L_14 = ___end1;
		V_3 = L_14;
		float L_15 = V_1;
		V_2 = ((float)((float)L_15/(float)(4.0f)));
		goto IL_0077;
	}

IL_0066:
	{
		float L_16 = V_1;
		float L_17 = ___end1;
		float L_18 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_19 = asinf(((float)((float)L_17/(float)L_18)));
		V_2 = ((float)((float)((float)((float)L_16/(float)(6.28318548f)))*(float)L_19));
	}

IL_0077:
	{
		float L_20 = V_3;
		float L_21 = ___value2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_22 = powf((2.0f), ((float)((float)(-10.0f)*(float)L_21)));
		float L_23 = ___value2;
		float L_24 = V_0;
		float L_25 = V_2;
		float L_26 = V_1;
		float L_27 = sinf(((float)((float)((float)((float)((float)((float)((float)((float)L_23*(float)L_24))-(float)L_25))*(float)(6.28318548f)))/(float)L_26)));
		float L_28 = ___end1;
		float L_29 = ___start0;
		return ((float)((float)((float)((float)((float)((float)((float)((float)L_20*(float)L_22))*(float)L_27))+(float)L_28))+(float)L_29));
	}
}
// System.Void HutongGames.PlayMaker.Actions.EaseFsmAction/EasingFunction::.ctor(System.Object,System.IntPtr)
extern "C"  void EasingFunction__ctor_m768378126 (EasingFunction_t524263911 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction/EasingFunction::Invoke(System.Single,System.Single,System.Single)
extern "C"  float EasingFunction_Invoke_m2080563457 (EasingFunction_t524263911 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		EasingFunction_Invoke_m2080563457((EasingFunction_t524263911 *)__this->get_prev_9(),___start0, ___end1, ___value2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (Il2CppObject *, void* __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___start0, ___end1, ___value2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef float (*FunctionPointerType) (void* __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___start0, ___end1, ___value2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  float DelegatePInvokeWrapper_EasingFunction_t524263911 (EasingFunction_t524263911 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method)
{
	typedef float (STDCALL *PInvokeFunc)(float, float, float);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	float returnValue = il2cppPInvokeFunc(___start0, ___end1, ___value2);

	return returnValue;
}
// System.IAsyncResult HutongGames.PlayMaker.Actions.EaseFsmAction/EasingFunction::BeginInvoke(System.Single,System.Single,System.Single,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const uint32_t EasingFunction_BeginInvoke_m2448994828_MetadataUsageId;
extern "C"  Il2CppObject * EasingFunction_BeginInvoke_m2448994828 (EasingFunction_t524263911 * __this, float ___start0, float ___end1, float ___value2, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EasingFunction_BeginInvoke_m2448994828_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(Single_t4291918972_il2cpp_TypeInfo_var, &___start0);
	__d_args[1] = Box(Single_t4291918972_il2cpp_TypeInfo_var, &___end1);
	__d_args[2] = Box(Single_t4291918972_il2cpp_TypeInfo_var, &___value2);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction/EasingFunction::EndInvoke(System.IAsyncResult)
extern "C"  float EasingFunction_EndInvoke_m1531560938 (EasingFunction_t524263911 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(float*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void HutongGames.PlayMaker.Actions.EaseRect::.ctor()
extern "C"  void EaseRect__ctor_m865838092 (EaseRect_t3254272922 * __this, const MethodInfo* method)
{
	{
		EaseFsmAction__ctor_m1487085792(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EaseRect::Reset()
extern "C"  void EaseRect_Reset_m2807238329 (EaseRect_t3254272922 * __this, const MethodInfo* method)
{
	{
		EaseFsmAction_Reset_m3428486029(__this, /*hidden argument*/NULL);
		__this->set_rectVariable_32((FsmRect_t1076426478 *)NULL);
		__this->set_fromValue_30((FsmRect_t1076426478 *)NULL);
		__this->set_toValue_31((FsmRect_t1076426478 *)NULL);
		__this->set_finishInNextStep_33((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EaseRect::OnEnter()
extern Il2CppClass* SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var;
extern const uint32_t EaseRect_OnEnter_m2533446883_MetadataUsageId;
extern "C"  void EaseRect_OnEnter_m2533446883 (EaseRect_t3254272922 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EaseRect_OnEnter_m2533446883_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t4241904616  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t4241904616  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t4241904616  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t4241904616  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Rect_t4241904616  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Rect_t4241904616  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Rect_t4241904616  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		EaseFsmAction_OnEnter_m2552032439(__this, /*hidden argument*/NULL);
		((EaseFsmAction_t595986710 *)__this)->set_fromFloats_23(((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)4)));
		SingleU5BU5D_t2316563989* L_0 = ((EaseFsmAction_t595986710 *)__this)->get_fromFloats_23();
		FsmRect_t1076426478 * L_1 = __this->get_fromValue_30();
		NullCheck(L_1);
		Rect_t4241904616  L_2 = FsmRect_get_Value_m1002500317(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_x_m982385354((&V_0), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)L_3);
		SingleU5BU5D_t2316563989* L_4 = ((EaseFsmAction_t595986710 *)__this)->get_fromFloats_23();
		FsmRect_t1076426478 * L_5 = __this->get_fromValue_30();
		NullCheck(L_5);
		Rect_t4241904616  L_6 = FsmRect_get_Value_m1002500317(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		float L_7 = Rect_get_y_m982386315((&V_1), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (float)L_7);
		SingleU5BU5D_t2316563989* L_8 = ((EaseFsmAction_t595986710 *)__this)->get_fromFloats_23();
		FsmRect_t1076426478 * L_9 = __this->get_fromValue_30();
		NullCheck(L_9);
		Rect_t4241904616  L_10 = FsmRect_get_Value_m1002500317(L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		float L_11 = Rect_get_width_m2824209432((&V_2), /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (float)L_11);
		SingleU5BU5D_t2316563989* L_12 = ((EaseFsmAction_t595986710 *)__this)->get_fromFloats_23();
		FsmRect_t1076426478 * L_13 = __this->get_fromValue_30();
		NullCheck(L_13);
		Rect_t4241904616  L_14 = FsmRect_get_Value_m1002500317(L_13, /*hidden argument*/NULL);
		V_3 = L_14;
		float L_15 = Rect_get_height_m2154960823((&V_3), /*hidden argument*/NULL);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (float)L_15);
		((EaseFsmAction_t595986710 *)__this)->set_toFloats_24(((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)4)));
		SingleU5BU5D_t2316563989* L_16 = ((EaseFsmAction_t595986710 *)__this)->get_toFloats_24();
		FsmRect_t1076426478 * L_17 = __this->get_toValue_31();
		NullCheck(L_17);
		Rect_t4241904616  L_18 = FsmRect_get_Value_m1002500317(L_17, /*hidden argument*/NULL);
		V_4 = L_18;
		float L_19 = Rect_get_x_m982385354((&V_4), /*hidden argument*/NULL);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 0);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)L_19);
		SingleU5BU5D_t2316563989* L_20 = ((EaseFsmAction_t595986710 *)__this)->get_toFloats_24();
		FsmRect_t1076426478 * L_21 = __this->get_toValue_31();
		NullCheck(L_21);
		Rect_t4241904616  L_22 = FsmRect_get_Value_m1002500317(L_21, /*hidden argument*/NULL);
		V_5 = L_22;
		float L_23 = Rect_get_y_m982386315((&V_5), /*hidden argument*/NULL);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 1);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(1), (float)L_23);
		SingleU5BU5D_t2316563989* L_24 = ((EaseFsmAction_t595986710 *)__this)->get_toFloats_24();
		FsmRect_t1076426478 * L_25 = __this->get_toValue_31();
		NullCheck(L_25);
		Rect_t4241904616  L_26 = FsmRect_get_Value_m1002500317(L_25, /*hidden argument*/NULL);
		V_6 = L_26;
		float L_27 = Rect_get_width_m2824209432((&V_6), /*hidden argument*/NULL);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 2);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(2), (float)L_27);
		SingleU5BU5D_t2316563989* L_28 = ((EaseFsmAction_t595986710 *)__this)->get_toFloats_24();
		FsmRect_t1076426478 * L_29 = __this->get_toValue_31();
		NullCheck(L_29);
		Rect_t4241904616  L_30 = FsmRect_get_Value_m1002500317(L_29, /*hidden argument*/NULL);
		V_7 = L_30;
		float L_31 = Rect_get_height_m2154960823((&V_7), /*hidden argument*/NULL);
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 3);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(3), (float)L_31);
		((EaseFsmAction_t595986710 *)__this)->set_resultFloats_25(((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)4)));
		__this->set_finishInNextStep_33((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EaseRect::OnExit()
extern "C"  void EaseRect_OnExit_m2584495701 (EaseRect_t3254272922 * __this, const MethodInfo* method)
{
	{
		EaseFsmAction_OnExit_m368337921(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EaseRect::OnUpdate()
extern "C"  void EaseRect_OnUpdate_m361001408 (EaseRect_t3254272922 * __this, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t4241904616  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t4241904616  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t4241904616  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t4241904616  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Rect_t4241904616  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Rect_t4241904616  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Rect_t4241904616  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Rect_t4241904616  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Rect_t4241904616  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Rect_t4241904616  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Rect_t4241904616  V_11;
	memset(&V_11, 0, sizeof(V_11));
	FsmRect_t1076426478 * G_B11_0 = NULL;
	FsmRect_t1076426478 * G_B10_0 = NULL;
	float G_B14_0 = 0.0f;
	FsmRect_t1076426478 * G_B14_1 = NULL;
	FsmRect_t1076426478 * G_B13_0 = NULL;
	FsmRect_t1076426478 * G_B12_0 = NULL;
	float G_B16_0 = 0.0f;
	FsmRect_t1076426478 * G_B16_1 = NULL;
	float G_B15_0 = 0.0f;
	FsmRect_t1076426478 * G_B15_1 = NULL;
	float G_B19_0 = 0.0f;
	float G_B19_1 = 0.0f;
	FsmRect_t1076426478 * G_B19_2 = NULL;
	float G_B18_0 = 0.0f;
	FsmRect_t1076426478 * G_B18_1 = NULL;
	float G_B17_0 = 0.0f;
	FsmRect_t1076426478 * G_B17_1 = NULL;
	float G_B21_0 = 0.0f;
	float G_B21_1 = 0.0f;
	FsmRect_t1076426478 * G_B21_2 = NULL;
	float G_B20_0 = 0.0f;
	float G_B20_1 = 0.0f;
	FsmRect_t1076426478 * G_B20_2 = NULL;
	float G_B24_0 = 0.0f;
	float G_B24_1 = 0.0f;
	float G_B24_2 = 0.0f;
	FsmRect_t1076426478 * G_B24_3 = NULL;
	float G_B23_0 = 0.0f;
	float G_B23_1 = 0.0f;
	FsmRect_t1076426478 * G_B23_2 = NULL;
	float G_B22_0 = 0.0f;
	float G_B22_1 = 0.0f;
	FsmRect_t1076426478 * G_B22_2 = NULL;
	float G_B26_0 = 0.0f;
	float G_B26_1 = 0.0f;
	float G_B26_2 = 0.0f;
	FsmRect_t1076426478 * G_B26_3 = NULL;
	float G_B25_0 = 0.0f;
	float G_B25_1 = 0.0f;
	float G_B25_2 = 0.0f;
	FsmRect_t1076426478 * G_B25_3 = NULL;
	float G_B29_0 = 0.0f;
	float G_B29_1 = 0.0f;
	float G_B29_2 = 0.0f;
	float G_B29_3 = 0.0f;
	FsmRect_t1076426478 * G_B29_4 = NULL;
	float G_B28_0 = 0.0f;
	float G_B28_1 = 0.0f;
	float G_B28_2 = 0.0f;
	FsmRect_t1076426478 * G_B28_3 = NULL;
	float G_B27_0 = 0.0f;
	float G_B27_1 = 0.0f;
	float G_B27_2 = 0.0f;
	FsmRect_t1076426478 * G_B27_3 = NULL;
	{
		EaseFsmAction_OnUpdate_m937153644(__this, /*hidden argument*/NULL);
		FsmRect_t1076426478 * L_0 = __this->get_rectVariable_32();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0051;
		}
	}
	{
		bool L_2 = ((EaseFsmAction_t595986710 *)__this)->get_isRunning_29();
		if (!L_2)
		{
			goto IL_0051;
		}
	}
	{
		FsmRect_t1076426478 * L_3 = __this->get_rectVariable_32();
		SingleU5BU5D_t2316563989* L_4 = ((EaseFsmAction_t595986710 *)__this)->get_resultFloats_25();
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		int32_t L_5 = 0;
		float L_6 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		SingleU5BU5D_t2316563989* L_7 = ((EaseFsmAction_t595986710 *)__this)->get_resultFloats_25();
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 1);
		int32_t L_8 = 1;
		float L_9 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		SingleU5BU5D_t2316563989* L_10 = ((EaseFsmAction_t595986710 *)__this)->get_resultFloats_25();
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		int32_t L_11 = 2;
		float L_12 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		SingleU5BU5D_t2316563989* L_13 = ((EaseFsmAction_t595986710 *)__this)->get_resultFloats_25();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 3);
		int32_t L_14 = 3;
		float L_15 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		Rect_t4241904616  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Rect__ctor_m3291325233(&L_16, L_6, L_9, L_12, L_15, /*hidden argument*/NULL);
		NullCheck(L_3);
		FsmRect_set_Value_m1159518952(L_3, L_16, /*hidden argument*/NULL);
	}

IL_0051:
	{
		bool L_17 = __this->get_finishInNextStep_33();
		if (!L_17)
		{
			goto IL_007e;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_18 = ((EaseFsmAction_t595986710 *)__this)->get_finishEvent_14();
		if (!L_18)
		{
			goto IL_007e;
		}
	}
	{
		Fsm_t1527112426 * L_19 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_20 = ((EaseFsmAction_t595986710 *)__this)->get_finishEvent_14();
		NullCheck(L_19);
		Fsm_Event_m625948263(L_19, L_20, /*hidden argument*/NULL);
	}

IL_007e:
	{
		bool L_21 = ((EaseFsmAction_t595986710 *)__this)->get_finishAction_26();
		if (!L_21)
		{
			goto IL_024f;
		}
	}
	{
		bool L_22 = __this->get_finishInNextStep_33();
		if (L_22)
		{
			goto IL_024f;
		}
	}
	{
		FsmRect_t1076426478 * L_23 = __this->get_rectVariable_32();
		NullCheck(L_23);
		bool L_24 = NamedVariable_get_IsNone_m281035543(L_23, /*hidden argument*/NULL);
		if (L_24)
		{
			goto IL_0248;
		}
	}
	{
		FsmRect_t1076426478 * L_25 = __this->get_rectVariable_32();
		FsmBool_t1075959796 * L_26 = ((EaseFsmAction_t595986710 *)__this)->get_reverse_13();
		NullCheck(L_26);
		bool L_27 = NamedVariable_get_IsNone_m281035543(L_26, /*hidden argument*/NULL);
		G_B10_0 = L_25;
		if (!L_27)
		{
			G_B11_0 = L_25;
			goto IL_00d2;
		}
	}
	{
		FsmRect_t1076426478 * L_28 = __this->get_toValue_31();
		NullCheck(L_28);
		Rect_t4241904616  L_29 = FsmRect_get_Value_m1002500317(L_28, /*hidden argument*/NULL);
		V_0 = L_29;
		float L_30 = Rect_get_x_m982385354((&V_0), /*hidden argument*/NULL);
		G_B14_0 = L_30;
		G_B14_1 = G_B10_0;
		goto IL_010d;
	}

IL_00d2:
	{
		FsmBool_t1075959796 * L_31 = ((EaseFsmAction_t595986710 *)__this)->get_reverse_13();
		NullCheck(L_31);
		bool L_32 = FsmBool_get_Value_m3101329097(L_31, /*hidden argument*/NULL);
		G_B12_0 = G_B11_0;
		if (!L_32)
		{
			G_B13_0 = G_B11_0;
			goto IL_00fa;
		}
	}
	{
		FsmRect_t1076426478 * L_33 = __this->get_fromValue_30();
		NullCheck(L_33);
		Rect_t4241904616  L_34 = FsmRect_get_Value_m1002500317(L_33, /*hidden argument*/NULL);
		V_1 = L_34;
		float L_35 = Rect_get_x_m982385354((&V_1), /*hidden argument*/NULL);
		G_B14_0 = L_35;
		G_B14_1 = G_B12_0;
		goto IL_010d;
	}

IL_00fa:
	{
		FsmRect_t1076426478 * L_36 = __this->get_toValue_31();
		NullCheck(L_36);
		Rect_t4241904616  L_37 = FsmRect_get_Value_m1002500317(L_36, /*hidden argument*/NULL);
		V_2 = L_37;
		float L_38 = Rect_get_x_m982385354((&V_2), /*hidden argument*/NULL);
		G_B14_0 = L_38;
		G_B14_1 = G_B13_0;
	}

IL_010d:
	{
		FsmBool_t1075959796 * L_39 = ((EaseFsmAction_t595986710 *)__this)->get_reverse_13();
		NullCheck(L_39);
		bool L_40 = NamedVariable_get_IsNone_m281035543(L_39, /*hidden argument*/NULL);
		G_B15_0 = G_B14_0;
		G_B15_1 = G_B14_1;
		if (!L_40)
		{
			G_B16_0 = G_B14_0;
			G_B16_1 = G_B14_1;
			goto IL_0135;
		}
	}
	{
		FsmRect_t1076426478 * L_41 = __this->get_toValue_31();
		NullCheck(L_41);
		Rect_t4241904616  L_42 = FsmRect_get_Value_m1002500317(L_41, /*hidden argument*/NULL);
		V_3 = L_42;
		float L_43 = Rect_get_y_m982386315((&V_3), /*hidden argument*/NULL);
		G_B19_0 = L_43;
		G_B19_1 = G_B15_0;
		G_B19_2 = G_B15_1;
		goto IL_0172;
	}

IL_0135:
	{
		FsmBool_t1075959796 * L_44 = ((EaseFsmAction_t595986710 *)__this)->get_reverse_13();
		NullCheck(L_44);
		bool L_45 = FsmBool_get_Value_m3101329097(L_44, /*hidden argument*/NULL);
		G_B17_0 = G_B16_0;
		G_B17_1 = G_B16_1;
		if (!L_45)
		{
			G_B18_0 = G_B16_0;
			G_B18_1 = G_B16_1;
			goto IL_015e;
		}
	}
	{
		FsmRect_t1076426478 * L_46 = __this->get_fromValue_30();
		NullCheck(L_46);
		Rect_t4241904616  L_47 = FsmRect_get_Value_m1002500317(L_46, /*hidden argument*/NULL);
		V_4 = L_47;
		float L_48 = Rect_get_y_m982386315((&V_4), /*hidden argument*/NULL);
		G_B19_0 = L_48;
		G_B19_1 = G_B17_0;
		G_B19_2 = G_B17_1;
		goto IL_0172;
	}

IL_015e:
	{
		FsmRect_t1076426478 * L_49 = __this->get_toValue_31();
		NullCheck(L_49);
		Rect_t4241904616  L_50 = FsmRect_get_Value_m1002500317(L_49, /*hidden argument*/NULL);
		V_5 = L_50;
		float L_51 = Rect_get_y_m982386315((&V_5), /*hidden argument*/NULL);
		G_B19_0 = L_51;
		G_B19_1 = G_B18_0;
		G_B19_2 = G_B18_1;
	}

IL_0172:
	{
		FsmBool_t1075959796 * L_52 = ((EaseFsmAction_t595986710 *)__this)->get_reverse_13();
		NullCheck(L_52);
		bool L_53 = NamedVariable_get_IsNone_m281035543(L_52, /*hidden argument*/NULL);
		G_B20_0 = G_B19_0;
		G_B20_1 = G_B19_1;
		G_B20_2 = G_B19_2;
		if (!L_53)
		{
			G_B21_0 = G_B19_0;
			G_B21_1 = G_B19_1;
			G_B21_2 = G_B19_2;
			goto IL_019b;
		}
	}
	{
		FsmRect_t1076426478 * L_54 = __this->get_toValue_31();
		NullCheck(L_54);
		Rect_t4241904616  L_55 = FsmRect_get_Value_m1002500317(L_54, /*hidden argument*/NULL);
		V_6 = L_55;
		float L_56 = Rect_get_width_m2824209432((&V_6), /*hidden argument*/NULL);
		G_B24_0 = L_56;
		G_B24_1 = G_B20_0;
		G_B24_2 = G_B20_1;
		G_B24_3 = G_B20_2;
		goto IL_01d8;
	}

IL_019b:
	{
		FsmBool_t1075959796 * L_57 = ((EaseFsmAction_t595986710 *)__this)->get_reverse_13();
		NullCheck(L_57);
		bool L_58 = FsmBool_get_Value_m3101329097(L_57, /*hidden argument*/NULL);
		G_B22_0 = G_B21_0;
		G_B22_1 = G_B21_1;
		G_B22_2 = G_B21_2;
		if (!L_58)
		{
			G_B23_0 = G_B21_0;
			G_B23_1 = G_B21_1;
			G_B23_2 = G_B21_2;
			goto IL_01c4;
		}
	}
	{
		FsmRect_t1076426478 * L_59 = __this->get_fromValue_30();
		NullCheck(L_59);
		Rect_t4241904616  L_60 = FsmRect_get_Value_m1002500317(L_59, /*hidden argument*/NULL);
		V_7 = L_60;
		float L_61 = Rect_get_width_m2824209432((&V_7), /*hidden argument*/NULL);
		G_B24_0 = L_61;
		G_B24_1 = G_B22_0;
		G_B24_2 = G_B22_1;
		G_B24_3 = G_B22_2;
		goto IL_01d8;
	}

IL_01c4:
	{
		FsmRect_t1076426478 * L_62 = __this->get_toValue_31();
		NullCheck(L_62);
		Rect_t4241904616  L_63 = FsmRect_get_Value_m1002500317(L_62, /*hidden argument*/NULL);
		V_8 = L_63;
		float L_64 = Rect_get_width_m2824209432((&V_8), /*hidden argument*/NULL);
		G_B24_0 = L_64;
		G_B24_1 = G_B23_0;
		G_B24_2 = G_B23_1;
		G_B24_3 = G_B23_2;
	}

IL_01d8:
	{
		FsmBool_t1075959796 * L_65 = ((EaseFsmAction_t595986710 *)__this)->get_reverse_13();
		NullCheck(L_65);
		bool L_66 = NamedVariable_get_IsNone_m281035543(L_65, /*hidden argument*/NULL);
		G_B25_0 = G_B24_0;
		G_B25_1 = G_B24_1;
		G_B25_2 = G_B24_2;
		G_B25_3 = G_B24_3;
		if (!L_66)
		{
			G_B26_0 = G_B24_0;
			G_B26_1 = G_B24_1;
			G_B26_2 = G_B24_2;
			G_B26_3 = G_B24_3;
			goto IL_0201;
		}
	}
	{
		FsmRect_t1076426478 * L_67 = __this->get_toValue_31();
		NullCheck(L_67);
		Rect_t4241904616  L_68 = FsmRect_get_Value_m1002500317(L_67, /*hidden argument*/NULL);
		V_9 = L_68;
		float L_69 = Rect_get_height_m2154960823((&V_9), /*hidden argument*/NULL);
		G_B29_0 = L_69;
		G_B29_1 = G_B25_0;
		G_B29_2 = G_B25_1;
		G_B29_3 = G_B25_2;
		G_B29_4 = G_B25_3;
		goto IL_023e;
	}

IL_0201:
	{
		FsmBool_t1075959796 * L_70 = ((EaseFsmAction_t595986710 *)__this)->get_reverse_13();
		NullCheck(L_70);
		bool L_71 = FsmBool_get_Value_m3101329097(L_70, /*hidden argument*/NULL);
		G_B27_0 = G_B26_0;
		G_B27_1 = G_B26_1;
		G_B27_2 = G_B26_2;
		G_B27_3 = G_B26_3;
		if (!L_71)
		{
			G_B28_0 = G_B26_0;
			G_B28_1 = G_B26_1;
			G_B28_2 = G_B26_2;
			G_B28_3 = G_B26_3;
			goto IL_022a;
		}
	}
	{
		FsmRect_t1076426478 * L_72 = __this->get_fromValue_30();
		NullCheck(L_72);
		Rect_t4241904616  L_73 = FsmRect_get_Value_m1002500317(L_72, /*hidden argument*/NULL);
		V_10 = L_73;
		float L_74 = Rect_get_height_m2154960823((&V_10), /*hidden argument*/NULL);
		G_B29_0 = L_74;
		G_B29_1 = G_B27_0;
		G_B29_2 = G_B27_1;
		G_B29_3 = G_B27_2;
		G_B29_4 = G_B27_3;
		goto IL_023e;
	}

IL_022a:
	{
		FsmRect_t1076426478 * L_75 = __this->get_toValue_31();
		NullCheck(L_75);
		Rect_t4241904616  L_76 = FsmRect_get_Value_m1002500317(L_75, /*hidden argument*/NULL);
		V_11 = L_76;
		float L_77 = Rect_get_height_m2154960823((&V_11), /*hidden argument*/NULL);
		G_B29_0 = L_77;
		G_B29_1 = G_B28_0;
		G_B29_2 = G_B28_1;
		G_B29_3 = G_B28_2;
		G_B29_4 = G_B28_3;
	}

IL_023e:
	{
		Rect_t4241904616  L_78;
		memset(&L_78, 0, sizeof(L_78));
		Rect__ctor_m3291325233(&L_78, G_B29_3, G_B29_2, G_B29_1, G_B29_0, /*hidden argument*/NULL);
		NullCheck(G_B29_4);
		FsmRect_set_Value_m1159518952(G_B29_4, L_78, /*hidden argument*/NULL);
	}

IL_0248:
	{
		__this->set_finishInNextStep_33((bool)1);
	}

IL_024f:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EaseVector3::.ctor()
extern "C"  void EaseVector3__ctor_m1457857318 (EaseVector3_t1125521936 * __this, const MethodInfo* method)
{
	{
		EaseFsmAction__ctor_m1487085792(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EaseVector3::Reset()
extern "C"  void EaseVector3_Reset_m3399257555 (EaseVector3_t1125521936 * __this, const MethodInfo* method)
{
	{
		EaseFsmAction_Reset_m3428486029(__this, /*hidden argument*/NULL);
		__this->set_vector3Variable_32((FsmVector3_t533912882 *)NULL);
		__this->set_fromValue_30((FsmVector3_t533912882 *)NULL);
		__this->set_toValue_31((FsmVector3_t533912882 *)NULL);
		__this->set_finishInNextStep_33((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EaseVector3::OnEnter()
extern Il2CppClass* SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var;
extern const uint32_t EaseVector3_OnEnter_m233272701_MetadataUsageId;
extern "C"  void EaseVector3_OnEnter_m233272701 (EaseVector3_t1125521936 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EaseVector3_OnEnter_m233272701_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t4282066566  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t4282066566  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		EaseFsmAction_OnEnter_m2552032439(__this, /*hidden argument*/NULL);
		((EaseFsmAction_t595986710 *)__this)->set_fromFloats_23(((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)3)));
		SingleU5BU5D_t2316563989* L_0 = ((EaseFsmAction_t595986710 *)__this)->get_fromFloats_23();
		FsmVector3_t533912882 * L_1 = __this->get_fromValue_30();
		NullCheck(L_1);
		Vector3_t4282066566  L_2 = FsmVector3_get_Value_m2779135117(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = (&V_0)->get_x_1();
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)L_3);
		SingleU5BU5D_t2316563989* L_4 = ((EaseFsmAction_t595986710 *)__this)->get_fromFloats_23();
		FsmVector3_t533912882 * L_5 = __this->get_fromValue_30();
		NullCheck(L_5);
		Vector3_t4282066566  L_6 = FsmVector3_get_Value_m2779135117(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		float L_7 = (&V_1)->get_y_2();
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (float)L_7);
		SingleU5BU5D_t2316563989* L_8 = ((EaseFsmAction_t595986710 *)__this)->get_fromFloats_23();
		FsmVector3_t533912882 * L_9 = __this->get_fromValue_30();
		NullCheck(L_9);
		Vector3_t4282066566  L_10 = FsmVector3_get_Value_m2779135117(L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		float L_11 = (&V_2)->get_z_3();
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (float)L_11);
		((EaseFsmAction_t595986710 *)__this)->set_toFloats_24(((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)3)));
		SingleU5BU5D_t2316563989* L_12 = ((EaseFsmAction_t595986710 *)__this)->get_toFloats_24();
		FsmVector3_t533912882 * L_13 = __this->get_toValue_31();
		NullCheck(L_13);
		Vector3_t4282066566  L_14 = FsmVector3_get_Value_m2779135117(L_13, /*hidden argument*/NULL);
		V_3 = L_14;
		float L_15 = (&V_3)->get_x_1();
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 0);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)L_15);
		SingleU5BU5D_t2316563989* L_16 = ((EaseFsmAction_t595986710 *)__this)->get_toFloats_24();
		FsmVector3_t533912882 * L_17 = __this->get_toValue_31();
		NullCheck(L_17);
		Vector3_t4282066566  L_18 = FsmVector3_get_Value_m2779135117(L_17, /*hidden argument*/NULL);
		V_4 = L_18;
		float L_19 = (&V_4)->get_y_2();
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 1);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(1), (float)L_19);
		SingleU5BU5D_t2316563989* L_20 = ((EaseFsmAction_t595986710 *)__this)->get_toFloats_24();
		FsmVector3_t533912882 * L_21 = __this->get_toValue_31();
		NullCheck(L_21);
		Vector3_t4282066566  L_22 = FsmVector3_get_Value_m2779135117(L_21, /*hidden argument*/NULL);
		V_5 = L_22;
		float L_23 = (&V_5)->get_z_3();
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 2);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(2), (float)L_23);
		((EaseFsmAction_t595986710 *)__this)->set_resultFloats_25(((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)3)));
		__this->set_finishInNextStep_33((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EaseVector3::OnExit()
extern "C"  void EaseVector3_OnExit_m3757222523 (EaseVector3_t1125521936 * __this, const MethodInfo* method)
{
	{
		EaseFsmAction_OnExit_m368337921(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EaseVector3::OnUpdate()
extern "C"  void EaseVector3_OnUpdate_m2070045798 (EaseVector3_t1125521936 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t4282066566  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t4282066566  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t4282066566  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t4282066566  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t4282066566  V_8;
	memset(&V_8, 0, sizeof(V_8));
	FsmVector3_t533912882 * G_B11_0 = NULL;
	FsmVector3_t533912882 * G_B10_0 = NULL;
	float G_B14_0 = 0.0f;
	FsmVector3_t533912882 * G_B14_1 = NULL;
	FsmVector3_t533912882 * G_B13_0 = NULL;
	FsmVector3_t533912882 * G_B12_0 = NULL;
	float G_B16_0 = 0.0f;
	FsmVector3_t533912882 * G_B16_1 = NULL;
	float G_B15_0 = 0.0f;
	FsmVector3_t533912882 * G_B15_1 = NULL;
	float G_B19_0 = 0.0f;
	float G_B19_1 = 0.0f;
	FsmVector3_t533912882 * G_B19_2 = NULL;
	float G_B18_0 = 0.0f;
	FsmVector3_t533912882 * G_B18_1 = NULL;
	float G_B17_0 = 0.0f;
	FsmVector3_t533912882 * G_B17_1 = NULL;
	float G_B21_0 = 0.0f;
	float G_B21_1 = 0.0f;
	FsmVector3_t533912882 * G_B21_2 = NULL;
	float G_B20_0 = 0.0f;
	float G_B20_1 = 0.0f;
	FsmVector3_t533912882 * G_B20_2 = NULL;
	float G_B24_0 = 0.0f;
	float G_B24_1 = 0.0f;
	float G_B24_2 = 0.0f;
	FsmVector3_t533912882 * G_B24_3 = NULL;
	float G_B23_0 = 0.0f;
	float G_B23_1 = 0.0f;
	FsmVector3_t533912882 * G_B23_2 = NULL;
	float G_B22_0 = 0.0f;
	float G_B22_1 = 0.0f;
	FsmVector3_t533912882 * G_B22_2 = NULL;
	{
		EaseFsmAction_OnUpdate_m937153644(__this, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_0 = __this->get_vector3Variable_32();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0049;
		}
	}
	{
		bool L_2 = ((EaseFsmAction_t595986710 *)__this)->get_isRunning_29();
		if (!L_2)
		{
			goto IL_0049;
		}
	}
	{
		FsmVector3_t533912882 * L_3 = __this->get_vector3Variable_32();
		SingleU5BU5D_t2316563989* L_4 = ((EaseFsmAction_t595986710 *)__this)->get_resultFloats_25();
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		int32_t L_5 = 0;
		float L_6 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		SingleU5BU5D_t2316563989* L_7 = ((EaseFsmAction_t595986710 *)__this)->get_resultFloats_25();
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 1);
		int32_t L_8 = 1;
		float L_9 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		SingleU5BU5D_t2316563989* L_10 = ((EaseFsmAction_t595986710 *)__this)->get_resultFloats_25();
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		int32_t L_11 = 2;
		float L_12 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Vector3_t4282066566  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Vector3__ctor_m2926210380(&L_13, L_6, L_9, L_12, /*hidden argument*/NULL);
		NullCheck(L_3);
		FsmVector3_set_Value_m716982822(L_3, L_13, /*hidden argument*/NULL);
	}

IL_0049:
	{
		bool L_14 = __this->get_finishInNextStep_33();
		if (!L_14)
		{
			goto IL_0076;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_15 = ((EaseFsmAction_t595986710 *)__this)->get_finishEvent_14();
		if (!L_15)
		{
			goto IL_0076;
		}
	}
	{
		Fsm_t1527112426 * L_16 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_17 = ((EaseFsmAction_t595986710 *)__this)->get_finishEvent_14();
		NullCheck(L_16);
		Fsm_Event_m625948263(L_16, L_17, /*hidden argument*/NULL);
	}

IL_0076:
	{
		bool L_18 = ((EaseFsmAction_t595986710 *)__this)->get_finishAction_26();
		if (!L_18)
		{
			goto IL_01e1;
		}
	}
	{
		bool L_19 = __this->get_finishInNextStep_33();
		if (L_19)
		{
			goto IL_01e1;
		}
	}
	{
		FsmVector3_t533912882 * L_20 = __this->get_vector3Variable_32();
		NullCheck(L_20);
		bool L_21 = NamedVariable_get_IsNone_m281035543(L_20, /*hidden argument*/NULL);
		if (L_21)
		{
			goto IL_01da;
		}
	}
	{
		FsmVector3_t533912882 * L_22 = __this->get_vector3Variable_32();
		FsmBool_t1075959796 * L_23 = ((EaseFsmAction_t595986710 *)__this)->get_reverse_13();
		NullCheck(L_23);
		bool L_24 = NamedVariable_get_IsNone_m281035543(L_23, /*hidden argument*/NULL);
		G_B10_0 = L_22;
		if (!L_24)
		{
			G_B11_0 = L_22;
			goto IL_00ca;
		}
	}
	{
		FsmVector3_t533912882 * L_25 = __this->get_toValue_31();
		NullCheck(L_25);
		Vector3_t4282066566  L_26 = FsmVector3_get_Value_m2779135117(L_25, /*hidden argument*/NULL);
		V_0 = L_26;
		float L_27 = (&V_0)->get_x_1();
		G_B14_0 = L_27;
		G_B14_1 = G_B10_0;
		goto IL_0105;
	}

IL_00ca:
	{
		FsmBool_t1075959796 * L_28 = ((EaseFsmAction_t595986710 *)__this)->get_reverse_13();
		NullCheck(L_28);
		bool L_29 = FsmBool_get_Value_m3101329097(L_28, /*hidden argument*/NULL);
		G_B12_0 = G_B11_0;
		if (!L_29)
		{
			G_B13_0 = G_B11_0;
			goto IL_00f2;
		}
	}
	{
		FsmVector3_t533912882 * L_30 = __this->get_fromValue_30();
		NullCheck(L_30);
		Vector3_t4282066566  L_31 = FsmVector3_get_Value_m2779135117(L_30, /*hidden argument*/NULL);
		V_1 = L_31;
		float L_32 = (&V_1)->get_x_1();
		G_B14_0 = L_32;
		G_B14_1 = G_B12_0;
		goto IL_0105;
	}

IL_00f2:
	{
		FsmVector3_t533912882 * L_33 = __this->get_toValue_31();
		NullCheck(L_33);
		Vector3_t4282066566  L_34 = FsmVector3_get_Value_m2779135117(L_33, /*hidden argument*/NULL);
		V_2 = L_34;
		float L_35 = (&V_2)->get_x_1();
		G_B14_0 = L_35;
		G_B14_1 = G_B13_0;
	}

IL_0105:
	{
		FsmBool_t1075959796 * L_36 = ((EaseFsmAction_t595986710 *)__this)->get_reverse_13();
		NullCheck(L_36);
		bool L_37 = NamedVariable_get_IsNone_m281035543(L_36, /*hidden argument*/NULL);
		G_B15_0 = G_B14_0;
		G_B15_1 = G_B14_1;
		if (!L_37)
		{
			G_B16_0 = G_B14_0;
			G_B16_1 = G_B14_1;
			goto IL_012d;
		}
	}
	{
		FsmVector3_t533912882 * L_38 = __this->get_toValue_31();
		NullCheck(L_38);
		Vector3_t4282066566  L_39 = FsmVector3_get_Value_m2779135117(L_38, /*hidden argument*/NULL);
		V_3 = L_39;
		float L_40 = (&V_3)->get_y_2();
		G_B19_0 = L_40;
		G_B19_1 = G_B15_0;
		G_B19_2 = G_B15_1;
		goto IL_016a;
	}

IL_012d:
	{
		FsmBool_t1075959796 * L_41 = ((EaseFsmAction_t595986710 *)__this)->get_reverse_13();
		NullCheck(L_41);
		bool L_42 = FsmBool_get_Value_m3101329097(L_41, /*hidden argument*/NULL);
		G_B17_0 = G_B16_0;
		G_B17_1 = G_B16_1;
		if (!L_42)
		{
			G_B18_0 = G_B16_0;
			G_B18_1 = G_B16_1;
			goto IL_0156;
		}
	}
	{
		FsmVector3_t533912882 * L_43 = __this->get_fromValue_30();
		NullCheck(L_43);
		Vector3_t4282066566  L_44 = FsmVector3_get_Value_m2779135117(L_43, /*hidden argument*/NULL);
		V_4 = L_44;
		float L_45 = (&V_4)->get_y_2();
		G_B19_0 = L_45;
		G_B19_1 = G_B17_0;
		G_B19_2 = G_B17_1;
		goto IL_016a;
	}

IL_0156:
	{
		FsmVector3_t533912882 * L_46 = __this->get_toValue_31();
		NullCheck(L_46);
		Vector3_t4282066566  L_47 = FsmVector3_get_Value_m2779135117(L_46, /*hidden argument*/NULL);
		V_5 = L_47;
		float L_48 = (&V_5)->get_y_2();
		G_B19_0 = L_48;
		G_B19_1 = G_B18_0;
		G_B19_2 = G_B18_1;
	}

IL_016a:
	{
		FsmBool_t1075959796 * L_49 = ((EaseFsmAction_t595986710 *)__this)->get_reverse_13();
		NullCheck(L_49);
		bool L_50 = NamedVariable_get_IsNone_m281035543(L_49, /*hidden argument*/NULL);
		G_B20_0 = G_B19_0;
		G_B20_1 = G_B19_1;
		G_B20_2 = G_B19_2;
		if (!L_50)
		{
			G_B21_0 = G_B19_0;
			G_B21_1 = G_B19_1;
			G_B21_2 = G_B19_2;
			goto IL_0193;
		}
	}
	{
		FsmVector3_t533912882 * L_51 = __this->get_toValue_31();
		NullCheck(L_51);
		Vector3_t4282066566  L_52 = FsmVector3_get_Value_m2779135117(L_51, /*hidden argument*/NULL);
		V_6 = L_52;
		float L_53 = (&V_6)->get_z_3();
		G_B24_0 = L_53;
		G_B24_1 = G_B20_0;
		G_B24_2 = G_B20_1;
		G_B24_3 = G_B20_2;
		goto IL_01d0;
	}

IL_0193:
	{
		FsmBool_t1075959796 * L_54 = ((EaseFsmAction_t595986710 *)__this)->get_reverse_13();
		NullCheck(L_54);
		bool L_55 = FsmBool_get_Value_m3101329097(L_54, /*hidden argument*/NULL);
		G_B22_0 = G_B21_0;
		G_B22_1 = G_B21_1;
		G_B22_2 = G_B21_2;
		if (!L_55)
		{
			G_B23_0 = G_B21_0;
			G_B23_1 = G_B21_1;
			G_B23_2 = G_B21_2;
			goto IL_01bc;
		}
	}
	{
		FsmVector3_t533912882 * L_56 = __this->get_fromValue_30();
		NullCheck(L_56);
		Vector3_t4282066566  L_57 = FsmVector3_get_Value_m2779135117(L_56, /*hidden argument*/NULL);
		V_7 = L_57;
		float L_58 = (&V_7)->get_z_3();
		G_B24_0 = L_58;
		G_B24_1 = G_B22_0;
		G_B24_2 = G_B22_1;
		G_B24_3 = G_B22_2;
		goto IL_01d0;
	}

IL_01bc:
	{
		FsmVector3_t533912882 * L_59 = __this->get_toValue_31();
		NullCheck(L_59);
		Vector3_t4282066566  L_60 = FsmVector3_get_Value_m2779135117(L_59, /*hidden argument*/NULL);
		V_8 = L_60;
		float L_61 = (&V_8)->get_z_3();
		G_B24_0 = L_61;
		G_B24_1 = G_B23_0;
		G_B24_2 = G_B23_1;
		G_B24_3 = G_B23_2;
	}

IL_01d0:
	{
		Vector3_t4282066566  L_62;
		memset(&L_62, 0, sizeof(L_62));
		Vector3__ctor_m2926210380(&L_62, G_B24_2, G_B24_1, G_B24_0, /*hidden argument*/NULL);
		NullCheck(G_B24_3);
		FsmVector3_set_Value_m716982822(G_B24_3, L_62, /*hidden argument*/NULL);
	}

IL_01da:
	{
		__this->set_finishInNextStep_33((bool)1);
	}

IL_01e1:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EnableAnimation::.ctor()
extern "C"  void EnableAnimation__ctor_m3959089607 (EnableAnimation_t3592025679 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EnableAnimation::Reset()
extern "C"  void EnableAnimation_Reset_m1605522548 (EnableAnimation_t3592025679 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_animName_10((FsmString_t952858651 *)NULL);
		FsmBool_t1075959796 * L_0 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_enable_11(L_0);
		FsmBool_t1075959796 * L_1 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_resetOnExit_12(L_1);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EnableAnimation::OnEnter()
extern "C"  void EnableAnimation_OnEnter_m3030783966 (EnableAnimation_t3592025679 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		EnableAnimation_DoEnableAnimation_m3112802771(__this, L_2, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EnableAnimation::DoEnableAnimation(UnityEngine.GameObject)
extern const MethodInfo* GameObject_GetComponent_TisAnimation_t1724966010_m2530801684_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1119642490;
extern const uint32_t EnableAnimation_DoEnableAnimation_m3112802771_MetadataUsageId;
extern "C"  void EnableAnimation_DoEnableAnimation_m3112802771 (EnableAnimation_t3592025679 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnableAnimation_DoEnableAnimation_m3112802771_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Animation_t1724966010 * V_0 = NULL;
	{
		GameObject_t3674682005 * L_0 = ___go0;
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		GameObject_t3674682005 * L_2 = ___go0;
		NullCheck(L_2);
		Animation_t1724966010 * L_3 = GameObject_GetComponent_TisAnimation_t1724966010_m2530801684(L_2, /*hidden argument*/GameObject_GetComponent_TisAnimation_t1724966010_m2530801684_MethodInfo_var);
		V_0 = L_3;
		Animation_t1724966010 * L_4 = V_0;
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002c;
		}
	}
	{
		FsmStateAction_LogError_m3478223492(__this, _stringLiteral1119642490, /*hidden argument*/NULL);
		return;
	}

IL_002c:
	{
		Animation_t1724966010 * L_6 = V_0;
		FsmString_t952858651 * L_7 = __this->get_animName_10();
		NullCheck(L_7);
		String_t* L_8 = FsmString_get_Value_m872383149(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		AnimationState_t3682323633 * L_9 = Animation_get_Item_m2669576386(L_6, L_8, /*hidden argument*/NULL);
		__this->set_anim_13(L_9);
		AnimationState_t3682323633 * L_10 = __this->get_anim_13();
		bool L_11 = TrackedReference_op_Inequality_m2008054821(NULL /*static, unused*/, L_10, (TrackedReference_t2089686725 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_006a;
		}
	}
	{
		AnimationState_t3682323633 * L_12 = __this->get_anim_13();
		FsmBool_t1075959796 * L_13 = __this->get_enable_11();
		NullCheck(L_13);
		bool L_14 = FsmBool_get_Value_m3101329097(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		AnimationState_set_enabled_m1879429625(L_12, L_14, /*hidden argument*/NULL);
	}

IL_006a:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EnableAnimation::OnExit()
extern "C"  void EnableAnimation_OnExit_m3986012154 (EnableAnimation_t3592025679 * __this, const MethodInfo* method)
{
	{
		FsmBool_t1075959796 * L_0 = __this->get_resetOnExit_12();
		NullCheck(L_0);
		bool L_1 = FsmBool_get_Value_m3101329097(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003a;
		}
	}
	{
		AnimationState_t3682323633 * L_2 = __this->get_anim_13();
		bool L_3 = TrackedReference_op_Inequality_m2008054821(NULL /*static, unused*/, L_2, (TrackedReference_t2089686725 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003a;
		}
	}
	{
		AnimationState_t3682323633 * L_4 = __this->get_anim_13();
		FsmBool_t1075959796 * L_5 = __this->get_enable_11();
		NullCheck(L_5);
		bool L_6 = FsmBool_get_Value_m3101329097(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		AnimationState_set_enabled_m1879429625(L_4, (bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_003a:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EnableBehaviour::.ctor()
extern "C"  void EnableBehaviour__ctor_m2264218798 (EnableBehaviour_t2067166088 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EnableBehaviour::Reset()
extern "C"  void EnableBehaviour_Reset_m4205619035 (EnableBehaviour_t2067166088 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_behaviour_10((FsmString_t952858651 *)NULL);
		__this->set_component_11((Component_t3501516275 *)NULL);
		FsmBool_t1075959796 * L_0 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_enable_12(L_0);
		FsmBool_t1075959796 * L_1 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_resetOnExit_13(L_1);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EnableBehaviour::OnEnter()
extern "C"  void EnableBehaviour_OnEnter_m2052541701 (EnableBehaviour_t2067166088 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		EnableBehaviour_DoEnableBehaviour_m1719342387(__this, L_2, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EnableBehaviour::DoEnableBehaviour(UnityEngine.GameObject)
extern Il2CppClass* Behaviour_t200106419_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral32;
extern Il2CppCodeGenString* _stringLiteral2683139273;
extern const uint32_t EnableBehaviour_DoEnableBehaviour_m1719342387_MetadataUsageId;
extern "C"  void EnableBehaviour_DoEnableBehaviour_m1719342387 (EnableBehaviour_t2067166088 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnableBehaviour_DoEnableBehaviour_m1719342387_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = ___go0;
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		Component_t3501516275 * L_2 = __this->get_component_11();
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0034;
		}
	}
	{
		Component_t3501516275 * L_4 = __this->get_component_11();
		__this->set_componentTarget_14(((Behaviour_t200106419 *)IsInstClass(L_4, Behaviour_t200106419_il2cpp_TypeInfo_var)));
		goto IL_0050;
	}

IL_0034:
	{
		GameObject_t3674682005 * L_5 = ___go0;
		FsmString_t952858651 * L_6 = __this->get_behaviour_10();
		NullCheck(L_6);
		String_t* L_7 = FsmString_get_Value_m872383149(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Component_t3501516275 * L_8 = GameObject_GetComponent_m2525409030(L_5, L_7, /*hidden argument*/NULL);
		__this->set_componentTarget_14(((Behaviour_t200106419 *)IsInstClass(L_8, Behaviour_t200106419_il2cpp_TypeInfo_var)));
	}

IL_0050:
	{
		Behaviour_t200106419 * L_9 = __this->get_componentTarget_14();
		bool L_10 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_9, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0088;
		}
	}
	{
		GameObject_t3674682005 * L_11 = ___go0;
		NullCheck(L_11);
		String_t* L_12 = Object_get_name_m3709440845(L_11, /*hidden argument*/NULL);
		FsmString_t952858651 * L_13 = __this->get_behaviour_10();
		NullCheck(L_13);
		String_t* L_14 = FsmString_get_Value_m872383149(L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m2933632197(NULL /*static, unused*/, _stringLiteral32, L_12, _stringLiteral2683139273, L_14, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_15, /*hidden argument*/NULL);
		return;
	}

IL_0088:
	{
		Behaviour_t200106419 * L_16 = __this->get_componentTarget_14();
		FsmBool_t1075959796 * L_17 = __this->get_enable_12();
		NullCheck(L_17);
		bool L_18 = FsmBool_get_Value_m3101329097(L_17, /*hidden argument*/NULL);
		NullCheck(L_16);
		Behaviour_set_enabled_m2046806933(L_16, L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EnableBehaviour::OnExit()
extern "C"  void EnableBehaviour_OnExit_m2984624627 (EnableBehaviour_t2067166088 * __this, const MethodInfo* method)
{
	{
		Behaviour_t200106419 * L_0 = __this->get_componentTarget_14();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FsmBool_t1075959796 * L_2 = __this->get_resetOnExit_13();
		NullCheck(L_2);
		bool L_3 = FsmBool_get_Value_m3101329097(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003b;
		}
	}
	{
		Behaviour_t200106419 * L_4 = __this->get_componentTarget_14();
		FsmBool_t1075959796 * L_5 = __this->get_enable_12();
		NullCheck(L_5);
		bool L_6 = FsmBool_get_Value_m3101329097(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		Behaviour_set_enabled_m2046806933(L_4, (bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_003b:
	{
		return;
	}
}
// System.String HutongGames.PlayMaker.Actions.EnableBehaviour::ErrorCheck()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Behaviour_t200106419_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1876814051;
extern const uint32_t EnableBehaviour_ErrorCheck_m2524918515_MetadataUsageId;
extern "C"  String_t* EnableBehaviour_ErrorCheck_m2524918515 (EnableBehaviour_t2067166088 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnableBehaviour_ErrorCheck_m2524918515_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Behaviour_t200106419 * V_1 = NULL;
	String_t* G_B8_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0054;
		}
	}
	{
		Component_t3501516275 * L_5 = __this->get_component_11();
		bool L_6 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_5, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0054;
		}
	}
	{
		FsmString_t952858651 * L_7 = __this->get_behaviour_10();
		NullCheck(L_7);
		bool L_8 = NamedVariable_get_IsNone_m281035543(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0054;
		}
	}
	{
		FsmString_t952858651 * L_9 = __this->get_behaviour_10();
		NullCheck(L_9);
		String_t* L_10 = FsmString_get_Value_m872383149(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0056;
		}
	}

IL_0054:
	{
		return (String_t*)NULL;
	}

IL_0056:
	{
		GameObject_t3674682005 * L_12 = V_0;
		FsmString_t952858651 * L_13 = __this->get_behaviour_10();
		NullCheck(L_13);
		String_t* L_14 = FsmString_get_Value_m872383149(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		Component_t3501516275 * L_15 = GameObject_GetComponent_m2525409030(L_12, L_14, /*hidden argument*/NULL);
		V_1 = ((Behaviour_t200106419 *)IsInstClass(L_15, Behaviour_t200106419_il2cpp_TypeInfo_var));
		Behaviour_t200106419 * L_16 = V_1;
		bool L_17 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_16, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_007f;
		}
	}
	{
		G_B8_0 = ((String_t*)(NULL));
		goto IL_0084;
	}

IL_007f:
	{
		G_B8_0 = _stringLiteral1876814051;
	}

IL_0084:
	{
		return G_B8_0;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EnableFog::.ctor()
extern "C"  void EnableFog__ctor_m1453658061 (EnableFog_t3773849481 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EnableFog::Reset()
extern "C"  void EnableFog_Reset_m3395058298 (EnableFog_t3773849481 * __this, const MethodInfo* method)
{
	{
		FsmBool_t1075959796 * L_0 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_enableFog_9(L_0);
		__this->set_everyFrame_10((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EnableFog::OnEnter()
extern "C"  void EnableFog_OnEnter_m492754020 (EnableFog_t3773849481 * __this, const MethodInfo* method)
{
	{
		FsmBool_t1075959796 * L_0 = __this->get_enableFog_9();
		NullCheck(L_0);
		bool L_1 = FsmBool_get_Value_m3101329097(L_0, /*hidden argument*/NULL);
		RenderSettings_set_fog_m1757489802(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		bool L_2 = __this->get_everyFrame_10();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EnableFog::OnUpdate()
extern "C"  void EnableFog_OnUpdate_m1524032095 (EnableFog_t3773849481 * __this, const MethodInfo* method)
{
	{
		FsmBool_t1075959796 * L_0 = __this->get_enableFog_9();
		NullCheck(L_0);
		bool L_1 = FsmBool_get_Value_m3101329097(L_0, /*hidden argument*/NULL);
		RenderSettings_set_fog_m1757489802(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EnableFSM::.ctor()
extern "C"  void EnableFSM__ctor_m1043072395 (EnableFSM_t3773848587 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EnableFSM::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t EnableFSM_Reset_m2984472632_MetadataUsageId;
extern "C"  void EnableFSM_Reset_m2984472632 (EnableFSM_t3773848587 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnableFSM_Reset_m2984472632_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_10(L_1);
		FsmBool_t1075959796 * L_2 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_enable_11(L_2);
		FsmBool_t1075959796 * L_3 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_resetOnExit_12(L_3);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EnableFSM::OnEnter()
extern "C"  void EnableFSM_OnEnter_m1056920226 (EnableFSM_t3773848587 * __this, const MethodInfo* method)
{
	{
		EnableFSM_DoEnableFSM_m4022046875(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EnableFSM::DoEnableFSM()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponents_TisPlayMakerFSM_t3799847376_m3603576560_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayMakerFSM_t3799847376_m3107677963_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1341597354;
extern const uint32_t EnableFSM_DoEnableFSM_m4022046875_MetadataUsageId;
extern "C"  void EnableFSM_DoEnableFSM_m4022046875 (EnableFSM_t3773848587 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnableFSM_DoEnableFSM_m4022046875_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	PlayMakerFSMU5BU5D_t191094001* V_1 = NULL;
	PlayMakerFSM_t3799847376 * V_2 = NULL;
	PlayMakerFSMU5BU5D_t191094001* V_3 = NULL;
	int32_t V_4 = 0;
	GameObject_t3674682005 * G_B3_0 = NULL;
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_9();
		NullCheck(L_0);
		int32_t L_1 = FsmOwnerDefault_get_OwnerOption_m3357910390(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001b;
		}
	}
	{
		GameObject_t3674682005 * L_2 = FsmStateAction_get_Owner_m1855633209(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_002b;
	}

IL_001b:
	{
		FsmOwnerDefault_t251897112 * L_3 = __this->get_gameObject_9();
		NullCheck(L_3);
		FsmGameObject_t1697147867 * L_4 = FsmOwnerDefault_get_GameObject_m3249227945(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_t3674682005 * L_5 = FsmGameObject_get_Value_m673294275(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
	}

IL_002b:
	{
		V_0 = G_B3_0;
		GameObject_t3674682005 * L_6 = V_0;
		bool L_7 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_6, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0039;
		}
	}
	{
		return;
	}

IL_0039:
	{
		FsmString_t952858651 * L_8 = __this->get_fsmName_10();
		NullCheck(L_8);
		String_t* L_9 = FsmString_get_Value_m872383149(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_00a0;
		}
	}
	{
		GameObject_t3674682005 * L_11 = V_0;
		NullCheck(L_11);
		PlayMakerFSMU5BU5D_t191094001* L_12 = GameObject_GetComponents_TisPlayMakerFSM_t3799847376_m3603576560(L_11, /*hidden argument*/GameObject_GetComponents_TisPlayMakerFSM_t3799847376_m3603576560_MethodInfo_var);
		V_1 = L_12;
		PlayMakerFSMU5BU5D_t191094001* L_13 = V_1;
		V_3 = L_13;
		V_4 = 0;
		goto IL_0091;
	}

IL_005f:
	{
		PlayMakerFSMU5BU5D_t191094001* L_14 = V_3;
		int32_t L_15 = V_4;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		PlayMakerFSM_t3799847376 * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		V_2 = L_17;
		PlayMakerFSM_t3799847376 * L_18 = V_2;
		NullCheck(L_18);
		String_t* L_19 = PlayMakerFSM_get_FsmName_m1703792682(L_18, /*hidden argument*/NULL);
		FsmString_t952858651 * L_20 = __this->get_fsmName_10();
		NullCheck(L_20);
		String_t* L_21 = FsmString_get_Value_m872383149(L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_22 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_19, L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_008b;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_23 = V_2;
		__this->set_fsmComponent_13(L_23);
		goto IL_009b;
	}

IL_008b:
	{
		int32_t L_24 = V_4;
		V_4 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_0091:
	{
		int32_t L_25 = V_4;
		PlayMakerFSMU5BU5D_t191094001* L_26 = V_3;
		NullCheck(L_26);
		if ((((int32_t)L_25) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_26)->max_length)))))))
		{
			goto IL_005f;
		}
	}

IL_009b:
	{
		goto IL_00ac;
	}

IL_00a0:
	{
		GameObject_t3674682005 * L_27 = V_0;
		NullCheck(L_27);
		PlayMakerFSM_t3799847376 * L_28 = GameObject_GetComponent_TisPlayMakerFSM_t3799847376_m3107677963(L_27, /*hidden argument*/GameObject_GetComponent_TisPlayMakerFSM_t3799847376_m3107677963_MethodInfo_var);
		__this->set_fsmComponent_13(L_28);
	}

IL_00ac:
	{
		PlayMakerFSM_t3799847376 * L_29 = __this->get_fsmComponent_13();
		bool L_30 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_29, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_00c9;
		}
	}
	{
		FsmStateAction_LogError_m3478223492(__this, _stringLiteral1341597354, /*hidden argument*/NULL);
		return;
	}

IL_00c9:
	{
		PlayMakerFSM_t3799847376 * L_31 = __this->get_fsmComponent_13();
		FsmBool_t1075959796 * L_32 = __this->get_enable_11();
		NullCheck(L_32);
		bool L_33 = FsmBool_get_Value_m3101329097(L_32, /*hidden argument*/NULL);
		NullCheck(L_31);
		Behaviour_set_enabled_m2046806933(L_31, L_33, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EnableFSM::OnExit()
extern "C"  void EnableFSM_OnExit_m3783791798 (EnableFSM_t3773848587 * __this, const MethodInfo* method)
{
	{
		PlayMakerFSM_t3799847376 * L_0 = __this->get_fsmComponent_13();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FsmBool_t1075959796 * L_2 = __this->get_resetOnExit_12();
		NullCheck(L_2);
		bool L_3 = FsmBool_get_Value_m3101329097(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003b;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_4 = __this->get_fsmComponent_13();
		FsmBool_t1075959796 * L_5 = __this->get_enable_11();
		NullCheck(L_5);
		bool L_6 = FsmBool_get_Value_m3101329097(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		Behaviour_set_enabled_m2046806933(L_4, (bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_003b:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EnableGUI::.ctor()
extern "C"  void EnableGUI__ctor_m2659273712 (EnableGUI_t3773849606 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EnableGUI::Reset()
extern "C"  void EnableGUI_Reset_m305706653 (EnableGUI_t3773849606 * __this, const MethodInfo* method)
{
	{
		FsmBool_t1075959796 * L_0 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_enableGUI_9(L_0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.EnableGUI::OnEnter()
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern const uint32_t EnableGUI_OnEnter_m3743192007_MetadataUsageId;
extern "C"  void EnableGUI_OnEnter_m3743192007 (EnableGUI_t3773849606 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnableGUI_OnEnter_m3743192007_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		PlayMakerGUI_t3799848395 * L_0 = PlayMakerGUI_get_Instance_m1734509702(NULL /*static, unused*/, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_1 = __this->get_enableGUI_9();
		NullCheck(L_1);
		bool L_2 = FsmBool_get_Value_m3101329097(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Behaviour_set_enabled_m2046806933(L_0, L_2, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.Explosion::.ctor()
extern "C"  void Explosion__ctor_m2742373939 (Explosion_t444282467 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.Explosion::Reset()
extern "C"  void Explosion_Reset_m388806880 (Explosion_t444282467 * __this, const MethodInfo* method)
{
	{
		__this->set_center_9((FsmVector3_t533912882 *)NULL);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_upwardsModifier_12(L_0);
		__this->set_forceMode_13(0);
		__this->set_everyFrame_17((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.Explosion::Awake()
extern "C"  void Explosion_Awake_m2979979158 (Explosion_t444282467 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Fsm_set_HandleFixedUpdate_m3395950082(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.Explosion::OnEnter()
extern "C"  void Explosion_OnEnter_m1998131530 (Explosion_t444282467 * __this, const MethodInfo* method)
{
	{
		Explosion_DoExplosion_m4098056091(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_17();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.Explosion::OnFixedUpdate()
extern "C"  void Explosion_OnFixedUpdate_m861276623 (Explosion_t444282467 * __this, const MethodInfo* method)
{
	{
		Explosion_DoExplosion_m4098056091(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.Explosion::DoExplosion()
extern const MethodInfo* GameObject_GetComponent_TisRigidbody_t3346577219_m1334743405_MethodInfo_var;
extern const uint32_t Explosion_DoExplosion_m4098056091_MetadataUsageId;
extern "C"  void Explosion_DoExplosion_m4098056091 (Explosion_t444282467 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Explosion_DoExplosion_m4098056091_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ColliderU5BU5D_t2697150633* V_0 = NULL;
	Collider_t2939674232 * V_1 = NULL;
	ColliderU5BU5D_t2697150633* V_2 = NULL;
	int32_t V_3 = 0;
	Rigidbody_t3346577219 * V_4 = NULL;
	{
		FsmVector3_t533912882 * L_0 = __this->get_center_9();
		NullCheck(L_0);
		Vector3_t4282066566  L_1 = FsmVector3_get_Value_m2779135117(L_0, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_2 = __this->get_radius_11();
		NullCheck(L_2);
		float L_3 = FsmFloat_get_Value_m4137923823(L_2, /*hidden argument*/NULL);
		ColliderU5BU5D_t2697150633* L_4 = Physics_OverlapSphere_m359079608(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		ColliderU5BU5D_t2697150633* L_5 = V_0;
		V_2 = L_5;
		V_3 = 0;
		goto IL_0091;
	}

IL_0025:
	{
		ColliderU5BU5D_t2697150633* L_6 = V_2;
		int32_t L_7 = V_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		Collider_t2939674232 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = L_9;
		Collider_t2939674232 * L_10 = V_1;
		NullCheck(L_10);
		GameObject_t3674682005 * L_11 = Component_get_gameObject_m1170635899(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Rigidbody_t3346577219 * L_12 = GameObject_GetComponent_TisRigidbody_t3346577219_m1334743405(L_11, /*hidden argument*/GameObject_GetComponent_TisRigidbody_t3346577219_m1334743405_MethodInfo_var);
		V_4 = L_12;
		Rigidbody_t3346577219 * L_13 = V_4;
		bool L_14 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_13, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_008d;
		}
	}
	{
		Collider_t2939674232 * L_15 = V_1;
		NullCheck(L_15);
		GameObject_t3674682005 * L_16 = Component_get_gameObject_m1170635899(L_15, /*hidden argument*/NULL);
		bool L_17 = Explosion_ShouldApplyForce_m776029029(__this, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_008d;
		}
	}
	{
		Rigidbody_t3346577219 * L_18 = V_4;
		FsmFloat_t2134102846 * L_19 = __this->get_force_10();
		NullCheck(L_19);
		float L_20 = FsmFloat_get_Value_m4137923823(L_19, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_21 = __this->get_center_9();
		NullCheck(L_21);
		Vector3_t4282066566  L_22 = FsmVector3_get_Value_m2779135117(L_21, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_23 = __this->get_radius_11();
		NullCheck(L_23);
		float L_24 = FsmFloat_get_Value_m4137923823(L_23, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_25 = __this->get_upwardsModifier_12();
		NullCheck(L_25);
		float L_26 = FsmFloat_get_Value_m4137923823(L_25, /*hidden argument*/NULL);
		int32_t L_27 = __this->get_forceMode_13();
		NullCheck(L_18);
		Rigidbody_AddExplosionForce_m196999228(L_18, L_20, L_22, L_24, L_26, L_27, /*hidden argument*/NULL);
	}

IL_008d:
	{
		int32_t L_28 = V_3;
		V_3 = ((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_0091:
	{
		int32_t L_29 = V_3;
		ColliderU5BU5D_t2697150633* L_30 = V_2;
		NullCheck(L_30);
		if ((((int32_t)L_29) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_30)->max_length)))))))
		{
			goto IL_0025;
		}
	}
	{
		return;
	}
}
// System.Boolean HutongGames.PlayMaker.Actions.Explosion::ShouldApplyForce(UnityEngine.GameObject)
extern "C"  bool Explosion_ShouldApplyForce_m776029029 (Explosion_t444282467 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmIntU5BU5D_t1976821196* L_0 = __this->get_layerMask_15();
		FsmBool_t1075959796 * L_1 = __this->get_invertMask_16();
		NullCheck(L_1);
		bool L_2 = FsmBool_get_Value_m3101329097(L_1, /*hidden argument*/NULL);
		int32_t L_3 = ActionHelpers_LayerArrayToLayerMask_m3090395306(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t3674682005 * L_4 = ___go0;
		NullCheck(L_4);
		int32_t L_5 = GameObject_get_layer_m1648550306(L_4, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		return (bool)((((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)31)))))&(int32_t)L_6))) > ((int32_t)0))? 1 : 0);
	}
}
// System.Void HutongGames.PlayMaker.Actions.FindChild::.ctor()
extern "C"  void FindChild__ctor_m3234311717 (FindChild_t514440753 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FindChild::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t FindChild_Reset_m880744658_MetadataUsageId;
extern "C"  void FindChild_Reset_m880744658 (FindChild_t514440753 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FindChild_Reset_m880744658_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_childName_10(L_1);
		__this->set_storeResult_11((FsmGameObject_t1697147867 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FindChild::OnEnter()
extern "C"  void FindChild_OnEnter_m2303933628 (FindChild_t514440753 * __this, const MethodInfo* method)
{
	{
		FindChild_DoFindChild_m1885490779(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FindChild::DoFindChild()
extern "C"  void FindChild_DoFindChild_m1885490779 (FindChild_t514440753 * __this, const MethodInfo* method)
{
	GameObject_t3674682005 * V_0 = NULL;
	Transform_t1659122786 * V_1 = NULL;
	FsmGameObject_t1697147867 * G_B4_0 = NULL;
	FsmGameObject_t1697147867 * G_B3_0 = NULL;
	GameObject_t3674682005 * G_B5_0 = NULL;
	FsmGameObject_t1697147867 * G_B5_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Transform_t1659122786 * L_6 = GameObject_get_transform_m1278640159(L_5, /*hidden argument*/NULL);
		FsmString_t952858651 * L_7 = __this->get_childName_10();
		NullCheck(L_7);
		String_t* L_8 = FsmString_get_Value_m872383149(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_t1659122786 * L_9 = Transform_FindChild_m2149912886(L_6, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		FsmGameObject_t1697147867 * L_10 = __this->get_storeResult_11();
		Transform_t1659122786 * L_11 = V_1;
		bool L_12 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_11, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		G_B3_0 = L_10;
		if (!L_12)
		{
			G_B4_0 = L_10;
			goto IL_0053;
		}
	}
	{
		Transform_t1659122786 * L_13 = V_1;
		NullCheck(L_13);
		GameObject_t3674682005 * L_14 = Component_get_gameObject_m1170635899(L_13, /*hidden argument*/NULL);
		G_B5_0 = L_14;
		G_B5_1 = G_B3_0;
		goto IL_0054;
	}

IL_0053:
	{
		G_B5_0 = ((GameObject_t3674682005 *)(NULL));
		G_B5_1 = G_B4_0;
	}

IL_0054:
	{
		NullCheck(G_B5_1);
		FsmGameObject_set_Value_m297051598(G_B5_1, G_B5_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FindClosest::.ctor()
extern "C"  void FindClosest__ctor_m838832648 (FindClosest_t1284703470 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FindClosest::Reset()
extern Il2CppCodeGenString* _stringLiteral69913957;
extern const uint32_t FindClosest_Reset_m2780232885_MetadataUsageId;
extern "C"  void FindClosest_Reset_m2780232885 (FindClosest_t1284703470 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FindClosest_Reset_m2780232885_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		FsmString_t952858651 * L_0 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, _stringLiteral69913957, /*hidden argument*/NULL);
		__this->set_withTag_10(L_0);
		FsmBool_t1075959796 * L_1 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_ignoreOwner_11(L_1);
		FsmBool_t1075959796 * L_2 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_mustBeVisible_12(L_2);
		__this->set_storeObject_13((FsmGameObject_t1697147867 *)NULL);
		__this->set_storeDistance_14((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_15((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FindClosest::OnEnter()
extern "C"  void FindClosest_OnEnter_m2351018975 (FindClosest_t1284703470 * __this, const MethodInfo* method)
{
	{
		FindClosest_DoFindClosest_m996722107(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FindClosest::OnUpdate()
extern "C"  void FindClosest_OnUpdate_m3295670852 (FindClosest_t1284703470 * __this, const MethodInfo* method)
{
	{
		FindClosest_DoFindClosest_m996722107(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FindClosest::DoFindClosest()
extern const Il2CppType* GameObject_t3674682005_0_0_0_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObjectU5BU5D_t2662109048_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral69913957;
extern const uint32_t FindClosest_DoFindClosest_m996722107_MetadataUsageId;
extern "C"  void FindClosest_DoFindClosest_m996722107 (FindClosest_t1284703470 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FindClosest_DoFindClosest_m996722107_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	GameObjectU5BU5D_t2662109048* V_1 = NULL;
	GameObject_t3674682005 * V_2 = NULL;
	float V_3 = 0.0f;
	GameObject_t3674682005 * V_4 = NULL;
	GameObjectU5BU5D_t2662109048* V_5 = NULL;
	int32_t V_6 = 0;
	float V_7 = 0.0f;
	Vector3_t4282066566  V_8;
	memset(&V_8, 0, sizeof(V_8));
	GameObject_t3674682005 * G_B3_0 = NULL;
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_9();
		NullCheck(L_0);
		int32_t L_1 = FsmOwnerDefault_get_OwnerOption_m3357910390(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001b;
		}
	}
	{
		GameObject_t3674682005 * L_2 = FsmStateAction_get_Owner_m1855633209(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_002b;
	}

IL_001b:
	{
		FsmOwnerDefault_t251897112 * L_3 = __this->get_gameObject_9();
		NullCheck(L_3);
		FsmGameObject_t1697147867 * L_4 = FsmOwnerDefault_get_GameObject_m3249227945(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_t3674682005 * L_5 = FsmGameObject_get_Value_m673294275(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
	}

IL_002b:
	{
		V_0 = G_B3_0;
		FsmString_t952858651 * L_6 = __this->get_withTag_10();
		NullCheck(L_6);
		String_t* L_7 = FsmString_get_Value_m872383149(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_005b;
		}
	}
	{
		FsmString_t952858651 * L_9 = __this->get_withTag_10();
		NullCheck(L_9);
		String_t* L_10 = FsmString_get_Value_m872383149(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_10, _stringLiteral69913957, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0075;
		}
	}

IL_005b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_12 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GameObject_t3674682005_0_0_0_var), /*hidden argument*/NULL);
		ObjectU5BU5D_t1015136018* L_13 = Object_FindObjectsOfType_m975740280(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		V_1 = ((GameObjectU5BU5D_t2662109048*)Castclass(L_13, GameObjectU5BU5D_t2662109048_il2cpp_TypeInfo_var));
		goto IL_0086;
	}

IL_0075:
	{
		FsmString_t952858651 * L_14 = __this->get_withTag_10();
		NullCheck(L_14);
		String_t* L_15 = FsmString_get_Value_m872383149(L_14, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_16 = GameObject_FindGameObjectsWithTag_m3058873418(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		V_1 = L_16;
	}

IL_0086:
	{
		V_2 = (GameObject_t3674682005 *)NULL;
		V_3 = (std::numeric_limits<float>::infinity());
		GameObjectU5BU5D_t2662109048* L_17 = V_1;
		V_5 = L_17;
		V_6 = 0;
		goto IL_0123;
	}

IL_0099:
	{
		GameObjectU5BU5D_t2662109048* L_18 = V_5;
		int32_t L_19 = V_6;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		int32_t L_20 = L_19;
		GameObject_t3674682005 * L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		V_4 = L_21;
		FsmBool_t1075959796 * L_22 = __this->get_ignoreOwner_11();
		NullCheck(L_22);
		bool L_23 = FsmBool_get_Value_m3101329097(L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00c7;
		}
	}
	{
		GameObject_t3674682005 * L_24 = V_4;
		GameObject_t3674682005 * L_25 = FsmStateAction_get_Owner_m1855633209(__this, /*hidden argument*/NULL);
		bool L_26 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00c7;
		}
	}
	{
		goto IL_011d;
	}

IL_00c7:
	{
		FsmBool_t1075959796 * L_27 = __this->get_mustBeVisible_12();
		NullCheck(L_27);
		bool L_28 = FsmBool_get_Value_m3101329097(L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00e8;
		}
	}
	{
		GameObject_t3674682005 * L_29 = V_4;
		bool L_30 = ActionHelpers_IsVisible_m98613596(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		if (L_30)
		{
			goto IL_00e8;
		}
	}
	{
		goto IL_011d;
	}

IL_00e8:
	{
		GameObject_t3674682005 * L_31 = V_0;
		NullCheck(L_31);
		Transform_t1659122786 * L_32 = GameObject_get_transform_m1278640159(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		Vector3_t4282066566  L_33 = Transform_get_position_m2211398607(L_32, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_34 = V_4;
		NullCheck(L_34);
		Transform_t1659122786 * L_35 = GameObject_get_transform_m1278640159(L_34, /*hidden argument*/NULL);
		NullCheck(L_35);
		Vector3_t4282066566  L_36 = Transform_get_position_m2211398607(L_35, /*hidden argument*/NULL);
		Vector3_t4282066566  L_37 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_33, L_36, /*hidden argument*/NULL);
		V_8 = L_37;
		float L_38 = Vector3_get_sqrMagnitude_m1207423764((&V_8), /*hidden argument*/NULL);
		V_7 = L_38;
		float L_39 = V_7;
		float L_40 = V_3;
		if ((!(((float)L_39) < ((float)L_40))))
		{
			goto IL_011d;
		}
	}
	{
		float L_41 = V_7;
		V_3 = L_41;
		GameObject_t3674682005 * L_42 = V_4;
		V_2 = L_42;
	}

IL_011d:
	{
		int32_t L_43 = V_6;
		V_6 = ((int32_t)((int32_t)L_43+(int32_t)1));
	}

IL_0123:
	{
		int32_t L_44 = V_6;
		GameObjectU5BU5D_t2662109048* L_45 = V_5;
		NullCheck(L_45);
		if ((((int32_t)L_44) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_45)->max_length)))))))
		{
			goto IL_0099;
		}
	}
	{
		FsmGameObject_t1697147867 * L_46 = __this->get_storeObject_13();
		GameObject_t3674682005 * L_47 = V_2;
		NullCheck(L_46);
		FsmGameObject_set_Value_m297051598(L_46, L_47, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_48 = __this->get_storeDistance_14();
		NullCheck(L_48);
		bool L_49 = NamedVariable_get_IsNone_m281035543(L_48, /*hidden argument*/NULL);
		if (L_49)
		{
			goto IL_015b;
		}
	}
	{
		FsmFloat_t2134102846 * L_50 = __this->get_storeDistance_14();
		float L_51 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_52 = sqrtf(L_51);
		NullCheck(L_50);
		FsmFloat_set_Value_m1568963140(L_50, L_52, /*hidden argument*/NULL);
	}

IL_015b:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FindGameObject::.ctor()
extern "C"  void FindGameObject__ctor_m1127948628 (FindGameObject_t2514584402 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FindGameObject::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral69913957;
extern const uint32_t FindGameObject_Reset_m3069348865_MetadataUsageId;
extern "C"  void FindGameObject_Reset_m3069348865 (FindGameObject_t2514584402 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FindGameObject_Reset_m3069348865_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_objectName_9(L_1);
		FsmString_t952858651 * L_2 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, _stringLiteral69913957, /*hidden argument*/NULL);
		__this->set_withTag_10(L_2);
		__this->set_store_11((FsmGameObject_t1697147867 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FindGameObject::OnEnter()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral69913957;
extern const uint32_t FindGameObject_OnEnter_m1018601515_MetadataUsageId;
extern "C"  void FindGameObject_OnEnter_m1018601515 (FindGameObject_t2514584402 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FindGameObject_OnEnter_m1018601515_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObjectU5BU5D_t2662109048* V_0 = NULL;
	GameObject_t3674682005 * V_1 = NULL;
	GameObjectU5BU5D_t2662109048* V_2 = NULL;
	int32_t V_3 = 0;
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		FsmString_t952858651 * L_0 = __this->get_withTag_10();
		NullCheck(L_0);
		String_t* L_1 = FsmString_get_Value_m872383149(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_1, _stringLiteral69913957, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00b1;
		}
	}
	{
		FsmString_t952858651 * L_3 = __this->get_objectName_9();
		NullCheck(L_3);
		String_t* L_4 = FsmString_get_Value_m872383149(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0095;
		}
	}
	{
		FsmString_t952858651 * L_6 = __this->get_withTag_10();
		NullCheck(L_6);
		String_t* L_7 = FsmString_get_Value_m872383149(L_6, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_8 = GameObject_FindGameObjectsWithTag_m3058873418(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		GameObjectU5BU5D_t2662109048* L_9 = V_0;
		V_2 = L_9;
		V_3 = 0;
		goto IL_007f;
	}

IL_004f:
	{
		GameObjectU5BU5D_t2662109048* L_10 = V_2;
		int32_t L_11 = V_3;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		GameObject_t3674682005 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		V_1 = L_13;
		GameObject_t3674682005 * L_14 = V_1;
		NullCheck(L_14);
		String_t* L_15 = Object_get_name_m3709440845(L_14, /*hidden argument*/NULL);
		FsmString_t952858651 * L_16 = __this->get_objectName_9();
		NullCheck(L_16);
		String_t* L_17 = FsmString_get_Value_m872383149(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_18 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_15, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_007b;
		}
	}
	{
		FsmGameObject_t1697147867 * L_19 = __this->get_store_11();
		GameObject_t3674682005 * L_20 = V_1;
		NullCheck(L_19);
		FsmGameObject_set_Value_m297051598(L_19, L_20, /*hidden argument*/NULL);
		return;
	}

IL_007b:
	{
		int32_t L_21 = V_3;
		V_3 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_007f:
	{
		int32_t L_22 = V_3;
		GameObjectU5BU5D_t2662109048* L_23 = V_2;
		NullCheck(L_23);
		if ((((int32_t)L_22) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_23)->max_length)))))))
		{
			goto IL_004f;
		}
	}
	{
		FsmGameObject_t1697147867 * L_24 = __this->get_store_11();
		NullCheck(L_24);
		FsmGameObject_set_Value_m297051598(L_24, (GameObject_t3674682005 *)NULL, /*hidden argument*/NULL);
		return;
	}

IL_0095:
	{
		FsmGameObject_t1697147867 * L_25 = __this->get_store_11();
		FsmString_t952858651 * L_26 = __this->get_withTag_10();
		NullCheck(L_26);
		String_t* L_27 = FsmString_get_Value_m872383149(L_26, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_28 = GameObject_FindGameObjectWithTag_m2635560165(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		NullCheck(L_25);
		FsmGameObject_set_Value_m297051598(L_25, L_28, /*hidden argument*/NULL);
		return;
	}

IL_00b1:
	{
		FsmGameObject_t1697147867 * L_29 = __this->get_store_11();
		FsmString_t952858651 * L_30 = __this->get_objectName_9();
		NullCheck(L_30);
		String_t* L_31 = FsmString_get_Value_m872383149(L_30, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_32 = GameObject_Find_m332785498(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		NullCheck(L_29);
		FsmGameObject_set_Value_m297051598(L_29, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.String HutongGames.PlayMaker.Actions.FindGameObject::ErrorCheck()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2307432544;
extern const uint32_t FindGameObject_ErrorCheck_m2641209971_MetadataUsageId;
extern "C"  String_t* FindGameObject_ErrorCheck_m2641209971 (FindGameObject_t2514584402 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FindGameObject_ErrorCheck_m2641209971_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmString_t952858651 * L_0 = __this->get_objectName_9();
		NullCheck(L_0);
		String_t* L_1 = FsmString_get_Value_m872383149(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0030;
		}
	}
	{
		FsmString_t952858651 * L_3 = __this->get_withTag_10();
		NullCheck(L_3);
		String_t* L_4 = FsmString_get_Value_m872383149(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0030;
		}
	}
	{
		return _stringLiteral2307432544;
	}

IL_0030:
	{
		return (String_t*)NULL;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FinishFSM::.ctor()
extern "C"  void FinishFSM__ctor_m1366185115 (FinishFSM_t701881083 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FinishFSM::OnEnter()
extern "C"  void FinishFSM_OnEnter_m2330598834 (FinishFSM_t701881083 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Fsm_Stop_m177462513(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.Flicker::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m994342140_MethodInfo_var;
extern const uint32_t Flicker__ctor_m3158791152_MetadataUsageId;
extern "C"  void Flicker__ctor_m3158791152 (Flicker_t1041969670 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Flicker__ctor_m3158791152_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m994342140(__this, /*hidden argument*/ComponentAction_1__ctor_m994342140_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.Flicker::Reset()
extern "C"  void Flicker_Reset_m805224093 (Flicker_t1041969670 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.1f), /*hidden argument*/NULL);
		__this->set_frequency_12(L_0);
		FsmFloat_t2134102846 * L_1 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.5f), /*hidden argument*/NULL);
		__this->set_amountOn_13(L_1);
		__this->set_rendererOnly_14((bool)1);
		__this->set_realTime_15((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.Flicker::OnEnter()
extern "C"  void Flicker_OnEnter_m2743114695 (Flicker_t1041969670 * __this, const MethodInfo* method)
{
	{
		float L_0 = FsmTime_get_RealtimeSinceStartup_m372847495(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_startTime_16(L_0);
		__this->set_timer_17((0.0f));
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.Flicker::OnUpdate()
extern const MethodInfo* ComponentAction_1_UpdateCache_m3595067967_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_renderer_m773530769_MethodInfo_var;
extern const uint32_t Flicker_OnUpdate_m2565736284_MetadataUsageId;
extern "C"  void Flicker_OnUpdate_m2565736284 (Flicker_t1041969670 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Flicker_OnUpdate_m2565736284_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	bool V_1 = false;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		bool L_5 = __this->get_realTime_15();
		if (!L_5)
		{
			goto IL_0041;
		}
	}
	{
		float L_6 = FsmTime_get_RealtimeSinceStartup_m372847495(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_7 = __this->get_startTime_16();
		__this->set_timer_17(((float)((float)L_6-(float)L_7)));
		goto IL_0053;
	}

IL_0041:
	{
		float L_8 = __this->get_timer_17();
		float L_9 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_timer_17(((float)((float)L_8+(float)L_9)));
	}

IL_0053:
	{
		float L_10 = __this->get_timer_17();
		FsmFloat_t2134102846 * L_11 = __this->get_frequency_12();
		NullCheck(L_11);
		float L_12 = FsmFloat_get_Value_m4137923823(L_11, /*hidden argument*/NULL);
		if ((!(((float)L_10) > ((float)L_12))))
		{
			goto IL_00cc;
		}
	}
	{
		float L_13 = Random_Range_m3362417303(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_14 = __this->get_amountOn_13();
		NullCheck(L_14);
		float L_15 = FsmFloat_get_Value_m4137923823(L_14, /*hidden argument*/NULL);
		V_1 = (bool)((((float)L_13) < ((float)L_15))? 1 : 0);
		bool L_16 = __this->get_rendererOnly_14();
		if (!L_16)
		{
			goto IL_00ae;
		}
	}
	{
		GameObject_t3674682005 * L_17 = V_0;
		bool L_18 = ComponentAction_1_UpdateCache_m3595067967(__this, L_17, /*hidden argument*/ComponentAction_1_UpdateCache_m3595067967_MethodInfo_var);
		if (!L_18)
		{
			goto IL_00a9;
		}
	}
	{
		Renderer_t3076687687 * L_19 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		bool L_20 = V_1;
		NullCheck(L_19);
		Renderer_set_enabled_m2514140131(L_19, L_20, /*hidden argument*/NULL);
	}

IL_00a9:
	{
		goto IL_00b5;
	}

IL_00ae:
	{
		GameObject_t3674682005 * L_21 = V_0;
		bool L_22 = V_1;
		NullCheck(L_21);
		GameObject_SetActive_m3538205401(L_21, L_22, /*hidden argument*/NULL);
	}

IL_00b5:
	{
		float L_23 = __this->get_timer_17();
		__this->set_startTime_16(L_23);
		__this->set_timer_17((0.0f));
	}

IL_00cc:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatAbs::.ctor()
extern "C"  void FloatAbs__ctor_m1440920328 (FloatAbs_t1757489438 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatAbs::Reset()
extern "C"  void FloatAbs_Reset_m3382320565 (FloatAbs_t1757489438 * __this, const MethodInfo* method)
{
	{
		__this->set_floatVariable_9((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_10((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatAbs::OnEnter()
extern "C"  void FloatAbs_OnEnter_m1136694495 (FloatAbs_t1757489438 * __this, const MethodInfo* method)
{
	{
		FloatAbs_DoFloatAbs_m2648064733(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_10();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatAbs::OnUpdate()
extern "C"  void FloatAbs_OnUpdate_m11350340 (FloatAbs_t1757489438 * __this, const MethodInfo* method)
{
	{
		FloatAbs_DoFloatAbs_m2648064733(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatAbs::DoFloatAbs()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t FloatAbs_DoFloatAbs_m2648064733_MetadataUsageId;
extern "C"  void FloatAbs_DoFloatAbs_m2648064733 (FloatAbs_t1757489438 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FloatAbs_DoFloatAbs_m2648064733_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmFloat_t2134102846 * L_0 = __this->get_floatVariable_9();
		FsmFloat_t2134102846 * L_1 = __this->get_floatVariable_9();
		NullCheck(L_1);
		float L_2 = FsmFloat_get_Value_m4137923823(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_3 = fabsf(L_2);
		NullCheck(L_0);
		FsmFloat_set_Value_m1568963140(L_0, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatAdd::.ctor()
extern "C"  void FloatAdd__ctor_m794720185 (FloatAdd_t1757489485 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatAdd::Reset()
extern "C"  void FloatAdd_Reset_m2736120422 (FloatAdd_t1757489485 * __this, const MethodInfo* method)
{
	{
		__this->set_floatVariable_9((FsmFloat_t2134102846 *)NULL);
		__this->set_add_10((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_11((bool)0);
		__this->set_perSecond_12((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatAdd::OnEnter()
extern "C"  void FloatAdd_OnEnter_m2908614992 (FloatAdd_t1757489485 * __this, const MethodInfo* method)
{
	{
		FloatAdd_DoFloatAdd_m488600891(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_11();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatAdd::OnUpdate()
extern "C"  void FloatAdd_OnUpdate_m3401278195 (FloatAdd_t1757489485 * __this, const MethodInfo* method)
{
	{
		FloatAdd_DoFloatAdd_m488600891(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatAdd::DoFloatAdd()
extern "C"  void FloatAdd_DoFloatAdd_m488600891 (FloatAdd_t1757489485 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_perSecond_12();
		if (L_0)
		{
			goto IL_002d;
		}
	}
	{
		FsmFloat_t2134102846 * L_1 = __this->get_floatVariable_9();
		FsmFloat_t2134102846 * L_2 = L_1;
		NullCheck(L_2);
		float L_3 = FsmFloat_get_Value_m4137923823(L_2, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_4 = __this->get_add_10();
		NullCheck(L_4);
		float L_5 = FsmFloat_get_Value_m4137923823(L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmFloat_set_Value_m1568963140(L_2, ((float)((float)L_3+(float)L_5)), /*hidden argument*/NULL);
		goto IL_0050;
	}

IL_002d:
	{
		FsmFloat_t2134102846 * L_6 = __this->get_floatVariable_9();
		FsmFloat_t2134102846 * L_7 = L_6;
		NullCheck(L_7);
		float L_8 = FsmFloat_get_Value_m4137923823(L_7, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_9 = __this->get_add_10();
		NullCheck(L_9);
		float L_10 = FsmFloat_get_Value_m4137923823(L_9, /*hidden argument*/NULL);
		float L_11 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		FsmFloat_set_Value_m1568963140(L_7, ((float)((float)L_8+(float)((float)((float)L_10*(float)L_11)))), /*hidden argument*/NULL);
	}

IL_0050:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatAddMutiple::.ctor()
extern "C"  void FloatAddMutiple__ctor_m873260929 (FloatAddMutiple_t949141333 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatAddMutiple::Reset()
extern "C"  void FloatAddMutiple_Reset_m2814661166 (FloatAddMutiple_t949141333 * __this, const MethodInfo* method)
{
	{
		__this->set_floatVariables_9((FsmFloatU5BU5D_t2945380875*)NULL);
		__this->set_addTo_10((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatAddMutiple::OnEnter()
extern "C"  void FloatAddMutiple_OnEnter_m1076858648 (FloatAddMutiple_t949141333 * __this, const MethodInfo* method)
{
	{
		FloatAddMutiple_DoFloatAdd_m2489819763(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_11();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatAddMutiple::OnUpdate()
extern "C"  void FloatAddMutiple_OnUpdate_m2451406379 (FloatAddMutiple_t949141333 * __this, const MethodInfo* method)
{
	{
		FloatAddMutiple_DoFloatAdd_m2489819763(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatAddMutiple::DoFloatAdd()
extern "C"  void FloatAddMutiple_DoFloatAdd_m2489819763 (FloatAddMutiple_t949141333 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_002a;
	}

IL_0007:
	{
		FsmFloat_t2134102846 * L_0 = __this->get_addTo_10();
		FsmFloat_t2134102846 * L_1 = L_0;
		NullCheck(L_1);
		float L_2 = FsmFloat_get_Value_m4137923823(L_1, /*hidden argument*/NULL);
		FsmFloatU5BU5D_t2945380875* L_3 = __this->get_floatVariables_9();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		FsmFloat_t2134102846 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_6);
		float L_7 = FsmFloat_get_Value_m4137923823(L_6, /*hidden argument*/NULL);
		NullCheck(L_1);
		FsmFloat_set_Value_m1568963140(L_1, ((float)((float)L_2+(float)L_7)), /*hidden argument*/NULL);
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_002a:
	{
		int32_t L_9 = V_0;
		FsmFloatU5BU5D_t2945380875* L_10 = __this->get_floatVariables_9();
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatChanged::.ctor()
extern "C"  void FloatChanged__ctor_m1836121990 (FloatChanged_t1905781472 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatChanged::Reset()
extern "C"  void FloatChanged_Reset_m3777522227 (FloatChanged_t1905781472 * __this, const MethodInfo* method)
{
	{
		__this->set_floatVariable_9((FsmFloat_t2134102846 *)NULL);
		__this->set_changedEvent_10((FsmEvent_t2133468028 *)NULL);
		__this->set_storeResult_11((FsmBool_t1075959796 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatChanged::OnEnter()
extern "C"  void FloatChanged_OnEnter_m2968369629 (FloatChanged_t1905781472 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = __this->get_floatVariable_9();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0017:
	{
		FsmFloat_t2134102846 * L_2 = __this->get_floatVariable_9();
		NullCheck(L_2);
		float L_3 = FsmFloat_get_Value_m4137923823(L_2, /*hidden argument*/NULL);
		__this->set_previousValue_12(L_3);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatChanged::OnUpdate()
extern "C"  void FloatChanged_OnUpdate_m958704646 (FloatChanged_t1905781472 * __this, const MethodInfo* method)
{
	{
		FsmBool_t1075959796 * L_0 = __this->get_storeResult_11();
		NullCheck(L_0);
		FsmBool_set_Value_m1126216340(L_0, (bool)0, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_1 = __this->get_floatVariable_9();
		NullCheck(L_1);
		float L_2 = FsmFloat_get_Value_m4137923823(L_1, /*hidden argument*/NULL);
		float L_3 = __this->get_previousValue_12();
		if ((((float)L_2) == ((float)L_3)))
		{
			goto IL_0050;
		}
	}
	{
		FsmFloat_t2134102846 * L_4 = __this->get_floatVariable_9();
		NullCheck(L_4);
		float L_5 = FsmFloat_get_Value_m4137923823(L_4, /*hidden argument*/NULL);
		__this->set_previousValue_12(L_5);
		FsmBool_t1075959796 * L_6 = __this->get_storeResult_11();
		NullCheck(L_6);
		FsmBool_set_Value_m1126216340(L_6, (bool)1, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_7 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_8 = __this->get_changedEvent_10();
		NullCheck(L_7);
		Fsm_Event_m625948263(L_7, L_8, /*hidden argument*/NULL);
	}

IL_0050:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatClamp::.ctor()
extern "C"  void FloatClamp__ctor_m3898602271 (FloatClamp_t1735441703 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatClamp::Reset()
extern "C"  void FloatClamp_Reset_m1545035212 (FloatClamp_t1735441703 * __this, const MethodInfo* method)
{
	{
		__this->set_floatVariable_9((FsmFloat_t2134102846 *)NULL);
		__this->set_minValue_10((FsmFloat_t2134102846 *)NULL);
		__this->set_maxValue_11((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_12((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatClamp::OnEnter()
extern "C"  void FloatClamp_OnEnter_m737028918 (FloatClamp_t1735441703 * __this, const MethodInfo* method)
{
	{
		FloatClamp_DoClamp_m3332393005(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_12();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatClamp::OnUpdate()
extern "C"  void FloatClamp_OnUpdate_m506619341 (FloatClamp_t1735441703 * __this, const MethodInfo* method)
{
	{
		FloatClamp_DoClamp_m3332393005(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatClamp::DoClamp()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t FloatClamp_DoClamp_m3332393005_MetadataUsageId;
extern "C"  void FloatClamp_DoClamp_m3332393005 (FloatClamp_t1735441703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FloatClamp_DoClamp_m3332393005_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmFloat_t2134102846 * L_0 = __this->get_floatVariable_9();
		FsmFloat_t2134102846 * L_1 = __this->get_floatVariable_9();
		NullCheck(L_1);
		float L_2 = FsmFloat_get_Value_m4137923823(L_1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_3 = __this->get_minValue_10();
		NullCheck(L_3);
		float L_4 = FsmFloat_get_Value_m4137923823(L_3, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_5 = __this->get_maxValue_11();
		NullCheck(L_5);
		float L_6 = FsmFloat_get_Value_m4137923823(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_7 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_2, L_4, L_6, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmFloat_set_Value_m1568963140(L_0, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatCompare::.ctor()
extern "C"  void FloatCompare__ctor_m2878934133 (FloatCompare_t2117322001 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatCompare::Reset()
extern "C"  void FloatCompare_Reset_m525367074 (FloatCompare_t2117322001 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_float1_9(L_0);
		FsmFloat_t2134102846 * L_1 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_float2_10(L_1);
		FsmFloat_t2134102846 * L_2 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_tolerance_11(L_2);
		__this->set_equal_12((FsmEvent_t2133468028 *)NULL);
		__this->set_lessThan_13((FsmEvent_t2133468028 *)NULL);
		__this->set_greaterThan_14((FsmEvent_t2133468028 *)NULL);
		__this->set_everyFrame_15((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatCompare::OnEnter()
extern "C"  void FloatCompare_OnEnter_m88491788 (FloatCompare_t2117322001 * __this, const MethodInfo* method)
{
	{
		FloatCompare_DoCompare_m978022381(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatCompare::OnUpdate()
extern "C"  void FloatCompare_OnUpdate_m1876804791 (FloatCompare_t2117322001 * __this, const MethodInfo* method)
{
	{
		FloatCompare_DoCompare_m978022381(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatCompare::DoCompare()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t FloatCompare_DoCompare_m978022381_MetadataUsageId;
extern "C"  void FloatCompare_DoCompare_m978022381 (FloatCompare_t2117322001 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FloatCompare_DoCompare_m978022381_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmFloat_t2134102846 * L_0 = __this->get_float1_9();
		NullCheck(L_0);
		float L_1 = FsmFloat_get_Value_m4137923823(L_0, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_2 = __this->get_float2_10();
		NullCheck(L_2);
		float L_3 = FsmFloat_get_Value_m4137923823(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_4 = fabsf(((float)((float)L_1-(float)L_3)));
		FsmFloat_t2134102846 * L_5 = __this->get_tolerance_11();
		NullCheck(L_5);
		float L_6 = FsmFloat_get_Value_m4137923823(L_5, /*hidden argument*/NULL);
		if ((!(((float)L_4) <= ((float)L_6))))
		{
			goto IL_003e;
		}
	}
	{
		Fsm_t1527112426 * L_7 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_8 = __this->get_equal_12();
		NullCheck(L_7);
		Fsm_Event_m625948263(L_7, L_8, /*hidden argument*/NULL);
		return;
	}

IL_003e:
	{
		FsmFloat_t2134102846 * L_9 = __this->get_float1_9();
		NullCheck(L_9);
		float L_10 = FsmFloat_get_Value_m4137923823(L_9, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_11 = __this->get_float2_10();
		NullCheck(L_11);
		float L_12 = FsmFloat_get_Value_m4137923823(L_11, /*hidden argument*/NULL);
		if ((!(((float)L_10) < ((float)L_12))))
		{
			goto IL_006b;
		}
	}
	{
		Fsm_t1527112426 * L_13 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_14 = __this->get_lessThan_13();
		NullCheck(L_13);
		Fsm_Event_m625948263(L_13, L_14, /*hidden argument*/NULL);
		return;
	}

IL_006b:
	{
		FsmFloat_t2134102846 * L_15 = __this->get_float1_9();
		NullCheck(L_15);
		float L_16 = FsmFloat_get_Value_m4137923823(L_15, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_17 = __this->get_float2_10();
		NullCheck(L_17);
		float L_18 = FsmFloat_get_Value_m4137923823(L_17, /*hidden argument*/NULL);
		if ((!(((float)L_16) > ((float)L_18))))
		{
			goto IL_0097;
		}
	}
	{
		Fsm_t1527112426 * L_19 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_20 = __this->get_greaterThan_14();
		NullCheck(L_19);
		Fsm_Event_m625948263(L_19, L_20, /*hidden argument*/NULL);
	}

IL_0097:
	{
		return;
	}
}
// System.String HutongGames.PlayMaker.Actions.FloatCompare::ErrorCheck()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1226149032;
extern const uint32_t FloatCompare_ErrorCheck_m3586345138_MetadataUsageId;
extern "C"  String_t* FloatCompare_ErrorCheck_m3586345138 (FloatCompare_t2117322001 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FloatCompare_ErrorCheck_m3586345138_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmEvent_t2133468028 * L_0 = __this->get_equal_12();
		bool L_1 = FsmEvent_IsNullOrEmpty_m3021350928(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0036;
		}
	}
	{
		FsmEvent_t2133468028 * L_2 = __this->get_lessThan_13();
		bool L_3 = FsmEvent_IsNullOrEmpty_m3021350928(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0036;
		}
	}
	{
		FsmEvent_t2133468028 * L_4 = __this->get_greaterThan_14();
		bool L_5 = FsmEvent_IsNullOrEmpty_m3021350928(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0036;
		}
	}
	{
		return _stringLiteral1226149032;
	}

IL_0036:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_6;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatDivide::.ctor()
extern "C"  void FloatDivide__ctor_m631157267 (FloatDivide_t1636605059 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatDivide::Reset()
extern "C"  void FloatDivide_Reset_m2572557504 (FloatDivide_t1636605059 * __this, const MethodInfo* method)
{
	{
		__this->set_floatVariable_9((FsmFloat_t2134102846 *)NULL);
		__this->set_divideBy_10((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatDivide::OnEnter()
extern "C"  void FloatDivide_OnEnter_m343473450 (FloatDivide_t1636605059 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = __this->get_floatVariable_9();
		FsmFloat_t2134102846 * L_1 = L_0;
		NullCheck(L_1);
		float L_2 = FsmFloat_get_Value_m4137923823(L_1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_3 = __this->get_divideBy_10();
		NullCheck(L_3);
		float L_4 = FsmFloat_get_Value_m4137923823(L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		FsmFloat_set_Value_m1568963140(L_1, ((float)((float)L_2/(float)L_4)), /*hidden argument*/NULL);
		bool L_5 = __this->get_everyFrame_11();
		if (L_5)
		{
			goto IL_002e;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_002e:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatDivide::OnUpdate()
extern "C"  void FloatDivide_OnUpdate_m1191301721 (FloatDivide_t1636605059 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = __this->get_floatVariable_9();
		FsmFloat_t2134102846 * L_1 = L_0;
		NullCheck(L_1);
		float L_2 = FsmFloat_get_Value_m4137923823(L_1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_3 = __this->get_divideBy_10();
		NullCheck(L_3);
		float L_4 = FsmFloat_get_Value_m4137923823(L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		FsmFloat_set_Value_m1568963140(L_1, ((float)((float)L_2/(float)L_4)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatInterpolate::.ctor()
extern "C"  void FloatInterpolate__ctor_m748084313 (FloatInterpolate_t1095399597 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatInterpolate::Reset()
extern "C"  void FloatInterpolate_Reset_m2689484550 (FloatInterpolate_t1095399597 * __this, const MethodInfo* method)
{
	{
		__this->set_mode_9(0);
		__this->set_fromFloat_10((FsmFloat_t2134102846 *)NULL);
		__this->set_toFloat_11((FsmFloat_t2134102846 *)NULL);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_time_12(L_0);
		__this->set_storeResult_13((FsmFloat_t2134102846 *)NULL);
		__this->set_finishEvent_14((FsmEvent_t2133468028 *)NULL);
		__this->set_realTime_15((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatInterpolate::OnEnter()
extern "C"  void FloatInterpolate_OnEnter_m1041214960 (FloatInterpolate_t1095399597 * __this, const MethodInfo* method)
{
	{
		float L_0 = FsmTime_get_RealtimeSinceStartup_m372847495(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_startTime_16(L_0);
		__this->set_currentTime_17((0.0f));
		FsmFloat_t2134102846 * L_1 = __this->get_storeResult_13();
		if (L_1)
		{
			goto IL_002c;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		goto IL_0042;
	}

IL_002c:
	{
		FsmFloat_t2134102846 * L_2 = __this->get_storeResult_13();
		FsmFloat_t2134102846 * L_3 = __this->get_fromFloat_10();
		NullCheck(L_3);
		float L_4 = FsmFloat_get_Value_m4137923823(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmFloat_set_Value_m1568963140(L_2, L_4, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatInterpolate::OnUpdate()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t FloatInterpolate_OnUpdate_m1346452051_MetadataUsageId;
extern "C"  void FloatInterpolate_OnUpdate_m1346452051 (FloatInterpolate_t1095399597 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FloatInterpolate_OnUpdate_m1346452051_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	{
		bool L_0 = __this->get_realTime_15();
		if (!L_0)
		{
			goto IL_0022;
		}
	}
	{
		float L_1 = FsmTime_get_RealtimeSinceStartup_m372847495(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_2 = __this->get_startTime_16();
		__this->set_currentTime_17(((float)((float)L_1-(float)L_2)));
		goto IL_0034;
	}

IL_0022:
	{
		float L_3 = __this->get_currentTime_17();
		float L_4 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_currentTime_17(((float)((float)L_3+(float)L_4)));
	}

IL_0034:
	{
		float L_5 = __this->get_currentTime_17();
		FsmFloat_t2134102846 * L_6 = __this->get_time_12();
		NullCheck(L_6);
		float L_7 = FsmFloat_get_Value_m4137923823(L_6, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_5/(float)L_7));
		int32_t L_8 = __this->get_mode_9();
		V_1 = L_8;
		int32_t L_9 = V_1;
		if (!L_9)
		{
			goto IL_0060;
		}
	}
	{
		int32_t L_10 = V_1;
		if ((((int32_t)L_10) == ((int32_t)1)))
		{
			goto IL_008c;
		}
	}
	{
		goto IL_00b8;
	}

IL_0060:
	{
		FsmFloat_t2134102846 * L_11 = __this->get_storeResult_13();
		FsmFloat_t2134102846 * L_12 = __this->get_fromFloat_10();
		NullCheck(L_12);
		float L_13 = FsmFloat_get_Value_m4137923823(L_12, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_14 = __this->get_toFloat_11();
		NullCheck(L_14);
		float L_15 = FsmFloat_get_Value_m4137923823(L_14, /*hidden argument*/NULL);
		float L_16 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_17 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_13, L_15, L_16, /*hidden argument*/NULL);
		NullCheck(L_11);
		FsmFloat_set_Value_m1568963140(L_11, L_17, /*hidden argument*/NULL);
		goto IL_00b8;
	}

IL_008c:
	{
		FsmFloat_t2134102846 * L_18 = __this->get_storeResult_13();
		FsmFloat_t2134102846 * L_19 = __this->get_fromFloat_10();
		NullCheck(L_19);
		float L_20 = FsmFloat_get_Value_m4137923823(L_19, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_21 = __this->get_toFloat_11();
		NullCheck(L_21);
		float L_22 = FsmFloat_get_Value_m4137923823(L_21, /*hidden argument*/NULL);
		float L_23 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_24 = Mathf_SmoothStep_m1876640478(NULL /*static, unused*/, L_20, L_22, L_23, /*hidden argument*/NULL);
		NullCheck(L_18);
		FsmFloat_set_Value_m1568963140(L_18, L_24, /*hidden argument*/NULL);
		goto IL_00b8;
	}

IL_00b8:
	{
		float L_25 = V_0;
		if ((!(((float)L_25) > ((float)(1.0f)))))
		{
			goto IL_00e5;
		}
	}
	{
		FsmEvent_t2133468028 * L_26 = __this->get_finishEvent_14();
		if (!L_26)
		{
			goto IL_00df;
		}
	}
	{
		Fsm_t1527112426 * L_27 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_28 = __this->get_finishEvent_14();
		NullCheck(L_27);
		Fsm_Event_m625948263(L_27, L_28, /*hidden argument*/NULL);
	}

IL_00df:
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_00e5:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatMultiply::.ctor()
extern "C"  void FloatMultiply__ctor_m3361890952 (FloatMultiply_t1817102958 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatMultiply::Reset()
extern "C"  void FloatMultiply_Reset_m1008323893 (FloatMultiply_t1817102958 * __this, const MethodInfo* method)
{
	{
		__this->set_floatVariable_9((FsmFloat_t2134102846 *)NULL);
		__this->set_multiplyBy_10((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatMultiply::OnEnter()
extern "C"  void FloatMultiply_OnEnter_m353526879 (FloatMultiply_t1817102958 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = __this->get_floatVariable_9();
		FsmFloat_t2134102846 * L_1 = L_0;
		NullCheck(L_1);
		float L_2 = FsmFloat_get_Value_m4137923823(L_1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_3 = __this->get_multiplyBy_10();
		NullCheck(L_3);
		float L_4 = FsmFloat_get_Value_m4137923823(L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		FsmFloat_set_Value_m1568963140(L_1, ((float)((float)L_2*(float)L_4)), /*hidden argument*/NULL);
		bool L_5 = __this->get_everyFrame_11();
		if (L_5)
		{
			goto IL_002e;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_002e:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatMultiply::OnUpdate()
extern "C"  void FloatMultiply_OnUpdate_m1502958020 (FloatMultiply_t1817102958 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = __this->get_floatVariable_9();
		FsmFloat_t2134102846 * L_1 = L_0;
		NullCheck(L_1);
		float L_2 = FsmFloat_get_Value_m4137923823(L_1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_3 = __this->get_multiplyBy_10();
		NullCheck(L_3);
		float L_4 = FsmFloat_get_Value_m4137923823(L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		FsmFloat_set_Value_m1568963140(L_1, ((float)((float)L_2*(float)L_4)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatOperator::.ctor()
extern "C"  void FloatOperator__ctor_m2222698504 (FloatOperator_t662719726 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatOperator::Reset()
extern "C"  void FloatOperator_Reset_m4164098741 (FloatOperator_t662719726 * __this, const MethodInfo* method)
{
	{
		__this->set_float1_9((FsmFloat_t2134102846 *)NULL);
		__this->set_float2_10((FsmFloat_t2134102846 *)NULL);
		__this->set_operation_11(0);
		__this->set_storeResult_12((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatOperator::OnEnter()
extern "C"  void FloatOperator_OnEnter_m806244831 (FloatOperator_t662719726 * __this, const MethodInfo* method)
{
	{
		FloatOperator_DoFloatOperator_m4708859(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatOperator::OnUpdate()
extern "C"  void FloatOperator_OnUpdate_m2652312644 (FloatOperator_t662719726 * __this, const MethodInfo* method)
{
	{
		FloatOperator_DoFloatOperator_m4708859(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatOperator::DoFloatOperator()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t FloatOperator_DoFloatOperator_m4708859_MetadataUsageId;
extern "C"  void FloatOperator_DoFloatOperator_m4708859 (FloatOperator_t662719726 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FloatOperator_DoFloatOperator_m4708859_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	{
		FsmFloat_t2134102846 * L_0 = __this->get_float1_9();
		NullCheck(L_0);
		float L_1 = FsmFloat_get_Value_m4137923823(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		FsmFloat_t2134102846 * L_2 = __this->get_float2_10();
		NullCheck(L_2);
		float L_3 = FsmFloat_get_Value_m4137923823(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = __this->get_operation_11();
		V_2 = L_4;
		int32_t L_5 = V_2;
		if (L_5 == 0)
		{
			goto IL_0042;
		}
		if (L_5 == 1)
		{
			goto IL_0055;
		}
		if (L_5 == 2)
		{
			goto IL_0068;
		}
		if (L_5 == 3)
		{
			goto IL_007b;
		}
		if (L_5 == 4)
		{
			goto IL_008e;
		}
		if (L_5 == 5)
		{
			goto IL_00a5;
		}
	}
	{
		goto IL_00bc;
	}

IL_0042:
	{
		FsmFloat_t2134102846 * L_6 = __this->get_storeResult_12();
		float L_7 = V_0;
		float L_8 = V_1;
		NullCheck(L_6);
		FsmFloat_set_Value_m1568963140(L_6, ((float)((float)L_7+(float)L_8)), /*hidden argument*/NULL);
		goto IL_00bc;
	}

IL_0055:
	{
		FsmFloat_t2134102846 * L_9 = __this->get_storeResult_12();
		float L_10 = V_0;
		float L_11 = V_1;
		NullCheck(L_9);
		FsmFloat_set_Value_m1568963140(L_9, ((float)((float)L_10-(float)L_11)), /*hidden argument*/NULL);
		goto IL_00bc;
	}

IL_0068:
	{
		FsmFloat_t2134102846 * L_12 = __this->get_storeResult_12();
		float L_13 = V_0;
		float L_14 = V_1;
		NullCheck(L_12);
		FsmFloat_set_Value_m1568963140(L_12, ((float)((float)L_13*(float)L_14)), /*hidden argument*/NULL);
		goto IL_00bc;
	}

IL_007b:
	{
		FsmFloat_t2134102846 * L_15 = __this->get_storeResult_12();
		float L_16 = V_0;
		float L_17 = V_1;
		NullCheck(L_15);
		FsmFloat_set_Value_m1568963140(L_15, ((float)((float)L_16/(float)L_17)), /*hidden argument*/NULL);
		goto IL_00bc;
	}

IL_008e:
	{
		FsmFloat_t2134102846 * L_18 = __this->get_storeResult_12();
		float L_19 = V_0;
		float L_20 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_21 = Mathf_Min_m2322067385(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		NullCheck(L_18);
		FsmFloat_set_Value_m1568963140(L_18, L_21, /*hidden argument*/NULL);
		goto IL_00bc;
	}

IL_00a5:
	{
		FsmFloat_t2134102846 * L_22 = __this->get_storeResult_12();
		float L_23 = V_0;
		float L_24 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_25 = Mathf_Max_m3923796455(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		FsmFloat_set_Value_m1568963140(L_22, L_25, /*hidden argument*/NULL);
		goto IL_00bc;
	}

IL_00bc:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatSignTest::.ctor()
extern "C"  void FloatSignTest__ctor_m1954245917 (FloatSignTest_t1474597945 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatSignTest::Reset()
extern "C"  void FloatSignTest_Reset_m3895646154 (FloatSignTest_t1474597945 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_floatValue_9(L_0);
		__this->set_isPositive_10((FsmEvent_t2133468028 *)NULL);
		__this->set_isNegative_11((FsmEvent_t2133468028 *)NULL);
		__this->set_everyFrame_12((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatSignTest::OnEnter()
extern "C"  void FloatSignTest_OnEnter_m521346484 (FloatSignTest_t1474597945 * __this, const MethodInfo* method)
{
	{
		FloatSignTest_DoSignTest_m1316262689(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_12();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatSignTest::OnUpdate()
extern "C"  void FloatSignTest_OnUpdate_m2410398479 (FloatSignTest_t1474597945 * __this, const MethodInfo* method)
{
	{
		FloatSignTest_DoSignTest_m1316262689(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatSignTest::DoSignTest()
extern "C"  void FloatSignTest_DoSignTest_m1316262689 (FloatSignTest_t1474597945 * __this, const MethodInfo* method)
{
	Fsm_t1527112426 * G_B4_0 = NULL;
	Fsm_t1527112426 * G_B3_0 = NULL;
	FsmEvent_t2133468028 * G_B5_0 = NULL;
	Fsm_t1527112426 * G_B5_1 = NULL;
	{
		FsmFloat_t2134102846 * L_0 = __this->get_floatValue_9();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_2 = __this->get_floatValue_9();
		NullCheck(L_2);
		float L_3 = FsmFloat_get_Value_m4137923823(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		if ((!(((float)L_3) < ((float)(0.0f)))))
		{
			G_B4_0 = L_1;
			goto IL_0032;
		}
	}
	{
		FsmEvent_t2133468028 * L_4 = __this->get_isNegative_11();
		G_B5_0 = L_4;
		G_B5_1 = G_B3_0;
		goto IL_0038;
	}

IL_0032:
	{
		FsmEvent_t2133468028 * L_5 = __this->get_isPositive_10();
		G_B5_0 = L_5;
		G_B5_1 = G_B4_0;
	}

IL_0038:
	{
		NullCheck(G_B5_1);
		Fsm_Event_m625948263(G_B5_1, G_B5_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String HutongGames.PlayMaker.Actions.FloatSignTest::ErrorCheck()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1226149032;
extern const uint32_t FloatSignTest_ErrorCheck_m2356136740_MetadataUsageId;
extern "C"  String_t* FloatSignTest_ErrorCheck_m2356136740 (FloatSignTest_t1474597945 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FloatSignTest_ErrorCheck_m2356136740_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmEvent_t2133468028 * L_0 = __this->get_isPositive_10();
		bool L_1 = FsmEvent_IsNullOrEmpty_m3021350928(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		FsmEvent_t2133468028 * L_2 = __this->get_isNegative_11();
		bool L_3 = FsmEvent_IsNullOrEmpty_m3021350928(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		return _stringLiteral1226149032;
	}

IL_0026:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatSubtract::.ctor()
extern "C"  void FloatSubtract__ctor_m394558360 (FloatSubtract_t3397992286 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatSubtract::Reset()
extern "C"  void FloatSubtract_Reset_m2335958597 (FloatSubtract_t3397992286 * __this, const MethodInfo* method)
{
	{
		__this->set_floatVariable_9((FsmFloat_t2134102846 *)NULL);
		__this->set_subtract_10((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_11((bool)0);
		__this->set_perSecond_12((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatSubtract::OnEnter()
extern "C"  void FloatSubtract_OnEnter_m605190511 (FloatSubtract_t3397992286 * __this, const MethodInfo* method)
{
	{
		FloatSubtract_DoFloatSubtract_m2286520315(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_11();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatSubtract::OnUpdate()
extern "C"  void FloatSubtract_OnUpdate_m714596020 (FloatSubtract_t3397992286 * __this, const MethodInfo* method)
{
	{
		FloatSubtract_DoFloatSubtract_m2286520315(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatSubtract::DoFloatSubtract()
extern "C"  void FloatSubtract_DoFloatSubtract_m2286520315 (FloatSubtract_t3397992286 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_perSecond_12();
		if (L_0)
		{
			goto IL_002d;
		}
	}
	{
		FsmFloat_t2134102846 * L_1 = __this->get_floatVariable_9();
		FsmFloat_t2134102846 * L_2 = L_1;
		NullCheck(L_2);
		float L_3 = FsmFloat_get_Value_m4137923823(L_2, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_4 = __this->get_subtract_10();
		NullCheck(L_4);
		float L_5 = FsmFloat_get_Value_m4137923823(L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmFloat_set_Value_m1568963140(L_2, ((float)((float)L_3-(float)L_5)), /*hidden argument*/NULL);
		goto IL_0050;
	}

IL_002d:
	{
		FsmFloat_t2134102846 * L_6 = __this->get_floatVariable_9();
		FsmFloat_t2134102846 * L_7 = L_6;
		NullCheck(L_7);
		float L_8 = FsmFloat_get_Value_m4137923823(L_7, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_9 = __this->get_subtract_10();
		NullCheck(L_9);
		float L_10 = FsmFloat_get_Value_m4137923823(L_9, /*hidden argument*/NULL);
		float L_11 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		FsmFloat_set_Value_m1568963140(L_7, ((float)((float)L_8-(float)((float)((float)L_10*(float)L_11)))), /*hidden argument*/NULL);
	}

IL_0050:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatSwitch::.ctor()
extern "C"  void FloatSwitch__ctor_m1133337016 (FloatSwitch_t2078594878 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatSwitch::Reset()
extern Il2CppClass* FsmFloatU5BU5D_t2945380875_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmEventU5BU5D_t2862142229_il2cpp_TypeInfo_var;
extern const uint32_t FloatSwitch_Reset_m3074737253_MetadataUsageId;
extern "C"  void FloatSwitch_Reset_m3074737253 (FloatSwitch_t2078594878 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FloatSwitch_Reset_m3074737253_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_floatVariable_9((FsmFloat_t2134102846 *)NULL);
		__this->set_lessThan_10(((FsmFloatU5BU5D_t2945380875*)SZArrayNew(FsmFloatU5BU5D_t2945380875_il2cpp_TypeInfo_var, (uint32_t)1)));
		__this->set_sendEvent_11(((FsmEventU5BU5D_t2862142229*)SZArrayNew(FsmEventU5BU5D_t2862142229_il2cpp_TypeInfo_var, (uint32_t)1)));
		__this->set_everyFrame_12((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatSwitch::OnEnter()
extern "C"  void FloatSwitch_OnEnter_m1901875087 (FloatSwitch_t2078594878 * __this, const MethodInfo* method)
{
	{
		FloatSwitch_DoFloatSwitch_m863132603(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_12();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatSwitch::OnUpdate()
extern "C"  void FloatSwitch_OnUpdate_m2257112212 (FloatSwitch_t2078594878 * __this, const MethodInfo* method)
{
	{
		FloatSwitch_DoFloatSwitch_m863132603(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatSwitch::DoFloatSwitch()
extern "C"  void FloatSwitch_DoFloatSwitch_m863132603 (FloatSwitch_t2078594878 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmFloat_t2134102846 * L_0 = __this->get_floatVariable_9();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		V_0 = 0;
		goto IL_004d;
	}

IL_0018:
	{
		FsmFloat_t2134102846 * L_2 = __this->get_floatVariable_9();
		NullCheck(L_2);
		float L_3 = FsmFloat_get_Value_m4137923823(L_2, /*hidden argument*/NULL);
		FsmFloatU5BU5D_t2945380875* L_4 = __this->get_lessThan_10();
		int32_t L_5 = V_0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		FsmFloat_t2134102846 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_7);
		float L_8 = FsmFloat_get_Value_m4137923823(L_7, /*hidden argument*/NULL);
		if ((!(((float)L_3) < ((float)L_8))))
		{
			goto IL_0049;
		}
	}
	{
		Fsm_t1527112426 * L_9 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEventU5BU5D_t2862142229* L_10 = __this->get_sendEvent_11();
		int32_t L_11 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		FsmEvent_t2133468028 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck(L_9);
		Fsm_Event_m625948263(L_9, L_13, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		int32_t L_14 = V_0;
		V_0 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_004d:
	{
		int32_t L_15 = V_0;
		FsmFloatU5BU5D_t2945380875* L_16 = __this->get_lessThan_10();
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length)))))))
		{
			goto IL_0018;
		}
	}
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FormatString::.ctor()
extern "C"  void FormatString__ctor_m994606934 (FormatString_t1206542608 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FormatString::Reset()
extern "C"  void FormatString_Reset_m2936007171 (FormatString_t1206542608 * __this, const MethodInfo* method)
{
	{
		__this->set_format_9((FsmString_t952858651 *)NULL);
		__this->set_variables_10((FsmVarU5BU5D_t3498949300*)NULL);
		__this->set_storeResult_11((FsmString_t952858651 *)NULL);
		__this->set_everyFrame_12((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FormatString::OnEnter()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t FormatString_OnEnter_m1726252461_MetadataUsageId;
extern "C"  void FormatString_OnEnter_m1726252461 (FormatString_t1206542608 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FormatString_OnEnter_m1726252461_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmVarU5BU5D_t3498949300* L_0 = __this->get_variables_10();
		NullCheck(L_0);
		__this->set_objectArray_13(((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length)))))));
		FormatString_DoFormatString_m2819192129(__this, /*hidden argument*/NULL);
		bool L_1 = __this->get_everyFrame_12();
		if (L_1)
		{
			goto IL_002a;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FormatString::OnUpdate()
extern "C"  void FormatString_OnUpdate_m1107778102 (FormatString_t1206542608 * __this, const MethodInfo* method)
{
	{
		FormatString_DoFormatString_m2819192129(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FormatString::DoFormatString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FormatException_t3455918062_il2cpp_TypeInfo_var;
extern const uint32_t FormatString_DoFormatString_m2819192129_MetadataUsageId;
extern "C"  void FormatString_DoFormatString_m2819192129 (FormatString_t1206542608 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FormatString_DoFormatString_m2819192129_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	FormatException_t3455918062 * V_1 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = 0;
		goto IL_002d;
	}

IL_0007:
	{
		FsmVarU5BU5D_t3498949300* L_0 = __this->get_variables_10();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		FsmVar_t1596150537 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		FsmVar_UpdateValue_m1579759472(L_3, /*hidden argument*/NULL);
		ObjectU5BU5D_t1108656482* L_4 = __this->get_objectArray_13();
		int32_t L_5 = V_0;
		FsmVarU5BU5D_t3498949300* L_6 = __this->get_variables_10();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		FsmVar_t1596150537 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_9);
		Il2CppObject * L_10 = FsmVar_GetValue_m2309870922(L_9, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_10);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)L_10);
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_002d:
	{
		int32_t L_12 = V_0;
		FsmVarU5BU5D_t3498949300* L_13 = __this->get_variables_10();
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length)))))))
		{
			goto IL_0007;
		}
	}

IL_003b:
	try
	{ // begin try (depth: 1)
		FsmString_t952858651 * L_14 = __this->get_storeResult_11();
		FsmString_t952858651 * L_15 = __this->get_format_9();
		NullCheck(L_15);
		String_t* L_16 = FsmString_get_Value_m872383149(L_15, /*hidden argument*/NULL);
		ObjectU5BU5D_t1108656482* L_17 = __this->get_objectArray_13();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Format_m4050103162(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_14);
		FsmString_set_Value_m829393196(L_14, L_18, /*hidden argument*/NULL);
		goto IL_0079;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (FormatException_t3455918062_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0061;
		throw e;
	}

CATCH_0061:
	{ // begin catch(System.FormatException)
		V_1 = ((FormatException_t3455918062 *)__exception_local);
		FormatException_t3455918062 * L_19 = V_1;
		NullCheck(L_19);
		String_t* L_20 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_19);
		FsmStateAction_LogError_m3478223492(__this, L_20, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		goto IL_0079;
	} // end catch (depth: 1)

IL_0079:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ForwardAllEvents::.ctor()
extern "C"  void ForwardAllEvents__ctor_m3245051529 (ForwardAllEvents_t4134244477 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ForwardAllEvents::Reset()
extern Il2CppClass* FsmEventTarget_t1823904941_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmEventU5BU5D_t2862142229_il2cpp_TypeInfo_var;
extern const uint32_t ForwardAllEvents_Reset_m891484470_MetadataUsageId;
extern "C"  void ForwardAllEvents_Reset_m891484470 (ForwardAllEvents_t4134244477 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ForwardAllEvents_Reset_m891484470_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmEventTarget_t1823904941 * V_0 = NULL;
	{
		FsmEventTarget_t1823904941 * L_0 = (FsmEventTarget_t1823904941 *)il2cpp_codegen_object_new(FsmEventTarget_t1823904941_il2cpp_TypeInfo_var);
		FsmEventTarget__ctor_m3123387462(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmEventTarget_t1823904941 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_target_0(3);
		FsmEventTarget_t1823904941 * L_2 = V_0;
		__this->set_forwardTo_9(L_2);
		FsmEventU5BU5D_t2862142229* L_3 = ((FsmEventU5BU5D_t2862142229*)SZArrayNew(FsmEventU5BU5D_t2862142229_il2cpp_TypeInfo_var, (uint32_t)1));
		FsmEvent_t2133468028 * L_4 = FsmEvent_get_Finished_m374313731(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (FsmEvent_t2133468028 *)L_4);
		__this->set_exceptThese_10(L_3);
		__this->set_eatEvents_11((bool)1);
		return;
	}
}
// System.Boolean HutongGames.PlayMaker.Actions.ForwardAllEvents::Event(HutongGames.PlayMaker.FsmEvent)
extern "C"  bool ForwardAllEvents_Event_m4176117383 (ForwardAllEvents_t4134244477 * __this, FsmEvent_t2133468028 * ___fsmEvent0, const MethodInfo* method)
{
	FsmEvent_t2133468028 * V_0 = NULL;
	FsmEventU5BU5D_t2862142229* V_1 = NULL;
	int32_t V_2 = 0;
	{
		FsmEventU5BU5D_t2862142229* L_0 = __this->get_exceptThese_10();
		if (!L_0)
		{
			goto IL_0033;
		}
	}
	{
		FsmEventU5BU5D_t2862142229* L_1 = __this->get_exceptThese_10();
		V_1 = L_1;
		V_2 = 0;
		goto IL_002a;
	}

IL_0019:
	{
		FsmEventU5BU5D_t2862142229* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		FsmEvent_t2133468028 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_0 = L_5;
		FsmEvent_t2133468028 * L_6 = V_0;
		FsmEvent_t2133468028 * L_7 = ___fsmEvent0;
		if ((!(((Il2CppObject*)(FsmEvent_t2133468028 *)L_6) == ((Il2CppObject*)(FsmEvent_t2133468028 *)L_7))))
		{
			goto IL_0026;
		}
	}
	{
		return (bool)0;
	}

IL_0026:
	{
		int32_t L_8 = V_2;
		V_2 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_002a:
	{
		int32_t L_9 = V_2;
		FsmEventU5BU5D_t2862142229* L_10 = V_1;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_0019;
		}
	}

IL_0033:
	{
		Fsm_t1527112426 * L_11 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEventTarget_t1823904941 * L_12 = __this->get_forwardTo_9();
		FsmEvent_t2133468028 * L_13 = ___fsmEvent0;
		NullCheck(L_11);
		Fsm_Event_m1295831978(L_11, L_12, L_13, /*hidden argument*/NULL);
		bool L_14 = __this->get_eatEvents_11();
		return L_14;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ForwardEvent::.ctor()
extern "C"  void ForwardEvent__ctor_m2830231049 (ForwardEvent_t3275232509 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ForwardEvent::Reset()
extern Il2CppClass* FsmEventTarget_t1823904941_il2cpp_TypeInfo_var;
extern const uint32_t ForwardEvent_Reset_m476663990_MetadataUsageId;
extern "C"  void ForwardEvent_Reset_m476663990 (ForwardEvent_t3275232509 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ForwardEvent_Reset_m476663990_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmEventTarget_t1823904941 * V_0 = NULL;
	{
		FsmEventTarget_t1823904941 * L_0 = (FsmEventTarget_t1823904941 *)il2cpp_codegen_object_new(FsmEventTarget_t1823904941_il2cpp_TypeInfo_var);
		FsmEventTarget__ctor_m3123387462(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmEventTarget_t1823904941 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_target_0(3);
		FsmEventTarget_t1823904941 * L_2 = V_0;
		__this->set_forwardTo_9(L_2);
		__this->set_eventsToForward_10((FsmEventU5BU5D_t2862142229*)NULL);
		__this->set_eatEvents_11((bool)1);
		return;
	}
}
// System.Boolean HutongGames.PlayMaker.Actions.ForwardEvent::Event(HutongGames.PlayMaker.FsmEvent)
extern "C"  bool ForwardEvent_Event_m2780265479 (ForwardEvent_t3275232509 * __this, FsmEvent_t2133468028 * ___fsmEvent0, const MethodInfo* method)
{
	FsmEvent_t2133468028 * V_0 = NULL;
	FsmEventU5BU5D_t2862142229* V_1 = NULL;
	int32_t V_2 = 0;
	{
		FsmEventU5BU5D_t2862142229* L_0 = __this->get_eventsToForward_10();
		if (!L_0)
		{
			goto IL_004a;
		}
	}
	{
		FsmEventU5BU5D_t2862142229* L_1 = __this->get_eventsToForward_10();
		V_1 = L_1;
		V_2 = 0;
		goto IL_0041;
	}

IL_0019:
	{
		FsmEventU5BU5D_t2862142229* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		FsmEvent_t2133468028 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_0 = L_5;
		FsmEvent_t2133468028 * L_6 = V_0;
		FsmEvent_t2133468028 * L_7 = ___fsmEvent0;
		if ((!(((Il2CppObject*)(FsmEvent_t2133468028 *)L_6) == ((Il2CppObject*)(FsmEvent_t2133468028 *)L_7))))
		{
			goto IL_003d;
		}
	}
	{
		Fsm_t1527112426 * L_8 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEventTarget_t1823904941 * L_9 = __this->get_forwardTo_9();
		FsmEvent_t2133468028 * L_10 = ___fsmEvent0;
		NullCheck(L_8);
		Fsm_Event_m1295831978(L_8, L_9, L_10, /*hidden argument*/NULL);
		bool L_11 = __this->get_eatEvents_11();
		return L_11;
	}

IL_003d:
	{
		int32_t L_12 = V_2;
		V_2 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0041:
	{
		int32_t L_13 = V_2;
		FsmEventU5BU5D_t2862142229* L_14 = V_1;
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0019;
		}
	}

IL_004a:
	{
		return (bool)0;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmEventOptions::.ctor()
extern "C"  void FsmEventOptions__ctor_m3240264292 (FsmEventOptions_t3746547986 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmEventOptions::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t FsmEventOptions_Reset_m886697233_MetadataUsageId;
extern "C"  void FsmEventOptions_Reset_m886697233 (FsmEventOptions_t3746547986 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FsmEventOptions_Reset_m886697233_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_sendToFsmComponent_9((PlayMakerFSM_t3799847376 *)NULL);
		__this->set_sendToGameObject_10((FsmGameObject_t1697147867 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_11(L_1);
		FsmBool_t1075959796 * L_2 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_sendToChildren_12(L_2);
		FsmBool_t1075959796 * L_3 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_broadcastToAll_13(L_3);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmEventOptions::OnUpdate()
extern "C"  void FsmEventOptions_OnUpdate_m3075527784 (FsmEventOptions_t3746547986 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmStateSwitch::.ctor()
extern "C"  void FsmStateSwitch__ctor_m1908935257 (FsmStateSwitch_t3425333421 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmStateSwitch::Reset()
extern Il2CppClass* FsmStringU5BU5D_t2523845914_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmEventU5BU5D_t2862142229_il2cpp_TypeInfo_var;
extern const uint32_t FsmStateSwitch_Reset_m3850335494_MetadataUsageId;
extern "C"  void FsmStateSwitch_Reset_m3850335494 (FsmStateSwitch_t3425333421 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FsmStateSwitch_Reset_m3850335494_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_9((FsmGameObject_t1697147867 *)NULL);
		__this->set_fsmName_10((FsmString_t952858651 *)NULL);
		__this->set_compareTo_11(((FsmStringU5BU5D_t2523845914*)SZArrayNew(FsmStringU5BU5D_t2523845914_il2cpp_TypeInfo_var, (uint32_t)1)));
		__this->set_sendEvent_12(((FsmEventU5BU5D_t2862142229*)SZArrayNew(FsmEventU5BU5D_t2862142229_il2cpp_TypeInfo_var, (uint32_t)1)));
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmStateSwitch::OnEnter()
extern "C"  void FsmStateSwitch_OnEnter_m4222442480 (FsmStateSwitch_t3425333421 * __this, const MethodInfo* method)
{
	{
		FsmStateSwitch_DoFsmStateSwitch_m3415497723(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmStateSwitch::OnUpdate()
extern "C"  void FsmStateSwitch_OnUpdate_m1180257363 (FsmStateSwitch_t3425333421 * __this, const MethodInfo* method)
{
	{
		FsmStateSwitch_DoFsmStateSwitch_m3415497723(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmStateSwitch::DoFsmStateSwitch()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t FsmStateSwitch_DoFsmStateSwitch_m3415497723_MetadataUsageId;
extern "C"  void FsmStateSwitch_DoFsmStateSwitch_m3415497723 (FsmStateSwitch_t3425333421 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FsmStateSwitch_DoFsmStateSwitch_m3415497723_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	{
		FsmGameObject_t1697147867 * L_0 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_1 = FsmGameObject_get_Value_m673294275(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t3674682005 * L_2 = V_0;
		bool L_3 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_2, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0019;
		}
	}
	{
		return;
	}

IL_0019:
	{
		GameObject_t3674682005 * L_4 = V_0;
		GameObject_t3674682005 * L_5 = __this->get_previousGo_14();
		bool L_6 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0048;
		}
	}
	{
		GameObject_t3674682005 * L_7 = V_0;
		FsmString_t952858651 * L_8 = __this->get_fsmName_10();
		NullCheck(L_8);
		String_t* L_9 = FsmString_get_Value_m872383149(L_8, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_10 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_7, L_9, /*hidden argument*/NULL);
		__this->set_fsm_15(L_10);
		GameObject_t3674682005 * L_11 = V_0;
		__this->set_previousGo_14(L_11);
	}

IL_0048:
	{
		PlayMakerFSM_t3799847376 * L_12 = __this->get_fsm_15();
		bool L_13 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_12, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_005a;
		}
	}
	{
		return;
	}

IL_005a:
	{
		PlayMakerFSM_t3799847376 * L_14 = __this->get_fsm_15();
		NullCheck(L_14);
		String_t* L_15 = PlayMakerFSM_get_ActiveStateName_m1548898773(L_14, /*hidden argument*/NULL);
		V_1 = L_15;
		V_2 = 0;
		goto IL_009d;
	}

IL_006d:
	{
		String_t* L_16 = V_1;
		FsmStringU5BU5D_t2523845914* L_17 = __this->get_compareTo_11();
		int32_t L_18 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		FsmString_t952858651 * L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		NullCheck(L_20);
		String_t* L_21 = FsmString_get_Value_m872383149(L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_22 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_16, L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0099;
		}
	}
	{
		Fsm_t1527112426 * L_23 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEventU5BU5D_t2862142229* L_24 = __this->get_sendEvent_12();
		int32_t L_25 = V_2;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
		int32_t L_26 = L_25;
		FsmEvent_t2133468028 * L_27 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		NullCheck(L_23);
		Fsm_Event_m625948263(L_23, L_27, /*hidden argument*/NULL);
		return;
	}

IL_0099:
	{
		int32_t L_28 = V_2;
		V_2 = ((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_009d:
	{
		int32_t L_29 = V_2;
		FsmStringU5BU5D_t2523845914* L_30 = __this->get_compareTo_11();
		NullCheck(L_30);
		if ((((int32_t)L_29) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_30)->max_length)))))))
		{
			goto IL_006d;
		}
	}
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmStateTest::.ctor()
extern "C"  void FsmStateTest__ctor_m3033383227 (FsmStateTest_t3341384075 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmStateTest::Reset()
extern "C"  void FsmStateTest_Reset_m679816168 (FsmStateTest_t3341384075 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmGameObject_t1697147867 *)NULL);
		__this->set_fsmName_10((FsmString_t952858651 *)NULL);
		__this->set_stateName_11((FsmString_t952858651 *)NULL);
		__this->set_trueEvent_12((FsmEvent_t2133468028 *)NULL);
		__this->set_falseEvent_13((FsmEvent_t2133468028 *)NULL);
		__this->set_storeResult_14((FsmBool_t1075959796 *)NULL);
		__this->set_everyFrame_15((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmStateTest::OnEnter()
extern "C"  void FsmStateTest_OnEnter_m2485183058 (FsmStateTest_t3341384075 * __this, const MethodInfo* method)
{
	{
		FsmStateTest_DoFsmStateTest_m3949619895(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmStateTest::OnUpdate()
extern "C"  void FsmStateTest_OnUpdate_m3159790129 (FsmStateTest_t3341384075 * __this, const MethodInfo* method)
{
	{
		FsmStateTest_DoFsmStateTest_m3949619895(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmStateTest::DoFsmStateTest()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t FsmStateTest_DoFsmStateTest_m3949619895_MetadataUsageId;
extern "C"  void FsmStateTest_DoFsmStateTest_m3949619895 (FsmStateTest_t3341384075 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FsmStateTest_DoFsmStateTest_m3949619895_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	bool V_1 = false;
	{
		FsmGameObject_t1697147867 * L_0 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_1 = FsmGameObject_get_Value_m673294275(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t3674682005 * L_2 = V_0;
		bool L_3 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_2, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0019;
		}
	}
	{
		return;
	}

IL_0019:
	{
		GameObject_t3674682005 * L_4 = V_0;
		GameObject_t3674682005 * L_5 = __this->get_previousGo_16();
		bool L_6 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0048;
		}
	}
	{
		GameObject_t3674682005 * L_7 = V_0;
		FsmString_t952858651 * L_8 = __this->get_fsmName_10();
		NullCheck(L_8);
		String_t* L_9 = FsmString_get_Value_m872383149(L_8, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_10 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_7, L_9, /*hidden argument*/NULL);
		__this->set_fsm_17(L_10);
		GameObject_t3674682005 * L_11 = V_0;
		__this->set_previousGo_16(L_11);
	}

IL_0048:
	{
		PlayMakerFSM_t3799847376 * L_12 = __this->get_fsm_17();
		bool L_13 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_12, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_005a;
		}
	}
	{
		return;
	}

IL_005a:
	{
		V_1 = (bool)0;
		PlayMakerFSM_t3799847376 * L_14 = __this->get_fsm_17();
		NullCheck(L_14);
		String_t* L_15 = PlayMakerFSM_get_ActiveStateName_m1548898773(L_14, /*hidden argument*/NULL);
		FsmString_t952858651 * L_16 = __this->get_stateName_11();
		NullCheck(L_16);
		String_t* L_17 = FsmString_get_Value_m872383149(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_18 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_15, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0094;
		}
	}
	{
		Fsm_t1527112426 * L_19 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_20 = __this->get_trueEvent_12();
		NullCheck(L_19);
		Fsm_Event_m625948263(L_19, L_20, /*hidden argument*/NULL);
		V_1 = (bool)1;
		goto IL_00a5;
	}

IL_0094:
	{
		Fsm_t1527112426 * L_21 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_22 = __this->get_falseEvent_13();
		NullCheck(L_21);
		Fsm_Event_m625948263(L_21, L_22, /*hidden argument*/NULL);
	}

IL_00a5:
	{
		FsmBool_t1075959796 * L_23 = __this->get_storeResult_14();
		bool L_24 = V_1;
		NullCheck(L_23);
		FsmBool_set_Value_m1126216340(L_23, L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectChanged::.ctor()
extern "C"  void GameObjectChanged__ctor_m3955321957 (GameObjectChanged_t641634289 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectChanged::Reset()
extern "C"  void GameObjectChanged_Reset_m1601754898 (GameObjectChanged_t641634289 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObjectVariable_9((FsmGameObject_t1697147867 *)NULL);
		__this->set_changedEvent_10((FsmEvent_t2133468028 *)NULL);
		__this->set_storeResult_11((FsmBool_t1075959796 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectChanged::OnEnter()
extern "C"  void GameObjectChanged_OnEnter_m3705039612 (GameObjectChanged_t641634289 * __this, const MethodInfo* method)
{
	{
		FsmGameObject_t1697147867 * L_0 = __this->get_gameObjectVariable_9();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0017:
	{
		FsmGameObject_t1697147867 * L_2 = __this->get_gameObjectVariable_9();
		NullCheck(L_2);
		GameObject_t3674682005 * L_3 = FsmGameObject_get_Value_m673294275(L_2, /*hidden argument*/NULL);
		__this->set_previousValue_12(L_3);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectChanged::OnUpdate()
extern "C"  void GameObjectChanged_OnUpdate_m2320637639 (GameObjectChanged_t641634289 * __this, const MethodInfo* method)
{
	{
		FsmBool_t1075959796 * L_0 = __this->get_storeResult_11();
		NullCheck(L_0);
		FsmBool_set_Value_m1126216340(L_0, (bool)0, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_1 = __this->get_gameObjectVariable_9();
		NullCheck(L_1);
		GameObject_t3674682005 * L_2 = FsmGameObject_get_Value_m673294275(L_1, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_3 = __this->get_previousValue_12();
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0044;
		}
	}
	{
		FsmBool_t1075959796 * L_5 = __this->get_storeResult_11();
		NullCheck(L_5);
		FsmBool_set_Value_m1126216340(L_5, (bool)1, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_6 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_7 = __this->get_changedEvent_10();
		NullCheck(L_6);
		Fsm_Event_m625948263(L_6, L_7, /*hidden argument*/NULL);
	}

IL_0044:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompare::.ctor()
extern "C"  void GameObjectCompare__ctor_m703166804 (GameObjectCompare_t853174818 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompare::Reset()
extern "C"  void GameObjectCompare_Reset_m2644567041 (GameObjectCompare_t853174818 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObjectVariable_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_compareTo_10((FsmGameObject_t1697147867 *)NULL);
		__this->set_equalEvent_11((FsmEvent_t2133468028 *)NULL);
		__this->set_notEqualEvent_12((FsmEvent_t2133468028 *)NULL);
		__this->set_storeResult_13((FsmBool_t1075959796 *)NULL);
		__this->set_everyFrame_14((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompare::OnEnter()
extern "C"  void GameObjectCompare_OnEnter_m825161771 (GameObjectCompare_t853174818 * __this, const MethodInfo* method)
{
	{
		GameObjectCompare_DoGameObjectCompare_m2271525627(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_14();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompare::OnUpdate()
extern "C"  void GameObjectCompare_OnUpdate_m3238737784 (GameObjectCompare_t853174818 * __this, const MethodInfo* method)
{
	{
		GameObjectCompare_DoGameObjectCompare_m2271525627(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompare::DoGameObjectCompare()
extern "C"  void GameObjectCompare_DoGameObjectCompare_m2271525627 (GameObjectCompare_t853174818 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObjectVariable_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_3 = __this->get_compareTo_10();
		NullCheck(L_3);
		GameObject_t3674682005 * L_4 = FsmGameObject_get_Value_m673294275(L_3, /*hidden argument*/NULL);
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		FsmBool_t1075959796 * L_6 = __this->get_storeResult_13();
		bool L_7 = V_0;
		NullCheck(L_6);
		FsmBool_set_Value_m1126216340(L_6, L_7, /*hidden argument*/NULL);
		bool L_8 = V_0;
		if (!L_8)
		{
			goto IL_0055;
		}
	}
	{
		FsmEvent_t2133468028 * L_9 = __this->get_equalEvent_11();
		if (!L_9)
		{
			goto IL_0055;
		}
	}
	{
		Fsm_t1527112426 * L_10 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_11 = __this->get_equalEvent_11();
		NullCheck(L_10);
		Fsm_Event_m625948263(L_10, L_11, /*hidden argument*/NULL);
		goto IL_0077;
	}

IL_0055:
	{
		bool L_12 = V_0;
		if (L_12)
		{
			goto IL_0077;
		}
	}
	{
		FsmEvent_t2133468028 * L_13 = __this->get_notEqualEvent_12();
		if (!L_13)
		{
			goto IL_0077;
		}
	}
	{
		Fsm_t1527112426 * L_14 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_15 = __this->get_notEqualEvent_12();
		NullCheck(L_14);
		Fsm_Event_m625948263(L_14, L_15, /*hidden argument*/NULL);
	}

IL_0077:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompareTag::.ctor()
extern "C"  void GameObjectCompareTag__ctor_m1747131352 (GameObjectCompareTag_t3437146702 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompareTag::Reset()
extern Il2CppCodeGenString* _stringLiteral69913957;
extern const uint32_t GameObjectCompareTag_Reset_m3688531589_MetadataUsageId;
extern "C"  void GameObjectCompareTag_Reset_m3688531589 (GameObjectCompareTag_t3437146702 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObjectCompareTag_Reset_m3688531589_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_9((FsmGameObject_t1697147867 *)NULL);
		FsmString_t952858651 * L_0 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, _stringLiteral69913957, /*hidden argument*/NULL);
		__this->set_tag_10(L_0);
		__this->set_trueEvent_11((FsmEvent_t2133468028 *)NULL);
		__this->set_falseEvent_12((FsmEvent_t2133468028 *)NULL);
		__this->set_storeResult_13((FsmBool_t1075959796 *)NULL);
		__this->set_everyFrame_14((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompareTag::OnEnter()
extern "C"  void GameObjectCompareTag_OnEnter_m3347712431 (GameObjectCompareTag_t3437146702 * __this, const MethodInfo* method)
{
	{
		GameObjectCompareTag_DoCompareTag_m623402700(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_14();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompareTag::OnUpdate()
extern "C"  void GameObjectCompareTag_OnUpdate_m4128396916 (GameObjectCompareTag_t3437146702 * __this, const MethodInfo* method)
{
	{
		GameObjectCompareTag_DoCompareTag_m623402700(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompareTag::DoCompareTag()
extern "C"  void GameObjectCompareTag_DoCompareTag_m623402700 (GameObjectCompareTag_t3437146702 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	Fsm_t1527112426 * G_B4_0 = NULL;
	Fsm_t1527112426 * G_B3_0 = NULL;
	FsmEvent_t2133468028 * G_B5_0 = NULL;
	Fsm_t1527112426 * G_B5_1 = NULL;
	{
		V_0 = (bool)0;
		FsmGameObject_t1697147867 * L_0 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_1 = FsmGameObject_get_Value_m673294275(L_0, /*hidden argument*/NULL);
		bool L_2 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0034;
		}
	}
	{
		FsmGameObject_t1697147867 * L_3 = __this->get_gameObject_9();
		NullCheck(L_3);
		GameObject_t3674682005 * L_4 = FsmGameObject_get_Value_m673294275(L_3, /*hidden argument*/NULL);
		FsmString_t952858651 * L_5 = __this->get_tag_10();
		NullCheck(L_5);
		String_t* L_6 = FsmString_get_Value_m872383149(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_7 = GameObject_CompareTag_m3153977471(L_4, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
	}

IL_0034:
	{
		FsmBool_t1075959796 * L_8 = __this->get_storeResult_13();
		bool L_9 = V_0;
		NullCheck(L_8);
		FsmBool_set_Value_m1126216340(L_8, L_9, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_10 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		bool L_11 = V_0;
		G_B3_0 = L_10;
		if (!L_11)
		{
			G_B4_0 = L_10;
			goto IL_0057;
		}
	}
	{
		FsmEvent_t2133468028 * L_12 = __this->get_trueEvent_11();
		G_B5_0 = L_12;
		G_B5_1 = G_B3_0;
		goto IL_005d;
	}

IL_0057:
	{
		FsmEvent_t2133468028 * L_13 = __this->get_falseEvent_12();
		G_B5_0 = L_13;
		G_B5_1 = G_B4_0;
	}

IL_005d:
	{
		NullCheck(G_B5_1);
		Fsm_Event_m625948263(G_B5_1, G_B5_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectHasChildren::.ctor()
extern "C"  void GameObjectHasChildren__ctor_m776204032 (GameObjectHasChildren_t3474747126 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectHasChildren::Reset()
extern "C"  void GameObjectHasChildren_Reset_m2717604269 (GameObjectHasChildren_t3474747126 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_trueEvent_10((FsmEvent_t2133468028 *)NULL);
		__this->set_falseEvent_11((FsmEvent_t2133468028 *)NULL);
		__this->set_storeResult_12((FsmBool_t1075959796 *)NULL);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectHasChildren::OnEnter()
extern "C"  void GameObjectHasChildren_OnEnter_m2294461143 (GameObjectHasChildren_t3474747126 * __this, const MethodInfo* method)
{
	{
		GameObjectHasChildren_DoHasChildren_m1996182732(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectHasChildren::OnUpdate()
extern "C"  void GameObjectHasChildren_OnUpdate_m1542378060 (GameObjectHasChildren_t3474747126 * __this, const MethodInfo* method)
{
	{
		GameObjectHasChildren_DoHasChildren_m1996182732(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectHasChildren::DoHasChildren()
extern "C"  void GameObjectHasChildren_DoHasChildren_m1996182732 (GameObjectHasChildren_t3474747126 * __this, const MethodInfo* method)
{
	GameObject_t3674682005 * V_0 = NULL;
	bool V_1 = false;
	Fsm_t1527112426 * G_B4_0 = NULL;
	Fsm_t1527112426 * G_B3_0 = NULL;
	FsmEvent_t2133468028 * G_B5_0 = NULL;
	Fsm_t1527112426 * G_B5_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Transform_t1659122786 * L_6 = GameObject_get_transform_m1278640159(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = Transform_get_childCount_m2107810675(L_6, /*hidden argument*/NULL);
		V_1 = (bool)((((int32_t)L_7) > ((int32_t)0))? 1 : 0);
		FsmBool_t1075959796 * L_8 = __this->get_storeResult_12();
		bool L_9 = V_1;
		NullCheck(L_8);
		FsmBool_set_Value_m1126216340(L_8, L_9, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_10 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		bool L_11 = V_1;
		G_B3_0 = L_10;
		if (!L_11)
		{
			G_B4_0 = L_10;
			goto IL_0051;
		}
	}
	{
		FsmEvent_t2133468028 * L_12 = __this->get_trueEvent_10();
		G_B5_0 = L_12;
		G_B5_1 = G_B3_0;
		goto IL_0057;
	}

IL_0051:
	{
		FsmEvent_t2133468028 * L_13 = __this->get_falseEvent_11();
		G_B5_0 = L_13;
		G_B5_1 = G_B4_0;
	}

IL_0057:
	{
		NullCheck(G_B5_1);
		Fsm_Event_m625948263(G_B5_1, G_B5_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsChildOf::.ctor()
extern "C"  void GameObjectIsChildOf__ctor_m3409596848 (GameObjectIsChildOf_t1849565254 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsChildOf::Reset()
extern "C"  void GameObjectIsChildOf_Reset_m1056029789 (GameObjectIsChildOf_t1849565254 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_isChildOf_10((FsmGameObject_t1697147867 *)NULL);
		__this->set_trueEvent_11((FsmEvent_t2133468028 *)NULL);
		__this->set_falseEvent_12((FsmEvent_t2133468028 *)NULL);
		__this->set_storeResult_13((FsmBool_t1075959796 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsChildOf::OnEnter()
extern "C"  void GameObjectIsChildOf_OnEnter_m3249219975 (GameObjectIsChildOf_t1849565254 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		GameObjectIsChildOf_DoIsChildOf_m970755908(__this, L_2, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsChildOf::DoIsChildOf(UnityEngine.GameObject)
extern "C"  void GameObjectIsChildOf_DoIsChildOf_m970755908 (GameObjectIsChildOf_t1849565254 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method)
{
	bool V_0 = false;
	Fsm_t1527112426 * G_B5_0 = NULL;
	Fsm_t1527112426 * G_B4_0 = NULL;
	FsmEvent_t2133468028 * G_B6_0 = NULL;
	Fsm_t1527112426 * G_B6_1 = NULL;
	{
		GameObject_t3674682005 * L_0 = ___go0;
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0017;
		}
	}
	{
		FsmGameObject_t1697147867 * L_2 = __this->get_isChildOf_10();
		if (L_2)
		{
			goto IL_0018;
		}
	}

IL_0017:
	{
		return;
	}

IL_0018:
	{
		GameObject_t3674682005 * L_3 = ___go0;
		NullCheck(L_3);
		Transform_t1659122786 * L_4 = GameObject_get_transform_m1278640159(L_3, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_5 = __this->get_isChildOf_10();
		NullCheck(L_5);
		GameObject_t3674682005 * L_6 = FsmGameObject_get_Value_m673294275(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_t1659122786 * L_7 = GameObject_get_transform_m1278640159(L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_8 = Transform_IsChildOf_m3321648579(L_4, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		FsmBool_t1075959796 * L_9 = __this->get_storeResult_13();
		bool L_10 = V_0;
		NullCheck(L_9);
		FsmBool_set_Value_m1126216340(L_9, L_10, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_11 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		bool L_12 = V_0;
		G_B4_0 = L_11;
		if (!L_12)
		{
			G_B5_0 = L_11;
			goto IL_0057;
		}
	}
	{
		FsmEvent_t2133468028 * L_13 = __this->get_trueEvent_11();
		G_B6_0 = L_13;
		G_B6_1 = G_B4_0;
		goto IL_005d;
	}

IL_0057:
	{
		FsmEvent_t2133468028 * L_14 = __this->get_falseEvent_12();
		G_B6_0 = L_14;
		G_B6_1 = G_B5_0;
	}

IL_005d:
	{
		NullCheck(G_B6_1);
		Fsm_Event_m625948263(G_B6_1, G_B6_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsNull::.ctor()
extern "C"  void GameObjectIsNull__ctor_m438504188 (GameObjectIsNull_t2162669226 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsNull::Reset()
extern "C"  void GameObjectIsNull_Reset_m2379904425 (GameObjectIsNull_t2162669226 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmGameObject_t1697147867 *)NULL);
		__this->set_isNull_10((FsmEvent_t2133468028 *)NULL);
		__this->set_isNotNull_11((FsmEvent_t2133468028 *)NULL);
		__this->set_storeResult_12((FsmBool_t1075959796 *)NULL);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsNull::OnEnter()
extern "C"  void GameObjectIsNull_OnEnter_m4182425555 (GameObjectIsNull_t2162669226 * __this, const MethodInfo* method)
{
	{
		GameObjectIsNull_DoIsGameObjectNull_m1826385077(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsNull::OnUpdate()
extern "C"  void GameObjectIsNull_OnUpdate_m4234699984 (GameObjectIsNull_t2162669226 * __this, const MethodInfo* method)
{
	{
		GameObjectIsNull_DoIsGameObjectNull_m1826385077(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsNull::DoIsGameObjectNull()
extern "C"  void GameObjectIsNull_DoIsGameObjectNull_m1826385077 (GameObjectIsNull_t2162669226 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	Fsm_t1527112426 * G_B4_0 = NULL;
	Fsm_t1527112426 * G_B3_0 = NULL;
	FsmEvent_t2133468028 * G_B5_0 = NULL;
	Fsm_t1527112426 * G_B5_1 = NULL;
	{
		FsmGameObject_t1697147867 * L_0 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_1 = FsmGameObject_get_Value_m673294275(L_0, /*hidden argument*/NULL);
		bool L_2 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		V_0 = L_2;
		FsmBool_t1075959796 * L_3 = __this->get_storeResult_12();
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		FsmBool_t1075959796 * L_4 = __this->get_storeResult_12();
		bool L_5 = V_0;
		NullCheck(L_4);
		FsmBool_set_Value_m1126216340(L_4, L_5, /*hidden argument*/NULL);
	}

IL_0029:
	{
		Fsm_t1527112426 * L_6 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		bool L_7 = V_0;
		G_B3_0 = L_6;
		if (!L_7)
		{
			G_B4_0 = L_6;
			goto IL_0040;
		}
	}
	{
		FsmEvent_t2133468028 * L_8 = __this->get_isNull_10();
		G_B5_0 = L_8;
		G_B5_1 = G_B3_0;
		goto IL_0046;
	}

IL_0040:
	{
		FsmEvent_t2133468028 * L_9 = __this->get_isNotNull_11();
		G_B5_0 = L_9;
		G_B5_1 = G_B4_0;
	}

IL_0046:
	{
		NullCheck(G_B5_1);
		Fsm_Event_m625948263(G_B5_1, G_B5_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsVisible::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m994342140_MethodInfo_var;
extern const uint32_t GameObjectIsVisible__ctor_m3292890161_MetadataUsageId;
extern "C"  void GameObjectIsVisible__ctor_m3292890161 (GameObjectIsVisible_t1570039973 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObjectIsVisible__ctor_m3292890161_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m994342140(__this, /*hidden argument*/ComponentAction_1__ctor_m994342140_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsVisible::Reset()
extern "C"  void GameObjectIsVisible_Reset_m939323102 (GameObjectIsVisible_t1570039973 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_trueEvent_12((FsmEvent_t2133468028 *)NULL);
		__this->set_falseEvent_13((FsmEvent_t2133468028 *)NULL);
		__this->set_storeResult_14((FsmBool_t1075959796 *)NULL);
		__this->set_everyFrame_15((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsVisible::OnEnter()
extern "C"  void GameObjectIsVisible_OnEnter_m2763243464 (GameObjectIsVisible_t1570039973 * __this, const MethodInfo* method)
{
	{
		GameObjectIsVisible_DoIsVisible_m1769454380(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsVisible::OnUpdate()
extern "C"  void GameObjectIsVisible_OnUpdate_m3189728123 (GameObjectIsVisible_t1570039973 * __this, const MethodInfo* method)
{
	{
		GameObjectIsVisible_DoIsVisible_m1769454380(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsVisible::DoIsVisible()
extern const MethodInfo* ComponentAction_1_UpdateCache_m3595067967_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_renderer_m773530769_MethodInfo_var;
extern const uint32_t GameObjectIsVisible_DoIsVisible_m1769454380_MetadataUsageId;
extern "C"  void GameObjectIsVisible_DoIsVisible_m1769454380 (GameObjectIsVisible_t1570039973 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObjectIsVisible_DoIsVisible_m1769454380_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	bool V_1 = false;
	Fsm_t1527112426 * G_B3_0 = NULL;
	Fsm_t1527112426 * G_B2_0 = NULL;
	FsmEvent_t2133468028 * G_B4_0 = NULL;
	Fsm_t1527112426 * G_B4_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m3595067967(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m3595067967_MethodInfo_var);
		if (!L_4)
		{
			goto IL_0058;
		}
	}
	{
		Renderer_t3076687687 * L_5 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_5);
		bool L_6 = Renderer_get_isVisible_m1011967393(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		FsmBool_t1075959796 * L_7 = __this->get_storeResult_14();
		bool L_8 = V_1;
		NullCheck(L_7);
		FsmBool_set_Value_m1126216340(L_7, L_8, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_9 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		bool L_10 = V_1;
		G_B2_0 = L_9;
		if (!L_10)
		{
			G_B3_0 = L_9;
			goto IL_004d;
		}
	}
	{
		FsmEvent_t2133468028 * L_11 = __this->get_trueEvent_12();
		G_B4_0 = L_11;
		G_B4_1 = G_B2_0;
		goto IL_0053;
	}

IL_004d:
	{
		FsmEvent_t2133468028 * L_12 = __this->get_falseEvent_13();
		G_B4_0 = L_12;
		G_B4_1 = G_B3_0;
	}

IL_0053:
	{
		NullCheck(G_B4_1);
		Fsm_Event_m625948263(G_B4_1, G_B4_0, /*hidden argument*/NULL);
	}

IL_0058:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectTagSwitch::.ctor()
extern "C"  void GameObjectTagSwitch__ctor_m2134117227 (GameObjectTagSwitch_t3433592875 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectTagSwitch::Reset()
extern Il2CppClass* FsmStringU5BU5D_t2523845914_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmEventU5BU5D_t2862142229_il2cpp_TypeInfo_var;
extern const uint32_t GameObjectTagSwitch_Reset_m4075517464_MetadataUsageId;
extern "C"  void GameObjectTagSwitch_Reset_m4075517464 (GameObjectTagSwitch_t3433592875 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObjectTagSwitch_Reset_m4075517464_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_9((FsmGameObject_t1697147867 *)NULL);
		__this->set_compareTo_10(((FsmStringU5BU5D_t2523845914*)SZArrayNew(FsmStringU5BU5D_t2523845914_il2cpp_TypeInfo_var, (uint32_t)1)));
		__this->set_sendEvent_11(((FsmEventU5BU5D_t2862142229*)SZArrayNew(FsmEventU5BU5D_t2862142229_il2cpp_TypeInfo_var, (uint32_t)1)));
		__this->set_everyFrame_12((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectTagSwitch::OnEnter()
extern "C"  void GameObjectTagSwitch_OnEnter_m1578983554 (GameObjectTagSwitch_t3433592875 * __this, const MethodInfo* method)
{
	{
		GameObjectTagSwitch_DoTagSwitch_m398210284(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_12();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectTagSwitch::OnUpdate()
extern "C"  void GameObjectTagSwitch_OnUpdate_m837409281 (GameObjectTagSwitch_t3433592875 * __this, const MethodInfo* method)
{
	{
		GameObjectTagSwitch_DoTagSwitch_m398210284(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectTagSwitch::DoTagSwitch()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObjectTagSwitch_DoTagSwitch_m398210284_MetadataUsageId;
extern "C"  void GameObjectTagSwitch_DoTagSwitch_m398210284 (GameObjectTagSwitch_t3433592875 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObjectTagSwitch_DoTagSwitch_m398210284_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		FsmGameObject_t1697147867 * L_0 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_1 = FsmGameObject_get_Value_m673294275(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t3674682005 * L_2 = V_0;
		bool L_3 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_2, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0019;
		}
	}
	{
		return;
	}

IL_0019:
	{
		V_1 = 0;
		goto IL_0055;
	}

IL_0020:
	{
		GameObject_t3674682005 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = GameObject_get_tag_m211612200(L_4, /*hidden argument*/NULL);
		FsmStringU5BU5D_t2523845914* L_6 = __this->get_compareTo_10();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		FsmString_t952858651 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_9);
		String_t* L_10 = FsmString_get_Value_m872383149(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_5, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0051;
		}
	}
	{
		Fsm_t1527112426 * L_12 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEventU5BU5D_t2862142229* L_13 = __this->get_sendEvent_11();
		int32_t L_14 = V_1;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		FsmEvent_t2133468028 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_12);
		Fsm_Event_m625948263(L_12, L_16, /*hidden argument*/NULL);
		return;
	}

IL_0051:
	{
		int32_t L_17 = V_1;
		V_1 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_0055:
	{
		int32_t L_18 = V_1;
		FsmStringU5BU5D_t2523845914* L_19 = __this->get_compareTo_10();
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_19)->max_length)))))))
		{
			goto IL_0020;
		}
	}
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAngleToTarget::.ctor()
extern "C"  void GetAngleToTarget__ctor_m1451390901 (GetAngleToTarget_t1034773969 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAngleToTarget::Reset()
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern const uint32_t GetAngleToTarget_Reset_m3392791138_MetadataUsageId;
extern "C"  void GetAngleToTarget_Reset_m3392791138 (GetAngleToTarget_t1034773969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAngleToTarget_Reset_m3392791138_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmVector3_t533912882 * V_0 = NULL;
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_targetObject_10((FsmGameObject_t1697147867 *)NULL);
		FsmVector3_t533912882 * L_0 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmVector3_t533912882 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_2 = V_0;
		__this->set_targetPosition_11(L_2);
		FsmBool_t1075959796 * L_3 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_ignoreHeight_12(L_3);
		__this->set_storeAngle_13((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_14((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAngleToTarget::OnLateUpdate()
extern "C"  void GetAngleToTarget_OnLateUpdate_m3267609533 (GetAngleToTarget_t1034773969 * __this, const MethodInfo* method)
{
	{
		GetAngleToTarget_DoGetAngleToTarget_m677929539(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_14();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAngleToTarget::DoGetAngleToTarget()
extern "C"  void GetAngleToTarget_DoGetAngleToTarget_m677929539 (GetAngleToTarget_t1034773969 * __this, const MethodInfo* method)
{
	GameObject_t3674682005 * V_0 = NULL;
	GameObject_t3674682005 * V_1 = NULL;
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t4282066566  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t4282066566  G_B9_0;
	memset(&G_B9_0, 0, sizeof(G_B9_0));
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		FsmGameObject_t1697147867 * L_5 = __this->get_targetObject_10();
		NullCheck(L_5);
		GameObject_t3674682005 * L_6 = FsmGameObject_get_Value_m673294275(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		GameObject_t3674682005 * L_7 = V_1;
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0048;
		}
	}
	{
		FsmVector3_t533912882 * L_9 = __this->get_targetPosition_11();
		NullCheck(L_9);
		bool L_10 = NamedVariable_get_IsNone_m281035543(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0048;
		}
	}
	{
		return;
	}

IL_0048:
	{
		GameObject_t3674682005 * L_11 = V_1;
		bool L_12 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_11, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0090;
		}
	}
	{
		FsmVector3_t533912882 * L_13 = __this->get_targetPosition_11();
		NullCheck(L_13);
		bool L_14 = NamedVariable_get_IsNone_m281035543(L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_007f;
		}
	}
	{
		GameObject_t3674682005 * L_15 = V_1;
		NullCheck(L_15);
		Transform_t1659122786 * L_16 = GameObject_get_transform_m1278640159(L_15, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_17 = __this->get_targetPosition_11();
		NullCheck(L_17);
		Vector3_t4282066566  L_18 = FsmVector3_get_Value_m2779135117(L_17, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector3_t4282066566  L_19 = Transform_TransformPoint_m437395512(L_16, L_18, /*hidden argument*/NULL);
		G_B9_0 = L_19;
		goto IL_008a;
	}

IL_007f:
	{
		GameObject_t3674682005 * L_20 = V_1;
		NullCheck(L_20);
		Transform_t1659122786 * L_21 = GameObject_get_transform_m1278640159(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		Vector3_t4282066566  L_22 = Transform_get_position_m2211398607(L_21, /*hidden argument*/NULL);
		G_B9_0 = L_22;
	}

IL_008a:
	{
		V_2 = G_B9_0;
		goto IL_009c;
	}

IL_0090:
	{
		FsmVector3_t533912882 * L_23 = __this->get_targetPosition_11();
		NullCheck(L_23);
		Vector3_t4282066566  L_24 = FsmVector3_get_Value_m2779135117(L_23, /*hidden argument*/NULL);
		V_2 = L_24;
	}

IL_009c:
	{
		FsmBool_t1075959796 * L_25 = __this->get_ignoreHeight_12();
		NullCheck(L_25);
		bool L_26 = FsmBool_get_Value_m3101329097(L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00c7;
		}
	}
	{
		GameObject_t3674682005 * L_27 = V_0;
		NullCheck(L_27);
		Transform_t1659122786 * L_28 = GameObject_get_transform_m1278640159(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		Vector3_t4282066566  L_29 = Transform_get_position_m2211398607(L_28, /*hidden argument*/NULL);
		V_4 = L_29;
		float L_30 = (&V_4)->get_y_2();
		(&V_2)->set_y_2(L_30);
	}

IL_00c7:
	{
		Vector3_t4282066566  L_31 = V_2;
		GameObject_t3674682005 * L_32 = V_0;
		NullCheck(L_32);
		Transform_t1659122786 * L_33 = GameObject_get_transform_m1278640159(L_32, /*hidden argument*/NULL);
		NullCheck(L_33);
		Vector3_t4282066566  L_34 = Transform_get_position_m2211398607(L_33, /*hidden argument*/NULL);
		Vector3_t4282066566  L_35 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_31, L_34, /*hidden argument*/NULL);
		V_3 = L_35;
		FsmFloat_t2134102846 * L_36 = __this->get_storeAngle_13();
		Vector3_t4282066566  L_37 = V_3;
		GameObject_t3674682005 * L_38 = V_0;
		NullCheck(L_38);
		Transform_t1659122786 * L_39 = GameObject_get_transform_m1278640159(L_38, /*hidden argument*/NULL);
		NullCheck(L_39);
		Vector3_t4282066566  L_40 = Transform_get_forward_m877665793(L_39, /*hidden argument*/NULL);
		float L_41 = Vector3_Angle_m1904328934(NULL /*static, unused*/, L_37, L_40, /*hidden argument*/NULL);
		NullCheck(L_36);
		FsmFloat_set_Value_m1568963140(L_36, L_41, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorApplyRootMotion::.ctor()
extern "C"  void GetAnimatorApplyRootMotion__ctor_m1420210421 (GetAnimatorApplyRootMotion_t212715153 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorApplyRootMotion::Reset()
extern "C"  void GetAnimatorApplyRootMotion_Reset_m3361610658 (GetAnimatorApplyRootMotion_t212715153 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_rootMotionApplied_10((FsmBool_t1075959796 *)NULL);
		__this->set_rootMotionIsAppliedEvent_11((FsmEvent_t2133468028 *)NULL);
		__this->set_rootMotionIsNotAppliedEvent_12((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorApplyRootMotion::OnEnter()
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorApplyRootMotion_OnEnter_m2709310348_MetadataUsageId;
extern "C"  void GetAnimatorApplyRootMotion_OnEnter_m2709310348 (GetAnimatorApplyRootMotion_t212715153 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorApplyRootMotion_OnEnter_m2709310348_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_13(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_13();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorApplyRootMotion_GetApplyMotionRoot_m1558234815(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorApplyRootMotion::GetApplyMotionRoot()
extern "C"  void GetAnimatorApplyRootMotion_GetApplyMotionRoot_m1558234815 (GetAnimatorApplyRootMotion_t212715153 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Animator_t2776330603 * L_0 = __this->get__animator_13();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0056;
		}
	}
	{
		Animator_t2776330603 * L_2 = __this->get__animator_13();
		NullCheck(L_2);
		bool L_3 = Animator_get_applyRootMotion_m2388146907(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		FsmBool_t1075959796 * L_4 = __this->get_rootMotionApplied_10();
		bool L_5 = V_0;
		NullCheck(L_4);
		FsmBool_set_Value_m1126216340(L_4, L_5, /*hidden argument*/NULL);
		bool L_6 = V_0;
		if (!L_6)
		{
			goto IL_0045;
		}
	}
	{
		Fsm_t1527112426 * L_7 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_8 = __this->get_rootMotionIsAppliedEvent_11();
		NullCheck(L_7);
		Fsm_Event_m625948263(L_7, L_8, /*hidden argument*/NULL);
		goto IL_0056;
	}

IL_0045:
	{
		Fsm_t1527112426 * L_9 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_10 = __this->get_rootMotionIsNotAppliedEvent_12();
		NullCheck(L_9);
		Fsm_Event_m625948263(L_9, L_10, /*hidden argument*/NULL);
	}

IL_0056:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBody::.ctor()
extern "C"  void GetAnimatorBody__ctor_m1903242537 (GetAnimatorBody_t3376544941 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBody::Reset()
extern "C"  void GetAnimatorBody_Reset_m3844642774 (GetAnimatorBody_t3376544941 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_bodyPosition_11((FsmVector3_t533912882 *)NULL);
		__this->set_bodyRotation_12((FsmQuaternion_t3871136040 *)NULL);
		__this->set_bodyGameObject_13((FsmGameObject_t1697147867 *)NULL);
		__this->set_everyFrame_10((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBody::OnEnter()
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623_MethodInfo_var;
extern const MethodInfo* GetAnimatorBody_OnAnimatorMoveEvent_m1268613386_MethodInfo_var;
extern const uint32_t GetAnimatorBody_OnEnter_m3046705856_MetadataUsageId;
extern "C"  void GetAnimatorBody_OnEnter_m3046705856 (GetAnimatorBody_t3376544941 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorBody_OnEnter_m3046705856_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	GameObject_t3674682005 * V_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_15(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_15();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GameObject_t3674682005 * L_9 = V_0;
		NullCheck(L_9);
		PlayMakerAnimatorMoveProxy_t4175490694 * L_10 = GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623(L_9, /*hidden argument*/GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623_MethodInfo_var);
		__this->set__animatorProxy_14(L_10);
		PlayMakerAnimatorMoveProxy_t4175490694 * L_11 = __this->get__animatorProxy_14();
		bool L_12 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_11, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_007d;
		}
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_13 = __this->get__animatorProxy_14();
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)GetAnimatorBody_OnAnimatorMoveEvent_m1268613386_MethodInfo_var);
		Action_t3771233898 * L_15 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_15, __this, L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		PlayMakerAnimatorMoveProxy_add_OnAnimatorMoveEvent_m2003874533(L_13, L_15, /*hidden argument*/NULL);
	}

IL_007d:
	{
		FsmGameObject_t1697147867 * L_16 = __this->get_bodyGameObject_13();
		NullCheck(L_16);
		GameObject_t3674682005 * L_17 = FsmGameObject_get_Value_m673294275(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		GameObject_t3674682005 * L_18 = V_1;
		bool L_19 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_18, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00a1;
		}
	}
	{
		GameObject_t3674682005 * L_20 = V_1;
		NullCheck(L_20);
		Transform_t1659122786 * L_21 = GameObject_get_transform_m1278640159(L_20, /*hidden argument*/NULL);
		__this->set__transform_16(L_21);
	}

IL_00a1:
	{
		GetAnimatorBody_DoGetBodyPosition_m4155442973(__this, /*hidden argument*/NULL);
		bool L_22 = __this->get_everyFrame_10();
		if (L_22)
		{
			goto IL_00b8;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBody::OnAnimatorMoveEvent()
extern "C"  void GetAnimatorBody_OnAnimatorMoveEvent_m1268613386 (GetAnimatorBody_t3376544941 * __this, const MethodInfo* method)
{
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_14();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		GetAnimatorBody_DoGetBodyPosition_m4155442973(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBody::OnUpdate()
extern "C"  void GetAnimatorBody_OnUpdate_m3387127683 (GetAnimatorBody_t3376544941 * __this, const MethodInfo* method)
{
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_14();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		GetAnimatorBody_DoGetBodyPosition_m4155442973(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBody::DoGetBodyPosition()
extern "C"  void GetAnimatorBody_DoGetBodyPosition_m4155442973 (GetAnimatorBody_t3376544941 * __this, const MethodInfo* method)
{
	{
		Animator_t2776330603 * L_0 = __this->get__animator_15();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FsmVector3_t533912882 * L_2 = __this->get_bodyPosition_11();
		Animator_t2776330603 * L_3 = __this->get__animator_15();
		NullCheck(L_3);
		Vector3_t4282066566  L_4 = Animator_get_bodyPosition_m2404872492(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmVector3_set_Value_m716982822(L_2, L_4, /*hidden argument*/NULL);
		FsmQuaternion_t3871136040 * L_5 = __this->get_bodyRotation_12();
		Animator_t2776330603 * L_6 = __this->get__animator_15();
		NullCheck(L_6);
		Quaternion_t1553702882  L_7 = Animator_get_bodyRotation_m994115073(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		FsmQuaternion_set_Value_m446581172(L_5, L_7, /*hidden argument*/NULL);
		Transform_t1659122786 * L_8 = __this->get__transform_16();
		bool L_9 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_8, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_007b;
		}
	}
	{
		Transform_t1659122786 * L_10 = __this->get__transform_16();
		Animator_t2776330603 * L_11 = __this->get__animator_15();
		NullCheck(L_11);
		Vector3_t4282066566  L_12 = Animator_get_bodyPosition_m2404872492(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_set_position_m3111394108(L_10, L_12, /*hidden argument*/NULL);
		Transform_t1659122786 * L_13 = __this->get__transform_16();
		Animator_t2776330603 * L_14 = __this->get__animator_15();
		NullCheck(L_14);
		Quaternion_t1553702882  L_15 = Animator_get_bodyRotation_m994115073(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_set_rotation_m1525803229(L_13, L_15, /*hidden argument*/NULL);
	}

IL_007b:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBody::OnExit()
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const MethodInfo* GetAnimatorBody_OnAnimatorMoveEvent_m1268613386_MethodInfo_var;
extern const uint32_t GetAnimatorBody_OnExit_m384295128_MetadataUsageId;
extern "C"  void GetAnimatorBody_OnExit_m384295128 (GetAnimatorBody_t3376544941 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorBody_OnExit_m384295128_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_14();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_2 = __this->get__animatorProxy_14();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)GetAnimatorBody_OnAnimatorMoveEvent_m1268613386_MethodInfo_var);
		Action_t3771233898 * L_4 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_4, __this, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		PlayMakerAnimatorMoveProxy_remove_OnAnimatorMoveEvent_m2967347206(L_2, L_4, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBoneGameObject::.ctor()
extern "C"  void GetAnimatorBoneGameObject__ctor_m891884150 (GetAnimatorBoneGameObject_t14629568 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBoneGameObject::Reset()
extern Il2CppClass* FsmString_t952858651_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorBoneGameObject_Reset_m2833284387_MetadataUsageId;
extern "C"  void GetAnimatorBoneGameObject_Reset_m2833284387 (GetAnimatorBoneGameObject_t14629568 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorBoneGameObject_Reset_m2833284387_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmString_t952858651 * V_0 = NULL;
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_bone_10(0);
		FsmString_t952858651 * L_0 = (FsmString_t952858651 *)il2cpp_codegen_object_new(FsmString_t952858651_il2cpp_TypeInfo_var);
		FsmString__ctor_m2679108260(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmString_t952858651 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)0, /*hidden argument*/NULL);
		FsmString_t952858651 * L_2 = V_0;
		__this->set_boneAsString_11(L_2);
		__this->set_boneGameObject_12((FsmGameObject_t1697147867 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBoneGameObject::OnEnter()
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorBoneGameObject_OnEnter_m1793904845_MetadataUsageId;
extern "C"  void GetAnimatorBoneGameObject_OnEnter_m1793904845 (GetAnimatorBoneGameObject_t14629568 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorBoneGameObject_OnEnter_m1793904845_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_13(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_13();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorBoneGameObject_GetBoneTransform_m2228430176(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBoneGameObject::GetBoneTransform()
extern const Il2CppType* HumanBodyBones_t1606609988_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2862688501_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorBoneGameObject_GetBoneTransform_m2228430176_MetadataUsageId;
extern "C"  void GetAnimatorBoneGameObject_GetBoneTransform_m2228430176 (GetAnimatorBoneGameObject_t14629568 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorBoneGameObject_GetBoneTransform_m2228430176_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		FsmString_t952858651 * L_0 = __this->get_boneAsString_11();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		int32_t L_2 = __this->get_bone_10();
		G_B3_0 = ((int32_t)(L_2));
		goto IL_003b;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(HumanBodyBones_t1606609988_0_0_0_var), /*hidden argument*/NULL);
		FsmString_t952858651 * L_4 = __this->get_boneAsString_11();
		NullCheck(L_4);
		String_t* L_5 = FsmString_get_Value_m872383149(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = Enum_Parse_m1799348482(NULL /*static, unused*/, L_3, L_5, (bool)1, /*hidden argument*/NULL);
		G_B3_0 = ((*(int32_t*)((int32_t*)UnBox (L_6, Int32_t1153838500_il2cpp_TypeInfo_var))));
	}

IL_003b:
	{
		V_0 = G_B3_0;
		FsmGameObject_t1697147867 * L_7 = __this->get_boneGameObject_12();
		Animator_t2776330603 * L_8 = __this->get__animator_13();
		int32_t L_9 = V_0;
		NullCheck(L_8);
		Transform_t1659122786 * L_10 = Animator_GetBoneTransform_m3449809847(L_8, L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_t3674682005 * L_11 = Component_get_gameObject_m1170635899(L_10, /*hidden argument*/NULL);
		NullCheck(L_7);
		FsmGameObject_set_Value_m297051598(L_7, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBool::.ctor()
extern "C"  void GetAnimatorBool__ctor_m1871322337 (GetAnimatorBool_t3376545269 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBool::Reset()
extern "C"  void GetAnimatorBool_Reset_m3812722574 (GetAnimatorBool_t3376545269 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_parameter_10((FsmString_t952858651 *)NULL);
		__this->set_result_12((FsmBool_t1075959796 *)NULL);
		__this->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBool::OnEnter()
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623_MethodInfo_var;
extern const MethodInfo* GetAnimatorBool_OnAnimatorMoveEvent_m765234882_MethodInfo_var;
extern const uint32_t GetAnimatorBool_OnEnter_m2436164728_MetadataUsageId;
extern "C"  void GetAnimatorBool_OnEnter_m2436164728 (GetAnimatorBool_t3376545269 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorBool_OnEnter_m2436164728_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_14(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_14();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GameObject_t3674682005 * L_9 = V_0;
		NullCheck(L_9);
		PlayMakerAnimatorMoveProxy_t4175490694 * L_10 = GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623(L_9, /*hidden argument*/GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623_MethodInfo_var);
		__this->set__animatorProxy_13(L_10);
		PlayMakerAnimatorMoveProxy_t4175490694 * L_11 = __this->get__animatorProxy_13();
		bool L_12 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_11, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_007d;
		}
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_13 = __this->get__animatorProxy_13();
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)GetAnimatorBool_OnAnimatorMoveEvent_m765234882_MethodInfo_var);
		Action_t3771233898 * L_15 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_15, __this, L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		PlayMakerAnimatorMoveProxy_add_OnAnimatorMoveEvent_m2003874533(L_13, L_15, /*hidden argument*/NULL);
	}

IL_007d:
	{
		FsmString_t952858651 * L_16 = __this->get_parameter_10();
		NullCheck(L_16);
		String_t* L_17 = FsmString_get_Value_m872383149(L_16, /*hidden argument*/NULL);
		int32_t L_18 = Animator_StringToHash_m4020897098(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		__this->set__paramID_15(L_18);
		GetAnimatorBool_GetParameter_m1210073718(__this, /*hidden argument*/NULL);
		bool L_19 = __this->get_everyFrame_11();
		if (L_19)
		{
			goto IL_00aa;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_00aa:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBool::OnAnimatorMoveEvent()
extern "C"  void GetAnimatorBool_OnAnimatorMoveEvent_m765234882 (GetAnimatorBool_t3376545269 * __this, const MethodInfo* method)
{
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_13();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		GetAnimatorBool_GetParameter_m1210073718(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBool::OnUpdate()
extern "C"  void GetAnimatorBool_OnUpdate_m1640221899 (GetAnimatorBool_t3376545269 * __this, const MethodInfo* method)
{
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_13();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		GetAnimatorBool_GetParameter_m1210073718(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBool::GetParameter()
extern "C"  void GetAnimatorBool_GetParameter_m1210073718 (GetAnimatorBool_t3376545269 * __this, const MethodInfo* method)
{
	{
		Animator_t2776330603 * L_0 = __this->get__animator_14();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		FsmBool_t1075959796 * L_2 = __this->get_result_12();
		Animator_t2776330603 * L_3 = __this->get__animator_14();
		int32_t L_4 = __this->get__paramID_15();
		NullCheck(L_3);
		bool L_5 = Animator_GetBool_m1246282447(L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmBool_set_Value_m1126216340(L_2, L_5, /*hidden argument*/NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBool::OnExit()
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const MethodInfo* GetAnimatorBool_OnAnimatorMoveEvent_m765234882_MethodInfo_var;
extern const uint32_t GetAnimatorBool_OnExit_m3689736224_MetadataUsageId;
extern "C"  void GetAnimatorBool_OnExit_m3689736224 (GetAnimatorBool_t3376545269 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorBool_OnExit_m3689736224_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_13();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_2 = __this->get__animatorProxy_13();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)GetAnimatorBool_OnAnimatorMoveEvent_m765234882_MethodInfo_var);
		Action_t3771233898 * L_4 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_4, __this, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		PlayMakerAnimatorMoveProxy_remove_OnAnimatorMoveEvent_m2967347206(L_2, L_4, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCullingMode::.ctor()
extern "C"  void GetAnimatorCullingMode__ctor_m1074548200 (GetAnimatorCullingMode_t994674750 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCullingMode::Reset()
extern "C"  void GetAnimatorCullingMode_Reset_m3015948437 (GetAnimatorCullingMode_t994674750 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_alwaysAnimate_10((FsmBool_t1075959796 *)NULL);
		__this->set_alwaysAnimateEvent_11((FsmEvent_t2133468028 *)NULL);
		__this->set_basedOnRenderersEvent_12((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCullingMode::OnEnter()
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorCullingMode_OnEnter_m1240397759_MetadataUsageId;
extern "C"  void GetAnimatorCullingMode_OnEnter_m1240397759 (GetAnimatorCullingMode_t994674750 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorCullingMode_OnEnter_m1240397759_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_13(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_13();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorCullingMode_DoCheckCulling_m2645242639(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCullingMode::DoCheckCulling()
extern "C"  void GetAnimatorCullingMode_DoCheckCulling_m2645242639 (GetAnimatorCullingMode_t994674750 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		Animator_t2776330603 * L_0 = __this->get__animator_13();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Animator_t2776330603 * L_2 = __this->get__animator_13();
		NullCheck(L_2);
		int32_t L_3 = Animator_get_cullingMode_m1008819440(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0028;
		}
	}
	{
		G_B5_0 = 1;
		goto IL_0029;
	}

IL_0028:
	{
		G_B5_0 = 0;
	}

IL_0029:
	{
		V_0 = (bool)G_B5_0;
		FsmBool_t1075959796 * L_4 = __this->get_alwaysAnimate_10();
		bool L_5 = V_0;
		NullCheck(L_4);
		FsmBool_set_Value_m1126216340(L_4, L_5, /*hidden argument*/NULL);
		bool L_6 = V_0;
		if (!L_6)
		{
			goto IL_0052;
		}
	}
	{
		Fsm_t1527112426 * L_7 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_8 = __this->get_alwaysAnimateEvent_11();
		NullCheck(L_7);
		Fsm_Event_m625948263(L_7, L_8, /*hidden argument*/NULL);
		goto IL_0063;
	}

IL_0052:
	{
		Fsm_t1527112426 * L_9 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_10 = __this->get_basedOnRenderersEvent_12();
		NullCheck(L_9);
		Fsm_Event_m625948263(L_9, L_10, /*hidden argument*/NULL);
	}

IL_0063:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::.ctor()
extern "C"  void GetAnimatorCurrentStateInfo__ctor_m3574624229 (GetAnimatorCurrentStateInfo_t2823805553 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::Reset()
extern "C"  void GetAnimatorCurrentStateInfo_Reset_m1221057170 (GetAnimatorCurrentStateInfo_t2823805553 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_layerIndex_10((FsmInt_t1596138449 *)NULL);
		__this->set_name_12((FsmString_t952858651 *)NULL);
		__this->set_nameHash_13((FsmInt_t1596138449 *)NULL);
		__this->set_fullPathHash_14((FsmInt_t1596138449 *)NULL);
		__this->set_shortPathHash_15((FsmInt_t1596138449 *)NULL);
		__this->set_tagHash_16((FsmInt_t1596138449 *)NULL);
		__this->set_length_18((FsmFloat_t2134102846 *)NULL);
		__this->set_normalizedTime_19((FsmFloat_t2134102846 *)NULL);
		__this->set_isStateLooping_17((FsmBool_t1075959796 *)NULL);
		__this->set_loopCount_20((FsmInt_t1596138449 *)NULL);
		__this->set_currentLoopProgress_21((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::OnEnter()
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623_MethodInfo_var;
extern const MethodInfo* GetAnimatorCurrentStateInfo_OnAnimatorMoveEvent_m4219685574_MethodInfo_var;
extern const uint32_t GetAnimatorCurrentStateInfo_OnEnter_m2926743164_MetadataUsageId;
extern "C"  void GetAnimatorCurrentStateInfo_OnEnter_m2926743164 (GetAnimatorCurrentStateInfo_t2823805553 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorCurrentStateInfo_OnEnter_m2926743164_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_23(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_23();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GameObject_t3674682005 * L_9 = V_0;
		NullCheck(L_9);
		PlayMakerAnimatorMoveProxy_t4175490694 * L_10 = GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623(L_9, /*hidden argument*/GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623_MethodInfo_var);
		__this->set__animatorProxy_22(L_10);
		PlayMakerAnimatorMoveProxy_t4175490694 * L_11 = __this->get__animatorProxy_22();
		bool L_12 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_11, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_007d;
		}
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_13 = __this->get__animatorProxy_22();
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)GetAnimatorCurrentStateInfo_OnAnimatorMoveEvent_m4219685574_MethodInfo_var);
		Action_t3771233898 * L_15 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_15, __this, L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		PlayMakerAnimatorMoveProxy_add_OnAnimatorMoveEvent_m2003874533(L_13, L_15, /*hidden argument*/NULL);
	}

IL_007d:
	{
		GetAnimatorCurrentStateInfo_GetLayerInfo_m2468317032(__this, /*hidden argument*/NULL);
		bool L_16 = __this->get_everyFrame_11();
		if (L_16)
		{
			goto IL_0094;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0094:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::OnUpdate()
extern "C"  void GetAnimatorCurrentStateInfo_OnUpdate_m3963251527 (GetAnimatorCurrentStateInfo_t2823805553 * __this, const MethodInfo* method)
{
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_22();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		GetAnimatorCurrentStateInfo_GetLayerInfo_m2468317032(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::OnAnimatorMoveEvent()
extern "C"  void GetAnimatorCurrentStateInfo_OnAnimatorMoveEvent_m4219685574 (GetAnimatorCurrentStateInfo_t2823805553 * __this, const MethodInfo* method)
{
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_22();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		GetAnimatorCurrentStateInfo_GetLayerInfo_m2468317032(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::GetLayerInfo()
extern "C"  void GetAnimatorCurrentStateInfo_GetLayerInfo_m2468317032 (GetAnimatorCurrentStateInfo_t2823805553 * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t323110318  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_t2776330603 * L_0 = __this->get__animator_23();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_012f;
		}
	}
	{
		Animator_t2776330603 * L_2 = __this->get__animator_23();
		FsmInt_t1596138449 * L_3 = __this->get_layerIndex_10();
		NullCheck(L_3);
		int32_t L_4 = FsmInt_get_Value_m27059446(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		AnimatorStateInfo_t323110318  L_5 = Animator_GetCurrentAnimatorStateInfo_m3061859448(L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		FsmInt_t1596138449 * L_6 = __this->get_fullPathHash_14();
		int32_t L_7 = AnimatorStateInfo_get_fullPathHash_m3257074542((&V_0), /*hidden argument*/NULL);
		NullCheck(L_6);
		FsmInt_set_Value_m2087583461(L_6, L_7, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_8 = __this->get_shortPathHash_15();
		int32_t L_9 = AnimatorStateInfo_get_shortNameHash_m994885515((&V_0), /*hidden argument*/NULL);
		NullCheck(L_8);
		FsmInt_set_Value_m2087583461(L_8, L_9, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_10 = __this->get_nameHash_13();
		int32_t L_11 = AnimatorStateInfo_get_shortNameHash_m994885515((&V_0), /*hidden argument*/NULL);
		NullCheck(L_10);
		FsmInt_set_Value_m2087583461(L_10, L_11, /*hidden argument*/NULL);
		FsmString_t952858651 * L_12 = __this->get_name_12();
		NullCheck(L_12);
		bool L_13 = NamedVariable_get_IsNone_m281035543(L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_008f;
		}
	}
	{
		FsmString_t952858651 * L_14 = __this->get_name_12();
		Animator_t2776330603 * L_15 = __this->get__animator_23();
		FsmInt_t1596138449 * L_16 = __this->get_layerIndex_10();
		NullCheck(L_16);
		int32_t L_17 = FsmInt_get_Value_m27059446(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		String_t* L_18 = Animator_GetLayerName_m3480300056(L_15, L_17, /*hidden argument*/NULL);
		NullCheck(L_14);
		FsmString_set_Value_m829393196(L_14, L_18, /*hidden argument*/NULL);
	}

IL_008f:
	{
		FsmInt_t1596138449 * L_19 = __this->get_tagHash_16();
		int32_t L_20 = AnimatorStateInfo_get_tagHash_m3543262078((&V_0), /*hidden argument*/NULL);
		NullCheck(L_19);
		FsmInt_set_Value_m2087583461(L_19, L_20, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_21 = __this->get_length_18();
		float L_22 = AnimatorStateInfo_get_length_m3147284742((&V_0), /*hidden argument*/NULL);
		NullCheck(L_21);
		FsmFloat_set_Value_m1568963140(L_21, L_22, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_23 = __this->get_isStateLooping_17();
		bool L_24 = AnimatorStateInfo_get_loop_m1495892586((&V_0), /*hidden argument*/NULL);
		NullCheck(L_23);
		FsmBool_set_Value_m1126216340(L_23, L_24, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_25 = __this->get_normalizedTime_19();
		float L_26 = AnimatorStateInfo_get_normalizedTime_m2531821060((&V_0), /*hidden argument*/NULL);
		NullCheck(L_25);
		FsmFloat_set_Value_m1568963140(L_25, L_26, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_27 = __this->get_loopCount_20();
		NullCheck(L_27);
		bool L_28 = NamedVariable_get_IsNone_m281035543(L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00f7;
		}
	}
	{
		FsmFloat_t2134102846 * L_29 = __this->get_currentLoopProgress_21();
		NullCheck(L_29);
		bool L_30 = NamedVariable_get_IsNone_m281035543(L_29, /*hidden argument*/NULL);
		if (L_30)
		{
			goto IL_012f;
		}
	}

IL_00f7:
	{
		FsmInt_t1596138449 * L_31 = __this->get_loopCount_20();
		float L_32 = AnimatorStateInfo_get_normalizedTime_m2531821060((&V_0), /*hidden argument*/NULL);
		double L_33 = Math_Truncate_m534017384(NULL /*static, unused*/, (((double)((double)L_32))), /*hidden argument*/NULL);
		NullCheck(L_31);
		FsmInt_set_Value_m2087583461(L_31, (((int32_t)((int32_t)L_33))), /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_34 = __this->get_currentLoopProgress_21();
		float L_35 = AnimatorStateInfo_get_normalizedTime_m2531821060((&V_0), /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_36 = __this->get_loopCount_20();
		NullCheck(L_36);
		int32_t L_37 = FsmInt_get_Value_m27059446(L_36, /*hidden argument*/NULL);
		NullCheck(L_34);
		FsmFloat_set_Value_m1568963140(L_34, ((float)((float)L_35-(float)(((float)((float)L_37))))), /*hidden argument*/NULL);
	}

IL_012f:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::OnExit()
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const MethodInfo* GetAnimatorCurrentStateInfo_OnAnimatorMoveEvent_m4219685574_MethodInfo_var;
extern const uint32_t GetAnimatorCurrentStateInfo_OnExit_m657520028_MetadataUsageId;
extern "C"  void GetAnimatorCurrentStateInfo_OnExit_m657520028 (GetAnimatorCurrentStateInfo_t2823805553 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorCurrentStateInfo_OnExit_m657520028_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_22();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_2 = __this->get__animatorProxy_22();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)GetAnimatorCurrentStateInfo_OnAnimatorMoveEvent_m4219685574_MethodInfo_var);
		Action_t3771233898 * L_4 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_4, __this, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		PlayMakerAnimatorMoveProxy_remove_OnAnimatorMoveEvent_m2967347206(L_2, L_4, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsName::.ctor()
extern "C"  void GetAnimatorCurrentStateInfoIsName__ctor_m934420240 (GetAnimatorCurrentStateInfoIsName_t2852409574 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsName::Reset()
extern "C"  void GetAnimatorCurrentStateInfoIsName_Reset_m2875820477 (GetAnimatorCurrentStateInfoIsName_t2852409574 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_layerIndex_10((FsmInt_t1596138449 *)NULL);
		__this->set_name_11((FsmString_t952858651 *)NULL);
		__this->set_nameMatchEvent_13((FsmEvent_t2133468028 *)NULL);
		__this->set_nameDoNotMatchEvent_14((FsmEvent_t2133468028 *)NULL);
		__this->set_everyFrame_12((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsName::OnEnter()
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorCurrentStateInfoIsName_OnEnter_m4016381671_MetadataUsageId;
extern "C"  void GetAnimatorCurrentStateInfoIsName_OnEnter_m4016381671 (GetAnimatorCurrentStateInfoIsName_t2852409574 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorCurrentStateInfoIsName_OnEnter_m4016381671_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_16(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_16();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorCurrentStateInfoIsName_IsName_m3222692105(__this, /*hidden argument*/NULL);
		bool L_9 = __this->get_everyFrame_12();
		if (L_9)
		{
			goto IL_0060;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0060:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsName::OnUpdate()
extern "C"  void GetAnimatorCurrentStateInfoIsName_OnUpdate_m3382306876 (GetAnimatorCurrentStateInfoIsName_t2852409574 * __this, const MethodInfo* method)
{
	{
		GetAnimatorCurrentStateInfoIsName_IsName_m3222692105(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsName::IsName()
extern "C"  void GetAnimatorCurrentStateInfoIsName_IsName_m3222692105 (GetAnimatorCurrentStateInfoIsName_t2852409574 * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t323110318  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_t2776330603 * L_0 = __this->get__animator_16();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0066;
		}
	}
	{
		Animator_t2776330603 * L_2 = __this->get__animator_16();
		FsmInt_t1596138449 * L_3 = __this->get_layerIndex_10();
		NullCheck(L_3);
		int32_t L_4 = FsmInt_get_Value_m27059446(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		AnimatorStateInfo_t323110318  L_5 = Animator_GetCurrentAnimatorStateInfo_m3061859448(L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		FsmString_t952858651 * L_6 = __this->get_name_11();
		NullCheck(L_6);
		String_t* L_7 = FsmString_get_Value_m872383149(L_6, /*hidden argument*/NULL);
		bool L_8 = AnimatorStateInfo_IsName_m1653922768((&V_0), L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0055;
		}
	}
	{
		Fsm_t1527112426 * L_9 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_10 = __this->get_nameMatchEvent_13();
		NullCheck(L_9);
		Fsm_Event_m625948263(L_9, L_10, /*hidden argument*/NULL);
		goto IL_0066;
	}

IL_0055:
	{
		Fsm_t1527112426 * L_11 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_12 = __this->get_nameDoNotMatchEvent_14();
		NullCheck(L_11);
		Fsm_Event_m625948263(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0066:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::.ctor()
extern "C"  void GetAnimatorCurrentStateInfoIsTag__ctor_m3945847409 (GetAnimatorCurrentStateInfoIsTag_t2329710485 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::Reset()
extern "C"  void GetAnimatorCurrentStateInfoIsTag_Reset_m1592280350 (GetAnimatorCurrentStateInfoIsTag_t2329710485 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_layerIndex_10((FsmInt_t1596138449 *)NULL);
		__this->set_tag_11((FsmString_t952858651 *)NULL);
		__this->set_tagMatch_13((FsmBool_t1075959796 *)NULL);
		__this->set_tagMatchEvent_14((FsmEvent_t2133468028 *)NULL);
		__this->set_tagDoNotMatchEvent_15((FsmEvent_t2133468028 *)NULL);
		__this->set_everyFrame_12((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::OnEnter()
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorCurrentStateInfoIsTag_OnEnter_m3189933576_MetadataUsageId;
extern "C"  void GetAnimatorCurrentStateInfoIsTag_OnEnter_m3189933576 (GetAnimatorCurrentStateInfoIsTag_t2329710485 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorCurrentStateInfoIsTag_OnEnter_m3189933576_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_17(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_17();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorCurrentStateInfoIsTag_IsTag_m2566729119(__this, /*hidden argument*/NULL);
		bool L_9 = __this->get_everyFrame_12();
		if (L_9)
		{
			goto IL_0060;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0060:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::OnUpdate()
extern "C"  void GetAnimatorCurrentStateInfoIsTag_OnUpdate_m3532219707 (GetAnimatorCurrentStateInfoIsTag_t2329710485 * __this, const MethodInfo* method)
{
	{
		GetAnimatorCurrentStateInfoIsTag_IsTag_m2566729119(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::IsTag()
extern "C"  void GetAnimatorCurrentStateInfoIsTag_IsTag_m2566729119 (GetAnimatorCurrentStateInfoIsTag_t2329710485 * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t323110318  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_t2776330603 * L_0 = __this->get__animator_17();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_007e;
		}
	}
	{
		Animator_t2776330603 * L_2 = __this->get__animator_17();
		FsmInt_t1596138449 * L_3 = __this->get_layerIndex_10();
		NullCheck(L_3);
		int32_t L_4 = FsmInt_get_Value_m27059446(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		AnimatorStateInfo_t323110318  L_5 = Animator_GetCurrentAnimatorStateInfo_m3061859448(L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		FsmString_t952858651 * L_6 = __this->get_tag_11();
		NullCheck(L_6);
		String_t* L_7 = FsmString_get_Value_m872383149(L_6, /*hidden argument*/NULL);
		bool L_8 = AnimatorStateInfo_IsTag_m119936877((&V_0), L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0061;
		}
	}
	{
		FsmBool_t1075959796 * L_9 = __this->get_tagMatch_13();
		NullCheck(L_9);
		FsmBool_set_Value_m1126216340(L_9, (bool)1, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_10 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_11 = __this->get_tagMatchEvent_14();
		NullCheck(L_10);
		Fsm_Event_m625948263(L_10, L_11, /*hidden argument*/NULL);
		goto IL_007e;
	}

IL_0061:
	{
		FsmBool_t1075959796 * L_12 = __this->get_tagMatch_13();
		NullCheck(L_12);
		FsmBool_set_Value_m1126216340(L_12, (bool)0, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_13 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_14 = __this->get_tagDoNotMatchEvent_15();
		NullCheck(L_13);
		Fsm_Event_m625948263(L_13, L_14, /*hidden argument*/NULL);
	}

IL_007e:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::.ctor()
extern "C"  void GetAnimatorCurrentTransitionInfo__ctor_m3945219967 (GetAnimatorCurrentTransitionInfo_t114989767 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::Reset()
extern "C"  void GetAnimatorCurrentTransitionInfo_Reset_m1591652908 (GetAnimatorCurrentTransitionInfo_t114989767 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_layerIndex_10((FsmInt_t1596138449 *)NULL);
		__this->set_name_12((FsmString_t952858651 *)NULL);
		__this->set_nameHash_13((FsmInt_t1596138449 *)NULL);
		__this->set_userNameHash_14((FsmInt_t1596138449 *)NULL);
		__this->set_normalizedTime_15((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::OnEnter()
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623_MethodInfo_var;
extern const MethodInfo* GetAnimatorCurrentTransitionInfo_OnAnimatorMoveEvent_m122932448_MethodInfo_var;
extern const uint32_t GetAnimatorCurrentTransitionInfo_OnEnter_m2586961814_MetadataUsageId;
extern "C"  void GetAnimatorCurrentTransitionInfo_OnEnter_m2586961814 (GetAnimatorCurrentTransitionInfo_t114989767 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorCurrentTransitionInfo_OnEnter_m2586961814_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_17(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_17();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GameObject_t3674682005 * L_9 = V_0;
		NullCheck(L_9);
		PlayMakerAnimatorMoveProxy_t4175490694 * L_10 = GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623(L_9, /*hidden argument*/GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623_MethodInfo_var);
		__this->set__animatorProxy_16(L_10);
		PlayMakerAnimatorMoveProxy_t4175490694 * L_11 = __this->get__animatorProxy_16();
		bool L_12 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_11, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_007d;
		}
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_13 = __this->get__animatorProxy_16();
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)GetAnimatorCurrentTransitionInfo_OnAnimatorMoveEvent_m122932448_MethodInfo_var);
		Action_t3771233898 * L_15 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_15, __this, L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		PlayMakerAnimatorMoveProxy_add_OnAnimatorMoveEvent_m2003874533(L_13, L_15, /*hidden argument*/NULL);
	}

IL_007d:
	{
		GetAnimatorCurrentTransitionInfo_GetTransitionInfo_m3403661558(__this, /*hidden argument*/NULL);
		bool L_16 = __this->get_everyFrame_11();
		if (L_16)
		{
			goto IL_0094;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0094:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::OnUpdate()
extern "C"  void GetAnimatorCurrentTransitionInfo_OnUpdate_m2019964269 (GetAnimatorCurrentTransitionInfo_t114989767 * __this, const MethodInfo* method)
{
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_16();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		GetAnimatorCurrentTransitionInfo_GetTransitionInfo_m3403661558(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::OnAnimatorMoveEvent()
extern "C"  void GetAnimatorCurrentTransitionInfo_OnAnimatorMoveEvent_m122932448 (GetAnimatorCurrentTransitionInfo_t114989767 * __this, const MethodInfo* method)
{
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_16();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		GetAnimatorCurrentTransitionInfo_GetTransitionInfo_m3403661558(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::GetTransitionInfo()
extern "C"  void GetAnimatorCurrentTransitionInfo_GetTransitionInfo_m3403661558 (GetAnimatorCurrentTransitionInfo_t114989767 * __this, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2817229998  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_t2776330603 * L_0 = __this->get__animator_17();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_008f;
		}
	}
	{
		Animator_t2776330603 * L_2 = __this->get__animator_17();
		FsmInt_t1596138449 * L_3 = __this->get_layerIndex_10();
		NullCheck(L_3);
		int32_t L_4 = FsmInt_get_Value_m27059446(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		AnimatorTransitionInfo_t2817229998  L_5 = Animator_GetAnimatorTransitionInfo_m3858104711(L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		FsmString_t952858651 * L_6 = __this->get_name_12();
		NullCheck(L_6);
		bool L_7 = NamedVariable_get_IsNone_m281035543(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0059;
		}
	}
	{
		FsmString_t952858651 * L_8 = __this->get_name_12();
		Animator_t2776330603 * L_9 = __this->get__animator_17();
		FsmInt_t1596138449 * L_10 = __this->get_layerIndex_10();
		NullCheck(L_10);
		int32_t L_11 = FsmInt_get_Value_m27059446(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		String_t* L_12 = Animator_GetLayerName_m3480300056(L_9, L_11, /*hidden argument*/NULL);
		NullCheck(L_8);
		FsmString_set_Value_m829393196(L_8, L_12, /*hidden argument*/NULL);
	}

IL_0059:
	{
		FsmInt_t1596138449 * L_13 = __this->get_nameHash_13();
		int32_t L_14 = AnimatorTransitionInfo_get_nameHash_m2102867203((&V_0), /*hidden argument*/NULL);
		NullCheck(L_13);
		FsmInt_set_Value_m2087583461(L_13, L_14, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_15 = __this->get_userNameHash_14();
		int32_t L_16 = AnimatorTransitionInfo_get_userNameHash_m249220782((&V_0), /*hidden argument*/NULL);
		NullCheck(L_15);
		FsmInt_set_Value_m2087583461(L_15, L_16, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_17 = __this->get_normalizedTime_15();
		float L_18 = AnimatorTransitionInfo_get_normalizedTime_m3258684986((&V_0), /*hidden argument*/NULL);
		NullCheck(L_17);
		FsmFloat_set_Value_m1568963140(L_17, L_18, /*hidden argument*/NULL);
	}

IL_008f:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::OnExit()
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const MethodInfo* GetAnimatorCurrentTransitionInfo_OnAnimatorMoveEvent_m122932448_MethodInfo_var;
extern const uint32_t GetAnimatorCurrentTransitionInfo_OnExit_m3556053314_MetadataUsageId;
extern "C"  void GetAnimatorCurrentTransitionInfo_OnExit_m3556053314 (GetAnimatorCurrentTransitionInfo_t114989767 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorCurrentTransitionInfo_OnExit_m3556053314_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_16();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_2 = __this->get__animatorProxy_16();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)GetAnimatorCurrentTransitionInfo_OnAnimatorMoveEvent_m122932448_MethodInfo_var);
		Action_t3771233898 * L_4 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_4, __this, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		PlayMakerAnimatorMoveProxy_remove_OnAnimatorMoveEvent_m2967347206(L_2, L_4, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName::.ctor()
extern "C"  void GetAnimatorCurrentTransitionInfoIsName__ctor_m327899946 (GetAnimatorCurrentTransitionInfoIsName_t3939956924 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName::Reset()
extern "C"  void GetAnimatorCurrentTransitionInfoIsName_Reset_m2269300183 (GetAnimatorCurrentTransitionInfoIsName_t3939956924 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_layerIndex_10((FsmInt_t1596138449 *)NULL);
		__this->set_name_11((FsmString_t952858651 *)NULL);
		__this->set_nameMatch_13((FsmBool_t1075959796 *)NULL);
		__this->set_nameMatchEvent_14((FsmEvent_t2133468028 *)NULL);
		__this->set_nameDoNotMatchEvent_15((FsmEvent_t2133468028 *)NULL);
		__this->set_everyFrame_12((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName::OnEnter()
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorCurrentTransitionInfoIsName_OnEnter_m970964097_MetadataUsageId;
extern "C"  void GetAnimatorCurrentTransitionInfoIsName_OnEnter_m970964097 (GetAnimatorCurrentTransitionInfoIsName_t3939956924 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorCurrentTransitionInfoIsName_OnEnter_m970964097_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_17(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_17();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorCurrentTransitionInfoIsName_IsName_m1600432175(__this, /*hidden argument*/NULL);
		bool L_9 = __this->get_everyFrame_12();
		if (L_9)
		{
			goto IL_0060;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0060:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName::OnUpdate()
extern "C"  void GetAnimatorCurrentTransitionInfoIsName_OnUpdate_m3463642594 (GetAnimatorCurrentTransitionInfoIsName_t3939956924 * __this, const MethodInfo* method)
{
	{
		GetAnimatorCurrentTransitionInfoIsName_IsName_m1600432175(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName::IsName()
extern "C"  void GetAnimatorCurrentTransitionInfoIsName_IsName_m1600432175 (GetAnimatorCurrentTransitionInfoIsName_t3939956924 * __this, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2817229998  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_t2776330603 * L_0 = __this->get__animator_17();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_007e;
		}
	}
	{
		Animator_t2776330603 * L_2 = __this->get__animator_17();
		FsmInt_t1596138449 * L_3 = __this->get_layerIndex_10();
		NullCheck(L_3);
		int32_t L_4 = FsmInt_get_Value_m27059446(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		AnimatorTransitionInfo_t2817229998  L_5 = Animator_GetAnimatorTransitionInfo_m3858104711(L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		FsmString_t952858651 * L_6 = __this->get_name_11();
		NullCheck(L_6);
		String_t* L_7 = FsmString_get_Value_m872383149(L_6, /*hidden argument*/NULL);
		bool L_8 = AnimatorTransitionInfo_IsName_m1283663078((&V_0), L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0061;
		}
	}
	{
		FsmBool_t1075959796 * L_9 = __this->get_nameMatch_13();
		NullCheck(L_9);
		FsmBool_set_Value_m1126216340(L_9, (bool)1, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_10 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_11 = __this->get_nameMatchEvent_14();
		NullCheck(L_10);
		Fsm_Event_m625948263(L_10, L_11, /*hidden argument*/NULL);
		goto IL_007e;
	}

IL_0061:
	{
		FsmBool_t1075959796 * L_12 = __this->get_nameMatch_13();
		NullCheck(L_12);
		FsmBool_set_Value_m1126216340(L_12, (bool)0, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_13 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_14 = __this->get_nameDoNotMatchEvent_15();
		NullCheck(L_13);
		Fsm_Event_m625948263(L_13, L_14, /*hidden argument*/NULL);
	}

IL_007e:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsUserName::.ctor()
extern "C"  void GetAnimatorCurrentTransitionInfoIsUserName__ctor_m3001526303 (GetAnimatorCurrentTransitionInfoIsUserName_t1580140583 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsUserName::Reset()
extern "C"  void GetAnimatorCurrentTransitionInfoIsUserName_Reset_m647959244 (GetAnimatorCurrentTransitionInfoIsUserName_t1580140583 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_layerIndex_10((FsmInt_t1596138449 *)NULL);
		__this->set_userName_11((FsmString_t952858651 *)NULL);
		__this->set_nameMatch_13((FsmBool_t1075959796 *)NULL);
		__this->set_nameMatchEvent_14((FsmEvent_t2133468028 *)NULL);
		__this->set_nameDoNotMatchEvent_15((FsmEvent_t2133468028 *)NULL);
		__this->set_everyFrame_12((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsUserName::OnEnter()
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorCurrentTransitionInfoIsUserName_OnEnter_m1935450166_MetadataUsageId;
extern "C"  void GetAnimatorCurrentTransitionInfoIsUserName_OnEnter_m1935450166 (GetAnimatorCurrentTransitionInfoIsUserName_t1580140583 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorCurrentTransitionInfoIsUserName_OnEnter_m1935450166_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_17(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_17();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorCurrentTransitionInfoIsUserName_IsName_m2878470618(__this, /*hidden argument*/NULL);
		bool L_9 = __this->get_everyFrame_12();
		if (L_9)
		{
			goto IL_0060;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0060:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsUserName::OnUpdate()
extern "C"  void GetAnimatorCurrentTransitionInfoIsUserName_OnUpdate_m3297939661 (GetAnimatorCurrentTransitionInfoIsUserName_t1580140583 * __this, const MethodInfo* method)
{
	{
		GetAnimatorCurrentTransitionInfoIsUserName_IsName_m2878470618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsUserName::IsName()
extern "C"  void GetAnimatorCurrentTransitionInfoIsUserName_IsName_m2878470618 (GetAnimatorCurrentTransitionInfoIsUserName_t1580140583 * __this, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2817229998  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_t2776330603 * L_0 = __this->get__animator_17();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_007e;
		}
	}
	{
		Animator_t2776330603 * L_2 = __this->get__animator_17();
		FsmInt_t1596138449 * L_3 = __this->get_layerIndex_10();
		NullCheck(L_3);
		int32_t L_4 = FsmInt_get_Value_m27059446(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		AnimatorTransitionInfo_t2817229998  L_5 = Animator_GetAnimatorTransitionInfo_m3858104711(L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		FsmString_t952858651 * L_6 = __this->get_userName_11();
		NullCheck(L_6);
		String_t* L_7 = FsmString_get_Value_m872383149(L_6, /*hidden argument*/NULL);
		bool L_8 = AnimatorTransitionInfo_IsUserName_m2732197659((&V_0), L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0061;
		}
	}
	{
		FsmBool_t1075959796 * L_9 = __this->get_nameMatch_13();
		NullCheck(L_9);
		FsmBool_set_Value_m1126216340(L_9, (bool)1, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_10 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_11 = __this->get_nameMatchEvent_14();
		NullCheck(L_10);
		Fsm_Event_m625948263(L_10, L_11, /*hidden argument*/NULL);
		goto IL_007e;
	}

IL_0061:
	{
		FsmBool_t1075959796 * L_12 = __this->get_nameMatch_13();
		NullCheck(L_12);
		FsmBool_set_Value_m1126216340(L_12, (bool)0, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_13 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_14 = __this->get_nameDoNotMatchEvent_15();
		NullCheck(L_13);
		Fsm_Event_m625948263(L_13, L_14, /*hidden argument*/NULL);
	}

IL_007e:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorDelta::.ctor()
extern "C"  void GetAnimatorDelta__ctor_m2311017667 (GetAnimatorDelta_t946274563 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorDelta::Reset()
extern "C"  void GetAnimatorDelta_Reset_m4252417904 (GetAnimatorDelta_t946274563 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_deltaPosition_10((FsmVector3_t533912882 *)NULL);
		__this->set_deltaRotation_11((FsmQuaternion_t3871136040 *)NULL);
		__this->set_everyFrame_12((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorDelta::OnEnter()
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623_MethodInfo_var;
extern const MethodInfo* GetAnimatorDelta_OnAnimatorMoveEvent_m1050235684_MethodInfo_var;
extern const uint32_t GetAnimatorDelta_OnEnter_m4076581850_MetadataUsageId;
extern "C"  void GetAnimatorDelta_OnEnter_m4076581850 (GetAnimatorDelta_t946274563 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorDelta_OnEnter_m4076581850_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_15(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_15();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GameObject_t3674682005 * L_9 = V_0;
		NullCheck(L_9);
		PlayMakerAnimatorMoveProxy_t4175490694 * L_10 = GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623(L_9, /*hidden argument*/GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623_MethodInfo_var);
		__this->set__animatorProxy_13(L_10);
		PlayMakerAnimatorMoveProxy_t4175490694 * L_11 = __this->get__animatorProxy_13();
		bool L_12 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_11, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_007d;
		}
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_13 = __this->get__animatorProxy_13();
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)GetAnimatorDelta_OnAnimatorMoveEvent_m1050235684_MethodInfo_var);
		Action_t3771233898 * L_15 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_15, __this, L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		PlayMakerAnimatorMoveProxy_add_OnAnimatorMoveEvent_m2003874533(L_13, L_15, /*hidden argument*/NULL);
	}

IL_007d:
	{
		GetAnimatorDelta_DoGetDeltaPosition_m84291223(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorDelta::OnUpdate()
extern "C"  void GetAnimatorDelta_OnUpdate_m953545129 (GetAnimatorDelta_t946274563 * __this, const MethodInfo* method)
{
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_13();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		GetAnimatorDelta_DoGetDeltaPosition_m84291223(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorDelta::OnAnimatorMoveEvent()
extern "C"  void GetAnimatorDelta_OnAnimatorMoveEvent_m1050235684 (GetAnimatorDelta_t946274563 * __this, const MethodInfo* method)
{
	{
		GetAnimatorDelta_DoGetDeltaPosition_m84291223(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorDelta::DoGetDeltaPosition()
extern "C"  void GetAnimatorDelta_DoGetDeltaPosition_m84291223 (GetAnimatorDelta_t946274563 * __this, const MethodInfo* method)
{
	{
		Animator_t2776330603 * L_0 = __this->get__animator_15();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FsmVector3_t533912882 * L_2 = __this->get_deltaPosition_10();
		Animator_t2776330603 * L_3 = __this->get__animator_15();
		NullCheck(L_3);
		Vector3_t4282066566  L_4 = Animator_get_deltaPosition_m1658225602(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmVector3_set_Value_m716982822(L_2, L_4, /*hidden argument*/NULL);
		FsmQuaternion_t3871136040 * L_5 = __this->get_deltaRotation_11();
		Animator_t2776330603 * L_6 = __this->get__animator_15();
		NullCheck(L_6);
		Quaternion_t1553702882  L_7 = Animator_get_deltaRotation_m1583110423(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		FsmQuaternion_set_Value_m446581172(L_5, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorDelta::OnExit()
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const MethodInfo* GetAnimatorDelta_OnAnimatorMoveEvent_m1050235684_MethodInfo_var;
extern const uint32_t GetAnimatorDelta_OnExit_m140422270_MetadataUsageId;
extern "C"  void GetAnimatorDelta_OnExit_m140422270 (GetAnimatorDelta_t946274563 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorDelta_OnExit_m140422270_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_13();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_2 = __this->get__animatorProxy_13();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)GetAnimatorDelta_OnAnimatorMoveEvent_m1050235684_MethodInfo_var);
		Action_t3771233898 * L_4 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_4, __this, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		PlayMakerAnimatorMoveProxy_remove_OnAnimatorMoveEvent_m2967347206(L_2, L_4, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFeetPivotActive::.ctor()
extern "C"  void GetAnimatorFeetPivotActive__ctor_m502507873 (GetAnimatorFeetPivotActive_t705651877 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFeetPivotActive::Reset()
extern "C"  void GetAnimatorFeetPivotActive_Reset_m2443908110 (GetAnimatorFeetPivotActive_t705651877 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_feetPivotActive_10((FsmFloat_t2134102846 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFeetPivotActive::OnEnter()
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorFeetPivotActive_OnEnter_m1265457400_MetadataUsageId;
extern "C"  void GetAnimatorFeetPivotActive_OnEnter_m1265457400 (GetAnimatorFeetPivotActive_t705651877 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorFeetPivotActive_OnEnter_m1265457400_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_11(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_11();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorFeetPivotActive_DoGetFeetPivotActive_m3179232530(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFeetPivotActive::DoGetFeetPivotActive()
extern "C"  void GetAnimatorFeetPivotActive_DoGetFeetPivotActive_m3179232530 (GetAnimatorFeetPivotActive_t705651877 * __this, const MethodInfo* method)
{
	{
		Animator_t2776330603 * L_0 = __this->get__animator_11();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FsmFloat_t2134102846 * L_2 = __this->get_feetPivotActive_10();
		Animator_t2776330603 * L_3 = __this->get__animator_11();
		NullCheck(L_3);
		float L_4 = Animator_get_feetPivotActive_m4087239689(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmFloat_set_Value_m1568963140(L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFloat::.ctor()
extern "C"  void GetAnimatorFloat__ctor_m4271842975 (GetAnimatorFloat_t948332455 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFloat::Reset()
extern "C"  void GetAnimatorFloat_Reset_m1918275916 (GetAnimatorFloat_t948332455 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_parameter_10((FsmString_t952858651 *)NULL);
		__this->set_result_12((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFloat::OnEnter()
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623_MethodInfo_var;
extern const MethodInfo* GetAnimatorFloat_OnAnimatorMoveEvent_m2609296384_MethodInfo_var;
extern const uint32_t GetAnimatorFloat_OnEnter_m2939059894_MetadataUsageId;
extern "C"  void GetAnimatorFloat_OnEnter_m2939059894 (GetAnimatorFloat_t948332455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorFloat_OnEnter_m2939059894_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_14(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_14();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GameObject_t3674682005 * L_9 = V_0;
		NullCheck(L_9);
		PlayMakerAnimatorMoveProxy_t4175490694 * L_10 = GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623(L_9, /*hidden argument*/GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623_MethodInfo_var);
		__this->set__animatorProxy_13(L_10);
		PlayMakerAnimatorMoveProxy_t4175490694 * L_11 = __this->get__animatorProxy_13();
		bool L_12 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_11, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_007d;
		}
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_13 = __this->get__animatorProxy_13();
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)GetAnimatorFloat_OnAnimatorMoveEvent_m2609296384_MethodInfo_var);
		Action_t3771233898 * L_15 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_15, __this, L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		PlayMakerAnimatorMoveProxy_add_OnAnimatorMoveEvent_m2003874533(L_13, L_15, /*hidden argument*/NULL);
	}

IL_007d:
	{
		FsmString_t952858651 * L_16 = __this->get_parameter_10();
		NullCheck(L_16);
		String_t* L_17 = FsmString_get_Value_m872383149(L_16, /*hidden argument*/NULL);
		int32_t L_18 = Animator_StringToHash_m4020897098(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		__this->set__paramID_15(L_18);
		GetAnimatorFloat_GetParameter_m2334025464(__this, /*hidden argument*/NULL);
		bool L_19 = __this->get_everyFrame_11();
		if (L_19)
		{
			goto IL_00aa;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_00aa:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFloat::OnAnimatorMoveEvent()
extern "C"  void GetAnimatorFloat_OnAnimatorMoveEvent_m2609296384 (GetAnimatorFloat_t948332455 * __this, const MethodInfo* method)
{
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_13();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		GetAnimatorFloat_GetParameter_m2334025464(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFloat::OnUpdate()
extern "C"  void GetAnimatorFloat_OnUpdate_m50102861 (GetAnimatorFloat_t948332455 * __this, const MethodInfo* method)
{
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_13();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		GetAnimatorFloat_GetParameter_m2334025464(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFloat::GetParameter()
extern "C"  void GetAnimatorFloat_GetParameter_m2334025464 (GetAnimatorFloat_t948332455 * __this, const MethodInfo* method)
{
	{
		Animator_t2776330603 * L_0 = __this->get__animator_14();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		FsmFloat_t2134102846 * L_2 = __this->get_result_12();
		Animator_t2776330603 * L_3 = __this->get__animator_14();
		int32_t L_4 = __this->get__paramID_15();
		NullCheck(L_3);
		float L_5 = Animator_GetFloat_m2965884705(L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmFloat_set_Value_m1568963140(L_2, L_5, /*hidden argument*/NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFloat::OnExit()
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const MethodInfo* GetAnimatorFloat_OnAnimatorMoveEvent_m2609296384_MethodInfo_var;
extern const uint32_t GetAnimatorFloat_OnExit_m796464674_MetadataUsageId;
extern "C"  void GetAnimatorFloat_OnExit_m796464674 (GetAnimatorFloat_t948332455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorFloat_OnExit_m796464674_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_13();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_2 = __this->get__animatorProxy_13();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)GetAnimatorFloat_OnAnimatorMoveEvent_m2609296384_MethodInfo_var);
		Action_t3771233898 * L_4 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_4, __this, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		PlayMakerAnimatorMoveProxy_remove_OnAnimatorMoveEvent_m2967347206(L_2, L_4, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight::.ctor()
extern "C"  void GetAnimatorGravityWeight__ctor_m2113375701 (GetAnimatorGravityWeight_t3107738545 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight::Reset()
extern "C"  void GetAnimatorGravityWeight_Reset_m4054775938 (GetAnimatorGravityWeight_t3107738545 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_gravityWeight_11((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_10((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight::OnEnter()
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623_MethodInfo_var;
extern const MethodInfo* GetAnimatorGravityWeight_OnAnimatorMoveEvent_m2632402102_MethodInfo_var;
extern const uint32_t GetAnimatorGravityWeight_OnEnter_m3121213548_MetadataUsageId;
extern "C"  void GetAnimatorGravityWeight_OnEnter_m3121213548 (GetAnimatorGravityWeight_t3107738545 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorGravityWeight_OnEnter_m3121213548_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_13(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_13();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GameObject_t3674682005 * L_9 = V_0;
		NullCheck(L_9);
		PlayMakerAnimatorMoveProxy_t4175490694 * L_10 = GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623(L_9, /*hidden argument*/GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623_MethodInfo_var);
		__this->set__animatorProxy_12(L_10);
		PlayMakerAnimatorMoveProxy_t4175490694 * L_11 = __this->get__animatorProxy_12();
		bool L_12 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_11, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_007d;
		}
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_13 = __this->get__animatorProxy_12();
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)GetAnimatorGravityWeight_OnAnimatorMoveEvent_m2632402102_MethodInfo_var);
		Action_t3771233898 * L_15 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_15, __this, L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		PlayMakerAnimatorMoveProxy_add_OnAnimatorMoveEvent_m2003874533(L_13, L_15, /*hidden argument*/NULL);
	}

IL_007d:
	{
		GetAnimatorGravityWeight_DoGetGravityWeight_m3454642922(__this, /*hidden argument*/NULL);
		bool L_16 = __this->get_everyFrame_10();
		if (L_16)
		{
			goto IL_0094;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0094:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight::OnAnimatorMoveEvent()
extern "C"  void GetAnimatorGravityWeight_OnAnimatorMoveEvent_m2632402102 (GetAnimatorGravityWeight_t3107738545 * __this, const MethodInfo* method)
{
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_12();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		GetAnimatorGravityWeight_DoGetGravityWeight_m3454642922(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight::OnUpdate()
extern "C"  void GetAnimatorGravityWeight_OnUpdate_m1401898839 (GetAnimatorGravityWeight_t3107738545 * __this, const MethodInfo* method)
{
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_12();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		GetAnimatorGravityWeight_DoGetGravityWeight_m3454642922(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight::DoGetGravityWeight()
extern "C"  void GetAnimatorGravityWeight_DoGetGravityWeight_m3454642922 (GetAnimatorGravityWeight_t3107738545 * __this, const MethodInfo* method)
{
	{
		Animator_t2776330603 * L_0 = __this->get__animator_13();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FsmFloat_t2134102846 * L_2 = __this->get_gravityWeight_11();
		Animator_t2776330603 * L_3 = __this->get__animator_13();
		NullCheck(L_3);
		float L_4 = Animator_get_gravityWeight_m1393695637(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmFloat_set_Value_m1568963140(L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight::OnExit()
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const MethodInfo* GetAnimatorGravityWeight_OnAnimatorMoveEvent_m2632402102_MethodInfo_var;
extern const uint32_t GetAnimatorGravityWeight_OnExit_m2603455916_MetadataUsageId;
extern "C"  void GetAnimatorGravityWeight_OnExit_m2603455916 (GetAnimatorGravityWeight_t3107738545 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorGravityWeight_OnExit_m2603455916_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_12();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_2 = __this->get__animatorProxy_12();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)GetAnimatorGravityWeight_OnAnimatorMoveEvent_m2632402102_MethodInfo_var);
		Action_t3771233898 * L_4 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_4, __this, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		PlayMakerAnimatorMoveProxy_remove_OnAnimatorMoveEvent_m2967347206(L_2, L_4, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorHumanScale::.ctor()
extern "C"  void GetAnimatorHumanScale__ctor_m3985010446 (GetAnimatorHumanScale_t3915188008 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorHumanScale::Reset()
extern "C"  void GetAnimatorHumanScale_Reset_m1631443387 (GetAnimatorHumanScale_t3915188008 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_humanScale_10((FsmFloat_t2134102846 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorHumanScale::OnEnter()
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorHumanScale_OnEnter_m2170906469_MetadataUsageId;
extern "C"  void GetAnimatorHumanScale_OnEnter_m2170906469 (GetAnimatorHumanScale_t3915188008 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorHumanScale_OnEnter_m2170906469_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_11(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_11();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorHumanScale_DoGetHumanScale_m527444404(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorHumanScale::DoGetHumanScale()
extern "C"  void GetAnimatorHumanScale_DoGetHumanScale_m527444404 (GetAnimatorHumanScale_t3915188008 * __this, const MethodInfo* method)
{
	{
		Animator_t2776330603 * L_0 = __this->get__animator_11();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FsmFloat_t2134102846 * L_2 = __this->get_humanScale_10();
		Animator_t2776330603 * L_3 = __this->get__animator_11();
		NullCheck(L_3);
		float L_4 = Animator_get_humanScale_m13697776(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmFloat_set_Value_m1568963140(L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::.ctor()
extern "C"  void GetAnimatorIKGoal__ctor_m1678076662 (GetAnimatorIKGoal_t3033774656 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::Reset()
extern "C"  void GetAnimatorIKGoal_Reset_m3619476899 (GetAnimatorIKGoal_t3033774656 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_goal_12((FsmGameObject_t1697147867 *)NULL);
		__this->set_position_13((FsmVector3_t533912882 *)NULL);
		__this->set_rotation_14((FsmQuaternion_t3871136040 *)NULL);
		__this->set_positionWeight_15((FsmFloat_t2134102846 *)NULL);
		__this->set_rotationWeight_16((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::OnEnter()
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorIKGoal_OnEnter_m1410664781_MetadataUsageId;
extern "C"  void GetAnimatorIKGoal_OnEnter_m1410664781 (GetAnimatorIKGoal_t3033774656 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorIKGoal_OnEnter_m1410664781_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	GameObject_t3674682005 * V_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_17(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_17();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		FsmGameObject_t1697147867 * L_9 = __this->get_goal_12();
		NullCheck(L_9);
		GameObject_t3674682005 * L_10 = FsmGameObject_get_Value_m673294275(L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		GameObject_t3674682005 * L_11 = V_1;
		bool L_12 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_11, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_006d;
		}
	}
	{
		GameObject_t3674682005 * L_13 = V_1;
		NullCheck(L_13);
		Transform_t1659122786 * L_14 = GameObject_get_transform_m1278640159(L_13, /*hidden argument*/NULL);
		__this->set__transform_18(L_14);
	}

IL_006d:
	{
		GetAnimatorIKGoal_DoGetIKGoal_m3978612660(__this, /*hidden argument*/NULL);
		bool L_15 = __this->get_everyFrame_11();
		if (L_15)
		{
			goto IL_0084;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0084:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::OnUpdate()
extern "C"  void GetAnimatorIKGoal_OnUpdate_m4209461910 (GetAnimatorIKGoal_t3033774656 * __this, const MethodInfo* method)
{
	{
		GetAnimatorIKGoal_DoGetIKGoal_m3978612660(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::DoGetIKGoal()
extern "C"  void GetAnimatorIKGoal_DoGetIKGoal_m3978612660 (GetAnimatorIKGoal_t3033774656 * __this, const MethodInfo* method)
{
	{
		Animator_t2776330603 * L_0 = __this->get__animator_17();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Transform_t1659122786 * L_2 = __this->get__transform_18();
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_005b;
		}
	}
	{
		Transform_t1659122786 * L_4 = __this->get__transform_18();
		Animator_t2776330603 * L_5 = __this->get__animator_17();
		int32_t L_6 = __this->get_iKGoal_10();
		NullCheck(L_5);
		Vector3_t4282066566  L_7 = Animator_GetIKPosition_m385128518(L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_position_m3111394108(L_4, L_7, /*hidden argument*/NULL);
		Transform_t1659122786 * L_8 = __this->get__transform_18();
		Animator_t2776330603 * L_9 = __this->get__animator_17();
		int32_t L_10 = __this->get_iKGoal_10();
		NullCheck(L_9);
		Quaternion_t1553702882  L_11 = Animator_GetIKRotation_m3297713819(L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_set_rotation_m1525803229(L_8, L_11, /*hidden argument*/NULL);
	}

IL_005b:
	{
		FsmVector3_t533912882 * L_12 = __this->get_position_13();
		NullCheck(L_12);
		bool L_13 = NamedVariable_get_IsNone_m281035543(L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0087;
		}
	}
	{
		FsmVector3_t533912882 * L_14 = __this->get_position_13();
		Animator_t2776330603 * L_15 = __this->get__animator_17();
		int32_t L_16 = __this->get_iKGoal_10();
		NullCheck(L_15);
		Vector3_t4282066566  L_17 = Animator_GetIKPosition_m385128518(L_15, L_16, /*hidden argument*/NULL);
		NullCheck(L_14);
		FsmVector3_set_Value_m716982822(L_14, L_17, /*hidden argument*/NULL);
	}

IL_0087:
	{
		FsmQuaternion_t3871136040 * L_18 = __this->get_rotation_14();
		NullCheck(L_18);
		bool L_19 = NamedVariable_get_IsNone_m281035543(L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_00b3;
		}
	}
	{
		FsmQuaternion_t3871136040 * L_20 = __this->get_rotation_14();
		Animator_t2776330603 * L_21 = __this->get__animator_17();
		int32_t L_22 = __this->get_iKGoal_10();
		NullCheck(L_21);
		Quaternion_t1553702882  L_23 = Animator_GetIKRotation_m3297713819(L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_20);
		FsmQuaternion_set_Value_m446581172(L_20, L_23, /*hidden argument*/NULL);
	}

IL_00b3:
	{
		FsmFloat_t2134102846 * L_24 = __this->get_positionWeight_15();
		NullCheck(L_24);
		bool L_25 = NamedVariable_get_IsNone_m281035543(L_24, /*hidden argument*/NULL);
		if (L_25)
		{
			goto IL_00df;
		}
	}
	{
		FsmFloat_t2134102846 * L_26 = __this->get_positionWeight_15();
		Animator_t2776330603 * L_27 = __this->get__animator_17();
		int32_t L_28 = __this->get_iKGoal_10();
		NullCheck(L_27);
		float L_29 = Animator_GetIKPositionWeight_m2079943564(L_27, L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		FsmFloat_set_Value_m1568963140(L_26, L_29, /*hidden argument*/NULL);
	}

IL_00df:
	{
		FsmFloat_t2134102846 * L_30 = __this->get_rotationWeight_16();
		NullCheck(L_30);
		bool L_31 = NamedVariable_get_IsNone_m281035543(L_30, /*hidden argument*/NULL);
		if (L_31)
		{
			goto IL_010b;
		}
	}
	{
		FsmFloat_t2134102846 * L_32 = __this->get_rotationWeight_16();
		Animator_t2776330603 * L_33 = __this->get__animator_17();
		int32_t L_34 = __this->get_iKGoal_10();
		NullCheck(L_33);
		float L_35 = Animator_GetIKRotationWeight_m3997781473(L_33, L_34, /*hidden argument*/NULL);
		NullCheck(L_32);
		FsmFloat_set_Value_m1568963140(L_32, L_35, /*hidden argument*/NULL);
	}

IL_010b:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorInt::.ctor()
extern "C"  void GetAnimatorInt__ctor_m2122261612 (GetAnimatorInt_t3593545018 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorInt::Reset()
extern "C"  void GetAnimatorInt_Reset_m4063661849 (GetAnimatorInt_t3593545018 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_parameter_10((FsmString_t952858651 *)NULL);
		__this->set_result_12((FsmInt_t1596138449 *)NULL);
		__this->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorInt::OnEnter()
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623_MethodInfo_var;
extern const MethodInfo* GetAnimatorInt_OnAnimatorMoveEvent_m903612173_MethodInfo_var;
extern const uint32_t GetAnimatorInt_OnEnter_m3070639427_MetadataUsageId;
extern "C"  void GetAnimatorInt_OnEnter_m3070639427 (GetAnimatorInt_t3593545018 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorInt_OnEnter_m3070639427_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_14(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_14();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GameObject_t3674682005 * L_9 = V_0;
		NullCheck(L_9);
		PlayMakerAnimatorMoveProxy_t4175490694 * L_10 = GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623(L_9, /*hidden argument*/GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623_MethodInfo_var);
		__this->set__animatorProxy_13(L_10);
		PlayMakerAnimatorMoveProxy_t4175490694 * L_11 = __this->get__animatorProxy_13();
		bool L_12 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_11, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_007d;
		}
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_13 = __this->get__animatorProxy_13();
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)GetAnimatorInt_OnAnimatorMoveEvent_m903612173_MethodInfo_var);
		Action_t3771233898 * L_15 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_15, __this, L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		PlayMakerAnimatorMoveProxy_add_OnAnimatorMoveEvent_m2003874533(L_13, L_15, /*hidden argument*/NULL);
	}

IL_007d:
	{
		FsmString_t952858651 * L_16 = __this->get_parameter_10();
		NullCheck(L_16);
		String_t* L_17 = FsmString_get_Value_m872383149(L_16, /*hidden argument*/NULL);
		int32_t L_18 = Animator_StringToHash_m4020897098(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		__this->set__paramID_15(L_18);
		GetAnimatorInt_GetParameter_m4211652747(__this, /*hidden argument*/NULL);
		bool L_19 = __this->get_everyFrame_11();
		if (L_19)
		{
			goto IL_00aa;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_00aa:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorInt::OnAnimatorMoveEvent()
extern "C"  void GetAnimatorInt_OnAnimatorMoveEvent_m903612173 (GetAnimatorInt_t3593545018 * __this, const MethodInfo* method)
{
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_13();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		GetAnimatorInt_GetParameter_m4211652747(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorInt::OnUpdate()
extern "C"  void GetAnimatorInt_OnUpdate_m4129068384 (GetAnimatorInt_t3593545018 * __this, const MethodInfo* method)
{
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_13();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		GetAnimatorInt_GetParameter_m4211652747(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorInt::GetParameter()
extern "C"  void GetAnimatorInt_GetParameter_m4211652747 (GetAnimatorInt_t3593545018 * __this, const MethodInfo* method)
{
	{
		Animator_t2776330603 * L_0 = __this->get__animator_14();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		FsmInt_t1596138449 * L_2 = __this->get_result_12();
		Animator_t2776330603 * L_3 = __this->get__animator_14();
		int32_t L_4 = __this->get__paramID_15();
		NullCheck(L_3);
		int32_t L_5 = Animator_GetInteger_m3944178743(L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmInt_set_Value_m2087583461(L_2, L_5, /*hidden argument*/NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorInt::OnExit()
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const MethodInfo* GetAnimatorInt_OnAnimatorMoveEvent_m903612173_MethodInfo_var;
extern const uint32_t GetAnimatorInt_OnExit_m2878919157_MetadataUsageId;
extern "C"  void GetAnimatorInt_OnExit_m2878919157 (GetAnimatorInt_t3593545018 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorInt_OnExit_m2878919157_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_13();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_2 = __this->get__animatorProxy_13();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)GetAnimatorInt_OnAnimatorMoveEvent_m903612173_MethodInfo_var);
		Action_t3771233898 * L_4 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_4, __this, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		PlayMakerAnimatorMoveProxy_remove_OnAnimatorMoveEvent_m2967347206(L_2, L_4, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsControlled::.ctor()
extern "C"  void GetAnimatorIsControlled__ctor_m2819795763 (GetAnimatorIsControlled_t34751331 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsHuman::.ctor()
extern "C"  void GetAnimatorIsHuman__ctor_m57879736 (GetAnimatorIsHuman_t55053166 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsHuman::Reset()
extern "C"  void GetAnimatorIsHuman_Reset_m1999279973 (GetAnimatorIsHuman_t55053166 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_isHuman_10((FsmBool_t1075959796 *)NULL);
		__this->set_isHumanEvent_11((FsmEvent_t2133468028 *)NULL);
		__this->set_isGenericEvent_12((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsHuman::OnEnter()
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorIsHuman_OnEnter_m3474547343_MetadataUsageId;
extern "C"  void GetAnimatorIsHuman_OnEnter_m3474547343 (GetAnimatorIsHuman_t55053166 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorIsHuman_OnEnter_m3474547343_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_13(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_13();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorIsHuman_DoCheckIsHuman_m3251639922(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsHuman::DoCheckIsHuman()
extern "C"  void GetAnimatorIsHuman_DoCheckIsHuman_m3251639922 (GetAnimatorIsHuman_t55053166 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Animator_t2776330603 * L_0 = __this->get__animator_13();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Animator_t2776330603 * L_2 = __this->get__animator_13();
		NullCheck(L_2);
		bool L_3 = Animator_get_isHuman_m4030001272(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		FsmBool_t1075959796 * L_4 = __this->get_isHuman_10();
		bool L_5 = V_0;
		NullCheck(L_4);
		FsmBool_set_Value_m1126216340(L_4, L_5, /*hidden argument*/NULL);
		bool L_6 = V_0;
		if (!L_6)
		{
			goto IL_0046;
		}
	}
	{
		Fsm_t1527112426 * L_7 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_8 = __this->get_isHumanEvent_11();
		NullCheck(L_7);
		Fsm_Event_m625948263(L_7, L_8, /*hidden argument*/NULL);
		goto IL_0057;
	}

IL_0046:
	{
		Fsm_t1527112426 * L_9 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_10 = __this->get_isGenericEvent_12();
		NullCheck(L_9);
		Fsm_Event_m625948263(L_9, L_10, /*hidden argument*/NULL);
	}

IL_0057:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::.ctor()
extern "C"  void GetAnimatorIsLayerInTransition__ctor_m3017439322 (GetAnimatorIsLayerInTransition_t3666075020 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::Reset()
extern "C"  void GetAnimatorIsLayerInTransition_Reset_m663872263 (GetAnimatorIsLayerInTransition_t3666075020 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_isInTransition_12((FsmBool_t1075959796 *)NULL);
		__this->set_isInTransitionEvent_13((FsmEvent_t2133468028 *)NULL);
		__this->set_isNotInTransitionEvent_14((FsmEvent_t2133468028 *)NULL);
		__this->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::OnEnter()
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623_MethodInfo_var;
extern const MethodInfo* GetAnimatorIsLayerInTransition_OnAnimatorMoveEvent_m493281915_MethodInfo_var;
extern const uint32_t GetAnimatorIsLayerInTransition_OnEnter_m47992241_MetadataUsageId;
extern "C"  void GetAnimatorIsLayerInTransition_OnEnter_m47992241 (GetAnimatorIsLayerInTransition_t3666075020 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorIsLayerInTransition_OnEnter_m47992241_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_16(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_16();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GameObject_t3674682005 * L_9 = V_0;
		NullCheck(L_9);
		PlayMakerAnimatorMoveProxy_t4175490694 * L_10 = GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623(L_9, /*hidden argument*/GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623_MethodInfo_var);
		__this->set__animatorProxy_15(L_10);
		PlayMakerAnimatorMoveProxy_t4175490694 * L_11 = __this->get__animatorProxy_15();
		bool L_12 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_11, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_007d;
		}
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_13 = __this->get__animatorProxy_15();
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)GetAnimatorIsLayerInTransition_OnAnimatorMoveEvent_m493281915_MethodInfo_var);
		Action_t3771233898 * L_15 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_15, __this, L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		PlayMakerAnimatorMoveProxy_add_OnAnimatorMoveEvent_m2003874533(L_13, L_15, /*hidden argument*/NULL);
	}

IL_007d:
	{
		GetAnimatorIsLayerInTransition_DoCheckIsInTransition_m701433465(__this, /*hidden argument*/NULL);
		bool L_16 = __this->get_everyFrame_11();
		if (L_16)
		{
			goto IL_0094;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0094:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::OnAnimatorMoveEvent()
extern "C"  void GetAnimatorIsLayerInTransition_OnAnimatorMoveEvent_m493281915 (GetAnimatorIsLayerInTransition_t3666075020 * __this, const MethodInfo* method)
{
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_15();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		GetAnimatorIsLayerInTransition_DoCheckIsInTransition_m701433465(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::OnUpdate()
extern "C"  void GetAnimatorIsLayerInTransition_OnUpdate_m621318834 (GetAnimatorIsLayerInTransition_t3666075020 * __this, const MethodInfo* method)
{
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_15();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		GetAnimatorIsLayerInTransition_DoCheckIsInTransition_m701433465(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::DoCheckIsInTransition()
extern "C"  void GetAnimatorIsLayerInTransition_DoCheckIsInTransition_m701433465 (GetAnimatorIsLayerInTransition_t3666075020 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Animator_t2776330603 * L_0 = __this->get__animator_16();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Animator_t2776330603 * L_2 = __this->get__animator_16();
		FsmInt_t1596138449 * L_3 = __this->get_layerIndex_10();
		NullCheck(L_3);
		int32_t L_4 = FsmInt_get_Value_m27059446(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_5 = Animator_IsInTransition_m2609196857(L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		FsmBool_t1075959796 * L_6 = __this->get_isInTransition_12();
		bool L_7 = V_0;
		NullCheck(L_6);
		FsmBool_set_Value_m1126216340(L_6, L_7, /*hidden argument*/NULL);
		bool L_8 = V_0;
		if (!L_8)
		{
			goto IL_0051;
		}
	}
	{
		Fsm_t1527112426 * L_9 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_10 = __this->get_isInTransitionEvent_13();
		NullCheck(L_9);
		Fsm_Event_m625948263(L_9, L_10, /*hidden argument*/NULL);
		goto IL_0062;
	}

IL_0051:
	{
		Fsm_t1527112426 * L_11 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_12 = __this->get_isNotInTransitionEvent_14();
		NullCheck(L_11);
		Fsm_Event_m625948263(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0062:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::OnExit()
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const MethodInfo* GetAnimatorIsLayerInTransition_OnAnimatorMoveEvent_m493281915_MethodInfo_var;
extern const uint32_t GetAnimatorIsLayerInTransition_OnExit_m564657095_MetadataUsageId;
extern "C"  void GetAnimatorIsLayerInTransition_OnExit_m564657095 (GetAnimatorIsLayerInTransition_t3666075020 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorIsLayerInTransition_OnExit_m564657095_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_15();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_2 = __this->get__animatorProxy_15();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)GetAnimatorIsLayerInTransition_OnAnimatorMoveEvent_m493281915_MethodInfo_var);
		Action_t3771233898 * L_4 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_4, __this, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		PlayMakerAnimatorMoveProxy_remove_OnAnimatorMoveEvent_m2967347206(L_2, L_4, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::.ctor()
extern "C"  void GetAnimatorIsMatchingTarget__ctor_m669529043 (GetAnimatorIsMatchingTarget_t2980401859 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::Reset()
extern "C"  void GetAnimatorIsMatchingTarget_Reset_m2610929280 (GetAnimatorIsMatchingTarget_t2980401859 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_isMatchingActive_11((FsmBool_t1075959796 *)NULL);
		__this->set_matchingActivatedEvent_12((FsmEvent_t2133468028 *)NULL);
		__this->set_matchingDeactivedEvent_13((FsmEvent_t2133468028 *)NULL);
		__this->set_everyFrame_10((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::OnEnter()
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623_MethodInfo_var;
extern const MethodInfo* GetAnimatorIsMatchingTarget_OnAnimatorMoveEvent_m3053884980_MethodInfo_var;
extern const uint32_t GetAnimatorIsMatchingTarget_OnEnter_m2859011818_MetadataUsageId;
extern "C"  void GetAnimatorIsMatchingTarget_OnEnter_m2859011818 (GetAnimatorIsMatchingTarget_t2980401859 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorIsMatchingTarget_OnEnter_m2859011818_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_15(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_15();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GameObject_t3674682005 * L_9 = V_0;
		NullCheck(L_9);
		PlayMakerAnimatorMoveProxy_t4175490694 * L_10 = GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623(L_9, /*hidden argument*/GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623_MethodInfo_var);
		__this->set__animatorProxy_14(L_10);
		PlayMakerAnimatorMoveProxy_t4175490694 * L_11 = __this->get__animatorProxy_14();
		bool L_12 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_11, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_007d;
		}
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_13 = __this->get__animatorProxy_14();
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)GetAnimatorIsMatchingTarget_OnAnimatorMoveEvent_m3053884980_MethodInfo_var);
		Action_t3771233898 * L_15 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_15, __this, L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		PlayMakerAnimatorMoveProxy_add_OnAnimatorMoveEvent_m2003874533(L_13, L_15, /*hidden argument*/NULL);
	}

IL_007d:
	{
		GetAnimatorIsMatchingTarget_DoCheckIsMatchingActive_m3097764091(__this, /*hidden argument*/NULL);
		bool L_16 = __this->get_everyFrame_10();
		if (L_16)
		{
			goto IL_0094;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0094:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::OnUpdate()
extern "C"  void GetAnimatorIsMatchingTarget_OnUpdate_m1863579801 (GetAnimatorIsMatchingTarget_t2980401859 * __this, const MethodInfo* method)
{
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_14();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		GetAnimatorIsMatchingTarget_DoCheckIsMatchingActive_m3097764091(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::OnAnimatorMoveEvent()
extern "C"  void GetAnimatorIsMatchingTarget_OnAnimatorMoveEvent_m3053884980 (GetAnimatorIsMatchingTarget_t2980401859 * __this, const MethodInfo* method)
{
	{
		GetAnimatorIsMatchingTarget_DoCheckIsMatchingActive_m3097764091(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::DoCheckIsMatchingActive()
extern "C"  void GetAnimatorIsMatchingTarget_DoCheckIsMatchingActive_m3097764091 (GetAnimatorIsMatchingTarget_t2980401859 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Animator_t2776330603 * L_0 = __this->get__animator_15();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Animator_t2776330603 * L_2 = __this->get__animator_15();
		NullCheck(L_2);
		bool L_3 = Animator_get_isMatchingTarget_m3696235301(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		FsmBool_t1075959796 * L_4 = __this->get_isMatchingActive_11();
		bool L_5 = V_0;
		NullCheck(L_4);
		FsmBool_set_Value_m1126216340(L_4, L_5, /*hidden argument*/NULL);
		bool L_6 = V_0;
		if (!L_6)
		{
			goto IL_0046;
		}
	}
	{
		Fsm_t1527112426 * L_7 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_8 = __this->get_matchingActivatedEvent_12();
		NullCheck(L_7);
		Fsm_Event_m625948263(L_7, L_8, /*hidden argument*/NULL);
		goto IL_0057;
	}

IL_0046:
	{
		Fsm_t1527112426 * L_9 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_10 = __this->get_matchingDeactivedEvent_13();
		NullCheck(L_9);
		Fsm_Event_m625948263(L_9, L_10, /*hidden argument*/NULL);
	}

IL_0057:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::OnExit()
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const MethodInfo* GetAnimatorIsMatchingTarget_OnAnimatorMoveEvent_m3053884980_MethodInfo_var;
extern const uint32_t GetAnimatorIsMatchingTarget_OnExit_m793882478_MetadataUsageId;
extern "C"  void GetAnimatorIsMatchingTarget_OnExit_m793882478 (GetAnimatorIsMatchingTarget_t2980401859 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorIsMatchingTarget_OnExit_m793882478_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_14();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_2 = __this->get__animatorProxy_14();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)GetAnimatorIsMatchingTarget_OnAnimatorMoveEvent_m3053884980_MethodInfo_var);
		Action_t3771233898 * L_4 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_4, __this, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		PlayMakerAnimatorMoveProxy_remove_OnAnimatorMoveEvent_m2967347206(L_2, L_4, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve::.ctor()
extern "C"  void GetAnimatorIsParameterControlledByCurve__ctor_m1539452256 (GetAnimatorIsParameterControlledByCurve_t1394563734 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve::Reset()
extern "C"  void GetAnimatorIsParameterControlledByCurve_Reset_m3480852493 (GetAnimatorIsParameterControlledByCurve_t1394563734 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_parameterName_10((FsmString_t952858651 *)NULL);
		__this->set_isControlledByCurve_11((FsmBool_t1075959796 *)NULL);
		__this->set_isControlledByCurveEvent_12((FsmEvent_t2133468028 *)NULL);
		__this->set_isNotControlledByCurveEvent_13((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve::OnEnter()
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorIsParameterControlledByCurve_OnEnter_m1336596791_MetadataUsageId;
extern "C"  void GetAnimatorIsParameterControlledByCurve_OnEnter_m1336596791 (GetAnimatorIsParameterControlledByCurve_t1394563734 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorIsParameterControlledByCurve_OnEnter_m1336596791_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_14(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_14();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorIsParameterControlledByCurve_DoCheckIsParameterControlledByCurve_m4108133670(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve::DoCheckIsParameterControlledByCurve()
extern "C"  void GetAnimatorIsParameterControlledByCurve_DoCheckIsParameterControlledByCurve_m4108133670 (GetAnimatorIsParameterControlledByCurve_t1394563734 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Animator_t2776330603 * L_0 = __this->get__animator_14();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Animator_t2776330603 * L_2 = __this->get__animator_14();
		FsmString_t952858651 * L_3 = __this->get_parameterName_10();
		NullCheck(L_3);
		String_t* L_4 = FsmString_get_Value_m872383149(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_5 = Animator_IsParameterControlledByCurve_m3328157587(L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		FsmBool_t1075959796 * L_6 = __this->get_isControlledByCurve_11();
		bool L_7 = V_0;
		NullCheck(L_6);
		FsmBool_set_Value_m1126216340(L_6, L_7, /*hidden argument*/NULL);
		bool L_8 = V_0;
		if (!L_8)
		{
			goto IL_0051;
		}
	}
	{
		Fsm_t1527112426 * L_9 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_10 = __this->get_isControlledByCurveEvent_12();
		NullCheck(L_9);
		Fsm_Event_m625948263(L_9, L_10, /*hidden argument*/NULL);
		goto IL_0062;
	}

IL_0051:
	{
		Fsm_t1527112426 * L_11 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_12 = __this->get_isNotControlledByCurveEvent_13();
		NullCheck(L_11);
		Fsm_Event_m625948263(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0062:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerCount::.ctor()
extern "C"  void GetAnimatorLayerCount__ctor_m2729802253 (GetAnimatorLayerCount_t3712536905 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerCount::Reset()
extern "C"  void GetAnimatorLayerCount_Reset_m376235194 (GetAnimatorLayerCount_t3712536905 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_layerCount_10((FsmInt_t1596138449 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerCount::OnEnter()
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorLayerCount_OnEnter_m2801643172_MetadataUsageId;
extern "C"  void GetAnimatorLayerCount_OnEnter_m2801643172 (GetAnimatorLayerCount_t3712536905 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorLayerCount_OnEnter_m2801643172_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_11(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_11();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorLayerCount_DoGetLayerCount_m1288341460(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerCount::DoGetLayerCount()
extern "C"  void GetAnimatorLayerCount_DoGetLayerCount_m1288341460 (GetAnimatorLayerCount_t3712536905 * __this, const MethodInfo* method)
{
	{
		Animator_t2776330603 * L_0 = __this->get__animator_11();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FsmInt_t1596138449 * L_2 = __this->get_layerCount_10();
		Animator_t2776330603 * L_3 = __this->get__animator_11();
		NullCheck(L_3);
		int32_t L_4 = Animator_get_layerCount_m3326924613(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmInt_set_Value_m2087583461(L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerName::.ctor()
extern "C"  void GetAnimatorLayerName__ctor_m3972242815 (GetAnimatorLayerName_t4020332743 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerName::Reset()
extern "C"  void GetAnimatorLayerName_Reset_m1618675756 (GetAnimatorLayerName_t4020332743 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_layerIndex_10((FsmInt_t1596138449 *)NULL);
		__this->set_layerName_11((FsmString_t952858651 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerName::OnEnter()
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorLayerName_OnEnter_m2786114966_MetadataUsageId;
extern "C"  void GetAnimatorLayerName_OnEnter_m2786114966 (GetAnimatorLayerName_t4020332743 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorLayerName_OnEnter_m2786114966_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_12(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_12();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorLayerName_DoGetLayerName_m639249302(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerName::DoGetLayerName()
extern "C"  void GetAnimatorLayerName_DoGetLayerName_m639249302 (GetAnimatorLayerName_t4020332743 * __this, const MethodInfo* method)
{
	{
		Animator_t2776330603 * L_0 = __this->get__animator_12();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FsmString_t952858651 * L_2 = __this->get_layerName_11();
		Animator_t2776330603 * L_3 = __this->get__animator_12();
		FsmInt_t1596138449 * L_4 = __this->get_layerIndex_10();
		NullCheck(L_4);
		int32_t L_5 = FsmInt_get_Value_m27059446(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_6 = Animator_GetLayerName_m3480300056(L_3, L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmString_set_Value_m829393196(L_2, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayersAffectMassCenter::.ctor()
extern "C"  void GetAnimatorLayersAffectMassCenter__ctor_m1837935563 (GetAnimatorLayersAffectMassCenter_t2244244939 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayersAffectMassCenter::Reset()
extern "C"  void GetAnimatorLayersAffectMassCenter_Reset_m3779335800 (GetAnimatorLayersAffectMassCenter_t2244244939 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_affectMassCenter_10((FsmBool_t1075959796 *)NULL);
		__this->set_affectMassCenterEvent_11((FsmEvent_t2133468028 *)NULL);
		__this->set_doNotAffectMassCenterEvent_12((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayersAffectMassCenter::OnEnter()
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorLayersAffectMassCenter_OnEnter_m416245986_MetadataUsageId;
extern "C"  void GetAnimatorLayersAffectMassCenter_OnEnter_m416245986 (GetAnimatorLayersAffectMassCenter_t2244244939 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorLayersAffectMassCenter_OnEnter_m416245986_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_13(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_13();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorLayersAffectMassCenter_CheckAffectMassCenter_m3496781295(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayersAffectMassCenter::CheckAffectMassCenter()
extern "C"  void GetAnimatorLayersAffectMassCenter_CheckAffectMassCenter_m3496781295 (GetAnimatorLayersAffectMassCenter_t2244244939 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Animator_t2776330603 * L_0 = __this->get__animator_13();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Animator_t2776330603 * L_2 = __this->get__animator_13();
		NullCheck(L_2);
		bool L_3 = Animator_get_layersAffectMassCenter_m3409296173(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		FsmBool_t1075959796 * L_4 = __this->get_affectMassCenter_10();
		bool L_5 = V_0;
		NullCheck(L_4);
		FsmBool_set_Value_m1126216340(L_4, L_5, /*hidden argument*/NULL);
		bool L_6 = V_0;
		if (!L_6)
		{
			goto IL_0046;
		}
	}
	{
		Fsm_t1527112426 * L_7 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_8 = __this->get_affectMassCenterEvent_11();
		NullCheck(L_7);
		Fsm_Event_m625948263(L_7, L_8, /*hidden argument*/NULL);
		goto IL_0057;
	}

IL_0046:
	{
		Fsm_t1527112426 * L_9 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_10 = __this->get_doNotAffectMassCenterEvent_12();
		NullCheck(L_9);
		Fsm_Event_m625948263(L_9, L_10, /*hidden argument*/NULL);
	}

IL_0057:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight::.ctor()
extern "C"  void GetAnimatorLayerWeight__ctor_m2928099954 (GetAnimatorLayerWeight_t3333517428 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight::Reset()
extern "C"  void GetAnimatorLayerWeight_Reset_m574532895 (GetAnimatorLayerWeight_t3333517428 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_layerIndex_10((FsmInt_t1596138449 *)NULL);
		__this->set_layerWeight_12((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight::OnEnter()
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623_MethodInfo_var;
extern const MethodInfo* GetAnimatorLayerWeight_OnAnimatorMoveEvent_m2306613907_MethodInfo_var;
extern const uint32_t GetAnimatorLayerWeight_OnEnter_m92205513_MetadataUsageId;
extern "C"  void GetAnimatorLayerWeight_OnEnter_m92205513 (GetAnimatorLayerWeight_t3333517428 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorLayerWeight_OnEnter_m92205513_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_14(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_14();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GameObject_t3674682005 * L_9 = V_0;
		NullCheck(L_9);
		PlayMakerAnimatorMoveProxy_t4175490694 * L_10 = GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623(L_9, /*hidden argument*/GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623_MethodInfo_var);
		__this->set__animatorProxy_13(L_10);
		PlayMakerAnimatorMoveProxy_t4175490694 * L_11 = __this->get__animatorProxy_13();
		bool L_12 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_11, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_007d;
		}
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_13 = __this->get__animatorProxy_13();
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)GetAnimatorLayerWeight_OnAnimatorMoveEvent_m2306613907_MethodInfo_var);
		Action_t3771233898 * L_15 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_15, __this, L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		PlayMakerAnimatorMoveProxy_add_OnAnimatorMoveEvent_m2003874533(L_13, L_15, /*hidden argument*/NULL);
	}

IL_007d:
	{
		GetAnimatorLayerWeight_GetLayerWeight_m1458072709(__this, /*hidden argument*/NULL);
		bool L_16 = __this->get_everyFrame_11();
		if (L_16)
		{
			goto IL_0094;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0094:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight::OnAnimatorMoveEvent()
extern "C"  void GetAnimatorLayerWeight_OnAnimatorMoveEvent_m2306613907 (GetAnimatorLayerWeight_t3333517428 * __this, const MethodInfo* method)
{
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_13();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		GetAnimatorLayerWeight_GetLayerWeight_m1458072709(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight::OnUpdate()
extern "C"  void GetAnimatorLayerWeight_OnUpdate_m1991930266 (GetAnimatorLayerWeight_t3333517428 * __this, const MethodInfo* method)
{
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_13();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		GetAnimatorLayerWeight_GetLayerWeight_m1458072709(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight::GetLayerWeight()
extern "C"  void GetAnimatorLayerWeight_GetLayerWeight_m1458072709 (GetAnimatorLayerWeight_t3333517428 * __this, const MethodInfo* method)
{
	{
		Animator_t2776330603 * L_0 = __this->get__animator_14();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		FsmFloat_t2134102846 * L_2 = __this->get_layerWeight_12();
		Animator_t2776330603 * L_3 = __this->get__animator_14();
		FsmInt_t1596138449 * L_4 = __this->get_layerIndex_10();
		NullCheck(L_4);
		int32_t L_5 = FsmInt_get_Value_m27059446(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		float L_6 = Animator_GetLayerWeight_m3878421230(L_3, L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmFloat_set_Value_m1568963140(L_2, L_6, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight::OnExit()
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const MethodInfo* GetAnimatorLayerWeight_OnAnimatorMoveEvent_m2306613907_MethodInfo_var;
extern const uint32_t GetAnimatorLayerWeight_OnExit_m2090103983_MetadataUsageId;
extern "C"  void GetAnimatorLayerWeight_OnExit_m2090103983 (GetAnimatorLayerWeight_t3333517428 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorLayerWeight_OnExit_m2090103983_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_13();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_2 = __this->get__animatorProxy_13();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)GetAnimatorLayerWeight_OnAnimatorMoveEvent_m2306613907_MethodInfo_var);
		Action_t3771233898 * L_4 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_4, __this, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		PlayMakerAnimatorMoveProxy_remove_OnAnimatorMoveEvent_m2967347206(L_2, L_4, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLeftFootBottomHeight::.ctor()
extern "C"  void GetAnimatorLeftFootBottomHeight__ctor_m1634568068 (GetAnimatorLeftFootBottomHeight_t4090212338 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLeftFootBottomHeight::Reset()
extern "C"  void GetAnimatorLeftFootBottomHeight_Reset_m3575968305 (GetAnimatorLeftFootBottomHeight_t4090212338 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_leftFootHeight_10((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLeftFootBottomHeight::OnEnter()
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorLeftFootBottomHeight_OnEnter_m2548578907_MetadataUsageId;
extern "C"  void GetAnimatorLeftFootBottomHeight_OnEnter_m2548578907 (GetAnimatorLeftFootBottomHeight_t4090212338 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorLeftFootBottomHeight_OnEnter_m2548578907_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_12(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_12();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorLeftFootBottomHeight__getLeftFootBottonHeight_m3696279871(__this, /*hidden argument*/NULL);
		bool L_9 = __this->get_everyFrame_11();
		if (L_9)
		{
			goto IL_0060;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0060:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLeftFootBottomHeight::OnLateUpdate()
extern "C"  void GetAnimatorLeftFootBottomHeight_OnLateUpdate_m3660389646 (GetAnimatorLeftFootBottomHeight_t4090212338 * __this, const MethodInfo* method)
{
	{
		GetAnimatorLeftFootBottomHeight__getLeftFootBottonHeight_m3696279871(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLeftFootBottomHeight::_getLeftFootBottonHeight()
extern "C"  void GetAnimatorLeftFootBottomHeight__getLeftFootBottonHeight_m3696279871 (GetAnimatorLeftFootBottomHeight_t4090212338 * __this, const MethodInfo* method)
{
	{
		Animator_t2776330603 * L_0 = __this->get__animator_12();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		FsmFloat_t2134102846 * L_2 = __this->get_leftFootHeight_10();
		Animator_t2776330603 * L_3 = __this->get__animator_12();
		NullCheck(L_3);
		float L_4 = Animator_get_leftFeetBottomHeight_m4166530042(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmFloat_set_Value_m1568963140(L_2, L_4, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::.ctor()
extern "C"  void GetAnimatorNextStateInfo__ctor_m1442289615 (GetAnimatorNextStateInfo_t3502000247 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::Reset()
extern "C"  void GetAnimatorNextStateInfo_Reset_m3383689852 (GetAnimatorNextStateInfo_t3502000247 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_layerIndex_10((FsmInt_t1596138449 *)NULL);
		__this->set_name_12((FsmString_t952858651 *)NULL);
		__this->set_nameHash_13((FsmInt_t1596138449 *)NULL);
		__this->set_fullPathHash_14((FsmInt_t1596138449 *)NULL);
		__this->set_shortPathHash_15((FsmInt_t1596138449 *)NULL);
		__this->set_tagHash_16((FsmInt_t1596138449 *)NULL);
		__this->set_length_18((FsmFloat_t2134102846 *)NULL);
		__this->set_normalizedTime_19((FsmFloat_t2134102846 *)NULL);
		__this->set_isStateLooping_17((FsmBool_t1075959796 *)NULL);
		__this->set_loopCount_20((FsmInt_t1596138449 *)NULL);
		__this->set_currentLoopProgress_21((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::OnEnter()
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623_MethodInfo_var;
extern const MethodInfo* GetAnimatorNextStateInfo_OnAnimatorMoveEvent_m3957936432_MethodInfo_var;
extern const uint32_t GetAnimatorNextStateInfo_OnEnter_m2452579302_MetadataUsageId;
extern "C"  void GetAnimatorNextStateInfo_OnEnter_m2452579302 (GetAnimatorNextStateInfo_t3502000247 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorNextStateInfo_OnEnter_m2452579302_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_23(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_23();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GameObject_t3674682005 * L_9 = V_0;
		NullCheck(L_9);
		PlayMakerAnimatorMoveProxy_t4175490694 * L_10 = GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623(L_9, /*hidden argument*/GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623_MethodInfo_var);
		__this->set__animatorProxy_22(L_10);
		PlayMakerAnimatorMoveProxy_t4175490694 * L_11 = __this->get__animatorProxy_22();
		bool L_12 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_11, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_007d;
		}
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_13 = __this->get__animatorProxy_22();
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)GetAnimatorNextStateInfo_OnAnimatorMoveEvent_m3957936432_MethodInfo_var);
		Action_t3771233898 * L_15 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_15, __this, L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		PlayMakerAnimatorMoveProxy_add_OnAnimatorMoveEvent_m2003874533(L_13, L_15, /*hidden argument*/NULL);
	}

IL_007d:
	{
		GetAnimatorNextStateInfo_GetLayerInfo_m3523314750(__this, /*hidden argument*/NULL);
		bool L_16 = __this->get_everyFrame_11();
		if (L_16)
		{
			goto IL_0094;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0094:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::OnAnimatorMoveEvent()
extern "C"  void GetAnimatorNextStateInfo_OnAnimatorMoveEvent_m3957936432 (GetAnimatorNextStateInfo_t3502000247 * __this, const MethodInfo* method)
{
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_22();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		GetAnimatorNextStateInfo_GetLayerInfo_m3523314750(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::OnUpdate()
extern "C"  void GetAnimatorNextStateInfo_OnUpdate_m2149073693 (GetAnimatorNextStateInfo_t3502000247 * __this, const MethodInfo* method)
{
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_22();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		GetAnimatorNextStateInfo_GetLayerInfo_m3523314750(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::GetLayerInfo()
extern "C"  void GetAnimatorNextStateInfo_GetLayerInfo_m3523314750 (GetAnimatorNextStateInfo_t3502000247 * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t323110318  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_t2776330603 * L_0 = __this->get__animator_23();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_012f;
		}
	}
	{
		Animator_t2776330603 * L_2 = __this->get__animator_23();
		FsmInt_t1596138449 * L_3 = __this->get_layerIndex_10();
		NullCheck(L_3);
		int32_t L_4 = FsmInt_get_Value_m27059446(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		AnimatorStateInfo_t323110318  L_5 = Animator_GetNextAnimatorStateInfo_m791156688(L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		FsmInt_t1596138449 * L_6 = __this->get_fullPathHash_14();
		int32_t L_7 = AnimatorStateInfo_get_fullPathHash_m3257074542((&V_0), /*hidden argument*/NULL);
		NullCheck(L_6);
		FsmInt_set_Value_m2087583461(L_6, L_7, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_8 = __this->get_shortPathHash_15();
		int32_t L_9 = AnimatorStateInfo_get_shortNameHash_m994885515((&V_0), /*hidden argument*/NULL);
		NullCheck(L_8);
		FsmInt_set_Value_m2087583461(L_8, L_9, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_10 = __this->get_nameHash_13();
		int32_t L_11 = AnimatorStateInfo_get_shortNameHash_m994885515((&V_0), /*hidden argument*/NULL);
		NullCheck(L_10);
		FsmInt_set_Value_m2087583461(L_10, L_11, /*hidden argument*/NULL);
		FsmString_t952858651 * L_12 = __this->get_name_12();
		NullCheck(L_12);
		bool L_13 = NamedVariable_get_IsNone_m281035543(L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_008f;
		}
	}
	{
		FsmString_t952858651 * L_14 = __this->get_name_12();
		Animator_t2776330603 * L_15 = __this->get__animator_23();
		FsmInt_t1596138449 * L_16 = __this->get_layerIndex_10();
		NullCheck(L_16);
		int32_t L_17 = FsmInt_get_Value_m27059446(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		String_t* L_18 = Animator_GetLayerName_m3480300056(L_15, L_17, /*hidden argument*/NULL);
		NullCheck(L_14);
		FsmString_set_Value_m829393196(L_14, L_18, /*hidden argument*/NULL);
	}

IL_008f:
	{
		FsmInt_t1596138449 * L_19 = __this->get_tagHash_16();
		int32_t L_20 = AnimatorStateInfo_get_tagHash_m3543262078((&V_0), /*hidden argument*/NULL);
		NullCheck(L_19);
		FsmInt_set_Value_m2087583461(L_19, L_20, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_21 = __this->get_length_18();
		float L_22 = AnimatorStateInfo_get_length_m3147284742((&V_0), /*hidden argument*/NULL);
		NullCheck(L_21);
		FsmFloat_set_Value_m1568963140(L_21, L_22, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_23 = __this->get_isStateLooping_17();
		bool L_24 = AnimatorStateInfo_get_loop_m1495892586((&V_0), /*hidden argument*/NULL);
		NullCheck(L_23);
		FsmBool_set_Value_m1126216340(L_23, L_24, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_25 = __this->get_normalizedTime_19();
		float L_26 = AnimatorStateInfo_get_normalizedTime_m2531821060((&V_0), /*hidden argument*/NULL);
		NullCheck(L_25);
		FsmFloat_set_Value_m1568963140(L_25, L_26, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_27 = __this->get_loopCount_20();
		NullCheck(L_27);
		bool L_28 = NamedVariable_get_IsNone_m281035543(L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00f7;
		}
	}
	{
		FsmFloat_t2134102846 * L_29 = __this->get_currentLoopProgress_21();
		NullCheck(L_29);
		bool L_30 = NamedVariable_get_IsNone_m281035543(L_29, /*hidden argument*/NULL);
		if (L_30)
		{
			goto IL_012f;
		}
	}

IL_00f7:
	{
		FsmInt_t1596138449 * L_31 = __this->get_loopCount_20();
		float L_32 = AnimatorStateInfo_get_normalizedTime_m2531821060((&V_0), /*hidden argument*/NULL);
		double L_33 = Math_Truncate_m534017384(NULL /*static, unused*/, (((double)((double)L_32))), /*hidden argument*/NULL);
		NullCheck(L_31);
		FsmInt_set_Value_m2087583461(L_31, (((int32_t)((int32_t)L_33))), /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_34 = __this->get_currentLoopProgress_21();
		float L_35 = AnimatorStateInfo_get_normalizedTime_m2531821060((&V_0), /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_36 = __this->get_loopCount_20();
		NullCheck(L_36);
		int32_t L_37 = FsmInt_get_Value_m27059446(L_36, /*hidden argument*/NULL);
		NullCheck(L_34);
		FsmFloat_set_Value_m1568963140(L_34, ((float)((float)L_35-(float)(((float)((float)L_37))))), /*hidden argument*/NULL);
	}

IL_012f:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::OnExit()
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const MethodInfo* GetAnimatorNextStateInfo_OnAnimatorMoveEvent_m3957936432_MethodInfo_var;
extern const uint32_t GetAnimatorNextStateInfo_OnExit_m3274623730_MetadataUsageId;
extern "C"  void GetAnimatorNextStateInfo_OnExit_m3274623730 (GetAnimatorNextStateInfo_t3502000247 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorNextStateInfo_OnExit_m3274623730_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_22();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_2 = __this->get__animatorProxy_22();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)GetAnimatorNextStateInfo_OnAnimatorMoveEvent_m3957936432_MethodInfo_var);
		Action_t3771233898 * L_4 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_4, __this, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		PlayMakerAnimatorMoveProxy_remove_OnAnimatorMoveEvent_m2967347206(L_2, L_4, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPivot::.ctor()
extern "C"  void GetAnimatorPivot__ctor_m1612529529 (GetAnimatorPivot_t957485453 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPivot::Reset()
extern "C"  void GetAnimatorPivot_Reset_m3553929766 (GetAnimatorPivot_t957485453 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_pivotWeight_11((FsmFloat_t2134102846 *)NULL);
		__this->set_pivotPosition_12((FsmVector3_t533912882 *)NULL);
		__this->set_everyFrame_10((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPivot::OnEnter()
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623_MethodInfo_var;
extern const MethodInfo* GetAnimatorPivot_OnAnimatorMoveEvent_m2673035098_MethodInfo_var;
extern const uint32_t GetAnimatorPivot_OnEnter_m2844379408_MetadataUsageId;
extern "C"  void GetAnimatorPivot_OnEnter_m2844379408 (GetAnimatorPivot_t957485453 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorPivot_OnEnter_m2844379408_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_14(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_14();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GameObject_t3674682005 * L_9 = V_0;
		NullCheck(L_9);
		PlayMakerAnimatorMoveProxy_t4175490694 * L_10 = GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623(L_9, /*hidden argument*/GameObject_GetComponent_TisPlayMakerAnimatorMoveProxy_t4175490694_m3232057623_MethodInfo_var);
		__this->set__animatorProxy_13(L_10);
		PlayMakerAnimatorMoveProxy_t4175490694 * L_11 = __this->get__animatorProxy_13();
		bool L_12 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_11, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_007d;
		}
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_13 = __this->get__animatorProxy_13();
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)GetAnimatorPivot_OnAnimatorMoveEvent_m2673035098_MethodInfo_var);
		Action_t3771233898 * L_15 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_15, __this, L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		PlayMakerAnimatorMoveProxy_add_OnAnimatorMoveEvent_m2003874533(L_13, L_15, /*hidden argument*/NULL);
	}

IL_007d:
	{
		GetAnimatorPivot_DoCheckPivot_m312014448(__this, /*hidden argument*/NULL);
		bool L_16 = __this->get_everyFrame_10();
		if (L_16)
		{
			goto IL_0094;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0094:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPivot::OnAnimatorMoveEvent()
extern "C"  void GetAnimatorPivot_OnAnimatorMoveEvent_m2673035098 (GetAnimatorPivot_t957485453 * __this, const MethodInfo* method)
{
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_13();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		GetAnimatorPivot_DoCheckPivot_m312014448(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPivot::OnUpdate()
extern "C"  void GetAnimatorPivot_OnUpdate_m1409975091 (GetAnimatorPivot_t957485453 * __this, const MethodInfo* method)
{
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_13();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		GetAnimatorPivot_DoCheckPivot_m312014448(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPivot::DoCheckPivot()
extern "C"  void GetAnimatorPivot_DoCheckPivot_m312014448 (GetAnimatorPivot_t957485453 * __this, const MethodInfo* method)
{
	{
		Animator_t2776330603 * L_0 = __this->get__animator_14();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FsmFloat_t2134102846 * L_2 = __this->get_pivotWeight_11();
		Animator_t2776330603 * L_3 = __this->get__animator_14();
		NullCheck(L_3);
		float L_4 = Animator_get_pivotWeight_m1500566793(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmFloat_set_Value_m1568963140(L_2, L_4, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_5 = __this->get_pivotPosition_12();
		Animator_t2776330603 * L_6 = __this->get__animator_14();
		NullCheck(L_6);
		Vector3_t4282066566  L_7 = Animator_get_pivotPosition_m1479929804(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		FsmVector3_set_Value_m716982822(L_5, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPivot::OnExit()
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const MethodInfo* GetAnimatorPivot_OnAnimatorMoveEvent_m2673035098_MethodInfo_var;
extern const uint32_t GetAnimatorPivot_OnExit_m4257093768_MetadataUsageId;
extern "C"  void GetAnimatorPivot_OnExit_m4257093768 (GetAnimatorPivot_t957485453 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorPivot_OnExit_m4257093768_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_0 = __this->get__animatorProxy_13();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		PlayMakerAnimatorMoveProxy_t4175490694 * L_2 = __this->get__animatorProxy_13();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)GetAnimatorPivot_OnAnimatorMoveEvent_m2673035098_MethodInfo_var);
		Action_t3771233898 * L_4 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_4, __this, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		PlayMakerAnimatorMoveProxy_remove_OnAnimatorMoveEvent_m2967347206(L_2, L_4, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackSpeed::.ctor()
extern "C"  void GetAnimatorPlayBackSpeed__ctor_m2603572271 (GetAnimatorPlayBackSpeed_t331618327 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackSpeed::Reset()
extern "C"  void GetAnimatorPlayBackSpeed_Reset_m250005212 (GetAnimatorPlayBackSpeed_t331618327 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_playBackSpeed_10((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackSpeed::OnEnter()
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorPlayBackSpeed_OnEnter_m1753714758_MetadataUsageId;
extern "C"  void GetAnimatorPlayBackSpeed_OnEnter_m1753714758 (GetAnimatorPlayBackSpeed_t331618327 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorPlayBackSpeed_OnEnter_m1753714758_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_12(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_12();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorPlayBackSpeed_GetPlayBackSpeed_m3957072587(__this, /*hidden argument*/NULL);
		bool L_9 = __this->get_everyFrame_11();
		if (L_9)
		{
			goto IL_0060;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0060:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackSpeed::OnUpdate()
extern "C"  void GetAnimatorPlayBackSpeed_OnUpdate_m1959109309 (GetAnimatorPlayBackSpeed_t331618327 * __this, const MethodInfo* method)
{
	{
		GetAnimatorPlayBackSpeed_GetPlayBackSpeed_m3957072587(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackSpeed::GetPlayBackSpeed()
extern "C"  void GetAnimatorPlayBackSpeed_GetPlayBackSpeed_m3957072587 (GetAnimatorPlayBackSpeed_t331618327 * __this, const MethodInfo* method)
{
	{
		Animator_t2776330603 * L_0 = __this->get__animator_12();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		FsmFloat_t2134102846 * L_2 = __this->get_playBackSpeed_10();
		Animator_t2776330603 * L_3 = __this->get__animator_12();
		NullCheck(L_3);
		float L_4 = Animator_get_speed_m1893369654(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmFloat_set_Value_m1568963140(L_2, L_4, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackTime::.ctor()
extern "C"  void GetAnimatorPlayBackTime__ctor_m876104163 (GetAnimatorPlayBackTime_t2941148851 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackTime::Reset()
extern "C"  void GetAnimatorPlayBackTime_Reset_m2817504400 (GetAnimatorPlayBackTime_t2941148851 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_9((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_playBackTime_10((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackTime::OnEnter()
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorPlayBackTime_OnEnter_m3809206522_MetadataUsageId;
extern "C"  void GetAnimatorPlayBackTime_OnEnter_m3809206522 (GetAnimatorPlayBackTime_t2941148851 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorPlayBackTime_OnEnter_m3809206522_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_12(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_12();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorPlayBackTime_GetPlayBackTime_m2202807711(__this, /*hidden argument*/NULL);
		bool L_9 = __this->get_everyFrame_11();
		if (L_9)
		{
			goto IL_0060;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0060:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackTime::OnUpdate()
extern "C"  void GetAnimatorPlayBackTime_OnUpdate_m1254844553 (GetAnimatorPlayBackTime_t2941148851 * __this, const MethodInfo* method)
{
	{
		GetAnimatorPlayBackTime_GetPlayBackTime_m2202807711(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackTime::GetPlayBackTime()
extern "C"  void GetAnimatorPlayBackTime_GetPlayBackTime_m2202807711 (GetAnimatorPlayBackTime_t2941148851 * __this, const MethodInfo* method)
{
	{
		Animator_t2776330603 * L_0 = __this->get__animator_12();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		FsmFloat_t2134102846 * L_2 = __this->get_playBackTime_10();
		Animator_t2776330603 * L_3 = __this->get__animator_12();
		NullCheck(L_3);
		float L_4 = Animator_get_playbackTime_m3871048475(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmFloat_set_Value_m1568963140(L_2, L_4, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
