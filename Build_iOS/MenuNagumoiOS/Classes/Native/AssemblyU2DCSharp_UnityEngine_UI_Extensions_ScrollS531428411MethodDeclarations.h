﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Extensions.ScrollSnap
struct ScrollSnap_t531428411;
// UnityEngine.UI.Extensions.ScrollSnap/PageSnapChange
struct PageSnapChange_t3629032906;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1848751023;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_Extensions_Scroll3629032906.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1848751023.h"

// System.Void UnityEngine.UI.Extensions.ScrollSnap::.ctor()
extern "C"  void ScrollSnap__ctor_m1781114829 (ScrollSnap_t531428411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnap::add_onPageChange(UnityEngine.UI.Extensions.ScrollSnap/PageSnapChange)
extern "C"  void ScrollSnap_add_onPageChange_m4121468565 (ScrollSnap_t531428411 * __this, PageSnapChange_t3629032906 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnap::remove_onPageChange(UnityEngine.UI.Extensions.ScrollSnap/PageSnapChange)
extern "C"  void ScrollSnap_remove_onPageChange_m4186988644 (ScrollSnap_t531428411 * __this, PageSnapChange_t3629032906 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnap::Awake()
extern "C"  void ScrollSnap_Awake_m2018720048 (ScrollSnap_t531428411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnap::Start()
extern "C"  void ScrollSnap_Start_m728252621 (ScrollSnap_t531428411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnap::UpdateListItemsSize()
extern "C"  void ScrollSnap_UpdateListItemsSize_m1076334469 (ScrollSnap_t531428411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnap::UpdateListItemPositions()
extern "C"  void ScrollSnap_UpdateListItemPositions_m2299341627 (ScrollSnap_t531428411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnap::ResetPage()
extern "C"  void ScrollSnap_ResetPage_m1363580137 (ScrollSnap_t531428411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnap::UpdateScrollbar(System.Boolean)
extern "C"  void ScrollSnap_UpdateScrollbar_m3391960063 (ScrollSnap_t531428411 * __this, bool ___linkSteps0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnap::LateUpdate()
extern "C"  void ScrollSnap_LateUpdate_m1466792614 (ScrollSnap_t531428411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnap::NextScreen()
extern "C"  void ScrollSnap_NextScreen_m3559784662 (ScrollSnap_t531428411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnap::PreviousScreen()
extern "C"  void ScrollSnap_PreviousScreen_m275698138 (ScrollSnap_t531428411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnap::NextScreenCommand()
extern "C"  void ScrollSnap_NextScreenCommand_m3802748919 (ScrollSnap_t531428411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnap::PrevScreenCommand()
extern "C"  void ScrollSnap_PrevScreenCommand_m1072617655 (ScrollSnap_t531428411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.UI.Extensions.ScrollSnap::CurrentPage()
extern "C"  int32_t ScrollSnap_CurrentPage_m1707797061 (ScrollSnap_t531428411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnap::ChangePage(System.Int32)
extern "C"  void ScrollSnap_ChangePage_m277629511 (ScrollSnap_t531428411 * __this, int32_t ___page0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnap::PageChanged(System.Int32)
extern "C"  void ScrollSnap_PageChanged_m652732065 (ScrollSnap_t531428411 * __this, int32_t ___currentPage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnap::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ScrollSnap_OnBeginDrag_m2177911637 (ScrollSnap_t531428411 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnap::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ScrollSnap_OnEndDrag_m505588259 (ScrollSnap_t531428411 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnap::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ScrollSnap_OnDrag_m2272579444 (ScrollSnap_t531428411 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnap::<Awake>m__8C()
extern "C"  void ScrollSnap_U3CAwakeU3Em__8C_m2446889018 (ScrollSnap_t531428411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollSnap::<Awake>m__8D()
extern "C"  void ScrollSnap_U3CAwakeU3Em__8D_m2446889979 (ScrollSnap_t531428411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
