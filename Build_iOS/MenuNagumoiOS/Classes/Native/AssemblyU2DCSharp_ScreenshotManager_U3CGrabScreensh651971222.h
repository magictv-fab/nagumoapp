﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScreenshotManager/<GrabScreenshot>c__Iterator30
struct  U3CGrabScreenshotU3Ec__Iterator30_t651971222  : public Il2CppObject
{
public:
	// UnityEngine.Rect ScreenshotManager/<GrabScreenshot>c__Iterator30::screenArea
	Rect_t4241904616  ___screenArea_0;
	// UnityEngine.Texture2D ScreenshotManager/<GrabScreenshot>c__Iterator30::<texture>__0
	Texture2D_t3884108195 * ___U3CtextureU3E__0_1;
	// System.String ScreenshotManager/<GrabScreenshot>c__Iterator30::fileType
	String_t* ___fileType_2;
	// System.Byte[] ScreenshotManager/<GrabScreenshot>c__Iterator30::<bytes>__1
	ByteU5BU5D_t4260760469* ___U3CbytesU3E__1_3;
	// System.String ScreenshotManager/<GrabScreenshot>c__Iterator30::<fileExt>__2
	String_t* ___U3CfileExtU3E__2_4;
	// System.String ScreenshotManager/<GrabScreenshot>c__Iterator30::<date>__3
	String_t* ___U3CdateU3E__3_5;
	// System.String ScreenshotManager/<GrabScreenshot>c__Iterator30::fileName
	String_t* ___fileName_6;
	// System.String ScreenshotManager/<GrabScreenshot>c__Iterator30::<screenshotFilename>__4
	String_t* ___U3CscreenshotFilenameU3E__4_7;
	// System.String ScreenshotManager/<GrabScreenshot>c__Iterator30::<path>__5
	String_t* ___U3CpathU3E__5_8;
	// System.Int32 ScreenshotManager/<GrabScreenshot>c__Iterator30::$PC
	int32_t ___U24PC_9;
	// System.Object ScreenshotManager/<GrabScreenshot>c__Iterator30::$current
	Il2CppObject * ___U24current_10;
	// UnityEngine.Rect ScreenshotManager/<GrabScreenshot>c__Iterator30::<$>screenArea
	Rect_t4241904616  ___U3CU24U3EscreenArea_11;
	// System.String ScreenshotManager/<GrabScreenshot>c__Iterator30::<$>fileType
	String_t* ___U3CU24U3EfileType_12;
	// System.String ScreenshotManager/<GrabScreenshot>c__Iterator30::<$>fileName
	String_t* ___U3CU24U3EfileName_13;

public:
	inline static int32_t get_offset_of_screenArea_0() { return static_cast<int32_t>(offsetof(U3CGrabScreenshotU3Ec__Iterator30_t651971222, ___screenArea_0)); }
	inline Rect_t4241904616  get_screenArea_0() const { return ___screenArea_0; }
	inline Rect_t4241904616 * get_address_of_screenArea_0() { return &___screenArea_0; }
	inline void set_screenArea_0(Rect_t4241904616  value)
	{
		___screenArea_0 = value;
	}

	inline static int32_t get_offset_of_U3CtextureU3E__0_1() { return static_cast<int32_t>(offsetof(U3CGrabScreenshotU3Ec__Iterator30_t651971222, ___U3CtextureU3E__0_1)); }
	inline Texture2D_t3884108195 * get_U3CtextureU3E__0_1() const { return ___U3CtextureU3E__0_1; }
	inline Texture2D_t3884108195 ** get_address_of_U3CtextureU3E__0_1() { return &___U3CtextureU3E__0_1; }
	inline void set_U3CtextureU3E__0_1(Texture2D_t3884108195 * value)
	{
		___U3CtextureU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtextureU3E__0_1, value);
	}

	inline static int32_t get_offset_of_fileType_2() { return static_cast<int32_t>(offsetof(U3CGrabScreenshotU3Ec__Iterator30_t651971222, ___fileType_2)); }
	inline String_t* get_fileType_2() const { return ___fileType_2; }
	inline String_t** get_address_of_fileType_2() { return &___fileType_2; }
	inline void set_fileType_2(String_t* value)
	{
		___fileType_2 = value;
		Il2CppCodeGenWriteBarrier(&___fileType_2, value);
	}

	inline static int32_t get_offset_of_U3CbytesU3E__1_3() { return static_cast<int32_t>(offsetof(U3CGrabScreenshotU3Ec__Iterator30_t651971222, ___U3CbytesU3E__1_3)); }
	inline ByteU5BU5D_t4260760469* get_U3CbytesU3E__1_3() const { return ___U3CbytesU3E__1_3; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CbytesU3E__1_3() { return &___U3CbytesU3E__1_3; }
	inline void set_U3CbytesU3E__1_3(ByteU5BU5D_t4260760469* value)
	{
		___U3CbytesU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbytesU3E__1_3, value);
	}

	inline static int32_t get_offset_of_U3CfileExtU3E__2_4() { return static_cast<int32_t>(offsetof(U3CGrabScreenshotU3Ec__Iterator30_t651971222, ___U3CfileExtU3E__2_4)); }
	inline String_t* get_U3CfileExtU3E__2_4() const { return ___U3CfileExtU3E__2_4; }
	inline String_t** get_address_of_U3CfileExtU3E__2_4() { return &___U3CfileExtU3E__2_4; }
	inline void set_U3CfileExtU3E__2_4(String_t* value)
	{
		___U3CfileExtU3E__2_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfileExtU3E__2_4, value);
	}

	inline static int32_t get_offset_of_U3CdateU3E__3_5() { return static_cast<int32_t>(offsetof(U3CGrabScreenshotU3Ec__Iterator30_t651971222, ___U3CdateU3E__3_5)); }
	inline String_t* get_U3CdateU3E__3_5() const { return ___U3CdateU3E__3_5; }
	inline String_t** get_address_of_U3CdateU3E__3_5() { return &___U3CdateU3E__3_5; }
	inline void set_U3CdateU3E__3_5(String_t* value)
	{
		___U3CdateU3E__3_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdateU3E__3_5, value);
	}

	inline static int32_t get_offset_of_fileName_6() { return static_cast<int32_t>(offsetof(U3CGrabScreenshotU3Ec__Iterator30_t651971222, ___fileName_6)); }
	inline String_t* get_fileName_6() const { return ___fileName_6; }
	inline String_t** get_address_of_fileName_6() { return &___fileName_6; }
	inline void set_fileName_6(String_t* value)
	{
		___fileName_6 = value;
		Il2CppCodeGenWriteBarrier(&___fileName_6, value);
	}

	inline static int32_t get_offset_of_U3CscreenshotFilenameU3E__4_7() { return static_cast<int32_t>(offsetof(U3CGrabScreenshotU3Ec__Iterator30_t651971222, ___U3CscreenshotFilenameU3E__4_7)); }
	inline String_t* get_U3CscreenshotFilenameU3E__4_7() const { return ___U3CscreenshotFilenameU3E__4_7; }
	inline String_t** get_address_of_U3CscreenshotFilenameU3E__4_7() { return &___U3CscreenshotFilenameU3E__4_7; }
	inline void set_U3CscreenshotFilenameU3E__4_7(String_t* value)
	{
		___U3CscreenshotFilenameU3E__4_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CscreenshotFilenameU3E__4_7, value);
	}

	inline static int32_t get_offset_of_U3CpathU3E__5_8() { return static_cast<int32_t>(offsetof(U3CGrabScreenshotU3Ec__Iterator30_t651971222, ___U3CpathU3E__5_8)); }
	inline String_t* get_U3CpathU3E__5_8() const { return ___U3CpathU3E__5_8; }
	inline String_t** get_address_of_U3CpathU3E__5_8() { return &___U3CpathU3E__5_8; }
	inline void set_U3CpathU3E__5_8(String_t* value)
	{
		___U3CpathU3E__5_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpathU3E__5_8, value);
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CGrabScreenshotU3Ec__Iterator30_t651971222, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CGrabScreenshotU3Ec__Iterator30_t651971222, ___U24current_10)); }
	inline Il2CppObject * get_U24current_10() const { return ___U24current_10; }
	inline Il2CppObject ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(Il2CppObject * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_10, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EscreenArea_11() { return static_cast<int32_t>(offsetof(U3CGrabScreenshotU3Ec__Iterator30_t651971222, ___U3CU24U3EscreenArea_11)); }
	inline Rect_t4241904616  get_U3CU24U3EscreenArea_11() const { return ___U3CU24U3EscreenArea_11; }
	inline Rect_t4241904616 * get_address_of_U3CU24U3EscreenArea_11() { return &___U3CU24U3EscreenArea_11; }
	inline void set_U3CU24U3EscreenArea_11(Rect_t4241904616  value)
	{
		___U3CU24U3EscreenArea_11 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3EfileType_12() { return static_cast<int32_t>(offsetof(U3CGrabScreenshotU3Ec__Iterator30_t651971222, ___U3CU24U3EfileType_12)); }
	inline String_t* get_U3CU24U3EfileType_12() const { return ___U3CU24U3EfileType_12; }
	inline String_t** get_address_of_U3CU24U3EfileType_12() { return &___U3CU24U3EfileType_12; }
	inline void set_U3CU24U3EfileType_12(String_t* value)
	{
		___U3CU24U3EfileType_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EfileType_12, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EfileName_13() { return static_cast<int32_t>(offsetof(U3CGrabScreenshotU3Ec__Iterator30_t651971222, ___U3CU24U3EfileName_13)); }
	inline String_t* get_U3CU24U3EfileName_13() const { return ___U3CU24U3EfileName_13; }
	inline String_t** get_address_of_U3CU24U3EfileName_13() { return &___U3CU24U3EfileName_13; }
	inline void set_U3CU24U3EfileName_13(String_t* value)
	{
		___U3CU24U3EfileName_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EfileName_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
