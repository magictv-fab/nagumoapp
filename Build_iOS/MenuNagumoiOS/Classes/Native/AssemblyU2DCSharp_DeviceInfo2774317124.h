﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// DeviceInfo
struct DeviceInfo_t2774317124;

#include "AssemblyU2DCSharp_MagicTV_abstracts_device_DeviceIn471529032.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeviceInfo
struct  DeviceInfo_t2774317124  : public DeviceInfoAbstract_t471529032
{
public:
	// System.String DeviceInfo::lastPing
	String_t* ___lastPing_7;

public:
	inline static int32_t get_offset_of_lastPing_7() { return static_cast<int32_t>(offsetof(DeviceInfo_t2774317124, ___lastPing_7)); }
	inline String_t* get_lastPing_7() const { return ___lastPing_7; }
	inline String_t** get_address_of_lastPing_7() { return &___lastPing_7; }
	inline void set_lastPing_7(String_t* value)
	{
		___lastPing_7 = value;
		Il2CppCodeGenWriteBarrier(&___lastPing_7, value);
	}
};

struct DeviceInfo_t2774317124_StaticFields
{
public:
	// DeviceInfo DeviceInfo::Instance
	DeviceInfo_t2774317124 * ___Instance_6;
	// System.Int32 DeviceInfo::_lastInfoVersionControlIdToSave
	int32_t ____lastInfoVersionControlIdToSave_8;

public:
	inline static int32_t get_offset_of_Instance_6() { return static_cast<int32_t>(offsetof(DeviceInfo_t2774317124_StaticFields, ___Instance_6)); }
	inline DeviceInfo_t2774317124 * get_Instance_6() const { return ___Instance_6; }
	inline DeviceInfo_t2774317124 ** get_address_of_Instance_6() { return &___Instance_6; }
	inline void set_Instance_6(DeviceInfo_t2774317124 * value)
	{
		___Instance_6 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_6, value);
	}

	inline static int32_t get_offset_of__lastInfoVersionControlIdToSave_8() { return static_cast<int32_t>(offsetof(DeviceInfo_t2774317124_StaticFields, ____lastInfoVersionControlIdToSave_8)); }
	inline int32_t get__lastInfoVersionControlIdToSave_8() const { return ____lastInfoVersionControlIdToSave_8; }
	inline int32_t* get_address_of__lastInfoVersionControlIdToSave_8() { return &____lastInfoVersionControlIdToSave_8; }
	inline void set__lastInfoVersionControlIdToSave_8(int32_t value)
	{
		____lastInfoVersionControlIdToSave_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
