﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmColor
struct FsmColor_t2131419205;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetColorRGBA
struct  SetColorRGBA_t2803139325  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.SetColorRGBA::colorVariable
	FsmColor_t2131419205 * ___colorVariable_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetColorRGBA::red
	FsmFloat_t2134102846 * ___red_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetColorRGBA::green
	FsmFloat_t2134102846 * ___green_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetColorRGBA::blue
	FsmFloat_t2134102846 * ___blue_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetColorRGBA::alpha
	FsmFloat_t2134102846 * ___alpha_13;
	// System.Boolean HutongGames.PlayMaker.Actions.SetColorRGBA::everyFrame
	bool ___everyFrame_14;

public:
	inline static int32_t get_offset_of_colorVariable_9() { return static_cast<int32_t>(offsetof(SetColorRGBA_t2803139325, ___colorVariable_9)); }
	inline FsmColor_t2131419205 * get_colorVariable_9() const { return ___colorVariable_9; }
	inline FsmColor_t2131419205 ** get_address_of_colorVariable_9() { return &___colorVariable_9; }
	inline void set_colorVariable_9(FsmColor_t2131419205 * value)
	{
		___colorVariable_9 = value;
		Il2CppCodeGenWriteBarrier(&___colorVariable_9, value);
	}

	inline static int32_t get_offset_of_red_10() { return static_cast<int32_t>(offsetof(SetColorRGBA_t2803139325, ___red_10)); }
	inline FsmFloat_t2134102846 * get_red_10() const { return ___red_10; }
	inline FsmFloat_t2134102846 ** get_address_of_red_10() { return &___red_10; }
	inline void set_red_10(FsmFloat_t2134102846 * value)
	{
		___red_10 = value;
		Il2CppCodeGenWriteBarrier(&___red_10, value);
	}

	inline static int32_t get_offset_of_green_11() { return static_cast<int32_t>(offsetof(SetColorRGBA_t2803139325, ___green_11)); }
	inline FsmFloat_t2134102846 * get_green_11() const { return ___green_11; }
	inline FsmFloat_t2134102846 ** get_address_of_green_11() { return &___green_11; }
	inline void set_green_11(FsmFloat_t2134102846 * value)
	{
		___green_11 = value;
		Il2CppCodeGenWriteBarrier(&___green_11, value);
	}

	inline static int32_t get_offset_of_blue_12() { return static_cast<int32_t>(offsetof(SetColorRGBA_t2803139325, ___blue_12)); }
	inline FsmFloat_t2134102846 * get_blue_12() const { return ___blue_12; }
	inline FsmFloat_t2134102846 ** get_address_of_blue_12() { return &___blue_12; }
	inline void set_blue_12(FsmFloat_t2134102846 * value)
	{
		___blue_12 = value;
		Il2CppCodeGenWriteBarrier(&___blue_12, value);
	}

	inline static int32_t get_offset_of_alpha_13() { return static_cast<int32_t>(offsetof(SetColorRGBA_t2803139325, ___alpha_13)); }
	inline FsmFloat_t2134102846 * get_alpha_13() const { return ___alpha_13; }
	inline FsmFloat_t2134102846 ** get_address_of_alpha_13() { return &___alpha_13; }
	inline void set_alpha_13(FsmFloat_t2134102846 * value)
	{
		___alpha_13 = value;
		Il2CppCodeGenWriteBarrier(&___alpha_13, value);
	}

	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(SetColorRGBA_t2803139325, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
