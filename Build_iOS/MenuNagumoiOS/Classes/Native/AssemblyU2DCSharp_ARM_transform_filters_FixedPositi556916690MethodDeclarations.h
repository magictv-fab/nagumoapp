﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.transform.filters.FixedPositionAngleFilter
struct FixedPositionAngleFilter_t556916690;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "mscorlib_System_String7231557.h"

// System.Void ARM.transform.filters.FixedPositionAngleFilter::.ctor()
extern "C"  void FixedPositionAngleFilter__ctor_m2105848614 (FixedPositionAngleFilter_t556916690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.FixedPositionAngleFilter::Start()
extern "C"  void FixedPositionAngleFilter_Start_m1052986406 (FixedPositionAngleFilter_t556916690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.transform.filters.FixedPositionAngleFilter::_doFilter(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  FixedPositionAngleFilter__doFilter_m2855524224 (FixedPositionAngleFilter_t556916690 * __this, Vector3_t4282066566  ___last0, Vector3_t4282066566  ___nextValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion ARM.transform.filters.FixedPositionAngleFilter::_doFilter(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  FixedPositionAngleFilter__doFilter_m2528336072 (FixedPositionAngleFilter_t556916690 * __this, Quaternion_t1553702882  ___last0, Quaternion_t1553702882  ___nextValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion ARM.transform.filters.FixedPositionAngleFilter::filterCurrent(UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  FixedPositionAngleFilter_filterCurrent_m1000256520 (FixedPositionAngleFilter_t556916690 * __this, Quaternion_t1553702882  ___cur0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.transform.filters.FixedPositionAngleFilter::filterCurrent(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  FixedPositionAngleFilter_filterCurrent_m452521646 (FixedPositionAngleFilter_t556916690 * __this, Vector3_t4282066566  ___cur0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.FixedPositionAngleFilter::configByString(System.String)
extern "C"  void FixedPositionAngleFilter_configByString_m3457243354 (FixedPositionAngleFilter_t556916690 * __this, String_t* ___metadata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.FixedPositionAngleFilter::Reset()
extern "C"  void FixedPositionAngleFilter_Reset_m4047248851 (FixedPositionAngleFilter_t556916690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
