﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerUtils
struct PlayMakerUtils_t945993025;
// PlayMakerFSM
struct PlayMakerFSM_t3799847376;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;
// HutongGames.PlayMaker.FsmEventData
struct FsmEventData_t1076900934;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_PlayMakerFSM3799847376.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "mscorlib_System_String7231557.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventData1076900934.h"

// System.Void PlayMakerUtils::.ctor()
extern "C"  void PlayMakerUtils__ctor_m1871999098 (PlayMakerUtils_t945993025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUtils::SendEventToGameObject(PlayMakerFSM,UnityEngine.GameObject,System.String)
extern "C"  void PlayMakerUtils_SendEventToGameObject_m460189300 (Il2CppObject * __this /* static, unused */, PlayMakerFSM_t3799847376 * ___fromFsm0, GameObject_t3674682005 * ___target1, String_t* ___fsmEvent2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUtils::SendEventToGameObject(PlayMakerFSM,UnityEngine.GameObject,System.String,HutongGames.PlayMaker.FsmEventData)
extern "C"  void PlayMakerUtils_SendEventToGameObject_m544708034 (Il2CppObject * __this /* static, unused */, PlayMakerFSM_t3799847376 * ___fromFsm0, GameObject_t3674682005 * ___target1, String_t* ___fsmEvent2, FsmEventData_t1076900934 * ___eventData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
