﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t3771233898;
// System.Action`1<System.Boolean>
struct Action_1_t872614854;
// RenderListenerUtility
struct RenderListenerUtility_t2682722690;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderListenerUtility
struct  RenderListenerUtility_t2682722690  : public MonoBehaviour_t667441552
{
public:

public:
};

struct RenderListenerUtility_t2682722690_StaticFields
{
public:
	// System.Action RenderListenerUtility::onQuit
	Action_t3771233898 * ___onQuit_2;
	// System.Action`1<System.Boolean> RenderListenerUtility::onPause
	Action_1_t872614854 * ___onPause_3;
	// RenderListenerUtility RenderListenerUtility::instance
	RenderListenerUtility_t2682722690 * ___instance_4;

public:
	inline static int32_t get_offset_of_onQuit_2() { return static_cast<int32_t>(offsetof(RenderListenerUtility_t2682722690_StaticFields, ___onQuit_2)); }
	inline Action_t3771233898 * get_onQuit_2() const { return ___onQuit_2; }
	inline Action_t3771233898 ** get_address_of_onQuit_2() { return &___onQuit_2; }
	inline void set_onQuit_2(Action_t3771233898 * value)
	{
		___onQuit_2 = value;
		Il2CppCodeGenWriteBarrier(&___onQuit_2, value);
	}

	inline static int32_t get_offset_of_onPause_3() { return static_cast<int32_t>(offsetof(RenderListenerUtility_t2682722690_StaticFields, ___onPause_3)); }
	inline Action_1_t872614854 * get_onPause_3() const { return ___onPause_3; }
	inline Action_1_t872614854 ** get_address_of_onPause_3() { return &___onPause_3; }
	inline void set_onPause_3(Action_1_t872614854 * value)
	{
		___onPause_3 = value;
		Il2CppCodeGenWriteBarrier(&___onPause_3, value);
	}

	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(RenderListenerUtility_t2682722690_StaticFields, ___instance_4)); }
	inline RenderListenerUtility_t2682722690 * get_instance_4() const { return ___instance_4; }
	inline RenderListenerUtility_t2682722690 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(RenderListenerUtility_t2682722690 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier(&___instance_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
