﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OneSignal/OnPostNotificationFailure
struct OnPostNotificationFailure_t2834525727;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t696267445;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void OneSignal/OnPostNotificationFailure::.ctor(System.Object,System.IntPtr)
extern "C"  void OnPostNotificationFailure__ctor_m1270421622 (OnPostNotificationFailure_t2834525727 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal/OnPostNotificationFailure::Invoke(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void OnPostNotificationFailure_Invoke_m3572315951 (OnPostNotificationFailure_t2834525727 * __this, Dictionary_2_t696267445 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult OneSignal/OnPostNotificationFailure::BeginInvoke(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnPostNotificationFailure_BeginInvoke_m3036519748 (OnPostNotificationFailure_t2834525727 * __this, Dictionary_2_t696267445 * ___response0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal/OnPostNotificationFailure::EndInvoke(System.IAsyncResult)
extern "C"  void OnPostNotificationFailure_EndInvoke_m321336710 (OnPostNotificationFailure_t2834525727 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
