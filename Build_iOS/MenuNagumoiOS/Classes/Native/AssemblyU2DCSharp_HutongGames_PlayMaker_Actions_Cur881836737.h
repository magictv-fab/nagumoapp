﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmRect
struct FsmRect_t1076426478;
// HutongGames.PlayMaker.FsmAnimationCurve
struct FsmAnimationCurve_t2685995989;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Cu2975001167.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Cu2771812670.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.CurveRect
struct  CurveRect_t881836737  : public CurveFsmAction_t2975001167
{
public:
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.CurveRect::rectVariable
	FsmRect_t1076426478 * ___rectVariable_33;
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.CurveRect::fromValue
	FsmRect_t1076426478 * ___fromValue_34;
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.CurveRect::toValue
	FsmRect_t1076426478 * ___toValue_35;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.CurveRect::curveX
	FsmAnimationCurve_t2685995989 * ___curveX_36;
	// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation HutongGames.PlayMaker.Actions.CurveRect::calculationX
	int32_t ___calculationX_37;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.CurveRect::curveY
	FsmAnimationCurve_t2685995989 * ___curveY_38;
	// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation HutongGames.PlayMaker.Actions.CurveRect::calculationY
	int32_t ___calculationY_39;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.CurveRect::curveW
	FsmAnimationCurve_t2685995989 * ___curveW_40;
	// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation HutongGames.PlayMaker.Actions.CurveRect::calculationW
	int32_t ___calculationW_41;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.CurveRect::curveH
	FsmAnimationCurve_t2685995989 * ___curveH_42;
	// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation HutongGames.PlayMaker.Actions.CurveRect::calculationH
	int32_t ___calculationH_43;
	// UnityEngine.Rect HutongGames.PlayMaker.Actions.CurveRect::rct
	Rect_t4241904616  ___rct_44;
	// System.Boolean HutongGames.PlayMaker.Actions.CurveRect::finishInNextStep
	bool ___finishInNextStep_45;

public:
	inline static int32_t get_offset_of_rectVariable_33() { return static_cast<int32_t>(offsetof(CurveRect_t881836737, ___rectVariable_33)); }
	inline FsmRect_t1076426478 * get_rectVariable_33() const { return ___rectVariable_33; }
	inline FsmRect_t1076426478 ** get_address_of_rectVariable_33() { return &___rectVariable_33; }
	inline void set_rectVariable_33(FsmRect_t1076426478 * value)
	{
		___rectVariable_33 = value;
		Il2CppCodeGenWriteBarrier(&___rectVariable_33, value);
	}

	inline static int32_t get_offset_of_fromValue_34() { return static_cast<int32_t>(offsetof(CurveRect_t881836737, ___fromValue_34)); }
	inline FsmRect_t1076426478 * get_fromValue_34() const { return ___fromValue_34; }
	inline FsmRect_t1076426478 ** get_address_of_fromValue_34() { return &___fromValue_34; }
	inline void set_fromValue_34(FsmRect_t1076426478 * value)
	{
		___fromValue_34 = value;
		Il2CppCodeGenWriteBarrier(&___fromValue_34, value);
	}

	inline static int32_t get_offset_of_toValue_35() { return static_cast<int32_t>(offsetof(CurveRect_t881836737, ___toValue_35)); }
	inline FsmRect_t1076426478 * get_toValue_35() const { return ___toValue_35; }
	inline FsmRect_t1076426478 ** get_address_of_toValue_35() { return &___toValue_35; }
	inline void set_toValue_35(FsmRect_t1076426478 * value)
	{
		___toValue_35 = value;
		Il2CppCodeGenWriteBarrier(&___toValue_35, value);
	}

	inline static int32_t get_offset_of_curveX_36() { return static_cast<int32_t>(offsetof(CurveRect_t881836737, ___curveX_36)); }
	inline FsmAnimationCurve_t2685995989 * get_curveX_36() const { return ___curveX_36; }
	inline FsmAnimationCurve_t2685995989 ** get_address_of_curveX_36() { return &___curveX_36; }
	inline void set_curveX_36(FsmAnimationCurve_t2685995989 * value)
	{
		___curveX_36 = value;
		Il2CppCodeGenWriteBarrier(&___curveX_36, value);
	}

	inline static int32_t get_offset_of_calculationX_37() { return static_cast<int32_t>(offsetof(CurveRect_t881836737, ___calculationX_37)); }
	inline int32_t get_calculationX_37() const { return ___calculationX_37; }
	inline int32_t* get_address_of_calculationX_37() { return &___calculationX_37; }
	inline void set_calculationX_37(int32_t value)
	{
		___calculationX_37 = value;
	}

	inline static int32_t get_offset_of_curveY_38() { return static_cast<int32_t>(offsetof(CurveRect_t881836737, ___curveY_38)); }
	inline FsmAnimationCurve_t2685995989 * get_curveY_38() const { return ___curveY_38; }
	inline FsmAnimationCurve_t2685995989 ** get_address_of_curveY_38() { return &___curveY_38; }
	inline void set_curveY_38(FsmAnimationCurve_t2685995989 * value)
	{
		___curveY_38 = value;
		Il2CppCodeGenWriteBarrier(&___curveY_38, value);
	}

	inline static int32_t get_offset_of_calculationY_39() { return static_cast<int32_t>(offsetof(CurveRect_t881836737, ___calculationY_39)); }
	inline int32_t get_calculationY_39() const { return ___calculationY_39; }
	inline int32_t* get_address_of_calculationY_39() { return &___calculationY_39; }
	inline void set_calculationY_39(int32_t value)
	{
		___calculationY_39 = value;
	}

	inline static int32_t get_offset_of_curveW_40() { return static_cast<int32_t>(offsetof(CurveRect_t881836737, ___curveW_40)); }
	inline FsmAnimationCurve_t2685995989 * get_curveW_40() const { return ___curveW_40; }
	inline FsmAnimationCurve_t2685995989 ** get_address_of_curveW_40() { return &___curveW_40; }
	inline void set_curveW_40(FsmAnimationCurve_t2685995989 * value)
	{
		___curveW_40 = value;
		Il2CppCodeGenWriteBarrier(&___curveW_40, value);
	}

	inline static int32_t get_offset_of_calculationW_41() { return static_cast<int32_t>(offsetof(CurveRect_t881836737, ___calculationW_41)); }
	inline int32_t get_calculationW_41() const { return ___calculationW_41; }
	inline int32_t* get_address_of_calculationW_41() { return &___calculationW_41; }
	inline void set_calculationW_41(int32_t value)
	{
		___calculationW_41 = value;
	}

	inline static int32_t get_offset_of_curveH_42() { return static_cast<int32_t>(offsetof(CurveRect_t881836737, ___curveH_42)); }
	inline FsmAnimationCurve_t2685995989 * get_curveH_42() const { return ___curveH_42; }
	inline FsmAnimationCurve_t2685995989 ** get_address_of_curveH_42() { return &___curveH_42; }
	inline void set_curveH_42(FsmAnimationCurve_t2685995989 * value)
	{
		___curveH_42 = value;
		Il2CppCodeGenWriteBarrier(&___curveH_42, value);
	}

	inline static int32_t get_offset_of_calculationH_43() { return static_cast<int32_t>(offsetof(CurveRect_t881836737, ___calculationH_43)); }
	inline int32_t get_calculationH_43() const { return ___calculationH_43; }
	inline int32_t* get_address_of_calculationH_43() { return &___calculationH_43; }
	inline void set_calculationH_43(int32_t value)
	{
		___calculationH_43 = value;
	}

	inline static int32_t get_offset_of_rct_44() { return static_cast<int32_t>(offsetof(CurveRect_t881836737, ___rct_44)); }
	inline Rect_t4241904616  get_rct_44() const { return ___rct_44; }
	inline Rect_t4241904616 * get_address_of_rct_44() { return &___rct_44; }
	inline void set_rct_44(Rect_t4241904616  value)
	{
		___rct_44 = value;
	}

	inline static int32_t get_offset_of_finishInNextStep_45() { return static_cast<int32_t>(offsetof(CurveRect_t881836737, ___finishInNextStep_45)); }
	inline bool get_finishInNextStep_45() const { return ___finishInNextStep_45; }
	inline bool* get_address_of_finishInNextStep_45() { return &___finishInNextStep_45; }
	inline void set_finishInNextStep_45(bool value)
	{
		___finishInNextStep_45 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
