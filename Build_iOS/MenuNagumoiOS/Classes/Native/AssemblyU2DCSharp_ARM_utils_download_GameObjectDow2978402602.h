﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWW
struct WWW_t3134621005;
// System.String
struct String_t;

#include "AssemblyU2DCSharp_ARM_utils_download_DownloaderAbs3533812091.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.utils.download.GameObjectDownloader
struct  GameObjectDownloader_t2978402602  : public DownloaderAbstract_t3533812091
{
public:
	// System.Int32 ARM.utils.download.GameObjectDownloader::_totalAttempts
	int32_t ____totalAttempts_15;
	// System.Int32 ARM.utils.download.GameObjectDownloader::_attemptCount
	int32_t ____attemptCount_16;
	// UnityEngine.WWW ARM.utils.download.GameObjectDownloader::_loader
	WWW_t3134621005 * ____loader_17;
	// System.String ARM.utils.download.GameObjectDownloader::requestUrl
	String_t* ___requestUrl_18;
	// System.Boolean ARM.utils.download.GameObjectDownloader::_isDisposed
	bool ____isDisposed_19;

public:
	inline static int32_t get_offset_of__totalAttempts_15() { return static_cast<int32_t>(offsetof(GameObjectDownloader_t2978402602, ____totalAttempts_15)); }
	inline int32_t get__totalAttempts_15() const { return ____totalAttempts_15; }
	inline int32_t* get_address_of__totalAttempts_15() { return &____totalAttempts_15; }
	inline void set__totalAttempts_15(int32_t value)
	{
		____totalAttempts_15 = value;
	}

	inline static int32_t get_offset_of__attemptCount_16() { return static_cast<int32_t>(offsetof(GameObjectDownloader_t2978402602, ____attemptCount_16)); }
	inline int32_t get__attemptCount_16() const { return ____attemptCount_16; }
	inline int32_t* get_address_of__attemptCount_16() { return &____attemptCount_16; }
	inline void set__attemptCount_16(int32_t value)
	{
		____attemptCount_16 = value;
	}

	inline static int32_t get_offset_of__loader_17() { return static_cast<int32_t>(offsetof(GameObjectDownloader_t2978402602, ____loader_17)); }
	inline WWW_t3134621005 * get__loader_17() const { return ____loader_17; }
	inline WWW_t3134621005 ** get_address_of__loader_17() { return &____loader_17; }
	inline void set__loader_17(WWW_t3134621005 * value)
	{
		____loader_17 = value;
		Il2CppCodeGenWriteBarrier(&____loader_17, value);
	}

	inline static int32_t get_offset_of_requestUrl_18() { return static_cast<int32_t>(offsetof(GameObjectDownloader_t2978402602, ___requestUrl_18)); }
	inline String_t* get_requestUrl_18() const { return ___requestUrl_18; }
	inline String_t** get_address_of_requestUrl_18() { return &___requestUrl_18; }
	inline void set_requestUrl_18(String_t* value)
	{
		___requestUrl_18 = value;
		Il2CppCodeGenWriteBarrier(&___requestUrl_18, value);
	}

	inline static int32_t get_offset_of__isDisposed_19() { return static_cast<int32_t>(offsetof(GameObjectDownloader_t2978402602, ____isDisposed_19)); }
	inline bool get__isDisposed_19() const { return ____isDisposed_19; }
	inline bool* get_address_of__isDisposed_19() { return &____isDisposed_19; }
	inline void set__isDisposed_19(bool value)
	{
		____isDisposed_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
