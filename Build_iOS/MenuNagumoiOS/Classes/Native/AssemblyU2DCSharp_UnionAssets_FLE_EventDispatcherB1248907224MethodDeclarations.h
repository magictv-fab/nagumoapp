﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnionAssets.FLE.EventDispatcherBase
struct EventDispatcherBase_t1248907224;
// System.String
struct String_t;
// UnionAssets.FLE.EventHandlerFunction
struct EventHandlerFunction_t2672185540;
// UnionAssets.FLE.DataEventHandlerFunction
struct DataEventHandlerFunction_t438967822;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.List`1<UnionAssets.FLE.EventHandlerFunction>
struct List_1_t4040371092;
// System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>
struct List_1_t1807153374;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_UnionAssets_FLE_EventHandlerFunc2672185540.h"
#include "AssemblyU2DCSharp_UnionAssets_FLE_DataEventHandlerF438967822.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnionAssets.FLE.EventDispatcherBase::.ctor()
extern "C"  void EventDispatcherBase__ctor_m3581822756 (EventDispatcherBase_t1248907224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcherBase::addEventListener(System.String,UnionAssets.FLE.EventHandlerFunction)
extern "C"  void EventDispatcherBase_addEventListener_m3037004382 (EventDispatcherBase_t1248907224 * __this, String_t* ___eventName0, EventHandlerFunction_t2672185540 * ___handler1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcherBase::addEventListener(System.Int32,UnionAssets.FLE.EventHandlerFunction)
extern "C"  void EventDispatcherBase_addEventListener_m2751504949 (EventDispatcherBase_t1248907224 * __this, int32_t ___eventID0, EventHandlerFunction_t2672185540 * ___handler1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcherBase::addEventListener(System.Int32,UnionAssets.FLE.EventHandlerFunction,System.String)
extern "C"  void EventDispatcherBase_addEventListener_m673401265 (EventDispatcherBase_t1248907224 * __this, int32_t ___eventID0, EventHandlerFunction_t2672185540 * ___handler1, String_t* ___eventGraphName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcherBase::addEventListener(System.String,UnionAssets.FLE.DataEventHandlerFunction)
extern "C"  void EventDispatcherBase_addEventListener_m3251907156 (EventDispatcherBase_t1248907224 * __this, String_t* ___eventName0, DataEventHandlerFunction_t438967822 * ___handler1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcherBase::addEventListener(System.Int32,UnionAssets.FLE.DataEventHandlerFunction)
extern "C"  void EventDispatcherBase_addEventListener_m2277377707 (EventDispatcherBase_t1248907224 * __this, int32_t ___eventID0, DataEventHandlerFunction_t438967822 * ___handler1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcherBase::addEventListener(System.Int32,UnionAssets.FLE.DataEventHandlerFunction,System.String)
extern "C"  void EventDispatcherBase_addEventListener_m1573169063 (EventDispatcherBase_t1248907224 * __this, int32_t ___eventID0, DataEventHandlerFunction_t438967822 * ___handler1, String_t* ___eventGraphName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcherBase::removeEventListener(System.String,UnionAssets.FLE.EventHandlerFunction)
extern "C"  void EventDispatcherBase_removeEventListener_m511508989 (EventDispatcherBase_t1248907224 * __this, String_t* ___eventName0, EventHandlerFunction_t2672185540 * ___handler1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcherBase::removeEventListener(System.Int32,UnionAssets.FLE.EventHandlerFunction)
extern "C"  void EventDispatcherBase_removeEventListener_m868922038 (EventDispatcherBase_t1248907224 * __this, int32_t ___eventID0, EventHandlerFunction_t2672185540 * ___handler1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcherBase::removeEventListener(System.Int32,UnionAssets.FLE.EventHandlerFunction,System.String)
extern "C"  void EventDispatcherBase_removeEventListener_m1285678194 (EventDispatcherBase_t1248907224 * __this, int32_t ___eventID0, EventHandlerFunction_t2672185540 * ___handler1, String_t* ___eventGraphName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcherBase::removeEventListener(System.String,UnionAssets.FLE.DataEventHandlerFunction)
extern "C"  void EventDispatcherBase_removeEventListener_m2851422835 (EventDispatcherBase_t1248907224 * __this, String_t* ___eventName0, DataEventHandlerFunction_t438967822 * ___handler1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcherBase::removeEventListener(System.Int32,UnionAssets.FLE.DataEventHandlerFunction)
extern "C"  void EventDispatcherBase_removeEventListener_m186248876 (EventDispatcherBase_t1248907224 * __this, int32_t ___eventID0, DataEventHandlerFunction_t438967822 * ___handler1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcherBase::removeEventListener(System.Int32,UnionAssets.FLE.DataEventHandlerFunction,System.String)
extern "C"  void EventDispatcherBase_removeEventListener_m2550528488 (EventDispatcherBase_t1248907224 * __this, int32_t ___eventID0, DataEventHandlerFunction_t438967822 * ___handler1, String_t* ___eventGraphName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcherBase::dispatchEvent(System.String)
extern "C"  void EventDispatcherBase_dispatchEvent_m1697761440 (EventDispatcherBase_t1248907224 * __this, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcherBase::dispatchEvent(System.String,System.Object)
extern "C"  void EventDispatcherBase_dispatchEvent_m2907470702 (EventDispatcherBase_t1248907224 * __this, String_t* ___eventName0, Il2CppObject * ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcherBase::dispatchEvent(System.Int32)
extern "C"  void EventDispatcherBase_dispatchEvent_m40034291 (EventDispatcherBase_t1248907224 * __this, int32_t ___eventID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcherBase::dispatchEvent(System.Int32,System.Object)
extern "C"  void EventDispatcherBase_dispatchEvent_m3110913409 (EventDispatcherBase_t1248907224 * __this, int32_t ___eventID0, Il2CppObject * ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcherBase::dispatch(System.String)
extern "C"  void EventDispatcherBase_dispatch_m3766593864 (EventDispatcherBase_t1248907224 * __this, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcherBase::dispatch(System.String,System.Object)
extern "C"  void EventDispatcherBase_dispatch_m2807174678 (EventDispatcherBase_t1248907224 * __this, String_t* ___eventName0, Il2CppObject * ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcherBase::dispatch(System.Int32)
extern "C"  void EventDispatcherBase_dispatch_m2462075467 (EventDispatcherBase_t1248907224 * __this, int32_t ___eventID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcherBase::dispatch(System.Int32,System.Object)
extern "C"  void EventDispatcherBase_dispatch_m2692036057 (EventDispatcherBase_t1248907224 * __this, int32_t ___eventID0, Il2CppObject * ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcherBase::dispatch(System.Int32,System.Object,System.String)
extern "C"  void EventDispatcherBase_dispatch_m3626675797 (EventDispatcherBase_t1248907224 * __this, int32_t ___eventID0, Il2CppObject * ___data1, String_t* ___eventName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcherBase::clearEvents()
extern "C"  void EventDispatcherBase_clearEvents_m2065810408 (EventDispatcherBase_t1248907224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnionAssets.FLE.EventHandlerFunction> UnionAssets.FLE.EventDispatcherBase::cloenArray(System.Collections.Generic.List`1<UnionAssets.FLE.EventHandlerFunction>)
extern "C"  List_1_t4040371092 * EventDispatcherBase_cloenArray_m3141111905 (EventDispatcherBase_t1248907224 * __this, List_1_t4040371092 * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction> UnionAssets.FLE.EventDispatcherBase::cloenArray(System.Collections.Generic.List`1<UnionAssets.FLE.DataEventHandlerFunction>)
extern "C"  List_1_t1807153374 * EventDispatcherBase_cloenArray_m3450506485 (EventDispatcherBase_t1248907224 * __this, List_1_t1807153374 * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.EventDispatcherBase::OnDestroy()
extern "C"  void EventDispatcherBase_OnDestroy_m1624238621 (EventDispatcherBase_t1248907224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
