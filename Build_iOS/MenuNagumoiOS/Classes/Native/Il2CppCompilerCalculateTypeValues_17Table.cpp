﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_ChunkStream1623008007.h"
#include "System_System_Net_ChunkStream_State3599614847.h"
#include "System_System_Net_ChunkStream_Chunk3584500059.h"
#include "System_System_Net_Configuration_AuthenticationModu2852222000.h"
#include "System_System_Net_Configuration_AuthenticationModu2100993394.h"
#include "System_System_Net_Configuration_AuthenticationModu2854823584.h"
#include "System_System_Net_Configuration_BypassElementColle3745155276.h"
#include "System_System_Net_Configuration_BypassElement231441678.h"
#include "System_System_Net_Configuration_ConnectionManageme2991917843.h"
#include "System_System_Net_Configuration_ConnectionManageme2236605461.h"
#include "System_System_Net_Configuration_ConnectionManageme1139934645.h"
#include "System_System_Net_Configuration_HandlersUtil724146869.h"
#include "System_System_Net_Configuration_ConnectionManageme1574716414.h"
#include "System_System_Net_Configuration_DefaultProxySectio3757845394.h"
#include "System_System_Net_Configuration_FtpCachePolicyElem3660269092.h"
#include "System_System_Net_Configuration_HttpCachePolicyEle3335049818.h"
#include "System_System_Net_Configuration_HttpWebRequestElem3178920531.h"
#include "System_System_Net_Configuration_Ipv6Element2694178095.h"
#include "System_System_Net_Configuration_MailSettingsSectio1523994462.h"
#include "System_System_Net_Configuration_ModuleElement1599164170.h"
#include "System_System_Net_Configuration_NetConfigurationHa2944741579.h"
#include "System_System_Net_Configuration_NetSectionGroup1688890097.h"
#include "System_System_Net_Configuration_PerformanceCounters995147103.h"
#include "System_System_Net_Configuration_ProxyElement1949889880.h"
#include "System_System_Net_Configuration_ProxyElement_Bypas1443854029.h"
#include "System_System_Net_Configuration_ProxyElement_UseSy3736939686.h"
#include "System_System_Net_Configuration_ProxyElement_AutoD2572891277.h"
#include "System_System_Net_Configuration_RequestCachingSect3021090479.h"
#include "System_System_Net_Configuration_ServicePointManager783881716.h"
#include "System_System_Net_Configuration_SettingsSection2800213852.h"
#include "System_System_Net_Configuration_SocketElement3740463139.h"
#include "System_System_Net_Configuration_WebProxyScriptElem2008872561.h"
#include "System_System_Net_Configuration_WebRequestModuleEl2937316013.h"
#include "System_System_Net_Configuration_WebRequestModuleEl4025521199.h"
#include "System_System_Net_Configuration_WebRequestModuleHa2086194301.h"
#include "System_System_Net_Configuration_WebRequestModulesS2385643395.h"
#include "System_System_Net_CookieCollection2536410684.h"
#include "System_System_Net_CookieCollection_CookieCollectio2125669164.h"
#include "System_System_Net_CookieContainer230274359.h"
#include "System_System_Net_Cookie2033273982.h"
#include "System_System_Net_CookieException2122856709.h"
#include "System_System_Net_DecompressionMethods3697240007.h"
#include "System_System_Net_DefaultCertificatePolicy3264712578.h"
#include "System_System_Net_Dns3289571299.h"
#include "System_System_Net_EndPoint1026786191.h"
#include "System_System_Net_FileWebRequestCreator1433256879.h"
#include "System_System_Net_FileWebRequest1151586641.h"
#include "System_System_Net_FileWebRequest_FileWebStream902488976.h"
#include "System_System_Net_FileWebRequest_GetRequestStreamC3736561126.h"
#include "System_System_Net_FileWebRequest_GetResponseCallba1444420308.h"
#include "System_System_Net_FileWebResponse2971811667.h"
#include "System_System_Net_FtpAsyncResult1090897169.h"
#include "System_System_Net_FtpDataStream2383958406.h"
#include "System_System_Net_FtpDataStream_WriteDelegate1637408689.h"
#include "System_System_Net_FtpDataStream_ReadDelegate3891738222.h"
#include "System_System_Net_FtpRequestCreator3707472729.h"
#include "System_System_Net_FtpStatusCode1354479291.h"
#include "System_System_Net_FtpWebRequest3084461655.h"
#include "System_System_Net_FtpWebRequest_RequestState430963524.h"
#include "System_System_Net_FtpStatus1259919022.h"
#include "System_System_Net_FtpWebResponse2761394957.h"
#include "System_System_Net_GlobalProxySelection183150427.h"
#include "System_System_Net_HttpRequestCreator1200682847.h"
#include "System_System_Net_HttpStatusCode2219054529.h"
#include "System_System_Net_HttpVersion3218902506.h"
#include "System_System_Net_HttpWebRequest3949036893.h"
#include "System_System_Net_HttpWebResponse3793423559.h"
#include "System_System_Net_CookieParser1524389309.h"
#include "System_System_Net_IPAddress3525271463.h"
#include "System_System_Net_IPEndPoint2123960758.h"
#include "System_System_Net_IPHostEntry737820957.h"
#include "System_System_Net_IPv6Address1083476711.h"
#include "System_System_Net_NetConfig2400538105.h"
#include "System_System_Net_NetworkCredential1592396767.h"
#include "System_System_Net_NetworkInformation_IPInterfacePr3015049663.h"
#include "System_System_Net_NetworkInformation_Win32IPInterf3153501730.h"
#include "System_System_Net_NetworkInformation_IPv4InterfaceS107980849.h"
#include "System_System_Net_NetworkInformation_Win32IPv4Inte2649373942.h"
#include "System_System_Net_NetworkInformation_ifa_ifu3073026615.h"
#include "System_System_Net_NetworkInformation_ifaddrs3073171135.h"
#include "System_System_Net_NetworkInformation_sockaddr_in2133621513.h"
#include "System_System_Net_NetworkInformation_sockaddr_in6824315777.h"
#include "System_System_Net_NetworkInformation_in6_addr1454870809.h"
#include "System_System_Net_NetworkInformation_sockaddr_ll2133621604.h"
#include "System_System_Net_NetworkInformation_LinuxArpHardwa424852813.h"
#include "System_System_Net_NetworkInformation_MacOsStructs_i708620971.h"
#include "System_System_Net_NetworkInformation_MacOsStructs_3941944161.h"
#include "System_System_Net_NetworkInformation_MacOsStructs_s213596277.h"
#include "System_System_Net_NetworkInformation_MacOsStructs_1506812267.h"
#include "System_System_Net_NetworkInformation_MacOsStructs_1771628115.h"
#include "System_System_Net_NetworkInformation_MacOsStructs_s213596120.h"
#include "System_System_Net_NetworkInformation_MacOsArpHardw3918949710.h"
#include "System_System_Net_NetworkInformation_NetworkInform3136310987.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (ChunkStream_t1623008007), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1700[9] = 
{
	ChunkStream_t1623008007::get_offset_of_headers_0(),
	ChunkStream_t1623008007::get_offset_of_chunkSize_1(),
	ChunkStream_t1623008007::get_offset_of_chunkRead_2(),
	ChunkStream_t1623008007::get_offset_of_state_3(),
	ChunkStream_t1623008007::get_offset_of_saved_4(),
	ChunkStream_t1623008007::get_offset_of_sawCR_5(),
	ChunkStream_t1623008007::get_offset_of_gotit_6(),
	ChunkStream_t1623008007::get_offset_of_trailerState_7(),
	ChunkStream_t1623008007::get_offset_of_chunks_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (State_t3599614847)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1701[5] = 
{
	State_t3599614847::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (Chunk_t3584500059), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1702[2] = 
{
	Chunk_t3584500059::get_offset_of_Bytes_0(),
	Chunk_t3584500059::get_offset_of_Offset_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (AuthenticationModuleElementCollection_t2852222000), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (AuthenticationModuleElement_t2100993394), -1, sizeof(AuthenticationModuleElement_t2100993394_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1704[2] = 
{
	AuthenticationModuleElement_t2100993394_StaticFields::get_offset_of_properties_13(),
	AuthenticationModuleElement_t2100993394_StaticFields::get_offset_of_typeProp_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (AuthenticationModulesSection_t2854823584), -1, sizeof(AuthenticationModulesSection_t2854823584_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1705[2] = 
{
	AuthenticationModulesSection_t2854823584_StaticFields::get_offset_of_properties_17(),
	AuthenticationModulesSection_t2854823584_StaticFields::get_offset_of_authenticationModulesProp_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (BypassElementCollection_t3745155276), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (BypassElement_t231441678), -1, sizeof(BypassElement_t231441678_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1707[2] = 
{
	BypassElement_t231441678_StaticFields::get_offset_of_properties_13(),
	BypassElement_t231441678_StaticFields::get_offset_of_addressProp_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (ConnectionManagementElementCollection_t2991917843), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { sizeof (ConnectionManagementElement_t2236605461), -1, sizeof(ConnectionManagementElement_t2236605461_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1709[3] = 
{
	ConnectionManagementElement_t2236605461_StaticFields::get_offset_of_properties_13(),
	ConnectionManagementElement_t2236605461_StaticFields::get_offset_of_addressProp_14(),
	ConnectionManagementElement_t2236605461_StaticFields::get_offset_of_maxConnectionProp_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { sizeof (ConnectionManagementData_t1139934645), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1710[1] = 
{
	ConnectionManagementData_t1139934645::get_offset_of_data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { sizeof (HandlersUtil_t724146869), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { sizeof (ConnectionManagementSection_t1574716414), -1, sizeof(ConnectionManagementSection_t1574716414_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1712[2] = 
{
	ConnectionManagementSection_t1574716414_StaticFields::get_offset_of_connectionManagementProp_17(),
	ConnectionManagementSection_t1574716414_StaticFields::get_offset_of_properties_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { sizeof (DefaultProxySection_t3757845394), -1, sizeof(DefaultProxySection_t3757845394_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1713[6] = 
{
	DefaultProxySection_t3757845394_StaticFields::get_offset_of_properties_17(),
	DefaultProxySection_t3757845394_StaticFields::get_offset_of_bypassListProp_18(),
	DefaultProxySection_t3757845394_StaticFields::get_offset_of_enabledProp_19(),
	DefaultProxySection_t3757845394_StaticFields::get_offset_of_moduleProp_20(),
	DefaultProxySection_t3757845394_StaticFields::get_offset_of_proxyProp_21(),
	DefaultProxySection_t3757845394_StaticFields::get_offset_of_useDefaultCredentialsProp_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { sizeof (FtpCachePolicyElement_t3660269092), -1, sizeof(FtpCachePolicyElement_t3660269092_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1714[2] = 
{
	FtpCachePolicyElement_t3660269092_StaticFields::get_offset_of_policyLevelProp_13(),
	FtpCachePolicyElement_t3660269092_StaticFields::get_offset_of_properties_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { sizeof (HttpCachePolicyElement_t3335049818), -1, sizeof(HttpCachePolicyElement_t3335049818_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1715[5] = 
{
	HttpCachePolicyElement_t3335049818_StaticFields::get_offset_of_maximumAgeProp_13(),
	HttpCachePolicyElement_t3335049818_StaticFields::get_offset_of_maximumStaleProp_14(),
	HttpCachePolicyElement_t3335049818_StaticFields::get_offset_of_minimumFreshProp_15(),
	HttpCachePolicyElement_t3335049818_StaticFields::get_offset_of_policyLevelProp_16(),
	HttpCachePolicyElement_t3335049818_StaticFields::get_offset_of_properties_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (HttpWebRequestElement_t3178920531), -1, sizeof(HttpWebRequestElement_t3178920531_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1716[5] = 
{
	HttpWebRequestElement_t3178920531_StaticFields::get_offset_of_maximumErrorResponseLengthProp_13(),
	HttpWebRequestElement_t3178920531_StaticFields::get_offset_of_maximumResponseHeadersLengthProp_14(),
	HttpWebRequestElement_t3178920531_StaticFields::get_offset_of_maximumUnauthorizedUploadLengthProp_15(),
	HttpWebRequestElement_t3178920531_StaticFields::get_offset_of_useUnsafeHeaderParsingProp_16(),
	HttpWebRequestElement_t3178920531_StaticFields::get_offset_of_properties_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { sizeof (Ipv6Element_t2694178095), -1, sizeof(Ipv6Element_t2694178095_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1717[2] = 
{
	Ipv6Element_t2694178095_StaticFields::get_offset_of_properties_13(),
	Ipv6Element_t2694178095_StaticFields::get_offset_of_enabledProp_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { sizeof (MailSettingsSectionGroup_t1523994462), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { sizeof (ModuleElement_t1599164170), -1, sizeof(ModuleElement_t1599164170_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1719[2] = 
{
	ModuleElement_t1599164170_StaticFields::get_offset_of_properties_13(),
	ModuleElement_t1599164170_StaticFields::get_offset_of_typeProp_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { sizeof (NetConfigurationHandler_t2944741579), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { sizeof (NetSectionGroup_t1688890097), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { sizeof (PerformanceCountersElement_t995147103), -1, sizeof(PerformanceCountersElement_t995147103_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1722[2] = 
{
	PerformanceCountersElement_t995147103_StaticFields::get_offset_of_enabledProp_13(),
	PerformanceCountersElement_t995147103_StaticFields::get_offset_of_properties_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { sizeof (ProxyElement_t1949889880), -1, sizeof(ProxyElement_t1949889880_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1723[6] = 
{
	ProxyElement_t1949889880_StaticFields::get_offset_of_properties_13(),
	ProxyElement_t1949889880_StaticFields::get_offset_of_autoDetectProp_14(),
	ProxyElement_t1949889880_StaticFields::get_offset_of_bypassOnLocalProp_15(),
	ProxyElement_t1949889880_StaticFields::get_offset_of_proxyAddressProp_16(),
	ProxyElement_t1949889880_StaticFields::get_offset_of_scriptLocationProp_17(),
	ProxyElement_t1949889880_StaticFields::get_offset_of_useSystemDefaultProp_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1724 = { sizeof (BypassOnLocalValues_t1443854029)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1724[4] = 
{
	BypassOnLocalValues_t1443854029::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1725 = { sizeof (UseSystemDefaultValues_t3736939686)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1725[4] = 
{
	UseSystemDefaultValues_t3736939686::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1726 = { sizeof (AutoDetectValues_t2572891277)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1726[4] = 
{
	AutoDetectValues_t2572891277::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1727 = { sizeof (RequestCachingSection_t3021090479), -1, sizeof(RequestCachingSection_t3021090479_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1727[7] = 
{
	RequestCachingSection_t3021090479_StaticFields::get_offset_of_properties_17(),
	RequestCachingSection_t3021090479_StaticFields::get_offset_of_defaultFtpCachePolicyProp_18(),
	RequestCachingSection_t3021090479_StaticFields::get_offset_of_defaultHttpCachePolicyProp_19(),
	RequestCachingSection_t3021090479_StaticFields::get_offset_of_defaultPolicyLevelProp_20(),
	RequestCachingSection_t3021090479_StaticFields::get_offset_of_disableAllCachingProp_21(),
	RequestCachingSection_t3021090479_StaticFields::get_offset_of_isPrivateCacheProp_22(),
	RequestCachingSection_t3021090479_StaticFields::get_offset_of_unspecifiedMaximumAgeProp_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1728 = { sizeof (ServicePointManagerElement_t783881716), -1, sizeof(ServicePointManagerElement_t783881716_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1728[7] = 
{
	ServicePointManagerElement_t783881716_StaticFields::get_offset_of_properties_13(),
	ServicePointManagerElement_t783881716_StaticFields::get_offset_of_checkCertificateNameProp_14(),
	ServicePointManagerElement_t783881716_StaticFields::get_offset_of_checkCertificateRevocationListProp_15(),
	ServicePointManagerElement_t783881716_StaticFields::get_offset_of_dnsRefreshTimeoutProp_16(),
	ServicePointManagerElement_t783881716_StaticFields::get_offset_of_enableDnsRoundRobinProp_17(),
	ServicePointManagerElement_t783881716_StaticFields::get_offset_of_expect100ContinueProp_18(),
	ServicePointManagerElement_t783881716_StaticFields::get_offset_of_useNagleAlgorithmProp_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1729 = { sizeof (SettingsSection_t2800213852), -1, sizeof(SettingsSection_t2800213852_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1729[7] = 
{
	SettingsSection_t2800213852_StaticFields::get_offset_of_properties_17(),
	SettingsSection_t2800213852_StaticFields::get_offset_of_httpWebRequestProp_18(),
	SettingsSection_t2800213852_StaticFields::get_offset_of_ipv6Prop_19(),
	SettingsSection_t2800213852_StaticFields::get_offset_of_performanceCountersProp_20(),
	SettingsSection_t2800213852_StaticFields::get_offset_of_servicePointManagerProp_21(),
	SettingsSection_t2800213852_StaticFields::get_offset_of_webProxyScriptProp_22(),
	SettingsSection_t2800213852_StaticFields::get_offset_of_socketProp_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1730 = { sizeof (SocketElement_t3740463139), -1, sizeof(SocketElement_t3740463139_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1730[3] = 
{
	SocketElement_t3740463139_StaticFields::get_offset_of_properties_13(),
	SocketElement_t3740463139_StaticFields::get_offset_of_alwaysUseCompletionPortsForAcceptProp_14(),
	SocketElement_t3740463139_StaticFields::get_offset_of_alwaysUseCompletionPortsForConnectProp_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1731 = { sizeof (WebProxyScriptElement_t2008872561), -1, sizeof(WebProxyScriptElement_t2008872561_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1731[2] = 
{
	WebProxyScriptElement_t2008872561_StaticFields::get_offset_of_downloadTimeoutProp_13(),
	WebProxyScriptElement_t2008872561_StaticFields::get_offset_of_properties_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1732 = { sizeof (WebRequestModuleElementCollection_t2937316013), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1733 = { sizeof (WebRequestModuleElement_t4025521199), -1, sizeof(WebRequestModuleElement_t4025521199_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1733[3] = 
{
	WebRequestModuleElement_t4025521199_StaticFields::get_offset_of_properties_13(),
	WebRequestModuleElement_t4025521199_StaticFields::get_offset_of_prefixProp_14(),
	WebRequestModuleElement_t4025521199_StaticFields::get_offset_of_typeProp_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1734 = { sizeof (WebRequestModuleHandler_t2086194301), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1735 = { sizeof (WebRequestModulesSection_t2385643395), -1, sizeof(WebRequestModulesSection_t2385643395_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1735[2] = 
{
	WebRequestModulesSection_t2385643395_StaticFields::get_offset_of_properties_17(),
	WebRequestModulesSection_t2385643395_StaticFields::get_offset_of_webRequestModulesProp_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1736 = { sizeof (CookieCollection_t2536410684), -1, sizeof(CookieCollection_t2536410684_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1736[2] = 
{
	CookieCollection_t2536410684::get_offset_of_list_0(),
	CookieCollection_t2536410684_StaticFields::get_offset_of_Comparer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1737 = { sizeof (CookieCollectionComparer_t2125669164), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1738 = { sizeof (CookieContainer_t230274359), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1738[4] = 
{
	CookieContainer_t230274359::get_offset_of_capacity_0(),
	CookieContainer_t230274359::get_offset_of_perDomainCapacity_1(),
	CookieContainer_t230274359::get_offset_of_maxCookieSize_2(),
	CookieContainer_t230274359::get_offset_of_cookies_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1739 = { sizeof (Cookie_t2033273982), -1, sizeof(Cookie_t2033273982_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1739[18] = 
{
	Cookie_t2033273982::get_offset_of_comment_0(),
	Cookie_t2033273982::get_offset_of_commentUri_1(),
	Cookie_t2033273982::get_offset_of_discard_2(),
	Cookie_t2033273982::get_offset_of_domain_3(),
	Cookie_t2033273982::get_offset_of_expires_4(),
	Cookie_t2033273982::get_offset_of_httpOnly_5(),
	Cookie_t2033273982::get_offset_of_name_6(),
	Cookie_t2033273982::get_offset_of_path_7(),
	Cookie_t2033273982::get_offset_of_port_8(),
	Cookie_t2033273982::get_offset_of_ports_9(),
	Cookie_t2033273982::get_offset_of_secure_10(),
	Cookie_t2033273982::get_offset_of_timestamp_11(),
	Cookie_t2033273982::get_offset_of_val_12(),
	Cookie_t2033273982::get_offset_of_version_13(),
	Cookie_t2033273982_StaticFields::get_offset_of_reservedCharsName_14(),
	Cookie_t2033273982_StaticFields::get_offset_of_portSeparators_15(),
	Cookie_t2033273982_StaticFields::get_offset_of_tspecials_16(),
	Cookie_t2033273982::get_offset_of_exact_domain_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1740 = { sizeof (CookieException_t2122856709), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1741 = { sizeof (DecompressionMethods_t3697240007)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1741[4] = 
{
	DecompressionMethods_t3697240007::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1742 = { sizeof (DefaultCertificatePolicy_t3264712578), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1743 = { sizeof (Dns_t3289571299), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1744 = { sizeof (EndPoint_t1026786191), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1745 = { sizeof (FileWebRequestCreator_t1433256879), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1746 = { sizeof (FileWebRequest_t1151586641), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1746[15] = 
{
	FileWebRequest_t1151586641::get_offset_of_uri_6(),
	FileWebRequest_t1151586641::get_offset_of_webHeaders_7(),
	FileWebRequest_t1151586641::get_offset_of_credentials_8(),
	FileWebRequest_t1151586641::get_offset_of_connectionGroup_9(),
	FileWebRequest_t1151586641::get_offset_of_contentLength_10(),
	FileWebRequest_t1151586641::get_offset_of_fileAccess_11(),
	FileWebRequest_t1151586641::get_offset_of_method_12(),
	FileWebRequest_t1151586641::get_offset_of_proxy_13(),
	FileWebRequest_t1151586641::get_offset_of_preAuthenticate_14(),
	FileWebRequest_t1151586641::get_offset_of_timeout_15(),
	FileWebRequest_t1151586641::get_offset_of_requestStream_16(),
	FileWebRequest_t1151586641::get_offset_of_webResponse_17(),
	FileWebRequest_t1151586641::get_offset_of_requestEndEvent_18(),
	FileWebRequest_t1151586641::get_offset_of_requesting_19(),
	FileWebRequest_t1151586641::get_offset_of_asyncResponding_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1747 = { sizeof (FileWebStream_t902488976), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1747[1] = 
{
	FileWebStream_t902488976::get_offset_of_webRequest_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1748 = { sizeof (GetRequestStreamCallback_t3736561126), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1749 = { sizeof (GetResponseCallback_t1444420308), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1750 = { sizeof (FileWebResponse_t2971811667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1750[5] = 
{
	FileWebResponse_t2971811667::get_offset_of_responseUri_1(),
	FileWebResponse_t2971811667::get_offset_of_fileStream_2(),
	FileWebResponse_t2971811667::get_offset_of_contentLength_3(),
	FileWebResponse_t2971811667::get_offset_of_webHeaders_4(),
	FileWebResponse_t2971811667::get_offset_of_disposed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1751 = { sizeof (FtpAsyncResult_t1090897169), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1751[9] = 
{
	FtpAsyncResult_t1090897169::get_offset_of_response_0(),
	FtpAsyncResult_t1090897169::get_offset_of_waitHandle_1(),
	FtpAsyncResult_t1090897169::get_offset_of_exception_2(),
	FtpAsyncResult_t1090897169::get_offset_of_callback_3(),
	FtpAsyncResult_t1090897169::get_offset_of_stream_4(),
	FtpAsyncResult_t1090897169::get_offset_of_state_5(),
	FtpAsyncResult_t1090897169::get_offset_of_completed_6(),
	FtpAsyncResult_t1090897169::get_offset_of_synch_7(),
	FtpAsyncResult_t1090897169::get_offset_of_locker_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1752 = { sizeof (FtpDataStream_t2383958406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1752[5] = 
{
	FtpDataStream_t2383958406::get_offset_of_request_2(),
	FtpDataStream_t2383958406::get_offset_of_networkStream_3(),
	FtpDataStream_t2383958406::get_offset_of_disposed_4(),
	FtpDataStream_t2383958406::get_offset_of_isRead_5(),
	FtpDataStream_t2383958406::get_offset_of_totalRead_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1753 = { sizeof (WriteDelegate_t1637408689), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1754 = { sizeof (ReadDelegate_t3891738222), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1755 = { sizeof (FtpRequestCreator_t3707472729), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1756 = { sizeof (FtpStatusCode_t1354479291)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1756[38] = 
{
	FtpStatusCode_t1354479291::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1757 = { sizeof (FtpWebRequest_t3084461655), -1, sizeof(FtpWebRequest_t3084461655_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1757[31] = 
{
	FtpWebRequest_t3084461655::get_offset_of_requestUri_6(),
	FtpWebRequest_t3084461655::get_offset_of_file_name_7(),
	FtpWebRequest_t3084461655::get_offset_of_servicePoint_8(),
	FtpWebRequest_t3084461655::get_offset_of_origDataStream_9(),
	FtpWebRequest_t3084461655::get_offset_of_dataStream_10(),
	FtpWebRequest_t3084461655::get_offset_of_controlStream_11(),
	FtpWebRequest_t3084461655::get_offset_of_controlReader_12(),
	FtpWebRequest_t3084461655::get_offset_of_credentials_13(),
	FtpWebRequest_t3084461655::get_offset_of_hostEntry_14(),
	FtpWebRequest_t3084461655::get_offset_of_localEndPoint_15(),
	FtpWebRequest_t3084461655::get_offset_of_proxy_16(),
	FtpWebRequest_t3084461655::get_offset_of_timeout_17(),
	FtpWebRequest_t3084461655::get_offset_of_rwTimeout_18(),
	FtpWebRequest_t3084461655::get_offset_of_offset_19(),
	FtpWebRequest_t3084461655::get_offset_of_binary_20(),
	FtpWebRequest_t3084461655::get_offset_of_enableSsl_21(),
	FtpWebRequest_t3084461655::get_offset_of_usePassive_22(),
	FtpWebRequest_t3084461655::get_offset_of_keepAlive_23(),
	FtpWebRequest_t3084461655::get_offset_of_method_24(),
	FtpWebRequest_t3084461655::get_offset_of_renameTo_25(),
	FtpWebRequest_t3084461655::get_offset_of_locker_26(),
	FtpWebRequest_t3084461655::get_offset_of_requestState_27(),
	FtpWebRequest_t3084461655::get_offset_of_asyncResult_28(),
	FtpWebRequest_t3084461655::get_offset_of_ftpResponse_29(),
	FtpWebRequest_t3084461655::get_offset_of_requestStream_30(),
	FtpWebRequest_t3084461655::get_offset_of_initial_path_31(),
	FtpWebRequest_t3084461655_StaticFields::get_offset_of_supportedCommands_32(),
	FtpWebRequest_t3084461655::get_offset_of_callback_33(),
	FtpWebRequest_t3084461655_StaticFields::get_offset_of_U3CU3Ef__amU24cache1C_34(),
	FtpWebRequest_t3084461655_StaticFields::get_offset_of_U3CU3Ef__switchU24mapA_35(),
	FtpWebRequest_t3084461655_StaticFields::get_offset_of_U3CU3Ef__switchU24mapB_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1758 = { sizeof (RequestState_t430963524)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1758[10] = 
{
	RequestState_t430963524::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1759 = { sizeof (FtpStatus_t1259919022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1759[2] = 
{
	FtpStatus_t1259919022::get_offset_of_statusCode_0(),
	FtpStatus_t1259919022::get_offset_of_statusDescription_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1760 = { sizeof (FtpWebResponse_t2761394957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1760[12] = 
{
	FtpWebResponse_t2761394957::get_offset_of_stream_1(),
	FtpWebResponse_t2761394957::get_offset_of_uri_2(),
	FtpWebResponse_t2761394957::get_offset_of_statusCode_3(),
	FtpWebResponse_t2761394957::get_offset_of_lastModified_4(),
	FtpWebResponse_t2761394957::get_offset_of_bannerMessage_5(),
	FtpWebResponse_t2761394957::get_offset_of_welcomeMessage_6(),
	FtpWebResponse_t2761394957::get_offset_of_exitMessage_7(),
	FtpWebResponse_t2761394957::get_offset_of_statusDescription_8(),
	FtpWebResponse_t2761394957::get_offset_of_method_9(),
	FtpWebResponse_t2761394957::get_offset_of_disposed_10(),
	FtpWebResponse_t2761394957::get_offset_of_request_11(),
	FtpWebResponse_t2761394957::get_offset_of_contentLength_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1761 = { sizeof (GlobalProxySelection_t183150427), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1762 = { sizeof (HttpRequestCreator_t1200682847), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1763 = { sizeof (HttpStatusCode_t2219054529)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1763[47] = 
{
	HttpStatusCode_t2219054529::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1764 = { sizeof (HttpVersion_t3218902506), -1, sizeof(HttpVersion_t3218902506_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1764[2] = 
{
	HttpVersion_t3218902506_StaticFields::get_offset_of_Version10_0(),
	HttpVersion_t3218902506_StaticFields::get_offset_of_Version11_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1765 = { sizeof (HttpWebRequest_t3949036893), -1, sizeof(HttpWebRequest_t3949036893_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1765[51] = 
{
	HttpWebRequest_t3949036893::get_offset_of_requestUri_6(),
	HttpWebRequest_t3949036893::get_offset_of_actualUri_7(),
	HttpWebRequest_t3949036893::get_offset_of_hostChanged_8(),
	HttpWebRequest_t3949036893::get_offset_of_allowAutoRedirect_9(),
	HttpWebRequest_t3949036893::get_offset_of_allowBuffering_10(),
	HttpWebRequest_t3949036893::get_offset_of_certificates_11(),
	HttpWebRequest_t3949036893::get_offset_of_connectionGroup_12(),
	HttpWebRequest_t3949036893::get_offset_of_contentLength_13(),
	HttpWebRequest_t3949036893::get_offset_of_continueDelegate_14(),
	HttpWebRequest_t3949036893::get_offset_of_cookieContainer_15(),
	HttpWebRequest_t3949036893::get_offset_of_credentials_16(),
	HttpWebRequest_t3949036893::get_offset_of_haveResponse_17(),
	HttpWebRequest_t3949036893::get_offset_of_haveRequest_18(),
	HttpWebRequest_t3949036893::get_offset_of_requestSent_19(),
	HttpWebRequest_t3949036893::get_offset_of_webHeaders_20(),
	HttpWebRequest_t3949036893::get_offset_of_keepAlive_21(),
	HttpWebRequest_t3949036893::get_offset_of_maxAutoRedirect_22(),
	HttpWebRequest_t3949036893::get_offset_of_mediaType_23(),
	HttpWebRequest_t3949036893::get_offset_of_method_24(),
	HttpWebRequest_t3949036893::get_offset_of_initialMethod_25(),
	HttpWebRequest_t3949036893::get_offset_of_pipelined_26(),
	HttpWebRequest_t3949036893::get_offset_of_preAuthenticate_27(),
	HttpWebRequest_t3949036893::get_offset_of_usedPreAuth_28(),
	HttpWebRequest_t3949036893::get_offset_of_version_29(),
	HttpWebRequest_t3949036893::get_offset_of_actualVersion_30(),
	HttpWebRequest_t3949036893::get_offset_of_proxy_31(),
	HttpWebRequest_t3949036893::get_offset_of_sendChunked_32(),
	HttpWebRequest_t3949036893::get_offset_of_servicePoint_33(),
	HttpWebRequest_t3949036893::get_offset_of_timeout_34(),
	HttpWebRequest_t3949036893::get_offset_of_writeStream_35(),
	HttpWebRequest_t3949036893::get_offset_of_webResponse_36(),
	HttpWebRequest_t3949036893::get_offset_of_asyncWrite_37(),
	HttpWebRequest_t3949036893::get_offset_of_asyncRead_38(),
	HttpWebRequest_t3949036893::get_offset_of_abortHandler_39(),
	HttpWebRequest_t3949036893::get_offset_of_aborted_40(),
	HttpWebRequest_t3949036893::get_offset_of_gotRequestStream_41(),
	HttpWebRequest_t3949036893::get_offset_of_redirects_42(),
	HttpWebRequest_t3949036893::get_offset_of_expectContinue_43(),
	HttpWebRequest_t3949036893::get_offset_of_authCompleted_44(),
	HttpWebRequest_t3949036893::get_offset_of_bodyBuffer_45(),
	HttpWebRequest_t3949036893::get_offset_of_bodyBufferLength_46(),
	HttpWebRequest_t3949036893::get_offset_of_getResponseCalled_47(),
	HttpWebRequest_t3949036893::get_offset_of_saved_exc_48(),
	HttpWebRequest_t3949036893::get_offset_of_locker_49(),
	HttpWebRequest_t3949036893::get_offset_of_is_ntlm_auth_50(),
	HttpWebRequest_t3949036893::get_offset_of_finished_reading_51(),
	HttpWebRequest_t3949036893::get_offset_of_WebConnection_52(),
	HttpWebRequest_t3949036893::get_offset_of_auto_decomp_53(),
	HttpWebRequest_t3949036893_StaticFields::get_offset_of_defaultMaxResponseHeadersLength_54(),
	HttpWebRequest_t3949036893::get_offset_of_readWriteTimeout_55(),
	HttpWebRequest_t3949036893::get_offset_of_unsafe_auth_blah_56(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1766 = { sizeof (HttpWebResponse_t3793423559), -1, sizeof(HttpWebResponse_t3793423559_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1766[14] = 
{
	HttpWebResponse_t3793423559::get_offset_of_uri_1(),
	HttpWebResponse_t3793423559::get_offset_of_webHeaders_2(),
	HttpWebResponse_t3793423559::get_offset_of_cookieCollection_3(),
	HttpWebResponse_t3793423559::get_offset_of_method_4(),
	HttpWebResponse_t3793423559::get_offset_of_version_5(),
	HttpWebResponse_t3793423559::get_offset_of_statusCode_6(),
	HttpWebResponse_t3793423559::get_offset_of_statusDescription_7(),
	HttpWebResponse_t3793423559::get_offset_of_contentLength_8(),
	HttpWebResponse_t3793423559::get_offset_of_contentType_9(),
	HttpWebResponse_t3793423559::get_offset_of_cookie_container_10(),
	HttpWebResponse_t3793423559::get_offset_of_disposed_11(),
	HttpWebResponse_t3793423559::get_offset_of_stream_12(),
	HttpWebResponse_t3793423559::get_offset_of_cookieExpiresFormats_13(),
	HttpWebResponse_t3793423559_StaticFields::get_offset_of_U3CU3Ef__switchU24mapD_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1767 = { sizeof (CookieParser_t1524389309), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1767[3] = 
{
	CookieParser_t1524389309::get_offset_of_header_0(),
	CookieParser_t1524389309::get_offset_of_pos_1(),
	CookieParser_t1524389309::get_offset_of_length_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1768 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1769 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1770 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1771 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1772 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1773 = { sizeof (IPAddress_t3525271463), -1, sizeof(IPAddress_t3525271463_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1773[11] = 
{
	IPAddress_t3525271463::get_offset_of_m_Address_0(),
	IPAddress_t3525271463::get_offset_of_m_Family_1(),
	IPAddress_t3525271463::get_offset_of_m_Numbers_2(),
	IPAddress_t3525271463::get_offset_of_m_ScopeId_3(),
	IPAddress_t3525271463_StaticFields::get_offset_of_Any_4(),
	IPAddress_t3525271463_StaticFields::get_offset_of_Broadcast_5(),
	IPAddress_t3525271463_StaticFields::get_offset_of_Loopback_6(),
	IPAddress_t3525271463_StaticFields::get_offset_of_None_7(),
	IPAddress_t3525271463_StaticFields::get_offset_of_IPv6Any_8(),
	IPAddress_t3525271463_StaticFields::get_offset_of_IPv6Loopback_9(),
	IPAddress_t3525271463_StaticFields::get_offset_of_IPv6None_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1774 = { sizeof (IPEndPoint_t2123960758), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1774[2] = 
{
	IPEndPoint_t2123960758::get_offset_of_address_0(),
	IPEndPoint_t2123960758::get_offset_of_port_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1775 = { sizeof (IPHostEntry_t737820957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1775[3] = 
{
	IPHostEntry_t737820957::get_offset_of_addressList_0(),
	IPHostEntry_t737820957::get_offset_of_aliases_1(),
	IPHostEntry_t737820957::get_offset_of_hostName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1776 = { sizeof (IPv6Address_t1083476711), -1, sizeof(IPv6Address_t1083476711_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1776[5] = 
{
	IPv6Address_t1083476711::get_offset_of_address_0(),
	IPv6Address_t1083476711::get_offset_of_prefixLength_1(),
	IPv6Address_t1083476711::get_offset_of_scopeId_2(),
	IPv6Address_t1083476711_StaticFields::get_offset_of_Loopback_3(),
	IPv6Address_t1083476711_StaticFields::get_offset_of_Unspecified_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1777 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1778 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1779 = { sizeof (NetConfig_t2400538105), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1779[2] = 
{
	NetConfig_t2400538105::get_offset_of_ipv6Enabled_0(),
	NetConfig_t2400538105::get_offset_of_MaxResponseHeadersLength_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1780 = { sizeof (NetworkCredential_t1592396767), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1780[3] = 
{
	NetworkCredential_t1592396767::get_offset_of_userName_0(),
	NetworkCredential_t1592396767::get_offset_of_password_1(),
	NetworkCredential_t1592396767::get_offset_of_domain_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1781 = { sizeof (IPInterfaceProperties_t3015049663), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1782 = { sizeof (Win32IPInterfaceProperties2_t3153501730), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1782[3] = 
{
	Win32IPInterfaceProperties2_t3153501730::get_offset_of_addr_0(),
	Win32IPInterfaceProperties2_t3153501730::get_offset_of_mib4_1(),
	Win32IPInterfaceProperties2_t3153501730::get_offset_of_mib6_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1783 = { sizeof (IPv4InterfaceStatistics_t107980849), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1784 = { sizeof (Win32IPv4InterfaceStatistics_t2649373942), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1784[1] = 
{
	Win32IPv4InterfaceStatistics_t2649373942::get_offset_of_info_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1785 = { sizeof (ifa_ifu_t3073026615)+ sizeof (Il2CppObject), sizeof(ifa_ifu_t3073026615_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1785[2] = 
{
	ifa_ifu_t3073026615::get_offset_of_ifu_broadaddr_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifa_ifu_t3073026615::get_offset_of_ifu_dstaddr_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1786 = { sizeof (ifaddrs_t3073171135)+ sizeof (Il2CppObject), sizeof(ifaddrs_t3073171135_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1786[7] = 
{
	ifaddrs_t3073171135::get_offset_of_ifa_next_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t3073171135::get_offset_of_ifa_name_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t3073171135::get_offset_of_ifa_flags_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t3073171135::get_offset_of_ifa_addr_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t3073171135::get_offset_of_ifa_netmask_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t3073171135::get_offset_of_ifa_ifu_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t3073171135::get_offset_of_ifa_data_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1787 = { sizeof (sockaddr_in_t2133621513)+ sizeof (Il2CppObject), sizeof(sockaddr_in_t2133621513_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1787[3] = 
{
	sockaddr_in_t2133621513::get_offset_of_sin_family_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in_t2133621513::get_offset_of_sin_port_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in_t2133621513::get_offset_of_sin_addr_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1788 = { sizeof (sockaddr_in6_t824315777)+ sizeof (Il2CppObject), sizeof(sockaddr_in6_t824315777_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1788[5] = 
{
	sockaddr_in6_t824315777::get_offset_of_sin6_family_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in6_t824315777::get_offset_of_sin6_port_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in6_t824315777::get_offset_of_sin6_flowinfo_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in6_t824315777::get_offset_of_sin6_addr_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in6_t824315777::get_offset_of_sin6_scope_id_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1789 = { sizeof (in6_addr_t1454870809)+ sizeof (Il2CppObject), sizeof(in6_addr_t1454870809_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1789[1] = 
{
	in6_addr_t1454870809::get_offset_of_u6_addr8_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1790 = { sizeof (sockaddr_ll_t2133621604)+ sizeof (Il2CppObject), sizeof(sockaddr_ll_t2133621604_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1790[7] = 
{
	sockaddr_ll_t2133621604::get_offset_of_sll_family_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_ll_t2133621604::get_offset_of_sll_protocol_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_ll_t2133621604::get_offset_of_sll_ifindex_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_ll_t2133621604::get_offset_of_sll_hatype_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_ll_t2133621604::get_offset_of_sll_pkttype_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_ll_t2133621604::get_offset_of_sll_halen_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_ll_t2133621604::get_offset_of_sll_addr_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1791 = { sizeof (LinuxArpHardware_t424852813)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1791[11] = 
{
	LinuxArpHardware_t424852813::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1792 = { sizeof (ifaddrs_t708620971)+ sizeof (Il2CppObject), sizeof(ifaddrs_t708620971_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1792[7] = 
{
	ifaddrs_t708620971::get_offset_of_ifa_next_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t708620971::get_offset_of_ifa_name_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t708620971::get_offset_of_ifa_flags_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t708620971::get_offset_of_ifa_addr_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t708620971::get_offset_of_ifa_netmask_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t708620971::get_offset_of_ifa_dstaddr_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t708620971::get_offset_of_ifa_data_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1793 = { sizeof (sockaddr_t3941944161)+ sizeof (Il2CppObject), sizeof(sockaddr_t3941944161_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1793[2] = 
{
	sockaddr_t3941944161::get_offset_of_sa_len_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_t3941944161::get_offset_of_sa_family_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1794 = { sizeof (sockaddr_in_t213596277)+ sizeof (Il2CppObject), sizeof(sockaddr_in_t213596277_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1794[4] = 
{
	sockaddr_in_t213596277::get_offset_of_sin_len_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in_t213596277::get_offset_of_sin_family_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in_t213596277::get_offset_of_sin_port_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in_t213596277::get_offset_of_sin_addr_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1795 = { sizeof (in6_addr_t1506812267)+ sizeof (Il2CppObject), sizeof(in6_addr_t1506812267_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1795[1] = 
{
	in6_addr_t1506812267::get_offset_of_u6_addr8_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1796 = { sizeof (sockaddr_in6_t1771628115)+ sizeof (Il2CppObject), sizeof(sockaddr_in6_t1771628115_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1796[6] = 
{
	sockaddr_in6_t1771628115::get_offset_of_sin6_len_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in6_t1771628115::get_offset_of_sin6_family_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in6_t1771628115::get_offset_of_sin6_port_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in6_t1771628115::get_offset_of_sin6_flowinfo_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in6_t1771628115::get_offset_of_sin6_addr_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in6_t1771628115::get_offset_of_sin6_scope_id_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1797 = { sizeof (sockaddr_dl_t213596120)+ sizeof (Il2CppObject), sizeof(sockaddr_dl_t213596120_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1797[8] = 
{
	sockaddr_dl_t213596120::get_offset_of_sdl_len_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_dl_t213596120::get_offset_of_sdl_family_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_dl_t213596120::get_offset_of_sdl_index_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_dl_t213596120::get_offset_of_sdl_type_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_dl_t213596120::get_offset_of_sdl_nlen_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_dl_t213596120::get_offset_of_sdl_alen_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_dl_t213596120::get_offset_of_sdl_slen_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_dl_t213596120::get_offset_of_sdl_data_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1798 = { sizeof (MacOsArpHardware_t3918949710)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1798[7] = 
{
	MacOsArpHardware_t3918949710::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1799 = { sizeof (NetworkInformationException_t3136310987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1799[1] = 
{
	NetworkInformationException_t3136310987::get_offset_of_error_code_12(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
