﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorIsControlled
struct GetAnimatorIsControlled_t34751331;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsControlled::.ctor()
extern "C"  void GetAnimatorIsControlled__ctor_m2819795763 (GetAnimatorIsControlled_t34751331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
