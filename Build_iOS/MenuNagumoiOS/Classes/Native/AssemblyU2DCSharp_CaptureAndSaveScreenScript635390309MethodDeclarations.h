﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CaptureAndSaveScreenScript
struct CaptureAndSaveScreenScript_t635390309;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void CaptureAndSaveScreenScript::.ctor()
extern "C"  void CaptureAndSaveScreenScript__ctor_m1054138582 (CaptureAndSaveScreenScript_t635390309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSaveScreenScript::Start()
extern "C"  void CaptureAndSaveScreenScript_Start_m1276374 (CaptureAndSaveScreenScript_t635390309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSaveScreenScript::OnEnable()
extern "C"  void CaptureAndSaveScreenScript_OnEnable_m1113206512 (CaptureAndSaveScreenScript_t635390309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSaveScreenScript::OnDisable()
extern "C"  void CaptureAndSaveScreenScript_OnDisable_m590600765 (CaptureAndSaveScreenScript_t635390309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSaveScreenScript::OnError(System.String)
extern "C"  void CaptureAndSaveScreenScript_OnError_m1941657541 (CaptureAndSaveScreenScript_t635390309 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSaveScreenScript::OnSuccess(System.String)
extern "C"  void CaptureAndSaveScreenScript_OnSuccess_m3760634218 (CaptureAndSaveScreenScript_t635390309 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSaveScreenScript::saveOnFolder()
extern "C"  void CaptureAndSaveScreenScript_saveOnFolder_m3781038552 (CaptureAndSaveScreenScript_t635390309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
