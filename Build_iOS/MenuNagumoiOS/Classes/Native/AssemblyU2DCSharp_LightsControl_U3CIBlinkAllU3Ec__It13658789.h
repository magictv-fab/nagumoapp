﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object
struct Il2CppObject;
// LightsControl
struct LightsControl_t2444629088;

#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat767573031.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LightsControl/<IBlinkAll>c__Iterator5F
struct  U3CIBlinkAllU3Ec__Iterator5F_t13658789  : public Il2CppObject
{
public:
	// System.Int32 LightsControl/<IBlinkAll>c__Iterator5F::<i>__0
	int32_t ___U3CiU3E__0_0;
	// System.Int32 LightsControl/<IBlinkAll>c__Iterator5F::repeat
	int32_t ___repeat_1;
	// System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject> LightsControl/<IBlinkAll>c__Iterator5F::<$s_316>__1
	Enumerator_t767573031  ___U3CU24s_316U3E__1_2;
	// UnityEngine.GameObject LightsControl/<IBlinkAll>c__Iterator5F::<obj>__2
	GameObject_t3674682005 * ___U3CobjU3E__2_3;
	// System.Int32 LightsControl/<IBlinkAll>c__Iterator5F::$PC
	int32_t ___U24PC_4;
	// System.Object LightsControl/<IBlinkAll>c__Iterator5F::$current
	Il2CppObject * ___U24current_5;
	// System.Int32 LightsControl/<IBlinkAll>c__Iterator5F::<$>repeat
	int32_t ___U3CU24U3Erepeat_6;
	// LightsControl LightsControl/<IBlinkAll>c__Iterator5F::<>f__this
	LightsControl_t2444629088 * ___U3CU3Ef__this_7;

public:
	inline static int32_t get_offset_of_U3CiU3E__0_0() { return static_cast<int32_t>(offsetof(U3CIBlinkAllU3Ec__Iterator5F_t13658789, ___U3CiU3E__0_0)); }
	inline int32_t get_U3CiU3E__0_0() const { return ___U3CiU3E__0_0; }
	inline int32_t* get_address_of_U3CiU3E__0_0() { return &___U3CiU3E__0_0; }
	inline void set_U3CiU3E__0_0(int32_t value)
	{
		___U3CiU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_repeat_1() { return static_cast<int32_t>(offsetof(U3CIBlinkAllU3Ec__Iterator5F_t13658789, ___repeat_1)); }
	inline int32_t get_repeat_1() const { return ___repeat_1; }
	inline int32_t* get_address_of_repeat_1() { return &___repeat_1; }
	inline void set_repeat_1(int32_t value)
	{
		___repeat_1 = value;
	}

	inline static int32_t get_offset_of_U3CU24s_316U3E__1_2() { return static_cast<int32_t>(offsetof(U3CIBlinkAllU3Ec__Iterator5F_t13658789, ___U3CU24s_316U3E__1_2)); }
	inline Enumerator_t767573031  get_U3CU24s_316U3E__1_2() const { return ___U3CU24s_316U3E__1_2; }
	inline Enumerator_t767573031 * get_address_of_U3CU24s_316U3E__1_2() { return &___U3CU24s_316U3E__1_2; }
	inline void set_U3CU24s_316U3E__1_2(Enumerator_t767573031  value)
	{
		___U3CU24s_316U3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U3CobjU3E__2_3() { return static_cast<int32_t>(offsetof(U3CIBlinkAllU3Ec__Iterator5F_t13658789, ___U3CobjU3E__2_3)); }
	inline GameObject_t3674682005 * get_U3CobjU3E__2_3() const { return ___U3CobjU3E__2_3; }
	inline GameObject_t3674682005 ** get_address_of_U3CobjU3E__2_3() { return &___U3CobjU3E__2_3; }
	inline void set_U3CobjU3E__2_3(GameObject_t3674682005 * value)
	{
		___U3CobjU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CobjU3E__2_3, value);
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CIBlinkAllU3Ec__Iterator5F_t13658789, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CIBlinkAllU3Ec__Iterator5F_t13658789, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Erepeat_6() { return static_cast<int32_t>(offsetof(U3CIBlinkAllU3Ec__Iterator5F_t13658789, ___U3CU24U3Erepeat_6)); }
	inline int32_t get_U3CU24U3Erepeat_6() const { return ___U3CU24U3Erepeat_6; }
	inline int32_t* get_address_of_U3CU24U3Erepeat_6() { return &___U3CU24U3Erepeat_6; }
	inline void set_U3CU24U3Erepeat_6(int32_t value)
	{
		___U3CU24U3Erepeat_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_7() { return static_cast<int32_t>(offsetof(U3CIBlinkAllU3Ec__Iterator5F_t13658789, ___U3CU3Ef__this_7)); }
	inline LightsControl_t2444629088 * get_U3CU3Ef__this_7() const { return ___U3CU3Ef__this_7; }
	inline LightsControl_t2444629088 ** get_address_of_U3CU3Ef__this_7() { return &___U3CU3Ef__this_7; }
	inline void set_U3CU3Ef__this_7(LightsControl_t2444629088 * value)
	{
		___U3CU3Ef__this_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
