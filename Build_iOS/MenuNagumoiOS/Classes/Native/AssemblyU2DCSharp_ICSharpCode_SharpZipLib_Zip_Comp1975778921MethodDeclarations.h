﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.Compression.Inflater
struct Inflater_t1975778921;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"

// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Inflater::.ctor()
extern "C"  void Inflater__ctor_m2536952234 (Inflater_t1975778921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Inflater::.ctor(System.Boolean)
extern "C"  void Inflater__ctor_m2425378209 (Inflater_t1975778921 * __this, bool ___noHeader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Inflater::.cctor()
extern "C"  void Inflater__cctor_m854011715 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Inflater::Reset()
extern "C"  void Inflater_Reset_m183385175 (Inflater_t1975778921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::DecodeHeader()
extern "C"  bool Inflater_DecodeHeader_m3673849705 (Inflater_t1975778921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::DecodeDict()
extern "C"  bool Inflater_DecodeDict_m1712065810 (Inflater_t1975778921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::DecodeHuffman()
extern "C"  bool Inflater_DecodeHuffman_m247143859 (Inflater_t1975778921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::DecodeChksum()
extern "C"  bool Inflater_DecodeChksum_m2217892545 (Inflater_t1975778921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::Decode()
extern "C"  bool Inflater_Decode_m2858565692 (Inflater_t1975778921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Inflater::SetDictionary(System.Byte[])
extern "C"  void Inflater_SetDictionary_m3967871849 (Inflater_t1975778921 * __this, ByteU5BU5D_t4260760469* ___buffer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Inflater::SetDictionary(System.Byte[],System.Int32,System.Int32)
extern "C"  void Inflater_SetDictionary_m491970441 (Inflater_t1975778921 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___index1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Inflater::SetInput(System.Byte[])
extern "C"  void Inflater_SetInput_m3688853159 (Inflater_t1975778921 * __this, ByteU5BU5D_t4260760469* ___buffer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Inflater::SetInput(System.Byte[],System.Int32,System.Int32)
extern "C"  void Inflater_SetInput_m3496325447 (Inflater_t1975778921 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___index1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::Inflate(System.Byte[])
extern "C"  int32_t Inflater_Inflate_m3215740648 (Inflater_t1975778921 * __this, ByteU5BU5D_t4260760469* ___buffer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::Inflate(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t Inflater_Inflate_m3379548744 (Inflater_t1975778921 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_IsNeedingInput()
extern "C"  bool Inflater_get_IsNeedingInput_m3362945023 (Inflater_t1975778921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_IsNeedingDictionary()
extern "C"  bool Inflater_get_IsNeedingDictionary_m405615331 (Inflater_t1975778921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_IsFinished()
extern "C"  bool Inflater_get_IsFinished_m410633171 (Inflater_t1975778921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_Adler()
extern "C"  int32_t Inflater_get_Adler_m3346609575 (Inflater_t1975778921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_TotalOut()
extern "C"  int64_t Inflater_get_TotalOut_m1037009500 (Inflater_t1975778921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_TotalIn()
extern "C"  int64_t Inflater_get_TotalIn_m3219852729 (Inflater_t1975778921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_RemainingInput()
extern "C"  int32_t Inflater_get_RemainingInput_m2042383973 (Inflater_t1975778921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
