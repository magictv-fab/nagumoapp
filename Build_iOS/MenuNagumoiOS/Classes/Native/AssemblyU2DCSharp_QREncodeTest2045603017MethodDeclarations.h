﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// QREncodeTest
struct QREncodeTest_t2045603017;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"

// System.Void QREncodeTest::.ctor()
extern "C"  void QREncodeTest__ctor_m2984105970 (QREncodeTest_t2045603017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QREncodeTest::Start()
extern "C"  void QREncodeTest_Start_m1931243762 (QREncodeTest_t2045603017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QREncodeTest::Update()
extern "C"  void QREncodeTest_Update_m4039833947 (QREncodeTest_t2045603017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QREncodeTest::qrEncodeFinished(UnityEngine.Texture2D)
extern "C"  void QREncodeTest_qrEncodeFinished_m1227826337 (QREncodeTest_t2045603017 * __this, Texture2D_t3884108195 * ___tex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QREncodeTest::Encode()
extern "C"  void QREncodeTest_Encode_m133488488 (QREncodeTest_t2045603017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QREncodeTest::ClearCode()
extern "C"  void QREncodeTest_ClearCode_m4287461610 (QREncodeTest_t2045603017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
