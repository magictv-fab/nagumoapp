﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.Datamatrix.Internal.Version/ECB[]
struct ECBU5BU5D_t1855874200;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Datamatrix.Internal.Version/ECBlocks
struct  ECBlocks_t3710297555  : public Il2CppObject
{
public:
	// System.Int32 ZXing.Datamatrix.Internal.Version/ECBlocks::ecCodewords
	int32_t ___ecCodewords_0;
	// ZXing.Datamatrix.Internal.Version/ECB[] ZXing.Datamatrix.Internal.Version/ECBlocks::_ecBlocksValue
	ECBU5BU5D_t1855874200* ____ecBlocksValue_1;

public:
	inline static int32_t get_offset_of_ecCodewords_0() { return static_cast<int32_t>(offsetof(ECBlocks_t3710297555, ___ecCodewords_0)); }
	inline int32_t get_ecCodewords_0() const { return ___ecCodewords_0; }
	inline int32_t* get_address_of_ecCodewords_0() { return &___ecCodewords_0; }
	inline void set_ecCodewords_0(int32_t value)
	{
		___ecCodewords_0 = value;
	}

	inline static int32_t get_offset_of__ecBlocksValue_1() { return static_cast<int32_t>(offsetof(ECBlocks_t3710297555, ____ecBlocksValue_1)); }
	inline ECBU5BU5D_t1855874200* get__ecBlocksValue_1() const { return ____ecBlocksValue_1; }
	inline ECBU5BU5D_t1855874200** get_address_of__ecBlocksValue_1() { return &____ecBlocksValue_1; }
	inline void set__ecBlocksValue_1(ECBU5BU5D_t1855874200* value)
	{
		____ecBlocksValue_1 = value;
		Il2CppCodeGenWriteBarrier(&____ecBlocksValue_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
