﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DecodeByStaticPic
struct DecodeByStaticPic_t603858615;

#include "codegen/il2cpp-codegen.h"

// System.Void DecodeByStaticPic::.ctor()
extern "C"  void DecodeByStaticPic__ctor_m2564826260 (DecodeByStaticPic_t603858615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DecodeByStaticPic::Start()
extern "C"  void DecodeByStaticPic_Start_m1511964052 (DecodeByStaticPic_t603858615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DecodeByStaticPic::Update()
extern "C"  void DecodeByStaticPic_Update_m3927064825 (DecodeByStaticPic_t603858615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DecodeByStaticPic::Decode()
extern "C"  void DecodeByStaticPic_Decode_m3175277790 (DecodeByStaticPic_t603858615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
