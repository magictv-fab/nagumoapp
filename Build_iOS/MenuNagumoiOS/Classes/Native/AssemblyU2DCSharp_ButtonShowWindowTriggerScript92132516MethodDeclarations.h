﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ButtonShowWindowTriggerScript
struct ButtonShowWindowTriggerScript_t92132516;

#include "codegen/il2cpp-codegen.h"

// System.Void ButtonShowWindowTriggerScript::.ctor()
extern "C"  void ButtonShowWindowTriggerScript__ctor_m4258837831 (ButtonShowWindowTriggerScript_t92132516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonShowWindowTriggerScript::OnClick()
extern "C"  void ButtonShowWindowTriggerScript_OnClick_m1483527694 (ButtonShowWindowTriggerScript_t92132516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
