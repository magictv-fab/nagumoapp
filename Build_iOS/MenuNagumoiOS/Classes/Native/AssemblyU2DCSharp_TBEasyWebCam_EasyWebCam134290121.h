﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IEasyWebCam
struct IEasyWebCam_t3223070630;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "EasyWebCamLib_TBEasyWebCam_Setting_ResolutionMode805264687.h"
#include "EasyWebCamLib_TBEasyWebCam_Setting_FocusMode2918518777.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TBEasyWebCam.EasyWebCam
struct  EasyWebCam_t134290121  : public MonoBehaviour_t667441552
{
public:
	// TBEasyWebCam.Setting.ResolutionMode TBEasyWebCam.EasyWebCam::mCamResolution
	uint8_t ___mCamResolution_3;
	// TBEasyWebCam.Setting.FocusMode TBEasyWebCam.EasyWebCam::mFocusMode
	int32_t ___mFocusMode_4;

public:
	inline static int32_t get_offset_of_mCamResolution_3() { return static_cast<int32_t>(offsetof(EasyWebCam_t134290121, ___mCamResolution_3)); }
	inline uint8_t get_mCamResolution_3() const { return ___mCamResolution_3; }
	inline uint8_t* get_address_of_mCamResolution_3() { return &___mCamResolution_3; }
	inline void set_mCamResolution_3(uint8_t value)
	{
		___mCamResolution_3 = value;
	}

	inline static int32_t get_offset_of_mFocusMode_4() { return static_cast<int32_t>(offsetof(EasyWebCam_t134290121, ___mFocusMode_4)); }
	inline int32_t get_mFocusMode_4() const { return ___mFocusMode_4; }
	inline int32_t* get_address_of_mFocusMode_4() { return &___mFocusMode_4; }
	inline void set_mFocusMode_4(int32_t value)
	{
		___mFocusMode_4 = value;
	}
};

struct EasyWebCam_t134290121_StaticFields
{
public:
	// IEasyWebCam TBEasyWebCam.EasyWebCam::easyWebCamInterface
	Il2CppObject * ___easyWebCamInterface_2;
	// System.Int32 TBEasyWebCam.EasyWebCam::LogDD
	int32_t ___LogDD_5;

public:
	inline static int32_t get_offset_of_easyWebCamInterface_2() { return static_cast<int32_t>(offsetof(EasyWebCam_t134290121_StaticFields, ___easyWebCamInterface_2)); }
	inline Il2CppObject * get_easyWebCamInterface_2() const { return ___easyWebCamInterface_2; }
	inline Il2CppObject ** get_address_of_easyWebCamInterface_2() { return &___easyWebCamInterface_2; }
	inline void set_easyWebCamInterface_2(Il2CppObject * value)
	{
		___easyWebCamInterface_2 = value;
		Il2CppCodeGenWriteBarrier(&___easyWebCamInterface_2, value);
	}

	inline static int32_t get_offset_of_LogDD_5() { return static_cast<int32_t>(offsetof(EasyWebCam_t134290121_StaticFields, ___LogDD_5)); }
	inline int32_t get_LogDD_5() const { return ___LogDD_5; }
	inline int32_t* get_address_of_LogDD_5() { return &___LogDD_5; }
	inline void set_LogDD_5(int32_t value)
	{
		___LogDD_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
