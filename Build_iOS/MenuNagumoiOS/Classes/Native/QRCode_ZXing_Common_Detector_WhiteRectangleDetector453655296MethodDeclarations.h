﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Common.Detector.WhiteRectangleDetector
struct WhiteRectangleDetector_t453655296;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// ZXing.ResultPoint[]
struct ResultPointU5BU5D_t1195164344;
// ZXing.ResultPoint
struct ResultPoint_t1538592853;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitMatrix1058711404.h"
#include "QRCode_ZXing_ResultPoint1538592853.h"

// ZXing.Common.Detector.WhiteRectangleDetector ZXing.Common.Detector.WhiteRectangleDetector::Create(ZXing.Common.BitMatrix)
extern "C"  WhiteRectangleDetector_t453655296 * WhiteRectangleDetector_Create_m1508075629 (Il2CppObject * __this /* static, unused */, BitMatrix_t1058711404 * ___image0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.Detector.WhiteRectangleDetector ZXing.Common.Detector.WhiteRectangleDetector::Create(ZXing.Common.BitMatrix,System.Int32,System.Int32,System.Int32)
extern "C"  WhiteRectangleDetector_t453655296 * WhiteRectangleDetector_Create_m3168152170 (Il2CppObject * __this /* static, unused */, BitMatrix_t1058711404 * ___image0, int32_t ___initSize1, int32_t ___x2, int32_t ___y3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.Detector.WhiteRectangleDetector::.ctor(ZXing.Common.BitMatrix)
extern "C"  void WhiteRectangleDetector__ctor_m2511657867 (WhiteRectangleDetector_t453655296 * __this, BitMatrix_t1058711404 * ___image0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.Detector.WhiteRectangleDetector::.ctor(ZXing.Common.BitMatrix,System.Int32,System.Int32,System.Int32)
extern "C"  void WhiteRectangleDetector__ctor_m1889243532 (WhiteRectangleDetector_t453655296 * __this, BitMatrix_t1058711404 * ___image0, int32_t ___initSize1, int32_t ___x2, int32_t ___y3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.ResultPoint[] ZXing.Common.Detector.WhiteRectangleDetector::detect()
extern "C"  ResultPointU5BU5D_t1195164344* WhiteRectangleDetector_detect_m941992175 (WhiteRectangleDetector_t453655296 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.ResultPoint ZXing.Common.Detector.WhiteRectangleDetector::getBlackPointOnSegment(System.Single,System.Single,System.Single,System.Single)
extern "C"  ResultPoint_t1538592853 * WhiteRectangleDetector_getBlackPointOnSegment_m3614402751 (WhiteRectangleDetector_t453655296 * __this, float ___aX0, float ___aY1, float ___bX2, float ___bY3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.ResultPoint[] ZXing.Common.Detector.WhiteRectangleDetector::centerEdges(ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint)
extern "C"  ResultPointU5BU5D_t1195164344* WhiteRectangleDetector_centerEdges_m2815038299 (WhiteRectangleDetector_t453655296 * __this, ResultPoint_t1538592853 * ___y0, ResultPoint_t1538592853 * ___z1, ResultPoint_t1538592853 * ___x2, ResultPoint_t1538592853 * ___t3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Common.Detector.WhiteRectangleDetector::containsBlackPoint(System.Int32,System.Int32,System.Int32,System.Boolean)
extern "C"  bool WhiteRectangleDetector_containsBlackPoint_m1269505444 (WhiteRectangleDetector_t453655296 * __this, int32_t ___a0, int32_t ___b1, int32_t ___fixed2, bool ___horizontal3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
