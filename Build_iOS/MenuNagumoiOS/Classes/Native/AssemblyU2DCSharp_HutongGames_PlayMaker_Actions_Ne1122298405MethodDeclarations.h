﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkGetLocalPlayerProperties
struct NetworkGetLocalPlayerProperties_t1122298405;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkGetLocalPlayerProperties::.ctor()
extern "C"  void NetworkGetLocalPlayerProperties__ctor_m1287067313 (NetworkGetLocalPlayerProperties_t1122298405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetLocalPlayerProperties::Reset()
extern "C"  void NetworkGetLocalPlayerProperties_Reset_m3228467550 (NetworkGetLocalPlayerProperties_t1122298405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetLocalPlayerProperties::OnEnter()
extern "C"  void NetworkGetLocalPlayerProperties_OnEnter_m3607802440 (NetworkGetLocalPlayerProperties_t1122298405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
