﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MNUseExample
struct MNUseExample_t1970404964;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MNDialogResult3125317574.h"

// System.Void MNUseExample::.ctor()
extern "C"  void MNUseExample__ctor_m3965969591 (MNUseExample_t1970404964 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNUseExample::Awake()
extern "C"  void MNUseExample_Awake_m4203574810 (MNUseExample_t1970404964 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNUseExample::OnGUI()
extern "C"  void MNUseExample_OnGUI_m3461368241 (MNUseExample_t1970404964 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNUseExample::OnPreloaderTimeOut()
extern "C"  void MNUseExample_OnPreloaderTimeOut_m377559575 (MNUseExample_t1970404964 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNUseExample::OnRatePopUpClose(MNDialogResult)
extern "C"  void MNUseExample_OnRatePopUpClose_m3024292082 (MNUseExample_t1970404964 * __this, int32_t ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNUseExample::OnDialogClose(MNDialogResult)
extern "C"  void MNUseExample_OnDialogClose_m4193601024 (MNUseExample_t1970404964 * __this, int32_t ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNUseExample::OnMessageClose()
extern "C"  void MNUseExample_OnMessageClose_m3817908317 (MNUseExample_t1970404964 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
