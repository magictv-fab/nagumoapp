﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertasManagerCRM_Exclusivas/<LoadLoadeds>c__IteratorB
struct U3CLoadLoadedsU3Ec__IteratorB_t3334583097;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OfertasManagerCRM_Exclusivas/<LoadLoadeds>c__IteratorB::.ctor()
extern "C"  void U3CLoadLoadedsU3Ec__IteratorB__ctor_m3663180162 (U3CLoadLoadedsU3Ec__IteratorB_t3334583097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManagerCRM_Exclusivas/<LoadLoadeds>c__IteratorB::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadLoadedsU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1305493466 (U3CLoadLoadedsU3Ec__IteratorB_t3334583097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManagerCRM_Exclusivas/<LoadLoadeds>c__IteratorB::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadLoadedsU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m3777050478 (U3CLoadLoadedsU3Ec__IteratorB_t3334583097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OfertasManagerCRM_Exclusivas/<LoadLoadeds>c__IteratorB::MoveNext()
extern "C"  bool U3CLoadLoadedsU3Ec__IteratorB_MoveNext_m2423516698 (U3CLoadLoadedsU3Ec__IteratorB_t3334583097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Exclusivas/<LoadLoadeds>c__IteratorB::Dispose()
extern "C"  void U3CLoadLoadedsU3Ec__IteratorB_Dispose_m2636203327 (U3CLoadLoadedsU3Ec__IteratorB_t3334583097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_Exclusivas/<LoadLoadeds>c__IteratorB::Reset()
extern "C"  void U3CLoadLoadedsU3Ec__IteratorB_Reset_m1309613103 (U3CLoadLoadedsU3Ec__IteratorB_t3334583097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
