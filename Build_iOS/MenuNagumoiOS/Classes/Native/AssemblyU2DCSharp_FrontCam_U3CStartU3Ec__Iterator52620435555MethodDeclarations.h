﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FrontCam/<Start>c__Iterator56
struct U3CStartU3Ec__Iterator56_t2620435555;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void FrontCam/<Start>c__Iterator56::.ctor()
extern "C"  void U3CStartU3Ec__Iterator56__ctor_m3224577128 (U3CStartU3Ec__Iterator56_t2620435555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FrontCam/<Start>c__Iterator56::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator56_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3317418 (U3CStartU3Ec__Iterator56_t2620435555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FrontCam/<Start>c__Iterator56::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator56_System_Collections_IEnumerator_get_Current_m3510318654 (U3CStartU3Ec__Iterator56_t2620435555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FrontCam/<Start>c__Iterator56::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator56_MoveNext_m1717928844 (U3CStartU3Ec__Iterator56_t2620435555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FrontCam/<Start>c__Iterator56::Dispose()
extern "C"  void U3CStartU3Ec__Iterator56_Dispose_m2045482661 (U3CStartU3Ec__Iterator56_t2620435555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FrontCam/<Start>c__Iterator56::Reset()
extern "C"  void U3CStartU3Ec__Iterator56_Reset_m871010069 (U3CStartU3Ec__Iterator56_t2620435555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
