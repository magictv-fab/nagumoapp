﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Core.ExtendedPathFilter
struct ExtendedPathFilter_t3673500740;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_DateTime4283661327.h"

// System.Void ICSharpCode.SharpZipLib.Core.ExtendedPathFilter::.ctor(System.String,System.Int64,System.Int64)
extern "C"  void ExtendedPathFilter__ctor_m2250728491 (ExtendedPathFilter_t3673500740 * __this, String_t* ___filter0, int64_t ___minSize1, int64_t ___maxSize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.ExtendedPathFilter::.ctor(System.String,System.DateTime,System.DateTime)
extern "C"  void ExtendedPathFilter__ctor_m2541541071 (ExtendedPathFilter_t3673500740 * __this, String_t* ___filter0, DateTime_t4283661327  ___minDate1, DateTime_t4283661327  ___maxDate2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.ExtendedPathFilter::.ctor(System.String,System.Int64,System.Int64,System.DateTime,System.DateTime)
extern "C"  void ExtendedPathFilter__ctor_m637502991 (ExtendedPathFilter_t3673500740 * __this, String_t* ___filter0, int64_t ___minSize1, int64_t ___maxSize2, DateTime_t4283661327  ___minDate3, DateTime_t4283661327  ___maxDate4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Core.ExtendedPathFilter::IsMatch(System.String)
extern "C"  bool ExtendedPathFilter_IsMatch_m343265342 (ExtendedPathFilter_t3673500740 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Core.ExtendedPathFilter::get_MinSize()
extern "C"  int64_t ExtendedPathFilter_get_MinSize_m3727027694 (ExtendedPathFilter_t3673500740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.ExtendedPathFilter::set_MinSize(System.Int64)
extern "C"  void ExtendedPathFilter_set_MinSize_m3325757181 (ExtendedPathFilter_t3673500740 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Core.ExtendedPathFilter::get_MaxSize()
extern "C"  int64_t ExtendedPathFilter_get_MaxSize_m2954549120 (ExtendedPathFilter_t3673500740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.ExtendedPathFilter::set_MaxSize(System.Int64)
extern "C"  void ExtendedPathFilter_set_MaxSize_m2446599567 (ExtendedPathFilter_t3673500740 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime ICSharpCode.SharpZipLib.Core.ExtendedPathFilter::get_MinDate()
extern "C"  DateTime_t4283661327  ExtendedPathFilter_get_MinDate_m2146052133 (ExtendedPathFilter_t3673500740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.ExtendedPathFilter::set_MinDate(System.DateTime)
extern "C"  void ExtendedPathFilter_set_MinDate_m2841566240 (ExtendedPathFilter_t3673500740 * __this, DateTime_t4283661327  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime ICSharpCode.SharpZipLib.Core.ExtendedPathFilter::get_MaxDate()
extern "C"  DateTime_t4283661327  ExtendedPathFilter_get_MaxDate_m1373573559 (ExtendedPathFilter_t3673500740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.ExtendedPathFilter::set_MaxDate(System.DateTime)
extern "C"  void ExtendedPathFilter_set_MaxDate_m2567658574 (ExtendedPathFilter_t3673500740 * __this, DateTime_t4283661327  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
