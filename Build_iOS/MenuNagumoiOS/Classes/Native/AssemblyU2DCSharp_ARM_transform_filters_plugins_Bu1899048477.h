﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.transform.filters.BussolaFilter
struct BussolaFilter_t1088096095;
// ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract
struct TransformPositionAngleFilterAbstract_t4158746314;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.transform.filters.plugins.BussolaPluginRotationCalibrate
struct  BussolaPluginRotationCalibrate_t1899048477  : public MonoBehaviour_t667441552
{
public:
	// ARM.transform.filters.BussolaFilter ARM.transform.filters.plugins.BussolaPluginRotationCalibrate::bussolaFilter
	BussolaFilter_t1088096095 * ___bussolaFilter_2;
	// ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract ARM.transform.filters.plugins.BussolaPluginRotationCalibrate::objectReferenceToToggleOn
	TransformPositionAngleFilterAbstract_t4158746314 * ___objectReferenceToToggleOn_3;
	// System.Boolean ARM.transform.filters.plugins.BussolaPluginRotationCalibrate::_bussolaFilterActive
	bool ____bussolaFilterActive_4;
	// System.Single ARM.transform.filters.plugins.BussolaPluginRotationCalibrate::rotationReference
	float ___rotationReference_5;
	// System.Single ARM.transform.filters.plugins.BussolaPluginRotationCalibrate::timeToAutoOff
	float ___timeToAutoOff_6;
	// System.Boolean ARM.transform.filters.plugins.BussolaPluginRotationCalibrate::_isActive
	bool ____isActive_7;
	// System.Boolean ARM.transform.filters.plugins.BussolaPluginRotationCalibrate::byPass
	bool ___byPass_8;
	// System.Boolean ARM.transform.filters.plugins.BussolaPluginRotationCalibrate::_hasObjects
	bool ____hasObjects_9;
	// System.Int32 ARM.transform.filters.plugins.BussolaPluginRotationCalibrate::_timeout
	int32_t ____timeout_10;
	// System.Boolean ARM.transform.filters.plugins.BussolaPluginRotationCalibrate::DebugCalibrando
	bool ___DebugCalibrando_11;

public:
	inline static int32_t get_offset_of_bussolaFilter_2() { return static_cast<int32_t>(offsetof(BussolaPluginRotationCalibrate_t1899048477, ___bussolaFilter_2)); }
	inline BussolaFilter_t1088096095 * get_bussolaFilter_2() const { return ___bussolaFilter_2; }
	inline BussolaFilter_t1088096095 ** get_address_of_bussolaFilter_2() { return &___bussolaFilter_2; }
	inline void set_bussolaFilter_2(BussolaFilter_t1088096095 * value)
	{
		___bussolaFilter_2 = value;
		Il2CppCodeGenWriteBarrier(&___bussolaFilter_2, value);
	}

	inline static int32_t get_offset_of_objectReferenceToToggleOn_3() { return static_cast<int32_t>(offsetof(BussolaPluginRotationCalibrate_t1899048477, ___objectReferenceToToggleOn_3)); }
	inline TransformPositionAngleFilterAbstract_t4158746314 * get_objectReferenceToToggleOn_3() const { return ___objectReferenceToToggleOn_3; }
	inline TransformPositionAngleFilterAbstract_t4158746314 ** get_address_of_objectReferenceToToggleOn_3() { return &___objectReferenceToToggleOn_3; }
	inline void set_objectReferenceToToggleOn_3(TransformPositionAngleFilterAbstract_t4158746314 * value)
	{
		___objectReferenceToToggleOn_3 = value;
		Il2CppCodeGenWriteBarrier(&___objectReferenceToToggleOn_3, value);
	}

	inline static int32_t get_offset_of__bussolaFilterActive_4() { return static_cast<int32_t>(offsetof(BussolaPluginRotationCalibrate_t1899048477, ____bussolaFilterActive_4)); }
	inline bool get__bussolaFilterActive_4() const { return ____bussolaFilterActive_4; }
	inline bool* get_address_of__bussolaFilterActive_4() { return &____bussolaFilterActive_4; }
	inline void set__bussolaFilterActive_4(bool value)
	{
		____bussolaFilterActive_4 = value;
	}

	inline static int32_t get_offset_of_rotationReference_5() { return static_cast<int32_t>(offsetof(BussolaPluginRotationCalibrate_t1899048477, ___rotationReference_5)); }
	inline float get_rotationReference_5() const { return ___rotationReference_5; }
	inline float* get_address_of_rotationReference_5() { return &___rotationReference_5; }
	inline void set_rotationReference_5(float value)
	{
		___rotationReference_5 = value;
	}

	inline static int32_t get_offset_of_timeToAutoOff_6() { return static_cast<int32_t>(offsetof(BussolaPluginRotationCalibrate_t1899048477, ___timeToAutoOff_6)); }
	inline float get_timeToAutoOff_6() const { return ___timeToAutoOff_6; }
	inline float* get_address_of_timeToAutoOff_6() { return &___timeToAutoOff_6; }
	inline void set_timeToAutoOff_6(float value)
	{
		___timeToAutoOff_6 = value;
	}

	inline static int32_t get_offset_of__isActive_7() { return static_cast<int32_t>(offsetof(BussolaPluginRotationCalibrate_t1899048477, ____isActive_7)); }
	inline bool get__isActive_7() const { return ____isActive_7; }
	inline bool* get_address_of__isActive_7() { return &____isActive_7; }
	inline void set__isActive_7(bool value)
	{
		____isActive_7 = value;
	}

	inline static int32_t get_offset_of_byPass_8() { return static_cast<int32_t>(offsetof(BussolaPluginRotationCalibrate_t1899048477, ___byPass_8)); }
	inline bool get_byPass_8() const { return ___byPass_8; }
	inline bool* get_address_of_byPass_8() { return &___byPass_8; }
	inline void set_byPass_8(bool value)
	{
		___byPass_8 = value;
	}

	inline static int32_t get_offset_of__hasObjects_9() { return static_cast<int32_t>(offsetof(BussolaPluginRotationCalibrate_t1899048477, ____hasObjects_9)); }
	inline bool get__hasObjects_9() const { return ____hasObjects_9; }
	inline bool* get_address_of__hasObjects_9() { return &____hasObjects_9; }
	inline void set__hasObjects_9(bool value)
	{
		____hasObjects_9 = value;
	}

	inline static int32_t get_offset_of__timeout_10() { return static_cast<int32_t>(offsetof(BussolaPluginRotationCalibrate_t1899048477, ____timeout_10)); }
	inline int32_t get__timeout_10() const { return ____timeout_10; }
	inline int32_t* get_address_of__timeout_10() { return &____timeout_10; }
	inline void set__timeout_10(int32_t value)
	{
		____timeout_10 = value;
	}

	inline static int32_t get_offset_of_DebugCalibrando_11() { return static_cast<int32_t>(offsetof(BussolaPluginRotationCalibrate_t1899048477, ___DebugCalibrando_11)); }
	inline bool get_DebugCalibrando_11() const { return ___DebugCalibrando_11; }
	inline bool* get_address_of_DebugCalibrando_11() { return &___DebugCalibrando_11; }
	inline void set_DebugCalibrando_11(bool value)
	{
		___DebugCalibrando_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
