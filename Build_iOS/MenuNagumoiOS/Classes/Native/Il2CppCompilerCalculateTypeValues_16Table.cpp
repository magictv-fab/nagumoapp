﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_ComponentModel_CharConverter3176686224.h"
#include "System_System_ComponentModel_CollectionConverter1327433352.h"
#include "System_System_ComponentModel_ComponentCollection659278177.h"
#include "System_System_ComponentModel_ComponentConverter3558277359.h"
#include "System_System_ComponentModel_Component332074787.h"
#include "System_System_ComponentModel_CultureInfoConverter1170086032.h"
#include "System_System_ComponentModel_CultureInfoConverter_1565406081.h"
#include "System_System_ComponentModel_DateTimeConverter211680619.h"
#include "System_System_ComponentModel_DecimalConverter3800356315.h"
#include "System_System_ComponentModel_DefaultEventAttribute2032492649.h"
#include "System_System_ComponentModel_DefaultPropertyAttrib2593178002.h"
#include "System_System_ComponentModel_DefaultValueAttribute3756983154.h"
#include "System_System_ComponentModel_DescriptionAttribute2239065356.h"
#include "System_System_ComponentModel_DesignerAttribute1894265111.h"
#include "System_System_ComponentModel_DesignerCategoryAttri1635082169.h"
#include "System_System_ComponentModel_DesignerSerialization2366110769.h"
#include "System_System_ComponentModel_DesignerSerialization1524395549.h"
#include "System_System_ComponentModel_Design_Serialization_2281915717.h"
#include "System_System_ComponentModel_Design_Serialization_1276167776.h"
#include "System_System_ComponentModel_Design_Serialization_3260083331.h"
#include "System_System_ComponentModel_DoubleConverter719751477.h"
#include "System_System_ComponentModel_EditorBrowsableAttrib1665593056.h"
#include "System_System_ComponentModel_EditorBrowsableState1963658837.h"
#include "System_System_ComponentModel_EnumConverter2767655237.h"
#include "System_System_ComponentModel_EnumConverter_EnumCom1375181057.h"
#include "System_System_ComponentModel_ListEntry483724442.h"
#include "System_System_ComponentModel_EventHandlerList1056591002.h"
#include "System_System_ComponentModel_GuidConverter646662333.h"
#include "System_System_ComponentModel_Int16Converter3886635768.h"
#include "System_System_ComponentModel_Int32Converter1078787070.h"
#include "System_System_ComponentModel_Int64Converter3884840575.h"
#include "System_System_ComponentModel_LocalizableAttribute723561746.h"
#include "System_System_ComponentModel_MemberDescriptor2617136693.h"
#include "System_System_ComponentModel_NullableConverter2193771621.h"
#include "System_System_ComponentModel_PropertyDescriptorCol3344846062.h"
#include "System_System_ComponentModel_PropertyDescriptor2073374448.h"
#include "System_System_ComponentModel_ReadOnlyAttribute2204781440.h"
#include "System_System_ComponentModel_RecommendedAsConfigur3266379586.h"
#include "System_System_ComponentModel_ReferenceConverter4239496801.h"
#include "System_System_ComponentModel_ReflectionPropertyDesc735670971.h"
#include "System_System_ComponentModel_SByteConverter3013403921.h"
#include "System_System_ComponentModel_SingleConverter3295811230.h"
#include "System_System_ComponentModel_StringConverter1422224629.h"
#include "System_System_ComponentModel_TimeSpanConverter2449472719.h"
#include "System_System_ComponentModel_TypeConverterAttribut1738187778.h"
#include "System_System_ComponentModel_TypeConverter1753450284.h"
#include "System_System_ComponentModel_TypeConverter_Standar1784247913.h"
#include "System_System_ComponentModel_TypeDescriptionProvid3543085017.h"
#include "System_System_ComponentModel_TypeDescriptor1537159061.h"
#include "System_System_ComponentModel_Info3741775290.h"
#include "System_System_ComponentModel_ComponentInfo809310961.h"
#include "System_System_ComponentModel_TypeInfo1685774996.h"
#include "System_System_ComponentModel_UInt16Converter1984177639.h"
#include "System_System_ComponentModel_UInt32Converter3471296237.h"
#include "System_System_ComponentModel_UInt64Converter1982382446.h"
#include "System_System_ComponentModel_WeakObjectWrapper1518976226.h"
#include "System_System_ComponentModel_WeakObjectWrapperComp3263379503.h"
#include "System_System_ComponentModel_Win32Exception819808416.h"
#include "System_System_Configuration_ConfigurationException1824528701.h"
#include "System_System_Configuration_ConfigurationSettings1141820591.h"
#include "System_System_Configuration_DefaultConfig3583776601.h"
#include "System_System_DefaultUriParser3145002206.h"
#include "System_System_Diagnostics_CorrelationManager2001016395.h"
#include "System_System_Diagnostics_DefaultTraceListener1514692248.h"
#include "System_System_Diagnostics_DiagnosticsConfiguration3411527786.h"
#include "System_System_Diagnostics_DiagnosticsConfiguration1317916382.h"
#include "System_System_Diagnostics_DiagnosticsConfiguration2574338366.h"
#include "System_System_Diagnostics_SourceLevels2222852266.h"
#include "System_System_Diagnostics_TraceImplSettings251944998.h"
#include "System_System_Diagnostics_TraceImpl2492736483.h"
#include "System_System_Diagnostics_TraceListenerCollection1716455733.h"
#include "System_System_Diagnostics_TraceListener964863095.h"
#include "System_System_Diagnostics_TraceOptions122930393.h"
#include "System_System_Diagnostics_TraceSourceInfo2591263276.h"
#include "System_System_GenericUriParser444686856.h"
#include "System_System_IO_Compression_CompressionMode1453657991.h"
#include "System_System_IO_Compression_DeflateStream2030147241.h"
#include "System_System_IO_Compression_DeflateStream_Unmanag2055733333.h"
#include "System_System_IO_Compression_DeflateStream_ReadMet1873379884.h"
#include "System_System_IO_Compression_DeflateStream_WriteMe3250749483.h"
#include "System_System_IO_Compression_GZipStream183418746.h"
#include "System_System_Net_AuthenticationManager3105338575.h"
#include "System_System_Net_Authorization3486603059.h"
#include "System_System_Net_Cache_HttpRequestCacheLevel1130966227.h"
#include "System_System_Net_Cache_RequestCacheLevel3182319227.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1600 = { sizeof (CharConverter_t3176686224), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1601 = { sizeof (CollectionConverter_t1327433352), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1602 = { sizeof (ComponentCollection_t659278177), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1603 = { sizeof (ComponentConverter_t3558277359), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1604 = { sizeof (Component_t332074787), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1604[3] = 
{
	Component_t332074787::get_offset_of_event_handlers_1(),
	Component_t332074787::get_offset_of_mySite_2(),
	Component_t332074787::get_offset_of_disposedEvent_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1605 = { sizeof (CultureInfoConverter_t1170086032), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1605[1] = 
{
	CultureInfoConverter_t1170086032::get_offset_of__standardValues_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1606 = { sizeof (CultureInfoComparer_t1565406081), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1607 = { sizeof (DateTimeConverter_t211680619), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1608 = { sizeof (DecimalConverter_t3800356315), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1609 = { sizeof (DefaultEventAttribute_t2032492649), -1, sizeof(DefaultEventAttribute_t2032492649_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1609[2] = 
{
	DefaultEventAttribute_t2032492649::get_offset_of_eventName_0(),
	DefaultEventAttribute_t2032492649_StaticFields::get_offset_of_Default_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1610 = { sizeof (DefaultPropertyAttribute_t2593178002), -1, sizeof(DefaultPropertyAttribute_t2593178002_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1610[2] = 
{
	DefaultPropertyAttribute_t2593178002::get_offset_of_property_name_0(),
	DefaultPropertyAttribute_t2593178002_StaticFields::get_offset_of_Default_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1611 = { sizeof (DefaultValueAttribute_t3756983154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1611[1] = 
{
	DefaultValueAttribute_t3756983154::get_offset_of_DefaultValue_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1612 = { sizeof (DescriptionAttribute_t2239065356), -1, sizeof(DescriptionAttribute_t2239065356_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1612[2] = 
{
	DescriptionAttribute_t2239065356::get_offset_of_desc_0(),
	DescriptionAttribute_t2239065356_StaticFields::get_offset_of_Default_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1613 = { sizeof (DesignerAttribute_t1894265111), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1613[2] = 
{
	DesignerAttribute_t1894265111::get_offset_of_name_0(),
	DesignerAttribute_t1894265111::get_offset_of_basetypename_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1614 = { sizeof (DesignerCategoryAttribute_t1635082169), -1, sizeof(DesignerCategoryAttribute_t1635082169_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1614[5] = 
{
	DesignerCategoryAttribute_t1635082169::get_offset_of_category_0(),
	DesignerCategoryAttribute_t1635082169_StaticFields::get_offset_of_Component_1(),
	DesignerCategoryAttribute_t1635082169_StaticFields::get_offset_of_Form_2(),
	DesignerCategoryAttribute_t1635082169_StaticFields::get_offset_of_Generic_3(),
	DesignerCategoryAttribute_t1635082169_StaticFields::get_offset_of_Default_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1615 = { sizeof (DesignerSerializationVisibilityAttribute_t2366110769), -1, sizeof(DesignerSerializationVisibilityAttribute_t2366110769_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1615[5] = 
{
	DesignerSerializationVisibilityAttribute_t2366110769::get_offset_of_visibility_0(),
	DesignerSerializationVisibilityAttribute_t2366110769_StaticFields::get_offset_of_Default_1(),
	DesignerSerializationVisibilityAttribute_t2366110769_StaticFields::get_offset_of_Content_2(),
	DesignerSerializationVisibilityAttribute_t2366110769_StaticFields::get_offset_of_Hidden_3(),
	DesignerSerializationVisibilityAttribute_t2366110769_StaticFields::get_offset_of_Visible_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1616 = { sizeof (DesignerSerializationVisibility_t1524395549)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1616[4] = 
{
	DesignerSerializationVisibility_t1524395549::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1617 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1618 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1619 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1620 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1621 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1622 = { sizeof (DesignerSerializerAttribute_t2281915717), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1622[2] = 
{
	DesignerSerializerAttribute_t2281915717::get_offset_of_serializerTypeName_0(),
	DesignerSerializerAttribute_t2281915717::get_offset_of_baseSerializerTypeName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1623 = { sizeof (InstanceDescriptor_t1276167776), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1623[3] = 
{
	InstanceDescriptor_t1276167776::get_offset_of_member_0(),
	InstanceDescriptor_t1276167776::get_offset_of_arguments_1(),
	InstanceDescriptor_t1276167776::get_offset_of_isComplete_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1624 = { sizeof (RootDesignerSerializerAttribute_t3260083331), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1624[3] = 
{
	RootDesignerSerializerAttribute_t3260083331::get_offset_of_serializer_0(),
	RootDesignerSerializerAttribute_t3260083331::get_offset_of_baseserializer_1(),
	RootDesignerSerializerAttribute_t3260083331::get_offset_of_reload_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1625 = { sizeof (DoubleConverter_t719751477), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1626 = { sizeof (EditorBrowsableAttribute_t1665593056), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1626[1] = 
{
	EditorBrowsableAttribute_t1665593056::get_offset_of_state_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1627 = { sizeof (EditorBrowsableState_t1963658837)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1627[4] = 
{
	EditorBrowsableState_t1963658837::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1628 = { sizeof (EnumConverter_t2767655237), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1628[2] = 
{
	EnumConverter_t2767655237::get_offset_of_type_0(),
	EnumConverter_t2767655237::get_offset_of_stdValues_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1629 = { sizeof (EnumComparer_t1375181057), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1630 = { sizeof (ListEntry_t483724442), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1630[3] = 
{
	ListEntry_t483724442::get_offset_of_key_0(),
	ListEntry_t483724442::get_offset_of_value_1(),
	ListEntry_t483724442::get_offset_of_next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1631 = { sizeof (EventHandlerList_t1056591002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1631[2] = 
{
	EventHandlerList_t1056591002::get_offset_of_entries_0(),
	EventHandlerList_t1056591002::get_offset_of_null_entry_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1632 = { sizeof (GuidConverter_t646662333), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1633 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1634 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1635 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1636 = { sizeof (Int16Converter_t3886635768), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1637 = { sizeof (Int32Converter_t1078787070), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1638 = { sizeof (Int64Converter_t3884840575), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1639 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1640 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1641 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1642 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1643 = { sizeof (LocalizableAttribute_t723561746), -1, sizeof(LocalizableAttribute_t723561746_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1643[4] = 
{
	LocalizableAttribute_t723561746::get_offset_of_localizable_0(),
	LocalizableAttribute_t723561746_StaticFields::get_offset_of_Default_1(),
	LocalizableAttribute_t723561746_StaticFields::get_offset_of_No_2(),
	LocalizableAttribute_t723561746_StaticFields::get_offset_of_Yes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1644 = { sizeof (MemberDescriptor_t2617136693), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1644[3] = 
{
	MemberDescriptor_t2617136693::get_offset_of_name_0(),
	MemberDescriptor_t2617136693::get_offset_of_attrs_1(),
	MemberDescriptor_t2617136693::get_offset_of_attrCollection_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1645 = { sizeof (NullableConverter_t2193771621), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1645[3] = 
{
	NullableConverter_t2193771621::get_offset_of_nullableType_0(),
	NullableConverter_t2193771621::get_offset_of_underlyingType_1(),
	NullableConverter_t2193771621::get_offset_of_underlyingTypeConverter_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1646 = { sizeof (PropertyDescriptorCollection_t3344846062), -1, sizeof(PropertyDescriptorCollection_t3344846062_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1646[3] = 
{
	PropertyDescriptorCollection_t3344846062_StaticFields::get_offset_of_Empty_0(),
	PropertyDescriptorCollection_t3344846062::get_offset_of_properties_1(),
	PropertyDescriptorCollection_t3344846062::get_offset_of_readOnly_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1647 = { sizeof (PropertyDescriptor_t2073374448), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1648 = { sizeof (ReadOnlyAttribute_t2204781440), -1, sizeof(ReadOnlyAttribute_t2204781440_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1648[4] = 
{
	ReadOnlyAttribute_t2204781440::get_offset_of_read_only_0(),
	ReadOnlyAttribute_t2204781440_StaticFields::get_offset_of_No_1(),
	ReadOnlyAttribute_t2204781440_StaticFields::get_offset_of_Yes_2(),
	ReadOnlyAttribute_t2204781440_StaticFields::get_offset_of_Default_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1649 = { sizeof (RecommendedAsConfigurableAttribute_t3266379586), -1, sizeof(RecommendedAsConfigurableAttribute_t3266379586_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1649[4] = 
{
	RecommendedAsConfigurableAttribute_t3266379586::get_offset_of_recommendedAsConfigurable_0(),
	RecommendedAsConfigurableAttribute_t3266379586_StaticFields::get_offset_of_Default_1(),
	RecommendedAsConfigurableAttribute_t3266379586_StaticFields::get_offset_of_No_2(),
	RecommendedAsConfigurableAttribute_t3266379586_StaticFields::get_offset_of_Yes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1650 = { sizeof (ReferenceConverter_t4239496801), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1650[1] = 
{
	ReferenceConverter_t4239496801::get_offset_of_reference_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1651 = { sizeof (ReflectionPropertyDescriptor_t735670971), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1651[3] = 
{
	ReflectionPropertyDescriptor_t735670971::get_offset_of__member_3(),
	ReflectionPropertyDescriptor_t735670971::get_offset_of__componentType_4(),
	ReflectionPropertyDescriptor_t735670971::get_offset_of__propertyType_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1652 = { sizeof (SByteConverter_t3013403921), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1653 = { sizeof (SingleConverter_t3295811230), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1654 = { sizeof (StringConverter_t1422224629), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1655 = { sizeof (TimeSpanConverter_t2449472719), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1656 = { sizeof (TypeConverterAttribute_t1738187778), -1, sizeof(TypeConverterAttribute_t1738187778_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1656[2] = 
{
	TypeConverterAttribute_t1738187778_StaticFields::get_offset_of_Default_0(),
	TypeConverterAttribute_t1738187778::get_offset_of_converter_type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1657 = { sizeof (TypeConverter_t1753450284), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1658 = { sizeof (StandardValuesCollection_t1784247913), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1658[1] = 
{
	StandardValuesCollection_t1784247913::get_offset_of_values_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1659 = { sizeof (TypeDescriptionProvider_t3543085017), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1660 = { sizeof (TypeDescriptor_t1537159061), -1, sizeof(TypeDescriptor_t1537159061_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1660[9] = 
{
	TypeDescriptor_t1537159061_StaticFields::get_offset_of_creatingDefaultConverters_0(),
	TypeDescriptor_t1537159061_StaticFields::get_offset_of_defaultConverters_1(),
	TypeDescriptor_t1537159061_StaticFields::get_offset_of_componentTable_2(),
	TypeDescriptor_t1537159061_StaticFields::get_offset_of_typeTable_3(),
	TypeDescriptor_t1537159061_StaticFields::get_offset_of_typeDescriptionProvidersLock_4(),
	TypeDescriptor_t1537159061_StaticFields::get_offset_of_typeDescriptionProviders_5(),
	TypeDescriptor_t1537159061_StaticFields::get_offset_of_componentDescriptionProvidersLock_6(),
	TypeDescriptor_t1537159061_StaticFields::get_offset_of_componentDescriptionProviders_7(),
	TypeDescriptor_t1537159061_StaticFields::get_offset_of_onDispose_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1661 = { sizeof (Info_t3741775290), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1661[2] = 
{
	Info_t3741775290::get_offset_of__infoType_0(),
	Info_t3741775290::get_offset_of__attributes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1662 = { sizeof (ComponentInfo_t809310961), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1662[2] = 
{
	ComponentInfo_t809310961::get_offset_of__component_2(),
	ComponentInfo_t809310961::get_offset_of__properties_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1663 = { sizeof (TypeInfo_t1685774996), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1663[1] = 
{
	TypeInfo_t1685774996::get_offset_of__properties_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1664 = { sizeof (UInt16Converter_t1984177639), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1665 = { sizeof (UInt32Converter_t3471296237), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1666 = { sizeof (UInt64Converter_t1982382446), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1667 = { sizeof (WeakObjectWrapper_t1518976226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1667[2] = 
{
	WeakObjectWrapper_t1518976226::get_offset_of_U3CTargetHashCodeU3Ek__BackingField_0(),
	WeakObjectWrapper_t1518976226::get_offset_of_U3CWeakU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1668 = { sizeof (WeakObjectWrapperComparer_t3263379503), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1669 = { sizeof (Win32Exception_t819808416), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1669[1] = 
{
	Win32Exception_t819808416::get_offset_of_native_error_code_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1670 = { sizeof (ConfigurationException_t1824528701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1670[2] = 
{
	ConfigurationException_t1824528701::get_offset_of_filename_11(),
	ConfigurationException_t1824528701::get_offset_of_line_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1671 = { sizeof (ConfigurationSettings_t1141820591), -1, sizeof(ConfigurationSettings_t1141820591_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1671[2] = 
{
	ConfigurationSettings_t1141820591_StaticFields::get_offset_of_config_0(),
	ConfigurationSettings_t1141820591_StaticFields::get_offset_of_lockobj_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1672 = { sizeof (DefaultConfig_t3583776601), -1, sizeof(DefaultConfig_t3583776601_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1672[1] = 
{
	DefaultConfig_t3583776601_StaticFields::get_offset_of_instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1673 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1674 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1675 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1676 = { sizeof (DefaultUriParser_t3145002206), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1677 = { sizeof (CorrelationManager_t2001016395), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1677[1] = 
{
	CorrelationManager_t2001016395::get_offset_of_op_stack_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1678 = { sizeof (DefaultTraceListener_t1514692248), -1, sizeof(DefaultTraceListener_t1514692248_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1678[5] = 
{
	DefaultTraceListener_t1514692248_StaticFields::get_offset_of_OnWin32_7(),
	DefaultTraceListener_t1514692248_StaticFields::get_offset_of_MonoTracePrefix_8(),
	DefaultTraceListener_t1514692248_StaticFields::get_offset_of_MonoTraceFile_9(),
	DefaultTraceListener_t1514692248::get_offset_of_logFileName_10(),
	DefaultTraceListener_t1514692248::get_offset_of_assertUiEnabled_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1679 = { sizeof (DiagnosticsConfiguration_t3411527786), -1, sizeof(DiagnosticsConfiguration_t3411527786_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1679[1] = 
{
	DiagnosticsConfiguration_t3411527786_StaticFields::get_offset_of_settings_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1680 = { sizeof (DiagnosticsConfigurationHandler_t1317916382), -1, sizeof(DiagnosticsConfigurationHandler_t1317916382_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1680[5] = 
{
	DiagnosticsConfigurationHandler_t1317916382::get_offset_of_configValues_0(),
	DiagnosticsConfigurationHandler_t1317916382::get_offset_of_elementHandlers_1(),
	DiagnosticsConfigurationHandler_t1317916382_StaticFields::get_offset_of_U3CU3Ef__switchU24map5_2(),
	DiagnosticsConfigurationHandler_t1317916382_StaticFields::get_offset_of_U3CU3Ef__switchU24map6_3(),
	DiagnosticsConfigurationHandler_t1317916382_StaticFields::get_offset_of_U3CU3Ef__switchU24map7_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1681 = { sizeof (ElementHandler_t2574338366), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1682 = { sizeof (SourceLevels_t2222852266)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1682[9] = 
{
	SourceLevels_t2222852266::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1683 = { sizeof (TraceImplSettings_t251944998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1683[4] = 
{
	TraceImplSettings_t251944998::get_offset_of_AutoFlush_0(),
	TraceImplSettings_t251944998::get_offset_of_IndentLevel_1(),
	TraceImplSettings_t251944998::get_offset_of_IndentSize_2(),
	TraceImplSettings_t251944998::get_offset_of_Listeners_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1684 = { sizeof (TraceImpl_t2492736483), -1, sizeof(TraceImpl_t2492736483_StaticFields), sizeof(TraceImpl_t2492736483_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable1684[6] = 
{
	TraceImpl_t2492736483_StaticFields::get_offset_of_initLock_0(),
	TraceImpl_t2492736483_StaticFields::get_offset_of_autoFlush_1(),
	THREAD_STATIC_FIELD_OFFSET,
	THREAD_STATIC_FIELD_OFFSET,
	TraceImpl_t2492736483_StaticFields::get_offset_of_listeners_4(),
	TraceImpl_t2492736483_StaticFields::get_offset_of_correlation_manager_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1685 = { sizeof (TraceListenerCollection_t1716455733), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1685[1] = 
{
	TraceListenerCollection_t1716455733::get_offset_of_listeners_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1686 = { sizeof (TraceListener_t964863095), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1686[6] = 
{
	TraceListener_t964863095::get_offset_of_indentLevel_1(),
	TraceListener_t964863095::get_offset_of_indentSize_2(),
	TraceListener_t964863095::get_offset_of_attributes_3(),
	TraceListener_t964863095::get_offset_of_options_4(),
	TraceListener_t964863095::get_offset_of_name_5(),
	TraceListener_t964863095::get_offset_of_needIndent_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1687 = { sizeof (TraceOptions_t122930393)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1687[8] = 
{
	TraceOptions_t122930393::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1688 = { sizeof (TraceSourceInfo_t2591263276), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1688[3] = 
{
	TraceSourceInfo_t2591263276::get_offset_of_name_0(),
	TraceSourceInfo_t2591263276::get_offset_of_levels_1(),
	TraceSourceInfo_t2591263276::get_offset_of_listeners_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1689 = { sizeof (GenericUriParser_t444686856), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1690 = { sizeof (CompressionMode_t1453657991)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1690[3] = 
{
	CompressionMode_t1453657991::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1691 = { sizeof (DeflateStream_t2030147241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1691[8] = 
{
	DeflateStream_t2030147241::get_offset_of_base_stream_2(),
	DeflateStream_t2030147241::get_offset_of_mode_3(),
	DeflateStream_t2030147241::get_offset_of_leaveOpen_4(),
	DeflateStream_t2030147241::get_offset_of_disposed_5(),
	DeflateStream_t2030147241::get_offset_of_feeder_6(),
	DeflateStream_t2030147241::get_offset_of_z_stream_7(),
	DeflateStream_t2030147241::get_offset_of_io_buffer_8(),
	DeflateStream_t2030147241::get_offset_of_data_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1692 = { sizeof (UnmanagedReadOrWrite_t2055733333), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1693 = { sizeof (ReadMethod_t1873379884), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1694 = { sizeof (WriteMethod_t3250749483), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1695 = { sizeof (GZipStream_t183418746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1695[1] = 
{
	GZipStream_t183418746::get_offset_of_deflateStream_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1696 = { sizeof (AuthenticationManager_t3105338575), -1, sizeof(AuthenticationManager_t3105338575_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1696[3] = 
{
	AuthenticationManager_t3105338575_StaticFields::get_offset_of_modules_0(),
	AuthenticationManager_t3105338575_StaticFields::get_offset_of_locker_1(),
	AuthenticationManager_t3105338575_StaticFields::get_offset_of_credential_policy_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1697 = { sizeof (Authorization_t3486603059), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1697[3] = 
{
	Authorization_t3486603059::get_offset_of_token_0(),
	Authorization_t3486603059::get_offset_of_complete_1(),
	Authorization_t3486603059::get_offset_of_module_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1698 = { sizeof (HttpRequestCacheLevel_t1130966227)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1698[10] = 
{
	HttpRequestCacheLevel_t1130966227::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1699 = { sizeof (RequestCacheLevel_t3182319227)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1699[8] = 
{
	RequestCacheLevel_t3182319227::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
