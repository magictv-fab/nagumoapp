﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetPreviousStateName
struct GetPreviousStateName_t1600272887;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetPreviousStateName::.ctor()
extern "C"  void GetPreviousStateName__ctor_m4163052111 (GetPreviousStateName_t1600272887 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetPreviousStateName::Reset()
extern "C"  void GetPreviousStateName_Reset_m1809485052 (GetPreviousStateName_t1600272887 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetPreviousStateName::OnEnter()
extern "C"  void GetPreviousStateName_OnEnter_m1470254694 (GetPreviousStateName_t1600272887 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
