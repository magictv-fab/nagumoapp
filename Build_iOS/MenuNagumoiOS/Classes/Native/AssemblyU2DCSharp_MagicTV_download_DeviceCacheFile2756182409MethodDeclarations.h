﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.download.DeviceCacheFileManager
struct DeviceCacheFileManager_t2756182409;
// MagicTV.vo.ResultRevisionVO
struct ResultRevisionVO_t2445597391;
// MagicTV.vo.result.ConfirmResultVO
struct ConfirmResultVO_t1731700412;
// MagicTV.vo.UrlInfoVO
struct UrlInfoVO_t1761987528;
// UnityEngine.WWW
struct WWW_t3134621005;
// System.Collections.Generic.IEnumerable`1<MagicTV.vo.BundleVO>
struct IEnumerable_1_t990463734;
// MagicTV.vo.result.ReturnResultVO
struct ReturnResultVO_t972950402;
// MagicTV.vo.BundleVO
struct BundleVO_t1984518073;
// MagicTV.vo.BundleVO[]
struct BundleVOU5BU5D_t2162892100;
// MagicTV.vo.FileVO
struct FileVO_t1935348659;
// MagicTV.vo.FileVO[]
struct FileVOU5BU5D_t573513762;
// MagicTV.vo.UrlInfoVO[]
struct UrlInfoVOU5BU5D_t1681515545;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_vo_ResultRevisionVO2445597391.h"
#include "AssemblyU2DCSharp_MagicTV_vo_result_ConfirmResultV1731700412.h"
#include "AssemblyU2DCSharp_MagicTV_vo_UrlInfoVO1761987528.h"
#include "UnityEngine_UnityEngine_WWW3134621005.h"
#include "AssemblyU2DCSharp_MagicTV_vo_BundleVO1984518073.h"
#include "AssemblyU2DCSharp_MagicTV_vo_FileVO1935348659.h"

// System.Void MagicTV.download.DeviceCacheFileManager::.ctor()
extern "C"  void DeviceCacheFileManager__ctor_m277431711 (DeviceCacheFileManager_t2756182409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.DeviceCacheFileManager::prepare()
extern "C"  void DeviceCacheFileManager_prepare_m1250882340 (DeviceCacheFileManager_t2756182409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.DeviceCacheFileManager::deleteLocalFiles()
extern "C"  void DeviceCacheFileManager_deleteLocalFiles_m2159360444 (DeviceCacheFileManager_t2756182409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.DeviceCacheFileManager::reset()
extern "C"  void DeviceCacheFileManager_reset_m554178668 (DeviceCacheFileManager_t2756182409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.DeviceCacheFileManager::SetFileList(MagicTV.vo.ResultRevisionVO)
extern "C"  void DeviceCacheFileManager_SetFileList_m152291026 (DeviceCacheFileManager_t2756182409 * __this, ResultRevisionVO_t2445597391 * ___revisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.DeviceCacheFileManager::StartCache()
extern "C"  void DeviceCacheFileManager_StartCache_m255913221 (DeviceCacheFileManager_t2756182409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.DeviceCacheFileManager::userConfirm(MagicTV.vo.result.ConfirmResultVO)
extern "C"  void DeviceCacheFileManager_userConfirm_m1289358021 (DeviceCacheFileManager_t2756182409 * __this, ConfirmResultVO_t1731700412 * ___vo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.DeviceCacheFileManager::cancelDownload()
extern "C"  void DeviceCacheFileManager_cancelDownload_m2116702023 (DeviceCacheFileManager_t2756182409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.DeviceCacheFileManager::Cancel()
extern "C"  void DeviceCacheFileManager_Cancel_m291540671 (DeviceCacheFileManager_t2756182409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.DeviceCacheFileManager::StartScreenDownloadNoCancel()
extern "C"  void DeviceCacheFileManager_StartScreenDownloadNoCancel_m868468046 (DeviceCacheFileManager_t2756182409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.DeviceCacheFileManager::StartScreenDownloadCancel()
extern "C"  void DeviceCacheFileManager_StartScreenDownloadCancel_m882235629 (DeviceCacheFileManager_t2756182409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.DeviceCacheFileManager::onComplete()
extern "C"  void DeviceCacheFileManager_onComplete_m2485216989 (DeviceCacheFileManager_t2756182409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.DeviceCacheFileManager::onProgress(System.Single)
extern "C"  void DeviceCacheFileManager_onProgress_m731140730 (DeviceCacheFileManager_t2756182409 * __this, float ___progress0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.DeviceCacheFileManager::onItemComplete(MagicTV.vo.UrlInfoVO,UnityEngine.WWW)
extern "C"  void DeviceCacheFileManager_onItemComplete_m2985461916 (DeviceCacheFileManager_t2756182409 * __this, UrlInfoVO_t1761987528 * ___urlToSave0, WWW_t3134621005 * ___www1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.DeviceCacheFileManager::DoStartCache()
extern "C"  void DeviceCacheFileManager_DoStartCache_m1079637040 (DeviceCacheFileManager_t2756182409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.download.DeviceCacheFileManager::DoStartCacheDownload(System.Collections.Generic.IEnumerable`1<MagicTV.vo.BundleVO>)
extern "C"  void DeviceCacheFileManager_DoStartCacheDownload_m1377349604 (DeviceCacheFileManager_t2756182409 * __this, Il2CppObject* ___bundleList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.result.ReturnResultVO MagicTV.download.DeviceCacheFileManager::Delete(MagicTV.vo.ResultRevisionVO)
extern "C"  ReturnResultVO_t972950402 * DeviceCacheFileManager_Delete_m1963247191 (DeviceCacheFileManager_t2756182409 * __this, ResultRevisionVO_t2445597391 * ___revision0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.result.ReturnResultVO MagicTV.download.DeviceCacheFileManager::Delete(MagicTV.vo.BundleVO)
extern "C"  ReturnResultVO_t972950402 * DeviceCacheFileManager_Delete_m2623400877 (DeviceCacheFileManager_t2756182409 * __this, BundleVO_t1984518073 * ___bundle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.result.ReturnResultVO MagicTV.download.DeviceCacheFileManager::Delete(MagicTV.vo.BundleVO[])
extern "C"  ReturnResultVO_t972950402 * DeviceCacheFileManager_Delete_m4237458315 (DeviceCacheFileManager_t2756182409 * __this, BundleVOU5BU5D_t2162892100* ___bundles0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.result.ReturnResultVO MagicTV.download.DeviceCacheFileManager::Delete(MagicTV.vo.FileVO)
extern "C"  ReturnResultVO_t972950402 * DeviceCacheFileManager_Delete_m3712383027 (DeviceCacheFileManager_t2756182409 * __this, FileVO_t1935348659 * ___file0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.result.ReturnResultVO MagicTV.download.DeviceCacheFileManager::Delete(MagicTV.vo.FileVO[])
extern "C"  ReturnResultVO_t972950402 * DeviceCacheFileManager_Delete_m2777284241 (DeviceCacheFileManager_t2756182409 * __this, FileVOU5BU5D_t573513762* ___files0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.result.ReturnResultVO MagicTV.download.DeviceCacheFileManager::Delete(MagicTV.vo.UrlInfoVO)
extern "C"  ReturnResultVO_t972950402 * DeviceCacheFileManager_Delete_m4085140916 (DeviceCacheFileManager_t2756182409 * __this, UrlInfoVO_t1761987528 * ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.result.ReturnResultVO MagicTV.download.DeviceCacheFileManager::Delete(MagicTV.vo.UrlInfoVO[])
extern "C"  ReturnResultVO_t972950402 * DeviceCacheFileManager_Delete_m220362706 (DeviceCacheFileManager_t2756182409 * __this, UrlInfoVOU5BU5D_t1681515545* ___urls0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.download.DeviceCacheFileManager::<deleteLocalFiles>m__28(MagicTV.vo.UrlInfoVO)
extern "C"  String_t* DeviceCacheFileManager_U3CdeleteLocalFilesU3Em__28_m3325179976 (Il2CppObject * __this /* static, unused */, UrlInfoVO_t1761987528 * ___u0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
