﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CarnesData[]
struct CarnesDataU5BU5D_t2965870023;
// CarnesData
struct CarnesData_t3441984818;
// UnityEngine.WWW
struct WWW_t3134621005;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// UnityEngine.Sprite
struct Sprite_t3199167241;
// System.Object
struct Il2CppObject;
// PedidoAcougue
struct PedidoAcougue_t9638794;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PedidoAcougue/<ILoadImgs>c__Iterator70
struct  U3CILoadImgsU3Ec__Iterator70_t2033076116  : public Il2CppObject
{
public:
	// CarnesData[] PedidoAcougue/<ILoadImgs>c__Iterator70::<$s_335>__0
	CarnesDataU5BU5D_t2965870023* ___U3CU24s_335U3E__0_0;
	// System.Int32 PedidoAcougue/<ILoadImgs>c__Iterator70::<$s_336>__1
	int32_t ___U3CU24s_336U3E__1_1;
	// CarnesData PedidoAcougue/<ILoadImgs>c__Iterator70::<carneData>__2
	CarnesData_t3441984818 * ___U3CcarneDataU3E__2_2;
	// UnityEngine.WWW PedidoAcougue/<ILoadImgs>c__Iterator70::<www>__3
	WWW_t3134621005 * ___U3CwwwU3E__3_3;
	// UnityEngine.Texture2D PedidoAcougue/<ILoadImgs>c__Iterator70::<texture>__4
	Texture2D_t3884108195 * ___U3CtextureU3E__4_4;
	// UnityEngine.Sprite PedidoAcougue/<ILoadImgs>c__Iterator70::<spt>__5
	Sprite_t3199167241 * ___U3CsptU3E__5_5;
	// System.Int32 PedidoAcougue/<ILoadImgs>c__Iterator70::$PC
	int32_t ___U24PC_6;
	// System.Object PedidoAcougue/<ILoadImgs>c__Iterator70::$current
	Il2CppObject * ___U24current_7;
	// PedidoAcougue PedidoAcougue/<ILoadImgs>c__Iterator70::<>f__this
	PedidoAcougue_t9638794 * ___U3CU3Ef__this_8;

public:
	inline static int32_t get_offset_of_U3CU24s_335U3E__0_0() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator70_t2033076116, ___U3CU24s_335U3E__0_0)); }
	inline CarnesDataU5BU5D_t2965870023* get_U3CU24s_335U3E__0_0() const { return ___U3CU24s_335U3E__0_0; }
	inline CarnesDataU5BU5D_t2965870023** get_address_of_U3CU24s_335U3E__0_0() { return &___U3CU24s_335U3E__0_0; }
	inline void set_U3CU24s_335U3E__0_0(CarnesDataU5BU5D_t2965870023* value)
	{
		___U3CU24s_335U3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_335U3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CU24s_336U3E__1_1() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator70_t2033076116, ___U3CU24s_336U3E__1_1)); }
	inline int32_t get_U3CU24s_336U3E__1_1() const { return ___U3CU24s_336U3E__1_1; }
	inline int32_t* get_address_of_U3CU24s_336U3E__1_1() { return &___U3CU24s_336U3E__1_1; }
	inline void set_U3CU24s_336U3E__1_1(int32_t value)
	{
		___U3CU24s_336U3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CcarneDataU3E__2_2() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator70_t2033076116, ___U3CcarneDataU3E__2_2)); }
	inline CarnesData_t3441984818 * get_U3CcarneDataU3E__2_2() const { return ___U3CcarneDataU3E__2_2; }
	inline CarnesData_t3441984818 ** get_address_of_U3CcarneDataU3E__2_2() { return &___U3CcarneDataU3E__2_2; }
	inline void set_U3CcarneDataU3E__2_2(CarnesData_t3441984818 * value)
	{
		___U3CcarneDataU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcarneDataU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__3_3() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator70_t2033076116, ___U3CwwwU3E__3_3)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__3_3() const { return ___U3CwwwU3E__3_3; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__3_3() { return &___U3CwwwU3E__3_3; }
	inline void set_U3CwwwU3E__3_3(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CtextureU3E__4_4() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator70_t2033076116, ___U3CtextureU3E__4_4)); }
	inline Texture2D_t3884108195 * get_U3CtextureU3E__4_4() const { return ___U3CtextureU3E__4_4; }
	inline Texture2D_t3884108195 ** get_address_of_U3CtextureU3E__4_4() { return &___U3CtextureU3E__4_4; }
	inline void set_U3CtextureU3E__4_4(Texture2D_t3884108195 * value)
	{
		___U3CtextureU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtextureU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U3CsptU3E__5_5() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator70_t2033076116, ___U3CsptU3E__5_5)); }
	inline Sprite_t3199167241 * get_U3CsptU3E__5_5() const { return ___U3CsptU3E__5_5; }
	inline Sprite_t3199167241 ** get_address_of_U3CsptU3E__5_5() { return &___U3CsptU3E__5_5; }
	inline void set_U3CsptU3E__5_5(Sprite_t3199167241 * value)
	{
		___U3CsptU3E__5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsptU3E__5_5, value);
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator70_t2033076116, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator70_t2033076116, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_8() { return static_cast<int32_t>(offsetof(U3CILoadImgsU3Ec__Iterator70_t2033076116, ___U3CU3Ef__this_8)); }
	inline PedidoAcougue_t9638794 * get_U3CU3Ef__this_8() const { return ___U3CU3Ef__this_8; }
	inline PedidoAcougue_t9638794 ** get_address_of_U3CU3Ef__this_8() { return &___U3CU3Ef__this_8; }
	inline void set_U3CU3Ef__this_8(PedidoAcougue_t9638794 * value)
	{
		___U3CU3Ef__this_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
