﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.FB/<Init>c__AnonStorey90
struct U3CInitU3Ec__AnonStorey90_t3153034682;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.FB/<Init>c__AnonStorey90::.ctor()
extern "C"  void U3CInitU3Ec__AnonStorey90__ctor_m3290227441 (U3CInitU3Ec__AnonStorey90_t3153034682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FB/<Init>c__AnonStorey90::<>m__C()
extern "C"  void U3CInitU3Ec__AnonStorey90_U3CU3Em__C_m3148725483 (U3CInitU3Ec__AnonStorey90_t3153034682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FB/<Init>c__AnonStorey90::<>m__D()
extern "C"  void U3CInitU3Ec__AnonStorey90_U3CU3Em__D_m3148726444 (U3CInitU3Ec__AnonStorey90_t3153034682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FB/<Init>c__AnonStorey90::<>m__E()
extern "C"  void U3CInitU3Ec__AnonStorey90_U3CU3Em__E_m3148727405 (U3CInitU3Ec__AnonStorey90_t3153034682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FB/<Init>c__AnonStorey90::<>m__F()
extern "C"  void U3CInitU3Ec__AnonStorey90_U3CU3Em__F_m3148728366 (U3CInitU3Ec__AnonStorey90_t3153034682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
