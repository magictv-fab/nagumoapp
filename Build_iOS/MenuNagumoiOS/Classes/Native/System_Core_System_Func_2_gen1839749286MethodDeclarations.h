﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen785513668MethodDeclarations.h"

// System.Void System.Func`2<MagicTV.vo.BundleVO,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m1882989162(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t1839749286 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m949542583_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<MagicTV.vo.BundleVO,System.Boolean>::Invoke(T)
#define Func_2_Invoke_m253790796(__this, ___arg10, method) ((  bool (*) (Func_2_t1839749286 *, BundleVO_t1984518073 *, const MethodInfo*))Func_2_Invoke_m1882130143_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<MagicTV.vo.BundleVO,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m1754639871(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t1839749286 *, BundleVO_t1984518073 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m1852288274_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<MagicTV.vo.BundleVO,System.Boolean>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m3925958920(__this, ___result0, method) ((  bool (*) (Func_2_t1839749286 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m1659014741_gshared)(__this, ___result0, method)
