﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString[]
struct FsmStringU5BU5D_t2523845914;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.Utility.Arrays`1<HutongGames.PlayMaker.FsmString>
struct  Arrays_1_t1869704253  : public Il2CppObject
{
public:

public:
};

struct Arrays_1_t1869704253_StaticFields
{
public:
	// T[] HutongGames.Utility.Arrays`1::Empty
	FsmStringU5BU5D_t2523845914* ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Arrays_1_t1869704253_StaticFields, ___Empty_0)); }
	inline FsmStringU5BU5D_t2523845914* get_Empty_0() const { return ___Empty_0; }
	inline FsmStringU5BU5D_t2523845914** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(FsmStringU5BU5D_t2523845914* value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier(&___Empty_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
