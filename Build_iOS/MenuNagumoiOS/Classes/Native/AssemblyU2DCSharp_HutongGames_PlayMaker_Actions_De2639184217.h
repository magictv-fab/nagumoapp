﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t2131419205;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "UnityEngine_UnityEngine_FullScreenMovieControlMode3302654991.h"
#include "UnityEngine_UnityEngine_FullScreenMovieScalingMode4213044537.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DevicePlayFullScreenMovie
struct  DevicePlayFullScreenMovie_t2639184217  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.DevicePlayFullScreenMovie::moviePath
	FsmString_t952858651 * ___moviePath_9;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.DevicePlayFullScreenMovie::fadeColor
	FsmColor_t2131419205 * ___fadeColor_10;
	// UnityEngine.FullScreenMovieControlMode HutongGames.PlayMaker.Actions.DevicePlayFullScreenMovie::movieControlMode
	int32_t ___movieControlMode_11;
	// UnityEngine.FullScreenMovieScalingMode HutongGames.PlayMaker.Actions.DevicePlayFullScreenMovie::movieScalingMode
	int32_t ___movieScalingMode_12;

public:
	inline static int32_t get_offset_of_moviePath_9() { return static_cast<int32_t>(offsetof(DevicePlayFullScreenMovie_t2639184217, ___moviePath_9)); }
	inline FsmString_t952858651 * get_moviePath_9() const { return ___moviePath_9; }
	inline FsmString_t952858651 ** get_address_of_moviePath_9() { return &___moviePath_9; }
	inline void set_moviePath_9(FsmString_t952858651 * value)
	{
		___moviePath_9 = value;
		Il2CppCodeGenWriteBarrier(&___moviePath_9, value);
	}

	inline static int32_t get_offset_of_fadeColor_10() { return static_cast<int32_t>(offsetof(DevicePlayFullScreenMovie_t2639184217, ___fadeColor_10)); }
	inline FsmColor_t2131419205 * get_fadeColor_10() const { return ___fadeColor_10; }
	inline FsmColor_t2131419205 ** get_address_of_fadeColor_10() { return &___fadeColor_10; }
	inline void set_fadeColor_10(FsmColor_t2131419205 * value)
	{
		___fadeColor_10 = value;
		Il2CppCodeGenWriteBarrier(&___fadeColor_10, value);
	}

	inline static int32_t get_offset_of_movieControlMode_11() { return static_cast<int32_t>(offsetof(DevicePlayFullScreenMovie_t2639184217, ___movieControlMode_11)); }
	inline int32_t get_movieControlMode_11() const { return ___movieControlMode_11; }
	inline int32_t* get_address_of_movieControlMode_11() { return &___movieControlMode_11; }
	inline void set_movieControlMode_11(int32_t value)
	{
		___movieControlMode_11 = value;
	}

	inline static int32_t get_offset_of_movieScalingMode_12() { return static_cast<int32_t>(offsetof(DevicePlayFullScreenMovie_t2639184217, ___movieScalingMode_12)); }
	inline int32_t get_movieScalingMode_12() const { return ___movieScalingMode_12; }
	inline int32_t* get_address_of_movieScalingMode_12() { return &___movieScalingMode_12; }
	inline void set_movieScalingMode_12(int32_t value)
	{
		___movieScalingMode_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
