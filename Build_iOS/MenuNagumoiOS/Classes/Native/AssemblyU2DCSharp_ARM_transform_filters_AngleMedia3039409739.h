﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.utils.PoolQuaternion
struct PoolQuaternion_t2988316782;

#include "AssemblyU2DCSharp_ARM_transform_filters_abstracts_4059889395.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.transform.filters.AngleMedianFilter
struct  AngleMedianFilter_t3039409739  : public HistoryAngleFilterAbstract_t4059889395
{
public:
	// System.Int32 ARM.transform.filters.AngleMedianFilter::maxSizeCache
	int32_t ___maxSizeCache_15;
	// System.Int32 ARM.transform.filters.AngleMedianFilter::_intialMaxSizeCache
	int32_t ____intialMaxSizeCache_16;
	// ARM.utils.PoolQuaternion ARM.transform.filters.AngleMedianFilter::_history
	PoolQuaternion_t2988316782 * ____history_17;
	// System.Boolean ARM.transform.filters.AngleMedianFilter::_hasItem
	bool ____hasItem_18;
	// System.Boolean ARM.transform.filters.AngleMedianFilter::_loaded
	bool ____loaded_19;
	// System.Boolean ARM.transform.filters.AngleMedianFilter::isFull
	bool ___isFull_20;

public:
	inline static int32_t get_offset_of_maxSizeCache_15() { return static_cast<int32_t>(offsetof(AngleMedianFilter_t3039409739, ___maxSizeCache_15)); }
	inline int32_t get_maxSizeCache_15() const { return ___maxSizeCache_15; }
	inline int32_t* get_address_of_maxSizeCache_15() { return &___maxSizeCache_15; }
	inline void set_maxSizeCache_15(int32_t value)
	{
		___maxSizeCache_15 = value;
	}

	inline static int32_t get_offset_of__intialMaxSizeCache_16() { return static_cast<int32_t>(offsetof(AngleMedianFilter_t3039409739, ____intialMaxSizeCache_16)); }
	inline int32_t get__intialMaxSizeCache_16() const { return ____intialMaxSizeCache_16; }
	inline int32_t* get_address_of__intialMaxSizeCache_16() { return &____intialMaxSizeCache_16; }
	inline void set__intialMaxSizeCache_16(int32_t value)
	{
		____intialMaxSizeCache_16 = value;
	}

	inline static int32_t get_offset_of__history_17() { return static_cast<int32_t>(offsetof(AngleMedianFilter_t3039409739, ____history_17)); }
	inline PoolQuaternion_t2988316782 * get__history_17() const { return ____history_17; }
	inline PoolQuaternion_t2988316782 ** get_address_of__history_17() { return &____history_17; }
	inline void set__history_17(PoolQuaternion_t2988316782 * value)
	{
		____history_17 = value;
		Il2CppCodeGenWriteBarrier(&____history_17, value);
	}

	inline static int32_t get_offset_of__hasItem_18() { return static_cast<int32_t>(offsetof(AngleMedianFilter_t3039409739, ____hasItem_18)); }
	inline bool get__hasItem_18() const { return ____hasItem_18; }
	inline bool* get_address_of__hasItem_18() { return &____hasItem_18; }
	inline void set__hasItem_18(bool value)
	{
		____hasItem_18 = value;
	}

	inline static int32_t get_offset_of__loaded_19() { return static_cast<int32_t>(offsetof(AngleMedianFilter_t3039409739, ____loaded_19)); }
	inline bool get__loaded_19() const { return ____loaded_19; }
	inline bool* get_address_of__loaded_19() { return &____loaded_19; }
	inline void set__loaded_19(bool value)
	{
		____loaded_19 = value;
	}

	inline static int32_t get_offset_of_isFull_20() { return static_cast<int32_t>(offsetof(AngleMedianFilter_t3039409739, ___isFull_20)); }
	inline bool get_isFull_20() const { return ___isFull_20; }
	inline bool* get_address_of_isFull_20() { return &___isFull_20; }
	inline void set_isFull_20(bool value)
	{
		___isFull_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
