﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// PlayMakerAnimatorMoveProxy
struct PlayMakerAnimatorMoveProxy_t4175490694;
// UnityEngine.Animator
struct Animator_t2776330603;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag
struct  GetAnimatorCurrentStateInfoIsTag_t2329710485  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::layerIndex
	FsmInt_t1596138449 * ___layerIndex_10;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::tag
	FsmString_t952858651 * ___tag_11;
	// System.Boolean HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::everyFrame
	bool ___everyFrame_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::tagMatch
	FsmBool_t1075959796 * ___tagMatch_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::tagMatchEvent
	FsmEvent_t2133468028 * ___tagMatchEvent_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::tagDoNotMatchEvent
	FsmEvent_t2133468028 * ___tagDoNotMatchEvent_15;
	// PlayMakerAnimatorMoveProxy HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::_animatorProxy
	PlayMakerAnimatorMoveProxy_t4175490694 * ____animatorProxy_16;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::_animator
	Animator_t2776330603 * ____animator_17;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfoIsTag_t2329710485, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_layerIndex_10() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfoIsTag_t2329710485, ___layerIndex_10)); }
	inline FsmInt_t1596138449 * get_layerIndex_10() const { return ___layerIndex_10; }
	inline FsmInt_t1596138449 ** get_address_of_layerIndex_10() { return &___layerIndex_10; }
	inline void set_layerIndex_10(FsmInt_t1596138449 * value)
	{
		___layerIndex_10 = value;
		Il2CppCodeGenWriteBarrier(&___layerIndex_10, value);
	}

	inline static int32_t get_offset_of_tag_11() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfoIsTag_t2329710485, ___tag_11)); }
	inline FsmString_t952858651 * get_tag_11() const { return ___tag_11; }
	inline FsmString_t952858651 ** get_address_of_tag_11() { return &___tag_11; }
	inline void set_tag_11(FsmString_t952858651 * value)
	{
		___tag_11 = value;
		Il2CppCodeGenWriteBarrier(&___tag_11, value);
	}

	inline static int32_t get_offset_of_everyFrame_12() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfoIsTag_t2329710485, ___everyFrame_12)); }
	inline bool get_everyFrame_12() const { return ___everyFrame_12; }
	inline bool* get_address_of_everyFrame_12() { return &___everyFrame_12; }
	inline void set_everyFrame_12(bool value)
	{
		___everyFrame_12 = value;
	}

	inline static int32_t get_offset_of_tagMatch_13() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfoIsTag_t2329710485, ___tagMatch_13)); }
	inline FsmBool_t1075959796 * get_tagMatch_13() const { return ___tagMatch_13; }
	inline FsmBool_t1075959796 ** get_address_of_tagMatch_13() { return &___tagMatch_13; }
	inline void set_tagMatch_13(FsmBool_t1075959796 * value)
	{
		___tagMatch_13 = value;
		Il2CppCodeGenWriteBarrier(&___tagMatch_13, value);
	}

	inline static int32_t get_offset_of_tagMatchEvent_14() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfoIsTag_t2329710485, ___tagMatchEvent_14)); }
	inline FsmEvent_t2133468028 * get_tagMatchEvent_14() const { return ___tagMatchEvent_14; }
	inline FsmEvent_t2133468028 ** get_address_of_tagMatchEvent_14() { return &___tagMatchEvent_14; }
	inline void set_tagMatchEvent_14(FsmEvent_t2133468028 * value)
	{
		___tagMatchEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___tagMatchEvent_14, value);
	}

	inline static int32_t get_offset_of_tagDoNotMatchEvent_15() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfoIsTag_t2329710485, ___tagDoNotMatchEvent_15)); }
	inline FsmEvent_t2133468028 * get_tagDoNotMatchEvent_15() const { return ___tagDoNotMatchEvent_15; }
	inline FsmEvent_t2133468028 ** get_address_of_tagDoNotMatchEvent_15() { return &___tagDoNotMatchEvent_15; }
	inline void set_tagDoNotMatchEvent_15(FsmEvent_t2133468028 * value)
	{
		___tagDoNotMatchEvent_15 = value;
		Il2CppCodeGenWriteBarrier(&___tagDoNotMatchEvent_15, value);
	}

	inline static int32_t get_offset_of__animatorProxy_16() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfoIsTag_t2329710485, ____animatorProxy_16)); }
	inline PlayMakerAnimatorMoveProxy_t4175490694 * get__animatorProxy_16() const { return ____animatorProxy_16; }
	inline PlayMakerAnimatorMoveProxy_t4175490694 ** get_address_of__animatorProxy_16() { return &____animatorProxy_16; }
	inline void set__animatorProxy_16(PlayMakerAnimatorMoveProxy_t4175490694 * value)
	{
		____animatorProxy_16 = value;
		Il2CppCodeGenWriteBarrier(&____animatorProxy_16, value);
	}

	inline static int32_t get_offset_of__animator_17() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfoIsTag_t2329710485, ____animator_17)); }
	inline Animator_t2776330603 * get__animator_17() const { return ____animator_17; }
	inline Animator_t2776330603 ** get_address_of__animator_17() { return &____animator_17; }
	inline void set__animator_17(Animator_t2776330603 * value)
	{
		____animator_17 = value;
		Il2CppCodeGenWriteBarrier(&____animator_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
