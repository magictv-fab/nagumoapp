﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.transform.filters.LockMoveFilter
struct LockMoveFilter_t1293691288;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void ARM.transform.filters.LockMoveFilter::.ctor()
extern "C"  void LockMoveFilter__ctor_m3420177952 (LockMoveFilter_t1293691288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion ARM.transform.filters.LockMoveFilter::get__lastAngle()
extern "C"  Quaternion_t1553702882  LockMoveFilter_get__lastAngle_m1078166899 (LockMoveFilter_t1293691288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.LockMoveFilter::set__lastAngle(UnityEngine.Quaternion)
extern "C"  void LockMoveFilter_set__lastAngle_m3039843690 (LockMoveFilter_t1293691288 * __this, Quaternion_t1553702882  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.transform.filters.LockMoveFilter::get__lastPosition()
extern "C"  Vector3_t4282066566  LockMoveFilter_get__lastPosition_m3394053369 (LockMoveFilter_t1293691288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.LockMoveFilter::set__lastPosition(UnityEngine.Vector3)
extern "C"  void LockMoveFilter_set__lastPosition_m425305914 (LockMoveFilter_t1293691288 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.LockMoveFilter::_init()
extern "C"  void LockMoveFilter__init_m4124242317 (LockMoveFilter_t1293691288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.LockMoveFilter::_On()
extern "C"  void LockMoveFilter__On_m2725299644 (LockMoveFilter_t1293691288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.LockMoveFilter::_Off()
extern "C"  void LockMoveFilter__Off_m2879731604 (LockMoveFilter_t1293691288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion ARM.transform.filters.LockMoveFilter::filter(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  LockMoveFilter_filter_m1616034146 (LockMoveFilter_t1293691288 * __this, Quaternion_t1553702882  ___currentValue0, Quaternion_t1553702882  ___nextValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.transform.filters.LockMoveFilter::filter(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  LockMoveFilter_filter_m2880507510 (LockMoveFilter_t1293691288 * __this, Vector3_t4282066566  ___currentValue0, Vector3_t4282066566  ___nextValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.transform.filters.LockMoveFilter::_doFilter(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  LockMoveFilter__doFilter_m153615558 (LockMoveFilter_t1293691288 * __this, Vector3_t4282066566  ___last0, Vector3_t4282066566  ___nextValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion ARM.transform.filters.LockMoveFilter::_doFilter(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  LockMoveFilter__doFilter_m3994839822 (LockMoveFilter_t1293691288 * __this, Quaternion_t1553702882  ___last0, Quaternion_t1553702882  ___nextValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
