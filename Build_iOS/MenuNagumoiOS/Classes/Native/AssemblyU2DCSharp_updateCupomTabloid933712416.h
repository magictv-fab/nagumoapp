﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t538875265;
// UnityEngine.Sprite
struct Sprite_t3199167241;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// updateCupomTabloid
struct  updateCupomTabloid_t933712416  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.Image updateCupomTabloid::container
	Image_t538875265 * ___container_2;
	// UnityEngine.Sprite updateCupomTabloid::cupom10
	Sprite_t3199167241 * ___cupom10_3;
	// UnityEngine.Sprite updateCupomTabloid::cupom50
	Sprite_t3199167241 * ___cupom50_4;
	// UnityEngine.Sprite updateCupomTabloid::cupom100
	Sprite_t3199167241 * ___cupom100_5;
	// UnityEngine.Sprite updateCupomTabloid::cupom200
	Sprite_t3199167241 * ___cupom200_6;
	// UnityEngine.Sprite updateCupomTabloid::cupom500
	Sprite_t3199167241 * ___cupom500_7;
	// System.Int32 updateCupomTabloid::cupomValue
	int32_t ___cupomValue_8;

public:
	inline static int32_t get_offset_of_container_2() { return static_cast<int32_t>(offsetof(updateCupomTabloid_t933712416, ___container_2)); }
	inline Image_t538875265 * get_container_2() const { return ___container_2; }
	inline Image_t538875265 ** get_address_of_container_2() { return &___container_2; }
	inline void set_container_2(Image_t538875265 * value)
	{
		___container_2 = value;
		Il2CppCodeGenWriteBarrier(&___container_2, value);
	}

	inline static int32_t get_offset_of_cupom10_3() { return static_cast<int32_t>(offsetof(updateCupomTabloid_t933712416, ___cupom10_3)); }
	inline Sprite_t3199167241 * get_cupom10_3() const { return ___cupom10_3; }
	inline Sprite_t3199167241 ** get_address_of_cupom10_3() { return &___cupom10_3; }
	inline void set_cupom10_3(Sprite_t3199167241 * value)
	{
		___cupom10_3 = value;
		Il2CppCodeGenWriteBarrier(&___cupom10_3, value);
	}

	inline static int32_t get_offset_of_cupom50_4() { return static_cast<int32_t>(offsetof(updateCupomTabloid_t933712416, ___cupom50_4)); }
	inline Sprite_t3199167241 * get_cupom50_4() const { return ___cupom50_4; }
	inline Sprite_t3199167241 ** get_address_of_cupom50_4() { return &___cupom50_4; }
	inline void set_cupom50_4(Sprite_t3199167241 * value)
	{
		___cupom50_4 = value;
		Il2CppCodeGenWriteBarrier(&___cupom50_4, value);
	}

	inline static int32_t get_offset_of_cupom100_5() { return static_cast<int32_t>(offsetof(updateCupomTabloid_t933712416, ___cupom100_5)); }
	inline Sprite_t3199167241 * get_cupom100_5() const { return ___cupom100_5; }
	inline Sprite_t3199167241 ** get_address_of_cupom100_5() { return &___cupom100_5; }
	inline void set_cupom100_5(Sprite_t3199167241 * value)
	{
		___cupom100_5 = value;
		Il2CppCodeGenWriteBarrier(&___cupom100_5, value);
	}

	inline static int32_t get_offset_of_cupom200_6() { return static_cast<int32_t>(offsetof(updateCupomTabloid_t933712416, ___cupom200_6)); }
	inline Sprite_t3199167241 * get_cupom200_6() const { return ___cupom200_6; }
	inline Sprite_t3199167241 ** get_address_of_cupom200_6() { return &___cupom200_6; }
	inline void set_cupom200_6(Sprite_t3199167241 * value)
	{
		___cupom200_6 = value;
		Il2CppCodeGenWriteBarrier(&___cupom200_6, value);
	}

	inline static int32_t get_offset_of_cupom500_7() { return static_cast<int32_t>(offsetof(updateCupomTabloid_t933712416, ___cupom500_7)); }
	inline Sprite_t3199167241 * get_cupom500_7() const { return ___cupom500_7; }
	inline Sprite_t3199167241 ** get_address_of_cupom500_7() { return &___cupom500_7; }
	inline void set_cupom500_7(Sprite_t3199167241 * value)
	{
		___cupom500_7 = value;
		Il2CppCodeGenWriteBarrier(&___cupom500_7, value);
	}

	inline static int32_t get_offset_of_cupomValue_8() { return static_cast<int32_t>(offsetof(updateCupomTabloid_t933712416, ___cupomValue_8)); }
	inline int32_t get_cupomValue_8() const { return ___cupomValue_8; }
	inline int32_t* get_address_of_cupomValue_8() { return &___cupomValue_8; }
	inline void set_cupomValue_8(int32_t value)
	{
		___cupomValue_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
