﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TabloideManager/<LoadLoadeds>c__Iterator82
struct U3CLoadLoadedsU3Ec__Iterator82_t3625498093;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void TabloideManager/<LoadLoadeds>c__Iterator82::.ctor()
extern "C"  void U3CLoadLoadedsU3Ec__Iterator82__ctor_m4154842446 (U3CLoadLoadedsU3Ec__Iterator82_t3625498093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TabloideManager/<LoadLoadeds>c__Iterator82::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadLoadedsU3Ec__Iterator82_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2291974926 (U3CLoadLoadedsU3Ec__Iterator82_t3625498093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TabloideManager/<LoadLoadeds>c__Iterator82::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadLoadedsU3Ec__Iterator82_System_Collections_IEnumerator_get_Current_m3684868770 (U3CLoadLoadedsU3Ec__Iterator82_t3625498093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TabloideManager/<LoadLoadeds>c__Iterator82::MoveNext()
extern "C"  bool U3CLoadLoadedsU3Ec__Iterator82_MoveNext_m3477376206 (U3CLoadLoadedsU3Ec__Iterator82_t3625498093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TabloideManager/<LoadLoadeds>c__Iterator82::Dispose()
extern "C"  void U3CLoadLoadedsU3Ec__Iterator82_Dispose_m2677255691 (U3CLoadLoadedsU3Ec__Iterator82_t3625498093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TabloideManager/<LoadLoadeds>c__Iterator82::Reset()
extern "C"  void U3CLoadLoadedsU3Ec__Iterator82_Reset_m1801275387 (U3CLoadLoadedsU3Ec__Iterator82_t3625498093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
