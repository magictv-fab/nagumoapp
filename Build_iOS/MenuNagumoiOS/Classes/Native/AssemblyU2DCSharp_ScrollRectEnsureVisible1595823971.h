﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.RectTransform
struct RectTransform_t972643934;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t3606982749;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2662109048;
// UnityEngine.UI.Extensions.ScrollSnapBase
struct ScrollSnapBase_t1686927404;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScrollRectEnsureVisible
struct  ScrollRectEnsureVisible_t1595823971  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.RectTransform ScrollRectEnsureVisible::maskTransform
	RectTransform_t972643934 * ___maskTransform_2;
	// UnityEngine.UI.ScrollRect ScrollRectEnsureVisible::mScrollRect
	ScrollRect_t3606982749 * ___mScrollRect_3;
	// UnityEngine.RectTransform ScrollRectEnsureVisible::mScrollTransform
	RectTransform_t972643934 * ___mScrollTransform_4;
	// UnityEngine.RectTransform ScrollRectEnsureVisible::mContent
	RectTransform_t972643934 * ___mContent_5;
	// UnityEngine.GameObject[] ScrollRectEnsureVisible::scrollTransformItem
	GameObjectU5BU5D_t2662109048* ___scrollTransformItem_6;
	// UnityEngine.UI.Extensions.ScrollSnapBase ScrollRectEnsureVisible::_horizontalSnapScript
	ScrollSnapBase_t1686927404 * ____horizontalSnapScript_7;

public:
	inline static int32_t get_offset_of_maskTransform_2() { return static_cast<int32_t>(offsetof(ScrollRectEnsureVisible_t1595823971, ___maskTransform_2)); }
	inline RectTransform_t972643934 * get_maskTransform_2() const { return ___maskTransform_2; }
	inline RectTransform_t972643934 ** get_address_of_maskTransform_2() { return &___maskTransform_2; }
	inline void set_maskTransform_2(RectTransform_t972643934 * value)
	{
		___maskTransform_2 = value;
		Il2CppCodeGenWriteBarrier(&___maskTransform_2, value);
	}

	inline static int32_t get_offset_of_mScrollRect_3() { return static_cast<int32_t>(offsetof(ScrollRectEnsureVisible_t1595823971, ___mScrollRect_3)); }
	inline ScrollRect_t3606982749 * get_mScrollRect_3() const { return ___mScrollRect_3; }
	inline ScrollRect_t3606982749 ** get_address_of_mScrollRect_3() { return &___mScrollRect_3; }
	inline void set_mScrollRect_3(ScrollRect_t3606982749 * value)
	{
		___mScrollRect_3 = value;
		Il2CppCodeGenWriteBarrier(&___mScrollRect_3, value);
	}

	inline static int32_t get_offset_of_mScrollTransform_4() { return static_cast<int32_t>(offsetof(ScrollRectEnsureVisible_t1595823971, ___mScrollTransform_4)); }
	inline RectTransform_t972643934 * get_mScrollTransform_4() const { return ___mScrollTransform_4; }
	inline RectTransform_t972643934 ** get_address_of_mScrollTransform_4() { return &___mScrollTransform_4; }
	inline void set_mScrollTransform_4(RectTransform_t972643934 * value)
	{
		___mScrollTransform_4 = value;
		Il2CppCodeGenWriteBarrier(&___mScrollTransform_4, value);
	}

	inline static int32_t get_offset_of_mContent_5() { return static_cast<int32_t>(offsetof(ScrollRectEnsureVisible_t1595823971, ___mContent_5)); }
	inline RectTransform_t972643934 * get_mContent_5() const { return ___mContent_5; }
	inline RectTransform_t972643934 ** get_address_of_mContent_5() { return &___mContent_5; }
	inline void set_mContent_5(RectTransform_t972643934 * value)
	{
		___mContent_5 = value;
		Il2CppCodeGenWriteBarrier(&___mContent_5, value);
	}

	inline static int32_t get_offset_of_scrollTransformItem_6() { return static_cast<int32_t>(offsetof(ScrollRectEnsureVisible_t1595823971, ___scrollTransformItem_6)); }
	inline GameObjectU5BU5D_t2662109048* get_scrollTransformItem_6() const { return ___scrollTransformItem_6; }
	inline GameObjectU5BU5D_t2662109048** get_address_of_scrollTransformItem_6() { return &___scrollTransformItem_6; }
	inline void set_scrollTransformItem_6(GameObjectU5BU5D_t2662109048* value)
	{
		___scrollTransformItem_6 = value;
		Il2CppCodeGenWriteBarrier(&___scrollTransformItem_6, value);
	}

	inline static int32_t get_offset_of__horizontalSnapScript_7() { return static_cast<int32_t>(offsetof(ScrollRectEnsureVisible_t1595823971, ____horizontalSnapScript_7)); }
	inline ScrollSnapBase_t1686927404 * get__horizontalSnapScript_7() const { return ____horizontalSnapScript_7; }
	inline ScrollSnapBase_t1686927404 ** get_address_of__horizontalSnapScript_7() { return &____horizontalSnapScript_7; }
	inline void set__horizontalSnapScript_7(ScrollSnapBase_t1686927404 * value)
	{
		____horizontalSnapScript_7 = value;
		Il2CppCodeGenWriteBarrier(&____horizontalSnapScript_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
