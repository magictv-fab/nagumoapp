﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t696267445;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSNotificationPayload
struct  OSNotificationPayload_t1451568543  : public Il2CppObject
{
public:
	// System.String OSNotificationPayload::notificationID
	String_t* ___notificationID_0;
	// System.String OSNotificationPayload::sound
	String_t* ___sound_1;
	// System.String OSNotificationPayload::title
	String_t* ___title_2;
	// System.String OSNotificationPayload::body
	String_t* ___body_3;
	// System.String OSNotificationPayload::subtitle
	String_t* ___subtitle_4;
	// System.String OSNotificationPayload::launchURL
	String_t* ___launchURL_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> OSNotificationPayload::additionalData
	Dictionary_2_t696267445 * ___additionalData_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> OSNotificationPayload::actionButtons
	Dictionary_2_t696267445 * ___actionButtons_7;
	// System.Boolean OSNotificationPayload::contentAvailable
	bool ___contentAvailable_8;
	// System.Int32 OSNotificationPayload::badge
	int32_t ___badge_9;
	// System.String OSNotificationPayload::smallIcon
	String_t* ___smallIcon_10;
	// System.String OSNotificationPayload::largeIcon
	String_t* ___largeIcon_11;
	// System.String OSNotificationPayload::bigPicture
	String_t* ___bigPicture_12;
	// System.String OSNotificationPayload::smallIconAccentColor
	String_t* ___smallIconAccentColor_13;
	// System.String OSNotificationPayload::ledColor
	String_t* ___ledColor_14;
	// System.Int32 OSNotificationPayload::lockScreenVisibility
	int32_t ___lockScreenVisibility_15;
	// System.String OSNotificationPayload::groupKey
	String_t* ___groupKey_16;
	// System.String OSNotificationPayload::groupMessage
	String_t* ___groupMessage_17;
	// System.String OSNotificationPayload::fromProjectNumber
	String_t* ___fromProjectNumber_18;

public:
	inline static int32_t get_offset_of_notificationID_0() { return static_cast<int32_t>(offsetof(OSNotificationPayload_t1451568543, ___notificationID_0)); }
	inline String_t* get_notificationID_0() const { return ___notificationID_0; }
	inline String_t** get_address_of_notificationID_0() { return &___notificationID_0; }
	inline void set_notificationID_0(String_t* value)
	{
		___notificationID_0 = value;
		Il2CppCodeGenWriteBarrier(&___notificationID_0, value);
	}

	inline static int32_t get_offset_of_sound_1() { return static_cast<int32_t>(offsetof(OSNotificationPayload_t1451568543, ___sound_1)); }
	inline String_t* get_sound_1() const { return ___sound_1; }
	inline String_t** get_address_of_sound_1() { return &___sound_1; }
	inline void set_sound_1(String_t* value)
	{
		___sound_1 = value;
		Il2CppCodeGenWriteBarrier(&___sound_1, value);
	}

	inline static int32_t get_offset_of_title_2() { return static_cast<int32_t>(offsetof(OSNotificationPayload_t1451568543, ___title_2)); }
	inline String_t* get_title_2() const { return ___title_2; }
	inline String_t** get_address_of_title_2() { return &___title_2; }
	inline void set_title_2(String_t* value)
	{
		___title_2 = value;
		Il2CppCodeGenWriteBarrier(&___title_2, value);
	}

	inline static int32_t get_offset_of_body_3() { return static_cast<int32_t>(offsetof(OSNotificationPayload_t1451568543, ___body_3)); }
	inline String_t* get_body_3() const { return ___body_3; }
	inline String_t** get_address_of_body_3() { return &___body_3; }
	inline void set_body_3(String_t* value)
	{
		___body_3 = value;
		Il2CppCodeGenWriteBarrier(&___body_3, value);
	}

	inline static int32_t get_offset_of_subtitle_4() { return static_cast<int32_t>(offsetof(OSNotificationPayload_t1451568543, ___subtitle_4)); }
	inline String_t* get_subtitle_4() const { return ___subtitle_4; }
	inline String_t** get_address_of_subtitle_4() { return &___subtitle_4; }
	inline void set_subtitle_4(String_t* value)
	{
		___subtitle_4 = value;
		Il2CppCodeGenWriteBarrier(&___subtitle_4, value);
	}

	inline static int32_t get_offset_of_launchURL_5() { return static_cast<int32_t>(offsetof(OSNotificationPayload_t1451568543, ___launchURL_5)); }
	inline String_t* get_launchURL_5() const { return ___launchURL_5; }
	inline String_t** get_address_of_launchURL_5() { return &___launchURL_5; }
	inline void set_launchURL_5(String_t* value)
	{
		___launchURL_5 = value;
		Il2CppCodeGenWriteBarrier(&___launchURL_5, value);
	}

	inline static int32_t get_offset_of_additionalData_6() { return static_cast<int32_t>(offsetof(OSNotificationPayload_t1451568543, ___additionalData_6)); }
	inline Dictionary_2_t696267445 * get_additionalData_6() const { return ___additionalData_6; }
	inline Dictionary_2_t696267445 ** get_address_of_additionalData_6() { return &___additionalData_6; }
	inline void set_additionalData_6(Dictionary_2_t696267445 * value)
	{
		___additionalData_6 = value;
		Il2CppCodeGenWriteBarrier(&___additionalData_6, value);
	}

	inline static int32_t get_offset_of_actionButtons_7() { return static_cast<int32_t>(offsetof(OSNotificationPayload_t1451568543, ___actionButtons_7)); }
	inline Dictionary_2_t696267445 * get_actionButtons_7() const { return ___actionButtons_7; }
	inline Dictionary_2_t696267445 ** get_address_of_actionButtons_7() { return &___actionButtons_7; }
	inline void set_actionButtons_7(Dictionary_2_t696267445 * value)
	{
		___actionButtons_7 = value;
		Il2CppCodeGenWriteBarrier(&___actionButtons_7, value);
	}

	inline static int32_t get_offset_of_contentAvailable_8() { return static_cast<int32_t>(offsetof(OSNotificationPayload_t1451568543, ___contentAvailable_8)); }
	inline bool get_contentAvailable_8() const { return ___contentAvailable_8; }
	inline bool* get_address_of_contentAvailable_8() { return &___contentAvailable_8; }
	inline void set_contentAvailable_8(bool value)
	{
		___contentAvailable_8 = value;
	}

	inline static int32_t get_offset_of_badge_9() { return static_cast<int32_t>(offsetof(OSNotificationPayload_t1451568543, ___badge_9)); }
	inline int32_t get_badge_9() const { return ___badge_9; }
	inline int32_t* get_address_of_badge_9() { return &___badge_9; }
	inline void set_badge_9(int32_t value)
	{
		___badge_9 = value;
	}

	inline static int32_t get_offset_of_smallIcon_10() { return static_cast<int32_t>(offsetof(OSNotificationPayload_t1451568543, ___smallIcon_10)); }
	inline String_t* get_smallIcon_10() const { return ___smallIcon_10; }
	inline String_t** get_address_of_smallIcon_10() { return &___smallIcon_10; }
	inline void set_smallIcon_10(String_t* value)
	{
		___smallIcon_10 = value;
		Il2CppCodeGenWriteBarrier(&___smallIcon_10, value);
	}

	inline static int32_t get_offset_of_largeIcon_11() { return static_cast<int32_t>(offsetof(OSNotificationPayload_t1451568543, ___largeIcon_11)); }
	inline String_t* get_largeIcon_11() const { return ___largeIcon_11; }
	inline String_t** get_address_of_largeIcon_11() { return &___largeIcon_11; }
	inline void set_largeIcon_11(String_t* value)
	{
		___largeIcon_11 = value;
		Il2CppCodeGenWriteBarrier(&___largeIcon_11, value);
	}

	inline static int32_t get_offset_of_bigPicture_12() { return static_cast<int32_t>(offsetof(OSNotificationPayload_t1451568543, ___bigPicture_12)); }
	inline String_t* get_bigPicture_12() const { return ___bigPicture_12; }
	inline String_t** get_address_of_bigPicture_12() { return &___bigPicture_12; }
	inline void set_bigPicture_12(String_t* value)
	{
		___bigPicture_12 = value;
		Il2CppCodeGenWriteBarrier(&___bigPicture_12, value);
	}

	inline static int32_t get_offset_of_smallIconAccentColor_13() { return static_cast<int32_t>(offsetof(OSNotificationPayload_t1451568543, ___smallIconAccentColor_13)); }
	inline String_t* get_smallIconAccentColor_13() const { return ___smallIconAccentColor_13; }
	inline String_t** get_address_of_smallIconAccentColor_13() { return &___smallIconAccentColor_13; }
	inline void set_smallIconAccentColor_13(String_t* value)
	{
		___smallIconAccentColor_13 = value;
		Il2CppCodeGenWriteBarrier(&___smallIconAccentColor_13, value);
	}

	inline static int32_t get_offset_of_ledColor_14() { return static_cast<int32_t>(offsetof(OSNotificationPayload_t1451568543, ___ledColor_14)); }
	inline String_t* get_ledColor_14() const { return ___ledColor_14; }
	inline String_t** get_address_of_ledColor_14() { return &___ledColor_14; }
	inline void set_ledColor_14(String_t* value)
	{
		___ledColor_14 = value;
		Il2CppCodeGenWriteBarrier(&___ledColor_14, value);
	}

	inline static int32_t get_offset_of_lockScreenVisibility_15() { return static_cast<int32_t>(offsetof(OSNotificationPayload_t1451568543, ___lockScreenVisibility_15)); }
	inline int32_t get_lockScreenVisibility_15() const { return ___lockScreenVisibility_15; }
	inline int32_t* get_address_of_lockScreenVisibility_15() { return &___lockScreenVisibility_15; }
	inline void set_lockScreenVisibility_15(int32_t value)
	{
		___lockScreenVisibility_15 = value;
	}

	inline static int32_t get_offset_of_groupKey_16() { return static_cast<int32_t>(offsetof(OSNotificationPayload_t1451568543, ___groupKey_16)); }
	inline String_t* get_groupKey_16() const { return ___groupKey_16; }
	inline String_t** get_address_of_groupKey_16() { return &___groupKey_16; }
	inline void set_groupKey_16(String_t* value)
	{
		___groupKey_16 = value;
		Il2CppCodeGenWriteBarrier(&___groupKey_16, value);
	}

	inline static int32_t get_offset_of_groupMessage_17() { return static_cast<int32_t>(offsetof(OSNotificationPayload_t1451568543, ___groupMessage_17)); }
	inline String_t* get_groupMessage_17() const { return ___groupMessage_17; }
	inline String_t** get_address_of_groupMessage_17() { return &___groupMessage_17; }
	inline void set_groupMessage_17(String_t* value)
	{
		___groupMessage_17 = value;
		Il2CppCodeGenWriteBarrier(&___groupMessage_17, value);
	}

	inline static int32_t get_offset_of_fromProjectNumber_18() { return static_cast<int32_t>(offsetof(OSNotificationPayload_t1451568543, ___fromProjectNumber_18)); }
	inline String_t* get_fromProjectNumber_18() const { return ___fromProjectNumber_18; }
	inline String_t** get_address_of_fromProjectNumber_18() { return &___fromProjectNumber_18; }
	inline void set_fromProjectNumber_18(String_t* value)
	{
		___fromProjectNumber_18 = value;
		Il2CppCodeGenWriteBarrier(&___fromProjectNumber_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
