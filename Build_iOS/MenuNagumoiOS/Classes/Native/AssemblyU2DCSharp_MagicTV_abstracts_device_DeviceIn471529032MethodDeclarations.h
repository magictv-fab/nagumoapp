﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.abstracts.device.DeviceInfoAbstract
struct DeviceInfoAbstract_t471529032;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.abstracts.device.DeviceInfoAbstract::.ctor()
extern "C"  void DeviceInfoAbstract__ctor_m2316496713 (DeviceInfoAbstract_t471529032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
