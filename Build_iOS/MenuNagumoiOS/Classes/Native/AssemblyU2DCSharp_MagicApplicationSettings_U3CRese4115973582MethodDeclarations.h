﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicApplicationSettings/<ResetHard>c__AnonStorey97
struct U3CResetHardU3Ec__AnonStorey97_t4115973582;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MagicApplicationSettings/<ResetHard>c__AnonStorey97::.ctor()
extern "C"  void U3CResetHardU3Ec__AnonStorey97__ctor_m1296717149 (U3CResetHardU3Ec__AnonStorey97_t4115973582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicApplicationSettings/<ResetHard>c__AnonStorey97::<>m__24(System.String)
extern "C"  bool U3CResetHardU3Ec__AnonStorey97_U3CU3Em__24_m900501030 (U3CResetHardU3Ec__AnonStorey97_t4115973582 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicApplicationSettings/<ResetHard>c__AnonStorey97::<>m__25(System.String)
extern "C"  bool U3CResetHardU3Ec__AnonStorey97_U3CU3Em__25_m389966853 (U3CResetHardU3Ec__AnonStorey97_t4115973582 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
