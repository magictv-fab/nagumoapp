﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// iTween/<TweenRestart>c__Iterator88
struct U3CTweenRestartU3Ec__Iterator88_t3643232966;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void iTween/<TweenRestart>c__Iterator88::.ctor()
extern "C"  void U3CTweenRestartU3Ec__Iterator88__ctor_m736443541 (U3CTweenRestartU3Ec__Iterator88_t3643232966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTween/<TweenRestart>c__Iterator88::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTweenRestartU3Ec__Iterator88_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3214436135 (U3CTweenRestartU3Ec__Iterator88_t3643232966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTween/<TweenRestart>c__Iterator88::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTweenRestartU3Ec__Iterator88_System_Collections_IEnumerator_get_Current_m380868795 (U3CTweenRestartU3Ec__Iterator88_t3643232966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTween/<TweenRestart>c__Iterator88::MoveNext()
extern "C"  bool U3CTweenRestartU3Ec__Iterator88_MoveNext_m500828199 (U3CTweenRestartU3Ec__Iterator88_t3643232966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween/<TweenRestart>c__Iterator88::Dispose()
extern "C"  void U3CTweenRestartU3Ec__Iterator88_Dispose_m3245889426 (U3CTweenRestartU3Ec__Iterator88_t3643232966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween/<TweenRestart>c__Iterator88::Reset()
extern "C"  void U3CTweenRestartU3Ec__Iterator88_Reset_m2677843778 (U3CTweenRestartU3Ec__Iterator88_t3643232966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
