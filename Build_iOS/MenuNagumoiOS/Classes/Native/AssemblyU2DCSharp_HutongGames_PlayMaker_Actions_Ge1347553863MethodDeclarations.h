﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorRightFootBottomHeight
struct GetAnimatorRightFootBottomHeight_t1347553863;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorRightFootBottomHeight::.ctor()
extern "C"  void GetAnimatorRightFootBottomHeight__ctor_m1414875135 (GetAnimatorRightFootBottomHeight_t1347553863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorRightFootBottomHeight::Reset()
extern "C"  void GetAnimatorRightFootBottomHeight_Reset_m3356275372 (GetAnimatorRightFootBottomHeight_t1347553863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorRightFootBottomHeight::OnEnter()
extern "C"  void GetAnimatorRightFootBottomHeight_OnEnter_m1877067798 (GetAnimatorRightFootBottomHeight_t1347553863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorRightFootBottomHeight::OnLateUpdate()
extern "C"  void GetAnimatorRightFootBottomHeight_OnLateUpdate_m4030590003 (GetAnimatorRightFootBottomHeight_t1347553863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorRightFootBottomHeight::_getRightFootBottonHeight()
extern "C"  void GetAnimatorRightFootBottomHeight__getRightFootBottonHeight_m2162975651 (GetAnimatorRightFootBottomHeight_t1347553863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
