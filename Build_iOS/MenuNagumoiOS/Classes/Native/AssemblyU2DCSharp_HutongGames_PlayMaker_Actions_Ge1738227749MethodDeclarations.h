﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAxis
struct GetAxis_t1738227749;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAxis::.ctor()
extern "C"  void GetAxis__ctor_m3688609969 (GetAxis_t1738227749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAxis::Reset()
extern "C"  void GetAxis_Reset_m1335042910 (GetAxis_t1738227749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAxis::OnEnter()
extern "C"  void GetAxis_OnEnter_m797889608 (GetAxis_t1738227749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAxis::OnUpdate()
extern "C"  void GetAxis_OnUpdate_m2393300731 (GetAxis_t1738227749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAxis::DoGetAxis()
extern "C"  void GetAxis_DoGetAxis_m1514384603 (GetAxis_t1738227749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
