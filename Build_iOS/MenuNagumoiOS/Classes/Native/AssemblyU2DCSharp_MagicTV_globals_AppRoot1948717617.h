﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// MagicTV.abstracts.device.DeviceInfoAbstract
struct DeviceInfoAbstract_t471529032;
// MagicTV.abstracts.device.DeviceCacheFileManagerAbstract
struct DeviceCacheFileManagerAbstract_t121423689;
// MagicTV.abstracts.device.DeviceFileInfoAbstract
struct DeviceFileInfoAbstract_t3788299492;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_MagicTV_globals_StateMachine367995870.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.globals.AppRoot
struct  AppRoot_t1948717617  : public Il2CppObject
{
public:
	// System.String MagicTV.globals.AppRoot::info
	String_t* ___info_5;

public:
	inline static int32_t get_offset_of_info_5() { return static_cast<int32_t>(offsetof(AppRoot_t1948717617, ___info_5)); }
	inline String_t* get_info_5() const { return ___info_5; }
	inline String_t** get_address_of_info_5() { return &___info_5; }
	inline void set_info_5(String_t* value)
	{
		___info_5 = value;
		Il2CppCodeGenWriteBarrier(&___info_5, value);
	}
};

struct AppRoot_t1948717617_StaticFields
{
public:
	// MagicTV.globals.StateMachine MagicTV.globals.AppRoot::_stateMachine
	int32_t ____stateMachine_6;
	// System.String MagicTV.globals.AppRoot::_device_id
	String_t* ____device_id_7;
	// System.String MagicTV.globals.AppRoot::connection
	String_t* ___connection_8;
	// System.Boolean MagicTV.globals.AppRoot::canTrack
	bool ___canTrack_9;
	// System.String MagicTV.globals.AppRoot::revision
	String_t* ___revision_10;
	// MagicTV.abstracts.device.DeviceInfoAbstract MagicTV.globals.AppRoot::deviceInfo
	DeviceInfoAbstract_t471529032 * ___deviceInfo_11;
	// System.String MagicTV.globals.AppRoot::user_agent
	String_t* ___user_agent_12;
	// MagicTV.abstracts.device.DeviceCacheFileManagerAbstract MagicTV.globals.AppRoot::cacheFileManager
	DeviceCacheFileManagerAbstract_t121423689 * ___cacheFileManager_13;
	// MagicTV.abstracts.device.DeviceFileInfoAbstract MagicTV.globals.AppRoot::deviceFileInfo
	DeviceFileInfoAbstract_t3788299492 * ___deviceFileInfo_14;

public:
	inline static int32_t get_offset_of__stateMachine_6() { return static_cast<int32_t>(offsetof(AppRoot_t1948717617_StaticFields, ____stateMachine_6)); }
	inline int32_t get__stateMachine_6() const { return ____stateMachine_6; }
	inline int32_t* get_address_of__stateMachine_6() { return &____stateMachine_6; }
	inline void set__stateMachine_6(int32_t value)
	{
		____stateMachine_6 = value;
	}

	inline static int32_t get_offset_of__device_id_7() { return static_cast<int32_t>(offsetof(AppRoot_t1948717617_StaticFields, ____device_id_7)); }
	inline String_t* get__device_id_7() const { return ____device_id_7; }
	inline String_t** get_address_of__device_id_7() { return &____device_id_7; }
	inline void set__device_id_7(String_t* value)
	{
		____device_id_7 = value;
		Il2CppCodeGenWriteBarrier(&____device_id_7, value);
	}

	inline static int32_t get_offset_of_connection_8() { return static_cast<int32_t>(offsetof(AppRoot_t1948717617_StaticFields, ___connection_8)); }
	inline String_t* get_connection_8() const { return ___connection_8; }
	inline String_t** get_address_of_connection_8() { return &___connection_8; }
	inline void set_connection_8(String_t* value)
	{
		___connection_8 = value;
		Il2CppCodeGenWriteBarrier(&___connection_8, value);
	}

	inline static int32_t get_offset_of_canTrack_9() { return static_cast<int32_t>(offsetof(AppRoot_t1948717617_StaticFields, ___canTrack_9)); }
	inline bool get_canTrack_9() const { return ___canTrack_9; }
	inline bool* get_address_of_canTrack_9() { return &___canTrack_9; }
	inline void set_canTrack_9(bool value)
	{
		___canTrack_9 = value;
	}

	inline static int32_t get_offset_of_revision_10() { return static_cast<int32_t>(offsetof(AppRoot_t1948717617_StaticFields, ___revision_10)); }
	inline String_t* get_revision_10() const { return ___revision_10; }
	inline String_t** get_address_of_revision_10() { return &___revision_10; }
	inline void set_revision_10(String_t* value)
	{
		___revision_10 = value;
		Il2CppCodeGenWriteBarrier(&___revision_10, value);
	}

	inline static int32_t get_offset_of_deviceInfo_11() { return static_cast<int32_t>(offsetof(AppRoot_t1948717617_StaticFields, ___deviceInfo_11)); }
	inline DeviceInfoAbstract_t471529032 * get_deviceInfo_11() const { return ___deviceInfo_11; }
	inline DeviceInfoAbstract_t471529032 ** get_address_of_deviceInfo_11() { return &___deviceInfo_11; }
	inline void set_deviceInfo_11(DeviceInfoAbstract_t471529032 * value)
	{
		___deviceInfo_11 = value;
		Il2CppCodeGenWriteBarrier(&___deviceInfo_11, value);
	}

	inline static int32_t get_offset_of_user_agent_12() { return static_cast<int32_t>(offsetof(AppRoot_t1948717617_StaticFields, ___user_agent_12)); }
	inline String_t* get_user_agent_12() const { return ___user_agent_12; }
	inline String_t** get_address_of_user_agent_12() { return &___user_agent_12; }
	inline void set_user_agent_12(String_t* value)
	{
		___user_agent_12 = value;
		Il2CppCodeGenWriteBarrier(&___user_agent_12, value);
	}

	inline static int32_t get_offset_of_cacheFileManager_13() { return static_cast<int32_t>(offsetof(AppRoot_t1948717617_StaticFields, ___cacheFileManager_13)); }
	inline DeviceCacheFileManagerAbstract_t121423689 * get_cacheFileManager_13() const { return ___cacheFileManager_13; }
	inline DeviceCacheFileManagerAbstract_t121423689 ** get_address_of_cacheFileManager_13() { return &___cacheFileManager_13; }
	inline void set_cacheFileManager_13(DeviceCacheFileManagerAbstract_t121423689 * value)
	{
		___cacheFileManager_13 = value;
		Il2CppCodeGenWriteBarrier(&___cacheFileManager_13, value);
	}

	inline static int32_t get_offset_of_deviceFileInfo_14() { return static_cast<int32_t>(offsetof(AppRoot_t1948717617_StaticFields, ___deviceFileInfo_14)); }
	inline DeviceFileInfoAbstract_t3788299492 * get_deviceFileInfo_14() const { return ___deviceFileInfo_14; }
	inline DeviceFileInfoAbstract_t3788299492 ** get_address_of_deviceFileInfo_14() { return &___deviceFileInfo_14; }
	inline void set_deviceFileInfo_14(DeviceFileInfoAbstract_t3788299492 * value)
	{
		___deviceFileInfo_14 = value;
		Il2CppCodeGenWriteBarrier(&___deviceFileInfo_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
