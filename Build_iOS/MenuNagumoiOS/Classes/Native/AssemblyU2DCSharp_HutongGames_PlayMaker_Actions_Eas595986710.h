﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.Actions.EaseFsmAction/EasingFunction
struct EasingFunction_t524263911;
// System.Single[]
struct SingleU5BU5D_t2316563989;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Eas397396748.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EaseFsmAction
struct  EaseFsmAction_t595986710  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.EaseFsmAction::time
	FsmFloat_t2134102846 * ___time_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.EaseFsmAction::speed
	FsmFloat_t2134102846 * ___speed_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.EaseFsmAction::delay
	FsmFloat_t2134102846 * ___delay_11;
	// HutongGames.PlayMaker.Actions.EaseFsmAction/EaseType HutongGames.PlayMaker.Actions.EaseFsmAction::easeType
	int32_t ___easeType_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.EaseFsmAction::reverse
	FsmBool_t1075959796 * ___reverse_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.EaseFsmAction::finishEvent
	FsmEvent_t2133468028 * ___finishEvent_14;
	// System.Boolean HutongGames.PlayMaker.Actions.EaseFsmAction::realTime
	bool ___realTime_15;
	// HutongGames.PlayMaker.Actions.EaseFsmAction/EasingFunction HutongGames.PlayMaker.Actions.EaseFsmAction::ease
	EasingFunction_t524263911 * ___ease_16;
	// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::runningTime
	float ___runningTime_17;
	// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::lastTime
	float ___lastTime_18;
	// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::startTime
	float ___startTime_19;
	// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::deltaTime
	float ___deltaTime_20;
	// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::delayTime
	float ___delayTime_21;
	// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::percentage
	float ___percentage_22;
	// System.Single[] HutongGames.PlayMaker.Actions.EaseFsmAction::fromFloats
	SingleU5BU5D_t2316563989* ___fromFloats_23;
	// System.Single[] HutongGames.PlayMaker.Actions.EaseFsmAction::toFloats
	SingleU5BU5D_t2316563989* ___toFloats_24;
	// System.Single[] HutongGames.PlayMaker.Actions.EaseFsmAction::resultFloats
	SingleU5BU5D_t2316563989* ___resultFloats_25;
	// System.Boolean HutongGames.PlayMaker.Actions.EaseFsmAction::finishAction
	bool ___finishAction_26;
	// System.Boolean HutongGames.PlayMaker.Actions.EaseFsmAction::start
	bool ___start_27;
	// System.Boolean HutongGames.PlayMaker.Actions.EaseFsmAction::finished
	bool ___finished_28;
	// System.Boolean HutongGames.PlayMaker.Actions.EaseFsmAction::isRunning
	bool ___isRunning_29;

public:
	inline static int32_t get_offset_of_time_9() { return static_cast<int32_t>(offsetof(EaseFsmAction_t595986710, ___time_9)); }
	inline FsmFloat_t2134102846 * get_time_9() const { return ___time_9; }
	inline FsmFloat_t2134102846 ** get_address_of_time_9() { return &___time_9; }
	inline void set_time_9(FsmFloat_t2134102846 * value)
	{
		___time_9 = value;
		Il2CppCodeGenWriteBarrier(&___time_9, value);
	}

	inline static int32_t get_offset_of_speed_10() { return static_cast<int32_t>(offsetof(EaseFsmAction_t595986710, ___speed_10)); }
	inline FsmFloat_t2134102846 * get_speed_10() const { return ___speed_10; }
	inline FsmFloat_t2134102846 ** get_address_of_speed_10() { return &___speed_10; }
	inline void set_speed_10(FsmFloat_t2134102846 * value)
	{
		___speed_10 = value;
		Il2CppCodeGenWriteBarrier(&___speed_10, value);
	}

	inline static int32_t get_offset_of_delay_11() { return static_cast<int32_t>(offsetof(EaseFsmAction_t595986710, ___delay_11)); }
	inline FsmFloat_t2134102846 * get_delay_11() const { return ___delay_11; }
	inline FsmFloat_t2134102846 ** get_address_of_delay_11() { return &___delay_11; }
	inline void set_delay_11(FsmFloat_t2134102846 * value)
	{
		___delay_11 = value;
		Il2CppCodeGenWriteBarrier(&___delay_11, value);
	}

	inline static int32_t get_offset_of_easeType_12() { return static_cast<int32_t>(offsetof(EaseFsmAction_t595986710, ___easeType_12)); }
	inline int32_t get_easeType_12() const { return ___easeType_12; }
	inline int32_t* get_address_of_easeType_12() { return &___easeType_12; }
	inline void set_easeType_12(int32_t value)
	{
		___easeType_12 = value;
	}

	inline static int32_t get_offset_of_reverse_13() { return static_cast<int32_t>(offsetof(EaseFsmAction_t595986710, ___reverse_13)); }
	inline FsmBool_t1075959796 * get_reverse_13() const { return ___reverse_13; }
	inline FsmBool_t1075959796 ** get_address_of_reverse_13() { return &___reverse_13; }
	inline void set_reverse_13(FsmBool_t1075959796 * value)
	{
		___reverse_13 = value;
		Il2CppCodeGenWriteBarrier(&___reverse_13, value);
	}

	inline static int32_t get_offset_of_finishEvent_14() { return static_cast<int32_t>(offsetof(EaseFsmAction_t595986710, ___finishEvent_14)); }
	inline FsmEvent_t2133468028 * get_finishEvent_14() const { return ___finishEvent_14; }
	inline FsmEvent_t2133468028 ** get_address_of_finishEvent_14() { return &___finishEvent_14; }
	inline void set_finishEvent_14(FsmEvent_t2133468028 * value)
	{
		___finishEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___finishEvent_14, value);
	}

	inline static int32_t get_offset_of_realTime_15() { return static_cast<int32_t>(offsetof(EaseFsmAction_t595986710, ___realTime_15)); }
	inline bool get_realTime_15() const { return ___realTime_15; }
	inline bool* get_address_of_realTime_15() { return &___realTime_15; }
	inline void set_realTime_15(bool value)
	{
		___realTime_15 = value;
	}

	inline static int32_t get_offset_of_ease_16() { return static_cast<int32_t>(offsetof(EaseFsmAction_t595986710, ___ease_16)); }
	inline EasingFunction_t524263911 * get_ease_16() const { return ___ease_16; }
	inline EasingFunction_t524263911 ** get_address_of_ease_16() { return &___ease_16; }
	inline void set_ease_16(EasingFunction_t524263911 * value)
	{
		___ease_16 = value;
		Il2CppCodeGenWriteBarrier(&___ease_16, value);
	}

	inline static int32_t get_offset_of_runningTime_17() { return static_cast<int32_t>(offsetof(EaseFsmAction_t595986710, ___runningTime_17)); }
	inline float get_runningTime_17() const { return ___runningTime_17; }
	inline float* get_address_of_runningTime_17() { return &___runningTime_17; }
	inline void set_runningTime_17(float value)
	{
		___runningTime_17 = value;
	}

	inline static int32_t get_offset_of_lastTime_18() { return static_cast<int32_t>(offsetof(EaseFsmAction_t595986710, ___lastTime_18)); }
	inline float get_lastTime_18() const { return ___lastTime_18; }
	inline float* get_address_of_lastTime_18() { return &___lastTime_18; }
	inline void set_lastTime_18(float value)
	{
		___lastTime_18 = value;
	}

	inline static int32_t get_offset_of_startTime_19() { return static_cast<int32_t>(offsetof(EaseFsmAction_t595986710, ___startTime_19)); }
	inline float get_startTime_19() const { return ___startTime_19; }
	inline float* get_address_of_startTime_19() { return &___startTime_19; }
	inline void set_startTime_19(float value)
	{
		___startTime_19 = value;
	}

	inline static int32_t get_offset_of_deltaTime_20() { return static_cast<int32_t>(offsetof(EaseFsmAction_t595986710, ___deltaTime_20)); }
	inline float get_deltaTime_20() const { return ___deltaTime_20; }
	inline float* get_address_of_deltaTime_20() { return &___deltaTime_20; }
	inline void set_deltaTime_20(float value)
	{
		___deltaTime_20 = value;
	}

	inline static int32_t get_offset_of_delayTime_21() { return static_cast<int32_t>(offsetof(EaseFsmAction_t595986710, ___delayTime_21)); }
	inline float get_delayTime_21() const { return ___delayTime_21; }
	inline float* get_address_of_delayTime_21() { return &___delayTime_21; }
	inline void set_delayTime_21(float value)
	{
		___delayTime_21 = value;
	}

	inline static int32_t get_offset_of_percentage_22() { return static_cast<int32_t>(offsetof(EaseFsmAction_t595986710, ___percentage_22)); }
	inline float get_percentage_22() const { return ___percentage_22; }
	inline float* get_address_of_percentage_22() { return &___percentage_22; }
	inline void set_percentage_22(float value)
	{
		___percentage_22 = value;
	}

	inline static int32_t get_offset_of_fromFloats_23() { return static_cast<int32_t>(offsetof(EaseFsmAction_t595986710, ___fromFloats_23)); }
	inline SingleU5BU5D_t2316563989* get_fromFloats_23() const { return ___fromFloats_23; }
	inline SingleU5BU5D_t2316563989** get_address_of_fromFloats_23() { return &___fromFloats_23; }
	inline void set_fromFloats_23(SingleU5BU5D_t2316563989* value)
	{
		___fromFloats_23 = value;
		Il2CppCodeGenWriteBarrier(&___fromFloats_23, value);
	}

	inline static int32_t get_offset_of_toFloats_24() { return static_cast<int32_t>(offsetof(EaseFsmAction_t595986710, ___toFloats_24)); }
	inline SingleU5BU5D_t2316563989* get_toFloats_24() const { return ___toFloats_24; }
	inline SingleU5BU5D_t2316563989** get_address_of_toFloats_24() { return &___toFloats_24; }
	inline void set_toFloats_24(SingleU5BU5D_t2316563989* value)
	{
		___toFloats_24 = value;
		Il2CppCodeGenWriteBarrier(&___toFloats_24, value);
	}

	inline static int32_t get_offset_of_resultFloats_25() { return static_cast<int32_t>(offsetof(EaseFsmAction_t595986710, ___resultFloats_25)); }
	inline SingleU5BU5D_t2316563989* get_resultFloats_25() const { return ___resultFloats_25; }
	inline SingleU5BU5D_t2316563989** get_address_of_resultFloats_25() { return &___resultFloats_25; }
	inline void set_resultFloats_25(SingleU5BU5D_t2316563989* value)
	{
		___resultFloats_25 = value;
		Il2CppCodeGenWriteBarrier(&___resultFloats_25, value);
	}

	inline static int32_t get_offset_of_finishAction_26() { return static_cast<int32_t>(offsetof(EaseFsmAction_t595986710, ___finishAction_26)); }
	inline bool get_finishAction_26() const { return ___finishAction_26; }
	inline bool* get_address_of_finishAction_26() { return &___finishAction_26; }
	inline void set_finishAction_26(bool value)
	{
		___finishAction_26 = value;
	}

	inline static int32_t get_offset_of_start_27() { return static_cast<int32_t>(offsetof(EaseFsmAction_t595986710, ___start_27)); }
	inline bool get_start_27() const { return ___start_27; }
	inline bool* get_address_of_start_27() { return &___start_27; }
	inline void set_start_27(bool value)
	{
		___start_27 = value;
	}

	inline static int32_t get_offset_of_finished_28() { return static_cast<int32_t>(offsetof(EaseFsmAction_t595986710, ___finished_28)); }
	inline bool get_finished_28() const { return ___finished_28; }
	inline bool* get_address_of_finished_28() { return &___finished_28; }
	inline void set_finished_28(bool value)
	{
		___finished_28 = value;
	}

	inline static int32_t get_offset_of_isRunning_29() { return static_cast<int32_t>(offsetof(EaseFsmAction_t595986710, ___isRunning_29)); }
	inline bool get_isRunning_29() const { return ___isRunning_29; }
	inline bool* get_address_of_isRunning_29() { return &___isRunning_29; }
	inline void set_isRunning_29(bool value)
	{
		___isRunning_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
