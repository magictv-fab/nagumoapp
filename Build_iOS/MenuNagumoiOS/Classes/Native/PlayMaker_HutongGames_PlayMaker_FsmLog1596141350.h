﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmLog>
struct List_1_t2964326902;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmLogEntry>
struct List_1_t3983052136;
// HutongGames.PlayMaker.Fsm
struct Fsm_t1527112426;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmLog
struct  FsmLog_t1596141350  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmLogEntry> HutongGames.PlayMaker.FsmLog::entries
	List_1_t3983052136 * ___entries_3;
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.FsmLog::<Fsm>k__BackingField
	Fsm_t1527112426 * ___U3CFsmU3Ek__BackingField_6;
	// System.Boolean HutongGames.PlayMaker.FsmLog::<Resized>k__BackingField
	bool ___U3CResizedU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_entries_3() { return static_cast<int32_t>(offsetof(FsmLog_t1596141350, ___entries_3)); }
	inline List_1_t3983052136 * get_entries_3() const { return ___entries_3; }
	inline List_1_t3983052136 ** get_address_of_entries_3() { return &___entries_3; }
	inline void set_entries_3(List_1_t3983052136 * value)
	{
		___entries_3 = value;
		Il2CppCodeGenWriteBarrier(&___entries_3, value);
	}

	inline static int32_t get_offset_of_U3CFsmU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(FsmLog_t1596141350, ___U3CFsmU3Ek__BackingField_6)); }
	inline Fsm_t1527112426 * get_U3CFsmU3Ek__BackingField_6() const { return ___U3CFsmU3Ek__BackingField_6; }
	inline Fsm_t1527112426 ** get_address_of_U3CFsmU3Ek__BackingField_6() { return &___U3CFsmU3Ek__BackingField_6; }
	inline void set_U3CFsmU3Ek__BackingField_6(Fsm_t1527112426 * value)
	{
		___U3CFsmU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CFsmU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3CResizedU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(FsmLog_t1596141350, ___U3CResizedU3Ek__BackingField_7)); }
	inline bool get_U3CResizedU3Ek__BackingField_7() const { return ___U3CResizedU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CResizedU3Ek__BackingField_7() { return &___U3CResizedU3Ek__BackingField_7; }
	inline void set_U3CResizedU3Ek__BackingField_7(bool value)
	{
		___U3CResizedU3Ek__BackingField_7 = value;
	}
};

struct FsmLog_t1596141350_StaticFields
{
public:
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmLog> HutongGames.PlayMaker.FsmLog::Logs
	List_1_t2964326902 * ___Logs_1;
	// System.Boolean HutongGames.PlayMaker.FsmLog::loggingEnabled
	bool ___loggingEnabled_2;
	// System.Boolean HutongGames.PlayMaker.FsmLog::<MirrorDebugLog>k__BackingField
	bool ___U3CMirrorDebugLogU3Ek__BackingField_4;
	// System.Boolean HutongGames.PlayMaker.FsmLog::<EnableDebugFlow>k__BackingField
	bool ___U3CEnableDebugFlowU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_Logs_1() { return static_cast<int32_t>(offsetof(FsmLog_t1596141350_StaticFields, ___Logs_1)); }
	inline List_1_t2964326902 * get_Logs_1() const { return ___Logs_1; }
	inline List_1_t2964326902 ** get_address_of_Logs_1() { return &___Logs_1; }
	inline void set_Logs_1(List_1_t2964326902 * value)
	{
		___Logs_1 = value;
		Il2CppCodeGenWriteBarrier(&___Logs_1, value);
	}

	inline static int32_t get_offset_of_loggingEnabled_2() { return static_cast<int32_t>(offsetof(FsmLog_t1596141350_StaticFields, ___loggingEnabled_2)); }
	inline bool get_loggingEnabled_2() const { return ___loggingEnabled_2; }
	inline bool* get_address_of_loggingEnabled_2() { return &___loggingEnabled_2; }
	inline void set_loggingEnabled_2(bool value)
	{
		___loggingEnabled_2 = value;
	}

	inline static int32_t get_offset_of_U3CMirrorDebugLogU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(FsmLog_t1596141350_StaticFields, ___U3CMirrorDebugLogU3Ek__BackingField_4)); }
	inline bool get_U3CMirrorDebugLogU3Ek__BackingField_4() const { return ___U3CMirrorDebugLogU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CMirrorDebugLogU3Ek__BackingField_4() { return &___U3CMirrorDebugLogU3Ek__BackingField_4; }
	inline void set_U3CMirrorDebugLogU3Ek__BackingField_4(bool value)
	{
		___U3CMirrorDebugLogU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CEnableDebugFlowU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(FsmLog_t1596141350_StaticFields, ___U3CEnableDebugFlowU3Ek__BackingField_5)); }
	inline bool get_U3CEnableDebugFlowU3Ek__BackingField_5() const { return ___U3CEnableDebugFlowU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CEnableDebugFlowU3Ek__BackingField_5() { return &___U3CEnableDebugFlowU3Ek__BackingField_5; }
	inline void set_U3CEnableDebugFlowU3Ek__BackingField_5(bool value)
	{
		___U3CEnableDebugFlowU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
