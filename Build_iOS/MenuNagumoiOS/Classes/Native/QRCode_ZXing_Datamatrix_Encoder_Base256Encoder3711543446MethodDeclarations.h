﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Datamatrix.Encoder.Base256Encoder
struct Base256Encoder_t3711543446;
// ZXing.Datamatrix.Encoder.EncoderContext
struct EncoderContext_t1774722223;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Datamatrix_Encoder_EncoderContext1774722223.h"

// System.Int32 ZXing.Datamatrix.Encoder.Base256Encoder::get_EncodingMode()
extern "C"  int32_t Base256Encoder_get_EncodingMode_m17465999 (Base256Encoder_t3711543446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.Base256Encoder::encode(ZXing.Datamatrix.Encoder.EncoderContext)
extern "C"  void Base256Encoder_encode_m2216119424 (Base256Encoder_t3711543446 * __this, EncoderContext_t1774722223 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char ZXing.Datamatrix.Encoder.Base256Encoder::randomize255State(System.Char,System.Int32)
extern "C"  Il2CppChar Base256Encoder_randomize255State_m849158636 (Il2CppObject * __this /* static, unused */, Il2CppChar ___ch0, int32_t ___codewordPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.Base256Encoder::.ctor()
extern "C"  void Base256Encoder__ctor_m846908038 (Base256Encoder_t3711543446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
