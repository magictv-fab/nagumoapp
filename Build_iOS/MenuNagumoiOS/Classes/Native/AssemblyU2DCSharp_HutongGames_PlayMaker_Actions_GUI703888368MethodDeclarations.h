﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutEndScrollView
struct GUILayoutEndScrollView_t703888368;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutEndScrollView::.ctor()
extern "C"  void GUILayoutEndScrollView__ctor_m3256050294 (GUILayoutEndScrollView_t703888368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutEndScrollView::OnGUI()
extern "C"  void GUILayoutEndScrollView_OnGUI_m2751448944 (GUILayoutEndScrollView_t703888368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
