﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.Common.ReedSolomon.GenericGF
struct GenericGF_t2563420960;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.ReedSolomon.ReedSolomonDecoder
struct  ReedSolomonDecoder_t4166992619  : public Il2CppObject
{
public:
	// ZXing.Common.ReedSolomon.GenericGF ZXing.Common.ReedSolomon.ReedSolomonDecoder::field
	GenericGF_t2563420960 * ___field_0;

public:
	inline static int32_t get_offset_of_field_0() { return static_cast<int32_t>(offsetof(ReedSolomonDecoder_t4166992619, ___field_0)); }
	inline GenericGF_t2563420960 * get_field_0() const { return ___field_0; }
	inline GenericGF_t2563420960 ** get_address_of_field_0() { return &___field_0; }
	inline void set_field_0(GenericGF_t2563420960 * value)
	{
		___field_0 = value;
		Il2CppCodeGenWriteBarrier(&___field_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
