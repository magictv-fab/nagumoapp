﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Tar.InvalidHeaderException
struct InvalidHeaderException_t3009170913;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.String
struct String_t;
// System.Exception
struct Exception_t3991598821;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Exception3991598821.h"

// System.Void ICSharpCode.SharpZipLib.Tar.InvalidHeaderException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void InvalidHeaderException__ctor_m1672815767 (InvalidHeaderException_t3009170913 * __this, SerializationInfo_t2185721892 * ___information0, StreamingContext_t2761351129  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.InvalidHeaderException::.ctor()
extern "C"  void InvalidHeaderException__ctor_m2134134870 (InvalidHeaderException_t3009170913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.InvalidHeaderException::.ctor(System.String)
extern "C"  void InvalidHeaderException__ctor_m3677424236 (InvalidHeaderException_t3009170913 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.InvalidHeaderException::.ctor(System.String,System.Exception)
extern "C"  void InvalidHeaderException__ctor_m2147704906 (InvalidHeaderException_t3009170913 * __this, String_t* ___message0, Exception_t3991598821 * ___exception1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
