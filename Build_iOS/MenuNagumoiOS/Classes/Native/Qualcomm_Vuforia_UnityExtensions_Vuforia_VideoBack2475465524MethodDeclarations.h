﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.VideoBackgroundAbstractBehaviour
struct VideoBackgroundAbstractBehaviour_t2475465524;

#include "codegen/il2cpp-codegen.h"

// System.Void Vuforia.VideoBackgroundAbstractBehaviour::ResetBackgroundPlane(System.Boolean)
extern "C"  void VideoBackgroundAbstractBehaviour_ResetBackgroundPlane_m2522181409 (VideoBackgroundAbstractBehaviour_t2475465524 * __this, bool ___disable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::SetStereoDepth(System.Single)
extern "C"  void VideoBackgroundAbstractBehaviour_SetStereoDepth_m761667223 (VideoBackgroundAbstractBehaviour_t2475465524 * __this, float ___depth0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::ApplyStereoDepthToMatrices()
extern "C"  void VideoBackgroundAbstractBehaviour_ApplyStereoDepthToMatrices_m3074108573 (VideoBackgroundAbstractBehaviour_t2475465524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::RenderOnUpdate()
extern "C"  void VideoBackgroundAbstractBehaviour_RenderOnUpdate_m3519044009 (VideoBackgroundAbstractBehaviour_t2475465524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::Awake()
extern "C"  void VideoBackgroundAbstractBehaviour_Awake_m1055765788 (VideoBackgroundAbstractBehaviour_t2475465524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::Start()
extern "C"  void VideoBackgroundAbstractBehaviour_Start_m4060265657 (VideoBackgroundAbstractBehaviour_t2475465524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::OnPreRender()
extern "C"  void VideoBackgroundAbstractBehaviour_OnPreRender_m3981136081 (VideoBackgroundAbstractBehaviour_t2475465524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::OnPostRender()
extern "C"  void VideoBackgroundAbstractBehaviour_OnPostRender_m2310717632 (VideoBackgroundAbstractBehaviour_t2475465524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::OnDestroy()
extern "C"  void VideoBackgroundAbstractBehaviour_OnDestroy_m3053155378 (VideoBackgroundAbstractBehaviour_t2475465524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::.ctor()
extern "C"  void VideoBackgroundAbstractBehaviour__ctor_m818160569 (VideoBackgroundAbstractBehaviour_t2475465524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
