﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CaptureAndSaveEventListener
struct CaptureAndSaveEventListener_t2941400608;
// CaptureAndSaveEventListener/OnError
struct OnError_t1516540506;
// CaptureAndSaveEventListener/OnSuccess
struct OnSuccess_t1013330901;
// CaptureAndSaveEventListener/OnScreenShot
struct OnScreenShot_t3427048564;

#include "codegen/il2cpp-codegen.h"
#include "CaptureAndSaveLib_CaptureAndSaveEventListener_OnEr1516540506.h"
#include "CaptureAndSaveLib_CaptureAndSaveEventListener_OnSu1013330901.h"
#include "CaptureAndSaveLib_CaptureAndSaveEventListener_OnSc3427048564.h"

// System.Void CaptureAndSaveEventListener::.ctor()
extern "C"  void CaptureAndSaveEventListener__ctor_m3649768002 (CaptureAndSaveEventListener_t2941400608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSaveEventListener::add_onError(CaptureAndSaveEventListener/OnError)
extern "C"  void CaptureAndSaveEventListener_add_onError_m3586673325 (Il2CppObject * __this /* static, unused */, OnError_t1516540506 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSaveEventListener::remove_onError(CaptureAndSaveEventListener/OnError)
extern "C"  void CaptureAndSaveEventListener_remove_onError_m2299742024 (Il2CppObject * __this /* static, unused */, OnError_t1516540506 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSaveEventListener::add_onSuccess(CaptureAndSaveEventListener/OnSuccess)
extern "C"  void CaptureAndSaveEventListener_add_onSuccess_m31473463 (Il2CppObject * __this /* static, unused */, OnSuccess_t1013330901 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSaveEventListener::remove_onSuccess(CaptureAndSaveEventListener/OnSuccess)
extern "C"  void CaptureAndSaveEventListener_remove_onSuccess_m3889526354 (Il2CppObject * __this /* static, unused */, OnSuccess_t1013330901 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSaveEventListener::add_onScreenShot(CaptureAndSaveEventListener/OnScreenShot)
extern "C"  void CaptureAndSaveEventListener_add_onScreenShot_m407281169 (Il2CppObject * __this /* static, unused */, OnScreenShot_t3427048564 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSaveEventListener::remove_onScreenShot(CaptureAndSaveEventListener/OnScreenShot)
extern "C"  void CaptureAndSaveEventListener_remove_onScreenShot_m4252947692 (Il2CppObject * __this /* static, unused */, OnScreenShot_t3427048564 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSaveEventListener::AddHandler_onError(CaptureAndSaveEventListener/OnError)
extern "C"  void CaptureAndSaveEventListener_AddHandler_onError_m928399587 (Il2CppObject * __this /* static, unused */, OnError_t1516540506 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSaveEventListener::RemoveHandler_onError(CaptureAndSaveEventListener/OnError)
extern "C"  void CaptureAndSaveEventListener_RemoveHandler_onError_m926266408 (Il2CppObject * __this /* static, unused */, OnError_t1516540506 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSaveEventListener::AddHandler_onSuccess(CaptureAndSaveEventListener/OnSuccess)
extern "C"  void CaptureAndSaveEventListener_AddHandler_onSuccess_m1652304493 (Il2CppObject * __this /* static, unused */, OnSuccess_t1013330901 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSaveEventListener::RemoveHandler_onSuccess(CaptureAndSaveEventListener/OnSuccess)
extern "C"  void CaptureAndSaveEventListener_RemoveHandler_onSuccess_m3006690098 (Il2CppObject * __this /* static, unused */, OnSuccess_t1013330901 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSaveEventListener::AddHandler_onScreenShot(CaptureAndSaveEventListener/OnScreenShot)
extern "C"  void CaptureAndSaveEventListener_AddHandler_onScreenShot_m3650537671 (Il2CppObject * __this /* static, unused */, OnScreenShot_t3427048564 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaptureAndSaveEventListener::RemoveHandler_onScreenShot(CaptureAndSaveEventListener/OnScreenShot)
extern "C"  void CaptureAndSaveEventListener_RemoveHandler_onScreenShot_m2896603596 (Il2CppObject * __this /* static, unused */, OnScreenShot_t3427048564 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
