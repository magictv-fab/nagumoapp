﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CustomECPNManager
struct  CustomECPNManager_t1031774400  : public MonoBehaviour_t667441552
{
public:
	// System.String CustomECPNManager::GoogleCloudMessageProjectID
	String_t* ___GoogleCloudMessageProjectID_2;
	// System.String CustomECPNManager::packageName
	String_t* ___packageName_3;
	// System.String CustomECPNManager::devToken
	String_t* ___devToken_4;
	// System.Boolean CustomECPNManager::pollIOSDeviceToken
	bool ___pollIOSDeviceToken_5;

public:
	inline static int32_t get_offset_of_GoogleCloudMessageProjectID_2() { return static_cast<int32_t>(offsetof(CustomECPNManager_t1031774400, ___GoogleCloudMessageProjectID_2)); }
	inline String_t* get_GoogleCloudMessageProjectID_2() const { return ___GoogleCloudMessageProjectID_2; }
	inline String_t** get_address_of_GoogleCloudMessageProjectID_2() { return &___GoogleCloudMessageProjectID_2; }
	inline void set_GoogleCloudMessageProjectID_2(String_t* value)
	{
		___GoogleCloudMessageProjectID_2 = value;
		Il2CppCodeGenWriteBarrier(&___GoogleCloudMessageProjectID_2, value);
	}

	inline static int32_t get_offset_of_packageName_3() { return static_cast<int32_t>(offsetof(CustomECPNManager_t1031774400, ___packageName_3)); }
	inline String_t* get_packageName_3() const { return ___packageName_3; }
	inline String_t** get_address_of_packageName_3() { return &___packageName_3; }
	inline void set_packageName_3(String_t* value)
	{
		___packageName_3 = value;
		Il2CppCodeGenWriteBarrier(&___packageName_3, value);
	}

	inline static int32_t get_offset_of_devToken_4() { return static_cast<int32_t>(offsetof(CustomECPNManager_t1031774400, ___devToken_4)); }
	inline String_t* get_devToken_4() const { return ___devToken_4; }
	inline String_t** get_address_of_devToken_4() { return &___devToken_4; }
	inline void set_devToken_4(String_t* value)
	{
		___devToken_4 = value;
		Il2CppCodeGenWriteBarrier(&___devToken_4, value);
	}

	inline static int32_t get_offset_of_pollIOSDeviceToken_5() { return static_cast<int32_t>(offsetof(CustomECPNManager_t1031774400, ___pollIOSDeviceToken_5)); }
	inline bool get_pollIOSDeviceToken_5() const { return ___pollIOSDeviceToken_5; }
	inline bool* get_address_of_pollIOSDeviceToken_5() { return &___pollIOSDeviceToken_5; }
	inline void set_pollIOSDeviceToken_5(bool value)
	{
		___pollIOSDeviceToken_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
