﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "AssemblyU2DCSharp_ARM_transform_filters_abstracts_4158746314.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.transform.filters.ZeroFilter
struct  ZeroFilter_t1729114916  : public TransformPositionAngleFilterAbstract_t4158746314
{
public:
	// System.Boolean ARM.transform.filters.ZeroFilter::keepHistory
	bool ___keepHistory_15;
	// UnityEngine.GameObject ARM.transform.filters.ZeroFilter::arCamera
	GameObject_t3674682005 * ___arCamera_16;
	// UnityEngine.Quaternion ARM.transform.filters.ZeroFilter::_lastHistoryRotation
	Quaternion_t1553702882  ____lastHistoryRotation_17;
	// UnityEngine.Vector3 ARM.transform.filters.ZeroFilter::_lastHistoryPosition
	Vector3_t4282066566  ____lastHistoryPosition_18;

public:
	inline static int32_t get_offset_of_keepHistory_15() { return static_cast<int32_t>(offsetof(ZeroFilter_t1729114916, ___keepHistory_15)); }
	inline bool get_keepHistory_15() const { return ___keepHistory_15; }
	inline bool* get_address_of_keepHistory_15() { return &___keepHistory_15; }
	inline void set_keepHistory_15(bool value)
	{
		___keepHistory_15 = value;
	}

	inline static int32_t get_offset_of_arCamera_16() { return static_cast<int32_t>(offsetof(ZeroFilter_t1729114916, ___arCamera_16)); }
	inline GameObject_t3674682005 * get_arCamera_16() const { return ___arCamera_16; }
	inline GameObject_t3674682005 ** get_address_of_arCamera_16() { return &___arCamera_16; }
	inline void set_arCamera_16(GameObject_t3674682005 * value)
	{
		___arCamera_16 = value;
		Il2CppCodeGenWriteBarrier(&___arCamera_16, value);
	}

	inline static int32_t get_offset_of__lastHistoryRotation_17() { return static_cast<int32_t>(offsetof(ZeroFilter_t1729114916, ____lastHistoryRotation_17)); }
	inline Quaternion_t1553702882  get__lastHistoryRotation_17() const { return ____lastHistoryRotation_17; }
	inline Quaternion_t1553702882 * get_address_of__lastHistoryRotation_17() { return &____lastHistoryRotation_17; }
	inline void set__lastHistoryRotation_17(Quaternion_t1553702882  value)
	{
		____lastHistoryRotation_17 = value;
	}

	inline static int32_t get_offset_of__lastHistoryPosition_18() { return static_cast<int32_t>(offsetof(ZeroFilter_t1729114916, ____lastHistoryPosition_18)); }
	inline Vector3_t4282066566  get__lastHistoryPosition_18() const { return ____lastHistoryPosition_18; }
	inline Vector3_t4282066566 * get_address_of__lastHistoryPosition_18() { return &____lastHistoryPosition_18; }
	inline void set__lastHistoryPosition_18(Vector3_t4282066566  value)
	{
		____lastHistoryPosition_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
