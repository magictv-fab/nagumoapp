﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen3781873254MethodDeclarations.h"

// System.Void System.Predicate`1<ZXing.OneD.UPCEANReader>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m2418145388(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t3138227582 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m982040097_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<ZXing.OneD.UPCEANReader>::Invoke(T)
#define Predicate_1_Invoke_m1478489050(__this, ___obj0, method) ((  bool (*) (Predicate_1_t3138227582 *, UPCEANReader_t3527170699 *, const MethodInfo*))Predicate_1_Invoke_m4106178309_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<ZXing.OneD.UPCEANReader>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m818362541(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t3138227582 *, UPCEANReader_t3527170699 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m2038073176_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<ZXing.OneD.UPCEANReader>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m4263189562(__this, ___result0, method) ((  bool (*) (Predicate_1_t3138227582 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m3970497007_gshared)(__this, ___result0, method)
