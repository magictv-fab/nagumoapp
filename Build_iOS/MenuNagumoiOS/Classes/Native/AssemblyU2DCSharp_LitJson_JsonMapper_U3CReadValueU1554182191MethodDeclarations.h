﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LitJson.JsonMapper/<ReadValue>c__AnonStoreyA1
struct U3CReadValueU3Ec__AnonStoreyA1_t1554182191;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_PropertyInfo924268725.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void LitJson.JsonMapper/<ReadValue>c__AnonStoreyA1::.ctor()
extern "C"  void U3CReadValueU3Ec__AnonStoreyA1__ctor_m1778619932 (U3CReadValueU3Ec__AnonStoreyA1_t1554182191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMapper/<ReadValue>c__AnonStoreyA1::<>m__53(System.Reflection.PropertyInfo)
extern "C"  bool U3CReadValueU3Ec__AnonStoreyA1_U3CU3Em__53_m4044704176 (U3CReadValueU3Ec__AnonStoreyA1_t1554182191 * __this, PropertyInfo_t * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMapper/<ReadValue>c__AnonStoreyA1::<>m__54(System.Reflection.PropertyInfo)
extern "C"  bool U3CReadValueU3Ec__AnonStoreyA1_U3CU3Em__54_m1861027249 (U3CReadValueU3Ec__AnonStoreyA1_t1554182191 * __this, PropertyInfo_t * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMapper/<ReadValue>c__AnonStoreyA1::<>m__73(System.Object)
extern "C"  bool U3CReadValueU3Ec__AnonStoreyA1_U3CU3Em__73_m880409407 (U3CReadValueU3Ec__AnonStoreyA1_t1554182191 * __this, Il2CppObject * ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
