﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTVWindowConfirmControllerScript
struct MagicTVWindowConfirmControllerScript_t165107144;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTVWindowConfirmControllerScript::.ctor()
extern "C"  void MagicTVWindowConfirmControllerScript__ctor_m959043027 (MagicTVWindowConfirmControllerScript_t165107144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
