﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetFlareStrength
struct  SetFlareStrength_t2048001173  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetFlareStrength::flareStrength
	FsmFloat_t2134102846 * ___flareStrength_9;
	// System.Boolean HutongGames.PlayMaker.Actions.SetFlareStrength::everyFrame
	bool ___everyFrame_10;

public:
	inline static int32_t get_offset_of_flareStrength_9() { return static_cast<int32_t>(offsetof(SetFlareStrength_t2048001173, ___flareStrength_9)); }
	inline FsmFloat_t2134102846 * get_flareStrength_9() const { return ___flareStrength_9; }
	inline FsmFloat_t2134102846 ** get_address_of_flareStrength_9() { return &___flareStrength_9; }
	inline void set_flareStrength_9(FsmFloat_t2134102846 * value)
	{
		___flareStrength_9 = value;
		Il2CppCodeGenWriteBarrier(&___flareStrength_9, value);
	}

	inline static int32_t get_offset_of_everyFrame_10() { return static_cast<int32_t>(offsetof(SetFlareStrength_t2048001173, ___everyFrame_10)); }
	inline bool get_everyFrame_10() const { return ___everyFrame_10; }
	inline bool* get_address_of_everyFrame_10() { return &___everyFrame_10; }
	inline void set_everyFrame_10(bool value)
	{
		___everyFrame_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
