﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmTexture
struct FsmTexture_t3073272573;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetMouseCursor
struct  SetMouseCursor_t1046826369  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Actions.SetMouseCursor::cursorTexture
	FsmTexture_t3073272573 * ___cursorTexture_9;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetMouseCursor::hideCursor
	FsmBool_t1075959796 * ___hideCursor_10;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetMouseCursor::lockCursor
	FsmBool_t1075959796 * ___lockCursor_11;

public:
	inline static int32_t get_offset_of_cursorTexture_9() { return static_cast<int32_t>(offsetof(SetMouseCursor_t1046826369, ___cursorTexture_9)); }
	inline FsmTexture_t3073272573 * get_cursorTexture_9() const { return ___cursorTexture_9; }
	inline FsmTexture_t3073272573 ** get_address_of_cursorTexture_9() { return &___cursorTexture_9; }
	inline void set_cursorTexture_9(FsmTexture_t3073272573 * value)
	{
		___cursorTexture_9 = value;
		Il2CppCodeGenWriteBarrier(&___cursorTexture_9, value);
	}

	inline static int32_t get_offset_of_hideCursor_10() { return static_cast<int32_t>(offsetof(SetMouseCursor_t1046826369, ___hideCursor_10)); }
	inline FsmBool_t1075959796 * get_hideCursor_10() const { return ___hideCursor_10; }
	inline FsmBool_t1075959796 ** get_address_of_hideCursor_10() { return &___hideCursor_10; }
	inline void set_hideCursor_10(FsmBool_t1075959796 * value)
	{
		___hideCursor_10 = value;
		Il2CppCodeGenWriteBarrier(&___hideCursor_10, value);
	}

	inline static int32_t get_offset_of_lockCursor_11() { return static_cast<int32_t>(offsetof(SetMouseCursor_t1046826369, ___lockCursor_11)); }
	inline FsmBool_t1075959796 * get_lockCursor_11() const { return ___lockCursor_11; }
	inline FsmBool_t1075959796 ** get_address_of_lockCursor_11() { return &___lockCursor_11; }
	inline void set_lockCursor_11(FsmBool_t1075959796 * value)
	{
		___lockCursor_11 = value;
		Il2CppCodeGenWriteBarrier(&___lockCursor_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
