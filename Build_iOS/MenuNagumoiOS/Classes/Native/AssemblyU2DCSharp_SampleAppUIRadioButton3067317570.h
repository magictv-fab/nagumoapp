﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Boolean[]
struct BooleanU5BU5D_t3456302923;
// SampleAppUIRect[]
struct SampleAppUIRectU5BU5D_t2382086294;
// UnityEngine.GUIStyle[]
struct GUIStyleU5BU5D_t565654559;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Action`1<System.Int32>
struct Action_1_t1549654636;

#include "AssemblyU2DCSharp_ISampleAppUIElement2180050874.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SampleAppUIRadioButton
struct  SampleAppUIRadioButton_t3067317570  : public ISampleAppUIElement_t2180050874
{
public:
	// System.Single SampleAppUIRadioButton::mIndex
	float ___mIndex_0;
	// System.Boolean[] SampleAppUIRadioButton::mOptionsTapped
	BooleanU5BU5D_t3456302923* ___mOptionsTapped_1;
	// System.Boolean[] SampleAppUIRadioButton::mOptionsSelected
	BooleanU5BU5D_t3456302923* ___mOptionsSelected_2;
	// SampleAppUIRect[] SampleAppUIRadioButton::mRect
	SampleAppUIRectU5BU5D_t2382086294* ___mRect_3;
	// System.Boolean SampleAppUIRadioButton::mTappedOn
	bool ___mTappedOn_4;
	// UnityEngine.GUIStyle[] SampleAppUIRadioButton::mStyle
	GUIStyleU5BU5D_t565654559* ___mStyle_5;
	// System.Single SampleAppUIRadioButton::mWidth
	float ___mWidth_6;
	// System.Single SampleAppUIRadioButton::mHeight
	float ___mHeight_7;
	// System.String[] SampleAppUIRadioButton::titleList
	StringU5BU5D_t4054002952* ___titleList_8;
	// System.Action`1<System.Int32> SampleAppUIRadioButton::TappedOnOption
	Action_1_t1549654636 * ___TappedOnOption_9;

public:
	inline static int32_t get_offset_of_mIndex_0() { return static_cast<int32_t>(offsetof(SampleAppUIRadioButton_t3067317570, ___mIndex_0)); }
	inline float get_mIndex_0() const { return ___mIndex_0; }
	inline float* get_address_of_mIndex_0() { return &___mIndex_0; }
	inline void set_mIndex_0(float value)
	{
		___mIndex_0 = value;
	}

	inline static int32_t get_offset_of_mOptionsTapped_1() { return static_cast<int32_t>(offsetof(SampleAppUIRadioButton_t3067317570, ___mOptionsTapped_1)); }
	inline BooleanU5BU5D_t3456302923* get_mOptionsTapped_1() const { return ___mOptionsTapped_1; }
	inline BooleanU5BU5D_t3456302923** get_address_of_mOptionsTapped_1() { return &___mOptionsTapped_1; }
	inline void set_mOptionsTapped_1(BooleanU5BU5D_t3456302923* value)
	{
		___mOptionsTapped_1 = value;
		Il2CppCodeGenWriteBarrier(&___mOptionsTapped_1, value);
	}

	inline static int32_t get_offset_of_mOptionsSelected_2() { return static_cast<int32_t>(offsetof(SampleAppUIRadioButton_t3067317570, ___mOptionsSelected_2)); }
	inline BooleanU5BU5D_t3456302923* get_mOptionsSelected_2() const { return ___mOptionsSelected_2; }
	inline BooleanU5BU5D_t3456302923** get_address_of_mOptionsSelected_2() { return &___mOptionsSelected_2; }
	inline void set_mOptionsSelected_2(BooleanU5BU5D_t3456302923* value)
	{
		___mOptionsSelected_2 = value;
		Il2CppCodeGenWriteBarrier(&___mOptionsSelected_2, value);
	}

	inline static int32_t get_offset_of_mRect_3() { return static_cast<int32_t>(offsetof(SampleAppUIRadioButton_t3067317570, ___mRect_3)); }
	inline SampleAppUIRectU5BU5D_t2382086294* get_mRect_3() const { return ___mRect_3; }
	inline SampleAppUIRectU5BU5D_t2382086294** get_address_of_mRect_3() { return &___mRect_3; }
	inline void set_mRect_3(SampleAppUIRectU5BU5D_t2382086294* value)
	{
		___mRect_3 = value;
		Il2CppCodeGenWriteBarrier(&___mRect_3, value);
	}

	inline static int32_t get_offset_of_mTappedOn_4() { return static_cast<int32_t>(offsetof(SampleAppUIRadioButton_t3067317570, ___mTappedOn_4)); }
	inline bool get_mTappedOn_4() const { return ___mTappedOn_4; }
	inline bool* get_address_of_mTappedOn_4() { return &___mTappedOn_4; }
	inline void set_mTappedOn_4(bool value)
	{
		___mTappedOn_4 = value;
	}

	inline static int32_t get_offset_of_mStyle_5() { return static_cast<int32_t>(offsetof(SampleAppUIRadioButton_t3067317570, ___mStyle_5)); }
	inline GUIStyleU5BU5D_t565654559* get_mStyle_5() const { return ___mStyle_5; }
	inline GUIStyleU5BU5D_t565654559** get_address_of_mStyle_5() { return &___mStyle_5; }
	inline void set_mStyle_5(GUIStyleU5BU5D_t565654559* value)
	{
		___mStyle_5 = value;
		Il2CppCodeGenWriteBarrier(&___mStyle_5, value);
	}

	inline static int32_t get_offset_of_mWidth_6() { return static_cast<int32_t>(offsetof(SampleAppUIRadioButton_t3067317570, ___mWidth_6)); }
	inline float get_mWidth_6() const { return ___mWidth_6; }
	inline float* get_address_of_mWidth_6() { return &___mWidth_6; }
	inline void set_mWidth_6(float value)
	{
		___mWidth_6 = value;
	}

	inline static int32_t get_offset_of_mHeight_7() { return static_cast<int32_t>(offsetof(SampleAppUIRadioButton_t3067317570, ___mHeight_7)); }
	inline float get_mHeight_7() const { return ___mHeight_7; }
	inline float* get_address_of_mHeight_7() { return &___mHeight_7; }
	inline void set_mHeight_7(float value)
	{
		___mHeight_7 = value;
	}

	inline static int32_t get_offset_of_titleList_8() { return static_cast<int32_t>(offsetof(SampleAppUIRadioButton_t3067317570, ___titleList_8)); }
	inline StringU5BU5D_t4054002952* get_titleList_8() const { return ___titleList_8; }
	inline StringU5BU5D_t4054002952** get_address_of_titleList_8() { return &___titleList_8; }
	inline void set_titleList_8(StringU5BU5D_t4054002952* value)
	{
		___titleList_8 = value;
		Il2CppCodeGenWriteBarrier(&___titleList_8, value);
	}

	inline static int32_t get_offset_of_TappedOnOption_9() { return static_cast<int32_t>(offsetof(SampleAppUIRadioButton_t3067317570, ___TappedOnOption_9)); }
	inline Action_1_t1549654636 * get_TappedOnOption_9() const { return ___TappedOnOption_9; }
	inline Action_1_t1549654636 ** get_address_of_TappedOnOption_9() { return &___TappedOnOption_9; }
	inline void set_TappedOnOption_9(Action_1_t1549654636 * value)
	{
		___TappedOnOption_9 = value;
		Il2CppCodeGenWriteBarrier(&___TappedOnOption_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
