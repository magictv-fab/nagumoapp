﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmString952858651.h"

// System.String HutongGames.PlayMaker.FsmString::get_Value()
extern "C"  String_t* FsmString_get_Value_m872383149 (FsmString_t952858651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmString::set_Value(System.String)
extern "C"  void FsmString_set_Value_m829393196 (FsmString_t952858651 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmString::.ctor()
extern "C"  void FsmString__ctor_m2679108260 (FsmString_t952858651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmString::.ctor(System.String)
extern "C"  void FsmString__ctor_m3317700318 (FsmString_t952858651 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmString::.ctor(HutongGames.PlayMaker.FsmString)
extern "C"  void FsmString__ctor_m2617689225 (FsmString_t952858651 * __this, FsmString_t952858651 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmString::ToString()
extern "C"  String_t* FsmString_ToString_m684866441 (FsmString_t952858651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.FsmString::op_Implicit(System.String)
extern "C"  FsmString_t952858651 * FsmString_op_Implicit_m224809487 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
