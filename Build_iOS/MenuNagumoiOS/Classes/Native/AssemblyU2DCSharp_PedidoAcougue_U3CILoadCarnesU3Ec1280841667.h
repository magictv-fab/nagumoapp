﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// UnityEngine.WWW
struct WWW_t3134621005;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// CarnesData[]
struct CarnesDataU5BU5D_t2965870023;
// CarnesData
struct CarnesData_t3441984818;
// System.Object
struct Il2CppObject;
// PedidoAcougue
struct PedidoAcougue_t9638794;

#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat767573031.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PedidoAcougue/<ILoadCarnes>c__Iterator6F
struct  U3CILoadCarnesU3Ec__Iterator6F_t1280841667  : public Il2CppObject
{
public:
	// System.String PedidoAcougue/<ILoadCarnes>c__Iterator6F::<json>__0
	String_t* ___U3CjsonU3E__0_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> PedidoAcougue/<ILoadCarnes>c__Iterator6F::<headers>__1
	Dictionary_2_t827649927 * ___U3CheadersU3E__1_1;
	// System.Byte[] PedidoAcougue/<ILoadCarnes>c__Iterator6F::<pData>__2
	ByteU5BU5D_t4260760469* ___U3CpDataU3E__2_2;
	// UnityEngine.WWW PedidoAcougue/<ILoadCarnes>c__Iterator6F::<www>__3
	WWW_t3134621005 * ___U3CwwwU3E__3_3;
	// System.String PedidoAcougue/<ILoadCarnes>c__Iterator6F::search
	String_t* ___search_4;
	// System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject> PedidoAcougue/<ILoadCarnes>c__Iterator6F::<$s_332>__4
	Enumerator_t767573031  ___U3CU24s_332U3E__4_5;
	// UnityEngine.GameObject PedidoAcougue/<ILoadCarnes>c__Iterator6F::<obj>__5
	GameObject_t3674682005 * ___U3CobjU3E__5_6;
	// System.String PedidoAcougue/<ILoadCarnes>c__Iterator6F::<message>__6
	String_t* ___U3CmessageU3E__6_7;
	// CarnesData[] PedidoAcougue/<ILoadCarnes>c__Iterator6F::<$s_333>__7
	CarnesDataU5BU5D_t2965870023* ___U3CU24s_333U3E__7_8;
	// System.Int32 PedidoAcougue/<ILoadCarnes>c__Iterator6F::<$s_334>__8
	int32_t ___U3CU24s_334U3E__8_9;
	// CarnesData PedidoAcougue/<ILoadCarnes>c__Iterator6F::<carneData>__9
	CarnesData_t3441984818 * ___U3CcarneDataU3E__9_10;
	// System.Int32 PedidoAcougue/<ILoadCarnes>c__Iterator6F::$PC
	int32_t ___U24PC_11;
	// System.Object PedidoAcougue/<ILoadCarnes>c__Iterator6F::$current
	Il2CppObject * ___U24current_12;
	// System.String PedidoAcougue/<ILoadCarnes>c__Iterator6F::<$>search
	String_t* ___U3CU24U3Esearch_13;
	// PedidoAcougue PedidoAcougue/<ILoadCarnes>c__Iterator6F::<>f__this
	PedidoAcougue_t9638794 * ___U3CU3Ef__this_14;

public:
	inline static int32_t get_offset_of_U3CjsonU3E__0_0() { return static_cast<int32_t>(offsetof(U3CILoadCarnesU3Ec__Iterator6F_t1280841667, ___U3CjsonU3E__0_0)); }
	inline String_t* get_U3CjsonU3E__0_0() const { return ___U3CjsonU3E__0_0; }
	inline String_t** get_address_of_U3CjsonU3E__0_0() { return &___U3CjsonU3E__0_0; }
	inline void set_U3CjsonU3E__0_0(String_t* value)
	{
		___U3CjsonU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjsonU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CheadersU3E__1_1() { return static_cast<int32_t>(offsetof(U3CILoadCarnesU3Ec__Iterator6F_t1280841667, ___U3CheadersU3E__1_1)); }
	inline Dictionary_2_t827649927 * get_U3CheadersU3E__1_1() const { return ___U3CheadersU3E__1_1; }
	inline Dictionary_2_t827649927 ** get_address_of_U3CheadersU3E__1_1() { return &___U3CheadersU3E__1_1; }
	inline void set_U3CheadersU3E__1_1(Dictionary_2_t827649927 * value)
	{
		___U3CheadersU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CheadersU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CpDataU3E__2_2() { return static_cast<int32_t>(offsetof(U3CILoadCarnesU3Ec__Iterator6F_t1280841667, ___U3CpDataU3E__2_2)); }
	inline ByteU5BU5D_t4260760469* get_U3CpDataU3E__2_2() const { return ___U3CpDataU3E__2_2; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CpDataU3E__2_2() { return &___U3CpDataU3E__2_2; }
	inline void set_U3CpDataU3E__2_2(ByteU5BU5D_t4260760469* value)
	{
		___U3CpDataU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpDataU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__3_3() { return static_cast<int32_t>(offsetof(U3CILoadCarnesU3Ec__Iterator6F_t1280841667, ___U3CwwwU3E__3_3)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__3_3() const { return ___U3CwwwU3E__3_3; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__3_3() { return &___U3CwwwU3E__3_3; }
	inline void set_U3CwwwU3E__3_3(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__3_3, value);
	}

	inline static int32_t get_offset_of_search_4() { return static_cast<int32_t>(offsetof(U3CILoadCarnesU3Ec__Iterator6F_t1280841667, ___search_4)); }
	inline String_t* get_search_4() const { return ___search_4; }
	inline String_t** get_address_of_search_4() { return &___search_4; }
	inline void set_search_4(String_t* value)
	{
		___search_4 = value;
		Il2CppCodeGenWriteBarrier(&___search_4, value);
	}

	inline static int32_t get_offset_of_U3CU24s_332U3E__4_5() { return static_cast<int32_t>(offsetof(U3CILoadCarnesU3Ec__Iterator6F_t1280841667, ___U3CU24s_332U3E__4_5)); }
	inline Enumerator_t767573031  get_U3CU24s_332U3E__4_5() const { return ___U3CU24s_332U3E__4_5; }
	inline Enumerator_t767573031 * get_address_of_U3CU24s_332U3E__4_5() { return &___U3CU24s_332U3E__4_5; }
	inline void set_U3CU24s_332U3E__4_5(Enumerator_t767573031  value)
	{
		___U3CU24s_332U3E__4_5 = value;
	}

	inline static int32_t get_offset_of_U3CobjU3E__5_6() { return static_cast<int32_t>(offsetof(U3CILoadCarnesU3Ec__Iterator6F_t1280841667, ___U3CobjU3E__5_6)); }
	inline GameObject_t3674682005 * get_U3CobjU3E__5_6() const { return ___U3CobjU3E__5_6; }
	inline GameObject_t3674682005 ** get_address_of_U3CobjU3E__5_6() { return &___U3CobjU3E__5_6; }
	inline void set_U3CobjU3E__5_6(GameObject_t3674682005 * value)
	{
		___U3CobjU3E__5_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CobjU3E__5_6, value);
	}

	inline static int32_t get_offset_of_U3CmessageU3E__6_7() { return static_cast<int32_t>(offsetof(U3CILoadCarnesU3Ec__Iterator6F_t1280841667, ___U3CmessageU3E__6_7)); }
	inline String_t* get_U3CmessageU3E__6_7() const { return ___U3CmessageU3E__6_7; }
	inline String_t** get_address_of_U3CmessageU3E__6_7() { return &___U3CmessageU3E__6_7; }
	inline void set_U3CmessageU3E__6_7(String_t* value)
	{
		___U3CmessageU3E__6_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmessageU3E__6_7, value);
	}

	inline static int32_t get_offset_of_U3CU24s_333U3E__7_8() { return static_cast<int32_t>(offsetof(U3CILoadCarnesU3Ec__Iterator6F_t1280841667, ___U3CU24s_333U3E__7_8)); }
	inline CarnesDataU5BU5D_t2965870023* get_U3CU24s_333U3E__7_8() const { return ___U3CU24s_333U3E__7_8; }
	inline CarnesDataU5BU5D_t2965870023** get_address_of_U3CU24s_333U3E__7_8() { return &___U3CU24s_333U3E__7_8; }
	inline void set_U3CU24s_333U3E__7_8(CarnesDataU5BU5D_t2965870023* value)
	{
		___U3CU24s_333U3E__7_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_333U3E__7_8, value);
	}

	inline static int32_t get_offset_of_U3CU24s_334U3E__8_9() { return static_cast<int32_t>(offsetof(U3CILoadCarnesU3Ec__Iterator6F_t1280841667, ___U3CU24s_334U3E__8_9)); }
	inline int32_t get_U3CU24s_334U3E__8_9() const { return ___U3CU24s_334U3E__8_9; }
	inline int32_t* get_address_of_U3CU24s_334U3E__8_9() { return &___U3CU24s_334U3E__8_9; }
	inline void set_U3CU24s_334U3E__8_9(int32_t value)
	{
		___U3CU24s_334U3E__8_9 = value;
	}

	inline static int32_t get_offset_of_U3CcarneDataU3E__9_10() { return static_cast<int32_t>(offsetof(U3CILoadCarnesU3Ec__Iterator6F_t1280841667, ___U3CcarneDataU3E__9_10)); }
	inline CarnesData_t3441984818 * get_U3CcarneDataU3E__9_10() const { return ___U3CcarneDataU3E__9_10; }
	inline CarnesData_t3441984818 ** get_address_of_U3CcarneDataU3E__9_10() { return &___U3CcarneDataU3E__9_10; }
	inline void set_U3CcarneDataU3E__9_10(CarnesData_t3441984818 * value)
	{
		___U3CcarneDataU3E__9_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcarneDataU3E__9_10, value);
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CILoadCarnesU3Ec__Iterator6F_t1280841667, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}

	inline static int32_t get_offset_of_U24current_12() { return static_cast<int32_t>(offsetof(U3CILoadCarnesU3Ec__Iterator6F_t1280841667, ___U24current_12)); }
	inline Il2CppObject * get_U24current_12() const { return ___U24current_12; }
	inline Il2CppObject ** get_address_of_U24current_12() { return &___U24current_12; }
	inline void set_U24current_12(Il2CppObject * value)
	{
		___U24current_12 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_12, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Esearch_13() { return static_cast<int32_t>(offsetof(U3CILoadCarnesU3Ec__Iterator6F_t1280841667, ___U3CU24U3Esearch_13)); }
	inline String_t* get_U3CU24U3Esearch_13() const { return ___U3CU24U3Esearch_13; }
	inline String_t** get_address_of_U3CU24U3Esearch_13() { return &___U3CU24U3Esearch_13; }
	inline void set_U3CU24U3Esearch_13(String_t* value)
	{
		___U3CU24U3Esearch_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Esearch_13, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_14() { return static_cast<int32_t>(offsetof(U3CILoadCarnesU3Ec__Iterator6F_t1280841667, ___U3CU3Ef__this_14)); }
	inline PedidoAcougue_t9638794 * get_U3CU3Ef__this_14() const { return ___U3CU3Ef__this_14; }
	inline PedidoAcougue_t9638794 ** get_address_of_U3CU3Ef__this_14() { return &___U3CU3Ef__this_14; }
	inline void set_U3CU3Ef__this_14(PedidoAcougue_t9638794 * value)
	{
		___U3CU3Ef__this_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
