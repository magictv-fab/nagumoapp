﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1848751023;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ScrollRectEx/<OnBeginDrag>c__AnonStorey8D
struct  U3COnBeginDragU3Ec__AnonStorey8D_t1656158571  : public Il2CppObject
{
public:
	// UnityEngine.EventSystems.PointerEventData UnityEngine.UI.Extensions.ScrollRectEx/<OnBeginDrag>c__AnonStorey8D::eventData
	PointerEventData_t1848751023 * ___eventData_0;

public:
	inline static int32_t get_offset_of_eventData_0() { return static_cast<int32_t>(offsetof(U3COnBeginDragU3Ec__AnonStorey8D_t1656158571, ___eventData_0)); }
	inline PointerEventData_t1848751023 * get_eventData_0() const { return ___eventData_0; }
	inline PointerEventData_t1848751023 ** get_address_of_eventData_0() { return &___eventData_0; }
	inline void set_eventData_0(PointerEventData_t1848751023 * value)
	{
		___eventData_0 = value;
		Il2CppCodeGenWriteBarrier(&___eventData_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
