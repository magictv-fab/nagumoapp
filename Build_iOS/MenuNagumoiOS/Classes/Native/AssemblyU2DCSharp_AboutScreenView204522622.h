﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.TextAsset
struct TextAsset_t3836129977;
// System.Action
struct Action_t3771233898;
// UnityEngine.GUIStyle
struct GUIStyle_t2990928826;
// System.String
struct String_t;
// UnityEngine.GUISkin
struct GUISkin_t3371348110;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
struct Dictionary_2_t3811347196;
// SampleAppUIBox
struct SampleAppUIBox_t3692040448;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AboutScreenView
struct  AboutScreenView_t204522622  : public Il2CppObject
{
public:
	// UnityEngine.TextAsset AboutScreenView::m_AboutText
	TextAsset_t3836129977 * ___m_AboutText_2;
	// System.Action AboutScreenView::OnStartButtonTapped
	Action_t3771233898 * ___OnStartButtonTapped_3;
	// UnityEngine.GUIStyle AboutScreenView::mAboutTitleBgStyle
	GUIStyle_t2990928826 * ___mAboutTitleBgStyle_4;
	// UnityEngine.GUIStyle AboutScreenView::mOKButtonBgStyle
	GUIStyle_t2990928826 * ___mOKButtonBgStyle_5;
	// System.String AboutScreenView::mTitle
	String_t* ___mTitle_6;
	// UnityEngine.GUISkin AboutScreenView::mUISkin
	GUISkin_t3371348110 * ___mUISkin_7;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle> AboutScreenView::mButtonGUIStyles
	Dictionary_2_t3811347196 * ___mButtonGUIStyles_8;
	// UnityEngine.Vector2 AboutScreenView::mScrollPosition
	Vector2_t4282066565  ___mScrollPosition_9;
	// System.Single AboutScreenView::mStartButtonAreaHeight
	float ___mStartButtonAreaHeight_10;
	// System.Single AboutScreenView::mAboutTitleHeight
	float ___mAboutTitleHeight_11;
	// UnityEngine.Vector2 AboutScreenView::mLastTouchPosition
	Vector2_t4282066565  ___mLastTouchPosition_12;
	// SampleAppUIBox AboutScreenView::mBox
	SampleAppUIBox_t3692040448 * ___mBox_13;

public:
	inline static int32_t get_offset_of_m_AboutText_2() { return static_cast<int32_t>(offsetof(AboutScreenView_t204522622, ___m_AboutText_2)); }
	inline TextAsset_t3836129977 * get_m_AboutText_2() const { return ___m_AboutText_2; }
	inline TextAsset_t3836129977 ** get_address_of_m_AboutText_2() { return &___m_AboutText_2; }
	inline void set_m_AboutText_2(TextAsset_t3836129977 * value)
	{
		___m_AboutText_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_AboutText_2, value);
	}

	inline static int32_t get_offset_of_OnStartButtonTapped_3() { return static_cast<int32_t>(offsetof(AboutScreenView_t204522622, ___OnStartButtonTapped_3)); }
	inline Action_t3771233898 * get_OnStartButtonTapped_3() const { return ___OnStartButtonTapped_3; }
	inline Action_t3771233898 ** get_address_of_OnStartButtonTapped_3() { return &___OnStartButtonTapped_3; }
	inline void set_OnStartButtonTapped_3(Action_t3771233898 * value)
	{
		___OnStartButtonTapped_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnStartButtonTapped_3, value);
	}

	inline static int32_t get_offset_of_mAboutTitleBgStyle_4() { return static_cast<int32_t>(offsetof(AboutScreenView_t204522622, ___mAboutTitleBgStyle_4)); }
	inline GUIStyle_t2990928826 * get_mAboutTitleBgStyle_4() const { return ___mAboutTitleBgStyle_4; }
	inline GUIStyle_t2990928826 ** get_address_of_mAboutTitleBgStyle_4() { return &___mAboutTitleBgStyle_4; }
	inline void set_mAboutTitleBgStyle_4(GUIStyle_t2990928826 * value)
	{
		___mAboutTitleBgStyle_4 = value;
		Il2CppCodeGenWriteBarrier(&___mAboutTitleBgStyle_4, value);
	}

	inline static int32_t get_offset_of_mOKButtonBgStyle_5() { return static_cast<int32_t>(offsetof(AboutScreenView_t204522622, ___mOKButtonBgStyle_5)); }
	inline GUIStyle_t2990928826 * get_mOKButtonBgStyle_5() const { return ___mOKButtonBgStyle_5; }
	inline GUIStyle_t2990928826 ** get_address_of_mOKButtonBgStyle_5() { return &___mOKButtonBgStyle_5; }
	inline void set_mOKButtonBgStyle_5(GUIStyle_t2990928826 * value)
	{
		___mOKButtonBgStyle_5 = value;
		Il2CppCodeGenWriteBarrier(&___mOKButtonBgStyle_5, value);
	}

	inline static int32_t get_offset_of_mTitle_6() { return static_cast<int32_t>(offsetof(AboutScreenView_t204522622, ___mTitle_6)); }
	inline String_t* get_mTitle_6() const { return ___mTitle_6; }
	inline String_t** get_address_of_mTitle_6() { return &___mTitle_6; }
	inline void set_mTitle_6(String_t* value)
	{
		___mTitle_6 = value;
		Il2CppCodeGenWriteBarrier(&___mTitle_6, value);
	}

	inline static int32_t get_offset_of_mUISkin_7() { return static_cast<int32_t>(offsetof(AboutScreenView_t204522622, ___mUISkin_7)); }
	inline GUISkin_t3371348110 * get_mUISkin_7() const { return ___mUISkin_7; }
	inline GUISkin_t3371348110 ** get_address_of_mUISkin_7() { return &___mUISkin_7; }
	inline void set_mUISkin_7(GUISkin_t3371348110 * value)
	{
		___mUISkin_7 = value;
		Il2CppCodeGenWriteBarrier(&___mUISkin_7, value);
	}

	inline static int32_t get_offset_of_mButtonGUIStyles_8() { return static_cast<int32_t>(offsetof(AboutScreenView_t204522622, ___mButtonGUIStyles_8)); }
	inline Dictionary_2_t3811347196 * get_mButtonGUIStyles_8() const { return ___mButtonGUIStyles_8; }
	inline Dictionary_2_t3811347196 ** get_address_of_mButtonGUIStyles_8() { return &___mButtonGUIStyles_8; }
	inline void set_mButtonGUIStyles_8(Dictionary_2_t3811347196 * value)
	{
		___mButtonGUIStyles_8 = value;
		Il2CppCodeGenWriteBarrier(&___mButtonGUIStyles_8, value);
	}

	inline static int32_t get_offset_of_mScrollPosition_9() { return static_cast<int32_t>(offsetof(AboutScreenView_t204522622, ___mScrollPosition_9)); }
	inline Vector2_t4282066565  get_mScrollPosition_9() const { return ___mScrollPosition_9; }
	inline Vector2_t4282066565 * get_address_of_mScrollPosition_9() { return &___mScrollPosition_9; }
	inline void set_mScrollPosition_9(Vector2_t4282066565  value)
	{
		___mScrollPosition_9 = value;
	}

	inline static int32_t get_offset_of_mStartButtonAreaHeight_10() { return static_cast<int32_t>(offsetof(AboutScreenView_t204522622, ___mStartButtonAreaHeight_10)); }
	inline float get_mStartButtonAreaHeight_10() const { return ___mStartButtonAreaHeight_10; }
	inline float* get_address_of_mStartButtonAreaHeight_10() { return &___mStartButtonAreaHeight_10; }
	inline void set_mStartButtonAreaHeight_10(float value)
	{
		___mStartButtonAreaHeight_10 = value;
	}

	inline static int32_t get_offset_of_mAboutTitleHeight_11() { return static_cast<int32_t>(offsetof(AboutScreenView_t204522622, ___mAboutTitleHeight_11)); }
	inline float get_mAboutTitleHeight_11() const { return ___mAboutTitleHeight_11; }
	inline float* get_address_of_mAboutTitleHeight_11() { return &___mAboutTitleHeight_11; }
	inline void set_mAboutTitleHeight_11(float value)
	{
		___mAboutTitleHeight_11 = value;
	}

	inline static int32_t get_offset_of_mLastTouchPosition_12() { return static_cast<int32_t>(offsetof(AboutScreenView_t204522622, ___mLastTouchPosition_12)); }
	inline Vector2_t4282066565  get_mLastTouchPosition_12() const { return ___mLastTouchPosition_12; }
	inline Vector2_t4282066565 * get_address_of_mLastTouchPosition_12() { return &___mLastTouchPosition_12; }
	inline void set_mLastTouchPosition_12(Vector2_t4282066565  value)
	{
		___mLastTouchPosition_12 = value;
	}

	inline static int32_t get_offset_of_mBox_13() { return static_cast<int32_t>(offsetof(AboutScreenView_t204522622, ___mBox_13)); }
	inline SampleAppUIBox_t3692040448 * get_mBox_13() const { return ___mBox_13; }
	inline SampleAppUIBox_t3692040448 ** get_address_of_mBox_13() { return &___mBox_13; }
	inline void set_mBox_13(SampleAppUIBox_t3692040448 * value)
	{
		___mBox_13 = value;
		Il2CppCodeGenWriteBarrier(&___mBox_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
