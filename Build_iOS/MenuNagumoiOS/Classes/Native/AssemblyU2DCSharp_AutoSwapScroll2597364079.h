﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Extensions.HorizontalScrollSnap
struct HorizontalScrollSnap_t2651831999;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AutoSwapScroll
struct  AutoSwapScroll_t2597364079  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.Extensions.HorizontalScrollSnap AutoSwapScroll::HorizontalScrollSnap
	HorizontalScrollSnap_t2651831999 * ___HorizontalScrollSnap_2;
	// System.Single AutoSwapScroll::time
	float ___time_3;
	// System.Single AutoSwapScroll::inativityTime
	float ___inativityTime_4;
	// System.Int32 AutoSwapScroll::counter
	int32_t ___counter_5;
	// System.Int32 AutoSwapScroll::dir
	int32_t ___dir_6;
	// System.Single AutoSwapScroll::inativityTimeCtrl
	float ___inativityTimeCtrl_7;

public:
	inline static int32_t get_offset_of_HorizontalScrollSnap_2() { return static_cast<int32_t>(offsetof(AutoSwapScroll_t2597364079, ___HorizontalScrollSnap_2)); }
	inline HorizontalScrollSnap_t2651831999 * get_HorizontalScrollSnap_2() const { return ___HorizontalScrollSnap_2; }
	inline HorizontalScrollSnap_t2651831999 ** get_address_of_HorizontalScrollSnap_2() { return &___HorizontalScrollSnap_2; }
	inline void set_HorizontalScrollSnap_2(HorizontalScrollSnap_t2651831999 * value)
	{
		___HorizontalScrollSnap_2 = value;
		Il2CppCodeGenWriteBarrier(&___HorizontalScrollSnap_2, value);
	}

	inline static int32_t get_offset_of_time_3() { return static_cast<int32_t>(offsetof(AutoSwapScroll_t2597364079, ___time_3)); }
	inline float get_time_3() const { return ___time_3; }
	inline float* get_address_of_time_3() { return &___time_3; }
	inline void set_time_3(float value)
	{
		___time_3 = value;
	}

	inline static int32_t get_offset_of_inativityTime_4() { return static_cast<int32_t>(offsetof(AutoSwapScroll_t2597364079, ___inativityTime_4)); }
	inline float get_inativityTime_4() const { return ___inativityTime_4; }
	inline float* get_address_of_inativityTime_4() { return &___inativityTime_4; }
	inline void set_inativityTime_4(float value)
	{
		___inativityTime_4 = value;
	}

	inline static int32_t get_offset_of_counter_5() { return static_cast<int32_t>(offsetof(AutoSwapScroll_t2597364079, ___counter_5)); }
	inline int32_t get_counter_5() const { return ___counter_5; }
	inline int32_t* get_address_of_counter_5() { return &___counter_5; }
	inline void set_counter_5(int32_t value)
	{
		___counter_5 = value;
	}

	inline static int32_t get_offset_of_dir_6() { return static_cast<int32_t>(offsetof(AutoSwapScroll_t2597364079, ___dir_6)); }
	inline int32_t get_dir_6() const { return ___dir_6; }
	inline int32_t* get_address_of_dir_6() { return &___dir_6; }
	inline void set_dir_6(int32_t value)
	{
		___dir_6 = value;
	}

	inline static int32_t get_offset_of_inativityTimeCtrl_7() { return static_cast<int32_t>(offsetof(AutoSwapScroll_t2597364079, ___inativityTimeCtrl_7)); }
	inline float get_inativityTimeCtrl_7() const { return ___inativityTimeCtrl_7; }
	inline float* get_address_of_inativityTimeCtrl_7() { return &___inativityTimeCtrl_7; }
	inline void set_inativityTimeCtrl_7(float value)
	{
		___inativityTimeCtrl_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
