﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NotificationsScroll
struct NotificationsScroll_t2084299189;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void NotificationsScroll::.ctor()
extern "C"  void NotificationsScroll__ctor_m4238101846 (NotificationsScroll_t2084299189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationsScroll::OnEnable()
extern "C"  void NotificationsScroll_OnEnable_m210072176 (NotificationsScroll_t2084299189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationsScroll::InstantiateNotifications(System.String,System.String,System.String)
extern "C"  void NotificationsScroll_InstantiateNotifications_m1428554158 (NotificationsScroll_t2084299189 * __this, String_t* ___title0, String_t* ___text1, String_t* ___dateTime2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
