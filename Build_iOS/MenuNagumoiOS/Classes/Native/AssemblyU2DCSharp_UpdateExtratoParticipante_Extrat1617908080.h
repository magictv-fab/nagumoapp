﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UpdateExtratoParticipante/Extrato
struct  Extrato_t1617908080  : public Il2CppObject
{
public:
	// System.Int32 UpdateExtratoParticipante/Extrato::total_spins
	int32_t ___total_spins_0;
	// System.Single UpdateExtratoParticipante/Extrato::saldo_acumulado
	float ___saldo_acumulado_1;
	// System.Single UpdateExtratoParticipante/Extrato::saldo_anterior
	float ___saldo_anterior_2;

public:
	inline static int32_t get_offset_of_total_spins_0() { return static_cast<int32_t>(offsetof(Extrato_t1617908080, ___total_spins_0)); }
	inline int32_t get_total_spins_0() const { return ___total_spins_0; }
	inline int32_t* get_address_of_total_spins_0() { return &___total_spins_0; }
	inline void set_total_spins_0(int32_t value)
	{
		___total_spins_0 = value;
	}

	inline static int32_t get_offset_of_saldo_acumulado_1() { return static_cast<int32_t>(offsetof(Extrato_t1617908080, ___saldo_acumulado_1)); }
	inline float get_saldo_acumulado_1() const { return ___saldo_acumulado_1; }
	inline float* get_address_of_saldo_acumulado_1() { return &___saldo_acumulado_1; }
	inline void set_saldo_acumulado_1(float value)
	{
		___saldo_acumulado_1 = value;
	}

	inline static int32_t get_offset_of_saldo_anterior_2() { return static_cast<int32_t>(offsetof(Extrato_t1617908080, ___saldo_anterior_2)); }
	inline float get_saldo_anterior_2() const { return ___saldo_anterior_2; }
	inline float* get_address_of_saldo_anterior_2() { return &___saldo_anterior_2; }
	inline void set_saldo_anterior_2(float value)
	{
		___saldo_anterior_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
