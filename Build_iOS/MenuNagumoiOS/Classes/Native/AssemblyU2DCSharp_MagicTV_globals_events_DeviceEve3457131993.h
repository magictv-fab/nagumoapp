﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MagicTV.globals.events.DeviceEvents/OnSomething
struct OnSomething_t1610687155;
// MagicTV.globals.events.DeviceEvents/OnString
struct OnString_t2272041080;
// MagicTV.globals.events.DeviceEvents
struct DeviceEvents_t3457131993;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.globals.events.DeviceEvents
struct  DeviceEvents_t3457131993  : public MonoBehaviour_t667441552
{
public:
	// MagicTV.globals.events.DeviceEvents/OnSomething MagicTV.globals.events.DeviceEvents::doVibrate
	OnSomething_t1610687155 * ___doVibrate_2;
	// MagicTV.globals.events.DeviceEvents/OnString MagicTV.globals.events.DeviceEvents::_onSendTokenToServer
	OnString_t2272041080 * ____onSendTokenToServer_3;
	// MagicTV.globals.events.DeviceEvents/OnString MagicTV.globals.events.DeviceEvents::_onTokenSavedIntoServer
	OnString_t2272041080 * ____onTokenSavedIntoServer_4;
	// MagicTV.globals.events.DeviceEvents/OnString MagicTV.globals.events.DeviceEvents::onPinRecieved
	OnString_t2272041080 * ___onPinRecieved_5;
	// MagicTV.globals.events.DeviceEvents/OnSomething MagicTV.globals.events.DeviceEvents::onReset
	OnSomething_t1610687155 * ___onReset_6;
	// MagicTV.globals.events.DeviceEvents/OnSomething MagicTV.globals.events.DeviceEvents::onClearCacheCompleteNothingToDelete
	OnSomething_t1610687155 * ___onClearCacheCompleteNothingToDelete_7;
	// MagicTV.globals.events.DeviceEvents/OnSomething MagicTV.globals.events.DeviceEvents::onClearCacheComplete
	OnSomething_t1610687155 * ___onClearCacheComplete_8;
	// MagicTV.globals.events.DeviceEvents/OnSomething MagicTV.globals.events.DeviceEvents::onClearCache
	OnSomething_t1610687155 * ___onClearCache_9;
	// MagicTV.globals.events.DeviceEvents/OnSomething MagicTV.globals.events.DeviceEvents::onWantToClearCache
	OnSomething_t1610687155 * ___onWantToClearCache_10;
	// MagicTV.globals.events.DeviceEvents/OnSomething MagicTV.globals.events.DeviceEvents::onResetClicked
	OnSomething_t1610687155 * ___onResetClicked_11;
	// MagicTV.globals.events.DeviceEvents/OnSomething MagicTV.globals.events.DeviceEvents::onResume
	OnSomething_t1610687155 * ___onResume_12;
	// MagicTV.globals.events.DeviceEvents/OnSomething MagicTV.globals.events.DeviceEvents::onPause
	OnSomething_t1610687155 * ___onPause_13;
	// MagicTV.globals.events.DeviceEvents/OnSomething MagicTV.globals.events.DeviceEvents::onAwake
	OnSomething_t1610687155 * ___onAwake_14;

public:
	inline static int32_t get_offset_of_doVibrate_2() { return static_cast<int32_t>(offsetof(DeviceEvents_t3457131993, ___doVibrate_2)); }
	inline OnSomething_t1610687155 * get_doVibrate_2() const { return ___doVibrate_2; }
	inline OnSomething_t1610687155 ** get_address_of_doVibrate_2() { return &___doVibrate_2; }
	inline void set_doVibrate_2(OnSomething_t1610687155 * value)
	{
		___doVibrate_2 = value;
		Il2CppCodeGenWriteBarrier(&___doVibrate_2, value);
	}

	inline static int32_t get_offset_of__onSendTokenToServer_3() { return static_cast<int32_t>(offsetof(DeviceEvents_t3457131993, ____onSendTokenToServer_3)); }
	inline OnString_t2272041080 * get__onSendTokenToServer_3() const { return ____onSendTokenToServer_3; }
	inline OnString_t2272041080 ** get_address_of__onSendTokenToServer_3() { return &____onSendTokenToServer_3; }
	inline void set__onSendTokenToServer_3(OnString_t2272041080 * value)
	{
		____onSendTokenToServer_3 = value;
		Il2CppCodeGenWriteBarrier(&____onSendTokenToServer_3, value);
	}

	inline static int32_t get_offset_of__onTokenSavedIntoServer_4() { return static_cast<int32_t>(offsetof(DeviceEvents_t3457131993, ____onTokenSavedIntoServer_4)); }
	inline OnString_t2272041080 * get__onTokenSavedIntoServer_4() const { return ____onTokenSavedIntoServer_4; }
	inline OnString_t2272041080 ** get_address_of__onTokenSavedIntoServer_4() { return &____onTokenSavedIntoServer_4; }
	inline void set__onTokenSavedIntoServer_4(OnString_t2272041080 * value)
	{
		____onTokenSavedIntoServer_4 = value;
		Il2CppCodeGenWriteBarrier(&____onTokenSavedIntoServer_4, value);
	}

	inline static int32_t get_offset_of_onPinRecieved_5() { return static_cast<int32_t>(offsetof(DeviceEvents_t3457131993, ___onPinRecieved_5)); }
	inline OnString_t2272041080 * get_onPinRecieved_5() const { return ___onPinRecieved_5; }
	inline OnString_t2272041080 ** get_address_of_onPinRecieved_5() { return &___onPinRecieved_5; }
	inline void set_onPinRecieved_5(OnString_t2272041080 * value)
	{
		___onPinRecieved_5 = value;
		Il2CppCodeGenWriteBarrier(&___onPinRecieved_5, value);
	}

	inline static int32_t get_offset_of_onReset_6() { return static_cast<int32_t>(offsetof(DeviceEvents_t3457131993, ___onReset_6)); }
	inline OnSomething_t1610687155 * get_onReset_6() const { return ___onReset_6; }
	inline OnSomething_t1610687155 ** get_address_of_onReset_6() { return &___onReset_6; }
	inline void set_onReset_6(OnSomething_t1610687155 * value)
	{
		___onReset_6 = value;
		Il2CppCodeGenWriteBarrier(&___onReset_6, value);
	}

	inline static int32_t get_offset_of_onClearCacheCompleteNothingToDelete_7() { return static_cast<int32_t>(offsetof(DeviceEvents_t3457131993, ___onClearCacheCompleteNothingToDelete_7)); }
	inline OnSomething_t1610687155 * get_onClearCacheCompleteNothingToDelete_7() const { return ___onClearCacheCompleteNothingToDelete_7; }
	inline OnSomething_t1610687155 ** get_address_of_onClearCacheCompleteNothingToDelete_7() { return &___onClearCacheCompleteNothingToDelete_7; }
	inline void set_onClearCacheCompleteNothingToDelete_7(OnSomething_t1610687155 * value)
	{
		___onClearCacheCompleteNothingToDelete_7 = value;
		Il2CppCodeGenWriteBarrier(&___onClearCacheCompleteNothingToDelete_7, value);
	}

	inline static int32_t get_offset_of_onClearCacheComplete_8() { return static_cast<int32_t>(offsetof(DeviceEvents_t3457131993, ___onClearCacheComplete_8)); }
	inline OnSomething_t1610687155 * get_onClearCacheComplete_8() const { return ___onClearCacheComplete_8; }
	inline OnSomething_t1610687155 ** get_address_of_onClearCacheComplete_8() { return &___onClearCacheComplete_8; }
	inline void set_onClearCacheComplete_8(OnSomething_t1610687155 * value)
	{
		___onClearCacheComplete_8 = value;
		Il2CppCodeGenWriteBarrier(&___onClearCacheComplete_8, value);
	}

	inline static int32_t get_offset_of_onClearCache_9() { return static_cast<int32_t>(offsetof(DeviceEvents_t3457131993, ___onClearCache_9)); }
	inline OnSomething_t1610687155 * get_onClearCache_9() const { return ___onClearCache_9; }
	inline OnSomething_t1610687155 ** get_address_of_onClearCache_9() { return &___onClearCache_9; }
	inline void set_onClearCache_9(OnSomething_t1610687155 * value)
	{
		___onClearCache_9 = value;
		Il2CppCodeGenWriteBarrier(&___onClearCache_9, value);
	}

	inline static int32_t get_offset_of_onWantToClearCache_10() { return static_cast<int32_t>(offsetof(DeviceEvents_t3457131993, ___onWantToClearCache_10)); }
	inline OnSomething_t1610687155 * get_onWantToClearCache_10() const { return ___onWantToClearCache_10; }
	inline OnSomething_t1610687155 ** get_address_of_onWantToClearCache_10() { return &___onWantToClearCache_10; }
	inline void set_onWantToClearCache_10(OnSomething_t1610687155 * value)
	{
		___onWantToClearCache_10 = value;
		Il2CppCodeGenWriteBarrier(&___onWantToClearCache_10, value);
	}

	inline static int32_t get_offset_of_onResetClicked_11() { return static_cast<int32_t>(offsetof(DeviceEvents_t3457131993, ___onResetClicked_11)); }
	inline OnSomething_t1610687155 * get_onResetClicked_11() const { return ___onResetClicked_11; }
	inline OnSomething_t1610687155 ** get_address_of_onResetClicked_11() { return &___onResetClicked_11; }
	inline void set_onResetClicked_11(OnSomething_t1610687155 * value)
	{
		___onResetClicked_11 = value;
		Il2CppCodeGenWriteBarrier(&___onResetClicked_11, value);
	}

	inline static int32_t get_offset_of_onResume_12() { return static_cast<int32_t>(offsetof(DeviceEvents_t3457131993, ___onResume_12)); }
	inline OnSomething_t1610687155 * get_onResume_12() const { return ___onResume_12; }
	inline OnSomething_t1610687155 ** get_address_of_onResume_12() { return &___onResume_12; }
	inline void set_onResume_12(OnSomething_t1610687155 * value)
	{
		___onResume_12 = value;
		Il2CppCodeGenWriteBarrier(&___onResume_12, value);
	}

	inline static int32_t get_offset_of_onPause_13() { return static_cast<int32_t>(offsetof(DeviceEvents_t3457131993, ___onPause_13)); }
	inline OnSomething_t1610687155 * get_onPause_13() const { return ___onPause_13; }
	inline OnSomething_t1610687155 ** get_address_of_onPause_13() { return &___onPause_13; }
	inline void set_onPause_13(OnSomething_t1610687155 * value)
	{
		___onPause_13 = value;
		Il2CppCodeGenWriteBarrier(&___onPause_13, value);
	}

	inline static int32_t get_offset_of_onAwake_14() { return static_cast<int32_t>(offsetof(DeviceEvents_t3457131993, ___onAwake_14)); }
	inline OnSomething_t1610687155 * get_onAwake_14() const { return ___onAwake_14; }
	inline OnSomething_t1610687155 ** get_address_of_onAwake_14() { return &___onAwake_14; }
	inline void set_onAwake_14(OnSomething_t1610687155 * value)
	{
		___onAwake_14 = value;
		Il2CppCodeGenWriteBarrier(&___onAwake_14, value);
	}
};

struct DeviceEvents_t3457131993_StaticFields
{
public:
	// MagicTV.globals.events.DeviceEvents MagicTV.globals.events.DeviceEvents::_instance
	DeviceEvents_t3457131993 * ____instance_15;

public:
	inline static int32_t get_offset_of__instance_15() { return static_cast<int32_t>(offsetof(DeviceEvents_t3457131993_StaticFields, ____instance_15)); }
	inline DeviceEvents_t3457131993 * get__instance_15() const { return ____instance_15; }
	inline DeviceEvents_t3457131993 ** get_address_of__instance_15() { return &____instance_15; }
	inline void set__instance_15(DeviceEvents_t3457131993 * value)
	{
		____instance_15 = value;
		Il2CppCodeGenWriteBarrier(&____instance_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
