﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// shareScriptAndroid
struct shareScriptAndroid_t2567206277;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void shareScriptAndroid::.ctor()
extern "C"  void shareScriptAndroid__ctor_m2658988214 (shareScriptAndroid_t2567206277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void shareScriptAndroid::ShareBtnPress()
extern "C"  void shareScriptAndroid_ShareBtnPress_m1602293146 (shareScriptAndroid_t2567206277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator shareScriptAndroid::ShareScreenshot()
extern "C"  Il2CppObject * shareScriptAndroid_ShareScreenshot_m2227735089 (shareScriptAndroid_t2567206277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void shareScriptAndroid::OnApplicationFocus(System.Boolean)
extern "C"  void shareScriptAndroid_OnApplicationFocus_m62283628 (shareScriptAndroid_t2567206277 * __this, bool ___focus0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
