﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3363211663MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,ARM.events.GenericParameterEventDispatcher>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2353517751(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1654092311 *, Dictionary_2_t336768919 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,ARM.events.GenericParameterEventDispatcher>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3665809236(__this, method) ((  Il2CppObject * (*) (Enumerator_t1654092311 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,ARM.events.GenericParameterEventDispatcher>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2329434014(__this, method) ((  void (*) (Enumerator_t1654092311 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,ARM.events.GenericParameterEventDispatcher>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m615955349(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t1654092311 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,ARM.events.GenericParameterEventDispatcher>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m306396464(__this, method) ((  Il2CppObject * (*) (Enumerator_t1654092311 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,ARM.events.GenericParameterEventDispatcher>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3437662146(__this, method) ((  Il2CppObject * (*) (Enumerator_t1654092311 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,ARM.events.GenericParameterEventDispatcher>::MoveNext()
#define Enumerator_MoveNext_m2071566030(__this, method) ((  bool (*) (Enumerator_t1654092311 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,ARM.events.GenericParameterEventDispatcher>::get_Current()
#define Enumerator_get_Current_m3429048494(__this, method) ((  KeyValuePair_2_t235549625  (*) (Enumerator_t1654092311 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,ARM.events.GenericParameterEventDispatcher>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m844819031(__this, method) ((  String_t* (*) (Enumerator_t1654092311 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,ARM.events.GenericParameterEventDispatcher>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3233356375(__this, method) ((  GenericParameterEventDispatcher_t3811317845 * (*) (Enumerator_t1654092311 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,ARM.events.GenericParameterEventDispatcher>::Reset()
#define Enumerator_Reset_m3134540937(__this, method) ((  void (*) (Enumerator_t1654092311 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,ARM.events.GenericParameterEventDispatcher>::VerifyState()
#define Enumerator_VerifyState_m1643467218(__this, method) ((  void (*) (Enumerator_t1654092311 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,ARM.events.GenericParameterEventDispatcher>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1568903354(__this, method) ((  void (*) (Enumerator_t1654092311 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,ARM.events.GenericParameterEventDispatcher>::Dispose()
#define Enumerator_Dispose_m4045195033(__this, method) ((  void (*) (Enumerator_t1654092311 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
