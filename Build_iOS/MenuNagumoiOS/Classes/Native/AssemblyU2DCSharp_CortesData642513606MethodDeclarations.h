﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CortesData
struct CortesData_t642513606;

#include "codegen/il2cpp-codegen.h"

// System.Void CortesData::.ctor()
extern "C"  void CortesData__ctor_m52845717 (CortesData_t642513606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
