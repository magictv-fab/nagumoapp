﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.ZipFile/UpdateComparer
struct UpdateComparer_t3266384358;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/UpdateComparer::.ctor()
extern "C"  void UpdateComparer__ctor_m661475701 (UpdateComparer_t3266384358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipFile/UpdateComparer::Compare(System.Object,System.Object)
extern "C"  int32_t UpdateComparer_Compare_m3240303800 (UpdateComparer_t3266384358 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
