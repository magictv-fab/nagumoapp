﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CuponsPontos
struct CuponsPontos_t785589215;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"

// System.Void CuponsPontos::.ctor()
extern "C"  void CuponsPontos__ctor_m751155612 (CuponsPontos_t785589215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CuponsPontos::Start()
extern "C"  void CuponsPontos_Start_m3993260700 (CuponsPontos_t785589215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CuponsPontos::Update()
extern "C"  void CuponsPontos_Update_m3537849585 (CuponsPontos_t785589215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CuponsPontos::QREncodeFinished(UnityEngine.Texture2D)
extern "C"  void CuponsPontos_QREncodeFinished_m1260610123 (CuponsPontos_t785589215 * __this, Texture2D_t3884108195 * ___tex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CuponsPontos::Encode()
extern "C"  void CuponsPontos_Encode_m3926471422 (CuponsPontos_t785589215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
