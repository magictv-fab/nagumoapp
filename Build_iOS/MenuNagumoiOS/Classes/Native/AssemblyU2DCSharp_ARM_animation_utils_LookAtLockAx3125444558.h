﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.animation.utils.LookAtLockAxis
struct  LookAtLockAxis_t3125444558  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean ARM.animation.utils.LookAtLockAxis::lockX
	bool ___lockX_2;
	// System.Boolean ARM.animation.utils.LookAtLockAxis::lockY
	bool ___lockY_3;
	// System.Boolean ARM.animation.utils.LookAtLockAxis::lockZ
	bool ___lockZ_4;
	// System.Single ARM.animation.utils.LookAtLockAxis::calibrationX
	float ___calibrationX_5;
	// System.Single ARM.animation.utils.LookAtLockAxis::calibrationY
	float ___calibrationY_6;
	// System.Single ARM.animation.utils.LookAtLockAxis::calibrationZ
	float ___calibrationZ_7;
	// UnityEngine.GameObject ARM.animation.utils.LookAtLockAxis::targetToLook
	GameObject_t3674682005 * ___targetToLook_8;
	// System.Boolean ARM.animation.utils.LookAtLockAxis::lookToCamera
	bool ___lookToCamera_9;
	// UnityEngine.Vector3 ARM.animation.utils.LookAtLockAxis::debugRotation
	Vector3_t4282066566  ___debugRotation_10;
	// UnityEngine.Quaternion ARM.animation.utils.LookAtLockAxis::debugRotationQuaternion
	Quaternion_t1553702882  ___debugRotationQuaternion_11;
	// System.Boolean ARM.animation.utils.LookAtLockAxis::_hasTarget
	bool ____hasTarget_12;

public:
	inline static int32_t get_offset_of_lockX_2() { return static_cast<int32_t>(offsetof(LookAtLockAxis_t3125444558, ___lockX_2)); }
	inline bool get_lockX_2() const { return ___lockX_2; }
	inline bool* get_address_of_lockX_2() { return &___lockX_2; }
	inline void set_lockX_2(bool value)
	{
		___lockX_2 = value;
	}

	inline static int32_t get_offset_of_lockY_3() { return static_cast<int32_t>(offsetof(LookAtLockAxis_t3125444558, ___lockY_3)); }
	inline bool get_lockY_3() const { return ___lockY_3; }
	inline bool* get_address_of_lockY_3() { return &___lockY_3; }
	inline void set_lockY_3(bool value)
	{
		___lockY_3 = value;
	}

	inline static int32_t get_offset_of_lockZ_4() { return static_cast<int32_t>(offsetof(LookAtLockAxis_t3125444558, ___lockZ_4)); }
	inline bool get_lockZ_4() const { return ___lockZ_4; }
	inline bool* get_address_of_lockZ_4() { return &___lockZ_4; }
	inline void set_lockZ_4(bool value)
	{
		___lockZ_4 = value;
	}

	inline static int32_t get_offset_of_calibrationX_5() { return static_cast<int32_t>(offsetof(LookAtLockAxis_t3125444558, ___calibrationX_5)); }
	inline float get_calibrationX_5() const { return ___calibrationX_5; }
	inline float* get_address_of_calibrationX_5() { return &___calibrationX_5; }
	inline void set_calibrationX_5(float value)
	{
		___calibrationX_5 = value;
	}

	inline static int32_t get_offset_of_calibrationY_6() { return static_cast<int32_t>(offsetof(LookAtLockAxis_t3125444558, ___calibrationY_6)); }
	inline float get_calibrationY_6() const { return ___calibrationY_6; }
	inline float* get_address_of_calibrationY_6() { return &___calibrationY_6; }
	inline void set_calibrationY_6(float value)
	{
		___calibrationY_6 = value;
	}

	inline static int32_t get_offset_of_calibrationZ_7() { return static_cast<int32_t>(offsetof(LookAtLockAxis_t3125444558, ___calibrationZ_7)); }
	inline float get_calibrationZ_7() const { return ___calibrationZ_7; }
	inline float* get_address_of_calibrationZ_7() { return &___calibrationZ_7; }
	inline void set_calibrationZ_7(float value)
	{
		___calibrationZ_7 = value;
	}

	inline static int32_t get_offset_of_targetToLook_8() { return static_cast<int32_t>(offsetof(LookAtLockAxis_t3125444558, ___targetToLook_8)); }
	inline GameObject_t3674682005 * get_targetToLook_8() const { return ___targetToLook_8; }
	inline GameObject_t3674682005 ** get_address_of_targetToLook_8() { return &___targetToLook_8; }
	inline void set_targetToLook_8(GameObject_t3674682005 * value)
	{
		___targetToLook_8 = value;
		Il2CppCodeGenWriteBarrier(&___targetToLook_8, value);
	}

	inline static int32_t get_offset_of_lookToCamera_9() { return static_cast<int32_t>(offsetof(LookAtLockAxis_t3125444558, ___lookToCamera_9)); }
	inline bool get_lookToCamera_9() const { return ___lookToCamera_9; }
	inline bool* get_address_of_lookToCamera_9() { return &___lookToCamera_9; }
	inline void set_lookToCamera_9(bool value)
	{
		___lookToCamera_9 = value;
	}

	inline static int32_t get_offset_of_debugRotation_10() { return static_cast<int32_t>(offsetof(LookAtLockAxis_t3125444558, ___debugRotation_10)); }
	inline Vector3_t4282066566  get_debugRotation_10() const { return ___debugRotation_10; }
	inline Vector3_t4282066566 * get_address_of_debugRotation_10() { return &___debugRotation_10; }
	inline void set_debugRotation_10(Vector3_t4282066566  value)
	{
		___debugRotation_10 = value;
	}

	inline static int32_t get_offset_of_debugRotationQuaternion_11() { return static_cast<int32_t>(offsetof(LookAtLockAxis_t3125444558, ___debugRotationQuaternion_11)); }
	inline Quaternion_t1553702882  get_debugRotationQuaternion_11() const { return ___debugRotationQuaternion_11; }
	inline Quaternion_t1553702882 * get_address_of_debugRotationQuaternion_11() { return &___debugRotationQuaternion_11; }
	inline void set_debugRotationQuaternion_11(Quaternion_t1553702882  value)
	{
		___debugRotationQuaternion_11 = value;
	}

	inline static int32_t get_offset_of__hasTarget_12() { return static_cast<int32_t>(offsetof(LookAtLockAxis_t3125444558, ____hasTarget_12)); }
	inline bool get__hasTarget_12() const { return ____hasTarget_12; }
	inline bool* get_address_of__hasTarget_12() { return &____hasTarget_12; }
	inline void set__hasTarget_12(bool value)
	{
		____hasTarget_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
