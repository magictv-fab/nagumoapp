﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CaptureAndSaveScreen
struct CaptureAndSaveScreen_t237891578;

#include "codegen/il2cpp-codegen.h"

// System.Void CaptureAndSaveScreen::.ctor()
extern "C"  void CaptureAndSaveScreen__ctor_m3421488097 (CaptureAndSaveScreen_t237891578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
