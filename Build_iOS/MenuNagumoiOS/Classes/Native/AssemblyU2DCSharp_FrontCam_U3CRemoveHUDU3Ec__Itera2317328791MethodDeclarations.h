﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FrontCam/<RemoveHUD>c__Iterator59
struct U3CRemoveHUDU3Ec__Iterator59_t2317328791;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void FrontCam/<RemoveHUD>c__Iterator59::.ctor()
extern "C"  void U3CRemoveHUDU3Ec__Iterator59__ctor_m1395922356 (U3CRemoveHUDU3Ec__Iterator59_t2317328791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FrontCam/<RemoveHUD>c__Iterator59::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRemoveHUDU3Ec__Iterator59_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m580704094 (U3CRemoveHUDU3Ec__Iterator59_t2317328791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FrontCam/<RemoveHUD>c__Iterator59::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRemoveHUDU3Ec__Iterator59_System_Collections_IEnumerator_get_Current_m3027588850 (U3CRemoveHUDU3Ec__Iterator59_t2317328791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FrontCam/<RemoveHUD>c__Iterator59::MoveNext()
extern "C"  bool U3CRemoveHUDU3Ec__Iterator59_MoveNext_m1422047424 (U3CRemoveHUDU3Ec__Iterator59_t2317328791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FrontCam/<RemoveHUD>c__Iterator59::Dispose()
extern "C"  void U3CRemoveHUDU3Ec__Iterator59_Dispose_m1349870833 (U3CRemoveHUDU3Ec__Iterator59_t2317328791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FrontCam/<RemoveHUD>c__Iterator59::Reset()
extern "C"  void U3CRemoveHUDU3Ec__Iterator59_Reset_m3337322593 (U3CRemoveHUDU3Ec__Iterator59_t2317328791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
