﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ICSharpCode.SharpZipLib.Core.NameFilter
struct NameFilter_t123192849;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Core.PathFilter
struct  PathFilter_t1707447339  : public Il2CppObject
{
public:
	// ICSharpCode.SharpZipLib.Core.NameFilter ICSharpCode.SharpZipLib.Core.PathFilter::nameFilter_
	NameFilter_t123192849 * ___nameFilter__0;

public:
	inline static int32_t get_offset_of_nameFilter__0() { return static_cast<int32_t>(offsetof(PathFilter_t1707447339, ___nameFilter__0)); }
	inline NameFilter_t123192849 * get_nameFilter__0() const { return ___nameFilter__0; }
	inline NameFilter_t123192849 ** get_address_of_nameFilter__0() { return &___nameFilter__0; }
	inline void set_nameFilter__0(NameFilter_t123192849 * value)
	{
		___nameFilter__0 = value;
		Il2CppCodeGenWriteBarrier(&___nameFilter__0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
