﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAxis
struct  GetAxis_t1738227749  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetAxis::axisName
	FsmString_t952858651 * ___axisName_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAxis::multiplier
	FsmFloat_t2134102846 * ___multiplier_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAxis::store
	FsmFloat_t2134102846 * ___store_11;
	// System.Boolean HutongGames.PlayMaker.Actions.GetAxis::everyFrame
	bool ___everyFrame_12;

public:
	inline static int32_t get_offset_of_axisName_9() { return static_cast<int32_t>(offsetof(GetAxis_t1738227749, ___axisName_9)); }
	inline FsmString_t952858651 * get_axisName_9() const { return ___axisName_9; }
	inline FsmString_t952858651 ** get_address_of_axisName_9() { return &___axisName_9; }
	inline void set_axisName_9(FsmString_t952858651 * value)
	{
		___axisName_9 = value;
		Il2CppCodeGenWriteBarrier(&___axisName_9, value);
	}

	inline static int32_t get_offset_of_multiplier_10() { return static_cast<int32_t>(offsetof(GetAxis_t1738227749, ___multiplier_10)); }
	inline FsmFloat_t2134102846 * get_multiplier_10() const { return ___multiplier_10; }
	inline FsmFloat_t2134102846 ** get_address_of_multiplier_10() { return &___multiplier_10; }
	inline void set_multiplier_10(FsmFloat_t2134102846 * value)
	{
		___multiplier_10 = value;
		Il2CppCodeGenWriteBarrier(&___multiplier_10, value);
	}

	inline static int32_t get_offset_of_store_11() { return static_cast<int32_t>(offsetof(GetAxis_t1738227749, ___store_11)); }
	inline FsmFloat_t2134102846 * get_store_11() const { return ___store_11; }
	inline FsmFloat_t2134102846 ** get_address_of_store_11() { return &___store_11; }
	inline void set_store_11(FsmFloat_t2134102846 * value)
	{
		___store_11 = value;
		Il2CppCodeGenWriteBarrier(&___store_11, value);
	}

	inline static int32_t get_offset_of_everyFrame_12() { return static_cast<int32_t>(offsetof(GetAxis_t1738227749, ___everyFrame_12)); }
	inline bool get_everyFrame_12() const { return ___everyFrame_12; }
	inline bool* get_address_of_everyFrame_12() { return &___everyFrame_12; }
	inline void set_everyFrame_12(bool value)
	{
		___everyFrame_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
