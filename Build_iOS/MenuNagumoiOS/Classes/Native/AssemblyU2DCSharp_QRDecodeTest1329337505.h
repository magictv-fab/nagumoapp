﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// QRCodeDecodeController
struct QRCodeDecodeController_t2145328120;
// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QRDecodeTest
struct  QRDecodeTest_t1329337505  : public MonoBehaviour_t667441552
{
public:
	// QRCodeDecodeController QRDecodeTest::e_qrController
	QRCodeDecodeController_t2145328120 * ___e_qrController_2;
	// UnityEngine.UI.Text QRDecodeTest::UiText
	Text_t9039225 * ___UiText_3;
	// UnityEngine.GameObject QRDecodeTest::resetBtn
	GameObject_t3674682005 * ___resetBtn_4;
	// UnityEngine.GameObject QRDecodeTest::scanLineObj
	GameObject_t3674682005 * ___scanLineObj_5;
	// System.Boolean QRDecodeTest::isOpenBrowserIfUrl
	bool ___isOpenBrowserIfUrl_6;

public:
	inline static int32_t get_offset_of_e_qrController_2() { return static_cast<int32_t>(offsetof(QRDecodeTest_t1329337505, ___e_qrController_2)); }
	inline QRCodeDecodeController_t2145328120 * get_e_qrController_2() const { return ___e_qrController_2; }
	inline QRCodeDecodeController_t2145328120 ** get_address_of_e_qrController_2() { return &___e_qrController_2; }
	inline void set_e_qrController_2(QRCodeDecodeController_t2145328120 * value)
	{
		___e_qrController_2 = value;
		Il2CppCodeGenWriteBarrier(&___e_qrController_2, value);
	}

	inline static int32_t get_offset_of_UiText_3() { return static_cast<int32_t>(offsetof(QRDecodeTest_t1329337505, ___UiText_3)); }
	inline Text_t9039225 * get_UiText_3() const { return ___UiText_3; }
	inline Text_t9039225 ** get_address_of_UiText_3() { return &___UiText_3; }
	inline void set_UiText_3(Text_t9039225 * value)
	{
		___UiText_3 = value;
		Il2CppCodeGenWriteBarrier(&___UiText_3, value);
	}

	inline static int32_t get_offset_of_resetBtn_4() { return static_cast<int32_t>(offsetof(QRDecodeTest_t1329337505, ___resetBtn_4)); }
	inline GameObject_t3674682005 * get_resetBtn_4() const { return ___resetBtn_4; }
	inline GameObject_t3674682005 ** get_address_of_resetBtn_4() { return &___resetBtn_4; }
	inline void set_resetBtn_4(GameObject_t3674682005 * value)
	{
		___resetBtn_4 = value;
		Il2CppCodeGenWriteBarrier(&___resetBtn_4, value);
	}

	inline static int32_t get_offset_of_scanLineObj_5() { return static_cast<int32_t>(offsetof(QRDecodeTest_t1329337505, ___scanLineObj_5)); }
	inline GameObject_t3674682005 * get_scanLineObj_5() const { return ___scanLineObj_5; }
	inline GameObject_t3674682005 ** get_address_of_scanLineObj_5() { return &___scanLineObj_5; }
	inline void set_scanLineObj_5(GameObject_t3674682005 * value)
	{
		___scanLineObj_5 = value;
		Il2CppCodeGenWriteBarrier(&___scanLineObj_5, value);
	}

	inline static int32_t get_offset_of_isOpenBrowserIfUrl_6() { return static_cast<int32_t>(offsetof(QRDecodeTest_t1329337505, ___isOpenBrowserIfUrl_6)); }
	inline bool get_isOpenBrowserIfUrl_6() const { return ___isOpenBrowserIfUrl_6; }
	inline bool* get_address_of_isOpenBrowserIfUrl_6() { return &___isOpenBrowserIfUrl_6; }
	inline void set_isOpenBrowserIfUrl_6(bool value)
	{
		___isOpenBrowserIfUrl_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
