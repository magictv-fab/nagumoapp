﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>
struct Dictionary_2_t1289697713;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3516498417.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ZXing.BarcodeFormat,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3216901296_gshared (Enumerator_t3516498417 * __this, Dictionary_2_t1289697713 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m3216901296(__this, ___host0, method) ((  void (*) (Enumerator_t3516498417 *, Dictionary_2_t1289697713 *, const MethodInfo*))Enumerator__ctor_m3216901296_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ZXing.BarcodeFormat,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1817067953_gshared (Enumerator_t3516498417 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1817067953(__this, method) ((  Il2CppObject * (*) (Enumerator_t3516498417 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1817067953_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ZXing.BarcodeFormat,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4044619269_gshared (Enumerator_t3516498417 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m4044619269(__this, method) ((  void (*) (Enumerator_t3516498417 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4044619269_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ZXing.BarcodeFormat,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2312984402_gshared (Enumerator_t3516498417 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2312984402(__this, method) ((  void (*) (Enumerator_t3516498417 *, const MethodInfo*))Enumerator_Dispose_m2312984402_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ZXing.BarcodeFormat,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4236020209_gshared (Enumerator_t3516498417 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m4236020209(__this, method) ((  bool (*) (Enumerator_t3516498417 *, const MethodInfo*))Enumerator_MoveNext_m4236020209_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ZXing.BarcodeFormat,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m3353497073_gshared (Enumerator_t3516498417 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3353497073(__this, method) ((  Il2CppObject * (*) (Enumerator_t3516498417 *, const MethodInfo*))Enumerator_get_Current_m3353497073_gshared)(__this, method)
