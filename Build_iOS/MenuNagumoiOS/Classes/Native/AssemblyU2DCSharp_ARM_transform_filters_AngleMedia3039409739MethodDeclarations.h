﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.transform.filters.AngleMedianFilter
struct AngleMedianFilter_t3039409739;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_String7231557.h"

// System.Void ARM.transform.filters.AngleMedianFilter::.ctor()
extern "C"  void AngleMedianFilter__ctor_m1611151261 (AngleMedianFilter_t3039409739 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.AngleMedianFilter::Start()
extern "C"  void AngleMedianFilter_Start_m558289053 (AngleMedianFilter_t3039409739 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.AngleMedianFilter::Update()
extern "C"  void AngleMedianFilter_Update_m132943632 (AngleMedianFilter_t3039409739 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.AngleMedianFilter::reset()
extern "C"  void AngleMedianFilter_reset_m1887898218 (AngleMedianFilter_t3039409739 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion ARM.transform.filters.AngleMedianFilter::_doFilter(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  AngleMedianFilter__doFilter_m189663265 (AngleMedianFilter_t3039409739 * __this, Quaternion_t1553702882  ___currentPosition0, Quaternion_t1553702882  ___newValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.transform.filters.AngleMedianFilter::_doFilter(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  AngleMedianFilter__doFilter_m3583821301 (AngleMedianFilter_t3039409739 * __this, Vector3_t4282066566  ___currentValue0, Vector3_t4282066566  ___nextValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.AngleMedianFilter::save(UnityEngine.Quaternion)
extern "C"  void AngleMedianFilter_save_m1022046447 (AngleMedianFilter_t3039409739 * __this, Quaternion_t1553702882  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.AngleMedianFilter::configByString(System.String)
extern "C"  void AngleMedianFilter_configByString_m2892503569 (AngleMedianFilter_t3039409739 * __this, String_t* ___metadata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.AngleMedianFilter::Reset()
extern "C"  void AngleMedianFilter_Reset_m3552551498 (AngleMedianFilter_t3039409739 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.transform.filters.AngleMedianFilter::changeHistorySize(System.Int32)
extern "C"  void AngleMedianFilter_changeHistorySize_m1540952785 (AngleMedianFilter_t3039409739 * __this, int32_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
