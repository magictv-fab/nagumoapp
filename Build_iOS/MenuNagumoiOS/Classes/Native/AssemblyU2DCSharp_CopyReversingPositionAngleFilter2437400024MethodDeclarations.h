﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CopyReversingPositionAngleFilter
struct CopyReversingPositionAngleFilter_t2437400024;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

// System.Void CopyReversingPositionAngleFilter::.ctor()
extern "C"  void CopyReversingPositionAngleFilter__ctor_m3420668867 (CopyReversingPositionAngleFilter_t2437400024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 CopyReversingPositionAngleFilter::doCopyPosition(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  CopyReversingPositionAngleFilter_doCopyPosition_m843780021 (CopyReversingPositionAngleFilter_t2437400024 * __this, Vector3_t4282066566  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion CopyReversingPositionAngleFilter::doCopyRotation(UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  CopyReversingPositionAngleFilter_doCopyRotation_m1476360530 (CopyReversingPositionAngleFilter_t2437400024 * __this, Quaternion_t1553702882  ___rot0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
