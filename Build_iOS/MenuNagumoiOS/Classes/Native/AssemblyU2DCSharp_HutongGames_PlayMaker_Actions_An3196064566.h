﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AnimatorStopRecording
struct  AnimatorStopRecording_t3196064566  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AnimatorStopRecording::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AnimatorStopRecording::recorderStartTime
	FsmFloat_t2134102846 * ___recorderStartTime_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AnimatorStopRecording::recorderStopTime
	FsmFloat_t2134102846 * ___recorderStopTime_11;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(AnimatorStopRecording_t3196064566, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_recorderStartTime_10() { return static_cast<int32_t>(offsetof(AnimatorStopRecording_t3196064566, ___recorderStartTime_10)); }
	inline FsmFloat_t2134102846 * get_recorderStartTime_10() const { return ___recorderStartTime_10; }
	inline FsmFloat_t2134102846 ** get_address_of_recorderStartTime_10() { return &___recorderStartTime_10; }
	inline void set_recorderStartTime_10(FsmFloat_t2134102846 * value)
	{
		___recorderStartTime_10 = value;
		Il2CppCodeGenWriteBarrier(&___recorderStartTime_10, value);
	}

	inline static int32_t get_offset_of_recorderStopTime_11() { return static_cast<int32_t>(offsetof(AnimatorStopRecording_t3196064566, ___recorderStopTime_11)); }
	inline FsmFloat_t2134102846 * get_recorderStopTime_11() const { return ___recorderStopTime_11; }
	inline FsmFloat_t2134102846 ** get_address_of_recorderStopTime_11() { return &___recorderStopTime_11; }
	inline void set_recorderStopTime_11(FsmFloat_t2134102846 * value)
	{
		___recorderStopTime_11 = value;
		Il2CppCodeGenWriteBarrier(&___recorderStopTime_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
