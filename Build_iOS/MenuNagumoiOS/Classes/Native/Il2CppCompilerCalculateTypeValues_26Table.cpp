﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_ECI1296865545.h"
#include "QRCode_ZXing_Common_CharacterSetECI2542265168.h"
#include "QRCode_ZXing_Common_DecoderResult3752650303.h"
#include "QRCode_ZXing_Common_DecodingOptions3608870897.h"
#include "QRCode_ZXing_Common_GridSampler228036704.h"
#include "QRCode_ZXing_Common_DefaultGridSampler258327537.h"
#include "QRCode_ZXing_Common_GlobalHistogramBinarizer1656947045.h"
#include "QRCode_ZXing_Common_HybridBinarizer3517076856.h"
#include "QRCode_ZXing_Common_PerspectiveTransform2438931808.h"
#include "QRCode_ZXing_Common_StringUtils3420815774.h"
#include "QRCode_BigIntegerLibrary_Base10BigInteger541098638.h"
#include "QRCode_BigIntegerLibrary_Base10BigInteger_DigitCon3576869316.h"
#include "QRCode_BigIntegerLibrary_BigInteger416298622.h"
#include "QRCode_BigIntegerLibrary_BigInteger_DigitContainer1134993364.h"
#include "QRCode_BigIntegerLibrary_BigIntegerException556608303.h"
#include "QRCode_BigIntegerLibrary_Sign1848424669.h"
#include "QRCode_ZXing_Common_Detector_MathUtils3679559391.h"
#include "QRCode_ZXing_Common_Detector_WhiteRectangleDetector453655296.h"
#include "QRCode_ZXing_Common_ReedSolomon_GenericGF2563420960.h"
#include "QRCode_ZXing_Common_ReedSolomon_GenericGFPoly755870220.h"
#include "QRCode_ZXing_Common_ReedSolomon_ReedSolomonDecoder4166992619.h"
#include "QRCode_ZXing_Common_ReedSolomon_ReedSolomonEncoder1017191363.h"
#include "QRCode_ZXing_Datamatrix_DataMatrixReader1576451070.h"
#include "QRCode_ZXing_Datamatrix_DataMatrixWriter1731856302.h"
#include "QRCode_ZXing_Datamatrix_Internal_BitMatrixParser1222713543.h"
#include "QRCode_ZXing_Datamatrix_Internal_DataBlock594199773.h"
#include "QRCode_ZXing_Datamatrix_Internal_DecodedBitStreamP1520806048.h"
#include "QRCode_ZXing_Datamatrix_Internal_DecodedBitStreamP1613312036.h"
#include "QRCode_ZXing_Datamatrix_Internal_Decoder3504587806.h"
#include "QRCode_ZXing_Datamatrix_Internal_Version2313761970.h"
#include "QRCode_ZXing_Datamatrix_Internal_Version_ECBlocks3710297555.h"
#include "QRCode_ZXing_Datamatrix_Internal_Version_ECB2948426741.h"
#include "QRCode_ZXing_Datamatrix_Internal_Detector966593552.h"
#include "QRCode_ZXing_Datamatrix_Internal_Detector_ResultPo1206419768.h"
#include "QRCode_ZXing_Datamatrix_Internal_Detector_ResultPo2591786030.h"
#include "QRCode_ZXing_Datamatrix_Encoder_ASCIIEncoder2512198407.h"
#include "QRCode_ZXing_Datamatrix_Encoder_Base256Encoder3711543446.h"
#include "QRCode_ZXing_Datamatrix_Encoder_C40Encoder621845305.h"
#include "QRCode_ZXing_Datamatrix_Encoder_SymbolInfo553159586.h"
#include "QRCode_ZXing_Datamatrix_Encoder_DataMatrixSymbolIn3368906246.h"
#include "QRCode_ZXing_Datamatrix_Encoder_DefaultPlacement3907996320.h"
#include "QRCode_ZXing_Datamatrix_Encoder_EdifactEncoder1017178850.h"
#include "QRCode_ZXing_Datamatrix_Encoder_EncoderContext1774722223.h"
#include "QRCode_ZXing_Datamatrix_Encoder_ErrorCorrection3642199692.h"
#include "QRCode_ZXing_Datamatrix_Encoder_HighLevelEncoder1276020918.h"
#include "QRCode_ZXing_Datamatrix_Encoder_SymbolShapeHint2380532246.h"
#include "QRCode_ZXing_Datamatrix_Encoder_TextEncoder1930967221.h"
#include "QRCode_ZXing_Datamatrix_Encoder_X12Encoder1108078463.h"
#include "QRCode_ZXing_OneD_OneDReader3436042911.h"
#include "QRCode_ZXing_IMB_IMBReader1813842883.h"
#include "QRCode_ZXing_Maxicode_MaxiCodeReader3092722715.h"
#include "QRCode_ZXing_Maxicode_Internal_BitMatrixParser4050453827.h"
#include "QRCode_ZXing_Maxicode_Internal_DecodedBitStreamPar2389309002.h"
#include "QRCode_ZXing_Maxicode_Internal_Decoder3124105626.h"
#include "QRCode_ZXing_QrCode_QRCodeReader2288465583.h"
#include "QRCode_ZXing_QrCode_Internal_Detector967800930.h"
#include "QRCode_ZXing_QrCode_Internal_FinderPatternFinder1928752534.h"
#include "QRCode_ZXing_QrCode_Internal_FinderPatternFinder_Fu700570016.h"
#include "QRCode_ZXing_QrCode_Internal_FinderPatternFinder_C1145119813.h"
#include "QRCode_ZXing_OneD_CodaBarReader1573541919.h"
#include "QRCode_ZXing_OneD_OneDimensionalCodeWriter4068326409.h"
#include "QRCode_ZXing_OneD_CodaBarWriter1728947151.h"
#include "QRCode_ZXing_OneD_Code128Reader1206129631.h"
#include "QRCode_ZXing_OneD_Code128Writer1361534863.h"
#include "QRCode_ZXing_OneD_Code39Reader1346536404.h"
#include "QRCode_ZXing_OneD_Code39Writer1501941636.h"
#include "QRCode_ZXing_OneD_Code93Reader2183409032.h"
#include "QRCode_ZXing_OneD_UPCEANReader3527170699.h"
#include "QRCode_ZXing_OneD_EAN13Reader2319538857.h"
#include "QRCode_ZXing_OneD_UPCEANWriter3682575931.h"
#include "QRCode_ZXing_OneD_EAN13Writer2474944089.h"
#include "QRCode_ZXing_OneD_EAN8Reader1642208551.h"
#include "QRCode_ZXing_OneD_EAN8Writer1797613783.h"
#include "QRCode_ZXing_OneD_EANManufacturerOrgSupport3278460736.h"
#include "QRCode_ZXing_OneD_ITFReader4072828816.h"
#include "QRCode_ZXing_OneD_ITFWriter4228234048.h"
#include "QRCode_ZXing_OneD_MSIReader2057678968.h"
#include "QRCode_ZXing_OneD_MSIWriter2213084200.h"
#include "QRCode_ZXing_OneD_MultiFormatOneDReader4091483907.h"
#include "QRCode_ZXing_OneD_MultiFormatUPCEANReader2045775343.h"
#include "QRCode_ZXing_OneD_PlesseyWriter2711109794.h"
#include "QRCode_ZXing_OneD_UPCAReader3152467930.h"
#include "QRCode_ZXing_OneD_UPCAWriter3307873162.h"
#include "QRCode_ZXing_OneD_UPCEANExtension2Support2671369572.h"
#include "QRCode_ZXing_OneD_UPCEANExtension5Support3604833281.h"
#include "QRCode_ZXing_OneD_UPCEANExtensionSupport269614168.h"
#include "QRCode_ZXing_OneD_UPCEReader2407515358.h"
#include "QRCode_ZXing_OneD_RSS_AbstractRSSReader1625773429.h"
#include "QRCode_ZXing_OneD_RSS_DataCharacter770728801.h"
#include "QRCode_ZXing_OneD_RSS_FinderPattern3366792524.h"
#include "QRCode_ZXing_OneD_RSS_Pair1045409632.h"
#include "QRCode_ZXing_OneD_RSS_RSS14Reader1804556634.h"
#include "QRCode_ZXing_OneD_RSS_RSSUtils1038846213.h"
#include "QRCode_ZXing_OneD_RSS_Expanded_BitArrayBuilder2670818133.h"
#include "QRCode_ZXing_OneD_RSS_Expanded_ExpandedPair3413366687.h"
#include "QRCode_ZXing_OneD_RSS_Expanded_ExpandedRow1562831751.h"
#include "QRCode_ZXing_OneD_RSS_Expanded_RSSExpandedReader2976724276.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2600 = { sizeof (ECI_t1296865545), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2600[1] = 
{
	ECI_t1296865545::get_offset_of_value_Renamed_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2601 = { sizeof (CharacterSetECI_t2542265168), -1, sizeof(CharacterSetECI_t2542265168_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2601[3] = 
{
	CharacterSetECI_t2542265168_StaticFields::get_offset_of_VALUE_TO_ECI_1(),
	CharacterSetECI_t2542265168_StaticFields::get_offset_of_NAME_TO_ECI_2(),
	CharacterSetECI_t2542265168::get_offset_of_encodingName_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2602 = { sizeof (DecoderResult_t3752650303), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2602[9] = 
{
	DecoderResult_t3752650303::get_offset_of_U3CRawBytesU3Ek__BackingField_0(),
	DecoderResult_t3752650303::get_offset_of_U3CTextU3Ek__BackingField_1(),
	DecoderResult_t3752650303::get_offset_of_U3CByteSegmentsU3Ek__BackingField_2(),
	DecoderResult_t3752650303::get_offset_of_U3CECLevelU3Ek__BackingField_3(),
	DecoderResult_t3752650303::get_offset_of_U3CErrorsCorrectedU3Ek__BackingField_4(),
	DecoderResult_t3752650303::get_offset_of_U3CStructuredAppendSequenceNumberU3Ek__BackingField_5(),
	DecoderResult_t3752650303::get_offset_of_U3CErasuresU3Ek__BackingField_6(),
	DecoderResult_t3752650303::get_offset_of_U3CStructuredAppendParityU3Ek__BackingField_7(),
	DecoderResult_t3752650303::get_offset_of_U3COtherU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2603 = { sizeof (DecodingOptions_t3608870897), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2603[2] = 
{
	DecodingOptions_t3608870897::get_offset_of_ValueChanged_0(),
	DecodingOptions_t3608870897::get_offset_of_U3CHintsU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2604 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2604[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2605 = { sizeof (GridSampler_t228036704), -1, sizeof(GridSampler_t228036704_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2605[1] = 
{
	GridSampler_t228036704_StaticFields::get_offset_of_gridSampler_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2606 = { sizeof (DefaultGridSampler_t258327537), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2607 = { sizeof (GlobalHistogramBinarizer_t1656947045), -1, sizeof(GlobalHistogramBinarizer_t1656947045_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2607[3] = 
{
	GlobalHistogramBinarizer_t1656947045_StaticFields::get_offset_of_EMPTY_1(),
	GlobalHistogramBinarizer_t1656947045::get_offset_of_luminances_2(),
	GlobalHistogramBinarizer_t1656947045::get_offset_of_buckets_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2608 = { sizeof (HybridBinarizer_t3517076856), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2608[1] = 
{
	HybridBinarizer_t3517076856::get_offset_of_matrix_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2609 = { sizeof (PerspectiveTransform_t2438931808), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2609[9] = 
{
	PerspectiveTransform_t2438931808::get_offset_of_a11_0(),
	PerspectiveTransform_t2438931808::get_offset_of_a12_1(),
	PerspectiveTransform_t2438931808::get_offset_of_a13_2(),
	PerspectiveTransform_t2438931808::get_offset_of_a21_3(),
	PerspectiveTransform_t2438931808::get_offset_of_a22_4(),
	PerspectiveTransform_t2438931808::get_offset_of_a23_5(),
	PerspectiveTransform_t2438931808::get_offset_of_a31_6(),
	PerspectiveTransform_t2438931808::get_offset_of_a32_7(),
	PerspectiveTransform_t2438931808::get_offset_of_a33_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2610 = { sizeof (StringUtils_t3420815774), -1, sizeof(StringUtils_t3420815774_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2610[4] = 
{
	StringUtils_t3420815774_StaticFields::get_offset_of_PLATFORM_DEFAULT_ENCODING_0(),
	StringUtils_t3420815774_StaticFields::get_offset_of_SHIFT_JIS_1(),
	StringUtils_t3420815774_StaticFields::get_offset_of_GB2312_2(),
	StringUtils_t3420815774_StaticFields::get_offset_of_ASSUME_SHIFT_JIS_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2611 = { sizeof (Base10BigInteger_t541098638), -1, sizeof(Base10BigInteger_t541098638_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2611[5] = 
{
	Base10BigInteger_t541098638_StaticFields::get_offset_of_Zero_0(),
	Base10BigInteger_t541098638_StaticFields::get_offset_of_One_1(),
	Base10BigInteger_t541098638::get_offset_of_digits_2(),
	Base10BigInteger_t541098638::get_offset_of_size_3(),
	Base10BigInteger_t541098638::get_offset_of_sign_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2612 = { sizeof (DigitContainer_t3576869316), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2612[1] = 
{
	DigitContainer_t3576869316::get_offset_of_digits_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2613 = { sizeof (BigInteger_t416298622), -1, sizeof(BigInteger_t416298622_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2613[7] = 
{
	BigInteger_t416298622_StaticFields::get_offset_of_Zero_0(),
	BigInteger_t416298622_StaticFields::get_offset_of_One_1(),
	BigInteger_t416298622_StaticFields::get_offset_of_Two_2(),
	BigInteger_t416298622_StaticFields::get_offset_of_Ten_3(),
	BigInteger_t416298622::get_offset_of_digits_4(),
	BigInteger_t416298622::get_offset_of_size_5(),
	BigInteger_t416298622::get_offset_of_sign_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2614 = { sizeof (DigitContainer_t1134993364), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2614[1] = 
{
	DigitContainer_t1134993364::get_offset_of_digits_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2615 = { sizeof (BigIntegerException_t556608303), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2616 = { sizeof (Sign_t1848424669)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2616[3] = 
{
	Sign_t1848424669::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2617 = { sizeof (MathUtils_t3679559391), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2618 = { sizeof (WhiteRectangleDetector_t453655296), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2618[7] = 
{
	WhiteRectangleDetector_t453655296::get_offset_of_image_0(),
	WhiteRectangleDetector_t453655296::get_offset_of_height_1(),
	WhiteRectangleDetector_t453655296::get_offset_of_width_2(),
	WhiteRectangleDetector_t453655296::get_offset_of_leftInit_3(),
	WhiteRectangleDetector_t453655296::get_offset_of_rightInit_4(),
	WhiteRectangleDetector_t453655296::get_offset_of_downInit_5(),
	WhiteRectangleDetector_t453655296::get_offset_of_upInit_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2619 = { sizeof (GenericGF_t2563420960), -1, sizeof(GenericGF_t2563420960_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2619[15] = 
{
	GenericGF_t2563420960_StaticFields::get_offset_of_AZTEC_DATA_12_0(),
	GenericGF_t2563420960_StaticFields::get_offset_of_AZTEC_DATA_10_1(),
	GenericGF_t2563420960_StaticFields::get_offset_of_AZTEC_DATA_6_2(),
	GenericGF_t2563420960_StaticFields::get_offset_of_AZTEC_PARAM_3(),
	GenericGF_t2563420960_StaticFields::get_offset_of_QR_CODE_FIELD_256_4(),
	GenericGF_t2563420960_StaticFields::get_offset_of_DATA_MATRIX_FIELD_256_5(),
	GenericGF_t2563420960_StaticFields::get_offset_of_AZTEC_DATA_8_6(),
	GenericGF_t2563420960_StaticFields::get_offset_of_MAXICODE_FIELD_64_7(),
	GenericGF_t2563420960::get_offset_of_expTable_8(),
	GenericGF_t2563420960::get_offset_of_logTable_9(),
	GenericGF_t2563420960::get_offset_of_zero_10(),
	GenericGF_t2563420960::get_offset_of_one_11(),
	GenericGF_t2563420960::get_offset_of_size_12(),
	GenericGF_t2563420960::get_offset_of_primitive_13(),
	GenericGF_t2563420960::get_offset_of_generatorBase_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2620 = { sizeof (GenericGFPoly_t755870220), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2620[2] = 
{
	GenericGFPoly_t755870220::get_offset_of_field_0(),
	GenericGFPoly_t755870220::get_offset_of_coefficients_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2621 = { sizeof (ReedSolomonDecoder_t4166992619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2621[1] = 
{
	ReedSolomonDecoder_t4166992619::get_offset_of_field_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2622 = { sizeof (ReedSolomonEncoder_t1017191363), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2622[2] = 
{
	ReedSolomonEncoder_t1017191363::get_offset_of_field_0(),
	ReedSolomonEncoder_t1017191363::get_offset_of_cachedGenerators_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2623 = { sizeof (DataMatrixReader_t1576451070), -1, sizeof(DataMatrixReader_t1576451070_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2623[2] = 
{
	DataMatrixReader_t1576451070_StaticFields::get_offset_of_NO_POINTS_0(),
	DataMatrixReader_t1576451070::get_offset_of_decoder_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2624 = { sizeof (DataMatrixWriter_t1731856302), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2625 = { sizeof (BitMatrixParser_t1222713543), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2625[3] = 
{
	BitMatrixParser_t1222713543::get_offset_of_mappingBitMatrix_0(),
	BitMatrixParser_t1222713543::get_offset_of_readMappingMatrix_1(),
	BitMatrixParser_t1222713543::get_offset_of_version_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2626 = { sizeof (DataBlock_t594199773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2626[2] = 
{
	DataBlock_t594199773::get_offset_of_numDataCodewords_0(),
	DataBlock_t594199773::get_offset_of_codewords_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2627 = { sizeof (DecodedBitStreamParser_t1520806048), -1, sizeof(DecodedBitStreamParser_t1520806048_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2627[5] = 
{
	DecodedBitStreamParser_t1520806048_StaticFields::get_offset_of_C40_BASIC_SET_CHARS_0(),
	DecodedBitStreamParser_t1520806048_StaticFields::get_offset_of_C40_SHIFT2_SET_CHARS_1(),
	DecodedBitStreamParser_t1520806048_StaticFields::get_offset_of_TEXT_BASIC_SET_CHARS_2(),
	DecodedBitStreamParser_t1520806048_StaticFields::get_offset_of_TEXT_SHIFT2_SET_CHARS_3(),
	DecodedBitStreamParser_t1520806048_StaticFields::get_offset_of_TEXT_SHIFT3_SET_CHARS_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2628 = { sizeof (Mode_t1613312036)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2628[8] = 
{
	Mode_t1613312036::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2629 = { sizeof (Decoder_t3504587806), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2629[1] = 
{
	Decoder_t3504587806::get_offset_of_rsDecoder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2630 = { sizeof (Version_t2313761970), -1, sizeof(Version_t2313761970_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2630[8] = 
{
	Version_t2313761970_StaticFields::get_offset_of_VERSIONS_0(),
	Version_t2313761970::get_offset_of_versionNumber_1(),
	Version_t2313761970::get_offset_of_symbolSizeRows_2(),
	Version_t2313761970::get_offset_of_symbolSizeColumns_3(),
	Version_t2313761970::get_offset_of_dataRegionSizeRows_4(),
	Version_t2313761970::get_offset_of_dataRegionSizeColumns_5(),
	Version_t2313761970::get_offset_of_ecBlocks_6(),
	Version_t2313761970::get_offset_of_totalCodewords_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2631 = { sizeof (ECBlocks_t3710297555), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2631[2] = 
{
	ECBlocks_t3710297555::get_offset_of_ecCodewords_0(),
	ECBlocks_t3710297555::get_offset_of__ecBlocksValue_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2632 = { sizeof (ECB_t2948426741), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2632[2] = 
{
	ECB_t2948426741::get_offset_of_count_0(),
	ECB_t2948426741::get_offset_of_dataCodewords_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2633 = { sizeof (Detector_t966593552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2633[2] = 
{
	Detector_t966593552::get_offset_of_image_0(),
	Detector_t966593552::get_offset_of_rectangleDetector_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2634 = { sizeof (ResultPointsAndTransitions_t1206419768), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2634[3] = 
{
	ResultPointsAndTransitions_t1206419768::get_offset_of_U3CFromU3Ek__BackingField_0(),
	ResultPointsAndTransitions_t1206419768::get_offset_of_U3CToU3Ek__BackingField_1(),
	ResultPointsAndTransitions_t1206419768::get_offset_of_U3CTransitionsU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2635 = { sizeof (ResultPointsAndTransitionsComparator_t2591786030), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2636 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2637 = { sizeof (ASCIIEncoder_t2512198407), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2638 = { sizeof (Base256Encoder_t3711543446), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2639 = { sizeof (C40Encoder_t621845305), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2640 = { sizeof (SymbolInfo_t553159586), -1, sizeof(SymbolInfo_t553159586_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2640[10] = 
{
	SymbolInfo_t553159586_StaticFields::get_offset_of_PROD_SYMBOLS_0(),
	SymbolInfo_t553159586_StaticFields::get_offset_of_symbols_1(),
	SymbolInfo_t553159586::get_offset_of_rectangular_2(),
	SymbolInfo_t553159586::get_offset_of_dataCapacity_3(),
	SymbolInfo_t553159586::get_offset_of_errorCodewords_4(),
	SymbolInfo_t553159586::get_offset_of_matrixWidth_5(),
	SymbolInfo_t553159586::get_offset_of_matrixHeight_6(),
	SymbolInfo_t553159586::get_offset_of_dataRegions_7(),
	SymbolInfo_t553159586::get_offset_of_rsBlockData_8(),
	SymbolInfo_t553159586::get_offset_of_rsBlockError_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2641 = { sizeof (DataMatrixSymbolInfo144_t3368906246), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2642 = { sizeof (DefaultPlacement_t3907996320), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2642[4] = 
{
	DefaultPlacement_t3907996320::get_offset_of_codewords_0(),
	DefaultPlacement_t3907996320::get_offset_of_numrows_1(),
	DefaultPlacement_t3907996320::get_offset_of_numcols_2(),
	DefaultPlacement_t3907996320::get_offset_of_bits_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2643 = { sizeof (EdifactEncoder_t1017178850), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2644 = { sizeof (EncoderContext_t1774722223), -1, sizeof(EncoderContext_t1774722223_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2644[10] = 
{
	EncoderContext_t1774722223::get_offset_of_msg_0(),
	EncoderContext_t1774722223::get_offset_of_shape_1(),
	EncoderContext_t1774722223::get_offset_of_minSize_2(),
	EncoderContext_t1774722223::get_offset_of_maxSize_3(),
	EncoderContext_t1774722223::get_offset_of_codewords_4(),
	EncoderContext_t1774722223::get_offset_of_pos_5(),
	EncoderContext_t1774722223::get_offset_of_newEncoding_6(),
	EncoderContext_t1774722223::get_offset_of_symbolInfo_7(),
	EncoderContext_t1774722223::get_offset_of_skipAtEnd_8(),
	EncoderContext_t1774722223_StaticFields::get_offset_of_encoding_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2645 = { sizeof (ErrorCorrection_t3642199692), -1, sizeof(ErrorCorrection_t3642199692_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2645[4] = 
{
	ErrorCorrection_t3642199692_StaticFields::get_offset_of_FACTOR_SETS_0(),
	ErrorCorrection_t3642199692_StaticFields::get_offset_of_FACTORS_1(),
	ErrorCorrection_t3642199692_StaticFields::get_offset_of_LOG_2(),
	ErrorCorrection_t3642199692_StaticFields::get_offset_of_ALOG_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2646 = { sizeof (HighLevelEncoder_t1276020918), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2647 = { sizeof (SymbolShapeHint_t2380532246)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2647[4] = 
{
	SymbolShapeHint_t2380532246::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2648 = { sizeof (TextEncoder_t1930967221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2649 = { sizeof (X12Encoder_t1108078463), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2650 = { sizeof (OneDReader_t3436042911), -1, sizeof(OneDReader_t3436042911_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2650[2] = 
{
	OneDReader_t3436042911_StaticFields::get_offset_of_INTEGER_MATH_SHIFT_0(),
	OneDReader_t3436042911_StaticFields::get_offset_of_PATTERN_MATCH_RESULT_SCALE_FACTOR_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2651 = { sizeof (IMBReader_t1813842883), -1, sizeof(IMBReader_t1813842883_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2651[25] = 
{
	IMBReader_t1813842883_StaticFields::get_offset_of_barPosA_2(),
	IMBReader_t1813842883_StaticFields::get_offset_of_barPosB_3(),
	IMBReader_t1813842883_StaticFields::get_offset_of_barPosC_4(),
	IMBReader_t1813842883_StaticFields::get_offset_of_barPosD_5(),
	IMBReader_t1813842883_StaticFields::get_offset_of_barPosE_6(),
	IMBReader_t1813842883_StaticFields::get_offset_of_barPosF_7(),
	IMBReader_t1813842883_StaticFields::get_offset_of_barPosG_8(),
	IMBReader_t1813842883_StaticFields::get_offset_of_barPosH_9(),
	IMBReader_t1813842883_StaticFields::get_offset_of_barPosI_10(),
	IMBReader_t1813842883_StaticFields::get_offset_of_barPosJ_11(),
	IMBReader_t1813842883_StaticFields::get_offset_of_barPos_12(),
	IMBReader_t1813842883_StaticFields::get_offset_of_barTypeA_13(),
	IMBReader_t1813842883_StaticFields::get_offset_of_barTypeB_14(),
	IMBReader_t1813842883_StaticFields::get_offset_of_barTypeC_15(),
	IMBReader_t1813842883_StaticFields::get_offset_of_barTypeD_16(),
	IMBReader_t1813842883_StaticFields::get_offset_of_barTypeE_17(),
	IMBReader_t1813842883_StaticFields::get_offset_of_barTypeF_18(),
	IMBReader_t1813842883_StaticFields::get_offset_of_barTypeG_19(),
	IMBReader_t1813842883_StaticFields::get_offset_of_barTypeH_20(),
	IMBReader_t1813842883_StaticFields::get_offset_of_barTypeI_21(),
	IMBReader_t1813842883_StaticFields::get_offset_of_barTypeJ_22(),
	IMBReader_t1813842883_StaticFields::get_offset_of_barType_23(),
	IMBReader_t1813842883_StaticFields::get_offset_of_table1Check_24(),
	IMBReader_t1813842883_StaticFields::get_offset_of_table2Check_25(),
	IMBReader_t1813842883::get_offset_of_currentBitmap_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2652 = { sizeof (MaxiCodeReader_t3092722715), -1, sizeof(MaxiCodeReader_t3092722715_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2652[2] = 
{
	MaxiCodeReader_t3092722715_StaticFields::get_offset_of_NO_POINTS_0(),
	MaxiCodeReader_t3092722715::get_offset_of_decoder_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2653 = { sizeof (BitMatrixParser_t4050453827), -1, sizeof(BitMatrixParser_t4050453827_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2653[2] = 
{
	BitMatrixParser_t4050453827_StaticFields::get_offset_of_BITNR_0(),
	BitMatrixParser_t4050453827::get_offset_of_bitMatrix_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2654 = { sizeof (DecodedBitStreamParser_t2389309002), -1, sizeof(DecodedBitStreamParser_t2389309002_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2654[1] = 
{
	DecodedBitStreamParser_t2389309002_StaticFields::get_offset_of_SETS_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2655 = { sizeof (Decoder_t3124105626), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2655[1] = 
{
	Decoder_t3124105626::get_offset_of_rsDecoder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2656 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2657 = { sizeof (QRCodeReader_t2288465583), -1, sizeof(QRCodeReader_t2288465583_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2657[2] = 
{
	QRCodeReader_t2288465583_StaticFields::get_offset_of_NO_POINTS_0(),
	QRCodeReader_t2288465583::get_offset_of_decoder_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2658 = { sizeof (Detector_t967800930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2658[2] = 
{
	Detector_t967800930::get_offset_of_image_0(),
	Detector_t967800930::get_offset_of_resultPointCallback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2659 = { sizeof (FinderPatternFinder_t1928752534), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2659[5] = 
{
	FinderPatternFinder_t1928752534::get_offset_of_image_0(),
	FinderPatternFinder_t1928752534::get_offset_of_possibleCenters_1(),
	FinderPatternFinder_t1928752534::get_offset_of_hasSkipped_2(),
	FinderPatternFinder_t1928752534::get_offset_of_crossCheckStateCount_3(),
	FinderPatternFinder_t1928752534::get_offset_of_resultPointCallback_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2660 = { sizeof (FurthestFromAverageComparator_t700570016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2660[1] = 
{
	FurthestFromAverageComparator_t700570016::get_offset_of_average_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2661 = { sizeof (CenterComparator_t1145119813), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2661[1] = 
{
	CenterComparator_t1145119813::get_offset_of_average_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2662 = { sizeof (CodaBarReader_t1573541919), -1, sizeof(CodaBarReader_t1573541919_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2662[8] = 
{
	CodaBarReader_t1573541919_StaticFields::get_offset_of_MAX_ACCEPTABLE_2(),
	CodaBarReader_t1573541919_StaticFields::get_offset_of_PADDING_3(),
	CodaBarReader_t1573541919_StaticFields::get_offset_of_ALPHABET_4(),
	CodaBarReader_t1573541919_StaticFields::get_offset_of_CHARACTER_ENCODINGS_5(),
	CodaBarReader_t1573541919_StaticFields::get_offset_of_STARTEND_ENCODING_6(),
	CodaBarReader_t1573541919::get_offset_of_decodeRowResult_7(),
	CodaBarReader_t1573541919::get_offset_of_counters_8(),
	CodaBarReader_t1573541919::get_offset_of_counterLength_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2663 = { sizeof (OneDimensionalCodeWriter_t4068326409), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2664 = { sizeof (CodaBarWriter_t1728947151), -1, sizeof(CodaBarWriter_t1728947151_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2664[4] = 
{
	CodaBarWriter_t1728947151_StaticFields::get_offset_of_START_END_CHARS_0(),
	CodaBarWriter_t1728947151_StaticFields::get_offset_of_ALT_START_END_CHARS_1(),
	CodaBarWriter_t1728947151_StaticFields::get_offset_of_CHARS_WHICH_ARE_TEN_LENGTH_EACH_AFTER_DECODED_2(),
	CodaBarWriter_t1728947151_StaticFields::get_offset_of_DEFAULT_GUARD_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2665 = { sizeof (Code128Reader_t1206129631), -1, sizeof(Code128Reader_t1206129631_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2665[3] = 
{
	Code128Reader_t1206129631_StaticFields::get_offset_of_CODE_PATTERNS_2(),
	Code128Reader_t1206129631_StaticFields::get_offset_of_MAX_AVG_VARIANCE_3(),
	Code128Reader_t1206129631_StaticFields::get_offset_of_MAX_INDIVIDUAL_VARIANCE_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2666 = { sizeof (Code128Writer_t1361534863), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2666[1] = 
{
	Code128Writer_t1361534863::get_offset_of_forceCodesetB_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2667 = { sizeof (Code39Reader_t1346536404), -1, sizeof(Code39Reader_t1346536404_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2667[8] = 
{
	Code39Reader_t1346536404_StaticFields::get_offset_of_ALPHABET_STRING_2(),
	Code39Reader_t1346536404_StaticFields::get_offset_of_ALPHABET_3(),
	Code39Reader_t1346536404_StaticFields::get_offset_of_CHARACTER_ENCODINGS_4(),
	Code39Reader_t1346536404_StaticFields::get_offset_of_ASTERISK_ENCODING_5(),
	Code39Reader_t1346536404::get_offset_of_usingCheckDigit_6(),
	Code39Reader_t1346536404::get_offset_of_extendedMode_7(),
	Code39Reader_t1346536404::get_offset_of_decodeRowResult_8(),
	Code39Reader_t1346536404::get_offset_of_counters_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2668 = { sizeof (Code39Writer_t1501941636), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2669 = { sizeof (Code93Reader_t2183409032), -1, sizeof(Code93Reader_t2183409032_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2669[5] = 
{
	Code93Reader_t2183409032_StaticFields::get_offset_of_ALPHABET_2(),
	Code93Reader_t2183409032_StaticFields::get_offset_of_CHARACTER_ENCODINGS_3(),
	Code93Reader_t2183409032_StaticFields::get_offset_of_ASTERISK_ENCODING_4(),
	Code93Reader_t2183409032::get_offset_of_decodeRowResult_5(),
	Code93Reader_t2183409032::get_offset_of_counters_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2670 = { sizeof (UPCEANReader_t3527170699), -1, sizeof(UPCEANReader_t3527170699_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2670[9] = 
{
	UPCEANReader_t3527170699_StaticFields::get_offset_of_MAX_AVG_VARIANCE_2(),
	UPCEANReader_t3527170699_StaticFields::get_offset_of_MAX_INDIVIDUAL_VARIANCE_3(),
	UPCEANReader_t3527170699_StaticFields::get_offset_of_START_END_PATTERN_4(),
	UPCEANReader_t3527170699_StaticFields::get_offset_of_MIDDLE_PATTERN_5(),
	UPCEANReader_t3527170699_StaticFields::get_offset_of_L_PATTERNS_6(),
	UPCEANReader_t3527170699_StaticFields::get_offset_of_L_AND_G_PATTERNS_7(),
	UPCEANReader_t3527170699::get_offset_of_decodeRowStringBuffer_8(),
	UPCEANReader_t3527170699::get_offset_of_extensionReader_9(),
	UPCEANReader_t3527170699::get_offset_of_eanManSupport_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2671 = { sizeof (EAN13Reader_t2319538857), -1, sizeof(EAN13Reader_t2319538857_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2671[2] = 
{
	EAN13Reader_t2319538857_StaticFields::get_offset_of_FIRST_DIGIT_ENCODINGS_11(),
	EAN13Reader_t2319538857::get_offset_of_decodeMiddleCounters_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2672 = { sizeof (UPCEANWriter_t3682575931), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2673 = { sizeof (EAN13Writer_t2474944089), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2674 = { sizeof (EAN8Reader_t1642208551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2674[1] = 
{
	EAN8Reader_t1642208551::get_offset_of_decodeMiddleCounters_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2675 = { sizeof (EAN8Writer_t1797613783), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2676 = { sizeof (EANManufacturerOrgSupport_t3278460736), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2676[2] = 
{
	EANManufacturerOrgSupport_t3278460736::get_offset_of_ranges_0(),
	EANManufacturerOrgSupport_t3278460736::get_offset_of_countryIdentifiers_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2677 = { sizeof (ITFReader_t4072828816), -1, sizeof(ITFReader_t4072828816_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2677[7] = 
{
	ITFReader_t4072828816_StaticFields::get_offset_of_MAX_AVG_VARIANCE_2(),
	ITFReader_t4072828816_StaticFields::get_offset_of_MAX_INDIVIDUAL_VARIANCE_3(),
	ITFReader_t4072828816_StaticFields::get_offset_of_DEFAULT_ALLOWED_LENGTHS_4(),
	ITFReader_t4072828816::get_offset_of_narrowLineWidth_5(),
	ITFReader_t4072828816_StaticFields::get_offset_of_START_PATTERN_6(),
	ITFReader_t4072828816_StaticFields::get_offset_of_END_PATTERN_REVERSED_7(),
	ITFReader_t4072828816_StaticFields::get_offset_of_PATTERNS_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2678 = { sizeof (ITFWriter_t4228234048), -1, sizeof(ITFWriter_t4228234048_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2678[2] = 
{
	ITFWriter_t4228234048_StaticFields::get_offset_of_START_PATTERN_0(),
	ITFWriter_t4228234048_StaticFields::get_offset_of_END_PATTERN_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2679 = { sizeof (MSIReader_t2057678968), -1, sizeof(MSIReader_t2057678968_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2679[8] = 
{
	MSIReader_t2057678968_StaticFields::get_offset_of_ALPHABET_STRING_2(),
	MSIReader_t2057678968_StaticFields::get_offset_of_ALPHABET_3(),
	MSIReader_t2057678968_StaticFields::get_offset_of_CHARACTER_ENCODINGS_4(),
	MSIReader_t2057678968::get_offset_of_usingCheckDigit_5(),
	MSIReader_t2057678968::get_offset_of_decodeRowResult_6(),
	MSIReader_t2057678968::get_offset_of_counters_7(),
	MSIReader_t2057678968::get_offset_of_averageCounterWidth_8(),
	MSIReader_t2057678968_StaticFields::get_offset_of_doubleAndCrossSum_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2680 = { sizeof (MSIWriter_t2213084200), -1, sizeof(MSIWriter_t2213084200_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2680[3] = 
{
	MSIWriter_t2213084200_StaticFields::get_offset_of_startWidths_0(),
	MSIWriter_t2213084200_StaticFields::get_offset_of_endWidths_1(),
	MSIWriter_t2213084200_StaticFields::get_offset_of_numberWidths_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2681 = { sizeof (MultiFormatOneDReader_t4091483907), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2681[1] = 
{
	MultiFormatOneDReader_t4091483907::get_offset_of_readers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2682 = { sizeof (MultiFormatUPCEANReader_t2045775343), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2682[1] = 
{
	MultiFormatUPCEANReader_t2045775343::get_offset_of_readers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2683 = { sizeof (PlesseyWriter_t2711109794), -1, sizeof(PlesseyWriter_t2711109794_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2683[7] = 
{
	PlesseyWriter_t2711109794_StaticFields::get_offset_of_startWidths_0(),
	PlesseyWriter_t2711109794_StaticFields::get_offset_of_terminationWidths_1(),
	PlesseyWriter_t2711109794_StaticFields::get_offset_of_endWidths_2(),
	PlesseyWriter_t2711109794_StaticFields::get_offset_of_numberWidths_3(),
	PlesseyWriter_t2711109794_StaticFields::get_offset_of_crcGrid_4(),
	PlesseyWriter_t2711109794_StaticFields::get_offset_of_crc0Widths_5(),
	PlesseyWriter_t2711109794_StaticFields::get_offset_of_crc1Widths_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2684 = { sizeof (UPCAReader_t3152467930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2684[1] = 
{
	UPCAReader_t3152467930::get_offset_of_ean13Reader_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2685 = { sizeof (UPCAWriter_t3307873162), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2685[1] = 
{
	UPCAWriter_t3307873162::get_offset_of_subWriter_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2686 = { sizeof (UPCEANExtension2Support_t2671369572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2686[2] = 
{
	UPCEANExtension2Support_t2671369572::get_offset_of_decodeMiddleCounters_0(),
	UPCEANExtension2Support_t2671369572::get_offset_of_decodeRowStringBuffer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2687 = { sizeof (UPCEANExtension5Support_t3604833281), -1, sizeof(UPCEANExtension5Support_t3604833281_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2687[3] = 
{
	UPCEANExtension5Support_t3604833281_StaticFields::get_offset_of_CHECK_DIGIT_ENCODINGS_0(),
	UPCEANExtension5Support_t3604833281::get_offset_of_decodeMiddleCounters_1(),
	UPCEANExtension5Support_t3604833281::get_offset_of_decodeRowStringBuffer_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2688 = { sizeof (UPCEANExtensionSupport_t269614168), -1, sizeof(UPCEANExtensionSupport_t269614168_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2688[3] = 
{
	UPCEANExtensionSupport_t269614168_StaticFields::get_offset_of_EXTENSION_START_PATTERN_0(),
	UPCEANExtensionSupport_t269614168::get_offset_of_twoSupport_1(),
	UPCEANExtensionSupport_t269614168::get_offset_of_fiveSupport_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2689 = { sizeof (UPCEReader_t2407515358), -1, sizeof(UPCEReader_t2407515358_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2689[3] = 
{
	UPCEReader_t2407515358_StaticFields::get_offset_of_MIDDLE_END_PATTERN_11(),
	UPCEReader_t2407515358_StaticFields::get_offset_of_NUMSYS_AND_CHECK_DIGIT_PATTERNS_12(),
	UPCEReader_t2407515358::get_offset_of_decodeMiddleCounters_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2690 = { sizeof (AbstractRSSReader_t1625773429), -1, sizeof(AbstractRSSReader_t1625773429_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2690[8] = 
{
	AbstractRSSReader_t1625773429_StaticFields::get_offset_of_MAX_AVG_VARIANCE_2(),
	AbstractRSSReader_t1625773429_StaticFields::get_offset_of_MAX_INDIVIDUAL_VARIANCE_3(),
	AbstractRSSReader_t1625773429::get_offset_of_decodeFinderCounters_4(),
	AbstractRSSReader_t1625773429::get_offset_of_dataCharacterCounters_5(),
	AbstractRSSReader_t1625773429::get_offset_of_oddRoundingErrors_6(),
	AbstractRSSReader_t1625773429::get_offset_of_evenRoundingErrors_7(),
	AbstractRSSReader_t1625773429::get_offset_of_oddCounts_8(),
	AbstractRSSReader_t1625773429::get_offset_of_evenCounts_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2691 = { sizeof (DataCharacter_t770728801), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2691[2] = 
{
	DataCharacter_t770728801::get_offset_of_U3CValueU3Ek__BackingField_0(),
	DataCharacter_t770728801::get_offset_of_U3CChecksumPortionU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2692 = { sizeof (FinderPattern_t3366792524), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2692[3] = 
{
	FinderPattern_t3366792524::get_offset_of_U3CValueU3Ek__BackingField_0(),
	FinderPattern_t3366792524::get_offset_of_U3CStartEndU3Ek__BackingField_1(),
	FinderPattern_t3366792524::get_offset_of_U3CResultPointsU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2693 = { sizeof (Pair_t1045409632), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2693[2] = 
{
	Pair_t1045409632::get_offset_of_U3CFinderPatternU3Ek__BackingField_2(),
	Pair_t1045409632::get_offset_of_U3CCountU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2694 = { sizeof (RSS14Reader_t1804556634), -1, sizeof(RSS14Reader_t1804556634_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2694[9] = 
{
	RSS14Reader_t1804556634_StaticFields::get_offset_of_OUTSIDE_EVEN_TOTAL_SUBSET_10(),
	RSS14Reader_t1804556634_StaticFields::get_offset_of_INSIDE_ODD_TOTAL_SUBSET_11(),
	RSS14Reader_t1804556634_StaticFields::get_offset_of_OUTSIDE_GSUM_12(),
	RSS14Reader_t1804556634_StaticFields::get_offset_of_INSIDE_GSUM_13(),
	RSS14Reader_t1804556634_StaticFields::get_offset_of_OUTSIDE_ODD_WIDEST_14(),
	RSS14Reader_t1804556634_StaticFields::get_offset_of_INSIDE_ODD_WIDEST_15(),
	RSS14Reader_t1804556634_StaticFields::get_offset_of_FINDER_PATTERNS_16(),
	RSS14Reader_t1804556634::get_offset_of_possibleLeftPairs_17(),
	RSS14Reader_t1804556634::get_offset_of_possibleRightPairs_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2695 = { sizeof (RSSUtils_t1038846213), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2696 = { sizeof (BitArrayBuilder_t2670818133), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2697 = { sizeof (ExpandedPair_t3413366687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2697[4] = 
{
	ExpandedPair_t3413366687::get_offset_of_U3CMayBeLastU3Ek__BackingField_0(),
	ExpandedPair_t3413366687::get_offset_of_U3CLeftCharU3Ek__BackingField_1(),
	ExpandedPair_t3413366687::get_offset_of_U3CRightCharU3Ek__BackingField_2(),
	ExpandedPair_t3413366687::get_offset_of_U3CFinderPatternU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2698 = { sizeof (ExpandedRow_t1562831751), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2698[3] = 
{
	ExpandedRow_t1562831751::get_offset_of_U3CPairsU3Ek__BackingField_0(),
	ExpandedRow_t1562831751::get_offset_of_U3CRowNumberU3Ek__BackingField_1(),
	ExpandedRow_t1562831751::get_offset_of_U3CIsReversedU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2699 = { sizeof (RSSExpandedReader_t2976724276), -1, sizeof(RSSExpandedReader_t2976724276_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2699[10] = 
{
	RSSExpandedReader_t2976724276_StaticFields::get_offset_of_SYMBOL_WIDEST_10(),
	RSSExpandedReader_t2976724276_StaticFields::get_offset_of_EVEN_TOTAL_SUBSET_11(),
	RSSExpandedReader_t2976724276_StaticFields::get_offset_of_GSUM_12(),
	RSSExpandedReader_t2976724276_StaticFields::get_offset_of_FINDER_PATTERNS_13(),
	RSSExpandedReader_t2976724276_StaticFields::get_offset_of_WEIGHTS_14(),
	RSSExpandedReader_t2976724276_StaticFields::get_offset_of_FINDER_PATTERN_SEQUENCES_15(),
	RSSExpandedReader_t2976724276::get_offset_of_pairs_16(),
	RSSExpandedReader_t2976724276::get_offset_of_rows_17(),
	RSSExpandedReader_t2976724276::get_offset_of_startEnd_18(),
	RSSExpandedReader_t2976724276::get_offset_of_startFromEven_19(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
