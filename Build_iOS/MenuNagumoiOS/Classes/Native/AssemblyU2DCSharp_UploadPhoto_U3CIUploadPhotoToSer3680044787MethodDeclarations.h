﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UploadPhoto/<IUploadPhotoToServer>c__Iterator84
struct U3CIUploadPhotoToServerU3Ec__Iterator84_t3680044787;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UploadPhoto/<IUploadPhotoToServer>c__Iterator84::.ctor()
extern "C"  void U3CIUploadPhotoToServerU3Ec__Iterator84__ctor_m1549156824 (U3CIUploadPhotoToServerU3Ec__Iterator84_t3680044787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UploadPhoto/<IUploadPhotoToServer>c__Iterator84::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CIUploadPhotoToServerU3Ec__Iterator84_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3253793402 (U3CIUploadPhotoToServerU3Ec__Iterator84_t3680044787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UploadPhoto/<IUploadPhotoToServer>c__Iterator84::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CIUploadPhotoToServerU3Ec__Iterator84_System_Collections_IEnumerator_get_Current_m2839226894 (U3CIUploadPhotoToServerU3Ec__Iterator84_t3680044787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UploadPhoto/<IUploadPhotoToServer>c__Iterator84::MoveNext()
extern "C"  bool U3CIUploadPhotoToServerU3Ec__Iterator84_MoveNext_m1513444636 (U3CIUploadPhotoToServerU3Ec__Iterator84_t3680044787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UploadPhoto/<IUploadPhotoToServer>c__Iterator84::Dispose()
extern "C"  void U3CIUploadPhotoToServerU3Ec__Iterator84_Dispose_m2579306517 (U3CIUploadPhotoToServerU3Ec__Iterator84_t3680044787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UploadPhoto/<IUploadPhotoToServer>c__Iterator84::Reset()
extern "C"  void U3CIUploadPhotoToServerU3Ec__Iterator84_Reset_m3490557061 (U3CIUploadPhotoToServerU3Ec__Iterator84_t3680044787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
