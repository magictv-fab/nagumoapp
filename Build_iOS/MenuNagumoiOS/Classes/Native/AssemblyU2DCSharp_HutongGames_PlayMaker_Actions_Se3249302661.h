﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmMaterial
struct FsmMaterial_t924399665;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetProceduralFloat
struct  SetProceduralFloat_t3249302661  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Actions.SetProceduralFloat::substanceMaterial
	FsmMaterial_t924399665 * ___substanceMaterial_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetProceduralFloat::floatProperty
	FsmString_t952858651 * ___floatProperty_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetProceduralFloat::floatValue
	FsmFloat_t2134102846 * ___floatValue_11;
	// System.Boolean HutongGames.PlayMaker.Actions.SetProceduralFloat::everyFrame
	bool ___everyFrame_12;

public:
	inline static int32_t get_offset_of_substanceMaterial_9() { return static_cast<int32_t>(offsetof(SetProceduralFloat_t3249302661, ___substanceMaterial_9)); }
	inline FsmMaterial_t924399665 * get_substanceMaterial_9() const { return ___substanceMaterial_9; }
	inline FsmMaterial_t924399665 ** get_address_of_substanceMaterial_9() { return &___substanceMaterial_9; }
	inline void set_substanceMaterial_9(FsmMaterial_t924399665 * value)
	{
		___substanceMaterial_9 = value;
		Il2CppCodeGenWriteBarrier(&___substanceMaterial_9, value);
	}

	inline static int32_t get_offset_of_floatProperty_10() { return static_cast<int32_t>(offsetof(SetProceduralFloat_t3249302661, ___floatProperty_10)); }
	inline FsmString_t952858651 * get_floatProperty_10() const { return ___floatProperty_10; }
	inline FsmString_t952858651 ** get_address_of_floatProperty_10() { return &___floatProperty_10; }
	inline void set_floatProperty_10(FsmString_t952858651 * value)
	{
		___floatProperty_10 = value;
		Il2CppCodeGenWriteBarrier(&___floatProperty_10, value);
	}

	inline static int32_t get_offset_of_floatValue_11() { return static_cast<int32_t>(offsetof(SetProceduralFloat_t3249302661, ___floatValue_11)); }
	inline FsmFloat_t2134102846 * get_floatValue_11() const { return ___floatValue_11; }
	inline FsmFloat_t2134102846 ** get_address_of_floatValue_11() { return &___floatValue_11; }
	inline void set_floatValue_11(FsmFloat_t2134102846 * value)
	{
		___floatValue_11 = value;
		Il2CppCodeGenWriteBarrier(&___floatValue_11, value);
	}

	inline static int32_t get_offset_of_everyFrame_12() { return static_cast<int32_t>(offsetof(SetProceduralFloat_t3249302661, ___everyFrame_12)); }
	inline bool get_everyFrame_12() const { return ___everyFrame_12; }
	inline bool* get_address_of_everyFrame_12() { return &___everyFrame_12; }
	inline void set_everyFrame_12(bool value)
	{
		___everyFrame_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
