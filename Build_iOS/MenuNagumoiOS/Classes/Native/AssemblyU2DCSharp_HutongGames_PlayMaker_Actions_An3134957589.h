﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "UnityEngine_UnityEngine_WrapMode1491636113.h"
#include "UnityEngine_UnityEngine_AnimationBlendMode23503924.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AnimationSettings
struct  AnimationSettings_t3134957589  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AnimationSettings::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.AnimationSettings::animName
	FsmString_t952858651 * ___animName_10;
	// UnityEngine.WrapMode HutongGames.PlayMaker.Actions.AnimationSettings::wrapMode
	int32_t ___wrapMode_11;
	// UnityEngine.AnimationBlendMode HutongGames.PlayMaker.Actions.AnimationSettings::blendMode
	int32_t ___blendMode_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AnimationSettings::speed
	FsmFloat_t2134102846 * ___speed_13;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.AnimationSettings::layer
	FsmInt_t1596138449 * ___layer_14;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(AnimationSettings_t3134957589, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_animName_10() { return static_cast<int32_t>(offsetof(AnimationSettings_t3134957589, ___animName_10)); }
	inline FsmString_t952858651 * get_animName_10() const { return ___animName_10; }
	inline FsmString_t952858651 ** get_address_of_animName_10() { return &___animName_10; }
	inline void set_animName_10(FsmString_t952858651 * value)
	{
		___animName_10 = value;
		Il2CppCodeGenWriteBarrier(&___animName_10, value);
	}

	inline static int32_t get_offset_of_wrapMode_11() { return static_cast<int32_t>(offsetof(AnimationSettings_t3134957589, ___wrapMode_11)); }
	inline int32_t get_wrapMode_11() const { return ___wrapMode_11; }
	inline int32_t* get_address_of_wrapMode_11() { return &___wrapMode_11; }
	inline void set_wrapMode_11(int32_t value)
	{
		___wrapMode_11 = value;
	}

	inline static int32_t get_offset_of_blendMode_12() { return static_cast<int32_t>(offsetof(AnimationSettings_t3134957589, ___blendMode_12)); }
	inline int32_t get_blendMode_12() const { return ___blendMode_12; }
	inline int32_t* get_address_of_blendMode_12() { return &___blendMode_12; }
	inline void set_blendMode_12(int32_t value)
	{
		___blendMode_12 = value;
	}

	inline static int32_t get_offset_of_speed_13() { return static_cast<int32_t>(offsetof(AnimationSettings_t3134957589, ___speed_13)); }
	inline FsmFloat_t2134102846 * get_speed_13() const { return ___speed_13; }
	inline FsmFloat_t2134102846 ** get_address_of_speed_13() { return &___speed_13; }
	inline void set_speed_13(FsmFloat_t2134102846 * value)
	{
		___speed_13 = value;
		Il2CppCodeGenWriteBarrier(&___speed_13, value);
	}

	inline static int32_t get_offset_of_layer_14() { return static_cast<int32_t>(offsetof(AnimationSettings_t3134957589, ___layer_14)); }
	inline FsmInt_t1596138449 * get_layer_14() const { return ___layer_14; }
	inline FsmInt_t1596138449 ** get_address_of_layer_14() { return &___layer_14; }
	inline void set_layer_14(FsmInt_t1596138449 * value)
	{
		___layer_14 = value;
		Il2CppCodeGenWriteBarrier(&___layer_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
