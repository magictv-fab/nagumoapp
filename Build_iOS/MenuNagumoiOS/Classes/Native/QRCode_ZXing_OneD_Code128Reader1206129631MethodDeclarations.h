﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Int32[]
struct Int32U5BU5D_t3230847821;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// ZXing.OneD.Code128Reader
struct Code128Reader_t1206129631;
// ZXing.Result
struct Result_t2610723219;
// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>
struct IDictionary_2_t759989846;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"

// System.Int32[] ZXing.OneD.Code128Reader::findStartPattern(ZXing.Common.BitArray)
extern "C"  Int32U5BU5D_t3230847821* Code128Reader_findStartPattern_m1421691562 (Il2CppObject * __this /* static, unused */, BitArray_t4163851164 * ___row0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.Code128Reader::decodeCode(ZXing.Common.BitArray,System.Int32[],System.Int32,System.Int32&)
extern "C"  bool Code128Reader_decodeCode_m1864168293 (Il2CppObject * __this /* static, unused */, BitArray_t4163851164 * ___row0, Int32U5BU5D_t3230847821* ___counters1, int32_t ___rowOffset2, int32_t* ___code3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Result ZXing.OneD.Code128Reader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  Result_t2610723219 * Code128Reader_decodeRow_m2384147405 (Code128Reader_t1206129631 * __this, int32_t ___rowNumber0, BitArray_t4163851164 * ___row1, Il2CppObject* ___hints2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.Code128Reader::.ctor()
extern "C"  void Code128Reader__ctor_m500037012 (Code128Reader_t1206129631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.Code128Reader::.cctor()
extern "C"  void Code128Reader__cctor_m2134149273 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
