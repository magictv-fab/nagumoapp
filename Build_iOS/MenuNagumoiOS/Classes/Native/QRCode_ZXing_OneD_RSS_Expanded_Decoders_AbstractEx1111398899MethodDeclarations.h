﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.RSS.Expanded.Decoders.AbstractExpandedDecoder
struct AbstractExpandedDecoder_t1111398899;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder
struct GeneralAppIdDecoder_t1619283386;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"

// System.Void ZXing.OneD.RSS.Expanded.Decoders.AbstractExpandedDecoder::.ctor(ZXing.Common.BitArray)
extern "C"  void AbstractExpandedDecoder__ctor_m644015535 (AbstractExpandedDecoder_t1111398899 * __this, BitArray_t4163851164 * ___information0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.BitArray ZXing.OneD.RSS.Expanded.Decoders.AbstractExpandedDecoder::getInformation()
extern "C"  BitArray_t4163851164 * AbstractExpandedDecoder_getInformation_m4222871594 (AbstractExpandedDecoder_t1111398899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder ZXing.OneD.RSS.Expanded.Decoders.AbstractExpandedDecoder::getGeneralDecoder()
extern "C"  GeneralAppIdDecoder_t1619283386 * AbstractExpandedDecoder_getGeneralDecoder_m2083991733 (AbstractExpandedDecoder_t1111398899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.OneD.RSS.Expanded.Decoders.AbstractExpandedDecoder ZXing.OneD.RSS.Expanded.Decoders.AbstractExpandedDecoder::createDecoder(ZXing.Common.BitArray)
extern "C"  AbstractExpandedDecoder_t1111398899 * AbstractExpandedDecoder_createDecoder_m1165504457 (Il2CppObject * __this /* static, unused */, BitArray_t4163851164 * ___information0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
