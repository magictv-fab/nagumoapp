﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.camera.abstracts.TrackEventDispatcherAbstract/OnTrack
struct OnTrack_t1498492137;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.camera.abstracts.TrackEventDispatcherAbstract
struct  TrackEventDispatcherAbstract_t3123780554  : public MonoBehaviour_t667441552
{
public:
	// ARM.camera.abstracts.TrackEventDispatcherAbstract/OnTrack ARM.camera.abstracts.TrackEventDispatcherAbstract::onTrackIn
	OnTrack_t1498492137 * ___onTrackIn_2;
	// ARM.camera.abstracts.TrackEventDispatcherAbstract/OnTrack ARM.camera.abstracts.TrackEventDispatcherAbstract::onTrackOut
	OnTrack_t1498492137 * ___onTrackOut_3;

public:
	inline static int32_t get_offset_of_onTrackIn_2() { return static_cast<int32_t>(offsetof(TrackEventDispatcherAbstract_t3123780554, ___onTrackIn_2)); }
	inline OnTrack_t1498492137 * get_onTrackIn_2() const { return ___onTrackIn_2; }
	inline OnTrack_t1498492137 ** get_address_of_onTrackIn_2() { return &___onTrackIn_2; }
	inline void set_onTrackIn_2(OnTrack_t1498492137 * value)
	{
		___onTrackIn_2 = value;
		Il2CppCodeGenWriteBarrier(&___onTrackIn_2, value);
	}

	inline static int32_t get_offset_of_onTrackOut_3() { return static_cast<int32_t>(offsetof(TrackEventDispatcherAbstract_t3123780554, ___onTrackOut_3)); }
	inline OnTrack_t1498492137 * get_onTrackOut_3() const { return ___onTrackOut_3; }
	inline OnTrack_t1498492137 ** get_address_of_onTrackOut_3() { return &___onTrackOut_3; }
	inline void set_onTrackOut_3(OnTrack_t1498492137 * value)
	{
		___onTrackOut_3 = value;
		Il2CppCodeGenWriteBarrier(&___onTrackOut_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
