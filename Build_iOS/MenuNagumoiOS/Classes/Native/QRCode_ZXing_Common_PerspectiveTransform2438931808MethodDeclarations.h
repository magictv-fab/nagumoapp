﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Common.PerspectiveTransform
struct PerspectiveTransform_t2438931808;
// System.Single[]
struct SingleU5BU5D_t2316563989;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_PerspectiveTransform2438931808.h"

// System.Void ZXing.Common.PerspectiveTransform::.ctor(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void PerspectiveTransform__ctor_m561585105 (PerspectiveTransform_t2438931808 * __this, float ___a110, float ___a211, float ___a312, float ___a123, float ___a224, float ___a325, float ___a136, float ___a237, float ___a338, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.PerspectiveTransform ZXing.Common.PerspectiveTransform::quadrilateralToQuadrilateral(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  PerspectiveTransform_t2438931808 * PerspectiveTransform_quadrilateralToQuadrilateral_m2237966873 (Il2CppObject * __this /* static, unused */, float ___x00, float ___y01, float ___x12, float ___y13, float ___x24, float ___y25, float ___x36, float ___y37, float ___x0p8, float ___y0p9, float ___x1p10, float ___y1p11, float ___x2p12, float ___y2p13, float ___x3p14, float ___y3p15, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.PerspectiveTransform::transformPoints(System.Single[])
extern "C"  void PerspectiveTransform_transformPoints_m818436794 (PerspectiveTransform_t2438931808 * __this, SingleU5BU5D_t2316563989* ___points0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.PerspectiveTransform ZXing.Common.PerspectiveTransform::squareToQuadrilateral(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  PerspectiveTransform_t2438931808 * PerspectiveTransform_squareToQuadrilateral_m2151330019 (Il2CppObject * __this /* static, unused */, float ___x00, float ___y01, float ___x12, float ___y13, float ___x24, float ___y25, float ___x36, float ___y37, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.PerspectiveTransform ZXing.Common.PerspectiveTransform::quadrilateralToSquare(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  PerspectiveTransform_t2438931808 * PerspectiveTransform_quadrilateralToSquare_m786731699 (Il2CppObject * __this /* static, unused */, float ___x00, float ___y01, float ___x12, float ___y13, float ___x24, float ___y25, float ___x36, float ___y37, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.PerspectiveTransform ZXing.Common.PerspectiveTransform::buildAdjoint()
extern "C"  PerspectiveTransform_t2438931808 * PerspectiveTransform_buildAdjoint_m1575615697 (PerspectiveTransform_t2438931808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.PerspectiveTransform ZXing.Common.PerspectiveTransform::times(ZXing.Common.PerspectiveTransform)
extern "C"  PerspectiveTransform_t2438931808 * PerspectiveTransform_times_m1315391513 (PerspectiveTransform_t2438931808 * __this, PerspectiveTransform_t2438931808 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
