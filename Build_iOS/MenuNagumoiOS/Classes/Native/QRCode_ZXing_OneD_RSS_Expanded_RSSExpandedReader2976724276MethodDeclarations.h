﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.RSS.Expanded.RSSExpandedReader
struct RSSExpandedReader_t2976724276;
// ZXing.Result
struct Result_t2610723219;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>
struct IDictionary_2_t759989846;
// System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair>
struct List_1_t486584943;
// System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedRow>
struct List_1_t2931017303;
// System.Collections.Generic.IEnumerable`1<ZXing.OneD.RSS.Expanded.ExpandedPair>
struct IEnumerable_1_t2419312348;
// System.Collections.Generic.IEnumerable`1<ZXing.OneD.RSS.Expanded.ExpandedRow>
struct IEnumerable_1_t568777412;
// ZXing.OneD.RSS.Expanded.ExpandedPair
struct ExpandedPair_t3413366687;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// ZXing.OneD.RSS.FinderPattern
struct FinderPattern_t3366792524;
// ZXing.OneD.RSS.DataCharacter
struct DataCharacter_t770728801;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"
#include "QRCode_ZXing_OneD_RSS_FinderPattern3366792524.h"

// ZXing.Result ZXing.OneD.RSS.Expanded.RSSExpandedReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  Result_t2610723219 * RSSExpandedReader_decodeRow_m688892165 (RSSExpandedReader_t2976724276 * __this, int32_t ___rowNumber0, BitArray_t4163851164 * ___row1, Il2CppObject* ___hints2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.RSSExpandedReader::reset()
extern "C"  void RSSExpandedReader_reset_m3992792253 (RSSExpandedReader_t2976724276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.RSS.Expanded.RSSExpandedReader::decodeRow2pairs(System.Int32,ZXing.Common.BitArray)
extern "C"  bool RSSExpandedReader_decodeRow2pairs_m2206896653 (RSSExpandedReader_t2976724276 * __this, int32_t ___rowNumber0, BitArray_t4163851164 * ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair> ZXing.OneD.RSS.Expanded.RSSExpandedReader::checkRows(System.Boolean)
extern "C"  List_1_t486584943 * RSSExpandedReader_checkRows_m3797121183 (RSSExpandedReader_t2976724276 * __this, bool ___reverse0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair> ZXing.OneD.RSS.Expanded.RSSExpandedReader::checkRows(System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedRow>,System.Int32)
extern "C"  List_1_t486584943 * RSSExpandedReader_checkRows_m1404565789 (RSSExpandedReader_t2976724276 * __this, List_1_t2931017303 * ___collectedRows0, int32_t ___currentRow1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.RSS.Expanded.RSSExpandedReader::isValidSequence(System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair>)
extern "C"  bool RSSExpandedReader_isValidSequence_m2267305283 (Il2CppObject * __this /* static, unused */, List_1_t486584943 * ___pairs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.RSSExpandedReader::storeRow(System.Int32,System.Boolean)
extern "C"  void RSSExpandedReader_storeRow_m3663343423 (RSSExpandedReader_t2976724276 * __this, int32_t ___rowNumber0, bool ___wasReversed1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.RSSExpandedReader::removePartialRows(System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair>,System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedRow>)
extern "C"  void RSSExpandedReader_removePartialRows_m678209796 (Il2CppObject * __this /* static, unused */, List_1_t486584943 * ___pairs0, List_1_t2931017303 * ___rows1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.RSS.Expanded.RSSExpandedReader::isPartialRow(System.Collections.Generic.IEnumerable`1<ZXing.OneD.RSS.Expanded.ExpandedPair>,System.Collections.Generic.IEnumerable`1<ZXing.OneD.RSS.Expanded.ExpandedRow>)
extern "C"  bool RSSExpandedReader_isPartialRow_m456166375 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___pairs0, Il2CppObject* ___rows1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Result ZXing.OneD.RSS.Expanded.RSSExpandedReader::constructResult(System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair>)
extern "C"  Result_t2610723219 * RSSExpandedReader_constructResult_m4135952448 (Il2CppObject * __this /* static, unused */, List_1_t486584943 * ___pairs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.RSS.Expanded.RSSExpandedReader::checkChecksum()
extern "C"  bool RSSExpandedReader_checkChecksum_m2892112845 (RSSExpandedReader_t2976724276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.RSS.Expanded.RSSExpandedReader::getNextSecondBar(ZXing.Common.BitArray,System.Int32)
extern "C"  int32_t RSSExpandedReader_getNextSecondBar_m1137072582 (Il2CppObject * __this /* static, unused */, BitArray_t4163851164 * ___row0, int32_t ___initialPos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.OneD.RSS.Expanded.ExpandedPair ZXing.OneD.RSS.Expanded.RSSExpandedReader::retrieveNextPair(ZXing.Common.BitArray,System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair>,System.Int32)
extern "C"  ExpandedPair_t3413366687 * RSSExpandedReader_retrieveNextPair_m3910903230 (RSSExpandedReader_t2976724276 * __this, BitArray_t4163851164 * ___row0, List_1_t486584943 * ___previousPairs1, int32_t ___rowNumber2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.RSS.Expanded.RSSExpandedReader::findNextPair(ZXing.Common.BitArray,System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair>,System.Int32)
extern "C"  bool RSSExpandedReader_findNextPair_m2511853886 (RSSExpandedReader_t2976724276 * __this, BitArray_t4163851164 * ___row0, List_1_t486584943 * ___previousPairs1, int32_t ___forcedOffset2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.RSSExpandedReader::reverseCounters(System.Int32[])
extern "C"  void RSSExpandedReader_reverseCounters_m2879837078 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___counters0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.OneD.RSS.FinderPattern ZXing.OneD.RSS.Expanded.RSSExpandedReader::parseFoundFinderPattern(ZXing.Common.BitArray,System.Int32,System.Boolean)
extern "C"  FinderPattern_t3366792524 * RSSExpandedReader_parseFoundFinderPattern_m2192906709 (RSSExpandedReader_t2976724276 * __this, BitArray_t4163851164 * ___row0, int32_t ___rowNumber1, bool ___oddPattern2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.OneD.RSS.DataCharacter ZXing.OneD.RSS.Expanded.RSSExpandedReader::decodeDataCharacter(ZXing.Common.BitArray,ZXing.OneD.RSS.FinderPattern,System.Boolean,System.Boolean)
extern "C"  DataCharacter_t770728801 * RSSExpandedReader_decodeDataCharacter_m4104311120 (RSSExpandedReader_t2976724276 * __this, BitArray_t4163851164 * ___row0, FinderPattern_t3366792524 * ___pattern1, bool ___isOddPattern2, bool ___leftChar3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.RSS.Expanded.RSSExpandedReader::isNotA1left(ZXing.OneD.RSS.FinderPattern,System.Boolean,System.Boolean)
extern "C"  bool RSSExpandedReader_isNotA1left_m1825994394 (Il2CppObject * __this /* static, unused */, FinderPattern_t3366792524 * ___pattern0, bool ___isOddPattern1, bool ___leftChar2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.RSS.Expanded.RSSExpandedReader::adjustOddEvenCounts(System.Int32)
extern "C"  bool RSSExpandedReader_adjustOddEvenCounts_m631278705 (RSSExpandedReader_t2976724276 * __this, int32_t ___numModules0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.RSSExpandedReader::.ctor()
extern "C"  void RSSExpandedReader__ctor_m3716045296 (RSSExpandedReader_t2976724276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.RSSExpandedReader::.cctor()
extern "C"  void RSSExpandedReader__cctor_m3046158269 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
