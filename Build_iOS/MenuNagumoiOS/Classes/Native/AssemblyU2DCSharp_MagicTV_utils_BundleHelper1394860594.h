﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<MagicTV.vo.MetadataVO,System.String>
struct Func_2_t3445325228;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.utils.BundleHelper
struct  BundleHelper_t1394860594  : public Il2CppObject
{
public:

public:
};

struct BundleHelper_t1394860594_StaticFields
{
public:
	// System.Func`2<MagicTV.vo.MetadataVO,System.String> MagicTV.utils.BundleHelper::<>f__am$cache0
	Func_2_t3445325228 * ___U3CU3Ef__amU24cache0_0;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(BundleHelper_t1394860594_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_t3445325228 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_t3445325228 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_t3445325228 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
