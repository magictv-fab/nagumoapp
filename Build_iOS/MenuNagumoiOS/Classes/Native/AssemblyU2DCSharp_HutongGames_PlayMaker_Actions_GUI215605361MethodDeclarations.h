﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutEmailField
struct GUILayoutEmailField_t215605361;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutEmailField::.ctor()
extern "C"  void GUILayoutEmailField__ctor_m2284622309 (GUILayoutEmailField_t215605361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutEmailField::Reset()
extern "C"  void GUILayoutEmailField_Reset_m4226022546 (GUILayoutEmailField_t215605361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutEmailField::OnGUI()
extern "C"  void GUILayoutEmailField_OnGUI_m1780020959 (GUILayoutEmailField_t215605361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
