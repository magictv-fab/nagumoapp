﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UpdateTransformMetadata
struct UpdateTransformMetadata_t2275182802;
// MagicTV.abstracts.InAppAbstract
struct InAppAbstract_t382673128;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_abstracts_InAppAbstract382673128.h"

// System.Void UpdateTransformMetadata::.ctor()
extern "C"  void UpdateTransformMetadata__ctor_m1716101849 (UpdateTransformMetadata_t2275182802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdateTransformMetadata::setBundleID(System.Int32)
extern "C"  void UpdateTransformMetadata_setBundleID_m1221185479 (UpdateTransformMetadata_t2275182802 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdateTransformMetadata::setInAppGO(MagicTV.abstracts.InAppAbstract)
extern "C"  void UpdateTransformMetadata_setInAppGO_m2617087715 (UpdateTransformMetadata_t2275182802 * __this, InAppAbstract_t382673128 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdateTransformMetadata::Start()
extern "C"  void UpdateTransformMetadata_Start_m663239641 (UpdateTransformMetadata_t2275182802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdateTransformMetadata::___OnGUI()
extern "C"  void UpdateTransformMetadata____OnGUI_m2630952072 (UpdateTransformMetadata_t2275182802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdateTransformMetadata::completeSend(System.Boolean)
extern "C"  void UpdateTransformMetadata_completeSend_m1048326595 (UpdateTransformMetadata_t2275182802 * __this, bool ___success0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdateTransformMetadata::sendPosition()
extern "C"  void UpdateTransformMetadata_sendPosition_m2388126140 (UpdateTransformMetadata_t2275182802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
