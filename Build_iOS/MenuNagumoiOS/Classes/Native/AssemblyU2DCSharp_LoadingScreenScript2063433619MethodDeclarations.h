﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoadingScreenScript
struct LoadingScreenScript_t2063433619;

#include "codegen/il2cpp-codegen.h"

// System.Void LoadingScreenScript::.ctor()
extern "C"  void LoadingScreenScript__ctor_m3909839160 (LoadingScreenScript_t2063433619 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingScreenScript::Start()
extern "C"  void LoadingScreenScript_Start_m2856976952 (LoadingScreenScript_t2063433619 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingScreenScript::Update()
extern "C"  void LoadingScreenScript_Update_m2672791765 (LoadingScreenScript_t2063433619 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingScreenScript::OnGUI()
extern "C"  void LoadingScreenScript_OnGUI_m3405237810 (LoadingScreenScript_t2063433619 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
