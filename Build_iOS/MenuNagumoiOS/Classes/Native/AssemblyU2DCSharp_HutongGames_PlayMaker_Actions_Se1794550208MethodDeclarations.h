﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFsmVector3
struct SetFsmVector3_t1794550208;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFsmVector3::.ctor()
extern "C"  void SetFsmVector3__ctor_m2393612150 (SetFsmVector3_t1794550208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmVector3::Reset()
extern "C"  void SetFsmVector3_Reset_m40045091 (SetFsmVector3_t1794550208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmVector3::OnEnter()
extern "C"  void SetFsmVector3_OnEnter_m1845501389 (SetFsmVector3_t1794550208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmVector3::DoSetFsmVector3()
extern "C"  void SetFsmVector3_DoSetFsmVector3_m405817403 (SetFsmVector3_t1794550208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmVector3::OnUpdate()
extern "C"  void SetFsmVector3_OnUpdate_m509527574 (SetFsmVector3_t1794550208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
