﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.PerSecond
struct  PerSecond_t38067007  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.PerSecond::floatValue
	FsmFloat_t2134102846 * ___floatValue_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.PerSecond::storeResult
	FsmFloat_t2134102846 * ___storeResult_10;
	// System.Boolean HutongGames.PlayMaker.Actions.PerSecond::everyFrame
	bool ___everyFrame_11;

public:
	inline static int32_t get_offset_of_floatValue_9() { return static_cast<int32_t>(offsetof(PerSecond_t38067007, ___floatValue_9)); }
	inline FsmFloat_t2134102846 * get_floatValue_9() const { return ___floatValue_9; }
	inline FsmFloat_t2134102846 ** get_address_of_floatValue_9() { return &___floatValue_9; }
	inline void set_floatValue_9(FsmFloat_t2134102846 * value)
	{
		___floatValue_9 = value;
		Il2CppCodeGenWriteBarrier(&___floatValue_9, value);
	}

	inline static int32_t get_offset_of_storeResult_10() { return static_cast<int32_t>(offsetof(PerSecond_t38067007, ___storeResult_10)); }
	inline FsmFloat_t2134102846 * get_storeResult_10() const { return ___storeResult_10; }
	inline FsmFloat_t2134102846 ** get_address_of_storeResult_10() { return &___storeResult_10; }
	inline void set_storeResult_10(FsmFloat_t2134102846 * value)
	{
		___storeResult_10 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_10, value);
	}

	inline static int32_t get_offset_of_everyFrame_11() { return static_cast<int32_t>(offsetof(PerSecond_t38067007, ___everyFrame_11)); }
	inline bool get_everyFrame_11() const { return ___everyFrame_11; }
	inline bool* get_address_of_everyFrame_11() { return &___everyFrame_11; }
	inline void set_everyFrame_11(bool value)
	{
		___everyFrame_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
