﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// OfertasManagerCRM
struct OfertasManagerCRM_t3100875987;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OfertasManagerCRM/<LoadLoadeds>c__Iterator7
struct  U3CLoadLoadedsU3Ec__Iterator7_t2167093852  : public Il2CppObject
{
public:
	// System.Int32 OfertasManagerCRM/<LoadLoadeds>c__Iterator7::index
	int32_t ___index_0;
	// System.Int32 OfertasManagerCRM/<LoadLoadeds>c__Iterator7::<i>__0
	int32_t ___U3CiU3E__0_1;
	// System.Int32 OfertasManagerCRM/<LoadLoadeds>c__Iterator7::<i>__1
	int32_t ___U3CiU3E__1_2;
	// System.Int32 OfertasManagerCRM/<LoadLoadeds>c__Iterator7::$PC
	int32_t ___U24PC_3;
	// System.Object OfertasManagerCRM/<LoadLoadeds>c__Iterator7::$current
	Il2CppObject * ___U24current_4;
	// System.Int32 OfertasManagerCRM/<LoadLoadeds>c__Iterator7::<$>index
	int32_t ___U3CU24U3Eindex_5;
	// OfertasManagerCRM OfertasManagerCRM/<LoadLoadeds>c__Iterator7::<>f__this
	OfertasManagerCRM_t3100875987 * ___U3CU3Ef__this_6;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(U3CLoadLoadedsU3Ec__Iterator7_t2167093852, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadLoadedsU3Ec__Iterator7_t2167093852, ___U3CiU3E__0_1)); }
	inline int32_t get_U3CiU3E__0_1() const { return ___U3CiU3E__0_1; }
	inline int32_t* get_address_of_U3CiU3E__0_1() { return &___U3CiU3E__0_1; }
	inline void set_U3CiU3E__0_1(int32_t value)
	{
		___U3CiU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__1_2() { return static_cast<int32_t>(offsetof(U3CLoadLoadedsU3Ec__Iterator7_t2167093852, ___U3CiU3E__1_2)); }
	inline int32_t get_U3CiU3E__1_2() const { return ___U3CiU3E__1_2; }
	inline int32_t* get_address_of_U3CiU3E__1_2() { return &___U3CiU3E__1_2; }
	inline void set_U3CiU3E__1_2(int32_t value)
	{
		___U3CiU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CLoadLoadedsU3Ec__Iterator7_t2167093852, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CLoadLoadedsU3Ec__Iterator7_t2167093852, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eindex_5() { return static_cast<int32_t>(offsetof(U3CLoadLoadedsU3Ec__Iterator7_t2167093852, ___U3CU24U3Eindex_5)); }
	inline int32_t get_U3CU24U3Eindex_5() const { return ___U3CU24U3Eindex_5; }
	inline int32_t* get_address_of_U3CU24U3Eindex_5() { return &___U3CU24U3Eindex_5; }
	inline void set_U3CU24U3Eindex_5(int32_t value)
	{
		___U3CU24U3Eindex_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_6() { return static_cast<int32_t>(offsetof(U3CLoadLoadedsU3Ec__Iterator7_t2167093852, ___U3CU3Ef__this_6)); }
	inline OfertasManagerCRM_t3100875987 * get_U3CU3Ef__this_6() const { return ___U3CU3Ef__this_6; }
	inline OfertasManagerCRM_t3100875987 ** get_address_of_U3CU3Ef__this_6() { return &___U3CU3Ef__this_6; }
	inline void set_U3CU3Ef__this_6(OfertasManagerCRM_t3100875987 * value)
	{
		___U3CU3Ef__this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
