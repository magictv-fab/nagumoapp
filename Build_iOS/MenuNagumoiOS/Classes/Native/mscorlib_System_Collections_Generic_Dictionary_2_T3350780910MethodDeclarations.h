﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2401962938MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m1869104127(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t3350780910 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m3076305956_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m302463321(__this, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Transform_1_t3350780910 *, int32_t, Func_1_t3890737231 *, const MethodInfo*))Transform_1_Invoke_m3533572180_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m2231411204(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t3350780910 *, int32_t, Func_1_t3890737231 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m222801023_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m2027439505(__this, ___result0, method) ((  DictionaryEntry_t1751606614  (*) (Transform_1_t3350780910 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m482466742_gshared)(__this, ___result0, method)
