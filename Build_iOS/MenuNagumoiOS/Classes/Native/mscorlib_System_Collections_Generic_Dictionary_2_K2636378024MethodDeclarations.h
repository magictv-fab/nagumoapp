﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2916457164MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m960135474(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2636378024 *, Dictionary_2_t1009618573 *, const MethodInfo*))KeyCollection__ctor_m1378423703_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3307692260(__this, ___item0, method) ((  void (*) (KeyCollection_t2636378024 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3305513823_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2564321115(__this, method) ((  void (*) (KeyCollection_t2636378024 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1357616278_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3078042022(__this, ___item0, method) ((  bool (*) (KeyCollection_t2636378024 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3749742731_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3708802187(__this, ___item0, method) ((  bool (*) (KeyCollection_t2636378024 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3369836720_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2998669719(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2636378024 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4043272338_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m1839145805(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2636378024 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m2304067592_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1447728392(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2636378024 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3830333699_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1646974855(__this, method) ((  bool (*) (KeyCollection_t2636378024 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2906261804_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2969849081(__this, method) ((  bool (*) (KeyCollection_t2636378024 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2752600286_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m4281445861(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2636378024 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1476451402_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m1416179687(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2636378024 *, BarcodeFormatU5BU5D_t352465924*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3058865740_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::GetEnumerator()
#define KeyCollection_GetEnumerator_m1067951306(__this, method) ((  Enumerator_t1624554627  (*) (KeyCollection_t2636378024 *, const MethodInfo*))KeyCollection_GetEnumerator_m947525103_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::get_Count()
#define KeyCollection_get_Count_m3546198207(__this, method) ((  int32_t (*) (KeyCollection_t2636378024 *, const MethodInfo*))KeyCollection_get_Count_m774029348_gshared)(__this, method)
