﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ButtonsAction
struct ButtonsAction_t1330574583;

#include "codegen/il2cpp-codegen.h"

// System.Void ButtonsAction::.ctor()
extern "C"  void ButtonsAction__ctor_m2715423316 (ButtonsAction_t1330574583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonsAction::Start()
extern "C"  void ButtonsAction_Start_m1662561108 (ButtonsAction_t1330574583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonsAction::Update()
extern "C"  void ButtonsAction_Update_m5638969 (ButtonsAction_t1330574583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonsAction::ButtonBack()
extern "C"  void ButtonsAction_ButtonBack_m599997993 (ButtonsAction_t1330574583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
