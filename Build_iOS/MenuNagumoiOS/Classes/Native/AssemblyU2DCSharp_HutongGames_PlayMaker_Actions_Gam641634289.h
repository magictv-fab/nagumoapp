﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GameObjectChanged
struct  GameObjectChanged_t641634289  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GameObjectChanged::gameObjectVariable
	FsmGameObject_t1697147867 * ___gameObjectVariable_9;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GameObjectChanged::changedEvent
	FsmEvent_t2133468028 * ___changedEvent_10;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GameObjectChanged::storeResult
	FsmBool_t1075959796 * ___storeResult_11;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.GameObjectChanged::previousValue
	GameObject_t3674682005 * ___previousValue_12;

public:
	inline static int32_t get_offset_of_gameObjectVariable_9() { return static_cast<int32_t>(offsetof(GameObjectChanged_t641634289, ___gameObjectVariable_9)); }
	inline FsmGameObject_t1697147867 * get_gameObjectVariable_9() const { return ___gameObjectVariable_9; }
	inline FsmGameObject_t1697147867 ** get_address_of_gameObjectVariable_9() { return &___gameObjectVariable_9; }
	inline void set_gameObjectVariable_9(FsmGameObject_t1697147867 * value)
	{
		___gameObjectVariable_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObjectVariable_9, value);
	}

	inline static int32_t get_offset_of_changedEvent_10() { return static_cast<int32_t>(offsetof(GameObjectChanged_t641634289, ___changedEvent_10)); }
	inline FsmEvent_t2133468028 * get_changedEvent_10() const { return ___changedEvent_10; }
	inline FsmEvent_t2133468028 ** get_address_of_changedEvent_10() { return &___changedEvent_10; }
	inline void set_changedEvent_10(FsmEvent_t2133468028 * value)
	{
		___changedEvent_10 = value;
		Il2CppCodeGenWriteBarrier(&___changedEvent_10, value);
	}

	inline static int32_t get_offset_of_storeResult_11() { return static_cast<int32_t>(offsetof(GameObjectChanged_t641634289, ___storeResult_11)); }
	inline FsmBool_t1075959796 * get_storeResult_11() const { return ___storeResult_11; }
	inline FsmBool_t1075959796 ** get_address_of_storeResult_11() { return &___storeResult_11; }
	inline void set_storeResult_11(FsmBool_t1075959796 * value)
	{
		___storeResult_11 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_11, value);
	}

	inline static int32_t get_offset_of_previousValue_12() { return static_cast<int32_t>(offsetof(GameObjectChanged_t641634289, ___previousValue_12)); }
	inline GameObject_t3674682005 * get_previousValue_12() const { return ___previousValue_12; }
	inline GameObject_t3674682005 ** get_address_of_previousValue_12() { return &___previousValue_12; }
	inline void set_previousValue_12(GameObject_t3674682005 * value)
	{
		___previousValue_12 = value;
		Il2CppCodeGenWriteBarrier(&___previousValue_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
