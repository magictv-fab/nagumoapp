﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2607021105MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3551405387(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2326941965 *, Dictionary_2_t1009618573 *, const MethodInfo*))Enumerator__ctor_m1649207152_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2370583862(__this, method) ((  Il2CppObject * (*) (Enumerator_t2326941965 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m437284081_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1369068170(__this, method) ((  void (*) (Enumerator_t2326941965 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m837083973_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1789683987(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t2326941965 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1295832654_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m915028370(__this, method) ((  Il2CppObject * (*) (Enumerator_t2326941965 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m157053197_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m4217371556(__this, method) ((  Il2CppObject * (*) (Enumerator_t2326941965 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1652703327_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::MoveNext()
#define Enumerator_MoveNext_m167681910(__this, method) ((  bool (*) (Enumerator_t2326941965 *, const MethodInfo*))Enumerator_MoveNext_m2959870769_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::get_Current()
#define Enumerator_get_Current_m861513210(__this, method) ((  KeyValuePair_2_t908399279  (*) (Enumerator_t2326941965 *, const MethodInfo*))Enumerator_get_Current_m1243001311_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m917198275(__this, method) ((  int32_t (*) (Enumerator_t2326941965 *, const MethodInfo*))Enumerator_get_CurrentKey_m430159038_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2578274471(__this, method) ((  Func_1_t3890737231 * (*) (Enumerator_t2326941965 *, const MethodInfo*))Enumerator_get_CurrentValue_m1317751906_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::Reset()
#define Enumerator_Reset_m987889437(__this, method) ((  void (*) (Enumerator_t2326941965 *, const MethodInfo*))Enumerator_Reset_m3150272962_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::VerifyState()
#define Enumerator_VerifyState_m1397671270(__this, method) ((  void (*) (Enumerator_t2326941965 *, const MethodInfo*))Enumerator_VerifyState_m1730859083_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1582198606(__this, method) ((  void (*) (Enumerator_t2326941965 *, const MethodInfo*))Enumerator_VerifyCurrent_m3948106995_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ZXing.BarcodeFormat,System.Func`1<ZXing.Writer>>::Dispose()
#define Enumerator_Dispose_m2697405613(__this, method) ((  void (*) (Enumerator_t2326941965 *, const MethodInfo*))Enumerator_Dispose_m1983801874_gshared)(__this, method)
