﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.utils.request.ARMRequestVO
struct ARMRequestVO_t2431191322;
// System.String
struct String_t;
// ARM.utils.request.IRequestParameter
struct IRequestParameter_t2549190997;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ARM.utils.request.ARMRequestVO::.ctor(System.String,ARM.utils.request.IRequestParameter)
extern "C"  void ARMRequestVO__ctor_m3711120216 (ARMRequestVO_t2431191322 * __this, String_t* ___url0, Il2CppObject * ___metaData1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.request.ARMRequestVO::.ctor(System.String)
extern "C"  void ARMRequestVO__ctor_m132044417 (ARMRequestVO_t2431191322 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
