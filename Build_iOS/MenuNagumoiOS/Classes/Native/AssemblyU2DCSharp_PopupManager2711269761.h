﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_iTween_EaseType2734598229.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PopupManager
struct  PopupManager_t2711269761  : public MonoBehaviour_t667441552
{
public:
	// System.Single PopupManager::time
	float ___time_2;
	// System.Single PopupManager::lifeTime
	float ___lifeTime_3;
	// iTween/EaseType PopupManager::easetype
	int32_t ___easetype_4;
	// UnityEngine.GameObject PopupManager::fundo
	GameObject_t3674682005 * ___fundo_5;
	// System.Boolean PopupManager::automaticClose
	bool ___automaticClose_6;

public:
	inline static int32_t get_offset_of_time_2() { return static_cast<int32_t>(offsetof(PopupManager_t2711269761, ___time_2)); }
	inline float get_time_2() const { return ___time_2; }
	inline float* get_address_of_time_2() { return &___time_2; }
	inline void set_time_2(float value)
	{
		___time_2 = value;
	}

	inline static int32_t get_offset_of_lifeTime_3() { return static_cast<int32_t>(offsetof(PopupManager_t2711269761, ___lifeTime_3)); }
	inline float get_lifeTime_3() const { return ___lifeTime_3; }
	inline float* get_address_of_lifeTime_3() { return &___lifeTime_3; }
	inline void set_lifeTime_3(float value)
	{
		___lifeTime_3 = value;
	}

	inline static int32_t get_offset_of_easetype_4() { return static_cast<int32_t>(offsetof(PopupManager_t2711269761, ___easetype_4)); }
	inline int32_t get_easetype_4() const { return ___easetype_4; }
	inline int32_t* get_address_of_easetype_4() { return &___easetype_4; }
	inline void set_easetype_4(int32_t value)
	{
		___easetype_4 = value;
	}

	inline static int32_t get_offset_of_fundo_5() { return static_cast<int32_t>(offsetof(PopupManager_t2711269761, ___fundo_5)); }
	inline GameObject_t3674682005 * get_fundo_5() const { return ___fundo_5; }
	inline GameObject_t3674682005 ** get_address_of_fundo_5() { return &___fundo_5; }
	inline void set_fundo_5(GameObject_t3674682005 * value)
	{
		___fundo_5 = value;
		Il2CppCodeGenWriteBarrier(&___fundo_5, value);
	}

	inline static int32_t get_offset_of_automaticClose_6() { return static_cast<int32_t>(offsetof(PopupManager_t2711269761, ___automaticClose_6)); }
	inline bool get_automaticClose_6() const { return ___automaticClose_6; }
	inline bool* get_address_of_automaticClose_6() { return &___automaticClose_6; }
	inline void set_automaticClose_6(bool value)
	{
		___automaticClose_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
