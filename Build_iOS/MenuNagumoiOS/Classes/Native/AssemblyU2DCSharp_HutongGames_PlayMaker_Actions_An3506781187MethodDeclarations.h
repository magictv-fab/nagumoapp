﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AnimatorCrossFade
struct AnimatorCrossFade_t3506781187;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AnimatorCrossFade::.ctor()
extern "C"  void AnimatorCrossFade__ctor_m2229679251 (AnimatorCrossFade_t3506781187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorCrossFade::Reset()
extern "C"  void AnimatorCrossFade_Reset_m4171079488 (AnimatorCrossFade_t3506781187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorCrossFade::OnEnter()
extern "C"  void AnimatorCrossFade_OnEnter_m3219775402 (AnimatorCrossFade_t3506781187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
