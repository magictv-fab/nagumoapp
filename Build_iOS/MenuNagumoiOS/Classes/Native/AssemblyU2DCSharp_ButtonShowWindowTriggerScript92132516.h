﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GenericWindowControllerScript
struct GenericWindowControllerScript_t248075822;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonShowWindowTriggerScript
struct  ButtonShowWindowTriggerScript_t92132516  : public MonoBehaviour_t667441552
{
public:
	// GenericWindowControllerScript ButtonShowWindowTriggerScript::TargetWindow
	GenericWindowControllerScript_t248075822 * ___TargetWindow_2;

public:
	inline static int32_t get_offset_of_TargetWindow_2() { return static_cast<int32_t>(offsetof(ButtonShowWindowTriggerScript_t92132516, ___TargetWindow_2)); }
	inline GenericWindowControllerScript_t248075822 * get_TargetWindow_2() const { return ___TargetWindow_2; }
	inline GenericWindowControllerScript_t248075822 ** get_address_of_TargetWindow_2() { return &___TargetWindow_2; }
	inline void set_TargetWindow_2(GenericWindowControllerScript_t248075822 * value)
	{
		___TargetWindow_2 = value;
		Il2CppCodeGenWriteBarrier(&___TargetWindow_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
