﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.ReconstructionAbstractBehaviour
struct ReconstructionAbstractBehaviour_t1860057025;
// Vuforia.Reconstruction
struct Reconstruction_t1162784518;
// Vuforia.ISmartTerrainEventHandler
struct ISmartTerrainEventHandler_t2222092879;
// System.Action`1<Vuforia.SmartTerrainInitializationInfo>
struct Action_1_t983143684;
// System.Action`1<Vuforia.Prop>
struct Action_1_t2561125293;
// System.Action`1<Vuforia.Surface>
struct Action_1_t3490205855;
// Vuforia.PropAbstractBehaviour
struct PropAbstractBehaviour_t1293468098;
// Vuforia.Prop
struct Prop_t2165309157;
// Vuforia.SurfaceAbstractBehaviour
struct SurfaceAbstractBehaviour_t142368528;
// Vuforia.Surface
struct Surface_t3094389719;
// System.Collections.Generic.IEnumerable`1<Vuforia.Prop>
struct IEnumerable_1_t1171254818;
// System.Collections.Generic.IEnumerable`1<Vuforia.Surface>
struct IEnumerable_1_t2100335380;
// Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData[]
struct SmartTerrainRevisionDataU5BU5D_t525370068;
// Vuforia.VuforiaManagerImpl/SurfaceData[]
struct SurfaceDataU5BU5D_t1400262182;
// Vuforia.VuforiaManagerImpl/PropData[]
struct PropDataU5BU5D_t921971240;
// Vuforia.SmartTerrainTrackable
struct SmartTerrainTrackable_t1276145027;
// UnityEngine.Mesh
struct Mesh_t4241756145;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Collections.Generic.List`1<Vuforia.Prop>
struct List_1_t3533494709;
// System.Collections.Generic.List`1<Vuforia.Surface>
struct List_1_t167607975;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PropAbstr1293468098.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceAbs142368528.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMa1047832367.h"
#include "UnityEngine_UnityEngine_Mesh4241756145.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"

// Vuforia.Reconstruction Vuforia.ReconstructionAbstractBehaviour::get_Reconstruction()
extern "C"  Il2CppObject * ReconstructionAbstractBehaviour_get_Reconstruction_m2379354184 (ReconstructionAbstractBehaviour_t1860057025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::Start()
extern "C"  void ReconstructionAbstractBehaviour_Start_m1205861668 (ReconstructionAbstractBehaviour_t1860057025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::OnDrawGizmos()
extern "C"  void ReconstructionAbstractBehaviour_OnDrawGizmos_m1803189372 (ReconstructionAbstractBehaviour_t1860057025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::RegisterSmartTerrainEventHandler(Vuforia.ISmartTerrainEventHandler)
extern "C"  void ReconstructionAbstractBehaviour_RegisterSmartTerrainEventHandler_m3313516602 (ReconstructionAbstractBehaviour_t1860057025 * __this, Il2CppObject * ___trackableEventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionAbstractBehaviour::UnregisterSmartTerrainEventHandler(Vuforia.ISmartTerrainEventHandler)
extern "C"  bool ReconstructionAbstractBehaviour_UnregisterSmartTerrainEventHandler_m2945531885 (ReconstructionAbstractBehaviour_t1860057025 * __this, Il2CppObject * ___trackableEventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::RegisterInitializedCallback(System.Action`1<Vuforia.SmartTerrainInitializationInfo>)
extern "C"  void ReconstructionAbstractBehaviour_RegisterInitializedCallback_m1676354968 (ReconstructionAbstractBehaviour_t1860057025 * __this, Action_1_t983143684 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UnregisterInitializedCallback(System.Action`1<Vuforia.SmartTerrainInitializationInfo>)
extern "C"  void ReconstructionAbstractBehaviour_UnregisterInitializedCallback_m3828206065 (ReconstructionAbstractBehaviour_t1860057025 * __this, Action_1_t983143684 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::RegisterPropCreatedCallback(System.Action`1<Vuforia.Prop>)
extern "C"  void ReconstructionAbstractBehaviour_RegisterPropCreatedCallback_m958985328 (ReconstructionAbstractBehaviour_t1860057025 * __this, Action_1_t2561125293 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UnregisterPropCreatedCallback(System.Action`1<Vuforia.Prop>)
extern "C"  void ReconstructionAbstractBehaviour_UnregisterPropCreatedCallback_m3888109065 (ReconstructionAbstractBehaviour_t1860057025 * __this, Action_1_t2561125293 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::RegisterPropUpdatedCallback(System.Action`1<Vuforia.Prop>)
extern "C"  void ReconstructionAbstractBehaviour_RegisterPropUpdatedCallback_m1264778045 (ReconstructionAbstractBehaviour_t1860057025 * __this, Action_1_t2561125293 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UnregisterPropUpdatedCallback(System.Action`1<Vuforia.Prop>)
extern "C"  void ReconstructionAbstractBehaviour_UnregisterPropUpdatedCallback_m4193901782 (ReconstructionAbstractBehaviour_t1860057025 * __this, Action_1_t2561125293 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::RegisterPropDeletedCallback(System.Action`1<Vuforia.Prop>)
extern "C"  void ReconstructionAbstractBehaviour_RegisterPropDeletedCallback_m32718367 (ReconstructionAbstractBehaviour_t1860057025 * __this, Action_1_t2561125293 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UnregisterPropDeletedCallback(System.Action`1<Vuforia.Prop>)
extern "C"  void ReconstructionAbstractBehaviour_UnregisterPropDeletedCallback_m2961842104 (ReconstructionAbstractBehaviour_t1860057025 * __this, Action_1_t2561125293 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::RegisterSurfaceCreatedCallback(System.Action`1<Vuforia.Surface>)
extern "C"  void ReconstructionAbstractBehaviour_RegisterSurfaceCreatedCallback_m553006512 (ReconstructionAbstractBehaviour_t1860057025 * __this, Action_1_t3490205855 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UnregisterSurfaceCreatedCallback(System.Action`1<Vuforia.Surface>)
extern "C"  void ReconstructionAbstractBehaviour_UnregisterSurfaceCreatedCallback_m4273586313 (ReconstructionAbstractBehaviour_t1860057025 * __this, Action_1_t3490205855 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::RegisterSurfaceUpdatedCallback(System.Action`1<Vuforia.Surface>)
extern "C"  void ReconstructionAbstractBehaviour_RegisterSurfaceUpdatedCallback_m798203843 (ReconstructionAbstractBehaviour_t1860057025 * __this, Action_1_t3490205855 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UnregisterSurfaceUpdatedCallback(System.Action`1<Vuforia.Surface>)
extern "C"  void ReconstructionAbstractBehaviour_UnregisterSurfaceUpdatedCallback_m223816348 (ReconstructionAbstractBehaviour_t1860057025 * __this, Action_1_t3490205855 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::RegisterSurfaceDeletedCallback(System.Action`1<Vuforia.Surface>)
extern "C"  void ReconstructionAbstractBehaviour_RegisterSurfaceDeletedCallback_m1298848161 (ReconstructionAbstractBehaviour_t1860057025 * __this, Action_1_t3490205855 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UnregisterSurfaceDeletedCallback(System.Action`1<Vuforia.Surface>)
extern "C"  void ReconstructionAbstractBehaviour_UnregisterSurfaceDeletedCallback_m724460666 (ReconstructionAbstractBehaviour_t1860057025 * __this, Action_1_t3490205855 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.PropAbstractBehaviour Vuforia.ReconstructionAbstractBehaviour::AssociateProp(Vuforia.PropAbstractBehaviour,Vuforia.Prop)
extern "C"  PropAbstractBehaviour_t1293468098 * ReconstructionAbstractBehaviour_AssociateProp_m3949632283 (ReconstructionAbstractBehaviour_t1860057025 * __this, PropAbstractBehaviour_t1293468098 * ___templateBehaviour0, Il2CppObject * ___newProp1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.SurfaceAbstractBehaviour Vuforia.ReconstructionAbstractBehaviour::AssociateSurface(Vuforia.SurfaceAbstractBehaviour,Vuforia.Surface)
extern "C"  SurfaceAbstractBehaviour_t142368528 * ReconstructionAbstractBehaviour_AssociateSurface_m2064989829 (ReconstructionAbstractBehaviour_t1860057025 * __this, SurfaceAbstractBehaviour_t142368528 * ___templateBehaviour0, Il2CppObject * ___newSurface1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.Prop> Vuforia.ReconstructionAbstractBehaviour::GetActiveProps()
extern "C"  Il2CppObject* ReconstructionAbstractBehaviour_GetActiveProps_m1029168549 (ReconstructionAbstractBehaviour_t1860057025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionAbstractBehaviour::TryGetPropBehaviour(Vuforia.Prop,Vuforia.PropAbstractBehaviour&)
extern "C"  bool ReconstructionAbstractBehaviour_TryGetPropBehaviour_m1173776014 (ReconstructionAbstractBehaviour_t1860057025 * __this, Il2CppObject * ___prop0, PropAbstractBehaviour_t1293468098 ** ___behaviour1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.Surface> Vuforia.ReconstructionAbstractBehaviour::GetActiveSurfaces()
extern "C"  Il2CppObject* ReconstructionAbstractBehaviour_GetActiveSurfaces_m2664390161 (ReconstructionAbstractBehaviour_t1860057025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionAbstractBehaviour::TryGetSurfaceBehaviour(Vuforia.Surface,Vuforia.SurfaceAbstractBehaviour&)
extern "C"  bool ReconstructionAbstractBehaviour_TryGetSurfaceBehaviour_m3282041394 (ReconstructionAbstractBehaviour_t1860057025 * __this, Il2CppObject * ___surface0, SurfaceAbstractBehaviour_t142368528 ** ___behaviour1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::Initialize(Vuforia.Reconstruction)
extern "C"  void ReconstructionAbstractBehaviour_Initialize_m261649856 (ReconstructionAbstractBehaviour_t1860057025 * __this, Il2CppObject * ___reconstruction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::Deinitialize()
extern "C"  void ReconstructionAbstractBehaviour_Deinitialize_m261005361 (ReconstructionAbstractBehaviour_t1860057025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UpdateSmartTerrainData(Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData[],Vuforia.VuforiaManagerImpl/SurfaceData[],Vuforia.VuforiaManagerImpl/PropData[])
extern "C"  void ReconstructionAbstractBehaviour_UpdateSmartTerrainData_m2591311714 (ReconstructionAbstractBehaviour_t1860057025 * __this, SmartTerrainRevisionDataU5BU5D_t525370068* ___smartTerrainRevisions0, SurfaceDataU5BU5D_t1400262182* ___updatedSurfaces1, PropDataU5BU5D_t921971240* ___updatedProps2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::SetBehavioursToNotFound()
extern "C"  void ReconstructionAbstractBehaviour_SetBehavioursToNotFound_m244639236 (ReconstructionAbstractBehaviour_t1860057025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::ClearOnReset()
extern "C"  void ReconstructionAbstractBehaviour_ClearOnReset_m269311107 (ReconstructionAbstractBehaviour_t1860057025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::OnReconstructionRemoved()
extern "C"  void ReconstructionAbstractBehaviour_OnReconstructionRemoved_m3929146463 (ReconstructionAbstractBehaviour_t1860057025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.PropAbstractBehaviour Vuforia.ReconstructionAbstractBehaviour::InstantiatePropBehaviour(Vuforia.PropAbstractBehaviour)
extern "C"  PropAbstractBehaviour_t1293468098 * ReconstructionAbstractBehaviour_InstantiatePropBehaviour_m947540029 (Il2CppObject * __this /* static, unused */, PropAbstractBehaviour_t1293468098 * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::AssociatePropBehaviour(Vuforia.Prop,Vuforia.PropAbstractBehaviour)
extern "C"  void ReconstructionAbstractBehaviour_AssociatePropBehaviour_m2753819277 (ReconstructionAbstractBehaviour_t1860057025 * __this, Il2CppObject * ___trackable0, PropAbstractBehaviour_t1293468098 * ___behaviour1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.SurfaceAbstractBehaviour Vuforia.ReconstructionAbstractBehaviour::InstantiateSurfaceBehaviour(Vuforia.SurfaceAbstractBehaviour)
extern "C"  SurfaceAbstractBehaviour_t142368528 * ReconstructionAbstractBehaviour_InstantiateSurfaceBehaviour_m3740619293 (Il2CppObject * __this /* static, unused */, SurfaceAbstractBehaviour_t142368528 * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::AssociateSurfaceBehaviour(Vuforia.Surface,Vuforia.SurfaceAbstractBehaviour)
extern "C"  void ReconstructionAbstractBehaviour_AssociateSurfaceBehaviour_m360134699 (ReconstructionAbstractBehaviour_t1860057025 * __this, Il2CppObject * ___trackable0, SurfaceAbstractBehaviour_t142368528 * ___behaviour1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.SmartTerrainTrackable Vuforia.ReconstructionAbstractBehaviour::FindSmartTerrainTrackable(System.Int32)
extern "C"  Il2CppObject * ReconstructionAbstractBehaviour_FindSmartTerrainTrackable_m2673610041 (ReconstructionAbstractBehaviour_t1860057025 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::NotifySurfaceEventHandlers(System.Collections.Generic.IEnumerable`1<Vuforia.Surface>,System.Collections.Generic.IEnumerable`1<Vuforia.Surface>,System.Collections.Generic.IEnumerable`1<Vuforia.Surface>)
extern "C"  void ReconstructionAbstractBehaviour_NotifySurfaceEventHandlers_m1030891609 (ReconstructionAbstractBehaviour_t1860057025 * __this, Il2CppObject* ___newSurfaces0, Il2CppObject* ___updatedSurfaces1, Il2CppObject* ___deletedSurfaces2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::NotifyPropEventHandlers(System.Collections.Generic.IEnumerable`1<Vuforia.Prop>,System.Collections.Generic.IEnumerable`1<Vuforia.Prop>,System.Collections.Generic.IEnumerable`1<Vuforia.Prop>)
extern "C"  void ReconstructionAbstractBehaviour_NotifyPropEventHandlers_m2187973555 (ReconstructionAbstractBehaviour_t1860057025 * __this, Il2CppObject* ___newProps0, Il2CppObject* ___updatedProps1, Il2CppObject* ___deletedProps2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh Vuforia.ReconstructionAbstractBehaviour::UpdateMesh(Vuforia.VuforiaManagerImpl/MeshData,UnityEngine.Mesh,System.Boolean)
extern "C"  Mesh_t4241756145 * ReconstructionAbstractBehaviour_UpdateMesh_m4187858841 (Il2CppObject * __this /* static, unused */, MeshData_t1047832367  ___meshData0, Mesh_t4241756145 * ___oldMesh1, bool ___setNormalsUpwards2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] Vuforia.ReconstructionAbstractBehaviour::ReadMeshBoundaries(System.Int32,System.IntPtr)
extern "C"  Int32U5BU5D_t3230847821* ReconstructionAbstractBehaviour_ReadMeshBoundaries_m3740454630 (Il2CppObject * __this /* static, unused */, int32_t ___numBoundaries0, IntPtr_t ___boundaryArray1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UnregisterDeletedProps(System.Collections.Generic.List`1<Vuforia.Prop>)
extern "C"  void ReconstructionAbstractBehaviour_UnregisterDeletedProps_m1493024014 (ReconstructionAbstractBehaviour_t1860057025 * __this, List_1_t3533494709 * ___deletedProps0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UnregisterDeletedSurfaces(System.Collections.Generic.List`1<Vuforia.Surface>)
extern "C"  void ReconstructionAbstractBehaviour_UnregisterDeletedSurfaces_m1414197066 (ReconstructionAbstractBehaviour_t1860057025 * __this, List_1_t167607975 * ___deletedSurfaces0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UpdateSurfaces(Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData[],Vuforia.VuforiaManagerImpl/SurfaceData[])
extern "C"  void ReconstructionAbstractBehaviour_UpdateSurfaces_m565213325 (ReconstructionAbstractBehaviour_t1860057025 * __this, SmartTerrainRevisionDataU5BU5D_t525370068* ___smartTerrainRevisions0, SurfaceDataU5BU5D_t1400262182* ___updatedSurfaceData1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UpdateProps(Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData[],Vuforia.VuforiaManagerImpl/PropData[])
extern "C"  void ReconstructionAbstractBehaviour_UpdateProps_m1845822147 (ReconstructionAbstractBehaviour_t1860057025 * __this, SmartTerrainRevisionDataU5BU5D_t525370068* ___smartTerrainRevisions0, PropDataU5BU5D_t921971240* ___updatedPropData1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.get_InitializedInEditor()
extern "C"  bool ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_InitializedInEditor_m3330362354 (ReconstructionAbstractBehaviour_t1860057025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.SetInitializedInEditor(System.Boolean)
extern "C"  void ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetInitializedInEditor_m739613564 (ReconstructionAbstractBehaviour_t1860057025 * __this, bool ___initializedInEditor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.SetMaximumExtentEnabled(System.Boolean)
extern "C"  void ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetMaximumExtentEnabled_m3750699025 (ReconstructionAbstractBehaviour_t1860057025 * __this, bool ___maxExtendEnabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.get_MaximumExtentEnabled()
extern "C"  bool ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_MaximumExtentEnabled_m2676300941 (ReconstructionAbstractBehaviour_t1860057025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.SetMaximumExtent(UnityEngine.Rect)
extern "C"  void ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetMaximumExtent_m963697550 (ReconstructionAbstractBehaviour_t1860057025 * __this, Rect_t4241904616  ___rectangle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.get_MaximumExtent()
extern "C"  Rect_t4241904616  ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_MaximumExtent_m3885258280 (ReconstructionAbstractBehaviour_t1860057025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.SetAutomaticStart(System.Boolean)
extern "C"  void ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetAutomaticStart_m1371367441 (ReconstructionAbstractBehaviour_t1860057025 * __this, bool ___autoStart0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.get_AutomaticStart()
extern "C"  bool ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_AutomaticStart_m637040333 (ReconstructionAbstractBehaviour_t1860057025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.SetNavMeshUpdates(System.Boolean)
extern "C"  void ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetNavMeshUpdates_m2001536532 (ReconstructionAbstractBehaviour_t1860057025 * __this, bool ___navMeshUpdates0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.get_NavMeshUpdates()
extern "C"  bool ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_NavMeshUpdates_m2275509776 (ReconstructionAbstractBehaviour_t1860057025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.SetNavMeshPadding(System.Single)
extern "C"  void ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetNavMeshPadding_m1218148167 (ReconstructionAbstractBehaviour_t1860057025 * __this, float ___navMeshPadding0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.get_NavMeshPadding()
extern "C"  float ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_NavMeshPadding_m2655442231 (ReconstructionAbstractBehaviour_t1860057025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.ScaleEditorMeshesByFactor(System.Single)
extern "C"  void ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_ScaleEditorMeshesByFactor_m3187859378 (ReconstructionAbstractBehaviour_t1860057025 * __this, float ___scaleFactor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.ScaleEditorPropPositionsByFactor(System.Single)
extern "C"  void ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_ScaleEditorPropPositionsByFactor_m883409940 (ReconstructionAbstractBehaviour_t1860057025 * __this, float ___scaleFactor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::.ctor()
extern "C"  void ReconstructionAbstractBehaviour__ctor_m2258723876 (ReconstructionAbstractBehaviour_t1860057025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
