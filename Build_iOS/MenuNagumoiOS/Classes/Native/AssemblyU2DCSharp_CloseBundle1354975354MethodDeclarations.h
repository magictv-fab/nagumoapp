﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CloseBundle
struct CloseBundle_t1354975354;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void CloseBundle::.ctor()
extern "C"  void CloseBundle__ctor_m1083776049 (CloseBundle_t1354975354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CloseBundle::Close()
extern "C"  void CloseBundle_Close_m2794635591 (CloseBundle_t1354975354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CloseBundle::Dest()
extern "C"  Il2CppObject * CloseBundle_Dest_m3664818637 (CloseBundle_t1354975354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
