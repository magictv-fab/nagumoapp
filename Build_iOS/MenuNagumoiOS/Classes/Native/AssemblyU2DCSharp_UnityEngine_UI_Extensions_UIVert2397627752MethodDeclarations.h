﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Extensions.UIVerticalScroller
struct UIVerticalScroller_t2397627752;
// UnityEngine.RectTransform
struct RectTransform_t972643934;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2662109048;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RectTransform972643934.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void UnityEngine.UI.Extensions.UIVerticalScroller::.ctor()
extern "C"  void UIVerticalScroller__ctor_m274604352 (UIVerticalScroller_t2397627752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.UIVerticalScroller::.ctor(UnityEngine.RectTransform,UnityEngine.GameObject[],UnityEngine.RectTransform)
extern "C"  void UIVerticalScroller__ctor_m3321795062 (UIVerticalScroller_t2397627752 * __this, RectTransform_t972643934 * ___scrollingPanel0, GameObjectU5BU5D_t2662109048* ___arrayOfElements1, RectTransform_t972643934 * ___center2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.UIVerticalScroller::Awake()
extern "C"  void UIVerticalScroller_Awake_m512209571 (UIVerticalScroller_t2397627752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.UIVerticalScroller::Start()
extern "C"  void UIVerticalScroller_Start_m3516709440 (UIVerticalScroller_t2397627752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.UIVerticalScroller::AddListener(UnityEngine.GameObject,System.Int32)
extern "C"  void UIVerticalScroller_AddListener_m3773674412 (UIVerticalScroller_t2397627752 * __this, GameObject_t3674682005 * ___button0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.UIVerticalScroller::DoSomething(System.Int32)
extern "C"  void UIVerticalScroller_DoSomething_m3195604702 (UIVerticalScroller_t2397627752 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.UIVerticalScroller::Update()
extern "C"  void UIVerticalScroller_Update_m1649662413 (UIVerticalScroller_t2397627752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.UIVerticalScroller::ScrollingElements(System.Single)
extern "C"  void UIVerticalScroller_ScrollingElements_m2819120129 (UIVerticalScroller_t2397627752 * __this, float ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UI.Extensions.UIVerticalScroller::GetResults()
extern "C"  String_t* UIVerticalScroller_GetResults_m3287782887 (UIVerticalScroller_t2397627752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.UIVerticalScroller::SnapToElement(System.Int32)
extern "C"  void UIVerticalScroller_SnapToElement_m883431622 (UIVerticalScroller_t2397627752 * __this, int32_t ___element0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.UIVerticalScroller::ScrollUp()
extern "C"  void UIVerticalScroller_ScrollUp_m846277324 (UIVerticalScroller_t2397627752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.UIVerticalScroller::ScrollDown()
extern "C"  void UIVerticalScroller_ScrollDown_m1038491411 (UIVerticalScroller_t2397627752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.UIVerticalScroller::<Start>m__8E()
extern "C"  void UIVerticalScroller_U3CStartU3Em__8E_m1839678662 (UIVerticalScroller_t2397627752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.UIVerticalScroller::<Start>m__8F()
extern "C"  void UIVerticalScroller_U3CStartU3Em__8F_m1839679623 (UIVerticalScroller_t2397627752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
