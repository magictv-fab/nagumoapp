﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// OneSignal/IdsAvailableCallback
struct IdsAvailableCallback_t3679464791;
// OneSignal/OnSetEmailSuccess
struct OnSetEmailSuccess_t2952831753;
// OneSignal/OnSetEmailFailure
struct OnSetEmailFailure_t3733427344;
// OneSignal/OnLogoutEmailSuccess
struct OnLogoutEmailSuccess_t3366364849;
// OneSignal/OnLogoutEmailFailure
struct OnLogoutEmailFailure_t4146960440;
// OneSignal/OnPostNotificationSuccess
struct OnPostNotificationSuccess_t2053930136;
// OneSignal/OnPostNotificationFailure
struct OnPostNotificationFailure_t2834525727;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameControllerExample
struct  GameControllerExample_t2194515004  : public MonoBehaviour_t667441552
{
public:
	// System.String GameControllerExample::email
	String_t* ___email_3;

public:
	inline static int32_t get_offset_of_email_3() { return static_cast<int32_t>(offsetof(GameControllerExample_t2194515004, ___email_3)); }
	inline String_t* get_email_3() const { return ___email_3; }
	inline String_t** get_address_of_email_3() { return &___email_3; }
	inline void set_email_3(String_t* value)
	{
		___email_3 = value;
		Il2CppCodeGenWriteBarrier(&___email_3, value);
	}
};

struct GameControllerExample_t2194515004_StaticFields
{
public:
	// System.String GameControllerExample::extraMessage
	String_t* ___extraMessage_2;
	// System.Boolean GameControllerExample::requiresUserPrivacyConsent
	bool ___requiresUserPrivacyConsent_4;
	// OneSignal/IdsAvailableCallback GameControllerExample::<>f__am$cache3
	IdsAvailableCallback_t3679464791 * ___U3CU3Ef__amU24cache3_5;
	// OneSignal/IdsAvailableCallback GameControllerExample::<>f__am$cache4
	IdsAvailableCallback_t3679464791 * ___U3CU3Ef__amU24cache4_6;
	// OneSignal/OnSetEmailSuccess GameControllerExample::<>f__am$cache5
	OnSetEmailSuccess_t2952831753 * ___U3CU3Ef__amU24cache5_7;
	// OneSignal/OnSetEmailFailure GameControllerExample::<>f__am$cache6
	OnSetEmailFailure_t3733427344 * ___U3CU3Ef__amU24cache6_8;
	// OneSignal/OnLogoutEmailSuccess GameControllerExample::<>f__am$cache7
	OnLogoutEmailSuccess_t3366364849 * ___U3CU3Ef__amU24cache7_9;
	// OneSignal/OnLogoutEmailFailure GameControllerExample::<>f__am$cache8
	OnLogoutEmailFailure_t4146960440 * ___U3CU3Ef__amU24cache8_10;
	// OneSignal/OnPostNotificationSuccess GameControllerExample::<>f__am$cache9
	OnPostNotificationSuccess_t2053930136 * ___U3CU3Ef__amU24cache9_11;
	// OneSignal/OnPostNotificationFailure GameControllerExample::<>f__am$cacheA
	OnPostNotificationFailure_t2834525727 * ___U3CU3Ef__amU24cacheA_12;

public:
	inline static int32_t get_offset_of_extraMessage_2() { return static_cast<int32_t>(offsetof(GameControllerExample_t2194515004_StaticFields, ___extraMessage_2)); }
	inline String_t* get_extraMessage_2() const { return ___extraMessage_2; }
	inline String_t** get_address_of_extraMessage_2() { return &___extraMessage_2; }
	inline void set_extraMessage_2(String_t* value)
	{
		___extraMessage_2 = value;
		Il2CppCodeGenWriteBarrier(&___extraMessage_2, value);
	}

	inline static int32_t get_offset_of_requiresUserPrivacyConsent_4() { return static_cast<int32_t>(offsetof(GameControllerExample_t2194515004_StaticFields, ___requiresUserPrivacyConsent_4)); }
	inline bool get_requiresUserPrivacyConsent_4() const { return ___requiresUserPrivacyConsent_4; }
	inline bool* get_address_of_requiresUserPrivacyConsent_4() { return &___requiresUserPrivacyConsent_4; }
	inline void set_requiresUserPrivacyConsent_4(bool value)
	{
		___requiresUserPrivacyConsent_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_5() { return static_cast<int32_t>(offsetof(GameControllerExample_t2194515004_StaticFields, ___U3CU3Ef__amU24cache3_5)); }
	inline IdsAvailableCallback_t3679464791 * get_U3CU3Ef__amU24cache3_5() const { return ___U3CU3Ef__amU24cache3_5; }
	inline IdsAvailableCallback_t3679464791 ** get_address_of_U3CU3Ef__amU24cache3_5() { return &___U3CU3Ef__amU24cache3_5; }
	inline void set_U3CU3Ef__amU24cache3_5(IdsAvailableCallback_t3679464791 * value)
	{
		___U3CU3Ef__amU24cache3_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_6() { return static_cast<int32_t>(offsetof(GameControllerExample_t2194515004_StaticFields, ___U3CU3Ef__amU24cache4_6)); }
	inline IdsAvailableCallback_t3679464791 * get_U3CU3Ef__amU24cache4_6() const { return ___U3CU3Ef__amU24cache4_6; }
	inline IdsAvailableCallback_t3679464791 ** get_address_of_U3CU3Ef__amU24cache4_6() { return &___U3CU3Ef__amU24cache4_6; }
	inline void set_U3CU3Ef__amU24cache4_6(IdsAvailableCallback_t3679464791 * value)
	{
		___U3CU3Ef__amU24cache4_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_7() { return static_cast<int32_t>(offsetof(GameControllerExample_t2194515004_StaticFields, ___U3CU3Ef__amU24cache5_7)); }
	inline OnSetEmailSuccess_t2952831753 * get_U3CU3Ef__amU24cache5_7() const { return ___U3CU3Ef__amU24cache5_7; }
	inline OnSetEmailSuccess_t2952831753 ** get_address_of_U3CU3Ef__amU24cache5_7() { return &___U3CU3Ef__amU24cache5_7; }
	inline void set_U3CU3Ef__amU24cache5_7(OnSetEmailSuccess_t2952831753 * value)
	{
		___U3CU3Ef__amU24cache5_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_8() { return static_cast<int32_t>(offsetof(GameControllerExample_t2194515004_StaticFields, ___U3CU3Ef__amU24cache6_8)); }
	inline OnSetEmailFailure_t3733427344 * get_U3CU3Ef__amU24cache6_8() const { return ___U3CU3Ef__amU24cache6_8; }
	inline OnSetEmailFailure_t3733427344 ** get_address_of_U3CU3Ef__amU24cache6_8() { return &___U3CU3Ef__amU24cache6_8; }
	inline void set_U3CU3Ef__amU24cache6_8(OnSetEmailFailure_t3733427344 * value)
	{
		___U3CU3Ef__amU24cache6_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_9() { return static_cast<int32_t>(offsetof(GameControllerExample_t2194515004_StaticFields, ___U3CU3Ef__amU24cache7_9)); }
	inline OnLogoutEmailSuccess_t3366364849 * get_U3CU3Ef__amU24cache7_9() const { return ___U3CU3Ef__amU24cache7_9; }
	inline OnLogoutEmailSuccess_t3366364849 ** get_address_of_U3CU3Ef__amU24cache7_9() { return &___U3CU3Ef__amU24cache7_9; }
	inline void set_U3CU3Ef__amU24cache7_9(OnLogoutEmailSuccess_t3366364849 * value)
	{
		___U3CU3Ef__amU24cache7_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_10() { return static_cast<int32_t>(offsetof(GameControllerExample_t2194515004_StaticFields, ___U3CU3Ef__amU24cache8_10)); }
	inline OnLogoutEmailFailure_t4146960440 * get_U3CU3Ef__amU24cache8_10() const { return ___U3CU3Ef__amU24cache8_10; }
	inline OnLogoutEmailFailure_t4146960440 ** get_address_of_U3CU3Ef__amU24cache8_10() { return &___U3CU3Ef__amU24cache8_10; }
	inline void set_U3CU3Ef__amU24cache8_10(OnLogoutEmailFailure_t4146960440 * value)
	{
		___U3CU3Ef__amU24cache8_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_11() { return static_cast<int32_t>(offsetof(GameControllerExample_t2194515004_StaticFields, ___U3CU3Ef__amU24cache9_11)); }
	inline OnPostNotificationSuccess_t2053930136 * get_U3CU3Ef__amU24cache9_11() const { return ___U3CU3Ef__amU24cache9_11; }
	inline OnPostNotificationSuccess_t2053930136 ** get_address_of_U3CU3Ef__amU24cache9_11() { return &___U3CU3Ef__amU24cache9_11; }
	inline void set_U3CU3Ef__amU24cache9_11(OnPostNotificationSuccess_t2053930136 * value)
	{
		___U3CU3Ef__amU24cache9_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_12() { return static_cast<int32_t>(offsetof(GameControllerExample_t2194515004_StaticFields, ___U3CU3Ef__amU24cacheA_12)); }
	inline OnPostNotificationFailure_t2834525727 * get_U3CU3Ef__amU24cacheA_12() const { return ___U3CU3Ef__amU24cacheA_12; }
	inline OnPostNotificationFailure_t2834525727 ** get_address_of_U3CU3Ef__amU24cacheA_12() { return &___U3CU3Ef__amU24cacheA_12; }
	inline void set_U3CU3Ef__amU24cacheA_12(OnPostNotificationFailure_t2834525727 * value)
	{
		___U3CU3Ef__amU24cacheA_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheA_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
