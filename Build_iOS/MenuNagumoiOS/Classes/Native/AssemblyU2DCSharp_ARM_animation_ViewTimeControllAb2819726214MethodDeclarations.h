﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.animation.ViewTimeControllAbstract/OnReadyEventHandlerandler
struct OnReadyEventHandlerandler_t2819726214;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ARM.animation.ViewTimeControllAbstract/OnReadyEventHandlerandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnReadyEventHandlerandler__ctor_m722734381 (OnReadyEventHandlerandler_t2819726214 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.animation.ViewTimeControllAbstract/OnReadyEventHandlerandler::Invoke()
extern "C"  void OnReadyEventHandlerandler_Invoke_m1735940039 (OnReadyEventHandlerandler_t2819726214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ARM.animation.ViewTimeControllAbstract/OnReadyEventHandlerandler::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnReadyEventHandlerandler_BeginInvoke_m3156728668 (OnReadyEventHandlerandler_t2819726214 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.animation.ViewTimeControllAbstract/OnReadyEventHandlerandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnReadyEventHandlerandler_EndInvoke_m3316587965 (OnReadyEventHandlerandler_t2819726214 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
