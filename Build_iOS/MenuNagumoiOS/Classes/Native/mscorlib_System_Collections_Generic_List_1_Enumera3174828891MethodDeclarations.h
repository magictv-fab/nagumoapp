﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<ZXing.QrCode.Internal.AlignmentPattern>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m775231483(__this, ___l0, method) ((  void (*) (Enumerator_t3174828891 *, List_1_t3155156121 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ZXing.QrCode.Internal.AlignmentPattern>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2401479543(__this, method) ((  void (*) (Enumerator_t3174828891 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<ZXing.QrCode.Internal.AlignmentPattern>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3949472621(__this, method) ((  Il2CppObject * (*) (Enumerator_t3174828891 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ZXing.QrCode.Internal.AlignmentPattern>::Dispose()
#define Enumerator_Dispose_m1925125280(__this, method) ((  void (*) (Enumerator_t3174828891 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ZXing.QrCode.Internal.AlignmentPattern>::VerifyState()
#define Enumerator_VerifyState_m2266465241(__this, method) ((  void (*) (Enumerator_t3174828891 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<ZXing.QrCode.Internal.AlignmentPattern>::MoveNext()
#define Enumerator_MoveNext_m3555024935(__this, method) ((  bool (*) (Enumerator_t3174828891 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<ZXing.QrCode.Internal.AlignmentPattern>::get_Current()
#define Enumerator_get_Current_m1762810034(__this, method) ((  AlignmentPattern_t1786970569 * (*) (Enumerator_t3174828891 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
