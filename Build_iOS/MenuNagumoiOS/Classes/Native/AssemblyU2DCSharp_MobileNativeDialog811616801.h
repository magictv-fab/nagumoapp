﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<MNDialogResult>
struct Action_1_t3521133710;

#include "AssemblyU2DCSharp_UnionAssets_FLE_EventDispatcherB1248907224.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MobileNativeDialog
struct  MobileNativeDialog_t811616801  : public EventDispatcherBase_t1248907224
{
public:
	// System.Action`1<MNDialogResult> MobileNativeDialog::OnComplete
	Action_1_t3521133710 * ___OnComplete_2;

public:
	inline static int32_t get_offset_of_OnComplete_2() { return static_cast<int32_t>(offsetof(MobileNativeDialog_t811616801, ___OnComplete_2)); }
	inline Action_1_t3521133710 * get_OnComplete_2() const { return ___OnComplete_2; }
	inline Action_1_t3521133710 ** get_address_of_OnComplete_2() { return &___OnComplete_2; }
	inline void set_OnComplete_2(Action_1_t3521133710 * value)
	{
		___OnComplete_2 = value;
		Il2CppCodeGenWriteBarrier(&___OnComplete_2, value);
	}
};

struct MobileNativeDialog_t811616801_StaticFields
{
public:
	// System.Action`1<MNDialogResult> MobileNativeDialog::<>f__am$cache1
	Action_1_t3521133710 * ___U3CU3Ef__amU24cache1_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_3() { return static_cast<int32_t>(offsetof(MobileNativeDialog_t811616801_StaticFields, ___U3CU3Ef__amU24cache1_3)); }
	inline Action_1_t3521133710 * get_U3CU3Ef__amU24cache1_3() const { return ___U3CU3Ef__amU24cache1_3; }
	inline Action_1_t3521133710 ** get_address_of_U3CU3Ef__amU24cache1_3() { return &___U3CU3Ef__amU24cache1_3; }
	inline void set_U3CU3Ef__amU24cache1_3(Action_1_t3521133710 * value)
	{
		___U3CU3Ef__amU24cache1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
