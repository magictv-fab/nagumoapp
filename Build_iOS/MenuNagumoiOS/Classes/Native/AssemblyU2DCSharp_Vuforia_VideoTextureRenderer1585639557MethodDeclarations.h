﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.VideoTextureRenderer
struct VideoTextureRenderer_t1585639557;

#include "codegen/il2cpp-codegen.h"

// System.Void Vuforia.VideoTextureRenderer::.ctor()
extern "C"  void VideoTextureRenderer__ctor_m3307215884 (VideoTextureRenderer_t1585639557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
