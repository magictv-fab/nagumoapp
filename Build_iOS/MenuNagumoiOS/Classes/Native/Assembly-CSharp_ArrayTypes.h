﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// OfertasValuesCRM
struct OfertasValuesCRM_t2856949562;
// OfertaCRMData
struct OfertaCRMData_t637956887;
// OfertasCRMDataGroup
struct OfertasCRMDataGroup_t273423897;
// CortesData
struct CortesData_t642513606;
// OfertaData
struct OfertaData_t3417615771;
// Area730.Notifications.NotificationInstance
struct NotificationInstance_t3873938600;
// ISampleAppUIElement
struct ISampleAppUIElement_t2180050874;
// SampleAppUIRect
struct SampleAppUIRect_t2784570703;
// UnionAssets.FLE.EventHandlerFunction
struct EventHandlerFunction_t2672185540;
// UnionAssets.FLE.DataEventHandlerFunction
struct DataEventHandlerFunction_t438967822;
// Facebook.Unity.FacebookSettings/UrlSchemes
struct UrlSchemes_t3482710948;
// ICSharpCode.SharpZipLib.Tar.TarEntry
struct TarEntry_t762185187;
// ICSharpCode.SharpZipLib.Zip.ZipEntry
struct ZipEntry_t3141689087;
// ARM.abstracts.components.GeneralComponentsAbstract
struct GeneralComponentsAbstract_t3900398046;
// ARM.interfaces.GeneralComponentInterface
struct GeneralComponentInterface_t2984273286;
// MagicTV.abstracts.InAppAbstract
struct InAppAbstract_t382673128;
// ARM.abstracts.ProgressEventsAbstract
struct ProgressEventsAbstract_t2129719228;
// ARM.animation.GenericStartableComponentControllAbstract
struct GenericStartableComponentControllAbstract_t1794600275;
// ARM.animation.GenericTimeControllInterface
struct GenericTimeControllInterface_t4025522376;
// ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract
struct TransformPositionAngleFilterAbstract_t4158746314;
// ARM.transform.filters.interfaces.TransformPositionAngleFilterInterface
struct TransformPositionAngleFilterInterface_t2619784369;
// OnOffListenerEventInterface
struct OnOffListenerEventInterface_t3960143011;
// DataRow
struct DataRow_t3107836336;
// MagicTV.vo.BundleVO
struct BundleVO_t1984518073;
// MagicTV.vo.VOWithId
struct VOWithId_t2461972856;
// MagicTV.vo.MetadataVO
struct MetadataVO_t2511256998;
// MagicTV.vo.FileVO
struct FileVO_t1935348659;
// MagicTV.vo.UrlInfoVO
struct UrlInfoVO_t1761987528;
// MagicTV.vo.KeyValueVO
struct KeyValueVO_t1780100105;
// ConfigurableListenerEventInterface
struct ConfigurableListenerEventInterface_t1985629260;
// VideoPlaybackBehaviour
struct VideoPlaybackBehaviour_t2848361927;
// MagicTV.download.BundleDownloadItem
struct BundleDownloadItem_t715938369;
// MagicTV.vo.RequestImagesItemVO
struct RequestImagesItemVO_t2061191909;
// MagicTV.vo.BundleVO[]
struct BundleVOU5BU5D_t2162892100;
// MagicTV.globals.InAppAnalytics
struct InAppAnalytics_t2316734034;
// MagicTV.in_apps.InAppEventsChannel
struct InAppEventsChannel_t1397713486;
// MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler
struct OnChangeAnToPerspectiveEventHandler_t2702236419;
// MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler
struct OnChangeToAnStateEventHandler_t630243546;
// MagicTV.in_apps.GarbageItemVO
struct GarbageItemVO_t1335202303;
// InApp.InAppEventDispatcher/InAppEventDispatcherOnOff
struct InAppEventDispatcherOnOff_t3474551315;
// ARM.events.VoidEventDispatcher
struct VoidEventDispatcher_t1760461203;
// ARM.events.GenericParameterEventDispatcher
struct GenericParameterEventDispatcher_t3811317845;
// ARM.components.GroupInAppAbstractComponentsManager
struct GroupInAppAbstractComponentsManager_t739334794;
// LitJson.JsonData
struct JsonData_t1715015430;
// LitJson.ExporterFunc
struct ExporterFunc_t3330360473;
// LitJson.ImporterFunc
struct ImporterFunc_t2138319818;
// LitJson.WriterContext
struct WriterContext_t3060158226;
// LitJson.Lexer/StateHandler
struct StateHandler_t3261942315;
// Vuforia.ImageTargetBehaviour
struct ImageTargetBehaviour_t1735871187;
// MagicTV.vo.KeyTypeValue
struct KeyTypeValue_t654867638;
// Loom/DelayedQueueItem
struct DelayedQueueItem_t3219765808;
// PedidoData
struct PedidoData_t3443010735;
// CarnesData
struct CarnesData_t3441984818;
// CuponData
struct CuponData_t807724487;
// PublicationData
struct PublicationData_t473548758;
// PedidoEnvioData
struct PedidoEnvioData_t1309077304;
// OfertaValues
struct OfertaValues_t3488850643;
// CortesData[]
struct CortesDataU5BU5D_t1695339363;
// TabloideData
struct TabloideData_t1867721404;
// UniWebViewNativeListener
struct UniWebViewNativeListener_t795571060;
// Vuforia.WireframeBehaviour
struct WireframeBehaviour_t433318935;
// iTween
struct iTween_t3087282050;

#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_OfertasValuesCRM2856949562.h"
#include "AssemblyU2DCSharp_OfertaCRMData637956887.h"
#include "AssemblyU2DCSharp_OfertasCRMDataGroup273423897.h"
#include "AssemblyU2DCSharp_CortesData642513606.h"
#include "AssemblyU2DCSharp_OfertaData3417615771.h"
#include "AssemblyU2DCSharp_Area730_Notifications_Notificati3873938600.h"
#include "AssemblyU2DCSharp_ISampleAppUIElement2180050874.h"
#include "AssemblyU2DCSharp_SampleAppUIRect2784570703.h"
#include "AssemblyU2DCSharp_UnionAssets_FLE_EventHandlerFunc2672185540.h"
#include "AssemblyU2DCSharp_UnionAssets_FLE_DataEventHandlerF438967822.h"
#include "AssemblyU2DCSharp_Facebook_Unity_FacebookSettings_3482710948.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_BZip2_BZ2425423099.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Tar_TarEn762185187.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipE3141689087.h"
#include "AssemblyU2DCSharp_ARM_abstracts_components_General3900398046.h"
#include "AssemblyU2DCSharp_MagicTV_abstracts_InAppAbstract382673128.h"
#include "AssemblyU2DCSharp_ARM_abstracts_ProgressEventsAbst2129719228.h"
#include "AssemblyU2DCSharp_ARM_animation_GenericStartableCo1794600275.h"
#include "AssemblyU2DCSharp_ARM_transform_filters_abstracts_4158746314.h"
#include "AssemblyU2DCSharp_DataRow3107836336.h"
#include "AssemblyU2DCSharp_MagicTV_vo_BundleVO1984518073.h"
#include "AssemblyU2DCSharp_MagicTV_vo_VOWithId2461972856.h"
#include "AssemblyU2DCSharp_MagicTV_vo_MetadataVO2511256998.h"
#include "AssemblyU2DCSharp_MagicTV_vo_FileVO1935348659.h"
#include "AssemblyU2DCSharp_MagicTV_vo_UrlInfoVO1761987528.h"
#include "AssemblyU2DCSharp_MagicTV_vo_KeyValueVO1780100105.h"
#include "AssemblyU2DCSharp_VideoPlaybackBehaviour2848361927.h"
#include "AssemblyU2DCSharp_MagicTV_download_BundleDownloadIt715938369.h"
#include "AssemblyU2DCSharp_MagicTV_vo_RequestImagesItemVO2061191909.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "AssemblyU2DCSharp_MagicTV_globals_InAppAnalytics2316734034.h"
#include "AssemblyU2DCSharp_MagicTV_in_apps_InAppEventsChann1397713486.h"
#include "AssemblyU2DCSharp_MagicTV_globals_Perspective644553802.h"
#include "AssemblyU2DCSharp_MagicTV_globals_events_ScreenEve2702236419.h"
#include "AssemblyU2DCSharp_MagicTV_globals_StateMachine367995870.h"
#include "AssemblyU2DCSharp_MagicTV_globals_events_StateMagin630243546.h"
#include "AssemblyU2DCSharp_MagicTV_in_apps_GarbageItemVO1335202303.h"
#include "AssemblyU2DCSharp_InApp_InAppEventDispatcher_Event1614635846.h"
#include "AssemblyU2DCSharp_InApp_InAppEventDispatcher_InApp3474551315.h"
#include "AssemblyU2DCSharp_ARM_events_VoidEventDispatcher1760461203.h"
#include "AssemblyU2DCSharp_ARM_events_GenericParameterEvent3811317845.h"
#include "AssemblyU2DCSharp_ARM_components_GroupInAppAbstract739334794.h"
#include "AssemblyU2DCSharp_LitJson_JsonData1715015430.h"
#include "AssemblyU2DCSharp_LitJson_PropertyMetadata4066634616.h"
#include "AssemblyU2DCSharp_LitJson_ArrayMetadata4058342910.h"
#include "AssemblyU2DCSharp_LitJson_ObjectMetadata2009294498.h"
#include "AssemblyU2DCSharp_LitJson_ExporterFunc3330360473.h"
#include "AssemblyU2DCSharp_LitJson_ImporterFunc2138319818.h"
#include "AssemblyU2DCSharp_LitJson_WriterContext3060158226.h"
#include "AssemblyU2DCSharp_LitJson_Lexer_StateHandler3261942315.h"
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviour1735871187.h"
#include "AssemblyU2DCSharp_MagicTV_vo_KeyTypeValue654867638.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_An2191327052.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Cu2771812670.h"
#include "AssemblyU2DCSharp_Loom_DelayedQueueItem3219765808.h"
#include "AssemblyU2DCSharp_PedidoData3443010735.h"
#include "AssemblyU2DCSharp_CarnesData3441984818.h"
#include "AssemblyU2DCSharp_CuponData807724487.h"
#include "AssemblyU2DCSharp_PublicationData473548758.h"
#include "AssemblyU2DCSharp_PedidoEnvioData1309077304.h"
#include "AssemblyU2DCSharp_OfertaValues3488850643.h"
#include "AssemblyU2DCSharp_TabloideData1867721404.h"
#include "AssemblyU2DCSharp_UniWebViewNativeListener795571060.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviour433318935.h"
#include "AssemblyU2DCSharp_iTween3087282050.h"

#pragma once
// OfertasValuesCRM[]
struct OfertasValuesCRMU5BU5D_t1162547871  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) OfertasValuesCRM_t2856949562 * m_Items[1];

public:
	inline OfertasValuesCRM_t2856949562 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline OfertasValuesCRM_t2856949562 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, OfertasValuesCRM_t2856949562 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// OfertaCRMData[]
struct OfertaCRMDataU5BU5D_t2228159150  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) OfertaCRMData_t637956887 * m_Items[1];

public:
	inline OfertaCRMData_t637956887 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline OfertaCRMData_t637956887 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, OfertaCRMData_t637956887 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// OfertasCRMDataGroup[]
struct OfertasCRMDataGroupU5BU5D_t1016303972  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) OfertasCRMDataGroup_t273423897 * m_Items[1];

public:
	inline OfertasCRMDataGroup_t273423897 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline OfertasCRMDataGroup_t273423897 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, OfertasCRMDataGroup_t273423897 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CortesData[]
struct CortesDataU5BU5D_t1695339363  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CortesData_t642513606 * m_Items[1];

public:
	inline CortesData_t642513606 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CortesData_t642513606 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CortesData_t642513606 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// OfertaData[]
struct OfertaDataU5BU5D_t382376346  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) OfertaData_t3417615771 * m_Items[1];

public:
	inline OfertaData_t3417615771 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline OfertaData_t3417615771 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, OfertaData_t3417615771 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Area730.Notifications.NotificationInstance[]
struct NotificationInstanceU5BU5D_t1968526265  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) NotificationInstance_t3873938600 * m_Items[1];

public:
	inline NotificationInstance_t3873938600 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline NotificationInstance_t3873938600 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, NotificationInstance_t3873938600 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ISampleAppUIElement[]
struct ISampleAppUIElementU5BU5D_t343197727  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ISampleAppUIElement_t2180050874 * m_Items[1];

public:
	inline ISampleAppUIElement_t2180050874 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ISampleAppUIElement_t2180050874 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ISampleAppUIElement_t2180050874 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SampleAppUIRect[]
struct SampleAppUIRectU5BU5D_t2382086294  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SampleAppUIRect_t2784570703 * m_Items[1];

public:
	inline SampleAppUIRect_t2784570703 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SampleAppUIRect_t2784570703 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SampleAppUIRect_t2784570703 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnionAssets.FLE.EventHandlerFunction[]
struct EventHandlerFunctionU5BU5D_t262873005  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) EventHandlerFunction_t2672185540 * m_Items[1];

public:
	inline EventHandlerFunction_t2672185540 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline EventHandlerFunction_t2672185540 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, EventHandlerFunction_t2672185540 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnionAssets.FLE.DataEventHandlerFunction[]
struct DataEventHandlerFunctionU5BU5D_t3937964539  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DataEventHandlerFunction_t438967822 * m_Items[1];

public:
	inline DataEventHandlerFunction_t438967822 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DataEventHandlerFunction_t438967822 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DataEventHandlerFunction_t438967822 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Facebook.Unity.FacebookSettings/UrlSchemes[]
struct UrlSchemesU5BU5D_t1469920589  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UrlSchemes_t3482710948 * m_Items[1];

public:
	inline UrlSchemes_t3482710948 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UrlSchemes_t3482710948 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UrlSchemes_t3482710948 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ICSharpCode.SharpZipLib.BZip2.BZip2OutputStream/StackElement[]
struct StackElementU5BU5D_t3260291258  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) StackElement_t2425423099  m_Items[1];

public:
	inline StackElement_t2425423099  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline StackElement_t2425423099 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, StackElement_t2425423099  value)
	{
		m_Items[index] = value;
	}
};
// ICSharpCode.SharpZipLib.Tar.TarEntry[]
struct TarEntryU5BU5D_t277262130  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TarEntry_t762185187 * m_Items[1];

public:
	inline TarEntry_t762185187 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TarEntry_t762185187 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TarEntry_t762185187 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ICSharpCode.SharpZipLib.Zip.ZipEntry[]
struct ZipEntryU5BU5D_t3672810022  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ZipEntry_t3141689087 * m_Items[1];

public:
	inline ZipEntry_t3141689087 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ZipEntry_t3141689087 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ZipEntry_t3141689087 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ARM.abstracts.components.GeneralComponentsAbstract[]
struct GeneralComponentsAbstractU5BU5D_t4214250731  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GeneralComponentsAbstract_t3900398046 * m_Items[1];

public:
	inline GeneralComponentsAbstract_t3900398046 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GeneralComponentsAbstract_t3900398046 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GeneralComponentsAbstract_t3900398046 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ARM.interfaces.GeneralComponentInterface[]
struct GeneralComponentInterfaceU5BU5D_t4161906083  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// MagicTV.abstracts.InAppAbstract[]
struct InAppAbstractU5BU5D_t412350073  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) InAppAbstract_t382673128 * m_Items[1];

public:
	inline InAppAbstract_t382673128 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline InAppAbstract_t382673128 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, InAppAbstract_t382673128 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ARM.abstracts.ProgressEventsAbstract[]
struct ProgressEventsAbstractU5BU5D_t3177844181  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ProgressEventsAbstract_t2129719228 * m_Items[1];

public:
	inline ProgressEventsAbstract_t2129719228 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ProgressEventsAbstract_t2129719228 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ProgressEventsAbstract_t2129719228 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ARM.animation.GenericStartableComponentControllAbstract[]
struct GenericStartableComponentControllAbstractU5BU5D_t3957787650  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GenericStartableComponentControllAbstract_t1794600275 * m_Items[1];

public:
	inline GenericStartableComponentControllAbstract_t1794600275 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GenericStartableComponentControllAbstract_t1794600275 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GenericStartableComponentControllAbstract_t1794600275 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ARM.animation.GenericTimeControllInterface[]
struct GenericTimeControllInterfaceU5BU5D_t2530296601  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ARM.transform.filters.abstracts.TransformPositionAngleFilterAbstract[]
struct TransformPositionAngleFilterAbstractU5BU5D_t4281965263  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TransformPositionAngleFilterAbstract_t4158746314 * m_Items[1];

public:
	inline TransformPositionAngleFilterAbstract_t4158746314 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TransformPositionAngleFilterAbstract_t4158746314 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TransformPositionAngleFilterAbstract_t4158746314 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ARM.transform.filters.interfaces.TransformPositionAngleFilterInterface[]
struct TransformPositionAngleFilterInterfaceU5BU5D_t3110432108  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// OnOffListenerEventInterface[]
struct OnOffListenerEventInterfaceU5BU5D_t1819493746  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// DataRow[]
struct DataRowU5BU5D_t2624831889  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DataRow_t3107836336 * m_Items[1];

public:
	inline DataRow_t3107836336 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DataRow_t3107836336 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DataRow_t3107836336 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// MagicTV.vo.BundleVO[]
struct BundleVOU5BU5D_t2162892100  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BundleVO_t1984518073 * m_Items[1];

public:
	inline BundleVO_t1984518073 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BundleVO_t1984518073 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BundleVO_t1984518073 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// MagicTV.vo.VOWithId[]
struct VOWithIdU5BU5D_t2042010025  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) VOWithId_t2461972856 * m_Items[1];

public:
	inline VOWithId_t2461972856 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline VOWithId_t2461972856 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, VOWithId_t2461972856 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// MagicTV.vo.MetadataVO[]
struct MetadataVOU5BU5D_t4238035203  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MetadataVO_t2511256998 * m_Items[1];

public:
	inline MetadataVO_t2511256998 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MetadataVO_t2511256998 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MetadataVO_t2511256998 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// MagicTV.vo.FileVO[]
struct FileVOU5BU5D_t573513762  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FileVO_t1935348659 * m_Items[1];

public:
	inline FileVO_t1935348659 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FileVO_t1935348659 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FileVO_t1935348659 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// MagicTV.vo.UrlInfoVO[]
struct UrlInfoVOU5BU5D_t1681515545  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UrlInfoVO_t1761987528 * m_Items[1];

public:
	inline UrlInfoVO_t1761987528 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UrlInfoVO_t1761987528 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UrlInfoVO_t1761987528 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// MagicTV.vo.KeyValueVO[]
struct KeyValueVOU5BU5D_t1359699380  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValueVO_t1780100105 * m_Items[1];

public:
	inline KeyValueVO_t1780100105 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyValueVO_t1780100105 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyValueVO_t1780100105 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ConfigurableListenerEventInterface[]
struct ConfigurableListenerEventInterfaceU5BU5D_t901733637  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// VideoPlaybackBehaviour[]
struct VideoPlaybackBehaviourU5BU5D_t451491646  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) VideoPlaybackBehaviour_t2848361927 * m_Items[1];

public:
	inline VideoPlaybackBehaviour_t2848361927 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline VideoPlaybackBehaviour_t2848361927 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, VideoPlaybackBehaviour_t2848361927 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// MagicTV.download.BundleDownloadItem[]
struct BundleDownloadItemU5BU5D_t3523762076  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BundleDownloadItem_t715938369 * m_Items[1];

public:
	inline BundleDownloadItem_t715938369 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BundleDownloadItem_t715938369 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BundleDownloadItem_t715938369 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// MagicTV.vo.RequestImagesItemVO[]
struct RequestImagesItemVOU5BU5D_t245365736  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) RequestImagesItemVO_t2061191909 * m_Items[1];

public:
	inline RequestImagesItemVO_t2061191909 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline RequestImagesItemVO_t2061191909 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, RequestImagesItemVO_t2061191909 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// MagicTV.vo.BundleVO[][]
struct BundleVOU5BU5DU5BU5D_t370086189  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BundleVOU5BU5D_t2162892100* m_Items[1];

public:
	inline BundleVOU5BU5D_t2162892100* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BundleVOU5BU5D_t2162892100** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BundleVOU5BU5D_t2162892100* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// MagicTV.globals.InAppAnalytics[]
struct InAppAnalyticsU5BU5D_t1534043175  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) InAppAnalytics_t2316734034 * m_Items[1];

public:
	inline InAppAnalytics_t2316734034 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline InAppAnalytics_t2316734034 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, InAppAnalytics_t2316734034 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// MagicTV.in_apps.InAppEventsChannel[]
struct InAppEventsChannelU5BU5D_t3730918587  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) InAppEventsChannel_t1397713486 * m_Items[1];

public:
	inline InAppEventsChannel_t1397713486 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline InAppEventsChannel_t1397713486 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, InAppEventsChannel_t1397713486 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// MagicTV.globals.Perspective[]
struct PerspectiveU5BU5D_t1573652303  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler[]
struct OnChangeAnToPerspectiveEventHandlerU5BU5D_t2834383250  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) OnChangeAnToPerspectiveEventHandler_t2702236419 * m_Items[1];

public:
	inline OnChangeAnToPerspectiveEventHandler_t2702236419 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline OnChangeAnToPerspectiveEventHandler_t2702236419 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, OnChangeAnToPerspectiveEventHandler_t2702236419 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// MagicTV.globals.StateMachine[]
struct StateMachineU5BU5D_t1009320683  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// MagicTV.globals.events.StateMagineEvents/OnChangeToAnStateEventHandler[]
struct OnChangeToAnStateEventHandlerU5BU5D_t3921866367  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) OnChangeToAnStateEventHandler_t630243546 * m_Items[1];

public:
	inline OnChangeToAnStateEventHandler_t630243546 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline OnChangeToAnStateEventHandler_t630243546 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, OnChangeToAnStateEventHandler_t630243546 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// MagicTV.in_apps.GarbageItemVO[]
struct GarbageItemVOU5BU5D_t844972838  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GarbageItemVO_t1335202303 * m_Items[1];

public:
	inline GarbageItemVO_t1335202303 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GarbageItemVO_t1335202303 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GarbageItemVO_t1335202303 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// InApp.InAppEventDispatcher/EventNames[]
struct EventNamesU5BU5D_t3350345955  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// InApp.InAppEventDispatcher/InAppEventDispatcherOnOff[]
struct InAppEventDispatcherOnOffU5BU5D_t3213007938  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) InAppEventDispatcherOnOff_t3474551315 * m_Items[1];

public:
	inline InAppEventDispatcherOnOff_t3474551315 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline InAppEventDispatcherOnOff_t3474551315 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, InAppEventDispatcherOnOff_t3474551315 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ARM.events.VoidEventDispatcher[]
struct VoidEventDispatcherU5BU5D_t1335221954  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) VoidEventDispatcher_t1760461203 * m_Items[1];

public:
	inline VoidEventDispatcher_t1760461203 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline VoidEventDispatcher_t1760461203 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, VoidEventDispatcher_t1760461203 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ARM.events.GenericParameterEventDispatcher[]
struct GenericParameterEventDispatcherU5BU5D_t1293982392  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GenericParameterEventDispatcher_t3811317845 * m_Items[1];

public:
	inline GenericParameterEventDispatcher_t3811317845 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GenericParameterEventDispatcher_t3811317845 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GenericParameterEventDispatcher_t3811317845 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ARM.components.GroupInAppAbstractComponentsManager[]
struct GroupInAppAbstractComponentsManagerU5BU5D_t820496399  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GroupInAppAbstractComponentsManager_t739334794 * m_Items[1];

public:
	inline GroupInAppAbstractComponentsManager_t739334794 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GroupInAppAbstractComponentsManager_t739334794 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GroupInAppAbstractComponentsManager_t739334794 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// LitJson.JsonData[]
struct JsonDataU5BU5D_t3794642467  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JsonData_t1715015430 * m_Items[1];

public:
	inline JsonData_t1715015430 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JsonData_t1715015430 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JsonData_t1715015430 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// LitJson.PropertyMetadata[]
struct PropertyMetadataU5BU5D_t2846646185  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PropertyMetadata_t4066634616  m_Items[1];

public:
	inline PropertyMetadata_t4066634616  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PropertyMetadata_t4066634616 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PropertyMetadata_t4066634616  value)
	{
		m_Items[index] = value;
	}
};
// LitJson.ArrayMetadata[]
struct ArrayMetadataU5BU5D_t2077555787  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ArrayMetadata_t4058342910  m_Items[1];

public:
	inline ArrayMetadata_t4058342910  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ArrayMetadata_t4058342910 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ArrayMetadata_t4058342910  value)
	{
		m_Items[index] = value;
	}
};
// LitJson.ObjectMetadata[]
struct ObjectMetadataU5BU5D_t223301783  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ObjectMetadata_t2009294498  m_Items[1];

public:
	inline ObjectMetadata_t2009294498  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ObjectMetadata_t2009294498 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ObjectMetadata_t2009294498  value)
	{
		m_Items[index] = value;
	}
};
// LitJson.ExporterFunc[]
struct ExporterFuncU5BU5D_t1466466532  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ExporterFunc_t3330360473 * m_Items[1];

public:
	inline ExporterFunc_t3330360473 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ExporterFunc_t3330360473 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ExporterFunc_t3330360473 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// LitJson.ImporterFunc[]
struct ImporterFuncU5BU5D_t1734035919  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ImporterFunc_t2138319818 * m_Items[1];

public:
	inline ImporterFunc_t2138319818 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImporterFunc_t2138319818 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImporterFunc_t2138319818 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// LitJson.WriterContext[]
struct WriterContextU5BU5D_t4146268263  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) WriterContext_t3060158226 * m_Items[1];

public:
	inline WriterContext_t3060158226 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline WriterContext_t3060158226 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, WriterContext_t3060158226 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// LitJson.Lexer/StateHandler[]
struct StateHandlerU5BU5D_t2657855690  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) StateHandler_t3261942315 * m_Items[1];

public:
	inline StateHandler_t3261942315 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline StateHandler_t3261942315 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, StateHandler_t3261942315 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.ImageTargetBehaviour[]
struct ImageTargetBehaviourU5BU5D_t645171330  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ImageTargetBehaviour_t1735871187 * m_Items[1];

public:
	inline ImageTargetBehaviour_t1735871187 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImageTargetBehaviour_t1735871187 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImageTargetBehaviour_t1735871187 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// MagicTV.vo.KeyTypeValue[]
struct KeyTypeValueU5BU5D_t1961483187  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyTypeValue_t654867638 * m_Items[1];

public:
	inline KeyTypeValue_t654867638 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyTypeValue_t654867638 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyTypeValue_t654867638 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation[]
struct CalculationU5BU5D_t3054796293  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation[]
struct CalculationU5BU5D_t3522546699  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// Loom/DelayedQueueItem[]
struct DelayedQueueItemU5BU5D_t665102097  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DelayedQueueItem_t3219765808 * m_Items[1];

public:
	inline DelayedQueueItem_t3219765808 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DelayedQueueItem_t3219765808 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DelayedQueueItem_t3219765808 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PedidoData[]
struct PedidoDataU5BU5D_t3795515318  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PedidoData_t3443010735 * m_Items[1];

public:
	inline PedidoData_t3443010735 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PedidoData_t3443010735 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PedidoData_t3443010735 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CarnesData[]
struct CarnesDataU5BU5D_t2965870023  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CarnesData_t3441984818 * m_Items[1];

public:
	inline CarnesData_t3441984818 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CarnesData_t3441984818 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CarnesData_t3441984818 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CuponData[]
struct CuponDataU5BU5D_t2974711614  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CuponData_t807724487 * m_Items[1];

public:
	inline CuponData_t807724487 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CuponData_t807724487 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CuponData_t807724487 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PublicationData[]
struct PublicationDataU5BU5D_t1155489811  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PublicationData_t473548758 * m_Items[1];

public:
	inline PublicationData_t473548758 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PublicationData_t473548758 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PublicationData_t473548758 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PedidoEnvioData[]
struct PedidoEnvioDataU5BU5D_t3758101737  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PedidoEnvioData_t1309077304 * m_Items[1];

public:
	inline PedidoEnvioData_t1309077304 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PedidoEnvioData_t1309077304 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PedidoEnvioData_t1309077304 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// OfertaValues[]
struct OfertaValuesU5BU5D_t679727234  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) OfertaValues_t3488850643 * m_Items[1];

public:
	inline OfertaValues_t3488850643 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline OfertaValues_t3488850643 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, OfertaValues_t3488850643 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CortesData[][]
struct CortesDataU5BU5DU5BU5D_t3530259378  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CortesDataU5BU5D_t1695339363* m_Items[1];

public:
	inline CortesDataU5BU5D_t1695339363* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CortesDataU5BU5D_t1695339363** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CortesDataU5BU5D_t1695339363* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TabloideData[]
struct TabloideDataU5BU5D_t684230357  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TabloideData_t1867721404 * m_Items[1];

public:
	inline TabloideData_t1867721404 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TabloideData_t1867721404 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TabloideData_t1867721404 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniWebViewNativeListener[]
struct UniWebViewNativeListenerU5BU5D_t4282592317  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UniWebViewNativeListener_t795571060 * m_Items[1];

public:
	inline UniWebViewNativeListener_t795571060 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UniWebViewNativeListener_t795571060 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UniWebViewNativeListener_t795571060 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.WireframeBehaviour[]
struct WireframeBehaviourU5BU5D_t1176995246  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) WireframeBehaviour_t433318935 * m_Items[1];

public:
	inline WireframeBehaviour_t433318935 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline WireframeBehaviour_t433318935 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, WireframeBehaviour_t433318935 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// iTween[]
struct iTweenU5BU5D_t2929256503  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) iTween_t3087282050 * m_Items[1];

public:
	inline iTween_t3087282050 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline iTween_t3087282050 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, iTween_t3087282050 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
