﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.ArrayList
struct ArrayList_t3948406897;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Core.NameFilter
struct  NameFilter_t123192849  : public Il2CppObject
{
public:
	// System.String ICSharpCode.SharpZipLib.Core.NameFilter::filter_
	String_t* ___filter__0;
	// System.Collections.ArrayList ICSharpCode.SharpZipLib.Core.NameFilter::inclusions_
	ArrayList_t3948406897 * ___inclusions__1;
	// System.Collections.ArrayList ICSharpCode.SharpZipLib.Core.NameFilter::exclusions_
	ArrayList_t3948406897 * ___exclusions__2;

public:
	inline static int32_t get_offset_of_filter__0() { return static_cast<int32_t>(offsetof(NameFilter_t123192849, ___filter__0)); }
	inline String_t* get_filter__0() const { return ___filter__0; }
	inline String_t** get_address_of_filter__0() { return &___filter__0; }
	inline void set_filter__0(String_t* value)
	{
		___filter__0 = value;
		Il2CppCodeGenWriteBarrier(&___filter__0, value);
	}

	inline static int32_t get_offset_of_inclusions__1() { return static_cast<int32_t>(offsetof(NameFilter_t123192849, ___inclusions__1)); }
	inline ArrayList_t3948406897 * get_inclusions__1() const { return ___inclusions__1; }
	inline ArrayList_t3948406897 ** get_address_of_inclusions__1() { return &___inclusions__1; }
	inline void set_inclusions__1(ArrayList_t3948406897 * value)
	{
		___inclusions__1 = value;
		Il2CppCodeGenWriteBarrier(&___inclusions__1, value);
	}

	inline static int32_t get_offset_of_exclusions__2() { return static_cast<int32_t>(offsetof(NameFilter_t123192849, ___exclusions__2)); }
	inline ArrayList_t3948406897 * get_exclusions__2() const { return ___exclusions__2; }
	inline ArrayList_t3948406897 ** get_address_of_exclusions__2() { return &___exclusions__2; }
	inline void set_exclusions__2(ArrayList_t3948406897 * value)
	{
		___exclusions__2 = value;
		Il2CppCodeGenWriteBarrier(&___exclusions__2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
