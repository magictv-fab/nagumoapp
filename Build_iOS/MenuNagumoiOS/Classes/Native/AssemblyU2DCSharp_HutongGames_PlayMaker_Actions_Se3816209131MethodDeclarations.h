﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetGUIContentColor
struct SetGUIContentColor_t3816209131;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetGUIContentColor::.ctor()
extern "C"  void SetGUIContentColor__ctor_m4080376283 (SetGUIContentColor_t3816209131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUIContentColor::Reset()
extern "C"  void SetGUIContentColor_Reset_m1726809224 (SetGUIContentColor_t3816209131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUIContentColor::OnGUI()
extern "C"  void SetGUIContentColor_OnGUI_m3575774933 (SetGUIContentColor_t3816209131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
