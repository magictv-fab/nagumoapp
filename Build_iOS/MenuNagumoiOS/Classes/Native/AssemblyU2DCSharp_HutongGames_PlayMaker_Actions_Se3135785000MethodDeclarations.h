﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetLightFlare
struct SetLightFlare_t3135785000;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetLightFlare::.ctor()
extern "C"  void SetLightFlare__ctor_m797029902 (SetLightFlare_t3135785000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightFlare::Reset()
extern "C"  void SetLightFlare_Reset_m2738430139 (SetLightFlare_t3135785000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightFlare::OnEnter()
extern "C"  void SetLightFlare_OnEnter_m833285733 (SetLightFlare_t3135785000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightFlare::DoSetLightRange()
extern "C"  void SetLightFlare_DoSetLightRange_m3077066474 (SetLightFlare_t3135785000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
