﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RotateGUI
struct  RotateGUI_t3156048174  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RotateGUI::angle
	FsmFloat_t2134102846 * ___angle_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RotateGUI::pivotX
	FsmFloat_t2134102846 * ___pivotX_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RotateGUI::pivotY
	FsmFloat_t2134102846 * ___pivotY_11;
	// System.Boolean HutongGames.PlayMaker.Actions.RotateGUI::normalized
	bool ___normalized_12;
	// System.Boolean HutongGames.PlayMaker.Actions.RotateGUI::applyGlobally
	bool ___applyGlobally_13;
	// System.Boolean HutongGames.PlayMaker.Actions.RotateGUI::applied
	bool ___applied_14;

public:
	inline static int32_t get_offset_of_angle_9() { return static_cast<int32_t>(offsetof(RotateGUI_t3156048174, ___angle_9)); }
	inline FsmFloat_t2134102846 * get_angle_9() const { return ___angle_9; }
	inline FsmFloat_t2134102846 ** get_address_of_angle_9() { return &___angle_9; }
	inline void set_angle_9(FsmFloat_t2134102846 * value)
	{
		___angle_9 = value;
		Il2CppCodeGenWriteBarrier(&___angle_9, value);
	}

	inline static int32_t get_offset_of_pivotX_10() { return static_cast<int32_t>(offsetof(RotateGUI_t3156048174, ___pivotX_10)); }
	inline FsmFloat_t2134102846 * get_pivotX_10() const { return ___pivotX_10; }
	inline FsmFloat_t2134102846 ** get_address_of_pivotX_10() { return &___pivotX_10; }
	inline void set_pivotX_10(FsmFloat_t2134102846 * value)
	{
		___pivotX_10 = value;
		Il2CppCodeGenWriteBarrier(&___pivotX_10, value);
	}

	inline static int32_t get_offset_of_pivotY_11() { return static_cast<int32_t>(offsetof(RotateGUI_t3156048174, ___pivotY_11)); }
	inline FsmFloat_t2134102846 * get_pivotY_11() const { return ___pivotY_11; }
	inline FsmFloat_t2134102846 ** get_address_of_pivotY_11() { return &___pivotY_11; }
	inline void set_pivotY_11(FsmFloat_t2134102846 * value)
	{
		___pivotY_11 = value;
		Il2CppCodeGenWriteBarrier(&___pivotY_11, value);
	}

	inline static int32_t get_offset_of_normalized_12() { return static_cast<int32_t>(offsetof(RotateGUI_t3156048174, ___normalized_12)); }
	inline bool get_normalized_12() const { return ___normalized_12; }
	inline bool* get_address_of_normalized_12() { return &___normalized_12; }
	inline void set_normalized_12(bool value)
	{
		___normalized_12 = value;
	}

	inline static int32_t get_offset_of_applyGlobally_13() { return static_cast<int32_t>(offsetof(RotateGUI_t3156048174, ___applyGlobally_13)); }
	inline bool get_applyGlobally_13() const { return ___applyGlobally_13; }
	inline bool* get_address_of_applyGlobally_13() { return &___applyGlobally_13; }
	inline void set_applyGlobally_13(bool value)
	{
		___applyGlobally_13 = value;
	}

	inline static int32_t get_offset_of_applied_14() { return static_cast<int32_t>(offsetof(RotateGUI_t3156048174, ___applied_14)); }
	inline bool get_applied_14() const { return ___applied_14; }
	inline bool* get_address_of_applied_14() { return &___applied_14; }
	inline void set_applied_14(bool value)
	{
		___applied_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
