﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorTarget
struct GetAnimatorTarget_t3370286236;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorTarget::.ctor()
extern "C"  void GetAnimatorTarget__ctor_m1909821466 (GetAnimatorTarget_t3370286236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorTarget::Reset()
extern "C"  void GetAnimatorTarget_Reset_m3851221703 (GetAnimatorTarget_t3370286236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorTarget::OnEnter()
extern "C"  void GetAnimatorTarget_OnEnter_m779122033 (GetAnimatorTarget_t3370286236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorTarget::OnUpdate()
extern "C"  void GetAnimatorTarget_OnUpdate_m1811505906 (GetAnimatorTarget_t3370286236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorTarget::OnAnimatorMoveEvent()
extern "C"  void GetAnimatorTarget_OnAnimatorMoveEvent_m1664969275 (GetAnimatorTarget_t3370286236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorTarget::DoGetTarget()
extern "C"  void GetAnimatorTarget_DoGetTarget_m1632445748 (GetAnimatorTarget_t3370286236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorTarget::OnExit()
extern "C"  void GetAnimatorTarget_OnExit_m588241927 (GetAnimatorTarget_t3370286236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
