﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenScaleUpdate
struct iTweenScaleUpdate_t4241723967;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenScaleUpdate::.ctor()
extern "C"  void iTweenScaleUpdate__ctor_m2920227287 (iTweenScaleUpdate_t4241723967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleUpdate::Reset()
extern "C"  void iTweenScaleUpdate_Reset_m566660228 (iTweenScaleUpdate_t4241723967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleUpdate::OnEnter()
extern "C"  void iTweenScaleUpdate_OnEnter_m1116507118 (iTweenScaleUpdate_t4241723967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleUpdate::OnExit()
extern "C"  void iTweenScaleUpdate_OnExit_m1846051306 (iTweenScaleUpdate_t4241723967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleUpdate::OnUpdate()
extern "C"  void iTweenScaleUpdate_OnUpdate_m3680508949 (iTweenScaleUpdate_t4241723967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleUpdate::DoiTween()
extern "C"  void iTweenScaleUpdate_DoiTween_m1703114522 (iTweenScaleUpdate_t4241723967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
