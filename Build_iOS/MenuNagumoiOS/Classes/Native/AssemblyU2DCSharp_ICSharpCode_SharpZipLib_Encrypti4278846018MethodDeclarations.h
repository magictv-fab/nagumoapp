﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Encryption.PkzipClassic
struct PkzipClassic_t4278846018;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"

// System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassic::.ctor()
extern "C"  void PkzipClassic__ctor_m2584203653 (PkzipClassic_t4278846018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ICSharpCode.SharpZipLib.Encryption.PkzipClassic::GenerateKeys(System.Byte[])
extern "C"  ByteU5BU5D_t4260760469* PkzipClassic_GenerateKeys_m3974161611 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___seed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
