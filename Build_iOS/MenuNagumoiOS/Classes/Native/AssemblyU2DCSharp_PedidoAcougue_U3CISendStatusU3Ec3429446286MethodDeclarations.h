﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PedidoAcougue/<ISendStatus>c__Iterator6E
struct U3CISendStatusU3Ec__Iterator6E_t3429446286;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PedidoAcougue/<ISendStatus>c__Iterator6E::.ctor()
extern "C"  void U3CISendStatusU3Ec__Iterator6E__ctor_m1894656973 (U3CISendStatusU3Ec__Iterator6E_t3429446286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PedidoAcougue/<ISendStatus>c__Iterator6E::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CISendStatusU3Ec__Iterator6E_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1562013487 (U3CISendStatusU3Ec__Iterator6E_t3429446286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PedidoAcougue/<ISendStatus>c__Iterator6E::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CISendStatusU3Ec__Iterator6E_System_Collections_IEnumerator_get_Current_m1746317507 (U3CISendStatusU3Ec__Iterator6E_t3429446286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PedidoAcougue/<ISendStatus>c__Iterator6E::MoveNext()
extern "C"  bool U3CISendStatusU3Ec__Iterator6E_MoveNext_m2225575663 (U3CISendStatusU3Ec__Iterator6E_t3429446286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PedidoAcougue/<ISendStatus>c__Iterator6E::Dispose()
extern "C"  void U3CISendStatusU3Ec__Iterator6E_Dispose_m3892467914 (U3CISendStatusU3Ec__Iterator6E_t3429446286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PedidoAcougue/<ISendStatus>c__Iterator6E::Reset()
extern "C"  void U3CISendStatusU3Ec__Iterator6E_Reset_m3836057210 (U3CISendStatusU3Ec__Iterator6E_t3429446286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
