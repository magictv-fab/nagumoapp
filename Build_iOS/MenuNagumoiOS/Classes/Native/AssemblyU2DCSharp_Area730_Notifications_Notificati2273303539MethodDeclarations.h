﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Area730.Notifications.Notification
struct Notification_t2273303539;
// System.String
struct String_t;
// System.Int64[]
struct Int64U5BU5D_t2174042770;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void Area730.Notifications.Notification::.ctor(System.Int32,System.String,System.String,System.Int32,System.Boolean,System.String,System.String,System.String,System.String,System.Int64[],System.Int64,System.Int64,System.Boolean,System.Int64,System.Int32,System.Boolean,System.String,System.String,System.String)
extern "C"  void Notification__ctor_m394161417 (Notification_t2273303539 * __this, int32_t ___id0, String_t* ___smallIcon1, String_t* ___largeIcon2, int32_t ___defaults3, bool ___autoCancel4, String_t* ___sound5, String_t* ___ticker6, String_t* ___title7, String_t* ___body8, Int64U5BU5D_t2174042770* ___vibroPattern9, int64_t ___when10, int64_t ___delay11, bool ___repeating12, int64_t ___interval13, int32_t ___number14, bool ___alertOnce15, String_t* ___color16, String_t* ___group17, String_t* ___sortKey18, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Area730.Notifications.Notification::get_ID()
extern "C"  int32_t Notification_get_ID_m754691475 (Notification_t2273303539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Area730.Notifications.Notification::get_SmallIcon()
extern "C"  String_t* Notification_get_SmallIcon_m2866219701 (Notification_t2273303539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Area730.Notifications.Notification::get_LargeIcon()
extern "C"  String_t* Notification_get_LargeIcon_m2214509033 (Notification_t2273303539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Area730.Notifications.Notification::get_Defaults()
extern "C"  int32_t Notification_get_Defaults_m454179818 (Notification_t2273303539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Area730.Notifications.Notification::get_AutoCancel()
extern "C"  bool Notification_get_AutoCancel_m1572482247 (Notification_t2273303539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Area730.Notifications.Notification::get_Sound()
extern "C"  String_t* Notification_get_Sound_m7492580 (Notification_t2273303539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Area730.Notifications.Notification::get_Ticker()
extern "C"  String_t* Notification_get_Ticker_m427031159 (Notification_t2273303539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Area730.Notifications.Notification::get_Title()
extern "C"  String_t* Notification_get_Title_m722239213 (Notification_t2273303539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Area730.Notifications.Notification::get_Body()
extern "C"  String_t* Notification_get_Body_m1868353039 (Notification_t2273303539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64[] Area730.Notifications.Notification::get_VibratePattern()
extern "C"  Int64U5BU5D_t2174042770* Notification_get_VibratePattern_m2954760536 (Notification_t2273303539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Area730.Notifications.Notification::get_When()
extern "C"  int64_t Notification_get_When_m1760858131 (Notification_t2273303539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Area730.Notifications.Notification::get_Delay()
extern "C"  int64_t Notification_get_Delay_m3284561516 (Notification_t2273303539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Area730.Notifications.Notification::get_IsRepeating()
extern "C"  bool Notification_get_IsRepeating_m2316957377 (Notification_t2273303539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Area730.Notifications.Notification::get_Interval()
extern "C"  int64_t Notification_get_Interval_m2543429342 (Notification_t2273303539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Area730.Notifications.Notification::get_Number()
extern "C"  int32_t Notification_get_Number_m1711086817 (Notification_t2273303539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Area730.Notifications.Notification::get_AlertOnce()
extern "C"  bool Notification_get_AlertOnce_m1485104929 (Notification_t2273303539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Area730.Notifications.Notification::get_Color()
extern "C"  String_t* Notification_get_Color_m2979034424 (Notification_t2273303539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Area730.Notifications.Notification::get_Group()
extern "C"  String_t* Notification_get_Group_m2322916692 (Notification_t2273303539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Area730.Notifications.Notification::get_SortKey()
extern "C"  String_t* Notification_get_SortKey_m393473334 (Notification_t2273303539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Area730.Notifications.Notification::ToString()
extern "C"  String_t* Notification_ToString_m1560947536 (Notification_t2273303539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
