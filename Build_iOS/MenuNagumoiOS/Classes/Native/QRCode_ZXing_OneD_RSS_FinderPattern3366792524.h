﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;
// ZXing.ResultPoint[]
struct ResultPointU5BU5D_t1195164344;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.FinderPattern
struct  FinderPattern_t3366792524  : public Il2CppObject
{
public:
	// System.Int32 ZXing.OneD.RSS.FinderPattern::<Value>k__BackingField
	int32_t ___U3CValueU3Ek__BackingField_0;
	// System.Int32[] ZXing.OneD.RSS.FinderPattern::<StartEnd>k__BackingField
	Int32U5BU5D_t3230847821* ___U3CStartEndU3Ek__BackingField_1;
	// ZXing.ResultPoint[] ZXing.OneD.RSS.FinderPattern::<ResultPoints>k__BackingField
	ResultPointU5BU5D_t1195164344* ___U3CResultPointsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FinderPattern_t3366792524, ___U3CValueU3Ek__BackingField_0)); }
	inline int32_t get_U3CValueU3Ek__BackingField_0() const { return ___U3CValueU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CValueU3Ek__BackingField_0() { return &___U3CValueU3Ek__BackingField_0; }
	inline void set_U3CValueU3Ek__BackingField_0(int32_t value)
	{
		___U3CValueU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CStartEndU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FinderPattern_t3366792524, ___U3CStartEndU3Ek__BackingField_1)); }
	inline Int32U5BU5D_t3230847821* get_U3CStartEndU3Ek__BackingField_1() const { return ___U3CStartEndU3Ek__BackingField_1; }
	inline Int32U5BU5D_t3230847821** get_address_of_U3CStartEndU3Ek__BackingField_1() { return &___U3CStartEndU3Ek__BackingField_1; }
	inline void set_U3CStartEndU3Ek__BackingField_1(Int32U5BU5D_t3230847821* value)
	{
		___U3CStartEndU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CStartEndU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CResultPointsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FinderPattern_t3366792524, ___U3CResultPointsU3Ek__BackingField_2)); }
	inline ResultPointU5BU5D_t1195164344* get_U3CResultPointsU3Ek__BackingField_2() const { return ___U3CResultPointsU3Ek__BackingField_2; }
	inline ResultPointU5BU5D_t1195164344** get_address_of_U3CResultPointsU3Ek__BackingField_2() { return &___U3CResultPointsU3Ek__BackingField_2; }
	inline void set_U3CResultPointsU3Ek__BackingField_2(ResultPointU5BU5D_t1195164344* value)
	{
		___U3CResultPointsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CResultPointsU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
