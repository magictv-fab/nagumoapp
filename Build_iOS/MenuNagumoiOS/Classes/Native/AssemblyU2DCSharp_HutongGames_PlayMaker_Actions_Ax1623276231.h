﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AxisEvent
struct  AxisEvent_t1623276231  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.AxisEvent::horizontalAxis
	FsmString_t952858651 * ___horizontalAxis_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.AxisEvent::verticalAxis
	FsmString_t952858651 * ___verticalAxis_10;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.AxisEvent::leftEvent
	FsmEvent_t2133468028 * ___leftEvent_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.AxisEvent::rightEvent
	FsmEvent_t2133468028 * ___rightEvent_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.AxisEvent::upEvent
	FsmEvent_t2133468028 * ___upEvent_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.AxisEvent::downEvent
	FsmEvent_t2133468028 * ___downEvent_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.AxisEvent::anyDirection
	FsmEvent_t2133468028 * ___anyDirection_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.AxisEvent::noDirection
	FsmEvent_t2133468028 * ___noDirection_16;

public:
	inline static int32_t get_offset_of_horizontalAxis_9() { return static_cast<int32_t>(offsetof(AxisEvent_t1623276231, ___horizontalAxis_9)); }
	inline FsmString_t952858651 * get_horizontalAxis_9() const { return ___horizontalAxis_9; }
	inline FsmString_t952858651 ** get_address_of_horizontalAxis_9() { return &___horizontalAxis_9; }
	inline void set_horizontalAxis_9(FsmString_t952858651 * value)
	{
		___horizontalAxis_9 = value;
		Il2CppCodeGenWriteBarrier(&___horizontalAxis_9, value);
	}

	inline static int32_t get_offset_of_verticalAxis_10() { return static_cast<int32_t>(offsetof(AxisEvent_t1623276231, ___verticalAxis_10)); }
	inline FsmString_t952858651 * get_verticalAxis_10() const { return ___verticalAxis_10; }
	inline FsmString_t952858651 ** get_address_of_verticalAxis_10() { return &___verticalAxis_10; }
	inline void set_verticalAxis_10(FsmString_t952858651 * value)
	{
		___verticalAxis_10 = value;
		Il2CppCodeGenWriteBarrier(&___verticalAxis_10, value);
	}

	inline static int32_t get_offset_of_leftEvent_11() { return static_cast<int32_t>(offsetof(AxisEvent_t1623276231, ___leftEvent_11)); }
	inline FsmEvent_t2133468028 * get_leftEvent_11() const { return ___leftEvent_11; }
	inline FsmEvent_t2133468028 ** get_address_of_leftEvent_11() { return &___leftEvent_11; }
	inline void set_leftEvent_11(FsmEvent_t2133468028 * value)
	{
		___leftEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___leftEvent_11, value);
	}

	inline static int32_t get_offset_of_rightEvent_12() { return static_cast<int32_t>(offsetof(AxisEvent_t1623276231, ___rightEvent_12)); }
	inline FsmEvent_t2133468028 * get_rightEvent_12() const { return ___rightEvent_12; }
	inline FsmEvent_t2133468028 ** get_address_of_rightEvent_12() { return &___rightEvent_12; }
	inline void set_rightEvent_12(FsmEvent_t2133468028 * value)
	{
		___rightEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___rightEvent_12, value);
	}

	inline static int32_t get_offset_of_upEvent_13() { return static_cast<int32_t>(offsetof(AxisEvent_t1623276231, ___upEvent_13)); }
	inline FsmEvent_t2133468028 * get_upEvent_13() const { return ___upEvent_13; }
	inline FsmEvent_t2133468028 ** get_address_of_upEvent_13() { return &___upEvent_13; }
	inline void set_upEvent_13(FsmEvent_t2133468028 * value)
	{
		___upEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___upEvent_13, value);
	}

	inline static int32_t get_offset_of_downEvent_14() { return static_cast<int32_t>(offsetof(AxisEvent_t1623276231, ___downEvent_14)); }
	inline FsmEvent_t2133468028 * get_downEvent_14() const { return ___downEvent_14; }
	inline FsmEvent_t2133468028 ** get_address_of_downEvent_14() { return &___downEvent_14; }
	inline void set_downEvent_14(FsmEvent_t2133468028 * value)
	{
		___downEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___downEvent_14, value);
	}

	inline static int32_t get_offset_of_anyDirection_15() { return static_cast<int32_t>(offsetof(AxisEvent_t1623276231, ___anyDirection_15)); }
	inline FsmEvent_t2133468028 * get_anyDirection_15() const { return ___anyDirection_15; }
	inline FsmEvent_t2133468028 ** get_address_of_anyDirection_15() { return &___anyDirection_15; }
	inline void set_anyDirection_15(FsmEvent_t2133468028 * value)
	{
		___anyDirection_15 = value;
		Il2CppCodeGenWriteBarrier(&___anyDirection_15, value);
	}

	inline static int32_t get_offset_of_noDirection_16() { return static_cast<int32_t>(offsetof(AxisEvent_t1623276231, ___noDirection_16)); }
	inline FsmEvent_t2133468028 * get_noDirection_16() const { return ___noDirection_16; }
	inline FsmEvent_t2133468028 ** get_address_of_noDirection_16() { return &___noDirection_16; }
	inline void set_noDirection_16(FsmEvent_t2133468028 * value)
	{
		___noDirection_16 = value;
		Il2CppCodeGenWriteBarrier(&___noDirection_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
