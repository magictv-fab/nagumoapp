﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.UnixNetworkInterface
struct UnixNetworkInterface_t4099147213;
// System.String
struct String_t;
// System.Net.IPAddress
struct IPAddress_t3525271463;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Net.NetworkInformation.PhysicalAddress
struct PhysicalAddress_t2881305111;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "System_System_Net_IPAddress3525271463.h"
#include "System_System_Net_NetworkInformation_NetworkInterf1412148735.h"

// System.Void System.Net.NetworkInformation.UnixNetworkInterface::.ctor(System.String)
extern "C"  void UnixNetworkInterface__ctor_m2598046585 (UnixNetworkInterface_t4099147213 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.UnixNetworkInterface::AddAddress(System.Net.IPAddress)
extern "C"  void UnixNetworkInterface_AddAddress_m572371121 (UnixNetworkInterface_t4099147213 * __this, IPAddress_t3525271463 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.NetworkInformation.UnixNetworkInterface::SetLinkLayerInfo(System.Int32,System.Byte[],System.Net.NetworkInformation.NetworkInterfaceType)
extern "C"  void UnixNetworkInterface_SetLinkLayerInfo_m3644544527 (UnixNetworkInterface_t4099147213 * __this, int32_t ___index0, ByteU5BU5D_t4260760469* ___macAddress1, int32_t ___type2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.PhysicalAddress System.Net.NetworkInformation.UnixNetworkInterface::GetPhysicalAddress()
extern "C"  PhysicalAddress_t2881305111 * UnixNetworkInterface_GetPhysicalAddress_m511213688 (UnixNetworkInterface_t4099147213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
