﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "QRCode_ZXing_Common_DetectorResult3846928147.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Aztec.Internal.AztecDetectorResult
struct  AztecDetectorResult_t1662194462  : public DetectorResult_t3846928147
{
public:
	// System.Boolean ZXing.Aztec.Internal.AztecDetectorResult::<Compact>k__BackingField
	bool ___U3CCompactU3Ek__BackingField_2;
	// System.Int32 ZXing.Aztec.Internal.AztecDetectorResult::<NbDatablocks>k__BackingField
	int32_t ___U3CNbDatablocksU3Ek__BackingField_3;
	// System.Int32 ZXing.Aztec.Internal.AztecDetectorResult::<NbLayers>k__BackingField
	int32_t ___U3CNbLayersU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CCompactU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AztecDetectorResult_t1662194462, ___U3CCompactU3Ek__BackingField_2)); }
	inline bool get_U3CCompactU3Ek__BackingField_2() const { return ___U3CCompactU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CCompactU3Ek__BackingField_2() { return &___U3CCompactU3Ek__BackingField_2; }
	inline void set_U3CCompactU3Ek__BackingField_2(bool value)
	{
		___U3CCompactU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CNbDatablocksU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AztecDetectorResult_t1662194462, ___U3CNbDatablocksU3Ek__BackingField_3)); }
	inline int32_t get_U3CNbDatablocksU3Ek__BackingField_3() const { return ___U3CNbDatablocksU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CNbDatablocksU3Ek__BackingField_3() { return &___U3CNbDatablocksU3Ek__BackingField_3; }
	inline void set_U3CNbDatablocksU3Ek__BackingField_3(int32_t value)
	{
		___U3CNbDatablocksU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CNbLayersU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AztecDetectorResult_t1662194462, ___U3CNbLayersU3Ek__BackingField_4)); }
	inline int32_t get_U3CNbLayersU3Ek__BackingField_4() const { return ___U3CNbLayersU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CNbLayersU3Ek__BackingField_4() { return &___U3CNbLayersU3Ek__BackingField_4; }
	inline void set_U3CNbLayersU3Ek__BackingField_4(int32_t value)
	{
		___U3CNbLayersU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
