﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChangeBackgroundImage/<ColorTransition>c__Iterator18
struct U3CColorTransitionU3Ec__Iterator18_t2058937640;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ChangeBackgroundImage/<ColorTransition>c__Iterator18::.ctor()
extern "C"  void U3CColorTransitionU3Ec__Iterator18__ctor_m2576114291 (U3CColorTransitionU3Ec__Iterator18_t2058937640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ChangeBackgroundImage/<ColorTransition>c__Iterator18::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CColorTransitionU3Ec__Iterator18_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3515174089 (U3CColorTransitionU3Ec__Iterator18_t2058937640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ChangeBackgroundImage/<ColorTransition>c__Iterator18::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CColorTransitionU3Ec__Iterator18_System_Collections_IEnumerator_get_Current_m823394909 (U3CColorTransitionU3Ec__Iterator18_t2058937640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChangeBackgroundImage/<ColorTransition>c__Iterator18::MoveNext()
extern "C"  bool U3CColorTransitionU3Ec__Iterator18_MoveNext_m3565660297 (U3CColorTransitionU3Ec__Iterator18_t2058937640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeBackgroundImage/<ColorTransition>c__Iterator18::Dispose()
extern "C"  void U3CColorTransitionU3Ec__Iterator18_Dispose_m1642954224 (U3CColorTransitionU3Ec__Iterator18_t2058937640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeBackgroundImage/<ColorTransition>c__Iterator18::Reset()
extern "C"  void U3CColorTransitionU3Ec__Iterator18_Reset_m222547232 (U3CColorTransitionU3Ec__Iterator18_t2058937640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
