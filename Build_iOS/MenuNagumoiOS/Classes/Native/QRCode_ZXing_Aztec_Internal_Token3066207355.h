﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.Aztec.Internal.Token
struct Token_t3066207355;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Aztec.Internal.Token
struct  Token_t3066207355  : public Il2CppObject
{
public:
	// ZXing.Aztec.Internal.Token ZXing.Aztec.Internal.Token::previous
	Token_t3066207355 * ___previous_1;

public:
	inline static int32_t get_offset_of_previous_1() { return static_cast<int32_t>(offsetof(Token_t3066207355, ___previous_1)); }
	inline Token_t3066207355 * get_previous_1() const { return ___previous_1; }
	inline Token_t3066207355 ** get_address_of_previous_1() { return &___previous_1; }
	inline void set_previous_1(Token_t3066207355 * value)
	{
		___previous_1 = value;
		Il2CppCodeGenWriteBarrier(&___previous_1, value);
	}
};

struct Token_t3066207355_StaticFields
{
public:
	// ZXing.Aztec.Internal.Token ZXing.Aztec.Internal.Token::EMPTY
	Token_t3066207355 * ___EMPTY_0;

public:
	inline static int32_t get_offset_of_EMPTY_0() { return static_cast<int32_t>(offsetof(Token_t3066207355_StaticFields, ___EMPTY_0)); }
	inline Token_t3066207355 * get_EMPTY_0() const { return ___EMPTY_0; }
	inline Token_t3066207355 ** get_address_of_EMPTY_0() { return &___EMPTY_0; }
	inline void set_EMPTY_0(Token_t3066207355 * value)
	{
		___EMPTY_0 = value;
		Il2CppCodeGenWriteBarrier(&___EMPTY_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
