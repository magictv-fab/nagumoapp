﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BigIntegerLibrary.BigInteger
struct BigInteger_t416298622;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_BigIntegerLibrary_BigInteger416298622.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void BigIntegerLibrary.BigInteger::.ctor()
extern "C"  void BigInteger__ctor_m2765028332 (BigInteger_t416298622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BigIntegerLibrary.BigInteger::.ctor(System.Int64)
extern "C"  void BigInteger__ctor_m1317419710 (BigInteger_t416298622 * __this, int64_t ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BigIntegerLibrary.BigInteger::.ctor(BigIntegerLibrary.BigInteger)
extern "C"  void BigInteger__ctor_m4168691485 (BigInteger_t416298622 * __this, BigInteger_t416298622 * ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BigIntegerLibrary.BigInteger::.ctor(System.String)
extern "C"  void BigInteger__ctor_m2641912470 (BigInteger_t416298622 * __this, String_t* ___numberString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BigIntegerLibrary.BigInteger::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void BigInteger__ctor_m3078718253 (BigInteger_t416298622 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BigIntegerLibrary.BigInteger::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void BigInteger_GetObjectData_m999771530 (BigInteger_t416298622 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BigIntegerLibrary.BigInteger::Equals(BigIntegerLibrary.BigInteger)
extern "C"  bool BigInteger_Equals_m3708297660 (BigInteger_t416298622 * __this, BigInteger_t416298622 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BigIntegerLibrary.BigInteger::Equals(System.Object)
extern "C"  bool BigInteger_Equals_m2269743209 (BigInteger_t416298622 * __this, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BigIntegerLibrary.BigInteger::GetHashCode()
extern "C"  int32_t BigInteger_GetHashCode_m1706589069 (BigInteger_t416298622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String BigIntegerLibrary.BigInteger::ToString()
extern "C"  String_t* BigInteger_ToString_m1778182887 (BigInteger_t416298622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Parse(System.String)
extern "C"  BigInteger_t416298622 * BigInteger_Parse_m2138413779 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BigIntegerLibrary.BigInteger::CompareTo(BigIntegerLibrary.BigInteger)
extern "C"  int32_t BigInteger_CompareTo_m3063673453 (BigInteger_t416298622 * __this, BigInteger_t416298622 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BigIntegerLibrary.BigInteger::CompareTo(System.Object)
extern "C"  int32_t BigInteger_CompareTo_m3538848728 (BigInteger_t416298622 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Opposite(BigIntegerLibrary.BigInteger)
extern "C"  BigInteger_t416298622 * BigInteger_Opposite_m371459678 (Il2CppObject * __this /* static, unused */, BigInteger_t416298622 * ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BigIntegerLibrary.BigInteger::Greater(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern "C"  bool BigInteger_Greater_m3126596950 (Il2CppObject * __this /* static, unused */, BigInteger_t416298622 * ___a0, BigInteger_t416298622 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BigIntegerLibrary.BigInteger::GreaterOrEqual(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern "C"  bool BigInteger_GreaterOrEqual_m497234563 (Il2CppObject * __this /* static, unused */, BigInteger_t416298622 * ___a0, BigInteger_t416298622 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BigIntegerLibrary.BigInteger::Smaller(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern "C"  bool BigInteger_Smaller_m193741084 (Il2CppObject * __this /* static, unused */, BigInteger_t416298622 * ___a0, BigInteger_t416298622 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BigIntegerLibrary.BigInteger::SmallerOrEqual(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern "C"  bool BigInteger_SmallerOrEqual_m1092967421 (Il2CppObject * __this /* static, unused */, BigInteger_t416298622 * ___a0, BigInteger_t416298622 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Abs(BigIntegerLibrary.BigInteger)
extern "C"  BigInteger_t416298622 * BigInteger_Abs_m1611130239 (Il2CppObject * __this /* static, unused */, BigInteger_t416298622 * ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Addition(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern "C"  BigInteger_t416298622 * BigInteger_Addition_m2901086692 (Il2CppObject * __this /* static, unused */, BigInteger_t416298622 * ___a0, BigInteger_t416298622 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Subtraction(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern "C"  BigInteger_t416298622 * BigInteger_Subtraction_m2692156758 (Il2CppObject * __this /* static, unused */, BigInteger_t416298622 * ___a0, BigInteger_t416298622 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Multiplication(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern "C"  BigInteger_t416298622 * BigInteger_Multiplication_m2979210970 (Il2CppObject * __this /* static, unused */, BigInteger_t416298622 * ___a0, BigInteger_t416298622 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Division(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern "C"  BigInteger_t416298622 * BigInteger_Division_m3607370131 (Il2CppObject * __this /* static, unused */, BigInteger_t416298622 * ___a0, BigInteger_t416298622 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Modulo(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern "C"  BigInteger_t416298622 * BigInteger_Modulo_m4166204650 (Il2CppObject * __this /* static, unused */, BigInteger_t416298622 * ___a0, BigInteger_t416298622 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::op_Implicit(System.Int64)
extern "C"  BigInteger_t416298622 * BigInteger_op_Implicit_m4060285617 (Il2CppObject * __this /* static, unused */, int64_t ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::op_Implicit(System.Int32)
extern "C"  BigInteger_t416298622 * BigInteger_op_Implicit_m4060282672 (Il2CppObject * __this /* static, unused */, int32_t ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BigIntegerLibrary.BigInteger::op_Explicit(BigIntegerLibrary.BigInteger)
extern "C"  int32_t BigInteger_op_Explicit_m2007594239 (Il2CppObject * __this /* static, unused */, BigInteger_t416298622 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 BigIntegerLibrary.BigInteger::op_Explicit(BigIntegerLibrary.BigInteger)
extern "C"  uint64_t BigInteger_op_Explicit_m1995100401 (Il2CppObject * __this /* static, unused */, BigInteger_t416298622 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BigIntegerLibrary.BigInteger::op_Equality(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern "C"  bool BigInteger_op_Equality_m3768205112 (Il2CppObject * __this /* static, unused */, BigInteger_t416298622 * ___a0, BigInteger_t416298622 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BigIntegerLibrary.BigInteger::op_Inequality(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern "C"  bool BigInteger_op_Inequality_m3955291315 (Il2CppObject * __this /* static, unused */, BigInteger_t416298622 * ___a0, BigInteger_t416298622 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BigIntegerLibrary.BigInteger::op_GreaterThan(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern "C"  bool BigInteger_op_GreaterThan_m694860125 (Il2CppObject * __this /* static, unused */, BigInteger_t416298622 * ___a0, BigInteger_t416298622 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BigIntegerLibrary.BigInteger::op_LessThan(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern "C"  bool BigInteger_op_LessThan_m2909149528 (Il2CppObject * __this /* static, unused */, BigInteger_t416298622 * ___a0, BigInteger_t416298622 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BigIntegerLibrary.BigInteger::op_GreaterThanOrEqual(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern "C"  bool BigInteger_op_GreaterThanOrEqual_m1522288796 (Il2CppObject * __this /* static, unused */, BigInteger_t416298622 * ___a0, BigInteger_t416298622 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BigIntegerLibrary.BigInteger::op_LessThanOrEqual(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern "C"  bool BigInteger_op_LessThanOrEqual_m3754887233 (Il2CppObject * __this /* static, unused */, BigInteger_t416298622 * ___a0, BigInteger_t416298622 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::op_UnaryNegation(BigIntegerLibrary.BigInteger)
extern "C"  BigInteger_t416298622 * BigInteger_op_UnaryNegation_m3394917837 (Il2CppObject * __this /* static, unused */, BigInteger_t416298622 * ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::op_Addition(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern "C"  BigInteger_t416298622 * BigInteger_op_Addition_m2524922416 (Il2CppObject * __this /* static, unused */, BigInteger_t416298622 * ___a0, BigInteger_t416298622 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::op_Subtraction(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern "C"  BigInteger_t416298622 * BigInteger_op_Subtraction_m1951885706 (Il2CppObject * __this /* static, unused */, BigInteger_t416298622 * ___a0, BigInteger_t416298622 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::op_Multiply(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern "C"  BigInteger_t416298622 * BigInteger_op_Multiply_m1983069704 (Il2CppObject * __this /* static, unused */, BigInteger_t416298622 * ___a0, BigInteger_t416298622 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::op_Division(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern "C"  BigInteger_t416298622 * BigInteger_op_Division_m3231205855 (Il2CppObject * __this /* static, unused */, BigInteger_t416298622 * ___a0, BigInteger_t416298622 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::op_Modulus(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern "C"  BigInteger_t416298622 * BigInteger_op_Modulus_m409778983 (Il2CppObject * __this /* static, unused */, BigInteger_t416298622 * ___a0, BigInteger_t416298622 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Add(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern "C"  BigInteger_t416298622 * BigInteger_Add_m3775225353 (Il2CppObject * __this /* static, unused */, BigInteger_t416298622 * ___a0, BigInteger_t416298622 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Subtract(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern "C"  BigInteger_t416298622 * BigInteger_Subtract_m3722101452 (Il2CppObject * __this /* static, unused */, BigInteger_t416298622 * ___a0, BigInteger_t416298622 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Multiply(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern "C"  BigInteger_t416298622 * BigInteger_Multiply_m2359233980 (Il2CppObject * __this /* static, unused */, BigInteger_t416298622 * ___a0, BigInteger_t416298622 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::DivideByOneDigitNumber(BigIntegerLibrary.BigInteger,System.Int64)
extern "C"  BigInteger_t416298622 * BigInteger_DivideByOneDigitNumber_m1420704097 (Il2CppObject * __this /* static, unused */, BigInteger_t416298622 * ___a0, int64_t ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::DivideByBigNumber(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern "C"  BigInteger_t416298622 * BigInteger_DivideByBigNumber_m3453275985 (Il2CppObject * __this /* static, unused */, BigInteger_t416298622 * ___a0, BigInteger_t416298622 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BigIntegerLibrary.BigInteger::DivideByBigNumberSmaller(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger,System.Int32,System.Int32)
extern "C"  bool BigInteger_DivideByBigNumberSmaller_m1416995327 (Il2CppObject * __this /* static, unused */, BigInteger_t416298622 * ___r0, BigInteger_t416298622 * ___dq1, int32_t ___k2, int32_t ___m3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BigIntegerLibrary.BigInteger::Difference(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger,System.Int32,System.Int32)
extern "C"  void BigInteger_Difference_m1704883281 (Il2CppObject * __this /* static, unused */, BigInteger_t416298622 * ___r0, BigInteger_t416298622 * ___dq1, int32_t ___k2, int32_t ___m3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 BigIntegerLibrary.BigInteger::Trial(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger,System.Int32,System.Int32)
extern "C"  int64_t BigInteger_Trial_m1375418933 (Il2CppObject * __this /* static, unused */, BigInteger_t416298622 * ___r0, BigInteger_t416298622 * ___d1, int32_t ___k2, int32_t ___m3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BigIntegerLibrary.BigInteger::.cctor()
extern "C"  void BigInteger__cctor_m3629403457 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
