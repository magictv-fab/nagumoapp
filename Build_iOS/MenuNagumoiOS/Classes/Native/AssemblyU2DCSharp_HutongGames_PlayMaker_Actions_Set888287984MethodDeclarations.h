﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFsmBool
struct SetFsmBool_t888287984;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFsmBool::.ctor()
extern "C"  void SetFsmBool__ctor_m442232694 (SetFsmBool_t888287984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmBool::Reset()
extern "C"  void SetFsmBool_Reset_m2383632931 (SetFsmBool_t888287984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmBool::OnEnter()
extern "C"  void SetFsmBool_OnEnter_m3470552525 (SetFsmBool_t888287984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmBool::DoSetFsmBool()
extern "C"  void SetFsmBool_DoSetFsmBool_m3371722177 (SetFsmBool_t888287984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmBool::OnUpdate()
extern "C"  void SetFsmBool_OnUpdate_m3641472534 (SetFsmBool_t888287984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
