﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmInt[]
struct FsmIntU5BU5D_t1976821196;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.MousePickEvent
struct  MousePickEvent_t909730492  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.MousePickEvent::GameObject
	FsmOwnerDefault_t251897112 * ___GameObject_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.MousePickEvent::rayDistance
	FsmFloat_t2134102846 * ___rayDistance_10;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.MousePickEvent::mouseOver
	FsmEvent_t2133468028 * ___mouseOver_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.MousePickEvent::mouseDown
	FsmEvent_t2133468028 * ___mouseDown_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.MousePickEvent::mouseUp
	FsmEvent_t2133468028 * ___mouseUp_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.MousePickEvent::mouseOff
	FsmEvent_t2133468028 * ___mouseOff_14;
	// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.Actions.MousePickEvent::layerMask
	FsmIntU5BU5D_t1976821196* ___layerMask_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.MousePickEvent::invertMask
	FsmBool_t1075959796 * ___invertMask_16;
	// System.Boolean HutongGames.PlayMaker.Actions.MousePickEvent::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_GameObject_9() { return static_cast<int32_t>(offsetof(MousePickEvent_t909730492, ___GameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_GameObject_9() const { return ___GameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_GameObject_9() { return &___GameObject_9; }
	inline void set_GameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___GameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___GameObject_9, value);
	}

	inline static int32_t get_offset_of_rayDistance_10() { return static_cast<int32_t>(offsetof(MousePickEvent_t909730492, ___rayDistance_10)); }
	inline FsmFloat_t2134102846 * get_rayDistance_10() const { return ___rayDistance_10; }
	inline FsmFloat_t2134102846 ** get_address_of_rayDistance_10() { return &___rayDistance_10; }
	inline void set_rayDistance_10(FsmFloat_t2134102846 * value)
	{
		___rayDistance_10 = value;
		Il2CppCodeGenWriteBarrier(&___rayDistance_10, value);
	}

	inline static int32_t get_offset_of_mouseOver_11() { return static_cast<int32_t>(offsetof(MousePickEvent_t909730492, ___mouseOver_11)); }
	inline FsmEvent_t2133468028 * get_mouseOver_11() const { return ___mouseOver_11; }
	inline FsmEvent_t2133468028 ** get_address_of_mouseOver_11() { return &___mouseOver_11; }
	inline void set_mouseOver_11(FsmEvent_t2133468028 * value)
	{
		___mouseOver_11 = value;
		Il2CppCodeGenWriteBarrier(&___mouseOver_11, value);
	}

	inline static int32_t get_offset_of_mouseDown_12() { return static_cast<int32_t>(offsetof(MousePickEvent_t909730492, ___mouseDown_12)); }
	inline FsmEvent_t2133468028 * get_mouseDown_12() const { return ___mouseDown_12; }
	inline FsmEvent_t2133468028 ** get_address_of_mouseDown_12() { return &___mouseDown_12; }
	inline void set_mouseDown_12(FsmEvent_t2133468028 * value)
	{
		___mouseDown_12 = value;
		Il2CppCodeGenWriteBarrier(&___mouseDown_12, value);
	}

	inline static int32_t get_offset_of_mouseUp_13() { return static_cast<int32_t>(offsetof(MousePickEvent_t909730492, ___mouseUp_13)); }
	inline FsmEvent_t2133468028 * get_mouseUp_13() const { return ___mouseUp_13; }
	inline FsmEvent_t2133468028 ** get_address_of_mouseUp_13() { return &___mouseUp_13; }
	inline void set_mouseUp_13(FsmEvent_t2133468028 * value)
	{
		___mouseUp_13 = value;
		Il2CppCodeGenWriteBarrier(&___mouseUp_13, value);
	}

	inline static int32_t get_offset_of_mouseOff_14() { return static_cast<int32_t>(offsetof(MousePickEvent_t909730492, ___mouseOff_14)); }
	inline FsmEvent_t2133468028 * get_mouseOff_14() const { return ___mouseOff_14; }
	inline FsmEvent_t2133468028 ** get_address_of_mouseOff_14() { return &___mouseOff_14; }
	inline void set_mouseOff_14(FsmEvent_t2133468028 * value)
	{
		___mouseOff_14 = value;
		Il2CppCodeGenWriteBarrier(&___mouseOff_14, value);
	}

	inline static int32_t get_offset_of_layerMask_15() { return static_cast<int32_t>(offsetof(MousePickEvent_t909730492, ___layerMask_15)); }
	inline FsmIntU5BU5D_t1976821196* get_layerMask_15() const { return ___layerMask_15; }
	inline FsmIntU5BU5D_t1976821196** get_address_of_layerMask_15() { return &___layerMask_15; }
	inline void set_layerMask_15(FsmIntU5BU5D_t1976821196* value)
	{
		___layerMask_15 = value;
		Il2CppCodeGenWriteBarrier(&___layerMask_15, value);
	}

	inline static int32_t get_offset_of_invertMask_16() { return static_cast<int32_t>(offsetof(MousePickEvent_t909730492, ___invertMask_16)); }
	inline FsmBool_t1075959796 * get_invertMask_16() const { return ___invertMask_16; }
	inline FsmBool_t1075959796 ** get_address_of_invertMask_16() { return &___invertMask_16; }
	inline void set_invertMask_16(FsmBool_t1075959796 * value)
	{
		___invertMask_16 = value;
		Il2CppCodeGenWriteBarrier(&___invertMask_16, value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(MousePickEvent_t909730492, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
