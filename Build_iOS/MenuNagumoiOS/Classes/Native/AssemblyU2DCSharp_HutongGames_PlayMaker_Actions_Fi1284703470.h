﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FindClosest
struct  FindClosest_t1284703470  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.FindClosest::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.FindClosest::withTag
	FsmString_t952858651 * ___withTag_10;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.FindClosest::ignoreOwner
	FsmBool_t1075959796 * ___ignoreOwner_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.FindClosest::mustBeVisible
	FsmBool_t1075959796 * ___mustBeVisible_12;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.FindClosest::storeObject
	FsmGameObject_t1697147867 * ___storeObject_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FindClosest::storeDistance
	FsmFloat_t2134102846 * ___storeDistance_14;
	// System.Boolean HutongGames.PlayMaker.Actions.FindClosest::everyFrame
	bool ___everyFrame_15;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(FindClosest_t1284703470, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_withTag_10() { return static_cast<int32_t>(offsetof(FindClosest_t1284703470, ___withTag_10)); }
	inline FsmString_t952858651 * get_withTag_10() const { return ___withTag_10; }
	inline FsmString_t952858651 ** get_address_of_withTag_10() { return &___withTag_10; }
	inline void set_withTag_10(FsmString_t952858651 * value)
	{
		___withTag_10 = value;
		Il2CppCodeGenWriteBarrier(&___withTag_10, value);
	}

	inline static int32_t get_offset_of_ignoreOwner_11() { return static_cast<int32_t>(offsetof(FindClosest_t1284703470, ___ignoreOwner_11)); }
	inline FsmBool_t1075959796 * get_ignoreOwner_11() const { return ___ignoreOwner_11; }
	inline FsmBool_t1075959796 ** get_address_of_ignoreOwner_11() { return &___ignoreOwner_11; }
	inline void set_ignoreOwner_11(FsmBool_t1075959796 * value)
	{
		___ignoreOwner_11 = value;
		Il2CppCodeGenWriteBarrier(&___ignoreOwner_11, value);
	}

	inline static int32_t get_offset_of_mustBeVisible_12() { return static_cast<int32_t>(offsetof(FindClosest_t1284703470, ___mustBeVisible_12)); }
	inline FsmBool_t1075959796 * get_mustBeVisible_12() const { return ___mustBeVisible_12; }
	inline FsmBool_t1075959796 ** get_address_of_mustBeVisible_12() { return &___mustBeVisible_12; }
	inline void set_mustBeVisible_12(FsmBool_t1075959796 * value)
	{
		___mustBeVisible_12 = value;
		Il2CppCodeGenWriteBarrier(&___mustBeVisible_12, value);
	}

	inline static int32_t get_offset_of_storeObject_13() { return static_cast<int32_t>(offsetof(FindClosest_t1284703470, ___storeObject_13)); }
	inline FsmGameObject_t1697147867 * get_storeObject_13() const { return ___storeObject_13; }
	inline FsmGameObject_t1697147867 ** get_address_of_storeObject_13() { return &___storeObject_13; }
	inline void set_storeObject_13(FsmGameObject_t1697147867 * value)
	{
		___storeObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___storeObject_13, value);
	}

	inline static int32_t get_offset_of_storeDistance_14() { return static_cast<int32_t>(offsetof(FindClosest_t1284703470, ___storeDistance_14)); }
	inline FsmFloat_t2134102846 * get_storeDistance_14() const { return ___storeDistance_14; }
	inline FsmFloat_t2134102846 ** get_address_of_storeDistance_14() { return &___storeDistance_14; }
	inline void set_storeDistance_14(FsmFloat_t2134102846 * value)
	{
		___storeDistance_14 = value;
		Il2CppCodeGenWriteBarrier(&___storeDistance_14, value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(FindClosest_t1284703470, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
