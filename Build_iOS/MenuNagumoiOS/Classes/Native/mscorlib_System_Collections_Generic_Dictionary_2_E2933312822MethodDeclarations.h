﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3363211663MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UniWebViewNativeListener>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m4014056703(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2933312822 *, Dictionary_2_t1615989430 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UniWebViewNativeListener>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2034895692(__this, method) ((  Il2CppObject * (*) (Enumerator_t2933312822 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UniWebViewNativeListener>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1380706902(__this, method) ((  void (*) (Enumerator_t2933312822 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,UniWebViewNativeListener>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2154823693(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t2933312822 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UniWebViewNativeListener>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1077845800(__this, method) ((  Il2CppObject * (*) (Enumerator_t2933312822 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UniWebViewNativeListener>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1771131834(__this, method) ((  Il2CppObject * (*) (Enumerator_t2933312822 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,UniWebViewNativeListener>::MoveNext()
#define Enumerator_MoveNext_m836602374(__this, method) ((  bool (*) (Enumerator_t2933312822 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,UniWebViewNativeListener>::get_Current()
#define Enumerator_get_Current_m2090164214(__this, method) ((  KeyValuePair_2_t1514770136  (*) (Enumerator_t2933312822 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,UniWebViewNativeListener>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3084183311(__this, method) ((  String_t* (*) (Enumerator_t2933312822 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,UniWebViewNativeListener>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3796555919(__this, method) ((  UniWebViewNativeListener_t795571060 * (*) (Enumerator_t2933312822 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UniWebViewNativeListener>::Reset()
#define Enumerator_Reset_m498544849(__this, method) ((  void (*) (Enumerator_t2933312822 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UniWebViewNativeListener>::VerifyState()
#define Enumerator_VerifyState_m1167908890(__this, method) ((  void (*) (Enumerator_t2933312822 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UniWebViewNativeListener>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m4118850818(__this, method) ((  void (*) (Enumerator_t2933312822 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UniWebViewNativeListener>::Dispose()
#define Enumerator_Dispose_m588691809(__this, method) ((  void (*) (Enumerator_t2933312822 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
