﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFsmInt
struct SetFsmInt_t326690015;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFsmInt::.ctor()
extern "C"  void SetFsmInt__ctor_m3045993271 (SetFsmInt_t326690015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmInt::Reset()
extern "C"  void SetFsmInt_Reset_m692426212 (SetFsmInt_t326690015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmInt::OnEnter()
extern "C"  void SetFsmInt_OnEnter_m1718533454 (SetFsmInt_t326690015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmInt::DoSetFsmInt()
extern "C"  void SetFsmInt_DoSetFsmInt_m1499949339 (SetFsmInt_t326690015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmInt::OnUpdate()
extern "C"  void SetFsmInt_OnUpdate_m868488885 (SetFsmInt_t326690015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
