﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.ExtendedUnixData
struct ExtendedUnixData_t1879413977;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Extend90301089.h"

// System.Void ICSharpCode.SharpZipLib.Zip.ExtendedUnixData::.ctor()
extern "C"  void ExtendedUnixData__ctor_m2972986098 (ExtendedUnixData_t1879413977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 ICSharpCode.SharpZipLib.Zip.ExtendedUnixData::get_TagID()
extern "C"  int16_t ExtendedUnixData_get_TagID_m1708683156 (ExtendedUnixData_t1879413977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ExtendedUnixData::SetData(System.Byte[],System.Int32,System.Int32)
extern "C"  void ExtendedUnixData_SetData_m563960813 (ExtendedUnixData_t1879413977 * __this, ByteU5BU5D_t4260760469* ___data0, int32_t ___index1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ICSharpCode.SharpZipLib.Zip.ExtendedUnixData::GetData()
extern "C"  ByteU5BU5D_t4260760469* ExtendedUnixData_GetData_m574504518 (ExtendedUnixData_t1879413977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ExtendedUnixData::IsValidValue(System.DateTime)
extern "C"  bool ExtendedUnixData_IsValidValue_m2599916531 (Il2CppObject * __this /* static, unused */, DateTime_t4283661327  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime ICSharpCode.SharpZipLib.Zip.ExtendedUnixData::get_ModificationTime()
extern "C"  DateTime_t4283661327  ExtendedUnixData_get_ModificationTime_m1215801917 (ExtendedUnixData_t1879413977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ExtendedUnixData::set_ModificationTime(System.DateTime)
extern "C"  void ExtendedUnixData_set_ModificationTime_m3055790560 (ExtendedUnixData_t1879413977 * __this, DateTime_t4283661327  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime ICSharpCode.SharpZipLib.Zip.ExtendedUnixData::get_AccessTime()
extern "C"  DateTime_t4283661327  ExtendedUnixData_get_AccessTime_m176425541 (ExtendedUnixData_t1879413977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ExtendedUnixData::set_AccessTime(System.DateTime)
extern "C"  void ExtendedUnixData_set_AccessTime_m584941720 (ExtendedUnixData_t1879413977 * __this, DateTime_t4283661327  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime ICSharpCode.SharpZipLib.Zip.ExtendedUnixData::get_CreateTime()
extern "C"  DateTime_t4283661327  ExtendedUnixData_get_CreateTime_m3994608605 (ExtendedUnixData_t1879413977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ExtendedUnixData::set_CreateTime(System.DateTime)
extern "C"  void ExtendedUnixData_set_CreateTime_m3094823936 (ExtendedUnixData_t1879413977 * __this, DateTime_t4283661327  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Zip.ExtendedUnixData/Flags ICSharpCode.SharpZipLib.Zip.ExtendedUnixData::get_Include()
extern "C"  uint8_t ExtendedUnixData_get_Include_m2230076499 (ExtendedUnixData_t1879413977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ExtendedUnixData::set_Include(ICSharpCode.SharpZipLib.Zip.ExtendedUnixData/Flags)
extern "C"  void ExtendedUnixData_set_Include_m1138493658 (ExtendedUnixData_t1879413977 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
