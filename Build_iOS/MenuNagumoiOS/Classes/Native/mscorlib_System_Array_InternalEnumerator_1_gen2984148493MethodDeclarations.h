﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2984148493.h"
#include "mscorlib_System_Array1146569071.h"
#include "QRCode_ZXing_BarcodeFormat4201805817.h"

// System.Void System.Array/InternalEnumerator`1<ZXing.BarcodeFormat>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2891803698_gshared (InternalEnumerator_1_t2984148493 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2891803698(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2984148493 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2891803698_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<ZXing.BarcodeFormat>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1692215342_gshared (InternalEnumerator_1_t2984148493 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1692215342(__this, method) ((  void (*) (InternalEnumerator_1_t2984148493 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1692215342_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<ZXing.BarcodeFormat>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3749813668_gshared (InternalEnumerator_1_t2984148493 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3749813668(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2984148493 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3749813668_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<ZXing.BarcodeFormat>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3420007561_gshared (InternalEnumerator_1_t2984148493 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3420007561(__this, method) ((  void (*) (InternalEnumerator_1_t2984148493 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3420007561_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<ZXing.BarcodeFormat>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1573065950_gshared (InternalEnumerator_1_t2984148493 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1573065950(__this, method) ((  bool (*) (InternalEnumerator_1_t2984148493 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1573065950_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<ZXing.BarcodeFormat>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m2861917147_gshared (InternalEnumerator_1_t2984148493 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2861917147(__this, method) ((  int32_t (*) (InternalEnumerator_1_t2984148493 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2861917147_gshared)(__this, method)
