﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.MasterServerRequestHostList
struct  MasterServerRequestHostList_t4119465886  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.MasterServerRequestHostList::gameTypeName
	FsmString_t952858651 * ___gameTypeName_9;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.MasterServerRequestHostList::HostListArrivedEvent
	FsmEvent_t2133468028 * ___HostListArrivedEvent_10;

public:
	inline static int32_t get_offset_of_gameTypeName_9() { return static_cast<int32_t>(offsetof(MasterServerRequestHostList_t4119465886, ___gameTypeName_9)); }
	inline FsmString_t952858651 * get_gameTypeName_9() const { return ___gameTypeName_9; }
	inline FsmString_t952858651 ** get_address_of_gameTypeName_9() { return &___gameTypeName_9; }
	inline void set_gameTypeName_9(FsmString_t952858651 * value)
	{
		___gameTypeName_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameTypeName_9, value);
	}

	inline static int32_t get_offset_of_HostListArrivedEvent_10() { return static_cast<int32_t>(offsetof(MasterServerRequestHostList_t4119465886, ___HostListArrivedEvent_10)); }
	inline FsmEvent_t2133468028 * get_HostListArrivedEvent_10() const { return ___HostListArrivedEvent_10; }
	inline FsmEvent_t2133468028 ** get_address_of_HostListArrivedEvent_10() { return &___HostListArrivedEvent_10; }
	inline void set_HostListArrivedEvent_10(FsmEvent_t2133468028 * value)
	{
		___HostListArrivedEvent_10 = value;
		Il2CppCodeGenWriteBarrier(&___HostListArrivedEvent_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
