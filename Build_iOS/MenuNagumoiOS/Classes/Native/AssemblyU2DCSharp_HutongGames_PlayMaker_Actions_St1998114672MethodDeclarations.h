﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.StopAnimation
struct StopAnimation_t1998114672;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.StopAnimation::.ctor()
extern "C"  void StopAnimation__ctor_m2560679878 (StopAnimation_t1998114672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StopAnimation::Reset()
extern "C"  void StopAnimation_Reset_m207112819 (StopAnimation_t1998114672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StopAnimation::OnEnter()
extern "C"  void StopAnimation_OnEnter_m3483798045 (StopAnimation_t1998114672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StopAnimation::DoStopAnimation()
extern "C"  void StopAnimation_DoStopAnimation_m876404283 (StopAnimation_t1998114672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
