﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co2052155886.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Flicker
struct  Flicker_t1041969670  : public ComponentAction_1_t2052155886
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.Flicker::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Flicker::frequency
	FsmFloat_t2134102846 * ___frequency_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Flicker::amountOn
	FsmFloat_t2134102846 * ___amountOn_13;
	// System.Boolean HutongGames.PlayMaker.Actions.Flicker::rendererOnly
	bool ___rendererOnly_14;
	// System.Boolean HutongGames.PlayMaker.Actions.Flicker::realTime
	bool ___realTime_15;
	// System.Single HutongGames.PlayMaker.Actions.Flicker::startTime
	float ___startTime_16;
	// System.Single HutongGames.PlayMaker.Actions.Flicker::timer
	float ___timer_17;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(Flicker_t1041969670, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_frequency_12() { return static_cast<int32_t>(offsetof(Flicker_t1041969670, ___frequency_12)); }
	inline FsmFloat_t2134102846 * get_frequency_12() const { return ___frequency_12; }
	inline FsmFloat_t2134102846 ** get_address_of_frequency_12() { return &___frequency_12; }
	inline void set_frequency_12(FsmFloat_t2134102846 * value)
	{
		___frequency_12 = value;
		Il2CppCodeGenWriteBarrier(&___frequency_12, value);
	}

	inline static int32_t get_offset_of_amountOn_13() { return static_cast<int32_t>(offsetof(Flicker_t1041969670, ___amountOn_13)); }
	inline FsmFloat_t2134102846 * get_amountOn_13() const { return ___amountOn_13; }
	inline FsmFloat_t2134102846 ** get_address_of_amountOn_13() { return &___amountOn_13; }
	inline void set_amountOn_13(FsmFloat_t2134102846 * value)
	{
		___amountOn_13 = value;
		Il2CppCodeGenWriteBarrier(&___amountOn_13, value);
	}

	inline static int32_t get_offset_of_rendererOnly_14() { return static_cast<int32_t>(offsetof(Flicker_t1041969670, ___rendererOnly_14)); }
	inline bool get_rendererOnly_14() const { return ___rendererOnly_14; }
	inline bool* get_address_of_rendererOnly_14() { return &___rendererOnly_14; }
	inline void set_rendererOnly_14(bool value)
	{
		___rendererOnly_14 = value;
	}

	inline static int32_t get_offset_of_realTime_15() { return static_cast<int32_t>(offsetof(Flicker_t1041969670, ___realTime_15)); }
	inline bool get_realTime_15() const { return ___realTime_15; }
	inline bool* get_address_of_realTime_15() { return &___realTime_15; }
	inline void set_realTime_15(bool value)
	{
		___realTime_15 = value;
	}

	inline static int32_t get_offset_of_startTime_16() { return static_cast<int32_t>(offsetof(Flicker_t1041969670, ___startTime_16)); }
	inline float get_startTime_16() const { return ___startTime_16; }
	inline float* get_address_of_startTime_16() { return &___startTime_16; }
	inline void set_startTime_16(float value)
	{
		___startTime_16 = value;
	}

	inline static int32_t get_offset_of_timer_17() { return static_cast<int32_t>(offsetof(Flicker_t1041969670, ___timer_17)); }
	inline float get_timer_17() const { return ___timer_17; }
	inline float* get_address_of_timer_17() { return &___timer_17; }
	inline void set_timer_17(float value)
	{
		___timer_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
