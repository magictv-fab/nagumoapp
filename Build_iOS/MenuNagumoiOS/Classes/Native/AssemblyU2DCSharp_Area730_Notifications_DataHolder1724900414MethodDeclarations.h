﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Area730.Notifications.DataHolder
struct DataHolder_t1724900414;

#include "codegen/il2cpp-codegen.h"

// System.Void Area730.Notifications.DataHolder::.ctor()
extern "C"  void DataHolder__ctor_m3468023800 (DataHolder_t1724900414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Area730.Notifications.DataHolder::print()
extern "C"  void DataHolder_print_m2332975267 (DataHolder_t1724900414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
