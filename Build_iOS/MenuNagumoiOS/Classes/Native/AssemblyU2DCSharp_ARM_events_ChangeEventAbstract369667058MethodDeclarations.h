﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.events.ChangeEventAbstract
struct ChangeEventAbstract_t369667058;
// ARM.events.ChangeEventAbstract/OnChangeEventHandler
struct OnChangeEventHandler_t2629944389;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ARM_events_ChangeEventAbstract_O2629944389.h"

// System.Void ARM.events.ChangeEventAbstract::.ctor()
extern "C"  void ChangeEventAbstract__ctor_m3489462578 (ChangeEventAbstract_t369667058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.events.ChangeEventAbstract::addChangeEventListener(ARM.events.ChangeEventAbstract/OnChangeEventHandler)
extern "C"  void ChangeEventAbstract_addChangeEventListener_m3131386238 (ChangeEventAbstract_t369667058 * __this, OnChangeEventHandler_t2629944389 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.events.ChangeEventAbstract::removeChangeEventListener(ARM.events.ChangeEventAbstract/OnChangeEventHandler)
extern "C"  void ChangeEventAbstract_removeChangeEventListener_m934973475 (ChangeEventAbstract_t369667058 * __this, OnChangeEventHandler_t2629944389 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.events.ChangeEventAbstract::RaiseChageEvent(System.Single)
extern "C"  void ChangeEventAbstract_RaiseChageEvent_m3071937871 (ChangeEventAbstract_t369667058 * __this, float ___percentChange0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
