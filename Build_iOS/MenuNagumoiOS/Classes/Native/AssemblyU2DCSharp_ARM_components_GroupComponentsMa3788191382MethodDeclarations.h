﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.components.GroupComponentsManager
struct GroupComponentsManager_t3788191382;
// ARM.interfaces.GeneralComponentInterface[]
struct GeneralComponentInterfaceU5BU5D_t4161906083;

#include "codegen/il2cpp-codegen.h"

// System.Void ARM.components.GroupComponentsManager::.ctor()
extern "C"  void GroupComponentsManager__ctor_m1623171437 (GroupComponentsManager_t3788191382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.components.GroupComponentsManager::SetComponents(ARM.interfaces.GeneralComponentInterface[])
extern "C"  void GroupComponentsManager_SetComponents_m2791269183 (GroupComponentsManager_t3788191382 * __this, GeneralComponentInterfaceU5BU5D_t4161906083* ___components0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.components.GroupComponentsManager::checkAllIsReady()
extern "C"  void GroupComponentsManager_checkAllIsReady_m259129099 (GroupComponentsManager_t3788191382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.components.GroupComponentsManager::prepare()
extern "C"  void GroupComponentsManager_prepare_m1721602930 (GroupComponentsManager_t3788191382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
