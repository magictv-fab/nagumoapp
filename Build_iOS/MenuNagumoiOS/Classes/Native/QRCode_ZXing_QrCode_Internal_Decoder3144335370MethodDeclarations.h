﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.QrCode.Internal.Decoder
struct Decoder_t3144335370;
// ZXing.Common.DecoderResult
struct DecoderResult_t3752650303;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>
struct IDictionary_2_t759989846;
// ZXing.QrCode.Internal.BitMatrixParser
struct BitMatrixParser_t3480170419;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitMatrix1058711404.h"
#include "QRCode_ZXing_QrCode_Internal_BitMatrixParser3480170419.h"

// System.Void ZXing.QrCode.Internal.Decoder::.ctor()
extern "C"  void Decoder__ctor_m1224764098 (Decoder_t3144335370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.DecoderResult ZXing.QrCode.Internal.Decoder::decode(ZXing.Common.BitMatrix,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  DecoderResult_t3752650303 * Decoder_decode_m1601168167 (Decoder_t3144335370 * __this, BitMatrix_t1058711404 * ___bits0, Il2CppObject* ___hints1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.DecoderResult ZXing.QrCode.Internal.Decoder::decode(ZXing.QrCode.Internal.BitMatrixParser,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  DecoderResult_t3752650303 * Decoder_decode_m2754042856 (Decoder_t3144335370 * __this, BitMatrixParser_t3480170419 * ___parser0, Il2CppObject* ___hints1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.QrCode.Internal.Decoder::correctErrors(System.Byte[],System.Int32)
extern "C"  bool Decoder_correctErrors_m2915770263 (Decoder_t3144335370 * __this, ByteU5BU5D_t4260760469* ___codewordBytes0, int32_t ___numDataCodewords1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
