﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnionAssets.FLE.CEvent
struct CEvent_t44106931;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnionAssets.FLE.IDispatcher
struct IDispatcher_t3890817398;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnionAssets.FLE.CEvent::.ctor(System.Int32,System.String,System.Object,UnionAssets.FLE.IDispatcher)
extern "C"  void CEvent__ctor_m1207211835 (CEvent_t44106931 * __this, int32_t ___id0, String_t* ___name1, Il2CppObject * ___data2, Il2CppObject * ___dispatcher3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.CEvent::stopPropagation()
extern "C"  void CEvent_stopPropagation_m2035128705 (CEvent_t44106931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnionAssets.FLE.CEvent::stopImmediatePropagation()
extern "C"  void CEvent_stopImmediatePropagation_m2578329704 (CEvent_t44106931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnionAssets.FLE.CEvent::canBeDisptached(System.Object)
extern "C"  bool CEvent_canBeDisptached_m3878495723 (CEvent_t44106931 * __this, Il2CppObject * ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnionAssets.FLE.CEvent::get_id()
extern "C"  int32_t CEvent_get_id_m1646141789 (CEvent_t44106931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnionAssets.FLE.CEvent::get_name()
extern "C"  String_t* CEvent_get_name_m1519241570 (CEvent_t44106931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnionAssets.FLE.CEvent::get_data()
extern "C"  Il2CppObject * CEvent_get_data_m949121235 (CEvent_t44106931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnionAssets.FLE.IDispatcher UnionAssets.FLE.CEvent::get_target()
extern "C"  Il2CppObject * CEvent_get_target_m3377140275 (CEvent_t44106931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnionAssets.FLE.IDispatcher UnionAssets.FLE.CEvent::get_dispatcher()
extern "C"  Il2CppObject * CEvent_get_dispatcher_m1271788873 (CEvent_t44106931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnionAssets.FLE.CEvent::get_currentTarget()
extern "C"  Il2CppObject * CEvent_get_currentTarget_m566308163 (CEvent_t44106931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnionAssets.FLE.CEvent::get_isStoped()
extern "C"  bool CEvent_get_isStoped_m868682131 (CEvent_t44106931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnionAssets.FLE.CEvent::get_isLocked()
extern "C"  bool CEvent_get_isLocked_m1063193148 (CEvent_t44106931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
