﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OfertaCRMData[]
struct OfertaCRMDataU5BU5D_t2228159150;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OfertasCRMDataGroup
struct  OfertasCRMDataGroup_t273423897  : public Il2CppObject
{
public:
	// OfertaCRMData[] OfertasCRMDataGroup::preco
	OfertaCRMDataU5BU5D_t2228159150* ___preco_0;
	// OfertaCRMData[] OfertasCRMDataGroup::depor
	OfertaCRMDataU5BU5D_t2228159150* ___depor_1;

public:
	inline static int32_t get_offset_of_preco_0() { return static_cast<int32_t>(offsetof(OfertasCRMDataGroup_t273423897, ___preco_0)); }
	inline OfertaCRMDataU5BU5D_t2228159150* get_preco_0() const { return ___preco_0; }
	inline OfertaCRMDataU5BU5D_t2228159150** get_address_of_preco_0() { return &___preco_0; }
	inline void set_preco_0(OfertaCRMDataU5BU5D_t2228159150* value)
	{
		___preco_0 = value;
		Il2CppCodeGenWriteBarrier(&___preco_0, value);
	}

	inline static int32_t get_offset_of_depor_1() { return static_cast<int32_t>(offsetof(OfertasCRMDataGroup_t273423897, ___depor_1)); }
	inline OfertaCRMDataU5BU5D_t2228159150* get_depor_1() const { return ___depor_1; }
	inline OfertaCRMDataU5BU5D_t2228159150** get_address_of_depor_1() { return &___depor_1; }
	inline void set_depor_1(OfertaCRMDataU5BU5D_t2228159150* value)
	{
		___depor_1 = value;
		Il2CppCodeGenWriteBarrier(&___depor_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
