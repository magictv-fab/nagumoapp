﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object
struct Il2CppObject;
// GameMain
struct GameMain_t2590236907;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameMain/<corrigeVoltaRoleta>c__Iterator5A
struct  U3CcorrigeVoltaRoletaU3Ec__Iterator5A_t946778943  : public Il2CppObject
{
public:
	// UnityEngine.GameObject GameMain/<corrigeVoltaRoleta>c__Iterator5A::<roleta>__0
	GameObject_t3674682005 * ___U3CroletaU3E__0_0;
	// UnityEngine.GameObject GameMain/<corrigeVoltaRoleta>c__Iterator5A::<roleta1>__1
	GameObject_t3674682005 * ___U3Croleta1U3E__1_1;
	// UnityEngine.GameObject GameMain/<corrigeVoltaRoleta>c__Iterator5A::<roleta2>__2
	GameObject_t3674682005 * ___U3Croleta2U3E__2_2;
	// System.Int32 GameMain/<corrigeVoltaRoleta>c__Iterator5A::$PC
	int32_t ___U24PC_3;
	// System.Object GameMain/<corrigeVoltaRoleta>c__Iterator5A::$current
	Il2CppObject * ___U24current_4;
	// GameMain GameMain/<corrigeVoltaRoleta>c__Iterator5A::<>f__this
	GameMain_t2590236907 * ___U3CU3Ef__this_5;

public:
	inline static int32_t get_offset_of_U3CroletaU3E__0_0() { return static_cast<int32_t>(offsetof(U3CcorrigeVoltaRoletaU3Ec__Iterator5A_t946778943, ___U3CroletaU3E__0_0)); }
	inline GameObject_t3674682005 * get_U3CroletaU3E__0_0() const { return ___U3CroletaU3E__0_0; }
	inline GameObject_t3674682005 ** get_address_of_U3CroletaU3E__0_0() { return &___U3CroletaU3E__0_0; }
	inline void set_U3CroletaU3E__0_0(GameObject_t3674682005 * value)
	{
		___U3CroletaU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CroletaU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3Croleta1U3E__1_1() { return static_cast<int32_t>(offsetof(U3CcorrigeVoltaRoletaU3Ec__Iterator5A_t946778943, ___U3Croleta1U3E__1_1)); }
	inline GameObject_t3674682005 * get_U3Croleta1U3E__1_1() const { return ___U3Croleta1U3E__1_1; }
	inline GameObject_t3674682005 ** get_address_of_U3Croleta1U3E__1_1() { return &___U3Croleta1U3E__1_1; }
	inline void set_U3Croleta1U3E__1_1(GameObject_t3674682005 * value)
	{
		___U3Croleta1U3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3Croleta1U3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3Croleta2U3E__2_2() { return static_cast<int32_t>(offsetof(U3CcorrigeVoltaRoletaU3Ec__Iterator5A_t946778943, ___U3Croleta2U3E__2_2)); }
	inline GameObject_t3674682005 * get_U3Croleta2U3E__2_2() const { return ___U3Croleta2U3E__2_2; }
	inline GameObject_t3674682005 ** get_address_of_U3Croleta2U3E__2_2() { return &___U3Croleta2U3E__2_2; }
	inline void set_U3Croleta2U3E__2_2(GameObject_t3674682005 * value)
	{
		___U3Croleta2U3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3Croleta2U3E__2_2, value);
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CcorrigeVoltaRoletaU3Ec__Iterator5A_t946778943, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CcorrigeVoltaRoletaU3Ec__Iterator5A_t946778943, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_5() { return static_cast<int32_t>(offsetof(U3CcorrigeVoltaRoletaU3Ec__Iterator5A_t946778943, ___U3CU3Ef__this_5)); }
	inline GameMain_t2590236907 * get_U3CU3Ef__this_5() const { return ___U3CU3Ef__this_5; }
	inline GameMain_t2590236907 ** get_address_of_U3CU3Ef__this_5() { return &___U3CU3Ef__this_5; }
	inline void set_U3CU3Ef__this_5(GameMain_t2590236907 * value)
	{
		___U3CU3Ef__this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
