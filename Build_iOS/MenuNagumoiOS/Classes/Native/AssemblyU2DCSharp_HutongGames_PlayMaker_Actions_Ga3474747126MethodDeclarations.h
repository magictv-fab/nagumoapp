﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GameObjectHasChildren
struct GameObjectHasChildren_t3474747126;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GameObjectHasChildren::.ctor()
extern "C"  void GameObjectHasChildren__ctor_m776204032 (GameObjectHasChildren_t3474747126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectHasChildren::Reset()
extern "C"  void GameObjectHasChildren_Reset_m2717604269 (GameObjectHasChildren_t3474747126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectHasChildren::OnEnter()
extern "C"  void GameObjectHasChildren_OnEnter_m2294461143 (GameObjectHasChildren_t3474747126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectHasChildren::OnUpdate()
extern "C"  void GameObjectHasChildren_OnUpdate_m1542378060 (GameObjectHasChildren_t3474747126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectHasChildren::DoHasChildren()
extern "C"  void GameObjectHasChildren_DoHasChildren_m1996182732 (GameObjectHasChildren_t3474747126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
