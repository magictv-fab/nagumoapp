﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.ZipOutputStream
struct ZipOutputStream_t2362341588;
// System.IO.Stream
struct Stream_t1561764144;
// System.String
struct String_t;
// ICSharpCode.SharpZipLib.Zip.ZipEntry
struct ZipEntry_t3141689087;
// ICSharpCode.SharpZipLib.Zip.ZipExtraData
struct ZipExtraData_t591052775;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_UseZ3006992774.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipE3141689087.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipEx591052775.h"

// System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::.ctor(System.IO.Stream)
extern "C"  void ZipOutputStream__ctor_m2367025854 (ZipOutputStream_t2362341588 * __this, Stream_t1561764144 * ___baseOutputStream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::.ctor(System.IO.Stream,System.Int32)
extern "C"  void ZipOutputStream__ctor_m1466044121 (ZipOutputStream_t2362341588 * __this, Stream_t1561764144 * ___baseOutputStream0, int32_t ___bufferSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipOutputStream::get_IsFinished()
extern "C"  bool ZipOutputStream_get_IsFinished_m1293865998 (ZipOutputStream_t2362341588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::SetComment(System.String)
extern "C"  void ZipOutputStream_SetComment_m4117163944 (ZipOutputStream_t2362341588 * __this, String_t* ___comment0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::SetLevel(System.Int32)
extern "C"  void ZipOutputStream_SetLevel_m2271639248 (ZipOutputStream_t2362341588 * __this, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipOutputStream::GetLevel()
extern "C"  int32_t ZipOutputStream_GetLevel_m1642963133 (ZipOutputStream_t2362341588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Zip.UseZip64 ICSharpCode.SharpZipLib.Zip.ZipOutputStream::get_UseZip64()
extern "C"  int32_t ZipOutputStream_get_UseZip64_m3842279735 (ZipOutputStream_t2362341588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::set_UseZip64(ICSharpCode.SharpZipLib.Zip.UseZip64)
extern "C"  void ZipOutputStream_set_UseZip64_m3766235228 (ZipOutputStream_t2362341588 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::WriteLeShort(System.Int32)
extern "C"  void ZipOutputStream_WriteLeShort_m936872626 (ZipOutputStream_t2362341588 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::WriteLeInt(System.Int32)
extern "C"  void ZipOutputStream_WriteLeInt_m1385128421 (ZipOutputStream_t2362341588 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::WriteLeLong(System.Int64)
extern "C"  void ZipOutputStream_WriteLeLong_m500391659 (ZipOutputStream_t2362341588 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::PutNextEntry(ICSharpCode.SharpZipLib.Zip.ZipEntry)
extern "C"  void ZipOutputStream_PutNextEntry_m612662558 (ZipOutputStream_t2362341588 * __this, ZipEntry_t3141689087 * ___entry0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::CloseEntry()
extern "C"  void ZipOutputStream_CloseEntry_m972545047 (ZipOutputStream_t2362341588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::WriteEncryptionHeader(System.Int64)
extern "C"  void ZipOutputStream_WriteEncryptionHeader_m2515245126 (ZipOutputStream_t2362341588 * __this, int64_t ___crcValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::AddExtraDataAES(ICSharpCode.SharpZipLib.Zip.ZipEntry,ICSharpCode.SharpZipLib.Zip.ZipExtraData)
extern "C"  void ZipOutputStream_AddExtraDataAES_m3549287299 (Il2CppObject * __this /* static, unused */, ZipEntry_t3141689087 * ___entry0, ZipExtraData_t591052775 * ___extraData1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::WriteAESHeader(ICSharpCode.SharpZipLib.Zip.ZipEntry)
extern "C"  void ZipOutputStream_WriteAESHeader_m1992202379 (ZipOutputStream_t2362341588 * __this, ZipEntry_t3141689087 * ___entry0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C"  void ZipOutputStream_Write_m2297616389 (ZipOutputStream_t2362341588 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::CopyAndEncrypt(System.Byte[],System.Int32,System.Int32)
extern "C"  void ZipOutputStream_CopyAndEncrypt_m3482781577 (ZipOutputStream_t2362341588 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::Finish()
extern "C"  void ZipOutputStream_Finish_m3854812944 (ZipOutputStream_t2362341588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
