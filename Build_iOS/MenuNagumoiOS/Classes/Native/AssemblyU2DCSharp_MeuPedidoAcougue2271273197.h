﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<PedidoEnvioData>
struct List_1_t2677262856;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.UI.Text
struct Text_t9039225;
// System.Collections.Generic.List`1<PedidoData>
struct List_1_t516228991;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MeuPedidoAcougue
struct  MeuPedidoAcougue_t2271273197  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject MeuPedidoAcougue::content
	GameObject_t3674682005 * ___content_3;
	// UnityEngine.GameObject MeuPedidoAcougue::itemPrefab
	GameObject_t3674682005 * ___itemPrefab_4;
	// UnityEngine.GameObject MeuPedidoAcougue::loadingObj
	GameObject_t3674682005 * ___loadingObj_5;
	// UnityEngine.GameObject MeuPedidoAcougue::popup
	GameObject_t3674682005 * ___popup_6;
	// UnityEngine.GameObject MeuPedidoAcougue::pedidoRealizado
	GameObject_t3674682005 * ___pedidoRealizado_7;
	// UnityEngine.GameObject MeuPedidoAcougue::pedidoEmProducao
	GameObject_t3674682005 * ___pedidoEmProducao_8;
	// UnityEngine.GameObject MeuPedidoAcougue::pedidoPronto
	GameObject_t3674682005 * ___pedidoPronto_9;
	// UnityEngine.UI.Text MeuPedidoAcougue::textPosicao
	Text_t9039225 * ___textPosicao_10;
	// System.Collections.Generic.List`1<PedidoData> MeuPedidoAcougue::pedidosAvaliarLst
	List_1_t516228991 * ___pedidosAvaliarLst_11;

public:
	inline static int32_t get_offset_of_content_3() { return static_cast<int32_t>(offsetof(MeuPedidoAcougue_t2271273197, ___content_3)); }
	inline GameObject_t3674682005 * get_content_3() const { return ___content_3; }
	inline GameObject_t3674682005 ** get_address_of_content_3() { return &___content_3; }
	inline void set_content_3(GameObject_t3674682005 * value)
	{
		___content_3 = value;
		Il2CppCodeGenWriteBarrier(&___content_3, value);
	}

	inline static int32_t get_offset_of_itemPrefab_4() { return static_cast<int32_t>(offsetof(MeuPedidoAcougue_t2271273197, ___itemPrefab_4)); }
	inline GameObject_t3674682005 * get_itemPrefab_4() const { return ___itemPrefab_4; }
	inline GameObject_t3674682005 ** get_address_of_itemPrefab_4() { return &___itemPrefab_4; }
	inline void set_itemPrefab_4(GameObject_t3674682005 * value)
	{
		___itemPrefab_4 = value;
		Il2CppCodeGenWriteBarrier(&___itemPrefab_4, value);
	}

	inline static int32_t get_offset_of_loadingObj_5() { return static_cast<int32_t>(offsetof(MeuPedidoAcougue_t2271273197, ___loadingObj_5)); }
	inline GameObject_t3674682005 * get_loadingObj_5() const { return ___loadingObj_5; }
	inline GameObject_t3674682005 ** get_address_of_loadingObj_5() { return &___loadingObj_5; }
	inline void set_loadingObj_5(GameObject_t3674682005 * value)
	{
		___loadingObj_5 = value;
		Il2CppCodeGenWriteBarrier(&___loadingObj_5, value);
	}

	inline static int32_t get_offset_of_popup_6() { return static_cast<int32_t>(offsetof(MeuPedidoAcougue_t2271273197, ___popup_6)); }
	inline GameObject_t3674682005 * get_popup_6() const { return ___popup_6; }
	inline GameObject_t3674682005 ** get_address_of_popup_6() { return &___popup_6; }
	inline void set_popup_6(GameObject_t3674682005 * value)
	{
		___popup_6 = value;
		Il2CppCodeGenWriteBarrier(&___popup_6, value);
	}

	inline static int32_t get_offset_of_pedidoRealizado_7() { return static_cast<int32_t>(offsetof(MeuPedidoAcougue_t2271273197, ___pedidoRealizado_7)); }
	inline GameObject_t3674682005 * get_pedidoRealizado_7() const { return ___pedidoRealizado_7; }
	inline GameObject_t3674682005 ** get_address_of_pedidoRealizado_7() { return &___pedidoRealizado_7; }
	inline void set_pedidoRealizado_7(GameObject_t3674682005 * value)
	{
		___pedidoRealizado_7 = value;
		Il2CppCodeGenWriteBarrier(&___pedidoRealizado_7, value);
	}

	inline static int32_t get_offset_of_pedidoEmProducao_8() { return static_cast<int32_t>(offsetof(MeuPedidoAcougue_t2271273197, ___pedidoEmProducao_8)); }
	inline GameObject_t3674682005 * get_pedidoEmProducao_8() const { return ___pedidoEmProducao_8; }
	inline GameObject_t3674682005 ** get_address_of_pedidoEmProducao_8() { return &___pedidoEmProducao_8; }
	inline void set_pedidoEmProducao_8(GameObject_t3674682005 * value)
	{
		___pedidoEmProducao_8 = value;
		Il2CppCodeGenWriteBarrier(&___pedidoEmProducao_8, value);
	}

	inline static int32_t get_offset_of_pedidoPronto_9() { return static_cast<int32_t>(offsetof(MeuPedidoAcougue_t2271273197, ___pedidoPronto_9)); }
	inline GameObject_t3674682005 * get_pedidoPronto_9() const { return ___pedidoPronto_9; }
	inline GameObject_t3674682005 ** get_address_of_pedidoPronto_9() { return &___pedidoPronto_9; }
	inline void set_pedidoPronto_9(GameObject_t3674682005 * value)
	{
		___pedidoPronto_9 = value;
		Il2CppCodeGenWriteBarrier(&___pedidoPronto_9, value);
	}

	inline static int32_t get_offset_of_textPosicao_10() { return static_cast<int32_t>(offsetof(MeuPedidoAcougue_t2271273197, ___textPosicao_10)); }
	inline Text_t9039225 * get_textPosicao_10() const { return ___textPosicao_10; }
	inline Text_t9039225 ** get_address_of_textPosicao_10() { return &___textPosicao_10; }
	inline void set_textPosicao_10(Text_t9039225 * value)
	{
		___textPosicao_10 = value;
		Il2CppCodeGenWriteBarrier(&___textPosicao_10, value);
	}

	inline static int32_t get_offset_of_pedidosAvaliarLst_11() { return static_cast<int32_t>(offsetof(MeuPedidoAcougue_t2271273197, ___pedidosAvaliarLst_11)); }
	inline List_1_t516228991 * get_pedidosAvaliarLst_11() const { return ___pedidosAvaliarLst_11; }
	inline List_1_t516228991 ** get_address_of_pedidosAvaliarLst_11() { return &___pedidosAvaliarLst_11; }
	inline void set_pedidosAvaliarLst_11(List_1_t516228991 * value)
	{
		___pedidosAvaliarLst_11 = value;
		Il2CppCodeGenWriteBarrier(&___pedidosAvaliarLst_11, value);
	}
};

struct MeuPedidoAcougue_t2271273197_StaticFields
{
public:
	// System.Collections.Generic.List`1<PedidoEnvioData> MeuPedidoAcougue::pedidoEnvioDatasLst
	List_1_t2677262856 * ___pedidoEnvioDatasLst_2;

public:
	inline static int32_t get_offset_of_pedidoEnvioDatasLst_2() { return static_cast<int32_t>(offsetof(MeuPedidoAcougue_t2271273197_StaticFields, ___pedidoEnvioDatasLst_2)); }
	inline List_1_t2677262856 * get_pedidoEnvioDatasLst_2() const { return ___pedidoEnvioDatasLst_2; }
	inline List_1_t2677262856 ** get_address_of_pedidoEnvioDatasLst_2() { return &___pedidoEnvioDatasLst_2; }
	inline void set_pedidoEnvioDatasLst_2(List_1_t2677262856 * value)
	{
		___pedidoEnvioDatasLst_2 = value;
		Il2CppCodeGenWriteBarrier(&___pedidoEnvioDatasLst_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
