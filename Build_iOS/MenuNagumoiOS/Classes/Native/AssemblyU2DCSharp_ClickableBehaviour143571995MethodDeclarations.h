﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ClickableBehaviour
struct ClickableBehaviour_t143571995;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Ray3134616544.h"

// System.Void ClickableBehaviour::.ctor()
extern "C"  void ClickableBehaviour__ctor_m1110029024 (ClickableBehaviour_t143571995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClickableBehaviour::Start()
extern "C"  void ClickableBehaviour_Start_m57166816 (ClickableBehaviour_t143571995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClickableBehaviour::OpenLink()
extern "C"  void ClickableBehaviour_OpenLink_m4013752648 (ClickableBehaviour_t143571995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClickableBehaviour::checkClick()
extern "C"  void ClickableBehaviour_checkClick_m1585049956 (ClickableBehaviour_t143571995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClickableBehaviour::UnlockClick()
extern "C"  void ClickableBehaviour_UnlockClick_m1058172386 (ClickableBehaviour_t143571995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Ray ClickableBehaviour::getRay()
extern "C"  Ray_t3134616544  ClickableBehaviour_getRay_m2051871320 (ClickableBehaviour_t143571995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClickableBehaviour::LateUpdate()
extern "C"  void ClickableBehaviour_LateUpdate_m1697145715 (ClickableBehaviour_t143571995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
