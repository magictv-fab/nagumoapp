﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen973669728.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_An2191327052.h"

// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3694985181_gshared (InternalEnumerator_1_t973669728 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3694985181(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t973669728 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3694985181_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m969991843_gshared (InternalEnumerator_1_t973669728 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m969991843(__this, method) ((  void (*) (InternalEnumerator_1_t973669728 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m969991843_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1319196367_gshared (InternalEnumerator_1_t973669728 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1319196367(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t973669728 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1319196367_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2669824500_gshared (InternalEnumerator_1_t973669728 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2669824500(__this, method) ((  void (*) (InternalEnumerator_1_t973669728 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2669824500_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2708343183_gshared (InternalEnumerator_1_t973669728 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2708343183(__this, method) ((  bool (*) (InternalEnumerator_1_t973669728 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2708343183_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m3061040996_gshared (InternalEnumerator_1_t973669728 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3061040996(__this, method) ((  int32_t (*) (InternalEnumerator_1_t973669728 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3061040996_gshared)(__this, method)
