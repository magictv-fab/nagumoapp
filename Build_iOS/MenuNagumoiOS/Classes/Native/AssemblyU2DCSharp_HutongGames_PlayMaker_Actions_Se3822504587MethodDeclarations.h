﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAnimatorPlayBackSpeed
struct SetAnimatorPlayBackSpeed_t3822504587;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAnimatorPlayBackSpeed::.ctor()
extern "C"  void SetAnimatorPlayBackSpeed__ctor_m2169252923 (SetAnimatorPlayBackSpeed_t3822504587 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorPlayBackSpeed::Reset()
extern "C"  void SetAnimatorPlayBackSpeed_Reset_m4110653160 (SetAnimatorPlayBackSpeed_t3822504587 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorPlayBackSpeed::OnEnter()
extern "C"  void SetAnimatorPlayBackSpeed_OnEnter_m984649042 (SetAnimatorPlayBackSpeed_t3822504587 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorPlayBackSpeed::OnUpdate()
extern "C"  void SetAnimatorPlayBackSpeed_OnUpdate_m3887875889 (SetAnimatorPlayBackSpeed_t3822504587 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorPlayBackSpeed::DoPlayBackSpeed()
extern "C"  void SetAnimatorPlayBackSpeed_DoPlayBackSpeed_m2323126042 (SetAnimatorPlayBackSpeed_t3822504587 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
