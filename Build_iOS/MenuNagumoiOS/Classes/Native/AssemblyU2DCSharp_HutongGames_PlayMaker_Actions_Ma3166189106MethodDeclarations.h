﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.MasterServerGetProperties
struct MasterServerGetProperties_t3166189106;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.MasterServerGetProperties::.ctor()
extern "C"  void MasterServerGetProperties__ctor_m384751428 (MasterServerGetProperties_t3166189106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerGetProperties::Reset()
extern "C"  void MasterServerGetProperties_Reset_m2326151665 (MasterServerGetProperties_t3166189106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerGetProperties::OnEnter()
extern "C"  void MasterServerGetProperties_OnEnter_m4065630747 (MasterServerGetProperties_t3166189106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerGetProperties::GetMasterServerProperties()
extern "C"  void MasterServerGetProperties_GetMasterServerProperties_m20900016 (MasterServerGetProperties_t3166189106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
