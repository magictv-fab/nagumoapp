﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// MagicTV.download.BulkBundleDownloader/OnFileDonwloadCompleteEventHandler
struct OnFileDonwloadCompleteEventHandler_t3421606269;
// System.Collections.Generic.List`1<MagicTV.vo.BundleVO>
struct List_1_t3352703625;
// System.Collections.Generic.List`1<MagicTV.download.BundleDownloadItem>
struct List_1_t2084123921;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// MagicTV.download.BulkBundleDownloader
struct BulkBundleDownloader_t2985631149;
// ARM.utils.download.GenericDownloadManager
struct GenericDownloadManager_t3298305714;

#include "AssemblyU2DCSharp_ARM_abstracts_ProgressEventsAbst2129719228.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.download.BulkBundleDownloader
struct  BulkBundleDownloader_t2985631149  : public ProgressEventsAbstract_t2129719228
{
public:
	// MagicTV.download.BulkBundleDownloader/OnFileDonwloadCompleteEventHandler MagicTV.download.BulkBundleDownloader::onFileDownloadComplete
	OnFileDonwloadCompleteEventHandler_t3421606269 * ___onFileDownloadComplete_16;
	// System.Collections.Generic.List`1<MagicTV.vo.BundleVO> MagicTV.download.BulkBundleDownloader::_bundles
	List_1_t3352703625 * ____bundles_17;
	// System.Int32 MagicTV.download.BulkBundleDownloader::_currentBundle
	int32_t ____currentBundle_18;
	// System.Single MagicTV.download.BulkBundleDownloader::_bulkTotalProgress
	float ____bulkTotalProgress_19;
	// System.Collections.Generic.List`1<MagicTV.download.BundleDownloadItem> MagicTV.download.BulkBundleDownloader::_itensToDownload
	List_1_t2084123921 * ____itensToDownload_20;

public:
	inline static int32_t get_offset_of_onFileDownloadComplete_16() { return static_cast<int32_t>(offsetof(BulkBundleDownloader_t2985631149, ___onFileDownloadComplete_16)); }
	inline OnFileDonwloadCompleteEventHandler_t3421606269 * get_onFileDownloadComplete_16() const { return ___onFileDownloadComplete_16; }
	inline OnFileDonwloadCompleteEventHandler_t3421606269 ** get_address_of_onFileDownloadComplete_16() { return &___onFileDownloadComplete_16; }
	inline void set_onFileDownloadComplete_16(OnFileDonwloadCompleteEventHandler_t3421606269 * value)
	{
		___onFileDownloadComplete_16 = value;
		Il2CppCodeGenWriteBarrier(&___onFileDownloadComplete_16, value);
	}

	inline static int32_t get_offset_of__bundles_17() { return static_cast<int32_t>(offsetof(BulkBundleDownloader_t2985631149, ____bundles_17)); }
	inline List_1_t3352703625 * get__bundles_17() const { return ____bundles_17; }
	inline List_1_t3352703625 ** get_address_of__bundles_17() { return &____bundles_17; }
	inline void set__bundles_17(List_1_t3352703625 * value)
	{
		____bundles_17 = value;
		Il2CppCodeGenWriteBarrier(&____bundles_17, value);
	}

	inline static int32_t get_offset_of__currentBundle_18() { return static_cast<int32_t>(offsetof(BulkBundleDownloader_t2985631149, ____currentBundle_18)); }
	inline int32_t get__currentBundle_18() const { return ____currentBundle_18; }
	inline int32_t* get_address_of__currentBundle_18() { return &____currentBundle_18; }
	inline void set__currentBundle_18(int32_t value)
	{
		____currentBundle_18 = value;
	}

	inline static int32_t get_offset_of__bulkTotalProgress_19() { return static_cast<int32_t>(offsetof(BulkBundleDownloader_t2985631149, ____bulkTotalProgress_19)); }
	inline float get__bulkTotalProgress_19() const { return ____bulkTotalProgress_19; }
	inline float* get_address_of__bulkTotalProgress_19() { return &____bulkTotalProgress_19; }
	inline void set__bulkTotalProgress_19(float value)
	{
		____bulkTotalProgress_19 = value;
	}

	inline static int32_t get_offset_of__itensToDownload_20() { return static_cast<int32_t>(offsetof(BulkBundleDownloader_t2985631149, ____itensToDownload_20)); }
	inline List_1_t2084123921 * get__itensToDownload_20() const { return ____itensToDownload_20; }
	inline List_1_t2084123921 ** get_address_of__itensToDownload_20() { return &____itensToDownload_20; }
	inline void set__itensToDownload_20(List_1_t2084123921 * value)
	{
		____itensToDownload_20 = value;
		Il2CppCodeGenWriteBarrier(&____itensToDownload_20, value);
	}
};

struct BulkBundleDownloader_t2985631149_StaticFields
{
public:
	// System.Boolean MagicTV.download.BulkBundleDownloader::_cancelBulk
	bool ____cancelBulk_21;
	// System.Int32 MagicTV.download.BulkBundleDownloader::alert_i
	int32_t ___alert_i_22;
	// System.Int32 MagicTV.download.BulkBundleDownloader::alert_a
	int32_t ___alert_a_23;
	// UnityEngine.GameObject MagicTV.download.BulkBundleDownloader::_currentBulkBundleDownloaderGO
	GameObject_t3674682005 * ____currentBulkBundleDownloaderGO_24;
	// MagicTV.download.BulkBundleDownloader MagicTV.download.BulkBundleDownloader::_instanceBulkBundleDownloader
	BulkBundleDownloader_t2985631149 * ____instanceBulkBundleDownloader_25;
	// UnityEngine.GameObject MagicTV.download.BulkBundleDownloader::_currentGerericDonwloadManagerGO
	GameObject_t3674682005 * ____currentGerericDonwloadManagerGO_26;
	// ARM.utils.download.GenericDownloadManager MagicTV.download.BulkBundleDownloader::_instanceGenericDownload
	GenericDownloadManager_t3298305714 * ____instanceGenericDownload_27;

public:
	inline static int32_t get_offset_of__cancelBulk_21() { return static_cast<int32_t>(offsetof(BulkBundleDownloader_t2985631149_StaticFields, ____cancelBulk_21)); }
	inline bool get__cancelBulk_21() const { return ____cancelBulk_21; }
	inline bool* get_address_of__cancelBulk_21() { return &____cancelBulk_21; }
	inline void set__cancelBulk_21(bool value)
	{
		____cancelBulk_21 = value;
	}

	inline static int32_t get_offset_of_alert_i_22() { return static_cast<int32_t>(offsetof(BulkBundleDownloader_t2985631149_StaticFields, ___alert_i_22)); }
	inline int32_t get_alert_i_22() const { return ___alert_i_22; }
	inline int32_t* get_address_of_alert_i_22() { return &___alert_i_22; }
	inline void set_alert_i_22(int32_t value)
	{
		___alert_i_22 = value;
	}

	inline static int32_t get_offset_of_alert_a_23() { return static_cast<int32_t>(offsetof(BulkBundleDownloader_t2985631149_StaticFields, ___alert_a_23)); }
	inline int32_t get_alert_a_23() const { return ___alert_a_23; }
	inline int32_t* get_address_of_alert_a_23() { return &___alert_a_23; }
	inline void set_alert_a_23(int32_t value)
	{
		___alert_a_23 = value;
	}

	inline static int32_t get_offset_of__currentBulkBundleDownloaderGO_24() { return static_cast<int32_t>(offsetof(BulkBundleDownloader_t2985631149_StaticFields, ____currentBulkBundleDownloaderGO_24)); }
	inline GameObject_t3674682005 * get__currentBulkBundleDownloaderGO_24() const { return ____currentBulkBundleDownloaderGO_24; }
	inline GameObject_t3674682005 ** get_address_of__currentBulkBundleDownloaderGO_24() { return &____currentBulkBundleDownloaderGO_24; }
	inline void set__currentBulkBundleDownloaderGO_24(GameObject_t3674682005 * value)
	{
		____currentBulkBundleDownloaderGO_24 = value;
		Il2CppCodeGenWriteBarrier(&____currentBulkBundleDownloaderGO_24, value);
	}

	inline static int32_t get_offset_of__instanceBulkBundleDownloader_25() { return static_cast<int32_t>(offsetof(BulkBundleDownloader_t2985631149_StaticFields, ____instanceBulkBundleDownloader_25)); }
	inline BulkBundleDownloader_t2985631149 * get__instanceBulkBundleDownloader_25() const { return ____instanceBulkBundleDownloader_25; }
	inline BulkBundleDownloader_t2985631149 ** get_address_of__instanceBulkBundleDownloader_25() { return &____instanceBulkBundleDownloader_25; }
	inline void set__instanceBulkBundleDownloader_25(BulkBundleDownloader_t2985631149 * value)
	{
		____instanceBulkBundleDownloader_25 = value;
		Il2CppCodeGenWriteBarrier(&____instanceBulkBundleDownloader_25, value);
	}

	inline static int32_t get_offset_of__currentGerericDonwloadManagerGO_26() { return static_cast<int32_t>(offsetof(BulkBundleDownloader_t2985631149_StaticFields, ____currentGerericDonwloadManagerGO_26)); }
	inline GameObject_t3674682005 * get__currentGerericDonwloadManagerGO_26() const { return ____currentGerericDonwloadManagerGO_26; }
	inline GameObject_t3674682005 ** get_address_of__currentGerericDonwloadManagerGO_26() { return &____currentGerericDonwloadManagerGO_26; }
	inline void set__currentGerericDonwloadManagerGO_26(GameObject_t3674682005 * value)
	{
		____currentGerericDonwloadManagerGO_26 = value;
		Il2CppCodeGenWriteBarrier(&____currentGerericDonwloadManagerGO_26, value);
	}

	inline static int32_t get_offset_of__instanceGenericDownload_27() { return static_cast<int32_t>(offsetof(BulkBundleDownloader_t2985631149_StaticFields, ____instanceGenericDownload_27)); }
	inline GenericDownloadManager_t3298305714 * get__instanceGenericDownload_27() const { return ____instanceGenericDownload_27; }
	inline GenericDownloadManager_t3298305714 ** get_address_of__instanceGenericDownload_27() { return &____instanceGenericDownload_27; }
	inline void set__instanceGenericDownload_27(GenericDownloadManager_t3298305714 * value)
	{
		____instanceGenericDownload_27 = value;
		Il2CppCodeGenWriteBarrier(&____instanceGenericDownload_27, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
