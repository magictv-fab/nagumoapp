﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// DeviceCamera
struct DeviceCamera_t3055454523;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeviceCameraController
struct  DeviceCameraController_t2043460407  : public MonoBehaviour_t667441552
{
public:
	// DeviceCamera DeviceCameraController::webcam
	DeviceCamera_t3055454523 * ___webcam_2;
	// UnityEngine.GameObject DeviceCameraController::e_CameraPlaneObj
	GameObject_t3674682005 * ___e_CameraPlaneObj_3;
	// System.Boolean DeviceCameraController::isCorrected
	bool ___isCorrected_4;
	// System.Single DeviceCameraController::screenVideoRatio
	float ___screenVideoRatio_5;
	// System.Boolean DeviceCameraController::isUseEasyWebCam
	bool ___isUseEasyWebCam_6;

public:
	inline static int32_t get_offset_of_webcam_2() { return static_cast<int32_t>(offsetof(DeviceCameraController_t2043460407, ___webcam_2)); }
	inline DeviceCamera_t3055454523 * get_webcam_2() const { return ___webcam_2; }
	inline DeviceCamera_t3055454523 ** get_address_of_webcam_2() { return &___webcam_2; }
	inline void set_webcam_2(DeviceCamera_t3055454523 * value)
	{
		___webcam_2 = value;
		Il2CppCodeGenWriteBarrier(&___webcam_2, value);
	}

	inline static int32_t get_offset_of_e_CameraPlaneObj_3() { return static_cast<int32_t>(offsetof(DeviceCameraController_t2043460407, ___e_CameraPlaneObj_3)); }
	inline GameObject_t3674682005 * get_e_CameraPlaneObj_3() const { return ___e_CameraPlaneObj_3; }
	inline GameObject_t3674682005 ** get_address_of_e_CameraPlaneObj_3() { return &___e_CameraPlaneObj_3; }
	inline void set_e_CameraPlaneObj_3(GameObject_t3674682005 * value)
	{
		___e_CameraPlaneObj_3 = value;
		Il2CppCodeGenWriteBarrier(&___e_CameraPlaneObj_3, value);
	}

	inline static int32_t get_offset_of_isCorrected_4() { return static_cast<int32_t>(offsetof(DeviceCameraController_t2043460407, ___isCorrected_4)); }
	inline bool get_isCorrected_4() const { return ___isCorrected_4; }
	inline bool* get_address_of_isCorrected_4() { return &___isCorrected_4; }
	inline void set_isCorrected_4(bool value)
	{
		___isCorrected_4 = value;
	}

	inline static int32_t get_offset_of_screenVideoRatio_5() { return static_cast<int32_t>(offsetof(DeviceCameraController_t2043460407, ___screenVideoRatio_5)); }
	inline float get_screenVideoRatio_5() const { return ___screenVideoRatio_5; }
	inline float* get_address_of_screenVideoRatio_5() { return &___screenVideoRatio_5; }
	inline void set_screenVideoRatio_5(float value)
	{
		___screenVideoRatio_5 = value;
	}

	inline static int32_t get_offset_of_isUseEasyWebCam_6() { return static_cast<int32_t>(offsetof(DeviceCameraController_t2043460407, ___isUseEasyWebCam_6)); }
	inline bool get_isUseEasyWebCam_6() const { return ___isUseEasyWebCam_6; }
	inline bool* get_address_of_isUseEasyWebCam_6() { return &___isUseEasyWebCam_6; }
	inline void set_isUseEasyWebCam_6(bool value)
	{
		___isUseEasyWebCam_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
