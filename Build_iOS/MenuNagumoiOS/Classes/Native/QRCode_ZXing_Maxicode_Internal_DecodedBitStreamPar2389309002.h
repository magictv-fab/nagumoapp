﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t4054002952;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Maxicode.Internal.DecodedBitStreamParser
struct  DecodedBitStreamParser_t2389309002  : public Il2CppObject
{
public:

public:
};

struct DecodedBitStreamParser_t2389309002_StaticFields
{
public:
	// System.String[] ZXing.Maxicode.Internal.DecodedBitStreamParser::SETS
	StringU5BU5D_t4054002952* ___SETS_0;

public:
	inline static int32_t get_offset_of_SETS_0() { return static_cast<int32_t>(offsetof(DecodedBitStreamParser_t2389309002_StaticFields, ___SETS_0)); }
	inline StringU5BU5D_t4054002952* get_SETS_0() const { return ___SETS_0; }
	inline StringU5BU5D_t4054002952** get_address_of_SETS_0() { return &___SETS_0; }
	inline void set_SETS_0(StringU5BU5D_t4054002952* value)
	{
		___SETS_0 = value;
		Il2CppCodeGenWriteBarrier(&___SETS_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
