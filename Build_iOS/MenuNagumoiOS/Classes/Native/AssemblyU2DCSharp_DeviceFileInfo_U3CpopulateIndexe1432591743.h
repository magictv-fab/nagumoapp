﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MagicTV.vo.ResultRevisionVO
struct ResultRevisionVO_t2445597391;
// DeviceFileInfo
struct DeviceFileInfo_t2242857184;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeviceFileInfo/<populateIndexedBundles>c__AnonStorey98
struct  U3CpopulateIndexedBundlesU3Ec__AnonStorey98_t1432591743  : public Il2CppObject
{
public:
	// MagicTV.vo.ResultRevisionVO DeviceFileInfo/<populateIndexedBundles>c__AnonStorey98::revision
	ResultRevisionVO_t2445597391 * ___revision_0;
	// DeviceFileInfo DeviceFileInfo/<populateIndexedBundles>c__AnonStorey98::<>f__this
	DeviceFileInfo_t2242857184 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_revision_0() { return static_cast<int32_t>(offsetof(U3CpopulateIndexedBundlesU3Ec__AnonStorey98_t1432591743, ___revision_0)); }
	inline ResultRevisionVO_t2445597391 * get_revision_0() const { return ___revision_0; }
	inline ResultRevisionVO_t2445597391 ** get_address_of_revision_0() { return &___revision_0; }
	inline void set_revision_0(ResultRevisionVO_t2445597391 * value)
	{
		___revision_0 = value;
		Il2CppCodeGenWriteBarrier(&___revision_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CpopulateIndexedBundlesU3Ec__AnonStorey98_t1432591743, ___U3CU3Ef__this_1)); }
	inline DeviceFileInfo_t2242857184 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline DeviceFileInfo_t2242857184 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(DeviceFileInfo_t2242857184 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
