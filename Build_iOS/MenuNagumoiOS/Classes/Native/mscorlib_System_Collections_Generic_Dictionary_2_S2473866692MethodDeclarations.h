﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.Aztec.Internal.Decoder/Table,System.Object>
struct ShimEnumerator_t2473866692;
// System.Collections.Generic.Dictionary`2<ZXing.Aztec.Internal.Decoder/Table,System.Object>
struct Dictionary_2_t2758088665;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.Aztec.Internal.Decoder/Table,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m898693817_gshared (ShimEnumerator_t2473866692 * __this, Dictionary_2_t2758088665 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m898693817(__this, ___host0, method) ((  void (*) (ShimEnumerator_t2473866692 *, Dictionary_2_t2758088665 *, const MethodInfo*))ShimEnumerator__ctor_m898693817_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.Aztec.Internal.Decoder/Table,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m276639116_gshared (ShimEnumerator_t2473866692 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m276639116(__this, method) ((  bool (*) (ShimEnumerator_t2473866692 *, const MethodInfo*))ShimEnumerator_MoveNext_m276639116_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.Aztec.Internal.Decoder/Table,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m513135326_gshared (ShimEnumerator_t2473866692 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m513135326(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t2473866692 *, const MethodInfo*))ShimEnumerator_get_Entry_m513135326_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.Aztec.Internal.Decoder/Table,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3163757341_gshared (ShimEnumerator_t2473866692 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m3163757341(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2473866692 *, const MethodInfo*))ShimEnumerator_get_Key_m3163757341_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.Aztec.Internal.Decoder/Table,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m582395503_gshared (ShimEnumerator_t2473866692 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m582395503(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2473866692 *, const MethodInfo*))ShimEnumerator_get_Value_m582395503_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.Aztec.Internal.Decoder/Table,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2761020727_gshared (ShimEnumerator_t2473866692 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m2761020727(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2473866692 *, const MethodInfo*))ShimEnumerator_get_Current_m2761020727_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<ZXing.Aztec.Internal.Decoder/Table,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m3467939723_gshared (ShimEnumerator_t2473866692 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m3467939723(__this, method) ((  void (*) (ShimEnumerator_t2473866692 *, const MethodInfo*))ShimEnumerator_Reset_m3467939723_gshared)(__this, method)
