﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[][]
struct Int32U5BU5DU5BU5D_t1820556512;

#include "QRCode_ZXing_OneD_OneDReader3436042911.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.Code128Reader
struct  Code128Reader_t1206129631  : public OneDReader_t3436042911
{
public:

public:
};

struct Code128Reader_t1206129631_StaticFields
{
public:
	// System.Int32[][] ZXing.OneD.Code128Reader::CODE_PATTERNS
	Int32U5BU5DU5BU5D_t1820556512* ___CODE_PATTERNS_2;
	// System.Int32 ZXing.OneD.Code128Reader::MAX_AVG_VARIANCE
	int32_t ___MAX_AVG_VARIANCE_3;
	// System.Int32 ZXing.OneD.Code128Reader::MAX_INDIVIDUAL_VARIANCE
	int32_t ___MAX_INDIVIDUAL_VARIANCE_4;

public:
	inline static int32_t get_offset_of_CODE_PATTERNS_2() { return static_cast<int32_t>(offsetof(Code128Reader_t1206129631_StaticFields, ___CODE_PATTERNS_2)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_CODE_PATTERNS_2() const { return ___CODE_PATTERNS_2; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_CODE_PATTERNS_2() { return &___CODE_PATTERNS_2; }
	inline void set_CODE_PATTERNS_2(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___CODE_PATTERNS_2 = value;
		Il2CppCodeGenWriteBarrier(&___CODE_PATTERNS_2, value);
	}

	inline static int32_t get_offset_of_MAX_AVG_VARIANCE_3() { return static_cast<int32_t>(offsetof(Code128Reader_t1206129631_StaticFields, ___MAX_AVG_VARIANCE_3)); }
	inline int32_t get_MAX_AVG_VARIANCE_3() const { return ___MAX_AVG_VARIANCE_3; }
	inline int32_t* get_address_of_MAX_AVG_VARIANCE_3() { return &___MAX_AVG_VARIANCE_3; }
	inline void set_MAX_AVG_VARIANCE_3(int32_t value)
	{
		___MAX_AVG_VARIANCE_3 = value;
	}

	inline static int32_t get_offset_of_MAX_INDIVIDUAL_VARIANCE_4() { return static_cast<int32_t>(offsetof(Code128Reader_t1206129631_StaticFields, ___MAX_INDIVIDUAL_VARIANCE_4)); }
	inline int32_t get_MAX_INDIVIDUAL_VARIANCE_4() const { return ___MAX_INDIVIDUAL_VARIANCE_4; }
	inline int32_t* get_address_of_MAX_INDIVIDUAL_VARIANCE_4() { return &___MAX_INDIVIDUAL_VARIANCE_4; }
	inline void set_MAX_INDIVIDUAL_VARIANCE_4(int32_t value)
	{
		___MAX_INDIVIDUAL_VARIANCE_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
