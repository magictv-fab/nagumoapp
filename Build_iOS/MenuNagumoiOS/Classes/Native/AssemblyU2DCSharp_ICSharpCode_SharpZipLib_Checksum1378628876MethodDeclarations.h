﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Checksums.StrangeCRC
struct StrangeCRC_t1378628876;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"

// System.Void ICSharpCode.SharpZipLib.Checksums.StrangeCRC::.ctor()
extern "C"  void StrangeCRC__ctor_m1051986100 (StrangeCRC_t1378628876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Checksums.StrangeCRC::.cctor()
extern "C"  void StrangeCRC__cctor_m2064701817 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Checksums.StrangeCRC::Reset()
extern "C"  void StrangeCRC_Reset_m2993386337 (StrangeCRC_t1378628876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Checksums.StrangeCRC::get_Value()
extern "C"  int64_t StrangeCRC_get_Value_m3080610795 (StrangeCRC_t1378628876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Checksums.StrangeCRC::Update(System.Int32)
extern "C"  void StrangeCRC_Update_m2701727914 (StrangeCRC_t1378628876 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Checksums.StrangeCRC::Update(System.Byte[])
extern "C"  void StrangeCRC_Update_m548079504 (StrangeCRC_t1378628876 * __this, ByteU5BU5D_t4260760469* ___buffer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Checksums.StrangeCRC::Update(System.Byte[],System.Int32,System.Int32)
extern "C"  void StrangeCRC_Update_m2854979824 (StrangeCRC_t1378628876 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
