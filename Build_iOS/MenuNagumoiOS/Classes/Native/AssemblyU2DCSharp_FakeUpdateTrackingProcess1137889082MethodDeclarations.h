﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FakeUpdateTrackingProcess
struct FakeUpdateTrackingProcess_t1137889082;

#include "codegen/il2cpp-codegen.h"

// System.Void FakeUpdateTrackingProcess::.ctor()
extern "C"  void FakeUpdateTrackingProcess__ctor_m3221092209 (FakeUpdateTrackingProcess_t1137889082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FakeUpdateTrackingProcess::prepare()
extern "C"  void FakeUpdateTrackingProcess_prepare_m4020140150 (FakeUpdateTrackingProcess_t1137889082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FakeUpdateTrackingProcess::Play()
extern "C"  void FakeUpdateTrackingProcess_Play_m2193428967 (FakeUpdateTrackingProcess_t1137889082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FakeUpdateTrackingProcess::Stop()
extern "C"  void FakeUpdateTrackingProcess_Stop_m2287113013 (FakeUpdateTrackingProcess_t1137889082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FakeUpdateTrackingProcess::Pause()
extern "C"  void FakeUpdateTrackingProcess_Pause_m3275218181 (FakeUpdateTrackingProcess_t1137889082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
