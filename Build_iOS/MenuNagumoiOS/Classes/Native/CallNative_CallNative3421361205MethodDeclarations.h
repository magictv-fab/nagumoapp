﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Int32 CallNative::_SaveImage(System.String,System.Boolean,System.String)
extern "C"  int32_t CallNative__SaveImage_m175990345 (Il2CppObject * __this /* static, unused */, String_t* ___str0, bool ___move1, String_t* ___albumName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CallNative::_SaveVideo(System.String,System.Boolean,System.String)
extern "C"  int32_t CallNative__SaveVideo_m353804649 (Il2CppObject * __this /* static, unused */, String_t* ___str0, bool ___move1, String_t* ___albumName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CallNative::CopyImageToCameraRoll(System.String,System.String)
extern "C"  void CallNative_CopyImageToCameraRoll_m2433959120 (Il2CppObject * __this /* static, unused */, String_t* ___path0, String_t* ___albumName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CallNative::MoveImageToCameraRoll(System.String,System.String)
extern "C"  void CallNative_MoveImageToCameraRoll_m786993196 (Il2CppObject * __this /* static, unused */, String_t* ___path0, String_t* ___albumName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CallNative::CopyVideoToCameraRoll(System.String,System.String)
extern "C"  void CallNative_CopyVideoToCameraRoll_m1507384752 (Il2CppObject * __this /* static, unused */, String_t* ___path0, String_t* ___albumName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CallNative::MoveVideoToCameraRoll(System.String,System.String)
extern "C"  void CallNative_MoveVideoToCameraRoll_m4155386124 (Il2CppObject * __this /* static, unused */, String_t* ___path0, String_t* ___albumName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
