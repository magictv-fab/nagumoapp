﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.utils.request.ARMRequestEvents
struct ARMRequestEvents_t2525848346;
// System.String
struct String_t;
// ARM.utils.request.ARMSingleRequest/OnStartErrorEventHandler
struct OnStartErrorEventHandler_t726677771;
// ARM.utils.request.ARMRequestVO
struct ARMRequestVO_t2431191322;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.utils.request.ARMSingleRequest
struct  ARMSingleRequest_t410726329  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean ARM.utils.request.ARMSingleRequest::_isComplete
	bool ____isComplete_2;
	// ARM.utils.request.ARMRequestEvents ARM.utils.request.ARMSingleRequest::Events
	ARMRequestEvents_t2525848346 * ___Events_3;
	// System.String ARM.utils.request.ARMSingleRequest::log_url
	String_t* ___log_url_4;
	// System.Single ARM.utils.request.ARMSingleRequest::log_progress
	float ___log_progress_5;
	// System.Boolean ARM.utils.request.ARMSingleRequest::_isPrepared
	bool ____isPrepared_6;
	// System.Boolean ARM.utils.request.ARMSingleRequest::_started
	bool ____started_7;
	// ARM.utils.request.ARMSingleRequest/OnStartErrorEventHandler ARM.utils.request.ARMSingleRequest::onStartError
	OnStartErrorEventHandler_t726677771 * ___onStartError_8;
	// ARM.utils.request.ARMRequestVO ARM.utils.request.ARMSingleRequest::VO
	ARMRequestVO_t2431191322 * ___VO_9;

public:
	inline static int32_t get_offset_of__isComplete_2() { return static_cast<int32_t>(offsetof(ARMSingleRequest_t410726329, ____isComplete_2)); }
	inline bool get__isComplete_2() const { return ____isComplete_2; }
	inline bool* get_address_of__isComplete_2() { return &____isComplete_2; }
	inline void set__isComplete_2(bool value)
	{
		____isComplete_2 = value;
	}

	inline static int32_t get_offset_of_Events_3() { return static_cast<int32_t>(offsetof(ARMSingleRequest_t410726329, ___Events_3)); }
	inline ARMRequestEvents_t2525848346 * get_Events_3() const { return ___Events_3; }
	inline ARMRequestEvents_t2525848346 ** get_address_of_Events_3() { return &___Events_3; }
	inline void set_Events_3(ARMRequestEvents_t2525848346 * value)
	{
		___Events_3 = value;
		Il2CppCodeGenWriteBarrier(&___Events_3, value);
	}

	inline static int32_t get_offset_of_log_url_4() { return static_cast<int32_t>(offsetof(ARMSingleRequest_t410726329, ___log_url_4)); }
	inline String_t* get_log_url_4() const { return ___log_url_4; }
	inline String_t** get_address_of_log_url_4() { return &___log_url_4; }
	inline void set_log_url_4(String_t* value)
	{
		___log_url_4 = value;
		Il2CppCodeGenWriteBarrier(&___log_url_4, value);
	}

	inline static int32_t get_offset_of_log_progress_5() { return static_cast<int32_t>(offsetof(ARMSingleRequest_t410726329, ___log_progress_5)); }
	inline float get_log_progress_5() const { return ___log_progress_5; }
	inline float* get_address_of_log_progress_5() { return &___log_progress_5; }
	inline void set_log_progress_5(float value)
	{
		___log_progress_5 = value;
	}

	inline static int32_t get_offset_of__isPrepared_6() { return static_cast<int32_t>(offsetof(ARMSingleRequest_t410726329, ____isPrepared_6)); }
	inline bool get__isPrepared_6() const { return ____isPrepared_6; }
	inline bool* get_address_of__isPrepared_6() { return &____isPrepared_6; }
	inline void set__isPrepared_6(bool value)
	{
		____isPrepared_6 = value;
	}

	inline static int32_t get_offset_of__started_7() { return static_cast<int32_t>(offsetof(ARMSingleRequest_t410726329, ____started_7)); }
	inline bool get__started_7() const { return ____started_7; }
	inline bool* get_address_of__started_7() { return &____started_7; }
	inline void set__started_7(bool value)
	{
		____started_7 = value;
	}

	inline static int32_t get_offset_of_onStartError_8() { return static_cast<int32_t>(offsetof(ARMSingleRequest_t410726329, ___onStartError_8)); }
	inline OnStartErrorEventHandler_t726677771 * get_onStartError_8() const { return ___onStartError_8; }
	inline OnStartErrorEventHandler_t726677771 ** get_address_of_onStartError_8() { return &___onStartError_8; }
	inline void set_onStartError_8(OnStartErrorEventHandler_t726677771 * value)
	{
		___onStartError_8 = value;
		Il2CppCodeGenWriteBarrier(&___onStartError_8, value);
	}

	inline static int32_t get_offset_of_VO_9() { return static_cast<int32_t>(offsetof(ARMSingleRequest_t410726329, ___VO_9)); }
	inline ARMRequestVO_t2431191322 * get_VO_9() const { return ___VO_9; }
	inline ARMRequestVO_t2431191322 ** get_address_of_VO_9() { return &___VO_9; }
	inline void set_VO_9(ARMRequestVO_t2431191322 * value)
	{
		___VO_9 = value;
		Il2CppCodeGenWriteBarrier(&___VO_9, value);
	}
};

struct ARMSingleRequest_t410726329_StaticFields
{
public:
	// System.Int32 ARM.utils.request.ARMSingleRequest::_instanceID
	int32_t ____instanceID_10;

public:
	inline static int32_t get_offset_of__instanceID_10() { return static_cast<int32_t>(offsetof(ARMSingleRequest_t410726329_StaticFields, ____instanceID_10)); }
	inline int32_t get__instanceID_10() const { return ____instanceID_10; }
	inline int32_t* get_address_of__instanceID_10() { return &____instanceID_10; }
	inline void set__instanceID_10(int32_t value)
	{
		____instanceID_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
