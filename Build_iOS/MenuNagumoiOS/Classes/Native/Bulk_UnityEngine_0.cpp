﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// AOT.MonoPInvokeCallbackAttribute
struct MonoPInvokeCallbackAttribute_t1825429660;
// System.Type
struct Type_t;
// UnityEngine.AndroidJavaRunnable
struct AndroidJavaRunnable_t1289602340;
// UnityEngine.jvalue[]
struct jvalueU5BU5D_t1723627146;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Array
struct Il2CppArray;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t2362096582;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t813859935;
// UnityEngine.AndroidJavaClass
struct AndroidJavaClass_t1816259147;
// UnityEngine.AndroidJavaException
struct AndroidJavaException_t125722146;
// UnityEngine.AndroidJavaProxy
struct AndroidJavaProxy_t1828457281;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;
// UnityEngine.AndroidJavaRunnableProxy
struct AndroidJavaRunnableProxy_t1013435428;
// System.Boolean[]
struct BooleanU5BU5D_t3456302923;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Char[]
struct CharU5BU5D_t3324145743;
// System.Int16[]
struct Int16U5BU5D_t801762735;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Int64[]
struct Int64U5BU5D_t2174042770;
// System.Single[]
struct SingleU5BU5D_t2316563989;
// System.Double[]
struct DoubleU5BU5D_t2145413704;
// System.IntPtr[]
struct IntPtrU5BU5D_t3228729122;
// UnityEngine.Animation
struct Animation_t1724966010;
// UnityEngine.AnimationClip
struct AnimationClip_t2007702890;
// UnityEngine.AnimationState
struct AnimationState_t3682323633;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.Animation/Enumerator
struct Enumerator_t1374492422;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3667593487;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t3589549831;
// UnityEngine.AnimationEvent
struct AnimationEvent_t3669457594;
// UnityEngine.Object
struct Object_t3071478659;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.Animator
struct Animator_t2776330603;
// UnityEngine.RuntimeAnimatorController
struct RuntimeAnimatorController_t274649809;
// UnityEngine.AsyncOperation
struct AsyncOperation_t3699081103;
// UnityEngine.Application/LogCallback
struct LogCallback_t2984951347;
// UnityEngine.AssemblyIsEditorAssembly
struct AssemblyIsEditorAssembly_t1696890055;
// UnityEngine.AssetBundle
struct AssetBundle_t2070959688;
// UnityEngine.AssetBundleCreateRequest
struct AssetBundleCreateRequest_t1416890373;
// UnityEngine.AssetBundleRequest
struct AssetBundleRequest_t2154290273;
// UnityEngine.Object[]
struct ObjectU5BU5D_t1015136018;
// System.Type[]
struct TypeU5BU5D_t3339007067;
// UnityEngine.AudioClip
struct AudioClip_t794140988;
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t83861602;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_t4244274966;
// UnityEngine.AudioSettings/AudioConfigurationChangeHandler
struct AudioConfigurationChangeHandler_t1377657005;
// UnityEngine.AudioSource
struct AudioSource_t1740077639;
// UnityEngine.Behaviour
struct Behaviour_t200106419;
// UnityEngine.BitStream
struct BitStream_t239125475;
// UnityEngine.Camera
struct Camera_t2727095145;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;
// UnityEngine.Camera[]
struct CameraU5BU5D_t2716570836;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t1945583101;
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_t4247149838;
// UnityEngine.Canvas
struct Canvas_t2727140764;
// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.CanvasGroup
struct CanvasGroup_t3702418109;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t3950887807;
// UnityEngine.Texture
struct Texture_t2526458961;
// UnityEngine.Mesh
struct Mesh_t4241756145;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t1317283468;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t1967039240;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1355284821;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t1355284823;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// UnityEngine.CapsuleCollider
struct CapsuleCollider_t318617463;
// UnityEngine.CharacterController
struct CharacterController_t1618060635;
// UnityEngine.Cloth
struct Cloth_t4194460560;
// UnityEngine.Collider
struct Collider_t2939674232;
// UnityEngine.Rigidbody
struct Rigidbody_t3346577219;
// UnityEngine.PhysicMaterial
struct PhysicMaterial_t211873335;
// UnityEngine.Collider2D
struct Collider2D_t1552025098;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t1743771669;
// UnityEngine.Collision
struct Collision_t2494107688;
// UnityEngine.ContactPoint[]
struct ContactPointU5BU5D_t715040733;
// UnityEngine.Component
struct Component_t3501516275;
// UnityEngine.Collision2D
struct Collision2D_t2859305914;
// UnityEngine.ContactPoint2D[]
struct ContactPoint2DU5BU5D_t3916425411;
// UnityEngine.Compass
struct Compass_t599792712;
// UnityEngine.Component[]
struct ComponentU5BU5D_t663911650;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t574734531;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t2416790841;
// UnityEngine.Coroutine
struct Coroutine_t3621161934;
// UnityEngine.CullingGroup
struct CullingGroup_t1868862003;
// UnityEngine.CullingGroup/StateChanged
struct StateChanged_t2578300556;
// UnityEngine.CustomYieldInstruction
struct CustomYieldInstruction_t2666549910;
// UnityEngine.ILogger
struct ILogger_t629411471;
// System.Exception
struct Exception_t3991598821;
// UnityEngine.DebugLogHandler
struct DebugLogHandler_t2406589519;
// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_t62111112;
// UnityEngine.Display
struct Display_t1321072632;
// UnityEngine.Display/DisplaysUpdatedDelegate
struct DisplaysUpdatedDelegate_t581305515;
// UnityEngine.RectTransform
struct RectTransform_t972643934;
// UnityEngine.Event
struct Event_t4196595728;
// UnityEngine.Events.ArgumentCache
struct ArgumentCache_t1171347191;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t1559630662;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Delegate
struct Delegate_t3310234105;
// UnityEngine.Events.InvokableCall
struct InvokableCall_t1277370263;
// UnityEngine.Events.UnityAction
struct UnityAction_t594794173;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t3597236437;
// UnityEngine.Events.PersistentCall
struct PersistentCall_t2972625667;
// UnityEngine.Events.UnityEventBase
struct UnityEventBase_t1020378628;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t4062675100;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t1266085011;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "UnityEngine_U3CModuleU3E86524790.h"
#include "UnityEngine_U3CModuleU3E86524790MethodDeclarations.h"
#include "UnityEngine_AOT_MonoPInvokeCallbackAttribute1825429660.h"
#include "UnityEngine_AOT_MonoPInvokeCallbackAttribute1825429660MethodDeclarations.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Void2863195528.h"
#include "mscorlib_System_Attribute2523058482MethodDeclarations.h"
#include "UnityEngine_UnityEngine__AndroidJNIHelper2775494809.h"
#include "UnityEngine_UnityEngine__AndroidJNIHelper2775494809MethodDeclarations.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "UnityEngine_UnityEngine_AndroidJavaRunnable1289602340.h"
#include "UnityEngine_UnityEngine_AndroidJavaRunnableProxy1013435428MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AndroidJNIHelper1095326088MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AndroidJavaRunnableProxy1013435428.h"
#include "UnityEngine_UnityEngine_AndroidJavaProxy1828457281.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_jvalue3862675307.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071MethodDeclarations.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AndroidReflection1838616560MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AndroidJNISafe3064662183MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AndroidJavaObject2362096582MethodDeclarations.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "mscorlib_System_Exception3991598821MethodDeclarations.h"
#include "mscorlib_System_Int321153838500.h"
#include "mscorlib_System_IntPtr4010401971MethodDeclarations.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_Byte2862609660.h"
#include "mscorlib_System_Int161153838442.h"
#include "mscorlib_System_Int641153838595.h"
#include "mscorlib_System_Single4291918972.h"
#include "mscorlib_System_Double3868226565.h"
#include "mscorlib_System_Char2862622538.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_AndroidJavaClass1816259147.h"
#include "UnityEngine_UnityEngine_AndroidJavaObject2362096582.h"
#include "mscorlib_System_Exception3991598821.h"
#include "mscorlib_System_Type2863145774MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AndroidJNI3901305722MethodDeclarations.h"
#include "mscorlib_System_RuntimeTypeHandle2669177232.h"
#include "mscorlib_System_Text_StringBuilder243639308MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "UnityEngine_UnityEngine_AddComponentMenu813859935.h"
#include "UnityEngine_UnityEngine_AddComponentMenu813859935MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AndroidJavaClass1816259147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AndroidJavaException125722146.h"
#include "UnityEngine_UnityEngine_AndroidJavaException125722146MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug4195163081MethodDeclarations.h"
#include "mscorlib_System_GC1614687344MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AndroidJavaProxy1828457281MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AndroidJavaRunnable1289602340MethodDeclarations.h"
#include "mscorlib_System_AsyncCallback1369114871.h"
#include "UnityEngine_UnityEngine_AndroidJNI3901305722.h"
#include "UnityEngine_UnityEngine_AndroidJNIHelper1095326088.h"
#include "UnityEngine_UnityEngine_AndroidJNISafe3064662183.h"
#include "UnityEngine_UnityEngine_AndroidReflection1838616560.h"
#include "UnityEngine_UnityEngine_Animation1724966010.h"
#include "UnityEngine_UnityEngine_Animation1724966010MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationClip2007702890.h"
#include "UnityEngine_UnityEngine_AnimationState3682323633.h"
#include "UnityEngine_UnityEngine_PlayMode1155122555.h"
#include "UnityEngine_UnityEngine_Animation_Enumerator1374492422MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animation_Enumerator1374492422.h"
#include "UnityEngine_UnityEngine_AnimationBlendMode23503924.h"
#include "UnityEngine_UnityEngine_AnimationBlendMode23503924MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationClip2007702890MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Motion3026528250MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationCurve3667593487.h"
#include "UnityEngine_UnityEngine_AnimationCurve3667593487MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Keyframe4079056114.h"
#include "UnityEngine_UnityEngine_Keyframe4079056114MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WrapMode1491636113.h"
#include "UnityEngine_UnityEngine_AnimationEvent3669457594.h"
#include "UnityEngine_UnityEngine_AnimationEvent3669457594MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "UnityEngine_UnityEngine_AnimationEventSource2152433973.h"
#include "UnityEngine_UnityEngine_SendMessageOptions3856946179.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo323110318.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo2746035113.h"
#include "mscorlib_System_Single4291918972MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3071478659MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo323110318MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo2746035113MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationEventSource2152433973MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationState3682323633MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UnityEngine_Animator2776330603.h"
#include "UnityEngine_UnityEngine_Animator2776330603MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_AvatarIKGoal2036631794.h"
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo2817229998.h"
#include "UnityEngine_UnityEngine_AvatarTarget2373143374.h"
#include "UnityEngine_UnityEngine_MatchTargetWeightMask258413904.h"
#include "UnityEngine_UnityEngine_HumanBodyBones1606609988.h"
#include "UnityEngine_UnityEngine_AnimatorCullingMode3217251138.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController274649809.h"
#include "UnityEngine_UnityEngine_AnimatorCullingMode3217251138MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo2817229998MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application2856536070.h"
#include "UnityEngine_UnityEngine_Application2856536070MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform3050318497.h"
#include "mscorlib_System_Globalization_CultureInfo1065375142MethodDeclarations.h"
#include "mscorlib_System_Globalization_NumberFormatInfo1637637232.h"
#include "mscorlib_System_UInt3224667981.h"
#include "mscorlib_System_UInt1624667923.h"
#include "mscorlib_System_Globalization_CultureInfo1065375142.h"
#include "mscorlib_System_Double3868226565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_LogType4286006228.h"
#include "UnityEngine_UnityEngine_Application_LogCallback2984951347MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application_LogCallback2984951347.h"
#include "UnityEngine_UnityEngine_NetworkReachability612403035.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManag2940962239MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1080795294.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1080795294MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_LoadSceneM3067001883.h"
#include "UnityEngine_UnityEngine_AsyncOperation3699081103.h"
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssembly1696890055.h"
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssembly1696890055MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AssetBundle2070959688.h"
#include "UnityEngine_UnityEngine_AssetBundle2070959688MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AssetBundleCreateRequest1416890373.h"
#include "UnityEngine_UnityEngine_AssetBundleCreateRequest1416890373MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AsyncOperation3699081103MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AssetBundleRequest2154290273.h"
#include "UnityEngine_UnityEngine_AssetBundleRequest2154290273MethodDeclarations.h"
#include "UnityEngine_UnityEngine_YieldInstruction2048002629MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AttributeHelperEngine4212589506.h"
#include "UnityEngine_UnityEngine_AttributeHelperEngine4212589506MethodDeclarations.h"
#include "UnityEngine_UnityEngine_DisallowMultipleComponent62111112.h"
#include "UnityEngine_UnityEngine_ExecuteInEditMode3132250205.h"
#include "UnityEngine_UnityEngine_RequireComponent1687166108.h"
#include "System_System_Collections_Generic_Stack_1_gen1666739402MethodDeclarations.h"
#include "System_System_Collections_Generic_Stack_1_gen1666739402.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4231331326MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4231331326.h"
#include "UnityEngine_UnityEngine_AudioClip794140988.h"
#include "UnityEngine_UnityEngine_AudioClip794140988MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallback83861602.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCa4244274966.h"
#include "mscorlib_System_NullReferenceException1441619295MethodDeclarations.h"
#include "mscorlib_System_ArgumentException928607144MethodDeclarations.h"
#include "mscorlib_System_Delegate3310234105MethodDeclarations.h"
#include "mscorlib_System_NullReferenceException1441619295.h"
#include "mscorlib_System_ArgumentException928607144.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallback83861602MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCa4244274966MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioListener3685735200.h"
#include "UnityEngine_UnityEngine_AudioListener3685735200MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSettings3774206607.h"
#include "UnityEngine_UnityEngine_AudioSettings3774206607MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigu1377657005MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigu1377657005.h"
#include "UnityEngine_UnityEngine_AudioSource1740077639.h"
#include "UnityEngine_UnityEngine_AudioSource1740077639MethodDeclarations.h"
#include "mscorlib_System_UInt6424668076.h"
#include "UnityEngine_UnityEngine_GameObject3674682005MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform1659122786MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time4241968337MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Component3501516275.h"
#include "UnityEngine_UnityEngine_AudioType794660134.h"
#include "UnityEngine_UnityEngine_AudioType794660134MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AvatarIKGoal2036631794MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AvatarTarget2373143374MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour200106419.h"
#include "UnityEngine_UnityEngine_Behaviour200106419MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3501516275MethodDeclarations.h"
#include "UnityEngine_UnityEngine_BitStream239125475.h"
#include "UnityEngine_UnityEngine_BitStream239125475MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkViewID3400394436.h"
#include "UnityEngine_UnityEngine_NetworkPlayer3231273765.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UnityEngine_Bounds2711641849MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UnityString3369712284MethodDeclarations.h"
#include "UnityEngine_UnityEngine_BoxCollider2538127765.h"
#include "UnityEngine_UnityEngine_BoxCollider2538127765MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Caching189518581.h"
#include "UnityEngine_UnityEngine_Caching189518581MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "UnityEngine_UnityEngine_Camera2727095145MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "UnityEngine_UnityEngine_CameraClearFlags2093155523.h"
#include "UnityEngine_UnityEngine_Ray3134616544.h"
#include "UnityEngine_UnityEngine_Camera_CameraCallback1945583101MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera_CameraCallback1945583101.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteraction577895768.h"
#include "UnityEngine_UnityEngine_CameraClearFlags2093155523MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Canvas2727140764.h"
#include "UnityEngine_UnityEngine_Canvas2727140764MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvases4247149838.h"
#include "UnityEngine_UnityEngine_RenderMode77252893.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvases4247149838MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CanvasGroup3702418109.h"
#include "UnityEngine_UnityEngine_CanvasGroup3702418109MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_CanvasRenderer3950887807.h"
#include "UnityEngine_UnityEngine_CanvasRenderer3950887807MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "mscorlib_System_Math2862914300MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mesh4241756145.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1317283468.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1355284822.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1967039240.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1355284821.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1355284823.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2522024052.h"
#include "UnityEngine_UnityEngine_CapsuleCollider318617463.h"
#include "UnityEngine_UnityEngine_CapsuleCollider318617463MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CharacterController1618060635.h"
#include "UnityEngine_UnityEngine_CharacterController1618060635MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CollisionFlags490137529.h"
#include "UnityEngine_UnityEngine_Cloth4194460560.h"
#include "UnityEngine_UnityEngine_Cloth4194460560MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"
#include "UnityEngine_UnityEngine_Collider2939674232MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody3346577219.h"
#include "UnityEngine_UnityEngine_PhysicMaterial211873335.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody2D1743771669.h"
#include "UnityEngine_UnityEngine_Collision2494107688.h"
#include "UnityEngine_UnityEngine_Collision2494107688MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ContactPoint243083348.h"
#include "UnityEngine_UnityEngine_ContactPoint243083348MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collision2D2859305914.h"
#include "UnityEngine_UnityEngine_Collision2D2859305914MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ContactPoint2D4288432358.h"
#include "UnityEngine_UnityEngine_ContactPoint2D4288432358MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector24282066565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CollisionFlags490137529MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color4194546905MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "UnityEngine_UnityEngine_Vector44282066567MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf4203372500MethodDeclarations.h"
#include "mscorlib_System_IndexOutOfRangeException3456360697MethodDeclarations.h"
#include "mscorlib_System_IndexOutOfRangeException3456360697.h"
#include "UnityEngine_UnityEngine_Color32598853688.h"
#include "UnityEngine_UnityEngine_Color32598853688MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ColorSpace161844263.h"
#include "UnityEngine_UnityEngine_ColorSpace161844263MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Compass599792712.h"
#include "UnityEngine_UnityEngine_Compass599792712MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen574734531.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit2416790841.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit2416790841MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Coroutine3621161934.h"
#include "UnityEngine_UnityEngine_Coroutine3621161934MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CullingGroup1868862003.h"
#include "UnityEngine_UnityEngine_CullingGroup1868862003MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CullingGroup_StateChanged2578300556MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CullingGroupEvent2820176033.h"
#include "UnityEngine_UnityEngine_CullingGroup_StateChanged2578300556.h"
#include "UnityEngine_UnityEngine_CullingGroupEvent2820176033MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Cursor2745727898.h"
#include "UnityEngine_UnityEngine_Cursor2745727898MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CursorLockMode1155278888.h"
#include "UnityEngine_UnityEngine_CursorLockMode1155278888MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CustomYieldInstruction2666549910.h"
#include "UnityEngine_UnityEngine_CustomYieldInstruction2666549910MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug4195163081.h"
#include "UnityEngine_UnityEngine_DebugLogHandler2406589519MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Logger2997509588MethodDeclarations.h"
#include "UnityEngine_UnityEngine_DebugLogHandler2406589519.h"
#include "UnityEngine_UnityEngine_Logger2997509588.h"
#include "UnityEngine_UnityEngine_DeviceOrientation1141857680.h"
#include "UnityEngine_UnityEngine_DeviceOrientation1141857680MethodDeclarations.h"
#include "UnityEngine_UnityEngine_DeviceType3959528308.h"
#include "UnityEngine_UnityEngine_DeviceType3959528308MethodDeclarations.h"
#include "UnityEngine_UnityEngine_DisallowMultipleComponent62111112MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Display1321072632.h"
#include "UnityEngine_UnityEngine_Display1321072632MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Display_DisplaysUpdatedDele581305515.h"
#include "UnityEngine_UnityEngine_Display_DisplaysUpdatedDele581305515MethodDeclarations.h"
#include "UnityEngine_UnityEngine_DrivenRectTransformTracker4185719096.h"
#include "UnityEngine_UnityEngine_DrivenRectTransformTracker4185719096MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform972643934.h"
#include "UnityEngine_UnityEngine_DrivenTransformProperties624110065.h"
#include "UnityEngine_UnityEngine_DrivenTransformProperties624110065MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Event4196595728.h"
#include "UnityEngine_UnityEngine_Event4196595728MethodDeclarations.h"
#include "UnityEngine_UnityEngine_EventModifiers4195406918.h"
#include "UnityEngine_UnityEngine_EventType637886954.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1974256870MethodDeclarations.h"
#include "mscorlib_System_Enum2862688501MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1974256870.h"
#include "UnityEngine_UnityEngine_KeyCode3128317986.h"
#include "UnityEngine_UnityEngine_EventModifiers4195406918MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_ArgumentCache1171347191.h"
#include "UnityEngine_UnityEngine_Events_ArgumentCache1171347191MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall1559630662.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall1559630662MethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodInfo318736065.h"
#include "mscorlib_System_ArgumentNullException3573189601MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3573189601.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall1277370263.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall1277370263MethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_NetFxCoreExtension2541795172MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction594794173.h"
#include "UnityEngine_UnityEngine_Events_UnityAction594794173MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCallList3597236437.h"
#include "UnityEngine_UnityEngine_Events_InvokableCallList3597236437MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2927816214MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2927816214.h"
#include "mscorlib_System_Predicate_1_gen1170687545MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen1170687545.h"
#include "UnityEngine_UnityEngine_Events_PersistentCall2972625667.h"
#include "UnityEngine_UnityEngine_Events_PersistentCall2972625667MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEventCallState3502354656.h"
#include "UnityEngine_UnityEngine_Events_PersistentListenerM3900400668.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase1020378628.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase1020378628MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall3880155831MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_742075359MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall3890435712MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_165035577MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall3880155831.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_742075359.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall3890435712.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_165035577.h"
#include "mscorlib_System_Reflection_ConstructorInfo4136801618MethodDeclarations.h"
#include "mscorlib_System_Reflection_ConstructorInfo4136801618.h"
#include "UnityEngine_UnityEngine_Events_PersistentCallGroup4062675100.h"
#include "UnityEngine_UnityEngine_Events_PersistentCallGroup4062675100MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen45843923MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen45843923.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerato65516693.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerato65516693MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_PersistentListenerM3900400668MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent1266085011.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent1266085011MethodDeclarations.h"

// !!0 UnityEngine.AndroidJavaObject::Call<System.Object>(System.String,System.Object[])
extern "C"  Il2CppObject * AndroidJavaObject_Call_TisIl2CppObject_m3039876358_gshared (AndroidJavaObject_t2362096582 * __this, String_t* p0, ObjectU5BU5D_t1108656482* p1, const MethodInfo* method);
#define AndroidJavaObject_Call_TisIl2CppObject_m3039876358(__this, p0, p1, method) ((  Il2CppObject * (*) (AndroidJavaObject_t2362096582 *, String_t*, ObjectU5BU5D_t1108656482*, const MethodInfo*))AndroidJavaObject_Call_TisIl2CppObject_m3039876358_gshared)(__this, p0, p1, method)
// !!0 UnityEngine.AndroidJavaObject::Call<System.String>(System.String,System.Object[])
#define AndroidJavaObject_Call_TisString_t_m370002392(__this, p0, p1, method) ((  String_t* (*) (AndroidJavaObject_t2362096582 *, String_t*, ObjectU5BU5D_t1108656482*, const MethodInfo*))AndroidJavaObject_Call_TisIl2CppObject_m3039876358_gshared)(__this, p0, p1, method)
// !!0 UnityEngine.AndroidJavaObject::Call<UnityEngine.AndroidJavaObject>(System.String,System.Object[])
#define AndroidJavaObject_Call_TisAndroidJavaObject_t2362096582_m301711009(__this, p0, p1, method) ((  AndroidJavaObject_t2362096582 * (*) (AndroidJavaObject_t2362096582 *, String_t*, ObjectU5BU5D_t1108656482*, const MethodInfo*))AndroidJavaObject_Call_TisIl2CppObject_m3039876358_gshared)(__this, p0, p1, method)
// !!0 UnityEngine.AndroidJavaObject::CallStatic<System.Object>(System.String,System.Object[])
extern "C"  Il2CppObject * AndroidJavaObject_CallStatic_TisIl2CppObject_m4248195924_gshared (AndroidJavaObject_t2362096582 * __this, String_t* p0, ObjectU5BU5D_t1108656482* p1, const MethodInfo* method);
#define AndroidJavaObject_CallStatic_TisIl2CppObject_m4248195924(__this, p0, p1, method) ((  Il2CppObject * (*) (AndroidJavaObject_t2362096582 *, String_t*, ObjectU5BU5D_t1108656482*, const MethodInfo*))AndroidJavaObject_CallStatic_TisIl2CppObject_m4248195924_gshared)(__this, p0, p1, method)
// !!0 UnityEngine.AndroidJavaObject::CallStatic<UnityEngine.AndroidJavaObject>(System.String,System.Object[])
#define AndroidJavaObject_CallStatic_TisAndroidJavaObject_t2362096582_m3741769967(__this, p0, p1, method) ((  AndroidJavaObject_t2362096582 * (*) (AndroidJavaObject_t2362096582 *, String_t*, ObjectU5BU5D_t1108656482*, const MethodInfo*))AndroidJavaObject_CallStatic_TisIl2CppObject_m4248195924_gshared)(__this, p0, p1, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AOT.MonoPInvokeCallbackAttribute::.ctor(System.Type)
extern "C"  void MonoPInvokeCallbackAttribute__ctor_m706261135 (MonoPInvokeCallbackAttribute_t1825429660 * __this, Type_t * ___type0, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IntPtr UnityEngine._AndroidJNIHelper::CreateJavaRunnable(UnityEngine.AndroidJavaRunnable)
extern Il2CppClass* AndroidJavaRunnableProxy_t1013435428_il2cpp_TypeInfo_var;
extern const uint32_t _AndroidJNIHelper_CreateJavaRunnable_m2949886695_MetadataUsageId;
extern "C"  IntPtr_t _AndroidJNIHelper_CreateJavaRunnable_m2949886695 (Il2CppObject * __this /* static, unused */, AndroidJavaRunnable_t1289602340 * ___jrunnable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (_AndroidJNIHelper_CreateJavaRunnable_m2949886695_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AndroidJavaRunnable_t1289602340 * L_0 = ___jrunnable0;
		AndroidJavaRunnableProxy_t1013435428 * L_1 = (AndroidJavaRunnableProxy_t1013435428 *)il2cpp_codegen_object_new(AndroidJavaRunnableProxy_t1013435428_il2cpp_TypeInfo_var);
		AndroidJavaRunnableProxy__ctor_m1203400718(L_1, L_0, /*hidden argument*/NULL);
		IntPtr_t L_2 = AndroidJNIHelper_CreateJavaProxy_m1509148942(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.jvalue[] UnityEngine._AndroidJNIHelper::CreateJNIArgArray(System.Object[])
extern Il2CppClass* jvalueU5BU5D_t1723627146_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidReflection_t1838616560_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* Byte_t2862609660_il2cpp_TypeInfo_var;
extern Il2CppClass* Int16_t1153838442_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t1153838595_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2862622538_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidJavaClass_t1816259147_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidJavaObject_t2362096582_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppArray_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidJavaProxy_t1828457281_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidJavaRunnable_t1289602340_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1206652612;
extern Il2CppCodeGenString* _stringLiteral39;
extern const uint32_t _AndroidJNIHelper_CreateJNIArgArray_m3400436963_MetadataUsageId;
extern "C"  jvalueU5BU5D_t1723627146* _AndroidJNIHelper_CreateJNIArgArray_m3400436963 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (_AndroidJNIHelper_CreateJNIArgArray_m3400436963_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	jvalueU5BU5D_t1723627146* V_0 = NULL;
	int32_t V_1 = 0;
	Il2CppObject * V_2 = NULL;
	ObjectU5BU5D_t1108656482* V_3 = NULL;
	int32_t V_4 = 0;
	{
		ObjectU5BU5D_t1108656482* L_0 = ___args0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		int32_t L_1 = Array_GetLength_m1450876743((Il2CppArray *)(Il2CppArray *)L_0, 0, /*hidden argument*/NULL);
		V_0 = ((jvalueU5BU5D_t1723627146*)SZArrayNew(jvalueU5BU5D_t1723627146_il2cpp_TypeInfo_var, (uint32_t)L_1));
		V_1 = 0;
		ObjectU5BU5D_t1108656482* L_2 = ___args0;
		V_3 = L_2;
		V_4 = 0;
		goto IL_0269;
	}

IL_0019:
	{
		ObjectU5BU5D_t1108656482* L_3 = V_3;
		int32_t L_4 = V_4;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_2 = L_6;
		Il2CppObject * L_7 = V_2;
		if (L_7)
		{
			goto IL_003a;
		}
	}
	{
		jvalueU5BU5D_t1723627146* L_8 = V_0;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		IntPtr_t L_10 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9)))->set_l_8(L_10);
		goto IL_025f;
	}

IL_003a:
	{
		Il2CppObject * L_11 = V_2;
		NullCheck(L_11);
		Type_t * L_12 = Object_GetType_m2022236990(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AndroidReflection_t1838616560_il2cpp_TypeInfo_var);
		bool L_13 = AndroidReflection_IsPrimitive_m2208855481(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_015a;
		}
	}
	{
		Il2CppObject * L_14 = V_2;
		if (!((Il2CppObject *)IsInstSealed(L_14, Int32_t1153838500_il2cpp_TypeInfo_var)))
		{
			goto IL_006c;
		}
	}
	{
		jvalueU5BU5D_t1723627146* L_15 = V_0;
		int32_t L_16 = V_1;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		Il2CppObject * L_17 = V_2;
		((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_16)))->set_i_4(((*(int32_t*)((int32_t*)UnBox (L_17, Int32_t1153838500_il2cpp_TypeInfo_var)))));
		goto IL_0155;
	}

IL_006c:
	{
		Il2CppObject * L_18 = V_2;
		if (!((Il2CppObject *)IsInstSealed(L_18, Boolean_t476798718_il2cpp_TypeInfo_var)))
		{
			goto IL_008e;
		}
	}
	{
		jvalueU5BU5D_t1723627146* L_19 = V_0;
		int32_t L_20 = V_1;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_20);
		Il2CppObject * L_21 = V_2;
		((L_19)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_20)))->set_z_0(((*(bool*)((bool*)UnBox (L_21, Boolean_t476798718_il2cpp_TypeInfo_var)))));
		goto IL_0155;
	}

IL_008e:
	{
		Il2CppObject * L_22 = V_2;
		if (!((Il2CppObject *)IsInstSealed(L_22, Byte_t2862609660_il2cpp_TypeInfo_var)))
		{
			goto IL_00b0;
		}
	}
	{
		jvalueU5BU5D_t1723627146* L_23 = V_0;
		int32_t L_24 = V_1;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		Il2CppObject * L_25 = V_2;
		((L_23)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_24)))->set_b_1(((*(uint8_t*)((uint8_t*)UnBox (L_25, Byte_t2862609660_il2cpp_TypeInfo_var)))));
		goto IL_0155;
	}

IL_00b0:
	{
		Il2CppObject * L_26 = V_2;
		if (!((Il2CppObject *)IsInstSealed(L_26, Int16_t1153838442_il2cpp_TypeInfo_var)))
		{
			goto IL_00d2;
		}
	}
	{
		jvalueU5BU5D_t1723627146* L_27 = V_0;
		int32_t L_28 = V_1;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, L_28);
		Il2CppObject * L_29 = V_2;
		((L_27)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28)))->set_s_3(((*(int16_t*)((int16_t*)UnBox (L_29, Int16_t1153838442_il2cpp_TypeInfo_var)))));
		goto IL_0155;
	}

IL_00d2:
	{
		Il2CppObject * L_30 = V_2;
		if (!((Il2CppObject *)IsInstSealed(L_30, Int64_t1153838595_il2cpp_TypeInfo_var)))
		{
			goto IL_00f4;
		}
	}
	{
		jvalueU5BU5D_t1723627146* L_31 = V_0;
		int32_t L_32 = V_1;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		Il2CppObject * L_33 = V_2;
		((L_31)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_32)))->set_j_5(((*(int64_t*)((int64_t*)UnBox (L_33, Int64_t1153838595_il2cpp_TypeInfo_var)))));
		goto IL_0155;
	}

IL_00f4:
	{
		Il2CppObject * L_34 = V_2;
		if (!((Il2CppObject *)IsInstSealed(L_34, Single_t4291918972_il2cpp_TypeInfo_var)))
		{
			goto IL_0116;
		}
	}
	{
		jvalueU5BU5D_t1723627146* L_35 = V_0;
		int32_t L_36 = V_1;
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, L_36);
		Il2CppObject * L_37 = V_2;
		((L_35)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_36)))->set_f_6(((*(float*)((float*)UnBox (L_37, Single_t4291918972_il2cpp_TypeInfo_var)))));
		goto IL_0155;
	}

IL_0116:
	{
		Il2CppObject * L_38 = V_2;
		if (!((Il2CppObject *)IsInstSealed(L_38, Double_t3868226565_il2cpp_TypeInfo_var)))
		{
			goto IL_0138;
		}
	}
	{
		jvalueU5BU5D_t1723627146* L_39 = V_0;
		int32_t L_40 = V_1;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, L_40);
		Il2CppObject * L_41 = V_2;
		((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_40)))->set_d_7(((*(double*)((double*)UnBox (L_41, Double_t3868226565_il2cpp_TypeInfo_var)))));
		goto IL_0155;
	}

IL_0138:
	{
		Il2CppObject * L_42 = V_2;
		if (!((Il2CppObject *)IsInstSealed(L_42, Char_t2862622538_il2cpp_TypeInfo_var)))
		{
			goto IL_0155;
		}
	}
	{
		jvalueU5BU5D_t1723627146* L_43 = V_0;
		int32_t L_44 = V_1;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, L_44);
		Il2CppObject * L_45 = V_2;
		((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_44)))->set_c_2(((*(Il2CppChar*)((Il2CppChar*)UnBox (L_45, Char_t2862622538_il2cpp_TypeInfo_var)))));
	}

IL_0155:
	{
		goto IL_025f;
	}

IL_015a:
	{
		Il2CppObject * L_46 = V_2;
		if (!((String_t*)IsInstSealed(L_46, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_0181;
		}
	}
	{
		jvalueU5BU5D_t1723627146* L_47 = V_0;
		int32_t L_48 = V_1;
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, L_48);
		Il2CppObject * L_49 = V_2;
		IntPtr_t L_50 = AndroidJNISafe_NewStringUTF_m1650627069(NULL /*static, unused*/, ((String_t*)CastclassSealed(L_49, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		((L_47)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48)))->set_l_8(L_50);
		goto IL_025f;
	}

IL_0181:
	{
		Il2CppObject * L_51 = V_2;
		if (!((AndroidJavaClass_t1816259147 *)IsInstClass(L_51, AndroidJavaClass_t1816259147_il2cpp_TypeInfo_var)))
		{
			goto IL_01a8;
		}
	}
	{
		jvalueU5BU5D_t1723627146* L_52 = V_0;
		int32_t L_53 = V_1;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, L_53);
		Il2CppObject * L_54 = V_2;
		NullCheck(((AndroidJavaClass_t1816259147 *)CastclassClass(L_54, AndroidJavaClass_t1816259147_il2cpp_TypeInfo_var)));
		IntPtr_t L_55 = AndroidJavaObject_GetRawClass_m842200578(((AndroidJavaClass_t1816259147 *)CastclassClass(L_54, AndroidJavaClass_t1816259147_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		((L_52)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_53)))->set_l_8(L_55);
		goto IL_025f;
	}

IL_01a8:
	{
		Il2CppObject * L_56 = V_2;
		if (!((AndroidJavaObject_t2362096582 *)IsInstClass(L_56, AndroidJavaObject_t2362096582_il2cpp_TypeInfo_var)))
		{
			goto IL_01cf;
		}
	}
	{
		jvalueU5BU5D_t1723627146* L_57 = V_0;
		int32_t L_58 = V_1;
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, L_58);
		Il2CppObject * L_59 = V_2;
		NullCheck(((AndroidJavaObject_t2362096582 *)CastclassClass(L_59, AndroidJavaObject_t2362096582_il2cpp_TypeInfo_var)));
		IntPtr_t L_60 = AndroidJavaObject_GetRawObject_m4031496215(((AndroidJavaObject_t2362096582 *)CastclassClass(L_59, AndroidJavaObject_t2362096582_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		((L_57)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_58)))->set_l_8(L_60);
		goto IL_025f;
	}

IL_01cf:
	{
		Il2CppObject * L_61 = V_2;
		if (!((Il2CppArray *)IsInstClass(L_61, Il2CppArray_il2cpp_TypeInfo_var)))
		{
			goto IL_01f6;
		}
	}
	{
		jvalueU5BU5D_t1723627146* L_62 = V_0;
		int32_t L_63 = V_1;
		NullCheck(L_62);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_62, L_63);
		Il2CppObject * L_64 = V_2;
		IntPtr_t L_65 = _AndroidJNIHelper_ConvertToJNIArray_m1911420401(NULL /*static, unused*/, ((Il2CppArray *)CastclassClass(L_64, Il2CppArray_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		((L_62)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_63)))->set_l_8(L_65);
		goto IL_025f;
	}

IL_01f6:
	{
		Il2CppObject * L_66 = V_2;
		if (!((AndroidJavaProxy_t1828457281 *)IsInstClass(L_66, AndroidJavaProxy_t1828457281_il2cpp_TypeInfo_var)))
		{
			goto IL_021d;
		}
	}
	{
		jvalueU5BU5D_t1723627146* L_67 = V_0;
		int32_t L_68 = V_1;
		NullCheck(L_67);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_67, L_68);
		Il2CppObject * L_69 = V_2;
		IntPtr_t L_70 = AndroidJNIHelper_CreateJavaProxy_m1509148942(NULL /*static, unused*/, ((AndroidJavaProxy_t1828457281 *)CastclassClass(L_69, AndroidJavaProxy_t1828457281_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		((L_67)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_68)))->set_l_8(L_70);
		goto IL_025f;
	}

IL_021d:
	{
		Il2CppObject * L_71 = V_2;
		if (!((AndroidJavaRunnable_t1289602340 *)IsInstSealed(L_71, AndroidJavaRunnable_t1289602340_il2cpp_TypeInfo_var)))
		{
			goto IL_0244;
		}
	}
	{
		jvalueU5BU5D_t1723627146* L_72 = V_0;
		int32_t L_73 = V_1;
		NullCheck(L_72);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_72, L_73);
		Il2CppObject * L_74 = V_2;
		IntPtr_t L_75 = AndroidJNIHelper_CreateJavaRunnable_m1606930224(NULL /*static, unused*/, ((AndroidJavaRunnable_t1289602340 *)CastclassSealed(L_74, AndroidJavaRunnable_t1289602340_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		((L_72)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_73)))->set_l_8(L_75);
		goto IL_025f;
	}

IL_0244:
	{
		Il2CppObject * L_76 = V_2;
		NullCheck(L_76);
		Type_t * L_77 = Object_GetType_m2022236990(L_76, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_78 = String_Concat_m2809334143(NULL /*static, unused*/, _stringLiteral1206652612, L_77, _stringLiteral39, /*hidden argument*/NULL);
		Exception_t3991598821 * L_79 = (Exception_t3991598821 *)il2cpp_codegen_object_new(Exception_t3991598821_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_79, L_78, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_79);
	}

IL_025f:
	{
		int32_t L_80 = V_1;
		V_1 = ((int32_t)((int32_t)L_80+(int32_t)1));
		int32_t L_81 = V_4;
		V_4 = ((int32_t)((int32_t)L_81+(int32_t)1));
	}

IL_0269:
	{
		int32_t L_82 = V_4;
		ObjectU5BU5D_t1108656482* L_83 = V_3;
		NullCheck(L_83);
		if ((((int32_t)L_82) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_83)->max_length)))))))
		{
			goto IL_0019;
		}
	}
	{
		jvalueU5BU5D_t1723627146* L_84 = V_0;
		return L_84;
	}
}
// System.Void UnityEngine._AndroidJNIHelper::DeleteJNIArgArray(System.Object[],UnityEngine.jvalue[])
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidJavaRunnable_t1289602340_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidJavaProxy_t1828457281_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppArray_il2cpp_TypeInfo_var;
extern const uint32_t _AndroidJNIHelper_DeleteJNIArgArray_m2053299397_MetadataUsageId;
extern "C"  void _AndroidJNIHelper_DeleteJNIArgArray_m2053299397 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___args0, jvalueU5BU5D_t1723627146* ___jniArgs1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (_AndroidJNIHelper_DeleteJNIArgArray_m2053299397_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	ObjectU5BU5D_t1108656482* V_2 = NULL;
	int32_t V_3 = 0;
	{
		V_0 = 0;
		ObjectU5BU5D_t1108656482* L_0 = ___args0;
		V_2 = L_0;
		V_3 = 0;
		goto IL_0054;
	}

IL_000b:
	{
		ObjectU5BU5D_t1108656482* L_1 = V_2;
		int32_t L_2 = V_3;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_1 = L_4;
		Il2CppObject * L_5 = V_1;
		if (((String_t*)IsInstSealed(L_5, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_003b;
		}
	}
	{
		Il2CppObject * L_6 = V_1;
		if (((AndroidJavaRunnable_t1289602340 *)IsInstSealed(L_6, AndroidJavaRunnable_t1289602340_il2cpp_TypeInfo_var)))
		{
			goto IL_003b;
		}
	}
	{
		Il2CppObject * L_7 = V_1;
		if (((AndroidJavaProxy_t1828457281 *)IsInstClass(L_7, AndroidJavaProxy_t1828457281_il2cpp_TypeInfo_var)))
		{
			goto IL_003b;
		}
	}
	{
		Il2CppObject * L_8 = V_1;
		if (!((Il2CppArray *)IsInstClass(L_8, Il2CppArray_il2cpp_TypeInfo_var)))
		{
			goto IL_004c;
		}
	}

IL_003b:
	{
		jvalueU5BU5D_t1723627146* L_9 = ___jniArgs1;
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		IntPtr_t L_11 = ((L_9)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_10)))->get_l_8();
		AndroidJNISafe_DeleteLocalRef_m2718059367(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
	}

IL_004c:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
		int32_t L_13 = V_3;
		V_3 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0054:
	{
		int32_t L_14 = V_3;
		ObjectU5BU5D_t1108656482* L_15 = V_2;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_000b;
		}
	}
	{
		return;
	}
}
// System.IntPtr UnityEngine._AndroidJNIHelper::ConvertToJNIArray(System.Array)
extern const Il2CppType* Int32_t1153838500_0_0_0_var;
extern const Il2CppType* Boolean_t476798718_0_0_0_var;
extern const Il2CppType* Byte_t2862609660_0_0_0_var;
extern const Il2CppType* Int16_t1153838442_0_0_0_var;
extern const Il2CppType* Int64_t1153838595_0_0_0_var;
extern const Il2CppType* Single_t4291918972_0_0_0_var;
extern const Il2CppType* Double_t3868226565_0_0_0_var;
extern const Il2CppType* Char_t2862622538_0_0_0_var;
extern const Il2CppType* String_t_0_0_0_var;
extern const Il2CppType* AndroidJavaObject_t2362096582_0_0_0_var;
extern Il2CppClass* AndroidReflection_t1838616560_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var;
extern Il2CppClass* BooleanU5BU5D_t3456302923_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var;
extern Il2CppClass* Int16U5BU5D_t801762735_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64U5BU5D_t2174042770_il2cpp_TypeInfo_var;
extern Il2CppClass* SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var;
extern Il2CppClass* DoubleU5BU5D_t2145413704_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t3324145743_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidJavaObjectU5BU5D_t3281907299_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtrU5BU5D_t3228729122_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2211845893;
extern Il2CppCodeGenString* _stringLiteral2080463411;
extern Il2CppCodeGenString* _stringLiteral4146613544;
extern Il2CppCodeGenString* _stringLiteral39;
extern const uint32_t _AndroidJNIHelper_ConvertToJNIArray_m1911420401_MetadataUsageId;
extern "C"  IntPtr_t _AndroidJNIHelper_ConvertToJNIArray_m1911420401 (Il2CppObject * __this /* static, unused */, Il2CppArray * ___array0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (_AndroidJNIHelper_ConvertToJNIArray_m1911420401_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	StringU5BU5D_t4054002952* V_1 = NULL;
	int32_t V_2 = 0;
	IntPtr_t V_3;
	memset(&V_3, 0, sizeof(V_3));
	IntPtr_t V_4;
	memset(&V_4, 0, sizeof(V_4));
	int32_t V_5 = 0;
	IntPtr_t V_6;
	memset(&V_6, 0, sizeof(V_6));
	AndroidJavaObjectU5BU5D_t3281907299* V_7 = NULL;
	int32_t V_8 = 0;
	IntPtrU5BU5D_t3228729122* V_9 = NULL;
	IntPtr_t V_10;
	memset(&V_10, 0, sizeof(V_10));
	IntPtr_t V_11;
	memset(&V_11, 0, sizeof(V_11));
	int32_t V_12 = 0;
	IntPtr_t V_13;
	memset(&V_13, 0, sizeof(V_13));
	IntPtr_t V_14;
	memset(&V_14, 0, sizeof(V_14));
	{
		Il2CppArray * L_0 = ___array0;
		NullCheck(L_0);
		Type_t * L_1 = Object_GetType_m2022236990(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Type_t * L_2 = VirtFuncInvoker0< Type_t * >::Invoke(45 /* System.Type System.Type::GetElementType() */, L_1);
		V_0 = L_2;
		Type_t * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(AndroidReflection_t1838616560_il2cpp_TypeInfo_var);
		bool L_4 = AndroidReflection_IsPrimitive_m2208855481(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_00fc;
		}
	}
	{
		Type_t * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int32_t1153838500_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_5) == ((Il2CppObject*)(Type_t *)L_6))))
		{
			goto IL_0033;
		}
	}
	{
		Il2CppArray * L_7 = ___array0;
		IntPtr_t L_8 = AndroidJNISafe_ToIntArray_m952526659(NULL /*static, unused*/, ((Int32U5BU5D_t3230847821*)Castclass(L_7, Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_8;
	}

IL_0033:
	{
		Type_t * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_10 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Boolean_t476798718_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_9) == ((Il2CppObject*)(Type_t *)L_10))))
		{
			goto IL_004f;
		}
	}
	{
		Il2CppArray * L_11 = ___array0;
		IntPtr_t L_12 = AndroidJNISafe_ToBooleanArray_m2993741840(NULL /*static, unused*/, ((BooleanU5BU5D_t3456302923*)Castclass(L_11, BooleanU5BU5D_t3456302923_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_12;
	}

IL_004f:
	{
		Type_t * L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_14 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Byte_t2862609660_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_13) == ((Il2CppObject*)(Type_t *)L_14))))
		{
			goto IL_006b;
		}
	}
	{
		Il2CppArray * L_15 = ___array0;
		IntPtr_t L_16 = AndroidJNISafe_ToByteArray_m1438477888(NULL /*static, unused*/, ((ByteU5BU5D_t4260760469*)Castclass(L_15, ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_16;
	}

IL_006b:
	{
		Type_t * L_17 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int16_t1153838442_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18))))
		{
			goto IL_0087;
		}
	}
	{
		Il2CppArray * L_19 = ___array0;
		IntPtr_t L_20 = AndroidJNISafe_ToShortArray_m130416912(NULL /*static, unused*/, ((Int16U5BU5D_t801762735*)Castclass(L_19, Int16U5BU5D_t801762735_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_20;
	}

IL_0087:
	{
		Type_t * L_21 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int64_t1153838595_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_21) == ((Il2CppObject*)(Type_t *)L_22))))
		{
			goto IL_00a3;
		}
	}
	{
		Il2CppArray * L_23 = ___array0;
		IntPtr_t L_24 = AndroidJNISafe_ToLongArray_m2453751525(NULL /*static, unused*/, ((Int64U5BU5D_t2174042770*)Castclass(L_23, Int64U5BU5D_t2174042770_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_24;
	}

IL_00a3:
	{
		Type_t * L_25 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Single_t4291918972_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_25) == ((Il2CppObject*)(Type_t *)L_26))))
		{
			goto IL_00bf;
		}
	}
	{
		Il2CppArray * L_27 = ___array0;
		IntPtr_t L_28 = AndroidJNISafe_ToFloatArray_m942365282(NULL /*static, unused*/, ((SingleU5BU5D_t2316563989*)Castclass(L_27, SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_28;
	}

IL_00bf:
	{
		Type_t * L_29 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_30 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Double_t3868226565_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_29) == ((Il2CppObject*)(Type_t *)L_30))))
		{
			goto IL_00db;
		}
	}
	{
		Il2CppArray * L_31 = ___array0;
		IntPtr_t L_32 = AndroidJNISafe_ToDoubleArray_m1391050336(NULL /*static, unused*/, ((DoubleU5BU5D_t2145413704*)Castclass(L_31, DoubleU5BU5D_t2145413704_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_32;
	}

IL_00db:
	{
		Type_t * L_33 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_34 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Char_t2862622538_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_33) == ((Il2CppObject*)(Type_t *)L_34))))
		{
			goto IL_00f7;
		}
	}
	{
		Il2CppArray * L_35 = ___array0;
		IntPtr_t L_36 = AndroidJNISafe_ToCharArray_m589918336(NULL /*static, unused*/, ((CharU5BU5D_t3324145743*)Castclass(L_35, CharU5BU5D_t3324145743_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_36;
	}

IL_00f7:
	{
		goto IL_0261;
	}

IL_00fc:
	{
		Type_t * L_37 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_38 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_37) == ((Il2CppObject*)(Type_t *)L_38))))
		{
			goto IL_0170;
		}
	}
	{
		Il2CppArray * L_39 = ___array0;
		V_1 = ((StringU5BU5D_t4054002952*)Castclass(L_39, StringU5BU5D_t4054002952_il2cpp_TypeInfo_var));
		Il2CppArray * L_40 = ___array0;
		NullCheck(L_40);
		int32_t L_41 = Array_GetLength_m1450876743(L_40, 0, /*hidden argument*/NULL);
		V_2 = L_41;
		IntPtr_t L_42 = AndroidJNISafe_FindClass_m952975504(NULL /*static, unused*/, _stringLiteral2211845893, /*hidden argument*/NULL);
		V_3 = L_42;
		int32_t L_43 = V_2;
		IntPtr_t L_44 = V_3;
		IntPtr_t L_45 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		IntPtr_t L_46 = AndroidJNI_NewObjectArray_m2427147689(NULL /*static, unused*/, L_43, L_44, L_45, /*hidden argument*/NULL);
		V_4 = L_46;
		V_5 = 0;
		goto IL_015f;
	}

IL_013c:
	{
		StringU5BU5D_t4054002952* L_47 = V_1;
		int32_t L_48 = V_5;
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, L_48);
		int32_t L_49 = L_48;
		String_t* L_50 = (L_47)->GetAt(static_cast<il2cpp_array_size_t>(L_49));
		IntPtr_t L_51 = AndroidJNISafe_NewStringUTF_m1650627069(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		V_6 = L_51;
		IntPtr_t L_52 = V_4;
		int32_t L_53 = V_5;
		IntPtr_t L_54 = V_6;
		AndroidJNI_SetObjectArrayElement_m2717496490(NULL /*static, unused*/, L_52, L_53, L_54, /*hidden argument*/NULL);
		IntPtr_t L_55 = V_6;
		AndroidJNISafe_DeleteLocalRef_m2718059367(NULL /*static, unused*/, L_55, /*hidden argument*/NULL);
		int32_t L_56 = V_5;
		V_5 = ((int32_t)((int32_t)L_56+(int32_t)1));
	}

IL_015f:
	{
		int32_t L_57 = V_5;
		int32_t L_58 = V_2;
		if ((((int32_t)L_57) < ((int32_t)L_58)))
		{
			goto IL_013c;
		}
	}
	{
		IntPtr_t L_59 = V_3;
		AndroidJNISafe_DeleteLocalRef_m2718059367(NULL /*static, unused*/, L_59, /*hidden argument*/NULL);
		IntPtr_t L_60 = V_4;
		return L_60;
	}

IL_0170:
	{
		Type_t * L_61 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_62 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(AndroidJavaObject_t2362096582_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_61) == ((Il2CppObject*)(Type_t *)L_62))))
		{
			goto IL_024b;
		}
	}
	{
		Il2CppArray * L_63 = ___array0;
		V_7 = ((AndroidJavaObjectU5BU5D_t3281907299*)Castclass(L_63, AndroidJavaObjectU5BU5D_t3281907299_il2cpp_TypeInfo_var));
		Il2CppArray * L_64 = ___array0;
		NullCheck(L_64);
		int32_t L_65 = Array_GetLength_m1450876743(L_64, 0, /*hidden argument*/NULL);
		V_8 = L_65;
		int32_t L_66 = V_8;
		V_9 = ((IntPtrU5BU5D_t3228729122*)SZArrayNew(IntPtrU5BU5D_t3228729122_il2cpp_TypeInfo_var, (uint32_t)L_66));
		IntPtr_t L_67 = AndroidJNISafe_FindClass_m952975504(NULL /*static, unused*/, _stringLiteral2080463411, /*hidden argument*/NULL);
		V_10 = L_67;
		IntPtr_t L_68 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		V_11 = L_68;
		V_12 = 0;
		goto IL_022d;
	}

IL_01b5:
	{
		AndroidJavaObjectU5BU5D_t3281907299* L_69 = V_7;
		int32_t L_70 = V_12;
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, L_70);
		int32_t L_71 = L_70;
		AndroidJavaObject_t2362096582 * L_72 = (L_69)->GetAt(static_cast<il2cpp_array_size_t>(L_71));
		if (!L_72)
		{
			goto IL_0214;
		}
	}
	{
		IntPtrU5BU5D_t3228729122* L_73 = V_9;
		int32_t L_74 = V_12;
		NullCheck(L_73);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_73, L_74);
		AndroidJavaObjectU5BU5D_t3281907299* L_75 = V_7;
		int32_t L_76 = V_12;
		NullCheck(L_75);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_75, L_76);
		int32_t L_77 = L_76;
		AndroidJavaObject_t2362096582 * L_78 = (L_75)->GetAt(static_cast<il2cpp_array_size_t>(L_77));
		NullCheck(L_78);
		IntPtr_t L_79 = AndroidJavaObject_GetRawObject_m4031496215(L_78, /*hidden argument*/NULL);
		(*(IntPtr_t*)((L_73)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_74)))) = L_79;
		AndroidJavaObjectU5BU5D_t3281907299* L_80 = V_7;
		int32_t L_81 = V_12;
		NullCheck(L_80);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_80, L_81);
		int32_t L_82 = L_81;
		AndroidJavaObject_t2362096582 * L_83 = (L_80)->GetAt(static_cast<il2cpp_array_size_t>(L_82));
		NullCheck(L_83);
		IntPtr_t L_84 = AndroidJavaObject_GetRawClass_m842200578(L_83, /*hidden argument*/NULL);
		V_13 = L_84;
		IntPtr_t L_85 = V_11;
		IntPtr_t L_86 = V_13;
		bool L_87 = IntPtr_op_Inequality_m10053967(NULL /*static, unused*/, L_85, L_86, /*hidden argument*/NULL);
		if (!L_87)
		{
			goto IL_020f;
		}
	}
	{
		IntPtr_t L_88 = V_11;
		IntPtr_t L_89 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_90 = IntPtr_op_Equality_m72843924(NULL /*static, unused*/, L_88, L_89, /*hidden argument*/NULL);
		if (!L_90)
		{
			goto IL_020b;
		}
	}
	{
		IntPtr_t L_91 = V_13;
		V_11 = L_91;
		goto IL_020f;
	}

IL_020b:
	{
		IntPtr_t L_92 = V_10;
		V_11 = L_92;
	}

IL_020f:
	{
		goto IL_0227;
	}

IL_0214:
	{
		IntPtrU5BU5D_t3228729122* L_93 = V_9;
		int32_t L_94 = V_12;
		NullCheck(L_93);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_93, L_94);
		IntPtr_t L_95 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		(*(IntPtr_t*)((L_93)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_94)))) = L_95;
	}

IL_0227:
	{
		int32_t L_96 = V_12;
		V_12 = ((int32_t)((int32_t)L_96+(int32_t)1));
	}

IL_022d:
	{
		int32_t L_97 = V_12;
		int32_t L_98 = V_8;
		if ((((int32_t)L_97) < ((int32_t)L_98)))
		{
			goto IL_01b5;
		}
	}
	{
		IntPtrU5BU5D_t3228729122* L_99 = V_9;
		IntPtr_t L_100 = V_11;
		IntPtr_t L_101 = AndroidJNISafe_ToObjectArray_m2343440750(NULL /*static, unused*/, L_99, L_100, /*hidden argument*/NULL);
		V_14 = L_101;
		IntPtr_t L_102 = V_10;
		AndroidJNISafe_DeleteLocalRef_m2718059367(NULL /*static, unused*/, L_102, /*hidden argument*/NULL);
		IntPtr_t L_103 = V_14;
		return L_103;
	}

IL_024b:
	{
		Type_t * L_104 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_105 = String_Concat_m2809334143(NULL /*static, unused*/, _stringLiteral4146613544, L_104, _stringLiteral39, /*hidden argument*/NULL);
		Exception_t3991598821 * L_106 = (Exception_t3991598821 *)il2cpp_codegen_object_new(Exception_t3991598821_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_106, L_105, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_106);
	}

IL_0261:
	{
		IntPtr_t L_107 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		return L_107;
	}
}
// System.IntPtr UnityEngine._AndroidJNIHelper::GetConstructorID(System.IntPtr,System.Object[])
extern "C"  IntPtr_t _AndroidJNIHelper_GetConstructorID_m955179592 (Il2CppObject * __this /* static, unused */, IntPtr_t ___jclass0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___jclass0;
		ObjectU5BU5D_t1108656482* L_1 = ___args1;
		String_t* L_2 = _AndroidJNIHelper_GetSignature_m357465637(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_3 = AndroidJNIHelper_GetConstructorID_m2334065185(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.IntPtr UnityEngine._AndroidJNIHelper::GetMethodID(System.IntPtr,System.String,System.Object[],System.Boolean)
extern "C"  IntPtr_t _AndroidJNIHelper_GetMethodID_m4173184412 (Il2CppObject * __this /* static, unused */, IntPtr_t ___jclass0, String_t* ___methodName1, ObjectU5BU5D_t1108656482* ___args2, bool ___isStatic3, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___jclass0;
		String_t* L_1 = ___methodName1;
		ObjectU5BU5D_t1108656482* L_2 = ___args2;
		String_t* L_3 = _AndroidJNIHelper_GetSignature_m357465637(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		bool L_4 = ___isStatic3;
		IntPtr_t L_5 = AndroidJNIHelper_GetMethodID_m951538837(NULL /*static, unused*/, L_0, L_1, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.IntPtr UnityEngine._AndroidJNIHelper::GetConstructorID(System.IntPtr,System.String)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidReflection_t1838616560_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1818100338;
extern const uint32_t _AndroidJNIHelper_GetConstructorID_m3153181464_MetadataUsageId;
extern "C"  IntPtr_t _AndroidJNIHelper_GetConstructorID_m3153181464 (Il2CppObject * __this /* static, unused */, IntPtr_t ___jclass0, String_t* ___signature1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (_AndroidJNIHelper_GetConstructorID_m3153181464_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t3991598821 * V_1 = NULL;
	IntPtr_t V_2;
	memset(&V_2, 0, sizeof(V_2));
	IntPtr_t V_3;
	memset(&V_3, 0, sizeof(V_3));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		V_0 = L_0;
	}

IL_0006:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			{
				IntPtr_t L_1 = ___jclass0;
				String_t* L_2 = ___signature1;
				IL2CPP_RUNTIME_CLASS_INIT(AndroidReflection_t1838616560_il2cpp_TypeInfo_var);
				IntPtr_t L_3 = AndroidReflection_GetConstructorMember_m671869730(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
				V_0 = L_3;
				IntPtr_t L_4 = V_0;
				IntPtr_t L_5 = AndroidJNISafe_FromReflectedMethod_m103801358(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
				V_3 = L_5;
				IL2CPP_LEAVE(0x52, FINALLY_004b);
			}

IL_001a:
			{
				; // IL_001a: leave IL_0052
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t3991598821 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_001f;
			throw e;
		}

CATCH_001f:
		{ // begin catch(System.Exception)
			{
				V_1 = ((Exception_t3991598821 *)__exception_local);
				IntPtr_t L_6 = ___jclass0;
				String_t* L_7 = ___signature1;
				IntPtr_t L_8 = AndroidJNISafe_GetMethodID_m3184876807(NULL /*static, unused*/, L_6, _stringLiteral1818100338, L_7, /*hidden argument*/NULL);
				V_2 = L_8;
				IntPtr_t L_9 = V_2;
				IntPtr_t L_10 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
				bool L_11 = IntPtr_op_Inequality_m10053967(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
				if (!L_11)
				{
					goto IL_0044;
				}
			}

IL_003d:
			{
				IntPtr_t L_12 = V_2;
				V_3 = L_12;
				IL2CPP_LEAVE(0x52, FINALLY_004b);
			}

IL_0044:
			{
				Exception_t3991598821 * L_13 = V_1;
				IL2CPP_RAISE_MANAGED_EXCEPTION(L_13);
			}

IL_0046:
			{
				IL2CPP_LEAVE(0x52, FINALLY_004b);
			}
		} // end catch (depth: 2)
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_004b;
	}

FINALLY_004b:
	{ // begin finally (depth: 1)
		IntPtr_t L_14 = V_0;
		AndroidJNISafe_DeleteLocalRef_m2718059367(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(75)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(75)
	{
		IL2CPP_JUMP_TBL(0x52, IL_0052)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0052:
	{
		IntPtr_t L_15 = V_3;
		return L_15;
	}
}
// System.IntPtr UnityEngine._AndroidJNIHelper::GetMethodID(System.IntPtr,System.String,System.String,System.Boolean)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidReflection_t1838616560_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern const uint32_t _AndroidJNIHelper_GetMethodID_m3581696140_MetadataUsageId;
extern "C"  IntPtr_t _AndroidJNIHelper_GetMethodID_m3581696140 (Il2CppObject * __this /* static, unused */, IntPtr_t ___jclass0, String_t* ___methodName1, String_t* ___signature2, bool ___isStatic3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (_AndroidJNIHelper_GetMethodID_m3581696140_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t3991598821 * V_1 = NULL;
	IntPtr_t V_2;
	memset(&V_2, 0, sizeof(V_2));
	IntPtr_t V_3;
	memset(&V_3, 0, sizeof(V_3));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		V_0 = L_0;
	}

IL_0006:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			{
				IntPtr_t L_1 = ___jclass0;
				String_t* L_2 = ___methodName1;
				String_t* L_3 = ___signature2;
				bool L_4 = ___isStatic3;
				IL2CPP_RUNTIME_CLASS_INIT(AndroidReflection_t1838616560_il2cpp_TypeInfo_var);
				IntPtr_t L_5 = AndroidReflection_GetMethodMember_m2966799924(NULL /*static, unused*/, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
				V_0 = L_5;
				IntPtr_t L_6 = V_0;
				IntPtr_t L_7 = AndroidJNISafe_FromReflectedMethod_m103801358(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
				V_3 = L_7;
				IL2CPP_LEAVE(0x51, FINALLY_004a);
			}

IL_001c:
			{
				; // IL_001c: leave IL_0051
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t3991598821 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0021;
			throw e;
		}

CATCH_0021:
		{ // begin catch(System.Exception)
			{
				V_1 = ((Exception_t3991598821 *)__exception_local);
				IntPtr_t L_8 = ___jclass0;
				String_t* L_9 = ___methodName1;
				String_t* L_10 = ___signature2;
				bool L_11 = ___isStatic3;
				IntPtr_t L_12 = _AndroidJNIHelper_GetMethodIDFallback_m2222932750(NULL /*static, unused*/, L_8, L_9, L_10, L_11, /*hidden argument*/NULL);
				V_2 = L_12;
				IntPtr_t L_13 = V_2;
				IntPtr_t L_14 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
				bool L_15 = IntPtr_op_Inequality_m10053967(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
				if (!L_15)
				{
					goto IL_0043;
				}
			}

IL_003c:
			{
				IntPtr_t L_16 = V_2;
				V_3 = L_16;
				IL2CPP_LEAVE(0x51, FINALLY_004a);
			}

IL_0043:
			{
				Exception_t3991598821 * L_17 = V_1;
				IL2CPP_RAISE_MANAGED_EXCEPTION(L_17);
			}

IL_0045:
			{
				IL2CPP_LEAVE(0x51, FINALLY_004a);
			}
		} // end catch (depth: 2)
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_004a;
	}

FINALLY_004a:
	{ // begin finally (depth: 1)
		IntPtr_t L_18 = V_0;
		AndroidJNISafe_DeleteLocalRef_m2718059367(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(74)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(74)
	{
		IL2CPP_JUMP_TBL(0x51, IL_0051)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0051:
	{
		IntPtr_t L_19 = V_3;
		return L_19;
	}
}
// System.IntPtr UnityEngine._AndroidJNIHelper::GetMethodIDFallback(System.IntPtr,System.String,System.String,System.Boolean)
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t _AndroidJNIHelper_GetMethodIDFallback_m2222932750_MetadataUsageId;
extern "C"  IntPtr_t _AndroidJNIHelper_GetMethodIDFallback_m2222932750 (Il2CppObject * __this /* static, unused */, IntPtr_t ___jclass0, String_t* ___methodName1, String_t* ___signature2, bool ___isStatic3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (_AndroidJNIHelper_GetMethodIDFallback_m2222932750_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	IntPtr_t G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			bool L_0 = ___isStatic3;
			if (!L_0)
			{
				goto IL_0013;
			}
		}

IL_0006:
		{
			IntPtr_t L_1 = ___jclass0;
			String_t* L_2 = ___methodName1;
			String_t* L_3 = ___signature2;
			IntPtr_t L_4 = AndroidJNISafe_GetStaticMethodID_m2373470649(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
			G_B3_0 = L_4;
			goto IL_001b;
		}

IL_0013:
		{
			IntPtr_t L_5 = ___jclass0;
			String_t* L_6 = ___methodName1;
			String_t* L_7 = ___signature2;
			IntPtr_t L_8 = AndroidJNISafe_GetMethodID_m3184876807(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
			G_B3_0 = L_8;
		}

IL_001b:
		{
			V_0 = G_B3_0;
			goto IL_0032;
		}

IL_0021:
		{
			; // IL_0021: leave IL_002c
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0026;
		throw e;
	}

CATCH_0026:
	{ // begin catch(System.Exception)
		goto IL_002c;
	} // end catch (depth: 1)

IL_002c:
	{
		IntPtr_t L_9 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		return L_9;
	}

IL_0032:
	{
		IntPtr_t L_10 = V_0;
		return L_10;
	}
}
// System.IntPtr UnityEngine._AndroidJNIHelper::GetFieldID(System.IntPtr,System.String,System.String,System.Boolean)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidReflection_t1838616560_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern const uint32_t _AndroidJNIHelper_GetFieldID_m597111337_MetadataUsageId;
extern "C"  IntPtr_t _AndroidJNIHelper_GetFieldID_m597111337 (Il2CppObject * __this /* static, unused */, IntPtr_t ___jclass0, String_t* ___fieldName1, String_t* ___signature2, bool ___isStatic3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (_AndroidJNIHelper_GetFieldID_m597111337_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t3991598821 * V_1 = NULL;
	IntPtr_t V_2;
	memset(&V_2, 0, sizeof(V_2));
	IntPtr_t V_3;
	memset(&V_3, 0, sizeof(V_3));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	IntPtr_t G_B6_0;
	memset(&G_B6_0, 0, sizeof(G_B6_0));
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		V_0 = L_0;
	}

IL_0006:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			{
				IntPtr_t L_1 = ___jclass0;
				String_t* L_2 = ___fieldName1;
				String_t* L_3 = ___signature2;
				bool L_4 = ___isStatic3;
				IL2CPP_RUNTIME_CLASS_INIT(AndroidReflection_t1838616560_il2cpp_TypeInfo_var);
				IntPtr_t L_5 = AndroidReflection_GetFieldMember_m2787640223(NULL /*static, unused*/, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
				V_0 = L_5;
				IntPtr_t L_6 = V_0;
				IntPtr_t L_7 = AndroidJNISafe_FromReflectedField_m2369452317(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
				V_3 = L_7;
				IL2CPP_LEAVE(0x63, FINALLY_005c);
			}

IL_001c:
			{
				; // IL_001c: leave IL_0063
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t3991598821 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0021;
			throw e;
		}

CATCH_0021:
		{ // begin catch(System.Exception)
			{
				V_1 = ((Exception_t3991598821 *)__exception_local);
				bool L_8 = ___isStatic3;
				if (!L_8)
				{
					goto IL_0035;
				}
			}

IL_0028:
			{
				IntPtr_t L_9 = ___jclass0;
				String_t* L_10 = ___fieldName1;
				String_t* L_11 = ___signature2;
				IntPtr_t L_12 = AndroidJNISafe_GetStaticFieldID_m3373414124(NULL /*static, unused*/, L_9, L_10, L_11, /*hidden argument*/NULL);
				G_B6_0 = L_12;
				goto IL_003d;
			}

IL_0035:
			{
				IntPtr_t L_13 = ___jclass0;
				String_t* L_14 = ___fieldName1;
				String_t* L_15 = ___signature2;
				IntPtr_t L_16 = AndroidJNISafe_GetFieldID_m1044283870(NULL /*static, unused*/, L_13, L_14, L_15, /*hidden argument*/NULL);
				G_B6_0 = L_16;
			}

IL_003d:
			{
				V_2 = G_B6_0;
				IntPtr_t L_17 = V_2;
				IntPtr_t L_18 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
				bool L_19 = IntPtr_op_Inequality_m10053967(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
				if (!L_19)
				{
					goto IL_0055;
				}
			}

IL_004e:
			{
				IntPtr_t L_20 = V_2;
				V_3 = L_20;
				IL2CPP_LEAVE(0x63, FINALLY_005c);
			}

IL_0055:
			{
				Exception_t3991598821 * L_21 = V_1;
				IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
			}

IL_0057:
			{
				IL2CPP_LEAVE(0x63, FINALLY_005c);
			}
		} // end catch (depth: 2)
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_005c;
	}

FINALLY_005c:
	{ // begin finally (depth: 1)
		IntPtr_t L_22 = V_0;
		AndroidJNISafe_DeleteLocalRef_m2718059367(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(92)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(92)
	{
		IL2CPP_JUMP_TBL(0x63, IL_0063)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0063:
	{
		IntPtr_t L_23 = V_3;
		return L_23;
	}
}
// System.String UnityEngine._AndroidJNIHelper::GetSignature(System.Object)
extern const Il2CppType* Int32_t1153838500_0_0_0_var;
extern const Il2CppType* Boolean_t476798718_0_0_0_var;
extern const Il2CppType* Byte_t2862609660_0_0_0_var;
extern const Il2CppType* Int16_t1153838442_0_0_0_var;
extern const Il2CppType* Int64_t1153838595_0_0_0_var;
extern const Il2CppType* Single_t4291918972_0_0_0_var;
extern const Il2CppType* Double_t3868226565_0_0_0_var;
extern const Il2CppType* Char_t2862622538_0_0_0_var;
extern const Il2CppType* String_t_0_0_0_var;
extern const Il2CppType* AndroidJavaRunnable_t1289602340_0_0_0_var;
extern const Il2CppType* AndroidJavaClass_t1816259147_0_0_0_var;
extern const Il2CppType* AndroidJavaObject_t2362096582_0_0_0_var;
extern const Il2CppType* Il2CppArray_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidReflection_t1838616560_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidJavaProxy_t1828457281_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidJavaObject_t2362096582_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppClass* StringBuilder_t243639308_il2cpp_TypeInfo_var;
extern const MethodInfo* AndroidJavaObject_Call_TisString_t_m370002392_MethodInfo_var;
extern const MethodInfo* AndroidJavaObject_Call_TisAndroidJavaObject_t2362096582_m301711009_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1601768860;
extern Il2CppCodeGenString* _stringLiteral73;
extern Il2CppCodeGenString* _stringLiteral90;
extern Il2CppCodeGenString* _stringLiteral66;
extern Il2CppCodeGenString* _stringLiteral83;
extern Il2CppCodeGenString* _stringLiteral74;
extern Il2CppCodeGenString* _stringLiteral70;
extern Il2CppCodeGenString* _stringLiteral68;
extern Il2CppCodeGenString* _stringLiteral67;
extern Il2CppCodeGenString* _stringLiteral1379658506;
extern Il2CppCodeGenString* _stringLiteral76;
extern Il2CppCodeGenString* _stringLiteral4219659009;
extern Il2CppCodeGenString* _stringLiteral59;
extern Il2CppCodeGenString* _stringLiteral3133188478;
extern Il2CppCodeGenString* _stringLiteral1518216451;
extern Il2CppCodeGenString* _stringLiteral1950568386;
extern Il2CppCodeGenString* _stringLiteral1101415206;
extern Il2CppCodeGenString* _stringLiteral718587265;
extern Il2CppCodeGenString* _stringLiteral2576048763;
extern Il2CppCodeGenString* _stringLiteral1303;
extern Il2CppCodeGenString* _stringLiteral96757556;
extern Il2CppCodeGenString* _stringLiteral555127957;
extern const uint32_t _AndroidJNIHelper_GetSignature_m1099812039_MetadataUsageId;
extern "C"  String_t* _AndroidJNIHelper_GetSignature_m1099812039 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (_AndroidJNIHelper_GetSignature_m1099812039_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	AndroidJavaObject_t2362096582 * V_1 = NULL;
	AndroidJavaObject_t2362096582 * V_2 = NULL;
	AndroidJavaObject_t2362096582 * V_3 = NULL;
	StringBuilder_t243639308 * V_4 = NULL;
	String_t* V_5 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	Type_t * G_B5_0 = NULL;
	int32_t G_B46_0 = 0;
	ObjectU5BU5D_t1108656482* G_B46_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B46_2 = NULL;
	int32_t G_B45_0 = 0;
	ObjectU5BU5D_t1108656482* G_B45_1 = NULL;
	ObjectU5BU5D_t1108656482* G_B45_2 = NULL;
	String_t* G_B47_0 = NULL;
	int32_t G_B47_1 = 0;
	ObjectU5BU5D_t1108656482* G_B47_2 = NULL;
	ObjectU5BU5D_t1108656482* G_B47_3 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return _stringLiteral1601768860;
	}

IL_000c:
	{
		Il2CppObject * L_1 = ___obj0;
		if (!((Type_t *)IsInstClass(L_1, Type_t_il2cpp_TypeInfo_var)))
		{
			goto IL_0022;
		}
	}
	{
		Il2CppObject * L_2 = ___obj0;
		G_B5_0 = ((Type_t *)CastclassClass(L_2, Type_t_il2cpp_TypeInfo_var));
		goto IL_0028;
	}

IL_0022:
	{
		Il2CppObject * L_3 = ___obj0;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m2022236990(L_3, /*hidden argument*/NULL);
		G_B5_0 = L_4;
	}

IL_0028:
	{
		V_0 = G_B5_0;
		Type_t * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(AndroidReflection_t1838616560_il2cpp_TypeInfo_var);
		bool L_6 = AndroidReflection_IsPrimitive_m2208855481(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0111;
		}
	}
	{
		Type_t * L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int32_t1153838500_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_9 = Type_Equals_m3120004755(L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_004f;
		}
	}
	{
		return _stringLiteral73;
	}

IL_004f:
	{
		Type_t * L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_11 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Boolean_t476798718_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_10);
		bool L_12 = Type_Equals_m3120004755(L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_006a;
		}
	}
	{
		return _stringLiteral90;
	}

IL_006a:
	{
		Type_t * L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_14 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Byte_t2862609660_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_13);
		bool L_15 = Type_Equals_m3120004755(L_13, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0085;
		}
	}
	{
		return _stringLiteral66;
	}

IL_0085:
	{
		Type_t * L_16 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_17 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int16_t1153838442_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_16);
		bool L_18 = Type_Equals_m3120004755(L_16, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00a0;
		}
	}
	{
		return _stringLiteral83;
	}

IL_00a0:
	{
		Type_t * L_19 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int64_t1153838595_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_19);
		bool L_21 = Type_Equals_m3120004755(L_19, L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00bb;
		}
	}
	{
		return _stringLiteral74;
	}

IL_00bb:
	{
		Type_t * L_22 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_23 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Single_t4291918972_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_22);
		bool L_24 = Type_Equals_m3120004755(L_22, L_23, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00d6;
		}
	}
	{
		return _stringLiteral70;
	}

IL_00d6:
	{
		Type_t * L_25 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Double_t3868226565_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_25);
		bool L_27 = Type_Equals_m3120004755(L_25, L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_00f1;
		}
	}
	{
		return _stringLiteral68;
	}

IL_00f1:
	{
		Type_t * L_28 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_29 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Char_t2862622538_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_28);
		bool L_30 = Type_Equals_m3120004755(L_28, L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_010c;
		}
	}
	{
		return _stringLiteral67;
	}

IL_010c:
	{
		goto IL_02ba;
	}

IL_0111:
	{
		Type_t * L_31 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_32 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_31);
		bool L_33 = Type_Equals_m3120004755(L_31, L_32, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_012c;
		}
	}
	{
		return _stringLiteral1379658506;
	}

IL_012c:
	{
		Il2CppObject * L_34 = ___obj0;
		if (!((AndroidJavaProxy_t1828457281 *)IsInstClass(L_34, AndroidJavaProxy_t1828457281_il2cpp_TypeInfo_var)))
		{
			goto IL_016e;
		}
	}
	{
		Il2CppObject * L_35 = ___obj0;
		NullCheck(((AndroidJavaProxy_t1828457281 *)CastclassClass(L_35, AndroidJavaProxy_t1828457281_il2cpp_TypeInfo_var)));
		AndroidJavaClass_t1816259147 * L_36 = ((AndroidJavaProxy_t1828457281 *)CastclassClass(L_35, AndroidJavaProxy_t1828457281_il2cpp_TypeInfo_var))->get_javaInterface_0();
		NullCheck(L_36);
		IntPtr_t L_37 = AndroidJavaObject_GetRawClass_m842200578(L_36, /*hidden argument*/NULL);
		AndroidJavaObject_t2362096582 * L_38 = (AndroidJavaObject_t2362096582 *)il2cpp_codegen_object_new(AndroidJavaObject_t2362096582_il2cpp_TypeInfo_var);
		AndroidJavaObject__ctor_m2221125099(L_38, L_37, /*hidden argument*/NULL);
		V_1 = L_38;
		AndroidJavaObject_t2362096582 * L_39 = V_1;
		NullCheck(L_39);
		String_t* L_40 = AndroidJavaObject_Call_TisString_t_m370002392(L_39, _stringLiteral4219659009, ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/AndroidJavaObject_Call_TisString_t_m370002392_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_41 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral76, L_40, _stringLiteral59, /*hidden argument*/NULL);
		return L_41;
	}

IL_016e:
	{
		Type_t * L_42 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_43 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(AndroidJavaRunnable_t1289602340_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_42);
		bool L_44 = Type_Equals_m3120004755(L_42, L_43, /*hidden argument*/NULL);
		if (!L_44)
		{
			goto IL_0189;
		}
	}
	{
		return _stringLiteral3133188478;
	}

IL_0189:
	{
		Type_t * L_45 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_46 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(AndroidJavaClass_t1816259147_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_45);
		bool L_47 = Type_Equals_m3120004755(L_45, L_46, /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_01a4;
		}
	}
	{
		return _stringLiteral1518216451;
	}

IL_01a4:
	{
		Type_t * L_48 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_49 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(AndroidJavaObject_t2362096582_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_48);
		bool L_50 = Type_Equals_m3120004755(L_48, L_49, /*hidden argument*/NULL);
		if (!L_50)
		{
			goto IL_0218;
		}
	}
	{
		Il2CppObject * L_51 = ___obj0;
		Type_t * L_52 = V_0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_51) == ((Il2CppObject*)(Type_t *)L_52))))
		{
			goto IL_01c6;
		}
	}
	{
		return _stringLiteral1601768860;
	}

IL_01c6:
	{
		Il2CppObject * L_53 = ___obj0;
		V_2 = ((AndroidJavaObject_t2362096582 *)CastclassClass(L_53, AndroidJavaObject_t2362096582_il2cpp_TypeInfo_var));
		AndroidJavaObject_t2362096582 * L_54 = V_2;
		NullCheck(L_54);
		AndroidJavaObject_t2362096582 * L_55 = AndroidJavaObject_Call_TisAndroidJavaObject_t2362096582_m301711009(L_54, _stringLiteral1950568386, ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/AndroidJavaObject_Call_TisAndroidJavaObject_t2362096582_m301711009_MethodInfo_var);
		V_3 = L_55;
	}

IL_01df:
	try
	{ // begin try (depth: 1)
		{
			AndroidJavaObject_t2362096582 * L_56 = V_3;
			NullCheck(L_56);
			String_t* L_57 = AndroidJavaObject_Call_TisString_t_m370002392(L_56, _stringLiteral4219659009, ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/AndroidJavaObject_Call_TisString_t_m370002392_MethodInfo_var);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_58 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral76, L_57, _stringLiteral59, /*hidden argument*/NULL);
			V_5 = L_58;
			IL2CPP_LEAVE(0x2C0, FINALLY_020b);
		}

IL_0206:
		{
			; // IL_0206: leave IL_0218
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_020b;
	}

FINALLY_020b:
	{ // begin finally (depth: 1)
		{
			AndroidJavaObject_t2362096582 * L_59 = V_3;
			if (!L_59)
			{
				goto IL_0217;
			}
		}

IL_0211:
		{
			AndroidJavaObject_t2362096582 * L_60 = V_3;
			NullCheck(L_60);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_60);
		}

IL_0217:
		{
			IL2CPP_END_FINALLY(523)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(523)
	{
		IL2CPP_JUMP_TBL(0x2C0, IL_02c0)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0218:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_61 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Il2CppArray_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_62 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(AndroidReflection_t1838616560_il2cpp_TypeInfo_var);
		bool L_63 = AndroidReflection_IsAssignableFrom_m4117338608(NULL /*static, unused*/, L_61, L_62, /*hidden argument*/NULL);
		if (!L_63)
		{
			goto IL_0270;
		}
	}
	{
		Type_t * L_64 = V_0;
		NullCheck(L_64);
		int32_t L_65 = VirtFuncInvoker0< int32_t >::Invoke(44 /* System.Int32 System.Type::GetArrayRank() */, L_64);
		if ((((int32_t)L_65) == ((int32_t)1)))
		{
			goto IL_0244;
		}
	}
	{
		Exception_t3991598821 * L_66 = (Exception_t3991598821 *)il2cpp_codegen_object_new(Exception_t3991598821_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_66, _stringLiteral1101415206, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_66);
	}

IL_0244:
	{
		StringBuilder_t243639308 * L_67 = (StringBuilder_t243639308 *)il2cpp_codegen_object_new(StringBuilder_t243639308_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_67, /*hidden argument*/NULL);
		V_4 = L_67;
		StringBuilder_t243639308 * L_68 = V_4;
		NullCheck(L_68);
		StringBuilder_Append_m2143093878(L_68, ((int32_t)91), /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_69 = V_4;
		Type_t * L_70 = V_0;
		NullCheck(L_70);
		Type_t * L_71 = VirtFuncInvoker0< Type_t * >::Invoke(45 /* System.Type System.Type::GetElementType() */, L_70);
		String_t* L_72 = _AndroidJNIHelper_GetSignature_m1099812039(NULL /*static, unused*/, L_71, /*hidden argument*/NULL);
		NullCheck(L_69);
		StringBuilder_Append_m3898090075(L_69, L_72, /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_73 = V_4;
		NullCheck(L_73);
		String_t* L_74 = StringBuilder_ToString_m350379841(L_73, /*hidden argument*/NULL);
		return L_74;
	}

IL_0270:
	{
		ObjectU5BU5D_t1108656482* L_75 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_75);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_75, 0);
		ArrayElementTypeCheck (L_75, _stringLiteral718587265);
		(L_75)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral718587265);
		ObjectU5BU5D_t1108656482* L_76 = L_75;
		Type_t * L_77 = V_0;
		NullCheck(L_76);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_76, 1);
		ArrayElementTypeCheck (L_76, L_77);
		(L_76)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_77);
		ObjectU5BU5D_t1108656482* L_78 = L_76;
		NullCheck(L_78);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_78, 2);
		ArrayElementTypeCheck (L_78, _stringLiteral2576048763);
		(L_78)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral2576048763);
		ObjectU5BU5D_t1108656482* L_79 = L_78;
		Il2CppObject * L_80 = ___obj0;
		NullCheck(L_79);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_79, 3);
		ArrayElementTypeCheck (L_79, L_80);
		(L_79)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_80);
		ObjectU5BU5D_t1108656482* L_81 = L_79;
		NullCheck(L_81);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_81, 4);
		ArrayElementTypeCheck (L_81, _stringLiteral1303);
		(L_81)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral1303);
		ObjectU5BU5D_t1108656482* L_82 = L_81;
		Type_t * L_83 = V_0;
		Il2CppObject * L_84 = ___obj0;
		G_B45_0 = 5;
		G_B45_1 = L_82;
		G_B45_2 = L_82;
		if ((!(((Il2CppObject*)(Type_t *)L_83) == ((Il2CppObject*)(Il2CppObject *)L_84))))
		{
			G_B46_0 = 5;
			G_B46_1 = L_82;
			G_B46_2 = L_82;
			goto IL_02a9;
		}
	}
	{
		G_B47_0 = _stringLiteral96757556;
		G_B47_1 = G_B45_0;
		G_B47_2 = G_B45_1;
		G_B47_3 = G_B45_2;
		goto IL_02ae;
	}

IL_02a9:
	{
		G_B47_0 = _stringLiteral555127957;
		G_B47_1 = G_B46_0;
		G_B47_2 = G_B46_1;
		G_B47_3 = G_B46_2;
	}

IL_02ae:
	{
		NullCheck(G_B47_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B47_2, G_B47_1);
		ArrayElementTypeCheck (G_B47_2, G_B47_0);
		(G_B47_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B47_1), (Il2CppObject *)G_B47_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_85 = String_Concat_m3016520001(NULL /*static, unused*/, G_B47_3, /*hidden argument*/NULL);
		Exception_t3991598821 * L_86 = (Exception_t3991598821 *)il2cpp_codegen_object_new(Exception_t3991598821_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_86, L_85, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_86);
	}

IL_02ba:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_87 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_87;
	}

IL_02c0:
	{
		String_t* L_88 = V_5;
		return L_88;
	}
}
// System.String UnityEngine._AndroidJNIHelper::GetSignature(System.Object[])
extern Il2CppClass* StringBuilder_t243639308_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1357;
extern const uint32_t _AndroidJNIHelper_GetSignature_m357465637_MetadataUsageId;
extern "C"  String_t* _AndroidJNIHelper_GetSignature_m357465637 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (_AndroidJNIHelper_GetSignature_m357465637_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t243639308 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ObjectU5BU5D_t1108656482* V_2 = NULL;
	int32_t V_3 = 0;
	{
		StringBuilder_t243639308 * L_0 = (StringBuilder_t243639308 *)il2cpp_codegen_object_new(StringBuilder_t243639308_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t243639308 * L_1 = V_0;
		NullCheck(L_1);
		StringBuilder_Append_m2143093878(L_1, ((int32_t)40), /*hidden argument*/NULL);
		ObjectU5BU5D_t1108656482* L_2 = ___args0;
		V_2 = L_2;
		V_3 = 0;
		goto IL_002d;
	}

IL_0018:
	{
		ObjectU5BU5D_t1108656482* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_1 = L_6;
		StringBuilder_t243639308 * L_7 = V_0;
		Il2CppObject * L_8 = V_1;
		String_t* L_9 = _AndroidJNIHelper_GetSignature_m1099812039(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		StringBuilder_Append_m3898090075(L_7, L_9, /*hidden argument*/NULL);
		int32_t L_10 = V_3;
		V_3 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_002d:
	{
		int32_t L_11 = V_3;
		ObjectU5BU5D_t1108656482* L_12 = V_2;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_0018;
		}
	}
	{
		StringBuilder_t243639308 * L_13 = V_0;
		NullCheck(L_13);
		StringBuilder_Append_m3898090075(L_13, _stringLiteral1357, /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_14 = V_0;
		NullCheck(L_14);
		String_t* L_15 = StringBuilder_ToString_m350379841(L_14, /*hidden argument*/NULL);
		return L_15;
	}
}
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
extern "C"  void AddComponentMenu__ctor_m1521317712 (AddComponentMenu_t813859935 * __this, String_t* ___menuName0, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___menuName0;
		__this->set_m_AddComponentMenu_0(L_0);
		__this->set_m_Ordering_1(0);
		return;
	}
}
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String,System.Int32)
extern "C"  void AddComponentMenu__ctor_m3281913735 (AddComponentMenu_t813859935 * __this, String_t* ___menuName0, int32_t ___order1, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___menuName0;
		__this->set_m_AddComponentMenu_0(L_0);
		int32_t L_1 = ___order1;
		__this->set_m_Ordering_1(L_1);
		return;
	}
}
// System.Void UnityEngine.AndroidJavaClass::.ctor(System.String)
extern Il2CppClass* AndroidJavaObject_t2362096582_il2cpp_TypeInfo_var;
extern const uint32_t AndroidJavaClass__ctor_m2757518396_MetadataUsageId;
extern "C"  void AndroidJavaClass__ctor_m2757518396 (AndroidJavaClass_t1816259147 * __this, String_t* ___className0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidJavaClass__ctor_m2757518396_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidJavaObject_t2362096582_il2cpp_TypeInfo_var);
		AndroidJavaObject__ctor_m4108365065(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___className0;
		AndroidJavaClass__AndroidJavaClass_m1016029752(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AndroidJavaClass::.ctor(System.IntPtr)
extern Il2CppClass* AndroidJavaObject_t2362096582_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2764423363;
extern const uint32_t AndroidJavaClass__ctor_m2301749646_MetadataUsageId;
extern "C"  void AndroidJavaClass__ctor_m2301749646 (AndroidJavaClass_t1816259147 * __this, IntPtr_t ___jclass0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidJavaClass__ctor_m2301749646_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidJavaObject_t2362096582_il2cpp_TypeInfo_var);
		AndroidJavaObject__ctor_m4108365065(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = ___jclass0;
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_2 = IntPtr_op_Equality_m72843924(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		Exception_t3991598821 * L_3 = (Exception_t3991598821 *)il2cpp_codegen_object_new(Exception_t3991598821_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_3, _stringLiteral2764423363, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0021:
	{
		IntPtr_t L_4 = ___jclass0;
		IntPtr_t L_5 = AndroidJNI_NewGlobalRef_m172051970(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		((AndroidJavaObject_t2362096582 *)__this)->set_m_jclass_3(L_5);
		IntPtr_t L_6 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		((AndroidJavaObject_t2362096582 *)__this)->set_m_jobject_2(L_6);
		return;
	}
}
// System.Void UnityEngine.AndroidJavaClass::_AndroidJavaClass(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidJavaObject_t2362096582_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3701155940;
extern const uint32_t AndroidJavaClass__AndroidJavaClass_m1016029752_MetadataUsageId;
extern "C"  void AndroidJavaClass__AndroidJavaClass_m1016029752 (AndroidJavaClass_t1816259147 * __this, String_t* ___className0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidJavaClass__AndroidJavaClass_m1016029752_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AndroidJavaObject_t2362096582 * V_0 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___className0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3701155940, L_0, /*hidden argument*/NULL);
		AndroidJavaObject_DebugPrint_m4163122157(__this, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___className0;
		IL2CPP_RUNTIME_CLASS_INIT(AndroidJavaObject_t2362096582_il2cpp_TypeInfo_var);
		AndroidJavaObject_t2362096582 * L_3 = AndroidJavaObject_FindClass_m3919844898(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		AndroidJavaObject_t2362096582 * L_4 = V_0;
		NullCheck(L_4);
		IntPtr_t L_5 = AndroidJavaObject_GetRawObject_m4031496215(L_4, /*hidden argument*/NULL);
		IntPtr_t L_6 = AndroidJNI_NewGlobalRef_m172051970(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		((AndroidJavaObject_t2362096582 *)__this)->set_m_jclass_3(L_6);
		IntPtr_t L_7 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		((AndroidJavaObject_t2362096582 *)__this)->set_m_jobject_2(L_7);
		IL2CPP_LEAVE(0x46, FINALLY_0039);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0039;
	}

FINALLY_0039:
	{ // begin finally (depth: 1)
		{
			AndroidJavaObject_t2362096582 * L_8 = V_0;
			if (!L_8)
			{
				goto IL_0045;
			}
		}

IL_003f:
		{
			AndroidJavaObject_t2362096582 * L_9 = V_0;
			NullCheck(L_9);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_9);
		}

IL_0045:
		{
			IL2CPP_END_FINALLY(57)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(57)
	{
		IL2CPP_JUMP_TBL(0x46, IL_0046)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0046:
	{
		return;
	}
}
// System.Void UnityEngine.AndroidJavaException::.ctor(System.String,System.String)
extern "C"  void AndroidJavaException__ctor_m297033807 (AndroidJavaException_t125722146 * __this, String_t* ___message0, String_t* ___javaStackTrace1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception__ctor_m3870771296(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___javaStackTrace1;
		__this->set_mJavaStackTrace_11(L_1);
		return;
	}
}
// System.String UnityEngine.AndroidJavaException::get_StackTrace()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t AndroidJavaException_get_StackTrace_m3440401566_MetadataUsageId;
extern "C"  String_t* AndroidJavaException_get_StackTrace_m3440401566 (AndroidJavaException_t125722146 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidJavaException_get_StackTrace_m3440401566_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_mJavaStackTrace_11();
		String_t* L_1 = Exception_get_StackTrace_m382134187(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m138640077(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.AndroidJavaObject::.ctor(System.String,System.Object[])
extern "C"  void AndroidJavaObject__ctor_m219376901 (AndroidJavaObject_t2362096582 * __this, String_t* ___className0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method)
{
	{
		AndroidJavaObject__ctor_m4108365065(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___className0;
		ObjectU5BU5D_t1108656482* L_1 = ___args1;
		AndroidJavaObject__AndroidJavaObject_m3200907906(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AndroidJavaObject::.ctor(System.IntPtr)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1754557738;
extern const uint32_t AndroidJavaObject__ctor_m2221125099_MetadataUsageId;
extern "C"  void AndroidJavaObject__ctor_m2221125099 (AndroidJavaObject_t2362096582 * __this, IntPtr_t ___jobject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidJavaObject__ctor_m2221125099_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		AndroidJavaObject__ctor_m4108365065(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = ___jobject0;
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_2 = IntPtr_op_Equality_m72843924(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		Exception_t3991598821 * L_3 = (Exception_t3991598821 *)il2cpp_codegen_object_new(Exception_t3991598821_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_3, _stringLiteral1754557738, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0021:
	{
		IntPtr_t L_4 = ___jobject0;
		IntPtr_t L_5 = AndroidJNISafe_GetObjectClass_m3301160194(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		IntPtr_t L_6 = ___jobject0;
		IntPtr_t L_7 = AndroidJNI_NewGlobalRef_m172051970(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		__this->set_m_jobject_2(L_7);
		IntPtr_t L_8 = V_0;
		IntPtr_t L_9 = AndroidJNI_NewGlobalRef_m172051970(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		__this->set_m_jclass_3(L_9);
		IntPtr_t L_10 = V_0;
		AndroidJNISafe_DeleteLocalRef_m2718059367(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AndroidJavaObject::.ctor()
extern "C"  void AndroidJavaObject__ctor_m4108365065 (AndroidJavaObject_t2362096582 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AndroidJavaObject::.cctor()
extern "C"  void AndroidJavaObject__cctor_m2323169220 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.AndroidJavaObject::Dispose()
extern "C"  void AndroidJavaObject_Dispose_m962165510 (AndroidJavaObject_t2362096582 * __this, const MethodInfo* method)
{
	{
		AndroidJavaObject__Dispose_m3642455739(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AndroidJavaObject::Call(System.String,System.Object[])
extern "C"  void AndroidJavaObject_Call_m3151096341 (AndroidJavaObject_t2362096582 * __this, String_t* ___methodName0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___methodName0;
		ObjectU5BU5D_t1108656482* L_1 = ___args1;
		AndroidJavaObject__Call_m1041300874(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.IntPtr UnityEngine.AndroidJavaObject::GetRawObject()
extern "C"  IntPtr_t AndroidJavaObject_GetRawObject_m4031496215 (AndroidJavaObject_t2362096582 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = AndroidJavaObject__GetRawObject_m945549836(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.IntPtr UnityEngine.AndroidJavaObject::GetRawClass()
extern "C"  IntPtr_t AndroidJavaObject_GetRawClass_m842200578 (AndroidJavaObject_t2362096582 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = AndroidJavaObject__GetRawClass_m1158295917(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.AndroidJavaObject::DebugPrint(System.String)
extern Il2CppClass* AndroidJavaObject_t2362096582_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const uint32_t AndroidJavaObject_DebugPrint_m4163122157_MetadataUsageId;
extern "C"  void AndroidJavaObject_DebugPrint_m4163122157 (AndroidJavaObject_t2362096582 * __this, String_t* ___msg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidJavaObject_DebugPrint_m4163122157_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidJavaObject_t2362096582_il2cpp_TypeInfo_var);
		bool L_0 = ((AndroidJavaObject_t2362096582_StaticFields*)AndroidJavaObject_t2362096582_il2cpp_TypeInfo_var->static_fields)->get_enableDebugPrints_0();
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		String_t* L_1 = ___msg0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AndroidJavaObject::_AndroidJavaObject(System.String,System.Object[])
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidJavaObject_t2362096582_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral576933119;
extern const uint32_t AndroidJavaObject__AndroidJavaObject_m3200907906_MetadataUsageId;
extern "C"  void AndroidJavaObject__AndroidJavaObject_m3200907906 (AndroidJavaObject_t2362096582 * __this, String_t* ___className0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidJavaObject__AndroidJavaObject_m3200907906_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AndroidJavaObject_t2362096582 * V_0 = NULL;
	jvalueU5BU5D_t1723627146* V_1 = NULL;
	IntPtr_t V_2;
	memset(&V_2, 0, sizeof(V_2));
	IntPtr_t V_3;
	memset(&V_3, 0, sizeof(V_3));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___className0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral576933119, L_0, /*hidden argument*/NULL);
		AndroidJavaObject_DebugPrint_m4163122157(__this, L_1, /*hidden argument*/NULL);
		ObjectU5BU5D_t1108656482* L_2 = ___args1;
		if (L_2)
		{
			goto IL_001f;
		}
	}
	{
		___args1 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
	}

IL_001f:
	{
		String_t* L_3 = ___className0;
		IL2CPP_RUNTIME_CLASS_INIT(AndroidJavaObject_t2362096582_il2cpp_TypeInfo_var);
		AndroidJavaObject_t2362096582 * L_4 = AndroidJavaObject_FindClass_m3919844898(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
	}

IL_0026:
	try
	{ // begin try (depth: 1)
		{
			AndroidJavaObject_t2362096582 * L_5 = V_0;
			NullCheck(L_5);
			IntPtr_t L_6 = AndroidJavaObject_GetRawObject_m4031496215(L_5, /*hidden argument*/NULL);
			IntPtr_t L_7 = AndroidJNI_NewGlobalRef_m172051970(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
			__this->set_m_jclass_3(L_7);
			ObjectU5BU5D_t1108656482* L_8 = ___args1;
			jvalueU5BU5D_t1723627146* L_9 = AndroidJNIHelper_CreateJNIArgArray_m3777113646(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
			V_1 = L_9;
		}

IL_003e:
		try
		{ // begin try (depth: 2)
			IntPtr_t L_10 = __this->get_m_jclass_3();
			ObjectU5BU5D_t1108656482* L_11 = ___args1;
			IntPtr_t L_12 = AndroidJNIHelper_GetConstructorID_m4058417937(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
			V_2 = L_12;
			IntPtr_t L_13 = __this->get_m_jclass_3();
			IntPtr_t L_14 = V_2;
			jvalueU5BU5D_t1723627146* L_15 = V_1;
			IntPtr_t L_16 = AndroidJNISafe_NewObject_m2725834966(NULL /*static, unused*/, L_13, L_14, L_15, /*hidden argument*/NULL);
			V_3 = L_16;
			IntPtr_t L_17 = V_3;
			IntPtr_t L_18 = AndroidJNI_NewGlobalRef_m172051970(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
			__this->set_m_jobject_2(L_18);
			IntPtr_t L_19 = V_3;
			AndroidJNISafe_DeleteLocalRef_m2718059367(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x78, FINALLY_0070);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
			goto FINALLY_0070;
		}

FINALLY_0070:
		{ // begin finally (depth: 2)
			ObjectU5BU5D_t1108656482* L_20 = ___args1;
			jvalueU5BU5D_t1723627146* L_21 = V_1;
			AndroidJNIHelper_DeleteJNIArgArray_m4114677528(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(112)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(112)
		{
			IL2CPP_JUMP_TBL(0x78, IL_0078)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
		}

IL_0078:
		{
			IL2CPP_LEAVE(0x8A, FINALLY_007d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_007d;
	}

FINALLY_007d:
	{ // begin finally (depth: 1)
		{
			AndroidJavaObject_t2362096582 * L_22 = V_0;
			if (!L_22)
			{
				goto IL_0089;
			}
		}

IL_0083:
		{
			AndroidJavaObject_t2362096582 * L_23 = V_0;
			NullCheck(L_23);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_23);
		}

IL_0089:
		{
			IL2CPP_END_FINALLY(125)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(125)
	{
		IL2CPP_JUMP_TBL(0x8A, IL_008a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_008a:
	{
		return;
	}
}
// System.Void UnityEngine.AndroidJavaObject::Finalize()
extern "C"  void AndroidJavaObject_Finalize_m3018091193 (AndroidJavaObject_t2362096582 * __this, const MethodInfo* method)
{
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		VirtActionInvoker1< bool >::Invoke(5 /* System.Void UnityEngine.AndroidJavaObject::Dispose(System.Boolean) */, __this, (bool)1);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m3027285644(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.AndroidJavaObject::Dispose(System.Boolean)
extern "C"  void AndroidJavaObject_Dispose_m4080102909 (AndroidJavaObject_t2362096582 * __this, bool ___disposing0, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_disposed_1();
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		__this->set_m_disposed_1((bool)1);
		IntPtr_t L_1 = __this->get_m_jobject_2();
		AndroidJNISafe_DeleteGlobalRef_m3119229927(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_2 = __this->get_m_jclass_3();
		AndroidJNISafe_DeleteGlobalRef_m3119229927(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AndroidJavaObject::_Dispose()
extern "C"  void AndroidJavaObject__Dispose_m3642455739 (AndroidJavaObject_t2362096582 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker1< bool >::Invoke(5 /* System.Void UnityEngine.AndroidJavaObject::Dispose(System.Boolean) */, __this, (bool)1);
		GC_SuppressFinalize_m1160635446(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AndroidJavaObject::_Call(System.String,System.Object[])
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t AndroidJavaObject__Call_m1041300874_MetadataUsageId;
extern "C"  void AndroidJavaObject__Call_m1041300874 (AndroidJavaObject_t2362096582 * __this, String_t* ___methodName0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidJavaObject__Call_m1041300874_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	jvalueU5BU5D_t1723627146* V_1 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ObjectU5BU5D_t1108656482* L_0 = ___args1;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		___args1 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
	}

IL_000e:
	{
		IntPtr_t L_1 = __this->get_m_jclass_3();
		String_t* L_2 = ___methodName0;
		ObjectU5BU5D_t1108656482* L_3 = ___args1;
		IntPtr_t L_4 = AndroidJNIHelper_GetMethodID_m2032786277(NULL /*static, unused*/, L_1, L_2, L_3, (bool)0, /*hidden argument*/NULL);
		V_0 = L_4;
		ObjectU5BU5D_t1108656482* L_5 = ___args1;
		jvalueU5BU5D_t1723627146* L_6 = AndroidJNIHelper_CreateJNIArgArray_m3777113646(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
	}

IL_0024:
	try
	{ // begin try (depth: 1)
		IntPtr_t L_7 = __this->get_m_jobject_2();
		IntPtr_t L_8 = V_0;
		jvalueU5BU5D_t1723627146* L_9 = V_1;
		AndroidJNISafe_CallVoidMethod_m2934136849(NULL /*static, unused*/, L_7, L_8, L_9, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x3E, FINALLY_0036);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0036;
	}

FINALLY_0036:
	{ // begin finally (depth: 1)
		ObjectU5BU5D_t1108656482* L_10 = ___args1;
		jvalueU5BU5D_t1723627146* L_11 = V_1;
		AndroidJNIHelper_DeleteJNIArgArray_m4114677528(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(54)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(54)
	{
		IL2CPP_JUMP_TBL(0x3E, IL_003e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_003e:
	{
		return;
	}
}
// UnityEngine.AndroidJavaObject UnityEngine.AndroidJavaObject::AndroidJavaObjectDeleteLocalRef(System.IntPtr)
extern Il2CppClass* AndroidJavaObject_t2362096582_il2cpp_TypeInfo_var;
extern const uint32_t AndroidJavaObject_AndroidJavaObjectDeleteLocalRef_m4126635600_MetadataUsageId;
extern "C"  AndroidJavaObject_t2362096582 * AndroidJavaObject_AndroidJavaObjectDeleteLocalRef_m4126635600 (Il2CppObject * __this /* static, unused */, IntPtr_t ___jobject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidJavaObject_AndroidJavaObjectDeleteLocalRef_m4126635600_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AndroidJavaObject_t2362096582 * V_0 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___jobject0;
			AndroidJavaObject_t2362096582 * L_1 = (AndroidJavaObject_t2362096582 *)il2cpp_codegen_object_new(AndroidJavaObject_t2362096582_il2cpp_TypeInfo_var);
			AndroidJavaObject__ctor_m2221125099(L_1, L_0, /*hidden argument*/NULL);
			V_0 = L_1;
			IL2CPP_LEAVE(0x18, FINALLY_0011);
		}

IL_000c:
		{
			; // IL_000c: leave IL_0018
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0011;
	}

FINALLY_0011:
	{ // begin finally (depth: 1)
		IntPtr_t L_2 = ___jobject0;
		AndroidJNISafe_DeleteLocalRef_m2718059367(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(17)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(17)
	{
		IL2CPP_JUMP_TBL(0x18, IL_0018)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0018:
	{
		AndroidJavaObject_t2362096582 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.AndroidJavaClass UnityEngine.AndroidJavaObject::AndroidJavaClassDeleteLocalRef(System.IntPtr)
extern Il2CppClass* AndroidJavaClass_t1816259147_il2cpp_TypeInfo_var;
extern const uint32_t AndroidJavaObject_AndroidJavaClassDeleteLocalRef_m474662064_MetadataUsageId;
extern "C"  AndroidJavaClass_t1816259147 * AndroidJavaObject_AndroidJavaClassDeleteLocalRef_m474662064 (Il2CppObject * __this /* static, unused */, IntPtr_t ___jclass0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidJavaObject_AndroidJavaClassDeleteLocalRef_m474662064_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AndroidJavaClass_t1816259147 * V_0 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___jclass0;
			AndroidJavaClass_t1816259147 * L_1 = (AndroidJavaClass_t1816259147 *)il2cpp_codegen_object_new(AndroidJavaClass_t1816259147_il2cpp_TypeInfo_var);
			AndroidJavaClass__ctor_m2301749646(L_1, L_0, /*hidden argument*/NULL);
			V_0 = L_1;
			IL2CPP_LEAVE(0x18, FINALLY_0011);
		}

IL_000c:
		{
			; // IL_000c: leave IL_0018
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0011;
	}

FINALLY_0011:
	{ // begin finally (depth: 1)
		IntPtr_t L_2 = ___jclass0;
		AndroidJNISafe_DeleteLocalRef_m2718059367(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(17)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(17)
	{
		IL2CPP_JUMP_TBL(0x18, IL_0018)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0018:
	{
		AndroidJavaClass_t1816259147 * L_3 = V_0;
		return L_3;
	}
}
// System.IntPtr UnityEngine.AndroidJavaObject::_GetRawObject()
extern "C"  IntPtr_t AndroidJavaObject__GetRawObject_m945549836 (AndroidJavaObject_t2362096582 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = __this->get_m_jobject_2();
		return L_0;
	}
}
// System.IntPtr UnityEngine.AndroidJavaObject::_GetRawClass()
extern "C"  IntPtr_t AndroidJavaObject__GetRawClass_m1158295917 (AndroidJavaObject_t2362096582 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = __this->get_m_jclass_3();
		return L_0;
	}
}
// UnityEngine.AndroidJavaObject UnityEngine.AndroidJavaObject::FindClass(System.String)
extern Il2CppClass* AndroidJavaObject_t2362096582_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const MethodInfo* AndroidJavaObject_CallStatic_TisAndroidJavaObject_t2362096582_m3741769967_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3616599796;
extern const uint32_t AndroidJavaObject_FindClass_m3919844898_MetadataUsageId;
extern "C"  AndroidJavaObject_t2362096582 * AndroidJavaObject_FindClass_m3919844898 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidJavaObject_FindClass_m3919844898_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidJavaObject_t2362096582_il2cpp_TypeInfo_var);
		AndroidJavaClass_t1816259147 * L_0 = AndroidJavaObject_get_JavaLangClass_m1175199895(NULL /*static, unused*/, /*hidden argument*/NULL);
		ObjectU5BU5D_t1108656482* L_1 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_2 = ___name0;
		NullCheck(L_2);
		String_t* L_3 = String_Replace_m3369701083(L_2, ((int32_t)47), ((int32_t)46), /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		NullCheck(L_0);
		AndroidJavaObject_t2362096582 * L_4 = AndroidJavaObject_CallStatic_TisAndroidJavaObject_t2362096582_m3741769967(L_0, _stringLiteral3616599796, L_1, /*hidden argument*/AndroidJavaObject_CallStatic_TisAndroidJavaObject_t2362096582_m3741769967_MethodInfo_var);
		return L_4;
	}
}
// UnityEngine.AndroidJavaClass UnityEngine.AndroidJavaObject::get_JavaLangClass()
extern Il2CppClass* AndroidJavaObject_t2362096582_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidJavaClass_t1816259147_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1995981828;
extern const uint32_t AndroidJavaObject_get_JavaLangClass_m1175199895_MetadataUsageId;
extern "C"  AndroidJavaClass_t1816259147 * AndroidJavaObject_get_JavaLangClass_m1175199895 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidJavaObject_get_JavaLangClass_m1175199895_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidJavaObject_t2362096582_il2cpp_TypeInfo_var);
		AndroidJavaClass_t1816259147 * L_0 = ((AndroidJavaObject_t2362096582_StaticFields*)AndroidJavaObject_t2362096582_il2cpp_TypeInfo_var->static_fields)->get_s_JavaLangClass_4();
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_1 = AndroidJNISafe_FindClass_m952975504(NULL /*static, unused*/, _stringLiteral1995981828, /*hidden argument*/NULL);
		AndroidJavaClass_t1816259147 * L_2 = (AndroidJavaClass_t1816259147 *)il2cpp_codegen_object_new(AndroidJavaClass_t1816259147_il2cpp_TypeInfo_var);
		AndroidJavaClass__ctor_m2301749646(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AndroidJavaObject_t2362096582_il2cpp_TypeInfo_var);
		((AndroidJavaObject_t2362096582_StaticFields*)AndroidJavaObject_t2362096582_il2cpp_TypeInfo_var->static_fields)->set_s_JavaLangClass_4(L_2);
	}

IL_001e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AndroidJavaObject_t2362096582_il2cpp_TypeInfo_var);
		AndroidJavaClass_t1816259147 * L_3 = ((AndroidJavaObject_t2362096582_StaticFields*)AndroidJavaObject_t2362096582_il2cpp_TypeInfo_var->static_fields)->get_s_JavaLangClass_4();
		return L_3;
	}
}
// System.Void UnityEngine.AndroidJavaProxy::.ctor(System.String)
extern Il2CppClass* AndroidJavaClass_t1816259147_il2cpp_TypeInfo_var;
extern const uint32_t AndroidJavaProxy__ctor_m2087176370_MetadataUsageId;
extern "C"  void AndroidJavaProxy__ctor_m2087176370 (AndroidJavaProxy_t1828457281 * __this, String_t* ___javaInterface0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidJavaProxy__ctor_m2087176370_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___javaInterface0;
		AndroidJavaClass_t1816259147 * L_1 = (AndroidJavaClass_t1816259147 *)il2cpp_codegen_object_new(AndroidJavaClass_t1816259147_il2cpp_TypeInfo_var);
		AndroidJavaClass__ctor_m2757518396(L_1, L_0, /*hidden argument*/NULL);
		AndroidJavaProxy__ctor_m3193168274(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AndroidJavaProxy::.ctor(UnityEngine.AndroidJavaClass)
extern "C"  void AndroidJavaProxy__ctor_m3193168274 (AndroidJavaProxy_t1828457281 * __this, AndroidJavaClass_t1816259147 * ___javaInterface0, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		AndroidJavaClass_t1816259147 * L_0 = ___javaInterface0;
		__this->set_javaInterface_0(L_0);
		return;
	}
}
// System.Void UnityEngine.AndroidJavaRunnable::.ctor(System.Object,System.IntPtr)
extern "C"  void AndroidJavaRunnable__ctor_m1140889495 (AndroidJavaRunnable_t1289602340 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.AndroidJavaRunnable::Invoke()
extern "C"  void AndroidJavaRunnable_Invoke_m3101692593 (AndroidJavaRunnable_t1289602340 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		AndroidJavaRunnable_Invoke_m3101692593((AndroidJavaRunnable_t1289602340 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_AndroidJavaRunnable_t1289602340 (AndroidJavaRunnable_t1289602340 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.IAsyncResult UnityEngine.AndroidJavaRunnable::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * AndroidJavaRunnable_BeginInvoke_m364328122 (AndroidJavaRunnable_t1289602340 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void UnityEngine.AndroidJavaRunnable::EndInvoke(System.IAsyncResult)
extern "C"  void AndroidJavaRunnable_EndInvoke_m3666177319 (AndroidJavaRunnable_t1289602340 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.AndroidJavaRunnableProxy::.ctor(UnityEngine.AndroidJavaRunnable)
extern Il2CppCodeGenString* _stringLiteral2904464657;
extern const uint32_t AndroidJavaRunnableProxy__ctor_m1203400718_MetadataUsageId;
extern "C"  void AndroidJavaRunnableProxy__ctor_m1203400718 (AndroidJavaRunnableProxy_t1013435428 * __this, AndroidJavaRunnable_t1289602340 * ___runnable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidJavaRunnableProxy__ctor_m1203400718_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AndroidJavaProxy__ctor_m2087176370(__this, _stringLiteral2904464657, /*hidden argument*/NULL);
		AndroidJavaRunnable_t1289602340 * L_0 = ___runnable0;
		__this->set_mRunnable_1(L_0);
		return;
	}
}
// System.IntPtr UnityEngine.AndroidJNI::FindClass(System.String)
extern "C"  IntPtr_t AndroidJNI_FindClass_m1230259107 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	typedef IntPtr_t (*AndroidJNI_FindClass_m1230259107_ftn) (String_t*);
	static AndroidJNI_FindClass_m1230259107_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_FindClass_m1230259107_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::FindClass(System.String)");
	return _il2cpp_icall_func(___name0);
}
// System.IntPtr UnityEngine.AndroidJNI::FromReflectedMethod(System.IntPtr)
extern "C"  IntPtr_t AndroidJNI_FromReflectedMethod_m352872801 (Il2CppObject * __this /* static, unused */, IntPtr_t ___refMethod0, const MethodInfo* method)
{
	typedef IntPtr_t (*AndroidJNI_FromReflectedMethod_m352872801_ftn) (IntPtr_t);
	static AndroidJNI_FromReflectedMethod_m352872801_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_FromReflectedMethod_m352872801_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::FromReflectedMethod(System.IntPtr)");
	return _il2cpp_icall_func(___refMethod0);
}
// System.IntPtr UnityEngine.AndroidJNI::FromReflectedField(System.IntPtr)
extern "C"  IntPtr_t AndroidJNI_FromReflectedField_m576371562 (Il2CppObject * __this /* static, unused */, IntPtr_t ___refField0, const MethodInfo* method)
{
	typedef IntPtr_t (*AndroidJNI_FromReflectedField_m576371562_ftn) (IntPtr_t);
	static AndroidJNI_FromReflectedField_m576371562_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_FromReflectedField_m576371562_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::FromReflectedField(System.IntPtr)");
	return _il2cpp_icall_func(___refField0);
}
// System.IntPtr UnityEngine.AndroidJNI::ExceptionOccurred()
extern "C"  IntPtr_t AndroidJNI_ExceptionOccurred_m3001920724 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef IntPtr_t (*AndroidJNI_ExceptionOccurred_m3001920724_ftn) ();
	static AndroidJNI_ExceptionOccurred_m3001920724_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_ExceptionOccurred_m3001920724_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::ExceptionOccurred()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.AndroidJNI::ExceptionClear()
extern "C"  void AndroidJNI_ExceptionClear_m970305067 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*AndroidJNI_ExceptionClear_m970305067_ftn) ();
	static AndroidJNI_ExceptionClear_m970305067_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_ExceptionClear_m970305067_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::ExceptionClear()");
	_il2cpp_icall_func();
}
// System.IntPtr UnityEngine.AndroidJNI::NewGlobalRef(System.IntPtr)
extern "C"  IntPtr_t AndroidJNI_NewGlobalRef_m172051970 (Il2CppObject * __this /* static, unused */, IntPtr_t ___obj0, const MethodInfo* method)
{
	typedef IntPtr_t (*AndroidJNI_NewGlobalRef_m172051970_ftn) (IntPtr_t);
	static AndroidJNI_NewGlobalRef_m172051970_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_NewGlobalRef_m172051970_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::NewGlobalRef(System.IntPtr)");
	return _il2cpp_icall_func(___obj0);
}
// System.Void UnityEngine.AndroidJNI::DeleteGlobalRef(System.IntPtr)
extern "C"  void AndroidJNI_DeleteGlobalRef_m3178615354 (Il2CppObject * __this /* static, unused */, IntPtr_t ___obj0, const MethodInfo* method)
{
	typedef void (*AndroidJNI_DeleteGlobalRef_m3178615354_ftn) (IntPtr_t);
	static AndroidJNI_DeleteGlobalRef_m3178615354_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_DeleteGlobalRef_m3178615354_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::DeleteGlobalRef(System.IntPtr)");
	_il2cpp_icall_func(___obj0);
}
// System.Void UnityEngine.AndroidJNI::DeleteLocalRef(System.IntPtr)
extern "C"  void AndroidJNI_DeleteLocalRef_m641765044 (Il2CppObject * __this /* static, unused */, IntPtr_t ___obj0, const MethodInfo* method)
{
	typedef void (*AndroidJNI_DeleteLocalRef_m641765044_ftn) (IntPtr_t);
	static AndroidJNI_DeleteLocalRef_m641765044_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_DeleteLocalRef_m641765044_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::DeleteLocalRef(System.IntPtr)");
	_il2cpp_icall_func(___obj0);
}
// System.IntPtr UnityEngine.AndroidJNI::NewObject(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  IntPtr_t AndroidJNI_NewObject_m2867608803 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	typedef IntPtr_t (*AndroidJNI_NewObject_m2867608803_ftn) (IntPtr_t, IntPtr_t, jvalueU5BU5D_t1723627146*);
	static AndroidJNI_NewObject_m2867608803_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_NewObject_m2867608803_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::NewObject(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])");
	return _il2cpp_icall_func(___clazz0, ___methodID1, ___args2);
}
// System.IntPtr UnityEngine.AndroidJNI::GetObjectClass(System.IntPtr)
extern "C"  IntPtr_t AndroidJNI_GetObjectClass_m798139855 (Il2CppObject * __this /* static, unused */, IntPtr_t ___obj0, const MethodInfo* method)
{
	typedef IntPtr_t (*AndroidJNI_GetObjectClass_m798139855_ftn) (IntPtr_t);
	static AndroidJNI_GetObjectClass_m798139855_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_GetObjectClass_m798139855_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::GetObjectClass(System.IntPtr)");
	return _il2cpp_icall_func(___obj0);
}
// System.IntPtr UnityEngine.AndroidJNI::GetMethodID(System.IntPtr,System.String,System.String)
extern "C"  IntPtr_t AndroidJNI_GetMethodID_m2948332762 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, String_t* ___name1, String_t* ___sig2, const MethodInfo* method)
{
	typedef IntPtr_t (*AndroidJNI_GetMethodID_m2948332762_ftn) (IntPtr_t, String_t*, String_t*);
	static AndroidJNI_GetMethodID_m2948332762_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_GetMethodID_m2948332762_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::GetMethodID(System.IntPtr,System.String,System.String)");
	return _il2cpp_icall_func(___clazz0, ___name1, ___sig2);
}
// System.IntPtr UnityEngine.AndroidJNI::GetFieldID(System.IntPtr,System.String,System.String)
extern "C"  IntPtr_t AndroidJNI_GetFieldID_m3253410731 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, String_t* ___name1, String_t* ___sig2, const MethodInfo* method)
{
	typedef IntPtr_t (*AndroidJNI_GetFieldID_m3253410731_ftn) (IntPtr_t, String_t*, String_t*);
	static AndroidJNI_GetFieldID_m3253410731_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_GetFieldID_m3253410731_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::GetFieldID(System.IntPtr,System.String,System.String)");
	return _il2cpp_icall_func(___clazz0, ___name1, ___sig2);
}
// System.IntPtr UnityEngine.AndroidJNI::GetStaticMethodID(System.IntPtr,System.String,System.String)
extern "C"  IntPtr_t AndroidJNI_GetStaticMethodID_m2473492300 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, String_t* ___name1, String_t* ___sig2, const MethodInfo* method)
{
	typedef IntPtr_t (*AndroidJNI_GetStaticMethodID_m2473492300_ftn) (IntPtr_t, String_t*, String_t*);
	static AndroidJNI_GetStaticMethodID_m2473492300_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_GetStaticMethodID_m2473492300_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::GetStaticMethodID(System.IntPtr,System.String,System.String)");
	return _il2cpp_icall_func(___clazz0, ___name1, ___sig2);
}
// System.IntPtr UnityEngine.AndroidJNI::GetStaticFieldID(System.IntPtr,System.String,System.String)
extern "C"  IntPtr_t AndroidJNI_GetStaticFieldID_m3515187961 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, String_t* ___name1, String_t* ___sig2, const MethodInfo* method)
{
	typedef IntPtr_t (*AndroidJNI_GetStaticFieldID_m3515187961_ftn) (IntPtr_t, String_t*, String_t*);
	static AndroidJNI_GetStaticFieldID_m3515187961_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_GetStaticFieldID_m3515187961_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::GetStaticFieldID(System.IntPtr,System.String,System.String)");
	return _il2cpp_icall_func(___clazz0, ___name1, ___sig2);
}
// System.IntPtr UnityEngine.AndroidJNI::NewStringUTF(System.String)
extern "C"  IntPtr_t AndroidJNI_NewStringUTF_m2984333834 (Il2CppObject * __this /* static, unused */, String_t* ___bytes0, const MethodInfo* method)
{
	typedef IntPtr_t (*AndroidJNI_NewStringUTF_m2984333834_ftn) (String_t*);
	static AndroidJNI_NewStringUTF_m2984333834_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_NewStringUTF_m2984333834_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::NewStringUTF(System.String)");
	return _il2cpp_icall_func(___bytes0);
}
// System.String UnityEngine.AndroidJNI::GetStringUTFChars(System.IntPtr)
extern "C"  String_t* AndroidJNI_GetStringUTFChars_m2788209029 (Il2CppObject * __this /* static, unused */, IntPtr_t ___str0, const MethodInfo* method)
{
	typedef String_t* (*AndroidJNI_GetStringUTFChars_m2788209029_ftn) (IntPtr_t);
	static AndroidJNI_GetStringUTFChars_m2788209029_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_GetStringUTFChars_m2788209029_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::GetStringUTFChars(System.IntPtr)");
	return _il2cpp_icall_func(___str0);
}
// System.String UnityEngine.AndroidJNI::CallStringMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  String_t* AndroidJNI_CallStringMethod_m3969973572 (Il2CppObject * __this /* static, unused */, IntPtr_t ___obj0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	typedef String_t* (*AndroidJNI_CallStringMethod_m3969973572_ftn) (IntPtr_t, IntPtr_t, jvalueU5BU5D_t1723627146*);
	static AndroidJNI_CallStringMethod_m3969973572_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_CallStringMethod_m3969973572_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::CallStringMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])");
	return _il2cpp_icall_func(___obj0, ___methodID1, ___args2);
}
// System.IntPtr UnityEngine.AndroidJNI::CallObjectMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  IntPtr_t AndroidJNI_CallObjectMethod_m637661508 (Il2CppObject * __this /* static, unused */, IntPtr_t ___obj0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	typedef IntPtr_t (*AndroidJNI_CallObjectMethod_m637661508_ftn) (IntPtr_t, IntPtr_t, jvalueU5BU5D_t1723627146*);
	static AndroidJNI_CallObjectMethod_m637661508_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_CallObjectMethod_m637661508_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::CallObjectMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])");
	return _il2cpp_icall_func(___obj0, ___methodID1, ___args2);
}
// System.Int32 UnityEngine.AndroidJNI::CallIntMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  int32_t AndroidJNI_CallIntMethod_m2566231915 (Il2CppObject * __this /* static, unused */, IntPtr_t ___obj0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	typedef int32_t (*AndroidJNI_CallIntMethod_m2566231915_ftn) (IntPtr_t, IntPtr_t, jvalueU5BU5D_t1723627146*);
	static AndroidJNI_CallIntMethod_m2566231915_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_CallIntMethod_m2566231915_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::CallIntMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])");
	return _il2cpp_icall_func(___obj0, ___methodID1, ___args2);
}
// System.Boolean UnityEngine.AndroidJNI::CallBooleanMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  bool AndroidJNI_CallBooleanMethod_m2659085182 (Il2CppObject * __this /* static, unused */, IntPtr_t ___obj0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	typedef bool (*AndroidJNI_CallBooleanMethod_m2659085182_ftn) (IntPtr_t, IntPtr_t, jvalueU5BU5D_t1723627146*);
	static AndroidJNI_CallBooleanMethod_m2659085182_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_CallBooleanMethod_m2659085182_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::CallBooleanMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])");
	return _il2cpp_icall_func(___obj0, ___methodID1, ___args2);
}
// System.Int16 UnityEngine.AndroidJNI::CallShortMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  int16_t AndroidJNI_CallShortMethod_m1918107582 (Il2CppObject * __this /* static, unused */, IntPtr_t ___obj0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	typedef int16_t (*AndroidJNI_CallShortMethod_m1918107582_ftn) (IntPtr_t, IntPtr_t, jvalueU5BU5D_t1723627146*);
	static AndroidJNI_CallShortMethod_m1918107582_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_CallShortMethod_m1918107582_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::CallShortMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])");
	return _il2cpp_icall_func(___obj0, ___methodID1, ___args2);
}
// System.Byte UnityEngine.AndroidJNI::CallByteMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  uint8_t AndroidJNI_CallByteMethod_m4179587108 (Il2CppObject * __this /* static, unused */, IntPtr_t ___obj0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	typedef uint8_t (*AndroidJNI_CallByteMethod_m4179587108_ftn) (IntPtr_t, IntPtr_t, jvalueU5BU5D_t1723627146*);
	static AndroidJNI_CallByteMethod_m4179587108_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_CallByteMethod_m4179587108_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::CallByteMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])");
	return _il2cpp_icall_func(___obj0, ___methodID1, ___args2);
}
// System.Char UnityEngine.AndroidJNI::CallCharMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  Il2CppChar AndroidJNI_CallCharMethod_m1814770148 (Il2CppObject * __this /* static, unused */, IntPtr_t ___obj0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	typedef Il2CppChar (*AndroidJNI_CallCharMethod_m1814770148_ftn) (IntPtr_t, IntPtr_t, jvalueU5BU5D_t1723627146*);
	static AndroidJNI_CallCharMethod_m1814770148_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_CallCharMethod_m1814770148_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::CallCharMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])");
	return _il2cpp_icall_func(___obj0, ___methodID1, ___args2);
}
// System.Single UnityEngine.AndroidJNI::CallFloatMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  float AndroidJNI_CallFloatMethod_m4115747916 (Il2CppObject * __this /* static, unused */, IntPtr_t ___obj0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	typedef float (*AndroidJNI_CallFloatMethod_m4115747916_ftn) (IntPtr_t, IntPtr_t, jvalueU5BU5D_t1723627146*);
	static AndroidJNI_CallFloatMethod_m4115747916_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_CallFloatMethod_m4115747916_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::CallFloatMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])");
	return _il2cpp_icall_func(___obj0, ___methodID1, ___args2);
}
// System.Double UnityEngine.AndroidJNI::CallDoubleMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  double AndroidJNI_CallDoubleMethod_m2741331268 (Il2CppObject * __this /* static, unused */, IntPtr_t ___obj0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	typedef double (*AndroidJNI_CallDoubleMethod_m2741331268_ftn) (IntPtr_t, IntPtr_t, jvalueU5BU5D_t1723627146*);
	static AndroidJNI_CallDoubleMethod_m2741331268_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_CallDoubleMethod_m2741331268_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::CallDoubleMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])");
	return _il2cpp_icall_func(___obj0, ___methodID1, ___args2);
}
// System.Int64 UnityEngine.AndroidJNI::CallLongMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  int64_t AndroidJNI_CallLongMethod_m4476813 (Il2CppObject * __this /* static, unused */, IntPtr_t ___obj0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	typedef int64_t (*AndroidJNI_CallLongMethod_m4476813_ftn) (IntPtr_t, IntPtr_t, jvalueU5BU5D_t1723627146*);
	static AndroidJNI_CallLongMethod_m4476813_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_CallLongMethod_m4476813_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::CallLongMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])");
	return _il2cpp_icall_func(___obj0, ___methodID1, ___args2);
}
// System.Void UnityEngine.AndroidJNI::CallVoidMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  void AndroidJNI_CallVoidMethod_m3567923620 (Il2CppObject * __this /* static, unused */, IntPtr_t ___obj0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	typedef void (*AndroidJNI_CallVoidMethod_m3567923620_ftn) (IntPtr_t, IntPtr_t, jvalueU5BU5D_t1723627146*);
	static AndroidJNI_CallVoidMethod_m3567923620_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_CallVoidMethod_m3567923620_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::CallVoidMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])");
	_il2cpp_icall_func(___obj0, ___methodID1, ___args2);
}
// System.String UnityEngine.AndroidJNI::CallStaticStringMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  String_t* AndroidJNI_CallStaticStringMethod_m1017965970 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	typedef String_t* (*AndroidJNI_CallStaticStringMethod_m1017965970_ftn) (IntPtr_t, IntPtr_t, jvalueU5BU5D_t1723627146*);
	static AndroidJNI_CallStaticStringMethod_m1017965970_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_CallStaticStringMethod_m1017965970_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::CallStaticStringMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])");
	return _il2cpp_icall_func(___clazz0, ___methodID1, ___args2);
}
// System.IntPtr UnityEngine.AndroidJNI::CallStaticObjectMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  IntPtr_t AndroidJNI_CallStaticObjectMethod_m3595893778 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	typedef IntPtr_t (*AndroidJNI_CallStaticObjectMethod_m3595893778_ftn) (IntPtr_t, IntPtr_t, jvalueU5BU5D_t1723627146*);
	static AndroidJNI_CallStaticObjectMethod_m3595893778_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_CallStaticObjectMethod_m3595893778_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::CallStaticObjectMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])");
	return _il2cpp_icall_func(___clazz0, ___methodID1, ___args2);
}
// System.Int32 UnityEngine.AndroidJNI::CallStaticIntMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  int32_t AndroidJNI_CallStaticIntMethod_m1635023773 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	typedef int32_t (*AndroidJNI_CallStaticIntMethod_m1635023773_ftn) (IntPtr_t, IntPtr_t, jvalueU5BU5D_t1723627146*);
	static AndroidJNI_CallStaticIntMethod_m1635023773_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_CallStaticIntMethod_m1635023773_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::CallStaticIntMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])");
	return _il2cpp_icall_func(___clazz0, ___methodID1, ___args2);
}
// System.Boolean UnityEngine.AndroidJNI::CallStaticBooleanMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  bool AndroidJNI_CallStaticBooleanMethod_m4255357744 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	typedef bool (*AndroidJNI_CallStaticBooleanMethod_m4255357744_ftn) (IntPtr_t, IntPtr_t, jvalueU5BU5D_t1723627146*);
	static AndroidJNI_CallStaticBooleanMethod_m4255357744_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_CallStaticBooleanMethod_m4255357744_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::CallStaticBooleanMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])");
	return _il2cpp_icall_func(___clazz0, ___methodID1, ___args2);
}
// System.Int16 UnityEngine.AndroidJNI::CallStaticShortMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  int16_t AndroidJNI_CallStaticShortMethod_m751555312 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	typedef int16_t (*AndroidJNI_CallStaticShortMethod_m751555312_ftn) (IntPtr_t, IntPtr_t, jvalueU5BU5D_t1723627146*);
	static AndroidJNI_CallStaticShortMethod_m751555312_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_CallStaticShortMethod_m751555312_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::CallStaticShortMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])");
	return _il2cpp_icall_func(___clazz0, ___methodID1, ___args2);
}
// System.Byte UnityEngine.AndroidJNI::CallStaticByteMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  uint8_t AndroidJNI_CallStaticByteMethod_m4090362162 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	typedef uint8_t (*AndroidJNI_CallStaticByteMethod_m4090362162_ftn) (IntPtr_t, IntPtr_t, jvalueU5BU5D_t1723627146*);
	static AndroidJNI_CallStaticByteMethod_m4090362162_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_CallStaticByteMethod_m4090362162_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::CallStaticByteMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])");
	return _il2cpp_icall_func(___clazz0, ___methodID1, ___args2);
}
// System.Char UnityEngine.AndroidJNI::CallStaticCharMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  Il2CppChar AndroidJNI_CallStaticCharMethod_m289365362 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	typedef Il2CppChar (*AndroidJNI_CallStaticCharMethod_m289365362_ftn) (IntPtr_t, IntPtr_t, jvalueU5BU5D_t1723627146*);
	static AndroidJNI_CallStaticCharMethod_m289365362_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_CallStaticCharMethod_m289365362_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::CallStaticCharMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])");
	return _il2cpp_icall_func(___clazz0, ___methodID1, ___args2);
}
// System.Single UnityEngine.AndroidJNI::CallStaticFloatMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  float AndroidJNI_CallStaticFloatMethod_m3389844734 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	typedef float (*AndroidJNI_CallStaticFloatMethod_m3389844734_ftn) (IntPtr_t, IntPtr_t, jvalueU5BU5D_t1723627146*);
	static AndroidJNI_CallStaticFloatMethod_m3389844734_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_CallStaticFloatMethod_m3389844734_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::CallStaticFloatMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])");
	return _il2cpp_icall_func(___clazz0, ___methodID1, ___args2);
}
// System.Double UnityEngine.AndroidJNI::CallStaticDoubleMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  double AndroidJNI_CallStaticDoubleMethod_m2003248530 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	typedef double (*AndroidJNI_CallStaticDoubleMethod_m2003248530_ftn) (IntPtr_t, IntPtr_t, jvalueU5BU5D_t1723627146*);
	static AndroidJNI_CallStaticDoubleMethod_m2003248530_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_CallStaticDoubleMethod_m2003248530_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::CallStaticDoubleMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])");
	return _il2cpp_icall_func(___clazz0, ___methodID1, ___args2);
}
// System.Int64 UnityEngine.AndroidJNI::CallStaticLongMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  int64_t AndroidJNI_CallStaticLongMethod_m3580480731 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	typedef int64_t (*AndroidJNI_CallStaticLongMethod_m3580480731_ftn) (IntPtr_t, IntPtr_t, jvalueU5BU5D_t1723627146*);
	static AndroidJNI_CallStaticLongMethod_m3580480731_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_CallStaticLongMethod_m3580480731_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::CallStaticLongMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])");
	return _il2cpp_icall_func(___clazz0, ___methodID1, ___args2);
}
// System.String UnityEngine.AndroidJNI::GetStaticStringField(System.IntPtr,System.IntPtr)
extern "C"  String_t* AndroidJNI_GetStaticStringField_m1861664525 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___fieldID1, const MethodInfo* method)
{
	typedef String_t* (*AndroidJNI_GetStaticStringField_m1861664525_ftn) (IntPtr_t, IntPtr_t);
	static AndroidJNI_GetStaticStringField_m1861664525_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_GetStaticStringField_m1861664525_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::GetStaticStringField(System.IntPtr,System.IntPtr)");
	return _il2cpp_icall_func(___clazz0, ___fieldID1);
}
// System.IntPtr UnityEngine.AndroidJNI::GetStaticObjectField(System.IntPtr,System.IntPtr)
extern "C"  IntPtr_t AndroidJNI_GetStaticObjectField_m3723206057 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___fieldID1, const MethodInfo* method)
{
	typedef IntPtr_t (*AndroidJNI_GetStaticObjectField_m3723206057_ftn) (IntPtr_t, IntPtr_t);
	static AndroidJNI_GetStaticObjectField_m3723206057_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_GetStaticObjectField_m3723206057_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::GetStaticObjectField(System.IntPtr,System.IntPtr)");
	return _il2cpp_icall_func(___clazz0, ___fieldID1);
}
// System.Boolean UnityEngine.AndroidJNI::GetStaticBooleanField(System.IntPtr,System.IntPtr)
extern "C"  bool AndroidJNI_GetStaticBooleanField_m4125732253 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___fieldID1, const MethodInfo* method)
{
	typedef bool (*AndroidJNI_GetStaticBooleanField_m4125732253_ftn) (IntPtr_t, IntPtr_t);
	static AndroidJNI_GetStaticBooleanField_m4125732253_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_GetStaticBooleanField_m4125732253_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::GetStaticBooleanField(System.IntPtr,System.IntPtr)");
	return _il2cpp_icall_func(___clazz0, ___fieldID1);
}
// System.Byte UnityEngine.AndroidJNI::GetStaticByteField(System.IntPtr,System.IntPtr)
extern "C"  uint8_t AndroidJNI_GetStaticByteField_m1987720379 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___fieldID1, const MethodInfo* method)
{
	typedef uint8_t (*AndroidJNI_GetStaticByteField_m1987720379_ftn) (IntPtr_t, IntPtr_t);
	static AndroidJNI_GetStaticByteField_m1987720379_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_GetStaticByteField_m1987720379_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::GetStaticByteField(System.IntPtr,System.IntPtr)");
	return _il2cpp_icall_func(___clazz0, ___fieldID1);
}
// System.Char UnityEngine.AndroidJNI::GetStaticCharField(System.IntPtr,System.IntPtr)
extern "C"  Il2CppChar AndroidJNI_GetStaticCharField_m448194519 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___fieldID1, const MethodInfo* method)
{
	typedef Il2CppChar (*AndroidJNI_GetStaticCharField_m448194519_ftn) (IntPtr_t, IntPtr_t);
	static AndroidJNI_GetStaticCharField_m448194519_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_GetStaticCharField_m448194519_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::GetStaticCharField(System.IntPtr,System.IntPtr)");
	return _il2cpp_icall_func(___clazz0, ___fieldID1);
}
// System.Int16 UnityEngine.AndroidJNI::GetStaticShortField(System.IntPtr,System.IntPtr)
extern "C"  int16_t AndroidJNI_GetStaticShortField_m3116124933 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___fieldID1, const MethodInfo* method)
{
	typedef int16_t (*AndroidJNI_GetStaticShortField_m3116124933_ftn) (IntPtr_t, IntPtr_t);
	static AndroidJNI_GetStaticShortField_m3116124933_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_GetStaticShortField_m3116124933_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::GetStaticShortField(System.IntPtr,System.IntPtr)");
	return _il2cpp_icall_func(___clazz0, ___fieldID1);
}
// System.Int32 UnityEngine.AndroidJNI::GetStaticIntField(System.IntPtr,System.IntPtr)
extern "C"  int32_t AndroidJNI_GetStaticIntField_m642583742 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___fieldID1, const MethodInfo* method)
{
	typedef int32_t (*AndroidJNI_GetStaticIntField_m642583742_ftn) (IntPtr_t, IntPtr_t);
	static AndroidJNI_GetStaticIntField_m642583742_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_GetStaticIntField_m642583742_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::GetStaticIntField(System.IntPtr,System.IntPtr)");
	return _il2cpp_icall_func(___clazz0, ___fieldID1);
}
// System.Int64 UnityEngine.AndroidJNI::GetStaticLongField(System.IntPtr,System.IntPtr)
extern "C"  int64_t AndroidJNI_GetStaticLongField_m724040346 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___fieldID1, const MethodInfo* method)
{
	typedef int64_t (*AndroidJNI_GetStaticLongField_m724040346_ftn) (IntPtr_t, IntPtr_t);
	static AndroidJNI_GetStaticLongField_m724040346_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_GetStaticLongField_m724040346_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::GetStaticLongField(System.IntPtr,System.IntPtr)");
	return _il2cpp_icall_func(___clazz0, ___fieldID1);
}
// System.Single UnityEngine.AndroidJNI::GetStaticFloatField(System.IntPtr,System.IntPtr)
extern "C"  float AndroidJNI_GetStaticFloatField_m2480042103 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___fieldID1, const MethodInfo* method)
{
	typedef float (*AndroidJNI_GetStaticFloatField_m2480042103_ftn) (IntPtr_t, IntPtr_t);
	static AndroidJNI_GetStaticFloatField_m2480042103_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_GetStaticFloatField_m2480042103_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::GetStaticFloatField(System.IntPtr,System.IntPtr)");
	return _il2cpp_icall_func(___clazz0, ___fieldID1);
}
// System.Double UnityEngine.AndroidJNI::GetStaticDoubleField(System.IntPtr,System.IntPtr)
extern "C"  double AndroidJNI_GetStaticDoubleField_m2149996173 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___fieldID1, const MethodInfo* method)
{
	typedef double (*AndroidJNI_GetStaticDoubleField_m2149996173_ftn) (IntPtr_t, IntPtr_t);
	static AndroidJNI_GetStaticDoubleField_m2149996173_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_GetStaticDoubleField_m2149996173_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::GetStaticDoubleField(System.IntPtr,System.IntPtr)");
	return _il2cpp_icall_func(___clazz0, ___fieldID1);
}
// System.IntPtr UnityEngine.AndroidJNI::ToBooleanArray(System.Boolean[])
extern "C"  IntPtr_t AndroidJNI_ToBooleanArray_m442048547 (Il2CppObject * __this /* static, unused */, BooleanU5BU5D_t3456302923* ___array0, const MethodInfo* method)
{
	typedef IntPtr_t (*AndroidJNI_ToBooleanArray_m442048547_ftn) (BooleanU5BU5D_t3456302923*);
	static AndroidJNI_ToBooleanArray_m442048547_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_ToBooleanArray_m442048547_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::ToBooleanArray(System.Boolean[])");
	return _il2cpp_icall_func(___array0);
}
// System.IntPtr UnityEngine.AndroidJNI::ToByteArray(System.Byte[])
extern "C"  IntPtr_t AndroidJNI_ToByteArray_m1620048019 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___array0, const MethodInfo* method)
{
	typedef IntPtr_t (*AndroidJNI_ToByteArray_m1620048019_ftn) (ByteU5BU5D_t4260760469*);
	static AndroidJNI_ToByteArray_m1620048019_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_ToByteArray_m1620048019_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::ToByteArray(System.Byte[])");
	return _il2cpp_icall_func(___array0);
}
// System.IntPtr UnityEngine.AndroidJNI::ToCharArray(System.Char[])
extern "C"  IntPtr_t AndroidJNI_ToCharArray_m771488467 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3324145743* ___array0, const MethodInfo* method)
{
	typedef IntPtr_t (*AndroidJNI_ToCharArray_m771488467_ftn) (CharU5BU5D_t3324145743*);
	static AndroidJNI_ToCharArray_m771488467_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_ToCharArray_m771488467_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::ToCharArray(System.Char[])");
	return _il2cpp_icall_func(___array0);
}
// System.IntPtr UnityEngine.AndroidJNI::ToShortArray(System.Int16[])
extern "C"  IntPtr_t AndroidJNI_ToShortArray_m2820620963 (Il2CppObject * __this /* static, unused */, Int16U5BU5D_t801762735* ___array0, const MethodInfo* method)
{
	typedef IntPtr_t (*AndroidJNI_ToShortArray_m2820620963_ftn) (Int16U5BU5D_t801762735*);
	static AndroidJNI_ToShortArray_m2820620963_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_ToShortArray_m2820620963_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::ToShortArray(System.Int16[])");
	return _il2cpp_icall_func(___array0);
}
// System.IntPtr UnityEngine.AndroidJNI::ToIntArray(System.Int32[])
extern "C"  IntPtr_t AndroidJNI_ToIntArray_m1134096790 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___array0, const MethodInfo* method)
{
	typedef IntPtr_t (*AndroidJNI_ToIntArray_m1134096790_ftn) (Int32U5BU5D_t3230847821*);
	static AndroidJNI_ToIntArray_m1134096790_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_ToIntArray_m1134096790_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::ToIntArray(System.Int32[])");
	return _il2cpp_icall_func(___array0);
}
// System.IntPtr UnityEngine.AndroidJNI::ToLongArray(System.Int64[])
extern "C"  IntPtr_t AndroidJNI_ToLongArray_m3787458290 (Il2CppObject * __this /* static, unused */, Int64U5BU5D_t2174042770* ___array0, const MethodInfo* method)
{
	typedef IntPtr_t (*AndroidJNI_ToLongArray_m3787458290_ftn) (Int64U5BU5D_t2174042770*);
	static AndroidJNI_ToLongArray_m3787458290_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_ToLongArray_m3787458290_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::ToLongArray(System.Int64[])");
	return _il2cpp_icall_func(___array0);
}
// System.IntPtr UnityEngine.AndroidJNI::ToFloatArray(System.Single[])
extern "C"  IntPtr_t AndroidJNI_ToFloatArray_m2734312239 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t2316563989* ___array0, const MethodInfo* method)
{
	typedef IntPtr_t (*AndroidJNI_ToFloatArray_m2734312239_ftn) (SingleU5BU5D_t2316563989*);
	static AndroidJNI_ToFloatArray_m2734312239_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_ToFloatArray_m2734312239_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::ToFloatArray(System.Single[])");
	return _il2cpp_icall_func(___array0);
}
// System.IntPtr UnityEngine.AndroidJNI::ToDoubleArray(System.Double[])
extern "C"  IntPtr_t AndroidJNI_ToDoubleArray_m1106831155 (Il2CppObject * __this /* static, unused */, DoubleU5BU5D_t2145413704* ___array0, const MethodInfo* method)
{
	typedef IntPtr_t (*AndroidJNI_ToDoubleArray_m1106831155_ftn) (DoubleU5BU5D_t2145413704*);
	static AndroidJNI_ToDoubleArray_m1106831155_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_ToDoubleArray_m1106831155_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::ToDoubleArray(System.Double[])");
	return _il2cpp_icall_func(___array0);
}
// System.IntPtr UnityEngine.AndroidJNI::ToObjectArray(System.IntPtr[],System.IntPtr)
extern "C"  IntPtr_t AndroidJNI_ToObjectArray_m3438267649 (Il2CppObject * __this /* static, unused */, IntPtrU5BU5D_t3228729122* ___array0, IntPtr_t ___arrayClass1, const MethodInfo* method)
{
	typedef IntPtr_t (*AndroidJNI_ToObjectArray_m3438267649_ftn) (IntPtrU5BU5D_t3228729122*, IntPtr_t);
	static AndroidJNI_ToObjectArray_m3438267649_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_ToObjectArray_m3438267649_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::ToObjectArray(System.IntPtr[],System.IntPtr)");
	return _il2cpp_icall_func(___array0, ___arrayClass1);
}
// System.Boolean[] UnityEngine.AndroidJNI::FromBooleanArray(System.IntPtr)
extern "C"  BooleanU5BU5D_t3456302923* AndroidJNI_FromBooleanArray_m3435863496 (Il2CppObject * __this /* static, unused */, IntPtr_t ___array0, const MethodInfo* method)
{
	typedef BooleanU5BU5D_t3456302923* (*AndroidJNI_FromBooleanArray_m3435863496_ftn) (IntPtr_t);
	static AndroidJNI_FromBooleanArray_m3435863496_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_FromBooleanArray_m3435863496_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::FromBooleanArray(System.IntPtr)");
	return _il2cpp_icall_func(___array0);
}
// System.Byte[] UnityEngine.AndroidJNI::FromByteArray(System.IntPtr)
extern "C"  ByteU5BU5D_t4260760469* AndroidJNI_FromByteArray_m1645613058 (Il2CppObject * __this /* static, unused */, IntPtr_t ___array0, const MethodInfo* method)
{
	typedef ByteU5BU5D_t4260760469* (*AndroidJNI_FromByteArray_m1645613058_ftn) (IntPtr_t);
	static AndroidJNI_FromByteArray_m1645613058_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_FromByteArray_m1645613058_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::FromByteArray(System.IntPtr)");
	return _il2cpp_icall_func(___array0);
}
// System.Char[] UnityEngine.AndroidJNI::FromCharArray(System.IntPtr)
extern "C"  CharU5BU5D_t3324145743* AndroidJNI_FromCharArray_m325115202 (Il2CppObject * __this /* static, unused */, IntPtr_t ___array0, const MethodInfo* method)
{
	typedef CharU5BU5D_t3324145743* (*AndroidJNI_FromCharArray_m325115202_ftn) (IntPtr_t);
	static AndroidJNI_FromCharArray_m325115202_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_FromCharArray_m325115202_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::FromCharArray(System.IntPtr)");
	return _il2cpp_icall_func(___array0);
}
// System.Int16[] UnityEngine.AndroidJNI::FromShortArray(System.IntPtr)
extern "C"  Int16U5BU5D_t801762735* AndroidJNI_FromShortArray_m2832106632 (Il2CppObject * __this /* static, unused */, IntPtr_t ___array0, const MethodInfo* method)
{
	typedef Int16U5BU5D_t801762735* (*AndroidJNI_FromShortArray_m2832106632_ftn) (IntPtr_t);
	static AndroidJNI_FromShortArray_m2832106632_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_FromShortArray_m2832106632_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::FromShortArray(System.IntPtr)");
	return _il2cpp_icall_func(___array0);
}
// System.Int32[] UnityEngine.AndroidJNI::FromIntArray(System.IntPtr)
extern "C"  Int32U5BU5D_t3230847821* AndroidJNI_FromIntArray_m2891457973 (Il2CppObject * __this /* static, unused */, IntPtr_t ___array0, const MethodInfo* method)
{
	typedef Int32U5BU5D_t3230847821* (*AndroidJNI_FromIntArray_m2891457973_ftn) (IntPtr_t);
	static AndroidJNI_FromIntArray_m2891457973_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_FromIntArray_m2891457973_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::FromIntArray(System.IntPtr)");
	return _il2cpp_icall_func(___array0);
}
// System.Int64[] UnityEngine.AndroidJNI::FromLongArray(System.IntPtr)
extern "C"  Int64U5BU5D_t2174042770* AndroidJNI_FromLongArray_m2965481131 (Il2CppObject * __this /* static, unused */, IntPtr_t ___array0, const MethodInfo* method)
{
	typedef Int64U5BU5D_t2174042770* (*AndroidJNI_FromLongArray_m2965481131_ftn) (IntPtr_t);
	static AndroidJNI_FromLongArray_m2965481131_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_FromLongArray_m2965481131_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::FromLongArray(System.IntPtr)");
	return _il2cpp_icall_func(___array0);
}
// System.Single[] UnityEngine.AndroidJNI::FromFloatArray(System.IntPtr)
extern "C"  SingleU5BU5D_t2316563989* AndroidJNI_FromFloatArray_m1756058774 (Il2CppObject * __this /* static, unused */, IntPtr_t ___array0, const MethodInfo* method)
{
	typedef SingleU5BU5D_t2316563989* (*AndroidJNI_FromFloatArray_m1756058774_ftn) (IntPtr_t);
	static AndroidJNI_FromFloatArray_m1756058774_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_FromFloatArray_m1756058774_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::FromFloatArray(System.IntPtr)");
	return _il2cpp_icall_func(___array0);
}
// System.Double[] UnityEngine.AndroidJNI::FromDoubleArray(System.IntPtr)
extern "C"  DoubleU5BU5D_t2145413704* AndroidJNI_FromDoubleArray_m2441308386 (Il2CppObject * __this /* static, unused */, IntPtr_t ___array0, const MethodInfo* method)
{
	typedef DoubleU5BU5D_t2145413704* (*AndroidJNI_FromDoubleArray_m2441308386_ftn) (IntPtr_t);
	static AndroidJNI_FromDoubleArray_m2441308386_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_FromDoubleArray_m2441308386_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::FromDoubleArray(System.IntPtr)");
	return _il2cpp_icall_func(___array0);
}
// System.Int32 UnityEngine.AndroidJNI::GetArrayLength(System.IntPtr)
extern "C"  int32_t AndroidJNI_GetArrayLength_m2368966142 (Il2CppObject * __this /* static, unused */, IntPtr_t ___array0, const MethodInfo* method)
{
	typedef int32_t (*AndroidJNI_GetArrayLength_m2368966142_ftn) (IntPtr_t);
	static AndroidJNI_GetArrayLength_m2368966142_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_GetArrayLength_m2368966142_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::GetArrayLength(System.IntPtr)");
	return _il2cpp_icall_func(___array0);
}
// System.IntPtr UnityEngine.AndroidJNI::NewObjectArray(System.Int32,System.IntPtr,System.IntPtr)
extern "C"  IntPtr_t AndroidJNI_NewObjectArray_m2427147689 (Il2CppObject * __this /* static, unused */, int32_t ___size0, IntPtr_t ___clazz1, IntPtr_t ___obj2, const MethodInfo* method)
{
	typedef IntPtr_t (*AndroidJNI_NewObjectArray_m2427147689_ftn) (int32_t, IntPtr_t, IntPtr_t);
	static AndroidJNI_NewObjectArray_m2427147689_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_NewObjectArray_m2427147689_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::NewObjectArray(System.Int32,System.IntPtr,System.IntPtr)");
	return _il2cpp_icall_func(___size0, ___clazz1, ___obj2);
}
// System.IntPtr UnityEngine.AndroidJNI::GetObjectArrayElement(System.IntPtr,System.Int32)
extern "C"  IntPtr_t AndroidJNI_GetObjectArrayElement_m3151488507 (Il2CppObject * __this /* static, unused */, IntPtr_t ___array0, int32_t ___index1, const MethodInfo* method)
{
	typedef IntPtr_t (*AndroidJNI_GetObjectArrayElement_m3151488507_ftn) (IntPtr_t, int32_t);
	static AndroidJNI_GetObjectArrayElement_m3151488507_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_GetObjectArrayElement_m3151488507_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::GetObjectArrayElement(System.IntPtr,System.Int32)");
	return _il2cpp_icall_func(___array0, ___index1);
}
// System.Void UnityEngine.AndroidJNI::SetObjectArrayElement(System.IntPtr,System.Int32,System.IntPtr)
extern "C"  void AndroidJNI_SetObjectArrayElement_m2717496490 (Il2CppObject * __this /* static, unused */, IntPtr_t ___array0, int32_t ___index1, IntPtr_t ___obj2, const MethodInfo* method)
{
	typedef void (*AndroidJNI_SetObjectArrayElement_m2717496490_ftn) (IntPtr_t, int32_t, IntPtr_t);
	static AndroidJNI_SetObjectArrayElement_m2717496490_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNI_SetObjectArrayElement_m2717496490_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNI::SetObjectArrayElement(System.IntPtr,System.Int32,System.IntPtr)");
	_il2cpp_icall_func(___array0, ___index1, ___obj2);
}
// System.IntPtr UnityEngine.AndroidJNIHelper::GetConstructorID(System.IntPtr,System.String)
extern "C"  IntPtr_t AndroidJNIHelper_GetConstructorID_m2334065185 (Il2CppObject * __this /* static, unused */, IntPtr_t ___javaClass0, String_t* ___signature1, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___javaClass0;
		String_t* L_1 = ___signature1;
		IntPtr_t L_2 = _AndroidJNIHelper_GetConstructorID_m3153181464(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.IntPtr UnityEngine.AndroidJNIHelper::GetMethodID(System.IntPtr,System.String,System.String,System.Boolean)
extern "C"  IntPtr_t AndroidJNIHelper_GetMethodID_m951538837 (Il2CppObject * __this /* static, unused */, IntPtr_t ___javaClass0, String_t* ___methodName1, String_t* ___signature2, bool ___isStatic3, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___javaClass0;
		String_t* L_1 = ___methodName1;
		String_t* L_2 = ___signature2;
		bool L_3 = ___isStatic3;
		IntPtr_t L_4 = _AndroidJNIHelper_GetMethodID_m3581696140(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.IntPtr UnityEngine.AndroidJNIHelper::GetFieldID(System.IntPtr,System.String,System.String,System.Boolean)
extern "C"  IntPtr_t AndroidJNIHelper_GetFieldID_m3698856192 (Il2CppObject * __this /* static, unused */, IntPtr_t ___javaClass0, String_t* ___fieldName1, String_t* ___signature2, bool ___isStatic3, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___javaClass0;
		String_t* L_1 = ___fieldName1;
		String_t* L_2 = ___signature2;
		bool L_3 = ___isStatic3;
		IntPtr_t L_4 = _AndroidJNIHelper_GetFieldID_m597111337(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.IntPtr UnityEngine.AndroidJNIHelper::CreateJavaRunnable(UnityEngine.AndroidJavaRunnable)
extern "C"  IntPtr_t AndroidJNIHelper_CreateJavaRunnable_m1606930224 (Il2CppObject * __this /* static, unused */, AndroidJavaRunnable_t1289602340 * ___jrunnable0, const MethodInfo* method)
{
	{
		AndroidJavaRunnable_t1289602340 * L_0 = ___jrunnable0;
		IntPtr_t L_1 = _AndroidJNIHelper_CreateJavaRunnable_m2949886695(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.IntPtr UnityEngine.AndroidJNIHelper::CreateJavaProxy(UnityEngine.AndroidJavaProxy)
extern "C"  IntPtr_t AndroidJNIHelper_CreateJavaProxy_m1509148942 (Il2CppObject * __this /* static, unused */, AndroidJavaProxy_t1828457281 * ___proxy0, const MethodInfo* method)
{
	typedef IntPtr_t (*AndroidJNIHelper_CreateJavaProxy_m1509148942_ftn) (AndroidJavaProxy_t1828457281 *);
	static AndroidJNIHelper_CreateJavaProxy_m1509148942_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AndroidJNIHelper_CreateJavaProxy_m1509148942_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AndroidJNIHelper::CreateJavaProxy(UnityEngine.AndroidJavaProxy)");
	return _il2cpp_icall_func(___proxy0);
}
// UnityEngine.jvalue[] UnityEngine.AndroidJNIHelper::CreateJNIArgArray(System.Object[])
extern "C"  jvalueU5BU5D_t1723627146* AndroidJNIHelper_CreateJNIArgArray_m3777113646 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = ___args0;
		jvalueU5BU5D_t1723627146* L_1 = _AndroidJNIHelper_CreateJNIArgArray_m3400436963(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.AndroidJNIHelper::DeleteJNIArgArray(System.Object[],UnityEngine.jvalue[])
extern "C"  void AndroidJNIHelper_DeleteJNIArgArray_m4114677528 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___args0, jvalueU5BU5D_t1723627146* ___jniArgs1, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = ___args0;
		jvalueU5BU5D_t1723627146* L_1 = ___jniArgs1;
		_AndroidJNIHelper_DeleteJNIArgArray_m2053299397(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.IntPtr UnityEngine.AndroidJNIHelper::GetConstructorID(System.IntPtr,System.Object[])
extern "C"  IntPtr_t AndroidJNIHelper_GetConstructorID_m4058417937 (Il2CppObject * __this /* static, unused */, IntPtr_t ___jclass0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___jclass0;
		ObjectU5BU5D_t1108656482* L_1 = ___args1;
		IntPtr_t L_2 = _AndroidJNIHelper_GetConstructorID_m955179592(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.IntPtr UnityEngine.AndroidJNIHelper::GetMethodID(System.IntPtr,System.String,System.Object[],System.Boolean)
extern "C"  IntPtr_t AndroidJNIHelper_GetMethodID_m2032786277 (Il2CppObject * __this /* static, unused */, IntPtr_t ___jclass0, String_t* ___methodName1, ObjectU5BU5D_t1108656482* ___args2, bool ___isStatic3, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___jclass0;
		String_t* L_1 = ___methodName1;
		ObjectU5BU5D_t1108656482* L_2 = ___args2;
		bool L_3 = ___isStatic3;
		IntPtr_t L_4 = _AndroidJNIHelper_GetMethodID_m4173184412(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void UnityEngine.AndroidJNISafe::CheckException()
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* jvalueU5BU5D_t1723627146_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidJavaException_t125722146_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2941373900;
extern Il2CppCodeGenString* _stringLiteral3188629303;
extern Il2CppCodeGenString* _stringLiteral2518045292;
extern Il2CppCodeGenString* _stringLiteral1942950347;
extern Il2CppCodeGenString* _stringLiteral764501380;
extern Il2CppCodeGenString* _stringLiteral3725141248;
extern const uint32_t AndroidJNISafe_CheckException_m43129153_MetadataUsageId;
extern "C"  void AndroidJNISafe_CheckException_m43129153 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidJNISafe_CheckException_m43129153_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	IntPtr_t V_1;
	memset(&V_1, 0, sizeof(V_1));
	IntPtr_t V_2;
	memset(&V_2, 0, sizeof(V_2));
	IntPtr_t V_3;
	memset(&V_3, 0, sizeof(V_3));
	IntPtr_t V_4;
	memset(&V_4, 0, sizeof(V_4));
	String_t* V_5 = NULL;
	jvalueU5BU5D_t1723627146* V_6 = NULL;
	String_t* V_7 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IntPtr_t L_0 = AndroidJNI_ExceptionOccurred_m3001920724(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		IntPtr_t L_1 = V_0;
		IntPtr_t L_2 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_3 = IntPtr_op_Inequality_m10053967(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_00a7;
		}
	}
	{
		AndroidJNI_ExceptionClear_m970305067(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_4 = AndroidJNI_FindClass_m1230259107(NULL /*static, unused*/, _stringLiteral2941373900, /*hidden argument*/NULL);
		V_1 = L_4;
		IntPtr_t L_5 = AndroidJNI_FindClass_m1230259107(NULL /*static, unused*/, _stringLiteral3188629303, /*hidden argument*/NULL);
		V_2 = L_5;
	}

IL_0031:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_6 = V_1;
			IntPtr_t L_7 = AndroidJNI_GetMethodID_m2948332762(NULL /*static, unused*/, L_6, _stringLiteral2518045292, _stringLiteral1942950347, /*hidden argument*/NULL);
			V_3 = L_7;
			IntPtr_t L_8 = V_2;
			IntPtr_t L_9 = AndroidJNI_GetStaticMethodID_m2473492300(NULL /*static, unused*/, L_8, _stringLiteral764501380, _stringLiteral3725141248, /*hidden argument*/NULL);
			V_4 = L_9;
			IntPtr_t L_10 = V_0;
			IntPtr_t L_11 = V_3;
			String_t* L_12 = AndroidJNI_CallStringMethod_m3969973572(NULL /*static, unused*/, L_10, L_11, ((jvalueU5BU5D_t1723627146*)SZArrayNew(jvalueU5BU5D_t1723627146_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
			V_5 = L_12;
			V_6 = ((jvalueU5BU5D_t1723627146*)SZArrayNew(jvalueU5BU5D_t1723627146_il2cpp_TypeInfo_var, (uint32_t)1));
			jvalueU5BU5D_t1723627146* L_13 = V_6;
			NullCheck(L_13);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 0);
			IntPtr_t L_14 = V_0;
			((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_l_8(L_14);
			IntPtr_t L_15 = V_2;
			IntPtr_t L_16 = V_4;
			jvalueU5BU5D_t1723627146* L_17 = V_6;
			String_t* L_18 = AndroidJNI_CallStaticStringMethod_m1017965970(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
			V_7 = L_18;
			String_t* L_19 = V_5;
			String_t* L_20 = V_7;
			AndroidJavaException_t125722146 * L_21 = (AndroidJavaException_t125722146 *)il2cpp_codegen_object_new(AndroidJavaException_t125722146_il2cpp_TypeInfo_var);
			AndroidJavaException__ctor_m297033807(L_21, L_19, L_20, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
		}

IL_008f:
		{
			IL2CPP_LEAVE(0xA7, FINALLY_0094);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0094;
	}

FINALLY_0094:
	{ // begin finally (depth: 1)
		IntPtr_t L_22 = V_0;
		AndroidJNISafe_DeleteLocalRef_m2718059367(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		IntPtr_t L_23 = V_1;
		AndroidJNISafe_DeleteLocalRef_m2718059367(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		IntPtr_t L_24 = V_2;
		AndroidJNISafe_DeleteLocalRef_m2718059367(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(148)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(148)
	{
		IL2CPP_JUMP_TBL(0xA7, IL_00a7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00a7:
	{
		return;
	}
}
// System.Void UnityEngine.AndroidJNISafe::DeleteGlobalRef(System.IntPtr)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t AndroidJNISafe_DeleteGlobalRef_m3119229927_MetadataUsageId;
extern "C"  void AndroidJNISafe_DeleteGlobalRef_m3119229927 (Il2CppObject * __this /* static, unused */, IntPtr_t ___globalref0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidJNISafe_DeleteGlobalRef_m3119229927_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___globalref0;
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_2 = IntPtr_op_Inequality_m10053967(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0016;
		}
	}
	{
		IntPtr_t L_3 = ___globalref0;
		AndroidJNI_DeleteGlobalRef_m3178615354(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityEngine.AndroidJNISafe::DeleteLocalRef(System.IntPtr)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t AndroidJNISafe_DeleteLocalRef_m2718059367_MetadataUsageId;
extern "C"  void AndroidJNISafe_DeleteLocalRef_m2718059367 (Il2CppObject * __this /* static, unused */, IntPtr_t ___localref0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidJNISafe_DeleteLocalRef_m2718059367_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___localref0;
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_2 = IntPtr_op_Inequality_m10053967(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0016;
		}
	}
	{
		IntPtr_t L_3 = ___localref0;
		AndroidJNI_DeleteLocalRef_m641765044(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.IntPtr UnityEngine.AndroidJNISafe::NewStringUTF(System.String)
extern "C"  IntPtr_t AndroidJNISafe_NewStringUTF_m1650627069 (Il2CppObject * __this /* static, unused */, String_t* ___bytes0, const MethodInfo* method)
{
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_0 = ___bytes0;
			IntPtr_t L_1 = AndroidJNI_NewStringUTF_m2984333834(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			V_0 = L_1;
			IL2CPP_LEAVE(0x17, FINALLY_0011);
		}

IL_000c:
		{
			; // IL_000c: leave IL_0017
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0011;
	}

FINALLY_0011:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(17)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(17)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0017:
	{
		IntPtr_t L_2 = V_0;
		return L_2;
	}
}
// System.String UnityEngine.AndroidJNISafe::GetStringUTFChars(System.IntPtr)
extern "C"  String_t* AndroidJNISafe_GetStringUTFChars_m1288727154 (Il2CppObject * __this /* static, unused */, IntPtr_t ___str0, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___str0;
			String_t* L_1 = AndroidJNI_GetStringUTFChars_m2788209029(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			V_0 = L_1;
			IL2CPP_LEAVE(0x17, FINALLY_0011);
		}

IL_000c:
		{
			; // IL_000c: leave IL_0017
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0011;
	}

FINALLY_0011:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(17)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(17)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0017:
	{
		String_t* L_2 = V_0;
		return L_2;
	}
}
// System.IntPtr UnityEngine.AndroidJNISafe::GetObjectClass(System.IntPtr)
extern "C"  IntPtr_t AndroidJNISafe_GetObjectClass_m3301160194 (Il2CppObject * __this /* static, unused */, IntPtr_t ___ptr0, const MethodInfo* method)
{
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___ptr0;
			IntPtr_t L_1 = AndroidJNI_GetObjectClass_m798139855(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			V_0 = L_1;
			IL2CPP_LEAVE(0x17, FINALLY_0011);
		}

IL_000c:
		{
			; // IL_000c: leave IL_0017
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0011;
	}

FINALLY_0011:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(17)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(17)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0017:
	{
		IntPtr_t L_2 = V_0;
		return L_2;
	}
}
// System.IntPtr UnityEngine.AndroidJNISafe::GetStaticMethodID(System.IntPtr,System.String,System.String)
extern "C"  IntPtr_t AndroidJNISafe_GetStaticMethodID_m2373470649 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, String_t* ___name1, String_t* ___sig2, const MethodInfo* method)
{
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___clazz0;
			String_t* L_1 = ___name1;
			String_t* L_2 = ___sig2;
			IntPtr_t L_3 = AndroidJNI_GetStaticMethodID_m2473492300(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
			V_0 = L_3;
			IL2CPP_LEAVE(0x19, FINALLY_0013);
		}

IL_000e:
		{
			; // IL_000e: leave IL_0019
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0019:
	{
		IntPtr_t L_4 = V_0;
		return L_4;
	}
}
// System.IntPtr UnityEngine.AndroidJNISafe::GetMethodID(System.IntPtr,System.String,System.String)
extern "C"  IntPtr_t AndroidJNISafe_GetMethodID_m3184876807 (Il2CppObject * __this /* static, unused */, IntPtr_t ___obj0, String_t* ___name1, String_t* ___sig2, const MethodInfo* method)
{
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___obj0;
			String_t* L_1 = ___name1;
			String_t* L_2 = ___sig2;
			IntPtr_t L_3 = AndroidJNI_GetMethodID_m2948332762(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
			V_0 = L_3;
			IL2CPP_LEAVE(0x19, FINALLY_0013);
		}

IL_000e:
		{
			; // IL_000e: leave IL_0019
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0019:
	{
		IntPtr_t L_4 = V_0;
		return L_4;
	}
}
// System.IntPtr UnityEngine.AndroidJNISafe::GetFieldID(System.IntPtr,System.String,System.String)
extern "C"  IntPtr_t AndroidJNISafe_GetFieldID_m1044283870 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, String_t* ___name1, String_t* ___sig2, const MethodInfo* method)
{
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___clazz0;
			String_t* L_1 = ___name1;
			String_t* L_2 = ___sig2;
			IntPtr_t L_3 = AndroidJNI_GetFieldID_m3253410731(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
			V_0 = L_3;
			IL2CPP_LEAVE(0x19, FINALLY_0013);
		}

IL_000e:
		{
			; // IL_000e: leave IL_0019
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0019:
	{
		IntPtr_t L_4 = V_0;
		return L_4;
	}
}
// System.IntPtr UnityEngine.AndroidJNISafe::GetStaticFieldID(System.IntPtr,System.String,System.String)
extern "C"  IntPtr_t AndroidJNISafe_GetStaticFieldID_m3373414124 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, String_t* ___name1, String_t* ___sig2, const MethodInfo* method)
{
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___clazz0;
			String_t* L_1 = ___name1;
			String_t* L_2 = ___sig2;
			IntPtr_t L_3 = AndroidJNI_GetStaticFieldID_m3515187961(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
			V_0 = L_3;
			IL2CPP_LEAVE(0x19, FINALLY_0013);
		}

IL_000e:
		{
			; // IL_000e: leave IL_0019
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0019:
	{
		IntPtr_t L_4 = V_0;
		return L_4;
	}
}
// System.IntPtr UnityEngine.AndroidJNISafe::FromReflectedMethod(System.IntPtr)
extern "C"  IntPtr_t AndroidJNISafe_FromReflectedMethod_m103801358 (Il2CppObject * __this /* static, unused */, IntPtr_t ___refMethod0, const MethodInfo* method)
{
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___refMethod0;
			IntPtr_t L_1 = AndroidJNI_FromReflectedMethod_m352872801(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			V_0 = L_1;
			IL2CPP_LEAVE(0x17, FINALLY_0011);
		}

IL_000c:
		{
			; // IL_000c: leave IL_0017
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0011;
	}

FINALLY_0011:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(17)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(17)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0017:
	{
		IntPtr_t L_2 = V_0;
		return L_2;
	}
}
// System.IntPtr UnityEngine.AndroidJNISafe::FromReflectedField(System.IntPtr)
extern "C"  IntPtr_t AndroidJNISafe_FromReflectedField_m2369452317 (Il2CppObject * __this /* static, unused */, IntPtr_t ___refField0, const MethodInfo* method)
{
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___refField0;
			IntPtr_t L_1 = AndroidJNI_FromReflectedField_m576371562(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			V_0 = L_1;
			IL2CPP_LEAVE(0x17, FINALLY_0011);
		}

IL_000c:
		{
			; // IL_000c: leave IL_0017
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0011;
	}

FINALLY_0011:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(17)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(17)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0017:
	{
		IntPtr_t L_2 = V_0;
		return L_2;
	}
}
// System.IntPtr UnityEngine.AndroidJNISafe::FindClass(System.String)
extern "C"  IntPtr_t AndroidJNISafe_FindClass_m952975504 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_0 = ___name0;
			IntPtr_t L_1 = AndroidJNI_FindClass_m1230259107(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			V_0 = L_1;
			IL2CPP_LEAVE(0x17, FINALLY_0011);
		}

IL_000c:
		{
			; // IL_000c: leave IL_0017
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0011;
	}

FINALLY_0011:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(17)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(17)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0017:
	{
		IntPtr_t L_2 = V_0;
		return L_2;
	}
}
// System.IntPtr UnityEngine.AndroidJNISafe::NewObject(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  IntPtr_t AndroidJNISafe_NewObject_m2725834966 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___clazz0;
			IntPtr_t L_1 = ___methodID1;
			jvalueU5BU5D_t1723627146* L_2 = ___args2;
			IntPtr_t L_3 = AndroidJNI_NewObject_m2867608803(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
			V_0 = L_3;
			IL2CPP_LEAVE(0x19, FINALLY_0013);
		}

IL_000e:
		{
			; // IL_000e: leave IL_0019
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0019:
	{
		IntPtr_t L_4 = V_0;
		return L_4;
	}
}
// System.IntPtr UnityEngine.AndroidJNISafe::GetStaticObjectField(System.IntPtr,System.IntPtr)
extern "C"  IntPtr_t AndroidJNISafe_GetStaticObjectField_m1769477468 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___fieldID1, const MethodInfo* method)
{
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___clazz0;
			IntPtr_t L_1 = ___fieldID1;
			IntPtr_t L_2 = AndroidJNI_GetStaticObjectField_m3723206057(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
			V_0 = L_2;
			IL2CPP_LEAVE(0x18, FINALLY_0012);
		}

IL_000d:
		{
			; // IL_000d: leave IL_0018
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x18, IL_0018)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0018:
	{
		IntPtr_t L_3 = V_0;
		return L_3;
	}
}
// System.String UnityEngine.AndroidJNISafe::GetStaticStringField(System.IntPtr,System.IntPtr)
extern "C"  String_t* AndroidJNISafe_GetStaticStringField_m3486277056 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___fieldID1, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___clazz0;
			IntPtr_t L_1 = ___fieldID1;
			String_t* L_2 = AndroidJNI_GetStaticStringField_m1861664525(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
			V_0 = L_2;
			IL2CPP_LEAVE(0x18, FINALLY_0012);
		}

IL_000d:
		{
			; // IL_000d: leave IL_0018
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x18, IL_0018)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0018:
	{
		String_t* L_3 = V_0;
		return L_3;
	}
}
// System.Char UnityEngine.AndroidJNISafe::GetStaticCharField(System.IntPtr,System.IntPtr)
extern "C"  Il2CppChar AndroidJNISafe_GetStaticCharField_m1351765194 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___fieldID1, const MethodInfo* method)
{
	Il2CppChar V_0 = 0x0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___clazz0;
			IntPtr_t L_1 = ___fieldID1;
			Il2CppChar L_2 = AndroidJNI_GetStaticCharField_m448194519(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
			V_0 = L_2;
			IL2CPP_LEAVE(0x18, FINALLY_0012);
		}

IL_000d:
		{
			; // IL_000d: leave IL_0018
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x18, IL_0018)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0018:
	{
		Il2CppChar L_3 = V_0;
		return L_3;
	}
}
// System.Double UnityEngine.AndroidJNISafe::GetStaticDoubleField(System.IntPtr,System.IntPtr)
extern "C"  double AndroidJNISafe_GetStaticDoubleField_m3445626176 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___fieldID1, const MethodInfo* method)
{
	double V_0 = 0.0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___clazz0;
			IntPtr_t L_1 = ___fieldID1;
			double L_2 = AndroidJNI_GetStaticDoubleField_m2149996173(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
			V_0 = L_2;
			IL2CPP_LEAVE(0x18, FINALLY_0012);
		}

IL_000d:
		{
			; // IL_000d: leave IL_0018
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x18, IL_0018)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0018:
	{
		double L_3 = V_0;
		return L_3;
	}
}
// System.Single UnityEngine.AndroidJNISafe::GetStaticFloatField(System.IntPtr,System.IntPtr)
extern "C"  float AndroidJNISafe_GetStaticFloatField_m2553918180 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___fieldID1, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___clazz0;
			IntPtr_t L_1 = ___fieldID1;
			float L_2 = AndroidJNI_GetStaticFloatField_m2480042103(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
			V_0 = L_2;
			IL2CPP_LEAVE(0x18, FINALLY_0012);
		}

IL_000d:
		{
			; // IL_000d: leave IL_0018
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x18, IL_0018)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0018:
	{
		float L_3 = V_0;
		return L_3;
	}
}
// System.Int64 UnityEngine.AndroidJNISafe::GetStaticLongField(System.IntPtr,System.IntPtr)
extern "C"  int64_t AndroidJNISafe_GetStaticLongField_m1440712973 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___fieldID1, const MethodInfo* method)
{
	int64_t V_0 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___clazz0;
			IntPtr_t L_1 = ___fieldID1;
			int64_t L_2 = AndroidJNI_GetStaticLongField_m724040346(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
			V_0 = L_2;
			IL2CPP_LEAVE(0x18, FINALLY_0012);
		}

IL_000d:
		{
			; // IL_000d: leave IL_0018
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x18, IL_0018)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0018:
	{
		int64_t L_3 = V_0;
		return L_3;
	}
}
// System.Int16 UnityEngine.AndroidJNISafe::GetStaticShortField(System.IntPtr,System.IntPtr)
extern "C"  int16_t AndroidJNISafe_GetStaticShortField_m3249645170 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___fieldID1, const MethodInfo* method)
{
	int16_t V_0 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___clazz0;
			IntPtr_t L_1 = ___fieldID1;
			int16_t L_2 = AndroidJNI_GetStaticShortField_m3116124933(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
			V_0 = L_2;
			IL2CPP_LEAVE(0x18, FINALLY_0012);
		}

IL_000d:
		{
			; // IL_000d: leave IL_0018
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x18, IL_0018)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0018:
	{
		int16_t L_3 = V_0;
		return L_3;
	}
}
// System.Byte UnityEngine.AndroidJNISafe::GetStaticByteField(System.IntPtr,System.IntPtr)
extern "C"  uint8_t AndroidJNISafe_GetStaticByteField_m292843694 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___fieldID1, const MethodInfo* method)
{
	uint8_t V_0 = 0x0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___clazz0;
			IntPtr_t L_1 = ___fieldID1;
			uint8_t L_2 = AndroidJNI_GetStaticByteField_m1987720379(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
			V_0 = L_2;
			IL2CPP_LEAVE(0x18, FINALLY_0012);
		}

IL_000d:
		{
			; // IL_000d: leave IL_0018
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x18, IL_0018)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0018:
	{
		uint8_t L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.AndroidJNISafe::GetStaticBooleanField(System.IntPtr,System.IntPtr)
extern "C"  bool AndroidJNISafe_GetStaticBooleanField_m3886861258 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___fieldID1, const MethodInfo* method)
{
	bool V_0 = false;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___clazz0;
			IntPtr_t L_1 = ___fieldID1;
			bool L_2 = AndroidJNI_GetStaticBooleanField_m4125732253(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
			V_0 = L_2;
			IL2CPP_LEAVE(0x18, FINALLY_0012);
		}

IL_000d:
		{
			; // IL_000d: leave IL_0018
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x18, IL_0018)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0018:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Int32 UnityEngine.AndroidJNISafe::GetStaticIntField(System.IntPtr,System.IntPtr)
extern "C"  int32_t AndroidJNISafe_GetStaticIntField_m1159703659 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___fieldID1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___clazz0;
			IntPtr_t L_1 = ___fieldID1;
			int32_t L_2 = AndroidJNI_GetStaticIntField_m642583742(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
			V_0 = L_2;
			IL2CPP_LEAVE(0x18, FINALLY_0012);
		}

IL_000d:
		{
			; // IL_000d: leave IL_0018
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x18, IL_0018)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0018:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.IntPtr UnityEngine.AndroidJNISafe::CallStaticObjectMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  IntPtr_t AndroidJNISafe_CallStaticObjectMethod_m4224289535 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___clazz0;
			IntPtr_t L_1 = ___methodID1;
			jvalueU5BU5D_t1723627146* L_2 = ___args2;
			IntPtr_t L_3 = AndroidJNI_CallStaticObjectMethod_m3595893778(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
			V_0 = L_3;
			IL2CPP_LEAVE(0x19, FINALLY_0013);
		}

IL_000e:
		{
			; // IL_000e: leave IL_0019
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0019:
	{
		IntPtr_t L_4 = V_0;
		return L_4;
	}
}
// System.String UnityEngine.AndroidJNISafe::CallStaticStringMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  String_t* AndroidJNISafe_CallStaticStringMethod_m3883693439 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___clazz0;
			IntPtr_t L_1 = ___methodID1;
			jvalueU5BU5D_t1723627146* L_2 = ___args2;
			String_t* L_3 = AndroidJNI_CallStaticStringMethod_m1017965970(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
			V_0 = L_3;
			IL2CPP_LEAVE(0x19, FINALLY_0013);
		}

IL_000e:
		{
			; // IL_000e: leave IL_0019
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0019:
	{
		String_t* L_4 = V_0;
		return L_4;
	}
}
// System.Char UnityEngine.AndroidJNISafe::CallStaticCharMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  Il2CppChar AndroidJNISafe_CallStaticCharMethod_m1179522847 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	Il2CppChar V_0 = 0x0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___clazz0;
			IntPtr_t L_1 = ___methodID1;
			jvalueU5BU5D_t1723627146* L_2 = ___args2;
			Il2CppChar L_3 = AndroidJNI_CallStaticCharMethod_m289365362(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
			V_0 = L_3;
			IL2CPP_LEAVE(0x19, FINALLY_0013);
		}

IL_000e:
		{
			; // IL_000e: leave IL_0019
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0019:
	{
		Il2CppChar L_4 = V_0;
		return L_4;
	}
}
// System.Double UnityEngine.AndroidJNISafe::CallStaticDoubleMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  double AndroidJNISafe_CallStaticDoubleMethod_m3073281407 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	double V_0 = 0.0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___clazz0;
			IntPtr_t L_1 = ___methodID1;
			jvalueU5BU5D_t1723627146* L_2 = ___args2;
			double L_3 = AndroidJNI_CallStaticDoubleMethod_m2003248530(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
			V_0 = L_3;
			IL2CPP_LEAVE(0x19, FINALLY_0013);
		}

IL_000e:
		{
			; // IL_000e: leave IL_0019
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0019:
	{
		double L_4 = V_0;
		return L_4;
	}
}
// System.Single UnityEngine.AndroidJNISafe::CallStaticFloatMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  float AndroidJNISafe_CallStaticFloatMethod_m1967472625 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___clazz0;
			IntPtr_t L_1 = ___methodID1;
			jvalueU5BU5D_t1723627146* L_2 = ___args2;
			float L_3 = AndroidJNI_CallStaticFloatMethod_m3389844734(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
			V_0 = L_3;
			IL2CPP_LEAVE(0x19, FINALLY_0013);
		}

IL_000e:
		{
			; // IL_000e: leave IL_0019
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0019:
	{
		float L_4 = V_0;
		return L_4;
	}
}
// System.Int64 UnityEngine.AndroidJNISafe::CallStaticLongMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  int64_t AndroidJNISafe_CallStaticLongMethod_m2663755016 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	int64_t V_0 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___clazz0;
			IntPtr_t L_1 = ___methodID1;
			jvalueU5BU5D_t1723627146* L_2 = ___args2;
			int64_t L_3 = AndroidJNI_CallStaticLongMethod_m3580480731(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
			V_0 = L_3;
			IL2CPP_LEAVE(0x19, FINALLY_0013);
		}

IL_000e:
		{
			; // IL_000e: leave IL_0019
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0019:
	{
		int64_t L_4 = V_0;
		return L_4;
	}
}
// System.Int16 UnityEngine.AndroidJNISafe::CallStaticShortMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  int16_t AndroidJNISafe_CallStaticShortMethod_m3685346531 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	int16_t V_0 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___clazz0;
			IntPtr_t L_1 = ___methodID1;
			jvalueU5BU5D_t1723627146* L_2 = ___args2;
			int16_t L_3 = AndroidJNI_CallStaticShortMethod_m751555312(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
			V_0 = L_3;
			IL2CPP_LEAVE(0x19, FINALLY_0013);
		}

IL_000e:
		{
			; // IL_000e: leave IL_0019
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0019:
	{
		int16_t L_4 = V_0;
		return L_4;
	}
}
// System.Byte UnityEngine.AndroidJNISafe::CallStaticByteMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  uint8_t AndroidJNISafe_CallStaticByteMethod_m821820383 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	uint8_t V_0 = 0x0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___clazz0;
			IntPtr_t L_1 = ___methodID1;
			jvalueU5BU5D_t1723627146* L_2 = ___args2;
			uint8_t L_3 = AndroidJNI_CallStaticByteMethod_m4090362162(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
			V_0 = L_3;
			IL2CPP_LEAVE(0x19, FINALLY_0013);
		}

IL_000e:
		{
			; // IL_000e: leave IL_0019
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0019:
	{
		uint8_t L_4 = V_0;
		return L_4;
	}
}
// System.Boolean UnityEngine.AndroidJNISafe::CallStaticBooleanMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  bool AndroidJNISafe_CallStaticBooleanMethod_m998562403 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	bool V_0 = false;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___clazz0;
			IntPtr_t L_1 = ___methodID1;
			jvalueU5BU5D_t1723627146* L_2 = ___args2;
			bool L_3 = AndroidJNI_CallStaticBooleanMethod_m4255357744(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
			V_0 = L_3;
			IL2CPP_LEAVE(0x19, FINALLY_0013);
		}

IL_000e:
		{
			; // IL_000e: leave IL_0019
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0019:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Int32 UnityEngine.AndroidJNISafe::CallStaticIntMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  int32_t AndroidJNISafe_CallStaticIntMethod_m4213999952 (Il2CppObject * __this /* static, unused */, IntPtr_t ___clazz0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___clazz0;
			IntPtr_t L_1 = ___methodID1;
			jvalueU5BU5D_t1723627146* L_2 = ___args2;
			int32_t L_3 = AndroidJNI_CallStaticIntMethod_m1635023773(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
			V_0 = L_3;
			IL2CPP_LEAVE(0x19, FINALLY_0013);
		}

IL_000e:
		{
			; // IL_000e: leave IL_0019
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0019:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
// System.Void UnityEngine.AndroidJNISafe::CallVoidMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  void AndroidJNISafe_CallVoidMethod_m2934136849 (Il2CppObject * __this /* static, unused */, IntPtr_t ___obj0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		IntPtr_t L_0 = ___obj0;
		IntPtr_t L_1 = ___methodID1;
		jvalueU5BU5D_t1723627146* L_2 = ___args2;
		AndroidJNI_CallVoidMethod_m3567923620(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000d);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_000d;
	}

FINALLY_000d:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(13)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(13)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0013:
	{
		return;
	}
}
// System.IntPtr UnityEngine.AndroidJNISafe::CallObjectMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  IntPtr_t AndroidJNISafe_CallObjectMethod_m3860035057 (Il2CppObject * __this /* static, unused */, IntPtr_t ___obj0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___obj0;
			IntPtr_t L_1 = ___methodID1;
			jvalueU5BU5D_t1723627146* L_2 = ___args2;
			IntPtr_t L_3 = AndroidJNI_CallObjectMethod_m637661508(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
			V_0 = L_3;
			IL2CPP_LEAVE(0x19, FINALLY_0013);
		}

IL_000e:
		{
			; // IL_000e: leave IL_0019
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0019:
	{
		IntPtr_t L_4 = V_0;
		return L_4;
	}
}
// System.String UnityEngine.AndroidJNISafe::CallStringMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  String_t* AndroidJNISafe_CallStringMethod_m737950449 (Il2CppObject * __this /* static, unused */, IntPtr_t ___obj0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___obj0;
			IntPtr_t L_1 = ___methodID1;
			jvalueU5BU5D_t1723627146* L_2 = ___args2;
			String_t* L_3 = AndroidJNI_CallStringMethod_m3969973572(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
			V_0 = L_3;
			IL2CPP_LEAVE(0x19, FINALLY_0013);
		}

IL_000e:
		{
			; // IL_000e: leave IL_0019
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0019:
	{
		String_t* L_4 = V_0;
		return L_4;
	}
}
// System.Char UnityEngine.AndroidJNISafe::CallCharMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  Il2CppChar AndroidJNISafe_CallCharMethod_m3665686865 (Il2CppObject * __this /* static, unused */, IntPtr_t ___obj0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	Il2CppChar V_0 = 0x0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___obj0;
			IntPtr_t L_1 = ___methodID1;
			jvalueU5BU5D_t1723627146* L_2 = ___args2;
			Il2CppChar L_3 = AndroidJNI_CallCharMethod_m1814770148(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
			V_0 = L_3;
			IL2CPP_LEAVE(0x19, FINALLY_0013);
		}

IL_000e:
		{
			; // IL_000e: leave IL_0019
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0019:
	{
		Il2CppChar L_4 = V_0;
		return L_4;
	}
}
// System.Double UnityEngine.AndroidJNISafe::CallDoubleMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  double AndroidJNISafe_CallDoubleMethod_m119571185 (Il2CppObject * __this /* static, unused */, IntPtr_t ___obj0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	double V_0 = 0.0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___obj0;
			IntPtr_t L_1 = ___methodID1;
			jvalueU5BU5D_t1723627146* L_2 = ___args2;
			double L_3 = AndroidJNI_CallDoubleMethod_m2741331268(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
			V_0 = L_3;
			IL2CPP_LEAVE(0x19, FINALLY_0013);
		}

IL_000e:
		{
			; // IL_000e: leave IL_0019
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0019:
	{
		double L_4 = V_0;
		return L_4;
	}
}
// System.Single UnityEngine.AndroidJNISafe::CallFloatMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  float AndroidJNISafe_CallFloatMethod_m3123943807 (Il2CppObject * __this /* static, unused */, IntPtr_t ___obj0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___obj0;
			IntPtr_t L_1 = ___methodID1;
			jvalueU5BU5D_t1723627146* L_2 = ___args2;
			float L_3 = AndroidJNI_CallFloatMethod_m4115747916(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
			V_0 = L_3;
			IL2CPP_LEAVE(0x19, FINALLY_0013);
		}

IL_000e:
		{
			; // IL_000e: leave IL_0019
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0019:
	{
		float L_4 = V_0;
		return L_4;
	}
}
// System.Int64 UnityEngine.AndroidJNISafe::CallLongMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  int64_t AndroidJNISafe_CallLongMethod_m4083357050 (Il2CppObject * __this /* static, unused */, IntPtr_t ___obj0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	int64_t V_0 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___obj0;
			IntPtr_t L_1 = ___methodID1;
			jvalueU5BU5D_t1723627146* L_2 = ___args2;
			int64_t L_3 = AndroidJNI_CallLongMethod_m4476813(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
			V_0 = L_3;
			IL2CPP_LEAVE(0x19, FINALLY_0013);
		}

IL_000e:
		{
			; // IL_000e: leave IL_0019
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0019:
	{
		int64_t L_4 = V_0;
		return L_4;
	}
}
// System.Int16 UnityEngine.AndroidJNISafe::CallShortMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  int16_t AndroidJNISafe_CallShortMethod_m1081953265 (Il2CppObject * __this /* static, unused */, IntPtr_t ___obj0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	int16_t V_0 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___obj0;
			IntPtr_t L_1 = ___methodID1;
			jvalueU5BU5D_t1723627146* L_2 = ___args2;
			int16_t L_3 = AndroidJNI_CallShortMethod_m1918107582(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
			V_0 = L_3;
			IL2CPP_LEAVE(0x19, FINALLY_0013);
		}

IL_000e:
		{
			; // IL_000e: leave IL_0019
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0019:
	{
		int16_t L_4 = V_0;
		return L_4;
	}
}
// System.Byte UnityEngine.AndroidJNISafe::CallByteMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  uint8_t AndroidJNISafe_CallByteMethod_m2801547409 (Il2CppObject * __this /* static, unused */, IntPtr_t ___obj0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	uint8_t V_0 = 0x0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___obj0;
			IntPtr_t L_1 = ___methodID1;
			jvalueU5BU5D_t1723627146* L_2 = ___args2;
			uint8_t L_3 = AndroidJNI_CallByteMethod_m4179587108(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
			V_0 = L_3;
			IL2CPP_LEAVE(0x19, FINALLY_0013);
		}

IL_000e:
		{
			; // IL_000e: leave IL_0019
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0019:
	{
		uint8_t L_4 = V_0;
		return L_4;
	}
}
// System.Boolean UnityEngine.AndroidJNISafe::CallBooleanMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  bool AndroidJNISafe_CallBooleanMethod_m2973748465 (Il2CppObject * __this /* static, unused */, IntPtr_t ___obj0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	bool V_0 = false;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___obj0;
			IntPtr_t L_1 = ___methodID1;
			jvalueU5BU5D_t1723627146* L_2 = ___args2;
			bool L_3 = AndroidJNI_CallBooleanMethod_m2659085182(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
			V_0 = L_3;
			IL2CPP_LEAVE(0x19, FINALLY_0013);
		}

IL_000e:
		{
			; // IL_000e: leave IL_0019
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0019:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Int32 UnityEngine.AndroidJNISafe::CallIntMethod(System.IntPtr,System.IntPtr,UnityEngine.jvalue[])
extern "C"  int32_t AndroidJNISafe_CallIntMethod_m711632734 (Il2CppObject * __this /* static, unused */, IntPtr_t ___obj0, IntPtr_t ___methodID1, jvalueU5BU5D_t1723627146* ___args2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___obj0;
			IntPtr_t L_1 = ___methodID1;
			jvalueU5BU5D_t1723627146* L_2 = ___args2;
			int32_t L_3 = AndroidJNI_CallIntMethod_m2566231915(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
			V_0 = L_3;
			IL2CPP_LEAVE(0x19, FINALLY_0013);
		}

IL_000e:
		{
			; // IL_000e: leave IL_0019
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(19)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x19, IL_0019)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0019:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
// System.Char[] UnityEngine.AndroidJNISafe::FromCharArray(System.IntPtr)
extern "C"  CharU5BU5D_t3324145743* AndroidJNISafe_FromCharArray_m2296041519 (Il2CppObject * __this /* static, unused */, IntPtr_t ___array0, const MethodInfo* method)
{
	CharU5BU5D_t3324145743* V_0 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___array0;
			CharU5BU5D_t3324145743* L_1 = AndroidJNI_FromCharArray_m325115202(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			V_0 = L_1;
			IL2CPP_LEAVE(0x17, FINALLY_0011);
		}

IL_000c:
		{
			; // IL_000c: leave IL_0017
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0011;
	}

FINALLY_0011:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(17)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(17)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0017:
	{
		CharU5BU5D_t3324145743* L_2 = V_0;
		return L_2;
	}
}
// System.Double[] UnityEngine.AndroidJNISafe::FromDoubleArray(System.IntPtr)
extern "C"  DoubleU5BU5D_t2145413704* AndroidJNISafe_FromDoubleArray_m1010842127 (Il2CppObject * __this /* static, unused */, IntPtr_t ___array0, const MethodInfo* method)
{
	DoubleU5BU5D_t2145413704* V_0 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___array0;
			DoubleU5BU5D_t2145413704* L_1 = AndroidJNI_FromDoubleArray_m2441308386(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			V_0 = L_1;
			IL2CPP_LEAVE(0x17, FINALLY_0011);
		}

IL_000c:
		{
			; // IL_000c: leave IL_0017
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0011;
	}

FINALLY_0011:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(17)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(17)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0017:
	{
		DoubleU5BU5D_t2145413704* L_2 = V_0;
		return L_2;
	}
}
// System.Single[] UnityEngine.AndroidJNISafe::FromFloatArray(System.IntPtr)
extern "C"  SingleU5BU5D_t2316563989* AndroidJNISafe_FromFloatArray_m2529334857 (Il2CppObject * __this /* static, unused */, IntPtr_t ___array0, const MethodInfo* method)
{
	SingleU5BU5D_t2316563989* V_0 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___array0;
			SingleU5BU5D_t2316563989* L_1 = AndroidJNI_FromFloatArray_m1756058774(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			V_0 = L_1;
			IL2CPP_LEAVE(0x17, FINALLY_0011);
		}

IL_000c:
		{
			; // IL_000c: leave IL_0017
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0011;
	}

FINALLY_0011:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(17)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(17)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0017:
	{
		SingleU5BU5D_t2316563989* L_2 = V_0;
		return L_2;
	}
}
// System.Int64[] UnityEngine.AndroidJNISafe::FromLongArray(System.IntPtr)
extern "C"  Int64U5BU5D_t2174042770* AndroidJNISafe_FromLongArray_m1200701464 (Il2CppObject * __this /* static, unused */, IntPtr_t ___array0, const MethodInfo* method)
{
	Int64U5BU5D_t2174042770* V_0 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___array0;
			Int64U5BU5D_t2174042770* L_1 = AndroidJNI_FromLongArray_m2965481131(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			V_0 = L_1;
			IL2CPP_LEAVE(0x17, FINALLY_0011);
		}

IL_000c:
		{
			; // IL_000c: leave IL_0017
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0011;
	}

FINALLY_0011:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(17)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(17)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0017:
	{
		Int64U5BU5D_t2174042770* L_2 = V_0;
		return L_2;
	}
}
// System.Int16[] UnityEngine.AndroidJNISafe::FromShortArray(System.IntPtr)
extern "C"  Int16U5BU5D_t801762735* AndroidJNISafe_FromShortArray_m4045311291 (Il2CppObject * __this /* static, unused */, IntPtr_t ___array0, const MethodInfo* method)
{
	Int16U5BU5D_t801762735* V_0 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___array0;
			Int16U5BU5D_t801762735* L_1 = AndroidJNI_FromShortArray_m2832106632(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			V_0 = L_1;
			IL2CPP_LEAVE(0x17, FINALLY_0011);
		}

IL_000c:
		{
			; // IL_000c: leave IL_0017
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0011;
	}

FINALLY_0011:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(17)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(17)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0017:
	{
		Int16U5BU5D_t801762735* L_2 = V_0;
		return L_2;
	}
}
// System.Byte[] UnityEngine.AndroidJNISafe::FromByteArray(System.IntPtr)
extern "C"  ByteU5BU5D_t4260760469* AndroidJNISafe_FromByteArray_m1064061935 (Il2CppObject * __this /* static, unused */, IntPtr_t ___array0, const MethodInfo* method)
{
	ByteU5BU5D_t4260760469* V_0 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___array0;
			ByteU5BU5D_t4260760469* L_1 = AndroidJNI_FromByteArray_m1645613058(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			V_0 = L_1;
			IL2CPP_LEAVE(0x17, FINALLY_0011);
		}

IL_000c:
		{
			; // IL_000c: leave IL_0017
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0011;
	}

FINALLY_0011:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(17)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(17)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0017:
	{
		ByteU5BU5D_t4260760469* L_2 = V_0;
		return L_2;
	}
}
// System.Boolean[] UnityEngine.AndroidJNISafe::FromBooleanArray(System.IntPtr)
extern "C"  BooleanU5BU5D_t3456302923* AndroidJNISafe_FromBooleanArray_m1181457339 (Il2CppObject * __this /* static, unused */, IntPtr_t ___array0, const MethodInfo* method)
{
	BooleanU5BU5D_t3456302923* V_0 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___array0;
			BooleanU5BU5D_t3456302923* L_1 = AndroidJNI_FromBooleanArray_m3435863496(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			V_0 = L_1;
			IL2CPP_LEAVE(0x17, FINALLY_0011);
		}

IL_000c:
		{
			; // IL_000c: leave IL_0017
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0011;
	}

FINALLY_0011:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(17)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(17)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0017:
	{
		BooleanU5BU5D_t3456302923* L_2 = V_0;
		return L_2;
	}
}
// System.Int32[] UnityEngine.AndroidJNISafe::FromIntArray(System.IntPtr)
extern "C"  Int32U5BU5D_t3230847821* AndroidJNISafe_FromIntArray_m2726154792 (Il2CppObject * __this /* static, unused */, IntPtr_t ___array0, const MethodInfo* method)
{
	Int32U5BU5D_t3230847821* V_0 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___array0;
			Int32U5BU5D_t3230847821* L_1 = AndroidJNI_FromIntArray_m2891457973(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			V_0 = L_1;
			IL2CPP_LEAVE(0x17, FINALLY_0011);
		}

IL_000c:
		{
			; // IL_000c: leave IL_0017
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0011;
	}

FINALLY_0011:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(17)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(17)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0017:
	{
		Int32U5BU5D_t3230847821* L_2 = V_0;
		return L_2;
	}
}
// System.IntPtr UnityEngine.AndroidJNISafe::ToObjectArray(System.IntPtr[],System.IntPtr)
extern "C"  IntPtr_t AndroidJNISafe_ToObjectArray_m2343440750 (Il2CppObject * __this /* static, unused */, IntPtrU5BU5D_t3228729122* ___array0, IntPtr_t ___type1, const MethodInfo* method)
{
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtrU5BU5D_t3228729122* L_0 = ___array0;
			IntPtr_t L_1 = ___type1;
			IntPtr_t L_2 = AndroidJNI_ToObjectArray_m3438267649(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
			V_0 = L_2;
			IL2CPP_LEAVE(0x18, FINALLY_0012);
		}

IL_000d:
		{
			; // IL_000d: leave IL_0018
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(18)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x18, IL_0018)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0018:
	{
		IntPtr_t L_3 = V_0;
		return L_3;
	}
}
// System.IntPtr UnityEngine.AndroidJNISafe::ToCharArray(System.Char[])
extern "C"  IntPtr_t AndroidJNISafe_ToCharArray_m589918336 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3324145743* ___array0, const MethodInfo* method)
{
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			CharU5BU5D_t3324145743* L_0 = ___array0;
			IntPtr_t L_1 = AndroidJNI_ToCharArray_m771488467(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			V_0 = L_1;
			IL2CPP_LEAVE(0x17, FINALLY_0011);
		}

IL_000c:
		{
			; // IL_000c: leave IL_0017
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0011;
	}

FINALLY_0011:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(17)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(17)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0017:
	{
		IntPtr_t L_2 = V_0;
		return L_2;
	}
}
// System.IntPtr UnityEngine.AndroidJNISafe::ToDoubleArray(System.Double[])
extern "C"  IntPtr_t AndroidJNISafe_ToDoubleArray_m1391050336 (Il2CppObject * __this /* static, unused */, DoubleU5BU5D_t2145413704* ___array0, const MethodInfo* method)
{
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			DoubleU5BU5D_t2145413704* L_0 = ___array0;
			IntPtr_t L_1 = AndroidJNI_ToDoubleArray_m1106831155(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			V_0 = L_1;
			IL2CPP_LEAVE(0x17, FINALLY_0011);
		}

IL_000c:
		{
			; // IL_000c: leave IL_0017
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0011;
	}

FINALLY_0011:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(17)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(17)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0017:
	{
		IntPtr_t L_2 = V_0;
		return L_2;
	}
}
// System.IntPtr UnityEngine.AndroidJNISafe::ToFloatArray(System.Single[])
extern "C"  IntPtr_t AndroidJNISafe_ToFloatArray_m942365282 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t2316563989* ___array0, const MethodInfo* method)
{
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			SingleU5BU5D_t2316563989* L_0 = ___array0;
			IntPtr_t L_1 = AndroidJNI_ToFloatArray_m2734312239(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			V_0 = L_1;
			IL2CPP_LEAVE(0x17, FINALLY_0011);
		}

IL_000c:
		{
			; // IL_000c: leave IL_0017
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0011;
	}

FINALLY_0011:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(17)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(17)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0017:
	{
		IntPtr_t L_2 = V_0;
		return L_2;
	}
}
// System.IntPtr UnityEngine.AndroidJNISafe::ToLongArray(System.Int64[])
extern "C"  IntPtr_t AndroidJNISafe_ToLongArray_m2453751525 (Il2CppObject * __this /* static, unused */, Int64U5BU5D_t2174042770* ___array0, const MethodInfo* method)
{
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Int64U5BU5D_t2174042770* L_0 = ___array0;
			IntPtr_t L_1 = AndroidJNI_ToLongArray_m3787458290(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			V_0 = L_1;
			IL2CPP_LEAVE(0x17, FINALLY_0011);
		}

IL_000c:
		{
			; // IL_000c: leave IL_0017
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0011;
	}

FINALLY_0011:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(17)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(17)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0017:
	{
		IntPtr_t L_2 = V_0;
		return L_2;
	}
}
// System.IntPtr UnityEngine.AndroidJNISafe::ToShortArray(System.Int16[])
extern "C"  IntPtr_t AndroidJNISafe_ToShortArray_m130416912 (Il2CppObject * __this /* static, unused */, Int16U5BU5D_t801762735* ___array0, const MethodInfo* method)
{
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Int16U5BU5D_t801762735* L_0 = ___array0;
			IntPtr_t L_1 = AndroidJNI_ToShortArray_m2820620963(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			V_0 = L_1;
			IL2CPP_LEAVE(0x17, FINALLY_0011);
		}

IL_000c:
		{
			; // IL_000c: leave IL_0017
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0011;
	}

FINALLY_0011:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(17)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(17)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0017:
	{
		IntPtr_t L_2 = V_0;
		return L_2;
	}
}
// System.IntPtr UnityEngine.AndroidJNISafe::ToByteArray(System.Byte[])
extern "C"  IntPtr_t AndroidJNISafe_ToByteArray_m1438477888 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___array0, const MethodInfo* method)
{
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			ByteU5BU5D_t4260760469* L_0 = ___array0;
			IntPtr_t L_1 = AndroidJNI_ToByteArray_m1620048019(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			V_0 = L_1;
			IL2CPP_LEAVE(0x17, FINALLY_0011);
		}

IL_000c:
		{
			; // IL_000c: leave IL_0017
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0011;
	}

FINALLY_0011:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(17)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(17)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0017:
	{
		IntPtr_t L_2 = V_0;
		return L_2;
	}
}
// System.IntPtr UnityEngine.AndroidJNISafe::ToBooleanArray(System.Boolean[])
extern "C"  IntPtr_t AndroidJNISafe_ToBooleanArray_m2993741840 (Il2CppObject * __this /* static, unused */, BooleanU5BU5D_t3456302923* ___array0, const MethodInfo* method)
{
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			BooleanU5BU5D_t3456302923* L_0 = ___array0;
			IntPtr_t L_1 = AndroidJNI_ToBooleanArray_m442048547(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			V_0 = L_1;
			IL2CPP_LEAVE(0x17, FINALLY_0011);
		}

IL_000c:
		{
			; // IL_000c: leave IL_0017
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0011;
	}

FINALLY_0011:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(17)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(17)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0017:
	{
		IntPtr_t L_2 = V_0;
		return L_2;
	}
}
// System.IntPtr UnityEngine.AndroidJNISafe::ToIntArray(System.Int32[])
extern "C"  IntPtr_t AndroidJNISafe_ToIntArray_m952526659 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___array0, const MethodInfo* method)
{
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Int32U5BU5D_t3230847821* L_0 = ___array0;
			IntPtr_t L_1 = AndroidJNI_ToIntArray_m1134096790(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			V_0 = L_1;
			IL2CPP_LEAVE(0x17, FINALLY_0011);
		}

IL_000c:
		{
			; // IL_000c: leave IL_0017
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0011;
	}

FINALLY_0011:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(17)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(17)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0017:
	{
		IntPtr_t L_2 = V_0;
		return L_2;
	}
}
// System.Int32 UnityEngine.AndroidJNISafe::GetArrayLength(System.IntPtr)
extern "C"  int32_t AndroidJNISafe_GetArrayLength_m832146097 (Il2CppObject * __this /* static, unused */, IntPtr_t ___array0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = ___array0;
			int32_t L_1 = AndroidJNI_GetArrayLength_m2368966142(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			V_0 = L_1;
			IL2CPP_LEAVE(0x17, FINALLY_0011);
		}

IL_000c:
		{
			; // IL_000c: leave IL_0017
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0011;
	}

FINALLY_0011:
	{ // begin finally (depth: 1)
		AndroidJNISafe_CheckException_m43129153(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(17)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(17)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0017:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.AndroidReflection::.cctor()
extern Il2CppClass* AndroidReflection_t1838616560_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3440532080;
extern Il2CppCodeGenString* _stringLiteral1432127199;
extern Il2CppCodeGenString* _stringLiteral485513135;
extern Il2CppCodeGenString* _stringLiteral1634689842;
extern Il2CppCodeGenString* _stringLiteral614573222;
extern Il2CppCodeGenString* _stringLiteral175607647;
extern Il2CppCodeGenString* _stringLiteral99766229;
extern Il2CppCodeGenString* _stringLiteral2672074595;
extern Il2CppCodeGenString* _stringLiteral589937731;
extern const uint32_t AndroidReflection__cctor_m1416718190_MetadataUsageId;
extern "C"  void AndroidReflection__cctor_m1416718190 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidReflection__cctor_m1416718190_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = AndroidJNISafe_FindClass_m952975504(NULL /*static, unused*/, _stringLiteral3440532080, /*hidden argument*/NULL);
		IntPtr_t L_1 = AndroidJNI_NewGlobalRef_m172051970(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		((AndroidReflection_t1838616560_StaticFields*)AndroidReflection_t1838616560_il2cpp_TypeInfo_var->static_fields)->set_s_ReflectionHelperClass_0(L_1);
		IntPtr_t L_2 = AndroidReflection_GetStaticMethodID_m842671816(NULL /*static, unused*/, _stringLiteral3440532080, _stringLiteral1432127199, _stringLiteral485513135, /*hidden argument*/NULL);
		((AndroidReflection_t1838616560_StaticFields*)AndroidReflection_t1838616560_il2cpp_TypeInfo_var->static_fields)->set_s_ReflectionHelperGetConstructorID_1(L_2);
		IntPtr_t L_3 = AndroidReflection_GetStaticMethodID_m842671816(NULL /*static, unused*/, _stringLiteral3440532080, _stringLiteral1634689842, _stringLiteral614573222, /*hidden argument*/NULL);
		((AndroidReflection_t1838616560_StaticFields*)AndroidReflection_t1838616560_il2cpp_TypeInfo_var->static_fields)->set_s_ReflectionHelperGetMethodID_2(L_3);
		IntPtr_t L_4 = AndroidReflection_GetStaticMethodID_m842671816(NULL /*static, unused*/, _stringLiteral3440532080, _stringLiteral175607647, _stringLiteral99766229, /*hidden argument*/NULL);
		((AndroidReflection_t1838616560_StaticFields*)AndroidReflection_t1838616560_il2cpp_TypeInfo_var->static_fields)->set_s_ReflectionHelperGetFieldID_3(L_4);
		IntPtr_t L_5 = AndroidReflection_GetStaticMethodID_m842671816(NULL /*static, unused*/, _stringLiteral3440532080, _stringLiteral2672074595, _stringLiteral589937731, /*hidden argument*/NULL);
		((AndroidReflection_t1838616560_StaticFields*)AndroidReflection_t1838616560_il2cpp_TypeInfo_var->static_fields)->set_s_ReflectionHelperNewProxyInstance_4(L_5);
		return;
	}
}
// System.Boolean UnityEngine.AndroidReflection::IsPrimitive(System.Type)
extern "C"  bool AndroidReflection_IsPrimitive_m2208855481 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___t0;
		NullCheck(L_0);
		bool L_1 = Type_get_IsPrimitive_m992199183(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.AndroidReflection::IsAssignableFrom(System.Type,System.Type)
extern "C"  bool AndroidReflection_IsAssignableFrom_m4117338608 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, Type_t * ___from1, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___t0;
		Type_t * L_1 = ___from1;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(42 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_0, L_1);
		return L_2;
	}
}
// System.IntPtr UnityEngine.AndroidReflection::GetStaticMethodID(System.String,System.String,System.String)
extern "C"  IntPtr_t AndroidReflection_GetStaticMethodID_m842671816 (Il2CppObject * __this /* static, unused */, String_t* ___clazz0, String_t* ___methodName1, String_t* ___signature2, const MethodInfo* method)
{
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	IntPtr_t V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___clazz0;
		IntPtr_t L_1 = AndroidJNISafe_FindClass_m952975504(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_2 = V_0;
			String_t* L_3 = ___methodName1;
			String_t* L_4 = ___signature2;
			IntPtr_t L_5 = AndroidJNISafe_GetStaticMethodID_m2373470649(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
			V_1 = L_5;
			IL2CPP_LEAVE(0x21, FINALLY_001a);
		}

IL_0015:
		{
			; // IL_0015: leave IL_0021
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_001a;
	}

FINALLY_001a:
	{ // begin finally (depth: 1)
		IntPtr_t L_6 = V_0;
		AndroidJNISafe_DeleteLocalRef_m2718059367(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(26)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(26)
	{
		IL2CPP_JUMP_TBL(0x21, IL_0021)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0021:
	{
		IntPtr_t L_7 = V_1;
		return L_7;
	}
}
// System.IntPtr UnityEngine.AndroidReflection::GetConstructorMember(System.IntPtr,System.String)
extern Il2CppClass* jvalueU5BU5D_t1723627146_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidReflection_t1838616560_il2cpp_TypeInfo_var;
extern const uint32_t AndroidReflection_GetConstructorMember_m671869730_MetadataUsageId;
extern "C"  IntPtr_t AndroidReflection_GetConstructorMember_m671869730 (Il2CppObject * __this /* static, unused */, IntPtr_t ___jclass0, String_t* ___signature1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidReflection_GetConstructorMember_m671869730_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	jvalueU5BU5D_t1723627146* V_0 = NULL;
	IntPtr_t V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = ((jvalueU5BU5D_t1723627146*)SZArrayNew(jvalueU5BU5D_t1723627146_il2cpp_TypeInfo_var, (uint32_t)2));
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			jvalueU5BU5D_t1723627146* L_0 = V_0;
			NullCheck(L_0);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
			IntPtr_t L_1 = ___jclass0;
			((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_l_8(L_1);
			jvalueU5BU5D_t1723627146* L_2 = V_0;
			NullCheck(L_2);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
			String_t* L_3 = ___signature1;
			IntPtr_t L_4 = AndroidJNISafe_NewStringUTF_m1650627069(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
			((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_l_8(L_4);
			IL2CPP_RUNTIME_CLASS_INIT(AndroidReflection_t1838616560_il2cpp_TypeInfo_var);
			IntPtr_t L_5 = ((AndroidReflection_t1838616560_StaticFields*)AndroidReflection_t1838616560_il2cpp_TypeInfo_var->static_fields)->get_s_ReflectionHelperClass_0();
			IntPtr_t L_6 = ((AndroidReflection_t1838616560_StaticFields*)AndroidReflection_t1838616560_il2cpp_TypeInfo_var->static_fields)->get_s_ReflectionHelperGetConstructorID_1();
			jvalueU5BU5D_t1723627146* L_7 = V_0;
			IntPtr_t L_8 = AndroidJNISafe_CallStaticObjectMethod_m4224289535(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
			V_1 = L_8;
			IL2CPP_LEAVE(0x53, FINALLY_0041);
		}

IL_003c:
		{
			; // IL_003c: leave IL_0053
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0041;
	}

FINALLY_0041:
	{ // begin finally (depth: 1)
		jvalueU5BU5D_t1723627146* L_9 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 1);
		IntPtr_t L_10 = ((L_9)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->get_l_8();
		AndroidJNISafe_DeleteLocalRef_m2718059367(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(65)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(65)
	{
		IL2CPP_JUMP_TBL(0x53, IL_0053)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0053:
	{
		IntPtr_t L_11 = V_1;
		return L_11;
	}
}
// System.IntPtr UnityEngine.AndroidReflection::GetMethodMember(System.IntPtr,System.String,System.String,System.Boolean)
extern Il2CppClass* jvalueU5BU5D_t1723627146_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidReflection_t1838616560_il2cpp_TypeInfo_var;
extern const uint32_t AndroidReflection_GetMethodMember_m2966799924_MetadataUsageId;
extern "C"  IntPtr_t AndroidReflection_GetMethodMember_m2966799924 (Il2CppObject * __this /* static, unused */, IntPtr_t ___jclass0, String_t* ___methodName1, String_t* ___signature2, bool ___isStatic3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidReflection_GetMethodMember_m2966799924_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	jvalueU5BU5D_t1723627146* V_0 = NULL;
	IntPtr_t V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = ((jvalueU5BU5D_t1723627146*)SZArrayNew(jvalueU5BU5D_t1723627146_il2cpp_TypeInfo_var, (uint32_t)4));
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			jvalueU5BU5D_t1723627146* L_0 = V_0;
			NullCheck(L_0);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
			IntPtr_t L_1 = ___jclass0;
			((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_l_8(L_1);
			jvalueU5BU5D_t1723627146* L_2 = V_0;
			NullCheck(L_2);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
			String_t* L_3 = ___methodName1;
			IntPtr_t L_4 = AndroidJNISafe_NewStringUTF_m1650627069(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
			((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_l_8(L_4);
			jvalueU5BU5D_t1723627146* L_5 = V_0;
			NullCheck(L_5);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
			String_t* L_6 = ___signature2;
			IntPtr_t L_7 = AndroidJNISafe_NewStringUTF_m1650627069(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
			((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_l_8(L_7);
			jvalueU5BU5D_t1723627146* L_8 = V_0;
			NullCheck(L_8);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 3);
			bool L_9 = ___isStatic3;
			((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))->set_z_0(L_9);
			IL2CPP_RUNTIME_CLASS_INIT(AndroidReflection_t1838616560_il2cpp_TypeInfo_var);
			IntPtr_t L_10 = ((AndroidReflection_t1838616560_StaticFields*)AndroidReflection_t1838616560_il2cpp_TypeInfo_var->static_fields)->get_s_ReflectionHelperClass_0();
			IntPtr_t L_11 = ((AndroidReflection_t1838616560_StaticFields*)AndroidReflection_t1838616560_il2cpp_TypeInfo_var->static_fields)->get_s_ReflectionHelperGetMethodID_2();
			jvalueU5BU5D_t1723627146* L_12 = V_0;
			IntPtr_t L_13 = AndroidJNISafe_CallStaticObjectMethod_m4224289535(NULL /*static, unused*/, L_10, L_11, L_12, /*hidden argument*/NULL);
			V_1 = L_13;
			IL2CPP_LEAVE(0x83, FINALLY_0060);
		}

IL_005b:
		{
			; // IL_005b: leave IL_0083
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0060;
	}

FINALLY_0060:
	{ // begin finally (depth: 1)
		jvalueU5BU5D_t1723627146* L_14 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 1);
		IntPtr_t L_15 = ((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->get_l_8();
		AndroidJNISafe_DeleteLocalRef_m2718059367(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		jvalueU5BU5D_t1723627146* L_16 = V_0;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 2);
		IntPtr_t L_17 = ((L_16)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->get_l_8();
		AndroidJNISafe_DeleteLocalRef_m2718059367(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(96)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(96)
	{
		IL2CPP_JUMP_TBL(0x83, IL_0083)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0083:
	{
		IntPtr_t L_18 = V_1;
		return L_18;
	}
}
// System.IntPtr UnityEngine.AndroidReflection::GetFieldMember(System.IntPtr,System.String,System.String,System.Boolean)
extern Il2CppClass* jvalueU5BU5D_t1723627146_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidReflection_t1838616560_il2cpp_TypeInfo_var;
extern const uint32_t AndroidReflection_GetFieldMember_m2787640223_MetadataUsageId;
extern "C"  IntPtr_t AndroidReflection_GetFieldMember_m2787640223 (Il2CppObject * __this /* static, unused */, IntPtr_t ___jclass0, String_t* ___fieldName1, String_t* ___signature2, bool ___isStatic3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidReflection_GetFieldMember_m2787640223_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	jvalueU5BU5D_t1723627146* V_0 = NULL;
	IntPtr_t V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = ((jvalueU5BU5D_t1723627146*)SZArrayNew(jvalueU5BU5D_t1723627146_il2cpp_TypeInfo_var, (uint32_t)4));
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			jvalueU5BU5D_t1723627146* L_0 = V_0;
			NullCheck(L_0);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
			IntPtr_t L_1 = ___jclass0;
			((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_l_8(L_1);
			jvalueU5BU5D_t1723627146* L_2 = V_0;
			NullCheck(L_2);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
			String_t* L_3 = ___fieldName1;
			IntPtr_t L_4 = AndroidJNISafe_NewStringUTF_m1650627069(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
			((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_l_8(L_4);
			jvalueU5BU5D_t1723627146* L_5 = V_0;
			NullCheck(L_5);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
			String_t* L_6 = ___signature2;
			IntPtr_t L_7 = AndroidJNISafe_NewStringUTF_m1650627069(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
			((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_l_8(L_7);
			jvalueU5BU5D_t1723627146* L_8 = V_0;
			NullCheck(L_8);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 3);
			bool L_9 = ___isStatic3;
			((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))->set_z_0(L_9);
			IL2CPP_RUNTIME_CLASS_INIT(AndroidReflection_t1838616560_il2cpp_TypeInfo_var);
			IntPtr_t L_10 = ((AndroidReflection_t1838616560_StaticFields*)AndroidReflection_t1838616560_il2cpp_TypeInfo_var->static_fields)->get_s_ReflectionHelperClass_0();
			IntPtr_t L_11 = ((AndroidReflection_t1838616560_StaticFields*)AndroidReflection_t1838616560_il2cpp_TypeInfo_var->static_fields)->get_s_ReflectionHelperGetFieldID_3();
			jvalueU5BU5D_t1723627146* L_12 = V_0;
			IntPtr_t L_13 = AndroidJNISafe_CallStaticObjectMethod_m4224289535(NULL /*static, unused*/, L_10, L_11, L_12, /*hidden argument*/NULL);
			V_1 = L_13;
			IL2CPP_LEAVE(0x83, FINALLY_0060);
		}

IL_005b:
		{
			; // IL_005b: leave IL_0083
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0060;
	}

FINALLY_0060:
	{ // begin finally (depth: 1)
		jvalueU5BU5D_t1723627146* L_14 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 1);
		IntPtr_t L_15 = ((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->get_l_8();
		AndroidJNISafe_DeleteLocalRef_m2718059367(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		jvalueU5BU5D_t1723627146* L_16 = V_0;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 2);
		IntPtr_t L_17 = ((L_16)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->get_l_8();
		AndroidJNISafe_DeleteLocalRef_m2718059367(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(96)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(96)
	{
		IL2CPP_JUMP_TBL(0x83, IL_0083)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0083:
	{
		IntPtr_t L_18 = V_1;
		return L_18;
	}
}
// UnityEngine.AnimationClip UnityEngine.Animation::get_clip()
extern "C"  AnimationClip_t2007702890 * Animation_get_clip_m980560434 (Animation_t1724966010 * __this, const MethodInfo* method)
{
	typedef AnimationClip_t2007702890 * (*Animation_get_clip_m980560434_ftn) (Animation_t1724966010 *);
	static Animation_get_clip_m980560434_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_get_clip_m980560434_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::get_clip()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Animation::set_clip(UnityEngine.AnimationClip)
extern "C"  void Animation_set_clip_m1663398585 (Animation_t1724966010 * __this, AnimationClip_t2007702890 * ___value0, const MethodInfo* method)
{
	typedef void (*Animation_set_clip_m1663398585_ftn) (Animation_t1724966010 *, AnimationClip_t2007702890 *);
	static Animation_set_clip_m1663398585_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_set_clip_m1663398585_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::set_clip(UnityEngine.AnimationClip)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Animation::Stop()
extern "C"  void Animation_Stop_m3675770961 (Animation_t1724966010 * __this, const MethodInfo* method)
{
	{
		Animation_INTERNAL_CALL_Stop_m1370047957(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animation::INTERNAL_CALL_Stop(UnityEngine.Animation)
extern "C"  void Animation_INTERNAL_CALL_Stop_m1370047957 (Il2CppObject * __this /* static, unused */, Animation_t1724966010 * ___self0, const MethodInfo* method)
{
	typedef void (*Animation_INTERNAL_CALL_Stop_m1370047957_ftn) (Animation_t1724966010 *);
	static Animation_INTERNAL_CALL_Stop_m1370047957_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_INTERNAL_CALL_Stop_m1370047957_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::INTERNAL_CALL_Stop(UnityEngine.Animation)");
	_il2cpp_icall_func(___self0);
}
// System.Void UnityEngine.Animation::Stop(System.String)
extern "C"  void Animation_Stop_m2745735761 (Animation_t1724966010 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		Animation_Internal_StopByName_m322505897(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animation::Internal_StopByName(System.String)
extern "C"  void Animation_Internal_StopByName_m322505897 (Animation_t1724966010 * __this, String_t* ___name0, const MethodInfo* method)
{
	typedef void (*Animation_Internal_StopByName_m322505897_ftn) (Animation_t1724966010 *, String_t*);
	static Animation_Internal_StopByName_m322505897_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_Internal_StopByName_m322505897_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::Internal_StopByName(System.String)");
	_il2cpp_icall_func(__this, ___name0);
}
// System.Void UnityEngine.Animation::Rewind(System.String)
extern "C"  void Animation_Rewind_m502732792 (Animation_t1724966010 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		Animation_Internal_RewindByName_m3555379856(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animation::Internal_RewindByName(System.String)
extern "C"  void Animation_Internal_RewindByName_m3555379856 (Animation_t1724966010 * __this, String_t* ___name0, const MethodInfo* method)
{
	typedef void (*Animation_Internal_RewindByName_m3555379856_ftn) (Animation_t1724966010 *, String_t*);
	static Animation_Internal_RewindByName_m3555379856_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_Internal_RewindByName_m3555379856_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::Internal_RewindByName(System.String)");
	_il2cpp_icall_func(__this, ___name0);
}
// System.Void UnityEngine.Animation::Rewind()
extern "C"  void Animation_Rewind_m7024458 (Animation_t1724966010 * __this, const MethodInfo* method)
{
	{
		Animation_INTERNAL_CALL_Rewind_m4178761788(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animation::INTERNAL_CALL_Rewind(UnityEngine.Animation)
extern "C"  void Animation_INTERNAL_CALL_Rewind_m4178761788 (Il2CppObject * __this /* static, unused */, Animation_t1724966010 * ___self0, const MethodInfo* method)
{
	typedef void (*Animation_INTERNAL_CALL_Rewind_m4178761788_ftn) (Animation_t1724966010 *);
	static Animation_INTERNAL_CALL_Rewind_m4178761788_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_INTERNAL_CALL_Rewind_m4178761788_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::INTERNAL_CALL_Rewind(UnityEngine.Animation)");
	_il2cpp_icall_func(___self0);
}
// UnityEngine.AnimationState UnityEngine.Animation::get_Item(System.String)
extern "C"  AnimationState_t3682323633 * Animation_get_Item_m2669576386 (Animation_t1724966010 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		AnimationState_t3682323633 * L_1 = Animation_GetState_m528810595(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.Animation::Play()
extern "C"  bool Animation_Play_m4273654237 (Animation_t1724966010 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		bool L_1 = Animation_Play_m2860559471(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.Animation::Play(UnityEngine.PlayMode)
extern "C"  bool Animation_Play_m2860559471 (Animation_t1724966010 * __this, int32_t ___mode0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode0;
		bool L_1 = Animation_PlayDefaultAnimation_m170678930(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.Animation::Play(System.String,UnityEngine.PlayMode)
extern "C"  bool Animation_Play_m1881292147 (Animation_t1724966010 * __this, String_t* ___animation0, int32_t ___mode1, const MethodInfo* method)
{
	typedef bool (*Animation_Play_m1881292147_ftn) (Animation_t1724966010 *, String_t*, int32_t);
	static Animation_Play_m1881292147_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_Play_m1881292147_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::Play(System.String,UnityEngine.PlayMode)");
	return _il2cpp_icall_func(__this, ___animation0, ___mode1);
}
// System.Boolean UnityEngine.Animation::Play(System.String)
extern "C"  bool Animation_Play_m900498501 (Animation_t1724966010 * __this, String_t* ___animation0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		String_t* L_0 = ___animation0;
		int32_t L_1 = V_0;
		bool L_2 = Animation_Play_m1881292147(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Animation::CrossFade(System.String,System.Single,UnityEngine.PlayMode)
extern "C"  void Animation_CrossFade_m216828000 (Animation_t1724966010 * __this, String_t* ___animation0, float ___fadeLength1, int32_t ___mode2, const MethodInfo* method)
{
	typedef void (*Animation_CrossFade_m216828000_ftn) (Animation_t1724966010 *, String_t*, float, int32_t);
	static Animation_CrossFade_m216828000_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_CrossFade_m216828000_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::CrossFade(System.String,System.Single,UnityEngine.PlayMode)");
	_il2cpp_icall_func(__this, ___animation0, ___fadeLength1, ___mode2);
}
// System.Void UnityEngine.Animation::Blend(System.String,System.Single,System.Single)
extern "C"  void Animation_Blend_m2812291720 (Animation_t1724966010 * __this, String_t* ___animation0, float ___targetWeight1, float ___fadeLength2, const MethodInfo* method)
{
	typedef void (*Animation_Blend_m2812291720_ftn) (Animation_t1724966010 *, String_t*, float, float);
	static Animation_Blend_m2812291720_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_Blend_m2812291720_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::Blend(System.String,System.Single,System.Single)");
	_il2cpp_icall_func(__this, ___animation0, ___targetWeight1, ___fadeLength2);
}
// System.Void UnityEngine.Animation::AddClip(UnityEngine.AnimationClip,System.String)
extern "C"  void Animation_AddClip_m3358255085 (Animation_t1724966010 * __this, AnimationClip_t2007702890 * ___clip0, String_t* ___newName1, const MethodInfo* method)
{
	{
		AnimationClip_t2007702890 * L_0 = ___clip0;
		String_t* L_1 = ___newName1;
		Animation_AddClip_m2718334733(__this, L_0, L_1, ((int32_t)-2147483648LL), ((int32_t)2147483647LL), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animation::AddClip(UnityEngine.AnimationClip,System.String,System.Int32,System.Int32,System.Boolean)
extern "C"  void Animation_AddClip_m770980048 (Animation_t1724966010 * __this, AnimationClip_t2007702890 * ___clip0, String_t* ___newName1, int32_t ___firstFrame2, int32_t ___lastFrame3, bool ___addLoopFrame4, const MethodInfo* method)
{
	typedef void (*Animation_AddClip_m770980048_ftn) (Animation_t1724966010 *, AnimationClip_t2007702890 *, String_t*, int32_t, int32_t, bool);
	static Animation_AddClip_m770980048_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_AddClip_m770980048_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::AddClip(UnityEngine.AnimationClip,System.String,System.Int32,System.Int32,System.Boolean)");
	_il2cpp_icall_func(__this, ___clip0, ___newName1, ___firstFrame2, ___lastFrame3, ___addLoopFrame4);
}
// System.Void UnityEngine.Animation::AddClip(UnityEngine.AnimationClip,System.String,System.Int32,System.Int32)
extern "C"  void Animation_AddClip_m2718334733 (Animation_t1724966010 * __this, AnimationClip_t2007702890 * ___clip0, String_t* ___newName1, int32_t ___firstFrame2, int32_t ___lastFrame3, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		AnimationClip_t2007702890 * L_0 = ___clip0;
		String_t* L_1 = ___newName1;
		int32_t L_2 = ___firstFrame2;
		int32_t L_3 = ___lastFrame3;
		bool L_4 = V_0;
		Animation_AddClip_m770980048(__this, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Animation::PlayDefaultAnimation(UnityEngine.PlayMode)
extern "C"  bool Animation_PlayDefaultAnimation_m170678930 (Animation_t1724966010 * __this, int32_t ___mode0, const MethodInfo* method)
{
	typedef bool (*Animation_PlayDefaultAnimation_m170678930_ftn) (Animation_t1724966010 *, int32_t);
	static Animation_PlayDefaultAnimation_m170678930_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_PlayDefaultAnimation_m170678930_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::PlayDefaultAnimation(UnityEngine.PlayMode)");
	return _il2cpp_icall_func(__this, ___mode0);
}
// System.Collections.IEnumerator UnityEngine.Animation::GetEnumerator()
extern Il2CppClass* Enumerator_t1374492422_il2cpp_TypeInfo_var;
extern const uint32_t Animation_GetEnumerator_m3015582503_MetadataUsageId;
extern "C"  Il2CppObject * Animation_GetEnumerator_m3015582503 (Animation_t1724966010 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Animation_GetEnumerator_m3015582503_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t1374492422 * L_0 = (Enumerator_t1374492422 *)il2cpp_codegen_object_new(Enumerator_t1374492422_il2cpp_TypeInfo_var);
		Enumerator__ctor_m1213159655(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.AnimationState UnityEngine.Animation::GetState(System.String)
extern "C"  AnimationState_t3682323633 * Animation_GetState_m528810595 (Animation_t1724966010 * __this, String_t* ___name0, const MethodInfo* method)
{
	typedef AnimationState_t3682323633 * (*Animation_GetState_m528810595_ftn) (Animation_t1724966010 *, String_t*);
	static Animation_GetState_m528810595_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_GetState_m528810595_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::GetState(System.String)");
	return _il2cpp_icall_func(__this, ___name0);
}
// UnityEngine.AnimationState UnityEngine.Animation::GetStateAtIndex(System.Int32)
extern "C"  AnimationState_t3682323633 * Animation_GetStateAtIndex_m513456147 (Animation_t1724966010 * __this, int32_t ___index0, const MethodInfo* method)
{
	typedef AnimationState_t3682323633 * (*Animation_GetStateAtIndex_m513456147_ftn) (Animation_t1724966010 *, int32_t);
	static Animation_GetStateAtIndex_m513456147_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_GetStateAtIndex_m513456147_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::GetStateAtIndex(System.Int32)");
	return _il2cpp_icall_func(__this, ___index0);
}
// System.Int32 UnityEngine.Animation::GetStateCount()
extern "C"  int32_t Animation_GetStateCount_m3537607527 (Animation_t1724966010 * __this, const MethodInfo* method)
{
	typedef int32_t (*Animation_GetStateCount_m3537607527_ftn) (Animation_t1724966010 *);
	static Animation_GetStateCount_m3537607527_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_GetStateCount_m3537607527_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::GetStateCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Animation/Enumerator::.ctor(UnityEngine.Animation)
extern "C"  void Enumerator__ctor_m1213159655 (Enumerator_t1374492422 * __this, Animation_t1724966010 * ___outer0, const MethodInfo* method)
{
	{
		__this->set_m_CurrentIndex_1((-1));
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Animation_t1724966010 * L_0 = ___outer0;
		__this->set_m_Outer_0(L_0);
		return;
	}
}
// System.Object UnityEngine.Animation/Enumerator::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m1033634519 (Enumerator_t1374492422 * __this, const MethodInfo* method)
{
	{
		Animation_t1724966010 * L_0 = __this->get_m_Outer_0();
		int32_t L_1 = __this->get_m_CurrentIndex_1();
		NullCheck(L_0);
		AnimationState_t3682323633 * L_2 = Animation_GetStateAtIndex_m513456147(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Animation/Enumerator::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1178545744 (Enumerator_t1374492422 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Animation_t1724966010 * L_0 = __this->get_m_Outer_0();
		NullCheck(L_0);
		int32_t L_1 = Animation_GetStateCount_m3537607527(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = __this->get_m_CurrentIndex_1();
		__this->set_m_CurrentIndex_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = __this->get_m_CurrentIndex_1();
		int32_t L_4 = V_0;
		return (bool)((((int32_t)L_3) < ((int32_t)L_4))? 1 : 0);
	}
}
// System.Void UnityEngine.Animation/Enumerator::Reset()
extern "C"  void Enumerator_Reset_m1014152875 (Enumerator_t1374492422 * __this, const MethodInfo* method)
{
	{
		__this->set_m_CurrentIndex_1((-1));
		return;
	}
}
// System.Void UnityEngine.AnimationClip::.ctor()
extern "C"  void AnimationClip__ctor_m3734809445 (AnimationClip_t2007702890 * __this, const MethodInfo* method)
{
	{
		Motion__ctor_m2540972279(__this, /*hidden argument*/NULL);
		AnimationClip_Internal_CreateAnimationClip_m771412448(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationClip::Internal_CreateAnimationClip(UnityEngine.AnimationClip)
extern "C"  void AnimationClip_Internal_CreateAnimationClip_m771412448 (Il2CppObject * __this /* static, unused */, AnimationClip_t2007702890 * ___self0, const MethodInfo* method)
{
	typedef void (*AnimationClip_Internal_CreateAnimationClip_m771412448_ftn) (AnimationClip_t2007702890 *);
	static AnimationClip_Internal_CreateAnimationClip_m771412448_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClip_Internal_CreateAnimationClip_m771412448_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationClip::Internal_CreateAnimationClip(UnityEngine.AnimationClip)");
	_il2cpp_icall_func(___self0);
}
// System.Void UnityEngine.AnimationClip::SetCurve(System.String,System.Type,System.String,UnityEngine.AnimationCurve)
extern "C"  void AnimationClip_SetCurve_m4139164191 (AnimationClip_t2007702890 * __this, String_t* ___relativePath0, Type_t * ___type1, String_t* ___propertyName2, AnimationCurve_t3667593487 * ___curve3, const MethodInfo* method)
{
	typedef void (*AnimationClip_SetCurve_m4139164191_ftn) (AnimationClip_t2007702890 *, String_t*, Type_t *, String_t*, AnimationCurve_t3667593487 *);
	static AnimationClip_SetCurve_m4139164191_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClip_SetCurve_m4139164191_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationClip::SetCurve(System.String,System.Type,System.String,UnityEngine.AnimationCurve)");
	_il2cpp_icall_func(__this, ___relativePath0, ___type1, ___propertyName2, ___curve3);
}
// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve__ctor_m2436282331 (AnimationCurve_t3667593487 * __this, KeyframeU5BU5D_t3589549831* ___keys0, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		KeyframeU5BU5D_t3589549831* L_0 = ___keys0;
		AnimationCurve_Init_m4012213483(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::.ctor()
extern "C"  void AnimationCurve__ctor_m4037844610 (AnimationCurve_t3667593487 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		AnimationCurve_Init_m4012213483(__this, (KeyframeU5BU5D_t3589549831*)(KeyframeU5BU5D_t3589549831*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::Cleanup()
extern "C"  void AnimationCurve_Cleanup_m386995588 (AnimationCurve_t3667593487 * __this, const MethodInfo* method)
{
	typedef void (*AnimationCurve_Cleanup_m386995588_ftn) (AnimationCurve_t3667593487 *);
	static AnimationCurve_Cleanup_m386995588_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_Cleanup_m386995588_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationCurve::Finalize()
extern "C"  void AnimationCurve_Finalize_m2382224032 (AnimationCurve_t3667593487 * __this, const MethodInfo* method)
{
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		AnimationCurve_Cleanup_m386995588(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m3027285644(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Single UnityEngine.AnimationCurve::Evaluate(System.Single)
extern "C"  float AnimationCurve_Evaluate_m547727012 (AnimationCurve_t3667593487 * __this, float ___time0, const MethodInfo* method)
{
	typedef float (*AnimationCurve_Evaluate_m547727012_ftn) (AnimationCurve_t3667593487 *, float);
	static AnimationCurve_Evaluate_m547727012_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_Evaluate_m547727012_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Evaluate(System.Single)");
	return _il2cpp_icall_func(__this, ___time0);
}
// UnityEngine.Keyframe[] UnityEngine.AnimationCurve::get_keys()
extern "C"  KeyframeU5BU5D_t3589549831* AnimationCurve_get_keys_m450535207 (AnimationCurve_t3667593487 * __this, const MethodInfo* method)
{
	{
		KeyframeU5BU5D_t3589549831* L_0 = AnimationCurve_GetKeys_m3103532930(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationCurve::set_keys(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve_set_keys_m4001159148 (AnimationCurve_t3667593487 * __this, KeyframeU5BU5D_t3589549831* ___value0, const MethodInfo* method)
{
	{
		KeyframeU5BU5D_t3589549831* L_0 = ___value0;
		AnimationCurve_SetKeys_m1256411695(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.AnimationCurve::get_length()
extern "C"  int32_t AnimationCurve_get_length_m3019229777 (AnimationCurve_t3667593487 * __this, const MethodInfo* method)
{
	typedef int32_t (*AnimationCurve_get_length_m3019229777_ftn) (AnimationCurve_t3667593487 *);
	static AnimationCurve_get_length_m3019229777_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_get_length_m3019229777_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::get_length()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationCurve::SetKeys(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve_SetKeys_m1256411695 (AnimationCurve_t3667593487 * __this, KeyframeU5BU5D_t3589549831* ___keys0, const MethodInfo* method)
{
	typedef void (*AnimationCurve_SetKeys_m1256411695_ftn) (AnimationCurve_t3667593487 *, KeyframeU5BU5D_t3589549831*);
	static AnimationCurve_SetKeys_m1256411695_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_SetKeys_m1256411695_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::SetKeys(UnityEngine.Keyframe[])");
	_il2cpp_icall_func(__this, ___keys0);
}
// UnityEngine.Keyframe[] UnityEngine.AnimationCurve::GetKeys()
extern "C"  KeyframeU5BU5D_t3589549831* AnimationCurve_GetKeys_m3103532930 (AnimationCurve_t3667593487 * __this, const MethodInfo* method)
{
	typedef KeyframeU5BU5D_t3589549831* (*AnimationCurve_GetKeys_m3103532930_ftn) (AnimationCurve_t3667593487 *);
	static AnimationCurve_GetKeys_m3103532930_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_GetKeys_m3103532930_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::GetKeys()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.AnimationCurve UnityEngine.AnimationCurve::Linear(System.Single,System.Single,System.Single,System.Single)
extern Il2CppClass* KeyframeU5BU5D_t3589549831_il2cpp_TypeInfo_var;
extern Il2CppClass* AnimationCurve_t3667593487_il2cpp_TypeInfo_var;
extern const uint32_t AnimationCurve_Linear_m3091030662_MetadataUsageId;
extern "C"  AnimationCurve_t3667593487 * AnimationCurve_Linear_m3091030662 (Il2CppObject * __this /* static, unused */, float ___timeStart0, float ___valueStart1, float ___timeEnd2, float ___valueEnd3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimationCurve_Linear_m3091030662_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	KeyframeU5BU5D_t3589549831* V_1 = NULL;
	{
		float L_0 = ___valueEnd3;
		float L_1 = ___valueStart1;
		float L_2 = ___timeEnd2;
		float L_3 = ___timeStart0;
		V_0 = ((float)((float)((float)((float)L_0-(float)L_1))/(float)((float)((float)L_2-(float)L_3))));
		KeyframeU5BU5D_t3589549831* L_4 = ((KeyframeU5BU5D_t3589549831*)SZArrayNew(KeyframeU5BU5D_t3589549831_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		float L_5 = ___timeStart0;
		float L_6 = ___valueStart1;
		float L_7 = V_0;
		Keyframe_t4079056114  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Keyframe__ctor_m3412708539(&L_8, L_5, L_6, (0.0f), L_7, /*hidden argument*/NULL);
		(*(Keyframe_t4079056114 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_8;
		KeyframeU5BU5D_t3589549831* L_9 = L_4;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 1);
		float L_10 = ___timeEnd2;
		float L_11 = ___valueEnd3;
		float L_12 = V_0;
		Keyframe_t4079056114  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Keyframe__ctor_m3412708539(&L_13, L_10, L_11, L_12, (0.0f), /*hidden argument*/NULL);
		(*(Keyframe_t4079056114 *)((L_9)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_13;
		V_1 = L_9;
		KeyframeU5BU5D_t3589549831* L_14 = V_1;
		AnimationCurve_t3667593487 * L_15 = (AnimationCurve_t3667593487 *)il2cpp_codegen_object_new(AnimationCurve_t3667593487_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m2436282331(L_15, L_14, /*hidden argument*/NULL);
		return L_15;
	}
}
// UnityEngine.WrapMode UnityEngine.AnimationCurve::get_postWrapMode()
extern "C"  int32_t AnimationCurve_get_postWrapMode_m2205068323 (AnimationCurve_t3667593487 * __this, const MethodInfo* method)
{
	typedef int32_t (*AnimationCurve_get_postWrapMode_m2205068323_ftn) (AnimationCurve_t3667593487 *);
	static AnimationCurve_get_postWrapMode_m2205068323_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_get_postWrapMode_m2205068323_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::get_postWrapMode()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationCurve::set_postWrapMode(UnityEngine.WrapMode)
extern "C"  void AnimationCurve_set_postWrapMode_m1985742504 (AnimationCurve_t3667593487 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*AnimationCurve_set_postWrapMode_m1985742504_ftn) (AnimationCurve_t3667593487 *, int32_t);
	static AnimationCurve_set_postWrapMode_m1985742504_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_set_postWrapMode_m1985742504_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::set_postWrapMode(UnityEngine.WrapMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve_Init_m4012213483 (AnimationCurve_t3667593487 * __this, KeyframeU5BU5D_t3589549831* ___keys0, const MethodInfo* method)
{
	typedef void (*AnimationCurve_Init_m4012213483_ftn) (AnimationCurve_t3667593487 *, KeyframeU5BU5D_t3589549831*);
	static AnimationCurve_Init_m4012213483_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_Init_m4012213483_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])");
	_il2cpp_icall_func(__this, ___keys0);
}
// Conversion methods for marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t3667593487_marshal_pinvoke(const AnimationCurve_t3667593487& unmarshaled, AnimationCurve_t3667593487_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void AnimationCurve_t3667593487_marshal_pinvoke_back(const AnimationCurve_t3667593487_marshaled_pinvoke& marshaled, AnimationCurve_t3667593487& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t3667593487_marshal_pinvoke_cleanup(AnimationCurve_t3667593487_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t3667593487_marshal_com(const AnimationCurve_t3667593487& unmarshaled, AnimationCurve_t3667593487_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void AnimationCurve_t3667593487_marshal_com_back(const AnimationCurve_t3667593487_marshaled_com& marshaled, AnimationCurve_t3667593487& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t3667593487_marshal_com_cleanup(AnimationCurve_t3667593487_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.AnimationEvent::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t AnimationEvent__ctor_m3893222711_MetadataUsageId;
extern "C"  void AnimationEvent__ctor_m3893222711 (AnimationEvent_t3669457594 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimationEvent__ctor_m3893222711_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		__this->set_m_Time_0((0.0f));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_m_FunctionName_1(L_0);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_m_StringParameter_2(L_1);
		__this->set_m_ObjectReferenceParameter_3((Object_t3071478659 *)NULL);
		__this->set_m_FloatParameter_4((0.0f));
		__this->set_m_IntParameter_5(0);
		__this->set_m_MessageOptions_6(0);
		__this->set_m_Source_7(0);
		__this->set_m_StateSender_8((AnimationState_t3682323633 *)NULL);
		return;
	}
}
// System.String UnityEngine.AnimationEvent::get_data()
extern "C"  String_t* AnimationEvent_get_data_m2818928483 (AnimationEvent_t3669457594 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_StringParameter_2();
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_data(System.String)
extern "C"  void AnimationEvent_set_data_m1709280462 (AnimationEvent_t3669457594 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_m_StringParameter_2(L_0);
		return;
	}
}
// System.String UnityEngine.AnimationEvent::get_stringParameter()
extern "C"  String_t* AnimationEvent_get_stringParameter_m774178145 (AnimationEvent_t3669457594 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_StringParameter_2();
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_stringParameter(System.String)
extern "C"  void AnimationEvent_set_stringParameter_m4027163538 (AnimationEvent_t3669457594 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_m_StringParameter_2(L_0);
		return;
	}
}
// System.Single UnityEngine.AnimationEvent::get_floatParameter()
extern "C"  float AnimationEvent_get_floatParameter_m585550415 (AnimationEvent_t3669457594 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_FloatParameter_4();
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_floatParameter(System.Single)
extern "C"  void AnimationEvent_set_floatParameter_m3972419412 (AnimationEvent_t3669457594 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_FloatParameter_4(L_0);
		return;
	}
}
// System.Int32 UnityEngine.AnimationEvent::get_intParameter()
extern "C"  int32_t AnimationEvent_get_intParameter_m1837369744 (AnimationEvent_t3669457594 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_IntParameter_5();
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_intParameter(System.Int32)
extern "C"  void AnimationEvent_set_intParameter_m1983832533 (AnimationEvent_t3669457594 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_m_IntParameter_5(L_0);
		return;
	}
}
// UnityEngine.Object UnityEngine.AnimationEvent::get_objectReferenceParameter()
extern "C"  Object_t3071478659 * AnimationEvent_get_objectReferenceParameter_m1449907980 (AnimationEvent_t3669457594 * __this, const MethodInfo* method)
{
	{
		Object_t3071478659 * L_0 = __this->get_m_ObjectReferenceParameter_3();
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_objectReferenceParameter(UnityEngine.Object)
extern "C"  void AnimationEvent_set_objectReferenceParameter_m2952903185 (AnimationEvent_t3669457594 * __this, Object_t3071478659 * ___value0, const MethodInfo* method)
{
	{
		Object_t3071478659 * L_0 = ___value0;
		__this->set_m_ObjectReferenceParameter_3(L_0);
		return;
	}
}
// System.String UnityEngine.AnimationEvent::get_functionName()
extern "C"  String_t* AnimationEvent_get_functionName_m508701692 (AnimationEvent_t3669457594 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_FunctionName_1();
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_functionName(System.String)
extern "C"  void AnimationEvent_set_functionName_m3347976661 (AnimationEvent_t3669457594 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_m_FunctionName_1(L_0);
		return;
	}
}
// System.Single UnityEngine.AnimationEvent::get_time()
extern "C"  float AnimationEvent_get_time_m4165710255 (AnimationEvent_t3669457594 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Time_0();
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_time(System.Single)
extern "C"  void AnimationEvent_set_time_m2746909172 (AnimationEvent_t3669457594 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Time_0(L_0);
		return;
	}
}
// UnityEngine.SendMessageOptions UnityEngine.AnimationEvent::get_messageOptions()
extern "C"  int32_t AnimationEvent_get_messageOptions_m501221094 (AnimationEvent_t3669457594 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_MessageOptions_6();
		return (int32_t)(L_0);
	}
}
// System.Void UnityEngine.AnimationEvent::set_messageOptions(UnityEngine.SendMessageOptions)
extern "C"  void AnimationEvent_set_messageOptions_m927737387 (AnimationEvent_t3669457594 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_m_MessageOptions_6(L_0);
		return;
	}
}
// System.Boolean UnityEngine.AnimationEvent::get_isFiredByLegacy()
extern "C"  bool AnimationEvent_get_isFiredByLegacy_m1265747114 (AnimationEvent_t3669457594 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Source_7();
		return (bool)((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean UnityEngine.AnimationEvent::get_isFiredByAnimator()
extern "C"  bool AnimationEvent_get_isFiredByAnimator_m877765704 (AnimationEvent_t3669457594 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Source_7();
		return (bool)((((int32_t)L_0) == ((int32_t)2))? 1 : 0);
	}
}
// UnityEngine.AnimationState UnityEngine.AnimationEvent::get_animationState()
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2304947623;
extern const uint32_t AnimationEvent_get_animationState_m2712753230_MetadataUsageId;
extern "C"  AnimationState_t3682323633 * AnimationEvent_get_animationState_m2712753230 (AnimationEvent_t3669457594 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimationEvent_get_animationState_m2712753230_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = AnimationEvent_get_isFiredByLegacy_m1265747114(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral2304947623, /*hidden argument*/NULL);
	}

IL_0015:
	{
		AnimationState_t3682323633 * L_1 = __this->get_m_StateSender_8();
		return L_1;
	}
}
// UnityEngine.AnimatorStateInfo UnityEngine.AnimationEvent::get_animatorStateInfo()
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral277642195;
extern const uint32_t AnimationEvent_get_animatorStateInfo_m194903218_MetadataUsageId;
extern "C"  AnimatorStateInfo_t323110318  AnimationEvent_get_animatorStateInfo_m194903218 (AnimationEvent_t3669457594 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimationEvent_get_animatorStateInfo_m194903218_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = AnimationEvent_get_isFiredByAnimator_m877765704(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral277642195, /*hidden argument*/NULL);
	}

IL_0015:
	{
		AnimatorStateInfo_t323110318  L_1 = __this->get_m_AnimatorStateInfo_9();
		return L_1;
	}
}
// UnityEngine.AnimatorClipInfo UnityEngine.AnimationEvent::get_animatorClipInfo()
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3626112682;
extern const uint32_t AnimationEvent_get_animatorClipInfo_m493861262_MetadataUsageId;
extern "C"  AnimatorClipInfo_t2746035113  AnimationEvent_get_animatorClipInfo_m493861262 (AnimationEvent_t3669457594 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimationEvent_get_animatorClipInfo_m493861262_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = AnimationEvent_get_isFiredByAnimator_m877765704(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral3626112682, /*hidden argument*/NULL);
	}

IL_0015:
	{
		AnimatorClipInfo_t2746035113  L_1 = __this->get_m_AnimatorClipInfo_10();
		return L_1;
	}
}
// System.Int32 UnityEngine.AnimationEvent::GetHash()
extern "C"  int32_t AnimationEvent_GetHash_m2586883417 (AnimationEvent_t3669457594 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	{
		V_0 = 0;
		String_t* L_0 = AnimationEvent_get_functionName_m508701692(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = String_GetHashCode_m471729487(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		float L_3 = AnimationEvent_get_time_m4165710255(__this, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = Single_GetHashCode_m65342520((&V_1), /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)33)*(int32_t)L_2))+(int32_t)L_4));
		int32_t L_5 = V_0;
		return L_5;
	}
}
// Conversion methods for marshalling of: UnityEngine.AnimationEvent
extern "C" void AnimationEvent_t3669457594_marshal_pinvoke(const AnimationEvent_t3669457594& unmarshaled, AnimationEvent_t3669457594_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ObjectReferenceParameter_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ObjectReferenceParameter' of type 'AnimationEvent': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ObjectReferenceParameter_3Exception);
}
extern "C" void AnimationEvent_t3669457594_marshal_pinvoke_back(const AnimationEvent_t3669457594_marshaled_pinvoke& marshaled, AnimationEvent_t3669457594& unmarshaled)
{
	Il2CppCodeGenException* ___m_ObjectReferenceParameter_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ObjectReferenceParameter' of type 'AnimationEvent': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ObjectReferenceParameter_3Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationEvent
extern "C" void AnimationEvent_t3669457594_marshal_pinvoke_cleanup(AnimationEvent_t3669457594_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AnimationEvent
extern "C" void AnimationEvent_t3669457594_marshal_com(const AnimationEvent_t3669457594& unmarshaled, AnimationEvent_t3669457594_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ObjectReferenceParameter_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ObjectReferenceParameter' of type 'AnimationEvent': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ObjectReferenceParameter_3Exception);
}
extern "C" void AnimationEvent_t3669457594_marshal_com_back(const AnimationEvent_t3669457594_marshaled_com& marshaled, AnimationEvent_t3669457594& unmarshaled)
{
	Il2CppCodeGenException* ___m_ObjectReferenceParameter_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ObjectReferenceParameter' of type 'AnimationEvent': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ObjectReferenceParameter_3Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationEvent
extern "C" void AnimationEvent_t3669457594_marshal_com_cleanup(AnimationEvent_t3669457594_marshaled_com& marshaled)
{
}
// System.Boolean UnityEngine.AnimationState::get_enabled()
extern "C"  bool AnimationState_get_enabled_m1514601936 (AnimationState_t3682323633 * __this, const MethodInfo* method)
{
	typedef bool (*AnimationState_get_enabled_m1514601936_ftn) (AnimationState_t3682323633 *);
	static AnimationState_get_enabled_m1514601936_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_get_enabled_m1514601936_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::get_enabled()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationState::set_enabled(System.Boolean)
extern "C"  void AnimationState_set_enabled_m1879429625 (AnimationState_t3682323633 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*AnimationState_set_enabled_m1879429625_ftn) (AnimationState_t3682323633 *, bool);
	static AnimationState_set_enabled_m1879429625_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_set_enabled_m1879429625_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::set_enabled(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.AnimationState::set_weight(System.Single)
extern "C"  void AnimationState_set_weight_m1582950610 (AnimationState_t3682323633 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*AnimationState_set_weight_m1582950610_ftn) (AnimationState_t3682323633 *, float);
	static AnimationState_set_weight_m1582950610_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_set_weight_m1582950610_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::set_weight(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.WrapMode UnityEngine.AnimationState::get_wrapMode()
extern "C"  int32_t AnimationState_get_wrapMode_m2705936773 (AnimationState_t3682323633 * __this, const MethodInfo* method)
{
	typedef int32_t (*AnimationState_get_wrapMode_m2705936773_ftn) (AnimationState_t3682323633 *);
	static AnimationState_get_wrapMode_m2705936773_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_get_wrapMode_m2705936773_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::get_wrapMode()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationState::set_wrapMode(UnityEngine.WrapMode)
extern "C"  void AnimationState_set_wrapMode_m3837047690 (AnimationState_t3682323633 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*AnimationState_set_wrapMode_m3837047690_ftn) (AnimationState_t3682323633 *, int32_t);
	static AnimationState_set_wrapMode_m3837047690_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_set_wrapMode_m3837047690_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::set_wrapMode(UnityEngine.WrapMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.AnimationState::get_time()
extern "C"  float AnimationState_get_time_m2290731302 (AnimationState_t3682323633 * __this, const MethodInfo* method)
{
	typedef float (*AnimationState_get_time_m2290731302_ftn) (AnimationState_t3682323633 *);
	static AnimationState_get_time_m2290731302_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_get_time_m2290731302_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::get_time()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationState::set_time(System.Single)
extern "C"  void AnimationState_set_time_m3547497437 (AnimationState_t3682323633 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*AnimationState_set_time_m3547497437_ftn) (AnimationState_t3682323633 *, float);
	static AnimationState_set_time_m3547497437_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_set_time_m3547497437_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::set_time(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.AnimationState::set_normalizedTime(System.Single)
extern "C"  void AnimationState_set_normalizedTime_m1757568870 (AnimationState_t3682323633 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*AnimationState_set_normalizedTime_m1757568870_ftn) (AnimationState_t3682323633 *, float);
	static AnimationState_set_normalizedTime_m1757568870_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_set_normalizedTime_m1757568870_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::set_normalizedTime(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.AnimationState::set_speed(System.Single)
extern "C"  void AnimationState_set_speed_m4187319075 (AnimationState_t3682323633 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*AnimationState_set_speed_m4187319075_ftn) (AnimationState_t3682323633 *, float);
	static AnimationState_set_speed_m4187319075_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_set_speed_m4187319075_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::set_speed(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.AnimationState::get_length()
extern "C"  float AnimationState_get_length_m2089699583 (AnimationState_t3682323633 * __this, const MethodInfo* method)
{
	typedef float (*AnimationState_get_length_m2089699583_ftn) (AnimationState_t3682323633 *);
	static AnimationState_get_length_m2089699583_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_get_length_m2089699583_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::get_length()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationState::set_layer(System.Int32)
extern "C"  void AnimationState_set_layer_m2805758051 (AnimationState_t3682323633 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*AnimationState_set_layer_m2805758051_ftn) (AnimationState_t3682323633 *, int32_t);
	static AnimationState_set_layer_m2805758051_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_set_layer_m2805758051_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::set_layer(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.AnimationState::AddMixingTransform(UnityEngine.Transform,System.Boolean)
extern "C"  void AnimationState_AddMixingTransform_m4158051049 (AnimationState_t3682323633 * __this, Transform_t1659122786 * ___mix0, bool ___recursive1, const MethodInfo* method)
{
	typedef void (*AnimationState_AddMixingTransform_m4158051049_ftn) (AnimationState_t3682323633 *, Transform_t1659122786 *, bool);
	static AnimationState_AddMixingTransform_m4158051049_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_AddMixingTransform_m4158051049_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::AddMixingTransform(UnityEngine.Transform,System.Boolean)");
	_il2cpp_icall_func(__this, ___mix0, ___recursive1);
}
// System.Void UnityEngine.AnimationState::AddMixingTransform(UnityEngine.Transform)
extern "C"  void AnimationState_AddMixingTransform_m2145392148 (AnimationState_t3682323633 * __this, Transform_t1659122786 * ___mix0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)1;
		Transform_t1659122786 * L_0 = ___mix0;
		bool L_1 = V_0;
		AnimationState_AddMixingTransform_m4158051049(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.AnimationState::get_name()
extern "C"  String_t* AnimationState_get_name_m1230036347 (AnimationState_t3682323633 * __this, const MethodInfo* method)
{
	typedef String_t* (*AnimationState_get_name_m1230036347_ftn) (AnimationState_t3682323633 *);
	static AnimationState_get_name_m1230036347_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_get_name_m1230036347_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::get_name()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationState::set_blendMode(UnityEngine.AnimationBlendMode)
extern "C"  void AnimationState_set_blendMode_m2249435534 (AnimationState_t3682323633 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*AnimationState_set_blendMode_m2249435534_ftn) (AnimationState_t3682323633 *, int32_t);
	static AnimationState_set_blendMode_m2249435534_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_set_blendMode_m2249435534_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::set_blendMode(UnityEngine.AnimationBlendMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Animator::get_isHuman()
extern "C"  bool Animator_get_isHuman_m4030001272 (Animator_t2776330603 * __this, const MethodInfo* method)
{
	typedef bool (*Animator_get_isHuman_m4030001272_ftn) (Animator_t2776330603 *);
	static Animator_get_isHuman_m4030001272_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_isHuman_m4030001272_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_isHuman()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Animator::get_humanScale()
extern "C"  float Animator_get_humanScale_m13697776 (Animator_t2776330603 * __this, const MethodInfo* method)
{
	typedef float (*Animator_get_humanScale_m13697776_ftn) (Animator_t2776330603 *);
	static Animator_get_humanScale_m13697776_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_humanScale_m13697776_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_humanScale()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Animator::GetFloat(System.Int32)
extern "C"  float Animator_GetFloat_m2965884705 (Animator_t2776330603 * __this, int32_t ___id0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___id0;
		float L_1 = Animator_GetFloatID_m373159228(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Animator::SetFloat(System.String,System.Single,System.Single,System.Single)
extern "C"  void Animator_SetFloat_m359795641 (Animator_t2776330603 * __this, String_t* ___name0, float ___value1, float ___dampTime2, float ___deltaTime3, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		float L_1 = ___value1;
		float L_2 = ___dampTime2;
		float L_3 = ___deltaTime3;
		Animator_SetFloatStringDamp_m2274151144(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetFloat(System.Int32,System.Single)
extern "C"  void Animator_SetFloat_m57191598 (Animator_t2776330603 * __this, int32_t ___id0, float ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___id0;
		float L_1 = ___value1;
		Animator_SetFloatID_m819072393(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetFloat(System.Int32,System.Single,System.Single,System.Single)
extern "C"  void Animator_SetFloat_m2671615160 (Animator_t2776330603 * __this, int32_t ___id0, float ___value1, float ___dampTime2, float ___deltaTime3, const MethodInfo* method)
{
	{
		int32_t L_0 = ___id0;
		float L_1 = ___value1;
		float L_2 = ___dampTime2;
		float L_3 = ___deltaTime3;
		Animator_SetFloatIDDamp_m4107886867(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Animator::GetBool(System.Int32)
extern "C"  bool Animator_GetBool_m1246282447 (Animator_t2776330603 * __this, int32_t ___id0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___id0;
		bool L_1 = Animator_GetBoolID_m1397798250(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Animator::SetBool(System.String,System.Boolean)
extern "C"  void Animator_SetBool_m2336836203 (Animator_t2776330603 * __this, String_t* ___name0, bool ___value1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		bool L_1 = ___value1;
		Animator_SetBoolString_m275475356(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetBool(System.Int32,System.Boolean)
extern "C"  void Animator_SetBool_m1802007004 (Animator_t2776330603 * __this, int32_t ___id0, bool ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___id0;
		bool L_1 = ___value1;
		Animator_SetBoolID_m516262497(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Animator::GetInteger(System.Int32)
extern "C"  int32_t Animator_GetInteger_m3944178743 (Animator_t2776330603 * __this, int32_t ___id0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___id0;
		int32_t L_1 = Animator_GetIntegerID_m4210859218(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Animator::SetInteger(System.Int32,System.Int32)
extern "C"  void Animator_SetInteger_m812217484 (Animator_t2776330603 * __this, int32_t ___id0, int32_t ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___id0;
		int32_t L_1 = ___value1;
		Animator_SetIntegerID_m276290769(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetTrigger(System.String)
extern "C"  void Animator_SetTrigger_m514363822 (Animator_t2776330603 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		Animator_SetTriggerString_m1378271133(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::ResetTrigger(System.String)
extern "C"  void Animator_ResetTrigger_m4152421915 (Animator_t2776330603 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		Animator_ResetTriggerString_m1817269834(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Animator::IsParameterControlledByCurve(System.String)
extern "C"  bool Animator_IsParameterControlledByCurve_m3328157587 (Animator_t2776330603 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		bool L_1 = Animator_IsParameterControlledByCurveString_m1231885762(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.Animator::get_deltaPosition()
extern "C"  Vector3_t4282066566  Animator_get_deltaPosition_m1658225602 (Animator_t2776330603 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_INTERNAL_get_deltaPosition_m3729275047(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_get_deltaPosition(UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_get_deltaPosition_m3729275047 (Animator_t2776330603 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_get_deltaPosition_m3729275047_ftn) (Animator_t2776330603 *, Vector3_t4282066566 *);
	static Animator_INTERNAL_get_deltaPosition_m3729275047_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_get_deltaPosition_m3729275047_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_get_deltaPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Quaternion UnityEngine.Animator::get_deltaRotation()
extern "C"  Quaternion_t1553702882  Animator_get_deltaRotation_m1583110423 (Animator_t2776330603 * __this, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_INTERNAL_get_deltaRotation_m3667880600(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_get_deltaRotation(UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_get_deltaRotation_m3667880600 (Animator_t2776330603 * __this, Quaternion_t1553702882 * ___value0, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_get_deltaRotation_m3667880600_ftn) (Animator_t2776330603 *, Quaternion_t1553702882 *);
	static Animator_INTERNAL_get_deltaRotation_m3667880600_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_get_deltaRotation_m3667880600_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_get_deltaRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Animator::get_rootPosition()
extern "C"  Vector3_t4282066566  Animator_get_rootPosition_m425633900 (Animator_t2776330603 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_INTERNAL_get_rootPosition_m623412333(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_get_rootPosition(UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_get_rootPosition_m623412333 (Animator_t2776330603 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_get_rootPosition_m623412333_ftn) (Animator_t2776330603 *, Vector3_t4282066566 *);
	static Animator_INTERNAL_get_rootPosition_m623412333_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_get_rootPosition_m623412333_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_get_rootPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Quaternion UnityEngine.Animator::get_rootRotation()
extern "C"  Quaternion_t1553702882  Animator_get_rootRotation_m3309843777 (Animator_t2776330603 * __this, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_INTERNAL_get_rootRotation_m3392225554(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_get_rootRotation(UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_get_rootRotation_m3392225554 (Animator_t2776330603 * __this, Quaternion_t1553702882 * ___value0, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_get_rootRotation_m3392225554_ftn) (Animator_t2776330603 *, Quaternion_t1553702882 *);
	static Animator_INTERNAL_get_rootRotation_m3392225554_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_get_rootRotation_m3392225554_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_get_rootRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Animator::get_applyRootMotion()
extern "C"  bool Animator_get_applyRootMotion_m2388146907 (Animator_t2776330603 * __this, const MethodInfo* method)
{
	typedef bool (*Animator_get_applyRootMotion_m2388146907_ftn) (Animator_t2776330603 *);
	static Animator_get_applyRootMotion_m2388146907_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_applyRootMotion_m2388146907_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_applyRootMotion()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Animator::set_applyRootMotion(System.Boolean)
extern "C"  void Animator_set_applyRootMotion_m394805828 (Animator_t2776330603 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Animator_set_applyRootMotion_m394805828_ftn) (Animator_t2776330603 *, bool);
	static Animator_set_applyRootMotion_m394805828_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_set_applyRootMotion_m394805828_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::set_applyRootMotion(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Animator::get_gravityWeight()
extern "C"  float Animator_get_gravityWeight_m1393695637 (Animator_t2776330603 * __this, const MethodInfo* method)
{
	typedef float (*Animator_get_gravityWeight_m1393695637_ftn) (Animator_t2776330603 *);
	static Animator_get_gravityWeight_m1393695637_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_gravityWeight_m1393695637_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_gravityWeight()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Vector3 UnityEngine.Animator::get_bodyPosition()
extern "C"  Vector3_t4282066566  Animator_get_bodyPosition_m2404872492 (Animator_t2776330603 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_INTERNAL_get_bodyPosition_m3785714989(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Animator::set_bodyPosition(UnityEngine.Vector3)
extern "C"  void Animator_set_bodyPosition_m2325119987 (Animator_t2776330603 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		Animator_INTERNAL_set_bodyPosition_m1904526497(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_get_bodyPosition(UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_get_bodyPosition_m3785714989 (Animator_t2776330603 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_get_bodyPosition_m3785714989_ftn) (Animator_t2776330603 *, Vector3_t4282066566 *);
	static Animator_INTERNAL_get_bodyPosition_m3785714989_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_get_bodyPosition_m3785714989_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_get_bodyPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Animator::INTERNAL_set_bodyPosition(UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_set_bodyPosition_m1904526497 (Animator_t2776330603 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_set_bodyPosition_m1904526497_ftn) (Animator_t2776330603 *, Vector3_t4282066566 *);
	static Animator_INTERNAL_set_bodyPosition_m1904526497_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_set_bodyPosition_m1904526497_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_set_bodyPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Quaternion UnityEngine.Animator::get_bodyRotation()
extern "C"  Quaternion_t1553702882  Animator_get_bodyRotation_m994115073 (Animator_t2776330603 * __this, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_INTERNAL_get_bodyRotation_m1443012690(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Animator::set_bodyRotation(UnityEngine.Quaternion)
extern "C"  void Animator_set_bodyRotation_m2385096902 (Animator_t2776330603 * __this, Quaternion_t1553702882  ___value0, const MethodInfo* method)
{
	{
		Animator_INTERNAL_set_bodyRotation_m3984893022(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_get_bodyRotation(UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_get_bodyRotation_m1443012690 (Animator_t2776330603 * __this, Quaternion_t1553702882 * ___value0, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_get_bodyRotation_m1443012690_ftn) (Animator_t2776330603 *, Quaternion_t1553702882 *);
	static Animator_INTERNAL_get_bodyRotation_m1443012690_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_get_bodyRotation_m1443012690_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_get_bodyRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Animator::INTERNAL_set_bodyRotation(UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_set_bodyRotation_m3984893022 (Animator_t2776330603 * __this, Quaternion_t1553702882 * ___value0, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_set_bodyRotation_m3984893022_ftn) (Animator_t2776330603 *, Quaternion_t1553702882 *);
	static Animator_INTERNAL_set_bodyRotation_m3984893022_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_set_bodyRotation_m3984893022_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_set_bodyRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Animator::GetIKPosition(UnityEngine.AvatarIKGoal)
extern "C"  Vector3_t4282066566  Animator_GetIKPosition_m385128518 (Animator_t2776330603 * __this, int32_t ___goal0, const MethodInfo* method)
{
	{
		Animator_CheckIfInIKPass_m2875692449(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___goal0;
		Vector3_t4282066566  L_1 = Animator_GetIKPositionInternal_m142003939(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.Animator::GetIKPositionInternal(UnityEngine.AvatarIKGoal)
extern "C"  Vector3_t4282066566  Animator_GetIKPositionInternal_m142003939 (Animator_t2776330603 * __this, int32_t ___goal0, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___goal0;
		Animator_INTERNAL_CALL_GetIKPositionInternal_m976909533(NULL /*static, unused*/, __this, L_0, (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_CALL_GetIKPositionInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_CALL_GetIKPositionInternal_m976909533 (Il2CppObject * __this /* static, unused */, Animator_t2776330603 * ___self0, int32_t ___goal1, Vector3_t4282066566 * ___value2, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_CALL_GetIKPositionInternal_m976909533_ftn) (Animator_t2776330603 *, int32_t, Vector3_t4282066566 *);
	static Animator_INTERNAL_CALL_GetIKPositionInternal_m976909533_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_CALL_GetIKPositionInternal_m976909533_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_CALL_GetIKPositionInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___goal1, ___value2);
}
// System.Void UnityEngine.Animator::SetIKPosition(UnityEngine.AvatarIKGoal,UnityEngine.Vector3)
extern "C"  void Animator_SetIKPosition_m2542416671 (Animator_t2776330603 * __this, int32_t ___goal0, Vector3_t4282066566  ___goalPosition1, const MethodInfo* method)
{
	{
		Animator_CheckIfInIKPass_m2875692449(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___goal0;
		Vector3_t4282066566  L_1 = ___goalPosition1;
		Animator_SetIKPositionInternal_m335958844(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetIKPositionInternal(UnityEngine.AvatarIKGoal,UnityEngine.Vector3)
extern "C"  void Animator_SetIKPositionInternal_m335958844 (Animator_t2776330603 * __this, int32_t ___goal0, Vector3_t4282066566  ___goalPosition1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___goal0;
		Animator_INTERNAL_CALL_SetIKPositionInternal_m647475433(NULL /*static, unused*/, __this, L_0, (&___goalPosition1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_CALL_SetIKPositionInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_CALL_SetIKPositionInternal_m647475433 (Il2CppObject * __this /* static, unused */, Animator_t2776330603 * ___self0, int32_t ___goal1, Vector3_t4282066566 * ___goalPosition2, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_CALL_SetIKPositionInternal_m647475433_ftn) (Animator_t2776330603 *, int32_t, Vector3_t4282066566 *);
	static Animator_INTERNAL_CALL_SetIKPositionInternal_m647475433_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_CALL_SetIKPositionInternal_m647475433_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_CALL_SetIKPositionInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___goal1, ___goalPosition2);
}
// UnityEngine.Quaternion UnityEngine.Animator::GetIKRotation(UnityEngine.AvatarIKGoal)
extern "C"  Quaternion_t1553702882  Animator_GetIKRotation_m3297713819 (Animator_t2776330603 * __this, int32_t ___goal0, const MethodInfo* method)
{
	{
		Animator_CheckIfInIKPass_m2875692449(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___goal0;
		Quaternion_t1553702882  L_1 = Animator_GetIKRotationInternal_m2296655928(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Quaternion UnityEngine.Animator::GetIKRotationInternal(UnityEngine.AvatarIKGoal)
extern "C"  Quaternion_t1553702882  Animator_GetIKRotationInternal_m2296655928 (Animator_t2776330603 * __this, int32_t ___goal0, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___goal0;
		Animator_INTERNAL_CALL_GetIKRotationInternal_m110687330(NULL /*static, unused*/, __this, L_0, (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_CALL_GetIKRotationInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_CALL_GetIKRotationInternal_m110687330 (Il2CppObject * __this /* static, unused */, Animator_t2776330603 * ___self0, int32_t ___goal1, Quaternion_t1553702882 * ___value2, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_CALL_GetIKRotationInternal_m110687330_ftn) (Animator_t2776330603 *, int32_t, Quaternion_t1553702882 *);
	static Animator_INTERNAL_CALL_GetIKRotationInternal_m110687330_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_CALL_GetIKRotationInternal_m110687330_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_CALL_GetIKRotationInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___self0, ___goal1, ___value2);
}
// System.Void UnityEngine.Animator::SetIKRotation(UnityEngine.AvatarIKGoal,UnityEngine.Quaternion)
extern "C"  void Animator_SetIKRotation_m2768899344 (Animator_t2776330603 * __this, int32_t ___goal0, Quaternion_t1553702882  ___goalRotation1, const MethodInfo* method)
{
	{
		Animator_CheckIfInIKPass_m2875692449(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___goal0;
		Quaternion_t1553702882  L_1 = ___goalRotation1;
		Animator_SetIKRotationInternal_m1596039379(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetIKRotationInternal(UnityEngine.AvatarIKGoal,UnityEngine.Quaternion)
extern "C"  void Animator_SetIKRotationInternal_m1596039379 (Animator_t2776330603 * __this, int32_t ___goal0, Quaternion_t1553702882  ___goalRotation1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___goal0;
		Animator_INTERNAL_CALL_SetIKRotationInternal_m4234652886(NULL /*static, unused*/, __this, L_0, (&___goalRotation1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_CALL_SetIKRotationInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_CALL_SetIKRotationInternal_m4234652886 (Il2CppObject * __this /* static, unused */, Animator_t2776330603 * ___self0, int32_t ___goal1, Quaternion_t1553702882 * ___goalRotation2, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_CALL_SetIKRotationInternal_m4234652886_ftn) (Animator_t2776330603 *, int32_t, Quaternion_t1553702882 *);
	static Animator_INTERNAL_CALL_SetIKRotationInternal_m4234652886_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_CALL_SetIKRotationInternal_m4234652886_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_CALL_SetIKRotationInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___self0, ___goal1, ___goalRotation2);
}
// System.Single UnityEngine.Animator::GetIKPositionWeight(UnityEngine.AvatarIKGoal)
extern "C"  float Animator_GetIKPositionWeight_m2079943564 (Animator_t2776330603 * __this, int32_t ___goal0, const MethodInfo* method)
{
	{
		Animator_CheckIfInIKPass_m2875692449(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___goal0;
		float L_1 = Animator_GetIKPositionWeightInternal_m464452649(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single UnityEngine.Animator::GetIKPositionWeightInternal(UnityEngine.AvatarIKGoal)
extern "C"  float Animator_GetIKPositionWeightInternal_m464452649 (Animator_t2776330603 * __this, int32_t ___goal0, const MethodInfo* method)
{
	typedef float (*Animator_GetIKPositionWeightInternal_m464452649_ftn) (Animator_t2776330603 *, int32_t);
	static Animator_GetIKPositionWeightInternal_m464452649_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetIKPositionWeightInternal_m464452649_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetIKPositionWeightInternal(UnityEngine.AvatarIKGoal)");
	return _il2cpp_icall_func(__this, ___goal0);
}
// System.Void UnityEngine.Animator::SetIKPositionWeight(UnityEngine.AvatarIKGoal,System.Single)
extern "C"  void Animator_SetIKPositionWeight_m1110244361 (Animator_t2776330603 * __this, int32_t ___goal0, float ___value1, const MethodInfo* method)
{
	{
		Animator_CheckIfInIKPass_m2875692449(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___goal0;
		float L_1 = ___value1;
		Animator_SetIKPositionWeightInternal_m1498324454(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetIKPositionWeightInternal(UnityEngine.AvatarIKGoal,System.Single)
extern "C"  void Animator_SetIKPositionWeightInternal_m1498324454 (Animator_t2776330603 * __this, int32_t ___goal0, float ___value1, const MethodInfo* method)
{
	typedef void (*Animator_SetIKPositionWeightInternal_m1498324454_ftn) (Animator_t2776330603 *, int32_t, float);
	static Animator_SetIKPositionWeightInternal_m1498324454_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetIKPositionWeightInternal_m1498324454_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetIKPositionWeightInternal(UnityEngine.AvatarIKGoal,System.Single)");
	_il2cpp_icall_func(__this, ___goal0, ___value1);
}
// System.Single UnityEngine.Animator::GetIKRotationWeight(UnityEngine.AvatarIKGoal)
extern "C"  float Animator_GetIKRotationWeight_m3997781473 (Animator_t2776330603 * __this, int32_t ___goal0, const MethodInfo* method)
{
	{
		Animator_CheckIfInIKPass_m2875692449(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___goal0;
		float L_1 = Animator_GetIKRotationWeightInternal_m1941289342(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single UnityEngine.Animator::GetIKRotationWeightInternal(UnityEngine.AvatarIKGoal)
extern "C"  float Animator_GetIKRotationWeightInternal_m1941289342 (Animator_t2776330603 * __this, int32_t ___goal0, const MethodInfo* method)
{
	typedef float (*Animator_GetIKRotationWeightInternal_m1941289342_ftn) (Animator_t2776330603 *, int32_t);
	static Animator_GetIKRotationWeightInternal_m1941289342_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetIKRotationWeightInternal_m1941289342_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetIKRotationWeightInternal(UnityEngine.AvatarIKGoal)");
	return _il2cpp_icall_func(__this, ___goal0);
}
// System.Void UnityEngine.Animator::SetIKRotationWeight(UnityEngine.AvatarIKGoal,System.Single)
extern "C"  void Animator_SetIKRotationWeight_m2279843230 (Animator_t2776330603 * __this, int32_t ___goal0, float ___value1, const MethodInfo* method)
{
	{
		Animator_CheckIfInIKPass_m2875692449(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___goal0;
		float L_1 = ___value1;
		Animator_SetIKRotationWeightInternal_m35709563(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetIKRotationWeightInternal(UnityEngine.AvatarIKGoal,System.Single)
extern "C"  void Animator_SetIKRotationWeightInternal_m35709563 (Animator_t2776330603 * __this, int32_t ___goal0, float ___value1, const MethodInfo* method)
{
	typedef void (*Animator_SetIKRotationWeightInternal_m35709563_ftn) (Animator_t2776330603 *, int32_t, float);
	static Animator_SetIKRotationWeightInternal_m35709563_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetIKRotationWeightInternal_m35709563_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetIKRotationWeightInternal(UnityEngine.AvatarIKGoal,System.Single)");
	_il2cpp_icall_func(__this, ___goal0, ___value1);
}
// System.Void UnityEngine.Animator::SetLookAtPosition(UnityEngine.Vector3)
extern "C"  void Animator_SetLookAtPosition_m384162552 (Animator_t2776330603 * __this, Vector3_t4282066566  ___lookAtPosition0, const MethodInfo* method)
{
	{
		Animator_CheckIfInIKPass_m2875692449(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = ___lookAtPosition0;
		Animator_SetLookAtPositionInternal_m1453739067(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetLookAtPositionInternal(UnityEngine.Vector3)
extern "C"  void Animator_SetLookAtPositionInternal_m1453739067 (Animator_t2776330603 * __this, Vector3_t4282066566  ___lookAtPosition0, const MethodInfo* method)
{
	{
		Animator_INTERNAL_CALL_SetLookAtPositionInternal_m816040728(NULL /*static, unused*/, __this, (&___lookAtPosition0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_CALL_SetLookAtPositionInternal(UnityEngine.Animator,UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_CALL_SetLookAtPositionInternal_m816040728 (Il2CppObject * __this /* static, unused */, Animator_t2776330603 * ___self0, Vector3_t4282066566 * ___lookAtPosition1, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_CALL_SetLookAtPositionInternal_m816040728_ftn) (Animator_t2776330603 *, Vector3_t4282066566 *);
	static Animator_INTERNAL_CALL_SetLookAtPositionInternal_m816040728_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_CALL_SetLookAtPositionInternal_m816040728_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_CALL_SetLookAtPositionInternal(UnityEngine.Animator,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___lookAtPosition1);
}
// System.Void UnityEngine.Animator::SetLookAtWeight(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Animator_SetLookAtWeight_m881446346 (Animator_t2776330603 * __this, float ___weight0, float ___bodyWeight1, float ___headWeight2, float ___eyesWeight3, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (0.5f);
		float L_0 = ___weight0;
		float L_1 = ___bodyWeight1;
		float L_2 = ___headWeight2;
		float L_3 = ___eyesWeight3;
		float L_4 = V_0;
		Animator_SetLookAtWeight_m2843438895(__this, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetLookAtWeight(System.Single,System.Single,System.Single)
extern "C"  void Animator_SetLookAtWeight_m96208805 (Animator_t2776330603 * __this, float ___weight0, float ___bodyWeight1, float ___headWeight2, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (0.5f);
		V_1 = (0.0f);
		float L_0 = ___weight0;
		float L_1 = ___bodyWeight1;
		float L_2 = ___headWeight2;
		float L_3 = V_1;
		float L_4 = V_0;
		Animator_SetLookAtWeight_m2843438895(__this, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetLookAtWeight(System.Single,System.Single)
extern "C"  void Animator_SetLookAtWeight_m3985034432 (Animator_t2776330603 * __this, float ___weight0, float ___bodyWeight1, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		V_0 = (0.5f);
		V_1 = (0.0f);
		V_2 = (1.0f);
		float L_0 = ___weight0;
		float L_1 = ___bodyWeight1;
		float L_2 = V_2;
		float L_3 = V_1;
		float L_4 = V_0;
		Animator_SetLookAtWeight_m2843438895(__this, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetLookAtWeight(System.Single)
extern "C"  void Animator_SetLookAtWeight_m2035469595 (Animator_t2776330603 * __this, float ___weight0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		V_0 = (0.5f);
		V_1 = (0.0f);
		V_2 = (1.0f);
		V_3 = (0.0f);
		float L_0 = ___weight0;
		float L_1 = V_3;
		float L_2 = V_2;
		float L_3 = V_1;
		float L_4 = V_0;
		Animator_SetLookAtWeight_m2843438895(__this, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetLookAtWeight(System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void Animator_SetLookAtWeight_m2843438895 (Animator_t2776330603 * __this, float ___weight0, float ___bodyWeight1, float ___headWeight2, float ___eyesWeight3, float ___clampWeight4, const MethodInfo* method)
{
	{
		Animator_CheckIfInIKPass_m2875692449(__this, /*hidden argument*/NULL);
		float L_0 = ___weight0;
		float L_1 = ___bodyWeight1;
		float L_2 = ___headWeight2;
		float L_3 = ___eyesWeight3;
		float L_4 = ___clampWeight4;
		Animator_SetLookAtWeightInternal_m3334195378(__this, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetLookAtWeightInternal(System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void Animator_SetLookAtWeightInternal_m3334195378 (Animator_t2776330603 * __this, float ___weight0, float ___bodyWeight1, float ___headWeight2, float ___eyesWeight3, float ___clampWeight4, const MethodInfo* method)
{
	typedef void (*Animator_SetLookAtWeightInternal_m3334195378_ftn) (Animator_t2776330603 *, float, float, float, float, float);
	static Animator_SetLookAtWeightInternal_m3334195378_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetLookAtWeightInternal_m3334195378_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetLookAtWeightInternal(System.Single,System.Single,System.Single,System.Single,System.Single)");
	_il2cpp_icall_func(__this, ___weight0, ___bodyWeight1, ___headWeight2, ___eyesWeight3, ___clampWeight4);
}
// System.Void UnityEngine.Animator::set_stabilizeFeet(System.Boolean)
extern "C"  void Animator_set_stabilizeFeet_m2533795611 (Animator_t2776330603 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Animator_set_stabilizeFeet_m2533795611_ftn) (Animator_t2776330603 *, bool);
	static Animator_set_stabilizeFeet_m2533795611_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_set_stabilizeFeet_m2533795611_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::set_stabilizeFeet(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.Animator::get_layerCount()
extern "C"  int32_t Animator_get_layerCount_m3326924613 (Animator_t2776330603 * __this, const MethodInfo* method)
{
	typedef int32_t (*Animator_get_layerCount_m3326924613_ftn) (Animator_t2776330603 *);
	static Animator_get_layerCount_m3326924613_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_layerCount_m3326924613_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_layerCount()");
	return _il2cpp_icall_func(__this);
}
// System.String UnityEngine.Animator::GetLayerName(System.Int32)
extern "C"  String_t* Animator_GetLayerName_m3480300056 (Animator_t2776330603 * __this, int32_t ___layerIndex0, const MethodInfo* method)
{
	typedef String_t* (*Animator_GetLayerName_m3480300056_ftn) (Animator_t2776330603 *, int32_t);
	static Animator_GetLayerName_m3480300056_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetLayerName_m3480300056_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetLayerName(System.Int32)");
	return _il2cpp_icall_func(__this, ___layerIndex0);
}
// System.Single UnityEngine.Animator::GetLayerWeight(System.Int32)
extern "C"  float Animator_GetLayerWeight_m3878421230 (Animator_t2776330603 * __this, int32_t ___layerIndex0, const MethodInfo* method)
{
	typedef float (*Animator_GetLayerWeight_m3878421230_ftn) (Animator_t2776330603 *, int32_t);
	static Animator_GetLayerWeight_m3878421230_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetLayerWeight_m3878421230_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetLayerWeight(System.Int32)");
	return _il2cpp_icall_func(__this, ___layerIndex0);
}
// System.Void UnityEngine.Animator::SetLayerWeight(System.Int32,System.Single)
extern "C"  void Animator_SetLayerWeight_m3838560187 (Animator_t2776330603 * __this, int32_t ___layerIndex0, float ___weight1, const MethodInfo* method)
{
	typedef void (*Animator_SetLayerWeight_m3838560187_ftn) (Animator_t2776330603 *, int32_t, float);
	static Animator_SetLayerWeight_m3838560187_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetLayerWeight_m3838560187_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetLayerWeight(System.Int32,System.Single)");
	_il2cpp_icall_func(__this, ___layerIndex0, ___weight1);
}
// UnityEngine.AnimatorStateInfo UnityEngine.Animator::GetCurrentAnimatorStateInfo(System.Int32)
extern "C"  AnimatorStateInfo_t323110318  Animator_GetCurrentAnimatorStateInfo_m3061859448 (Animator_t2776330603 * __this, int32_t ___layerIndex0, const MethodInfo* method)
{
	typedef AnimatorStateInfo_t323110318  (*Animator_GetCurrentAnimatorStateInfo_m3061859448_ftn) (Animator_t2776330603 *, int32_t);
	static Animator_GetCurrentAnimatorStateInfo_m3061859448_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetCurrentAnimatorStateInfo_m3061859448_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetCurrentAnimatorStateInfo(System.Int32)");
	return _il2cpp_icall_func(__this, ___layerIndex0);
}
// UnityEngine.AnimatorStateInfo UnityEngine.Animator::GetNextAnimatorStateInfo(System.Int32)
extern "C"  AnimatorStateInfo_t323110318  Animator_GetNextAnimatorStateInfo_m791156688 (Animator_t2776330603 * __this, int32_t ___layerIndex0, const MethodInfo* method)
{
	typedef AnimatorStateInfo_t323110318  (*Animator_GetNextAnimatorStateInfo_m791156688_ftn) (Animator_t2776330603 *, int32_t);
	static Animator_GetNextAnimatorStateInfo_m791156688_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetNextAnimatorStateInfo_m791156688_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetNextAnimatorStateInfo(System.Int32)");
	return _il2cpp_icall_func(__this, ___layerIndex0);
}
// UnityEngine.AnimatorTransitionInfo UnityEngine.Animator::GetAnimatorTransitionInfo(System.Int32)
extern "C"  AnimatorTransitionInfo_t2817229998  Animator_GetAnimatorTransitionInfo_m3858104711 (Animator_t2776330603 * __this, int32_t ___layerIndex0, const MethodInfo* method)
{
	typedef AnimatorTransitionInfo_t2817229998  (*Animator_GetAnimatorTransitionInfo_m3858104711_ftn) (Animator_t2776330603 *, int32_t);
	static Animator_GetAnimatorTransitionInfo_m3858104711_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetAnimatorTransitionInfo_m3858104711_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetAnimatorTransitionInfo(System.Int32)");
	return _il2cpp_icall_func(__this, ___layerIndex0);
}
// System.Boolean UnityEngine.Animator::IsInTransition(System.Int32)
extern "C"  bool Animator_IsInTransition_m2609196857 (Animator_t2776330603 * __this, int32_t ___layerIndex0, const MethodInfo* method)
{
	typedef bool (*Animator_IsInTransition_m2609196857_ftn) (Animator_t2776330603 *, int32_t);
	static Animator_IsInTransition_m2609196857_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_IsInTransition_m2609196857_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::IsInTransition(System.Int32)");
	return _il2cpp_icall_func(__this, ___layerIndex0);
}
// System.Single UnityEngine.Animator::get_feetPivotActive()
extern "C"  float Animator_get_feetPivotActive_m4087239689 (Animator_t2776330603 * __this, const MethodInfo* method)
{
	typedef float (*Animator_get_feetPivotActive_m4087239689_ftn) (Animator_t2776330603 *);
	static Animator_get_feetPivotActive_m4087239689_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_feetPivotActive_m4087239689_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_feetPivotActive()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Animator::set_feetPivotActive(System.Single)
extern "C"  void Animator_set_feetPivotActive_m293215914 (Animator_t2776330603 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Animator_set_feetPivotActive_m293215914_ftn) (Animator_t2776330603 *, float);
	static Animator_set_feetPivotActive_m293215914_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_set_feetPivotActive_m293215914_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::set_feetPivotActive(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Animator::get_pivotWeight()
extern "C"  float Animator_get_pivotWeight_m1500566793 (Animator_t2776330603 * __this, const MethodInfo* method)
{
	typedef float (*Animator_get_pivotWeight_m1500566793_ftn) (Animator_t2776330603 *);
	static Animator_get_pivotWeight_m1500566793_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_pivotWeight_m1500566793_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_pivotWeight()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Vector3 UnityEngine.Animator::get_pivotPosition()
extern "C"  Vector3_t4282066566  Animator_get_pivotPosition_m1479929804 (Animator_t2776330603 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_INTERNAL_get_pivotPosition_m1941126065(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_get_pivotPosition(UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_get_pivotPosition_m1941126065 (Animator_t2776330603 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_get_pivotPosition_m1941126065_ftn) (Animator_t2776330603 *, Vector3_t4282066566 *);
	static Animator_INTERNAL_get_pivotPosition_m1941126065_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_get_pivotPosition_m1941126065_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_get_pivotPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Animator::MatchTarget(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.AvatarTarget,UnityEngine.MatchTargetWeightMask,System.Single,System.Single)
extern "C"  void Animator_MatchTarget_m4236652838 (Animator_t2776330603 * __this, Vector3_t4282066566  ___matchPosition0, Quaternion_t1553702882  ___matchRotation1, int32_t ___targetBodyPart2, MatchTargetWeightMask_t258413904  ___weightMask3, float ___startNormalizedTime4, float ___targetNormalizedTime5, const MethodInfo* method)
{
	{
		int32_t L_0 = ___targetBodyPart2;
		float L_1 = ___startNormalizedTime4;
		float L_2 = ___targetNormalizedTime5;
		Animator_INTERNAL_CALL_MatchTarget_m2875283135(NULL /*static, unused*/, __this, (&___matchPosition0), (&___matchRotation1), L_0, (&___weightMask3), L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_CALL_MatchTarget(UnityEngine.Animator,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.AvatarTarget,UnityEngine.MatchTargetWeightMask&,System.Single,System.Single)
extern "C"  void Animator_INTERNAL_CALL_MatchTarget_m2875283135 (Il2CppObject * __this /* static, unused */, Animator_t2776330603 * ___self0, Vector3_t4282066566 * ___matchPosition1, Quaternion_t1553702882 * ___matchRotation2, int32_t ___targetBodyPart3, MatchTargetWeightMask_t258413904 * ___weightMask4, float ___startNormalizedTime5, float ___targetNormalizedTime6, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_CALL_MatchTarget_m2875283135_ftn) (Animator_t2776330603 *, Vector3_t4282066566 *, Quaternion_t1553702882 *, int32_t, MatchTargetWeightMask_t258413904 *, float, float);
	static Animator_INTERNAL_CALL_MatchTarget_m2875283135_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_CALL_MatchTarget_m2875283135_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_CALL_MatchTarget(UnityEngine.Animator,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.AvatarTarget,UnityEngine.MatchTargetWeightMask&,System.Single,System.Single)");
	_il2cpp_icall_func(___self0, ___matchPosition1, ___matchRotation2, ___targetBodyPart3, ___weightMask4, ___startNormalizedTime5, ___targetNormalizedTime6);
}
// System.Void UnityEngine.Animator::InterruptMatchTarget(System.Boolean)
extern "C"  void Animator_InterruptMatchTarget_m3501603944 (Animator_t2776330603 * __this, bool ___completeMatch0, const MethodInfo* method)
{
	typedef void (*Animator_InterruptMatchTarget_m3501603944_ftn) (Animator_t2776330603 *, bool);
	static Animator_InterruptMatchTarget_m3501603944_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_InterruptMatchTarget_m3501603944_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::InterruptMatchTarget(System.Boolean)");
	_il2cpp_icall_func(__this, ___completeMatch0);
}
// System.Boolean UnityEngine.Animator::get_isMatchingTarget()
extern "C"  bool Animator_get_isMatchingTarget_m3696235301 (Animator_t2776330603 * __this, const MethodInfo* method)
{
	typedef bool (*Animator_get_isMatchingTarget_m3696235301_ftn) (Animator_t2776330603 *);
	static Animator_get_isMatchingTarget_m3696235301_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_isMatchingTarget_m3696235301_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_isMatchingTarget()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Animator::get_speed()
extern "C"  float Animator_get_speed_m1893369654 (Animator_t2776330603 * __this, const MethodInfo* method)
{
	typedef float (*Animator_get_speed_m1893369654_ftn) (Animator_t2776330603 *);
	static Animator_get_speed_m1893369654_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_speed_m1893369654_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_speed()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Animator::set_speed(System.Single)
extern "C"  void Animator_set_speed_m2513936029 (Animator_t2776330603 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Animator_set_speed_m2513936029_ftn) (Animator_t2776330603 *, float);
	static Animator_set_speed_m2513936029_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_set_speed_m2513936029_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::set_speed(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Animator::CrossFade(System.String,System.Single,System.Int32,System.Single)
extern "C"  void Animator_CrossFade_m1281797781 (Animator_t2776330603 * __this, String_t* ___stateName0, float ___transitionDuration1, int32_t ___layer2, float ___normalizedTime3, const MethodInfo* method)
{
	{
		String_t* L_0 = ___stateName0;
		int32_t L_1 = Animator_StringToHash_m4020897098(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = ___transitionDuration1;
		int32_t L_3 = ___layer2;
		float L_4 = ___normalizedTime3;
		Animator_CrossFade_m358767974(__this, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::CrossFade(System.Int32,System.Single,System.Int32,System.Single)
extern "C"  void Animator_CrossFade_m358767974 (Animator_t2776330603 * __this, int32_t ___stateNameHash0, float ___transitionDuration1, int32_t ___layer2, float ___normalizedTime3, const MethodInfo* method)
{
	typedef void (*Animator_CrossFade_m358767974_ftn) (Animator_t2776330603 *, int32_t, float, int32_t, float);
	static Animator_CrossFade_m358767974_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_CrossFade_m358767974_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::CrossFade(System.Int32,System.Single,System.Int32,System.Single)");
	_il2cpp_icall_func(__this, ___stateNameHash0, ___transitionDuration1, ___layer2, ___normalizedTime3);
}
// System.Void UnityEngine.Animator::Play(System.String,System.Int32,System.Single)
extern "C"  void Animator_Play_m3631644044 (Animator_t2776330603 * __this, String_t* ___stateName0, int32_t ___layer1, float ___normalizedTime2, const MethodInfo* method)
{
	{
		String_t* L_0 = ___stateName0;
		int32_t L_1 = Animator_StringToHash_m4020897098(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___layer1;
		float L_3 = ___normalizedTime2;
		Animator_Play_m330123001(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::Play(System.Int32,System.Int32,System.Single)
extern "C"  void Animator_Play_m330123001 (Animator_t2776330603 * __this, int32_t ___stateNameHash0, int32_t ___layer1, float ___normalizedTime2, const MethodInfo* method)
{
	typedef void (*Animator_Play_m330123001_ftn) (Animator_t2776330603 *, int32_t, int32_t, float);
	static Animator_Play_m330123001_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_Play_m330123001_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::Play(System.Int32,System.Int32,System.Single)");
	_il2cpp_icall_func(__this, ___stateNameHash0, ___layer1, ___normalizedTime2);
}
// System.Void UnityEngine.Animator::SetTarget(UnityEngine.AvatarTarget,System.Single)
extern "C"  void Animator_SetTarget_m3335381339 (Animator_t2776330603 * __this, int32_t ___targetIndex0, float ___targetNormalizedTime1, const MethodInfo* method)
{
	typedef void (*Animator_SetTarget_m3335381339_ftn) (Animator_t2776330603 *, int32_t, float);
	static Animator_SetTarget_m3335381339_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetTarget_m3335381339_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetTarget(UnityEngine.AvatarTarget,System.Single)");
	_il2cpp_icall_func(__this, ___targetIndex0, ___targetNormalizedTime1);
}
// UnityEngine.Vector3 UnityEngine.Animator::get_targetPosition()
extern "C"  Vector3_t4282066566  Animator_get_targetPosition_m3699919451 (Animator_t2776330603 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_INTERNAL_get_targetPosition_m656463196(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_get_targetPosition(UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_get_targetPosition_m656463196 (Animator_t2776330603 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_get_targetPosition_m656463196_ftn) (Animator_t2776330603 *, Vector3_t4282066566 *);
	static Animator_INTERNAL_get_targetPosition_m656463196_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_get_targetPosition_m656463196_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_get_targetPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Quaternion UnityEngine.Animator::get_targetRotation()
extern "C"  Quaternion_t1553702882  Animator_get_targetRotation_m2080040752 (Animator_t2776330603 * __this, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_INTERNAL_get_targetRotation_m168007107(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_get_targetRotation(UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_get_targetRotation_m168007107 (Animator_t2776330603 * __this, Quaternion_t1553702882 * ___value0, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_get_targetRotation_m168007107_ftn) (Animator_t2776330603 *, Quaternion_t1553702882 *);
	static Animator_INTERNAL_get_targetRotation_m168007107_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_get_targetRotation_m168007107_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_get_targetRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Transform UnityEngine.Animator::GetBoneTransform(UnityEngine.HumanBodyBones)
extern "C"  Transform_t1659122786 * Animator_GetBoneTransform_m3449809847 (Animator_t2776330603 * __this, int32_t ___humanBoneId0, const MethodInfo* method)
{
	typedef Transform_t1659122786 * (*Animator_GetBoneTransform_m3449809847_ftn) (Animator_t2776330603 *, int32_t);
	static Animator_GetBoneTransform_m3449809847_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetBoneTransform_m3449809847_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetBoneTransform(UnityEngine.HumanBodyBones)");
	return _il2cpp_icall_func(__this, ___humanBoneId0);
}
// UnityEngine.AnimatorCullingMode UnityEngine.Animator::get_cullingMode()
extern "C"  int32_t Animator_get_cullingMode_m1008819440 (Animator_t2776330603 * __this, const MethodInfo* method)
{
	typedef int32_t (*Animator_get_cullingMode_m1008819440_ftn) (Animator_t2776330603 *);
	static Animator_get_cullingMode_m1008819440_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_cullingMode_m1008819440_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_cullingMode()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Animator::set_cullingMode(UnityEngine.AnimatorCullingMode)
extern "C"  void Animator_set_cullingMode_m773355747 (Animator_t2776330603 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Animator_set_cullingMode_m773355747_ftn) (Animator_t2776330603 *, int32_t);
	static Animator_set_cullingMode_m773355747_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_set_cullingMode_m773355747_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::set_cullingMode(UnityEngine.AnimatorCullingMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Animator::StartPlayback()
extern "C"  void Animator_StartPlayback_m3009121761 (Animator_t2776330603 * __this, const MethodInfo* method)
{
	typedef void (*Animator_StartPlayback_m3009121761_ftn) (Animator_t2776330603 *);
	static Animator_StartPlayback_m3009121761_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_StartPlayback_m3009121761_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::StartPlayback()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Animator::StopPlayback()
extern "C"  void Animator_StopPlayback_m3612462619 (Animator_t2776330603 * __this, const MethodInfo* method)
{
	typedef void (*Animator_StopPlayback_m3612462619_ftn) (Animator_t2776330603 *);
	static Animator_StopPlayback_m3612462619_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_StopPlayback_m3612462619_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::StopPlayback()");
	_il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Animator::get_playbackTime()
extern "C"  float Animator_get_playbackTime_m3871048475 (Animator_t2776330603 * __this, const MethodInfo* method)
{
	typedef float (*Animator_get_playbackTime_m3871048475_ftn) (Animator_t2776330603 *);
	static Animator_get_playbackTime_m3871048475_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_playbackTime_m3871048475_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_playbackTime()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Animator::set_playbackTime(System.Single)
extern "C"  void Animator_set_playbackTime_m281288968 (Animator_t2776330603 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Animator_set_playbackTime_m281288968_ftn) (Animator_t2776330603 *, float);
	static Animator_set_playbackTime_m281288968_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_set_playbackTime_m281288968_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::set_playbackTime(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Animator::StartRecording(System.Int32)
extern "C"  void Animator_StartRecording_m2916226974 (Animator_t2776330603 * __this, int32_t ___frameCount0, const MethodInfo* method)
{
	typedef void (*Animator_StartRecording_m2916226974_ftn) (Animator_t2776330603 *, int32_t);
	static Animator_StartRecording_m2916226974_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_StartRecording_m2916226974_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::StartRecording(System.Int32)");
	_il2cpp_icall_func(__this, ___frameCount0);
}
// System.Void UnityEngine.Animator::StopRecording()
extern "C"  void Animator_StopRecording_m4232410323 (Animator_t2776330603 * __this, const MethodInfo* method)
{
	typedef void (*Animator_StopRecording_m4232410323_ftn) (Animator_t2776330603 *);
	static Animator_StopRecording_m4232410323_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_StopRecording_m4232410323_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::StopRecording()");
	_il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Animator::get_recorderStartTime()
extern "C"  float Animator_get_recorderStartTime_m1936545440 (Animator_t2776330603 * __this, const MethodInfo* method)
{
	typedef float (*Animator_get_recorderStartTime_m1936545440_ftn) (Animator_t2776330603 *);
	static Animator_get_recorderStartTime_m1936545440_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_recorderStartTime_m1936545440_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_recorderStartTime()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Animator::get_recorderStopTime()
extern "C"  float Animator_get_recorderStopTime_m3086287392 (Animator_t2776330603 * __this, const MethodInfo* method)
{
	typedef float (*Animator_get_recorderStopTime_m3086287392_ftn) (Animator_t2776330603 *);
	static Animator_get_recorderStopTime_m3086287392_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_recorderStopTime_m3086287392_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_recorderStopTime()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.RuntimeAnimatorController UnityEngine.Animator::get_runtimeAnimatorController()
extern "C"  RuntimeAnimatorController_t274649809 * Animator_get_runtimeAnimatorController_m1822082727 (Animator_t2776330603 * __this, const MethodInfo* method)
{
	typedef RuntimeAnimatorController_t274649809 * (*Animator_get_runtimeAnimatorController_m1822082727_ftn) (Animator_t2776330603 *);
	static Animator_get_runtimeAnimatorController_m1822082727_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_runtimeAnimatorController_m1822082727_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_runtimeAnimatorController()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Animator::StringToHash(System.String)
extern "C"  int32_t Animator_StringToHash_m4020897098 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	typedef int32_t (*Animator_StringToHash_m4020897098_ftn) (String_t*);
	static Animator_StringToHash_m4020897098_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_StringToHash_m4020897098_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::StringToHash(System.String)");
	return _il2cpp_icall_func(___name0);
}
// System.Void UnityEngine.Animator::CheckIfInIKPass()
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3319930659;
extern const uint32_t Animator_CheckIfInIKPass_m2875692449_MetadataUsageId;
extern "C"  void Animator_CheckIfInIKPass_m2875692449 (Animator_t2776330603 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Animator_CheckIfInIKPass_m2875692449_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Animator_get_logWarnings_m1817672208(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		bool L_1 = Animator_CheckIfInIKPassInternal_m914167192(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3123317694(NULL /*static, unused*/, _stringLiteral3319930659, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Boolean UnityEngine.Animator::CheckIfInIKPassInternal()
extern "C"  bool Animator_CheckIfInIKPassInternal_m914167192 (Animator_t2776330603 * __this, const MethodInfo* method)
{
	typedef bool (*Animator_CheckIfInIKPassInternal_m914167192_ftn) (Animator_t2776330603 *);
	static Animator_CheckIfInIKPassInternal_m914167192_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_CheckIfInIKPassInternal_m914167192_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::CheckIfInIKPassInternal()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Animator::SetFloatID(System.Int32,System.Single)
extern "C"  void Animator_SetFloatID_m819072393 (Animator_t2776330603 * __this, int32_t ___id0, float ___value1, const MethodInfo* method)
{
	typedef void (*Animator_SetFloatID_m819072393_ftn) (Animator_t2776330603 *, int32_t, float);
	static Animator_SetFloatID_m819072393_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetFloatID_m819072393_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetFloatID(System.Int32,System.Single)");
	_il2cpp_icall_func(__this, ___id0, ___value1);
}
// System.Single UnityEngine.Animator::GetFloatID(System.Int32)
extern "C"  float Animator_GetFloatID_m373159228 (Animator_t2776330603 * __this, int32_t ___id0, const MethodInfo* method)
{
	typedef float (*Animator_GetFloatID_m373159228_ftn) (Animator_t2776330603 *, int32_t);
	static Animator_GetFloatID_m373159228_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetFloatID_m373159228_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetFloatID(System.Int32)");
	return _il2cpp_icall_func(__this, ___id0);
}
// System.Void UnityEngine.Animator::SetBoolString(System.String,System.Boolean)
extern "C"  void Animator_SetBoolString_m275475356 (Animator_t2776330603 * __this, String_t* ___name0, bool ___value1, const MethodInfo* method)
{
	typedef void (*Animator_SetBoolString_m275475356_ftn) (Animator_t2776330603 *, String_t*, bool);
	static Animator_SetBoolString_m275475356_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetBoolString_m275475356_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetBoolString(System.String,System.Boolean)");
	_il2cpp_icall_func(__this, ___name0, ___value1);
}
// System.Void UnityEngine.Animator::SetBoolID(System.Int32,System.Boolean)
extern "C"  void Animator_SetBoolID_m516262497 (Animator_t2776330603 * __this, int32_t ___id0, bool ___value1, const MethodInfo* method)
{
	typedef void (*Animator_SetBoolID_m516262497_ftn) (Animator_t2776330603 *, int32_t, bool);
	static Animator_SetBoolID_m516262497_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetBoolID_m516262497_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetBoolID(System.Int32,System.Boolean)");
	_il2cpp_icall_func(__this, ___id0, ___value1);
}
// System.Boolean UnityEngine.Animator::GetBoolID(System.Int32)
extern "C"  bool Animator_GetBoolID_m1397798250 (Animator_t2776330603 * __this, int32_t ___id0, const MethodInfo* method)
{
	typedef bool (*Animator_GetBoolID_m1397798250_ftn) (Animator_t2776330603 *, int32_t);
	static Animator_GetBoolID_m1397798250_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetBoolID_m1397798250_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetBoolID(System.Int32)");
	return _il2cpp_icall_func(__this, ___id0);
}
// System.Void UnityEngine.Animator::SetIntegerID(System.Int32,System.Int32)
extern "C"  void Animator_SetIntegerID_m276290769 (Animator_t2776330603 * __this, int32_t ___id0, int32_t ___value1, const MethodInfo* method)
{
	typedef void (*Animator_SetIntegerID_m276290769_ftn) (Animator_t2776330603 *, int32_t, int32_t);
	static Animator_SetIntegerID_m276290769_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetIntegerID_m276290769_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetIntegerID(System.Int32,System.Int32)");
	_il2cpp_icall_func(__this, ___id0, ___value1);
}
// System.Int32 UnityEngine.Animator::GetIntegerID(System.Int32)
extern "C"  int32_t Animator_GetIntegerID_m4210859218 (Animator_t2776330603 * __this, int32_t ___id0, const MethodInfo* method)
{
	typedef int32_t (*Animator_GetIntegerID_m4210859218_ftn) (Animator_t2776330603 *, int32_t);
	static Animator_GetIntegerID_m4210859218_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetIntegerID_m4210859218_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetIntegerID(System.Int32)");
	return _il2cpp_icall_func(__this, ___id0);
}
// System.Void UnityEngine.Animator::SetTriggerString(System.String)
extern "C"  void Animator_SetTriggerString_m1378271133 (Animator_t2776330603 * __this, String_t* ___name0, const MethodInfo* method)
{
	typedef void (*Animator_SetTriggerString_m1378271133_ftn) (Animator_t2776330603 *, String_t*);
	static Animator_SetTriggerString_m1378271133_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetTriggerString_m1378271133_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetTriggerString(System.String)");
	_il2cpp_icall_func(__this, ___name0);
}
// System.Void UnityEngine.Animator::ResetTriggerString(System.String)
extern "C"  void Animator_ResetTriggerString_m1817269834 (Animator_t2776330603 * __this, String_t* ___name0, const MethodInfo* method)
{
	typedef void (*Animator_ResetTriggerString_m1817269834_ftn) (Animator_t2776330603 *, String_t*);
	static Animator_ResetTriggerString_m1817269834_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_ResetTriggerString_m1817269834_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::ResetTriggerString(System.String)");
	_il2cpp_icall_func(__this, ___name0);
}
// System.Boolean UnityEngine.Animator::IsParameterControlledByCurveString(System.String)
extern "C"  bool Animator_IsParameterControlledByCurveString_m1231885762 (Animator_t2776330603 * __this, String_t* ___name0, const MethodInfo* method)
{
	typedef bool (*Animator_IsParameterControlledByCurveString_m1231885762_ftn) (Animator_t2776330603 *, String_t*);
	static Animator_IsParameterControlledByCurveString_m1231885762_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_IsParameterControlledByCurveString_m1231885762_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::IsParameterControlledByCurveString(System.String)");
	return _il2cpp_icall_func(__this, ___name0);
}
// System.Void UnityEngine.Animator::SetFloatStringDamp(System.String,System.Single,System.Single,System.Single)
extern "C"  void Animator_SetFloatStringDamp_m2274151144 (Animator_t2776330603 * __this, String_t* ___name0, float ___value1, float ___dampTime2, float ___deltaTime3, const MethodInfo* method)
{
	typedef void (*Animator_SetFloatStringDamp_m2274151144_ftn) (Animator_t2776330603 *, String_t*, float, float, float);
	static Animator_SetFloatStringDamp_m2274151144_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetFloatStringDamp_m2274151144_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetFloatStringDamp(System.String,System.Single,System.Single,System.Single)");
	_il2cpp_icall_func(__this, ___name0, ___value1, ___dampTime2, ___deltaTime3);
}
// System.Void UnityEngine.Animator::SetFloatIDDamp(System.Int32,System.Single,System.Single,System.Single)
extern "C"  void Animator_SetFloatIDDamp_m4107886867 (Animator_t2776330603 * __this, int32_t ___id0, float ___value1, float ___dampTime2, float ___deltaTime3, const MethodInfo* method)
{
	typedef void (*Animator_SetFloatIDDamp_m4107886867_ftn) (Animator_t2776330603 *, int32_t, float, float, float);
	static Animator_SetFloatIDDamp_m4107886867_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetFloatIDDamp_m4107886867_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetFloatIDDamp(System.Int32,System.Single,System.Single,System.Single)");
	_il2cpp_icall_func(__this, ___id0, ___value1, ___dampTime2, ___deltaTime3);
}
// System.Boolean UnityEngine.Animator::get_layersAffectMassCenter()
extern "C"  bool Animator_get_layersAffectMassCenter_m3409296173 (Animator_t2776330603 * __this, const MethodInfo* method)
{
	typedef bool (*Animator_get_layersAffectMassCenter_m3409296173_ftn) (Animator_t2776330603 *);
	static Animator_get_layersAffectMassCenter_m3409296173_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_layersAffectMassCenter_m3409296173_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_layersAffectMassCenter()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Animator::set_layersAffectMassCenter(System.Boolean)
extern "C"  void Animator_set_layersAffectMassCenter_m1296962674 (Animator_t2776330603 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Animator_set_layersAffectMassCenter_m1296962674_ftn) (Animator_t2776330603 *, bool);
	static Animator_set_layersAffectMassCenter_m1296962674_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_set_layersAffectMassCenter_m1296962674_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::set_layersAffectMassCenter(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Animator::get_leftFeetBottomHeight()
extern "C"  float Animator_get_leftFeetBottomHeight_m4166530042 (Animator_t2776330603 * __this, const MethodInfo* method)
{
	typedef float (*Animator_get_leftFeetBottomHeight_m4166530042_ftn) (Animator_t2776330603 *);
	static Animator_get_leftFeetBottomHeight_m4166530042_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_leftFeetBottomHeight_m4166530042_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_leftFeetBottomHeight()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Animator::get_rightFeetBottomHeight()
extern "C"  float Animator_get_rightFeetBottomHeight_m3839016939 (Animator_t2776330603 * __this, const MethodInfo* method)
{
	typedef float (*Animator_get_rightFeetBottomHeight_m3839016939_ftn) (Animator_t2776330603 *);
	static Animator_get_rightFeetBottomHeight_m3839016939_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_rightFeetBottomHeight_m3839016939_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_rightFeetBottomHeight()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Animator::get_logWarnings()
extern "C"  bool Animator_get_logWarnings_m1817672208 (Animator_t2776330603 * __this, const MethodInfo* method)
{
	typedef bool (*Animator_get_logWarnings_m1817672208_ftn) (Animator_t2776330603 *);
	static Animator_get_logWarnings_m1817672208_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_logWarnings_m1817672208_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_logWarnings()");
	return _il2cpp_icall_func(__this);
}
// Conversion methods for marshalling of: UnityEngine.AnimatorClipInfo
extern "C" void AnimatorClipInfo_t2746035113_marshal_pinvoke(const AnimatorClipInfo_t2746035113& unmarshaled, AnimatorClipInfo_t2746035113_marshaled_pinvoke& marshaled)
{
	marshaled.___m_ClipInstanceID_0 = unmarshaled.get_m_ClipInstanceID_0();
	marshaled.___m_Weight_1 = unmarshaled.get_m_Weight_1();
}
extern "C" void AnimatorClipInfo_t2746035113_marshal_pinvoke_back(const AnimatorClipInfo_t2746035113_marshaled_pinvoke& marshaled, AnimatorClipInfo_t2746035113& unmarshaled)
{
	int32_t unmarshaled_m_ClipInstanceID_temp_0 = 0;
	unmarshaled_m_ClipInstanceID_temp_0 = marshaled.___m_ClipInstanceID_0;
	unmarshaled.set_m_ClipInstanceID_0(unmarshaled_m_ClipInstanceID_temp_0);
	float unmarshaled_m_Weight_temp_1 = 0.0f;
	unmarshaled_m_Weight_temp_1 = marshaled.___m_Weight_1;
	unmarshaled.set_m_Weight_1(unmarshaled_m_Weight_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimatorClipInfo
extern "C" void AnimatorClipInfo_t2746035113_marshal_pinvoke_cleanup(AnimatorClipInfo_t2746035113_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AnimatorClipInfo
extern "C" void AnimatorClipInfo_t2746035113_marshal_com(const AnimatorClipInfo_t2746035113& unmarshaled, AnimatorClipInfo_t2746035113_marshaled_com& marshaled)
{
	marshaled.___m_ClipInstanceID_0 = unmarshaled.get_m_ClipInstanceID_0();
	marshaled.___m_Weight_1 = unmarshaled.get_m_Weight_1();
}
extern "C" void AnimatorClipInfo_t2746035113_marshal_com_back(const AnimatorClipInfo_t2746035113_marshaled_com& marshaled, AnimatorClipInfo_t2746035113& unmarshaled)
{
	int32_t unmarshaled_m_ClipInstanceID_temp_0 = 0;
	unmarshaled_m_ClipInstanceID_temp_0 = marshaled.___m_ClipInstanceID_0;
	unmarshaled.set_m_ClipInstanceID_0(unmarshaled_m_ClipInstanceID_temp_0);
	float unmarshaled_m_Weight_temp_1 = 0.0f;
	unmarshaled_m_Weight_temp_1 = marshaled.___m_Weight_1;
	unmarshaled.set_m_Weight_1(unmarshaled_m_Weight_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimatorClipInfo
extern "C" void AnimatorClipInfo_t2746035113_marshal_com_cleanup(AnimatorClipInfo_t2746035113_marshaled_com& marshaled)
{
}
// System.Boolean UnityEngine.AnimatorStateInfo::IsName(System.String)
extern "C"  bool AnimatorStateInfo_IsName_m1653922768 (AnimatorStateInfo_t323110318 * __this, String_t* ___name0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B4_0 = 0;
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = Animator_StringToHash_m4020897098(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		int32_t L_3 = __this->get_m_FullPath_2();
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_4 = V_0;
		int32_t L_5 = __this->get_m_Name_0();
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_6 = V_0;
		int32_t L_7 = __this->get_m_Path_1();
		G_B4_0 = ((((int32_t)L_6) == ((int32_t)L_7))? 1 : 0);
		goto IL_002b;
	}

IL_002a:
	{
		G_B4_0 = 1;
	}

IL_002b:
	{
		return (bool)G_B4_0;
	}
}
extern "C"  bool AnimatorStateInfo_IsName_m1653922768_AdjustorThunk (Il2CppObject * __this, String_t* ___name0, const MethodInfo* method)
{
	AnimatorStateInfo_t323110318 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t323110318 *>(__this + 1);
	return AnimatorStateInfo_IsName_m1653922768(_thisAdjusted, ___name0, method);
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_fullPathHash()
extern "C"  int32_t AnimatorStateInfo_get_fullPathHash_m3257074542 (AnimatorStateInfo_t323110318 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_FullPath_2();
		return L_0;
	}
}
extern "C"  int32_t AnimatorStateInfo_get_fullPathHash_m3257074542_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t323110318 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t323110318 *>(__this + 1);
	return AnimatorStateInfo_get_fullPathHash_m3257074542(_thisAdjusted, method);
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_nameHash()
extern "C"  int32_t AnimatorStateInfo_get_nameHash_m453823205 (AnimatorStateInfo_t323110318 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Path_1();
		return L_0;
	}
}
extern "C"  int32_t AnimatorStateInfo_get_nameHash_m453823205_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t323110318 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t323110318 *>(__this + 1);
	return AnimatorStateInfo_get_nameHash_m453823205(_thisAdjusted, method);
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_shortNameHash()
extern "C"  int32_t AnimatorStateInfo_get_shortNameHash_m994885515 (AnimatorStateInfo_t323110318 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Name_0();
		return L_0;
	}
}
extern "C"  int32_t AnimatorStateInfo_get_shortNameHash_m994885515_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t323110318 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t323110318 *>(__this + 1);
	return AnimatorStateInfo_get_shortNameHash_m994885515(_thisAdjusted, method);
}
// System.Single UnityEngine.AnimatorStateInfo::get_normalizedTime()
extern "C"  float AnimatorStateInfo_get_normalizedTime_m2531821060 (AnimatorStateInfo_t323110318 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_NormalizedTime_3();
		return L_0;
	}
}
extern "C"  float AnimatorStateInfo_get_normalizedTime_m2531821060_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t323110318 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t323110318 *>(__this + 1);
	return AnimatorStateInfo_get_normalizedTime_m2531821060(_thisAdjusted, method);
}
// System.Single UnityEngine.AnimatorStateInfo::get_length()
extern "C"  float AnimatorStateInfo_get_length_m3147284742 (AnimatorStateInfo_t323110318 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Length_4();
		return L_0;
	}
}
extern "C"  float AnimatorStateInfo_get_length_m3147284742_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t323110318 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t323110318 *>(__this + 1);
	return AnimatorStateInfo_get_length_m3147284742(_thisAdjusted, method);
}
// System.Single UnityEngine.AnimatorStateInfo::get_speed()
extern "C"  float AnimatorStateInfo_get_speed_m247405833 (AnimatorStateInfo_t323110318 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Speed_5();
		return L_0;
	}
}
extern "C"  float AnimatorStateInfo_get_speed_m247405833_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t323110318 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t323110318 *>(__this + 1);
	return AnimatorStateInfo_get_speed_m247405833(_thisAdjusted, method);
}
// System.Single UnityEngine.AnimatorStateInfo::get_speedMultiplier()
extern "C"  float AnimatorStateInfo_get_speedMultiplier_m489756842 (AnimatorStateInfo_t323110318 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_SpeedMultiplier_6();
		return L_0;
	}
}
extern "C"  float AnimatorStateInfo_get_speedMultiplier_m489756842_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t323110318 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t323110318 *>(__this + 1);
	return AnimatorStateInfo_get_speedMultiplier_m489756842(_thisAdjusted, method);
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_tagHash()
extern "C"  int32_t AnimatorStateInfo_get_tagHash_m3543262078 (AnimatorStateInfo_t323110318 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Tag_7();
		return L_0;
	}
}
extern "C"  int32_t AnimatorStateInfo_get_tagHash_m3543262078_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t323110318 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t323110318 *>(__this + 1);
	return AnimatorStateInfo_get_tagHash_m3543262078(_thisAdjusted, method);
}
// System.Boolean UnityEngine.AnimatorStateInfo::IsTag(System.String)
extern "C"  bool AnimatorStateInfo_IsTag_m119936877 (AnimatorStateInfo_t323110318 * __this, String_t* ___tag0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___tag0;
		int32_t L_1 = Animator_StringToHash_m4020897098(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_m_Tag_7();
		return (bool)((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
	}
}
extern "C"  bool AnimatorStateInfo_IsTag_m119936877_AdjustorThunk (Il2CppObject * __this, String_t* ___tag0, const MethodInfo* method)
{
	AnimatorStateInfo_t323110318 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t323110318 *>(__this + 1);
	return AnimatorStateInfo_IsTag_m119936877(_thisAdjusted, ___tag0, method);
}
// System.Boolean UnityEngine.AnimatorStateInfo::get_loop()
extern "C"  bool AnimatorStateInfo_get_loop_m1495892586 (AnimatorStateInfo_t323110318 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Loop_8();
		return (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
extern "C"  bool AnimatorStateInfo_get_loop_m1495892586_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t323110318 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t323110318 *>(__this + 1);
	return AnimatorStateInfo_get_loop_m1495892586(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.AnimatorStateInfo
extern "C" void AnimatorStateInfo_t323110318_marshal_pinvoke(const AnimatorStateInfo_t323110318& unmarshaled, AnimatorStateInfo_t323110318_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Name_0 = unmarshaled.get_m_Name_0();
	marshaled.___m_Path_1 = unmarshaled.get_m_Path_1();
	marshaled.___m_FullPath_2 = unmarshaled.get_m_FullPath_2();
	marshaled.___m_NormalizedTime_3 = unmarshaled.get_m_NormalizedTime_3();
	marshaled.___m_Length_4 = unmarshaled.get_m_Length_4();
	marshaled.___m_Speed_5 = unmarshaled.get_m_Speed_5();
	marshaled.___m_SpeedMultiplier_6 = unmarshaled.get_m_SpeedMultiplier_6();
	marshaled.___m_Tag_7 = unmarshaled.get_m_Tag_7();
	marshaled.___m_Loop_8 = unmarshaled.get_m_Loop_8();
}
extern "C" void AnimatorStateInfo_t323110318_marshal_pinvoke_back(const AnimatorStateInfo_t323110318_marshaled_pinvoke& marshaled, AnimatorStateInfo_t323110318& unmarshaled)
{
	int32_t unmarshaled_m_Name_temp_0 = 0;
	unmarshaled_m_Name_temp_0 = marshaled.___m_Name_0;
	unmarshaled.set_m_Name_0(unmarshaled_m_Name_temp_0);
	int32_t unmarshaled_m_Path_temp_1 = 0;
	unmarshaled_m_Path_temp_1 = marshaled.___m_Path_1;
	unmarshaled.set_m_Path_1(unmarshaled_m_Path_temp_1);
	int32_t unmarshaled_m_FullPath_temp_2 = 0;
	unmarshaled_m_FullPath_temp_2 = marshaled.___m_FullPath_2;
	unmarshaled.set_m_FullPath_2(unmarshaled_m_FullPath_temp_2);
	float unmarshaled_m_NormalizedTime_temp_3 = 0.0f;
	unmarshaled_m_NormalizedTime_temp_3 = marshaled.___m_NormalizedTime_3;
	unmarshaled.set_m_NormalizedTime_3(unmarshaled_m_NormalizedTime_temp_3);
	float unmarshaled_m_Length_temp_4 = 0.0f;
	unmarshaled_m_Length_temp_4 = marshaled.___m_Length_4;
	unmarshaled.set_m_Length_4(unmarshaled_m_Length_temp_4);
	float unmarshaled_m_Speed_temp_5 = 0.0f;
	unmarshaled_m_Speed_temp_5 = marshaled.___m_Speed_5;
	unmarshaled.set_m_Speed_5(unmarshaled_m_Speed_temp_5);
	float unmarshaled_m_SpeedMultiplier_temp_6 = 0.0f;
	unmarshaled_m_SpeedMultiplier_temp_6 = marshaled.___m_SpeedMultiplier_6;
	unmarshaled.set_m_SpeedMultiplier_6(unmarshaled_m_SpeedMultiplier_temp_6);
	int32_t unmarshaled_m_Tag_temp_7 = 0;
	unmarshaled_m_Tag_temp_7 = marshaled.___m_Tag_7;
	unmarshaled.set_m_Tag_7(unmarshaled_m_Tag_temp_7);
	int32_t unmarshaled_m_Loop_temp_8 = 0;
	unmarshaled_m_Loop_temp_8 = marshaled.___m_Loop_8;
	unmarshaled.set_m_Loop_8(unmarshaled_m_Loop_temp_8);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimatorStateInfo
extern "C" void AnimatorStateInfo_t323110318_marshal_pinvoke_cleanup(AnimatorStateInfo_t323110318_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AnimatorStateInfo
extern "C" void AnimatorStateInfo_t323110318_marshal_com(const AnimatorStateInfo_t323110318& unmarshaled, AnimatorStateInfo_t323110318_marshaled_com& marshaled)
{
	marshaled.___m_Name_0 = unmarshaled.get_m_Name_0();
	marshaled.___m_Path_1 = unmarshaled.get_m_Path_1();
	marshaled.___m_FullPath_2 = unmarshaled.get_m_FullPath_2();
	marshaled.___m_NormalizedTime_3 = unmarshaled.get_m_NormalizedTime_3();
	marshaled.___m_Length_4 = unmarshaled.get_m_Length_4();
	marshaled.___m_Speed_5 = unmarshaled.get_m_Speed_5();
	marshaled.___m_SpeedMultiplier_6 = unmarshaled.get_m_SpeedMultiplier_6();
	marshaled.___m_Tag_7 = unmarshaled.get_m_Tag_7();
	marshaled.___m_Loop_8 = unmarshaled.get_m_Loop_8();
}
extern "C" void AnimatorStateInfo_t323110318_marshal_com_back(const AnimatorStateInfo_t323110318_marshaled_com& marshaled, AnimatorStateInfo_t323110318& unmarshaled)
{
	int32_t unmarshaled_m_Name_temp_0 = 0;
	unmarshaled_m_Name_temp_0 = marshaled.___m_Name_0;
	unmarshaled.set_m_Name_0(unmarshaled_m_Name_temp_0);
	int32_t unmarshaled_m_Path_temp_1 = 0;
	unmarshaled_m_Path_temp_1 = marshaled.___m_Path_1;
	unmarshaled.set_m_Path_1(unmarshaled_m_Path_temp_1);
	int32_t unmarshaled_m_FullPath_temp_2 = 0;
	unmarshaled_m_FullPath_temp_2 = marshaled.___m_FullPath_2;
	unmarshaled.set_m_FullPath_2(unmarshaled_m_FullPath_temp_2);
	float unmarshaled_m_NormalizedTime_temp_3 = 0.0f;
	unmarshaled_m_NormalizedTime_temp_3 = marshaled.___m_NormalizedTime_3;
	unmarshaled.set_m_NormalizedTime_3(unmarshaled_m_NormalizedTime_temp_3);
	float unmarshaled_m_Length_temp_4 = 0.0f;
	unmarshaled_m_Length_temp_4 = marshaled.___m_Length_4;
	unmarshaled.set_m_Length_4(unmarshaled_m_Length_temp_4);
	float unmarshaled_m_Speed_temp_5 = 0.0f;
	unmarshaled_m_Speed_temp_5 = marshaled.___m_Speed_5;
	unmarshaled.set_m_Speed_5(unmarshaled_m_Speed_temp_5);
	float unmarshaled_m_SpeedMultiplier_temp_6 = 0.0f;
	unmarshaled_m_SpeedMultiplier_temp_6 = marshaled.___m_SpeedMultiplier_6;
	unmarshaled.set_m_SpeedMultiplier_6(unmarshaled_m_SpeedMultiplier_temp_6);
	int32_t unmarshaled_m_Tag_temp_7 = 0;
	unmarshaled_m_Tag_temp_7 = marshaled.___m_Tag_7;
	unmarshaled.set_m_Tag_7(unmarshaled_m_Tag_temp_7);
	int32_t unmarshaled_m_Loop_temp_8 = 0;
	unmarshaled_m_Loop_temp_8 = marshaled.___m_Loop_8;
	unmarshaled.set_m_Loop_8(unmarshaled_m_Loop_temp_8);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimatorStateInfo
extern "C" void AnimatorStateInfo_t323110318_marshal_com_cleanup(AnimatorStateInfo_t323110318_marshaled_com& marshaled)
{
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::IsName(System.String)
extern "C"  bool AnimatorTransitionInfo_IsName_m1283663078 (AnimatorTransitionInfo_t2817229998 * __this, String_t* ___name0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = Animator_StringToHash_m4020897098(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_m_Name_2();
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_3 = ___name0;
		int32_t L_4 = Animator_StringToHash_m4020897098(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		int32_t L_5 = __this->get_m_FullPath_0();
		G_B3_0 = ((((int32_t)L_4) == ((int32_t)L_5))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 1;
	}

IL_0022:
	{
		return (bool)G_B3_0;
	}
}
extern "C"  bool AnimatorTransitionInfo_IsName_m1283663078_AdjustorThunk (Il2CppObject * __this, String_t* ___name0, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2817229998 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2817229998 *>(__this + 1);
	return AnimatorTransitionInfo_IsName_m1283663078(_thisAdjusted, ___name0, method);
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::IsUserName(System.String)
extern "C"  bool AnimatorTransitionInfo_IsUserName_m2732197659 (AnimatorTransitionInfo_t2817229998 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = Animator_StringToHash_m4020897098(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_m_UserName_1();
		return (bool)((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
	}
}
extern "C"  bool AnimatorTransitionInfo_IsUserName_m2732197659_AdjustorThunk (Il2CppObject * __this, String_t* ___name0, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2817229998 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2817229998 *>(__this + 1);
	return AnimatorTransitionInfo_IsUserName_m2732197659(_thisAdjusted, ___name0, method);
}
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_fullPathHash()
extern "C"  int32_t AnimatorTransitionInfo_get_fullPathHash_m3335466636 (AnimatorTransitionInfo_t2817229998 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_FullPath_0();
		return L_0;
	}
}
extern "C"  int32_t AnimatorTransitionInfo_get_fullPathHash_m3335466636_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2817229998 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2817229998 *>(__this + 1);
	return AnimatorTransitionInfo_get_fullPathHash_m3335466636(_thisAdjusted, method);
}
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_nameHash()
extern "C"  int32_t AnimatorTransitionInfo_get_nameHash_m2102867203 (AnimatorTransitionInfo_t2817229998 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Name_2();
		return L_0;
	}
}
extern "C"  int32_t AnimatorTransitionInfo_get_nameHash_m2102867203_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2817229998 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2817229998 *>(__this + 1);
	return AnimatorTransitionInfo_get_nameHash_m2102867203(_thisAdjusted, method);
}
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_userNameHash()
extern "C"  int32_t AnimatorTransitionInfo_get_userNameHash_m249220782 (AnimatorTransitionInfo_t2817229998 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_UserName_1();
		return L_0;
	}
}
extern "C"  int32_t AnimatorTransitionInfo_get_userNameHash_m249220782_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2817229998 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2817229998 *>(__this + 1);
	return AnimatorTransitionInfo_get_userNameHash_m249220782(_thisAdjusted, method);
}
// System.Single UnityEngine.AnimatorTransitionInfo::get_normalizedTime()
extern "C"  float AnimatorTransitionInfo_get_normalizedTime_m3258684986 (AnimatorTransitionInfo_t2817229998 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_NormalizedTime_3();
		return L_0;
	}
}
extern "C"  float AnimatorTransitionInfo_get_normalizedTime_m3258684986_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2817229998 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2817229998 *>(__this + 1);
	return AnimatorTransitionInfo_get_normalizedTime_m3258684986(_thisAdjusted, method);
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::get_anyState()
extern "C"  bool AnimatorTransitionInfo_get_anyState_m3421556405 (AnimatorTransitionInfo_t2817229998 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_AnyState_4();
		return L_0;
	}
}
extern "C"  bool AnimatorTransitionInfo_get_anyState_m3421556405_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2817229998 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2817229998 *>(__this + 1);
	return AnimatorTransitionInfo_get_anyState_m3421556405(_thisAdjusted, method);
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::get_entry()
extern "C"  bool AnimatorTransitionInfo_get_entry_m2038132324 (AnimatorTransitionInfo_t2817229998 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_TransitionType_5();
		return (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)2))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
extern "C"  bool AnimatorTransitionInfo_get_entry_m2038132324_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2817229998 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2817229998 *>(__this + 1);
	return AnimatorTransitionInfo_get_entry_m2038132324(_thisAdjusted, method);
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::get_exit()
extern "C"  bool AnimatorTransitionInfo_get_exit_m2568505102 (AnimatorTransitionInfo_t2817229998 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_TransitionType_5();
		return (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)4))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
extern "C"  bool AnimatorTransitionInfo_get_exit_m2568505102_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2817229998 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2817229998 *>(__this + 1);
	return AnimatorTransitionInfo_get_exit_m2568505102(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.AnimatorTransitionInfo
extern "C" void AnimatorTransitionInfo_t2817229998_marshal_pinvoke(const AnimatorTransitionInfo_t2817229998& unmarshaled, AnimatorTransitionInfo_t2817229998_marshaled_pinvoke& marshaled)
{
	marshaled.___m_FullPath_0 = unmarshaled.get_m_FullPath_0();
	marshaled.___m_UserName_1 = unmarshaled.get_m_UserName_1();
	marshaled.___m_Name_2 = unmarshaled.get_m_Name_2();
	marshaled.___m_NormalizedTime_3 = unmarshaled.get_m_NormalizedTime_3();
	marshaled.___m_AnyState_4 = unmarshaled.get_m_AnyState_4();
	marshaled.___m_TransitionType_5 = unmarshaled.get_m_TransitionType_5();
}
extern "C" void AnimatorTransitionInfo_t2817229998_marshal_pinvoke_back(const AnimatorTransitionInfo_t2817229998_marshaled_pinvoke& marshaled, AnimatorTransitionInfo_t2817229998& unmarshaled)
{
	int32_t unmarshaled_m_FullPath_temp_0 = 0;
	unmarshaled_m_FullPath_temp_0 = marshaled.___m_FullPath_0;
	unmarshaled.set_m_FullPath_0(unmarshaled_m_FullPath_temp_0);
	int32_t unmarshaled_m_UserName_temp_1 = 0;
	unmarshaled_m_UserName_temp_1 = marshaled.___m_UserName_1;
	unmarshaled.set_m_UserName_1(unmarshaled_m_UserName_temp_1);
	int32_t unmarshaled_m_Name_temp_2 = 0;
	unmarshaled_m_Name_temp_2 = marshaled.___m_Name_2;
	unmarshaled.set_m_Name_2(unmarshaled_m_Name_temp_2);
	float unmarshaled_m_NormalizedTime_temp_3 = 0.0f;
	unmarshaled_m_NormalizedTime_temp_3 = marshaled.___m_NormalizedTime_3;
	unmarshaled.set_m_NormalizedTime_3(unmarshaled_m_NormalizedTime_temp_3);
	bool unmarshaled_m_AnyState_temp_4 = false;
	unmarshaled_m_AnyState_temp_4 = marshaled.___m_AnyState_4;
	unmarshaled.set_m_AnyState_4(unmarshaled_m_AnyState_temp_4);
	int32_t unmarshaled_m_TransitionType_temp_5 = 0;
	unmarshaled_m_TransitionType_temp_5 = marshaled.___m_TransitionType_5;
	unmarshaled.set_m_TransitionType_5(unmarshaled_m_TransitionType_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimatorTransitionInfo
extern "C" void AnimatorTransitionInfo_t2817229998_marshal_pinvoke_cleanup(AnimatorTransitionInfo_t2817229998_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AnimatorTransitionInfo
extern "C" void AnimatorTransitionInfo_t2817229998_marshal_com(const AnimatorTransitionInfo_t2817229998& unmarshaled, AnimatorTransitionInfo_t2817229998_marshaled_com& marshaled)
{
	marshaled.___m_FullPath_0 = unmarshaled.get_m_FullPath_0();
	marshaled.___m_UserName_1 = unmarshaled.get_m_UserName_1();
	marshaled.___m_Name_2 = unmarshaled.get_m_Name_2();
	marshaled.___m_NormalizedTime_3 = unmarshaled.get_m_NormalizedTime_3();
	marshaled.___m_AnyState_4 = unmarshaled.get_m_AnyState_4();
	marshaled.___m_TransitionType_5 = unmarshaled.get_m_TransitionType_5();
}
extern "C" void AnimatorTransitionInfo_t2817229998_marshal_com_back(const AnimatorTransitionInfo_t2817229998_marshaled_com& marshaled, AnimatorTransitionInfo_t2817229998& unmarshaled)
{
	int32_t unmarshaled_m_FullPath_temp_0 = 0;
	unmarshaled_m_FullPath_temp_0 = marshaled.___m_FullPath_0;
	unmarshaled.set_m_FullPath_0(unmarshaled_m_FullPath_temp_0);
	int32_t unmarshaled_m_UserName_temp_1 = 0;
	unmarshaled_m_UserName_temp_1 = marshaled.___m_UserName_1;
	unmarshaled.set_m_UserName_1(unmarshaled_m_UserName_temp_1);
	int32_t unmarshaled_m_Name_temp_2 = 0;
	unmarshaled_m_Name_temp_2 = marshaled.___m_Name_2;
	unmarshaled.set_m_Name_2(unmarshaled_m_Name_temp_2);
	float unmarshaled_m_NormalizedTime_temp_3 = 0.0f;
	unmarshaled_m_NormalizedTime_temp_3 = marshaled.___m_NormalizedTime_3;
	unmarshaled.set_m_NormalizedTime_3(unmarshaled_m_NormalizedTime_temp_3);
	bool unmarshaled_m_AnyState_temp_4 = false;
	unmarshaled_m_AnyState_temp_4 = marshaled.___m_AnyState_4;
	unmarshaled.set_m_AnyState_4(unmarshaled_m_AnyState_temp_4);
	int32_t unmarshaled_m_TransitionType_temp_5 = 0;
	unmarshaled_m_TransitionType_temp_5 = marshaled.___m_TransitionType_5;
	unmarshaled.set_m_TransitionType_5(unmarshaled_m_TransitionType_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimatorTransitionInfo
extern "C" void AnimatorTransitionInfo_t2817229998_marshal_com_cleanup(AnimatorTransitionInfo_t2817229998_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Application::Quit()
extern "C"  void Application_Quit_m1187862186 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*Application_Quit_m1187862186_ftn) ();
	static Application_Quit_m1187862186_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_Quit_m1187862186_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::Quit()");
	_il2cpp_icall_func();
}
// System.Boolean UnityEngine.Application::get_isLoadingLevel()
extern "C"  bool Application_get_isLoadingLevel_m1660649616 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Application_get_isLoadingLevel_m1660649616_ftn) ();
	static Application_get_isLoadingLevel_m1660649616_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_isLoadingLevel_m1660649616_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_isLoadingLevel()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.Application::get_isPlaying()
extern "C"  bool Application_get_isPlaying_m987993960 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Application_get_isPlaying_m987993960_ftn) ();
	static Application_get_isPlaying_m987993960_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_isPlaying_m987993960_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_isPlaying()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.Application::get_isEditor()
extern "C"  bool Application_get_isEditor_m1279348309 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Application_get_isEditor_m1279348309_ftn) ();
	static Application_get_isEditor_m1279348309_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_isEditor_m1279348309_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_isEditor()");
	return _il2cpp_icall_func();
}
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
extern "C"  int32_t Application_get_platform_m2918632856 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Application_get_platform_m2918632856_ftn) ();
	static Application_get_platform_m2918632856_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_platform_m2918632856_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_platform()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Application::CaptureScreenshot(System.String,System.Int32)
extern "C"  void Application_CaptureScreenshot_m3152082376 (Il2CppObject * __this /* static, unused */, String_t* ___filename0, int32_t ___superSize1, const MethodInfo* method)
{
	typedef void (*Application_CaptureScreenshot_m3152082376_ftn) (String_t*, int32_t);
	static Application_CaptureScreenshot_m3152082376_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_CaptureScreenshot_m3152082376_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::CaptureScreenshot(System.String,System.Int32)");
	_il2cpp_icall_func(___filename0, ___superSize1);
}
// System.Void UnityEngine.Application::CaptureScreenshot(System.String)
extern "C"  void Application_CaptureScreenshot_m1449096047 (Il2CppObject * __this /* static, unused */, String_t* ___filename0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		String_t* L_0 = ___filename0;
		int32_t L_1 = V_0;
		Application_CaptureScreenshot_m3152082376(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Application::set_runInBackground(System.Boolean)
extern "C"  void Application_set_runInBackground_m2333211263 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*Application_set_runInBackground_m2333211263_ftn) (bool);
	static Application_set_runInBackground_m2333211263_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_set_runInBackground_m2333211263_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::set_runInBackground(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.String UnityEngine.Application::get_dataPath()
extern "C"  String_t* Application_get_dataPath_m2519694320 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*Application_get_dataPath_m2519694320_ftn) ();
	static Application_get_dataPath_m2519694320_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_dataPath_m2519694320_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_dataPath()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.Application::get_streamingAssetsPath()
extern "C"  String_t* Application_get_streamingAssetsPath_m1181082379 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*Application_get_streamingAssetsPath_m1181082379_ftn) ();
	static Application_get_streamingAssetsPath_m1181082379_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_streamingAssetsPath_m1181082379_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_streamingAssetsPath()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.Application::get_persistentDataPath()
extern "C"  String_t* Application_get_persistentDataPath_m2554537447 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*Application_get_persistentDataPath_m2554537447_ftn) ();
	static Application_get_persistentDataPath_m2554537447_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_persistentDataPath_m2554537447_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_persistentDataPath()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.Application::ObjectToJSString(System.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2862622538_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Int16_t1153838442_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t24667981_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt16_t24667923_il2cpp_TypeInfo_var;
extern Il2CppClass* Byte_t2862609660_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t1065375142_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern Il2CppClass* StringBuilder_t243639308_il2cpp_TypeInfo_var;
extern Il2CppClass* ICollection_t2643922881_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3392903;
extern Il2CppCodeGenString* _stringLiteral92;
extern Il2CppCodeGenString* _stringLiteral2944;
extern Il2CppCodeGenString* _stringLiteral34;
extern Il2CppCodeGenString* _stringLiteral2886;
extern Il2CppCodeGenString* _stringLiteral10;
extern Il2CppCodeGenString* _stringLiteral2962;
extern Il2CppCodeGenString* _stringLiteral13;
extern Il2CppCodeGenString* _stringLiteral2966;
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral8232;
extern Il2CppCodeGenString* _stringLiteral8233;
extern Il2CppCodeGenString* _stringLiteral1102394;
extern Il2CppCodeGenString* _stringLiteral3530059663;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t Application_ObjectToJSString_m2317112936_MetadataUsageId;
extern "C"  String_t* Application_ObjectToJSString_m2317112936 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Application_ObjectToJSString_m2317112936_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	NumberFormatInfo_t1637637232 * V_1 = NULL;
	NumberFormatInfo_t1637637232 * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	StringBuilder_t243639308 * V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	float V_7 = 0.0f;
	double V_8 = 0.0;
	{
		Il2CppObject * L_0 = ___o0;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return _stringLiteral3392903;
	}

IL_000c:
	{
		Il2CppObject * L_1 = ___o0;
		if (!((String_t*)IsInstSealed(L_1, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_00a8;
		}
	}
	{
		Il2CppObject * L_2 = ___o0;
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_3);
		String_t* L_4 = String_Replace_m2915759397(L_3, _stringLiteral92, _stringLiteral2944, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = String_Replace_m2915759397(L_5, _stringLiteral34, _stringLiteral2886, /*hidden argument*/NULL);
		V_0 = L_6;
		String_t* L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = String_Replace_m2915759397(L_7, _stringLiteral10, _stringLiteral2962, /*hidden argument*/NULL);
		V_0 = L_8;
		String_t* L_9 = V_0;
		NullCheck(L_9);
		String_t* L_10 = String_Replace_m2915759397(L_9, _stringLiteral13, _stringLiteral2966, /*hidden argument*/NULL);
		V_0 = L_10;
		String_t* L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_11);
		String_t* L_13 = String_Replace_m2915759397(L_11, _stringLiteral1, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		String_t* L_14 = V_0;
		String_t* L_15 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_14);
		String_t* L_16 = String_Replace_m2915759397(L_14, _stringLiteral8232, L_15, /*hidden argument*/NULL);
		V_0 = L_16;
		String_t* L_17 = V_0;
		String_t* L_18 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_17);
		String_t* L_19 = String_Replace_m2915759397(L_17, _stringLiteral8233, L_18, /*hidden argument*/NULL);
		V_0 = L_19;
		Il2CppChar L_20 = ((Il2CppChar)((int32_t)34));
		Il2CppObject * L_21 = Box(Char_t2862622538_il2cpp_TypeInfo_var, &L_20);
		String_t* L_22 = V_0;
		Il2CppChar L_23 = ((Il2CppChar)((int32_t)34));
		Il2CppObject * L_24 = Box(Char_t2862622538_il2cpp_TypeInfo_var, &L_23);
		String_t* L_25 = String_Concat_m2809334143(NULL /*static, unused*/, L_21, L_22, L_24, /*hidden argument*/NULL);
		return L_25;
	}

IL_00a8:
	{
		Il2CppObject * L_26 = ___o0;
		if (((Il2CppObject *)IsInstSealed(L_26, Int32_t1153838500_il2cpp_TypeInfo_var)))
		{
			goto IL_00df;
		}
	}
	{
		Il2CppObject * L_27 = ___o0;
		if (((Il2CppObject *)IsInstSealed(L_27, Int16_t1153838442_il2cpp_TypeInfo_var)))
		{
			goto IL_00df;
		}
	}
	{
		Il2CppObject * L_28 = ___o0;
		if (((Il2CppObject *)IsInstSealed(L_28, UInt32_t24667981_il2cpp_TypeInfo_var)))
		{
			goto IL_00df;
		}
	}
	{
		Il2CppObject * L_29 = ___o0;
		if (((Il2CppObject *)IsInstSealed(L_29, UInt16_t24667923_il2cpp_TypeInfo_var)))
		{
			goto IL_00df;
		}
	}
	{
		Il2CppObject * L_30 = ___o0;
		if (!((Il2CppObject *)IsInstSealed(L_30, Byte_t2862609660_il2cpp_TypeInfo_var)))
		{
			goto IL_00e6;
		}
	}

IL_00df:
	{
		Il2CppObject * L_31 = ___o0;
		NullCheck(L_31);
		String_t* L_32 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_31);
		return L_32;
	}

IL_00e6:
	{
		Il2CppObject * L_33 = ___o0;
		if (!((Il2CppObject *)IsInstSealed(L_33, Single_t4291918972_il2cpp_TypeInfo_var)))
		{
			goto IL_010d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1065375142_il2cpp_TypeInfo_var);
		CultureInfo_t1065375142 * L_34 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_34);
		NumberFormatInfo_t1637637232 * L_35 = VirtFuncInvoker0< NumberFormatInfo_t1637637232 * >::Invoke(13 /* System.Globalization.NumberFormatInfo System.Globalization.CultureInfo::get_NumberFormat() */, L_34);
		V_1 = L_35;
		Il2CppObject * L_36 = ___o0;
		V_7 = ((*(float*)((float*)UnBox (L_36, Single_t4291918972_il2cpp_TypeInfo_var))));
		NumberFormatInfo_t1637637232 * L_37 = V_1;
		String_t* L_38 = Single_ToString_m1436803918((&V_7), L_37, /*hidden argument*/NULL);
		return L_38;
	}

IL_010d:
	{
		Il2CppObject * L_39 = ___o0;
		if (!((Il2CppObject *)IsInstSealed(L_39, Double_t3868226565_il2cpp_TypeInfo_var)))
		{
			goto IL_0134;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1065375142_il2cpp_TypeInfo_var);
		CultureInfo_t1065375142 * L_40 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_40);
		NumberFormatInfo_t1637637232 * L_41 = VirtFuncInvoker0< NumberFormatInfo_t1637637232 * >::Invoke(13 /* System.Globalization.NumberFormatInfo System.Globalization.CultureInfo::get_NumberFormat() */, L_40);
		V_2 = L_41;
		Il2CppObject * L_42 = ___o0;
		V_8 = ((*(double*)((double*)UnBox (L_42, Double_t3868226565_il2cpp_TypeInfo_var))));
		NumberFormatInfo_t1637637232 * L_43 = V_2;
		String_t* L_44 = Double_ToString_m937670807((&V_8), L_43, /*hidden argument*/NULL);
		return L_44;
	}

IL_0134:
	{
		Il2CppObject * L_45 = ___o0;
		if (!((Il2CppObject *)IsInstSealed(L_45, Char_t2862622538_il2cpp_TypeInfo_var)))
		{
			goto IL_016c;
		}
	}
	{
		Il2CppObject * L_46 = ___o0;
		if ((!(((uint32_t)((*(Il2CppChar*)((Il2CppChar*)UnBox (L_46, Char_t2862622538_il2cpp_TypeInfo_var))))) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_0152;
		}
	}
	{
		return _stringLiteral1102394;
	}

IL_0152:
	{
		Il2CppChar L_47 = ((Il2CppChar)((int32_t)34));
		Il2CppObject * L_48 = Box(Char_t2862622538_il2cpp_TypeInfo_var, &L_47);
		Il2CppObject * L_49 = ___o0;
		NullCheck(L_49);
		String_t* L_50 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_49);
		Il2CppChar L_51 = ((Il2CppChar)((int32_t)34));
		Il2CppObject * L_52 = Box(Char_t2862622538_il2cpp_TypeInfo_var, &L_51);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_53 = String_Concat_m2809334143(NULL /*static, unused*/, L_48, L_50, L_52, /*hidden argument*/NULL);
		return L_53;
	}

IL_016c:
	{
		Il2CppObject * L_54 = ___o0;
		if (!((Il2CppObject *)IsInst(L_54, IList_t1751339649_il2cpp_TypeInfo_var)))
		{
			goto IL_01ef;
		}
	}
	{
		Il2CppObject * L_55 = ___o0;
		V_3 = ((Il2CppObject *)Castclass(L_55, IList_t1751339649_il2cpp_TypeInfo_var));
		StringBuilder_t243639308 * L_56 = (StringBuilder_t243639308 *)il2cpp_codegen_object_new(StringBuilder_t243639308_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_56, /*hidden argument*/NULL);
		V_4 = L_56;
		StringBuilder_t243639308 * L_57 = V_4;
		NullCheck(L_57);
		StringBuilder_Append_m3898090075(L_57, _stringLiteral3530059663, /*hidden argument*/NULL);
		Il2CppObject * L_58 = V_3;
		NullCheck(L_58);
		int32_t L_59 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t2643922881_il2cpp_TypeInfo_var, L_58);
		V_5 = L_59;
		V_6 = 0;
		goto IL_01d1;
	}

IL_01a2:
	{
		int32_t L_60 = V_6;
		if (!L_60)
		{
			goto IL_01b6;
		}
	}
	{
		StringBuilder_t243639308 * L_61 = V_4;
		NullCheck(L_61);
		StringBuilder_Append_m3898090075(L_61, _stringLiteral1396, /*hidden argument*/NULL);
	}

IL_01b6:
	{
		StringBuilder_t243639308 * L_62 = V_4;
		Il2CppObject * L_63 = V_3;
		int32_t L_64 = V_6;
		NullCheck(L_63);
		Il2CppObject * L_65 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(2 /* System.Object System.Collections.IList::get_Item(System.Int32) */, IList_t1751339649_il2cpp_TypeInfo_var, L_63, L_64);
		String_t* L_66 = Application_ObjectToJSString_m2317112936(NULL /*static, unused*/, L_65, /*hidden argument*/NULL);
		NullCheck(L_62);
		StringBuilder_Append_m3898090075(L_62, L_66, /*hidden argument*/NULL);
		int32_t L_67 = V_6;
		V_6 = ((int32_t)((int32_t)L_67+(int32_t)1));
	}

IL_01d1:
	{
		int32_t L_68 = V_6;
		int32_t L_69 = V_5;
		if ((((int32_t)L_68) < ((int32_t)L_69)))
		{
			goto IL_01a2;
		}
	}
	{
		StringBuilder_t243639308 * L_70 = V_4;
		NullCheck(L_70);
		StringBuilder_Append_m3898090075(L_70, _stringLiteral41, /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_71 = V_4;
		NullCheck(L_71);
		String_t* L_72 = StringBuilder_ToString_m350379841(L_71, /*hidden argument*/NULL);
		return L_72;
	}

IL_01ef:
	{
		Il2CppObject * L_73 = ___o0;
		NullCheck(L_73);
		String_t* L_74 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_73);
		String_t* L_75 = Application_ObjectToJSString_m2317112936(NULL /*static, unused*/, L_74, /*hidden argument*/NULL);
		return L_75;
	}
}
// System.Void UnityEngine.Application::ExternalCall(System.String,System.Object[])
extern "C"  void Application_ExternalCall_m4293503818 (Il2CppObject * __this /* static, unused */, String_t* ___functionName0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___functionName0;
		ObjectU5BU5D_t1108656482* L_1 = ___args1;
		String_t* L_2 = Application_BuildInvocationForArguments_m777253945(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Application_Internal_ExternalCall_m2589949616(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Application::BuildInvocationForArguments(System.String,System.Object[])
extern Il2CppClass* StringBuilder_t243639308_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1396;
extern const uint32_t Application_BuildInvocationForArguments_m777253945_MetadataUsageId;
extern "C"  String_t* Application_BuildInvocationForArguments_m777253945 (Il2CppObject * __this /* static, unused */, String_t* ___functionName0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Application_BuildInvocationForArguments_m777253945_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t243639308 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		StringBuilder_t243639308 * L_0 = (StringBuilder_t243639308 *)il2cpp_codegen_object_new(StringBuilder_t243639308_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t243639308 * L_1 = V_0;
		String_t* L_2 = ___functionName0;
		NullCheck(L_1);
		StringBuilder_Append_m3898090075(L_1, L_2, /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_3 = V_0;
		NullCheck(L_3);
		StringBuilder_Append_m2143093878(L_3, ((int32_t)40), /*hidden argument*/NULL);
		ObjectU5BU5D_t1108656482* L_4 = ___args1;
		NullCheck(L_4);
		V_1 = (((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))));
		V_2 = 0;
		goto IL_0047;
	}

IL_0022:
	{
		int32_t L_5 = V_2;
		if (!L_5)
		{
			goto IL_0034;
		}
	}
	{
		StringBuilder_t243639308 * L_6 = V_0;
		NullCheck(L_6);
		StringBuilder_Append_m3898090075(L_6, _stringLiteral1396, /*hidden argument*/NULL);
	}

IL_0034:
	{
		StringBuilder_t243639308 * L_7 = V_0;
		ObjectU5BU5D_t1108656482* L_8 = ___args1;
		int32_t L_9 = V_2;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		String_t* L_12 = Application_ObjectToJSString_m2317112936(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		NullCheck(L_7);
		StringBuilder_Append_m3898090075(L_7, L_12, /*hidden argument*/NULL);
		int32_t L_13 = V_2;
		V_2 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0047:
	{
		int32_t L_14 = V_2;
		int32_t L_15 = V_1;
		if ((((int32_t)L_14) < ((int32_t)L_15)))
		{
			goto IL_0022;
		}
	}
	{
		StringBuilder_t243639308 * L_16 = V_0;
		NullCheck(L_16);
		StringBuilder_Append_m2143093878(L_16, ((int32_t)41), /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_17 = V_0;
		NullCheck(L_17);
		StringBuilder_Append_m2143093878(L_17, ((int32_t)59), /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_18 = V_0;
		NullCheck(L_18);
		String_t* L_19 = StringBuilder_ToString_m350379841(L_18, /*hidden argument*/NULL);
		return L_19;
	}
}
// System.Void UnityEngine.Application::ExternalEval(System.String)
extern Il2CppClass* Char_t2862622538_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Application_ExternalEval_m1886082784_MetadataUsageId;
extern "C"  void Application_ExternalEval_m1886082784 (Il2CppObject * __this /* static, unused */, String_t* ___script0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Application_ExternalEval_m1886082784_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___script0;
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m2979997331(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_0030;
		}
	}
	{
		String_t* L_2 = ___script0;
		String_t* L_3 = ___script0;
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m2979997331(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		Il2CppChar L_5 = String_get_Chars_m3015341861(L_2, ((int32_t)((int32_t)L_4-(int32_t)1)), /*hidden argument*/NULL);
		if ((((int32_t)L_5) == ((int32_t)((int32_t)59))))
		{
			goto IL_0030;
		}
	}
	{
		String_t* L_6 = ___script0;
		Il2CppChar L_7 = ((Il2CppChar)((int32_t)59));
		Il2CppObject * L_8 = Box(Char_t2862622538_il2cpp_TypeInfo_var, &L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m389863537(NULL /*static, unused*/, L_6, L_8, /*hidden argument*/NULL);
		___script0 = L_9;
	}

IL_0030:
	{
		String_t* L_10 = ___script0;
		Application_Internal_ExternalCall_m2589949616(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Application::Internal_ExternalCall(System.String)
extern "C"  void Application_Internal_ExternalCall_m2589949616 (Il2CppObject * __this /* static, unused */, String_t* ___script0, const MethodInfo* method)
{
	typedef void (*Application_Internal_ExternalCall_m2589949616_ftn) (String_t*);
	static Application_Internal_ExternalCall_m2589949616_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_Internal_ExternalCall_m2589949616_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::Internal_ExternalCall(System.String)");
	_il2cpp_icall_func(___script0);
}
// System.String UnityEngine.Application::get_unityVersion()
extern "C"  String_t* Application_get_unityVersion_m3443350436 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*Application_get_unityVersion_m3443350436_ftn) ();
	static Application_get_unityVersion_m3443350436_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_unityVersion_m3443350436_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_unityVersion()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.Application::get_productName()
extern "C"  String_t* Application_get_productName_m4188595931 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*Application_get_productName_m4188595931_ftn) ();
	static Application_get_productName_m4188595931_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_productName_m4188595931_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_productName()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Application::OpenURL(System.String)
extern "C"  void Application_OpenURL_m1861717334 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method)
{
	typedef void (*Application_OpenURL_m1861717334_ftn) (String_t*);
	static Application_OpenURL_m1861717334_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_OpenURL_m1861717334_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::OpenURL(System.String)");
	_il2cpp_icall_func(___url0);
}
// System.Void UnityEngine.Application::set_targetFrameRate(System.Int32)
extern "C"  void Application_set_targetFrameRate_m498658007 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Application_set_targetFrameRate_m498658007_ftn) (int32_t);
	static Application_set_targetFrameRate_m498658007_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_set_targetFrameRate_m498658007_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::set_targetFrameRate(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.Application::CallLogCallback(System.String,System.String,UnityEngine.LogType,System.Boolean)
extern Il2CppClass* Application_t2856536070_il2cpp_TypeInfo_var;
extern const uint32_t Application_CallLogCallback_m419361836_MetadataUsageId;
extern "C"  void Application_CallLogCallback_m419361836 (Il2CppObject * __this /* static, unused */, String_t* ___logString0, String_t* ___stackTrace1, int32_t ___type2, bool ___invokedOnMainThread3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Application_CallLogCallback_m419361836_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LogCallback_t2984951347 * V_0 = NULL;
	LogCallback_t2984951347 * V_1 = NULL;
	{
		bool L_0 = ___invokedOnMainThread3;
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		LogCallback_t2984951347 * L_1 = ((Application_t2856536070_StaticFields*)Application_t2856536070_il2cpp_TypeInfo_var->static_fields)->get_s_LogCallbackHandler_0();
		V_0 = L_1;
		LogCallback_t2984951347 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		LogCallback_t2984951347 * L_3 = V_0;
		String_t* L_4 = ___logString0;
		String_t* L_5 = ___stackTrace1;
		int32_t L_6 = ___type2;
		NullCheck(L_3);
		LogCallback_Invoke_m1886455446(L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
	}

IL_001b:
	{
		LogCallback_t2984951347 * L_7 = ((Application_t2856536070_StaticFields*)Application_t2856536070_il2cpp_TypeInfo_var->static_fields)->get_s_LogCallbackHandlerThreaded_1();
		V_1 = L_7;
		LogCallback_t2984951347 * L_8 = V_1;
		if (!L_8)
		{
			goto IL_0030;
		}
	}
	{
		LogCallback_t2984951347 * L_9 = V_1;
		String_t* L_10 = ___logString0;
		String_t* L_11 = ___stackTrace1;
		int32_t L_12 = ___type2;
		NullCheck(L_9);
		LogCallback_Invoke_m1886455446(L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
	}

IL_0030:
	{
		return;
	}
}
// UnityEngine.NetworkReachability UnityEngine.Application::get_internetReachability()
extern "C"  int32_t Application_get_internetReachability_m379356375 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Application_get_internetReachability_m379356375_ftn) ();
	static Application_get_internetReachability_m379356375_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_internetReachability_m379356375_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_internetReachability()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Application::get_loadedLevel()
extern "C"  int32_t Application_get_loadedLevel_m946446301 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Scene_t1080795294  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Scene_t1080795294  L_0 = SceneManager_GetActiveScene_m3062973092(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Scene_get_buildIndex_m3533090789((&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String UnityEngine.Application::get_loadedLevelName()
extern "C"  String_t* Application_get_loadedLevelName_m953500779 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Scene_t1080795294  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Scene_t1080795294  L_0 = SceneManager_GetActiveScene_m3062973092(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = Scene_get_name_m894591657((&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Application::LoadLevel(System.Int32)
extern "C"  void Application_LoadLevel_m1181471414 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		SceneManager_LoadScene_m2455768283(NULL /*static, unused*/, L_0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Application::LoadLevel(System.String)
extern "C"  void Application_LoadLevel_m2722573885 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		SceneManager_LoadScene_m3907168970(NULL /*static, unused*/, L_0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Application::LoadLevelAdditive(System.Int32)
extern "C"  void Application_LoadLevelAdditive_m3131015586 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		SceneManager_LoadScene_m2455768283(NULL /*static, unused*/, L_0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Application::LoadLevelAdditive(System.String)
extern "C"  void Application_LoadLevelAdditive_m3028901073 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		SceneManager_LoadScene_m3907168970(NULL /*static, unused*/, L_0, 1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.AsyncOperation UnityEngine.Application::LoadLevelAsync(System.String)
extern "C"  AsyncOperation_t3699081103 * Application_LoadLevelAsync_m572913174 (Il2CppObject * __this /* static, unused */, String_t* ___levelName0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___levelName0;
		AsyncOperation_t3699081103 * L_1 = SceneManager_LoadSceneAsync_m1685951617(NULL /*static, unused*/, L_0, 0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.AsyncOperation UnityEngine.Application::LoadLevelAdditiveAsync(System.String)
extern "C"  AsyncOperation_t3699081103 * Application_LoadLevelAdditiveAsync_m381683458 (Il2CppObject * __this /* static, unused */, String_t* ___levelName0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___levelName0;
		AsyncOperation_t3699081103 * L_1 = SceneManager_LoadSceneAsync_m1685951617(NULL /*static, unused*/, L_0, 1, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Application/LogCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LogCallback__ctor_m286543475 (LogCallback_t2984951347 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Application/LogCallback::Invoke(System.String,System.String,UnityEngine.LogType)
extern "C"  void LogCallback_Invoke_m1886455446 (LogCallback_t2984951347 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		LogCallback_Invoke_m1886455446((LogCallback_t2984951347 *)__this->get_prev_9(),___condition0, ___stackTrace1, ___type2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___condition0, ___stackTrace1, ___type2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___condition0, ___stackTrace1, ___type2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___condition0, ___stackTrace1, ___type2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_LogCallback_t2984951347 (LogCallback_t2984951347 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*, char*, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___condition0' to native representation
	char* ____condition0_marshaled = NULL;
	____condition0_marshaled = il2cpp_codegen_marshal_string(___condition0);

	// Marshaling of parameter '___stackTrace1' to native representation
	char* ____stackTrace1_marshaled = NULL;
	____stackTrace1_marshaled = il2cpp_codegen_marshal_string(___stackTrace1);

	// Native function invocation
	il2cppPInvokeFunc(____condition0_marshaled, ____stackTrace1_marshaled, ___type2);

	// Marshaling cleanup of parameter '___condition0' native representation
	il2cpp_codegen_marshal_free(____condition0_marshaled);
	____condition0_marshaled = NULL;

	// Marshaling cleanup of parameter '___stackTrace1' native representation
	il2cpp_codegen_marshal_free(____stackTrace1_marshaled);
	____stackTrace1_marshaled = NULL;

}
// System.IAsyncResult UnityEngine.Application/LogCallback::BeginInvoke(System.String,System.String,UnityEngine.LogType,System.AsyncCallback,System.Object)
extern Il2CppClass* LogType_t4286006228_il2cpp_TypeInfo_var;
extern const uint32_t LogCallback_BeginInvoke_m1640350401_MetadataUsageId;
extern "C"  Il2CppObject * LogCallback_BeginInvoke_m1640350401 (LogCallback_t2984951347 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LogCallback_BeginInvoke_m1640350401_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___condition0;
	__d_args[1] = ___stackTrace1;
	__d_args[2] = Box(LogType_t4286006228_il2cpp_TypeInfo_var, &___type2);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void UnityEngine.Application/LogCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LogCallback_EndInvoke_m2883514883 (LogCallback_t2984951347 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.AssemblyIsEditorAssembly::.ctor()
extern "C"  void AssemblyIsEditorAssembly__ctor_m582741130 (AssemblyIsEditorAssembly_t1696890055 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.AssetBundle::get_mainAsset()
extern "C"  Object_t3071478659 * AssetBundle_get_mainAsset_m3597227404 (AssetBundle_t2070959688 * __this, const MethodInfo* method)
{
	typedef Object_t3071478659 * (*AssetBundle_get_mainAsset_m3597227404_ftn) (AssetBundle_t2070959688 *);
	static AssetBundle_get_mainAsset_m3597227404_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundle_get_mainAsset_m3597227404_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundle::get_mainAsset()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AssetBundle::Unload(System.Boolean)
extern "C"  void AssetBundle_Unload_m913727763 (AssetBundle_t2070959688 * __this, bool ___unloadAllLoadedObjects0, const MethodInfo* method)
{
	typedef void (*AssetBundle_Unload_m913727763_ftn) (AssetBundle_t2070959688 *, bool);
	static AssetBundle_Unload_m913727763_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundle_Unload_m913727763_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundle::Unload(System.Boolean)");
	_il2cpp_icall_func(__this, ___unloadAllLoadedObjects0);
}
// System.Void UnityEngine.AssetBundleCreateRequest::.ctor()
extern "C"  void AssetBundleCreateRequest__ctor_m109192972 (AssetBundleCreateRequest_t1416890373 * __this, const MethodInfo* method)
{
	{
		AsyncOperation__ctor_m162101250(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.AssetBundle UnityEngine.AssetBundleCreateRequest::get_assetBundle()
extern "C"  AssetBundle_t2070959688 * AssetBundleCreateRequest_get_assetBundle_m198975995 (AssetBundleCreateRequest_t1416890373 * __this, const MethodInfo* method)
{
	typedef AssetBundle_t2070959688 * (*AssetBundleCreateRequest_get_assetBundle_m198975995_ftn) (AssetBundleCreateRequest_t1416890373 *);
	static AssetBundleCreateRequest_get_assetBundle_m198975995_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundleCreateRequest_get_assetBundle_m198975995_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundleCreateRequest::get_assetBundle()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AssetBundleCreateRequest::DisableCompatibilityChecks()
extern "C"  void AssetBundleCreateRequest_DisableCompatibilityChecks_m616322731 (AssetBundleCreateRequest_t1416890373 * __this, const MethodInfo* method)
{
	typedef void (*AssetBundleCreateRequest_DisableCompatibilityChecks_m616322731_ftn) (AssetBundleCreateRequest_t1416890373 *);
	static AssetBundleCreateRequest_DisableCompatibilityChecks_m616322731_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundleCreateRequest_DisableCompatibilityChecks_m616322731_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundleCreateRequest::DisableCompatibilityChecks()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AssetBundleRequest::.ctor()
extern "C"  void AssetBundleRequest__ctor_m1736261360 (AssetBundleRequest_t2154290273 * __this, const MethodInfo* method)
{
	{
		AsyncOperation__ctor_m162101250(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.AssetBundleRequest::get_asset()
extern "C"  Object_t3071478659 * AssetBundleRequest_get_asset_m1874521436 (AssetBundleRequest_t2154290273 * __this, const MethodInfo* method)
{
	typedef Object_t3071478659 * (*AssetBundleRequest_get_asset_m1874521436_ftn) (AssetBundleRequest_t2154290273 *);
	static AssetBundleRequest_get_asset_m1874521436_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundleRequest_get_asset_m1874521436_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundleRequest::get_asset()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Object[] UnityEngine.AssetBundleRequest::get_allAssets()
extern "C"  ObjectU5BU5D_t1015136018* AssetBundleRequest_get_allAssets_m2335877586 (AssetBundleRequest_t2154290273 * __this, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t1015136018* (*AssetBundleRequest_get_allAssets_m2335877586_ftn) (AssetBundleRequest_t2154290273 *);
	static AssetBundleRequest_get_allAssets_m2335877586_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundleRequest_get_allAssets_m2335877586_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundleRequest::get_allAssets()");
	return _il2cpp_icall_func(__this);
}
// Conversion methods for marshalling of: UnityEngine.AssetBundleRequest
extern "C" void AssetBundleRequest_t2154290273_marshal_pinvoke(const AssetBundleRequest_t2154290273& unmarshaled, AssetBundleRequest_t2154290273_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void AssetBundleRequest_t2154290273_marshal_pinvoke_back(const AssetBundleRequest_t2154290273_marshaled_pinvoke& marshaled, AssetBundleRequest_t2154290273& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AssetBundleRequest
extern "C" void AssetBundleRequest_t2154290273_marshal_pinvoke_cleanup(AssetBundleRequest_t2154290273_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AssetBundleRequest
extern "C" void AssetBundleRequest_t2154290273_marshal_com(const AssetBundleRequest_t2154290273& unmarshaled, AssetBundleRequest_t2154290273_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void AssetBundleRequest_t2154290273_marshal_com_back(const AssetBundleRequest_t2154290273_marshaled_com& marshaled, AssetBundleRequest_t2154290273& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AssetBundleRequest
extern "C" void AssetBundleRequest_t2154290273_marshal_com_cleanup(AssetBundleRequest_t2154290273_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.AsyncOperation::.ctor()
extern "C"  void AsyncOperation__ctor_m162101250 (AsyncOperation_t3699081103 * __this, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m539393484(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AsyncOperation::InternalDestroy()
extern "C"  void AsyncOperation_InternalDestroy_m838183325 (AsyncOperation_t3699081103 * __this, const MethodInfo* method)
{
	typedef void (*AsyncOperation_InternalDestroy_m838183325_ftn) (AsyncOperation_t3699081103 *);
	static AsyncOperation_InternalDestroy_m838183325_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_InternalDestroy_m838183325_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::InternalDestroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AsyncOperation::Finalize()
extern "C"  void AsyncOperation_Finalize_m1717604640 (AsyncOperation_t3699081103 * __this, const MethodInfo* method)
{
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		AsyncOperation_InternalDestroy_m838183325(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m3027285644(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Boolean UnityEngine.AsyncOperation::get_isDone()
extern "C"  bool AsyncOperation_get_isDone_m2747591837 (AsyncOperation_t3699081103 * __this, const MethodInfo* method)
{
	typedef bool (*AsyncOperation_get_isDone_m2747591837_ftn) (AsyncOperation_t3699081103 *);
	static AsyncOperation_get_isDone_m2747591837_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_get_isDone_m2747591837_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::get_isDone()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.AsyncOperation::get_progress()
extern "C"  float AsyncOperation_get_progress_m2178550628 (AsyncOperation_t3699081103 * __this, const MethodInfo* method)
{
	typedef float (*AsyncOperation_get_progress_m2178550628_ftn) (AsyncOperation_t3699081103 *);
	static AsyncOperation_get_progress_m2178550628_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_get_progress_m2178550628_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::get_progress()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.AsyncOperation::get_priority()
extern "C"  int32_t AsyncOperation_get_priority_m1711502735 (AsyncOperation_t3699081103 * __this, const MethodInfo* method)
{
	typedef int32_t (*AsyncOperation_get_priority_m1711502735_ftn) (AsyncOperation_t3699081103 *);
	static AsyncOperation_get_priority_m1711502735_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_get_priority_m1711502735_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::get_priority()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AsyncOperation::set_priority(System.Int32)
extern "C"  void AsyncOperation_set_priority_m4201963860 (AsyncOperation_t3699081103 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*AsyncOperation_set_priority_m4201963860_ftn) (AsyncOperation_t3699081103 *, int32_t);
	static AsyncOperation_set_priority_m4201963860_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_set_priority_m4201963860_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::set_priority(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.AsyncOperation::get_allowSceneActivation()
extern "C"  bool AsyncOperation_get_allowSceneActivation_m4131883466 (AsyncOperation_t3699081103 * __this, const MethodInfo* method)
{
	typedef bool (*AsyncOperation_get_allowSceneActivation_m4131883466_ftn) (AsyncOperation_t3699081103 *);
	static AsyncOperation_get_allowSceneActivation_m4131883466_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_get_allowSceneActivation_m4131883466_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::get_allowSceneActivation()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AsyncOperation::set_allowSceneActivation(System.Boolean)
extern "C"  void AsyncOperation_set_allowSceneActivation_m2093104207 (AsyncOperation_t3699081103 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*AsyncOperation_set_allowSceneActivation_m2093104207_ftn) (AsyncOperation_t3699081103 *, bool);
	static AsyncOperation_set_allowSceneActivation_m2093104207_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_set_allowSceneActivation_m2093104207_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::set_allowSceneActivation(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// Conversion methods for marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t3699081103_marshal_pinvoke(const AsyncOperation_t3699081103& unmarshaled, AsyncOperation_t3699081103_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void AsyncOperation_t3699081103_marshal_pinvoke_back(const AsyncOperation_t3699081103_marshaled_pinvoke& marshaled, AsyncOperation_t3699081103& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t3699081103_marshal_pinvoke_cleanup(AsyncOperation_t3699081103_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t3699081103_marshal_com(const AsyncOperation_t3699081103& unmarshaled, AsyncOperation_t3699081103_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void AsyncOperation_t3699081103_marshal_com_back(const AsyncOperation_t3699081103_marshaled_com& marshaled, AsyncOperation_t3699081103& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t3699081103_marshal_com_cleanup(AsyncOperation_t3699081103_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.AttributeHelperEngine::.cctor()
extern Il2CppClass* DisallowMultipleComponentU5BU5D_t3749917529_il2cpp_TypeInfo_var;
extern Il2CppClass* AttributeHelperEngine_t4212589506_il2cpp_TypeInfo_var;
extern Il2CppClass* ExecuteInEditModeU5BU5D_t156135824_il2cpp_TypeInfo_var;
extern Il2CppClass* RequireComponentU5BU5D_t968569205_il2cpp_TypeInfo_var;
extern const uint32_t AttributeHelperEngine__cctor_m2686080064_MetadataUsageId;
extern "C"  void AttributeHelperEngine__cctor_m2686080064 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine__cctor_m2686080064_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((AttributeHelperEngine_t4212589506_StaticFields*)AttributeHelperEngine_t4212589506_il2cpp_TypeInfo_var->static_fields)->set__disallowMultipleComponentArray_0(((DisallowMultipleComponentU5BU5D_t3749917529*)SZArrayNew(DisallowMultipleComponentU5BU5D_t3749917529_il2cpp_TypeInfo_var, (uint32_t)1)));
		((AttributeHelperEngine_t4212589506_StaticFields*)AttributeHelperEngine_t4212589506_il2cpp_TypeInfo_var->static_fields)->set__executeInEditModeArray_1(((ExecuteInEditModeU5BU5D_t156135824*)SZArrayNew(ExecuteInEditModeU5BU5D_t156135824_il2cpp_TypeInfo_var, (uint32_t)1)));
		((AttributeHelperEngine_t4212589506_StaticFields*)AttributeHelperEngine_t4212589506_il2cpp_TypeInfo_var->static_fields)->set__requireComponentArray_2(((RequireComponentU5BU5D_t968569205*)SZArrayNew(RequireComponentU5BU5D_t968569205_il2cpp_TypeInfo_var, (uint32_t)1)));
		return;
	}
}
// System.Type UnityEngine.AttributeHelperEngine::GetParentTypeDisallowingMultipleInclusion(System.Type)
extern const Il2CppType* MonoBehaviour_t667441552_0_0_0_var;
extern const Il2CppType* DisallowMultipleComponent_t62111112_0_0_0_var;
extern Il2CppClass* Stack_1_t1666739402_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1__ctor_m3371416491_MethodInfo_var;
extern const MethodInfo* Stack_1_Push_m3613564932_MethodInfo_var;
extern const MethodInfo* Stack_1_Pop_m167231674_MethodInfo_var;
extern const MethodInfo* Stack_1_get_Count_m2811274927_MethodInfo_var;
extern const uint32_t AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m799857845_MetadataUsageId;
extern "C"  Type_t * AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m799857845 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m799857845_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Stack_1_t1666739402 * V_0 = NULL;
	Type_t * V_1 = NULL;
	ObjectU5BU5D_t1108656482* V_2 = NULL;
	int32_t V_3 = 0;
	{
		Stack_1_t1666739402 * L_0 = (Stack_1_t1666739402 *)il2cpp_codegen_object_new(Stack_1_t1666739402_il2cpp_TypeInfo_var);
		Stack_1__ctor_m3371416491(L_0, /*hidden argument*/Stack_1__ctor_m3371416491_MethodInfo_var);
		V_0 = L_0;
		goto IL_001a;
	}

IL_000b:
	{
		Stack_1_t1666739402 * L_1 = V_0;
		Type_t * L_2 = ___type0;
		NullCheck(L_1);
		Stack_1_Push_m3613564932(L_1, L_2, /*hidden argument*/Stack_1_Push_m3613564932_MethodInfo_var);
		Type_t * L_3 = ___type0;
		NullCheck(L_3);
		Type_t * L_4 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_3);
		___type0 = L_4;
	}

IL_001a:
	{
		Type_t * L_5 = ___type0;
		if (!L_5)
		{
			goto IL_0030;
		}
	}
	{
		Type_t * L_6 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t667441552_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7))))
		{
			goto IL_000b;
		}
	}

IL_0030:
	{
		V_1 = (Type_t *)NULL;
		goto IL_005c;
	}

IL_0037:
	{
		Stack_1_t1666739402 * L_8 = V_0;
		NullCheck(L_8);
		Type_t * L_9 = Stack_1_Pop_m167231674(L_8, /*hidden argument*/Stack_1_Pop_m167231674_MethodInfo_var);
		V_1 = L_9;
		Type_t * L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_11 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(DisallowMultipleComponent_t62111112_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_10);
		ObjectU5BU5D_t1108656482* L_12 = VirtFuncInvoker2< ObjectU5BU5D_t1108656482*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_10, L_11, (bool)0);
		V_2 = L_12;
		ObjectU5BU5D_t1108656482* L_13 = V_2;
		NullCheck(L_13);
		V_3 = (((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length))));
		int32_t L_14 = V_3;
		if (!L_14)
		{
			goto IL_005c;
		}
	}
	{
		Type_t * L_15 = V_1;
		return L_15;
	}

IL_005c:
	{
		Stack_1_t1666739402 * L_16 = V_0;
		NullCheck(L_16);
		int32_t L_17 = Stack_1_get_Count_m2811274927(L_16, /*hidden argument*/Stack_1_get_Count_m2811274927_MethodInfo_var);
		if ((((int32_t)L_17) > ((int32_t)0)))
		{
			goto IL_0037;
		}
	}
	{
		return (Type_t *)NULL;
	}
}
// System.Type[] UnityEngine.AttributeHelperEngine::GetRequiredComponents(System.Type)
extern const Il2CppType* RequireComponent_t1687166108_0_0_0_var;
extern const Il2CppType* MonoBehaviour_t667441552_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* RequireComponentU5BU5D_t968569205_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t4231331326_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3115358433_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2809719249_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m1108445279_MethodInfo_var;
extern const uint32_t AttributeHelperEngine_GetRequiredComponents_m2666115467_MetadataUsageId;
extern "C"  TypeU5BU5D_t3339007067* AttributeHelperEngine_GetRequiredComponents_m2666115467 (Il2CppObject * __this /* static, unused */, Type_t * ___klass0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine_GetRequiredComponents_m2666115467_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t4231331326 * V_0 = NULL;
	RequireComponentU5BU5D_t968569205* V_1 = NULL;
	Type_t * V_2 = NULL;
	RequireComponent_t1687166108 * V_3 = NULL;
	RequireComponentU5BU5D_t968569205* V_4 = NULL;
	int32_t V_5 = 0;
	TypeU5BU5D_t3339007067* V_6 = NULL;
	{
		V_0 = (List_1_t4231331326 *)NULL;
		goto IL_00e0;
	}

IL_0007:
	{
		Type_t * L_0 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(RequireComponent_t1687166108_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectU5BU5D_t1108656482* L_2 = VirtFuncInvoker2< ObjectU5BU5D_t1108656482*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_0, L_1, (bool)0);
		V_1 = ((RequireComponentU5BU5D_t968569205*)Castclass(L_2, RequireComponentU5BU5D_t968569205_il2cpp_TypeInfo_var));
		Type_t * L_3 = ___klass0;
		NullCheck(L_3);
		Type_t * L_4 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_3);
		V_2 = L_4;
		RequireComponentU5BU5D_t968569205* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_00d2;
	}

IL_0030:
	{
		RequireComponentU5BU5D_t968569205* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		RequireComponent_t1687166108 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		List_1_t4231331326 * L_10 = V_0;
		if (L_10)
		{
			goto IL_007b;
		}
	}
	{
		RequireComponentU5BU5D_t968569205* L_11 = V_1;
		NullCheck(L_11);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length))))) == ((uint32_t)1))))
		{
			goto IL_007b;
		}
	}
	{
		Type_t * L_12 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_13 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t667441552_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_12) == ((Il2CppObject*)(Type_t *)L_13))))
		{
			goto IL_007b;
		}
	}
	{
		TypeU5BU5D_t3339007067* L_14 = ((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)3));
		RequireComponent_t1687166108 * L_15 = V_3;
		NullCheck(L_15);
		Type_t * L_16 = L_15->get_m_Type0_0();
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 0);
		ArrayElementTypeCheck (L_14, L_16);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_16);
		TypeU5BU5D_t3339007067* L_17 = L_14;
		RequireComponent_t1687166108 * L_18 = V_3;
		NullCheck(L_18);
		Type_t * L_19 = L_18->get_m_Type1_1();
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 1);
		ArrayElementTypeCheck (L_17, L_19);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_19);
		TypeU5BU5D_t3339007067* L_20 = L_17;
		RequireComponent_t1687166108 * L_21 = V_3;
		NullCheck(L_21);
		Type_t * L_22 = L_21->get_m_Type2_2();
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 2);
		ArrayElementTypeCheck (L_20, L_22);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_22);
		V_6 = L_20;
		TypeU5BU5D_t3339007067* L_23 = V_6;
		return L_23;
	}

IL_007b:
	{
		List_1_t4231331326 * L_24 = V_0;
		if (L_24)
		{
			goto IL_0087;
		}
	}
	{
		List_1_t4231331326 * L_25 = (List_1_t4231331326 *)il2cpp_codegen_object_new(List_1_t4231331326_il2cpp_TypeInfo_var);
		List_1__ctor_m3115358433(L_25, /*hidden argument*/List_1__ctor_m3115358433_MethodInfo_var);
		V_0 = L_25;
	}

IL_0087:
	{
		RequireComponent_t1687166108 * L_26 = V_3;
		NullCheck(L_26);
		Type_t * L_27 = L_26->get_m_Type0_0();
		if (!L_27)
		{
			goto IL_009e;
		}
	}
	{
		List_1_t4231331326 * L_28 = V_0;
		RequireComponent_t1687166108 * L_29 = V_3;
		NullCheck(L_29);
		Type_t * L_30 = L_29->get_m_Type0_0();
		NullCheck(L_28);
		List_1_Add_m2809719249(L_28, L_30, /*hidden argument*/List_1_Add_m2809719249_MethodInfo_var);
	}

IL_009e:
	{
		RequireComponent_t1687166108 * L_31 = V_3;
		NullCheck(L_31);
		Type_t * L_32 = L_31->get_m_Type1_1();
		if (!L_32)
		{
			goto IL_00b5;
		}
	}
	{
		List_1_t4231331326 * L_33 = V_0;
		RequireComponent_t1687166108 * L_34 = V_3;
		NullCheck(L_34);
		Type_t * L_35 = L_34->get_m_Type1_1();
		NullCheck(L_33);
		List_1_Add_m2809719249(L_33, L_35, /*hidden argument*/List_1_Add_m2809719249_MethodInfo_var);
	}

IL_00b5:
	{
		RequireComponent_t1687166108 * L_36 = V_3;
		NullCheck(L_36);
		Type_t * L_37 = L_36->get_m_Type2_2();
		if (!L_37)
		{
			goto IL_00cc;
		}
	}
	{
		List_1_t4231331326 * L_38 = V_0;
		RequireComponent_t1687166108 * L_39 = V_3;
		NullCheck(L_39);
		Type_t * L_40 = L_39->get_m_Type2_2();
		NullCheck(L_38);
		List_1_Add_m2809719249(L_38, L_40, /*hidden argument*/List_1_Add_m2809719249_MethodInfo_var);
	}

IL_00cc:
	{
		int32_t L_41 = V_5;
		V_5 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_00d2:
	{
		int32_t L_42 = V_5;
		RequireComponentU5BU5D_t968569205* L_43 = V_4;
		NullCheck(L_43);
		if ((((int32_t)L_42) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_43)->max_length)))))))
		{
			goto IL_0030;
		}
	}
	{
		Type_t * L_44 = V_2;
		___klass0 = L_44;
	}

IL_00e0:
	{
		Type_t * L_45 = ___klass0;
		if (!L_45)
		{
			goto IL_00f6;
		}
	}
	{
		Type_t * L_46 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_47 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t667441552_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_46) == ((Il2CppObject*)(Type_t *)L_47))))
		{
			goto IL_0007;
		}
	}

IL_00f6:
	{
		List_1_t4231331326 * L_48 = V_0;
		if (L_48)
		{
			goto IL_00fe;
		}
	}
	{
		return (TypeU5BU5D_t3339007067*)NULL;
	}

IL_00fe:
	{
		List_1_t4231331326 * L_49 = V_0;
		NullCheck(L_49);
		TypeU5BU5D_t3339007067* L_50 = List_1_ToArray_m1108445279(L_49, /*hidden argument*/List_1_ToArray_m1108445279_MethodInfo_var);
		return L_50;
	}
}
// System.Boolean UnityEngine.AttributeHelperEngine::CheckIsEditorScript(System.Type)
extern const Il2CppType* ExecuteInEditMode_t3132250205_0_0_0_var;
extern const Il2CppType* MonoBehaviour_t667441552_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t AttributeHelperEngine_CheckIsEditorScript_m2105533310_MetadataUsageId;
extern "C"  bool AttributeHelperEngine_CheckIsEditorScript_m2105533310 (Il2CppObject * __this /* static, unused */, Type_t * ___klass0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine_CheckIsEditorScript_m2105533310_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t1108656482* V_0 = NULL;
	int32_t V_1 = 0;
	{
		goto IL_002b;
	}

IL_0005:
	{
		Type_t * L_0 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(ExecuteInEditMode_t3132250205_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectU5BU5D_t1108656482* L_2 = VirtFuncInvoker2< ObjectU5BU5D_t1108656482*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_0, L_1, (bool)0);
		V_0 = L_2;
		ObjectU5BU5D_t1108656482* L_3 = V_0;
		NullCheck(L_3);
		V_1 = (((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))));
		int32_t L_4 = V_1;
		if (!L_4)
		{
			goto IL_0023;
		}
	}
	{
		return (bool)1;
	}

IL_0023:
	{
		Type_t * L_5 = ___klass0;
		NullCheck(L_5);
		Type_t * L_6 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_5);
		___klass0 = L_6;
	}

IL_002b:
	{
		Type_t * L_7 = ___klass0;
		if (!L_7)
		{
			goto IL_0041;
		}
	}
	{
		Type_t * L_8 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t667441552_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_8) == ((Il2CppObject*)(Type_t *)L_9))))
		{
			goto IL_0005;
		}
	}

IL_0041:
	{
		return (bool)0;
	}
}
// System.Single UnityEngine.AudioClip::get_length()
extern "C"  float AudioClip_get_length_m1976537364 (AudioClip_t794140988 * __this, const MethodInfo* method)
{
	typedef float (*AudioClip_get_length_m1976537364_ftn) (AudioClip_t794140988 *);
	static AudioClip_get_length_m1976537364_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioClip_get_length_m1976537364_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioClip::get_length()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.AudioClip::get_samples()
extern "C"  int32_t AudioClip_get_samples_m1244947761 (AudioClip_t794140988 * __this, const MethodInfo* method)
{
	typedef int32_t (*AudioClip_get_samples_m1244947761_ftn) (AudioClip_t794140988 *);
	static AudioClip_get_samples_m1244947761_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioClip_get_samples_m1244947761_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioClip::get_samples()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.AudioClip::get_channels()
extern "C"  int32_t AudioClip_get_channels_m2158838602 (AudioClip_t794140988 * __this, const MethodInfo* method)
{
	typedef int32_t (*AudioClip_get_channels_m2158838602_ftn) (AudioClip_t794140988 *);
	static AudioClip_get_channels_m2158838602_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioClip_get_channels_m2158838602_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioClip::get_channels()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.AudioClip::get_frequency()
extern "C"  int32_t AudioClip_get_frequency_m3651052548 (AudioClip_t794140988 * __this, const MethodInfo* method)
{
	typedef int32_t (*AudioClip_get_frequency_m3651052548_ftn) (AudioClip_t794140988 *);
	static AudioClip_get_frequency_m3651052548_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioClip_get_frequency_m3651052548_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioClip::get_frequency()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.AudioClip::GetData(System.Single[],System.Int32)
extern "C"  bool AudioClip_GetData_m3989861477 (AudioClip_t794140988 * __this, SingleU5BU5D_t2316563989* ___data0, int32_t ___offsetSamples1, const MethodInfo* method)
{
	typedef bool (*AudioClip_GetData_m3989861477_ftn) (AudioClip_t794140988 *, SingleU5BU5D_t2316563989*, int32_t);
	static AudioClip_GetData_m3989861477_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioClip_GetData_m3989861477_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioClip::GetData(System.Single[],System.Int32)");
	return _il2cpp_icall_func(__this, ___data0, ___offsetSamples1);
}
// System.Boolean UnityEngine.AudioClip::SetData(System.Single[],System.Int32)
extern "C"  bool AudioClip_SetData_m3652083313 (AudioClip_t794140988 * __this, SingleU5BU5D_t2316563989* ___data0, int32_t ___offsetSamples1, const MethodInfo* method)
{
	typedef bool (*AudioClip_SetData_m3652083313_ftn) (AudioClip_t794140988 *, SingleU5BU5D_t2316563989*, int32_t);
	static AudioClip_SetData_m3652083313_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioClip_SetData_m3652083313_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioClip::SetData(System.Single[],System.Int32)");
	return _il2cpp_icall_func(__this, ___data0, ___offsetSamples1);
}
// UnityEngine.AudioClip UnityEngine.AudioClip::Create(System.String,System.Int32,System.Int32,System.Int32,System.Boolean,System.Boolean)
extern "C"  AudioClip_t794140988 * AudioClip_Create_m3791795358 (Il2CppObject * __this /* static, unused */, String_t* ___name0, int32_t ___lengthSamples1, int32_t ___channels2, int32_t ___frequency3, bool ____3D4, bool ___stream5, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = ___lengthSamples1;
		int32_t L_2 = ___channels2;
		int32_t L_3 = ___frequency3;
		bool L_4 = ___stream5;
		AudioClip_t794140988 * L_5 = AudioClip_Create_m948314367(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.AudioClip UnityEngine.AudioClip::Create(System.String,System.Int32,System.Int32,System.Int32,System.Boolean)
extern "C"  AudioClip_t794140988 * AudioClip_Create_m948314367 (Il2CppObject * __this /* static, unused */, String_t* ___name0, int32_t ___lengthSamples1, int32_t ___channels2, int32_t ___frequency3, bool ___stream4, const MethodInfo* method)
{
	AudioClip_t794140988 * V_0 = NULL;
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = ___lengthSamples1;
		int32_t L_2 = ___channels2;
		int32_t L_3 = ___frequency3;
		bool L_4 = ___stream4;
		AudioClip_t794140988 * L_5 = AudioClip_Create_m3999204423(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, (PCMReaderCallback_t83861602 *)NULL, (PCMSetPositionCallback_t4244274966 *)NULL, /*hidden argument*/NULL);
		V_0 = L_5;
		AudioClip_t794140988 * L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.AudioClip UnityEngine.AudioClip::Create(System.String,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.AudioClip/PCMReaderCallback,UnityEngine.AudioClip/PCMSetPositionCallback)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppClass* PCMReaderCallback_t83861602_il2cpp_TypeInfo_var;
extern Il2CppClass* PCMSetPositionCallback_t4244274966_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1943756267;
extern Il2CppCodeGenString* _stringLiteral1258110904;
extern Il2CppCodeGenString* _stringLiteral434748158;
extern const uint32_t AudioClip_Create_m3999204423_MetadataUsageId;
extern "C"  AudioClip_t794140988 * AudioClip_Create_m3999204423 (Il2CppObject * __this /* static, unused */, String_t* ___name0, int32_t ___lengthSamples1, int32_t ___channels2, int32_t ___frequency3, bool ___stream4, PCMReaderCallback_t83861602 * ___pcmreadercallback5, PCMSetPositionCallback_t4244274966 * ___pcmsetpositioncallback6, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AudioClip_Create_m3999204423_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AudioClip_t794140988 * V_0 = NULL;
	{
		String_t* L_0 = ___name0;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		NullReferenceException_t1441619295 * L_1 = (NullReferenceException_t1441619295 *)il2cpp_codegen_object_new(NullReferenceException_t1441619295_il2cpp_TypeInfo_var);
		NullReferenceException__ctor_m622546858(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000c:
	{
		int32_t L_2 = ___lengthSamples1;
		if ((((int32_t)L_2) > ((int32_t)0)))
		{
			goto IL_001e;
		}
	}
	{
		ArgumentException_t928607144 * L_3 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_3, _stringLiteral1943756267, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		int32_t L_4 = ___channels2;
		if ((((int32_t)L_4) > ((int32_t)0)))
		{
			goto IL_0030;
		}
	}
	{
		ArgumentException_t928607144 * L_5 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_5, _stringLiteral1258110904, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0030:
	{
		int32_t L_6 = ___frequency3;
		if ((((int32_t)L_6) > ((int32_t)0)))
		{
			goto IL_0042;
		}
	}
	{
		ArgumentException_t928607144 * L_7 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_7, _stringLiteral434748158, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0042:
	{
		AudioClip_t794140988 * L_8 = AudioClip_Construct_Internal_m4167029778(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_8;
		PCMReaderCallback_t83861602 * L_9 = ___pcmreadercallback5;
		if (!L_9)
		{
			goto IL_0067;
		}
	}
	{
		AudioClip_t794140988 * L_10 = V_0;
		AudioClip_t794140988 * L_11 = L_10;
		NullCheck(L_11);
		PCMReaderCallback_t83861602 * L_12 = L_11->get_m_PCMReaderCallback_2();
		PCMReaderCallback_t83861602 * L_13 = ___pcmreadercallback5;
		Delegate_t3310234105 * L_14 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		NullCheck(L_11);
		L_11->set_m_PCMReaderCallback_2(((PCMReaderCallback_t83861602 *)CastclassSealed(L_14, PCMReaderCallback_t83861602_il2cpp_TypeInfo_var)));
	}

IL_0067:
	{
		PCMSetPositionCallback_t4244274966 * L_15 = ___pcmsetpositioncallback6;
		if (!L_15)
		{
			goto IL_0086;
		}
	}
	{
		AudioClip_t794140988 * L_16 = V_0;
		AudioClip_t794140988 * L_17 = L_16;
		NullCheck(L_17);
		PCMSetPositionCallback_t4244274966 * L_18 = L_17->get_m_PCMSetPositionCallback_3();
		PCMSetPositionCallback_t4244274966 * L_19 = ___pcmsetpositioncallback6;
		Delegate_t3310234105 * L_20 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		NullCheck(L_17);
		L_17->set_m_PCMSetPositionCallback_3(((PCMSetPositionCallback_t4244274966 *)CastclassSealed(L_20, PCMSetPositionCallback_t4244274966_il2cpp_TypeInfo_var)));
	}

IL_0086:
	{
		AudioClip_t794140988 * L_21 = V_0;
		String_t* L_22 = ___name0;
		int32_t L_23 = ___lengthSamples1;
		int32_t L_24 = ___channels2;
		int32_t L_25 = ___frequency3;
		bool L_26 = ___stream4;
		NullCheck(L_21);
		AudioClip_Init_Internal_m3875816683(L_21, L_22, L_23, L_24, L_25, L_26, /*hidden argument*/NULL);
		AudioClip_t794140988 * L_27 = V_0;
		return L_27;
	}
}
// System.Void UnityEngine.AudioClip::InvokePCMReaderCallback_Internal(System.Single[])
extern "C"  void AudioClip_InvokePCMReaderCallback_Internal_m963695910 (AudioClip_t794140988 * __this, SingleU5BU5D_t2316563989* ___data0, const MethodInfo* method)
{
	{
		PCMReaderCallback_t83861602 * L_0 = __this->get_m_PCMReaderCallback_2();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		PCMReaderCallback_t83861602 * L_1 = __this->get_m_PCMReaderCallback_2();
		SingleU5BU5D_t2316563989* L_2 = ___data0;
		NullCheck(L_1);
		PCMReaderCallback_Invoke_m2982892301(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.AudioClip::InvokePCMSetPositionCallback_Internal(System.Int32)
extern "C"  void AudioClip_InvokePCMSetPositionCallback_Internal_m194702160 (AudioClip_t794140988 * __this, int32_t ___position0, const MethodInfo* method)
{
	{
		PCMSetPositionCallback_t4244274966 * L_0 = __this->get_m_PCMSetPositionCallback_3();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		PCMSetPositionCallback_t4244274966 * L_1 = __this->get_m_PCMSetPositionCallback_3();
		int32_t L_2 = ___position0;
		NullCheck(L_1);
		PCMSetPositionCallback_Invoke_m1757186207(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// UnityEngine.AudioClip UnityEngine.AudioClip::Construct_Internal()
extern "C"  AudioClip_t794140988 * AudioClip_Construct_Internal_m4167029778 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef AudioClip_t794140988 * (*AudioClip_Construct_Internal_m4167029778_ftn) ();
	static AudioClip_Construct_Internal_m4167029778_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioClip_Construct_Internal_m4167029778_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioClip::Construct_Internal()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.AudioClip::Init_Internal(System.String,System.Int32,System.Int32,System.Int32,System.Boolean)
extern "C"  void AudioClip_Init_Internal_m3875816683 (AudioClip_t794140988 * __this, String_t* ___name0, int32_t ___lengthSamples1, int32_t ___channels2, int32_t ___frequency3, bool ___stream4, const MethodInfo* method)
{
	typedef void (*AudioClip_Init_Internal_m3875816683_ftn) (AudioClip_t794140988 *, String_t*, int32_t, int32_t, int32_t, bool);
	static AudioClip_Init_Internal_m3875816683_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioClip_Init_Internal_m3875816683_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioClip::Init_Internal(System.String,System.Int32,System.Int32,System.Int32,System.Boolean)");
	_il2cpp_icall_func(__this, ___name0, ___lengthSamples1, ___channels2, ___frequency3, ___stream4);
}
// System.Void UnityEngine.AudioClip/PCMReaderCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void PCMReaderCallback__ctor_m2350128290 (PCMReaderCallback_t83861602 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.AudioClip/PCMReaderCallback::Invoke(System.Single[])
extern "C"  void PCMReaderCallback_Invoke_m2982892301 (PCMReaderCallback_t83861602 * __this, SingleU5BU5D_t2316563989* ___data0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		PCMReaderCallback_Invoke_m2982892301((PCMReaderCallback_t83861602 *)__this->get_prev_9(),___data0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, SingleU5BU5D_t2316563989* ___data0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___data0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, SingleU5BU5D_t2316563989* ___data0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___data0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___data0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_PCMReaderCallback_t83861602 (PCMReaderCallback_t83861602 * __this, SingleU5BU5D_t2316563989* ___data0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(float*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___data0' to native representation
	float* ____data0_marshaled = NULL;
	____data0_marshaled = il2cpp_codegen_marshal_array<float>((Il2CppCodeGenArray*)___data0);

	// Native function invocation
	il2cppPInvokeFunc(____data0_marshaled);

}
// System.IAsyncResult UnityEngine.AudioClip/PCMReaderCallback::BeginInvoke(System.Single[],System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * PCMReaderCallback_BeginInvoke_m3711541228 (PCMReaderCallback_t83861602 * __this, SingleU5BU5D_t2316563989* ___data0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___data0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.AudioClip/PCMReaderCallback::EndInvoke(System.IAsyncResult)
extern "C"  void PCMReaderCallback_EndInvoke_m1134768050 (PCMReaderCallback_t83861602 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void PCMSetPositionCallback__ctor_m2986500852 (PCMSetPositionCallback_t4244274966 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::Invoke(System.Int32)
extern "C"  void PCMSetPositionCallback_Invoke_m1757186207 (PCMSetPositionCallback_t4244274966 * __this, int32_t ___position0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		PCMSetPositionCallback_Invoke_m1757186207((PCMSetPositionCallback_t4244274966 *)__this->get_prev_9(),___position0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___position0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___position0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___position0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___position0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_PCMSetPositionCallback_t4244274966 (PCMSetPositionCallback_t4244274966 * __this, int32_t ___position0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___position0);

}
// System.IAsyncResult UnityEngine.AudioClip/PCMSetPositionCallback::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const uint32_t PCMSetPositionCallback_BeginInvoke_m3959339032_MetadataUsageId;
extern "C"  Il2CppObject * PCMSetPositionCallback_BeginInvoke_m3959339032 (PCMSetPositionCallback_t4244274966 * __this, int32_t ___position0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PCMSetPositionCallback_BeginInvoke_m3959339032_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &___position0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::EndInvoke(System.IAsyncResult)
extern "C"  void PCMSetPositionCallback_EndInvoke_m239431428 (PCMSetPositionCallback_t4244274966 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.AudioListener::set_volume(System.Single)
extern "C"  void AudioListener_set_volume_m1072709503 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*AudioListener_set_volume_m1072709503_ftn) (float);
	static AudioListener_set_volume_m1072709503_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioListener_set_volume_m1072709503_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioListener::set_volume(System.Single)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.AudioSettings::InvokeOnAudioConfigurationChanged(System.Boolean)
extern Il2CppClass* AudioSettings_t3774206607_il2cpp_TypeInfo_var;
extern const uint32_t AudioSettings_InvokeOnAudioConfigurationChanged_m1851666322_MetadataUsageId;
extern "C"  void AudioSettings_InvokeOnAudioConfigurationChanged_m1851666322 (Il2CppObject * __this /* static, unused */, bool ___deviceWasChanged0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AudioSettings_InvokeOnAudioConfigurationChanged_m1851666322_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AudioConfigurationChangeHandler_t1377657005 * L_0 = ((AudioSettings_t3774206607_StaticFields*)AudioSettings_t3774206607_il2cpp_TypeInfo_var->static_fields)->get_OnAudioConfigurationChanged_0();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		AudioConfigurationChangeHandler_t1377657005 * L_1 = ((AudioSettings_t3774206607_StaticFields*)AudioSettings_t3774206607_il2cpp_TypeInfo_var->static_fields)->get_OnAudioConfigurationChanged_0();
		bool L_2 = ___deviceWasChanged0;
		NullCheck(L_1);
		AudioConfigurationChangeHandler_Invoke_m3047533822(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void AudioConfigurationChangeHandler__ctor_m2273531181 (AudioConfigurationChangeHandler_t1377657005 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::Invoke(System.Boolean)
extern "C"  void AudioConfigurationChangeHandler_Invoke_m3047533822 (AudioConfigurationChangeHandler_t1377657005 * __this, bool ___deviceWasChanged0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		AudioConfigurationChangeHandler_Invoke_m3047533822((AudioConfigurationChangeHandler_t1377657005 *)__this->get_prev_9(),___deviceWasChanged0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___deviceWasChanged0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___deviceWasChanged0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___deviceWasChanged0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___deviceWasChanged0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t1377657005 (AudioConfigurationChangeHandler_t1377657005 * __this, bool ___deviceWasChanged0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___deviceWasChanged0);

}
// System.IAsyncResult UnityEngine.AudioSettings/AudioConfigurationChangeHandler::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t AudioConfigurationChangeHandler_BeginInvoke_m194625699_MetadataUsageId;
extern "C"  Il2CppObject * AudioConfigurationChangeHandler_BeginInvoke_m194625699 (AudioConfigurationChangeHandler_t1377657005 * __this, bool ___deviceWasChanged0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AudioConfigurationChangeHandler_BeginInvoke_m194625699_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &___deviceWasChanged0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::EndInvoke(System.IAsyncResult)
extern "C"  void AudioConfigurationChangeHandler_EndInvoke_m218260925 (AudioConfigurationChangeHandler_t1377657005 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Single UnityEngine.AudioSource::get_volume()
extern "C"  float AudioSource_get_volume_m2334326995 (AudioSource_t1740077639 * __this, const MethodInfo* method)
{
	typedef float (*AudioSource_get_volume_m2334326995_ftn) (AudioSource_t1740077639 *);
	static AudioSource_get_volume_m2334326995_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_get_volume_m2334326995_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::get_volume()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioSource::set_volume(System.Single)
extern "C"  void AudioSource_set_volume_m1410546616 (AudioSource_t1740077639 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*AudioSource_set_volume_m1410546616_ftn) (AudioSource_t1740077639 *, float);
	static AudioSource_set_volume_m1410546616_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_volume_m1410546616_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_volume(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.AudioSource::get_pitch()
extern "C"  float AudioSource_get_pitch_m3729473033 (AudioSource_t1740077639 * __this, const MethodInfo* method)
{
	typedef float (*AudioSource_get_pitch_m3729473033_ftn) (AudioSource_t1740077639 *);
	static AudioSource_get_pitch_m3729473033_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_get_pitch_m3729473033_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::get_pitch()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioSource::set_pitch(System.Single)
extern "C"  void AudioSource_set_pitch_m1518407234 (AudioSource_t1740077639 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*AudioSource_set_pitch_m1518407234_ftn) (AudioSource_t1740077639 *, float);
	static AudioSource_set_pitch_m1518407234_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_pitch_m1518407234_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_pitch(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.AudioSource::get_time()
extern "C"  float AudioSource_get_time_m4113938886 (AudioSource_t1740077639 * __this, const MethodInfo* method)
{
	typedef float (*AudioSource_get_time_m4113938886_ftn) (AudioSource_t1740077639 *);
	static AudioSource_get_time_m4113938886_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_get_time_m4113938886_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::get_time()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioSource::set_time(System.Single)
extern "C"  void AudioSource_set_time_m264562085 (AudioSource_t1740077639 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*AudioSource_set_time_m264562085_ftn) (AudioSource_t1740077639 *, float);
	static AudioSource_set_time_m264562085_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_time_m264562085_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_time(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.AudioClip UnityEngine.AudioSource::get_clip()
extern "C"  AudioClip_t794140988 * AudioSource_get_clip_m2410835857 (AudioSource_t1740077639 * __this, const MethodInfo* method)
{
	typedef AudioClip_t794140988 * (*AudioSource_get_clip_m2410835857_ftn) (AudioSource_t1740077639 *);
	static AudioSource_get_clip_m2410835857_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_get_clip_m2410835857_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::get_clip()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)
extern "C"  void AudioSource_set_clip_m19502010 (AudioSource_t1740077639 * __this, AudioClip_t794140988 * ___value0, const MethodInfo* method)
{
	typedef void (*AudioSource_set_clip_m19502010_ftn) (AudioSource_t1740077639 *, AudioClip_t794140988 *);
	static AudioSource_set_clip_m19502010_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_clip_m19502010_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.AudioSource::Play(System.UInt64)
extern "C"  void AudioSource_Play_m3374467819 (AudioSource_t1740077639 * __this, uint64_t ___delay0, const MethodInfo* method)
{
	typedef void (*AudioSource_Play_m3374467819_ftn) (AudioSource_t1740077639 *, uint64_t);
	static AudioSource_Play_m3374467819_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_Play_m3374467819_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::Play(System.UInt64)");
	_il2cpp_icall_func(__this, ___delay0);
}
// System.Void UnityEngine.AudioSource::Play()
extern "C"  void AudioSource_Play_m1360558992 (AudioSource_t1740077639 * __this, const MethodInfo* method)
{
	uint64_t V_0 = 0;
	{
		V_0 = (((int64_t)((int64_t)0)));
		uint64_t L_0 = V_0;
		AudioSource_Play_m3374467819(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AudioSource::Stop()
extern "C"  void AudioSource_Stop_m1454243038 (AudioSource_t1740077639 * __this, const MethodInfo* method)
{
	typedef void (*AudioSource_Stop_m1454243038_ftn) (AudioSource_t1740077639 *);
	static AudioSource_Stop_m1454243038_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_Stop_m1454243038_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::Stop()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioSource::Pause()
extern "C"  void AudioSource_Pause_m3226052732 (AudioSource_t1740077639 * __this, const MethodInfo* method)
{
	{
		AudioSource_INTERNAL_CALL_Pause_m3343215099(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AudioSource::INTERNAL_CALL_Pause(UnityEngine.AudioSource)
extern "C"  void AudioSource_INTERNAL_CALL_Pause_m3343215099 (Il2CppObject * __this /* static, unused */, AudioSource_t1740077639 * ___self0, const MethodInfo* method)
{
	typedef void (*AudioSource_INTERNAL_CALL_Pause_m3343215099_ftn) (AudioSource_t1740077639 *);
	static AudioSource_INTERNAL_CALL_Pause_m3343215099_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_INTERNAL_CALL_Pause_m3343215099_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::INTERNAL_CALL_Pause(UnityEngine.AudioSource)");
	_il2cpp_icall_func(___self0);
}
// System.Boolean UnityEngine.AudioSource::get_isPlaying()
extern "C"  bool AudioSource_get_isPlaying_m4213444423 (AudioSource_t1740077639 * __this, const MethodInfo* method)
{
	typedef bool (*AudioSource_get_isPlaying_m4213444423_ftn) (AudioSource_t1740077639 *);
	static AudioSource_get_isPlaying_m4213444423_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_get_isPlaying_m4213444423_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::get_isPlaying()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioSource::PlayOneShot(UnityEngine.AudioClip,System.Single)
extern "C"  void AudioSource_PlayOneShot_m823779350 (AudioSource_t1740077639 * __this, AudioClip_t794140988 * ___clip0, float ___volumeScale1, const MethodInfo* method)
{
	typedef void (*AudioSource_PlayOneShot_m823779350_ftn) (AudioSource_t1740077639 *, AudioClip_t794140988 *, float);
	static AudioSource_PlayOneShot_m823779350_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_PlayOneShot_m823779350_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::PlayOneShot(UnityEngine.AudioClip,System.Single)");
	_il2cpp_icall_func(__this, ___clip0, ___volumeScale1);
}
// System.Void UnityEngine.AudioSource::PlayOneShot(UnityEngine.AudioClip)
extern "C"  void AudioSource_PlayOneShot_m1217449713 (AudioSource_t1740077639 * __this, AudioClip_t794140988 * ___clip0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (1.0f);
		AudioClip_t794140988 * L_0 = ___clip0;
		float L_1 = V_0;
		AudioSource_PlayOneShot_m823779350(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AudioSource::PlayClipAtPoint(UnityEngine.AudioClip,UnityEngine.Vector3,System.Single)
extern const Il2CppType* AudioSource_t1740077639_0_0_0_var;
extern Il2CppClass* GameObject_t3674682005_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* AudioSource_t1740077639_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral223576394;
extern const uint32_t AudioSource_PlayClipAtPoint_m2724275676_MetadataUsageId;
extern "C"  void AudioSource_PlayClipAtPoint_m2724275676 (Il2CppObject * __this /* static, unused */, AudioClip_t794140988 * ___clip0, Vector3_t4282066566  ___position1, float ___volume2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AudioSource_PlayClipAtPoint_m2724275676_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	AudioSource_t1740077639 * V_1 = NULL;
	float G_B2_0 = 0.0f;
	GameObject_t3674682005 * G_B2_1 = NULL;
	float G_B1_0 = 0.0f;
	GameObject_t3674682005 * G_B1_1 = NULL;
	float G_B3_0 = 0.0f;
	float G_B3_1 = 0.0f;
	GameObject_t3674682005 * G_B3_2 = NULL;
	{
		GameObject_t3674682005 * L_0 = (GameObject_t3674682005 *)il2cpp_codegen_object_new(GameObject_t3674682005_il2cpp_TypeInfo_var);
		GameObject__ctor_m3920833606(L_0, _stringLiteral223576394, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t3674682005 * L_1 = V_0;
		NullCheck(L_1);
		Transform_t1659122786 * L_2 = GameObject_get_transform_m1278640159(L_1, /*hidden argument*/NULL);
		Vector3_t4282066566  L_3 = ___position1;
		NullCheck(L_2);
		Transform_set_position_m3111394108(L_2, L_3, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(AudioSource_t1740077639_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_4);
		Component_t3501516275 * L_6 = GameObject_AddComponent_m2208780168(L_4, L_5, /*hidden argument*/NULL);
		V_1 = ((AudioSource_t1740077639 *)CastclassSealed(L_6, AudioSource_t1740077639_il2cpp_TypeInfo_var));
		AudioSource_t1740077639 * L_7 = V_1;
		AudioClip_t794140988 * L_8 = ___clip0;
		NullCheck(L_7);
		AudioSource_set_clip_m19502010(L_7, L_8, /*hidden argument*/NULL);
		AudioSource_t1740077639 * L_9 = V_1;
		NullCheck(L_9);
		AudioSource_set_spatialBlend_m2546548421(L_9, (1.0f), /*hidden argument*/NULL);
		AudioSource_t1740077639 * L_10 = V_1;
		float L_11 = ___volume2;
		NullCheck(L_10);
		AudioSource_set_volume_m1410546616(L_10, L_11, /*hidden argument*/NULL);
		AudioSource_t1740077639 * L_12 = V_1;
		NullCheck(L_12);
		AudioSource_Play_m1360558992(L_12, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_13 = V_0;
		AudioClip_t794140988 * L_14 = ___clip0;
		NullCheck(L_14);
		float L_15 = AudioClip_get_length_m1976537364(L_14, /*hidden argument*/NULL);
		float L_16 = Time_get_timeScale_m1970669766(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B1_0 = L_15;
		G_B1_1 = L_13;
		if ((!(((float)L_16) < ((float)(0.01f)))))
		{
			G_B2_0 = L_15;
			G_B2_1 = L_13;
			goto IL_006c;
		}
	}
	{
		G_B3_0 = (0.01f);
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0071;
	}

IL_006c:
	{
		float L_17 = Time_get_timeScale_m1970669766(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = L_17;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0071:
	{
		Object_Destroy_m2260435093(NULL /*static, unused*/, G_B3_2, ((float)((float)G_B3_1*(float)G_B3_0)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AudioSource::set_loop(System.Boolean)
extern "C"  void AudioSource_set_loop_m3617666708 (AudioSource_t1740077639 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*AudioSource_set_loop_m3617666708_ftn) (AudioSource_t1740077639 *, bool);
	static AudioSource_set_loop_m3617666708_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_loop_m3617666708_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_loop(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.AudioSource::set_playOnAwake(System.Boolean)
extern "C"  void AudioSource_set_playOnAwake_m1884534674 (AudioSource_t1740077639 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*AudioSource_set_playOnAwake_m1884534674_ftn) (AudioSource_t1740077639 *, bool);
	static AudioSource_set_playOnAwake_m1884534674_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_playOnAwake_m1884534674_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_playOnAwake(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.AudioSource::set_spatialBlend(System.Single)
extern "C"  void AudioSource_set_spatialBlend_m2546548421 (AudioSource_t1740077639 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*AudioSource_set_spatialBlend_m2546548421_ftn) (AudioSource_t1740077639 *, float);
	static AudioSource_set_spatialBlend_m2546548421_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_spatialBlend_m2546548421_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_spatialBlend(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.AudioSource::set_mute(System.Boolean)
extern "C"  void AudioSource_set_mute_m4040046601 (AudioSource_t1740077639 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*AudioSource_set_mute_m4040046601_ftn) (AudioSource_t1740077639 *, bool);
	static AudioSource_set_mute_m4040046601_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_mute_m4040046601_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_mute(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Behaviour::.ctor()
extern "C"  void Behaviour__ctor_m1624944828 (Behaviour_t200106419 * __this, const MethodInfo* method)
{
	{
		Component__ctor_m4238603388(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Behaviour::get_enabled()
extern "C"  bool Behaviour_get_enabled_m1239363704 (Behaviour_t200106419 * __this, const MethodInfo* method)
{
	typedef bool (*Behaviour_get_enabled_m1239363704_ftn) (Behaviour_t200106419 *);
	static Behaviour_get_enabled_m1239363704_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Behaviour_get_enabled_m1239363704_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Behaviour::get_enabled()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C"  void Behaviour_set_enabled_m2046806933 (Behaviour_t200106419 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Behaviour_set_enabled_m2046806933_ftn) (Behaviour_t200106419 *, bool);
	static Behaviour_set_enabled_m2046806933_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Behaviour_set_enabled_m2046806933_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Behaviour::set_enabled(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Behaviour::get_isActiveAndEnabled()
extern "C"  bool Behaviour_get_isActiveAndEnabled_m210167461 (Behaviour_t200106419 * __this, const MethodInfo* method)
{
	typedef bool (*Behaviour_get_isActiveAndEnabled_m210167461_ftn) (Behaviour_t200106419 *);
	static Behaviour_get_isActiveAndEnabled_m210167461_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Behaviour_get_isActiveAndEnabled_m210167461_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Behaviour::get_isActiveAndEnabled()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.BitStream::.ctor()
extern "C"  void BitStream__ctor_m922403980 (BitStream_t239125475 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::Serializeb(System.Int32&)
extern "C"  void BitStream_Serializeb_m1472011233 (BitStream_t239125475 * __this, int32_t* ___value0, const MethodInfo* method)
{
	typedef void (*BitStream_Serializeb_m1472011233_ftn) (BitStream_t239125475 *, int32_t*);
	static BitStream_Serializeb_m1472011233_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BitStream_Serializeb_m1472011233_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BitStream::Serializeb(System.Int32&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.BitStream::Serializec(System.Char&)
extern "C"  void BitStream_Serializec_m1377214154 (BitStream_t239125475 * __this, Il2CppChar* ___value0, const MethodInfo* method)
{
	typedef void (*BitStream_Serializec_m1377214154_ftn) (BitStream_t239125475 *, Il2CppChar*);
	static BitStream_Serializec_m1377214154_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BitStream_Serializec_m1377214154_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BitStream::Serializec(System.Char&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.BitStream::Serializes(System.Int16&)
extern "C"  void BitStream_Serializes_m1382809078 (BitStream_t239125475 * __this, int16_t* ___value0, const MethodInfo* method)
{
	typedef void (*BitStream_Serializes_m1382809078_ftn) (BitStream_t239125475 *, int16_t*);
	static BitStream_Serializes_m1382809078_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BitStream_Serializes_m1382809078_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BitStream::Serializes(System.Int16&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.BitStream::Serializei(System.Int32&)
extern "C"  void BitStream_Serializei_m2193239290 (BitStream_t239125475 * __this, int32_t* ___value0, const MethodInfo* method)
{
	typedef void (*BitStream_Serializei_m2193239290_ftn) (BitStream_t239125475 *, int32_t*);
	static BitStream_Serializei_m2193239290_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BitStream_Serializei_m2193239290_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BitStream::Serializei(System.Int32&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.BitStream::Serializef(System.Single&,System.Single)
extern "C"  void BitStream_Serializef_m145870244 (BitStream_t239125475 * __this, float* ___value0, float ___maximumDelta1, const MethodInfo* method)
{
	typedef void (*BitStream_Serializef_m145870244_ftn) (BitStream_t239125475 *, float*, float);
	static BitStream_Serializef_m145870244_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BitStream_Serializef_m145870244_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BitStream::Serializef(System.Single&,System.Single)");
	_il2cpp_icall_func(__this, ___value0, ___maximumDelta1);
}
// System.Void UnityEngine.BitStream::Serializeq(UnityEngine.Quaternion&,System.Single)
extern "C"  void BitStream_Serializeq_m2989370589 (BitStream_t239125475 * __this, Quaternion_t1553702882 * ___value0, float ___maximumDelta1, const MethodInfo* method)
{
	{
		Quaternion_t1553702882 * L_0 = ___value0;
		float L_1 = ___maximumDelta1;
		BitStream_INTERNAL_CALL_Serializeq_m999142740(NULL /*static, unused*/, __this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::INTERNAL_CALL_Serializeq(UnityEngine.BitStream,UnityEngine.Quaternion&,System.Single)
extern "C"  void BitStream_INTERNAL_CALL_Serializeq_m999142740 (Il2CppObject * __this /* static, unused */, BitStream_t239125475 * ___self0, Quaternion_t1553702882 * ___value1, float ___maximumDelta2, const MethodInfo* method)
{
	typedef void (*BitStream_INTERNAL_CALL_Serializeq_m999142740_ftn) (BitStream_t239125475 *, Quaternion_t1553702882 *, float);
	static BitStream_INTERNAL_CALL_Serializeq_m999142740_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BitStream_INTERNAL_CALL_Serializeq_m999142740_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BitStream::INTERNAL_CALL_Serializeq(UnityEngine.BitStream,UnityEngine.Quaternion&,System.Single)");
	_il2cpp_icall_func(___self0, ___value1, ___maximumDelta2);
}
// System.Void UnityEngine.BitStream::Serializev(UnityEngine.Vector3&,System.Single)
extern "C"  void BitStream_Serializev_m2820325190 (BitStream_t239125475 * __this, Vector3_t4282066566 * ___value0, float ___maximumDelta1, const MethodInfo* method)
{
	{
		Vector3_t4282066566 * L_0 = ___value0;
		float L_1 = ___maximumDelta1;
		BitStream_INTERNAL_CALL_Serializev_m2004861039(NULL /*static, unused*/, __this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::INTERNAL_CALL_Serializev(UnityEngine.BitStream,UnityEngine.Vector3&,System.Single)
extern "C"  void BitStream_INTERNAL_CALL_Serializev_m2004861039 (Il2CppObject * __this /* static, unused */, BitStream_t239125475 * ___self0, Vector3_t4282066566 * ___value1, float ___maximumDelta2, const MethodInfo* method)
{
	typedef void (*BitStream_INTERNAL_CALL_Serializev_m2004861039_ftn) (BitStream_t239125475 *, Vector3_t4282066566 *, float);
	static BitStream_INTERNAL_CALL_Serializev_m2004861039_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BitStream_INTERNAL_CALL_Serializev_m2004861039_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BitStream::INTERNAL_CALL_Serializev(UnityEngine.BitStream,UnityEngine.Vector3&,System.Single)");
	_il2cpp_icall_func(___self0, ___value1, ___maximumDelta2);
}
// System.Void UnityEngine.BitStream::Serializen(UnityEngine.NetworkViewID&)
extern "C"  void BitStream_Serializen_m3655149335 (BitStream_t239125475 * __this, NetworkViewID_t3400394436 * ___viewID0, const MethodInfo* method)
{
	{
		NetworkViewID_t3400394436 * L_0 = ___viewID0;
		BitStream_INTERNAL_CALL_Serializen_m4031551296(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::INTERNAL_CALL_Serializen(UnityEngine.BitStream,UnityEngine.NetworkViewID&)
extern "C"  void BitStream_INTERNAL_CALL_Serializen_m4031551296 (Il2CppObject * __this /* static, unused */, BitStream_t239125475 * ___self0, NetworkViewID_t3400394436 * ___viewID1, const MethodInfo* method)
{
	typedef void (*BitStream_INTERNAL_CALL_Serializen_m4031551296_ftn) (BitStream_t239125475 *, NetworkViewID_t3400394436 *);
	static BitStream_INTERNAL_CALL_Serializen_m4031551296_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BitStream_INTERNAL_CALL_Serializen_m4031551296_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BitStream::INTERNAL_CALL_Serializen(UnityEngine.BitStream,UnityEngine.NetworkViewID&)");
	_il2cpp_icall_func(___self0, ___viewID1);
}
// System.Void UnityEngine.BitStream::Serialize(System.Boolean&)
extern "C"  void BitStream_Serialize_m2001881067 (BitStream_t239125475 * __this, bool* ___value0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	bool* G_B5_0 = NULL;
	bool* G_B4_0 = NULL;
	int32_t G_B6_0 = 0;
	bool* G_B6_1 = NULL;
	{
		bool* L_0 = ___value0;
		if (!(*((int8_t*)L_0)))
		{
			goto IL_000d;
		}
	}
	{
		G_B3_0 = 1;
		goto IL_000e;
	}

IL_000d:
	{
		G_B3_0 = 0;
	}

IL_000e:
	{
		V_0 = G_B3_0;
		BitStream_Serializeb_m1472011233(__this, (&V_0), /*hidden argument*/NULL);
		bool* L_1 = ___value0;
		int32_t L_2 = V_0;
		G_B4_0 = L_1;
		if (L_2)
		{
			G_B5_0 = L_1;
			goto IL_0024;
		}
	}
	{
		G_B6_0 = 0;
		G_B6_1 = G_B4_0;
		goto IL_0025;
	}

IL_0024:
	{
		G_B6_0 = 1;
		G_B6_1 = G_B5_0;
	}

IL_0025:
	{
		*((int8_t*)(G_B6_1)) = (int8_t)G_B6_0;
		return;
	}
}
// System.Void UnityEngine.BitStream::Serialize(System.Char&)
extern "C"  void BitStream_Serialize_m2728753561 (BitStream_t239125475 * __this, Il2CppChar* ___value0, const MethodInfo* method)
{
	{
		Il2CppChar* L_0 = ___value0;
		BitStream_Serializec_m1377214154(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::Serialize(System.Int16&)
extern "C"  void BitStream_Serialize_m4204437271 (BitStream_t239125475 * __this, int16_t* ___value0, const MethodInfo* method)
{
	{
		int16_t* L_0 = ___value0;
		BitStream_Serializes_m1382809078(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::Serialize(System.Int32&)
extern "C"  void BitStream_Serialize_m4204493009 (BitStream_t239125475 * __this, int32_t* ___value0, const MethodInfo* method)
{
	{
		int32_t* L_0 = ___value0;
		BitStream_Serializei_m2193239290(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::Serialize(System.Single&)
extern "C"  void BitStream_Serialize_m1473984011 (BitStream_t239125475 * __this, float* ___value0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (1.0E-05f);
		float* L_0 = ___value0;
		float L_1 = V_0;
		BitStream_Serialize_m518836656(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::Serialize(System.Single&,System.Single)
extern "C"  void BitStream_Serialize_m518836656 (BitStream_t239125475 * __this, float* ___value0, float ___maxDelta1, const MethodInfo* method)
{
	{
		float* L_0 = ___value0;
		float L_1 = ___maxDelta1;
		BitStream_Serializef_m145870244(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::Serialize(UnityEngine.Quaternion&)
extern "C"  void BitStream_Serialize_m4132505111 (BitStream_t239125475 * __this, Quaternion_t1553702882 * ___value0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (1.0E-05f);
		Quaternion_t1553702882 * L_0 = ___value0;
		float L_1 = V_0;
		BitStream_Serialize_m1228905660(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::Serialize(UnityEngine.Quaternion&,System.Single)
extern "C"  void BitStream_Serialize_m1228905660 (BitStream_t239125475 * __this, Quaternion_t1553702882 * ___value0, float ___maxDelta1, const MethodInfo* method)
{
	{
		Quaternion_t1553702882 * L_0 = ___value0;
		float L_1 = ___maxDelta1;
		BitStream_Serializeq_m2989370589(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::Serialize(UnityEngine.Vector3&)
extern "C"  void BitStream_Serialize_m981417501 (BitStream_t239125475 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (1.0E-05f);
		Vector3_t4282066566 * L_0 = ___value0;
		float L_1 = V_0;
		BitStream_Serialize_m3209712194(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::Serialize(UnityEngine.Vector3&,System.Single)
extern "C"  void BitStream_Serialize_m3209712194 (BitStream_t239125475 * __this, Vector3_t4282066566 * ___value0, float ___maxDelta1, const MethodInfo* method)
{
	{
		Vector3_t4282066566 * L_0 = ___value0;
		float L_1 = ___maxDelta1;
		BitStream_Serializev_m2820325190(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::Serialize(UnityEngine.NetworkPlayer&)
extern "C"  void BitStream_Serialize_m4247024188 (BitStream_t239125475 * __this, NetworkPlayer_t3231273765 * ___value0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		NetworkPlayer_t3231273765 * L_0 = ___value0;
		int32_t L_1 = L_0->get_index_0();
		V_0 = L_1;
		BitStream_Serializei_m2193239290(__this, (&V_0), /*hidden argument*/NULL);
		NetworkPlayer_t3231273765 * L_2 = ___value0;
		int32_t L_3 = V_0;
		L_2->set_index_0(L_3);
		return;
	}
}
// System.Void UnityEngine.BitStream::Serialize(UnityEngine.NetworkViewID&)
extern "C"  void BitStream_Serialize_m3563231771 (BitStream_t239125475 * __this, NetworkViewID_t3400394436 * ___viewID0, const MethodInfo* method)
{
	{
		NetworkViewID_t3400394436 * L_0 = ___viewID0;
		BitStream_Serializen_m3655149335(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.BitStream::get_isReading()
extern "C"  bool BitStream_get_isReading_m3929869641 (BitStream_t239125475 * __this, const MethodInfo* method)
{
	typedef bool (*BitStream_get_isReading_m3929869641_ftn) (BitStream_t239125475 *);
	static BitStream_get_isReading_m3929869641_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BitStream_get_isReading_m3929869641_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BitStream::get_isReading()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.BitStream::get_isWriting()
extern "C"  bool BitStream_get_isWriting_m3632391065 (BitStream_t239125475 * __this, const MethodInfo* method)
{
	typedef bool (*BitStream_get_isWriting_m3632391065_ftn) (BitStream_t239125475 *);
	static BitStream_get_isWriting_m3632391065_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BitStream_get_isWriting_m3632391065_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BitStream::get_isWriting()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.BitStream::Serialize(System.String&)
extern "C"  void BitStream_Serialize_m2763015060 (BitStream_t239125475 * __this, String_t** ___value0, const MethodInfo* method)
{
	typedef void (*BitStream_Serialize_m2763015060_ftn) (BitStream_t239125475 *, String_t**);
	static BitStream_Serialize_m2763015060_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BitStream_Serialize_m2763015060_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BitStream::Serialize(System.String&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Bounds::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Bounds__ctor_m4160293652 (Bounds_t2711641849 * __this, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___size1, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = ___center0;
		__this->set_m_Center_0(L_0);
		Vector3_t4282066566  L_1 = ___size1;
		Vector3_t4282066566  L_2 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_1, (0.5f), /*hidden argument*/NULL);
		__this->set_m_Extents_1(L_2);
		return;
	}
}
extern "C"  void Bounds__ctor_m4160293652_AdjustorThunk (Il2CppObject * __this, Vector3_t4282066566  ___center0, Vector3_t4282066566  ___size1, const MethodInfo* method)
{
	Bounds_t2711641849 * _thisAdjusted = reinterpret_cast<Bounds_t2711641849 *>(__this + 1);
	Bounds__ctor_m4160293652(_thisAdjusted, ___center0, ___size1, method);
}
// System.Int32 UnityEngine.Bounds::GetHashCode()
extern "C"  int32_t Bounds_GetHashCode_m3067388679 (Bounds_t2711641849 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Vector3_t4282066566  L_0 = Bounds_get_center_m4084610404(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Vector3_GetHashCode_m3912867704((&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_2 = Bounds_get_extents_m2111648188(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Vector3_GetHashCode_m3912867704((&V_1), /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))));
	}
}
extern "C"  int32_t Bounds_GetHashCode_m3067388679_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Bounds_t2711641849 * _thisAdjusted = reinterpret_cast<Bounds_t2711641849 *>(__this + 1);
	return Bounds_GetHashCode_m3067388679(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Bounds::Equals(System.Object)
extern Il2CppClass* Bounds_t2711641849_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern const uint32_t Bounds_Equals_m2517451939_MetadataUsageId;
extern "C"  bool Bounds_Equals_m2517451939 (Bounds_t2711641849 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Bounds_Equals_m2517451939_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Bounds_t2711641849  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	int32_t G_B5_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Bounds_t2711641849_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(Bounds_t2711641849 *)((Bounds_t2711641849 *)UnBox (L_1, Bounds_t2711641849_il2cpp_TypeInfo_var))));
		Vector3_t4282066566  L_2 = Bounds_get_center_m4084610404(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		Vector3_t4282066566  L_3 = Bounds_get_center_m4084610404((&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_4 = L_3;
		Il2CppObject * L_5 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_4);
		bool L_6 = Vector3_Equals_m3337192096((&V_1), L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_004f;
		}
	}
	{
		Vector3_t4282066566  L_7 = Bounds_get_extents_m2111648188(__this, /*hidden argument*/NULL);
		V_2 = L_7;
		Vector3_t4282066566  L_8 = Bounds_get_extents_m2111648188((&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_9 = L_8;
		Il2CppObject * L_10 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_9);
		bool L_11 = Vector3_Equals_m3337192096((&V_2), L_10, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_11));
		goto IL_0050;
	}

IL_004f:
	{
		G_B5_0 = 0;
	}

IL_0050:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool Bounds_Equals_m2517451939_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Bounds_t2711641849 * _thisAdjusted = reinterpret_cast<Bounds_t2711641849 *>(__this + 1);
	return Bounds_Equals_m2517451939(_thisAdjusted, ___other0, method);
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_center()
extern "C"  Vector3_t4282066566  Bounds_get_center_m4084610404 (Bounds_t2711641849 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_m_Center_0();
		return L_0;
	}
}
extern "C"  Vector3_t4282066566  Bounds_get_center_m4084610404_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Bounds_t2711641849 * _thisAdjusted = reinterpret_cast<Bounds_t2711641849 *>(__this + 1);
	return Bounds_get_center_m4084610404(_thisAdjusted, method);
}
// System.Void UnityEngine.Bounds::set_center(UnityEngine.Vector3)
extern "C"  void Bounds_set_center_m2605643707 (Bounds_t2711641849 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = ___value0;
		__this->set_m_Center_0(L_0);
		return;
	}
}
extern "C"  void Bounds_set_center_m2605643707_AdjustorThunk (Il2CppObject * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	Bounds_t2711641849 * _thisAdjusted = reinterpret_cast<Bounds_t2711641849 *>(__this + 1);
	Bounds_set_center_m2605643707(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_size()
extern "C"  Vector3_t4282066566  Bounds_get_size_m3666348432 (Bounds_t2711641849 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_m_Extents_1();
		Vector3_t4282066566  L_1 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_0, (2.0f), /*hidden argument*/NULL);
		return L_1;
	}
}
extern "C"  Vector3_t4282066566  Bounds_get_size_m3666348432_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Bounds_t2711641849 * _thisAdjusted = reinterpret_cast<Bounds_t2711641849 *>(__this + 1);
	return Bounds_get_size_m3666348432(_thisAdjusted, method);
}
// System.Void UnityEngine.Bounds::set_size(UnityEngine.Vector3)
extern "C"  void Bounds_set_size_m4109809039 (Bounds_t2711641849 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = ___value0;
		Vector3_t4282066566  L_1 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_0, (0.5f), /*hidden argument*/NULL);
		__this->set_m_Extents_1(L_1);
		return;
	}
}
extern "C"  void Bounds_set_size_m4109809039_AdjustorThunk (Il2CppObject * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	Bounds_t2711641849 * _thisAdjusted = reinterpret_cast<Bounds_t2711641849 *>(__this + 1);
	Bounds_set_size_m4109809039(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_extents()
extern "C"  Vector3_t4282066566  Bounds_get_extents_m2111648188 (Bounds_t2711641849 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_m_Extents_1();
		return L_0;
	}
}
extern "C"  Vector3_t4282066566  Bounds_get_extents_m2111648188_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Bounds_t2711641849 * _thisAdjusted = reinterpret_cast<Bounds_t2711641849 *>(__this + 1);
	return Bounds_get_extents_m2111648188(_thisAdjusted, method);
}
// System.Void UnityEngine.Bounds::set_extents(UnityEngine.Vector3)
extern "C"  void Bounds_set_extents_m2603836823 (Bounds_t2711641849 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = ___value0;
		__this->set_m_Extents_1(L_0);
		return;
	}
}
extern "C"  void Bounds_set_extents_m2603836823_AdjustorThunk (Il2CppObject * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	Bounds_t2711641849 * _thisAdjusted = reinterpret_cast<Bounds_t2711641849 *>(__this + 1);
	Bounds_set_extents_m2603836823(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_min()
extern "C"  Vector3_t4282066566  Bounds_get_min_m2329472069 (Bounds_t2711641849 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = Bounds_get_center_m4084610404(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_1 = Bounds_get_extents_m2111648188(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_2 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  Vector3_t4282066566  Bounds_get_min_m2329472069_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Bounds_t2711641849 * _thisAdjusted = reinterpret_cast<Bounds_t2711641849 *>(__this + 1);
	return Bounds_get_min_m2329472069(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_max()
extern "C"  Vector3_t4282066566  Bounds_get_max_m2329243351 (Bounds_t2711641849 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = Bounds_get_center_m4084610404(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_1 = Bounds_get_extents_m2111648188(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_2 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  Vector3_t4282066566  Bounds_get_max_m2329243351_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Bounds_t2711641849 * _thisAdjusted = reinterpret_cast<Bounds_t2711641849 *>(__this + 1);
	return Bounds_get_max_m2329243351(_thisAdjusted, method);
}
// System.Void UnityEngine.Bounds::SetMinMax(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Bounds_SetMinMax_m2186020994 (Bounds_t2711641849 * __this, Vector3_t4282066566  ___min0, Vector3_t4282066566  ___max1, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = ___max1;
		Vector3_t4282066566  L_1 = ___min0;
		Vector3_t4282066566  L_2 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Vector3_t4282066566  L_3 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_2, (0.5f), /*hidden argument*/NULL);
		Bounds_set_extents_m2603836823(__this, L_3, /*hidden argument*/NULL);
		Vector3_t4282066566  L_4 = ___min0;
		Vector3_t4282066566  L_5 = Bounds_get_extents_m2111648188(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_6 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		Bounds_set_center_m2605643707(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Bounds_SetMinMax_m2186020994_AdjustorThunk (Il2CppObject * __this, Vector3_t4282066566  ___min0, Vector3_t4282066566  ___max1, const MethodInfo* method)
{
	Bounds_t2711641849 * _thisAdjusted = reinterpret_cast<Bounds_t2711641849 *>(__this + 1);
	Bounds_SetMinMax_m2186020994(_thisAdjusted, ___min0, ___max1, method);
}
// System.Void UnityEngine.Bounds::Encapsulate(UnityEngine.Vector3)
extern "C"  void Bounds_Encapsulate_m3624685234 (Bounds_t2711641849 * __this, Vector3_t4282066566  ___point0, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = Bounds_get_min_m2329472069(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_1 = ___point0;
		Vector3_t4282066566  L_2 = Vector3_Min_m10392601(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Vector3_t4282066566  L_3 = Bounds_get_max_m2329243351(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_4 = ___point0;
		Vector3_t4282066566  L_5 = Vector3_Max_m545730887(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Bounds_SetMinMax_m2186020994(__this, L_2, L_5, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Bounds_Encapsulate_m3624685234_AdjustorThunk (Il2CppObject * __this, Vector3_t4282066566  ___point0, const MethodInfo* method)
{
	Bounds_t2711641849 * _thisAdjusted = reinterpret_cast<Bounds_t2711641849 *>(__this + 1);
	Bounds_Encapsulate_m3624685234(_thisAdjusted, ___point0, method);
}
// System.String UnityEngine.Bounds::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral619960057;
extern const uint32_t Bounds_ToString_m1795228795_MetadataUsageId;
extern "C"  String_t* Bounds_ToString_m1795228795 (Bounds_t2711641849 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Bounds_ToString_m1795228795_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)2));
		Vector3_t4282066566  L_1 = __this->get_m_Center_0();
		Vector3_t4282066566  L_2 = L_1;
		Il2CppObject * L_3 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_0;
		Vector3_t4282066566  L_5 = __this->get_m_Extents_1();
		Vector3_t4282066566  L_6 = L_5;
		Il2CppObject * L_7 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		String_t* L_8 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral619960057, L_4, /*hidden argument*/NULL);
		return L_8;
	}
}
extern "C"  String_t* Bounds_ToString_m1795228795_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Bounds_t2711641849 * _thisAdjusted = reinterpret_cast<Bounds_t2711641849 *>(__this + 1);
	return Bounds_ToString_m1795228795(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Bounds::op_Equality(UnityEngine.Bounds,UnityEngine.Bounds)
extern "C"  bool Bounds_op_Equality_m2855966622 (Il2CppObject * __this /* static, unused */, Bounds_t2711641849  ___lhs0, Bounds_t2711641849  ___rhs1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Vector3_t4282066566  L_0 = Bounds_get_center_m4084610404((&___lhs0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_1 = Bounds_get_center_m4084610404((&___rhs1), /*hidden argument*/NULL);
		bool L_2 = Vector3_op_Equality_m582817895(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		Vector3_t4282066566  L_3 = Bounds_get_extents_m2111648188((&___lhs0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_4 = Bounds_get_extents_m2111648188((&___rhs1), /*hidden argument*/NULL);
		bool L_5 = Vector3_op_Equality_m582817895(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002e;
	}

IL_002d:
	{
		G_B3_0 = 0;
	}

IL_002e:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean UnityEngine.Bounds::op_Inequality(UnityEngine.Bounds,UnityEngine.Bounds)
extern "C"  bool Bounds_op_Inequality_m4292292377 (Il2CppObject * __this /* static, unused */, Bounds_t2711641849  ___lhs0, Bounds_t2711641849  ___rhs1, const MethodInfo* method)
{
	{
		Bounds_t2711641849  L_0 = ___lhs0;
		Bounds_t2711641849  L_1 = ___rhs1;
		bool L_2 = Bounds_op_Equality_m2855966622(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.Bounds
extern "C" void Bounds_t2711641849_marshal_pinvoke(const Bounds_t2711641849& unmarshaled, Bounds_t2711641849_marshaled_pinvoke& marshaled)
{
	Vector3_t4282066566_marshal_pinvoke(unmarshaled.get_m_Center_0(), marshaled.___m_Center_0);
	Vector3_t4282066566_marshal_pinvoke(unmarshaled.get_m_Extents_1(), marshaled.___m_Extents_1);
}
extern "C" void Bounds_t2711641849_marshal_pinvoke_back(const Bounds_t2711641849_marshaled_pinvoke& marshaled, Bounds_t2711641849& unmarshaled)
{
	Vector3_t4282066566  unmarshaled_m_Center_temp_0;
	memset(&unmarshaled_m_Center_temp_0, 0, sizeof(unmarshaled_m_Center_temp_0));
	Vector3_t4282066566_marshal_pinvoke_back(marshaled.___m_Center_0, unmarshaled_m_Center_temp_0);
	unmarshaled.set_m_Center_0(unmarshaled_m_Center_temp_0);
	Vector3_t4282066566  unmarshaled_m_Extents_temp_1;
	memset(&unmarshaled_m_Extents_temp_1, 0, sizeof(unmarshaled_m_Extents_temp_1));
	Vector3_t4282066566_marshal_pinvoke_back(marshaled.___m_Extents_1, unmarshaled_m_Extents_temp_1);
	unmarshaled.set_m_Extents_1(unmarshaled_m_Extents_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Bounds
extern "C" void Bounds_t2711641849_marshal_pinvoke_cleanup(Bounds_t2711641849_marshaled_pinvoke& marshaled)
{
	Vector3_t4282066566_marshal_pinvoke_cleanup(marshaled.___m_Center_0);
	Vector3_t4282066566_marshal_pinvoke_cleanup(marshaled.___m_Extents_1);
}
// Conversion methods for marshalling of: UnityEngine.Bounds
extern "C" void Bounds_t2711641849_marshal_com(const Bounds_t2711641849& unmarshaled, Bounds_t2711641849_marshaled_com& marshaled)
{
	Vector3_t4282066566_marshal_com(unmarshaled.get_m_Center_0(), marshaled.___m_Center_0);
	Vector3_t4282066566_marshal_com(unmarshaled.get_m_Extents_1(), marshaled.___m_Extents_1);
}
extern "C" void Bounds_t2711641849_marshal_com_back(const Bounds_t2711641849_marshaled_com& marshaled, Bounds_t2711641849& unmarshaled)
{
	Vector3_t4282066566  unmarshaled_m_Center_temp_0;
	memset(&unmarshaled_m_Center_temp_0, 0, sizeof(unmarshaled_m_Center_temp_0));
	Vector3_t4282066566_marshal_com_back(marshaled.___m_Center_0, unmarshaled_m_Center_temp_0);
	unmarshaled.set_m_Center_0(unmarshaled_m_Center_temp_0);
	Vector3_t4282066566  unmarshaled_m_Extents_temp_1;
	memset(&unmarshaled_m_Extents_temp_1, 0, sizeof(unmarshaled_m_Extents_temp_1));
	Vector3_t4282066566_marshal_com_back(marshaled.___m_Extents_1, unmarshaled_m_Extents_temp_1);
	unmarshaled.set_m_Extents_1(unmarshaled_m_Extents_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Bounds
extern "C" void Bounds_t2711641849_marshal_com_cleanup(Bounds_t2711641849_marshaled_com& marshaled)
{
	Vector3_t4282066566_marshal_com_cleanup(marshaled.___m_Center_0);
	Vector3_t4282066566_marshal_com_cleanup(marshaled.___m_Extents_1);
}
// System.Boolean UnityEngine.Caching::CleanCache()
extern "C"  bool Caching_CleanCache_m3047430173 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Caching_CleanCache_m3047430173_ftn) ();
	static Caching_CleanCache_m3047430173_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Caching_CleanCache_m3047430173_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Caching::CleanCache()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.Caching::get_ready()
extern "C"  bool Caching_get_ready_m3259280152 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Caching_get_ready_m3259280152_ftn) ();
	static Caching_get_ready_m3259280152_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Caching_get_ready_m3259280152_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Caching::get_ready()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Camera::get_fieldOfView()
extern "C"  float Camera_get_fieldOfView_m65126887 (Camera_t2727095145 * __this, const MethodInfo* method)
{
	typedef float (*Camera_get_fieldOfView_m65126887_ftn) (Camera_t2727095145 *);
	static Camera_get_fieldOfView_m65126887_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_fieldOfView_m65126887_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_fieldOfView()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::set_fieldOfView(System.Single)
extern "C"  void Camera_set_fieldOfView_m809388684 (Camera_t2727095145 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Camera_set_fieldOfView_m809388684_ftn) (Camera_t2727095145 *, float);
	static Camera_set_fieldOfView_m809388684_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_fieldOfView_m809388684_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_fieldOfView(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Camera::get_nearClipPlane()
extern "C"  float Camera_get_nearClipPlane_m4074655061 (Camera_t2727095145 * __this, const MethodInfo* method)
{
	typedef float (*Camera_get_nearClipPlane_m4074655061_ftn) (Camera_t2727095145 *);
	static Camera_get_nearClipPlane_m4074655061_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_nearClipPlane_m4074655061_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_nearClipPlane()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::set_nearClipPlane(System.Single)
extern "C"  void Camera_set_nearClipPlane_m534185950 (Camera_t2727095145 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Camera_set_nearClipPlane_m534185950_ftn) (Camera_t2727095145 *, float);
	static Camera_set_nearClipPlane_m534185950_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_nearClipPlane_m534185950_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_nearClipPlane(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Camera::get_farClipPlane()
extern "C"  float Camera_get_farClipPlane_m388706726 (Camera_t2727095145 * __this, const MethodInfo* method)
{
	typedef float (*Camera_get_farClipPlane_m388706726_ftn) (Camera_t2727095145 *);
	static Camera_get_farClipPlane_m388706726_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_farClipPlane_m388706726_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_farClipPlane()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::set_farClipPlane(System.Single)
extern "C"  void Camera_set_farClipPlane_m1540693853 (Camera_t2727095145 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Camera_set_farClipPlane_m1540693853_ftn) (Camera_t2727095145 *, float);
	static Camera_set_farClipPlane_m1540693853_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_farClipPlane_m1540693853_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_farClipPlane(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Camera::get_orthographicSize()
extern "C"  float Camera_get_orthographicSize_m3215515490 (Camera_t2727095145 * __this, const MethodInfo* method)
{
	typedef float (*Camera_get_orthographicSize_m3215515490_ftn) (Camera_t2727095145 *);
	static Camera_get_orthographicSize_m3215515490_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_orthographicSize_m3215515490_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_orthographicSize()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::set_orthographicSize(System.Single)
extern "C"  void Camera_set_orthographicSize_m3910539041 (Camera_t2727095145 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Camera_set_orthographicSize_m3910539041_ftn) (Camera_t2727095145 *, float);
	static Camera_set_orthographicSize_m3910539041_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_orthographicSize_m3910539041_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_orthographicSize(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Camera::set_orthographic(System.Boolean)
extern "C"  void Camera_set_orthographic_m3849949344 (Camera_t2727095145 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Camera_set_orthographic_m3849949344_ftn) (Camera_t2727095145 *, bool);
	static Camera_set_orthographic_m3849949344_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_orthographic_m3849949344_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_orthographic(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Camera::get_depth()
extern "C"  float Camera_get_depth_m3642810036 (Camera_t2727095145 * __this, const MethodInfo* method)
{
	typedef float (*Camera_get_depth_m3642810036_ftn) (Camera_t2727095145 *);
	static Camera_get_depth_m3642810036_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_depth_m3642810036_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_depth()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::set_aspect(System.Single)
extern "C"  void Camera_set_aspect_m2970032698 (Camera_t2727095145 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Camera_set_aspect_m2970032698_ftn) (Camera_t2727095145 *, float);
	static Camera_set_aspect_m2970032698_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_aspect_m2970032698_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_aspect(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.Camera::get_cullingMask()
extern "C"  int32_t Camera_get_cullingMask_m1045975289 (Camera_t2727095145 * __this, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_cullingMask_m1045975289_ftn) (Camera_t2727095145 *);
	static Camera_get_cullingMask_m1045975289_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_cullingMask_m1045975289_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_cullingMask()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::set_cullingMask(System.Int32)
extern "C"  void Camera_set_cullingMask_m2181279574 (Camera_t2727095145 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Camera_set_cullingMask_m2181279574_ftn) (Camera_t2727095145 *, int32_t);
	static Camera_set_cullingMask_m2181279574_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_cullingMask_m2181279574_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_cullingMask(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.Camera::get_eventMask()
extern "C"  int32_t Camera_get_eventMask_m3669132771 (Camera_t2727095145 * __this, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_eventMask_m3669132771_ftn) (Camera_t2727095145 *);
	static Camera_get_eventMask_m3669132771_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_eventMask_m3669132771_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_eventMask()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::set_backgroundColor(UnityEngine.Color)
extern "C"  void Camera_set_backgroundColor_m501006344 (Camera_t2727095145 * __this, Color_t4194546905  ___value0, const MethodInfo* method)
{
	{
		Camera_INTERNAL_set_backgroundColor_m2139927960(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_set_backgroundColor(UnityEngine.Color&)
extern "C"  void Camera_INTERNAL_set_backgroundColor_m2139927960 (Camera_t2727095145 * __this, Color_t4194546905 * ___value0, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_set_backgroundColor_m2139927960_ftn) (Camera_t2727095145 *, Color_t4194546905 *);
	static Camera_INTERNAL_set_backgroundColor_m2139927960_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_set_backgroundColor_m2139927960_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_set_backgroundColor(UnityEngine.Color&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Rect UnityEngine.Camera::get_rect()
extern "C"  Rect_t4241904616  Camera_get_rect_m3083266205 (Camera_t2727095145 * __this, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Camera_INTERNAL_get_rect_m1804455538(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t4241904616  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Camera::set_rect(UnityEngine.Rect)
extern "C"  void Camera_set_rect_m1907189602 (Camera_t2727095145 * __this, Rect_t4241904616  ___value0, const MethodInfo* method)
{
	{
		Camera_INTERNAL_set_rect_m458921854(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C"  void Camera_INTERNAL_get_rect_m1804455538 (Camera_t2727095145 * __this, Rect_t4241904616 * ___value0, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_get_rect_m1804455538_ftn) (Camera_t2727095145 *, Rect_t4241904616 *);
	static Camera_INTERNAL_get_rect_m1804455538_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_get_rect_m1804455538_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_get_rect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Camera::INTERNAL_set_rect(UnityEngine.Rect&)
extern "C"  void Camera_INTERNAL_set_rect_m458921854 (Camera_t2727095145 * __this, Rect_t4241904616 * ___value0, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_set_rect_m458921854_ftn) (Camera_t2727095145 *, Rect_t4241904616 *);
	static Camera_INTERNAL_set_rect_m458921854_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_set_rect_m458921854_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_set_rect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Rect UnityEngine.Camera::get_pixelRect()
extern "C"  Rect_t4241904616  Camera_get_pixelRect_m936851539 (Camera_t2727095145 * __this, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Camera_INTERNAL_get_pixelRect_m1853722860(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t4241904616  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)
extern "C"  void Camera_INTERNAL_get_pixelRect_m1853722860 (Camera_t2727095145 * __this, Rect_t4241904616 * ___value0, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_get_pixelRect_m1853722860_ftn) (Camera_t2727095145 *, Rect_t4241904616 *);
	static Camera_INTERNAL_get_pixelRect_m1853722860_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_get_pixelRect_m1853722860_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.RenderTexture UnityEngine.Camera::get_targetTexture()
extern "C"  RenderTexture_t1963041563 * Camera_get_targetTexture_m1468336738 (Camera_t2727095145 * __this, const MethodInfo* method)
{
	typedef RenderTexture_t1963041563 * (*Camera_get_targetTexture_m1468336738_ftn) (Camera_t2727095145 *);
	static Camera_get_targetTexture_m1468336738_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_targetTexture_m1468336738_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_targetTexture()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::set_targetTexture(UnityEngine.RenderTexture)
extern "C"  void Camera_set_targetTexture_m671169649 (Camera_t2727095145 * __this, RenderTexture_t1963041563 * ___value0, const MethodInfo* method)
{
	typedef void (*Camera_set_targetTexture_m671169649_ftn) (Camera_t2727095145 *, RenderTexture_t1963041563 *);
	static Camera_set_targetTexture_m671169649_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_targetTexture_m671169649_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_targetTexture(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Matrix4x4 UnityEngine.Camera::get_projectionMatrix()
extern "C"  Matrix4x4_t1651859333  Camera_get_projectionMatrix_m3070982480 (Camera_t2727095145 * __this, const MethodInfo* method)
{
	Matrix4x4_t1651859333  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Camera_INTERNAL_get_projectionMatrix_m684200627(__this, (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t1651859333  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Camera::set_projectionMatrix(UnityEngine.Matrix4x4)
extern "C"  void Camera_set_projectionMatrix_m2541009521 (Camera_t2727095145 * __this, Matrix4x4_t1651859333  ___value0, const MethodInfo* method)
{
	{
		Camera_INTERNAL_set_projectionMatrix_m1272775463(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_get_projectionMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Camera_INTERNAL_get_projectionMatrix_m684200627 (Camera_t2727095145 * __this, Matrix4x4_t1651859333 * ___value0, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_get_projectionMatrix_m684200627_ftn) (Camera_t2727095145 *, Matrix4x4_t1651859333 *);
	static Camera_INTERNAL_get_projectionMatrix_m684200627_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_get_projectionMatrix_m684200627_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_get_projectionMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Camera::INTERNAL_set_projectionMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Camera_INTERNAL_set_projectionMatrix_m1272775463 (Camera_t2727095145 * __this, Matrix4x4_t1651859333 * ___value0, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_set_projectionMatrix_m1272775463_ftn) (Camera_t2727095145 *, Matrix4x4_t1651859333 *);
	static Camera_INTERNAL_set_projectionMatrix_m1272775463_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_set_projectionMatrix_m1272775463_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_set_projectionMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.CameraClearFlags UnityEngine.Camera::get_clearFlags()
extern "C"  int32_t Camera_get_clearFlags_m192466552 (Camera_t2727095145 * __this, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_clearFlags_m192466552_ftn) (Camera_t2727095145 *);
	static Camera_get_clearFlags_m192466552_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_clearFlags_m192466552_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_clearFlags()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Vector3 UnityEngine.Camera::WorldToScreenPoint(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Camera_WorldToScreenPoint_m2400233676 (Camera_t2727095145 * __this, Vector3_t4282066566  ___position0, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Camera_INTERNAL_CALL_WorldToScreenPoint_m316056758(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_CALL_WorldToScreenPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Camera_INTERNAL_CALL_WorldToScreenPoint_m316056758 (Il2CppObject * __this /* static, unused */, Camera_t2727095145 * ___self0, Vector3_t4282066566 * ___position1, Vector3_t4282066566 * ___value2, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_CALL_WorldToScreenPoint_m316056758_ftn) (Camera_t2727095145 *, Vector3_t4282066566 *, Vector3_t4282066566 *);
	static Camera_INTERNAL_CALL_WorldToScreenPoint_m316056758_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_WorldToScreenPoint_m316056758_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_WorldToScreenPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Vector3 UnityEngine.Camera::ScreenToWorldPoint(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Camera_ScreenToWorldPoint_m1572306334 (Camera_t2727095145 * __this, Vector3_t4282066566  ___position0, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Camera_INTERNAL_CALL_ScreenToWorldPoint_m1475601444(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenToWorldPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Camera_INTERNAL_CALL_ScreenToWorldPoint_m1475601444 (Il2CppObject * __this /* static, unused */, Camera_t2727095145 * ___self0, Vector3_t4282066566 * ___position1, Vector3_t4282066566 * ___value2, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_CALL_ScreenToWorldPoint_m1475601444_ftn) (Camera_t2727095145 *, Vector3_t4282066566 *, Vector3_t4282066566 *);
	static Camera_INTERNAL_CALL_ScreenToWorldPoint_m1475601444_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ScreenToWorldPoint_m1475601444_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ScreenToWorldPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Vector3 UnityEngine.Camera::ScreenToViewportPoint(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Camera_ScreenToViewportPoint_m3727203754 (Camera_t2727095145 * __this, Vector3_t4282066566  ___position0, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Camera_INTERNAL_CALL_ScreenToViewportPoint_m3712526702(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Camera_INTERNAL_CALL_ScreenToViewportPoint_m3712526702 (Il2CppObject * __this /* static, unused */, Camera_t2727095145 * ___self0, Vector3_t4282066566 * ___position1, Vector3_t4282066566 * ___value2, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_CALL_ScreenToViewportPoint_m3712526702_ftn) (Camera_t2727095145 *, Vector3_t4282066566 *, Vector3_t4282066566 *);
	static Camera_INTERNAL_CALL_ScreenToViewportPoint_m3712526702_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ScreenToViewportPoint_m3712526702_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ScreenToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector3)
extern "C"  Ray_t3134616544  Camera_ScreenPointToRay_m1733083890 (Camera_t2727095145 * __this, Vector3_t4282066566  ___position0, const MethodInfo* method)
{
	Ray_t3134616544  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Camera_INTERNAL_CALL_ScreenPointToRay_m371614468(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Ray_t3134616544  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Ray&)
extern "C"  void Camera_INTERNAL_CALL_ScreenPointToRay_m371614468 (Il2CppObject * __this /* static, unused */, Camera_t2727095145 * ___self0, Vector3_t4282066566 * ___position1, Ray_t3134616544 * ___value2, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_CALL_ScreenPointToRay_m371614468_ftn) (Camera_t2727095145 *, Vector3_t4282066566 *, Ray_t3134616544 *);
	static Camera_INTERNAL_CALL_ScreenPointToRay_m371614468_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ScreenPointToRay_m371614468_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Ray&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C"  Camera_t2727095145 * Camera_get_main_m671815697 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef Camera_t2727095145 * (*Camera_get_main_m671815697_ftn) ();
	static Camera_get_main_m671815697_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_main_m671815697_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_main()");
	return _il2cpp_icall_func();
}
// UnityEngine.Camera UnityEngine.Camera::get_current()
extern "C"  Camera_t2727095145 * Camera_get_current_m475592003 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef Camera_t2727095145 * (*Camera_get_current_m475592003_ftn) ();
	static Camera_get_current_m475592003_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_current_m475592003_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_current()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Camera::get_allCamerasCount()
extern "C"  int32_t Camera_get_allCamerasCount_m3993434431 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_allCamerasCount_m3993434431_ftn) ();
	static Camera_get_allCamerasCount_m3993434431_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_allCamerasCount_m3993434431_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_allCamerasCount()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])
extern "C"  int32_t Camera_GetAllCameras_m3771867787 (Il2CppObject * __this /* static, unused */, CameraU5BU5D_t2716570836* ___cameras0, const MethodInfo* method)
{
	typedef int32_t (*Camera_GetAllCameras_m3771867787_ftn) (CameraU5BU5D_t2716570836*);
	static Camera_GetAllCameras_m3771867787_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_GetAllCameras_m3771867787_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])");
	return _il2cpp_icall_func(___cameras0);
}
// System.Void UnityEngine.Camera::FireOnPreCull(UnityEngine.Camera)
extern Il2CppClass* Camera_t2727095145_il2cpp_TypeInfo_var;
extern const uint32_t Camera_FireOnPreCull_m4184591338_MetadataUsageId;
extern "C"  void Camera_FireOnPreCull_m4184591338 (Il2CppObject * __this /* static, unused */, Camera_t2727095145 * ___cam0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Camera_FireOnPreCull_m4184591338_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CameraCallback_t1945583101 * L_0 = ((Camera_t2727095145_StaticFields*)Camera_t2727095145_il2cpp_TypeInfo_var->static_fields)->get_onPreCull_2();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		CameraCallback_t1945583101 * L_1 = ((Camera_t2727095145_StaticFields*)Camera_t2727095145_il2cpp_TypeInfo_var->static_fields)->get_onPreCull_2();
		Camera_t2727095145 * L_2 = ___cam0;
		NullCheck(L_1);
		CameraCallback_Invoke_m3757410555(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.Camera::FireOnPreRender(UnityEngine.Camera)
extern Il2CppClass* Camera_t2727095145_il2cpp_TypeInfo_var;
extern const uint32_t Camera_FireOnPreRender_m2754549070_MetadataUsageId;
extern "C"  void Camera_FireOnPreRender_m2754549070 (Il2CppObject * __this /* static, unused */, Camera_t2727095145 * ___cam0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Camera_FireOnPreRender_m2754549070_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CameraCallback_t1945583101 * L_0 = ((Camera_t2727095145_StaticFields*)Camera_t2727095145_il2cpp_TypeInfo_var->static_fields)->get_onPreRender_3();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		CameraCallback_t1945583101 * L_1 = ((Camera_t2727095145_StaticFields*)Camera_t2727095145_il2cpp_TypeInfo_var->static_fields)->get_onPreRender_3();
		Camera_t2727095145 * L_2 = ___cam0;
		NullCheck(L_1);
		CameraCallback_Invoke_m3757410555(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.Camera::FireOnPostRender(UnityEngine.Camera)
extern Il2CppClass* Camera_t2727095145_il2cpp_TypeInfo_var;
extern const uint32_t Camera_FireOnPostRender_m865551979_MetadataUsageId;
extern "C"  void Camera_FireOnPostRender_m865551979 (Il2CppObject * __this /* static, unused */, Camera_t2727095145 * ___cam0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Camera_FireOnPostRender_m865551979_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CameraCallback_t1945583101 * L_0 = ((Camera_t2727095145_StaticFields*)Camera_t2727095145_il2cpp_TypeInfo_var->static_fields)->get_onPostRender_4();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		CameraCallback_t1945583101 * L_1 = ((Camera_t2727095145_StaticFields*)Camera_t2727095145_il2cpp_TypeInfo_var->static_fields)->get_onPostRender_4();
		Camera_t2727095145 * L_2 = ___cam0;
		NullCheck(L_1);
		CameraCallback_Invoke_m3757410555(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.Camera::Render()
extern "C"  void Camera_Render_m945002290 (Camera_t2727095145 * __this, const MethodInfo* method)
{
	typedef void (*Camera_Render_m945002290_ftn) (Camera_t2727095145 *);
	static Camera_Render_m945002290_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_Render_m945002290_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::Render()");
	_il2cpp_icall_func(__this);
}
// UnityEngine.GameObject UnityEngine.Camera::RaycastTry(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  GameObject_t3674682005 * Camera_RaycastTry_m569221064 (Camera_t2727095145 * __this, Ray_t3134616544  ___ray0, float ___distance1, int32_t ___layerMask2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		float L_0 = ___distance1;
		int32_t L_1 = ___layerMask2;
		int32_t L_2 = V_0;
		GameObject_t3674682005 * L_3 = Camera_INTERNAL_CALL_RaycastTry_m2819346616(NULL /*static, unused*/, __this, (&___ray0), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  GameObject_t3674682005 * Camera_INTERNAL_CALL_RaycastTry_m2819346616 (Il2CppObject * __this /* static, unused */, Camera_t2727095145 * ___self0, Ray_t3134616544 * ___ray1, float ___distance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	typedef GameObject_t3674682005 * (*Camera_INTERNAL_CALL_RaycastTry_m2819346616_ftn) (Camera_t2727095145 *, Ray_t3134616544 *, float, int32_t, int32_t);
	static Camera_INTERNAL_CALL_RaycastTry_m2819346616_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_RaycastTry_m2819346616_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	return _il2cpp_icall_func(___self0, ___ray1, ___distance2, ___layerMask3, ___queryTriggerInteraction4);
}
// UnityEngine.GameObject UnityEngine.Camera::RaycastTry2D(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  GameObject_t3674682005 * Camera_RaycastTry2D_m3256311322 (Camera_t2727095145 * __this, Ray_t3134616544  ___ray0, float ___distance1, int32_t ___layerMask2, const MethodInfo* method)
{
	{
		float L_0 = ___distance1;
		int32_t L_1 = ___layerMask2;
		GameObject_t3674682005 * L_2 = Camera_INTERNAL_CALL_RaycastTry2D_m2796313289(NULL /*static, unused*/, __this, (&___ray0), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
extern "C"  GameObject_t3674682005 * Camera_INTERNAL_CALL_RaycastTry2D_m2796313289 (Il2CppObject * __this /* static, unused */, Camera_t2727095145 * ___self0, Ray_t3134616544 * ___ray1, float ___distance2, int32_t ___layerMask3, const MethodInfo* method)
{
	typedef GameObject_t3674682005 * (*Camera_INTERNAL_CALL_RaycastTry2D_m2796313289_ftn) (Camera_t2727095145 *, Ray_t3134616544 *, float, int32_t);
	static Camera_INTERNAL_CALL_RaycastTry2D_m2796313289_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_RaycastTry2D_m2796313289_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)");
	return _il2cpp_icall_func(___self0, ___ray1, ___distance2, ___layerMask3);
}
// System.Void UnityEngine.Camera/CameraCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void CameraCallback__ctor_m3976281469 (CameraCallback_t1945583101 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Camera/CameraCallback::Invoke(UnityEngine.Camera)
extern "C"  void CameraCallback_Invoke_m3757410555 (CameraCallback_t1945583101 * __this, Camera_t2727095145 * ___cam0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CameraCallback_Invoke_m3757410555((CameraCallback_t1945583101 *)__this->get_prev_9(),___cam0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Camera_t2727095145 * ___cam0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___cam0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Camera_t2727095145 * ___cam0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___cam0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___cam0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Camera/CameraCallback::BeginInvoke(UnityEngine.Camera,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CameraCallback_BeginInvoke_m3719545510 (CameraCallback_t1945583101 * __this, Camera_t2727095145 * ___cam0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___cam0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Camera/CameraCallback::EndInvoke(System.IAsyncResult)
extern "C"  void CameraCallback_EndInvoke_m534836749 (CameraCallback_t1945583101 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Canvas::add_willRenderCanvases(UnityEngine.Canvas/WillRenderCanvases)
extern Il2CppClass* Canvas_t2727140764_il2cpp_TypeInfo_var;
extern Il2CppClass* WillRenderCanvases_t4247149838_il2cpp_TypeInfo_var;
extern const uint32_t Canvas_add_willRenderCanvases_m716439881_MetadataUsageId;
extern "C"  void Canvas_add_willRenderCanvases_m716439881 (Il2CppObject * __this /* static, unused */, WillRenderCanvases_t4247149838 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Canvas_add_willRenderCanvases_m716439881_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		WillRenderCanvases_t4247149838 * L_0 = ((Canvas_t2727140764_StaticFields*)Canvas_t2727140764_il2cpp_TypeInfo_var->static_fields)->get_willRenderCanvases_2();
		WillRenderCanvases_t4247149838 * L_1 = ___value0;
		Delegate_t3310234105 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Canvas_t2727140764_StaticFields*)Canvas_t2727140764_il2cpp_TypeInfo_var->static_fields)->set_willRenderCanvases_2(((WillRenderCanvases_t4247149838 *)CastclassSealed(L_2, WillRenderCanvases_t4247149838_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UnityEngine.Canvas::remove_willRenderCanvases(UnityEngine.Canvas/WillRenderCanvases)
extern Il2CppClass* Canvas_t2727140764_il2cpp_TypeInfo_var;
extern Il2CppClass* WillRenderCanvases_t4247149838_il2cpp_TypeInfo_var;
extern const uint32_t Canvas_remove_willRenderCanvases_m158291336_MetadataUsageId;
extern "C"  void Canvas_remove_willRenderCanvases_m158291336 (Il2CppObject * __this /* static, unused */, WillRenderCanvases_t4247149838 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Canvas_remove_willRenderCanvases_m158291336_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		WillRenderCanvases_t4247149838 * L_0 = ((Canvas_t2727140764_StaticFields*)Canvas_t2727140764_il2cpp_TypeInfo_var->static_fields)->get_willRenderCanvases_2();
		WillRenderCanvases_t4247149838 * L_1 = ___value0;
		Delegate_t3310234105 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Canvas_t2727140764_StaticFields*)Canvas_t2727140764_il2cpp_TypeInfo_var->static_fields)->set_willRenderCanvases_2(((WillRenderCanvases_t4247149838 *)CastclassSealed(L_2, WillRenderCanvases_t4247149838_il2cpp_TypeInfo_var)));
		return;
	}
}
// UnityEngine.RenderMode UnityEngine.Canvas::get_renderMode()
extern "C"  int32_t Canvas_get_renderMode_m3669121200 (Canvas_t2727140764 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_renderMode_m3669121200_ftn) (Canvas_t2727140764 *);
	static Canvas_get_renderMode_m3669121200_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_renderMode_m3669121200_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_renderMode()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Canvas::get_isRootCanvas()
extern "C"  bool Canvas_get_isRootCanvas_m4030632706 (Canvas_t2727140764 * __this, const MethodInfo* method)
{
	typedef bool (*Canvas_get_isRootCanvas_m4030632706_ftn) (Canvas_t2727140764 *);
	static Canvas_get_isRootCanvas_m4030632706_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_isRootCanvas_m4030632706_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_isRootCanvas()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Camera UnityEngine.Canvas::get_worldCamera()
extern "C"  Camera_t2727095145 * Canvas_get_worldCamera_m2621449230 (Canvas_t2727140764 * __this, const MethodInfo* method)
{
	typedef Camera_t2727095145 * (*Canvas_get_worldCamera_m2621449230_ftn) (Canvas_t2727140764 *);
	static Canvas_get_worldCamera_m2621449230_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_worldCamera_m2621449230_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_worldCamera()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Canvas::get_scaleFactor()
extern "C"  float Canvas_get_scaleFactor_m1187077271 (Canvas_t2727140764 * __this, const MethodInfo* method)
{
	typedef float (*Canvas_get_scaleFactor_m1187077271_ftn) (Canvas_t2727140764 *);
	static Canvas_get_scaleFactor_m1187077271_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_scaleFactor_m1187077271_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_scaleFactor()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_scaleFactor(System.Single)
extern "C"  void Canvas_set_scaleFactor_m2206436828 (Canvas_t2727140764 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Canvas_set_scaleFactor_m2206436828_ftn) (Canvas_t2727140764 *, float);
	static Canvas_set_scaleFactor_m2206436828_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_scaleFactor_m2206436828_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_scaleFactor(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Canvas::get_referencePixelsPerUnit()
extern "C"  float Canvas_get_referencePixelsPerUnit_m3759589197 (Canvas_t2727140764 * __this, const MethodInfo* method)
{
	typedef float (*Canvas_get_referencePixelsPerUnit_m3759589197_ftn) (Canvas_t2727140764 *);
	static Canvas_get_referencePixelsPerUnit_m3759589197_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_referencePixelsPerUnit_m3759589197_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_referencePixelsPerUnit()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_referencePixelsPerUnit(System.Single)
extern "C"  void Canvas_set_referencePixelsPerUnit_m1910706966 (Canvas_t2727140764 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Canvas_set_referencePixelsPerUnit_m1910706966_ftn) (Canvas_t2727140764 *, float);
	static Canvas_set_referencePixelsPerUnit_m1910706966_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_referencePixelsPerUnit_m1910706966_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_referencePixelsPerUnit(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Canvas::get_pixelPerfect()
extern "C"  bool Canvas_get_pixelPerfect_m2847994693 (Canvas_t2727140764 * __this, const MethodInfo* method)
{
	typedef bool (*Canvas_get_pixelPerfect_m2847994693_ftn) (Canvas_t2727140764 *);
	static Canvas_get_pixelPerfect_m2847994693_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_pixelPerfect_m2847994693_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_pixelPerfect()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Canvas::get_renderOrder()
extern "C"  int32_t Canvas_get_renderOrder_m2295969794 (Canvas_t2727140764 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_renderOrder_m2295969794_ftn) (Canvas_t2727140764 *);
	static Canvas_get_renderOrder_m2295969794_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_renderOrder_m2295969794_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_renderOrder()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Canvas::get_overrideSorting()
extern "C"  bool Canvas_get_overrideSorting_m3812884636 (Canvas_t2727140764 * __this, const MethodInfo* method)
{
	typedef bool (*Canvas_get_overrideSorting_m3812884636_ftn) (Canvas_t2727140764 *);
	static Canvas_get_overrideSorting_m3812884636_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_overrideSorting_m3812884636_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_overrideSorting()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_overrideSorting(System.Boolean)
extern "C"  void Canvas_set_overrideSorting_m1850487749 (Canvas_t2727140764 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Canvas_set_overrideSorting_m1850487749_ftn) (Canvas_t2727140764 *, bool);
	static Canvas_set_overrideSorting_m1850487749_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_overrideSorting_m1850487749_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_overrideSorting(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.Canvas::get_sortingOrder()
extern "C"  int32_t Canvas_get_sortingOrder_m1274160674 (Canvas_t2727140764 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_sortingOrder_m1274160674_ftn) (Canvas_t2727140764 *);
	static Canvas_get_sortingOrder_m1274160674_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_sortingOrder_m1274160674_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_sortingOrder()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_sortingOrder(System.Int32)
extern "C"  void Canvas_set_sortingOrder_m1522807655 (Canvas_t2727140764 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Canvas_set_sortingOrder_m1522807655_ftn) (Canvas_t2727140764 *, int32_t);
	static Canvas_set_sortingOrder_m1522807655_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_sortingOrder_m1522807655_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_sortingOrder(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.Canvas::get_sortingLayerID()
extern "C"  int32_t Canvas_get_sortingLayerID_m3425838272 (Canvas_t2727140764 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_sortingLayerID_m3425838272_ftn) (Canvas_t2727140764 *);
	static Canvas_get_sortingLayerID_m3425838272_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_sortingLayerID_m3425838272_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_sortingLayerID()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_sortingLayerID(System.Int32)
extern "C"  void Canvas_set_sortingLayerID_m2317502469 (Canvas_t2727140764 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Canvas_set_sortingLayerID_m2317502469_ftn) (Canvas_t2727140764 *, int32_t);
	static Canvas_set_sortingLayerID_m2317502469_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_sortingLayerID_m2317502469_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_sortingLayerID(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Canvas UnityEngine.Canvas::get_rootCanvas()
extern "C"  Canvas_t2727140764 * Canvas_get_rootCanvas_m3652460242 (Canvas_t2727140764 * __this, const MethodInfo* method)
{
	typedef Canvas_t2727140764 * (*Canvas_get_rootCanvas_m3652460242_ftn) (Canvas_t2727140764 *);
	static Canvas_get_rootCanvas_m3652460242_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_rootCanvas_m3652460242_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_rootCanvas()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Material UnityEngine.Canvas::GetDefaultCanvasMaterial()
extern "C"  Material_t3870600107 * Canvas_GetDefaultCanvasMaterial_m1655363178 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef Material_t3870600107 * (*Canvas_GetDefaultCanvasMaterial_m1655363178_ftn) ();
	static Canvas_GetDefaultCanvasMaterial_m1655363178_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_GetDefaultCanvasMaterial_m1655363178_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::GetDefaultCanvasMaterial()");
	return _il2cpp_icall_func();
}
// UnityEngine.Material UnityEngine.Canvas::GetETC1SupportedCanvasMaterial()
extern "C"  Material_t3870600107 * Canvas_GetETC1SupportedCanvasMaterial_m91460986 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef Material_t3870600107 * (*Canvas_GetETC1SupportedCanvasMaterial_m91460986_ftn) ();
	static Canvas_GetETC1SupportedCanvasMaterial_m91460986_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_GetETC1SupportedCanvasMaterial_m91460986_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::GetETC1SupportedCanvasMaterial()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Canvas::SendWillRenderCanvases()
extern Il2CppClass* Canvas_t2727140764_il2cpp_TypeInfo_var;
extern const uint32_t Canvas_SendWillRenderCanvases_m1768133093_MetadataUsageId;
extern "C"  void Canvas_SendWillRenderCanvases_m1768133093 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Canvas_SendWillRenderCanvases_m1768133093_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		WillRenderCanvases_t4247149838 * L_0 = ((Canvas_t2727140764_StaticFields*)Canvas_t2727140764_il2cpp_TypeInfo_var->static_fields)->get_willRenderCanvases_2();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		WillRenderCanvases_t4247149838 * L_1 = ((Canvas_t2727140764_StaticFields*)Canvas_t2727140764_il2cpp_TypeInfo_var->static_fields)->get_willRenderCanvases_2();
		NullCheck(L_1);
		WillRenderCanvases_Invoke_m4113031528(L_1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void UnityEngine.Canvas::ForceUpdateCanvases()
extern "C"  void Canvas_ForceUpdateCanvases_m1970133101 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Canvas_SendWillRenderCanvases_m1768133093(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Canvas/WillRenderCanvases::.ctor(System.Object,System.IntPtr)
extern "C"  void WillRenderCanvases__ctor_m1509285774 (WillRenderCanvases_t4247149838 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Canvas/WillRenderCanvases::Invoke()
extern "C"  void WillRenderCanvases_Invoke_m4113031528 (WillRenderCanvases_t4247149838 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		WillRenderCanvases_Invoke_m4113031528((WillRenderCanvases_t4247149838 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_WillRenderCanvases_t4247149838 (WillRenderCanvases_t4247149838 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.IAsyncResult UnityEngine.Canvas/WillRenderCanvases::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * WillRenderCanvases_BeginInvoke_m205611811 (WillRenderCanvases_t4247149838 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void UnityEngine.Canvas/WillRenderCanvases::EndInvoke(System.IAsyncResult)
extern "C"  void WillRenderCanvases_EndInvoke_m2320713886 (WillRenderCanvases_t4247149838 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Single UnityEngine.CanvasGroup::get_alpha()
extern "C"  float CanvasGroup_get_alpha_m2646612977 (CanvasGroup_t3702418109 * __this, const MethodInfo* method)
{
	typedef float (*CanvasGroup_get_alpha_m2646612977_ftn) (CanvasGroup_t3702418109 *);
	static CanvasGroup_get_alpha_m2646612977_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_alpha_m2646612977_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_alpha()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasGroup::set_alpha(System.Single)
extern "C"  void CanvasGroup_set_alpha_m2140801370 (CanvasGroup_t3702418109 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*CanvasGroup_set_alpha_m2140801370_ftn) (CanvasGroup_t3702418109 *, float);
	static CanvasGroup_set_alpha_m2140801370_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_set_alpha_m2140801370_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::set_alpha(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.CanvasGroup::get_interactable()
extern "C"  bool CanvasGroup_get_interactable_m2411844645 (CanvasGroup_t3702418109 * __this, const MethodInfo* method)
{
	typedef bool (*CanvasGroup_get_interactable_m2411844645_ftn) (CanvasGroup_t3702418109 *);
	static CanvasGroup_get_interactable_m2411844645_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_interactable_m2411844645_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_interactable()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasGroup::set_interactable(System.Boolean)
extern "C"  void CanvasGroup_set_interactable_m2570638198 (CanvasGroup_t3702418109 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*CanvasGroup_set_interactable_m2570638198_ftn) (CanvasGroup_t3702418109 *, bool);
	static CanvasGroup_set_interactable_m2570638198_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_set_interactable_m2570638198_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::set_interactable(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.CanvasGroup::get_blocksRaycasts()
extern "C"  bool CanvasGroup_get_blocksRaycasts_m1777916261 (CanvasGroup_t3702418109 * __this, const MethodInfo* method)
{
	typedef bool (*CanvasGroup_get_blocksRaycasts_m1777916261_ftn) (CanvasGroup_t3702418109 *);
	static CanvasGroup_get_blocksRaycasts_m1777916261_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_blocksRaycasts_m1777916261_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_blocksRaycasts()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasGroup::set_blocksRaycasts(System.Boolean)
extern "C"  void CanvasGroup_set_blocksRaycasts_m4087114294 (CanvasGroup_t3702418109 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*CanvasGroup_set_blocksRaycasts_m4087114294_ftn) (CanvasGroup_t3702418109 *, bool);
	static CanvasGroup_set_blocksRaycasts_m4087114294_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_set_blocksRaycasts_m4087114294_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::set_blocksRaycasts(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.CanvasGroup::get_ignoreParentGroups()
extern "C"  bool CanvasGroup_get_ignoreParentGroups_m1831887525 (CanvasGroup_t3702418109 * __this, const MethodInfo* method)
{
	typedef bool (*CanvasGroup_get_ignoreParentGroups_m1831887525_ftn) (CanvasGroup_t3702418109 *);
	static CanvasGroup_get_ignoreParentGroups_m1831887525_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_ignoreParentGroups_m1831887525_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_ignoreParentGroups()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.CanvasGroup::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
extern "C"  bool CanvasGroup_IsRaycastLocationValid_m3946913220 (CanvasGroup_t3702418109 * __this, Vector2_t4282066565  ___sp0, Camera_t2727095145 * ___eventCamera1, const MethodInfo* method)
{
	{
		bool L_0 = CanvasGroup_get_blocksRaycasts_m1777916261(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.CanvasRenderer::SetColor(UnityEngine.Color)
extern "C"  void CanvasRenderer_SetColor_m3198078515 (CanvasRenderer_t3950887807 * __this, Color_t4194546905  ___color0, const MethodInfo* method)
{
	{
		CanvasRenderer_INTERNAL_CALL_SetColor_m2907146764(NULL /*static, unused*/, __this, (&___color0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_SetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)
extern "C"  void CanvasRenderer_INTERNAL_CALL_SetColor_m2907146764 (Il2CppObject * __this /* static, unused */, CanvasRenderer_t3950887807 * ___self0, Color_t4194546905 * ___color1, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_INTERNAL_CALL_SetColor_m2907146764_ftn) (CanvasRenderer_t3950887807 *, Color_t4194546905 *);
	static CanvasRenderer_INTERNAL_CALL_SetColor_m2907146764_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_INTERNAL_CALL_SetColor_m2907146764_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::INTERNAL_CALL_SetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)");
	_il2cpp_icall_func(___self0, ___color1);
}
// UnityEngine.Color UnityEngine.CanvasRenderer::GetColor()
extern "C"  Color_t4194546905  CanvasRenderer_GetColor_m3075393702 (CanvasRenderer_t3950887807 * __this, const MethodInfo* method)
{
	Color_t4194546905  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		CanvasRenderer_INTERNAL_CALL_GetColor_m3176368896(NULL /*static, unused*/, __this, (&V_0), /*hidden argument*/NULL);
		Color_t4194546905  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_GetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)
extern "C"  void CanvasRenderer_INTERNAL_CALL_GetColor_m3176368896 (Il2CppObject * __this /* static, unused */, CanvasRenderer_t3950887807 * ___self0, Color_t4194546905 * ___value1, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_INTERNAL_CALL_GetColor_m3176368896_ftn) (CanvasRenderer_t3950887807 *, Color_t4194546905 *);
	static CanvasRenderer_INTERNAL_CALL_GetColor_m3176368896_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_INTERNAL_CALL_GetColor_m3176368896_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::INTERNAL_CALL_GetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)");
	_il2cpp_icall_func(___self0, ___value1);
}
// System.Void UnityEngine.CanvasRenderer::EnableRectClipping(UnityEngine.Rect)
extern "C"  void CanvasRenderer_EnableRectClipping_m896218272 (CanvasRenderer_t3950887807 * __this, Rect_t4241904616  ___rect0, const MethodInfo* method)
{
	{
		CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m105013257(NULL /*static, unused*/, __this, (&___rect0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_EnableRectClipping(UnityEngine.CanvasRenderer,UnityEngine.Rect&)
extern "C"  void CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m105013257 (Il2CppObject * __this /* static, unused */, CanvasRenderer_t3950887807 * ___self0, Rect_t4241904616 * ___rect1, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m105013257_ftn) (CanvasRenderer_t3950887807 *, Rect_t4241904616 *);
	static CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m105013257_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m105013257_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::INTERNAL_CALL_EnableRectClipping(UnityEngine.CanvasRenderer,UnityEngine.Rect&)");
	_il2cpp_icall_func(___self0, ___rect1);
}
// System.Void UnityEngine.CanvasRenderer::DisableRectClipping()
extern "C"  void CanvasRenderer_DisableRectClipping_m2654388030 (CanvasRenderer_t3950887807 * __this, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_DisableRectClipping_m2654388030_ftn) (CanvasRenderer_t3950887807 *);
	static CanvasRenderer_DisableRectClipping_m2654388030_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_DisableRectClipping_m2654388030_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::DisableRectClipping()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasRenderer::set_hasPopInstruction(System.Boolean)
extern "C"  void CanvasRenderer_set_hasPopInstruction_m2112255713 (CanvasRenderer_t3950887807 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_set_hasPopInstruction_m2112255713_ftn) (CanvasRenderer_t3950887807 *, bool);
	static CanvasRenderer_set_hasPopInstruction_m2112255713_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_set_hasPopInstruction_m2112255713_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::set_hasPopInstruction(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.CanvasRenderer::get_materialCount()
extern "C"  int32_t CanvasRenderer_get_materialCount_m311942031 (CanvasRenderer_t3950887807 * __this, const MethodInfo* method)
{
	typedef int32_t (*CanvasRenderer_get_materialCount_m311942031_ftn) (CanvasRenderer_t3950887807 *);
	static CanvasRenderer_get_materialCount_m311942031_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_get_materialCount_m311942031_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::get_materialCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasRenderer::set_materialCount(System.Int32)
extern "C"  void CanvasRenderer_set_materialCount_m1694897644 (CanvasRenderer_t3950887807 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_set_materialCount_m1694897644_ftn) (CanvasRenderer_t3950887807 *, int32_t);
	static CanvasRenderer_set_materialCount_m1694897644_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_set_materialCount_m1694897644_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::set_materialCount(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,System.Int32)
extern "C"  void CanvasRenderer_SetMaterial_m355382012 (CanvasRenderer_t3950887807 * __this, Material_t3870600107 * ___material0, int32_t ___index1, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetMaterial_m355382012_ftn) (CanvasRenderer_t3950887807 *, Material_t3870600107 *, int32_t);
	static CanvasRenderer_SetMaterial_m355382012_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetMaterial_m355382012_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,System.Int32)");
	_il2cpp_icall_func(__this, ___material0, ___index1);
}
// System.Void UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,UnityEngine.Texture)
extern "C"  void CanvasRenderer_SetMaterial_m1603188579 (CanvasRenderer_t3950887807 * __this, Material_t3870600107 * ___material0, Texture_t2526458961 * ___texture1, const MethodInfo* method)
{
	{
		int32_t L_0 = CanvasRenderer_get_materialCount_m311942031(__this, /*hidden argument*/NULL);
		int32_t L_1 = Math_Max_m1309380475(NULL /*static, unused*/, 1, L_0, /*hidden argument*/NULL);
		CanvasRenderer_set_materialCount_m1694897644(__this, L_1, /*hidden argument*/NULL);
		Material_t3870600107 * L_2 = ___material0;
		CanvasRenderer_SetMaterial_m355382012(__this, L_2, 0, /*hidden argument*/NULL);
		Texture_t2526458961 * L_3 = ___texture1;
		CanvasRenderer_SetTexture_m137327939(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::set_popMaterialCount(System.Int32)
extern "C"  void CanvasRenderer_set_popMaterialCount_m589784503 (CanvasRenderer_t3950887807 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_set_popMaterialCount_m589784503_ftn) (CanvasRenderer_t3950887807 *, int32_t);
	static CanvasRenderer_set_popMaterialCount_m589784503_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_set_popMaterialCount_m589784503_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::set_popMaterialCount(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.CanvasRenderer::SetPopMaterial(UnityEngine.Material,System.Int32)
extern "C"  void CanvasRenderer_SetPopMaterial_m1315408685 (CanvasRenderer_t3950887807 * __this, Material_t3870600107 * ___material0, int32_t ___index1, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetPopMaterial_m1315408685_ftn) (CanvasRenderer_t3950887807 *, Material_t3870600107 *, int32_t);
	static CanvasRenderer_SetPopMaterial_m1315408685_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetPopMaterial_m1315408685_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetPopMaterial(UnityEngine.Material,System.Int32)");
	_il2cpp_icall_func(__this, ___material0, ___index1);
}
// System.Void UnityEngine.CanvasRenderer::SetTexture(UnityEngine.Texture)
extern "C"  void CanvasRenderer_SetTexture_m137327939 (CanvasRenderer_t3950887807 * __this, Texture_t2526458961 * ___texture0, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetTexture_m137327939_ftn) (CanvasRenderer_t3950887807 *, Texture_t2526458961 *);
	static CanvasRenderer_SetTexture_m137327939_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetTexture_m137327939_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetTexture(UnityEngine.Texture)");
	_il2cpp_icall_func(__this, ___texture0);
}
// System.Void UnityEngine.CanvasRenderer::SetAlphaTexture(UnityEngine.Texture)
extern "C"  void CanvasRenderer_SetAlphaTexture_m2838514431 (CanvasRenderer_t3950887807 * __this, Texture_t2526458961 * ___texture0, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetAlphaTexture_m2838514431_ftn) (CanvasRenderer_t3950887807 *, Texture_t2526458961 *);
	static CanvasRenderer_SetAlphaTexture_m2838514431_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetAlphaTexture_m2838514431_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetAlphaTexture(UnityEngine.Texture)");
	_il2cpp_icall_func(__this, ___texture0);
}
// System.Void UnityEngine.CanvasRenderer::SetMesh(UnityEngine.Mesh)
extern "C"  void CanvasRenderer_SetMesh_m1150548475 (CanvasRenderer_t3950887807 * __this, Mesh_t4241756145 * ___mesh0, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetMesh_m1150548475_ftn) (CanvasRenderer_t3950887807 *, Mesh_t4241756145 *);
	static CanvasRenderer_SetMesh_m1150548475_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetMesh_m1150548475_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetMesh(UnityEngine.Mesh)");
	_il2cpp_icall_func(__this, ___mesh0);
}
// System.Void UnityEngine.CanvasRenderer::Clear()
extern "C"  void CanvasRenderer_Clear_m1606508093 (CanvasRenderer_t3950887807 * __this, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_Clear_m1606508093_ftn) (CanvasRenderer_t3950887807 *);
	static CanvasRenderer_Clear_m1606508093_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_Clear_m1606508093_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::Clear()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasRenderer::SplitUIVertexStreams(System.Collections.Generic.List`1<UnityEngine.UIVertex>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Color32>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector4>,System.Collections.Generic.List`1<System.Int32>)
extern "C"  void CanvasRenderer_SplitUIVertexStreams_m4126093916 (Il2CppObject * __this /* static, unused */, List_1_t1317283468 * ___verts0, List_1_t1355284822 * ___positions1, List_1_t1967039240 * ___colors2, List_1_t1355284821 * ___uv0S3, List_1_t1355284821 * ___uv1S4, List_1_t1355284822 * ___normals5, List_1_t1355284823 * ___tangents6, List_1_t2522024052 * ___indicies7, const MethodInfo* method)
{
	{
		List_1_t1317283468 * L_0 = ___verts0;
		List_1_t1355284822 * L_1 = ___positions1;
		List_1_t1967039240 * L_2 = ___colors2;
		List_1_t1355284821 * L_3 = ___uv0S3;
		List_1_t1355284821 * L_4 = ___uv1S4;
		List_1_t1355284822 * L_5 = ___normals5;
		List_1_t1355284823 * L_6 = ___tangents6;
		CanvasRenderer_SplitUIVertexStreamsInternal_m987675768(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		List_1_t1317283468 * L_7 = ___verts0;
		List_1_t2522024052 * L_8 = ___indicies7;
		CanvasRenderer_SplitIndiciesStreamsInternal_m269466040(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::SplitUIVertexStreamsInternal(System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object)
extern "C"  void CanvasRenderer_SplitUIVertexStreamsInternal_m987675768 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___verts0, Il2CppObject * ___positions1, Il2CppObject * ___colors2, Il2CppObject * ___uv0S3, Il2CppObject * ___uv1S4, Il2CppObject * ___normals5, Il2CppObject * ___tangents6, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SplitUIVertexStreamsInternal_m987675768_ftn) (Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *);
	static CanvasRenderer_SplitUIVertexStreamsInternal_m987675768_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SplitUIVertexStreamsInternal_m987675768_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SplitUIVertexStreamsInternal(System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object)");
	_il2cpp_icall_func(___verts0, ___positions1, ___colors2, ___uv0S3, ___uv1S4, ___normals5, ___tangents6);
}
// System.Void UnityEngine.CanvasRenderer::SplitIndiciesStreamsInternal(System.Object,System.Object)
extern "C"  void CanvasRenderer_SplitIndiciesStreamsInternal_m269466040 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___verts0, Il2CppObject * ___indicies1, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SplitIndiciesStreamsInternal_m269466040_ftn) (Il2CppObject *, Il2CppObject *);
	static CanvasRenderer_SplitIndiciesStreamsInternal_m269466040_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SplitIndiciesStreamsInternal_m269466040_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SplitIndiciesStreamsInternal(System.Object,System.Object)");
	_il2cpp_icall_func(___verts0, ___indicies1);
}
// System.Void UnityEngine.CanvasRenderer::CreateUIVertexStream(System.Collections.Generic.List`1<UnityEngine.UIVertex>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Color32>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector4>,System.Collections.Generic.List`1<System.Int32>)
extern "C"  void CanvasRenderer_CreateUIVertexStream_m2702356137 (Il2CppObject * __this /* static, unused */, List_1_t1317283468 * ___verts0, List_1_t1355284822 * ___positions1, List_1_t1967039240 * ___colors2, List_1_t1355284821 * ___uv0S3, List_1_t1355284821 * ___uv1S4, List_1_t1355284822 * ___normals5, List_1_t1355284823 * ___tangents6, List_1_t2522024052 * ___indicies7, const MethodInfo* method)
{
	{
		List_1_t1317283468 * L_0 = ___verts0;
		List_1_t1355284822 * L_1 = ___positions1;
		List_1_t1967039240 * L_2 = ___colors2;
		List_1_t1355284821 * L_3 = ___uv0S3;
		List_1_t1355284821 * L_4 = ___uv1S4;
		List_1_t1355284822 * L_5 = ___normals5;
		List_1_t1355284823 * L_6 = ___tangents6;
		List_1_t2522024052 * L_7 = ___indicies7;
		CanvasRenderer_CreateUIVertexStreamInternal_m997370835(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::CreateUIVertexStreamInternal(System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object)
extern "C"  void CanvasRenderer_CreateUIVertexStreamInternal_m997370835 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___verts0, Il2CppObject * ___positions1, Il2CppObject * ___colors2, Il2CppObject * ___uv0S3, Il2CppObject * ___uv1S4, Il2CppObject * ___normals5, Il2CppObject * ___tangents6, Il2CppObject * ___indicies7, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_CreateUIVertexStreamInternal_m997370835_ftn) (Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *);
	static CanvasRenderer_CreateUIVertexStreamInternal_m997370835_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_CreateUIVertexStreamInternal_m997370835_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::CreateUIVertexStreamInternal(System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object)");
	_il2cpp_icall_func(___verts0, ___positions1, ___colors2, ___uv0S3, ___uv1S4, ___normals5, ___tangents6, ___indicies7);
}
// System.Void UnityEngine.CanvasRenderer::AddUIVertexStream(System.Collections.Generic.List`1<UnityEngine.UIVertex>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Color32>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern "C"  void CanvasRenderer_AddUIVertexStream_m1865056747 (Il2CppObject * __this /* static, unused */, List_1_t1317283468 * ___verts0, List_1_t1355284822 * ___positions1, List_1_t1967039240 * ___colors2, List_1_t1355284821 * ___uv0S3, List_1_t1355284821 * ___uv1S4, List_1_t1355284822 * ___normals5, List_1_t1355284823 * ___tangents6, const MethodInfo* method)
{
	{
		List_1_t1317283468 * L_0 = ___verts0;
		List_1_t1355284822 * L_1 = ___positions1;
		List_1_t1967039240 * L_2 = ___colors2;
		List_1_t1355284821 * L_3 = ___uv0S3;
		List_1_t1355284821 * L_4 = ___uv1S4;
		List_1_t1355284822 * L_5 = ___normals5;
		List_1_t1355284823 * L_6 = ___tangents6;
		CanvasRenderer_SplitUIVertexStreamsInternal_m987675768(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.CanvasRenderer::get_cull()
extern "C"  bool CanvasRenderer_get_cull_m3343855795 (CanvasRenderer_t3950887807 * __this, const MethodInfo* method)
{
	typedef bool (*CanvasRenderer_get_cull_m3343855795_ftn) (CanvasRenderer_t3950887807 *);
	static CanvasRenderer_get_cull_m3343855795_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_get_cull_m3343855795_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::get_cull()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasRenderer::set_cull(System.Boolean)
extern "C"  void CanvasRenderer_set_cull_m3433952120 (CanvasRenderer_t3950887807 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_set_cull_m3433952120_ftn) (CanvasRenderer_t3950887807 *, bool);
	static CanvasRenderer_set_cull_m3433952120_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_set_cull_m3433952120_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::set_cull(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.CanvasRenderer::get_absoluteDepth()
extern "C"  int32_t CanvasRenderer_get_absoluteDepth_m1613567475 (CanvasRenderer_t3950887807 * __this, const MethodInfo* method)
{
	typedef int32_t (*CanvasRenderer_get_absoluteDepth_m1613567475_ftn) (CanvasRenderer_t3950887807 *);
	static CanvasRenderer_get_absoluteDepth_m1613567475_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_get_absoluteDepth_m1613567475_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::get_absoluteDepth()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.CanvasRenderer::get_hasMoved()
extern "C"  bool CanvasRenderer_get_hasMoved_m1392755130 (CanvasRenderer_t3950887807 * __this, const MethodInfo* method)
{
	typedef bool (*CanvasRenderer_get_hasMoved_m1392755130_ftn) (CanvasRenderer_t3950887807 *);
	static CanvasRenderer_get_hasMoved_m1392755130_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_get_hasMoved_m1392755130_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::get_hasMoved()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Vector3 UnityEngine.CapsuleCollider::get_center()
extern "C"  Vector3_t4282066566  CapsuleCollider_get_center_m797170256 (CapsuleCollider_t318617463 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		CapsuleCollider_INTERNAL_get_center_m2426204393(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.CapsuleCollider::set_center(UnityEngine.Vector3)
extern "C"  void CapsuleCollider_set_center_m3578407707 (CapsuleCollider_t318617463 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		CapsuleCollider_INTERNAL_set_center_m4074762077(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CapsuleCollider::INTERNAL_get_center(UnityEngine.Vector3&)
extern "C"  void CapsuleCollider_INTERNAL_get_center_m2426204393 (CapsuleCollider_t318617463 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*CapsuleCollider_INTERNAL_get_center_m2426204393_ftn) (CapsuleCollider_t318617463 *, Vector3_t4282066566 *);
	static CapsuleCollider_INTERNAL_get_center_m2426204393_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CapsuleCollider_INTERNAL_get_center_m2426204393_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CapsuleCollider::INTERNAL_get_center(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.CapsuleCollider::INTERNAL_set_center(UnityEngine.Vector3&)
extern "C"  void CapsuleCollider_INTERNAL_set_center_m4074762077 (CapsuleCollider_t318617463 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*CapsuleCollider_INTERNAL_set_center_m4074762077_ftn) (CapsuleCollider_t318617463 *, Vector3_t4282066566 *);
	static CapsuleCollider_INTERNAL_set_center_m4074762077_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CapsuleCollider_INTERNAL_set_center_m4074762077_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CapsuleCollider::INTERNAL_set_center(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.CapsuleCollider::get_radius()
extern "C"  float CapsuleCollider_get_radius_m1913982715 (CapsuleCollider_t318617463 * __this, const MethodInfo* method)
{
	typedef float (*CapsuleCollider_get_radius_m1913982715_ftn) (CapsuleCollider_t318617463 *);
	static CapsuleCollider_get_radius_m1913982715_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CapsuleCollider_get_radius_m1913982715_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CapsuleCollider::get_radius()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CapsuleCollider::set_radius(System.Single)
extern "C"  void CapsuleCollider_set_radius_m447632528 (CapsuleCollider_t318617463 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*CapsuleCollider_set_radius_m447632528_ftn) (CapsuleCollider_t318617463 *, float);
	static CapsuleCollider_set_radius_m447632528_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CapsuleCollider_set_radius_m447632528_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CapsuleCollider::set_radius(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.CapsuleCollider::get_height()
extern "C"  float CapsuleCollider_get_height_m1061708368 (CapsuleCollider_t318617463 * __this, const MethodInfo* method)
{
	typedef float (*CapsuleCollider_get_height_m1061708368_ftn) (CapsuleCollider_t318617463 *);
	static CapsuleCollider_get_height_m1061708368_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CapsuleCollider_get_height_m1061708368_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CapsuleCollider::get_height()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CapsuleCollider::set_height(System.Single)
extern "C"  void CapsuleCollider_set_height_m2136855387 (CapsuleCollider_t318617463 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*CapsuleCollider_set_height_m2136855387_ftn) (CapsuleCollider_t318617463 *, float);
	static CapsuleCollider_set_height_m2136855387_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CapsuleCollider_set_height_m2136855387_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CapsuleCollider::set_height(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.CapsuleCollider::get_direction()
extern "C"  int32_t CapsuleCollider_get_direction_m3913411916 (CapsuleCollider_t318617463 * __this, const MethodInfo* method)
{
	typedef int32_t (*CapsuleCollider_get_direction_m3913411916_ftn) (CapsuleCollider_t318617463 *);
	static CapsuleCollider_get_direction_m3913411916_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CapsuleCollider_get_direction_m3913411916_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CapsuleCollider::get_direction()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CapsuleCollider::set_direction(System.Int32)
extern "C"  void CapsuleCollider_set_direction_m2435033385 (CapsuleCollider_t318617463 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*CapsuleCollider_set_direction_m2435033385_ftn) (CapsuleCollider_t318617463 *, int32_t);
	static CapsuleCollider_set_direction_m2435033385_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CapsuleCollider_set_direction_m2435033385_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CapsuleCollider::set_direction(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.CharacterController::SimpleMove(UnityEngine.Vector3)
extern "C"  bool CharacterController_SimpleMove_m3593592780 (CharacterController_t1618060635 * __this, Vector3_t4282066566  ___speed0, const MethodInfo* method)
{
	{
		bool L_0 = CharacterController_INTERNAL_CALL_SimpleMove_m34016609(NULL /*static, unused*/, __this, (&___speed0), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.CharacterController::INTERNAL_CALL_SimpleMove(UnityEngine.CharacterController,UnityEngine.Vector3&)
extern "C"  bool CharacterController_INTERNAL_CALL_SimpleMove_m34016609 (Il2CppObject * __this /* static, unused */, CharacterController_t1618060635 * ___self0, Vector3_t4282066566 * ___speed1, const MethodInfo* method)
{
	typedef bool (*CharacterController_INTERNAL_CALL_SimpleMove_m34016609_ftn) (CharacterController_t1618060635 *, Vector3_t4282066566 *);
	static CharacterController_INTERNAL_CALL_SimpleMove_m34016609_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterController_INTERNAL_CALL_SimpleMove_m34016609_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterController::INTERNAL_CALL_SimpleMove(UnityEngine.CharacterController,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self0, ___speed1);
}
// UnityEngine.CollisionFlags UnityEngine.CharacterController::Move(UnityEngine.Vector3)
extern "C"  int32_t CharacterController_Move_m3043020731 (CharacterController_t1618060635 * __this, Vector3_t4282066566  ___motion0, const MethodInfo* method)
{
	{
		int32_t L_0 = CharacterController_INTERNAL_CALL_Move_m985801042(NULL /*static, unused*/, __this, (&___motion0), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.CollisionFlags UnityEngine.CharacterController::INTERNAL_CALL_Move(UnityEngine.CharacterController,UnityEngine.Vector3&)
extern "C"  int32_t CharacterController_INTERNAL_CALL_Move_m985801042 (Il2CppObject * __this /* static, unused */, CharacterController_t1618060635 * ___self0, Vector3_t4282066566 * ___motion1, const MethodInfo* method)
{
	typedef int32_t (*CharacterController_INTERNAL_CALL_Move_m985801042_ftn) (CharacterController_t1618060635 *, Vector3_t4282066566 *);
	static CharacterController_INTERNAL_CALL_Move_m985801042_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterController_INTERNAL_CALL_Move_m985801042_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterController::INTERNAL_CALL_Move(UnityEngine.CharacterController,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self0, ___motion1);
}
// System.Boolean UnityEngine.CharacterController::get_isGrounded()
extern "C"  bool CharacterController_get_isGrounded_m1739295843 (CharacterController_t1618060635 * __this, const MethodInfo* method)
{
	typedef bool (*CharacterController_get_isGrounded_m1739295843_ftn) (CharacterController_t1618060635 *);
	static CharacterController_get_isGrounded_m1739295843_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterController_get_isGrounded_m1739295843_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterController::get_isGrounded()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.CollisionFlags UnityEngine.CharacterController::get_collisionFlags()
extern "C"  int32_t CharacterController_get_collisionFlags_m3620825515 (CharacterController_t1618060635 * __this, const MethodInfo* method)
{
	typedef int32_t (*CharacterController_get_collisionFlags_m3620825515_ftn) (CharacterController_t1618060635 *);
	static CharacterController_get_collisionFlags_m3620825515_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterController_get_collisionFlags_m3620825515_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterController::get_collisionFlags()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CharacterController::set_radius(System.Single)
extern "C"  void CharacterController_set_radius_m1330958508 (CharacterController_t1618060635 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*CharacterController_set_radius_m1330958508_ftn) (CharacterController_t1618060635 *, float);
	static CharacterController_set_radius_m1330958508_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterController_set_radius_m1330958508_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterController::set_radius(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.CharacterController::set_height(System.Single)
extern "C"  void CharacterController_set_height_m3020181367 (CharacterController_t1618060635 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*CharacterController_set_height_m3020181367_ftn) (CharacterController_t1618060635 *, float);
	static CharacterController_set_height_m3020181367_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterController_set_height_m3020181367_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterController::set_height(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.CharacterController::set_center(UnityEngine.Vector3)
extern "C"  void CharacterController_set_center_m3276939831 (CharacterController_t1618060635 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		CharacterController_INTERNAL_set_center_m899855225(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CharacterController::INTERNAL_set_center(UnityEngine.Vector3&)
extern "C"  void CharacterController_INTERNAL_set_center_m899855225 (CharacterController_t1618060635 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*CharacterController_INTERNAL_set_center_m899855225_ftn) (CharacterController_t1618060635 *, Vector3_t4282066566 *);
	static CharacterController_INTERNAL_set_center_m899855225_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterController_INTERNAL_set_center_m899855225_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterController::INTERNAL_set_center(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.CharacterController::set_slopeLimit(System.Single)
extern "C"  void CharacterController_set_slopeLimit_m1749870254 (CharacterController_t1618060635 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*CharacterController_set_slopeLimit_m1749870254_ftn) (CharacterController_t1618060635 *, float);
	static CharacterController_set_slopeLimit_m1749870254_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterController_set_slopeLimit_m1749870254_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterController::set_slopeLimit(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.CharacterController::set_stepOffset(System.Single)
extern "C"  void CharacterController_set_stepOffset_m3279469855 (CharacterController_t1618060635 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*CharacterController_set_stepOffset_m3279469855_ftn) (CharacterController_t1618060635 *, float);
	static CharacterController_set_stepOffset_m3279469855_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterController_set_stepOffset_m3279469855_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterController::set_stepOffset(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.CharacterController::set_detectCollisions(System.Boolean)
extern "C"  void CharacterController_set_detectCollisions_m3520854408 (CharacterController_t1618060635 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*CharacterController_set_detectCollisions_m3520854408_ftn) (CharacterController_t1618060635 *, bool);
	static CharacterController_set_detectCollisions_m3520854408_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterController_set_detectCollisions_m3520854408_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterController::set_detectCollisions(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Cloth::set_enabled(System.Boolean)
extern "C"  void Cloth_set_enabled_m134879832 (Cloth_t4194460560 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Cloth_set_enabled_m134879832_ftn) (Cloth_t4194460560 *, bool);
	static Cloth_set_enabled_m134879832_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Cloth_set_enabled_m134879832_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Cloth::set_enabled(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Collider::set_enabled(System.Boolean)
extern "C"  void Collider_set_enabled_m2575670866 (Collider_t2939674232 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Collider_set_enabled_m2575670866_ftn) (Collider_t2939674232 *, bool);
	static Collider_set_enabled_m2575670866_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider_set_enabled_m2575670866_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider::set_enabled(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Rigidbody UnityEngine.Collider::get_attachedRigidbody()
extern "C"  Rigidbody_t3346577219 * Collider_get_attachedRigidbody_m2821754842 (Collider_t2939674232 * __this, const MethodInfo* method)
{
	typedef Rigidbody_t3346577219 * (*Collider_get_attachedRigidbody_m2821754842_ftn) (Collider_t2939674232 *);
	static Collider_get_attachedRigidbody_m2821754842_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider_get_attachedRigidbody_m2821754842_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider::get_attachedRigidbody()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.PhysicMaterial UnityEngine.Collider::get_material()
extern "C"  PhysicMaterial_t211873335 * Collider_get_material_m2158909696 (Collider_t2939674232 * __this, const MethodInfo* method)
{
	typedef PhysicMaterial_t211873335 * (*Collider_get_material_m2158909696_ftn) (Collider_t2939674232 *);
	static Collider_get_material_m2158909696_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider_get_material_m2158909696_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider::get_material()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Rigidbody2D UnityEngine.Collider2D::get_attachedRigidbody()
extern "C"  Rigidbody2D_t1743771669 * Collider2D_get_attachedRigidbody_m2908627162 (Collider2D_t1552025098 * __this, const MethodInfo* method)
{
	typedef Rigidbody2D_t1743771669 * (*Collider2D_get_attachedRigidbody_m2908627162_ftn) (Collider2D_t1552025098 *);
	static Collider2D_get_attachedRigidbody_m2908627162_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider2D_get_attachedRigidbody_m2908627162_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider2D::get_attachedRigidbody()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Collision::.ctor()
extern "C"  void Collision__ctor_m183727591 (Collision_t2494107688 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Collision::get_relativeVelocity()
extern "C"  Vector3_t4282066566  Collision_get_relativeVelocity_m1067992885 (Collision_t2494107688 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_m_RelativeVelocity_1();
		return L_0;
	}
}
// UnityEngine.Rigidbody UnityEngine.Collision::get_rigidbody()
extern "C"  Rigidbody_t3346577219 * Collision_get_rigidbody_m1973745126 (Collision_t2494107688 * __this, const MethodInfo* method)
{
	{
		Rigidbody_t3346577219 * L_0 = __this->get_m_Rigidbody_2();
		return L_0;
	}
}
// UnityEngine.Collider UnityEngine.Collision::get_collider()
extern "C"  Collider_t2939674232 * Collision_get_collider_m1325344374 (Collision_t2494107688 * __this, const MethodInfo* method)
{
	{
		Collider_t2939674232 * L_0 = __this->get_m_Collider_3();
		return L_0;
	}
}
// UnityEngine.Transform UnityEngine.Collision::get_transform()
extern "C"  Transform_t1659122786 * Collision_get_transform_m3247945030 (Collision_t2494107688 * __this, const MethodInfo* method)
{
	Transform_t1659122786 * G_B3_0 = NULL;
	{
		Rigidbody_t3346577219 * L_0 = Collision_get_rigidbody_m1973745126(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Rigidbody_t3346577219 * L_2 = Collision_get_rigidbody_m1973745126(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t1659122786 * L_3 = Component_get_transform_m4257140443(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_002c;
	}

IL_0021:
	{
		Collider_t2939674232 * L_4 = Collision_get_collider_m1325344374(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t1659122786 * L_5 = Component_get_transform_m4257140443(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
	}

IL_002c:
	{
		return G_B3_0;
	}
}
// UnityEngine.GameObject UnityEngine.Collision::get_gameObject()
extern "C"  GameObject_t3674682005 * Collision_get_gameObject_m4245316464 (Collision_t2494107688 * __this, const MethodInfo* method)
{
	GameObject_t3674682005 * G_B3_0 = NULL;
	{
		Rigidbody_t3346577219 * L_0 = __this->get_m_Rigidbody_2();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Rigidbody_t3346577219 * L_2 = __this->get_m_Rigidbody_2();
		NullCheck(L_2);
		GameObject_t3674682005 * L_3 = Component_get_gameObject_m1170635899(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_002c;
	}

IL_0021:
	{
		Collider_t2939674232 * L_4 = __this->get_m_Collider_3();
		NullCheck(L_4);
		GameObject_t3674682005 * L_5 = Component_get_gameObject_m1170635899(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
	}

IL_002c:
	{
		return G_B3_0;
	}
}
// UnityEngine.ContactPoint[] UnityEngine.Collision::get_contacts()
extern "C"  ContactPointU5BU5D_t715040733* Collision_get_contacts_m658316947 (Collision_t2494107688 * __this, const MethodInfo* method)
{
	{
		ContactPointU5BU5D_t715040733* L_0 = __this->get_m_Contacts_4();
		return L_0;
	}
}
// System.Collections.IEnumerator UnityEngine.Collision::GetEnumerator()
extern "C"  Il2CppObject * Collision_GetEnumerator_m218695993 (Collision_t2494107688 * __this, const MethodInfo* method)
{
	{
		ContactPointU5BU5D_t715040733* L_0 = Collision_get_contacts_m658316947(__this, /*hidden argument*/NULL);
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Il2CppObject * L_1 = Array_GetEnumerator_m2728734362((Il2CppArray *)(Il2CppArray *)L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.Collision::get_impulse()
extern "C"  Vector3_t4282066566  Collision_get_impulse_m2182857643 (Collision_t2494107688 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_m_Impulse_0();
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Collision::get_impactForceSum()
extern "C"  Vector3_t4282066566  Collision_get_impactForceSum_m3741050290 (Collision_t2494107688 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = Collision_get_relativeVelocity_m1067992885(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Collision::get_frictionForceSum()
extern "C"  Vector3_t4282066566  Collision_get_frictionForceSum_m3009900454 (Collision_t2494107688 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Component UnityEngine.Collision::get_other()
extern "C"  Component_t3501516275 * Collision_get_other_m541712697 (Collision_t2494107688 * __this, const MethodInfo* method)
{
	Rigidbody_t3346577219 * G_B3_0 = NULL;
	{
		Rigidbody_t3346577219 * L_0 = __this->get_m_Rigidbody_2();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		Rigidbody_t3346577219 * L_2 = __this->get_m_Rigidbody_2();
		G_B3_0 = L_2;
		goto IL_0022;
	}

IL_001c:
	{
		Collider_t2939674232 * L_3 = __this->get_m_Collider_3();
		G_B3_0 = ((Rigidbody_t3346577219 *)(L_3));
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// Conversion methods for marshalling of: UnityEngine.Collision
extern "C" void Collision_t2494107688_marshal_pinvoke(const Collision_t2494107688& unmarshaled, Collision_t2494107688_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Rigidbody_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Rigidbody' of type 'Collision': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Rigidbody_2Exception);
}
extern "C" void Collision_t2494107688_marshal_pinvoke_back(const Collision_t2494107688_marshaled_pinvoke& marshaled, Collision_t2494107688& unmarshaled)
{
	Il2CppCodeGenException* ___m_Rigidbody_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Rigidbody' of type 'Collision': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Rigidbody_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.Collision
extern "C" void Collision_t2494107688_marshal_pinvoke_cleanup(Collision_t2494107688_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Collision
extern "C" void Collision_t2494107688_marshal_com(const Collision_t2494107688& unmarshaled, Collision_t2494107688_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Rigidbody_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Rigidbody' of type 'Collision': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Rigidbody_2Exception);
}
extern "C" void Collision_t2494107688_marshal_com_back(const Collision_t2494107688_marshaled_com& marshaled, Collision_t2494107688& unmarshaled)
{
	Il2CppCodeGenException* ___m_Rigidbody_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Rigidbody' of type 'Collision': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Rigidbody_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.Collision
extern "C" void Collision_t2494107688_marshal_com_cleanup(Collision_t2494107688_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Collision2D::.ctor()
extern "C"  void Collision2D__ctor_m3298468565 (Collision2D_t2859305914 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Collision2D::get_enabled()
extern "C"  bool Collision2D_get_enabled_m3761113681 (Collision2D_t2859305914 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_Enabled_4();
		return L_0;
	}
}
// UnityEngine.Rigidbody2D UnityEngine.Collision2D::get_rigidbody()
extern "C"  Rigidbody2D_t1743771669 * Collision2D_get_rigidbody_m4259461250 (Collision2D_t2859305914 * __this, const MethodInfo* method)
{
	{
		Rigidbody2D_t1743771669 * L_0 = __this->get_m_Rigidbody_0();
		return L_0;
	}
}
// UnityEngine.Collider2D UnityEngine.Collision2D::get_collider()
extern "C"  Collider2D_t1552025098 * Collision2D_get_collider_m4116040666 (Collision2D_t2859305914 * __this, const MethodInfo* method)
{
	{
		Collider2D_t1552025098 * L_0 = __this->get_m_Collider_1();
		return L_0;
	}
}
// UnityEngine.Transform UnityEngine.Collision2D::get_transform()
extern "C"  Transform_t1659122786 * Collision2D_get_transform_m2182628532 (Collision2D_t2859305914 * __this, const MethodInfo* method)
{
	Transform_t1659122786 * G_B3_0 = NULL;
	{
		Rigidbody2D_t1743771669 * L_0 = Collision2D_get_rigidbody_m4259461250(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Rigidbody2D_t1743771669 * L_2 = Collision2D_get_rigidbody_m4259461250(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t1659122786 * L_3 = Component_get_transform_m4257140443(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_002c;
	}

IL_0021:
	{
		Collider2D_t1552025098 * L_4 = Collision2D_get_collider_m4116040666(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t1659122786 * L_5 = Component_get_transform_m4257140443(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
	}

IL_002c:
	{
		return G_B3_0;
	}
}
// UnityEngine.GameObject UnityEngine.Collision2D::get_gameObject()
extern "C"  GameObject_t3674682005 * Collision2D_get_gameObject_m718845954 (Collision2D_t2859305914 * __this, const MethodInfo* method)
{
	GameObject_t3674682005 * G_B3_0 = NULL;
	{
		Rigidbody2D_t1743771669 * L_0 = __this->get_m_Rigidbody_0();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Rigidbody2D_t1743771669 * L_2 = __this->get_m_Rigidbody_0();
		NullCheck(L_2);
		GameObject_t3674682005 * L_3 = Component_get_gameObject_m1170635899(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_002c;
	}

IL_0021:
	{
		Collider2D_t1552025098 * L_4 = __this->get_m_Collider_1();
		NullCheck(L_4);
		GameObject_t3674682005 * L_5 = Component_get_gameObject_m1170635899(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
	}

IL_002c:
	{
		return G_B3_0;
	}
}
// UnityEngine.ContactPoint2D[] UnityEngine.Collision2D::get_contacts()
extern "C"  ContactPoint2DU5BU5D_t3916425411* Collision2D_get_contacts_m4248754423 (Collision2D_t2859305914 * __this, const MethodInfo* method)
{
	{
		ContactPoint2DU5BU5D_t3916425411* L_0 = __this->get_m_Contacts_2();
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Collision2D::get_relativeVelocity()
extern "C"  Vector2_t4282066565  Collision2D_get_relativeVelocity_m2960747782 (Collision2D_t2859305914 * __this, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0 = __this->get_m_RelativeVelocity_3();
		return L_0;
	}
}
// Conversion methods for marshalling of: UnityEngine.Collision2D
extern "C" void Collision2D_t2859305914_marshal_pinvoke(const Collision2D_t2859305914& unmarshaled, Collision2D_t2859305914_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Rigidbody_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Rigidbody' of type 'Collision2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Rigidbody_0Exception);
}
extern "C" void Collision2D_t2859305914_marshal_pinvoke_back(const Collision2D_t2859305914_marshaled_pinvoke& marshaled, Collision2D_t2859305914& unmarshaled)
{
	Il2CppCodeGenException* ___m_Rigidbody_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Rigidbody' of type 'Collision2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Rigidbody_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.Collision2D
extern "C" void Collision2D_t2859305914_marshal_pinvoke_cleanup(Collision2D_t2859305914_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Collision2D
extern "C" void Collision2D_t2859305914_marshal_com(const Collision2D_t2859305914& unmarshaled, Collision2D_t2859305914_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Rigidbody_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Rigidbody' of type 'Collision2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Rigidbody_0Exception);
}
extern "C" void Collision2D_t2859305914_marshal_com_back(const Collision2D_t2859305914_marshaled_com& marshaled, Collision2D_t2859305914& unmarshaled)
{
	Il2CppCodeGenException* ___m_Rigidbody_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Rigidbody' of type 'Collision2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Rigidbody_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.Collision2D
extern "C" void Collision2D_t2859305914_marshal_com_cleanup(Collision2D_t2859305914_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m2252924356 (Color_t4194546905 * __this, float ___r0, float ___g1, float ___b2, float ___a3, const MethodInfo* method)
{
	{
		float L_0 = ___r0;
		__this->set_r_0(L_0);
		float L_1 = ___g1;
		__this->set_g_1(L_1);
		float L_2 = ___b2;
		__this->set_b_2(L_2);
		float L_3 = ___a3;
		__this->set_a_3(L_3);
		return;
	}
}
extern "C"  void Color__ctor_m2252924356_AdjustorThunk (Il2CppObject * __this, float ___r0, float ___g1, float ___b2, float ___a3, const MethodInfo* method)
{
	Color_t4194546905 * _thisAdjusted = reinterpret_cast<Color_t4194546905 *>(__this + 1);
	Color__ctor_m2252924356(_thisAdjusted, ___r0, ___g1, ___b2, ___a3, method);
}
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m103496991 (Color_t4194546905 * __this, float ___r0, float ___g1, float ___b2, const MethodInfo* method)
{
	{
		float L_0 = ___r0;
		__this->set_r_0(L_0);
		float L_1 = ___g1;
		__this->set_g_1(L_1);
		float L_2 = ___b2;
		__this->set_b_2(L_2);
		__this->set_a_3((1.0f));
		return;
	}
}
extern "C"  void Color__ctor_m103496991_AdjustorThunk (Il2CppObject * __this, float ___r0, float ___g1, float ___b2, const MethodInfo* method)
{
	Color_t4194546905 * _thisAdjusted = reinterpret_cast<Color_t4194546905 *>(__this + 1);
	Color__ctor_m103496991(_thisAdjusted, ___r0, ___g1, ___b2, method);
}
// System.String UnityEngine.Color::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3797438823;
extern const uint32_t Color_ToString_m2277845527_MetadataUsageId;
extern "C"  String_t* Color_ToString_m2277845527 (Color_t4194546905 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Color_ToString_m2277845527_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = __this->get_r_0();
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_0;
		float L_5 = __this->get_g_1();
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t1108656482* L_8 = L_4;
		float L_9 = __this->get_b_2();
		float L_10 = L_9;
		Il2CppObject * L_11 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t1108656482* L_12 = L_8;
		float L_13 = __this->get_a_3();
		float L_14 = L_13;
		Il2CppObject * L_15 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral3797438823, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
extern "C"  String_t* Color_ToString_m2277845527_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Color_t4194546905 * _thisAdjusted = reinterpret_cast<Color_t4194546905 *>(__this + 1);
	return Color_ToString_m2277845527(_thisAdjusted, method);
}
// System.Int32 UnityEngine.Color::GetHashCode()
extern "C"  int32_t Color_GetHashCode_m170503301 (Color_t4194546905 * __this, const MethodInfo* method)
{
	Vector4_t4282066567  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector4_t4282066567  L_0 = Color_op_Implicit_m2638307542(NULL /*static, unused*/, (*(Color_t4194546905 *)__this), /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Vector4_GetHashCode_m3402333527((&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
extern "C"  int32_t Color_GetHashCode_m170503301_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Color_t4194546905 * _thisAdjusted = reinterpret_cast<Color_t4194546905 *>(__this + 1);
	return Color_GetHashCode_m170503301(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Color::Equals(System.Object)
extern Il2CppClass* Color_t4194546905_il2cpp_TypeInfo_var;
extern const uint32_t Color_Equals_m3016668205_MetadataUsageId;
extern "C"  bool Color_Equals_m3016668205 (Color_t4194546905 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Color_Equals_m3016668205_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Color_t4194546905  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Color_t4194546905_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(Color_t4194546905 *)((Color_t4194546905 *)UnBox (L_1, Color_t4194546905_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_r_0();
		float L_3 = (&V_0)->get_r_0();
		bool L_4 = Single_Equals_m2110115959(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_006d;
		}
	}
	{
		float* L_5 = __this->get_address_of_g_1();
		float L_6 = (&V_0)->get_g_1();
		bool L_7 = Single_Equals_m2110115959(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006d;
		}
	}
	{
		float* L_8 = __this->get_address_of_b_2();
		float L_9 = (&V_0)->get_b_2();
		bool L_10 = Single_Equals_m2110115959(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006d;
		}
	}
	{
		float* L_11 = __this->get_address_of_a_3();
		float L_12 = (&V_0)->get_a_3();
		bool L_13 = Single_Equals_m2110115959(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_006e;
	}

IL_006d:
	{
		G_B7_0 = 0;
	}

IL_006e:
	{
		return (bool)G_B7_0;
	}
}
extern "C"  bool Color_Equals_m3016668205_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Color_t4194546905 * _thisAdjusted = reinterpret_cast<Color_t4194546905 *>(__this + 1);
	return Color_Equals_m3016668205(_thisAdjusted, ___other0, method);
}
// UnityEngine.Color UnityEngine.Color::Lerp(UnityEngine.Color,UnityEngine.Color,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Color_Lerp_m3494628845_MetadataUsageId;
extern "C"  Color_t4194546905  Color_Lerp_m3494628845 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___a0, Color_t4194546905  ___b1, float ___t2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Color_Lerp_m3494628845_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___t2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m2272733930(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		___t2 = L_1;
		float L_2 = (&___a0)->get_r_0();
		float L_3 = (&___b1)->get_r_0();
		float L_4 = (&___a0)->get_r_0();
		float L_5 = ___t2;
		float L_6 = (&___a0)->get_g_1();
		float L_7 = (&___b1)->get_g_1();
		float L_8 = (&___a0)->get_g_1();
		float L_9 = ___t2;
		float L_10 = (&___a0)->get_b_2();
		float L_11 = (&___b1)->get_b_2();
		float L_12 = (&___a0)->get_b_2();
		float L_13 = ___t2;
		float L_14 = (&___a0)->get_a_3();
		float L_15 = (&___b1)->get_a_3();
		float L_16 = (&___a0)->get_a_3();
		float L_17 = ___t2;
		Color_t4194546905  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Color__ctor_m2252924356(&L_18, ((float)((float)L_2+(float)((float)((float)((float)((float)L_3-(float)L_4))*(float)L_5)))), ((float)((float)L_6+(float)((float)((float)((float)((float)L_7-(float)L_8))*(float)L_9)))), ((float)((float)L_10+(float)((float)((float)((float)((float)L_11-(float)L_12))*(float)L_13)))), ((float)((float)L_14+(float)((float)((float)((float)((float)L_15-(float)L_16))*(float)L_17)))), /*hidden argument*/NULL);
		return L_18;
	}
}
// UnityEngine.Color UnityEngine.Color::get_red()
extern "C"  Color_t4194546905  Color_get_red_m4288945411 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t4194546905  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m2252924356(&L_0, (1.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::get_green()
extern "C"  Color_t4194546905  Color_get_green_m2005284533 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t4194546905  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m2252924356(&L_0, (0.0f), (1.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::get_blue()
extern "C"  Color_t4194546905  Color_get_blue_m3657252170 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t4194546905  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m2252924356(&L_0, (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::get_white()
extern "C"  Color_t4194546905  Color_get_white_m3038282331 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t4194546905  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m2252924356(&L_0, (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::get_black()
extern "C"  Color_t4194546905  Color_get_black_m1687201969 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t4194546905  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m2252924356(&L_0, (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::get_yellow()
extern "C"  Color_t4194546905  Color_get_yellow_m599454500 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t4194546905  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m2252924356(&L_0, (1.0f), (0.921568632f), (0.0156862754f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::get_gray()
extern "C"  Color_t4194546905  Color_get_gray_m3805362451 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t4194546905  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m2252924356(&L_0, (0.5f), (0.5f), (0.5f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::get_grey()
extern "C"  Color_t4194546905  Color_get_grey_m3805481615 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t4194546905  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m2252924356(&L_0, (0.5f), (0.5f), (0.5f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::get_clear()
extern "C"  Color_t4194546905  Color_get_clear_m2578346879 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t4194546905  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m2252924356(&L_0, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.Color::get_Item(System.Int32)
extern Il2CppClass* IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral965278184;
extern const uint32_t Color_get_Item_m934758479_MetadataUsageId;
extern "C"  float Color_get_Item_m934758479 (Color_t4194546905 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Color_get_Item_m934758479_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_001d;
		}
		if (L_1 == 1)
		{
			goto IL_0024;
		}
		if (L_1 == 2)
		{
			goto IL_002b;
		}
		if (L_1 == 3)
		{
			goto IL_0032;
		}
	}
	{
		goto IL_0039;
	}

IL_001d:
	{
		float L_2 = __this->get_r_0();
		return L_2;
	}

IL_0024:
	{
		float L_3 = __this->get_g_1();
		return L_3;
	}

IL_002b:
	{
		float L_4 = __this->get_b_2();
		return L_4;
	}

IL_0032:
	{
		float L_5 = __this->get_a_3();
		return L_5;
	}

IL_0039:
	{
		IndexOutOfRangeException_t3456360697 * L_6 = (IndexOutOfRangeException_t3456360697 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1621772274(L_6, _stringLiteral965278184, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}
}
extern "C"  float Color_get_Item_m934758479_AdjustorThunk (Il2CppObject * __this, int32_t ___index0, const MethodInfo* method)
{
	Color_t4194546905 * _thisAdjusted = reinterpret_cast<Color_t4194546905 *>(__this + 1);
	return Color_get_Item_m934758479(_thisAdjusted, ___index0, method);
}
// System.Void UnityEngine.Color::set_Item(System.Int32,System.Single)
extern Il2CppClass* IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral965278184;
extern const uint32_t Color_set_Item_m2234100212_MetadataUsageId;
extern "C"  void Color_set_Item_m2234100212 (Color_t4194546905 * __this, int32_t ___index0, float ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Color_set_Item_m2234100212_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_001d;
		}
		if (L_1 == 1)
		{
			goto IL_0029;
		}
		if (L_1 == 2)
		{
			goto IL_0035;
		}
		if (L_1 == 3)
		{
			goto IL_0041;
		}
	}
	{
		goto IL_004d;
	}

IL_001d:
	{
		float L_2 = ___value1;
		__this->set_r_0(L_2);
		goto IL_0058;
	}

IL_0029:
	{
		float L_3 = ___value1;
		__this->set_g_1(L_3);
		goto IL_0058;
	}

IL_0035:
	{
		float L_4 = ___value1;
		__this->set_b_2(L_4);
		goto IL_0058;
	}

IL_0041:
	{
		float L_5 = ___value1;
		__this->set_a_3(L_5);
		goto IL_0058;
	}

IL_004d:
	{
		IndexOutOfRangeException_t3456360697 * L_6 = (IndexOutOfRangeException_t3456360697 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1621772274(L_6, _stringLiteral965278184, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0058:
	{
		return;
	}
}
extern "C"  void Color_set_Item_m2234100212_AdjustorThunk (Il2CppObject * __this, int32_t ___index0, float ___value1, const MethodInfo* method)
{
	Color_t4194546905 * _thisAdjusted = reinterpret_cast<Color_t4194546905 *>(__this + 1);
	Color_set_Item_m2234100212(_thisAdjusted, ___index0, ___value1, method);
}
// UnityEngine.Color UnityEngine.Color::op_Multiply(UnityEngine.Color,System.Single)
extern "C"  Color_t4194546905  Color_op_Multiply_m204757678 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___a0, float ___b1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_r_0();
		float L_1 = ___b1;
		float L_2 = (&___a0)->get_g_1();
		float L_3 = ___b1;
		float L_4 = (&___a0)->get_b_2();
		float L_5 = ___b1;
		float L_6 = (&___a0)->get_a_3();
		float L_7 = ___b1;
		Color_t4194546905  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Color__ctor_m2252924356(&L_8, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), ((float)((float)L_6*(float)L_7)), /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Boolean UnityEngine.Color::op_Equality(UnityEngine.Color,UnityEngine.Color)
extern "C"  bool Color_op_Equality_m4163276884 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___lhs0, Color_t4194546905  ___rhs1, const MethodInfo* method)
{
	{
		Color_t4194546905  L_0 = ___lhs0;
		Vector4_t4282066567  L_1 = Color_op_Implicit_m2638307542(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Color_t4194546905  L_2 = ___rhs1;
		Vector4_t4282066567  L_3 = Color_op_Implicit_m2638307542(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		bool L_4 = Vector4_op_Equality_m3533121638(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector4 UnityEngine.Color::op_Implicit(UnityEngine.Color)
extern "C"  Vector4_t4282066567  Color_op_Implicit_m2638307542 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___c0, const MethodInfo* method)
{
	{
		float L_0 = (&___c0)->get_r_0();
		float L_1 = (&___c0)->get_g_1();
		float L_2 = (&___c0)->get_b_2();
		float L_3 = (&___c0)->get_a_3();
		Vector4_t4282066567  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector4__ctor_m2441427762(&L_4, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// Conversion methods for marshalling of: UnityEngine.Color
extern "C" void Color_t4194546905_marshal_pinvoke(const Color_t4194546905& unmarshaled, Color_t4194546905_marshaled_pinvoke& marshaled)
{
	marshaled.___r_0 = unmarshaled.get_r_0();
	marshaled.___g_1 = unmarshaled.get_g_1();
	marshaled.___b_2 = unmarshaled.get_b_2();
	marshaled.___a_3 = unmarshaled.get_a_3();
}
extern "C" void Color_t4194546905_marshal_pinvoke_back(const Color_t4194546905_marshaled_pinvoke& marshaled, Color_t4194546905& unmarshaled)
{
	float unmarshaled_r_temp_0 = 0.0f;
	unmarshaled_r_temp_0 = marshaled.___r_0;
	unmarshaled.set_r_0(unmarshaled_r_temp_0);
	float unmarshaled_g_temp_1 = 0.0f;
	unmarshaled_g_temp_1 = marshaled.___g_1;
	unmarshaled.set_g_1(unmarshaled_g_temp_1);
	float unmarshaled_b_temp_2 = 0.0f;
	unmarshaled_b_temp_2 = marshaled.___b_2;
	unmarshaled.set_b_2(unmarshaled_b_temp_2);
	float unmarshaled_a_temp_3 = 0.0f;
	unmarshaled_a_temp_3 = marshaled.___a_3;
	unmarshaled.set_a_3(unmarshaled_a_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Color
extern "C" void Color_t4194546905_marshal_pinvoke_cleanup(Color_t4194546905_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Color
extern "C" void Color_t4194546905_marshal_com(const Color_t4194546905& unmarshaled, Color_t4194546905_marshaled_com& marshaled)
{
	marshaled.___r_0 = unmarshaled.get_r_0();
	marshaled.___g_1 = unmarshaled.get_g_1();
	marshaled.___b_2 = unmarshaled.get_b_2();
	marshaled.___a_3 = unmarshaled.get_a_3();
}
extern "C" void Color_t4194546905_marshal_com_back(const Color_t4194546905_marshaled_com& marshaled, Color_t4194546905& unmarshaled)
{
	float unmarshaled_r_temp_0 = 0.0f;
	unmarshaled_r_temp_0 = marshaled.___r_0;
	unmarshaled.set_r_0(unmarshaled_r_temp_0);
	float unmarshaled_g_temp_1 = 0.0f;
	unmarshaled_g_temp_1 = marshaled.___g_1;
	unmarshaled.set_g_1(unmarshaled_g_temp_1);
	float unmarshaled_b_temp_2 = 0.0f;
	unmarshaled_b_temp_2 = marshaled.___b_2;
	unmarshaled.set_b_2(unmarshaled_b_temp_2);
	float unmarshaled_a_temp_3 = 0.0f;
	unmarshaled_a_temp_3 = marshaled.___a_3;
	unmarshaled.set_a_3(unmarshaled_a_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Color
extern "C" void Color_t4194546905_marshal_com_cleanup(Color_t4194546905_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Color32::.ctor(System.Byte,System.Byte,System.Byte,System.Byte)
extern "C"  void Color32__ctor_m576906339 (Color32_t598853688 * __this, uint8_t ___r0, uint8_t ___g1, uint8_t ___b2, uint8_t ___a3, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___r0;
		__this->set_r_0(L_0);
		uint8_t L_1 = ___g1;
		__this->set_g_1(L_1);
		uint8_t L_2 = ___b2;
		__this->set_b_2(L_2);
		uint8_t L_3 = ___a3;
		__this->set_a_3(L_3);
		return;
	}
}
extern "C"  void Color32__ctor_m576906339_AdjustorThunk (Il2CppObject * __this, uint8_t ___r0, uint8_t ___g1, uint8_t ___b2, uint8_t ___a3, const MethodInfo* method)
{
	Color32_t598853688 * _thisAdjusted = reinterpret_cast<Color32_t598853688 *>(__this + 1);
	Color32__ctor_m576906339(_thisAdjusted, ___r0, ___g1, ___b2, ___a3, method);
}
// System.String UnityEngine.Color32::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Byte_t2862609660_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral632949419;
extern const uint32_t Color32_ToString_m909782902_MetadataUsageId;
extern "C"  String_t* Color32_ToString_m909782902 (Color32_t598853688 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Color32_ToString_m909782902_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		uint8_t L_1 = __this->get_r_0();
		uint8_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Byte_t2862609660_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_0;
		uint8_t L_5 = __this->get_g_1();
		uint8_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Byte_t2862609660_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t1108656482* L_8 = L_4;
		uint8_t L_9 = __this->get_b_2();
		uint8_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Byte_t2862609660_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t1108656482* L_12 = L_8;
		uint8_t L_13 = __this->get_a_3();
		uint8_t L_14 = L_13;
		Il2CppObject * L_15 = Box(Byte_t2862609660_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral632949419, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
extern "C"  String_t* Color32_ToString_m909782902_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Color32_t598853688 * _thisAdjusted = reinterpret_cast<Color32_t598853688 *>(__this + 1);
	return Color32_ToString_m909782902(_thisAdjusted, method);
}
// UnityEngine.Color32 UnityEngine.Color32::op_Implicit(UnityEngine.Color)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Color32_op_Implicit_m3684884838_MetadataUsageId;
extern "C"  Color32_t598853688  Color32_op_Implicit_m3684884838 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Color32_op_Implicit_m3684884838_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (&___c0)->get_r_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m2272733930(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = (&___c0)->get_g_1();
		float L_3 = Mathf_Clamp01_m2272733930(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		float L_4 = (&___c0)->get_b_2();
		float L_5 = Mathf_Clamp01_m2272733930(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		float L_6 = (&___c0)->get_a_3();
		float L_7 = Mathf_Clamp01_m2272733930(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Color32_t598853688  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Color32__ctor_m576906339(&L_8, (((int32_t)((uint8_t)((float)((float)L_1*(float)(255.0f)))))), (((int32_t)((uint8_t)((float)((float)L_3*(float)(255.0f)))))), (((int32_t)((uint8_t)((float)((float)L_5*(float)(255.0f)))))), (((int32_t)((uint8_t)((float)((float)L_7*(float)(255.0f)))))), /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Color UnityEngine.Color32::op_Implicit(UnityEngine.Color32)
extern "C"  Color_t4194546905  Color32_op_Implicit_m358459656 (Il2CppObject * __this /* static, unused */, Color32_t598853688  ___c0, const MethodInfo* method)
{
	{
		uint8_t L_0 = (&___c0)->get_r_0();
		uint8_t L_1 = (&___c0)->get_g_1();
		uint8_t L_2 = (&___c0)->get_b_2();
		uint8_t L_3 = (&___c0)->get_a_3();
		Color_t4194546905  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Color__ctor_m2252924356(&L_4, ((float)((float)(((float)((float)L_0)))/(float)(255.0f))), ((float)((float)(((float)((float)L_1)))/(float)(255.0f))), ((float)((float)(((float)((float)L_2)))/(float)(255.0f))), ((float)((float)(((float)((float)L_3)))/(float)(255.0f))), /*hidden argument*/NULL);
		return L_4;
	}
}
// Conversion methods for marshalling of: UnityEngine.Color32
extern "C" void Color32_t598853688_marshal_pinvoke(const Color32_t598853688& unmarshaled, Color32_t598853688_marshaled_pinvoke& marshaled)
{
	marshaled.___r_0 = unmarshaled.get_r_0();
	marshaled.___g_1 = unmarshaled.get_g_1();
	marshaled.___b_2 = unmarshaled.get_b_2();
	marshaled.___a_3 = unmarshaled.get_a_3();
}
extern "C" void Color32_t598853688_marshal_pinvoke_back(const Color32_t598853688_marshaled_pinvoke& marshaled, Color32_t598853688& unmarshaled)
{
	uint8_t unmarshaled_r_temp_0 = 0x0;
	unmarshaled_r_temp_0 = marshaled.___r_0;
	unmarshaled.set_r_0(unmarshaled_r_temp_0);
	uint8_t unmarshaled_g_temp_1 = 0x0;
	unmarshaled_g_temp_1 = marshaled.___g_1;
	unmarshaled.set_g_1(unmarshaled_g_temp_1);
	uint8_t unmarshaled_b_temp_2 = 0x0;
	unmarshaled_b_temp_2 = marshaled.___b_2;
	unmarshaled.set_b_2(unmarshaled_b_temp_2);
	uint8_t unmarshaled_a_temp_3 = 0x0;
	unmarshaled_a_temp_3 = marshaled.___a_3;
	unmarshaled.set_a_3(unmarshaled_a_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Color32
extern "C" void Color32_t598853688_marshal_pinvoke_cleanup(Color32_t598853688_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Color32
extern "C" void Color32_t598853688_marshal_com(const Color32_t598853688& unmarshaled, Color32_t598853688_marshaled_com& marshaled)
{
	marshaled.___r_0 = unmarshaled.get_r_0();
	marshaled.___g_1 = unmarshaled.get_g_1();
	marshaled.___b_2 = unmarshaled.get_b_2();
	marshaled.___a_3 = unmarshaled.get_a_3();
}
extern "C" void Color32_t598853688_marshal_com_back(const Color32_t598853688_marshaled_com& marshaled, Color32_t598853688& unmarshaled)
{
	uint8_t unmarshaled_r_temp_0 = 0x0;
	unmarshaled_r_temp_0 = marshaled.___r_0;
	unmarshaled.set_r_0(unmarshaled_r_temp_0);
	uint8_t unmarshaled_g_temp_1 = 0x0;
	unmarshaled_g_temp_1 = marshaled.___g_1;
	unmarshaled.set_g_1(unmarshaled_g_temp_1);
	uint8_t unmarshaled_b_temp_2 = 0x0;
	unmarshaled_b_temp_2 = marshaled.___b_2;
	unmarshaled.set_b_2(unmarshaled_b_temp_2);
	uint8_t unmarshaled_a_temp_3 = 0x0;
	unmarshaled_a_temp_3 = marshaled.___a_3;
	unmarshaled.set_a_3(unmarshaled_a_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Color32
extern "C" void Color32_t598853688_marshal_com_cleanup(Color32_t598853688_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Compass::.ctor()
extern "C"  void Compass__ctor_m2401581447 (Compass_t599792712 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.Compass::get_magneticHeading()
extern "C"  float Compass_get_magneticHeading_m2817211142 (Compass_t599792712 * __this, const MethodInfo* method)
{
	typedef float (*Compass_get_magneticHeading_m2817211142_ftn) (Compass_t599792712 *);
	static Compass_get_magneticHeading_m2817211142_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Compass_get_magneticHeading_m2817211142_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Compass::get_magneticHeading()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Vector3 UnityEngine.Compass::get_rawVector()
extern "C"  Vector3_t4282066566  Compass_get_rawVector_m4065151393 (Compass_t599792712 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Compass_INTERNAL_get_rawVector_m2650062384(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Compass::INTERNAL_get_rawVector(UnityEngine.Vector3&)
extern "C"  void Compass_INTERNAL_get_rawVector_m2650062384 (Compass_t599792712 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Compass_INTERNAL_get_rawVector_m2650062384_ftn) (Compass_t599792712 *, Vector3_t4282066566 *);
	static Compass_INTERNAL_get_rawVector_m2650062384_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Compass_INTERNAL_get_rawVector_m2650062384_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Compass::INTERNAL_get_rawVector(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Double UnityEngine.Compass::get_timestamp()
extern "C"  double Compass_get_timestamp_m1013803541 (Compass_t599792712 * __this, const MethodInfo* method)
{
	typedef double (*Compass_get_timestamp_m1013803541_ftn) (Compass_t599792712 *);
	static Compass_get_timestamp_m1013803541_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Compass_get_timestamp_m1013803541_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Compass::get_timestamp()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Compass::get_enabled()
extern "C"  bool Compass_get_enabled_m2328786563 (Compass_t599792712 * __this, const MethodInfo* method)
{
	typedef bool (*Compass_get_enabled_m2328786563_ftn) (Compass_t599792712 *);
	static Compass_get_enabled_m2328786563_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Compass_get_enabled_m2328786563_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Compass::get_enabled()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Compass::set_enabled(System.Boolean)
extern "C"  void Compass_set_enabled_m3741384928 (Compass_t599792712 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Compass_set_enabled_m3741384928_ftn) (Compass_t599792712 *, bool);
	static Compass_set_enabled_m3741384928_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Compass_set_enabled_m3741384928_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Compass::set_enabled(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Component::.ctor()
extern "C"  void Component__ctor_m4238603388 (Component_t3501516275 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m570634126(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t1659122786 * Component_get_transform_m4257140443 (Component_t3501516275 * __this, const MethodInfo* method)
{
	typedef Transform_t1659122786 * (*Component_get_transform_m4257140443_ftn) (Component_t3501516275 *);
	static Component_get_transform_m4257140443_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_get_transform_m4257140443_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_transform()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t3674682005 * Component_get_gameObject_m1170635899 (Component_t3501516275 * __this, const MethodInfo* method)
{
	typedef GameObject_t3674682005 * (*Component_get_gameObject_m1170635899_ftn) (Component_t3501516275 *);
	static Component_get_gameObject_m1170635899_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_get_gameObject_m1170635899_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_gameObject()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Component UnityEngine.Component::GetComponent(System.Type)
extern "C"  Component_t3501516275 * Component_GetComponent_m936021879 (Component_t3501516275 * __this, Type_t * ___type0, const MethodInfo* method)
{
	{
		GameObject_t3674682005 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___type0;
		NullCheck(L_0);
		Component_t3501516275 * L_2 = GameObject_GetComponent_m1004814461(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)
extern "C"  void Component_GetComponentFastPath_m1455568887 (Component_t3501516275 * __this, Type_t * ___type0, IntPtr_t ___oneFurtherThanResultValue1, const MethodInfo* method)
{
	typedef void (*Component_GetComponentFastPath_m1455568887_ftn) (Component_t3501516275 *, Type_t *, IntPtr_t);
	static Component_GetComponentFastPath_m1455568887_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_GetComponentFastPath_m1455568887_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)");
	_il2cpp_icall_func(__this, ___type0, ___oneFurtherThanResultValue1);
}
// UnityEngine.Component UnityEngine.Component::GetComponent(System.String)
extern "C"  Component_t3501516275 * Component_GetComponent_m840247168 (Component_t3501516275 * __this, String_t* ___type0, const MethodInfo* method)
{
	typedef Component_t3501516275 * (*Component_GetComponent_m840247168_ftn) (Component_t3501516275 *, String_t*);
	static Component_GetComponent_m840247168_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_GetComponent_m840247168_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::GetComponent(System.String)");
	return _il2cpp_icall_func(__this, ___type0);
}
// UnityEngine.Component UnityEngine.Component::GetComponentInChildren(System.Type,System.Boolean)
extern "C"  Component_t3501516275 * Component_GetComponentInChildren_m1899663946 (Component_t3501516275 * __this, Type_t * ___t0, bool ___includeInactive1, const MethodInfo* method)
{
	{
		GameObject_t3674682005 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___t0;
		bool L_2 = ___includeInactive1;
		NullCheck(L_0);
		Component_t3501516275 * L_3 = GameObject_GetComponentInChildren_m1490154500(L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Component UnityEngine.Component::GetComponentInChildren(System.Type)
extern "C"  Component_t3501516275 * Component_GetComponentInChildren_m680239315 (Component_t3501516275 * __this, Type_t * ___t0, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___t0;
		Component_t3501516275 * L_1 = Component_GetComponentInChildren_m1899663946(__this, L_0, (bool)0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Component[] UnityEngine.Component::GetComponentsInChildren(System.Type)
extern "C"  ComponentU5BU5D_t663911650* Component_GetComponentsInChildren_m1419556996 (Component_t3501516275 * __this, Type_t * ___t0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		Type_t * L_0 = ___t0;
		bool L_1 = V_0;
		ComponentU5BU5D_t663911650* L_2 = Component_GetComponentsInChildren_m2446525049(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Component[] UnityEngine.Component::GetComponentsInChildren(System.Type,System.Boolean)
extern "C"  ComponentU5BU5D_t663911650* Component_GetComponentsInChildren_m2446525049 (Component_t3501516275 * __this, Type_t * ___t0, bool ___includeInactive1, const MethodInfo* method)
{
	{
		GameObject_t3674682005 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___t0;
		bool L_2 = ___includeInactive1;
		NullCheck(L_0);
		ComponentU5BU5D_t663911650* L_3 = GameObject_GetComponentsInChildren_m305836803(L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Component UnityEngine.Component::GetComponentInParent(System.Type)
extern "C"  Component_t3501516275 * Component_GetComponentInParent_m1953645192 (Component_t3501516275 * __this, Type_t * ___t0, const MethodInfo* method)
{
	{
		GameObject_t3674682005 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___t0;
		NullCheck(L_0);
		Component_t3501516275 * L_2 = GameObject_GetComponentInParent_m434474382(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Component[] UnityEngine.Component::GetComponentsInParent(System.Type)
extern "C"  ComponentU5BU5D_t663911650* Component_GetComponentsInParent_m345477753 (Component_t3501516275 * __this, Type_t * ___t0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		Type_t * L_0 = ___t0;
		bool L_1 = V_0;
		ComponentU5BU5D_t663911650* L_2 = Component_GetComponentsInParent_m608460772(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Component[] UnityEngine.Component::GetComponentsInParent(System.Type,System.Boolean)
extern "C"  ComponentU5BU5D_t663911650* Component_GetComponentsInParent_m608460772 (Component_t3501516275 * __this, Type_t * ___t0, bool ___includeInactive1, const MethodInfo* method)
{
	{
		GameObject_t3674682005 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___t0;
		bool L_2 = ___includeInactive1;
		NullCheck(L_0);
		ComponentU5BU5D_t663911650* L_3 = GameObject_GetComponentsInParent_m521317102(L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Component[] UnityEngine.Component::GetComponents(System.Type)
extern "C"  ComponentU5BU5D_t663911650* Component_GetComponents_m2975031400 (Component_t3501516275 * __this, Type_t * ___type0, const MethodInfo* method)
{
	{
		GameObject_t3674682005 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___type0;
		NullCheck(L_0);
		ComponentU5BU5D_t663911650* L_2 = GameObject_GetComponents_m1126578334(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)
extern "C"  void Component_GetComponentsForListInternal_m814665735 (Component_t3501516275 * __this, Type_t * ___searchType0, Il2CppObject * ___resultList1, const MethodInfo* method)
{
	typedef void (*Component_GetComponentsForListInternal_m814665735_ftn) (Component_t3501516275 *, Type_t *, Il2CppObject *);
	static Component_GetComponentsForListInternal_m814665735_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_GetComponentsForListInternal_m814665735_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)");
	_il2cpp_icall_func(__this, ___searchType0, ___resultList1);
}
// System.Void UnityEngine.Component::GetComponents(System.Type,System.Collections.Generic.List`1<UnityEngine.Component>)
extern "C"  void Component_GetComponents_m3426812285 (Component_t3501516275 * __this, Type_t * ___type0, List_1_t574734531 * ___results1, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type0;
		List_1_t574734531 * L_1 = ___results1;
		Component_GetComponentsForListInternal_m814665735(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Component::get_tag()
extern "C"  String_t* Component_get_tag_m217485006 (Component_t3501516275 * __this, const MethodInfo* method)
{
	{
		GameObject_t3674682005 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = GameObject_get_tag_m211612200(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Component::set_tag(System.String)
extern "C"  void Component_set_tag_m3240989163 (Component_t3501516275 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		GameObject_t3674682005 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___value0;
		NullCheck(L_0);
		GameObject_set_tag_m859036203(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Component::CompareTag(System.String)
extern "C"  bool Component_CompareTag_m305486283 (Component_t3501516275 * __this, String_t* ___tag0, const MethodInfo* method)
{
	typedef bool (*Component_CompareTag_m305486283_ftn) (Component_t3501516275 *, String_t*);
	static Component_CompareTag_m305486283_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_CompareTag_m305486283_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::CompareTag(System.String)");
	return _il2cpp_icall_func(__this, ___tag0);
}
// System.Void UnityEngine.Component::SendMessageUpwards(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C"  void Component_SendMessageUpwards_m2772130661 (Component_t3501516275 * __this, String_t* ___methodName0, Il2CppObject * ___value1, int32_t ___options2, const MethodInfo* method)
{
	typedef void (*Component_SendMessageUpwards_m2772130661_ftn) (Component_t3501516275 *, String_t*, Il2CppObject *, int32_t);
	static Component_SendMessageUpwards_m2772130661_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_SendMessageUpwards_m2772130661_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::SendMessageUpwards(System.String,System.Object,UnityEngine.SendMessageOptions)");
	_il2cpp_icall_func(__this, ___methodName0, ___value1, ___options2);
}
// System.Void UnityEngine.Component::SendMessageUpwards(System.String,System.Object)
extern "C"  void Component_SendMessageUpwards_m2473191947 (Component_t3501516275 * __this, String_t* ___methodName0, Il2CppObject * ___value1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		String_t* L_0 = ___methodName0;
		Il2CppObject * L_1 = ___value1;
		int32_t L_2 = V_0;
		Component_SendMessageUpwards_m2772130661(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::SendMessageUpwards(System.String)
extern "C"  void Component_SendMessageUpwards_m767712765 (Component_t3501516275 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	{
		V_0 = 0;
		V_1 = NULL;
		String_t* L_0 = ___methodName0;
		Il2CppObject * L_1 = V_1;
		int32_t L_2 = V_0;
		Component_SendMessageUpwards_m2772130661(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::SendMessageUpwards(System.String,UnityEngine.SendMessageOptions)
extern "C"  void Component_SendMessageUpwards_m707848499 (Component_t3501516275 * __this, String_t* ___methodName0, int32_t ___options1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___methodName0;
		int32_t L_1 = ___options1;
		Component_SendMessageUpwards_m2772130661(__this, L_0, NULL, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C"  void Component_SendMessage_m1163914169 (Component_t3501516275 * __this, String_t* ___methodName0, Il2CppObject * ___value1, int32_t ___options2, const MethodInfo* method)
{
	typedef void (*Component_SendMessage_m1163914169_ftn) (Component_t3501516275 *, String_t*, Il2CppObject *, int32_t);
	static Component_SendMessage_m1163914169_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_SendMessage_m1163914169_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)");
	_il2cpp_icall_func(__this, ___methodName0, ___value1, ___options2);
}
// System.Void UnityEngine.Component::SendMessage(System.String,System.Object)
extern "C"  void Component_SendMessage_m904598583 (Component_t3501516275 * __this, String_t* ___methodName0, Il2CppObject * ___value1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		String_t* L_0 = ___methodName0;
		Il2CppObject * L_1 = ___value1;
		int32_t L_2 = V_0;
		Component_SendMessage_m1163914169(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::SendMessage(System.String)
extern "C"  void Component_SendMessage_m2359133481 (Component_t3501516275 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	{
		V_0 = 0;
		V_1 = NULL;
		String_t* L_0 = ___methodName0;
		Il2CppObject * L_1 = V_1;
		int32_t L_2 = V_0;
		Component_SendMessage_m1163914169(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::SendMessage(System.String,UnityEngine.SendMessageOptions)
extern "C"  void Component_SendMessage_m1912331399 (Component_t3501516275 * __this, String_t* ___methodName0, int32_t ___options1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___methodName0;
		int32_t L_1 = ___options1;
		Component_SendMessage_m1163914169(__this, L_0, NULL, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::BroadcastMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C"  void Component_BroadcastMessage_m3961995854 (Component_t3501516275 * __this, String_t* ___methodName0, Il2CppObject * ___parameter1, int32_t ___options2, const MethodInfo* method)
{
	typedef void (*Component_BroadcastMessage_m3961995854_ftn) (Component_t3501516275 *, String_t*, Il2CppObject *, int32_t);
	static Component_BroadcastMessage_m3961995854_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_BroadcastMessage_m3961995854_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::BroadcastMessage(System.String,System.Object,UnityEngine.SendMessageOptions)");
	_il2cpp_icall_func(__this, ___methodName0, ___parameter1, ___options2);
}
// System.Void UnityEngine.Component::BroadcastMessage(System.String,System.Object)
extern "C"  void Component_BroadcastMessage_m3546905666 (Component_t3501516275 * __this, String_t* ___methodName0, Il2CppObject * ___parameter1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		String_t* L_0 = ___methodName0;
		Il2CppObject * L_1 = ___parameter1;
		int32_t L_2 = V_0;
		Component_BroadcastMessage_m3961995854(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::BroadcastMessage(System.String)
extern "C"  void Component_BroadcastMessage_m2857110644 (Component_t3501516275 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	{
		V_0 = 0;
		V_1 = NULL;
		String_t* L_0 = ___methodName0;
		Il2CppObject * L_1 = V_1;
		int32_t L_2 = V_0;
		Component_BroadcastMessage_m3961995854(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::BroadcastMessage(System.String,UnityEngine.SendMessageOptions)
extern "C"  void Component_BroadcastMessage_m1985948636 (Component_t3501516275 * __this, String_t* ___methodName0, int32_t ___options1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___methodName0;
		int32_t L_1 = ___options1;
		Component_BroadcastMessage_m3961995854(__this, L_0, NULL, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.ContactPoint::get_point()
extern "C"  Vector3_t4282066566  ContactPoint_get_point_m1387782344 (ContactPoint_t243083348 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_m_Point_0();
		return L_0;
	}
}
extern "C"  Vector3_t4282066566  ContactPoint_get_point_m1387782344_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ContactPoint_t243083348 * _thisAdjusted = reinterpret_cast<ContactPoint_t243083348 *>(__this + 1);
	return ContactPoint_get_point_m1387782344(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.ContactPoint::get_normal()
extern "C"  Vector3_t4282066566  ContactPoint_get_normal_m1137164497 (ContactPoint_t243083348 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_m_Normal_1();
		return L_0;
	}
}
extern "C"  Vector3_t4282066566  ContactPoint_get_normal_m1137164497_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ContactPoint_t243083348 * _thisAdjusted = reinterpret_cast<ContactPoint_t243083348 *>(__this + 1);
	return ContactPoint_get_normal_m1137164497(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.ContactPoint
extern "C" void ContactPoint_t243083348_marshal_pinvoke(const ContactPoint_t243083348& unmarshaled, ContactPoint_t243083348_marshaled_pinvoke& marshaled)
{
	Vector3_t4282066566_marshal_pinvoke(unmarshaled.get_m_Point_0(), marshaled.___m_Point_0);
	Vector3_t4282066566_marshal_pinvoke(unmarshaled.get_m_Normal_1(), marshaled.___m_Normal_1);
	marshaled.___m_ThisColliderInstanceID_2 = unmarshaled.get_m_ThisColliderInstanceID_2();
	marshaled.___m_OtherColliderInstanceID_3 = unmarshaled.get_m_OtherColliderInstanceID_3();
}
extern "C" void ContactPoint_t243083348_marshal_pinvoke_back(const ContactPoint_t243083348_marshaled_pinvoke& marshaled, ContactPoint_t243083348& unmarshaled)
{
	Vector3_t4282066566  unmarshaled_m_Point_temp_0;
	memset(&unmarshaled_m_Point_temp_0, 0, sizeof(unmarshaled_m_Point_temp_0));
	Vector3_t4282066566_marshal_pinvoke_back(marshaled.___m_Point_0, unmarshaled_m_Point_temp_0);
	unmarshaled.set_m_Point_0(unmarshaled_m_Point_temp_0);
	Vector3_t4282066566  unmarshaled_m_Normal_temp_1;
	memset(&unmarshaled_m_Normal_temp_1, 0, sizeof(unmarshaled_m_Normal_temp_1));
	Vector3_t4282066566_marshal_pinvoke_back(marshaled.___m_Normal_1, unmarshaled_m_Normal_temp_1);
	unmarshaled.set_m_Normal_1(unmarshaled_m_Normal_temp_1);
	int32_t unmarshaled_m_ThisColliderInstanceID_temp_2 = 0;
	unmarshaled_m_ThisColliderInstanceID_temp_2 = marshaled.___m_ThisColliderInstanceID_2;
	unmarshaled.set_m_ThisColliderInstanceID_2(unmarshaled_m_ThisColliderInstanceID_temp_2);
	int32_t unmarshaled_m_OtherColliderInstanceID_temp_3 = 0;
	unmarshaled_m_OtherColliderInstanceID_temp_3 = marshaled.___m_OtherColliderInstanceID_3;
	unmarshaled.set_m_OtherColliderInstanceID_3(unmarshaled_m_OtherColliderInstanceID_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.ContactPoint
extern "C" void ContactPoint_t243083348_marshal_pinvoke_cleanup(ContactPoint_t243083348_marshaled_pinvoke& marshaled)
{
	Vector3_t4282066566_marshal_pinvoke_cleanup(marshaled.___m_Point_0);
	Vector3_t4282066566_marshal_pinvoke_cleanup(marshaled.___m_Normal_1);
}
// Conversion methods for marshalling of: UnityEngine.ContactPoint
extern "C" void ContactPoint_t243083348_marshal_com(const ContactPoint_t243083348& unmarshaled, ContactPoint_t243083348_marshaled_com& marshaled)
{
	Vector3_t4282066566_marshal_com(unmarshaled.get_m_Point_0(), marshaled.___m_Point_0);
	Vector3_t4282066566_marshal_com(unmarshaled.get_m_Normal_1(), marshaled.___m_Normal_1);
	marshaled.___m_ThisColliderInstanceID_2 = unmarshaled.get_m_ThisColliderInstanceID_2();
	marshaled.___m_OtherColliderInstanceID_3 = unmarshaled.get_m_OtherColliderInstanceID_3();
}
extern "C" void ContactPoint_t243083348_marshal_com_back(const ContactPoint_t243083348_marshaled_com& marshaled, ContactPoint_t243083348& unmarshaled)
{
	Vector3_t4282066566  unmarshaled_m_Point_temp_0;
	memset(&unmarshaled_m_Point_temp_0, 0, sizeof(unmarshaled_m_Point_temp_0));
	Vector3_t4282066566_marshal_com_back(marshaled.___m_Point_0, unmarshaled_m_Point_temp_0);
	unmarshaled.set_m_Point_0(unmarshaled_m_Point_temp_0);
	Vector3_t4282066566  unmarshaled_m_Normal_temp_1;
	memset(&unmarshaled_m_Normal_temp_1, 0, sizeof(unmarshaled_m_Normal_temp_1));
	Vector3_t4282066566_marshal_com_back(marshaled.___m_Normal_1, unmarshaled_m_Normal_temp_1);
	unmarshaled.set_m_Normal_1(unmarshaled_m_Normal_temp_1);
	int32_t unmarshaled_m_ThisColliderInstanceID_temp_2 = 0;
	unmarshaled_m_ThisColliderInstanceID_temp_2 = marshaled.___m_ThisColliderInstanceID_2;
	unmarshaled.set_m_ThisColliderInstanceID_2(unmarshaled_m_ThisColliderInstanceID_temp_2);
	int32_t unmarshaled_m_OtherColliderInstanceID_temp_3 = 0;
	unmarshaled_m_OtherColliderInstanceID_temp_3 = marshaled.___m_OtherColliderInstanceID_3;
	unmarshaled.set_m_OtherColliderInstanceID_3(unmarshaled_m_OtherColliderInstanceID_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.ContactPoint
extern "C" void ContactPoint_t243083348_marshal_com_cleanup(ContactPoint_t243083348_marshaled_com& marshaled)
{
	Vector3_t4282066566_marshal_com_cleanup(marshaled.___m_Point_0);
	Vector3_t4282066566_marshal_com_cleanup(marshaled.___m_Normal_1);
}
// Conversion methods for marshalling of: UnityEngine.ContactPoint2D
extern "C" void ContactPoint2D_t4288432358_marshal_pinvoke(const ContactPoint2D_t4288432358& unmarshaled, ContactPoint2D_t4288432358_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'ContactPoint2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_2Exception);
}
extern "C" void ContactPoint2D_t4288432358_marshal_pinvoke_back(const ContactPoint2D_t4288432358_marshaled_pinvoke& marshaled, ContactPoint2D_t4288432358& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'ContactPoint2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ContactPoint2D
extern "C" void ContactPoint2D_t4288432358_marshal_pinvoke_cleanup(ContactPoint2D_t4288432358_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ContactPoint2D
extern "C" void ContactPoint2D_t4288432358_marshal_com(const ContactPoint2D_t4288432358& unmarshaled, ContactPoint2D_t4288432358_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'ContactPoint2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_2Exception);
}
extern "C" void ContactPoint2D_t4288432358_marshal_com_back(const ContactPoint2D_t4288432358_marshaled_com& marshaled, ContactPoint2D_t4288432358& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'ContactPoint2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ContactPoint2D
extern "C" void ContactPoint2D_t4288432358_marshal_com_cleanup(ContactPoint2D_t4288432358_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ControllerColliderHit::.ctor()
extern "C"  void ControllerColliderHit__ctor_m3119252854 (ControllerColliderHit_t2416790841 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.CharacterController UnityEngine.ControllerColliderHit::get_controller()
extern "C"  CharacterController_t1618060635 * ControllerColliderHit_get_controller_m4144115758 (ControllerColliderHit_t2416790841 * __this, const MethodInfo* method)
{
	{
		CharacterController_t1618060635 * L_0 = __this->get_m_Controller_0();
		return L_0;
	}
}
// UnityEngine.Collider UnityEngine.ControllerColliderHit::get_collider()
extern "C"  Collider_t2939674232 * ControllerColliderHit_get_collider_m1024441863 (ControllerColliderHit_t2416790841 * __this, const MethodInfo* method)
{
	{
		Collider_t2939674232 * L_0 = __this->get_m_Collider_1();
		return L_0;
	}
}
// UnityEngine.Rigidbody UnityEngine.ControllerColliderHit::get_rigidbody()
extern "C"  Rigidbody_t3346577219 * ControllerColliderHit_get_rigidbody_m2091369205 (ControllerColliderHit_t2416790841 * __this, const MethodInfo* method)
{
	{
		Collider_t2939674232 * L_0 = __this->get_m_Collider_1();
		NullCheck(L_0);
		Rigidbody_t3346577219 * L_1 = Collider_get_attachedRigidbody_m2821754842(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.GameObject UnityEngine.ControllerColliderHit::get_gameObject()
extern "C"  GameObject_t3674682005 * ControllerColliderHit_get_gameObject_m3516749633 (ControllerColliderHit_t2416790841 * __this, const MethodInfo* method)
{
	{
		Collider_t2939674232 * L_0 = __this->get_m_Collider_1();
		NullCheck(L_0);
		GameObject_t3674682005 * L_1 = Component_get_gameObject_m1170635899(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Transform UnityEngine.ControllerColliderHit::get_transform()
extern "C"  Transform_t1659122786 * ControllerColliderHit_get_transform_m1182304469 (ControllerColliderHit_t2416790841 * __this, const MethodInfo* method)
{
	{
		Collider_t2939674232 * L_0 = __this->get_m_Collider_1();
		NullCheck(L_0);
		Transform_t1659122786 * L_1 = Component_get_transform_m4257140443(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::get_point()
extern "C"  Vector3_t4282066566  ControllerColliderHit_get_point_m1436351445 (ControllerColliderHit_t2416790841 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_m_Point_2();
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::get_normal()
extern "C"  Vector3_t4282066566  ControllerColliderHit_get_normal_m2642806628 (ControllerColliderHit_t2416790841 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_m_Normal_3();
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::get_moveDirection()
extern "C"  Vector3_t4282066566  ControllerColliderHit_get_moveDirection_m3645766003 (ControllerColliderHit_t2416790841 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_m_MoveDirection_4();
		return L_0;
	}
}
// System.Single UnityEngine.ControllerColliderHit::get_moveLength()
extern "C"  float ControllerColliderHit_get_moveLength_m3314200770 (ControllerColliderHit_t2416790841 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_MoveLength_5();
		return L_0;
	}
}
// System.Boolean UnityEngine.ControllerColliderHit::get_push()
extern "C"  bool ControllerColliderHit_get_push_m2385571563 (ControllerColliderHit_t2416790841 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Push_6();
		return (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.ControllerColliderHit::set_push(System.Boolean)
extern "C"  void ControllerColliderHit_set_push_m1358618172 (ControllerColliderHit_t2416790841 * __this, bool ___value0, const MethodInfo* method)
{
	ControllerColliderHit_t2416790841 * G_B2_0 = NULL;
	ControllerColliderHit_t2416790841 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	ControllerColliderHit_t2416790841 * G_B3_1 = NULL;
	{
		bool L_0 = ___value0;
		G_B1_0 = __this;
		if (!L_0)
		{
			G_B2_0 = __this;
			goto IL_000d;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		goto IL_000e;
	}

IL_000d:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_000e:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_m_Push_6(G_B3_0);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.ControllerColliderHit
extern "C" void ControllerColliderHit_t2416790841_marshal_pinvoke(const ControllerColliderHit_t2416790841& unmarshaled, ControllerColliderHit_t2416790841_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Controller_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Controller' of type 'ControllerColliderHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Controller_0Exception);
}
extern "C" void ControllerColliderHit_t2416790841_marshal_pinvoke_back(const ControllerColliderHit_t2416790841_marshaled_pinvoke& marshaled, ControllerColliderHit_t2416790841& unmarshaled)
{
	Il2CppCodeGenException* ___m_Controller_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Controller' of type 'ControllerColliderHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Controller_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ControllerColliderHit
extern "C" void ControllerColliderHit_t2416790841_marshal_pinvoke_cleanup(ControllerColliderHit_t2416790841_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ControllerColliderHit
extern "C" void ControllerColliderHit_t2416790841_marshal_com(const ControllerColliderHit_t2416790841& unmarshaled, ControllerColliderHit_t2416790841_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Controller_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Controller' of type 'ControllerColliderHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Controller_0Exception);
}
extern "C" void ControllerColliderHit_t2416790841_marshal_com_back(const ControllerColliderHit_t2416790841_marshaled_com& marshaled, ControllerColliderHit_t2416790841& unmarshaled)
{
	Il2CppCodeGenException* ___m_Controller_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Controller' of type 'ControllerColliderHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Controller_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ControllerColliderHit
extern "C" void ControllerColliderHit_t2416790841_marshal_com_cleanup(ControllerColliderHit_t2416790841_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Coroutine::.ctor()
extern "C"  void Coroutine__ctor_m3143611649 (Coroutine_t3621161934 * __this, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m539393484(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Coroutine::ReleaseCoroutine()
extern "C"  void Coroutine_ReleaseCoroutine_m3084231540 (Coroutine_t3621161934 * __this, const MethodInfo* method)
{
	typedef void (*Coroutine_ReleaseCoroutine_m3084231540_ftn) (Coroutine_t3621161934 *);
	static Coroutine_ReleaseCoroutine_m3084231540_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Coroutine_ReleaseCoroutine_m3084231540_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Coroutine::ReleaseCoroutine()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Coroutine::Finalize()
extern "C"  void Coroutine_Finalize_m3970219969 (Coroutine_t3621161934 * __this, const MethodInfo* method)
{
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Coroutine_ReleaseCoroutine_m3084231540(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m3027285644(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0012:
	{
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t3621161934_marshal_pinvoke(const Coroutine_t3621161934& unmarshaled, Coroutine_t3621161934_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void Coroutine_t3621161934_marshal_pinvoke_back(const Coroutine_t3621161934_marshaled_pinvoke& marshaled, Coroutine_t3621161934& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t3621161934_marshal_pinvoke_cleanup(Coroutine_t3621161934_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t3621161934_marshal_com(const Coroutine_t3621161934& unmarshaled, Coroutine_t3621161934_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void Coroutine_t3621161934_marshal_com_back(const Coroutine_t3621161934_marshaled_com& marshaled, Coroutine_t3621161934& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t3621161934_marshal_com_cleanup(Coroutine_t3621161934_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.CullingGroup::Finalize()
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t CullingGroup_Finalize_m1779710340_MetadataUsageId;
extern "C"  void CullingGroup_Finalize_m1779710340 (CullingGroup_t1868862003 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CullingGroup_Finalize_m1779710340_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = __this->get_m_Ptr_0();
			IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			bool L_2 = IntPtr_op_Inequality_m10053967(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
			if (!L_2)
			{
				goto IL_001b;
			}
		}

IL_0015:
		{
			CullingGroup_FinalizerFailure_m4240538940(__this, /*hidden argument*/NULL);
		}

IL_001b:
		{
			IL2CPP_LEAVE(0x27, FINALLY_0020);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0020;
	}

FINALLY_0020:
	{ // begin finally (depth: 1)
		Object_Finalize_m3027285644(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(32)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(32)
	{
		IL2CPP_JUMP_TBL(0x27, IL_0027)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0027:
	{
		return;
	}
}
// System.Void UnityEngine.CullingGroup::Dispose()
extern "C"  void CullingGroup_Dispose_m3554617051 (CullingGroup_t1868862003 * __this, const MethodInfo* method)
{
	typedef void (*CullingGroup_Dispose_m3554617051_ftn) (CullingGroup_t1868862003 *);
	static CullingGroup_Dispose_m3554617051_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CullingGroup_Dispose_m3554617051_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CullingGroup::Dispose()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CullingGroup::SendEvents(UnityEngine.CullingGroup,System.IntPtr,System.Int32)
extern "C"  void CullingGroup_SendEvents_m1355834984 (Il2CppObject * __this /* static, unused */, CullingGroup_t1868862003 * ___cullingGroup0, IntPtr_t ___eventsPtr1, int32_t ___count2, const MethodInfo* method)
{
	CullingGroupEvent_t2820176033 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		void* L_0 = IntPtr_ToPointer_m2676680906((&___eventsPtr1), /*hidden argument*/NULL);
		V_0 = (CullingGroupEvent_t2820176033 *)L_0;
		CullingGroup_t1868862003 * L_1 = ___cullingGroup0;
		NullCheck(L_1);
		StateChanged_t2578300556 * L_2 = L_1->get_m_OnStateChanged_1();
		if (L_2)
		{
			goto IL_0014;
		}
	}
	{
		return;
	}

IL_0014:
	{
		V_1 = 0;
		goto IL_0039;
	}

IL_001b:
	{
		CullingGroup_t1868862003 * L_3 = ___cullingGroup0;
		NullCheck(L_3);
		StateChanged_t2578300556 * L_4 = L_3->get_m_OnStateChanged_1();
		CullingGroupEvent_t2820176033 * L_5 = V_0;
		int32_t L_6 = V_1;
		uint32_t L_7 = sizeof(CullingGroupEvent_t2820176033 );
		NullCheck(L_4);
		StateChanged_Invoke_m3174530616(L_4, (*(CullingGroupEvent_t2820176033 *)((CullingGroupEvent_t2820176033 *)((intptr_t)L_5+(int32_t)((int32_t)((int32_t)L_6*(int32_t)L_7))))), /*hidden argument*/NULL);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0039:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = ___count2;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_001b;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.CullingGroup::FinalizerFailure()
extern "C"  void CullingGroup_FinalizerFailure_m4240538940 (CullingGroup_t1868862003 * __this, const MethodInfo* method)
{
	typedef void (*CullingGroup_FinalizerFailure_m4240538940_ftn) (CullingGroup_t1868862003 *);
	static CullingGroup_FinalizerFailure_m4240538940_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CullingGroup_FinalizerFailure_m4240538940_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CullingGroup::FinalizerFailure()");
	_il2cpp_icall_func(__this);
}
// Conversion methods for marshalling of: UnityEngine.CullingGroup
extern "C" void CullingGroup_t1868862003_marshal_pinvoke(const CullingGroup_t1868862003& unmarshaled, CullingGroup_t1868862003_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
	marshaled.___m_OnStateChanged_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_OnStateChanged_1()));
}
extern Il2CppClass* StateChanged_t2578300556_il2cpp_TypeInfo_var;
extern const uint32_t CullingGroup_t1868862003_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern "C" void CullingGroup_t1868862003_marshal_pinvoke_back(const CullingGroup_t1868862003_marshaled_pinvoke& marshaled, CullingGroup_t1868862003& unmarshaled)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CullingGroup_t1868862003_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_OnStateChanged_1(il2cpp_codegen_marshal_function_ptr_to_delegate<StateChanged_t2578300556>(marshaled.___m_OnStateChanged_1, StateChanged_t2578300556_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.CullingGroup
extern "C" void CullingGroup_t1868862003_marshal_pinvoke_cleanup(CullingGroup_t1868862003_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.CullingGroup
extern "C" void CullingGroup_t1868862003_marshal_com(const CullingGroup_t1868862003& unmarshaled, CullingGroup_t1868862003_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
	marshaled.___m_OnStateChanged_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_OnStateChanged_1()));
}
extern Il2CppClass* StateChanged_t2578300556_il2cpp_TypeInfo_var;
extern const uint32_t CullingGroup_t1868862003_com_FromNativeMethodDefinition_MetadataUsageId;
extern "C" void CullingGroup_t1868862003_marshal_com_back(const CullingGroup_t1868862003_marshaled_com& marshaled, CullingGroup_t1868862003& unmarshaled)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CullingGroup_t1868862003_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_OnStateChanged_1(il2cpp_codegen_marshal_function_ptr_to_delegate<StateChanged_t2578300556>(marshaled.___m_OnStateChanged_1, StateChanged_t2578300556_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.CullingGroup
extern "C" void CullingGroup_t1868862003_marshal_com_cleanup(CullingGroup_t1868862003_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.CullingGroup/StateChanged::.ctor(System.Object,System.IntPtr)
extern "C"  void StateChanged__ctor_m3267025420 (StateChanged_t2578300556 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.CullingGroup/StateChanged::Invoke(UnityEngine.CullingGroupEvent)
extern "C"  void StateChanged_Invoke_m3174530616 (StateChanged_t2578300556 * __this, CullingGroupEvent_t2820176033  ___sphere0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		StateChanged_Invoke_m3174530616((StateChanged_t2578300556 *)__this->get_prev_9(),___sphere0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, CullingGroupEvent_t2820176033  ___sphere0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___sphere0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, CullingGroupEvent_t2820176033  ___sphere0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___sphere0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_StateChanged_t2578300556 (StateChanged_t2578300556 * __this, CullingGroupEvent_t2820176033  ___sphere0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(CullingGroupEvent_t2820176033_marshaled_pinvoke);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___sphere0' to native representation
	CullingGroupEvent_t2820176033_marshaled_pinvoke ____sphere0_marshaled = { };
	CullingGroupEvent_t2820176033_marshal_pinvoke(___sphere0, ____sphere0_marshaled);

	// Native function invocation
	il2cppPInvokeFunc(____sphere0_marshaled);

	// Marshaling cleanup of parameter '___sphere0' native representation
	CullingGroupEvent_t2820176033_marshal_pinvoke_cleanup(____sphere0_marshaled);

}
// System.IAsyncResult UnityEngine.CullingGroup/StateChanged::BeginInvoke(UnityEngine.CullingGroupEvent,System.AsyncCallback,System.Object)
extern Il2CppClass* CullingGroupEvent_t2820176033_il2cpp_TypeInfo_var;
extern const uint32_t StateChanged_BeginInvoke_m163021837_MetadataUsageId;
extern "C"  Il2CppObject * StateChanged_BeginInvoke_m163021837 (StateChanged_t2578300556 * __this, CullingGroupEvent_t2820176033  ___sphere0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StateChanged_BeginInvoke_m163021837_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CullingGroupEvent_t2820176033_il2cpp_TypeInfo_var, &___sphere0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.CullingGroup/StateChanged::EndInvoke(System.IAsyncResult)
extern "C"  void StateChanged_EndInvoke_m4182061596 (StateChanged_t2578300556 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// Conversion methods for marshalling of: UnityEngine.CullingGroupEvent
extern "C" void CullingGroupEvent_t2820176033_marshal_pinvoke(const CullingGroupEvent_t2820176033& unmarshaled, CullingGroupEvent_t2820176033_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Index_0 = unmarshaled.get_m_Index_0();
	marshaled.___m_PrevState_1 = unmarshaled.get_m_PrevState_1();
	marshaled.___m_ThisState_2 = unmarshaled.get_m_ThisState_2();
}
extern "C" void CullingGroupEvent_t2820176033_marshal_pinvoke_back(const CullingGroupEvent_t2820176033_marshaled_pinvoke& marshaled, CullingGroupEvent_t2820176033& unmarshaled)
{
	int32_t unmarshaled_m_Index_temp_0 = 0;
	unmarshaled_m_Index_temp_0 = marshaled.___m_Index_0;
	unmarshaled.set_m_Index_0(unmarshaled_m_Index_temp_0);
	uint8_t unmarshaled_m_PrevState_temp_1 = 0x0;
	unmarshaled_m_PrevState_temp_1 = marshaled.___m_PrevState_1;
	unmarshaled.set_m_PrevState_1(unmarshaled_m_PrevState_temp_1);
	uint8_t unmarshaled_m_ThisState_temp_2 = 0x0;
	unmarshaled_m_ThisState_temp_2 = marshaled.___m_ThisState_2;
	unmarshaled.set_m_ThisState_2(unmarshaled_m_ThisState_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.CullingGroupEvent
extern "C" void CullingGroupEvent_t2820176033_marshal_pinvoke_cleanup(CullingGroupEvent_t2820176033_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.CullingGroupEvent
extern "C" void CullingGroupEvent_t2820176033_marshal_com(const CullingGroupEvent_t2820176033& unmarshaled, CullingGroupEvent_t2820176033_marshaled_com& marshaled)
{
	marshaled.___m_Index_0 = unmarshaled.get_m_Index_0();
	marshaled.___m_PrevState_1 = unmarshaled.get_m_PrevState_1();
	marshaled.___m_ThisState_2 = unmarshaled.get_m_ThisState_2();
}
extern "C" void CullingGroupEvent_t2820176033_marshal_com_back(const CullingGroupEvent_t2820176033_marshaled_com& marshaled, CullingGroupEvent_t2820176033& unmarshaled)
{
	int32_t unmarshaled_m_Index_temp_0 = 0;
	unmarshaled_m_Index_temp_0 = marshaled.___m_Index_0;
	unmarshaled.set_m_Index_0(unmarshaled_m_Index_temp_0);
	uint8_t unmarshaled_m_PrevState_temp_1 = 0x0;
	unmarshaled_m_PrevState_temp_1 = marshaled.___m_PrevState_1;
	unmarshaled.set_m_PrevState_1(unmarshaled_m_PrevState_temp_1);
	uint8_t unmarshaled_m_ThisState_temp_2 = 0x0;
	unmarshaled_m_ThisState_temp_2 = marshaled.___m_ThisState_2;
	unmarshaled.set_m_ThisState_2(unmarshaled_m_ThisState_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.CullingGroupEvent
extern "C" void CullingGroupEvent_t2820176033_marshal_com_cleanup(CullingGroupEvent_t2820176033_marshaled_com& marshaled)
{
}
// System.Boolean UnityEngine.Cursor::get_visible()
extern "C"  bool Cursor_get_visible_m3880003512 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Cursor_get_visible_m3880003512_ftn) ();
	static Cursor_get_visible_m3880003512_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Cursor_get_visible_m3880003512_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Cursor::get_visible()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Cursor::set_visible(System.Boolean)
extern "C"  void Cursor_set_visible_m4101409761 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*Cursor_set_visible_m4101409761_ftn) (bool);
	static Cursor_set_visible_m4101409761_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Cursor_set_visible_m4101409761_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Cursor::set_visible(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.CursorLockMode UnityEngine.Cursor::get_lockState()
extern "C"  int32_t Cursor_get_lockState_m2795946910 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Cursor_get_lockState_m2795946910_ftn) ();
	static Cursor_get_lockState_m2795946910_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Cursor_get_lockState_m2795946910_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Cursor::get_lockState()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Cursor::set_lockState(UnityEngine.CursorLockMode)
extern "C"  void Cursor_set_lockState_m3065915939 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Cursor_set_lockState_m3065915939_ftn) (int32_t);
	static Cursor_set_lockState_m3065915939_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Cursor_set_lockState_m3065915939_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Cursor::set_lockState(UnityEngine.CursorLockMode)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.CustomYieldInstruction::.ctor()
extern "C"  void CustomYieldInstruction__ctor_m2800749531 (CustomYieldInstruction_t2666549910 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityEngine.CustomYieldInstruction::get_Current()
extern "C"  Il2CppObject * CustomYieldInstruction_get_Current_m640561716 (CustomYieldInstruction_t2666549910 * __this, const MethodInfo* method)
{
	{
		return NULL;
	}
}
// System.Boolean UnityEngine.CustomYieldInstruction::MoveNext()
extern "C"  bool CustomYieldInstruction_MoveNext_m489270227 (CustomYieldInstruction_t2666549910 * __this, const MethodInfo* method)
{
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean UnityEngine.CustomYieldInstruction::get_keepWaiting() */, __this);
		return L_0;
	}
}
// System.Void UnityEngine.CustomYieldInstruction::Reset()
extern "C"  void CustomYieldInstruction_Reset_m447182472 (CustomYieldInstruction_t2666549910 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.Debug::.cctor()
extern Il2CppClass* DebugLogHandler_t2406589519_il2cpp_TypeInfo_var;
extern Il2CppClass* Logger_t2997509588_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const uint32_t Debug__cctor_m37515655_MetadataUsageId;
extern "C"  void Debug__cctor_m37515655 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Debug__cctor_m37515655_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DebugLogHandler_t2406589519 * L_0 = (DebugLogHandler_t2406589519 *)il2cpp_codegen_object_new(DebugLogHandler_t2406589519_il2cpp_TypeInfo_var);
		DebugLogHandler__ctor_m1521597024(L_0, /*hidden argument*/NULL);
		Logger_t2997509588 * L_1 = (Logger_t2997509588 *)il2cpp_codegen_object_new(Logger_t2997509588_il2cpp_TypeInfo_var);
		Logger__ctor_m654679389(L_1, L_0, /*hidden argument*/NULL);
		((Debug_t4195163081_StaticFields*)Debug_t4195163081_il2cpp_TypeInfo_var->static_fields)->set_s_Logger_0(L_1);
		return;
	}
}
// UnityEngine.ILogger UnityEngine.Debug::get_logger()
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const uint32_t Debug_get_logger_m275693158_MetadataUsageId;
extern "C"  Il2CppObject * Debug_get_logger_m275693158 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Debug_get_logger_m275693158_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Logger_t2997509588 * L_0 = ((Debug_t4195163081_StaticFields*)Debug_t4195163081_il2cpp_TypeInfo_var->static_fields)->get_s_Logger_0();
		return L_0;
	}
}
// System.Void UnityEngine.Debug::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color,System.Single,System.Boolean)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const uint32_t Debug_DrawLine_m712997666_MetadataUsageId;
extern "C"  void Debug_DrawLine_m712997666 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___start0, Vector3_t4282066566  ___end1, Color_t4194546905  ___color2, float ___duration3, bool ___depthTest4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Debug_DrawLine_m712997666_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___duration3;
		bool L_1 = ___depthTest4;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_INTERNAL_CALL_DrawLine_m4247131229(NULL /*static, unused*/, (&___start0), (&___end1), (&___color2), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const uint32_t Debug_DrawLine_m4257273494_MetadataUsageId;
extern "C"  void Debug_DrawLine_m4257273494 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___start0, Vector3_t4282066566  ___end1, Color_t4194546905  ___color2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Debug_DrawLine_m4257273494_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	float V_1 = 0.0f;
	{
		V_0 = (bool)1;
		V_1 = (0.0f);
		float L_0 = V_1;
		bool L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_INTERNAL_CALL_DrawLine_m4247131229(NULL /*static, unused*/, (&___start0), (&___end1), (&___color2), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const uint32_t Debug_DrawLine_m3405689270_MetadataUsageId;
extern "C"  void Debug_DrawLine_m3405689270 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___start0, Vector3_t4282066566  ___end1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Debug_DrawLine_m3405689270_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	float V_1 = 0.0f;
	Color_t4194546905  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		V_0 = (bool)1;
		V_1 = (0.0f);
		Color_t4194546905  L_0 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_0;
		float L_1 = V_1;
		bool L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_INTERNAL_CALL_DrawLine_m4247131229(NULL /*static, unused*/, (&___start0), (&___end1), (&V_2), L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::INTERNAL_CALL_DrawLine(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Color&,System.Single,System.Boolean)
extern "C"  void Debug_INTERNAL_CALL_DrawLine_m4247131229 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___start0, Vector3_t4282066566 * ___end1, Color_t4194546905 * ___color2, float ___duration3, bool ___depthTest4, const MethodInfo* method)
{
	typedef void (*Debug_INTERNAL_CALL_DrawLine_m4247131229_ftn) (Vector3_t4282066566 *, Vector3_t4282066566 *, Color_t4194546905 *, float, bool);
	static Debug_INTERNAL_CALL_DrawLine_m4247131229_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Debug_INTERNAL_CALL_DrawLine_m4247131229_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Debug::INTERNAL_CALL_DrawLine(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Color&,System.Single,System.Boolean)");
	_il2cpp_icall_func(___start0, ___end1, ___color2, ___duration3, ___depthTest4);
}
// System.Void UnityEngine.Debug::DrawRay(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const uint32_t Debug_DrawRay_m123146114_MetadataUsageId;
extern "C"  void Debug_DrawRay_m123146114 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___start0, Vector3_t4282066566  ___dir1, Color_t4194546905  ___color2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Debug_DrawRay_m123146114_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	float V_1 = 0.0f;
	{
		V_0 = (bool)1;
		V_1 = (0.0f);
		Vector3_t4282066566  L_0 = ___start0;
		Vector3_t4282066566  L_1 = ___dir1;
		Color_t4194546905  L_2 = ___color2;
		float L_3 = V_1;
		bool L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_DrawRay_m3043129782(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::DrawRay(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color,System.Single,System.Boolean)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const uint32_t Debug_DrawRay_m3043129782_MetadataUsageId;
extern "C"  void Debug_DrawRay_m3043129782 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___start0, Vector3_t4282066566  ___dir1, Color_t4194546905  ___color2, float ___duration3, bool ___depthTest4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Debug_DrawRay_m3043129782_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector3_t4282066566  L_0 = ___start0;
		Vector3_t4282066566  L_1 = ___start0;
		Vector3_t4282066566  L_2 = ___dir1;
		Vector3_t4282066566  L_3 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Color_t4194546905  L_4 = ___color2;
		float L_5 = ___duration3;
		bool L_6 = ___depthTest4;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_DrawLine_m712997666(NULL /*static, unused*/, L_0, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::Log(System.Object)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* ILogger_t629411471_il2cpp_TypeInfo_var;
extern const uint32_t Debug_Log_m1731103628_MetadataUsageId;
extern "C"  void Debug_Log_m1731103628 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Debug_Log_m1731103628_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = Debug_get_logger_m275693158(NULL /*static, unused*/, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___message0;
		NullCheck(L_0);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(0 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object) */, ILogger_t629411471_il2cpp_TypeInfo_var, L_0, 3, L_1);
		return;
	}
}
// System.Void UnityEngine.Debug::LogError(System.Object)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* ILogger_t629411471_il2cpp_TypeInfo_var;
extern const uint32_t Debug_LogError_m4127342994_MetadataUsageId;
extern "C"  void Debug_LogError_m4127342994 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogError_m4127342994_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = Debug_get_logger_m275693158(NULL /*static, unused*/, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___message0;
		NullCheck(L_0);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(0 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object) */, ILogger_t629411471_il2cpp_TypeInfo_var, L_0, 0, L_1);
		return;
	}
}
// System.Void UnityEngine.Debug::LogError(System.Object,UnityEngine.Object)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* ILogger_t629411471_il2cpp_TypeInfo_var;
extern const uint32_t Debug_LogError_m214246398_MetadataUsageId;
extern "C"  void Debug_LogError_m214246398 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, Object_t3071478659 * ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogError_m214246398_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = Debug_get_logger_m275693158(NULL /*static, unused*/, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___message0;
		Object_t3071478659 * L_2 = ___context1;
		NullCheck(L_0);
		InterfaceActionInvoker3< int32_t, Il2CppObject *, Object_t3071478659 * >::Invoke(1 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object,UnityEngine.Object) */, ILogger_t629411471_il2cpp_TypeInfo_var, L_0, 0, L_1, L_2);
		return;
	}
}
// System.Void UnityEngine.Debug::LogException(System.Exception)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* ILogHandler_t2265139045_il2cpp_TypeInfo_var;
extern const uint32_t Debug_LogException_m248970745_MetadataUsageId;
extern "C"  void Debug_LogException_m248970745 (Il2CppObject * __this /* static, unused */, Exception_t3991598821 * ___exception0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogException_m248970745_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = Debug_get_logger_m275693158(NULL /*static, unused*/, /*hidden argument*/NULL);
		Exception_t3991598821 * L_1 = ___exception0;
		NullCheck(L_0);
		InterfaceActionInvoker2< Exception_t3991598821 *, Object_t3071478659 * >::Invoke(1 /* System.Void UnityEngine.ILogHandler::LogException(System.Exception,UnityEngine.Object) */, ILogHandler_t2265139045_il2cpp_TypeInfo_var, L_0, L_1, (Object_t3071478659 *)NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::LogException(System.Exception,UnityEngine.Object)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* ILogHandler_t2265139045_il2cpp_TypeInfo_var;
extern const uint32_t Debug_LogException_m2264672311_MetadataUsageId;
extern "C"  void Debug_LogException_m2264672311 (Il2CppObject * __this /* static, unused */, Exception_t3991598821 * ___exception0, Object_t3071478659 * ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogException_m2264672311_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = Debug_get_logger_m275693158(NULL /*static, unused*/, /*hidden argument*/NULL);
		Exception_t3991598821 * L_1 = ___exception0;
		Object_t3071478659 * L_2 = ___context1;
		NullCheck(L_0);
		InterfaceActionInvoker2< Exception_t3991598821 *, Object_t3071478659 * >::Invoke(1 /* System.Void UnityEngine.ILogHandler::LogException(System.Exception,UnityEngine.Object) */, ILogHandler_t2265139045_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return;
	}
}
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* ILogger_t629411471_il2cpp_TypeInfo_var;
extern const uint32_t Debug_LogWarning_m3123317694_MetadataUsageId;
extern "C"  void Debug_LogWarning_m3123317694 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogWarning_m3123317694_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = Debug_get_logger_m275693158(NULL /*static, unused*/, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___message0;
		NullCheck(L_0);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(0 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object) */, ILogger_t629411471_il2cpp_TypeInfo_var, L_0, 2, L_1);
		return;
	}
}
// System.Void UnityEngine.Debug::LogWarning(System.Object,UnityEngine.Object)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* ILogger_t629411471_il2cpp_TypeInfo_var;
extern const uint32_t Debug_LogWarning_m4097176146_MetadataUsageId;
extern "C"  void Debug_LogWarning_m4097176146 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, Object_t3071478659 * ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogWarning_m4097176146_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = Debug_get_logger_m275693158(NULL /*static, unused*/, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___message0;
		Object_t3071478659 * L_2 = ___context1;
		NullCheck(L_0);
		InterfaceActionInvoker3< int32_t, Il2CppObject *, Object_t3071478659 * >::Invoke(1 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object,UnityEngine.Object) */, ILogger_t629411471_il2cpp_TypeInfo_var, L_0, 2, L_1, L_2);
		return;
	}
}
// System.Boolean UnityEngine.Debug::get_isDebugBuild()
extern "C"  bool Debug_get_isDebugBuild_m351497798 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Debug_get_isDebugBuild_m351497798_ftn) ();
	static Debug_get_isDebugBuild_m351497798_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Debug_get_isDebugBuild_m351497798_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Debug::get_isDebugBuild()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.DebugLogHandler::.ctor()
extern "C"  void DebugLogHandler__ctor_m1521597024 (DebugLogHandler_t2406589519 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.DebugLogHandler::Internal_Log(UnityEngine.LogType,System.String,UnityEngine.Object)
extern "C"  void DebugLogHandler_Internal_Log_m3731237167 (Il2CppObject * __this /* static, unused */, int32_t ___level0, String_t* ___msg1, Object_t3071478659 * ___obj2, const MethodInfo* method)
{
	typedef void (*DebugLogHandler_Internal_Log_m3731237167_ftn) (int32_t, String_t*, Object_t3071478659 *);
	static DebugLogHandler_Internal_Log_m3731237167_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DebugLogHandler_Internal_Log_m3731237167_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.DebugLogHandler::Internal_Log(UnityEngine.LogType,System.String,UnityEngine.Object)");
	_il2cpp_icall_func(___level0, ___msg1, ___obj2);
}
// System.Void UnityEngine.DebugLogHandler::Internal_LogException(System.Exception,UnityEngine.Object)
extern "C"  void DebugLogHandler_Internal_LogException_m1007136309 (Il2CppObject * __this /* static, unused */, Exception_t3991598821 * ___exception0, Object_t3071478659 * ___obj1, const MethodInfo* method)
{
	typedef void (*DebugLogHandler_Internal_LogException_m1007136309_ftn) (Exception_t3991598821 *, Object_t3071478659 *);
	static DebugLogHandler_Internal_LogException_m1007136309_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DebugLogHandler_Internal_LogException_m1007136309_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.DebugLogHandler::Internal_LogException(System.Exception,UnityEngine.Object)");
	_il2cpp_icall_func(___exception0, ___obj1);
}
// System.Void UnityEngine.DebugLogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[])
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t DebugLogHandler_LogFormat_m1190453926_MetadataUsageId;
extern "C"  void DebugLogHandler_LogFormat_m1190453926 (DebugLogHandler_t2406589519 * __this, int32_t ___logType0, Object_t3071478659 * ___context1, String_t* ___format2, ObjectU5BU5D_t1108656482* ___args3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DebugLogHandler_LogFormat_m1190453926_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___logType0;
		String_t* L_1 = ___format2;
		ObjectU5BU5D_t1108656482* L_2 = ___args3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m4050103162(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Object_t3071478659 * L_4 = ___context1;
		DebugLogHandler_Internal_Log_m3731237167(NULL /*static, unused*/, L_0, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.DebugLogHandler::LogException(System.Exception,UnityEngine.Object)
extern "C"  void DebugLogHandler_LogException_m3623531313 (DebugLogHandler_t2406589519 * __this, Exception_t3991598821 * ___exception0, Object_t3071478659 * ___context1, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ___exception0;
		Object_t3071478659 * L_1 = ___context1;
		DebugLogHandler_Internal_LogException_m1007136309(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.DisallowMultipleComponent::.ctor()
extern "C"  void DisallowMultipleComponent__ctor_m3170514567 (DisallowMultipleComponent_t62111112 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Display::.ctor()
extern "C"  void Display__ctor_m923501527 (Display_t1321072632 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		IntPtr_t L_0;
		memset(&L_0, 0, sizeof(L_0));
		IntPtr__ctor_m2136600871(&L_0, 0, /*hidden argument*/NULL);
		__this->set_nativeDisplay_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Display::.ctor(System.IntPtr)
extern "C"  void Display__ctor_m3179075037 (Display_t1321072632 * __this, IntPtr_t ___nativeDisplay0, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = ___nativeDisplay0;
		__this->set_nativeDisplay_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Display::.cctor()
extern Il2CppClass* DisplayU5BU5D_t3684569385_il2cpp_TypeInfo_var;
extern Il2CppClass* Display_t1321072632_il2cpp_TypeInfo_var;
extern const uint32_t Display__cctor_m2376647350_MetadataUsageId;
extern "C"  void Display__cctor_m2376647350 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Display__cctor_m2376647350_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DisplayU5BU5D_t3684569385* L_0 = ((DisplayU5BU5D_t3684569385*)SZArrayNew(DisplayU5BU5D_t3684569385_il2cpp_TypeInfo_var, (uint32_t)1));
		Display_t1321072632 * L_1 = (Display_t1321072632 *)il2cpp_codegen_object_new(Display_t1321072632_il2cpp_TypeInfo_var);
		Display__ctor_m923501527(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Display_t1321072632 *)L_1);
		((Display_t1321072632_StaticFields*)Display_t1321072632_il2cpp_TypeInfo_var->static_fields)->set_displays_1(L_0);
		DisplayU5BU5D_t3684569385* L_2 = ((Display_t1321072632_StaticFields*)Display_t1321072632_il2cpp_TypeInfo_var->static_fields)->get_displays_1();
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		int32_t L_3 = 0;
		Display_t1321072632 * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((Display_t1321072632_StaticFields*)Display_t1321072632_il2cpp_TypeInfo_var->static_fields)->set__mainDisplay_2(L_4);
		((Display_t1321072632_StaticFields*)Display_t1321072632_il2cpp_TypeInfo_var->static_fields)->set_onDisplaysUpdated_3((DisplaysUpdatedDelegate_t581305515 *)NULL);
		return;
	}
}
// System.Void UnityEngine.Display::RecreateDisplayList(System.IntPtr[])
extern Il2CppClass* DisplayU5BU5D_t3684569385_il2cpp_TypeInfo_var;
extern Il2CppClass* Display_t1321072632_il2cpp_TypeInfo_var;
extern const uint32_t Display_RecreateDisplayList_m581883884_MetadataUsageId;
extern "C"  void Display_RecreateDisplayList_m581883884 (Il2CppObject * __this /* static, unused */, IntPtrU5BU5D_t3228729122* ___nativeDisplay0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Display_RecreateDisplayList_m581883884_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IntPtrU5BU5D_t3228729122* L_0 = ___nativeDisplay0;
		NullCheck(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1321072632_il2cpp_TypeInfo_var);
		((Display_t1321072632_StaticFields*)Display_t1321072632_il2cpp_TypeInfo_var->static_fields)->set_displays_1(((DisplayU5BU5D_t3684569385*)SZArrayNew(DisplayU5BU5D_t3684569385_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length)))))));
		V_0 = 0;
		goto IL_0027;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1321072632_il2cpp_TypeInfo_var);
		DisplayU5BU5D_t3684569385* L_1 = ((Display_t1321072632_StaticFields*)Display_t1321072632_il2cpp_TypeInfo_var->static_fields)->get_displays_1();
		int32_t L_2 = V_0;
		IntPtrU5BU5D_t3228729122* L_3 = ___nativeDisplay0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		IntPtr_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		Display_t1321072632 * L_7 = (Display_t1321072632 *)il2cpp_codegen_object_new(Display_t1321072632_il2cpp_TypeInfo_var);
		Display__ctor_m3179075037(L_7, L_6, /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		ArrayElementTypeCheck (L_1, L_7);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (Display_t1321072632 *)L_7);
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0027:
	{
		int32_t L_9 = V_0;
		IntPtrU5BU5D_t3228729122* L_10 = ___nativeDisplay0;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1321072632_il2cpp_TypeInfo_var);
		DisplayU5BU5D_t3684569385* L_11 = ((Display_t1321072632_StaticFields*)Display_t1321072632_il2cpp_TypeInfo_var->static_fields)->get_displays_1();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		int32_t L_12 = 0;
		Display_t1321072632 * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		((Display_t1321072632_StaticFields*)Display_t1321072632_il2cpp_TypeInfo_var->static_fields)->set__mainDisplay_2(L_13);
		return;
	}
}
// System.Void UnityEngine.Display::FireDisplaysUpdated()
extern Il2CppClass* Display_t1321072632_il2cpp_TypeInfo_var;
extern const uint32_t Display_FireDisplaysUpdated_m3581134697_MetadataUsageId;
extern "C"  void Display_FireDisplaysUpdated_m3581134697 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Display_FireDisplaysUpdated_m3581134697_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1321072632_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t581305515 * L_0 = ((Display_t1321072632_StaticFields*)Display_t1321072632_il2cpp_TypeInfo_var->static_fields)->get_onDisplaysUpdated_3();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1321072632_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t581305515 * L_1 = ((Display_t1321072632_StaticFields*)Display_t1321072632_il2cpp_TypeInfo_var->static_fields)->get_onDisplaysUpdated_3();
		NullCheck(L_1);
		DisplaysUpdatedDelegate_Invoke_m3958989829(L_1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void DisplaysUpdatedDelegate__ctor_m3468699627 (DisplaysUpdatedDelegate_t581305515 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::Invoke()
extern "C"  void DisplaysUpdatedDelegate_Invoke_m3958989829 (DisplaysUpdatedDelegate_t581305515 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DisplaysUpdatedDelegate_Invoke_m3958989829((DisplaysUpdatedDelegate_t581305515 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t581305515 (DisplaysUpdatedDelegate_t581305515 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.IAsyncResult UnityEngine.Display/DisplaysUpdatedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DisplaysUpdatedDelegate_BeginInvoke_m3264685798 (DisplaysUpdatedDelegate_t581305515 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void DisplaysUpdatedDelegate_EndInvoke_m3466741627 (DisplaysUpdatedDelegate_t581305515 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.DrivenRectTransformTracker::Add(UnityEngine.Object,UnityEngine.RectTransform,UnityEngine.DrivenTransformProperties)
extern "C"  void DrivenRectTransformTracker_Add_m3461523141 (DrivenRectTransformTracker_t4185719096 * __this, Object_t3071478659 * ___driver0, RectTransform_t972643934 * ___rectTransform1, int32_t ___drivenProperties2, const MethodInfo* method)
{
	{
		return;
	}
}
extern "C"  void DrivenRectTransformTracker_Add_m3461523141_AdjustorThunk (Il2CppObject * __this, Object_t3071478659 * ___driver0, RectTransform_t972643934 * ___rectTransform1, int32_t ___drivenProperties2, const MethodInfo* method)
{
	DrivenRectTransformTracker_t4185719096 * _thisAdjusted = reinterpret_cast<DrivenRectTransformTracker_t4185719096 *>(__this + 1);
	DrivenRectTransformTracker_Add_m3461523141(_thisAdjusted, ___driver0, ___rectTransform1, ___drivenProperties2, method);
}
// System.Void UnityEngine.DrivenRectTransformTracker::Clear()
extern "C"  void DrivenRectTransformTracker_Clear_m309315364 (DrivenRectTransformTracker_t4185719096 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
extern "C"  void DrivenRectTransformTracker_Clear_m309315364_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	DrivenRectTransformTracker_t4185719096 * _thisAdjusted = reinterpret_cast<DrivenRectTransformTracker_t4185719096 *>(__this + 1);
	DrivenRectTransformTracker_Clear_m309315364(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.DrivenRectTransformTracker
extern "C" void DrivenRectTransformTracker_t4185719096_marshal_pinvoke(const DrivenRectTransformTracker_t4185719096& unmarshaled, DrivenRectTransformTracker_t4185719096_marshaled_pinvoke& marshaled)
{
}
extern "C" void DrivenRectTransformTracker_t4185719096_marshal_pinvoke_back(const DrivenRectTransformTracker_t4185719096_marshaled_pinvoke& marshaled, DrivenRectTransformTracker_t4185719096& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngine.DrivenRectTransformTracker
extern "C" void DrivenRectTransformTracker_t4185719096_marshal_pinvoke_cleanup(DrivenRectTransformTracker_t4185719096_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.DrivenRectTransformTracker
extern "C" void DrivenRectTransformTracker_t4185719096_marshal_com(const DrivenRectTransformTracker_t4185719096& unmarshaled, DrivenRectTransformTracker_t4185719096_marshaled_com& marshaled)
{
}
extern "C" void DrivenRectTransformTracker_t4185719096_marshal_com_back(const DrivenRectTransformTracker_t4185719096_marshaled_com& marshaled, DrivenRectTransformTracker_t4185719096& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngine.DrivenRectTransformTracker
extern "C" void DrivenRectTransformTracker_t4185719096_marshal_com_cleanup(DrivenRectTransformTracker_t4185719096_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Event::.ctor()
extern "C"  void Event__ctor_m1609448063 (Event_t4196595728 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Event_Init_m3525277126(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Event::.ctor(System.Int32)
extern "C"  void Event__ctor_m66398544 (Event_t4196595728 * __this, int32_t ___displayIndex0, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___displayIndex0;
		Event_Init_m3525277126(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Event::Finalize()
extern "C"  void Event_Finalize_m2449826179 (Event_t4196595728 * __this, const MethodInfo* method)
{
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Event_Cleanup_m3160122945(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m3027285644(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0012:
	{
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.Event::get_mousePosition()
extern "C"  Vector2_t4282066565  Event_get_mousePosition_m3610425949 (Event_t4196595728 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Event_Internal_GetMousePosition_m2054214537(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Event::get_delta()
extern "C"  Vector2_t4282066565  Event_get_delta_m489813159 (Event_t4196595728 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Event_Internal_GetMouseDelta_m798523742(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_0 = V_0;
		return L_0;
	}
}
// System.Boolean UnityEngine.Event::get_shift()
extern "C"  bool Event_get_shift_m643038780 (Event_t4196595728 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Event_get_modifiers_m4020990886(__this, /*hidden argument*/NULL);
		return (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)1))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UnityEngine.Event::get_alt()
extern "C"  bool Event_get_alt_m2196461539 (Event_t4196595728 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Event_get_modifiers_m4020990886(__this, /*hidden argument*/NULL);
		return (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)4))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// UnityEngine.Event UnityEngine.Event::get_current()
extern Il2CppClass* Event_t4196595728_il2cpp_TypeInfo_var;
extern const uint32_t Event_get_current_m238587645_MetadataUsageId;
extern "C"  Event_t4196595728 * Event_get_current_m238587645 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Event_get_current_m238587645_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Event_t4196595728 * L_0 = ((Event_t4196595728_StaticFields*)Event_t4196595728_il2cpp_TypeInfo_var->static_fields)->get_s_Current_1();
		return L_0;
	}
}
// System.Void UnityEngine.Event::Internal_MakeMasterEventCurrent(System.Int32)
extern Il2CppClass* Event_t4196595728_il2cpp_TypeInfo_var;
extern const uint32_t Event_Internal_MakeMasterEventCurrent_m213199903_MetadataUsageId;
extern "C"  void Event_Internal_MakeMasterEventCurrent_m213199903 (Il2CppObject * __this /* static, unused */, int32_t ___displayIndex0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Event_Internal_MakeMasterEventCurrent_m213199903_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Event_t4196595728 * L_0 = ((Event_t4196595728_StaticFields*)Event_t4196595728_il2cpp_TypeInfo_var->static_fields)->get_s_MasterEvent_2();
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_1 = ___displayIndex0;
		Event_t4196595728 * L_2 = (Event_t4196595728 *)il2cpp_codegen_object_new(Event_t4196595728_il2cpp_TypeInfo_var);
		Event__ctor_m66398544(L_2, L_1, /*hidden argument*/NULL);
		((Event_t4196595728_StaticFields*)Event_t4196595728_il2cpp_TypeInfo_var->static_fields)->set_s_MasterEvent_2(L_2);
	}

IL_0015:
	{
		Event_t4196595728 * L_3 = ((Event_t4196595728_StaticFields*)Event_t4196595728_il2cpp_TypeInfo_var->static_fields)->get_s_MasterEvent_2();
		int32_t L_4 = ___displayIndex0;
		NullCheck(L_3);
		Event_set_displayIndex_m4130090435(L_3, L_4, /*hidden argument*/NULL);
		Event_t4196595728 * L_5 = ((Event_t4196595728_StaticFields*)Event_t4196595728_il2cpp_TypeInfo_var->static_fields)->get_s_MasterEvent_2();
		((Event_t4196595728_StaticFields*)Event_t4196595728_il2cpp_TypeInfo_var->static_fields)->set_s_Current_1(L_5);
		Event_t4196595728 * L_6 = ((Event_t4196595728_StaticFields*)Event_t4196595728_il2cpp_TypeInfo_var->static_fields)->get_s_MasterEvent_2();
		NullCheck(L_6);
		IntPtr_t L_7 = L_6->get_m_Ptr_0();
		Event_Internal_SetNativeEvent_m930902932(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Event::get_isKey()
extern "C"  bool Event_get_isKey_m645126607 (Event_t4196595728 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = Event_get_type_m2209939250(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)4)))
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = V_0;
		G_B3_0 = ((((int32_t)L_2) == ((int32_t)5))? 1 : 0);
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 1;
	}

IL_0015:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean UnityEngine.Event::get_isMouse()
extern "C"  bool Event_get_isMouse_m3551276757 (Event_t4196595728 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = Event_get_type_m2209939250(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)2)))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_4 = V_0;
		G_B5_0 = ((((int32_t)L_4) == ((int32_t)3))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B5_0 = 1;
	}

IL_0022:
	{
		return (bool)G_B5_0;
	}
}
// UnityEngine.Event UnityEngine.Event::KeyboardEvent(System.String)
extern const Il2CppType* KeyCode_t3128317986_0_0_0_var;
extern Il2CppClass* Event_t4196595728_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t1974256870_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2862688501_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1958628151_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m4235384975_MethodInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m337170132_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral89032;
extern Il2CppCodeGenString* _stringLiteral89063;
extern Il2CppCodeGenString* _stringLiteral89094;
extern Il2CppCodeGenString* _stringLiteral89125;
extern Il2CppCodeGenString* _stringLiteral89156;
extern Il2CppCodeGenString* _stringLiteral89187;
extern Il2CppCodeGenString* _stringLiteral89218;
extern Il2CppCodeGenString* _stringLiteral89249;
extern Il2CppCodeGenString* _stringLiteral89280;
extern Il2CppCodeGenString* _stringLiteral89311;
extern Il2CppCodeGenString* _stringLiteral88970;
extern Il2CppCodeGenString* _stringLiteral89001;
extern Il2CppCodeGenString* _stringLiteral88939;
extern Il2CppCodeGenString* _stringLiteral88877;
extern Il2CppCodeGenString* _stringLiteral89435;
extern Il2CppCodeGenString* _stringLiteral2471652291;
extern Il2CppCodeGenString* _stringLiteral2155144352;
extern Il2CppCodeGenString* _stringLiteral3739;
extern Il2CppCodeGenString* _stringLiteral3089570;
extern Il2CppCodeGenString* _stringLiteral3317767;
extern Il2CppCodeGenString* _stringLiteral108511772;
extern Il2CppCodeGenString* _stringLiteral3111174841;
extern Il2CppCodeGenString* _stringLiteral3208415;
extern Il2CppCodeGenString* _stringLiteral100571;
extern Il2CppCodeGenString* _stringLiteral3439314;
extern Il2CppCodeGenString* _stringLiteral3491358156;
extern Il2CppCodeGenString* _stringLiteral3304677145;
extern Il2CppCodeGenString* _stringLiteral825226131;
extern Il2CppCodeGenString* _stringLiteral1353507967;
extern Il2CppCodeGenString* _stringLiteral2959508907;
extern Il2CppCodeGenString* _stringLiteral114581;
extern Il2CppCodeGenString* _stringLiteral3212;
extern Il2CppCodeGenString* _stringLiteral3213;
extern Il2CppCodeGenString* _stringLiteral3214;
extern Il2CppCodeGenString* _stringLiteral3215;
extern Il2CppCodeGenString* _stringLiteral3216;
extern Il2CppCodeGenString* _stringLiteral3217;
extern Il2CppCodeGenString* _stringLiteral3218;
extern Il2CppCodeGenString* _stringLiteral3219;
extern Il2CppCodeGenString* _stringLiteral3220;
extern Il2CppCodeGenString* _stringLiteral99589;
extern Il2CppCodeGenString* _stringLiteral99590;
extern Il2CppCodeGenString* _stringLiteral99591;
extern Il2CppCodeGenString* _stringLiteral99592;
extern Il2CppCodeGenString* _stringLiteral99593;
extern Il2CppCodeGenString* _stringLiteral99594;
extern Il2CppCodeGenString* _stringLiteral87162979;
extern Il2CppCodeGenString* _stringLiteral3360570672;
extern Il2CppCodeGenString* _stringLiteral109637894;
extern Il2CppCodeGenString* _stringLiteral988116965;
extern const uint32_t Event_KeyboardEvent_m1236547202_MetadataUsageId;
extern "C"  Event_t4196595728 * Event_KeyboardEvent_m1236547202 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Event_KeyboardEvent_m1236547202_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Event_t4196595728 * V_0 = NULL;
	int32_t V_1 = 0;
	bool V_2 = false;
	String_t* V_3 = NULL;
	Il2CppChar V_4 = 0x0;
	String_t* V_5 = NULL;
	Dictionary_2_t1974256870 * V_6 = NULL;
	int32_t V_7 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Event_t4196595728 * L_0 = (Event_t4196595728 *)il2cpp_codegen_object_new(Event_t4196595728_il2cpp_TypeInfo_var);
		Event__ctor_m66398544(L_0, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		Event_t4196595728 * L_1 = V_0;
		NullCheck(L_1);
		Event_set_type_m1699027001(L_1, 4, /*hidden argument*/NULL);
		String_t* L_2 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001b;
		}
	}
	{
		Event_t4196595728 * L_4 = V_0;
		return L_4;
	}

IL_001b:
	{
		V_1 = 0;
		V_2 = (bool)0;
	}

IL_001f:
	{
		V_2 = (bool)1;
		int32_t L_5 = V_1;
		String_t* L_6 = ___key0;
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m2979997331(L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_5) < ((int32_t)L_7)))
		{
			goto IL_0034;
		}
	}
	{
		V_2 = (bool)0;
		goto IL_00ce;
	}

IL_0034:
	{
		String_t* L_8 = ___key0;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		Il2CppChar L_10 = String_get_Chars_m3015341861(L_8, L_9, /*hidden argument*/NULL);
		V_4 = L_10;
		Il2CppChar L_11 = V_4;
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)35))) == 0)
		{
			goto IL_00aa;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)35))) == 1)
		{
			goto IL_0057;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)35))) == 2)
		{
			goto IL_0093;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)35))) == 3)
		{
			goto IL_0065;
		}
	}

IL_0057:
	{
		Il2CppChar L_12 = V_4;
		if ((((int32_t)L_12) == ((int32_t)((int32_t)94))))
		{
			goto IL_007c;
		}
	}
	{
		goto IL_00c1;
	}

IL_0065:
	{
		Event_t4196595728 * L_13 = V_0;
		Event_t4196595728 * L_14 = L_13;
		NullCheck(L_14);
		int32_t L_15 = Event_get_modifiers_m4020990886(L_14, /*hidden argument*/NULL);
		NullCheck(L_14);
		Event_set_modifiers_m755733059(L_14, ((int32_t)((int32_t)L_15|(int32_t)4)), /*hidden argument*/NULL);
		int32_t L_16 = V_1;
		V_1 = ((int32_t)((int32_t)L_16+(int32_t)1));
		goto IL_00c8;
	}

IL_007c:
	{
		Event_t4196595728 * L_17 = V_0;
		Event_t4196595728 * L_18 = L_17;
		NullCheck(L_18);
		int32_t L_19 = Event_get_modifiers_m4020990886(L_18, /*hidden argument*/NULL);
		NullCheck(L_18);
		Event_set_modifiers_m755733059(L_18, ((int32_t)((int32_t)L_19|(int32_t)2)), /*hidden argument*/NULL);
		int32_t L_20 = V_1;
		V_1 = ((int32_t)((int32_t)L_20+(int32_t)1));
		goto IL_00c8;
	}

IL_0093:
	{
		Event_t4196595728 * L_21 = V_0;
		Event_t4196595728 * L_22 = L_21;
		NullCheck(L_22);
		int32_t L_23 = Event_get_modifiers_m4020990886(L_22, /*hidden argument*/NULL);
		NullCheck(L_22);
		Event_set_modifiers_m755733059(L_22, ((int32_t)((int32_t)L_23|(int32_t)8)), /*hidden argument*/NULL);
		int32_t L_24 = V_1;
		V_1 = ((int32_t)((int32_t)L_24+(int32_t)1));
		goto IL_00c8;
	}

IL_00aa:
	{
		Event_t4196595728 * L_25 = V_0;
		Event_t4196595728 * L_26 = L_25;
		NullCheck(L_26);
		int32_t L_27 = Event_get_modifiers_m4020990886(L_26, /*hidden argument*/NULL);
		NullCheck(L_26);
		Event_set_modifiers_m755733059(L_26, ((int32_t)((int32_t)L_27|(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_28 = V_1;
		V_1 = ((int32_t)((int32_t)L_28+(int32_t)1));
		goto IL_00c8;
	}

IL_00c1:
	{
		V_2 = (bool)0;
		goto IL_00c8;
	}

IL_00c8:
	{
		bool L_29 = V_2;
		if (L_29)
		{
			goto IL_001f;
		}
	}

IL_00ce:
	{
		String_t* L_30 = ___key0;
		int32_t L_31 = V_1;
		String_t* L_32 = ___key0;
		NullCheck(L_32);
		int32_t L_33 = String_get_Length_m2979997331(L_32, /*hidden argument*/NULL);
		int32_t L_34 = V_1;
		NullCheck(L_30);
		String_t* L_35 = String_Substring_m675079568(L_30, L_31, ((int32_t)((int32_t)L_33-(int32_t)L_34)), /*hidden argument*/NULL);
		NullCheck(L_35);
		String_t* L_36 = String_ToLower_m2421900555(L_35, /*hidden argument*/NULL);
		V_3 = L_36;
		String_t* L_37 = V_3;
		V_5 = L_37;
		String_t* L_38 = V_5;
		if (!L_38)
		{
			goto IL_09e6;
		}
	}
	{
		Dictionary_2_t1974256870 * L_39 = ((Event_t4196595728_StaticFields*)Event_t4196595728_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map0_3();
		if (L_39)
		{
			goto IL_03ac;
		}
	}
	{
		Dictionary_2_t1974256870 * L_40 = (Dictionary_2_t1974256870 *)il2cpp_codegen_object_new(Dictionary_2_t1974256870_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1958628151(L_40, ((int32_t)49), /*hidden argument*/Dictionary_2__ctor_m1958628151_MethodInfo_var);
		V_6 = L_40;
		Dictionary_2_t1974256870 * L_41 = V_6;
		NullCheck(L_41);
		Dictionary_2_Add_m4235384975(L_41, _stringLiteral89032, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_42 = V_6;
		NullCheck(L_42);
		Dictionary_2_Add_m4235384975(L_42, _stringLiteral89063, 1, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_43 = V_6;
		NullCheck(L_43);
		Dictionary_2_Add_m4235384975(L_43, _stringLiteral89094, 2, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_44 = V_6;
		NullCheck(L_44);
		Dictionary_2_Add_m4235384975(L_44, _stringLiteral89125, 3, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_45 = V_6;
		NullCheck(L_45);
		Dictionary_2_Add_m4235384975(L_45, _stringLiteral89156, 4, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_46 = V_6;
		NullCheck(L_46);
		Dictionary_2_Add_m4235384975(L_46, _stringLiteral89187, 5, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_47 = V_6;
		NullCheck(L_47);
		Dictionary_2_Add_m4235384975(L_47, _stringLiteral89218, 6, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_48 = V_6;
		NullCheck(L_48);
		Dictionary_2_Add_m4235384975(L_48, _stringLiteral89249, 7, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_49 = V_6;
		NullCheck(L_49);
		Dictionary_2_Add_m4235384975(L_49, _stringLiteral89280, 8, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_50 = V_6;
		NullCheck(L_50);
		Dictionary_2_Add_m4235384975(L_50, _stringLiteral89311, ((int32_t)9), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_51 = V_6;
		NullCheck(L_51);
		Dictionary_2_Add_m4235384975(L_51, _stringLiteral88970, ((int32_t)10), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_52 = V_6;
		NullCheck(L_52);
		Dictionary_2_Add_m4235384975(L_52, _stringLiteral89001, ((int32_t)11), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_53 = V_6;
		NullCheck(L_53);
		Dictionary_2_Add_m4235384975(L_53, _stringLiteral88939, ((int32_t)12), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_54 = V_6;
		NullCheck(L_54);
		Dictionary_2_Add_m4235384975(L_54, _stringLiteral88877, ((int32_t)13), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_55 = V_6;
		NullCheck(L_55);
		Dictionary_2_Add_m4235384975(L_55, _stringLiteral89435, ((int32_t)14), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_56 = V_6;
		NullCheck(L_56);
		Dictionary_2_Add_m4235384975(L_56, _stringLiteral2471652291, ((int32_t)15), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_57 = V_6;
		NullCheck(L_57);
		Dictionary_2_Add_m4235384975(L_57, _stringLiteral2155144352, ((int32_t)16), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_58 = V_6;
		NullCheck(L_58);
		Dictionary_2_Add_m4235384975(L_58, _stringLiteral3739, ((int32_t)17), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_59 = V_6;
		NullCheck(L_59);
		Dictionary_2_Add_m4235384975(L_59, _stringLiteral3089570, ((int32_t)18), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_60 = V_6;
		NullCheck(L_60);
		Dictionary_2_Add_m4235384975(L_60, _stringLiteral3317767, ((int32_t)19), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_61 = V_6;
		NullCheck(L_61);
		Dictionary_2_Add_m4235384975(L_61, _stringLiteral108511772, ((int32_t)20), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_62 = V_6;
		NullCheck(L_62);
		Dictionary_2_Add_m4235384975(L_62, _stringLiteral3111174841, ((int32_t)21), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_63 = V_6;
		NullCheck(L_63);
		Dictionary_2_Add_m4235384975(L_63, _stringLiteral3208415, ((int32_t)22), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_64 = V_6;
		NullCheck(L_64);
		Dictionary_2_Add_m4235384975(L_64, _stringLiteral100571, ((int32_t)23), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_65 = V_6;
		NullCheck(L_65);
		Dictionary_2_Add_m4235384975(L_65, _stringLiteral3439314, ((int32_t)24), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_66 = V_6;
		NullCheck(L_66);
		Dictionary_2_Add_m4235384975(L_66, _stringLiteral3491358156, ((int32_t)25), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_67 = V_6;
		NullCheck(L_67);
		Dictionary_2_Add_m4235384975(L_67, _stringLiteral3304677145, ((int32_t)26), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_68 = V_6;
		NullCheck(L_68);
		Dictionary_2_Add_m4235384975(L_68, _stringLiteral825226131, ((int32_t)27), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_69 = V_6;
		NullCheck(L_69);
		Dictionary_2_Add_m4235384975(L_69, _stringLiteral1353507967, ((int32_t)28), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_70 = V_6;
		NullCheck(L_70);
		Dictionary_2_Add_m4235384975(L_70, _stringLiteral2959508907, ((int32_t)29), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_71 = V_6;
		NullCheck(L_71);
		Dictionary_2_Add_m4235384975(L_71, _stringLiteral114581, ((int32_t)30), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_72 = V_6;
		NullCheck(L_72);
		Dictionary_2_Add_m4235384975(L_72, _stringLiteral3212, ((int32_t)31), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_73 = V_6;
		NullCheck(L_73);
		Dictionary_2_Add_m4235384975(L_73, _stringLiteral3213, ((int32_t)32), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_74 = V_6;
		NullCheck(L_74);
		Dictionary_2_Add_m4235384975(L_74, _stringLiteral3214, ((int32_t)33), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_75 = V_6;
		NullCheck(L_75);
		Dictionary_2_Add_m4235384975(L_75, _stringLiteral3215, ((int32_t)34), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_76 = V_6;
		NullCheck(L_76);
		Dictionary_2_Add_m4235384975(L_76, _stringLiteral3216, ((int32_t)35), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_77 = V_6;
		NullCheck(L_77);
		Dictionary_2_Add_m4235384975(L_77, _stringLiteral3217, ((int32_t)36), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_78 = V_6;
		NullCheck(L_78);
		Dictionary_2_Add_m4235384975(L_78, _stringLiteral3218, ((int32_t)37), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_79 = V_6;
		NullCheck(L_79);
		Dictionary_2_Add_m4235384975(L_79, _stringLiteral3219, ((int32_t)38), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_80 = V_6;
		NullCheck(L_80);
		Dictionary_2_Add_m4235384975(L_80, _stringLiteral3220, ((int32_t)39), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_81 = V_6;
		NullCheck(L_81);
		Dictionary_2_Add_m4235384975(L_81, _stringLiteral99589, ((int32_t)40), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_82 = V_6;
		NullCheck(L_82);
		Dictionary_2_Add_m4235384975(L_82, _stringLiteral99590, ((int32_t)41), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_83 = V_6;
		NullCheck(L_83);
		Dictionary_2_Add_m4235384975(L_83, _stringLiteral99591, ((int32_t)42), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_84 = V_6;
		NullCheck(L_84);
		Dictionary_2_Add_m4235384975(L_84, _stringLiteral99592, ((int32_t)43), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_85 = V_6;
		NullCheck(L_85);
		Dictionary_2_Add_m4235384975(L_85, _stringLiteral99593, ((int32_t)44), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_86 = V_6;
		NullCheck(L_86);
		Dictionary_2_Add_m4235384975(L_86, _stringLiteral99594, ((int32_t)45), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_87 = V_6;
		NullCheck(L_87);
		Dictionary_2_Add_m4235384975(L_87, _stringLiteral87162979, ((int32_t)46), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_88 = V_6;
		NullCheck(L_88);
		Dictionary_2_Add_m4235384975(L_88, _stringLiteral3360570672, ((int32_t)47), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_89 = V_6;
		NullCheck(L_89);
		Dictionary_2_Add_m4235384975(L_89, _stringLiteral109637894, ((int32_t)48), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_90 = V_6;
		((Event_t4196595728_StaticFields*)Event_t4196595728_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__switchU24map0_3(L_90);
	}

IL_03ac:
	{
		Dictionary_2_t1974256870 * L_91 = ((Event_t4196595728_StaticFields*)Event_t4196595728_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map0_3();
		String_t* L_92 = V_5;
		NullCheck(L_91);
		bool L_93 = Dictionary_2_TryGetValue_m337170132(L_91, L_92, (&V_7), /*hidden argument*/Dictionary_2_TryGetValue_m337170132_MethodInfo_var);
		if (!L_93)
		{
			goto IL_09e6;
		}
	}
	{
		int32_t L_94 = V_7;
		if (L_94 == 0)
		{
			goto IL_048f;
		}
		if (L_94 == 1)
		{
			goto IL_04a7;
		}
		if (L_94 == 2)
		{
			goto IL_04bf;
		}
		if (L_94 == 3)
		{
			goto IL_04d7;
		}
		if (L_94 == 4)
		{
			goto IL_04ef;
		}
		if (L_94 == 5)
		{
			goto IL_0507;
		}
		if (L_94 == 6)
		{
			goto IL_051f;
		}
		if (L_94 == 7)
		{
			goto IL_0537;
		}
		if (L_94 == 8)
		{
			goto IL_054f;
		}
		if (L_94 == 9)
		{
			goto IL_0567;
		}
		if (L_94 == 10)
		{
			goto IL_057f;
		}
		if (L_94 == 11)
		{
			goto IL_0597;
		}
		if (L_94 == 12)
		{
			goto IL_05af;
		}
		if (L_94 == 13)
		{
			goto IL_05c7;
		}
		if (L_94 == 14)
		{
			goto IL_05df;
		}
		if (L_94 == 15)
		{
			goto IL_05f7;
		}
		if (L_94 == 16)
		{
			goto IL_060f;
		}
		if (L_94 == 17)
		{
			goto IL_0627;
		}
		if (L_94 == 18)
		{
			goto IL_0646;
		}
		if (L_94 == 19)
		{
			goto IL_0665;
		}
		if (L_94 == 20)
		{
			goto IL_0684;
		}
		if (L_94 == 21)
		{
			goto IL_06a3;
		}
		if (L_94 == 22)
		{
			goto IL_06c2;
		}
		if (L_94 == 23)
		{
			goto IL_06e1;
		}
		if (L_94 == 24)
		{
			goto IL_0700;
		}
		if (L_94 == 25)
		{
			goto IL_071f;
		}
		if (L_94 == 26)
		{
			goto IL_073e;
		}
		if (L_94 == 27)
		{
			goto IL_075d;
		}
		if (L_94 == 28)
		{
			goto IL_077c;
		}
		if (L_94 == 29)
		{
			goto IL_0797;
		}
		if (L_94 == 30)
		{
			goto IL_07b3;
		}
		if (L_94 == 31)
		{
			goto IL_07c0;
		}
		if (L_94 == 32)
		{
			goto IL_07df;
		}
		if (L_94 == 33)
		{
			goto IL_07fe;
		}
		if (L_94 == 34)
		{
			goto IL_081d;
		}
		if (L_94 == 35)
		{
			goto IL_083c;
		}
		if (L_94 == 36)
		{
			goto IL_085b;
		}
		if (L_94 == 37)
		{
			goto IL_087a;
		}
		if (L_94 == 38)
		{
			goto IL_0899;
		}
		if (L_94 == 39)
		{
			goto IL_08b8;
		}
		if (L_94 == 40)
		{
			goto IL_08d7;
		}
		if (L_94 == 41)
		{
			goto IL_08f6;
		}
		if (L_94 == 42)
		{
			goto IL_0915;
		}
		if (L_94 == 43)
		{
			goto IL_0934;
		}
		if (L_94 == 44)
		{
			goto IL_0953;
		}
		if (L_94 == 45)
		{
			goto IL_0972;
		}
		if (L_94 == 46)
		{
			goto IL_0991;
		}
		if (L_94 == 47)
		{
			goto IL_099e;
		}
		if (L_94 == 48)
		{
			goto IL_09c2;
		}
	}
	{
		goto IL_09e6;
	}

IL_048f:
	{
		Event_t4196595728 * L_95 = V_0;
		NullCheck(L_95);
		Event_set_character_m1518464756(L_95, ((int32_t)48), /*hidden argument*/NULL);
		Event_t4196595728 * L_96 = V_0;
		NullCheck(L_96);
		Event_set_keyCode_m1503068465(L_96, ((int32_t)256), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_04a7:
	{
		Event_t4196595728 * L_97 = V_0;
		NullCheck(L_97);
		Event_set_character_m1518464756(L_97, ((int32_t)49), /*hidden argument*/NULL);
		Event_t4196595728 * L_98 = V_0;
		NullCheck(L_98);
		Event_set_keyCode_m1503068465(L_98, ((int32_t)257), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_04bf:
	{
		Event_t4196595728 * L_99 = V_0;
		NullCheck(L_99);
		Event_set_character_m1518464756(L_99, ((int32_t)50), /*hidden argument*/NULL);
		Event_t4196595728 * L_100 = V_0;
		NullCheck(L_100);
		Event_set_keyCode_m1503068465(L_100, ((int32_t)258), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_04d7:
	{
		Event_t4196595728 * L_101 = V_0;
		NullCheck(L_101);
		Event_set_character_m1518464756(L_101, ((int32_t)51), /*hidden argument*/NULL);
		Event_t4196595728 * L_102 = V_0;
		NullCheck(L_102);
		Event_set_keyCode_m1503068465(L_102, ((int32_t)259), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_04ef:
	{
		Event_t4196595728 * L_103 = V_0;
		NullCheck(L_103);
		Event_set_character_m1518464756(L_103, ((int32_t)52), /*hidden argument*/NULL);
		Event_t4196595728 * L_104 = V_0;
		NullCheck(L_104);
		Event_set_keyCode_m1503068465(L_104, ((int32_t)260), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_0507:
	{
		Event_t4196595728 * L_105 = V_0;
		NullCheck(L_105);
		Event_set_character_m1518464756(L_105, ((int32_t)53), /*hidden argument*/NULL);
		Event_t4196595728 * L_106 = V_0;
		NullCheck(L_106);
		Event_set_keyCode_m1503068465(L_106, ((int32_t)261), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_051f:
	{
		Event_t4196595728 * L_107 = V_0;
		NullCheck(L_107);
		Event_set_character_m1518464756(L_107, ((int32_t)54), /*hidden argument*/NULL);
		Event_t4196595728 * L_108 = V_0;
		NullCheck(L_108);
		Event_set_keyCode_m1503068465(L_108, ((int32_t)262), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_0537:
	{
		Event_t4196595728 * L_109 = V_0;
		NullCheck(L_109);
		Event_set_character_m1518464756(L_109, ((int32_t)55), /*hidden argument*/NULL);
		Event_t4196595728 * L_110 = V_0;
		NullCheck(L_110);
		Event_set_keyCode_m1503068465(L_110, ((int32_t)263), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_054f:
	{
		Event_t4196595728 * L_111 = V_0;
		NullCheck(L_111);
		Event_set_character_m1518464756(L_111, ((int32_t)56), /*hidden argument*/NULL);
		Event_t4196595728 * L_112 = V_0;
		NullCheck(L_112);
		Event_set_keyCode_m1503068465(L_112, ((int32_t)264), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_0567:
	{
		Event_t4196595728 * L_113 = V_0;
		NullCheck(L_113);
		Event_set_character_m1518464756(L_113, ((int32_t)57), /*hidden argument*/NULL);
		Event_t4196595728 * L_114 = V_0;
		NullCheck(L_114);
		Event_set_keyCode_m1503068465(L_114, ((int32_t)265), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_057f:
	{
		Event_t4196595728 * L_115 = V_0;
		NullCheck(L_115);
		Event_set_character_m1518464756(L_115, ((int32_t)46), /*hidden argument*/NULL);
		Event_t4196595728 * L_116 = V_0;
		NullCheck(L_116);
		Event_set_keyCode_m1503068465(L_116, ((int32_t)266), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_0597:
	{
		Event_t4196595728 * L_117 = V_0;
		NullCheck(L_117);
		Event_set_character_m1518464756(L_117, ((int32_t)47), /*hidden argument*/NULL);
		Event_t4196595728 * L_118 = V_0;
		NullCheck(L_118);
		Event_set_keyCode_m1503068465(L_118, ((int32_t)267), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_05af:
	{
		Event_t4196595728 * L_119 = V_0;
		NullCheck(L_119);
		Event_set_character_m1518464756(L_119, ((int32_t)45), /*hidden argument*/NULL);
		Event_t4196595728 * L_120 = V_0;
		NullCheck(L_120);
		Event_set_keyCode_m1503068465(L_120, ((int32_t)269), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_05c7:
	{
		Event_t4196595728 * L_121 = V_0;
		NullCheck(L_121);
		Event_set_character_m1518464756(L_121, ((int32_t)43), /*hidden argument*/NULL);
		Event_t4196595728 * L_122 = V_0;
		NullCheck(L_122);
		Event_set_keyCode_m1503068465(L_122, ((int32_t)270), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_05df:
	{
		Event_t4196595728 * L_123 = V_0;
		NullCheck(L_123);
		Event_set_character_m1518464756(L_123, ((int32_t)61), /*hidden argument*/NULL);
		Event_t4196595728 * L_124 = V_0;
		NullCheck(L_124);
		Event_set_keyCode_m1503068465(L_124, ((int32_t)272), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_05f7:
	{
		Event_t4196595728 * L_125 = V_0;
		NullCheck(L_125);
		Event_set_character_m1518464756(L_125, ((int32_t)61), /*hidden argument*/NULL);
		Event_t4196595728 * L_126 = V_0;
		NullCheck(L_126);
		Event_set_keyCode_m1503068465(L_126, ((int32_t)272), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_060f:
	{
		Event_t4196595728 * L_127 = V_0;
		NullCheck(L_127);
		Event_set_character_m1518464756(L_127, ((int32_t)10), /*hidden argument*/NULL);
		Event_t4196595728 * L_128 = V_0;
		NullCheck(L_128);
		Event_set_keyCode_m1503068465(L_128, ((int32_t)271), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_0627:
	{
		Event_t4196595728 * L_129 = V_0;
		NullCheck(L_129);
		Event_set_keyCode_m1503068465(L_129, ((int32_t)273), /*hidden argument*/NULL);
		Event_t4196595728 * L_130 = V_0;
		Event_t4196595728 * L_131 = L_130;
		NullCheck(L_131);
		int32_t L_132 = Event_get_modifiers_m4020990886(L_131, /*hidden argument*/NULL);
		NullCheck(L_131);
		Event_set_modifiers_m755733059(L_131, ((int32_t)((int32_t)L_132|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_0646:
	{
		Event_t4196595728 * L_133 = V_0;
		NullCheck(L_133);
		Event_set_keyCode_m1503068465(L_133, ((int32_t)274), /*hidden argument*/NULL);
		Event_t4196595728 * L_134 = V_0;
		Event_t4196595728 * L_135 = L_134;
		NullCheck(L_135);
		int32_t L_136 = Event_get_modifiers_m4020990886(L_135, /*hidden argument*/NULL);
		NullCheck(L_135);
		Event_set_modifiers_m755733059(L_135, ((int32_t)((int32_t)L_136|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_0665:
	{
		Event_t4196595728 * L_137 = V_0;
		NullCheck(L_137);
		Event_set_keyCode_m1503068465(L_137, ((int32_t)276), /*hidden argument*/NULL);
		Event_t4196595728 * L_138 = V_0;
		Event_t4196595728 * L_139 = L_138;
		NullCheck(L_139);
		int32_t L_140 = Event_get_modifiers_m4020990886(L_139, /*hidden argument*/NULL);
		NullCheck(L_139);
		Event_set_modifiers_m755733059(L_139, ((int32_t)((int32_t)L_140|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_0684:
	{
		Event_t4196595728 * L_141 = V_0;
		NullCheck(L_141);
		Event_set_keyCode_m1503068465(L_141, ((int32_t)275), /*hidden argument*/NULL);
		Event_t4196595728 * L_142 = V_0;
		Event_t4196595728 * L_143 = L_142;
		NullCheck(L_143);
		int32_t L_144 = Event_get_modifiers_m4020990886(L_143, /*hidden argument*/NULL);
		NullCheck(L_143);
		Event_set_modifiers_m755733059(L_143, ((int32_t)((int32_t)L_144|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_06a3:
	{
		Event_t4196595728 * L_145 = V_0;
		NullCheck(L_145);
		Event_set_keyCode_m1503068465(L_145, ((int32_t)277), /*hidden argument*/NULL);
		Event_t4196595728 * L_146 = V_0;
		Event_t4196595728 * L_147 = L_146;
		NullCheck(L_147);
		int32_t L_148 = Event_get_modifiers_m4020990886(L_147, /*hidden argument*/NULL);
		NullCheck(L_147);
		Event_set_modifiers_m755733059(L_147, ((int32_t)((int32_t)L_148|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_06c2:
	{
		Event_t4196595728 * L_149 = V_0;
		NullCheck(L_149);
		Event_set_keyCode_m1503068465(L_149, ((int32_t)278), /*hidden argument*/NULL);
		Event_t4196595728 * L_150 = V_0;
		Event_t4196595728 * L_151 = L_150;
		NullCheck(L_151);
		int32_t L_152 = Event_get_modifiers_m4020990886(L_151, /*hidden argument*/NULL);
		NullCheck(L_151);
		Event_set_modifiers_m755733059(L_151, ((int32_t)((int32_t)L_152|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_06e1:
	{
		Event_t4196595728 * L_153 = V_0;
		NullCheck(L_153);
		Event_set_keyCode_m1503068465(L_153, ((int32_t)279), /*hidden argument*/NULL);
		Event_t4196595728 * L_154 = V_0;
		Event_t4196595728 * L_155 = L_154;
		NullCheck(L_155);
		int32_t L_156 = Event_get_modifiers_m4020990886(L_155, /*hidden argument*/NULL);
		NullCheck(L_155);
		Event_set_modifiers_m755733059(L_155, ((int32_t)((int32_t)L_156|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_0700:
	{
		Event_t4196595728 * L_157 = V_0;
		NullCheck(L_157);
		Event_set_keyCode_m1503068465(L_157, ((int32_t)281), /*hidden argument*/NULL);
		Event_t4196595728 * L_158 = V_0;
		Event_t4196595728 * L_159 = L_158;
		NullCheck(L_159);
		int32_t L_160 = Event_get_modifiers_m4020990886(L_159, /*hidden argument*/NULL);
		NullCheck(L_159);
		Event_set_modifiers_m755733059(L_159, ((int32_t)((int32_t)L_160|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_071f:
	{
		Event_t4196595728 * L_161 = V_0;
		NullCheck(L_161);
		Event_set_keyCode_m1503068465(L_161, ((int32_t)280), /*hidden argument*/NULL);
		Event_t4196595728 * L_162 = V_0;
		Event_t4196595728 * L_163 = L_162;
		NullCheck(L_163);
		int32_t L_164 = Event_get_modifiers_m4020990886(L_163, /*hidden argument*/NULL);
		NullCheck(L_163);
		Event_set_modifiers_m755733059(L_163, ((int32_t)((int32_t)L_164|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_073e:
	{
		Event_t4196595728 * L_165 = V_0;
		NullCheck(L_165);
		Event_set_keyCode_m1503068465(L_165, ((int32_t)280), /*hidden argument*/NULL);
		Event_t4196595728 * L_166 = V_0;
		Event_t4196595728 * L_167 = L_166;
		NullCheck(L_167);
		int32_t L_168 = Event_get_modifiers_m4020990886(L_167, /*hidden argument*/NULL);
		NullCheck(L_167);
		Event_set_modifiers_m755733059(L_167, ((int32_t)((int32_t)L_168|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_075d:
	{
		Event_t4196595728 * L_169 = V_0;
		NullCheck(L_169);
		Event_set_keyCode_m1503068465(L_169, ((int32_t)281), /*hidden argument*/NULL);
		Event_t4196595728 * L_170 = V_0;
		Event_t4196595728 * L_171 = L_170;
		NullCheck(L_171);
		int32_t L_172 = Event_get_modifiers_m4020990886(L_171, /*hidden argument*/NULL);
		NullCheck(L_171);
		Event_set_modifiers_m755733059(L_171, ((int32_t)((int32_t)L_172|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_077c:
	{
		Event_t4196595728 * L_173 = V_0;
		NullCheck(L_173);
		Event_set_keyCode_m1503068465(L_173, 8, /*hidden argument*/NULL);
		Event_t4196595728 * L_174 = V_0;
		Event_t4196595728 * L_175 = L_174;
		NullCheck(L_175);
		int32_t L_176 = Event_get_modifiers_m4020990886(L_175, /*hidden argument*/NULL);
		NullCheck(L_175);
		Event_set_modifiers_m755733059(L_175, ((int32_t)((int32_t)L_176|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_0797:
	{
		Event_t4196595728 * L_177 = V_0;
		NullCheck(L_177);
		Event_set_keyCode_m1503068465(L_177, ((int32_t)127), /*hidden argument*/NULL);
		Event_t4196595728 * L_178 = V_0;
		Event_t4196595728 * L_179 = L_178;
		NullCheck(L_179);
		int32_t L_180 = Event_get_modifiers_m4020990886(L_179, /*hidden argument*/NULL);
		NullCheck(L_179);
		Event_set_modifiers_m755733059(L_179, ((int32_t)((int32_t)L_180|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_07b3:
	{
		Event_t4196595728 * L_181 = V_0;
		NullCheck(L_181);
		Event_set_keyCode_m1503068465(L_181, ((int32_t)9), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_07c0:
	{
		Event_t4196595728 * L_182 = V_0;
		NullCheck(L_182);
		Event_set_keyCode_m1503068465(L_182, ((int32_t)282), /*hidden argument*/NULL);
		Event_t4196595728 * L_183 = V_0;
		Event_t4196595728 * L_184 = L_183;
		NullCheck(L_184);
		int32_t L_185 = Event_get_modifiers_m4020990886(L_184, /*hidden argument*/NULL);
		NullCheck(L_184);
		Event_set_modifiers_m755733059(L_184, ((int32_t)((int32_t)L_185|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_07df:
	{
		Event_t4196595728 * L_186 = V_0;
		NullCheck(L_186);
		Event_set_keyCode_m1503068465(L_186, ((int32_t)283), /*hidden argument*/NULL);
		Event_t4196595728 * L_187 = V_0;
		Event_t4196595728 * L_188 = L_187;
		NullCheck(L_188);
		int32_t L_189 = Event_get_modifiers_m4020990886(L_188, /*hidden argument*/NULL);
		NullCheck(L_188);
		Event_set_modifiers_m755733059(L_188, ((int32_t)((int32_t)L_189|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_07fe:
	{
		Event_t4196595728 * L_190 = V_0;
		NullCheck(L_190);
		Event_set_keyCode_m1503068465(L_190, ((int32_t)284), /*hidden argument*/NULL);
		Event_t4196595728 * L_191 = V_0;
		Event_t4196595728 * L_192 = L_191;
		NullCheck(L_192);
		int32_t L_193 = Event_get_modifiers_m4020990886(L_192, /*hidden argument*/NULL);
		NullCheck(L_192);
		Event_set_modifiers_m755733059(L_192, ((int32_t)((int32_t)L_193|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_081d:
	{
		Event_t4196595728 * L_194 = V_0;
		NullCheck(L_194);
		Event_set_keyCode_m1503068465(L_194, ((int32_t)285), /*hidden argument*/NULL);
		Event_t4196595728 * L_195 = V_0;
		Event_t4196595728 * L_196 = L_195;
		NullCheck(L_196);
		int32_t L_197 = Event_get_modifiers_m4020990886(L_196, /*hidden argument*/NULL);
		NullCheck(L_196);
		Event_set_modifiers_m755733059(L_196, ((int32_t)((int32_t)L_197|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_083c:
	{
		Event_t4196595728 * L_198 = V_0;
		NullCheck(L_198);
		Event_set_keyCode_m1503068465(L_198, ((int32_t)286), /*hidden argument*/NULL);
		Event_t4196595728 * L_199 = V_0;
		Event_t4196595728 * L_200 = L_199;
		NullCheck(L_200);
		int32_t L_201 = Event_get_modifiers_m4020990886(L_200, /*hidden argument*/NULL);
		NullCheck(L_200);
		Event_set_modifiers_m755733059(L_200, ((int32_t)((int32_t)L_201|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_085b:
	{
		Event_t4196595728 * L_202 = V_0;
		NullCheck(L_202);
		Event_set_keyCode_m1503068465(L_202, ((int32_t)287), /*hidden argument*/NULL);
		Event_t4196595728 * L_203 = V_0;
		Event_t4196595728 * L_204 = L_203;
		NullCheck(L_204);
		int32_t L_205 = Event_get_modifiers_m4020990886(L_204, /*hidden argument*/NULL);
		NullCheck(L_204);
		Event_set_modifiers_m755733059(L_204, ((int32_t)((int32_t)L_205|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_087a:
	{
		Event_t4196595728 * L_206 = V_0;
		NullCheck(L_206);
		Event_set_keyCode_m1503068465(L_206, ((int32_t)288), /*hidden argument*/NULL);
		Event_t4196595728 * L_207 = V_0;
		Event_t4196595728 * L_208 = L_207;
		NullCheck(L_208);
		int32_t L_209 = Event_get_modifiers_m4020990886(L_208, /*hidden argument*/NULL);
		NullCheck(L_208);
		Event_set_modifiers_m755733059(L_208, ((int32_t)((int32_t)L_209|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_0899:
	{
		Event_t4196595728 * L_210 = V_0;
		NullCheck(L_210);
		Event_set_keyCode_m1503068465(L_210, ((int32_t)289), /*hidden argument*/NULL);
		Event_t4196595728 * L_211 = V_0;
		Event_t4196595728 * L_212 = L_211;
		NullCheck(L_212);
		int32_t L_213 = Event_get_modifiers_m4020990886(L_212, /*hidden argument*/NULL);
		NullCheck(L_212);
		Event_set_modifiers_m755733059(L_212, ((int32_t)((int32_t)L_213|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_08b8:
	{
		Event_t4196595728 * L_214 = V_0;
		NullCheck(L_214);
		Event_set_keyCode_m1503068465(L_214, ((int32_t)290), /*hidden argument*/NULL);
		Event_t4196595728 * L_215 = V_0;
		Event_t4196595728 * L_216 = L_215;
		NullCheck(L_216);
		int32_t L_217 = Event_get_modifiers_m4020990886(L_216, /*hidden argument*/NULL);
		NullCheck(L_216);
		Event_set_modifiers_m755733059(L_216, ((int32_t)((int32_t)L_217|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_08d7:
	{
		Event_t4196595728 * L_218 = V_0;
		NullCheck(L_218);
		Event_set_keyCode_m1503068465(L_218, ((int32_t)291), /*hidden argument*/NULL);
		Event_t4196595728 * L_219 = V_0;
		Event_t4196595728 * L_220 = L_219;
		NullCheck(L_220);
		int32_t L_221 = Event_get_modifiers_m4020990886(L_220, /*hidden argument*/NULL);
		NullCheck(L_220);
		Event_set_modifiers_m755733059(L_220, ((int32_t)((int32_t)L_221|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_08f6:
	{
		Event_t4196595728 * L_222 = V_0;
		NullCheck(L_222);
		Event_set_keyCode_m1503068465(L_222, ((int32_t)292), /*hidden argument*/NULL);
		Event_t4196595728 * L_223 = V_0;
		Event_t4196595728 * L_224 = L_223;
		NullCheck(L_224);
		int32_t L_225 = Event_get_modifiers_m4020990886(L_224, /*hidden argument*/NULL);
		NullCheck(L_224);
		Event_set_modifiers_m755733059(L_224, ((int32_t)((int32_t)L_225|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_0915:
	{
		Event_t4196595728 * L_226 = V_0;
		NullCheck(L_226);
		Event_set_keyCode_m1503068465(L_226, ((int32_t)293), /*hidden argument*/NULL);
		Event_t4196595728 * L_227 = V_0;
		Event_t4196595728 * L_228 = L_227;
		NullCheck(L_228);
		int32_t L_229 = Event_get_modifiers_m4020990886(L_228, /*hidden argument*/NULL);
		NullCheck(L_228);
		Event_set_modifiers_m755733059(L_228, ((int32_t)((int32_t)L_229|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_0934:
	{
		Event_t4196595728 * L_230 = V_0;
		NullCheck(L_230);
		Event_set_keyCode_m1503068465(L_230, ((int32_t)294), /*hidden argument*/NULL);
		Event_t4196595728 * L_231 = V_0;
		Event_t4196595728 * L_232 = L_231;
		NullCheck(L_232);
		int32_t L_233 = Event_get_modifiers_m4020990886(L_232, /*hidden argument*/NULL);
		NullCheck(L_232);
		Event_set_modifiers_m755733059(L_232, ((int32_t)((int32_t)L_233|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_0953:
	{
		Event_t4196595728 * L_234 = V_0;
		NullCheck(L_234);
		Event_set_keyCode_m1503068465(L_234, ((int32_t)295), /*hidden argument*/NULL);
		Event_t4196595728 * L_235 = V_0;
		Event_t4196595728 * L_236 = L_235;
		NullCheck(L_236);
		int32_t L_237 = Event_get_modifiers_m4020990886(L_236, /*hidden argument*/NULL);
		NullCheck(L_236);
		Event_set_modifiers_m755733059(L_236, ((int32_t)((int32_t)L_237|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_0972:
	{
		Event_t4196595728 * L_238 = V_0;
		NullCheck(L_238);
		Event_set_keyCode_m1503068465(L_238, ((int32_t)296), /*hidden argument*/NULL);
		Event_t4196595728 * L_239 = V_0;
		Event_t4196595728 * L_240 = L_239;
		NullCheck(L_240);
		int32_t L_241 = Event_get_modifiers_m4020990886(L_240, /*hidden argument*/NULL);
		NullCheck(L_240);
		Event_set_modifiers_m755733059(L_240, ((int32_t)((int32_t)L_241|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_0991:
	{
		Event_t4196595728 * L_242 = V_0;
		NullCheck(L_242);
		Event_set_keyCode_m1503068465(L_242, ((int32_t)27), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_099e:
	{
		Event_t4196595728 * L_243 = V_0;
		NullCheck(L_243);
		Event_set_character_m1518464756(L_243, ((int32_t)10), /*hidden argument*/NULL);
		Event_t4196595728 * L_244 = V_0;
		NullCheck(L_244);
		Event_set_keyCode_m1503068465(L_244, ((int32_t)13), /*hidden argument*/NULL);
		Event_t4196595728 * L_245 = V_0;
		Event_t4196595728 * L_246 = L_245;
		NullCheck(L_246);
		int32_t L_247 = Event_get_modifiers_m4020990886(L_246, /*hidden argument*/NULL);
		NullCheck(L_246);
		Event_set_modifiers_m755733059(L_246, ((int32_t)((int32_t)L_247&(int32_t)((int32_t)-65))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_09c2:
	{
		Event_t4196595728 * L_248 = V_0;
		NullCheck(L_248);
		Event_set_keyCode_m1503068465(L_248, ((int32_t)32), /*hidden argument*/NULL);
		Event_t4196595728 * L_249 = V_0;
		NullCheck(L_249);
		Event_set_character_m1518464756(L_249, ((int32_t)32), /*hidden argument*/NULL);
		Event_t4196595728 * L_250 = V_0;
		Event_t4196595728 * L_251 = L_250;
		NullCheck(L_251);
		int32_t L_252 = Event_get_modifiers_m4020990886(L_251, /*hidden argument*/NULL);
		NullCheck(L_251);
		Event_set_modifiers_m755733059(L_251, ((int32_t)((int32_t)L_252&(int32_t)((int32_t)-65))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_09e6:
	{
		String_t* L_253 = V_3;
		NullCheck(L_253);
		int32_t L_254 = String_get_Length_m2979997331(L_253, /*hidden argument*/NULL);
		if ((((int32_t)L_254) == ((int32_t)1)))
		{
			goto IL_0a37;
		}
	}

IL_09f2:
	try
	{ // begin try (depth: 1)
		Event_t4196595728 * L_255 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_256 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(KeyCode_t3128317986_0_0_0_var), /*hidden argument*/NULL);
		String_t* L_257 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		Il2CppObject * L_258 = Enum_Parse_m1799348482(NULL /*static, unused*/, L_256, L_257, (bool)1, /*hidden argument*/NULL);
		NullCheck(L_255);
		Event_set_keyCode_m1503068465(L_255, ((*(int32_t*)((int32_t*)UnBox (L_258, Int32_t1153838500_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		goto IL_0a32;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (ArgumentException_t928607144_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0a13;
		throw e;
	}

CATCH_0a13:
	{ // begin catch(System.ArgumentException)
		ObjectU5BU5D_t1108656482* L_259 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_260 = V_3;
		NullCheck(L_259);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_259, 0);
		ArrayElementTypeCheck (L_259, L_260);
		(L_259)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_260);
		String_t* L_261 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral988116965, L_259, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, L_261, /*hidden argument*/NULL);
		goto IL_0a32;
	} // end catch (depth: 1)

IL_0a32:
	{
		goto IL_0a67;
	}

IL_0a37:
	{
		Event_t4196595728 * L_262 = V_0;
		String_t* L_263 = V_3;
		NullCheck(L_263);
		String_t* L_264 = String_ToLower_m2421900555(L_263, /*hidden argument*/NULL);
		NullCheck(L_264);
		Il2CppChar L_265 = String_get_Chars_m3015341861(L_264, 0, /*hidden argument*/NULL);
		NullCheck(L_262);
		Event_set_character_m1518464756(L_262, L_265, /*hidden argument*/NULL);
		Event_t4196595728 * L_266 = V_0;
		Event_t4196595728 * L_267 = V_0;
		NullCheck(L_267);
		Il2CppChar L_268 = Event_get_character_m3663172667(L_267, /*hidden argument*/NULL);
		NullCheck(L_266);
		Event_set_keyCode_m1503068465(L_266, L_268, /*hidden argument*/NULL);
		Event_t4196595728 * L_269 = V_0;
		NullCheck(L_269);
		int32_t L_270 = Event_get_modifiers_m4020990886(L_269, /*hidden argument*/NULL);
		if (!L_270)
		{
			goto IL_0a67;
		}
	}
	{
		Event_t4196595728 * L_271 = V_0;
		NullCheck(L_271);
		Event_set_character_m1518464756(L_271, 0, /*hidden argument*/NULL);
	}

IL_0a67:
	{
		goto IL_0a6c;
	}

IL_0a6c:
	{
		Event_t4196595728 * L_272 = V_0;
		return L_272;
	}
}
// System.Int32 UnityEngine.Event::GetHashCode()
extern "C"  int32_t Event_GetHashCode_m2341647470 (Event_t4196595728 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Vector2_t4282066565  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		V_0 = 1;
		bool L_0 = Event_get_isKey_m645126607(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_1 = Event_get_keyCode_m1820698462(__this, /*hidden argument*/NULL);
		V_0 = (((int32_t)((uint16_t)L_1)));
	}

IL_0015:
	{
		bool L_2 = Event_get_isMouse_m3551276757(__this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		Vector2_t4282066565  L_3 = Event_get_mousePosition_m3610425949(__this, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = Vector2_GetHashCode_m128434585((&V_1), /*hidden argument*/NULL);
		V_0 = L_4;
	}

IL_002f:
	{
		int32_t L_5 = V_0;
		int32_t L_6 = Event_get_modifiers_m4020990886(__this, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_5*(int32_t)((int32_t)37)))|(int32_t)L_6));
		int32_t L_7 = V_0;
		return L_7;
	}
}
// System.Boolean UnityEngine.Event::Equals(System.Object)
extern Il2CppClass* Event_t4196595728_il2cpp_TypeInfo_var;
extern const uint32_t Event_Equals_m2374615830_MetadataUsageId;
extern "C"  bool Event_Equals_m2374615830 (Event_t4196595728 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Event_Equals_m2374615830_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Event_t4196595728 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (bool)0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___obj0;
		bool L_2 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, __this, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0016;
		}
	}
	{
		return (bool)1;
	}

IL_0016:
	{
		Il2CppObject * L_3 = ___obj0;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m2022236990(L_3, /*hidden argument*/NULL);
		Type_t * L_5 = Object_GetType_m2022236990(__this, /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_0029;
		}
	}
	{
		return (bool)0;
	}

IL_0029:
	{
		Il2CppObject * L_6 = ___obj0;
		V_0 = ((Event_t4196595728 *)CastclassSealed(L_6, Event_t4196595728_il2cpp_TypeInfo_var));
		int32_t L_7 = Event_get_type_m2209939250(__this, /*hidden argument*/NULL);
		Event_t4196595728 * L_8 = V_0;
		NullCheck(L_8);
		int32_t L_9 = Event_get_type_m2209939250(L_8, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)L_9))))
		{
			goto IL_0058;
		}
	}
	{
		int32_t L_10 = Event_get_modifiers_m4020990886(__this, /*hidden argument*/NULL);
		Event_t4196595728 * L_11 = V_0;
		NullCheck(L_11);
		int32_t L_12 = Event_get_modifiers_m4020990886(L_11, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_10&(int32_t)((int32_t)-33)))) == ((int32_t)((int32_t)((int32_t)L_12&(int32_t)((int32_t)-33))))))
		{
			goto IL_005a;
		}
	}

IL_0058:
	{
		return (bool)0;
	}

IL_005a:
	{
		bool L_13 = Event_get_isKey_m645126607(__this, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0074;
		}
	}
	{
		int32_t L_14 = Event_get_keyCode_m1820698462(__this, /*hidden argument*/NULL);
		Event_t4196595728 * L_15 = V_0;
		NullCheck(L_15);
		int32_t L_16 = Event_get_keyCode_m1820698462(L_15, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_14) == ((int32_t)L_16))? 1 : 0);
	}

IL_0074:
	{
		bool L_17 = Event_get_isMouse_m3551276757(__this, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0091;
		}
	}
	{
		Vector2_t4282066565  L_18 = Event_get_mousePosition_m3610425949(__this, /*hidden argument*/NULL);
		Event_t4196595728 * L_19 = V_0;
		NullCheck(L_19);
		Vector2_t4282066565  L_20 = Event_get_mousePosition_m3610425949(L_19, /*hidden argument*/NULL);
		bool L_21 = Vector2_op_Equality_m1927481448(NULL /*static, unused*/, L_18, L_20, /*hidden argument*/NULL);
		return L_21;
	}

IL_0091:
	{
		return (bool)0;
	}
}
// System.String UnityEngine.Event::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* EventType_t637886954_il2cpp_TypeInfo_var;
extern Il2CppClass* EventModifiers_t4195406918_il2cpp_TypeInfo_var;
extern Il2CppClass* KeyCode_t3128317986_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector2_t4282066565_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral565784926;
extern Il2CppCodeGenString* _stringLiteral2087505152;
extern Il2CppCodeGenString* _stringLiteral607738481;
extern Il2CppCodeGenString* _stringLiteral559441790;
extern Il2CppCodeGenString* _stringLiteral3464389902;
extern Il2CppCodeGenString* _stringLiteral1980489796;
extern Il2CppCodeGenString* _stringLiteral1621281301;
extern const uint32_t Event_ToString_m3661517518_MetadataUsageId;
extern "C"  String_t* Event_ToString_m3661517518 (Event_t4196595728 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Event_ToString_m3661517518_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Event_get_isKey_m645126607(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_00b5;
		}
	}
	{
		Il2CppChar L_1 = Event_get_character_m3663172667(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0051;
		}
	}
	{
		ObjectU5BU5D_t1108656482* L_2 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)3));
		int32_t L_3 = Event_get_type_m2209939250(__this, /*hidden argument*/NULL);
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(EventType_t637886954_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_5);
		ObjectU5BU5D_t1108656482* L_6 = L_2;
		int32_t L_7 = Event_get_modifiers_m4020990886(__this, /*hidden argument*/NULL);
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(EventModifiers_t4195406918_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 1);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t1108656482* L_10 = L_6;
		int32_t L_11 = Event_get_keyCode_m1820698462(__this, /*hidden argument*/NULL);
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(KeyCode_t3128317986_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, L_13);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_13);
		String_t* L_14 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral565784926, L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0051:
	{
		ObjectU5BU5D_t1108656482* L_15 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)8));
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		ArrayElementTypeCheck (L_15, _stringLiteral2087505152);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2087505152);
		ObjectU5BU5D_t1108656482* L_16 = L_15;
		int32_t L_17 = Event_get_type_m2209939250(__this, /*hidden argument*/NULL);
		int32_t L_18 = L_17;
		Il2CppObject * L_19 = Box(EventType_t637886954_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 1);
		ArrayElementTypeCheck (L_16, L_19);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_19);
		ObjectU5BU5D_t1108656482* L_20 = L_16;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 2);
		ArrayElementTypeCheck (L_20, _stringLiteral607738481);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral607738481);
		ObjectU5BU5D_t1108656482* L_21 = L_20;
		Il2CppChar L_22 = Event_get_character_m3663172667(__this, /*hidden argument*/NULL);
		int32_t L_23 = ((int32_t)L_22);
		Il2CppObject * L_24 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_23);
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 3);
		ArrayElementTypeCheck (L_21, L_24);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_24);
		ObjectU5BU5D_t1108656482* L_25 = L_21;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 4);
		ArrayElementTypeCheck (L_25, _stringLiteral559441790);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral559441790);
		ObjectU5BU5D_t1108656482* L_26 = L_25;
		int32_t L_27 = Event_get_modifiers_m4020990886(__this, /*hidden argument*/NULL);
		int32_t L_28 = L_27;
		Il2CppObject * L_29 = Box(EventModifiers_t4195406918_il2cpp_TypeInfo_var, &L_28);
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 5);
		ArrayElementTypeCheck (L_26, L_29);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_29);
		ObjectU5BU5D_t1108656482* L_30 = L_26;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 6);
		ArrayElementTypeCheck (L_30, _stringLiteral3464389902);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral3464389902);
		ObjectU5BU5D_t1108656482* L_31 = L_30;
		int32_t L_32 = Event_get_keyCode_m1820698462(__this, /*hidden argument*/NULL);
		int32_t L_33 = L_32;
		Il2CppObject * L_34 = Box(KeyCode_t3128317986_il2cpp_TypeInfo_var, &L_33);
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 7);
		ArrayElementTypeCheck (L_31, L_34);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)L_34);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_35 = String_Concat_m3016520001(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		return L_35;
	}

IL_00b5:
	{
		bool L_36 = Event_get_isMouse_m3551276757(__this, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_00fb;
		}
	}
	{
		ObjectU5BU5D_t1108656482* L_37 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)3));
		int32_t L_38 = Event_get_type_m2209939250(__this, /*hidden argument*/NULL);
		int32_t L_39 = L_38;
		Il2CppObject * L_40 = Box(EventType_t637886954_il2cpp_TypeInfo_var, &L_39);
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, 0);
		ArrayElementTypeCheck (L_37, L_40);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_40);
		ObjectU5BU5D_t1108656482* L_41 = L_37;
		Vector2_t4282066565  L_42 = Event_get_mousePosition_m3610425949(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_43 = L_42;
		Il2CppObject * L_44 = Box(Vector2_t4282066565_il2cpp_TypeInfo_var, &L_43);
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, 1);
		ArrayElementTypeCheck (L_41, L_44);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_44);
		ObjectU5BU5D_t1108656482* L_45 = L_41;
		int32_t L_46 = Event_get_modifiers_m4020990886(__this, /*hidden argument*/NULL);
		int32_t L_47 = L_46;
		Il2CppObject * L_48 = Box(EventModifiers_t4195406918_il2cpp_TypeInfo_var, &L_47);
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, 2);
		ArrayElementTypeCheck (L_45, L_48);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_48);
		String_t* L_49 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral1980489796, L_45, /*hidden argument*/NULL);
		return L_49;
	}

IL_00fb:
	{
		int32_t L_50 = Event_get_type_m2209939250(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_50) == ((int32_t)((int32_t)14))))
		{
			goto IL_0115;
		}
	}
	{
		int32_t L_51 = Event_get_type_m2209939250(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_51) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_013d;
		}
	}

IL_0115:
	{
		ObjectU5BU5D_t1108656482* L_52 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_53 = Event_get_type_m2209939250(__this, /*hidden argument*/NULL);
		int32_t L_54 = L_53;
		Il2CppObject * L_55 = Box(EventType_t637886954_il2cpp_TypeInfo_var, &L_54);
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, 0);
		ArrayElementTypeCheck (L_52, L_55);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_55);
		ObjectU5BU5D_t1108656482* L_56 = L_52;
		String_t* L_57 = Event_get_commandName_m1197792621(__this, /*hidden argument*/NULL);
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, 1);
		ArrayElementTypeCheck (L_56, L_57);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_57);
		String_t* L_58 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral1621281301, L_56, /*hidden argument*/NULL);
		return L_58;
	}

IL_013d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_59 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		int32_t L_60 = Event_get_type_m2209939250(__this, /*hidden argument*/NULL);
		int32_t L_61 = L_60;
		Il2CppObject * L_62 = Box(EventType_t637886954_il2cpp_TypeInfo_var, &L_61);
		String_t* L_63 = String_Concat_m389863537(NULL /*static, unused*/, L_59, L_62, /*hidden argument*/NULL);
		return L_63;
	}
}
// System.Void UnityEngine.Event::Init(System.Int32)
extern "C"  void Event_Init_m3525277126 (Event_t4196595728 * __this, int32_t ___displayIndex0, const MethodInfo* method)
{
	typedef void (*Event_Init_m3525277126_ftn) (Event_t4196595728 *, int32_t);
	static Event_Init_m3525277126_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Init_m3525277126_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Init(System.Int32)");
	_il2cpp_icall_func(__this, ___displayIndex0);
}
// System.Void UnityEngine.Event::Cleanup()
extern "C"  void Event_Cleanup_m3160122945 (Event_t4196595728 * __this, const MethodInfo* method)
{
	typedef void (*Event_Cleanup_m3160122945_ftn) (Event_t4196595728 *);
	static Event_Cleanup_m3160122945_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Cleanup_m3160122945_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Cleanup()");
	_il2cpp_icall_func(__this);
}
// UnityEngine.EventType UnityEngine.Event::get_rawType()
extern "C"  int32_t Event_get_rawType_m696564524 (Event_t4196595728 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_rawType_m696564524_ftn) (Event_t4196595728 *);
	static Event_get_rawType_m696564524_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_rawType_m696564524_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_rawType()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.EventType UnityEngine.Event::get_type()
extern "C"  int32_t Event_get_type_m2209939250 (Event_t4196595728 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_type_m2209939250_ftn) (Event_t4196595728 *);
	static Event_get_type_m2209939250_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_type_m2209939250_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_type()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_type(UnityEngine.EventType)
extern "C"  void Event_set_type_m1699027001 (Event_t4196595728 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Event_set_type_m1699027001_ftn) (Event_t4196595728 *, int32_t);
	static Event_set_type_m1699027001_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_type_m1699027001_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_type(UnityEngine.EventType)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.EventType UnityEngine.Event::GetTypeForControl(System.Int32)
extern "C"  int32_t Event_GetTypeForControl_m854773288 (Event_t4196595728 * __this, int32_t ___controlID0, const MethodInfo* method)
{
	typedef int32_t (*Event_GetTypeForControl_m854773288_ftn) (Event_t4196595728 *, int32_t);
	static Event_GetTypeForControl_m854773288_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_GetTypeForControl_m854773288_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::GetTypeForControl(System.Int32)");
	return _il2cpp_icall_func(__this, ___controlID0);
}
// System.Void UnityEngine.Event::Internal_GetMousePosition(UnityEngine.Vector2&)
extern "C"  void Event_Internal_GetMousePosition_m2054214537 (Event_t4196595728 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*Event_Internal_GetMousePosition_m2054214537_ftn) (Event_t4196595728 *, Vector2_t4282066565 *);
	static Event_Internal_GetMousePosition_m2054214537_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Internal_GetMousePosition_m2054214537_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Internal_GetMousePosition(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Event::Internal_GetMouseDelta(UnityEngine.Vector2&)
extern "C"  void Event_Internal_GetMouseDelta_m798523742 (Event_t4196595728 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*Event_Internal_GetMouseDelta_m798523742_ftn) (Event_t4196595728 *, Vector2_t4282066565 *);
	static Event_Internal_GetMouseDelta_m798523742_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Internal_GetMouseDelta_m798523742_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Internal_GetMouseDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.EventModifiers UnityEngine.Event::get_modifiers()
extern "C"  int32_t Event_get_modifiers_m4020990886 (Event_t4196595728 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_modifiers_m4020990886_ftn) (Event_t4196595728 *);
	static Event_get_modifiers_m4020990886_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_modifiers_m4020990886_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_modifiers()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_modifiers(UnityEngine.EventModifiers)
extern "C"  void Event_set_modifiers_m755733059 (Event_t4196595728 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Event_set_modifiers_m755733059_ftn) (Event_t4196595728 *, int32_t);
	static Event_set_modifiers_m755733059_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_modifiers_m755733059_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_modifiers(UnityEngine.EventModifiers)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.Event::get_clickCount()
extern "C"  int32_t Event_get_clickCount_m57286197 (Event_t4196595728 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_clickCount_m57286197_ftn) (Event_t4196595728 *);
	static Event_get_clickCount_m57286197_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_clickCount_m57286197_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_clickCount()");
	return _il2cpp_icall_func(__this);
}
// System.Char UnityEngine.Event::get_character()
extern "C"  Il2CppChar Event_get_character_m3663172667 (Event_t4196595728 * __this, const MethodInfo* method)
{
	typedef Il2CppChar (*Event_get_character_m3663172667_ftn) (Event_t4196595728 *);
	static Event_get_character_m3663172667_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_character_m3663172667_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_character()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_character(System.Char)
extern "C"  void Event_set_character_m1518464756 (Event_t4196595728 * __this, Il2CppChar ___value0, const MethodInfo* method)
{
	typedef void (*Event_set_character_m1518464756_ftn) (Event_t4196595728 *, Il2CppChar);
	static Event_set_character_m1518464756_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_character_m1518464756_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_character(System.Char)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.String UnityEngine.Event::get_commandName()
extern "C"  String_t* Event_get_commandName_m1197792621 (Event_t4196595728 * __this, const MethodInfo* method)
{
	typedef String_t* (*Event_get_commandName_m1197792621_ftn) (Event_t4196595728 *);
	static Event_get_commandName_m1197792621_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_commandName_m1197792621_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_commandName()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.KeyCode UnityEngine.Event::get_keyCode()
extern "C"  int32_t Event_get_keyCode_m1820698462 (Event_t4196595728 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_keyCode_m1820698462_ftn) (Event_t4196595728 *);
	static Event_get_keyCode_m1820698462_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_keyCode_m1820698462_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_keyCode()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_keyCode(UnityEngine.KeyCode)
extern "C"  void Event_set_keyCode_m1503068465 (Event_t4196595728 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Event_set_keyCode_m1503068465_ftn) (Event_t4196595728 *, int32_t);
	static Event_set_keyCode_m1503068465_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_keyCode_m1503068465_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_keyCode(UnityEngine.KeyCode)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Event::Internal_SetNativeEvent(System.IntPtr)
extern "C"  void Event_Internal_SetNativeEvent_m930902932 (Il2CppObject * __this /* static, unused */, IntPtr_t ___ptr0, const MethodInfo* method)
{
	typedef void (*Event_Internal_SetNativeEvent_m930902932_ftn) (IntPtr_t);
	static Event_Internal_SetNativeEvent_m930902932_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Internal_SetNativeEvent_m930902932_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Internal_SetNativeEvent(System.IntPtr)");
	_il2cpp_icall_func(___ptr0);
}
// System.Void UnityEngine.Event::set_displayIndex(System.Int32)
extern "C"  void Event_set_displayIndex_m4130090435 (Event_t4196595728 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Event_set_displayIndex_m4130090435_ftn) (Event_t4196595728 *, int32_t);
	static Event_set_displayIndex_m4130090435_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_displayIndex_m4130090435_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_displayIndex(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Event::Use()
extern "C"  void Event_Use_m310777444 (Event_t4196595728 * __this, const MethodInfo* method)
{
	typedef void (*Event_Use_m310777444_ftn) (Event_t4196595728 *);
	static Event_Use_m310777444_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Use_m310777444_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Use()");
	_il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Event::PopEvent(UnityEngine.Event)
extern "C"  bool Event_PopEvent_m2790075015 (Il2CppObject * __this /* static, unused */, Event_t4196595728 * ___outEvent0, const MethodInfo* method)
{
	typedef bool (*Event_PopEvent_m2790075015_ftn) (Event_t4196595728 *);
	static Event_PopEvent_m2790075015_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_PopEvent_m2790075015_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::PopEvent(UnityEngine.Event)");
	return _il2cpp_icall_func(___outEvent0);
}
// Conversion methods for marshalling of: UnityEngine.Event
extern "C" void Event_t4196595728_marshal_pinvoke(const Event_t4196595728& unmarshaled, Event_t4196595728_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void Event_t4196595728_marshal_pinvoke_back(const Event_t4196595728_marshaled_pinvoke& marshaled, Event_t4196595728& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Event
extern "C" void Event_t4196595728_marshal_pinvoke_cleanup(Event_t4196595728_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Event
extern "C" void Event_t4196595728_marshal_com(const Event_t4196595728& unmarshaled, Event_t4196595728_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void Event_t4196595728_marshal_com_back(const Event_t4196595728_marshaled_com& marshaled, Event_t4196595728& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Event
extern "C" void Event_t4196595728_marshal_com_cleanup(Event_t4196595728_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Events.ArgumentCache::.ctor()
extern "C"  void ArgumentCache__ctor_m4004594141 (ArgumentCache_t1171347191 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.Events.ArgumentCache::get_unityObjectArgument()
extern "C"  Object_t3071478659 * ArgumentCache_get_unityObjectArgument_m3221396682 (ArgumentCache_t1171347191 * __this, const MethodInfo* method)
{
	{
		Object_t3071478659 * L_0 = __this->get_m_ObjectArgument_0();
		return L_0;
	}
}
// System.String UnityEngine.Events.ArgumentCache::get_unityObjectArgumentAssemblyTypeName()
extern "C"  String_t* ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m2706134059 (ArgumentCache_t1171347191 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		return L_0;
	}
}
// System.Int32 UnityEngine.Events.ArgumentCache::get_intArgument()
extern "C"  int32_t ArgumentCache_get_intArgument_m688472510 (ArgumentCache_t1171347191 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_IntArgument_2();
		return L_0;
	}
}
// System.Single UnityEngine.Events.ArgumentCache::get_floatArgument()
extern "C"  float ArgumentCache_get_floatArgument_m457274527 (ArgumentCache_t1171347191 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_FloatArgument_3();
		return L_0;
	}
}
// System.String UnityEngine.Events.ArgumentCache::get_stringArgument()
extern "C"  String_t* ArgumentCache_get_stringArgument_m621059457 (ArgumentCache_t1171347191 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_StringArgument_4();
		return L_0;
	}
}
// System.Boolean UnityEngine.Events.ArgumentCache::get_boolArgument()
extern "C"  bool ArgumentCache_get_boolArgument_m948935837 (ArgumentCache_t1171347191 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_BoolArgument_5();
		return L_0;
	}
}
// System.Void UnityEngine.Events.ArgumentCache::TidyAssemblyTypeName()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral313930745;
extern Il2CppCodeGenString* _stringLiteral2704938467;
extern Il2CppCodeGenString* _stringLiteral2013608998;
extern const uint32_t ArgumentCache_TidyAssemblyTypeName_m705559900_MetadataUsageId;
extern "C"  void ArgumentCache_TidyAssemblyTypeName_m705559900 (ArgumentCache_t1171347191 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArgumentCache_TidyAssemblyTypeName_m705559900_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		String_t* L_0 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		V_0 = ((int32_t)2147483647LL);
		String_t* L_2 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		NullCheck(L_2);
		int32_t L_3 = String_IndexOf_m1476794331(L_2, _stringLiteral313930745, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) == ((int32_t)(-1))))
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_5 = V_1;
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Min_m811624909(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
	}

IL_0037:
	{
		String_t* L_8 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		NullCheck(L_8);
		int32_t L_9 = String_IndexOf_m1476794331(L_8, _stringLiteral2704938467, /*hidden argument*/NULL);
		V_1 = L_9;
		int32_t L_10 = V_1;
		if ((((int32_t)L_10) == ((int32_t)(-1))))
		{
			goto IL_0057;
		}
	}
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		int32_t L_13 = Math_Min_m811624909(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
	}

IL_0057:
	{
		String_t* L_14 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		NullCheck(L_14);
		int32_t L_15 = String_IndexOf_m1476794331(L_14, _stringLiteral2013608998, /*hidden argument*/NULL);
		V_1 = L_15;
		int32_t L_16 = V_1;
		if ((((int32_t)L_16) == ((int32_t)(-1))))
		{
			goto IL_0077;
		}
	}
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		int32_t L_19 = Math_Min_m811624909(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		V_0 = L_19;
	}

IL_0077:
	{
		int32_t L_20 = V_0;
		if ((!(((uint32_t)L_20) == ((uint32_t)((int32_t)2147483647LL)))))
		{
			goto IL_0083;
		}
	}
	{
		return;
	}

IL_0083:
	{
		String_t* L_21 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		int32_t L_22 = V_0;
		NullCheck(L_21);
		String_t* L_23 = String_Substring_m675079568(L_21, 0, L_22, /*hidden argument*/NULL);
		__this->set_m_ObjectArgumentAssemblyTypeName_1(L_23);
		return;
	}
}
// System.Void UnityEngine.Events.ArgumentCache::OnBeforeSerialize()
extern "C"  void ArgumentCache_OnBeforeSerialize_m2323221949 (ArgumentCache_t1171347191 * __this, const MethodInfo* method)
{
	{
		ArgumentCache_TidyAssemblyTypeName_m705559900(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.ArgumentCache::OnAfterDeserialize()
extern "C"  void ArgumentCache_OnAfterDeserialize_m3764330697 (ArgumentCache_t1171347191 * __this, const MethodInfo* method)
{
	{
		ArgumentCache_TidyAssemblyTypeName_m705559900(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor()
extern "C"  void BaseInvokableCall__ctor_m2839815662 (BaseInvokableCall_t1559630662 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3414061457;
extern Il2CppCodeGenString* _stringLiteral1380938712;
extern const uint32_t BaseInvokableCall__ctor_m1692884045_MetadataUsageId;
extern "C"  void BaseInvokableCall__ctor_m1692884045 (BaseInvokableCall_t1559630662 * __this, Il2CppObject * ___target0, MethodInfo_t * ___function1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BaseInvokableCall__ctor_m1692884045_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___target0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, _stringLiteral3414061457, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		MethodInfo_t * L_2 = ___function1;
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_3 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_3, _stringLiteral1380938712, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0028:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.BaseInvokableCall::AllowInvoke(System.Delegate)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t BaseInvokableCall_AllowInvoke_m2941661095_MetadataUsageId;
extern "C"  bool BaseInvokableCall_AllowInvoke_m2941661095 (Il2CppObject * __this /* static, unused */, Delegate_t3310234105 * ___delegate0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BaseInvokableCall_AllowInvoke_m2941661095_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Object_t3071478659 * V_1 = NULL;
	{
		Delegate_t3310234105 * L_0 = ___delegate0;
		NullCheck(L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2860483769(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Il2CppObject * L_2 = V_0;
		if (L_2)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)1;
	}

IL_000f:
	{
		Il2CppObject * L_3 = V_0;
		V_1 = ((Object_t3071478659 *)IsInstClass(L_3, Object_t3071478659_il2cpp_TypeInfo_var));
		Object_t3071478659 * L_4 = V_1;
		bool L_5 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, L_4, NULL, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_002a;
		}
	}
	{
		Object_t3071478659 * L_6 = V_1;
		bool L_7 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		return L_7;
	}

IL_002a:
	{
		return (bool)1;
	}
}
// System.Void UnityEngine.Events.InvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern const Il2CppType* UnityAction_t594794173_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_t594794173_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall__ctor_m1927467228_MetadataUsageId;
extern "C"  void InvokableCall__ctor_m1927467228 (InvokableCall_t1277370263 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall__ctor_m1927467228_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		BaseInvokableCall__ctor_m1692884045(__this, L_0, L_1, /*hidden argument*/NULL);
		UnityAction_t594794173 * L_2 = __this->get_Delegate_0();
		MethodInfo_t * L_3 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(UnityAction_t594794173_0_0_0_var), /*hidden argument*/NULL);
		Il2CppObject * L_5 = ___target0;
		Delegate_t3310234105 * L_6 = NetFxCoreExtensions_CreateDelegate_m3408605866(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
		Delegate_t3310234105 * L_7 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_2, ((UnityAction_t594794173 *)CastclassSealed(L_6, UnityAction_t594794173_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_t594794173 *)CastclassSealed(L_7, UnityAction_t594794173_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall::.ctor(UnityEngine.Events.UnityAction)
extern Il2CppClass* UnityAction_t594794173_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall__ctor_m3567423952_MetadataUsageId;
extern "C"  void InvokableCall__ctor_m3567423952 (InvokableCall_t1277370263 * __this, UnityAction_t594794173 * ___action0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall__ctor_m3567423952_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		BaseInvokableCall__ctor_m2839815662(__this, /*hidden argument*/NULL);
		UnityAction_t594794173 * L_0 = __this->get_Delegate_0();
		UnityAction_t594794173 * L_1 = ___action0;
		Delegate_t3310234105 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_t594794173 *)CastclassSealed(L_2, UnityAction_t594794173_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall::Invoke(System.Object[])
extern "C"  void InvokableCall_Invoke_m1513455731 (InvokableCall_t1277370263 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method)
{
	{
		UnityAction_t594794173 * L_0 = __this->get_Delegate_0();
		bool L_1 = BaseInvokableCall_AllowInvoke_m2941661095(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		UnityAction_t594794173 * L_2 = __this->get_Delegate_0();
		NullCheck(L_2);
		UnityAction_Invoke_m1979593925(L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_Find_m2076119333 (InvokableCall_t1277370263 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		UnityAction_t594794173 * L_0 = __this->get_Delegate_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2860483769(L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0024;
		}
	}
	{
		UnityAction_t594794173 * L_3 = __this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m1628849205(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck(L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_4, L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0025;
	}

IL_0024:
	{
		G_B3_0 = 0;
	}

IL_0025:
	{
		return (bool)G_B3_0;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::.ctor()
extern Il2CppClass* List_1_t2927816214_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m825644024_MethodInfo_var;
extern const uint32_t InvokableCallList__ctor_m2413334847_MetadataUsageId;
extern "C"  void InvokableCallList__ctor_m2413334847 (InvokableCallList_t3597236437 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList__ctor_m2413334847_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t2927816214 * L_0 = (List_1_t2927816214 *)il2cpp_codegen_object_new(List_1_t2927816214_il2cpp_TypeInfo_var);
		List_1__ctor_m825644024(L_0, /*hidden argument*/List_1__ctor_m825644024_MethodInfo_var);
		__this->set_m_PersistentCalls_0(L_0);
		List_1_t2927816214 * L_1 = (List_1_t2927816214 *)il2cpp_codegen_object_new(List_1_t2927816214_il2cpp_TypeInfo_var);
		List_1__ctor_m825644024(L_1, /*hidden argument*/List_1__ctor_m825644024_MethodInfo_var);
		__this->set_m_RuntimeCalls_1(L_1);
		List_1_t2927816214 * L_2 = (List_1_t2927816214 *)il2cpp_codegen_object_new(List_1_t2927816214_il2cpp_TypeInfo_var);
		List_1__ctor_m825644024(L_2, /*hidden argument*/List_1__ctor_m825644024_MethodInfo_var);
		__this->set_m_ExecutingCalls_2(L_2);
		__this->set_m_NeedsUpdate_3((bool)1);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::AddPersistentInvokableCall(UnityEngine.Events.BaseInvokableCall)
extern const MethodInfo* List_1_Add_m520004840_MethodInfo_var;
extern const uint32_t InvokableCallList_AddPersistentInvokableCall_m4220625340_MetadataUsageId;
extern "C"  void InvokableCallList_AddPersistentInvokableCall_m4220625340 (InvokableCallList_t3597236437 * __this, BaseInvokableCall_t1559630662 * ___call0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList_AddPersistentInvokableCall_m4220625340_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t2927816214 * L_0 = __this->get_m_PersistentCalls_0();
		BaseInvokableCall_t1559630662 * L_1 = ___call0;
		NullCheck(L_0);
		List_1_Add_m520004840(L_0, L_1, /*hidden argument*/List_1_Add_m520004840_MethodInfo_var);
		__this->set_m_NeedsUpdate_3((bool)1);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::AddListener(UnityEngine.Events.BaseInvokableCall)
extern const MethodInfo* List_1_Add_m520004840_MethodInfo_var;
extern const uint32_t InvokableCallList_AddListener_m1356872764_MetadataUsageId;
extern "C"  void InvokableCallList_AddListener_m1356872764 (InvokableCallList_t3597236437 * __this, BaseInvokableCall_t1559630662 * ___call0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList_AddListener_m1356872764_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t2927816214 * L_0 = __this->get_m_RuntimeCalls_1();
		BaseInvokableCall_t1559630662 * L_1 = ___call0;
		NullCheck(L_0);
		List_1_Add_m520004840(L_0, L_1, /*hidden argument*/List_1_Add_m520004840_MethodInfo_var);
		__this->set_m_NeedsUpdate_3((bool)1);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* List_1_t2927816214_il2cpp_TypeInfo_var;
extern Il2CppClass* Predicate_1_t1170687545_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m825644024_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m641579417_MethodInfo_var;
extern const MethodInfo* List_1_Add_m520004840_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m1062916220_MethodInfo_var;
extern const MethodInfo* Predicate_1__ctor_m1324607315_MethodInfo_var;
extern const MethodInfo* List_1_RemoveAll_m1824039959_MethodInfo_var;
extern const uint32_t InvokableCallList_RemoveListener_m855004700_MetadataUsageId;
extern "C"  void InvokableCallList_RemoveListener_m855004700 (InvokableCallList_t3597236437 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList_RemoveListener_m855004700_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t2927816214 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		List_1_t2927816214 * L_0 = (List_1_t2927816214 *)il2cpp_codegen_object_new(List_1_t2927816214_il2cpp_TypeInfo_var);
		List_1__ctor_m825644024(L_0, /*hidden argument*/List_1__ctor_m825644024_MethodInfo_var);
		V_0 = L_0;
		V_1 = 0;
		goto IL_003b;
	}

IL_000d:
	{
		List_1_t2927816214 * L_1 = __this->get_m_RuntimeCalls_1();
		int32_t L_2 = V_1;
		NullCheck(L_1);
		BaseInvokableCall_t1559630662 * L_3 = List_1_get_Item_m641579417(L_1, L_2, /*hidden argument*/List_1_get_Item_m641579417_MethodInfo_var);
		Il2CppObject * L_4 = ___targetObj0;
		MethodInfo_t * L_5 = ___method1;
		NullCheck(L_3);
		bool L_6 = VirtFuncInvoker2< bool, Il2CppObject *, MethodInfo_t * >::Invoke(5 /* System.Boolean UnityEngine.Events.BaseInvokableCall::Find(System.Object,System.Reflection.MethodInfo) */, L_3, L_4, L_5);
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		List_1_t2927816214 * L_7 = V_0;
		List_1_t2927816214 * L_8 = __this->get_m_RuntimeCalls_1();
		int32_t L_9 = V_1;
		NullCheck(L_8);
		BaseInvokableCall_t1559630662 * L_10 = List_1_get_Item_m641579417(L_8, L_9, /*hidden argument*/List_1_get_Item_m641579417_MethodInfo_var);
		NullCheck(L_7);
		List_1_Add_m520004840(L_7, L_10, /*hidden argument*/List_1_Add_m520004840_MethodInfo_var);
	}

IL_0037:
	{
		int32_t L_11 = V_1;
		V_1 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_003b:
	{
		int32_t L_12 = V_1;
		List_1_t2927816214 * L_13 = __this->get_m_RuntimeCalls_1();
		NullCheck(L_13);
		int32_t L_14 = List_1_get_Count_m1062916220(L_13, /*hidden argument*/List_1_get_Count_m1062916220_MethodInfo_var);
		if ((((int32_t)L_12) < ((int32_t)L_14)))
		{
			goto IL_000d;
		}
	}
	{
		List_1_t2927816214 * L_15 = __this->get_m_RuntimeCalls_1();
		List_1_t2927816214 * L_16 = V_0;
		List_1_t2927816214 * L_17 = L_16;
		IntPtr_t L_18;
		L_18.set_m_value_0((void*)(void*)GetVirtualMethodInfo(L_17, 24));
		Predicate_1_t1170687545 * L_19 = (Predicate_1_t1170687545 *)il2cpp_codegen_object_new(Predicate_1_t1170687545_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m1324607315(L_19, L_17, L_18, /*hidden argument*/Predicate_1__ctor_m1324607315_MethodInfo_var);
		NullCheck(L_15);
		List_1_RemoveAll_m1824039959(L_15, L_19, /*hidden argument*/List_1_RemoveAll_m1824039959_MethodInfo_var);
		__this->set_m_NeedsUpdate_3((bool)1);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::ClearPersistent()
extern const MethodInfo* List_1_Clear_m2526744611_MethodInfo_var;
extern const uint32_t InvokableCallList_ClearPersistent_m1690187553_MetadataUsageId;
extern "C"  void InvokableCallList_ClearPersistent_m1690187553 (InvokableCallList_t3597236437 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList_ClearPersistent_m1690187553_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t2927816214 * L_0 = __this->get_m_PersistentCalls_0();
		NullCheck(L_0);
		List_1_Clear_m2526744611(L_0, /*hidden argument*/List_1_Clear_m2526744611_MethodInfo_var);
		__this->set_m_NeedsUpdate_3((bool)1);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::Invoke(System.Object[])
extern const MethodInfo* List_1_Clear_m2526744611_MethodInfo_var;
extern const MethodInfo* List_1_AddRange_m3828947330_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m641579417_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m1062916220_MethodInfo_var;
extern const uint32_t InvokableCallList_Invoke_m575840373_MetadataUsageId;
extern "C"  void InvokableCallList_Invoke_m575840373 (InvokableCallList_t3597236437 * __this, ObjectU5BU5D_t1108656482* ___parameters0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList_Invoke_m575840373_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_m_NeedsUpdate_3();
		if (!L_0)
		{
			goto IL_003f;
		}
	}
	{
		List_1_t2927816214 * L_1 = __this->get_m_ExecutingCalls_2();
		NullCheck(L_1);
		List_1_Clear_m2526744611(L_1, /*hidden argument*/List_1_Clear_m2526744611_MethodInfo_var);
		List_1_t2927816214 * L_2 = __this->get_m_ExecutingCalls_2();
		List_1_t2927816214 * L_3 = __this->get_m_PersistentCalls_0();
		NullCheck(L_2);
		List_1_AddRange_m3828947330(L_2, L_3, /*hidden argument*/List_1_AddRange_m3828947330_MethodInfo_var);
		List_1_t2927816214 * L_4 = __this->get_m_ExecutingCalls_2();
		List_1_t2927816214 * L_5 = __this->get_m_RuntimeCalls_1();
		NullCheck(L_4);
		List_1_AddRange_m3828947330(L_4, L_5, /*hidden argument*/List_1_AddRange_m3828947330_MethodInfo_var);
		__this->set_m_NeedsUpdate_3((bool)0);
	}

IL_003f:
	{
		V_0 = 0;
		goto IL_005c;
	}

IL_0046:
	{
		List_1_t2927816214 * L_6 = __this->get_m_ExecutingCalls_2();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		BaseInvokableCall_t1559630662 * L_8 = List_1_get_Item_m641579417(L_6, L_7, /*hidden argument*/List_1_get_Item_m641579417_MethodInfo_var);
		ObjectU5BU5D_t1108656482* L_9 = ___parameters0;
		NullCheck(L_8);
		VirtActionInvoker1< ObjectU5BU5D_t1108656482* >::Invoke(4 /* System.Void UnityEngine.Events.BaseInvokableCall::Invoke(System.Object[]) */, L_8, L_9);
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_005c:
	{
		int32_t L_11 = V_0;
		List_1_t2927816214 * L_12 = __this->get_m_ExecutingCalls_2();
		NullCheck(L_12);
		int32_t L_13 = List_1_get_Count_m1062916220(L_12, /*hidden argument*/List_1_get_Count_m1062916220_MethodInfo_var);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_0046;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.PersistentCall::.ctor()
extern Il2CppClass* ArgumentCache_t1171347191_il2cpp_TypeInfo_var;
extern const uint32_t PersistentCall__ctor_m3363339247_MetadataUsageId;
extern "C"  void PersistentCall__ctor_m3363339247 (PersistentCall_t2972625667 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PersistentCall__ctor_m3363339247_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArgumentCache_t1171347191 * L_0 = (ArgumentCache_t1171347191 *)il2cpp_codegen_object_new(ArgumentCache_t1171347191_il2cpp_TypeInfo_var);
		ArgumentCache__ctor_m4004594141(L_0, /*hidden argument*/NULL);
		__this->set_m_Arguments_3(L_0);
		__this->set_m_CallState_4(2);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.Events.PersistentCall::get_target()
extern "C"  Object_t3071478659 * PersistentCall_get_target_m3400889750 (PersistentCall_t2972625667 * __this, const MethodInfo* method)
{
	{
		Object_t3071478659 * L_0 = __this->get_m_Target_0();
		return L_0;
	}
}
// System.String UnityEngine.Events.PersistentCall::get_methodName()
extern "C"  String_t* PersistentCall_get_methodName_m3021973351 (PersistentCall_t2972625667 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_MethodName_1();
		return L_0;
	}
}
// UnityEngine.Events.PersistentListenerMode UnityEngine.Events.PersistentCall::get_mode()
extern "C"  int32_t PersistentCall_get_mode_m1522877562 (PersistentCall_t2972625667 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Mode_2();
		return L_0;
	}
}
// UnityEngine.Events.ArgumentCache UnityEngine.Events.PersistentCall::get_arguments()
extern "C"  ArgumentCache_t1171347191 * PersistentCall_get_arguments_m1871980514 (PersistentCall_t2972625667 * __this, const MethodInfo* method)
{
	{
		ArgumentCache_t1171347191 * L_0 = __this->get_m_Arguments_3();
		return L_0;
	}
}
// System.Boolean UnityEngine.Events.PersistentCall::IsValid()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t PersistentCall_IsValid_m1010437221_MetadataUsageId;
extern "C"  bool PersistentCall_IsValid_m1010437221 (PersistentCall_t2972625667 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PersistentCall_IsValid_m1010437221_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		Object_t3071478659 * L_0 = PersistentCall_get_target_m3400889750(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_2 = PersistentCall_get_methodName_m3021973351(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return (bool)G_B3_0;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetRuntimeCall(UnityEngine.Events.UnityEventBase)
extern Il2CppClass* CachedInvokableCall_1_t3880155831_il2cpp_TypeInfo_var;
extern Il2CppClass* CachedInvokableCall_1_t742075359_il2cpp_TypeInfo_var;
extern Il2CppClass* CachedInvokableCall_1_t3890435712_il2cpp_TypeInfo_var;
extern Il2CppClass* CachedInvokableCall_1_t65035577_il2cpp_TypeInfo_var;
extern Il2CppClass* InvokableCall_t1277370263_il2cpp_TypeInfo_var;
extern const MethodInfo* CachedInvokableCall_1__ctor_m3760156124_MethodInfo_var;
extern const MethodInfo* CachedInvokableCall_1__ctor_m1791898438_MethodInfo_var;
extern const MethodInfo* CachedInvokableCall_1__ctor_m3321389171_MethodInfo_var;
extern const MethodInfo* CachedInvokableCall_1__ctor_m562163564_MethodInfo_var;
extern const uint32_t PersistentCall_GetRuntimeCall_m3912860610_MetadataUsageId;
extern "C"  BaseInvokableCall_t1559630662 * PersistentCall_GetRuntimeCall_m3912860610 (PersistentCall_t2972625667 * __this, UnityEventBase_t1020378628 * ___theEvent0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PersistentCall_GetRuntimeCall_m3912860610_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->get_m_CallState_4();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		UnityEventBase_t1020378628 * L_1 = ___theEvent0;
		if (L_1)
		{
			goto IL_0013;
		}
	}

IL_0011:
	{
		return (BaseInvokableCall_t1559630662 *)NULL;
	}

IL_0013:
	{
		UnityEventBase_t1020378628 * L_2 = ___theEvent0;
		NullCheck(L_2);
		MethodInfo_t * L_3 = UnityEventBase_FindMethod_m1166071979(L_2, __this, /*hidden argument*/NULL);
		V_0 = L_3;
		MethodInfo_t * L_4 = V_0;
		if (L_4)
		{
			goto IL_0023;
		}
	}
	{
		return (BaseInvokableCall_t1559630662 *)NULL;
	}

IL_0023:
	{
		int32_t L_5 = __this->get_m_Mode_2();
		V_1 = L_5;
		int32_t L_6 = V_1;
		if (L_6 == 0)
		{
			goto IL_0051;
		}
		if (L_6 == 1)
		{
			goto IL_00d2;
		}
		if (L_6 == 2)
		{
			goto IL_005f;
		}
		if (L_6 == 3)
		{
			goto IL_008a;
		}
		if (L_6 == 4)
		{
			goto IL_0072;
		}
		if (L_6 == 5)
		{
			goto IL_00a2;
		}
		if (L_6 == 6)
		{
			goto IL_00ba;
		}
	}
	{
		goto IL_00df;
	}

IL_0051:
	{
		UnityEventBase_t1020378628 * L_7 = ___theEvent0;
		Object_t3071478659 * L_8 = PersistentCall_get_target_m3400889750(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_9 = V_0;
		NullCheck(L_7);
		BaseInvokableCall_t1559630662 * L_10 = VirtFuncInvoker2< BaseInvokableCall_t1559630662 *, Il2CppObject *, MethodInfo_t * >::Invoke(7 /* UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEventBase::GetDelegate(System.Object,System.Reflection.MethodInfo) */, L_7, L_8, L_9);
		return L_10;
	}

IL_005f:
	{
		Object_t3071478659 * L_11 = PersistentCall_get_target_m3400889750(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_12 = V_0;
		ArgumentCache_t1171347191 * L_13 = __this->get_m_Arguments_3();
		BaseInvokableCall_t1559630662 * L_14 = PersistentCall_GetObjectCall_m1043326447(NULL /*static, unused*/, L_11, L_12, L_13, /*hidden argument*/NULL);
		return L_14;
	}

IL_0072:
	{
		Object_t3071478659 * L_15 = PersistentCall_get_target_m3400889750(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_16 = V_0;
		ArgumentCache_t1171347191 * L_17 = __this->get_m_Arguments_3();
		NullCheck(L_17);
		float L_18 = ArgumentCache_get_floatArgument_m457274527(L_17, /*hidden argument*/NULL);
		CachedInvokableCall_1_t3880155831 * L_19 = (CachedInvokableCall_1_t3880155831 *)il2cpp_codegen_object_new(CachedInvokableCall_1_t3880155831_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m3760156124(L_19, L_15, L_16, L_18, /*hidden argument*/CachedInvokableCall_1__ctor_m3760156124_MethodInfo_var);
		return L_19;
	}

IL_008a:
	{
		Object_t3071478659 * L_20 = PersistentCall_get_target_m3400889750(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_21 = V_0;
		ArgumentCache_t1171347191 * L_22 = __this->get_m_Arguments_3();
		NullCheck(L_22);
		int32_t L_23 = ArgumentCache_get_intArgument_m688472510(L_22, /*hidden argument*/NULL);
		CachedInvokableCall_1_t742075359 * L_24 = (CachedInvokableCall_1_t742075359 *)il2cpp_codegen_object_new(CachedInvokableCall_1_t742075359_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m1791898438(L_24, L_20, L_21, L_23, /*hidden argument*/CachedInvokableCall_1__ctor_m1791898438_MethodInfo_var);
		return L_24;
	}

IL_00a2:
	{
		Object_t3071478659 * L_25 = PersistentCall_get_target_m3400889750(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_26 = V_0;
		ArgumentCache_t1171347191 * L_27 = __this->get_m_Arguments_3();
		NullCheck(L_27);
		String_t* L_28 = ArgumentCache_get_stringArgument_m621059457(L_27, /*hidden argument*/NULL);
		CachedInvokableCall_1_t3890435712 * L_29 = (CachedInvokableCall_1_t3890435712 *)il2cpp_codegen_object_new(CachedInvokableCall_1_t3890435712_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m3321389171(L_29, L_25, L_26, L_28, /*hidden argument*/CachedInvokableCall_1__ctor_m3321389171_MethodInfo_var);
		return L_29;
	}

IL_00ba:
	{
		Object_t3071478659 * L_30 = PersistentCall_get_target_m3400889750(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_31 = V_0;
		ArgumentCache_t1171347191 * L_32 = __this->get_m_Arguments_3();
		NullCheck(L_32);
		bool L_33 = ArgumentCache_get_boolArgument_m948935837(L_32, /*hidden argument*/NULL);
		CachedInvokableCall_1_t65035577 * L_34 = (CachedInvokableCall_1_t65035577 *)il2cpp_codegen_object_new(CachedInvokableCall_1_t65035577_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m562163564(L_34, L_30, L_31, L_33, /*hidden argument*/CachedInvokableCall_1__ctor_m562163564_MethodInfo_var);
		return L_34;
	}

IL_00d2:
	{
		Object_t3071478659 * L_35 = PersistentCall_get_target_m3400889750(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_36 = V_0;
		InvokableCall_t1277370263 * L_37 = (InvokableCall_t1277370263 *)il2cpp_codegen_object_new(InvokableCall_t1277370263_il2cpp_TypeInfo_var);
		InvokableCall__ctor_m1927467228(L_37, L_35, L_36, /*hidden argument*/NULL);
		return L_37;
	}

IL_00df:
	{
		return (BaseInvokableCall_t1559630662 *)NULL;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetObjectCall(UnityEngine.Object,System.Reflection.MethodInfo,UnityEngine.Events.ArgumentCache)
extern const Il2CppType* Object_t3071478659_0_0_0_var;
extern const Il2CppType* CachedInvokableCall_1_t1222316710_0_0_0_var;
extern const Il2CppType* MethodInfo_t_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* BaseInvokableCall_t1559630662_il2cpp_TypeInfo_var;
extern const uint32_t PersistentCall_GetObjectCall_m1043326447_MetadataUsageId;
extern "C"  BaseInvokableCall_t1559630662 * PersistentCall_GetObjectCall_m1043326447 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___target0, MethodInfo_t * ___method1, ArgumentCache_t1171347191 * ___arguments2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PersistentCall_GetObjectCall_m1043326447_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Type_t * V_2 = NULL;
	ConstructorInfo_t4136801618 * V_3 = NULL;
	Object_t3071478659 * V_4 = NULL;
	Type_t * G_B3_0 = NULL;
	Type_t * G_B2_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Object_t3071478659_0_0_0_var), /*hidden argument*/NULL);
		V_0 = L_0;
		ArgumentCache_t1171347191 * L_1 = ___arguments2;
		NullCheck(L_1);
		String_t* L_2 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m2706134059(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0039;
		}
	}
	{
		ArgumentCache_t1171347191 * L_4 = ___arguments2;
		NullCheck(L_4);
		String_t* L_5 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m2706134059(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = il2cpp_codegen_get_type((Il2CppMethodPointer)&Type_GetType_m3430407454, L_5, (bool)0, "UnityEngine, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null");
		Type_t * L_7 = L_6;
		G_B2_0 = L_7;
		if (L_7)
		{
			G_B3_0 = L_7;
			goto IL_0038;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Object_t3071478659_0_0_0_var), /*hidden argument*/NULL);
		G_B3_0 = L_8;
	}

IL_0038:
	{
		V_0 = G_B3_0;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(CachedInvokableCall_1_t1222316710_0_0_0_var), /*hidden argument*/NULL);
		V_1 = L_9;
		Type_t * L_10 = V_1;
		TypeU5BU5D_t3339007067* L_11 = ((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_12 = V_0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_12);
		NullCheck(L_10);
		Type_t * L_13 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3339007067* >::Invoke(86 /* System.Type System.Type::MakeGenericType(System.Type[]) */, L_10, L_11);
		V_2 = L_13;
		Type_t * L_14 = V_2;
		TypeU5BU5D_t3339007067* L_15 = ((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)3));
		Type_t * L_16 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Object_t3071478659_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		ArrayElementTypeCheck (L_15, L_16);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_16);
		TypeU5BU5D_t3339007067* L_17 = L_15;
		Type_t * L_18 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(MethodInfo_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 1);
		ArrayElementTypeCheck (L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_18);
		TypeU5BU5D_t3339007067* L_19 = L_17;
		Type_t * L_20 = V_0;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 2);
		ArrayElementTypeCheck (L_19, L_20);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_20);
		NullCheck(L_14);
		ConstructorInfo_t4136801618 * L_21 = Type_GetConstructor_m2586438681(L_14, L_19, /*hidden argument*/NULL);
		V_3 = L_21;
		ArgumentCache_t1171347191 * L_22 = ___arguments2;
		NullCheck(L_22);
		Object_t3071478659 * L_23 = ArgumentCache_get_unityObjectArgument_m3221396682(L_22, /*hidden argument*/NULL);
		V_4 = L_23;
		Object_t3071478659 * L_24 = V_4;
		bool L_25 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_24, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00aa;
		}
	}
	{
		Type_t * L_26 = V_0;
		Object_t3071478659 * L_27 = V_4;
		NullCheck(L_27);
		Type_t * L_28 = Object_GetType_m2022236990(L_27, /*hidden argument*/NULL);
		NullCheck(L_26);
		bool L_29 = VirtFuncInvoker1< bool, Type_t * >::Invoke(42 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_26, L_28);
		if (L_29)
		{
			goto IL_00aa;
		}
	}
	{
		V_4 = (Object_t3071478659 *)NULL;
	}

IL_00aa:
	{
		ConstructorInfo_t4136801618 * L_30 = V_3;
		ObjectU5BU5D_t1108656482* L_31 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)3));
		Object_t3071478659 * L_32 = ___target0;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 0);
		ArrayElementTypeCheck (L_31, L_32);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_32);
		ObjectU5BU5D_t1108656482* L_33 = L_31;
		MethodInfo_t * L_34 = ___method1;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, 1);
		ArrayElementTypeCheck (L_33, L_34);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_34);
		ObjectU5BU5D_t1108656482* L_35 = L_33;
		Object_t3071478659 * L_36 = V_4;
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, 2);
		ArrayElementTypeCheck (L_35, L_36);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_36);
		NullCheck(L_30);
		Il2CppObject * L_37 = ConstructorInfo_Invoke_m759007899(L_30, L_35, /*hidden argument*/NULL);
		return ((BaseInvokableCall_t1559630662 *)IsInstClass(L_37, BaseInvokableCall_t1559630662_il2cpp_TypeInfo_var));
	}
}
// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
extern Il2CppClass* List_1_t45843923_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m334889691_MethodInfo_var;
extern const uint32_t PersistentCallGroup__ctor_m3119615256_MetadataUsageId;
extern "C"  void PersistentCallGroup__ctor_m3119615256 (PersistentCallGroup_t4062675100 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PersistentCallGroup__ctor_m3119615256_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		List_1_t45843923 * L_0 = (List_1_t45843923 *)il2cpp_codegen_object_new(List_1_t45843923_il2cpp_TypeInfo_var);
		List_1__ctor_m334889691(L_0, /*hidden argument*/List_1__ctor_m334889691_MethodInfo_var);
		__this->set_m_Calls_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
extern Il2CppClass* Enumerator_t65516693_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1674667884_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2281273118_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2201791026_MethodInfo_var;
extern const uint32_t PersistentCallGroup_Initialize_m3484159567_MetadataUsageId;
extern "C"  void PersistentCallGroup_Initialize_m3484159567 (PersistentCallGroup_t4062675100 * __this, InvokableCallList_t3597236437 * ___invokableList0, UnityEventBase_t1020378628 * ___unityEventBase1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PersistentCallGroup_Initialize_m3484159567_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	PersistentCall_t2972625667 * V_0 = NULL;
	Enumerator_t65516693  V_1;
	memset(&V_1, 0, sizeof(V_1));
	BaseInvokableCall_t1559630662 * V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t45843923 * L_0 = __this->get_m_Calls_0();
		NullCheck(L_0);
		Enumerator_t65516693  L_1 = List_1_GetEnumerator_m1674667884(L_0, /*hidden argument*/List_1_GetEnumerator_m1674667884_MethodInfo_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003e;
		}

IL_0011:
		{
			PersistentCall_t2972625667 * L_2 = Enumerator_get_Current_m2281273118((&V_1), /*hidden argument*/Enumerator_get_Current_m2281273118_MethodInfo_var);
			V_0 = L_2;
			PersistentCall_t2972625667 * L_3 = V_0;
			NullCheck(L_3);
			bool L_4 = PersistentCall_IsValid_m1010437221(L_3, /*hidden argument*/NULL);
			if (L_4)
			{
				goto IL_0029;
			}
		}

IL_0024:
		{
			goto IL_003e;
		}

IL_0029:
		{
			PersistentCall_t2972625667 * L_5 = V_0;
			UnityEventBase_t1020378628 * L_6 = ___unityEventBase1;
			NullCheck(L_5);
			BaseInvokableCall_t1559630662 * L_7 = PersistentCall_GetRuntimeCall_m3912860610(L_5, L_6, /*hidden argument*/NULL);
			V_2 = L_7;
			BaseInvokableCall_t1559630662 * L_8 = V_2;
			if (!L_8)
			{
				goto IL_003e;
			}
		}

IL_0037:
		{
			InvokableCallList_t3597236437 * L_9 = ___invokableList0;
			BaseInvokableCall_t1559630662 * L_10 = V_2;
			NullCheck(L_9);
			InvokableCallList_AddPersistentInvokableCall_m4220625340(L_9, L_10, /*hidden argument*/NULL);
		}

IL_003e:
		{
			bool L_11 = Enumerator_MoveNext_m2201791026((&V_1), /*hidden argument*/Enumerator_MoveNext_m2201791026_MethodInfo_var);
			if (L_11)
			{
				goto IL_0011;
			}
		}

IL_004a:
		{
			IL2CPP_LEAVE(0x5B, FINALLY_004f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_004f;
	}

FINALLY_004f:
	{ // begin finally (depth: 1)
		Enumerator_t65516693  L_12 = V_1;
		Enumerator_t65516693  L_13 = L_12;
		Il2CppObject * L_14 = Box(Enumerator_t65516693_il2cpp_TypeInfo_var, &L_13);
		NullCheck((Il2CppObject *)L_14);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_14);
		IL2CPP_END_FINALLY(79)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(79)
	{
		IL2CPP_JUMP_TBL(0x5B, IL_005b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_005b:
	{
		return;
	}
}
// System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction__ctor_m4130179243 (UnityAction_t594794173 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction::Invoke()
extern "C"  void UnityAction_Invoke_m1979593925 (UnityAction_t594794173 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_Invoke_m1979593925((UnityAction_t594794173 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_UnityAction_t594794173 (UnityAction_t594794173 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.IAsyncResult UnityEngine.Events.UnityAction::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_BeginInvoke_m3541271198 (UnityAction_t594794173 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void UnityEngine.Events.UnityAction::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_EndInvoke_m794691643 (UnityAction_t594794173 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityEvent::.ctor()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent__ctor_m1715209183_MetadataUsageId;
extern "C"  void UnityEvent__ctor_m1715209183 (UnityEvent_t1266085011 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent__ctor_m1715209183_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)0)));
		UnityEventBase__ctor_m199506446(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent::AddListener(UnityEngine.Events.UnityAction)
extern "C"  void UnityEvent_AddListener_m4099140869 (UnityEvent_t1266085011 * __this, UnityAction_t594794173 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_t594794173 * L_0 = ___call0;
		BaseInvokableCall_t1559630662 * L_1 = UnityEvent_GetDelegate_m1012258596(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		UnityEventBase_AddCall_m1246572149(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent::FindMethod_Impl(System.String,System.Object)
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_FindMethod_Impl_m2897220818_MetadataUsageId;
extern "C"  MethodInfo_t * UnityEvent_FindMethod_Impl_m2897220818 (UnityEvent_t1266085011 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_FindMethod_Impl_m2897220818_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		MethodInfo_t * L_2 = UnityEventBase_GetValidMethodInfo_m2325301202(NULL /*static, unused*/, L_0, L_1, ((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* InvokableCall_t1277370263_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_GetDelegate_m2043983920_MetadataUsageId;
extern "C"  BaseInvokableCall_t1559630662 * UnityEvent_GetDelegate_m2043983920 (UnityEvent_t1266085011 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_GetDelegate_m2043983920_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_t1277370263 * L_2 = (InvokableCall_t1277370263 *)il2cpp_codegen_object_new(InvokableCall_t1277370263_il2cpp_TypeInfo_var);
		InvokableCall__ctor_m1927467228(L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(UnityEngine.Events.UnityAction)
extern Il2CppClass* InvokableCall_t1277370263_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_GetDelegate_m1012258596_MetadataUsageId;
extern "C"  BaseInvokableCall_t1559630662 * UnityEvent_GetDelegate_m1012258596 (Il2CppObject * __this /* static, unused */, UnityAction_t594794173 * ___action0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_GetDelegate_m1012258596_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAction_t594794173 * L_0 = ___action0;
		InvokableCall_t1277370263 * L_1 = (InvokableCall_t1277370263 *)il2cpp_codegen_object_new(InvokableCall_t1277370263_il2cpp_TypeInfo_var);
		InvokableCall__ctor_m3567423952(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Events.UnityEvent::Invoke()
extern "C"  void UnityEvent_Invoke_m2672830205 (UnityEvent_t1266085011 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = __this->get_m_InvokeArray_4();
		UnityEventBase_Invoke_m3681078084(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
