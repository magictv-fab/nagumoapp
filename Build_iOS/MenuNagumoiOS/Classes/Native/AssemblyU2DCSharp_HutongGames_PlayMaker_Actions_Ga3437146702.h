﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GameObjectCompareTag
struct  GameObjectCompareTag_t3437146702  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GameObjectCompareTag::gameObject
	FsmGameObject_t1697147867 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GameObjectCompareTag::tag
	FsmString_t952858651 * ___tag_10;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GameObjectCompareTag::trueEvent
	FsmEvent_t2133468028 * ___trueEvent_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GameObjectCompareTag::falseEvent
	FsmEvent_t2133468028 * ___falseEvent_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GameObjectCompareTag::storeResult
	FsmBool_t1075959796 * ___storeResult_13;
	// System.Boolean HutongGames.PlayMaker.Actions.GameObjectCompareTag::everyFrame
	bool ___everyFrame_14;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(GameObjectCompareTag_t3437146702, ___gameObject_9)); }
	inline FsmGameObject_t1697147867 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmGameObject_t1697147867 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmGameObject_t1697147867 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_tag_10() { return static_cast<int32_t>(offsetof(GameObjectCompareTag_t3437146702, ___tag_10)); }
	inline FsmString_t952858651 * get_tag_10() const { return ___tag_10; }
	inline FsmString_t952858651 ** get_address_of_tag_10() { return &___tag_10; }
	inline void set_tag_10(FsmString_t952858651 * value)
	{
		___tag_10 = value;
		Il2CppCodeGenWriteBarrier(&___tag_10, value);
	}

	inline static int32_t get_offset_of_trueEvent_11() { return static_cast<int32_t>(offsetof(GameObjectCompareTag_t3437146702, ___trueEvent_11)); }
	inline FsmEvent_t2133468028 * get_trueEvent_11() const { return ___trueEvent_11; }
	inline FsmEvent_t2133468028 ** get_address_of_trueEvent_11() { return &___trueEvent_11; }
	inline void set_trueEvent_11(FsmEvent_t2133468028 * value)
	{
		___trueEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___trueEvent_11, value);
	}

	inline static int32_t get_offset_of_falseEvent_12() { return static_cast<int32_t>(offsetof(GameObjectCompareTag_t3437146702, ___falseEvent_12)); }
	inline FsmEvent_t2133468028 * get_falseEvent_12() const { return ___falseEvent_12; }
	inline FsmEvent_t2133468028 ** get_address_of_falseEvent_12() { return &___falseEvent_12; }
	inline void set_falseEvent_12(FsmEvent_t2133468028 * value)
	{
		___falseEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___falseEvent_12, value);
	}

	inline static int32_t get_offset_of_storeResult_13() { return static_cast<int32_t>(offsetof(GameObjectCompareTag_t3437146702, ___storeResult_13)); }
	inline FsmBool_t1075959796 * get_storeResult_13() const { return ___storeResult_13; }
	inline FsmBool_t1075959796 ** get_address_of_storeResult_13() { return &___storeResult_13; }
	inline void set_storeResult_13(FsmBool_t1075959796 * value)
	{
		___storeResult_13 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_13, value);
	}

	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(GameObjectCompareTag_t3437146702, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
