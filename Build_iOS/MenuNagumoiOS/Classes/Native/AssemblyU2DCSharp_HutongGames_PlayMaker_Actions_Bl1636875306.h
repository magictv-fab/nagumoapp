﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co2052155886.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Blink
struct  Blink_t1636875306  : public ComponentAction_1_t2052155886
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.Blink::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Blink::timeOff
	FsmFloat_t2134102846 * ___timeOff_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Blink::timeOn
	FsmFloat_t2134102846 * ___timeOn_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.Blink::startOn
	FsmBool_t1075959796 * ___startOn_14;
	// System.Boolean HutongGames.PlayMaker.Actions.Blink::rendererOnly
	bool ___rendererOnly_15;
	// System.Boolean HutongGames.PlayMaker.Actions.Blink::realTime
	bool ___realTime_16;
	// System.Single HutongGames.PlayMaker.Actions.Blink::startTime
	float ___startTime_17;
	// System.Single HutongGames.PlayMaker.Actions.Blink::timer
	float ___timer_18;
	// System.Boolean HutongGames.PlayMaker.Actions.Blink::blinkOn
	bool ___blinkOn_19;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(Blink_t1636875306, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_timeOff_12() { return static_cast<int32_t>(offsetof(Blink_t1636875306, ___timeOff_12)); }
	inline FsmFloat_t2134102846 * get_timeOff_12() const { return ___timeOff_12; }
	inline FsmFloat_t2134102846 ** get_address_of_timeOff_12() { return &___timeOff_12; }
	inline void set_timeOff_12(FsmFloat_t2134102846 * value)
	{
		___timeOff_12 = value;
		Il2CppCodeGenWriteBarrier(&___timeOff_12, value);
	}

	inline static int32_t get_offset_of_timeOn_13() { return static_cast<int32_t>(offsetof(Blink_t1636875306, ___timeOn_13)); }
	inline FsmFloat_t2134102846 * get_timeOn_13() const { return ___timeOn_13; }
	inline FsmFloat_t2134102846 ** get_address_of_timeOn_13() { return &___timeOn_13; }
	inline void set_timeOn_13(FsmFloat_t2134102846 * value)
	{
		___timeOn_13 = value;
		Il2CppCodeGenWriteBarrier(&___timeOn_13, value);
	}

	inline static int32_t get_offset_of_startOn_14() { return static_cast<int32_t>(offsetof(Blink_t1636875306, ___startOn_14)); }
	inline FsmBool_t1075959796 * get_startOn_14() const { return ___startOn_14; }
	inline FsmBool_t1075959796 ** get_address_of_startOn_14() { return &___startOn_14; }
	inline void set_startOn_14(FsmBool_t1075959796 * value)
	{
		___startOn_14 = value;
		Il2CppCodeGenWriteBarrier(&___startOn_14, value);
	}

	inline static int32_t get_offset_of_rendererOnly_15() { return static_cast<int32_t>(offsetof(Blink_t1636875306, ___rendererOnly_15)); }
	inline bool get_rendererOnly_15() const { return ___rendererOnly_15; }
	inline bool* get_address_of_rendererOnly_15() { return &___rendererOnly_15; }
	inline void set_rendererOnly_15(bool value)
	{
		___rendererOnly_15 = value;
	}

	inline static int32_t get_offset_of_realTime_16() { return static_cast<int32_t>(offsetof(Blink_t1636875306, ___realTime_16)); }
	inline bool get_realTime_16() const { return ___realTime_16; }
	inline bool* get_address_of_realTime_16() { return &___realTime_16; }
	inline void set_realTime_16(bool value)
	{
		___realTime_16 = value;
	}

	inline static int32_t get_offset_of_startTime_17() { return static_cast<int32_t>(offsetof(Blink_t1636875306, ___startTime_17)); }
	inline float get_startTime_17() const { return ___startTime_17; }
	inline float* get_address_of_startTime_17() { return &___startTime_17; }
	inline void set_startTime_17(float value)
	{
		___startTime_17 = value;
	}

	inline static int32_t get_offset_of_timer_18() { return static_cast<int32_t>(offsetof(Blink_t1636875306, ___timer_18)); }
	inline float get_timer_18() const { return ___timer_18; }
	inline float* get_address_of_timer_18() { return &___timer_18; }
	inline void set_timer_18(float value)
	{
		___timer_18 = value;
	}

	inline static int32_t get_offset_of_blinkOn_19() { return static_cast<int32_t>(offsetof(Blink_t1636875306, ___blinkOn_19)); }
	inline bool get_blinkOn_19() const { return ___blinkOn_19; }
	inline bool* get_address_of_blinkOn_19() { return &___blinkOn_19; }
	inline void set_blinkOn_19(bool value)
	{
		___blinkOn_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
