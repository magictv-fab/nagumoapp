﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.ScrollRect
struct ScrollRect_t3606982749;
// TabloidesData
struct TabloidesData_t2107312651;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Collections.Generic.List`1<UnityEngine.Sprite>
struct List_1_t272385497;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TabloideManager
struct  TabloideManager_t3674153819  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.ScrollRect TabloideManager::scrollRect
	ScrollRect_t3606982749 * ___scrollRect_2;
	// UnityEngine.GameObject TabloideManager::loadingObj
	GameObject_t3674682005 * ___loadingObj_7;
	// UnityEngine.GameObject TabloideManager::popup
	GameObject_t3674682005 * ___popup_8;
	// UnityEngine.GameObject TabloideManager::itemPrefab
	GameObject_t3674682005 * ___itemPrefab_9;
	// UnityEngine.GameObject TabloideManager::containerItens
	GameObject_t3674682005 * ___containerItens_10;
	// UnityEngine.GameObject TabloideManager::currentItem
	GameObject_t3674682005 * ___currentItem_11;
	// System.Int32 TabloideManager::loadUtil
	int32_t ___loadUtil_12;
	// System.Int32 TabloideManager::precoCupom
	int32_t ___precoCupom_13;
	// System.Int32 TabloideManager::objCounter
	int32_t ___objCounter_14;
	// System.Int32 TabloideManager::imgLoadedCount
	int32_t ___imgLoadedCount_16;

public:
	inline static int32_t get_offset_of_scrollRect_2() { return static_cast<int32_t>(offsetof(TabloideManager_t3674153819, ___scrollRect_2)); }
	inline ScrollRect_t3606982749 * get_scrollRect_2() const { return ___scrollRect_2; }
	inline ScrollRect_t3606982749 ** get_address_of_scrollRect_2() { return &___scrollRect_2; }
	inline void set_scrollRect_2(ScrollRect_t3606982749 * value)
	{
		___scrollRect_2 = value;
		Il2CppCodeGenWriteBarrier(&___scrollRect_2, value);
	}

	inline static int32_t get_offset_of_loadingObj_7() { return static_cast<int32_t>(offsetof(TabloideManager_t3674153819, ___loadingObj_7)); }
	inline GameObject_t3674682005 * get_loadingObj_7() const { return ___loadingObj_7; }
	inline GameObject_t3674682005 ** get_address_of_loadingObj_7() { return &___loadingObj_7; }
	inline void set_loadingObj_7(GameObject_t3674682005 * value)
	{
		___loadingObj_7 = value;
		Il2CppCodeGenWriteBarrier(&___loadingObj_7, value);
	}

	inline static int32_t get_offset_of_popup_8() { return static_cast<int32_t>(offsetof(TabloideManager_t3674153819, ___popup_8)); }
	inline GameObject_t3674682005 * get_popup_8() const { return ___popup_8; }
	inline GameObject_t3674682005 ** get_address_of_popup_8() { return &___popup_8; }
	inline void set_popup_8(GameObject_t3674682005 * value)
	{
		___popup_8 = value;
		Il2CppCodeGenWriteBarrier(&___popup_8, value);
	}

	inline static int32_t get_offset_of_itemPrefab_9() { return static_cast<int32_t>(offsetof(TabloideManager_t3674153819, ___itemPrefab_9)); }
	inline GameObject_t3674682005 * get_itemPrefab_9() const { return ___itemPrefab_9; }
	inline GameObject_t3674682005 ** get_address_of_itemPrefab_9() { return &___itemPrefab_9; }
	inline void set_itemPrefab_9(GameObject_t3674682005 * value)
	{
		___itemPrefab_9 = value;
		Il2CppCodeGenWriteBarrier(&___itemPrefab_9, value);
	}

	inline static int32_t get_offset_of_containerItens_10() { return static_cast<int32_t>(offsetof(TabloideManager_t3674153819, ___containerItens_10)); }
	inline GameObject_t3674682005 * get_containerItens_10() const { return ___containerItens_10; }
	inline GameObject_t3674682005 ** get_address_of_containerItens_10() { return &___containerItens_10; }
	inline void set_containerItens_10(GameObject_t3674682005 * value)
	{
		___containerItens_10 = value;
		Il2CppCodeGenWriteBarrier(&___containerItens_10, value);
	}

	inline static int32_t get_offset_of_currentItem_11() { return static_cast<int32_t>(offsetof(TabloideManager_t3674153819, ___currentItem_11)); }
	inline GameObject_t3674682005 * get_currentItem_11() const { return ___currentItem_11; }
	inline GameObject_t3674682005 ** get_address_of_currentItem_11() { return &___currentItem_11; }
	inline void set_currentItem_11(GameObject_t3674682005 * value)
	{
		___currentItem_11 = value;
		Il2CppCodeGenWriteBarrier(&___currentItem_11, value);
	}

	inline static int32_t get_offset_of_loadUtil_12() { return static_cast<int32_t>(offsetof(TabloideManager_t3674153819, ___loadUtil_12)); }
	inline int32_t get_loadUtil_12() const { return ___loadUtil_12; }
	inline int32_t* get_address_of_loadUtil_12() { return &___loadUtil_12; }
	inline void set_loadUtil_12(int32_t value)
	{
		___loadUtil_12 = value;
	}

	inline static int32_t get_offset_of_precoCupom_13() { return static_cast<int32_t>(offsetof(TabloideManager_t3674153819, ___precoCupom_13)); }
	inline int32_t get_precoCupom_13() const { return ___precoCupom_13; }
	inline int32_t* get_address_of_precoCupom_13() { return &___precoCupom_13; }
	inline void set_precoCupom_13(int32_t value)
	{
		___precoCupom_13 = value;
	}

	inline static int32_t get_offset_of_objCounter_14() { return static_cast<int32_t>(offsetof(TabloideManager_t3674153819, ___objCounter_14)); }
	inline int32_t get_objCounter_14() const { return ___objCounter_14; }
	inline int32_t* get_address_of_objCounter_14() { return &___objCounter_14; }
	inline void set_objCounter_14(int32_t value)
	{
		___objCounter_14 = value;
	}

	inline static int32_t get_offset_of_imgLoadedCount_16() { return static_cast<int32_t>(offsetof(TabloideManager_t3674153819, ___imgLoadedCount_16)); }
	inline int32_t get_imgLoadedCount_16() const { return ___imgLoadedCount_16; }
	inline int32_t* get_address_of_imgLoadedCount_16() { return &___imgLoadedCount_16; }
	inline void set_imgLoadedCount_16(int32_t value)
	{
		___imgLoadedCount_16 = value;
	}
};

struct TabloideManager_t3674153819_StaticFields
{
public:
	// TabloidesData TabloideManager::tabloidesData
	TabloidesData_t2107312651 * ___tabloidesData_3;
	// System.Collections.Generic.List`1<System.String> TabloideManager::textoLst
	List_1_t1375417109 * ___textoLst_4;
	// System.Collections.Generic.List`1<System.String> TabloideManager::linkLst
	List_1_t1375417109 * ___linkLst_5;
	// System.Collections.Generic.List`1<UnityEngine.Sprite> TabloideManager::imgLst
	List_1_t272385497 * ___imgLst_6;
	// System.String TabloideManager::currentJsonTxt
	String_t* ___currentJsonTxt_15;

public:
	inline static int32_t get_offset_of_tabloidesData_3() { return static_cast<int32_t>(offsetof(TabloideManager_t3674153819_StaticFields, ___tabloidesData_3)); }
	inline TabloidesData_t2107312651 * get_tabloidesData_3() const { return ___tabloidesData_3; }
	inline TabloidesData_t2107312651 ** get_address_of_tabloidesData_3() { return &___tabloidesData_3; }
	inline void set_tabloidesData_3(TabloidesData_t2107312651 * value)
	{
		___tabloidesData_3 = value;
		Il2CppCodeGenWriteBarrier(&___tabloidesData_3, value);
	}

	inline static int32_t get_offset_of_textoLst_4() { return static_cast<int32_t>(offsetof(TabloideManager_t3674153819_StaticFields, ___textoLst_4)); }
	inline List_1_t1375417109 * get_textoLst_4() const { return ___textoLst_4; }
	inline List_1_t1375417109 ** get_address_of_textoLst_4() { return &___textoLst_4; }
	inline void set_textoLst_4(List_1_t1375417109 * value)
	{
		___textoLst_4 = value;
		Il2CppCodeGenWriteBarrier(&___textoLst_4, value);
	}

	inline static int32_t get_offset_of_linkLst_5() { return static_cast<int32_t>(offsetof(TabloideManager_t3674153819_StaticFields, ___linkLst_5)); }
	inline List_1_t1375417109 * get_linkLst_5() const { return ___linkLst_5; }
	inline List_1_t1375417109 ** get_address_of_linkLst_5() { return &___linkLst_5; }
	inline void set_linkLst_5(List_1_t1375417109 * value)
	{
		___linkLst_5 = value;
		Il2CppCodeGenWriteBarrier(&___linkLst_5, value);
	}

	inline static int32_t get_offset_of_imgLst_6() { return static_cast<int32_t>(offsetof(TabloideManager_t3674153819_StaticFields, ___imgLst_6)); }
	inline List_1_t272385497 * get_imgLst_6() const { return ___imgLst_6; }
	inline List_1_t272385497 ** get_address_of_imgLst_6() { return &___imgLst_6; }
	inline void set_imgLst_6(List_1_t272385497 * value)
	{
		___imgLst_6 = value;
		Il2CppCodeGenWriteBarrier(&___imgLst_6, value);
	}

	inline static int32_t get_offset_of_currentJsonTxt_15() { return static_cast<int32_t>(offsetof(TabloideManager_t3674153819_StaticFields, ___currentJsonTxt_15)); }
	inline String_t* get_currentJsonTxt_15() const { return ___currentJsonTxt_15; }
	inline String_t** get_address_of_currentJsonTxt_15() { return &___currentJsonTxt_15; }
	inline void set_currentJsonTxt_15(String_t* value)
	{
		___currentJsonTxt_15 = value;
		Il2CppCodeGenWriteBarrier(&___currentJsonTxt_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
