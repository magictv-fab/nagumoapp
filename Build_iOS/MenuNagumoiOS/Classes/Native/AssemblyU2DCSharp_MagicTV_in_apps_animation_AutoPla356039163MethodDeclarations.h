﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.in_apps.animation.AutoPlayController
struct AutoPlayController_t356039163;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.in_apps.animation.AutoPlayController::.ctor()
extern "C"  void AutoPlayController__ctor_m298105457 (AutoPlayController_t356039163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.animation.AutoPlayController::Start()
extern "C"  void AutoPlayController_Start_m3540210545 (AutoPlayController_t356039163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.animation.AutoPlayController::HandleOnChangeVisible(System.Boolean)
extern "C"  void AutoPlayController_HandleOnChangeVisible_m2583774337 (AutoPlayController_t356039163 * __this, bool ___m0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
