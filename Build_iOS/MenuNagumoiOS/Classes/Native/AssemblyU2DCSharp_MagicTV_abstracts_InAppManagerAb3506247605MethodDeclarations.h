﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.abstracts.InAppManagerAbstract
struct InAppManagerAbstract_t3506247605;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.abstracts.InAppManagerAbstract::.ctor()
extern "C"  void InAppManagerAbstract__ctor_m1421195916 (InAppManagerAbstract_t3506247605 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
