﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,ARM.events.VoidEventDispatcher>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3345085353(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2479660279 *, String_t*, VoidEventDispatcher_t1760461203 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,ARM.events.VoidEventDispatcher>::get_Key()
#define KeyValuePair_2_get_Key_m3570443807(__this, method) ((  String_t* (*) (KeyValuePair_2_t2479660279 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,ARM.events.VoidEventDispatcher>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1051459808(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2479660279 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,ARM.events.VoidEventDispatcher>::get_Value()
#define KeyValuePair_2_get_Value_m3919967519(__this, method) ((  VoidEventDispatcher_t1760461203 * (*) (KeyValuePair_2_t2479660279 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,ARM.events.VoidEventDispatcher>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m157017312(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2479660279 *, VoidEventDispatcher_t1760461203 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,ARM.events.VoidEventDispatcher>::ToString()
#define KeyValuePair_2_ToString_m849911362(__this, method) ((  String_t* (*) (KeyValuePair_2_t2479660279 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
