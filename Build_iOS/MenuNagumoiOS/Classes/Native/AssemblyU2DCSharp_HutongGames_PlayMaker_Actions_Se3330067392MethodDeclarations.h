﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetTag
struct SetTag_t3330067392;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetTag::.ctor()
extern "C"  void SetTag__ctor_m565644454 (SetTag_t3330067392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetTag::Reset()
extern "C"  void SetTag_Reset_m2507044691 (SetTag_t3330067392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetTag::OnEnter()
extern "C"  void SetTag_OnEnter_m1810169597 (SetTag_t3330067392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
