﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUIElementHitTest
struct GUIElementHitTest_t4187190930;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUIElementHitTest::.ctor()
extern "C"  void GUIElementHitTest__ctor_m884959972 (GUIElementHitTest_t4187190930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUIElementHitTest::Reset()
extern "C"  void GUIElementHitTest_Reset_m2826360209 (GUIElementHitTest_t4187190930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUIElementHitTest::OnEnter()
extern "C"  void GUIElementHitTest_OnEnter_m3729704379 (GUIElementHitTest_t4187190930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUIElementHitTest::OnUpdate()
extern "C"  void GUIElementHitTest_OnUpdate_m3085245416 (GUIElementHitTest_t4187190930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUIElementHitTest::DoHitTest()
extern "C"  void GUIElementHitTest_DoHitTest_m2885358780 (GUIElementHitTest_t4187190930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
