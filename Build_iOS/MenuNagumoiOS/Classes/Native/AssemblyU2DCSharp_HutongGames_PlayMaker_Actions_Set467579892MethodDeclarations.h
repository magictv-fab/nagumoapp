﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetTextureOffset
struct SetTextureOffset_t467579892;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetTextureOffset::.ctor()
extern "C"  void SetTextureOffset__ctor_m2784190706 (SetTextureOffset_t467579892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetTextureOffset::Reset()
extern "C"  void SetTextureOffset_Reset_m430623647 (SetTextureOffset_t467579892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetTextureOffset::OnEnter()
extern "C"  void SetTextureOffset_OnEnter_m3529338953 (SetTextureOffset_t467579892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetTextureOffset::OnUpdate()
extern "C"  void SetTextureOffset_OnUpdate_m1168884506 (SetTextureOffset_t467579892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetTextureOffset::DoSetTextureOffset()
extern "C"  void SetTextureOffset_DoSetTextureOffset_m879326601 (SetTextureOffset_t467579892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
