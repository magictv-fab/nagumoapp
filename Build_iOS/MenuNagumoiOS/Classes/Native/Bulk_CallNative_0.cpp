﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.String
struct String_t;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "CallNative_U3CModuleU3E86524790.h"
#include "CallNative_U3CModuleU3E86524790MethodDeclarations.h"
#include "CallNative_CallNative3421361205.h"
#include "CallNative_CallNative3421361205MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Int321153838500.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_Void2863195528.h"
#include "UnityEngine_UnityEngine_Application2856536070MethodDeclarations.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform3050318497.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C" int32_t DEFAULT_CALL _SaveImage(char*, int32_t, char*);
// System.Int32 CallNative::_SaveImage(System.String,System.Boolean,System.String)
extern "C"  int32_t CallNative__SaveImage_m175990345 (Il2CppObject * __this /* static, unused */, String_t* ___str0, bool ___move1, String_t* ___albumName2, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*, int32_t, char*);

	// Marshaling of parameter '___str0' to native representation
	char* ____str0_marshaled = NULL;
	____str0_marshaled = il2cpp_codegen_marshal_string(___str0);

	// Marshaling of parameter '___albumName2' to native representation
	char* ____albumName2_marshaled = NULL;
	____albumName2_marshaled = il2cpp_codegen_marshal_string(___albumName2);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(_SaveImage)(____str0_marshaled, ___move1, ____albumName2_marshaled);

	// Marshaling cleanup of parameter '___str0' native representation
	il2cpp_codegen_marshal_free(____str0_marshaled);
	____str0_marshaled = NULL;

	// Marshaling cleanup of parameter '___albumName2' native representation
	il2cpp_codegen_marshal_free(____albumName2_marshaled);
	____albumName2_marshaled = NULL;

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL _SaveVideo(char*, int32_t, char*);
// System.Int32 CallNative::_SaveVideo(System.String,System.Boolean,System.String)
extern "C"  int32_t CallNative__SaveVideo_m353804649 (Il2CppObject * __this /* static, unused */, String_t* ___str0, bool ___move1, String_t* ___albumName2, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*, int32_t, char*);

	// Marshaling of parameter '___str0' to native representation
	char* ____str0_marshaled = NULL;
	____str0_marshaled = il2cpp_codegen_marshal_string(___str0);

	// Marshaling of parameter '___albumName2' to native representation
	char* ____albumName2_marshaled = NULL;
	____albumName2_marshaled = il2cpp_codegen_marshal_string(___albumName2);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(_SaveVideo)(____str0_marshaled, ___move1, ____albumName2_marshaled);

	// Marshaling cleanup of parameter '___str0' native representation
	il2cpp_codegen_marshal_free(____str0_marshaled);
	____str0_marshaled = NULL;

	// Marshaling cleanup of parameter '___albumName2' native representation
	il2cpp_codegen_marshal_free(____albumName2_marshaled);
	____albumName2_marshaled = NULL;

	return returnValue;
}
// System.Void CallNative::CopyImageToCameraRoll(System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t CallNative_CopyImageToCameraRoll_m2433959120_MetadataUsageId;
extern "C"  void CallNative_CopyImageToCameraRoll_m2433959120 (Il2CppObject * __this /* static, unused */, String_t* ___path0, String_t* ___albumName1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CallNative_CopyImageToCameraRoll_m2433959120_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_002b;
		}
	}
	{
		String_t* L_1 = ___albumName1;
		NullCheck(L_1);
		String_t* L_2 = String_Trim_m1030489823(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0022;
		}
	}
	{
		String_t* L_4 = Application_get_productName_m4188595931(NULL /*static, unused*/, /*hidden argument*/NULL);
		___albumName1 = L_4;
	}

IL_0022:
	{
		String_t* L_5 = ___path0;
		String_t* L_6 = ___albumName1;
		CallNative__SaveImage_m175990345(NULL /*static, unused*/, L_5, (bool)0, L_6, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Void CallNative::MoveImageToCameraRoll(System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t CallNative_MoveImageToCameraRoll_m786993196_MetadataUsageId;
extern "C"  void CallNative_MoveImageToCameraRoll_m786993196 (Il2CppObject * __this /* static, unused */, String_t* ___path0, String_t* ___albumName1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CallNative_MoveImageToCameraRoll_m786993196_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_002b;
		}
	}
	{
		String_t* L_1 = ___albumName1;
		NullCheck(L_1);
		String_t* L_2 = String_Trim_m1030489823(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0022;
		}
	}
	{
		String_t* L_4 = Application_get_productName_m4188595931(NULL /*static, unused*/, /*hidden argument*/NULL);
		___albumName1 = L_4;
	}

IL_0022:
	{
		String_t* L_5 = ___path0;
		String_t* L_6 = ___albumName1;
		CallNative__SaveImage_m175990345(NULL /*static, unused*/, L_5, (bool)1, L_6, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Void CallNative::CopyVideoToCameraRoll(System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t CallNative_CopyVideoToCameraRoll_m1507384752_MetadataUsageId;
extern "C"  void CallNative_CopyVideoToCameraRoll_m1507384752 (Il2CppObject * __this /* static, unused */, String_t* ___path0, String_t* ___albumName1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CallNative_CopyVideoToCameraRoll_m1507384752_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_002b;
		}
	}
	{
		String_t* L_1 = ___albumName1;
		NullCheck(L_1);
		String_t* L_2 = String_Trim_m1030489823(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0022;
		}
	}
	{
		String_t* L_4 = Application_get_productName_m4188595931(NULL /*static, unused*/, /*hidden argument*/NULL);
		___albumName1 = L_4;
	}

IL_0022:
	{
		String_t* L_5 = ___path0;
		String_t* L_6 = ___albumName1;
		CallNative__SaveVideo_m353804649(NULL /*static, unused*/, L_5, (bool)0, L_6, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Void CallNative::MoveVideoToCameraRoll(System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t CallNative_MoveVideoToCameraRoll_m4155386124_MetadataUsageId;
extern "C"  void CallNative_MoveVideoToCameraRoll_m4155386124 (Il2CppObject * __this /* static, unused */, String_t* ___path0, String_t* ___albumName1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CallNative_MoveVideoToCameraRoll_m4155386124_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_002b;
		}
	}
	{
		String_t* L_1 = ___albumName1;
		NullCheck(L_1);
		String_t* L_2 = String_Trim_m1030489823(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0022;
		}
	}
	{
		String_t* L_4 = Application_get_productName_m4188595931(NULL /*static, unused*/, /*hidden argument*/NULL);
		___albumName1 = L_4;
	}

IL_0022:
	{
		String_t* L_5 = ___path0;
		String_t* L_6 = ___albumName1;
		CallNative__SaveVideo_m353804649(NULL /*static, unused*/, L_5, (bool)1, L_6, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
