﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1192007474.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22409664798.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<MagicTV.globals.Perspective,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3867566061_gshared (InternalEnumerator_1_t1192007474 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3867566061(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1192007474 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3867566061_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<MagicTV.globals.Perspective,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m996732051_gshared (InternalEnumerator_1_t1192007474 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m996732051(__this, method) ((  void (*) (InternalEnumerator_1_t1192007474 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m996732051_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<MagicTV.globals.Perspective,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2935990527_gshared (InternalEnumerator_1_t1192007474 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2935990527(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1192007474 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2935990527_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<MagicTV.globals.Perspective,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3851517444_gshared (InternalEnumerator_1_t1192007474 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3851517444(__this, method) ((  void (*) (InternalEnumerator_1_t1192007474 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3851517444_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<MagicTV.globals.Perspective,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1077126655_gshared (InternalEnumerator_1_t1192007474 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1077126655(__this, method) ((  bool (*) (InternalEnumerator_1_t1192007474 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1077126655_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<MagicTV.globals.Perspective,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t2409664798  InternalEnumerator_1_get_Current_m1232573108_gshared (InternalEnumerator_1_t1192007474 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1232573108(__this, method) ((  KeyValuePair_2_t2409664798  (*) (InternalEnumerator_1_t1192007474 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1232573108_gshared)(__this, method)
