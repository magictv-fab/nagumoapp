﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler
struct OnChangeAnToPerspectiveEventHandler_t2702236419;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnChangeAnToPerspectiveEventHandler__ctor_m1476048730 (OnChangeAnToPerspectiveEventHandler_t2702236419 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler::Invoke()
extern "C"  void OnChangeAnToPerspectiveEventHandler_Invoke_m3203719220 (OnChangeAnToPerspectiveEventHandler_t2702236419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnChangeAnToPerspectiveEventHandler_BeginInvoke_m614292695 (OnChangeAnToPerspectiveEventHandler_t2702236419 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnChangeAnToPerspectiveEventHandler_EndInvoke_m146007658 (OnChangeAnToPerspectiveEventHandler_t2702236419 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
