﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.BarcodeFormat,System.Object>
struct ValueCollection_t4285270722;
// System.Collections.Generic.Dictionary`2<ZXing.BarcodeFormat,System.Object>
struct Dictionary_2_t1289697713;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3516498417.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.BarcodeFormat,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m1099873897_gshared (ValueCollection_t4285270722 * __this, Dictionary_2_t1289697713 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m1099873897(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t4285270722 *, Dictionary_2_t1289697713 *, const MethodInfo*))ValueCollection__ctor_m1099873897_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.BarcodeFormat,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m182029481_gshared (ValueCollection_t4285270722 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m182029481(__this, ___item0, method) ((  void (*) (ValueCollection_t4285270722 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m182029481_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.BarcodeFormat,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m844772850_gshared (ValueCollection_t4285270722 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m844772850(__this, method) ((  void (*) (ValueCollection_t4285270722 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m844772850_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.BarcodeFormat,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2854902429_gshared (ValueCollection_t4285270722 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2854902429(__this, ___item0, method) ((  bool (*) (ValueCollection_t4285270722 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2854902429_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.BarcodeFormat,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3726866050_gshared (ValueCollection_t4285270722 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3726866050(__this, ___item0, method) ((  bool (*) (ValueCollection_t4285270722 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3726866050_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.BarcodeFormat,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2634734528_gshared (ValueCollection_t4285270722 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2634734528(__this, method) ((  Il2CppObject* (*) (ValueCollection_t4285270722 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2634734528_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.BarcodeFormat,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m4014182518_gshared (ValueCollection_t4285270722 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m4014182518(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t4285270722 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m4014182518_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.BarcodeFormat,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1222187889_gshared (ValueCollection_t4285270722 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1222187889(__this, method) ((  Il2CppObject * (*) (ValueCollection_t4285270722 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1222187889_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.BarcodeFormat,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1130078288_gshared (ValueCollection_t4285270722 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1130078288(__this, method) ((  bool (*) (ValueCollection_t4285270722 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1130078288_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.BarcodeFormat,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m580116016_gshared (ValueCollection_t4285270722 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m580116016(__this, method) ((  bool (*) (ValueCollection_t4285270722 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m580116016_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.BarcodeFormat,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m1351921628_gshared (ValueCollection_t4285270722 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1351921628(__this, method) ((  Il2CppObject * (*) (ValueCollection_t4285270722 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1351921628_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.BarcodeFormat,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m1572499568_gshared (ValueCollection_t4285270722 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m1572499568(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t4285270722 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1572499568_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.BarcodeFormat,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3516498417  ValueCollection_GetEnumerator_m1412598675_gshared (ValueCollection_t4285270722 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m1412598675(__this, method) ((  Enumerator_t3516498417  (*) (ValueCollection_t4285270722 *, const MethodInfo*))ValueCollection_GetEnumerator_m1412598675_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<ZXing.BarcodeFormat,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m1179143670_gshared (ValueCollection_t4285270722 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m1179143670(__this, method) ((  int32_t (*) (ValueCollection_t4285270722 *, const MethodInfo*))ValueCollection_get_Count_m1179143670_gshared)(__this, method)
