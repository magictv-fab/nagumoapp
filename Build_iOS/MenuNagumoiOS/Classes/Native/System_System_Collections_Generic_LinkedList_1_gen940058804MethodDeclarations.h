﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedList_1_gen2045451540MethodDeclarations.h"

// System.Void System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.State>::.ctor()
#define LinkedList_1__ctor_m3214676881(__this, method) ((  void (*) (LinkedList_1_t940058804 *, const MethodInfo*))LinkedList_1__ctor_m2955457271_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.State>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define LinkedList_1__ctor_m2620090833(__this, ___info0, ___context1, method) ((  void (*) (LinkedList_1_t940058804 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))LinkedList_1__ctor_m3369579448_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.State>::System.Collections.Generic.ICollection<T>.Add(T)
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3645605871(__this, ___value0, method) ((  void (*) (LinkedList_1_t940058804 *, State_t3065423635 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3576108392_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.State>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define LinkedList_1_System_Collections_ICollection_CopyTo_m4119756468(__this, ___array0, ___index1, method) ((  void (*) (LinkedList_1_t940058804 *, Il2CppArray *, int32_t, const MethodInfo*))LinkedList_1_System_Collections_ICollection_CopyTo_m2331638317_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.State>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2749271194(__this, method) ((  Il2CppObject* (*) (LinkedList_1_t940058804 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2027502985_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.State>::System.Collections.IEnumerable.GetEnumerator()
#define LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m3717860783(__this, method) ((  Il2CppObject * (*) (LinkedList_1_t940058804 *, const MethodInfo*))LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m51916412_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.State>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2828378771(__this, method) ((  bool (*) (LinkedList_1_t940058804 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2230847288_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.State>::System.Collections.ICollection.get_IsSynchronized()
#define LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m3958998282(__this, method) ((  bool (*) (LinkedList_1_t940058804 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m683991045_gshared)(__this, method)
// System.Object System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.State>::System.Collections.ICollection.get_SyncRoot()
#define LinkedList_1_System_Collections_ICollection_get_SyncRoot_m2270702312(__this, method) ((  Il2CppObject * (*) (LinkedList_1_t940058804 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_SyncRoot_m573420165_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.State>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedList_1_VerifyReferencedNode_m922033051(__this, ___node0, method) ((  void (*) (LinkedList_1_t940058804 *, LinkedListNode_1_t2682158342 *, const MethodInfo*))LinkedList_1_VerifyReferencedNode_m3939775124_gshared)(__this, ___node0, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.State>::AddFirst(T)
#define LinkedList_1_AddFirst_m4221429197(__this, ___value0, method) ((  LinkedListNode_1_t2682158342 * (*) (LinkedList_1_t940058804 *, State_t3065423635 *, const MethodInfo*))LinkedList_1_AddFirst_m2530092880_gshared)(__this, ___value0, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.State>::AddLast(T)
#define LinkedList_1_AddLast_m3725761022(__this, ___value0, method) ((  LinkedListNode_1_t2682158342 * (*) (LinkedList_1_t940058804 *, State_t3065423635 *, const MethodInfo*))LinkedList_1_AddLast_m4070107716_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.State>::Clear()
#define LinkedList_1_Clear_m3050593467(__this, method) ((  void (*) (LinkedList_1_t940058804 *, const MethodInfo*))LinkedList_1_Clear_m361590562_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.State>::Contains(T)
#define LinkedList_1_Contains_m2653532353(__this, ___value0, method) ((  bool (*) (LinkedList_1_t940058804 *, State_t3065423635 *, const MethodInfo*))LinkedList_1_Contains_m3484410556_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.State>::CopyTo(T[],System.Int32)
#define LinkedList_1_CopyTo_m2362898463(__this, ___array0, ___index1, method) ((  void (*) (LinkedList_1_t940058804 *, StateU5BU5D_t545846082*, int32_t, const MethodInfo*))LinkedList_1_CopyTo_m3470139544_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.State>::Find(T)
#define LinkedList_1_Find_m2991670755(__this, ___value0, method) ((  LinkedListNode_1_t2682158342 * (*) (LinkedList_1_t940058804 *, State_t3065423635 *, const MethodInfo*))LinkedList_1_Find_m2643247334_gshared)(__this, ___value0, method)
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.State>::GetEnumerator()
#define LinkedList_1_GetEnumerator_m4198488791(__this, method) ((  Enumerator_t2423213364  (*) (LinkedList_1_t940058804 *, const MethodInfo*))LinkedList_1_GetEnumerator_m3713737734_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.State>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define LinkedList_1_GetObjectData_m3738325038(__this, ___info0, ___context1, method) ((  void (*) (LinkedList_1_t940058804 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))LinkedList_1_GetObjectData_m3974480661_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.State>::OnDeserialization(System.Object)
#define LinkedList_1_OnDeserialization_m883360502(__this, ___sender0, method) ((  void (*) (LinkedList_1_t940058804 *, Il2CppObject *, const MethodInfo*))LinkedList_1_OnDeserialization_m3445006959_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.State>::Remove(T)
#define LinkedList_1_Remove_m478126676(__this, ___value0, method) ((  bool (*) (LinkedList_1_t940058804 *, State_t3065423635 *, const MethodInfo*))LinkedList_1_Remove_m3283493303_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.State>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedList_1_Remove_m3043898475(__this, ___node0, method) ((  void (*) (LinkedList_1_t940058804 *, LinkedListNode_1_t2682158342 *, const MethodInfo*))LinkedList_1_Remove_m4034790180_gshared)(__this, ___node0, method)
// System.Void System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.State>::RemoveLast()
#define LinkedList_1_RemoveLast_m2792890158(__this, method) ((  void (*) (LinkedList_1_t940058804 *, const MethodInfo*))LinkedList_1_RemoveLast_m2573038887_gshared)(__this, method)
// System.Int32 System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.State>::get_Count()
#define LinkedList_1_get_Count_m3756071108(__this, method) ((  int32_t (*) (LinkedList_1_t940058804 *, const MethodInfo*))LinkedList_1_get_Count_m1368924491_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<ZXing.Aztec.Internal.State>::get_First()
#define LinkedList_1_get_First_m674956807(__this, method) ((  LinkedListNode_1_t2682158342 * (*) (LinkedList_1_t940058804 *, const MethodInfo*))LinkedList_1_get_First_m3278587786_gshared)(__this, method)
