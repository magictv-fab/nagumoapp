﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// HutongGames.PlayMaker.FsmEventData
struct FsmEventData_t1076900934;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t667441552;
// FsmTemplate
struct FsmTemplate_t1237263802;
// HutongGames.PlayMaker.FsmState[]
struct FsmStateU5BU5D_t2644459362;
// HutongGames.PlayMaker.FsmEvent[]
struct FsmEventU5BU5D_t2862142229;
// HutongGames.PlayMaker.FsmTransition[]
struct FsmTransitionU5BU5D_t818210886;
// HutongGames.PlayMaker.FsmVariables
struct FsmVariables_t963491929;
// HutongGames.PlayMaker.Fsm
struct Fsm_t1527112426;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.Fsm>
struct List_1_t2895297978;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEvent>
struct List_1_t3501653580;
// HutongGames.PlayMaker.FsmLog
struct FsmLog_t1596141350;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.DelayedEvent>
struct List_1_t3307092330;
// HutongGames.PlayMaker.FsmState
struct FsmState_t2146334067;
// UnityEngine.Color[]
struct ColorU5BU5D_t2441545636;
// HutongGames.PlayMaker.FsmEventTarget
struct FsmEventTarget_t1823904941;
// HutongGames.PlayMaker.FsmTransition
struct FsmTransition_t3771611999;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Collision
struct Collision_t2494107688;
// UnityEngine.Collider
struct Collider_t2939674232;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t2416790841;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Fsm
struct  Fsm_t1527112426  : public Il2CppObject
{
public:
	// UnityEngine.MonoBehaviour HutongGames.PlayMaker.Fsm::owner
	MonoBehaviour_t667441552 * ___owner_5;
	// FsmTemplate HutongGames.PlayMaker.Fsm::usedInTemplate
	FsmTemplate_t1237263802 * ___usedInTemplate_6;
	// System.String HutongGames.PlayMaker.Fsm::name
	String_t* ___name_7;
	// System.String HutongGames.PlayMaker.Fsm::startState
	String_t* ___startState_8;
	// HutongGames.PlayMaker.FsmState[] HutongGames.PlayMaker.Fsm::states
	FsmStateU5BU5D_t2644459362* ___states_9;
	// HutongGames.PlayMaker.FsmEvent[] HutongGames.PlayMaker.Fsm::events
	FsmEventU5BU5D_t2862142229* ___events_10;
	// HutongGames.PlayMaker.FsmTransition[] HutongGames.PlayMaker.Fsm::globalTransitions
	FsmTransitionU5BU5D_t818210886* ___globalTransitions_11;
	// HutongGames.PlayMaker.FsmVariables HutongGames.PlayMaker.Fsm::variables
	FsmVariables_t963491929 * ___variables_12;
	// System.String HutongGames.PlayMaker.Fsm::description
	String_t* ___description_13;
	// System.String HutongGames.PlayMaker.Fsm::docUrl
	String_t* ___docUrl_14;
	// System.Boolean HutongGames.PlayMaker.Fsm::showStateLabel
	bool ___showStateLabel_15;
	// System.Int32 HutongGames.PlayMaker.Fsm::maxLoopCount
	int32_t ___maxLoopCount_16;
	// System.String HutongGames.PlayMaker.Fsm::watermark
	String_t* ___watermark_17;
	// System.Int32 HutongGames.PlayMaker.Fsm::version
	int32_t ___version_18;
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.Fsm::host
	Fsm_t1527112426 * ___host_19;
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.Fsm::rootFsm
	Fsm_t1527112426 * ___rootFsm_20;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.Fsm> HutongGames.PlayMaker.Fsm::subFsmList
	List_1_t2895297978 * ___subFsmList_21;
	// System.Boolean HutongGames.PlayMaker.Fsm::activeStateEntered
	bool ___activeStateEntered_22;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEvent> HutongGames.PlayMaker.Fsm::ExposedEvents
	List_1_t3501653580 * ___ExposedEvents_23;
	// HutongGames.PlayMaker.FsmLog HutongGames.PlayMaker.Fsm::myLog
	FsmLog_t1596141350 * ___myLog_24;
	// System.Boolean HutongGames.PlayMaker.Fsm::RestartOnEnable
	bool ___RestartOnEnable_25;
	// System.Boolean HutongGames.PlayMaker.Fsm::EnableDebugFlow
	bool ___EnableDebugFlow_26;
	// System.Boolean HutongGames.PlayMaker.Fsm::StepFrame
	bool ___StepFrame_27;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.DelayedEvent> HutongGames.PlayMaker.Fsm::delayedEvents
	List_1_t3307092330 * ___delayedEvents_28;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.DelayedEvent> HutongGames.PlayMaker.Fsm::updateEvents
	List_1_t3307092330 * ___updateEvents_29;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.DelayedEvent> HutongGames.PlayMaker.Fsm::removeEvents
	List_1_t3307092330 * ___removeEvents_30;
	// System.Boolean HutongGames.PlayMaker.Fsm::initialized
	bool ___initialized_31;
	// System.String HutongGames.PlayMaker.Fsm::activeStateName
	String_t* ___activeStateName_32;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.Fsm::activeState
	FsmState_t2146334067 * ___activeState_33;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.Fsm::switchToState
	FsmState_t2146334067 * ___switchToState_34;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.Fsm::previousActiveState
	FsmState_t2146334067 * ___previousActiveState_35;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.Fsm::editState
	FsmState_t2146334067 * ___editState_37;
	// System.Boolean HutongGames.PlayMaker.Fsm::mouseEvents
	bool ___mouseEvents_38;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleTriggerEnter
	bool ___handleTriggerEnter_39;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleTriggerExit
	bool ___handleTriggerExit_40;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleTriggerStay
	bool ___handleTriggerStay_41;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleCollisionEnter
	bool ___handleCollisionEnter_42;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleCollisionExit
	bool ___handleCollisionExit_43;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleCollisionStay
	bool ___handleCollisionStay_44;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleOnGUI
	bool ___handleOnGUI_45;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleFixedUpdate
	bool ___handleFixedUpdate_46;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleApplicationEvents
	bool ___handleApplicationEvents_47;
	// System.Boolean HutongGames.PlayMaker.Fsm::<Started>k__BackingField
	bool ___U3CStartedU3Ek__BackingField_49;
	// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.Fsm::<EventTarget>k__BackingField
	FsmEventTarget_t1823904941 * ___U3CEventTargetU3Ek__BackingField_50;
	// System.Boolean HutongGames.PlayMaker.Fsm::<Finished>k__BackingField
	bool ___U3CFinishedU3Ek__BackingField_51;
	// HutongGames.PlayMaker.FsmTransition HutongGames.PlayMaker.Fsm::<LastTransition>k__BackingField
	FsmTransition_t3771611999 * ___U3CLastTransitionU3Ek__BackingField_52;
	// System.Boolean HutongGames.PlayMaker.Fsm::<IsModifiedPrefabInstance>k__BackingField
	bool ___U3CIsModifiedPrefabInstanceU3Ek__BackingField_53;
	// System.Boolean HutongGames.PlayMaker.Fsm::<SwitchedState>k__BackingField
	bool ___U3CSwitchedStateU3Ek__BackingField_64;
	// UnityEngine.Collision HutongGames.PlayMaker.Fsm::<CollisionInfo>k__BackingField
	Collision_t2494107688 * ___U3CCollisionInfoU3Ek__BackingField_65;
	// UnityEngine.Collider HutongGames.PlayMaker.Fsm::<TriggerCollider>k__BackingField
	Collider_t2939674232 * ___U3CTriggerColliderU3Ek__BackingField_66;
	// UnityEngine.ControllerColliderHit HutongGames.PlayMaker.Fsm::<ControllerCollider>k__BackingField
	ControllerColliderHit_t2416790841 * ___U3CControllerColliderU3Ek__BackingField_67;
	// UnityEngine.RaycastHit HutongGames.PlayMaker.Fsm::<RaycastHitInfo>k__BackingField
	RaycastHit_t4003175726  ___U3CRaycastHitInfoU3Ek__BackingField_68;

public:
	inline static int32_t get_offset_of_owner_5() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___owner_5)); }
	inline MonoBehaviour_t667441552 * get_owner_5() const { return ___owner_5; }
	inline MonoBehaviour_t667441552 ** get_address_of_owner_5() { return &___owner_5; }
	inline void set_owner_5(MonoBehaviour_t667441552 * value)
	{
		___owner_5 = value;
		Il2CppCodeGenWriteBarrier(&___owner_5, value);
	}

	inline static int32_t get_offset_of_usedInTemplate_6() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___usedInTemplate_6)); }
	inline FsmTemplate_t1237263802 * get_usedInTemplate_6() const { return ___usedInTemplate_6; }
	inline FsmTemplate_t1237263802 ** get_address_of_usedInTemplate_6() { return &___usedInTemplate_6; }
	inline void set_usedInTemplate_6(FsmTemplate_t1237263802 * value)
	{
		___usedInTemplate_6 = value;
		Il2CppCodeGenWriteBarrier(&___usedInTemplate_6, value);
	}

	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier(&___name_7, value);
	}

	inline static int32_t get_offset_of_startState_8() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___startState_8)); }
	inline String_t* get_startState_8() const { return ___startState_8; }
	inline String_t** get_address_of_startState_8() { return &___startState_8; }
	inline void set_startState_8(String_t* value)
	{
		___startState_8 = value;
		Il2CppCodeGenWriteBarrier(&___startState_8, value);
	}

	inline static int32_t get_offset_of_states_9() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___states_9)); }
	inline FsmStateU5BU5D_t2644459362* get_states_9() const { return ___states_9; }
	inline FsmStateU5BU5D_t2644459362** get_address_of_states_9() { return &___states_9; }
	inline void set_states_9(FsmStateU5BU5D_t2644459362* value)
	{
		___states_9 = value;
		Il2CppCodeGenWriteBarrier(&___states_9, value);
	}

	inline static int32_t get_offset_of_events_10() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___events_10)); }
	inline FsmEventU5BU5D_t2862142229* get_events_10() const { return ___events_10; }
	inline FsmEventU5BU5D_t2862142229** get_address_of_events_10() { return &___events_10; }
	inline void set_events_10(FsmEventU5BU5D_t2862142229* value)
	{
		___events_10 = value;
		Il2CppCodeGenWriteBarrier(&___events_10, value);
	}

	inline static int32_t get_offset_of_globalTransitions_11() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___globalTransitions_11)); }
	inline FsmTransitionU5BU5D_t818210886* get_globalTransitions_11() const { return ___globalTransitions_11; }
	inline FsmTransitionU5BU5D_t818210886** get_address_of_globalTransitions_11() { return &___globalTransitions_11; }
	inline void set_globalTransitions_11(FsmTransitionU5BU5D_t818210886* value)
	{
		___globalTransitions_11 = value;
		Il2CppCodeGenWriteBarrier(&___globalTransitions_11, value);
	}

	inline static int32_t get_offset_of_variables_12() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___variables_12)); }
	inline FsmVariables_t963491929 * get_variables_12() const { return ___variables_12; }
	inline FsmVariables_t963491929 ** get_address_of_variables_12() { return &___variables_12; }
	inline void set_variables_12(FsmVariables_t963491929 * value)
	{
		___variables_12 = value;
		Il2CppCodeGenWriteBarrier(&___variables_12, value);
	}

	inline static int32_t get_offset_of_description_13() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___description_13)); }
	inline String_t* get_description_13() const { return ___description_13; }
	inline String_t** get_address_of_description_13() { return &___description_13; }
	inline void set_description_13(String_t* value)
	{
		___description_13 = value;
		Il2CppCodeGenWriteBarrier(&___description_13, value);
	}

	inline static int32_t get_offset_of_docUrl_14() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___docUrl_14)); }
	inline String_t* get_docUrl_14() const { return ___docUrl_14; }
	inline String_t** get_address_of_docUrl_14() { return &___docUrl_14; }
	inline void set_docUrl_14(String_t* value)
	{
		___docUrl_14 = value;
		Il2CppCodeGenWriteBarrier(&___docUrl_14, value);
	}

	inline static int32_t get_offset_of_showStateLabel_15() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___showStateLabel_15)); }
	inline bool get_showStateLabel_15() const { return ___showStateLabel_15; }
	inline bool* get_address_of_showStateLabel_15() { return &___showStateLabel_15; }
	inline void set_showStateLabel_15(bool value)
	{
		___showStateLabel_15 = value;
	}

	inline static int32_t get_offset_of_maxLoopCount_16() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___maxLoopCount_16)); }
	inline int32_t get_maxLoopCount_16() const { return ___maxLoopCount_16; }
	inline int32_t* get_address_of_maxLoopCount_16() { return &___maxLoopCount_16; }
	inline void set_maxLoopCount_16(int32_t value)
	{
		___maxLoopCount_16 = value;
	}

	inline static int32_t get_offset_of_watermark_17() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___watermark_17)); }
	inline String_t* get_watermark_17() const { return ___watermark_17; }
	inline String_t** get_address_of_watermark_17() { return &___watermark_17; }
	inline void set_watermark_17(String_t* value)
	{
		___watermark_17 = value;
		Il2CppCodeGenWriteBarrier(&___watermark_17, value);
	}

	inline static int32_t get_offset_of_version_18() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___version_18)); }
	inline int32_t get_version_18() const { return ___version_18; }
	inline int32_t* get_address_of_version_18() { return &___version_18; }
	inline void set_version_18(int32_t value)
	{
		___version_18 = value;
	}

	inline static int32_t get_offset_of_host_19() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___host_19)); }
	inline Fsm_t1527112426 * get_host_19() const { return ___host_19; }
	inline Fsm_t1527112426 ** get_address_of_host_19() { return &___host_19; }
	inline void set_host_19(Fsm_t1527112426 * value)
	{
		___host_19 = value;
		Il2CppCodeGenWriteBarrier(&___host_19, value);
	}

	inline static int32_t get_offset_of_rootFsm_20() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___rootFsm_20)); }
	inline Fsm_t1527112426 * get_rootFsm_20() const { return ___rootFsm_20; }
	inline Fsm_t1527112426 ** get_address_of_rootFsm_20() { return &___rootFsm_20; }
	inline void set_rootFsm_20(Fsm_t1527112426 * value)
	{
		___rootFsm_20 = value;
		Il2CppCodeGenWriteBarrier(&___rootFsm_20, value);
	}

	inline static int32_t get_offset_of_subFsmList_21() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___subFsmList_21)); }
	inline List_1_t2895297978 * get_subFsmList_21() const { return ___subFsmList_21; }
	inline List_1_t2895297978 ** get_address_of_subFsmList_21() { return &___subFsmList_21; }
	inline void set_subFsmList_21(List_1_t2895297978 * value)
	{
		___subFsmList_21 = value;
		Il2CppCodeGenWriteBarrier(&___subFsmList_21, value);
	}

	inline static int32_t get_offset_of_activeStateEntered_22() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___activeStateEntered_22)); }
	inline bool get_activeStateEntered_22() const { return ___activeStateEntered_22; }
	inline bool* get_address_of_activeStateEntered_22() { return &___activeStateEntered_22; }
	inline void set_activeStateEntered_22(bool value)
	{
		___activeStateEntered_22 = value;
	}

	inline static int32_t get_offset_of_ExposedEvents_23() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___ExposedEvents_23)); }
	inline List_1_t3501653580 * get_ExposedEvents_23() const { return ___ExposedEvents_23; }
	inline List_1_t3501653580 ** get_address_of_ExposedEvents_23() { return &___ExposedEvents_23; }
	inline void set_ExposedEvents_23(List_1_t3501653580 * value)
	{
		___ExposedEvents_23 = value;
		Il2CppCodeGenWriteBarrier(&___ExposedEvents_23, value);
	}

	inline static int32_t get_offset_of_myLog_24() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___myLog_24)); }
	inline FsmLog_t1596141350 * get_myLog_24() const { return ___myLog_24; }
	inline FsmLog_t1596141350 ** get_address_of_myLog_24() { return &___myLog_24; }
	inline void set_myLog_24(FsmLog_t1596141350 * value)
	{
		___myLog_24 = value;
		Il2CppCodeGenWriteBarrier(&___myLog_24, value);
	}

	inline static int32_t get_offset_of_RestartOnEnable_25() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___RestartOnEnable_25)); }
	inline bool get_RestartOnEnable_25() const { return ___RestartOnEnable_25; }
	inline bool* get_address_of_RestartOnEnable_25() { return &___RestartOnEnable_25; }
	inline void set_RestartOnEnable_25(bool value)
	{
		___RestartOnEnable_25 = value;
	}

	inline static int32_t get_offset_of_EnableDebugFlow_26() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___EnableDebugFlow_26)); }
	inline bool get_EnableDebugFlow_26() const { return ___EnableDebugFlow_26; }
	inline bool* get_address_of_EnableDebugFlow_26() { return &___EnableDebugFlow_26; }
	inline void set_EnableDebugFlow_26(bool value)
	{
		___EnableDebugFlow_26 = value;
	}

	inline static int32_t get_offset_of_StepFrame_27() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___StepFrame_27)); }
	inline bool get_StepFrame_27() const { return ___StepFrame_27; }
	inline bool* get_address_of_StepFrame_27() { return &___StepFrame_27; }
	inline void set_StepFrame_27(bool value)
	{
		___StepFrame_27 = value;
	}

	inline static int32_t get_offset_of_delayedEvents_28() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___delayedEvents_28)); }
	inline List_1_t3307092330 * get_delayedEvents_28() const { return ___delayedEvents_28; }
	inline List_1_t3307092330 ** get_address_of_delayedEvents_28() { return &___delayedEvents_28; }
	inline void set_delayedEvents_28(List_1_t3307092330 * value)
	{
		___delayedEvents_28 = value;
		Il2CppCodeGenWriteBarrier(&___delayedEvents_28, value);
	}

	inline static int32_t get_offset_of_updateEvents_29() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___updateEvents_29)); }
	inline List_1_t3307092330 * get_updateEvents_29() const { return ___updateEvents_29; }
	inline List_1_t3307092330 ** get_address_of_updateEvents_29() { return &___updateEvents_29; }
	inline void set_updateEvents_29(List_1_t3307092330 * value)
	{
		___updateEvents_29 = value;
		Il2CppCodeGenWriteBarrier(&___updateEvents_29, value);
	}

	inline static int32_t get_offset_of_removeEvents_30() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___removeEvents_30)); }
	inline List_1_t3307092330 * get_removeEvents_30() const { return ___removeEvents_30; }
	inline List_1_t3307092330 ** get_address_of_removeEvents_30() { return &___removeEvents_30; }
	inline void set_removeEvents_30(List_1_t3307092330 * value)
	{
		___removeEvents_30 = value;
		Il2CppCodeGenWriteBarrier(&___removeEvents_30, value);
	}

	inline static int32_t get_offset_of_initialized_31() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___initialized_31)); }
	inline bool get_initialized_31() const { return ___initialized_31; }
	inline bool* get_address_of_initialized_31() { return &___initialized_31; }
	inline void set_initialized_31(bool value)
	{
		___initialized_31 = value;
	}

	inline static int32_t get_offset_of_activeStateName_32() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___activeStateName_32)); }
	inline String_t* get_activeStateName_32() const { return ___activeStateName_32; }
	inline String_t** get_address_of_activeStateName_32() { return &___activeStateName_32; }
	inline void set_activeStateName_32(String_t* value)
	{
		___activeStateName_32 = value;
		Il2CppCodeGenWriteBarrier(&___activeStateName_32, value);
	}

	inline static int32_t get_offset_of_activeState_33() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___activeState_33)); }
	inline FsmState_t2146334067 * get_activeState_33() const { return ___activeState_33; }
	inline FsmState_t2146334067 ** get_address_of_activeState_33() { return &___activeState_33; }
	inline void set_activeState_33(FsmState_t2146334067 * value)
	{
		___activeState_33 = value;
		Il2CppCodeGenWriteBarrier(&___activeState_33, value);
	}

	inline static int32_t get_offset_of_switchToState_34() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___switchToState_34)); }
	inline FsmState_t2146334067 * get_switchToState_34() const { return ___switchToState_34; }
	inline FsmState_t2146334067 ** get_address_of_switchToState_34() { return &___switchToState_34; }
	inline void set_switchToState_34(FsmState_t2146334067 * value)
	{
		___switchToState_34 = value;
		Il2CppCodeGenWriteBarrier(&___switchToState_34, value);
	}

	inline static int32_t get_offset_of_previousActiveState_35() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___previousActiveState_35)); }
	inline FsmState_t2146334067 * get_previousActiveState_35() const { return ___previousActiveState_35; }
	inline FsmState_t2146334067 ** get_address_of_previousActiveState_35() { return &___previousActiveState_35; }
	inline void set_previousActiveState_35(FsmState_t2146334067 * value)
	{
		___previousActiveState_35 = value;
		Il2CppCodeGenWriteBarrier(&___previousActiveState_35, value);
	}

	inline static int32_t get_offset_of_editState_37() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___editState_37)); }
	inline FsmState_t2146334067 * get_editState_37() const { return ___editState_37; }
	inline FsmState_t2146334067 ** get_address_of_editState_37() { return &___editState_37; }
	inline void set_editState_37(FsmState_t2146334067 * value)
	{
		___editState_37 = value;
		Il2CppCodeGenWriteBarrier(&___editState_37, value);
	}

	inline static int32_t get_offset_of_mouseEvents_38() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___mouseEvents_38)); }
	inline bool get_mouseEvents_38() const { return ___mouseEvents_38; }
	inline bool* get_address_of_mouseEvents_38() { return &___mouseEvents_38; }
	inline void set_mouseEvents_38(bool value)
	{
		___mouseEvents_38 = value;
	}

	inline static int32_t get_offset_of_handleTriggerEnter_39() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___handleTriggerEnter_39)); }
	inline bool get_handleTriggerEnter_39() const { return ___handleTriggerEnter_39; }
	inline bool* get_address_of_handleTriggerEnter_39() { return &___handleTriggerEnter_39; }
	inline void set_handleTriggerEnter_39(bool value)
	{
		___handleTriggerEnter_39 = value;
	}

	inline static int32_t get_offset_of_handleTriggerExit_40() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___handleTriggerExit_40)); }
	inline bool get_handleTriggerExit_40() const { return ___handleTriggerExit_40; }
	inline bool* get_address_of_handleTriggerExit_40() { return &___handleTriggerExit_40; }
	inline void set_handleTriggerExit_40(bool value)
	{
		___handleTriggerExit_40 = value;
	}

	inline static int32_t get_offset_of_handleTriggerStay_41() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___handleTriggerStay_41)); }
	inline bool get_handleTriggerStay_41() const { return ___handleTriggerStay_41; }
	inline bool* get_address_of_handleTriggerStay_41() { return &___handleTriggerStay_41; }
	inline void set_handleTriggerStay_41(bool value)
	{
		___handleTriggerStay_41 = value;
	}

	inline static int32_t get_offset_of_handleCollisionEnter_42() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___handleCollisionEnter_42)); }
	inline bool get_handleCollisionEnter_42() const { return ___handleCollisionEnter_42; }
	inline bool* get_address_of_handleCollisionEnter_42() { return &___handleCollisionEnter_42; }
	inline void set_handleCollisionEnter_42(bool value)
	{
		___handleCollisionEnter_42 = value;
	}

	inline static int32_t get_offset_of_handleCollisionExit_43() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___handleCollisionExit_43)); }
	inline bool get_handleCollisionExit_43() const { return ___handleCollisionExit_43; }
	inline bool* get_address_of_handleCollisionExit_43() { return &___handleCollisionExit_43; }
	inline void set_handleCollisionExit_43(bool value)
	{
		___handleCollisionExit_43 = value;
	}

	inline static int32_t get_offset_of_handleCollisionStay_44() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___handleCollisionStay_44)); }
	inline bool get_handleCollisionStay_44() const { return ___handleCollisionStay_44; }
	inline bool* get_address_of_handleCollisionStay_44() { return &___handleCollisionStay_44; }
	inline void set_handleCollisionStay_44(bool value)
	{
		___handleCollisionStay_44 = value;
	}

	inline static int32_t get_offset_of_handleOnGUI_45() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___handleOnGUI_45)); }
	inline bool get_handleOnGUI_45() const { return ___handleOnGUI_45; }
	inline bool* get_address_of_handleOnGUI_45() { return &___handleOnGUI_45; }
	inline void set_handleOnGUI_45(bool value)
	{
		___handleOnGUI_45 = value;
	}

	inline static int32_t get_offset_of_handleFixedUpdate_46() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___handleFixedUpdate_46)); }
	inline bool get_handleFixedUpdate_46() const { return ___handleFixedUpdate_46; }
	inline bool* get_address_of_handleFixedUpdate_46() { return &___handleFixedUpdate_46; }
	inline void set_handleFixedUpdate_46(bool value)
	{
		___handleFixedUpdate_46 = value;
	}

	inline static int32_t get_offset_of_handleApplicationEvents_47() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___handleApplicationEvents_47)); }
	inline bool get_handleApplicationEvents_47() const { return ___handleApplicationEvents_47; }
	inline bool* get_address_of_handleApplicationEvents_47() { return &___handleApplicationEvents_47; }
	inline void set_handleApplicationEvents_47(bool value)
	{
		___handleApplicationEvents_47 = value;
	}

	inline static int32_t get_offset_of_U3CStartedU3Ek__BackingField_49() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___U3CStartedU3Ek__BackingField_49)); }
	inline bool get_U3CStartedU3Ek__BackingField_49() const { return ___U3CStartedU3Ek__BackingField_49; }
	inline bool* get_address_of_U3CStartedU3Ek__BackingField_49() { return &___U3CStartedU3Ek__BackingField_49; }
	inline void set_U3CStartedU3Ek__BackingField_49(bool value)
	{
		___U3CStartedU3Ek__BackingField_49 = value;
	}

	inline static int32_t get_offset_of_U3CEventTargetU3Ek__BackingField_50() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___U3CEventTargetU3Ek__BackingField_50)); }
	inline FsmEventTarget_t1823904941 * get_U3CEventTargetU3Ek__BackingField_50() const { return ___U3CEventTargetU3Ek__BackingField_50; }
	inline FsmEventTarget_t1823904941 ** get_address_of_U3CEventTargetU3Ek__BackingField_50() { return &___U3CEventTargetU3Ek__BackingField_50; }
	inline void set_U3CEventTargetU3Ek__BackingField_50(FsmEventTarget_t1823904941 * value)
	{
		___U3CEventTargetU3Ek__BackingField_50 = value;
		Il2CppCodeGenWriteBarrier(&___U3CEventTargetU3Ek__BackingField_50, value);
	}

	inline static int32_t get_offset_of_U3CFinishedU3Ek__BackingField_51() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___U3CFinishedU3Ek__BackingField_51)); }
	inline bool get_U3CFinishedU3Ek__BackingField_51() const { return ___U3CFinishedU3Ek__BackingField_51; }
	inline bool* get_address_of_U3CFinishedU3Ek__BackingField_51() { return &___U3CFinishedU3Ek__BackingField_51; }
	inline void set_U3CFinishedU3Ek__BackingField_51(bool value)
	{
		___U3CFinishedU3Ek__BackingField_51 = value;
	}

	inline static int32_t get_offset_of_U3CLastTransitionU3Ek__BackingField_52() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___U3CLastTransitionU3Ek__BackingField_52)); }
	inline FsmTransition_t3771611999 * get_U3CLastTransitionU3Ek__BackingField_52() const { return ___U3CLastTransitionU3Ek__BackingField_52; }
	inline FsmTransition_t3771611999 ** get_address_of_U3CLastTransitionU3Ek__BackingField_52() { return &___U3CLastTransitionU3Ek__BackingField_52; }
	inline void set_U3CLastTransitionU3Ek__BackingField_52(FsmTransition_t3771611999 * value)
	{
		___U3CLastTransitionU3Ek__BackingField_52 = value;
		Il2CppCodeGenWriteBarrier(&___U3CLastTransitionU3Ek__BackingField_52, value);
	}

	inline static int32_t get_offset_of_U3CIsModifiedPrefabInstanceU3Ek__BackingField_53() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___U3CIsModifiedPrefabInstanceU3Ek__BackingField_53)); }
	inline bool get_U3CIsModifiedPrefabInstanceU3Ek__BackingField_53() const { return ___U3CIsModifiedPrefabInstanceU3Ek__BackingField_53; }
	inline bool* get_address_of_U3CIsModifiedPrefabInstanceU3Ek__BackingField_53() { return &___U3CIsModifiedPrefabInstanceU3Ek__BackingField_53; }
	inline void set_U3CIsModifiedPrefabInstanceU3Ek__BackingField_53(bool value)
	{
		___U3CIsModifiedPrefabInstanceU3Ek__BackingField_53 = value;
	}

	inline static int32_t get_offset_of_U3CSwitchedStateU3Ek__BackingField_64() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___U3CSwitchedStateU3Ek__BackingField_64)); }
	inline bool get_U3CSwitchedStateU3Ek__BackingField_64() const { return ___U3CSwitchedStateU3Ek__BackingField_64; }
	inline bool* get_address_of_U3CSwitchedStateU3Ek__BackingField_64() { return &___U3CSwitchedStateU3Ek__BackingField_64; }
	inline void set_U3CSwitchedStateU3Ek__BackingField_64(bool value)
	{
		___U3CSwitchedStateU3Ek__BackingField_64 = value;
	}

	inline static int32_t get_offset_of_U3CCollisionInfoU3Ek__BackingField_65() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___U3CCollisionInfoU3Ek__BackingField_65)); }
	inline Collision_t2494107688 * get_U3CCollisionInfoU3Ek__BackingField_65() const { return ___U3CCollisionInfoU3Ek__BackingField_65; }
	inline Collision_t2494107688 ** get_address_of_U3CCollisionInfoU3Ek__BackingField_65() { return &___U3CCollisionInfoU3Ek__BackingField_65; }
	inline void set_U3CCollisionInfoU3Ek__BackingField_65(Collision_t2494107688 * value)
	{
		___U3CCollisionInfoU3Ek__BackingField_65 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCollisionInfoU3Ek__BackingField_65, value);
	}

	inline static int32_t get_offset_of_U3CTriggerColliderU3Ek__BackingField_66() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___U3CTriggerColliderU3Ek__BackingField_66)); }
	inline Collider_t2939674232 * get_U3CTriggerColliderU3Ek__BackingField_66() const { return ___U3CTriggerColliderU3Ek__BackingField_66; }
	inline Collider_t2939674232 ** get_address_of_U3CTriggerColliderU3Ek__BackingField_66() { return &___U3CTriggerColliderU3Ek__BackingField_66; }
	inline void set_U3CTriggerColliderU3Ek__BackingField_66(Collider_t2939674232 * value)
	{
		___U3CTriggerColliderU3Ek__BackingField_66 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTriggerColliderU3Ek__BackingField_66, value);
	}

	inline static int32_t get_offset_of_U3CControllerColliderU3Ek__BackingField_67() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___U3CControllerColliderU3Ek__BackingField_67)); }
	inline ControllerColliderHit_t2416790841 * get_U3CControllerColliderU3Ek__BackingField_67() const { return ___U3CControllerColliderU3Ek__BackingField_67; }
	inline ControllerColliderHit_t2416790841 ** get_address_of_U3CControllerColliderU3Ek__BackingField_67() { return &___U3CControllerColliderU3Ek__BackingField_67; }
	inline void set_U3CControllerColliderU3Ek__BackingField_67(ControllerColliderHit_t2416790841 * value)
	{
		___U3CControllerColliderU3Ek__BackingField_67 = value;
		Il2CppCodeGenWriteBarrier(&___U3CControllerColliderU3Ek__BackingField_67, value);
	}

	inline static int32_t get_offset_of_U3CRaycastHitInfoU3Ek__BackingField_68() { return static_cast<int32_t>(offsetof(Fsm_t1527112426, ___U3CRaycastHitInfoU3Ek__BackingField_68)); }
	inline RaycastHit_t4003175726  get_U3CRaycastHitInfoU3Ek__BackingField_68() const { return ___U3CRaycastHitInfoU3Ek__BackingField_68; }
	inline RaycastHit_t4003175726 * get_address_of_U3CRaycastHitInfoU3Ek__BackingField_68() { return &___U3CRaycastHitInfoU3Ek__BackingField_68; }
	inline void set_U3CRaycastHitInfoU3Ek__BackingField_68(RaycastHit_t4003175726  value)
	{
		___U3CRaycastHitInfoU3Ek__BackingField_68 = value;
	}
};

struct Fsm_t1527112426_StaticFields
{
public:
	// HutongGames.PlayMaker.FsmEventData HutongGames.PlayMaker.Fsm::EventData
	FsmEventData_t1076900934 * ___EventData_2;
	// UnityEngine.Color HutongGames.PlayMaker.Fsm::debugLookAtColor
	Color_t4194546905  ___debugLookAtColor_3;
	// UnityEngine.Color HutongGames.PlayMaker.Fsm::debugRaycastColor
	Color_t4194546905  ___debugRaycastColor_4;
	// UnityEngine.Color[] HutongGames.PlayMaker.Fsm::StateColors
	ColorU5BU5D_t2441545636* ___StateColors_36;
	// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.Fsm::targetSelf
	FsmEventTarget_t1823904941 * ___targetSelf_48;
	// UnityEngine.GameObject HutongGames.PlayMaker.Fsm::<LastClickedObject>k__BackingField
	GameObject_t3674682005 * ___U3CLastClickedObjectU3Ek__BackingField_54;
	// System.Boolean HutongGames.PlayMaker.Fsm::<BreakpointsEnabled>k__BackingField
	bool ___U3CBreakpointsEnabledU3Ek__BackingField_55;
	// System.Boolean HutongGames.PlayMaker.Fsm::<HitBreakpoint>k__BackingField
	bool ___U3CHitBreakpointU3Ek__BackingField_56;
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.Fsm::<BreakAtFsm>k__BackingField
	Fsm_t1527112426 * ___U3CBreakAtFsmU3Ek__BackingField_57;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.Fsm::<BreakAtState>k__BackingField
	FsmState_t2146334067 * ___U3CBreakAtStateU3Ek__BackingField_58;
	// System.Boolean HutongGames.PlayMaker.Fsm::<IsBreak>k__BackingField
	bool ___U3CIsBreakU3Ek__BackingField_59;
	// System.Boolean HutongGames.PlayMaker.Fsm::<IsErrorBreak>k__BackingField
	bool ___U3CIsErrorBreakU3Ek__BackingField_60;
	// System.String HutongGames.PlayMaker.Fsm::<LastError>k__BackingField
	String_t* ___U3CLastErrorU3Ek__BackingField_61;
	// System.Boolean HutongGames.PlayMaker.Fsm::<StepToStateChange>k__BackingField
	bool ___U3CStepToStateChangeU3Ek__BackingField_62;
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.Fsm::<StepFsm>k__BackingField
	Fsm_t1527112426 * ___U3CStepFsmU3Ek__BackingField_63;

public:
	inline static int32_t get_offset_of_EventData_2() { return static_cast<int32_t>(offsetof(Fsm_t1527112426_StaticFields, ___EventData_2)); }
	inline FsmEventData_t1076900934 * get_EventData_2() const { return ___EventData_2; }
	inline FsmEventData_t1076900934 ** get_address_of_EventData_2() { return &___EventData_2; }
	inline void set_EventData_2(FsmEventData_t1076900934 * value)
	{
		___EventData_2 = value;
		Il2CppCodeGenWriteBarrier(&___EventData_2, value);
	}

	inline static int32_t get_offset_of_debugLookAtColor_3() { return static_cast<int32_t>(offsetof(Fsm_t1527112426_StaticFields, ___debugLookAtColor_3)); }
	inline Color_t4194546905  get_debugLookAtColor_3() const { return ___debugLookAtColor_3; }
	inline Color_t4194546905 * get_address_of_debugLookAtColor_3() { return &___debugLookAtColor_3; }
	inline void set_debugLookAtColor_3(Color_t4194546905  value)
	{
		___debugLookAtColor_3 = value;
	}

	inline static int32_t get_offset_of_debugRaycastColor_4() { return static_cast<int32_t>(offsetof(Fsm_t1527112426_StaticFields, ___debugRaycastColor_4)); }
	inline Color_t4194546905  get_debugRaycastColor_4() const { return ___debugRaycastColor_4; }
	inline Color_t4194546905 * get_address_of_debugRaycastColor_4() { return &___debugRaycastColor_4; }
	inline void set_debugRaycastColor_4(Color_t4194546905  value)
	{
		___debugRaycastColor_4 = value;
	}

	inline static int32_t get_offset_of_StateColors_36() { return static_cast<int32_t>(offsetof(Fsm_t1527112426_StaticFields, ___StateColors_36)); }
	inline ColorU5BU5D_t2441545636* get_StateColors_36() const { return ___StateColors_36; }
	inline ColorU5BU5D_t2441545636** get_address_of_StateColors_36() { return &___StateColors_36; }
	inline void set_StateColors_36(ColorU5BU5D_t2441545636* value)
	{
		___StateColors_36 = value;
		Il2CppCodeGenWriteBarrier(&___StateColors_36, value);
	}

	inline static int32_t get_offset_of_targetSelf_48() { return static_cast<int32_t>(offsetof(Fsm_t1527112426_StaticFields, ___targetSelf_48)); }
	inline FsmEventTarget_t1823904941 * get_targetSelf_48() const { return ___targetSelf_48; }
	inline FsmEventTarget_t1823904941 ** get_address_of_targetSelf_48() { return &___targetSelf_48; }
	inline void set_targetSelf_48(FsmEventTarget_t1823904941 * value)
	{
		___targetSelf_48 = value;
		Il2CppCodeGenWriteBarrier(&___targetSelf_48, value);
	}

	inline static int32_t get_offset_of_U3CLastClickedObjectU3Ek__BackingField_54() { return static_cast<int32_t>(offsetof(Fsm_t1527112426_StaticFields, ___U3CLastClickedObjectU3Ek__BackingField_54)); }
	inline GameObject_t3674682005 * get_U3CLastClickedObjectU3Ek__BackingField_54() const { return ___U3CLastClickedObjectU3Ek__BackingField_54; }
	inline GameObject_t3674682005 ** get_address_of_U3CLastClickedObjectU3Ek__BackingField_54() { return &___U3CLastClickedObjectU3Ek__BackingField_54; }
	inline void set_U3CLastClickedObjectU3Ek__BackingField_54(GameObject_t3674682005 * value)
	{
		___U3CLastClickedObjectU3Ek__BackingField_54 = value;
		Il2CppCodeGenWriteBarrier(&___U3CLastClickedObjectU3Ek__BackingField_54, value);
	}

	inline static int32_t get_offset_of_U3CBreakpointsEnabledU3Ek__BackingField_55() { return static_cast<int32_t>(offsetof(Fsm_t1527112426_StaticFields, ___U3CBreakpointsEnabledU3Ek__BackingField_55)); }
	inline bool get_U3CBreakpointsEnabledU3Ek__BackingField_55() const { return ___U3CBreakpointsEnabledU3Ek__BackingField_55; }
	inline bool* get_address_of_U3CBreakpointsEnabledU3Ek__BackingField_55() { return &___U3CBreakpointsEnabledU3Ek__BackingField_55; }
	inline void set_U3CBreakpointsEnabledU3Ek__BackingField_55(bool value)
	{
		___U3CBreakpointsEnabledU3Ek__BackingField_55 = value;
	}

	inline static int32_t get_offset_of_U3CHitBreakpointU3Ek__BackingField_56() { return static_cast<int32_t>(offsetof(Fsm_t1527112426_StaticFields, ___U3CHitBreakpointU3Ek__BackingField_56)); }
	inline bool get_U3CHitBreakpointU3Ek__BackingField_56() const { return ___U3CHitBreakpointU3Ek__BackingField_56; }
	inline bool* get_address_of_U3CHitBreakpointU3Ek__BackingField_56() { return &___U3CHitBreakpointU3Ek__BackingField_56; }
	inline void set_U3CHitBreakpointU3Ek__BackingField_56(bool value)
	{
		___U3CHitBreakpointU3Ek__BackingField_56 = value;
	}

	inline static int32_t get_offset_of_U3CBreakAtFsmU3Ek__BackingField_57() { return static_cast<int32_t>(offsetof(Fsm_t1527112426_StaticFields, ___U3CBreakAtFsmU3Ek__BackingField_57)); }
	inline Fsm_t1527112426 * get_U3CBreakAtFsmU3Ek__BackingField_57() const { return ___U3CBreakAtFsmU3Ek__BackingField_57; }
	inline Fsm_t1527112426 ** get_address_of_U3CBreakAtFsmU3Ek__BackingField_57() { return &___U3CBreakAtFsmU3Ek__BackingField_57; }
	inline void set_U3CBreakAtFsmU3Ek__BackingField_57(Fsm_t1527112426 * value)
	{
		___U3CBreakAtFsmU3Ek__BackingField_57 = value;
		Il2CppCodeGenWriteBarrier(&___U3CBreakAtFsmU3Ek__BackingField_57, value);
	}

	inline static int32_t get_offset_of_U3CBreakAtStateU3Ek__BackingField_58() { return static_cast<int32_t>(offsetof(Fsm_t1527112426_StaticFields, ___U3CBreakAtStateU3Ek__BackingField_58)); }
	inline FsmState_t2146334067 * get_U3CBreakAtStateU3Ek__BackingField_58() const { return ___U3CBreakAtStateU3Ek__BackingField_58; }
	inline FsmState_t2146334067 ** get_address_of_U3CBreakAtStateU3Ek__BackingField_58() { return &___U3CBreakAtStateU3Ek__BackingField_58; }
	inline void set_U3CBreakAtStateU3Ek__BackingField_58(FsmState_t2146334067 * value)
	{
		___U3CBreakAtStateU3Ek__BackingField_58 = value;
		Il2CppCodeGenWriteBarrier(&___U3CBreakAtStateU3Ek__BackingField_58, value);
	}

	inline static int32_t get_offset_of_U3CIsBreakU3Ek__BackingField_59() { return static_cast<int32_t>(offsetof(Fsm_t1527112426_StaticFields, ___U3CIsBreakU3Ek__BackingField_59)); }
	inline bool get_U3CIsBreakU3Ek__BackingField_59() const { return ___U3CIsBreakU3Ek__BackingField_59; }
	inline bool* get_address_of_U3CIsBreakU3Ek__BackingField_59() { return &___U3CIsBreakU3Ek__BackingField_59; }
	inline void set_U3CIsBreakU3Ek__BackingField_59(bool value)
	{
		___U3CIsBreakU3Ek__BackingField_59 = value;
	}

	inline static int32_t get_offset_of_U3CIsErrorBreakU3Ek__BackingField_60() { return static_cast<int32_t>(offsetof(Fsm_t1527112426_StaticFields, ___U3CIsErrorBreakU3Ek__BackingField_60)); }
	inline bool get_U3CIsErrorBreakU3Ek__BackingField_60() const { return ___U3CIsErrorBreakU3Ek__BackingField_60; }
	inline bool* get_address_of_U3CIsErrorBreakU3Ek__BackingField_60() { return &___U3CIsErrorBreakU3Ek__BackingField_60; }
	inline void set_U3CIsErrorBreakU3Ek__BackingField_60(bool value)
	{
		___U3CIsErrorBreakU3Ek__BackingField_60 = value;
	}

	inline static int32_t get_offset_of_U3CLastErrorU3Ek__BackingField_61() { return static_cast<int32_t>(offsetof(Fsm_t1527112426_StaticFields, ___U3CLastErrorU3Ek__BackingField_61)); }
	inline String_t* get_U3CLastErrorU3Ek__BackingField_61() const { return ___U3CLastErrorU3Ek__BackingField_61; }
	inline String_t** get_address_of_U3CLastErrorU3Ek__BackingField_61() { return &___U3CLastErrorU3Ek__BackingField_61; }
	inline void set_U3CLastErrorU3Ek__BackingField_61(String_t* value)
	{
		___U3CLastErrorU3Ek__BackingField_61 = value;
		Il2CppCodeGenWriteBarrier(&___U3CLastErrorU3Ek__BackingField_61, value);
	}

	inline static int32_t get_offset_of_U3CStepToStateChangeU3Ek__BackingField_62() { return static_cast<int32_t>(offsetof(Fsm_t1527112426_StaticFields, ___U3CStepToStateChangeU3Ek__BackingField_62)); }
	inline bool get_U3CStepToStateChangeU3Ek__BackingField_62() const { return ___U3CStepToStateChangeU3Ek__BackingField_62; }
	inline bool* get_address_of_U3CStepToStateChangeU3Ek__BackingField_62() { return &___U3CStepToStateChangeU3Ek__BackingField_62; }
	inline void set_U3CStepToStateChangeU3Ek__BackingField_62(bool value)
	{
		___U3CStepToStateChangeU3Ek__BackingField_62 = value;
	}

	inline static int32_t get_offset_of_U3CStepFsmU3Ek__BackingField_63() { return static_cast<int32_t>(offsetof(Fsm_t1527112426_StaticFields, ___U3CStepFsmU3Ek__BackingField_63)); }
	inline Fsm_t1527112426 * get_U3CStepFsmU3Ek__BackingField_63() const { return ___U3CStepFsmU3Ek__BackingField_63; }
	inline Fsm_t1527112426 ** get_address_of_U3CStepFsmU3Ek__BackingField_63() { return &___U3CStepFsmU3Ek__BackingField_63; }
	inline void set_U3CStepFsmU3Ek__BackingField_63(Fsm_t1527112426 * value)
	{
		___U3CStepFsmU3Ek__BackingField_63 = value;
		Il2CppCodeGenWriteBarrier(&___U3CStepFsmU3Ek__BackingField_63, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
