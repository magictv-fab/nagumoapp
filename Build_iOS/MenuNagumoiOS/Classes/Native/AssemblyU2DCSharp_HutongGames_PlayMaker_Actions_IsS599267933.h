﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co2322045418.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.IsSleeping
struct  IsSleeping_t599267933  : public ComponentAction_1_t2322045418
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.IsSleeping::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.IsSleeping::trueEvent
	FsmEvent_t2133468028 * ___trueEvent_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.IsSleeping::falseEvent
	FsmEvent_t2133468028 * ___falseEvent_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.IsSleeping::store
	FsmBool_t1075959796 * ___store_14;
	// System.Boolean HutongGames.PlayMaker.Actions.IsSleeping::everyFrame
	bool ___everyFrame_15;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(IsSleeping_t599267933, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_trueEvent_12() { return static_cast<int32_t>(offsetof(IsSleeping_t599267933, ___trueEvent_12)); }
	inline FsmEvent_t2133468028 * get_trueEvent_12() const { return ___trueEvent_12; }
	inline FsmEvent_t2133468028 ** get_address_of_trueEvent_12() { return &___trueEvent_12; }
	inline void set_trueEvent_12(FsmEvent_t2133468028 * value)
	{
		___trueEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___trueEvent_12, value);
	}

	inline static int32_t get_offset_of_falseEvent_13() { return static_cast<int32_t>(offsetof(IsSleeping_t599267933, ___falseEvent_13)); }
	inline FsmEvent_t2133468028 * get_falseEvent_13() const { return ___falseEvent_13; }
	inline FsmEvent_t2133468028 ** get_address_of_falseEvent_13() { return &___falseEvent_13; }
	inline void set_falseEvent_13(FsmEvent_t2133468028 * value)
	{
		___falseEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___falseEvent_13, value);
	}

	inline static int32_t get_offset_of_store_14() { return static_cast<int32_t>(offsetof(IsSleeping_t599267933, ___store_14)); }
	inline FsmBool_t1075959796 * get_store_14() const { return ___store_14; }
	inline FsmBool_t1075959796 ** get_address_of_store_14() { return &___store_14; }
	inline void set_store_14(FsmBool_t1075959796 * value)
	{
		___store_14 = value;
		Il2CppCodeGenWriteBarrier(&___store_14, value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(IsSleeping_t599267933, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
