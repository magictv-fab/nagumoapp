﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// iTweenFSMEvents
struct iTweenFSMEvents_t871409943;
// System.String
struct String_t;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.iTweenFsmAction
struct  iTweenFsmAction_t410382178  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.iTweenFsmAction::startEvent
	FsmEvent_t2133468028 * ___startEvent_9;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.iTweenFsmAction::finishEvent
	FsmEvent_t2133468028 * ___finishEvent_10;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.iTweenFsmAction::realTime
	FsmBool_t1075959796 * ___realTime_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.iTweenFsmAction::stopOnExit
	FsmBool_t1075959796 * ___stopOnExit_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.iTweenFsmAction::loopDontFinish
	FsmBool_t1075959796 * ___loopDontFinish_13;
	// iTweenFSMEvents HutongGames.PlayMaker.Actions.iTweenFsmAction::itweenEvents
	iTweenFSMEvents_t871409943 * ___itweenEvents_14;
	// System.String HutongGames.PlayMaker.Actions.iTweenFsmAction::itweenType
	String_t* ___itweenType_15;
	// System.Int32 HutongGames.PlayMaker.Actions.iTweenFsmAction::itweenID
	int32_t ___itweenID_16;

public:
	inline static int32_t get_offset_of_startEvent_9() { return static_cast<int32_t>(offsetof(iTweenFsmAction_t410382178, ___startEvent_9)); }
	inline FsmEvent_t2133468028 * get_startEvent_9() const { return ___startEvent_9; }
	inline FsmEvent_t2133468028 ** get_address_of_startEvent_9() { return &___startEvent_9; }
	inline void set_startEvent_9(FsmEvent_t2133468028 * value)
	{
		___startEvent_9 = value;
		Il2CppCodeGenWriteBarrier(&___startEvent_9, value);
	}

	inline static int32_t get_offset_of_finishEvent_10() { return static_cast<int32_t>(offsetof(iTweenFsmAction_t410382178, ___finishEvent_10)); }
	inline FsmEvent_t2133468028 * get_finishEvent_10() const { return ___finishEvent_10; }
	inline FsmEvent_t2133468028 ** get_address_of_finishEvent_10() { return &___finishEvent_10; }
	inline void set_finishEvent_10(FsmEvent_t2133468028 * value)
	{
		___finishEvent_10 = value;
		Il2CppCodeGenWriteBarrier(&___finishEvent_10, value);
	}

	inline static int32_t get_offset_of_realTime_11() { return static_cast<int32_t>(offsetof(iTweenFsmAction_t410382178, ___realTime_11)); }
	inline FsmBool_t1075959796 * get_realTime_11() const { return ___realTime_11; }
	inline FsmBool_t1075959796 ** get_address_of_realTime_11() { return &___realTime_11; }
	inline void set_realTime_11(FsmBool_t1075959796 * value)
	{
		___realTime_11 = value;
		Il2CppCodeGenWriteBarrier(&___realTime_11, value);
	}

	inline static int32_t get_offset_of_stopOnExit_12() { return static_cast<int32_t>(offsetof(iTweenFsmAction_t410382178, ___stopOnExit_12)); }
	inline FsmBool_t1075959796 * get_stopOnExit_12() const { return ___stopOnExit_12; }
	inline FsmBool_t1075959796 ** get_address_of_stopOnExit_12() { return &___stopOnExit_12; }
	inline void set_stopOnExit_12(FsmBool_t1075959796 * value)
	{
		___stopOnExit_12 = value;
		Il2CppCodeGenWriteBarrier(&___stopOnExit_12, value);
	}

	inline static int32_t get_offset_of_loopDontFinish_13() { return static_cast<int32_t>(offsetof(iTweenFsmAction_t410382178, ___loopDontFinish_13)); }
	inline FsmBool_t1075959796 * get_loopDontFinish_13() const { return ___loopDontFinish_13; }
	inline FsmBool_t1075959796 ** get_address_of_loopDontFinish_13() { return &___loopDontFinish_13; }
	inline void set_loopDontFinish_13(FsmBool_t1075959796 * value)
	{
		___loopDontFinish_13 = value;
		Il2CppCodeGenWriteBarrier(&___loopDontFinish_13, value);
	}

	inline static int32_t get_offset_of_itweenEvents_14() { return static_cast<int32_t>(offsetof(iTweenFsmAction_t410382178, ___itweenEvents_14)); }
	inline iTweenFSMEvents_t871409943 * get_itweenEvents_14() const { return ___itweenEvents_14; }
	inline iTweenFSMEvents_t871409943 ** get_address_of_itweenEvents_14() { return &___itweenEvents_14; }
	inline void set_itweenEvents_14(iTweenFSMEvents_t871409943 * value)
	{
		___itweenEvents_14 = value;
		Il2CppCodeGenWriteBarrier(&___itweenEvents_14, value);
	}

	inline static int32_t get_offset_of_itweenType_15() { return static_cast<int32_t>(offsetof(iTweenFsmAction_t410382178, ___itweenType_15)); }
	inline String_t* get_itweenType_15() const { return ___itweenType_15; }
	inline String_t** get_address_of_itweenType_15() { return &___itweenType_15; }
	inline void set_itweenType_15(String_t* value)
	{
		___itweenType_15 = value;
		Il2CppCodeGenWriteBarrier(&___itweenType_15, value);
	}

	inline static int32_t get_offset_of_itweenID_16() { return static_cast<int32_t>(offsetof(iTweenFsmAction_t410382178, ___itweenID_16)); }
	inline int32_t get_itweenID_16() const { return ___itweenID_16; }
	inline int32_t* get_address_of_itweenID_16() { return &___itweenID_16; }
	inline void set_itweenID_16(int32_t value)
	{
		___itweenID_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
