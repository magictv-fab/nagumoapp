﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTVWindowInstructionsControllerScript
struct MagicTVWindowInstructionsControllerScript_t2425143435;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTVWindowInstructionsControllerScript::.ctor()
extern "C"  void MagicTVWindowInstructionsControllerScript__ctor_m3373867328 (MagicTVWindowInstructionsControllerScript_t2425143435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
