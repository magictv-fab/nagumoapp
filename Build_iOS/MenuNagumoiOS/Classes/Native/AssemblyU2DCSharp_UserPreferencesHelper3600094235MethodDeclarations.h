﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UserPreferencesHelper
struct UserPreferencesHelper_t3600094235;

#include "codegen/il2cpp-codegen.h"

// System.Void UserPreferencesHelper::.ctor()
extern "C"  void UserPreferencesHelper__ctor_m2231849904 (UserPreferencesHelper_t3600094235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
