﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OneSignal/PermissionObservable
struct PermissionObservable_t1360586611;
// System.Object
struct Il2CppObject;
// OSPermissionStateChanges
struct OSPermissionStateChanges_t1134260069;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_OSPermissionStateChanges1134260069.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void OneSignal/PermissionObservable::.ctor(System.Object,System.IntPtr)
extern "C"  void PermissionObservable__ctor_m3079266458 (PermissionObservable_t1360586611 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal/PermissionObservable::Invoke(OSPermissionStateChanges)
extern "C"  void PermissionObservable_Invoke_m1946363311 (PermissionObservable_t1360586611 * __this, OSPermissionStateChanges_t1134260069 * ___stateChanges0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult OneSignal/PermissionObservable::BeginInvoke(OSPermissionStateChanges,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * PermissionObservable_BeginInvoke_m1119302036 (PermissionObservable_t1360586611 * __this, OSPermissionStateChanges_t1134260069 * ___stateChanges0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal/PermissionObservable::EndInvoke(System.IAsyncResult)
extern "C"  void PermissionObservable_EndInvoke_m1724493738 (PermissionObservable_t1360586611 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
