﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// ServerControl
struct ServerControl_t2725829754;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ServerControl/<ConectandoLetreiro>c__Iterator7E
struct  U3CConectandoLetreiroU3Ec__Iterator7E_t539930572  : public Il2CppObject
{
public:
	// System.String ServerControl/<ConectandoLetreiro>c__Iterator7E::value
	String_t* ___value_0;
	// System.Int32 ServerControl/<ConectandoLetreiro>c__Iterator7E::$PC
	int32_t ___U24PC_1;
	// System.Object ServerControl/<ConectandoLetreiro>c__Iterator7E::$current
	Il2CppObject * ___U24current_2;
	// System.String ServerControl/<ConectandoLetreiro>c__Iterator7E::<$>value
	String_t* ___U3CU24U3Evalue_3;
	// ServerControl ServerControl/<ConectandoLetreiro>c__Iterator7E::<>f__this
	ServerControl_t2725829754 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(U3CConectandoLetreiroU3Ec__Iterator7E_t539930572, ___value_0)); }
	inline String_t* get_value_0() const { return ___value_0; }
	inline String_t** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(String_t* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier(&___value_0, value);
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CConectandoLetreiroU3Ec__Iterator7E_t539930572, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CConectandoLetreiroU3Ec__Iterator7E_t539930572, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Evalue_3() { return static_cast<int32_t>(offsetof(U3CConectandoLetreiroU3Ec__Iterator7E_t539930572, ___U3CU24U3Evalue_3)); }
	inline String_t* get_U3CU24U3Evalue_3() const { return ___U3CU24U3Evalue_3; }
	inline String_t** get_address_of_U3CU24U3Evalue_3() { return &___U3CU24U3Evalue_3; }
	inline void set_U3CU24U3Evalue_3(String_t* value)
	{
		___U3CU24U3Evalue_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Evalue_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CConectandoLetreiroU3Ec__Iterator7E_t539930572, ___U3CU3Ef__this_4)); }
	inline ServerControl_t2725829754 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline ServerControl_t2725829754 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(ServerControl_t2725829754 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
