﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En953886983MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.NetworkReachability,System.Action>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3067861064(__this, ___dictionary0, method) ((  void (*) (Enumerator_t554304510 *, Dictionary_2_t3531948414 *, const MethodInfo*))Enumerator__ctor_m3574981521_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.NetworkReachability,System.Action>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3538099865(__this, method) ((  Il2CppObject * (*) (Enumerator_t554304510 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1417087600_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.NetworkReachability,System.Action>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2863537005(__this, method) ((  void (*) (Enumerator_t554304510 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3040441476_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.NetworkReachability,System.Action>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3025275638(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t554304510 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1245982669_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.NetworkReachability,System.Action>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1119700149(__this, method) ((  Il2CppObject * (*) (Enumerator_t554304510 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3486561100_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.NetworkReachability,System.Action>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3338455559(__this, method) ((  Il2CppObject * (*) (Enumerator_t554304510 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1559162590_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.NetworkReachability,System.Action>::MoveNext()
#define Enumerator_MoveNext_m3787825753(__this, method) ((  bool (*) (Enumerator_t554304510 *, const MethodInfo*))Enumerator_MoveNext_m3297031152_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.NetworkReachability,System.Action>::get_Current()
#define Enumerator_get_Current_m1915572919(__this, method) ((  KeyValuePair_2_t3430729120  (*) (Enumerator_t554304510 *, const MethodInfo*))Enumerator_get_Current_m722290112_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.NetworkReachability,System.Action>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m300407526(__this, method) ((  int32_t (*) (Enumerator_t554304510 *, const MethodInfo*))Enumerator_get_CurrentKey_m656613181_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.NetworkReachability,System.Action>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1991261578(__this, method) ((  Action_t3771233898 * (*) (Enumerator_t554304510 *, const MethodInfo*))Enumerator_get_CurrentValue_m707512353_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.NetworkReachability,System.Action>::Reset()
#define Enumerator_Reset_m4123821722(__this, method) ((  void (*) (Enumerator_t554304510 *, const MethodInfo*))Enumerator_Reset_m2252190819_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.NetworkReachability,System.Action>::VerifyState()
#define Enumerator_VerifyState_m4100127523(__this, method) ((  void (*) (Enumerator_t554304510 *, const MethodInfo*))Enumerator_VerifyState_m2906844716_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.NetworkReachability,System.Action>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m187443659(__this, method) ((  void (*) (Enumerator_t554304510 *, const MethodInfo*))Enumerator_VerifyCurrent_m198934164_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.NetworkReachability,System.Action>::Dispose()
#define Enumerator_Dispose_m1261289706(__this, method) ((  void (*) (Enumerator_t554304510 *, const MethodInfo*))Enumerator_Dispose_m2215288947_gshared)(__this, method)
