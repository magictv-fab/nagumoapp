﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iTw410382178.h"
#include "AssemblyU2DCSharp_iTween_EaseType2734598229.h"
#include "AssemblyU2DCSharp_iTween_LoopType1485160459.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.iTweenScaleAdd
struct  iTweenScaleAdd_t289621857  : public iTweenFsmAction_t410382178
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.iTweenScaleAdd::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.iTweenScaleAdd::id
	FsmString_t952858651 * ___id_18;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.iTweenScaleAdd::vector
	FsmVector3_t533912882 * ___vector_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.iTweenScaleAdd::time
	FsmFloat_t2134102846 * ___time_20;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.iTweenScaleAdd::delay
	FsmFloat_t2134102846 * ___delay_21;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.iTweenScaleAdd::speed
	FsmFloat_t2134102846 * ___speed_22;
	// iTween/EaseType HutongGames.PlayMaker.Actions.iTweenScaleAdd::easeType
	int32_t ___easeType_23;
	// iTween/LoopType HutongGames.PlayMaker.Actions.iTweenScaleAdd::loopType
	int32_t ___loopType_24;

public:
	inline static int32_t get_offset_of_gameObject_17() { return static_cast<int32_t>(offsetof(iTweenScaleAdd_t289621857, ___gameObject_17)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_17() const { return ___gameObject_17; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_17() { return &___gameObject_17; }
	inline void set_gameObject_17(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_17 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_17, value);
	}

	inline static int32_t get_offset_of_id_18() { return static_cast<int32_t>(offsetof(iTweenScaleAdd_t289621857, ___id_18)); }
	inline FsmString_t952858651 * get_id_18() const { return ___id_18; }
	inline FsmString_t952858651 ** get_address_of_id_18() { return &___id_18; }
	inline void set_id_18(FsmString_t952858651 * value)
	{
		___id_18 = value;
		Il2CppCodeGenWriteBarrier(&___id_18, value);
	}

	inline static int32_t get_offset_of_vector_19() { return static_cast<int32_t>(offsetof(iTweenScaleAdd_t289621857, ___vector_19)); }
	inline FsmVector3_t533912882 * get_vector_19() const { return ___vector_19; }
	inline FsmVector3_t533912882 ** get_address_of_vector_19() { return &___vector_19; }
	inline void set_vector_19(FsmVector3_t533912882 * value)
	{
		___vector_19 = value;
		Il2CppCodeGenWriteBarrier(&___vector_19, value);
	}

	inline static int32_t get_offset_of_time_20() { return static_cast<int32_t>(offsetof(iTweenScaleAdd_t289621857, ___time_20)); }
	inline FsmFloat_t2134102846 * get_time_20() const { return ___time_20; }
	inline FsmFloat_t2134102846 ** get_address_of_time_20() { return &___time_20; }
	inline void set_time_20(FsmFloat_t2134102846 * value)
	{
		___time_20 = value;
		Il2CppCodeGenWriteBarrier(&___time_20, value);
	}

	inline static int32_t get_offset_of_delay_21() { return static_cast<int32_t>(offsetof(iTweenScaleAdd_t289621857, ___delay_21)); }
	inline FsmFloat_t2134102846 * get_delay_21() const { return ___delay_21; }
	inline FsmFloat_t2134102846 ** get_address_of_delay_21() { return &___delay_21; }
	inline void set_delay_21(FsmFloat_t2134102846 * value)
	{
		___delay_21 = value;
		Il2CppCodeGenWriteBarrier(&___delay_21, value);
	}

	inline static int32_t get_offset_of_speed_22() { return static_cast<int32_t>(offsetof(iTweenScaleAdd_t289621857, ___speed_22)); }
	inline FsmFloat_t2134102846 * get_speed_22() const { return ___speed_22; }
	inline FsmFloat_t2134102846 ** get_address_of_speed_22() { return &___speed_22; }
	inline void set_speed_22(FsmFloat_t2134102846 * value)
	{
		___speed_22 = value;
		Il2CppCodeGenWriteBarrier(&___speed_22, value);
	}

	inline static int32_t get_offset_of_easeType_23() { return static_cast<int32_t>(offsetof(iTweenScaleAdd_t289621857, ___easeType_23)); }
	inline int32_t get_easeType_23() const { return ___easeType_23; }
	inline int32_t* get_address_of_easeType_23() { return &___easeType_23; }
	inline void set_easeType_23(int32_t value)
	{
		___easeType_23 = value;
	}

	inline static int32_t get_offset_of_loopType_24() { return static_cast<int32_t>(offsetof(iTweenScaleAdd_t289621857, ___loopType_24)); }
	inline int32_t get_loopType_24() const { return ___loopType_24; }
	inline int32_t* get_address_of_loopType_24() { return &___loopType_24; }
	inline void set_loopType_24(int32_t value)
	{
		___loopType_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
