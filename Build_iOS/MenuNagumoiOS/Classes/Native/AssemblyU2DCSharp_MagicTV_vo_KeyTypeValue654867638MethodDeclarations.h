﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.vo.KeyTypeValue
struct KeyTypeValue_t654867638;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.vo.KeyTypeValue::.ctor()
extern "C"  void KeyTypeValue__ctor_m2562148957 (KeyTypeValue_t654867638 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MagicTV.vo.KeyTypeValue::getValueFloat()
extern "C"  float KeyTypeValue_getValueFloat_m2719449480 (KeyTypeValue_t654867638 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MagicTV.vo.KeyTypeValue::getIntValue()
extern "C"  int32_t KeyTypeValue_getIntValue_m2189040097 (KeyTypeValue_t654867638 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
