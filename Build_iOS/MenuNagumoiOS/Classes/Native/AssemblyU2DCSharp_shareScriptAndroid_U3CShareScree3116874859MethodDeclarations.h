﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// shareScriptAndroid/<ShareScreenshot>c__Iterator27
struct U3CShareScreenshotU3Ec__Iterator27_t3116874859;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void shareScriptAndroid/<ShareScreenshot>c__Iterator27::.ctor()
extern "C"  void U3CShareScreenshotU3Ec__Iterator27__ctor_m1973688672 (U3CShareScreenshotU3Ec__Iterator27_t3116874859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object shareScriptAndroid/<ShareScreenshot>c__Iterator27::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CShareScreenshotU3Ec__Iterator27_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2251362610 (U3CShareScreenshotU3Ec__Iterator27_t3116874859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object shareScriptAndroid/<ShareScreenshot>c__Iterator27::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CShareScreenshotU3Ec__Iterator27_System_Collections_IEnumerator_get_Current_m3258955974 (U3CShareScreenshotU3Ec__Iterator27_t3116874859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean shareScriptAndroid/<ShareScreenshot>c__Iterator27::MoveNext()
extern "C"  bool U3CShareScreenshotU3Ec__Iterator27_MoveNext_m3656445332 (U3CShareScreenshotU3Ec__Iterator27_t3116874859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void shareScriptAndroid/<ShareScreenshot>c__Iterator27::Dispose()
extern "C"  void U3CShareScreenshotU3Ec__Iterator27_Dispose_m2532519325 (U3CShareScreenshotU3Ec__Iterator27_t3116874859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void shareScriptAndroid/<ShareScreenshot>c__Iterator27::Reset()
extern "C"  void U3CShareScreenshotU3Ec__Iterator27_Reset_m3915088909 (U3CShareScreenshotU3Ec__Iterator27_t3116874859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean shareScriptAndroid/<ShareScreenshot>c__Iterator27::<>m__8()
extern "C"  bool U3CShareScreenshotU3Ec__Iterator27_U3CU3Em__8_m779713501 (U3CShareScreenshotU3Ec__Iterator27_t3116874859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
