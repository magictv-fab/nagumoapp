﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorSpeed
struct GetAnimatorSpeed_t960447890;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorSpeed::.ctor()
extern "C"  void GetAnimatorSpeed__ctor_m4076454164 (GetAnimatorSpeed_t960447890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorSpeed::Reset()
extern "C"  void GetAnimatorSpeed_Reset_m1722887105 (GetAnimatorSpeed_t960447890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorSpeed::OnEnter()
extern "C"  void GetAnimatorSpeed_OnEnter_m4148973547 (GetAnimatorSpeed_t960447890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorSpeed::OnUpdate()
extern "C"  void GetAnimatorSpeed_OnUpdate_m3197687736 (GetAnimatorSpeed_t960447890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorSpeed::GetPlaybackSpeed()
extern "C"  void GetAnimatorSpeed_GetPlaybackSpeed_m2498397414 (GetAnimatorSpeed_t960447890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
