﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameMain
struct GameMain_t2590236907;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PointsAngle2793040272.h"

// System.Void GameMain::.ctor()
extern "C"  void GameMain__ctor_m4024677904 (GameMain_t2590236907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMain::Awake()
extern "C"  void GameMain_Awake_m4262283123 (GameMain_t2590236907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameMain::corrigeVoltaRoleta()
extern "C"  Il2CppObject * GameMain_corrigeVoltaRoleta_m3812024132 (GameMain_t2590236907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameMain::Start()
extern "C"  Il2CppObject * GameMain_Start_m3538495944 (GameMain_t2590236907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMain::BtnPlay()
extern "C"  void GameMain_BtnPlay_m2392197022 (GameMain_t2590236907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMain::PlayServer(PointsAngle)
extern "C"  void GameMain_PlayServer_m2835728055 (GameMain_t2590236907 * __this, int32_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMain::Update()
extern "C"  void GameMain_Update_m1937825533 (GameMain_t2590236907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMain::Play()
extern "C"  void GameMain_Play_m4159013736 (GameMain_t2590236907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMain::RoletaStop()
extern "C"  void GameMain_RoletaStop_m3142529465 (GameMain_t2590236907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PointsAngle GameMain::RndEnum()
extern "C"  int32_t GameMain_RndEnum_m3718882196 (GameMain_t2590236907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMain::Letreiro()
extern "C"  void GameMain_Letreiro_m2304667084 (GameMain_t2590236907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMain::Win()
extern "C"  void GameMain_Win_m1110378314 (GameMain_t2590236907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMain::Stop()
extern "C"  void GameMain_Stop_m4252697782 (GameMain_t2590236907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMain::Lose()
extern "C"  void GameMain_Lose_m4047784713 (GameMain_t2590236907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
