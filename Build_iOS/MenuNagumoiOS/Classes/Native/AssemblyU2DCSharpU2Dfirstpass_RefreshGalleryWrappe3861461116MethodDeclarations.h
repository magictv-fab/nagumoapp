﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RefreshGalleryWrapper
struct RefreshGalleryWrapper_t3861461116;

#include "codegen/il2cpp-codegen.h"

// System.Void RefreshGalleryWrapper::.ctor()
extern "C"  void RefreshGalleryWrapper__ctor_m248395323 (RefreshGalleryWrapper_t3861461116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
