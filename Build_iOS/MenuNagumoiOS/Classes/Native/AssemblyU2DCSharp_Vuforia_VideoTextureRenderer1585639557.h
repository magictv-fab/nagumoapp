﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoText4076028962.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VideoTextureRenderer
struct  VideoTextureRenderer_t1585639557  : public VideoTextureRendererAbstractBehaviour_t4076028962
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
