﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DeviceFileInfo/<populateIndexedBundles>c__AnonStorey98/<populateIndexedBundles>c__AnonStorey99
struct U3CpopulateIndexedBundlesU3Ec__AnonStorey99_t1185851167;
// MagicTV.vo.BundleVO
struct BundleVO_t1984518073;
// MagicTV.vo.MetadataVO
struct MetadataVO_t2511256998;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_vo_BundleVO1984518073.h"
#include "AssemblyU2DCSharp_MagicTV_vo_MetadataVO2511256998.h"

// System.Void DeviceFileInfo/<populateIndexedBundles>c__AnonStorey98/<populateIndexedBundles>c__AnonStorey99::.ctor()
extern "C"  void U3CpopulateIndexedBundlesU3Ec__AnonStorey99__ctor_m4178561116 (U3CpopulateIndexedBundlesU3Ec__AnonStorey99_t1185851167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DeviceFileInfo/<populateIndexedBundles>c__AnonStorey98/<populateIndexedBundles>c__AnonStorey99::<>m__35(MagicTV.vo.BundleVO)
extern "C"  bool U3CpopulateIndexedBundlesU3Ec__AnonStorey99_U3CU3Em__35_m1747513486 (U3CpopulateIndexedBundlesU3Ec__AnonStorey99_t1185851167 * __this, BundleVO_t1984518073 * ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DeviceFileInfo/<populateIndexedBundles>c__AnonStorey98/<populateIndexedBundles>c__AnonStorey99::<>m__36(MagicTV.vo.MetadataVO)
extern "C"  bool U3CpopulateIndexedBundlesU3Ec__AnonStorey99_U3CU3Em__36_m3029248416 (U3CpopulateIndexedBundlesU3Ec__AnonStorey99_t1185851167 * __this, MetadataVO_t2511256998 * ___m0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
