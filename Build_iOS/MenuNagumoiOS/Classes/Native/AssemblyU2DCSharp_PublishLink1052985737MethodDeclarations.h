﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PublishLink
struct PublishLink_t1052985737;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Action`1<System.Boolean>
struct Action_1_t872614854;

#include "codegen/il2cpp-codegen.h"

// System.Void PublishLink::.ctor()
extern "C"  void PublishLink__ctor_m2891439106 (PublishLink_t1052985737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PublishLink::Start()
extern "C"  void PublishLink_Start_m1838576898 (PublishLink_t1052985737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PublishLink::checkInternetConnection(System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * PublishLink_checkInternetConnection_m2531756452 (PublishLink_t1052985737 * __this, Action_1_t872614854 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PublishLink::<Start>m__86(System.Boolean)
extern "C"  void PublishLink_U3CStartU3Em__86_m4066187308 (PublishLink_t1052985737 * __this, bool ___isConnected0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
