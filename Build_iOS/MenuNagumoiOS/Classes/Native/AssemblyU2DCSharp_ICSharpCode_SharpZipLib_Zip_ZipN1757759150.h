﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Char[]
struct CharU5BU5D_t3324145743;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ZipNameTransform
struct  ZipNameTransform_t1757759150  : public Il2CppObject
{
public:
	// System.String ICSharpCode.SharpZipLib.Zip.ZipNameTransform::trimPrefix_
	String_t* ___trimPrefix__0;

public:
	inline static int32_t get_offset_of_trimPrefix__0() { return static_cast<int32_t>(offsetof(ZipNameTransform_t1757759150, ___trimPrefix__0)); }
	inline String_t* get_trimPrefix__0() const { return ___trimPrefix__0; }
	inline String_t** get_address_of_trimPrefix__0() { return &___trimPrefix__0; }
	inline void set_trimPrefix__0(String_t* value)
	{
		___trimPrefix__0 = value;
		Il2CppCodeGenWriteBarrier(&___trimPrefix__0, value);
	}
};

struct ZipNameTransform_t1757759150_StaticFields
{
public:
	// System.Char[] ICSharpCode.SharpZipLib.Zip.ZipNameTransform::InvalidEntryChars
	CharU5BU5D_t3324145743* ___InvalidEntryChars_1;
	// System.Char[] ICSharpCode.SharpZipLib.Zip.ZipNameTransform::InvalidEntryCharsRelaxed
	CharU5BU5D_t3324145743* ___InvalidEntryCharsRelaxed_2;

public:
	inline static int32_t get_offset_of_InvalidEntryChars_1() { return static_cast<int32_t>(offsetof(ZipNameTransform_t1757759150_StaticFields, ___InvalidEntryChars_1)); }
	inline CharU5BU5D_t3324145743* get_InvalidEntryChars_1() const { return ___InvalidEntryChars_1; }
	inline CharU5BU5D_t3324145743** get_address_of_InvalidEntryChars_1() { return &___InvalidEntryChars_1; }
	inline void set_InvalidEntryChars_1(CharU5BU5D_t3324145743* value)
	{
		___InvalidEntryChars_1 = value;
		Il2CppCodeGenWriteBarrier(&___InvalidEntryChars_1, value);
	}

	inline static int32_t get_offset_of_InvalidEntryCharsRelaxed_2() { return static_cast<int32_t>(offsetof(ZipNameTransform_t1757759150_StaticFields, ___InvalidEntryCharsRelaxed_2)); }
	inline CharU5BU5D_t3324145743* get_InvalidEntryCharsRelaxed_2() const { return ___InvalidEntryCharsRelaxed_2; }
	inline CharU5BU5D_t3324145743** get_address_of_InvalidEntryCharsRelaxed_2() { return &___InvalidEntryCharsRelaxed_2; }
	inline void set_InvalidEntryCharsRelaxed_2(CharU5BU5D_t3324145743* value)
	{
		___InvalidEntryCharsRelaxed_2 = value;
		Il2CppCodeGenWriteBarrier(&___InvalidEntryCharsRelaxed_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
