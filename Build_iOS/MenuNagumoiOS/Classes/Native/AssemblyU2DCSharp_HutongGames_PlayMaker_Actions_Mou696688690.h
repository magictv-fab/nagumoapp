﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Mo3994311403.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.MouseLook
struct  MouseLook_t696688690  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.MouseLook::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.Actions.MouseLook/RotationAxes HutongGames.PlayMaker.Actions.MouseLook::axes
	int32_t ___axes_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.MouseLook::sensitivityX
	FsmFloat_t2134102846 * ___sensitivityX_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.MouseLook::sensitivityY
	FsmFloat_t2134102846 * ___sensitivityY_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.MouseLook::minimumX
	FsmFloat_t2134102846 * ___minimumX_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.MouseLook::maximumX
	FsmFloat_t2134102846 * ___maximumX_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.MouseLook::minimumY
	FsmFloat_t2134102846 * ___minimumY_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.MouseLook::maximumY
	FsmFloat_t2134102846 * ___maximumY_16;
	// System.Boolean HutongGames.PlayMaker.Actions.MouseLook::everyFrame
	bool ___everyFrame_17;
	// System.Single HutongGames.PlayMaker.Actions.MouseLook::rotationX
	float ___rotationX_18;
	// System.Single HutongGames.PlayMaker.Actions.MouseLook::rotationY
	float ___rotationY_19;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(MouseLook_t696688690, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_axes_10() { return static_cast<int32_t>(offsetof(MouseLook_t696688690, ___axes_10)); }
	inline int32_t get_axes_10() const { return ___axes_10; }
	inline int32_t* get_address_of_axes_10() { return &___axes_10; }
	inline void set_axes_10(int32_t value)
	{
		___axes_10 = value;
	}

	inline static int32_t get_offset_of_sensitivityX_11() { return static_cast<int32_t>(offsetof(MouseLook_t696688690, ___sensitivityX_11)); }
	inline FsmFloat_t2134102846 * get_sensitivityX_11() const { return ___sensitivityX_11; }
	inline FsmFloat_t2134102846 ** get_address_of_sensitivityX_11() { return &___sensitivityX_11; }
	inline void set_sensitivityX_11(FsmFloat_t2134102846 * value)
	{
		___sensitivityX_11 = value;
		Il2CppCodeGenWriteBarrier(&___sensitivityX_11, value);
	}

	inline static int32_t get_offset_of_sensitivityY_12() { return static_cast<int32_t>(offsetof(MouseLook_t696688690, ___sensitivityY_12)); }
	inline FsmFloat_t2134102846 * get_sensitivityY_12() const { return ___sensitivityY_12; }
	inline FsmFloat_t2134102846 ** get_address_of_sensitivityY_12() { return &___sensitivityY_12; }
	inline void set_sensitivityY_12(FsmFloat_t2134102846 * value)
	{
		___sensitivityY_12 = value;
		Il2CppCodeGenWriteBarrier(&___sensitivityY_12, value);
	}

	inline static int32_t get_offset_of_minimumX_13() { return static_cast<int32_t>(offsetof(MouseLook_t696688690, ___minimumX_13)); }
	inline FsmFloat_t2134102846 * get_minimumX_13() const { return ___minimumX_13; }
	inline FsmFloat_t2134102846 ** get_address_of_minimumX_13() { return &___minimumX_13; }
	inline void set_minimumX_13(FsmFloat_t2134102846 * value)
	{
		___minimumX_13 = value;
		Il2CppCodeGenWriteBarrier(&___minimumX_13, value);
	}

	inline static int32_t get_offset_of_maximumX_14() { return static_cast<int32_t>(offsetof(MouseLook_t696688690, ___maximumX_14)); }
	inline FsmFloat_t2134102846 * get_maximumX_14() const { return ___maximumX_14; }
	inline FsmFloat_t2134102846 ** get_address_of_maximumX_14() { return &___maximumX_14; }
	inline void set_maximumX_14(FsmFloat_t2134102846 * value)
	{
		___maximumX_14 = value;
		Il2CppCodeGenWriteBarrier(&___maximumX_14, value);
	}

	inline static int32_t get_offset_of_minimumY_15() { return static_cast<int32_t>(offsetof(MouseLook_t696688690, ___minimumY_15)); }
	inline FsmFloat_t2134102846 * get_minimumY_15() const { return ___minimumY_15; }
	inline FsmFloat_t2134102846 ** get_address_of_minimumY_15() { return &___minimumY_15; }
	inline void set_minimumY_15(FsmFloat_t2134102846 * value)
	{
		___minimumY_15 = value;
		Il2CppCodeGenWriteBarrier(&___minimumY_15, value);
	}

	inline static int32_t get_offset_of_maximumY_16() { return static_cast<int32_t>(offsetof(MouseLook_t696688690, ___maximumY_16)); }
	inline FsmFloat_t2134102846 * get_maximumY_16() const { return ___maximumY_16; }
	inline FsmFloat_t2134102846 ** get_address_of_maximumY_16() { return &___maximumY_16; }
	inline void set_maximumY_16(FsmFloat_t2134102846 * value)
	{
		___maximumY_16 = value;
		Il2CppCodeGenWriteBarrier(&___maximumY_16, value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(MouseLook_t696688690, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}

	inline static int32_t get_offset_of_rotationX_18() { return static_cast<int32_t>(offsetof(MouseLook_t696688690, ___rotationX_18)); }
	inline float get_rotationX_18() const { return ___rotationX_18; }
	inline float* get_address_of_rotationX_18() { return &___rotationX_18; }
	inline void set_rotationX_18(float value)
	{
		___rotationX_18 = value;
	}

	inline static int32_t get_offset_of_rotationY_19() { return static_cast<int32_t>(offsetof(MouseLook_t696688690, ___rotationY_19)); }
	inline float get_rotationY_19() const { return ___rotationY_19; }
	inline float* get_address_of_rotationY_19() { return &___rotationY_19; }
	inline void set_rotationY_19(float value)
	{
		___rotationY_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
