﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SharerPhoto
struct SharerPhoto_t2586188127;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void SharerPhoto::.ctor()
extern "C"  void SharerPhoto__ctor_m2534571244 (SharerPhoto_t2586188127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharerPhoto::Awake()
extern "C"  void SharerPhoto_Awake_m2772176463 (SharerPhoto_t2586188127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharerPhoto::Update()
extern "C"  void SharerPhoto_Update_m2989159329 (SharerPhoto_t2586188127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharerPhoto::Start()
extern "C"  void SharerPhoto_Start_m1481709036 (SharerPhoto_t2586188127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharerPhoto::TakePicture()
extern "C"  void SharerPhoto_TakePicture_m3548536833 (SharerPhoto_t2586188127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharerPhoto::OnPostRender()
extern "C"  void SharerPhoto_OnPostRender_m170014765 (SharerPhoto_t2586188127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SharerPhoto::delay()
extern "C"  Il2CppObject * SharerPhoto_delay_m1719417141 (SharerPhoto_t2586188127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SharerPhoto::showSharePanel()
extern "C"  void SharerPhoto_showSharePanel_m2362126778 (SharerPhoto_t2586188127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
