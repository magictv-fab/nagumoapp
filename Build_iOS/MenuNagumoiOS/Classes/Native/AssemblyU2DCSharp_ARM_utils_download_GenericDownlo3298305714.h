﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// ARM.utils.download.GameObjectDownloader
struct GameObjectDownloader_t2978402602;

#include "AssemblyU2DCSharp_ARM_utils_download_DownloaderAbs3533812091.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.utils.download.GenericDownloadManager
struct  GenericDownloadManager_t3298305714  : public DownloaderAbstract_t3533812091
{
public:
	// UnityEngine.GameObject ARM.utils.download.GenericDownloadManager::_downloaderInstanceGO
	GameObject_t3674682005 * ____downloaderInstanceGO_16;
	// ARM.utils.download.GameObjectDownloader ARM.utils.download.GenericDownloadManager::_downloader
	GameObjectDownloader_t2978402602 * ____downloader_17;

public:
	inline static int32_t get_offset_of__downloaderInstanceGO_16() { return static_cast<int32_t>(offsetof(GenericDownloadManager_t3298305714, ____downloaderInstanceGO_16)); }
	inline GameObject_t3674682005 * get__downloaderInstanceGO_16() const { return ____downloaderInstanceGO_16; }
	inline GameObject_t3674682005 ** get_address_of__downloaderInstanceGO_16() { return &____downloaderInstanceGO_16; }
	inline void set__downloaderInstanceGO_16(GameObject_t3674682005 * value)
	{
		____downloaderInstanceGO_16 = value;
		Il2CppCodeGenWriteBarrier(&____downloaderInstanceGO_16, value);
	}

	inline static int32_t get_offset_of__downloader_17() { return static_cast<int32_t>(offsetof(GenericDownloadManager_t3298305714, ____downloader_17)); }
	inline GameObjectDownloader_t2978402602 * get__downloader_17() const { return ____downloader_17; }
	inline GameObjectDownloader_t2978402602 ** get_address_of__downloader_17() { return &____downloader_17; }
	inline void set__downloader_17(GameObjectDownloader_t2978402602 * value)
	{
		____downloader_17 = value;
		Il2CppCodeGenWriteBarrier(&____downloader_17, value);
	}
};

struct GenericDownloadManager_t3298305714_StaticFields
{
public:
	// System.Int32 ARM.utils.download.GenericDownloadManager::realInstanceId
	int32_t ___realInstanceId_15;

public:
	inline static int32_t get_offset_of_realInstanceId_15() { return static_cast<int32_t>(offsetof(GenericDownloadManager_t3298305714_StaticFields, ___realInstanceId_15)); }
	inline int32_t get_realInstanceId_15() const { return ___realInstanceId_15; }
	inline int32_t* get_address_of_realInstanceId_15() { return &___realInstanceId_15; }
	inline void set_realInstanceId_15(int32_t value)
	{
		___realInstanceId_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
