﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReturnConfigTest
struct ReturnConfigTest_t510487652;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void ReturnConfigTest::.ctor()
extern "C"  void ReturnConfigTest__ctor_m485583543 (ReturnConfigTest_t510487652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReturnConfigTest::Start()
extern "C"  void ReturnConfigTest_Start_m3727688631 (ReturnConfigTest_t510487652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReturnConfigTest::Update()
extern "C"  void ReturnConfigTest_Update_m3895050038 (ReturnConfigTest_t510487652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ReturnConfigTest::getVector()
extern "C"  Vector3_t4282066566  ReturnConfigTest_getVector_m4290145460 (ReturnConfigTest_t510487652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ReturnConfigTest::getFloat()
extern "C"  float ReturnConfigTest_getFloat_m3736790911 (ReturnConfigTest_t510487652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
