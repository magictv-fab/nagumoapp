﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WebCamTexture
struct WebCamTexture_t1290350902;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.WebCamDevice[]
struct WebCamDeviceU5BU5D_t3721690872;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FrontCam
struct  FrontCam_t3116790022  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.WebCamTexture FrontCam::mCamera
	WebCamTexture_t1290350902 * ___mCamera_2;
	// System.Single FrontCam::bias
	float ___bias_3;
	// UnityEngine.GameObject FrontCam::frase
	GameObject_t3674682005 * ___frase_4;
	// UnityEngine.GameObject FrontCam::btnCam
	GameObject_t3674682005 * ___btnCam_5;
	// UnityEngine.GameObject FrontCam::btnFechar
	GameObject_t3674682005 * ___btnFechar_6;
	// System.Boolean FrontCam::back
	bool ___back_7;
	// UnityEngine.WebCamDevice[] FrontCam::devices
	WebCamDeviceU5BU5D_t3721690872* ___devices_8;

public:
	inline static int32_t get_offset_of_mCamera_2() { return static_cast<int32_t>(offsetof(FrontCam_t3116790022, ___mCamera_2)); }
	inline WebCamTexture_t1290350902 * get_mCamera_2() const { return ___mCamera_2; }
	inline WebCamTexture_t1290350902 ** get_address_of_mCamera_2() { return &___mCamera_2; }
	inline void set_mCamera_2(WebCamTexture_t1290350902 * value)
	{
		___mCamera_2 = value;
		Il2CppCodeGenWriteBarrier(&___mCamera_2, value);
	}

	inline static int32_t get_offset_of_bias_3() { return static_cast<int32_t>(offsetof(FrontCam_t3116790022, ___bias_3)); }
	inline float get_bias_3() const { return ___bias_3; }
	inline float* get_address_of_bias_3() { return &___bias_3; }
	inline void set_bias_3(float value)
	{
		___bias_3 = value;
	}

	inline static int32_t get_offset_of_frase_4() { return static_cast<int32_t>(offsetof(FrontCam_t3116790022, ___frase_4)); }
	inline GameObject_t3674682005 * get_frase_4() const { return ___frase_4; }
	inline GameObject_t3674682005 ** get_address_of_frase_4() { return &___frase_4; }
	inline void set_frase_4(GameObject_t3674682005 * value)
	{
		___frase_4 = value;
		Il2CppCodeGenWriteBarrier(&___frase_4, value);
	}

	inline static int32_t get_offset_of_btnCam_5() { return static_cast<int32_t>(offsetof(FrontCam_t3116790022, ___btnCam_5)); }
	inline GameObject_t3674682005 * get_btnCam_5() const { return ___btnCam_5; }
	inline GameObject_t3674682005 ** get_address_of_btnCam_5() { return &___btnCam_5; }
	inline void set_btnCam_5(GameObject_t3674682005 * value)
	{
		___btnCam_5 = value;
		Il2CppCodeGenWriteBarrier(&___btnCam_5, value);
	}

	inline static int32_t get_offset_of_btnFechar_6() { return static_cast<int32_t>(offsetof(FrontCam_t3116790022, ___btnFechar_6)); }
	inline GameObject_t3674682005 * get_btnFechar_6() const { return ___btnFechar_6; }
	inline GameObject_t3674682005 ** get_address_of_btnFechar_6() { return &___btnFechar_6; }
	inline void set_btnFechar_6(GameObject_t3674682005 * value)
	{
		___btnFechar_6 = value;
		Il2CppCodeGenWriteBarrier(&___btnFechar_6, value);
	}

	inline static int32_t get_offset_of_back_7() { return static_cast<int32_t>(offsetof(FrontCam_t3116790022, ___back_7)); }
	inline bool get_back_7() const { return ___back_7; }
	inline bool* get_address_of_back_7() { return &___back_7; }
	inline void set_back_7(bool value)
	{
		___back_7 = value;
	}

	inline static int32_t get_offset_of_devices_8() { return static_cast<int32_t>(offsetof(FrontCam_t3116790022, ___devices_8)); }
	inline WebCamDeviceU5BU5D_t3721690872* get_devices_8() const { return ___devices_8; }
	inline WebCamDeviceU5BU5D_t3721690872** get_address_of_devices_8() { return &___devices_8; }
	inline void set_devices_8(WebCamDeviceU5BU5D_t3721690872* value)
	{
		___devices_8 = value;
		Il2CppCodeGenWriteBarrier(&___devices_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
