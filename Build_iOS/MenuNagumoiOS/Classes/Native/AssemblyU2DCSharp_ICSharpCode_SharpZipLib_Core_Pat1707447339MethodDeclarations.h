﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Core.PathFilter
struct PathFilter_t1707447339;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ICSharpCode.SharpZipLib.Core.PathFilter::.ctor(System.String)
extern "C"  void PathFilter__ctor_m2692332946 (PathFilter_t1707447339 * __this, String_t* ___filter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Core.PathFilter::IsMatch(System.String)
extern "C"  bool PathFilter_IsMatch_m2509887269 (PathFilter_t1707447339 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
