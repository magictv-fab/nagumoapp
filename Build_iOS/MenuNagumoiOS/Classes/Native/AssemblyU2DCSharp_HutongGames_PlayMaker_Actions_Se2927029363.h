﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Com700434209.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAnimationSpeed
struct  SetAnimationSpeed_t2927029363  : public ComponentAction_1_t700434209
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAnimationSpeed::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetAnimationSpeed::animName
	FsmString_t952858651 * ___animName_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetAnimationSpeed::speed
	FsmFloat_t2134102846 * ___speed_13;
	// System.Boolean HutongGames.PlayMaker.Actions.SetAnimationSpeed::everyFrame
	bool ___everyFrame_14;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(SetAnimationSpeed_t2927029363, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_animName_12() { return static_cast<int32_t>(offsetof(SetAnimationSpeed_t2927029363, ___animName_12)); }
	inline FsmString_t952858651 * get_animName_12() const { return ___animName_12; }
	inline FsmString_t952858651 ** get_address_of_animName_12() { return &___animName_12; }
	inline void set_animName_12(FsmString_t952858651 * value)
	{
		___animName_12 = value;
		Il2CppCodeGenWriteBarrier(&___animName_12, value);
	}

	inline static int32_t get_offset_of_speed_13() { return static_cast<int32_t>(offsetof(SetAnimationSpeed_t2927029363, ___speed_13)); }
	inline FsmFloat_t2134102846 * get_speed_13() const { return ___speed_13; }
	inline FsmFloat_t2134102846 ** get_address_of_speed_13() { return &___speed_13; }
	inline void set_speed_13(FsmFloat_t2134102846 * value)
	{
		___speed_13 = value;
		Il2CppCodeGenWriteBarrier(&___speed_13, value);
	}

	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(SetAnimationSpeed_t2927029363, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
