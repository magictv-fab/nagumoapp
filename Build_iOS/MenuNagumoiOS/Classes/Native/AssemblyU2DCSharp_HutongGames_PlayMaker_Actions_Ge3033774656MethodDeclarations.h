﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorIKGoal
struct GetAnimatorIKGoal_t3033774656;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::.ctor()
extern "C"  void GetAnimatorIKGoal__ctor_m1678076662 (GetAnimatorIKGoal_t3033774656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::Reset()
extern "C"  void GetAnimatorIKGoal_Reset_m3619476899 (GetAnimatorIKGoal_t3033774656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::OnEnter()
extern "C"  void GetAnimatorIKGoal_OnEnter_m1410664781 (GetAnimatorIKGoal_t3033774656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::OnUpdate()
extern "C"  void GetAnimatorIKGoal_OnUpdate_m4209461910 (GetAnimatorIKGoal_t3033774656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::DoGetIKGoal()
extern "C"  void GetAnimatorIKGoal_DoGetIKGoal_m3978612660 (GetAnimatorIKGoal_t3033774656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
