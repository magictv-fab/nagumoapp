﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.display.ColorChange
struct ColorChange_t151994789;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

// System.Void ARM.display.ColorChange::.ctor()
extern "C"  void ColorChange__ctor_m2228787222 (ColorChange_t151994789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.display.ColorChange::Start()
extern "C"  void ColorChange_Start_m1175925014 (ColorChange_t151994789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.display.ColorChange::colorToVector3(UnityEngine.Color)
extern "C"  Vector3_t4282066566  ColorChange_colorToVector3_m1753468256 (ColorChange_t151994789 * __this, Color_t4194546905  ___cor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.display.ColorChange::changeColor()
extern "C"  void ColorChange_changeColor_m3436084199 (ColorChange_t151994789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.display.ColorChange::Update()
extern "C"  void ColorChange_Update_m2099789239 (ColorChange_t151994789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
