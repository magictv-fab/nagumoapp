﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_OSNotificationPermission431010846.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSPermissionState
struct  OSPermissionState_t1777959294  : public Il2CppObject
{
public:
	// System.Boolean OSPermissionState::hasPrompted
	bool ___hasPrompted_0;
	// OSNotificationPermission OSPermissionState::status
	int32_t ___status_1;

public:
	inline static int32_t get_offset_of_hasPrompted_0() { return static_cast<int32_t>(offsetof(OSPermissionState_t1777959294, ___hasPrompted_0)); }
	inline bool get_hasPrompted_0() const { return ___hasPrompted_0; }
	inline bool* get_address_of_hasPrompted_0() { return &___hasPrompted_0; }
	inline void set_hasPrompted_0(bool value)
	{
		___hasPrompted_0 = value;
	}

	inline static int32_t get_offset_of_status_1() { return static_cast<int32_t>(offsetof(OSPermissionState_t1777959294, ___status_1)); }
	inline int32_t get_status_1() const { return ___status_1; }
	inline int32_t* get_address_of_status_1() { return &___status_1; }
	inline void set_status_1(int32_t value)
	{
		___status_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
