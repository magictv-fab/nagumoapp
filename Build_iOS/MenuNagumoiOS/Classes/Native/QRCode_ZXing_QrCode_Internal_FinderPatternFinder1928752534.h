﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// System.Collections.Generic.List`1<ZXing.QrCode.Internal.FinderPattern>
struct List_1_t1192977248;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// ZXing.ResultPointCallback
struct ResultPointCallback_t207829946;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.FinderPatternFinder
struct  FinderPatternFinder_t1928752534  : public Il2CppObject
{
public:
	// ZXing.Common.BitMatrix ZXing.QrCode.Internal.FinderPatternFinder::image
	BitMatrix_t1058711404 * ___image_0;
	// System.Collections.Generic.List`1<ZXing.QrCode.Internal.FinderPattern> ZXing.QrCode.Internal.FinderPatternFinder::possibleCenters
	List_1_t1192977248 * ___possibleCenters_1;
	// System.Boolean ZXing.QrCode.Internal.FinderPatternFinder::hasSkipped
	bool ___hasSkipped_2;
	// System.Int32[] ZXing.QrCode.Internal.FinderPatternFinder::crossCheckStateCount
	Int32U5BU5D_t3230847821* ___crossCheckStateCount_3;
	// ZXing.ResultPointCallback ZXing.QrCode.Internal.FinderPatternFinder::resultPointCallback
	ResultPointCallback_t207829946 * ___resultPointCallback_4;

public:
	inline static int32_t get_offset_of_image_0() { return static_cast<int32_t>(offsetof(FinderPatternFinder_t1928752534, ___image_0)); }
	inline BitMatrix_t1058711404 * get_image_0() const { return ___image_0; }
	inline BitMatrix_t1058711404 ** get_address_of_image_0() { return &___image_0; }
	inline void set_image_0(BitMatrix_t1058711404 * value)
	{
		___image_0 = value;
		Il2CppCodeGenWriteBarrier(&___image_0, value);
	}

	inline static int32_t get_offset_of_possibleCenters_1() { return static_cast<int32_t>(offsetof(FinderPatternFinder_t1928752534, ___possibleCenters_1)); }
	inline List_1_t1192977248 * get_possibleCenters_1() const { return ___possibleCenters_1; }
	inline List_1_t1192977248 ** get_address_of_possibleCenters_1() { return &___possibleCenters_1; }
	inline void set_possibleCenters_1(List_1_t1192977248 * value)
	{
		___possibleCenters_1 = value;
		Il2CppCodeGenWriteBarrier(&___possibleCenters_1, value);
	}

	inline static int32_t get_offset_of_hasSkipped_2() { return static_cast<int32_t>(offsetof(FinderPatternFinder_t1928752534, ___hasSkipped_2)); }
	inline bool get_hasSkipped_2() const { return ___hasSkipped_2; }
	inline bool* get_address_of_hasSkipped_2() { return &___hasSkipped_2; }
	inline void set_hasSkipped_2(bool value)
	{
		___hasSkipped_2 = value;
	}

	inline static int32_t get_offset_of_crossCheckStateCount_3() { return static_cast<int32_t>(offsetof(FinderPatternFinder_t1928752534, ___crossCheckStateCount_3)); }
	inline Int32U5BU5D_t3230847821* get_crossCheckStateCount_3() const { return ___crossCheckStateCount_3; }
	inline Int32U5BU5D_t3230847821** get_address_of_crossCheckStateCount_3() { return &___crossCheckStateCount_3; }
	inline void set_crossCheckStateCount_3(Int32U5BU5D_t3230847821* value)
	{
		___crossCheckStateCount_3 = value;
		Il2CppCodeGenWriteBarrier(&___crossCheckStateCount_3, value);
	}

	inline static int32_t get_offset_of_resultPointCallback_4() { return static_cast<int32_t>(offsetof(FinderPatternFinder_t1928752534, ___resultPointCallback_4)); }
	inline ResultPointCallback_t207829946 * get_resultPointCallback_4() const { return ___resultPointCallback_4; }
	inline ResultPointCallback_t207829946 ** get_address_of_resultPointCallback_4() { return &___resultPointCallback_4; }
	inline void set_resultPointCallback_4(ResultPointCallback_t207829946 * value)
	{
		___resultPointCallback_4 = value;
		Il2CppCodeGenWriteBarrier(&___resultPointCallback_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
