﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>
struct ChangeNotifyDictionary_2_t812725187;
// System.Action`2<System.Object,System.EventArgs>
struct Action_2_t2663079113;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<ZXing.DecodeHintType>
struct ICollection_1_t2990371336;
// System.Collections.Generic.KeyValuePair`2<ZXing.DecodeHintType,System.Object>[]
struct KeyValuePair_2U5BU5D_t3618423950;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<ZXing.DecodeHintType,System.Object>>
struct IEnumerator_1_t2992762256;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_DecodeHintType2095781349.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21080897207.h"

// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::add_ValueChanged(System.Action`2<System.Object,System.EventArgs>)
extern "C"  void ChangeNotifyDictionary_2_add_ValueChanged_m4255839784_gshared (ChangeNotifyDictionary_2_t812725187 * __this, Action_2_t2663079113 * ___value0, const MethodInfo* method);
#define ChangeNotifyDictionary_2_add_ValueChanged_m4255839784(__this, ___value0, method) ((  void (*) (ChangeNotifyDictionary_2_t812725187 *, Action_2_t2663079113 *, const MethodInfo*))ChangeNotifyDictionary_2_add_ValueChanged_m4255839784_gshared)(__this, ___value0, method)
// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::remove_ValueChanged(System.Action`2<System.Object,System.EventArgs>)
extern "C"  void ChangeNotifyDictionary_2_remove_ValueChanged_m3378750005_gshared (ChangeNotifyDictionary_2_t812725187 * __this, Action_2_t2663079113 * ___value0, const MethodInfo* method);
#define ChangeNotifyDictionary_2_remove_ValueChanged_m3378750005(__this, ___value0, method) ((  void (*) (ChangeNotifyDictionary_2_t812725187 *, Action_2_t2663079113 *, const MethodInfo*))ChangeNotifyDictionary_2_remove_ValueChanged_m3378750005_gshared)(__this, ___value0, method)
// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::.ctor()
extern "C"  void ChangeNotifyDictionary_2__ctor_m908626654_gshared (ChangeNotifyDictionary_2_t812725187 * __this, const MethodInfo* method);
#define ChangeNotifyDictionary_2__ctor_m908626654(__this, method) ((  void (*) (ChangeNotifyDictionary_2_t812725187 *, const MethodInfo*))ChangeNotifyDictionary_2__ctor_m908626654_gshared)(__this, method)
// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::OnValueChanged()
extern "C"  void ChangeNotifyDictionary_2_OnValueChanged_m3164344168_gshared (ChangeNotifyDictionary_2_t812725187 * __this, const MethodInfo* method);
#define ChangeNotifyDictionary_2_OnValueChanged_m3164344168(__this, method) ((  void (*) (ChangeNotifyDictionary_2_t812725187 *, const MethodInfo*))ChangeNotifyDictionary_2_OnValueChanged_m3164344168_gshared)(__this, method)
// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::Add(TKey,TValue)
extern "C"  void ChangeNotifyDictionary_2_Add_m294257559_gshared (ChangeNotifyDictionary_2_t812725187 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define ChangeNotifyDictionary_2_Add_m294257559(__this, ___key0, ___value1, method) ((  void (*) (ChangeNotifyDictionary_2_t812725187 *, int32_t, Il2CppObject *, const MethodInfo*))ChangeNotifyDictionary_2_Add_m294257559_gshared)(__this, ___key0, ___value1, method)
// System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::ContainsKey(TKey)
extern "C"  bool ChangeNotifyDictionary_2_ContainsKey_m3860855781_gshared (ChangeNotifyDictionary_2_t812725187 * __this, int32_t ___key0, const MethodInfo* method);
#define ChangeNotifyDictionary_2_ContainsKey_m3860855781(__this, ___key0, method) ((  bool (*) (ChangeNotifyDictionary_2_t812725187 *, int32_t, const MethodInfo*))ChangeNotifyDictionary_2_ContainsKey_m3860855781_gshared)(__this, ___key0, method)
// System.Collections.Generic.ICollection`1<TKey> ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::get_Keys()
extern "C"  Il2CppObject* ChangeNotifyDictionary_2_get_Keys_m2515074822_gshared (ChangeNotifyDictionary_2_t812725187 * __this, const MethodInfo* method);
#define ChangeNotifyDictionary_2_get_Keys_m2515074822(__this, method) ((  Il2CppObject* (*) (ChangeNotifyDictionary_2_t812725187 *, const MethodInfo*))ChangeNotifyDictionary_2_get_Keys_m2515074822_gshared)(__this, method)
// System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::Remove(TKey)
extern "C"  bool ChangeNotifyDictionary_2_Remove_m1932632555_gshared (ChangeNotifyDictionary_2_t812725187 * __this, int32_t ___key0, const MethodInfo* method);
#define ChangeNotifyDictionary_2_Remove_m1932632555(__this, ___key0, method) ((  bool (*) (ChangeNotifyDictionary_2_t812725187 *, int32_t, const MethodInfo*))ChangeNotifyDictionary_2_Remove_m1932632555_gshared)(__this, ___key0, method)
// System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool ChangeNotifyDictionary_2_TryGetValue_m548970302_gshared (ChangeNotifyDictionary_2_t812725187 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define ChangeNotifyDictionary_2_TryGetValue_m548970302(__this, ___key0, ___value1, method) ((  bool (*) (ChangeNotifyDictionary_2_t812725187 *, int32_t, Il2CppObject **, const MethodInfo*))ChangeNotifyDictionary_2_TryGetValue_m548970302_gshared)(__this, ___key0, ___value1, method)
// TValue ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * ChangeNotifyDictionary_2_get_Item_m3244924311_gshared (ChangeNotifyDictionary_2_t812725187 * __this, int32_t ___key0, const MethodInfo* method);
#define ChangeNotifyDictionary_2_get_Item_m3244924311(__this, ___key0, method) ((  Il2CppObject * (*) (ChangeNotifyDictionary_2_t812725187 *, int32_t, const MethodInfo*))ChangeNotifyDictionary_2_get_Item_m3244924311_gshared)(__this, ___key0, method)
// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::set_Item(TKey,TValue)
extern "C"  void ChangeNotifyDictionary_2_set_Item_m1672045598_gshared (ChangeNotifyDictionary_2_t812725187 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define ChangeNotifyDictionary_2_set_Item_m1672045598(__this, ___key0, ___value1, method) ((  void (*) (ChangeNotifyDictionary_2_t812725187 *, int32_t, Il2CppObject *, const MethodInfo*))ChangeNotifyDictionary_2_set_Item_m1672045598_gshared)(__this, ___key0, ___value1, method)
// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void ChangeNotifyDictionary_2_Add_m4247760494_gshared (ChangeNotifyDictionary_2_t812725187 * __this, KeyValuePair_2_t1080897207  ___item0, const MethodInfo* method);
#define ChangeNotifyDictionary_2_Add_m4247760494(__this, ___item0, method) ((  void (*) (ChangeNotifyDictionary_2_t812725187 *, KeyValuePair_2_t1080897207 , const MethodInfo*))ChangeNotifyDictionary_2_Add_m4247760494_gshared)(__this, ___item0, method)
// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::Clear()
extern "C"  void ChangeNotifyDictionary_2_Clear_m2609727241_gshared (ChangeNotifyDictionary_2_t812725187 * __this, const MethodInfo* method);
#define ChangeNotifyDictionary_2_Clear_m2609727241(__this, method) ((  void (*) (ChangeNotifyDictionary_2_t812725187 *, const MethodInfo*))ChangeNotifyDictionary_2_Clear_m2609727241_gshared)(__this, method)
// System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool ChangeNotifyDictionary_2_Contains_m695462754_gshared (ChangeNotifyDictionary_2_t812725187 * __this, KeyValuePair_2_t1080897207  ___item0, const MethodInfo* method);
#define ChangeNotifyDictionary_2_Contains_m695462754(__this, ___item0, method) ((  bool (*) (ChangeNotifyDictionary_2_t812725187 *, KeyValuePair_2_t1080897207 , const MethodInfo*))ChangeNotifyDictionary_2_Contains_m695462754_gshared)(__this, ___item0, method)
// System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void ChangeNotifyDictionary_2_CopyTo_m2855476946_gshared (ChangeNotifyDictionary_2_t812725187 * __this, KeyValuePair_2U5BU5D_t3618423950* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define ChangeNotifyDictionary_2_CopyTo_m2855476946(__this, ___array0, ___arrayIndex1, method) ((  void (*) (ChangeNotifyDictionary_2_t812725187 *, KeyValuePair_2U5BU5D_t3618423950*, int32_t, const MethodInfo*))ChangeNotifyDictionary_2_CopyTo_m2855476946_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Int32 ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::get_Count()
extern "C"  int32_t ChangeNotifyDictionary_2_get_Count_m192141488_gshared (ChangeNotifyDictionary_2_t812725187 * __this, const MethodInfo* method);
#define ChangeNotifyDictionary_2_get_Count_m192141488(__this, method) ((  int32_t (*) (ChangeNotifyDictionary_2_t812725187 *, const MethodInfo*))ChangeNotifyDictionary_2_get_Count_m192141488_gshared)(__this, method)
// System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::get_IsReadOnly()
extern "C"  bool ChangeNotifyDictionary_2_get_IsReadOnly_m1306981607_gshared (ChangeNotifyDictionary_2_t812725187 * __this, const MethodInfo* method);
#define ChangeNotifyDictionary_2_get_IsReadOnly_m1306981607(__this, method) ((  bool (*) (ChangeNotifyDictionary_2_t812725187 *, const MethodInfo*))ChangeNotifyDictionary_2_get_IsReadOnly_m1306981607_gshared)(__this, method)
// System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool ChangeNotifyDictionary_2_Remove_m1718190855_gshared (ChangeNotifyDictionary_2_t812725187 * __this, KeyValuePair_2_t1080897207  ___item0, const MethodInfo* method);
#define ChangeNotifyDictionary_2_Remove_m1718190855(__this, ___item0, method) ((  bool (*) (ChangeNotifyDictionary_2_t812725187 *, KeyValuePair_2_t1080897207 , const MethodInfo*))ChangeNotifyDictionary_2_Remove_m1718190855_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ChangeNotifyDictionary_2_GetEnumerator_m2460357617_gshared (ChangeNotifyDictionary_2_t812725187 * __this, const MethodInfo* method);
#define ChangeNotifyDictionary_2_GetEnumerator_m2460357617(__this, method) ((  Il2CppObject* (*) (ChangeNotifyDictionary_2_t812725187 *, const MethodInfo*))ChangeNotifyDictionary_2_GetEnumerator_m2460357617_gshared)(__this, method)
// System.Collections.IEnumerator ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2<ZXing.DecodeHintType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ChangeNotifyDictionary_2_System_Collections_IEnumerable_GetEnumerator_m2392211331_gshared (ChangeNotifyDictionary_2_t812725187 * __this, const MethodInfo* method);
#define ChangeNotifyDictionary_2_System_Collections_IEnumerable_GetEnumerator_m2392211331(__this, method) ((  Il2CppObject * (*) (ChangeNotifyDictionary_2_t812725187 *, const MethodInfo*))ChangeNotifyDictionary_2_System_Collections_IEnumerable_GetEnumerator_m2392211331_gshared)(__this, method)
