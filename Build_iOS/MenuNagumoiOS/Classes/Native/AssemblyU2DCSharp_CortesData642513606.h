﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CortesData
struct  CortesData_t642513606  : public Il2CppObject
{
public:
	// System.String CortesData::corte
	String_t* ___corte_0;
	// System.Int32 CortesData::id_corte
	int32_t ___id_corte_1;

public:
	inline static int32_t get_offset_of_corte_0() { return static_cast<int32_t>(offsetof(CortesData_t642513606, ___corte_0)); }
	inline String_t* get_corte_0() const { return ___corte_0; }
	inline String_t** get_address_of_corte_0() { return &___corte_0; }
	inline void set_corte_0(String_t* value)
	{
		___corte_0 = value;
		Il2CppCodeGenWriteBarrier(&___corte_0, value);
	}

	inline static int32_t get_offset_of_id_corte_1() { return static_cast<int32_t>(offsetof(CortesData_t642513606, ___id_corte_1)); }
	inline int32_t get_id_corte_1() const { return ___id_corte_1; }
	inline int32_t* get_address_of_id_corte_1() { return &___id_corte_1; }
	inline void set_id_corte_1(int32_t value)
	{
		___id_corte_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
