﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.UPCEANExtension2Support
struct  UPCEANExtension2Support_t2671369572  : public Il2CppObject
{
public:
	// System.Int32[] ZXing.OneD.UPCEANExtension2Support::decodeMiddleCounters
	Int32U5BU5D_t3230847821* ___decodeMiddleCounters_0;
	// System.Text.StringBuilder ZXing.OneD.UPCEANExtension2Support::decodeRowStringBuffer
	StringBuilder_t243639308 * ___decodeRowStringBuffer_1;

public:
	inline static int32_t get_offset_of_decodeMiddleCounters_0() { return static_cast<int32_t>(offsetof(UPCEANExtension2Support_t2671369572, ___decodeMiddleCounters_0)); }
	inline Int32U5BU5D_t3230847821* get_decodeMiddleCounters_0() const { return ___decodeMiddleCounters_0; }
	inline Int32U5BU5D_t3230847821** get_address_of_decodeMiddleCounters_0() { return &___decodeMiddleCounters_0; }
	inline void set_decodeMiddleCounters_0(Int32U5BU5D_t3230847821* value)
	{
		___decodeMiddleCounters_0 = value;
		Il2CppCodeGenWriteBarrier(&___decodeMiddleCounters_0, value);
	}

	inline static int32_t get_offset_of_decodeRowStringBuffer_1() { return static_cast<int32_t>(offsetof(UPCEANExtension2Support_t2671369572, ___decodeRowStringBuffer_1)); }
	inline StringBuilder_t243639308 * get_decodeRowStringBuffer_1() const { return ___decodeRowStringBuffer_1; }
	inline StringBuilder_t243639308 ** get_address_of_decodeRowStringBuffer_1() { return &___decodeRowStringBuffer_1; }
	inline void set_decodeRowStringBuffer_1(StringBuilder_t243639308 * value)
	{
		___decodeRowStringBuffer_1 = value;
		Il2CppCodeGenWriteBarrier(&___decodeRowStringBuffer_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
