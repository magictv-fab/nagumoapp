﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScrollRectToolInfinity
struct ScrollRectToolInfinity_t2171516145;

#include "codegen/il2cpp-codegen.h"

// System.Void ScrollRectToolInfinity::.ctor()
extern "C"  void ScrollRectToolInfinity__ctor_m1979416266 (ScrollRectToolInfinity_t2171516145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollRectToolInfinity::Start()
extern "C"  void ScrollRectToolInfinity_Start_m926554058 (ScrollRectToolInfinity_t2171516145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollRectToolInfinity::Update()
extern "C"  void ScrollRectToolInfinity_Update_m2959224195 (ScrollRectToolInfinity_t2171516145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
