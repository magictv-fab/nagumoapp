﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// MagicTV.vo.request.ServerRequestParamsUpdateVO
struct ServerRequestParamsUpdateVO_t47984066;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.vo.request.ServerRequestUpdateVO
struct  ServerRequestUpdateVO_t3510446940  : public Il2CppObject
{
public:
	// System.String MagicTV.vo.request.ServerRequestUpdateVO::jsonrpc
	String_t* ___jsonrpc_0;
	// System.String MagicTV.vo.request.ServerRequestUpdateVO::method
	String_t* ___method_1;
	// MagicTV.vo.request.ServerRequestParamsUpdateVO MagicTV.vo.request.ServerRequestUpdateVO::param
	ServerRequestParamsUpdateVO_t47984066 * ___param_2;
	// System.Int32 MagicTV.vo.request.ServerRequestUpdateVO::id
	int32_t ___id_3;

public:
	inline static int32_t get_offset_of_jsonrpc_0() { return static_cast<int32_t>(offsetof(ServerRequestUpdateVO_t3510446940, ___jsonrpc_0)); }
	inline String_t* get_jsonrpc_0() const { return ___jsonrpc_0; }
	inline String_t** get_address_of_jsonrpc_0() { return &___jsonrpc_0; }
	inline void set_jsonrpc_0(String_t* value)
	{
		___jsonrpc_0 = value;
		Il2CppCodeGenWriteBarrier(&___jsonrpc_0, value);
	}

	inline static int32_t get_offset_of_method_1() { return static_cast<int32_t>(offsetof(ServerRequestUpdateVO_t3510446940, ___method_1)); }
	inline String_t* get_method_1() const { return ___method_1; }
	inline String_t** get_address_of_method_1() { return &___method_1; }
	inline void set_method_1(String_t* value)
	{
		___method_1 = value;
		Il2CppCodeGenWriteBarrier(&___method_1, value);
	}

	inline static int32_t get_offset_of_param_2() { return static_cast<int32_t>(offsetof(ServerRequestUpdateVO_t3510446940, ___param_2)); }
	inline ServerRequestParamsUpdateVO_t47984066 * get_param_2() const { return ___param_2; }
	inline ServerRequestParamsUpdateVO_t47984066 ** get_address_of_param_2() { return &___param_2; }
	inline void set_param_2(ServerRequestParamsUpdateVO_t47984066 * value)
	{
		___param_2 = value;
		Il2CppCodeGenWriteBarrier(&___param_2, value);
	}

	inline static int32_t get_offset_of_id_3() { return static_cast<int32_t>(offsetof(ServerRequestUpdateVO_t3510446940, ___id_3)); }
	inline int32_t get_id_3() const { return ___id_3; }
	inline int32_t* get_address_of_id_3() { return &___id_3; }
	inline void set_id_3(int32_t value)
	{
		___id_3 = value;
	}
};

struct ServerRequestUpdateVO_t3510446940_StaticFields
{
public:
	// System.Int32 MagicTV.vo.request.ServerRequestUpdateVO::_id
	int32_t ____id_4;

public:
	inline static int32_t get_offset_of__id_4() { return static_cast<int32_t>(offsetof(ServerRequestUpdateVO_t3510446940_StaticFields, ____id_4)); }
	inline int32_t get__id_4() const { return ____id_4; }
	inline int32_t* get_address_of__id_4() { return &____id_4; }
	inline void set__id_4(int32_t value)
	{
		____id_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
