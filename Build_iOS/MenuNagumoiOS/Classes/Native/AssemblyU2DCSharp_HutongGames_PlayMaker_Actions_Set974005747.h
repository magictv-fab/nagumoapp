﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetGUIAlpha
struct  SetGUIAlpha_t974005747  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetGUIAlpha::alpha
	FsmFloat_t2134102846 * ___alpha_9;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetGUIAlpha::applyGlobally
	FsmBool_t1075959796 * ___applyGlobally_10;

public:
	inline static int32_t get_offset_of_alpha_9() { return static_cast<int32_t>(offsetof(SetGUIAlpha_t974005747, ___alpha_9)); }
	inline FsmFloat_t2134102846 * get_alpha_9() const { return ___alpha_9; }
	inline FsmFloat_t2134102846 ** get_address_of_alpha_9() { return &___alpha_9; }
	inline void set_alpha_9(FsmFloat_t2134102846 * value)
	{
		___alpha_9 = value;
		Il2CppCodeGenWriteBarrier(&___alpha_9, value);
	}

	inline static int32_t get_offset_of_applyGlobally_10() { return static_cast<int32_t>(offsetof(SetGUIAlpha_t974005747, ___applyGlobally_10)); }
	inline FsmBool_t1075959796 * get_applyGlobally_10() const { return ___applyGlobally_10; }
	inline FsmBool_t1075959796 ** get_address_of_applyGlobally_10() { return &___applyGlobally_10; }
	inline void set_applyGlobally_10(FsmBool_t1075959796 * value)
	{
		___applyGlobally_10 = value;
		Il2CppCodeGenWriteBarrier(&___applyGlobally_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
