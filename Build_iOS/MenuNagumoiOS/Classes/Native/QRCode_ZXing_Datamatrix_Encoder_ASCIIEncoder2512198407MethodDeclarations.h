﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Datamatrix.Encoder.ASCIIEncoder
struct ASCIIEncoder_t2512198407;
// ZXing.Datamatrix.Encoder.EncoderContext
struct EncoderContext_t1774722223;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Datamatrix_Encoder_EncoderContext1774722223.h"

// System.Int32 ZXing.Datamatrix.Encoder.ASCIIEncoder::get_EncodingMode()
extern "C"  int32_t ASCIIEncoder_get_EncodingMode_m350516480 (ASCIIEncoder_t2512198407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.ASCIIEncoder::encode(ZXing.Datamatrix.Encoder.EncoderContext)
extern "C"  void ASCIIEncoder_encode_m3858063407 (ASCIIEncoder_t2512198407 * __this, EncoderContext_t1774722223 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char ZXing.Datamatrix.Encoder.ASCIIEncoder::encodeASCIIDigits(System.Char,System.Char)
extern "C"  Il2CppChar ASCIIEncoder_encodeASCIIDigits_m288414178 (Il2CppObject * __this /* static, unused */, Il2CppChar ___digit10, Il2CppChar ___digit21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.ASCIIEncoder::.ctor()
extern "C"  void ASCIIEncoder__ctor_m866220341 (ASCIIEncoder_t2512198407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
