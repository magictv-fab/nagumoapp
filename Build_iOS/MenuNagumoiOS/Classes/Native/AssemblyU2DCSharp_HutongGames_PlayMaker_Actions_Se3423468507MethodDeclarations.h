﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetMaterialFloat
struct SetMaterialFloat_t3423468507;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetMaterialFloat::.ctor()
extern "C"  void SetMaterialFloat__ctor_m2269711595 (SetMaterialFloat_t3423468507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMaterialFloat::Reset()
extern "C"  void SetMaterialFloat_Reset_m4211111832 (SetMaterialFloat_t3423468507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMaterialFloat::OnEnter()
extern "C"  void SetMaterialFloat_OnEnter_m3036152322 (SetMaterialFloat_t3423468507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMaterialFloat::OnUpdate()
extern "C"  void SetMaterialFloat_OnUpdate_m3059968129 (SetMaterialFloat_t3423468507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMaterialFloat::DoSetMaterialFloat()
extern "C"  void SetMaterialFloat_DoSetMaterialFloat_m488091735 (SetMaterialFloat_t3423468507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
