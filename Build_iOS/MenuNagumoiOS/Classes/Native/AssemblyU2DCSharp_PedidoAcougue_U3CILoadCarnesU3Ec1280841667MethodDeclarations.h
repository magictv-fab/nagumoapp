﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PedidoAcougue/<ILoadCarnes>c__Iterator6F
struct U3CILoadCarnesU3Ec__Iterator6F_t1280841667;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PedidoAcougue/<ILoadCarnes>c__Iterator6F::.ctor()
extern "C"  void U3CILoadCarnesU3Ec__Iterator6F__ctor_m1109742136 (U3CILoadCarnesU3Ec__Iterator6F_t1280841667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PedidoAcougue/<ILoadCarnes>c__Iterator6F::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CILoadCarnesU3Ec__Iterator6F_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1586296228 (U3CILoadCarnesU3Ec__Iterator6F_t1280841667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PedidoAcougue/<ILoadCarnes>c__Iterator6F::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CILoadCarnesU3Ec__Iterator6F_System_Collections_IEnumerator_get_Current_m3456549176 (U3CILoadCarnesU3Ec__Iterator6F_t1280841667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PedidoAcougue/<ILoadCarnes>c__Iterator6F::MoveNext()
extern "C"  bool U3CILoadCarnesU3Ec__Iterator6F_MoveNext_m629626020 (U3CILoadCarnesU3Ec__Iterator6F_t1280841667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PedidoAcougue/<ILoadCarnes>c__Iterator6F::Dispose()
extern "C"  void U3CILoadCarnesU3Ec__Iterator6F_Dispose_m1208586357 (U3CILoadCarnesU3Ec__Iterator6F_t1280841667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PedidoAcougue/<ILoadCarnes>c__Iterator6F::Reset()
extern "C"  void U3CILoadCarnesU3Ec__Iterator6F_Reset_m3051142373 (U3CILoadCarnesU3Ec__Iterator6F_t1280841667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
