﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Common.DecodingOptions
struct DecodingOptions_t3608870897;
// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>
struct IDictionary_2_t759989846;
// System.Action`2<System.Object,System.EventArgs>
struct Action_2_t2663079113;
// System.Object
struct Il2CppObject;
// System.EventArgs
struct EventArgs_t2540831021;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_EventArgs2540831021.h"

// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object> ZXing.Common.DecodingOptions::get_Hints()
extern "C"  Il2CppObject* DecodingOptions_get_Hints_m1725823998 (DecodingOptions_t3608870897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.DecodingOptions::set_Hints(System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  void DecodingOptions_set_Hints_m1793752853 (DecodingOptions_t3608870897 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.DecodingOptions::add_ValueChanged(System.Action`2<System.Object,System.EventArgs>)
extern "C"  void DecodingOptions_add_ValueChanged_m2818077131 (DecodingOptions_t3608870897 * __this, Action_2_t2663079113 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.DecodingOptions::remove_ValueChanged(System.Action`2<System.Object,System.EventArgs>)
extern "C"  void DecodingOptions_remove_ValueChanged_m405430194 (DecodingOptions_t3608870897 * __this, Action_2_t2663079113 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.DecodingOptions::set_UseCode39ExtendedMode(System.Boolean)
extern "C"  void DecodingOptions_set_UseCode39ExtendedMode_m2948443279 (DecodingOptions_t3608870897 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.DecodingOptions::set_UseCode39RelaxedExtendedMode(System.Boolean)
extern "C"  void DecodingOptions_set_UseCode39RelaxedExtendedMode_m3746816072 (DecodingOptions_t3608870897 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.DecodingOptions::.ctor()
extern "C"  void DecodingOptions__ctor_m1101142081 (DecodingOptions_t3608870897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.DecodingOptions::<.ctor>b__0(System.Object,System.EventArgs)
extern "C"  void DecodingOptions_U3C_ctorU3Eb__0_m3975830719 (DecodingOptions_t3608870897 * __this, Il2CppObject * ___o0, EventArgs_t2540831021 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
