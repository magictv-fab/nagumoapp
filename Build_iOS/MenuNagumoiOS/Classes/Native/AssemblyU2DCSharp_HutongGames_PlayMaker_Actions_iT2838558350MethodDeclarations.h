﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenShakeScale
struct iTweenShakeScale_t2838558350;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenShakeScale::.ctor()
extern "C"  void iTweenShakeScale__ctor_m3627641752 (iTweenShakeScale_t2838558350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenShakeScale::Reset()
extern "C"  void iTweenShakeScale_Reset_m1274074693 (iTweenShakeScale_t2838558350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenShakeScale::OnEnter()
extern "C"  void iTweenShakeScale_OnEnter_m2336975215 (iTweenShakeScale_t2838558350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenShakeScale::OnExit()
extern "C"  void iTweenShakeScale_OnExit_m2301063241 (iTweenShakeScale_t2838558350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenShakeScale::DoiTween()
extern "C"  void iTweenShakeScale_DoiTween_m882919865 (iTweenShakeScale_t2838558350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
