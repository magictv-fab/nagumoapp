﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.vo.result.ConfirmResultVO
struct  ConfirmResultVO_t1731700412  : public Il2CppObject
{
public:
	// System.String MagicTV.vo.result.ConfirmResultVO::id
	String_t* ___id_1;
	// System.Boolean MagicTV.vo.result.ConfirmResultVO::confirm
	bool ___confirm_2;
	// System.String MagicTV.vo.result.ConfirmResultVO::message
	String_t* ___message_3;

public:
	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(ConfirmResultVO_t1731700412, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier(&___id_1, value);
	}

	inline static int32_t get_offset_of_confirm_2() { return static_cast<int32_t>(offsetof(ConfirmResultVO_t1731700412, ___confirm_2)); }
	inline bool get_confirm_2() const { return ___confirm_2; }
	inline bool* get_address_of_confirm_2() { return &___confirm_2; }
	inline void set_confirm_2(bool value)
	{
		___confirm_2 = value;
	}

	inline static int32_t get_offset_of_message_3() { return static_cast<int32_t>(offsetof(ConfirmResultVO_t1731700412, ___message_3)); }
	inline String_t* get_message_3() const { return ___message_3; }
	inline String_t** get_address_of_message_3() { return &___message_3; }
	inline void set_message_3(String_t* value)
	{
		___message_3 = value;
		Il2CppCodeGenWriteBarrier(&___message_3, value);
	}
};

struct ConfirmResultVO_t1731700412_StaticFields
{
public:
	// System.Int32 MagicTV.vo.result.ConfirmResultVO::_autoId
	int32_t ____autoId_0;

public:
	inline static int32_t get_offset_of__autoId_0() { return static_cast<int32_t>(offsetof(ConfirmResultVO_t1731700412_StaticFields, ____autoId_0)); }
	inline int32_t get__autoId_0() const { return ____autoId_0; }
	inline int32_t* get_address_of__autoId_0() { return &____autoId_0; }
	inline void set__autoId_0(int32_t value)
	{
		____autoId_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
