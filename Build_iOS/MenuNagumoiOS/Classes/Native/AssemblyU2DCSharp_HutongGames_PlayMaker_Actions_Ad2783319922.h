﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co2322045418.h"
#include "UnityEngine_UnityEngine_Space4209342076.h"
#include "UnityEngine_UnityEngine_ForceMode2134283300.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AddForce
struct  AddForce_t2783319922  : public ComponentAction_1_t2322045418
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AddForce::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.AddForce::atPosition
	FsmVector3_t533912882 * ___atPosition_12;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.AddForce::vector
	FsmVector3_t533912882 * ___vector_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AddForce::x
	FsmFloat_t2134102846 * ___x_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AddForce::y
	FsmFloat_t2134102846 * ___y_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AddForce::z
	FsmFloat_t2134102846 * ___z_16;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.AddForce::space
	int32_t ___space_17;
	// UnityEngine.ForceMode HutongGames.PlayMaker.Actions.AddForce::forceMode
	int32_t ___forceMode_18;
	// System.Boolean HutongGames.PlayMaker.Actions.AddForce::everyFrame
	bool ___everyFrame_19;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(AddForce_t2783319922, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_atPosition_12() { return static_cast<int32_t>(offsetof(AddForce_t2783319922, ___atPosition_12)); }
	inline FsmVector3_t533912882 * get_atPosition_12() const { return ___atPosition_12; }
	inline FsmVector3_t533912882 ** get_address_of_atPosition_12() { return &___atPosition_12; }
	inline void set_atPosition_12(FsmVector3_t533912882 * value)
	{
		___atPosition_12 = value;
		Il2CppCodeGenWriteBarrier(&___atPosition_12, value);
	}

	inline static int32_t get_offset_of_vector_13() { return static_cast<int32_t>(offsetof(AddForce_t2783319922, ___vector_13)); }
	inline FsmVector3_t533912882 * get_vector_13() const { return ___vector_13; }
	inline FsmVector3_t533912882 ** get_address_of_vector_13() { return &___vector_13; }
	inline void set_vector_13(FsmVector3_t533912882 * value)
	{
		___vector_13 = value;
		Il2CppCodeGenWriteBarrier(&___vector_13, value);
	}

	inline static int32_t get_offset_of_x_14() { return static_cast<int32_t>(offsetof(AddForce_t2783319922, ___x_14)); }
	inline FsmFloat_t2134102846 * get_x_14() const { return ___x_14; }
	inline FsmFloat_t2134102846 ** get_address_of_x_14() { return &___x_14; }
	inline void set_x_14(FsmFloat_t2134102846 * value)
	{
		___x_14 = value;
		Il2CppCodeGenWriteBarrier(&___x_14, value);
	}

	inline static int32_t get_offset_of_y_15() { return static_cast<int32_t>(offsetof(AddForce_t2783319922, ___y_15)); }
	inline FsmFloat_t2134102846 * get_y_15() const { return ___y_15; }
	inline FsmFloat_t2134102846 ** get_address_of_y_15() { return &___y_15; }
	inline void set_y_15(FsmFloat_t2134102846 * value)
	{
		___y_15 = value;
		Il2CppCodeGenWriteBarrier(&___y_15, value);
	}

	inline static int32_t get_offset_of_z_16() { return static_cast<int32_t>(offsetof(AddForce_t2783319922, ___z_16)); }
	inline FsmFloat_t2134102846 * get_z_16() const { return ___z_16; }
	inline FsmFloat_t2134102846 ** get_address_of_z_16() { return &___z_16; }
	inline void set_z_16(FsmFloat_t2134102846 * value)
	{
		___z_16 = value;
		Il2CppCodeGenWriteBarrier(&___z_16, value);
	}

	inline static int32_t get_offset_of_space_17() { return static_cast<int32_t>(offsetof(AddForce_t2783319922, ___space_17)); }
	inline int32_t get_space_17() const { return ___space_17; }
	inline int32_t* get_address_of_space_17() { return &___space_17; }
	inline void set_space_17(int32_t value)
	{
		___space_17 = value;
	}

	inline static int32_t get_offset_of_forceMode_18() { return static_cast<int32_t>(offsetof(AddForce_t2783319922, ___forceMode_18)); }
	inline int32_t get_forceMode_18() const { return ___forceMode_18; }
	inline int32_t* get_address_of_forceMode_18() { return &___forceMode_18; }
	inline void set_forceMode_18(int32_t value)
	{
		___forceMode_18 = value;
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(AddForce_t2783319922, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
