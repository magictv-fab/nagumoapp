﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.QrCode.Internal.DataMask/DataMask110
struct DataMask110_t1996579292;

#include "codegen/il2cpp-codegen.h"

// System.Boolean ZXing.QrCode.Internal.DataMask/DataMask110::isMasked(System.Int32,System.Int32)
extern "C"  bool DataMask110_isMasked_m1013268600 (DataMask110_t1996579292 * __this, int32_t ___i0, int32_t ___j1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.DataMask/DataMask110::.ctor()
extern "C"  void DataMask110__ctor_m1418357471 (DataMask110_t1996579292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
