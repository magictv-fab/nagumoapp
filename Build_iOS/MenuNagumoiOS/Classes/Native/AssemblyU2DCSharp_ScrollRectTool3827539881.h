﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ScrollRectTool
struct ScrollRectTool_t3827539881;
// ScrollRectTool/SelectChildren
struct SelectChildren_t1389527649;
// UnityEngine.UI.GridLayoutGroup
struct GridLayoutGroup_t169317941;
// UnityEngine.RectTransform
struct RectTransform_t972643934;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t3606982749;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScrollRectTool
struct  ScrollRectTool_t3827539881  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean ScrollRectTool::changeColorScale
	bool ___changeColorScale_3;
	// System.Single ScrollRectTool::snapMinVel
	float ___snapMinVel_4;
	// System.Single ScrollRectTool::rotMultiplayer
	float ___rotMultiplayer_5;
	// ScrollRectTool/SelectChildren ScrollRectTool::selectChildren
	SelectChildren_t1389527649 * ___selectChildren_6;
	// System.Single ScrollRectTool::scaleFront
	float ___scaleFront_7;
	// System.Single ScrollRectTool::scaleBack
	float ___scaleBack_8;
	// System.Int32 ScrollRectTool::sideCells
	int32_t ___sideCells_9;
	// System.Boolean ScrollRectTool::center
	bool ___center_10;
	// System.Int32 ScrollRectTool::backCells
	int32_t ___backCells_11;
	// UnityEngine.UI.GridLayoutGroup ScrollRectTool::grid
	GridLayoutGroup_t169317941 * ___grid_12;
	// UnityEngine.RectTransform ScrollRectTool::rect
	RectTransform_t972643934 * ___rect_13;
	// UnityEngine.UI.ScrollRect ScrollRectTool::scrollRect
	ScrollRect_t3606982749 * ___scrollRect_14;
	// UnityEngine.Vector2 ScrollRectTool::targetPos
	Vector2_t4282066565  ___targetPos_15;
	// System.Boolean ScrollRectTool::done
	bool ___done_16;
	// System.Single ScrollRectTool::t
	float ___t_17;
	// System.Single ScrollRectTool::desaceleration
	float ___desaceleration_18;
	// UnityEngine.Transform ScrollRectTool::tmpChild
	Transform_t1659122786 * ___tmpChild_19;
	// System.Single ScrollRectTool::fixCelSize
	float ___fixCelSize_20;
	// System.Single ScrollRectTool::fixPair
	float ___fixPair_21;

public:
	inline static int32_t get_offset_of_changeColorScale_3() { return static_cast<int32_t>(offsetof(ScrollRectTool_t3827539881, ___changeColorScale_3)); }
	inline bool get_changeColorScale_3() const { return ___changeColorScale_3; }
	inline bool* get_address_of_changeColorScale_3() { return &___changeColorScale_3; }
	inline void set_changeColorScale_3(bool value)
	{
		___changeColorScale_3 = value;
	}

	inline static int32_t get_offset_of_snapMinVel_4() { return static_cast<int32_t>(offsetof(ScrollRectTool_t3827539881, ___snapMinVel_4)); }
	inline float get_snapMinVel_4() const { return ___snapMinVel_4; }
	inline float* get_address_of_snapMinVel_4() { return &___snapMinVel_4; }
	inline void set_snapMinVel_4(float value)
	{
		___snapMinVel_4 = value;
	}

	inline static int32_t get_offset_of_rotMultiplayer_5() { return static_cast<int32_t>(offsetof(ScrollRectTool_t3827539881, ___rotMultiplayer_5)); }
	inline float get_rotMultiplayer_5() const { return ___rotMultiplayer_5; }
	inline float* get_address_of_rotMultiplayer_5() { return &___rotMultiplayer_5; }
	inline void set_rotMultiplayer_5(float value)
	{
		___rotMultiplayer_5 = value;
	}

	inline static int32_t get_offset_of_selectChildren_6() { return static_cast<int32_t>(offsetof(ScrollRectTool_t3827539881, ___selectChildren_6)); }
	inline SelectChildren_t1389527649 * get_selectChildren_6() const { return ___selectChildren_6; }
	inline SelectChildren_t1389527649 ** get_address_of_selectChildren_6() { return &___selectChildren_6; }
	inline void set_selectChildren_6(SelectChildren_t1389527649 * value)
	{
		___selectChildren_6 = value;
		Il2CppCodeGenWriteBarrier(&___selectChildren_6, value);
	}

	inline static int32_t get_offset_of_scaleFront_7() { return static_cast<int32_t>(offsetof(ScrollRectTool_t3827539881, ___scaleFront_7)); }
	inline float get_scaleFront_7() const { return ___scaleFront_7; }
	inline float* get_address_of_scaleFront_7() { return &___scaleFront_7; }
	inline void set_scaleFront_7(float value)
	{
		___scaleFront_7 = value;
	}

	inline static int32_t get_offset_of_scaleBack_8() { return static_cast<int32_t>(offsetof(ScrollRectTool_t3827539881, ___scaleBack_8)); }
	inline float get_scaleBack_8() const { return ___scaleBack_8; }
	inline float* get_address_of_scaleBack_8() { return &___scaleBack_8; }
	inline void set_scaleBack_8(float value)
	{
		___scaleBack_8 = value;
	}

	inline static int32_t get_offset_of_sideCells_9() { return static_cast<int32_t>(offsetof(ScrollRectTool_t3827539881, ___sideCells_9)); }
	inline int32_t get_sideCells_9() const { return ___sideCells_9; }
	inline int32_t* get_address_of_sideCells_9() { return &___sideCells_9; }
	inline void set_sideCells_9(int32_t value)
	{
		___sideCells_9 = value;
	}

	inline static int32_t get_offset_of_center_10() { return static_cast<int32_t>(offsetof(ScrollRectTool_t3827539881, ___center_10)); }
	inline bool get_center_10() const { return ___center_10; }
	inline bool* get_address_of_center_10() { return &___center_10; }
	inline void set_center_10(bool value)
	{
		___center_10 = value;
	}

	inline static int32_t get_offset_of_backCells_11() { return static_cast<int32_t>(offsetof(ScrollRectTool_t3827539881, ___backCells_11)); }
	inline int32_t get_backCells_11() const { return ___backCells_11; }
	inline int32_t* get_address_of_backCells_11() { return &___backCells_11; }
	inline void set_backCells_11(int32_t value)
	{
		___backCells_11 = value;
	}

	inline static int32_t get_offset_of_grid_12() { return static_cast<int32_t>(offsetof(ScrollRectTool_t3827539881, ___grid_12)); }
	inline GridLayoutGroup_t169317941 * get_grid_12() const { return ___grid_12; }
	inline GridLayoutGroup_t169317941 ** get_address_of_grid_12() { return &___grid_12; }
	inline void set_grid_12(GridLayoutGroup_t169317941 * value)
	{
		___grid_12 = value;
		Il2CppCodeGenWriteBarrier(&___grid_12, value);
	}

	inline static int32_t get_offset_of_rect_13() { return static_cast<int32_t>(offsetof(ScrollRectTool_t3827539881, ___rect_13)); }
	inline RectTransform_t972643934 * get_rect_13() const { return ___rect_13; }
	inline RectTransform_t972643934 ** get_address_of_rect_13() { return &___rect_13; }
	inline void set_rect_13(RectTransform_t972643934 * value)
	{
		___rect_13 = value;
		Il2CppCodeGenWriteBarrier(&___rect_13, value);
	}

	inline static int32_t get_offset_of_scrollRect_14() { return static_cast<int32_t>(offsetof(ScrollRectTool_t3827539881, ___scrollRect_14)); }
	inline ScrollRect_t3606982749 * get_scrollRect_14() const { return ___scrollRect_14; }
	inline ScrollRect_t3606982749 ** get_address_of_scrollRect_14() { return &___scrollRect_14; }
	inline void set_scrollRect_14(ScrollRect_t3606982749 * value)
	{
		___scrollRect_14 = value;
		Il2CppCodeGenWriteBarrier(&___scrollRect_14, value);
	}

	inline static int32_t get_offset_of_targetPos_15() { return static_cast<int32_t>(offsetof(ScrollRectTool_t3827539881, ___targetPos_15)); }
	inline Vector2_t4282066565  get_targetPos_15() const { return ___targetPos_15; }
	inline Vector2_t4282066565 * get_address_of_targetPos_15() { return &___targetPos_15; }
	inline void set_targetPos_15(Vector2_t4282066565  value)
	{
		___targetPos_15 = value;
	}

	inline static int32_t get_offset_of_done_16() { return static_cast<int32_t>(offsetof(ScrollRectTool_t3827539881, ___done_16)); }
	inline bool get_done_16() const { return ___done_16; }
	inline bool* get_address_of_done_16() { return &___done_16; }
	inline void set_done_16(bool value)
	{
		___done_16 = value;
	}

	inline static int32_t get_offset_of_t_17() { return static_cast<int32_t>(offsetof(ScrollRectTool_t3827539881, ___t_17)); }
	inline float get_t_17() const { return ___t_17; }
	inline float* get_address_of_t_17() { return &___t_17; }
	inline void set_t_17(float value)
	{
		___t_17 = value;
	}

	inline static int32_t get_offset_of_desaceleration_18() { return static_cast<int32_t>(offsetof(ScrollRectTool_t3827539881, ___desaceleration_18)); }
	inline float get_desaceleration_18() const { return ___desaceleration_18; }
	inline float* get_address_of_desaceleration_18() { return &___desaceleration_18; }
	inline void set_desaceleration_18(float value)
	{
		___desaceleration_18 = value;
	}

	inline static int32_t get_offset_of_tmpChild_19() { return static_cast<int32_t>(offsetof(ScrollRectTool_t3827539881, ___tmpChild_19)); }
	inline Transform_t1659122786 * get_tmpChild_19() const { return ___tmpChild_19; }
	inline Transform_t1659122786 ** get_address_of_tmpChild_19() { return &___tmpChild_19; }
	inline void set_tmpChild_19(Transform_t1659122786 * value)
	{
		___tmpChild_19 = value;
		Il2CppCodeGenWriteBarrier(&___tmpChild_19, value);
	}

	inline static int32_t get_offset_of_fixCelSize_20() { return static_cast<int32_t>(offsetof(ScrollRectTool_t3827539881, ___fixCelSize_20)); }
	inline float get_fixCelSize_20() const { return ___fixCelSize_20; }
	inline float* get_address_of_fixCelSize_20() { return &___fixCelSize_20; }
	inline void set_fixCelSize_20(float value)
	{
		___fixCelSize_20 = value;
	}

	inline static int32_t get_offset_of_fixPair_21() { return static_cast<int32_t>(offsetof(ScrollRectTool_t3827539881, ___fixPair_21)); }
	inline float get_fixPair_21() const { return ___fixPair_21; }
	inline float* get_address_of_fixPair_21() { return &___fixPair_21; }
	inline void set_fixPair_21(float value)
	{
		___fixPair_21 = value;
	}
};

struct ScrollRectTool_t3827539881_StaticFields
{
public:
	// ScrollRectTool ScrollRectTool::instance
	ScrollRectTool_t3827539881 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(ScrollRectTool_t3827539881_StaticFields, ___instance_2)); }
	inline ScrollRectTool_t3827539881 * get_instance_2() const { return ___instance_2; }
	inline ScrollRectTool_t3827539881 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(ScrollRectTool_t3827539881 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
