﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2662109048;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Demo
struct  Demo_t2126339  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject[] Demo::effects
	GameObjectU5BU5D_t2662109048* ___effects_2;
	// System.Int32 Demo::selected
	int32_t ___selected_3;
	// UnityEngine.GameObject Demo::instance
	GameObject_t3674682005 * ___instance_4;
	// System.Boolean Demo::loopedEffect
	bool ___loopedEffect_5;

public:
	inline static int32_t get_offset_of_effects_2() { return static_cast<int32_t>(offsetof(Demo_t2126339, ___effects_2)); }
	inline GameObjectU5BU5D_t2662109048* get_effects_2() const { return ___effects_2; }
	inline GameObjectU5BU5D_t2662109048** get_address_of_effects_2() { return &___effects_2; }
	inline void set_effects_2(GameObjectU5BU5D_t2662109048* value)
	{
		___effects_2 = value;
		Il2CppCodeGenWriteBarrier(&___effects_2, value);
	}

	inline static int32_t get_offset_of_selected_3() { return static_cast<int32_t>(offsetof(Demo_t2126339, ___selected_3)); }
	inline int32_t get_selected_3() const { return ___selected_3; }
	inline int32_t* get_address_of_selected_3() { return &___selected_3; }
	inline void set_selected_3(int32_t value)
	{
		___selected_3 = value;
	}

	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(Demo_t2126339, ___instance_4)); }
	inline GameObject_t3674682005 * get_instance_4() const { return ___instance_4; }
	inline GameObject_t3674682005 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(GameObject_t3674682005 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier(&___instance_4, value);
	}

	inline static int32_t get_offset_of_loopedEffect_5() { return static_cast<int32_t>(offsetof(Demo_t2126339, ___loopedEffect_5)); }
	inline bool get_loopedEffect_5() const { return ___loopedEffect_5; }
	inline bool* get_address_of_loopedEffect_5() { return &___loopedEffect_5; }
	inline void set_loopedEffect_5(bool value)
	{
		___loopedEffect_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
