﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t538875265;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t1266085011;
// UnityEngine.RectTransform
struct RectTransform_t972643934;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t3606982749;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_iTween_EaseType2734598229.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemMoveDrag
struct  ItemMoveDrag_t687865080  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Vector3 ItemMoveDrag::velocity
	Vector3_t4282066566  ___velocity_2;
	// System.Single ItemMoveDrag::minVelToSelect
	float ___minVelToSelect_3;
	// System.Single ItemMoveDrag::rotFactor
	float ___rotFactor_4;
	// iTween/EaseType ItemMoveDrag::easetype
	int32_t ___easetype_5;
	// System.Single ItemMoveDrag::easetypeTime
	float ___easetypeTime_6;
	// UnityEngine.UI.Image ItemMoveDrag::allowImage
	Image_t538875265 * ___allowImage_7;
	// UnityEngine.UI.Image ItemMoveDrag::denyImage
	Image_t538875265 * ___denyImage_8;
	// UnityEngine.Events.UnityEvent ItemMoveDrag::onAllow
	UnityEvent_t1266085011 * ___onAllow_9;
	// UnityEngine.Events.UnityEvent ItemMoveDrag::onDeny
	UnityEvent_t1266085011 * ___onDeny_10;
	// UnityEngine.Events.UnityEvent ItemMoveDrag::onSelected
	UnityEvent_t1266085011 * ___onSelected_11;
	// UnityEngine.RectTransform ItemMoveDrag::rectTransform
	RectTransform_t972643934 * ___rectTransform_12;
	// UnityEngine.Vector3 ItemMoveDrag::mousePosition
	Vector3_t4282066566  ___mousePosition_13;
	// System.Boolean ItemMoveDrag::onSelectArea
	bool ___onSelectArea_14;
	// UnityEngine.Vector2 ItemMoveDrag::lastPosition
	Vector2_t4282066565  ___lastPosition_15;
	// UnityEngine.UI.ScrollRect ItemMoveDrag::scrollRect
	ScrollRect_t3606982749 * ___scrollRect_16;
	// System.Boolean ItemMoveDrag::isSelected
	bool ___isSelected_17;

public:
	inline static int32_t get_offset_of_velocity_2() { return static_cast<int32_t>(offsetof(ItemMoveDrag_t687865080, ___velocity_2)); }
	inline Vector3_t4282066566  get_velocity_2() const { return ___velocity_2; }
	inline Vector3_t4282066566 * get_address_of_velocity_2() { return &___velocity_2; }
	inline void set_velocity_2(Vector3_t4282066566  value)
	{
		___velocity_2 = value;
	}

	inline static int32_t get_offset_of_minVelToSelect_3() { return static_cast<int32_t>(offsetof(ItemMoveDrag_t687865080, ___minVelToSelect_3)); }
	inline float get_minVelToSelect_3() const { return ___minVelToSelect_3; }
	inline float* get_address_of_minVelToSelect_3() { return &___minVelToSelect_3; }
	inline void set_minVelToSelect_3(float value)
	{
		___minVelToSelect_3 = value;
	}

	inline static int32_t get_offset_of_rotFactor_4() { return static_cast<int32_t>(offsetof(ItemMoveDrag_t687865080, ___rotFactor_4)); }
	inline float get_rotFactor_4() const { return ___rotFactor_4; }
	inline float* get_address_of_rotFactor_4() { return &___rotFactor_4; }
	inline void set_rotFactor_4(float value)
	{
		___rotFactor_4 = value;
	}

	inline static int32_t get_offset_of_easetype_5() { return static_cast<int32_t>(offsetof(ItemMoveDrag_t687865080, ___easetype_5)); }
	inline int32_t get_easetype_5() const { return ___easetype_5; }
	inline int32_t* get_address_of_easetype_5() { return &___easetype_5; }
	inline void set_easetype_5(int32_t value)
	{
		___easetype_5 = value;
	}

	inline static int32_t get_offset_of_easetypeTime_6() { return static_cast<int32_t>(offsetof(ItemMoveDrag_t687865080, ___easetypeTime_6)); }
	inline float get_easetypeTime_6() const { return ___easetypeTime_6; }
	inline float* get_address_of_easetypeTime_6() { return &___easetypeTime_6; }
	inline void set_easetypeTime_6(float value)
	{
		___easetypeTime_6 = value;
	}

	inline static int32_t get_offset_of_allowImage_7() { return static_cast<int32_t>(offsetof(ItemMoveDrag_t687865080, ___allowImage_7)); }
	inline Image_t538875265 * get_allowImage_7() const { return ___allowImage_7; }
	inline Image_t538875265 ** get_address_of_allowImage_7() { return &___allowImage_7; }
	inline void set_allowImage_7(Image_t538875265 * value)
	{
		___allowImage_7 = value;
		Il2CppCodeGenWriteBarrier(&___allowImage_7, value);
	}

	inline static int32_t get_offset_of_denyImage_8() { return static_cast<int32_t>(offsetof(ItemMoveDrag_t687865080, ___denyImage_8)); }
	inline Image_t538875265 * get_denyImage_8() const { return ___denyImage_8; }
	inline Image_t538875265 ** get_address_of_denyImage_8() { return &___denyImage_8; }
	inline void set_denyImage_8(Image_t538875265 * value)
	{
		___denyImage_8 = value;
		Il2CppCodeGenWriteBarrier(&___denyImage_8, value);
	}

	inline static int32_t get_offset_of_onAllow_9() { return static_cast<int32_t>(offsetof(ItemMoveDrag_t687865080, ___onAllow_9)); }
	inline UnityEvent_t1266085011 * get_onAllow_9() const { return ___onAllow_9; }
	inline UnityEvent_t1266085011 ** get_address_of_onAllow_9() { return &___onAllow_9; }
	inline void set_onAllow_9(UnityEvent_t1266085011 * value)
	{
		___onAllow_9 = value;
		Il2CppCodeGenWriteBarrier(&___onAllow_9, value);
	}

	inline static int32_t get_offset_of_onDeny_10() { return static_cast<int32_t>(offsetof(ItemMoveDrag_t687865080, ___onDeny_10)); }
	inline UnityEvent_t1266085011 * get_onDeny_10() const { return ___onDeny_10; }
	inline UnityEvent_t1266085011 ** get_address_of_onDeny_10() { return &___onDeny_10; }
	inline void set_onDeny_10(UnityEvent_t1266085011 * value)
	{
		___onDeny_10 = value;
		Il2CppCodeGenWriteBarrier(&___onDeny_10, value);
	}

	inline static int32_t get_offset_of_onSelected_11() { return static_cast<int32_t>(offsetof(ItemMoveDrag_t687865080, ___onSelected_11)); }
	inline UnityEvent_t1266085011 * get_onSelected_11() const { return ___onSelected_11; }
	inline UnityEvent_t1266085011 ** get_address_of_onSelected_11() { return &___onSelected_11; }
	inline void set_onSelected_11(UnityEvent_t1266085011 * value)
	{
		___onSelected_11 = value;
		Il2CppCodeGenWriteBarrier(&___onSelected_11, value);
	}

	inline static int32_t get_offset_of_rectTransform_12() { return static_cast<int32_t>(offsetof(ItemMoveDrag_t687865080, ___rectTransform_12)); }
	inline RectTransform_t972643934 * get_rectTransform_12() const { return ___rectTransform_12; }
	inline RectTransform_t972643934 ** get_address_of_rectTransform_12() { return &___rectTransform_12; }
	inline void set_rectTransform_12(RectTransform_t972643934 * value)
	{
		___rectTransform_12 = value;
		Il2CppCodeGenWriteBarrier(&___rectTransform_12, value);
	}

	inline static int32_t get_offset_of_mousePosition_13() { return static_cast<int32_t>(offsetof(ItemMoveDrag_t687865080, ___mousePosition_13)); }
	inline Vector3_t4282066566  get_mousePosition_13() const { return ___mousePosition_13; }
	inline Vector3_t4282066566 * get_address_of_mousePosition_13() { return &___mousePosition_13; }
	inline void set_mousePosition_13(Vector3_t4282066566  value)
	{
		___mousePosition_13 = value;
	}

	inline static int32_t get_offset_of_onSelectArea_14() { return static_cast<int32_t>(offsetof(ItemMoveDrag_t687865080, ___onSelectArea_14)); }
	inline bool get_onSelectArea_14() const { return ___onSelectArea_14; }
	inline bool* get_address_of_onSelectArea_14() { return &___onSelectArea_14; }
	inline void set_onSelectArea_14(bool value)
	{
		___onSelectArea_14 = value;
	}

	inline static int32_t get_offset_of_lastPosition_15() { return static_cast<int32_t>(offsetof(ItemMoveDrag_t687865080, ___lastPosition_15)); }
	inline Vector2_t4282066565  get_lastPosition_15() const { return ___lastPosition_15; }
	inline Vector2_t4282066565 * get_address_of_lastPosition_15() { return &___lastPosition_15; }
	inline void set_lastPosition_15(Vector2_t4282066565  value)
	{
		___lastPosition_15 = value;
	}

	inline static int32_t get_offset_of_scrollRect_16() { return static_cast<int32_t>(offsetof(ItemMoveDrag_t687865080, ___scrollRect_16)); }
	inline ScrollRect_t3606982749 * get_scrollRect_16() const { return ___scrollRect_16; }
	inline ScrollRect_t3606982749 ** get_address_of_scrollRect_16() { return &___scrollRect_16; }
	inline void set_scrollRect_16(ScrollRect_t3606982749 * value)
	{
		___scrollRect_16 = value;
		Il2CppCodeGenWriteBarrier(&___scrollRect_16, value);
	}

	inline static int32_t get_offset_of_isSelected_17() { return static_cast<int32_t>(offsetof(ItemMoveDrag_t687865080, ___isSelected_17)); }
	inline bool get_isSelected_17() const { return ___isSelected_17; }
	inline bool* get_address_of_isSelected_17() { return &___isSelected_17; }
	inline void set_isSelected_17(bool value)
	{
		___isSelected_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
