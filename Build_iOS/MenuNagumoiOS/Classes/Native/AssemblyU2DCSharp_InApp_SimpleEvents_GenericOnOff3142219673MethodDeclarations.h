﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InApp.SimpleEvents.GenericOnOff
struct GenericOnOff_t3142219673;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void InApp.SimpleEvents.GenericOnOff::.ctor()
extern "C"  void GenericOnOff__ctor_m3135389793 (GenericOnOff_t3142219673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.SimpleEvents.GenericOnOff::Start()
extern "C"  void GenericOnOff_Start_m2082527585 (GenericOnOff_t3142219673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.SimpleEvents.GenericOnOff::configByString(System.String)
extern "C"  void GenericOnOff_configByString_m1652914133 (GenericOnOff_t3142219673 * __this, String_t* ___metadado0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.SimpleEvents.GenericOnOff::Reset()
extern "C"  void GenericOnOff_Reset_m781822734 (GenericOnOff_t3142219673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
