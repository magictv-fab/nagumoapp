﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{A1462213-BC44-4626-98E1-2B98D236945C}/__StaticArrayInitTypeSize=16
struct  __StaticArrayInitTypeSizeU3D16_t3501907547 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D16_t3501907547__padding[16];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: <PrivateImplementationDetails>{A1462213-BC44-4626-98E1-2B98D236945C}/__StaticArrayInitTypeSize=16
struct __StaticArrayInitTypeSizeU3D16_t3501907547_marshaled_pinvoke
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D16_t3501907547__padding[16];
	};
};
// Native definition for marshalling of: <PrivateImplementationDetails>{A1462213-BC44-4626-98E1-2B98D236945C}/__StaticArrayInitTypeSize=16
struct __StaticArrayInitTypeSizeU3D16_t3501907547_marshaled_com
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D16_t3501907547__padding[16];
	};
};
