﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SampleAppUILabel
struct SampleAppUILabel_t416684265;
// System.String
struct String_t;
// UnityEngine.GUIStyle
struct GUIStyle_t2990928826;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_GUIStyle2990928826.h"

// System.Void SampleAppUILabel::.ctor(System.Single,System.String,UnityEngine.GUIStyle)
extern "C"  void SampleAppUILabel__ctor_m3059384772 (SampleAppUILabel_t416684265 * __this, float ___index0, String_t* ___title1, GUIStyle_t2990928826 * ___style2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SampleAppUILabel::Draw()
extern "C"  void SampleAppUILabel_Draw_m3220165494 (SampleAppUILabel_t416684265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SampleAppUILabel::get_Width()
extern "C"  float SampleAppUILabel_get_Width_m1190347905 (SampleAppUILabel_t416684265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SampleAppUILabel::get_Height()
extern "C"  float SampleAppUILabel_get_Height_m3044861038 (SampleAppUILabel_t416684265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
