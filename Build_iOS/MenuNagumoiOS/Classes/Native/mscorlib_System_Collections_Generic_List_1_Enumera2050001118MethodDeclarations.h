﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ActionReport>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2566990377(__this, ___l0, method) ((  void (*) (Enumerator_t2050001118 *, List_1_t2030328348 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ActionReport>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2166649993(__this, method) ((  void (*) (Enumerator_t2050001118 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ActionReport>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3102949759(__this, method) ((  Il2CppObject * (*) (Enumerator_t2050001118 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ActionReport>::Dispose()
#define Enumerator_Dispose_m3199734350(__this, method) ((  void (*) (Enumerator_t2050001118 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ActionReport>::VerifyState()
#define Enumerator_VerifyState_m232651399(__this, method) ((  void (*) (Enumerator_t2050001118 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ActionReport>::MoveNext()
#define Enumerator_MoveNext_m1824819236(__this, method) ((  bool (*) (Enumerator_t2050001118 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ActionReport>::get_Current()
#define Enumerator_get_Current_m3936667320(__this, method) ((  ActionReport_t662142796 * (*) (Enumerator_t2050001118 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
