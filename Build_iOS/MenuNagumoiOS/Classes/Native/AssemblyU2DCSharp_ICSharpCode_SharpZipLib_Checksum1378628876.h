﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.UInt32[]
struct UInt32U5BU5D_t3230734560;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Checksums.StrangeCRC
struct  StrangeCRC_t1378628876  : public Il2CppObject
{
public:
	// System.Int32 ICSharpCode.SharpZipLib.Checksums.StrangeCRC::globalCrc
	int32_t ___globalCrc_1;

public:
	inline static int32_t get_offset_of_globalCrc_1() { return static_cast<int32_t>(offsetof(StrangeCRC_t1378628876, ___globalCrc_1)); }
	inline int32_t get_globalCrc_1() const { return ___globalCrc_1; }
	inline int32_t* get_address_of_globalCrc_1() { return &___globalCrc_1; }
	inline void set_globalCrc_1(int32_t value)
	{
		___globalCrc_1 = value;
	}
};

struct StrangeCRC_t1378628876_StaticFields
{
public:
	// System.UInt32[] ICSharpCode.SharpZipLib.Checksums.StrangeCRC::crc32Table
	UInt32U5BU5D_t3230734560* ___crc32Table_0;

public:
	inline static int32_t get_offset_of_crc32Table_0() { return static_cast<int32_t>(offsetof(StrangeCRC_t1378628876_StaticFields, ___crc32Table_0)); }
	inline UInt32U5BU5D_t3230734560* get_crc32Table_0() const { return ___crc32Table_0; }
	inline UInt32U5BU5D_t3230734560** get_address_of_crc32Table_0() { return &___crc32Table_0; }
	inline void set_crc32Table_0(UInt32U5BU5D_t3230734560* value)
	{
		___crc32Table_0 = value;
		Il2CppCodeGenWriteBarrier(&___crc32Table_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
