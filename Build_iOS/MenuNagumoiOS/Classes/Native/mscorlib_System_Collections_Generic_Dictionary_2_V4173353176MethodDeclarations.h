﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<MagicTV.globals.StateMachine,System.Object>
struct Dictionary_2_t1946552472;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V4173353176.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<MagicTV.globals.StateMachine,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1604712660_gshared (Enumerator_t4173353176 * __this, Dictionary_2_t1946552472 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m1604712660(__this, ___host0, method) ((  void (*) (Enumerator_t4173353176 *, Dictionary_2_t1946552472 *, const MethodInfo*))Enumerator__ctor_m1604712660_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<MagicTV.globals.StateMachine,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1388239767_gshared (Enumerator_t4173353176 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1388239767(__this, method) ((  Il2CppObject * (*) (Enumerator_t4173353176 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1388239767_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<MagicTV.globals.StateMachine,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2886941537_gshared (Enumerator_t4173353176 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2886941537(__this, method) ((  void (*) (Enumerator_t4173353176 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2886941537_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<MagicTV.globals.StateMachine,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2036358262_gshared (Enumerator_t4173353176 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2036358262(__this, method) ((  void (*) (Enumerator_t4173353176 *, const MethodInfo*))Enumerator_Dispose_m2036358262_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<MagicTV.globals.StateMachine,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1586787473_gshared (Enumerator_t4173353176 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1586787473(__this, method) ((  bool (*) (Enumerator_t4173353176 *, const MethodInfo*))Enumerator_MoveNext_m1586787473_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<MagicTV.globals.StateMachine,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m1679039481_gshared (Enumerator_t4173353176 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1679039481(__this, method) ((  Il2CppObject * (*) (Enumerator_t4173353176 *, const MethodInfo*))Enumerator_get_Current_m1679039481_gshared)(__this, method)
