﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "QRCode_ZXing_OneD_RSS_Expanded_Decoders_AI01decode1016996677.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.Decoders.AI01AndOtherAIs
struct  AI01AndOtherAIs_t4077022355  : public AI01decoder_t1016996677
{
public:

public:
};

struct AI01AndOtherAIs_t4077022355_StaticFields
{
public:
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.AI01AndOtherAIs::HEADER_SIZE
	int32_t ___HEADER_SIZE_3;

public:
	inline static int32_t get_offset_of_HEADER_SIZE_3() { return static_cast<int32_t>(offsetof(AI01AndOtherAIs_t4077022355_StaticFields, ___HEADER_SIZE_3)); }
	inline int32_t get_HEADER_SIZE_3() const { return ___HEADER_SIZE_3; }
	inline int32_t* get_address_of_HEADER_SIZE_3() { return &___HEADER_SIZE_3; }
	inline void set_HEADER_SIZE_3(int32_t value)
	{
		___HEADER_SIZE_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
