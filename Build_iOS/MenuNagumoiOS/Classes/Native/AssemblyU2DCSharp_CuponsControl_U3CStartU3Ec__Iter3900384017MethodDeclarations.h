﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CuponsControl/<Start>c__Iterator4D
struct U3CStartU3Ec__Iterator4D_t3900384017;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void CuponsControl/<Start>c__Iterator4D::.ctor()
extern "C"  void U3CStartU3Ec__Iterator4D__ctor_m3881799850 (U3CStartU3Ec__Iterator4D_t3900384017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CuponsControl/<Start>c__Iterator4D::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator4D_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1714157106 (U3CStartU3Ec__Iterator4D_t3900384017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CuponsControl/<Start>c__Iterator4D::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator4D_System_Collections_IEnumerator_get_Current_m4137983430 (U3CStartU3Ec__Iterator4D_t3900384017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CuponsControl/<Start>c__Iterator4D::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator4D_MoveNext_m509132786 (U3CStartU3Ec__Iterator4D_t3900384017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CuponsControl/<Start>c__Iterator4D::Dispose()
extern "C"  void U3CStartU3Ec__Iterator4D_Dispose_m2276325991 (U3CStartU3Ec__Iterator4D_t3900384017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CuponsControl/<Start>c__Iterator4D::Reset()
extern "C"  void U3CStartU3Ec__Iterator4D_Reset_m1528232791 (U3CStartU3Ec__Iterator4D_t3900384017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
