﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Aztec.Internal.AztecDetectorResult
struct AztecDetectorResult_t1662194462;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// ZXing.ResultPoint[]
struct ResultPointU5BU5D_t1195164344;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitMatrix1058711404.h"

// System.Boolean ZXing.Aztec.Internal.AztecDetectorResult::get_Compact()
extern "C"  bool AztecDetectorResult_get_Compact_m2835593205 (AztecDetectorResult_t1662194462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Aztec.Internal.AztecDetectorResult::set_Compact(System.Boolean)
extern "C"  void AztecDetectorResult_set_Compact_m1582125356 (AztecDetectorResult_t1662194462 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Aztec.Internal.AztecDetectorResult::get_NbDatablocks()
extern "C"  int32_t AztecDetectorResult_get_NbDatablocks_m934668526 (AztecDetectorResult_t1662194462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Aztec.Internal.AztecDetectorResult::set_NbDatablocks(System.Int32)
extern "C"  void AztecDetectorResult_set_NbDatablocks_m2814825317 (AztecDetectorResult_t1662194462 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Aztec.Internal.AztecDetectorResult::get_NbLayers()
extern "C"  int32_t AztecDetectorResult_get_NbLayers_m3352556160 (AztecDetectorResult_t1662194462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Aztec.Internal.AztecDetectorResult::set_NbLayers(System.Int32)
extern "C"  void AztecDetectorResult_set_NbLayers_m463475959 (AztecDetectorResult_t1662194462 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Aztec.Internal.AztecDetectorResult::.ctor(ZXing.Common.BitMatrix,ZXing.ResultPoint[],System.Boolean,System.Int32,System.Int32)
extern "C"  void AztecDetectorResult__ctor_m1524939936 (AztecDetectorResult_t1662194462 * __this, BitMatrix_t1058711404 * ___bits0, ResultPointU5BU5D_t1195164344* ___points1, bool ___compact2, int32_t ___nbDatablocks3, int32_t ___nbLayers4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
