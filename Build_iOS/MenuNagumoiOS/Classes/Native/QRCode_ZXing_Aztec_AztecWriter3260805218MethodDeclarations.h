﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Aztec.AztecWriter
struct AztecWriter_t3260805218;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// System.String
struct String_t;
// System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>
struct IDictionary_2_t3297010830;
// System.Text.Encoding
struct Encoding_t2012439129;
// ZXing.Aztec.Internal.AztecCode
struct AztecCode_t1281541704;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "QRCode_ZXing_BarcodeFormat4201805817.h"
#include "mscorlib_System_Text_Encoding2012439129.h"
#include "QRCode_ZXing_Aztec_Internal_AztecCode1281541704.h"

// System.Void ZXing.Aztec.AztecWriter::.cctor()
extern "C"  void AztecWriter__cctor_m3004345359 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.BitMatrix ZXing.Aztec.AztecWriter::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern "C"  BitMatrix_t1058711404 * AztecWriter_encode_m3985285576 (AztecWriter_t3260805218 * __this, String_t* ___contents0, int32_t ___format1, int32_t ___width2, int32_t ___height3, Il2CppObject* ___hints4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.BitMatrix ZXing.Aztec.AztecWriter::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Text.Encoding,System.Int32,System.Int32)
extern "C"  BitMatrix_t1058711404 * AztecWriter_encode_m156083652 (Il2CppObject * __this /* static, unused */, String_t* ___contents0, int32_t ___format1, int32_t ___width2, int32_t ___height3, Encoding_t2012439129 * ___charset4, int32_t ___eccPercent5, int32_t ___layers6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.BitMatrix ZXing.Aztec.AztecWriter::renderResult(ZXing.Aztec.Internal.AztecCode,System.Int32,System.Int32)
extern "C"  BitMatrix_t1058711404 * AztecWriter_renderResult_m1589522813 (Il2CppObject * __this /* static, unused */, AztecCode_t1281541704 * ___code0, int32_t ___width1, int32_t ___height2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Aztec.AztecWriter::.ctor()
extern "C"  void AztecWriter__ctor_m1082297182 (AztecWriter_t3260805218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
