﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SampleAppUIConstants
struct SampleAppUIConstants_t672344260;

#include "codegen/il2cpp-codegen.h"

// System.Void SampleAppUIConstants::.ctor()
extern "C"  void SampleAppUIConstants__ctor_m3596406359 (SampleAppUIConstants_t672344260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SampleAppUIConstants::.cctor()
extern "C"  void SampleAppUIConstants__cctor_m3632318518 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
