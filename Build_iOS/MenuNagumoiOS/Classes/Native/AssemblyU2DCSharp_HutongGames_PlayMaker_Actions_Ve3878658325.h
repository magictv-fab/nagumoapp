﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector3LowPassFilter
struct  Vector3LowPassFilter_t3878658325  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3LowPassFilter::vector3Variable
	FsmVector3_t533912882 * ___vector3Variable_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector3LowPassFilter::filteringFactor
	FsmFloat_t2134102846 * ___filteringFactor_10;
	// UnityEngine.Vector3 HutongGames.PlayMaker.Actions.Vector3LowPassFilter::filteredVector
	Vector3_t4282066566  ___filteredVector_11;

public:
	inline static int32_t get_offset_of_vector3Variable_9() { return static_cast<int32_t>(offsetof(Vector3LowPassFilter_t3878658325, ___vector3Variable_9)); }
	inline FsmVector3_t533912882 * get_vector3Variable_9() const { return ___vector3Variable_9; }
	inline FsmVector3_t533912882 ** get_address_of_vector3Variable_9() { return &___vector3Variable_9; }
	inline void set_vector3Variable_9(FsmVector3_t533912882 * value)
	{
		___vector3Variable_9 = value;
		Il2CppCodeGenWriteBarrier(&___vector3Variable_9, value);
	}

	inline static int32_t get_offset_of_filteringFactor_10() { return static_cast<int32_t>(offsetof(Vector3LowPassFilter_t3878658325, ___filteringFactor_10)); }
	inline FsmFloat_t2134102846 * get_filteringFactor_10() const { return ___filteringFactor_10; }
	inline FsmFloat_t2134102846 ** get_address_of_filteringFactor_10() { return &___filteringFactor_10; }
	inline void set_filteringFactor_10(FsmFloat_t2134102846 * value)
	{
		___filteringFactor_10 = value;
		Il2CppCodeGenWriteBarrier(&___filteringFactor_10, value);
	}

	inline static int32_t get_offset_of_filteredVector_11() { return static_cast<int32_t>(offsetof(Vector3LowPassFilter_t3878658325, ___filteredVector_11)); }
	inline Vector3_t4282066566  get_filteredVector_11() const { return ___filteredVector_11; }
	inline Vector3_t4282066566 * get_address_of_filteredVector_11() { return &___filteredVector_11; }
	inline void set_filteredVector_11(Vector3_t4282066566  value)
	{
		___filteredVector_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
