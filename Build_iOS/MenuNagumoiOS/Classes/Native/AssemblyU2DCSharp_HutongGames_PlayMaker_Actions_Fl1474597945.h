﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FloatSignTest
struct  FloatSignTest_t1474597945  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatSignTest::floatValue
	FsmFloat_t2134102846 * ___floatValue_9;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.FloatSignTest::isPositive
	FsmEvent_t2133468028 * ___isPositive_10;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.FloatSignTest::isNegative
	FsmEvent_t2133468028 * ___isNegative_11;
	// System.Boolean HutongGames.PlayMaker.Actions.FloatSignTest::everyFrame
	bool ___everyFrame_12;

public:
	inline static int32_t get_offset_of_floatValue_9() { return static_cast<int32_t>(offsetof(FloatSignTest_t1474597945, ___floatValue_9)); }
	inline FsmFloat_t2134102846 * get_floatValue_9() const { return ___floatValue_9; }
	inline FsmFloat_t2134102846 ** get_address_of_floatValue_9() { return &___floatValue_9; }
	inline void set_floatValue_9(FsmFloat_t2134102846 * value)
	{
		___floatValue_9 = value;
		Il2CppCodeGenWriteBarrier(&___floatValue_9, value);
	}

	inline static int32_t get_offset_of_isPositive_10() { return static_cast<int32_t>(offsetof(FloatSignTest_t1474597945, ___isPositive_10)); }
	inline FsmEvent_t2133468028 * get_isPositive_10() const { return ___isPositive_10; }
	inline FsmEvent_t2133468028 ** get_address_of_isPositive_10() { return &___isPositive_10; }
	inline void set_isPositive_10(FsmEvent_t2133468028 * value)
	{
		___isPositive_10 = value;
		Il2CppCodeGenWriteBarrier(&___isPositive_10, value);
	}

	inline static int32_t get_offset_of_isNegative_11() { return static_cast<int32_t>(offsetof(FloatSignTest_t1474597945, ___isNegative_11)); }
	inline FsmEvent_t2133468028 * get_isNegative_11() const { return ___isNegative_11; }
	inline FsmEvent_t2133468028 ** get_address_of_isNegative_11() { return &___isNegative_11; }
	inline void set_isNegative_11(FsmEvent_t2133468028 * value)
	{
		___isNegative_11 = value;
		Il2CppCodeGenWriteBarrier(&___isNegative_11, value);
	}

	inline static int32_t get_offset_of_everyFrame_12() { return static_cast<int32_t>(offsetof(FloatSignTest_t1474597945, ___everyFrame_12)); }
	inline bool get_everyFrame_12() const { return ___everyFrame_12; }
	inline bool* get_address_of_everyFrame_12() { return &___everyFrame_12; }
	inline void set_everyFrame_12(bool value)
	{
		___everyFrame_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
