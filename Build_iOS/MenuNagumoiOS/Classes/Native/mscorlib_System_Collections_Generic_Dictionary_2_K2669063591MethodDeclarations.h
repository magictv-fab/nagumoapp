﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4137643543MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m234412046(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2669063591 *, Dictionary_2_t1042304140 *, const MethodInfo*))KeyCollection__ctor_m2058062697_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3666686088(__this, ___item0, method) ((  void (*) (KeyCollection_t2669063591 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2538115149_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m4294301695(__this, method) ((  void (*) (KeyCollection_t2669063591 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1562404100_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m4180592002(__this, ___item0, method) ((  bool (*) (KeyCollection_t2669063591 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m597680733_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3557994343(__this, ___item0, method) ((  bool (*) (KeyCollection_t2669063591 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2857060098_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3617922491(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2669063591 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3234332160_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m399554033(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2669063591 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3642680182_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3535374764(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2669063591 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m188384753_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m340583523(__this, method) ((  bool (*) (KeyCollection_t2669063591 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1726625406_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1854635221(__this, method) ((  bool (*) (KeyCollection_t2669063591 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2726864048_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m746030657(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2669063591 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m2244505756_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m3898209987(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2669063591 *, PerspectiveU5BU5D_t1573652303*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m613996062_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::GetEnumerator()
#define KeyCollection_GetEnumerator_m1969638950(__this, method) ((  Enumerator_t1657240194  (*) (KeyCollection_t2669063591 *, const MethodInfo*))KeyCollection_GetEnumerator_m3434083777_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.Perspective,MagicTV.globals.events.ScreenEvents/OnChangeAnToPerspectiveEventHandler>::get_Count()
#define KeyCollection_get_Count_m680986011(__this, method) ((  int32_t (*) (KeyCollection_t2669063591 *, const MethodInfo*))KeyCollection_get_Count_m2713534454_gshared)(__this, method)
