﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2045888271MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::.ctor()
#define Dictionary_2__ctor_m3784032898(__this, method) ((  void (*) (Dictionary_2_t1615989430 *, const MethodInfo*))Dictionary_2__ctor_m3794638399_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m1270309378(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t1615989430 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m273898294_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::.ctor(System.Int32)
#define Dictionary_2__ctor_m661956572(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t1615989430 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m2504582416_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m3491521996(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1615989430 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m4162067200_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3997331821(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1615989430 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1029455407_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m1041335945(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1615989430 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m1780508229_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::System.Collections.IDictionary.get_Values()
#define Dictionary_2_System_Collections_IDictionary_get_Values_m2364017975(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1615989430 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m4038979059_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::System.Collections.IDictionary.get_IsFixedSize()
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1647806834(__this, method) ((  bool (*) (Dictionary_2_t1615989430 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m824729858_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::System.Collections.IDictionary.get_IsReadOnly()
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3278110535(__this, method) ((  bool (*) (Dictionary_2_t1615989430 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1311897015_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m1894956819(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1615989430 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m1551250025_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m4140849666(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1615989430 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1049066318_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m2009213711(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1615989430 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m781609539_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m1914112963(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1615989430 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1783363411_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m3945470016(__this, ___key0, method) ((  void (*) (Dictionary_2_t1615989430 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m2215006604_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2144137841(__this, method) ((  bool (*) (Dictionary_2_t1615989430 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1826238689_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3691640611(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1615989430 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2075478797_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3950889845(__this, method) ((  bool (*) (Dictionary_2_t1615989430 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1433083365_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2677908694(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t1615989430 *, KeyValuePair_2_t1514770136 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3257059362_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3357158896(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1615989430 *, KeyValuePair_2_t1514770136 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2744049760_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2995211066(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1615989430 *, KeyValuePair_2U5BU5D_t3896632009*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2008986502_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m4080734485(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1615989430 *, KeyValuePair_2_t1514770136 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2350489477_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m2951827609(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1615989430 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m3550803941_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2348713448(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1615989430 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m4115711264_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m323016735(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1615989430 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m52259357_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1134977452(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1615989430 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1395730616_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::get_Count()
#define Dictionary_2_get_Count_m3980799915(__this, method) ((  int32_t (*) (Dictionary_2_t1615989430 *, const MethodInfo*))Dictionary_2_get_Count_m1232250407_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::get_Item(TKey)
#define Dictionary_2_get_Item_m283342076(__this, ___key0, method) ((  UniWebViewNativeListener_t795571060 * (*) (Dictionary_2_t1615989430 *, String_t*, const MethodInfo*))Dictionary_2_get_Item_m2285357284_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m1390981515(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1615989430 *, String_t*, UniWebViewNativeListener_t795571060 *, const MethodInfo*))Dictionary_2_set_Item_m2627891647_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m415574659(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t1615989430 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m2966484407_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m4148955028(__this, ___size0, method) ((  void (*) (Dictionary_2_t1615989430 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2119297760_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m9441424(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1615989430 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2536521436_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m1903291172(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t1514770136  (*) (Il2CppObject * /* static, unused */, String_t*, UniWebViewNativeListener_t795571060 *, const MethodInfo*))Dictionary_2_make_pair_m2083407400_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m4106576154(__this /* static, unused */, ___key0, ___value1, method) ((  String_t* (*) (Il2CppObject * /* static, unused */, String_t*, UniWebViewNativeListener_t795571060 *, const MethodInfo*))Dictionary_2_pick_key_m3909093582_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m367021814(__this /* static, unused */, ___key0, ___value1, method) ((  UniWebViewNativeListener_t795571060 * (*) (Il2CppObject * /* static, unused */, String_t*, UniWebViewNativeListener_t795571060 *, const MethodInfo*))Dictionary_2_pick_value_m3477594126_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m1462714687(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1615989430 *, KeyValuePair_2U5BU5D_t3896632009*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m3401241971_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::Resize()
#define Dictionary_2_Resize_m3842580109(__this, method) ((  void (*) (Dictionary_2_t1615989430 *, const MethodInfo*))Dictionary_2_Resize_m1727470041_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::Add(TKey,TValue)
#define Dictionary_2_Add_m151305828(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1615989430 *, String_t*, UniWebViewNativeListener_t795571060 *, const MethodInfo*))Dictionary_2_Add_m3537188182_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::Clear()
#define Dictionary_2_Clear_m160622390(__this, method) ((  void (*) (Dictionary_2_t1615989430 *, const MethodInfo*))Dictionary_2_Clear_m1200771690_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m3968544864(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1615989430 *, String_t*, const MethodInfo*))Dictionary_2_ContainsKey_m3006991056_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m1293374560(__this, ___value0, method) ((  bool (*) (Dictionary_2_t1615989430 *, UniWebViewNativeListener_t795571060 *, const MethodInfo*))Dictionary_2_ContainsValue_m712275664_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m2595299625(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1615989430 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m1544184413_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m4242712795(__this, ___sender0, method) ((  void (*) (Dictionary_2_t1615989430 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m1638301735_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::Remove(TKey)
#define Dictionary_2_Remove_m3002715395(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1615989430 *, String_t*, const MethodInfo*))Dictionary_2_Remove_m2155719712_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m3112941713(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t1615989430 *, String_t*, UniWebViewNativeListener_t795571060 **, const MethodInfo*))Dictionary_2_TryGetValue_m2075628329_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::get_Keys()
#define Dictionary_2_get_Keys_m1247761154(__this, method) ((  KeyCollection_t3242748881 * (*) (Dictionary_2_t1615989430 *, const MethodInfo*))Dictionary_2_get_Keys_m2624609910_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::get_Values()
#define Dictionary_2_get_Values_m1104333982(__this, method) ((  ValueCollection_t316595143 * (*) (Dictionary_2_t1615989430 *, const MethodInfo*))Dictionary_2_get_Values_m2070602102_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m3556435061(__this, ___key0, method) ((  String_t* (*) (Dictionary_2_t1615989430 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m3358952489_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m2974303249(__this, ___value0, method) ((  UniWebViewNativeListener_t795571060 * (*) (Dictionary_2_t1615989430 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1789908265_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m1516567155(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t1615989430 *, KeyValuePair_2_t1514770136 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m3073235459_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m755416214(__this, method) ((  Enumerator_t2933312822  (*) (Dictionary_2_t1615989430 *, const MethodInfo*))Dictionary_2_GetEnumerator_m65675076_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>::<CopyTo>m__2(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__2_m2153562059(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, String_t*, UniWebViewNativeListener_t795571060 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__2_m2807612881_gshared)(__this /* static, unused */, ___key0, ___value1, method)
