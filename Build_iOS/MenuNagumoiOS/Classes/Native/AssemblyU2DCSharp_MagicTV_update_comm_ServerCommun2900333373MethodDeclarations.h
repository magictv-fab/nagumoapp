﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.update.comm.ServerCommunication
struct ServerCommunication_t2900333373;
// MagicTV.update.comm.ServerCommunication/OnBooleanEventHandler
struct OnBooleanEventHandler_t2032344769;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.WWW
struct WWW_t3134621005;
// System.String
struct String_t;
// MagicTV.vo.RevisionInfoVO
struct RevisionInfoVO_t1983749152;
// MagicTV.vo.RevisionInfoPingVO
struct RevisionInfoPingVO_t4026113458;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_update_comm_ServerCommun2032344769.h"
#include "UnityEngine_UnityEngine_WWW3134621005.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_MagicTV_vo_RevisionInfoVO1983749152.h"
#include "AssemblyU2DCSharp_MNDialogResult3125317574.h"
#include "AssemblyU2DCSharp_MagicTV_vo_RevisionInfoPingVO4026113458.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void MagicTV.update.comm.ServerCommunication::.ctor()
extern "C"  void ServerCommunication__ctor_m508323220 (ServerCommunication_t2900333373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.comm.ServerCommunication::AddUpdateMetadataEventHandler(MagicTV.update.comm.ServerCommunication/OnBooleanEventHandler)
extern "C"  void ServerCommunication_AddUpdateMetadataEventHandler_m3325454710 (ServerCommunication_t2900333373 * __this, OnBooleanEventHandler_t2032344769 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.comm.ServerCommunication::RemoveUpdateMetadataEventHandler(MagicTV.update.comm.ServerCommunication/OnBooleanEventHandler)
extern "C"  void ServerCommunication_RemoveUpdateMetadataEventHandler_m141010613 (ServerCommunication_t2900333373 * __this, OnBooleanEventHandler_t2032344769 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.comm.ServerCommunication::RaiseUpdateMetadataComplete(System.Boolean)
extern "C"  void ServerCommunication_RaiseUpdateMetadataComplete_m1104700262 (ServerCommunication_t2900333373 * __this, bool ___success0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.comm.ServerCommunication::Start()
extern "C"  void ServerCommunication_Start_m3750428308 (ServerCommunication_t2900333373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.comm.ServerCommunication::prepare()
extern "C"  void ServerCommunication_prepare_m4094290393 (ServerCommunication_t2900333373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.comm.ServerCommunication::RequestUpdate()
extern "C"  void ServerCommunication_RequestUpdate_m1468140522 (ServerCommunication_t2900333373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.comm.ServerCommunication::RequestRegisterDeviceOnceTime()
extern "C"  void ServerCommunication_RequestRegisterDeviceOnceTime_m2861805192 (ServerCommunication_t2900333373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.comm.ServerCommunication::MudaString()
extern "C"  void ServerCommunication_MudaString_m4028400902 (ServerCommunication_t2900333373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MagicTV.update.comm.ServerCommunication::WaitForRequestDeviceRegister(UnityEngine.WWW)
extern "C"  Il2CppObject * ServerCommunication_WaitForRequestDeviceRegister_m358022710 (ServerCommunication_t2900333373 * __this, WWW_t3134621005 * ___www0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.comm.ServerCommunication::dispatchResultDeviceJson(UnityEngine.WWW)
extern "C"  void ServerCommunication_dispatchResultDeviceJson_m116683917 (ServerCommunication_t2900333373 * __this, WWW_t3134621005 * ___www0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.comm.ServerCommunication::commitPin(System.String)
extern "C"  void ServerCommunication_commitPin_m3241380178 (ServerCommunication_t2900333373 * __this, String_t* ___pin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.comm.ServerCommunication::checkImageUpdate(MagicTV.vo.RevisionInfoVO)
extern "C"  void ServerCommunication_checkImageUpdate_m1693928718 (ServerCommunication_t2900333373 * __this, RevisionInfoVO_t1983749152 * ___revisionInfoVO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.comm.ServerCommunication::doCheckImageUpdate()
extern "C"  void ServerCommunication_doCheckImageUpdate_m2311181207 (ServerCommunication_t2900333373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.comm.ServerCommunication::RaiseUpdateJsonRequestError(System.String)
extern "C"  void ServerCommunication_RaiseUpdateJsonRequestError_m4215994842 (ServerCommunication_t2900333373 * __this, String_t* ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWW MagicTV.update.comm.ServerCommunication::pingServer()
extern "C"  WWW_t3134621005 * ServerCommunication_pingServer_m1411629234 (ServerCommunication_t2900333373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.comm.ServerCommunication::checkPingTimeoutIsFinished()
extern "C"  void ServerCommunication_checkPingTimeoutIsFinished_m3025746835 (ServerCommunication_t2900333373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.comm.ServerCommunication::OnDialogPingClose(MNDialogResult)
extern "C"  void ServerCommunication_OnDialogPingClose_m1223882699 (ServerCommunication_t2900333373 * __this, int32_t ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MagicTV.update.comm.ServerCommunication::WaitForRequestPing(UnityEngine.WWW)
extern "C"  Il2CppObject * ServerCommunication_WaitForRequestPing_m1727312509 (ServerCommunication_t2900333373 * __this, WWW_t3134621005 * ___www0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.comm.ServerCommunication::dispatchResultPing(UnityEngine.WWW)
extern "C"  void ServerCommunication_dispatchResultPing_m295406969 (ServerCommunication_t2900333373 * __this, WWW_t3134621005 * ___www0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.comm.ServerCommunication::PingRequestComplete(MagicTV.vo.RevisionInfoPingVO)
extern "C"  void ServerCommunication_PingRequestComplete_m3859983264 (ServerCommunication_t2900333373 * __this, RevisionInfoPingVO_t4026113458 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWW MagicTV.update.comm.ServerCommunication::getServerResult()
extern "C"  WWW_t3134621005 * ServerCommunication_getServerResult_m2760624923 (ServerCommunication_t2900333373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.comm.ServerCommunication::checkTimeoutIsFinished()
extern "C"  void ServerCommunication_checkTimeoutIsFinished_m2935917029 (ServerCommunication_t2900333373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.comm.ServerCommunication::OnDialogClose(MNDialogResult)
extern "C"  void ServerCommunication_OnDialogClose_m659493661 (ServerCommunication_t2900333373 * __this, int32_t ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MagicTV.update.comm.ServerCommunication::WaitForRequest(UnityEngine.WWW)
extern "C"  Il2CppObject * ServerCommunication_WaitForRequest_m1149239503 (ServerCommunication_t2900333373 * __this, WWW_t3134621005 * ___www0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.comm.ServerCommunication::dispatchResult(UnityEngine.WWW)
extern "C"  void ServerCommunication_dispatchResult_m702790091 (ServerCommunication_t2900333373 * __this, WWW_t3134621005 * ___www0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWW MagicTV.update.comm.ServerCommunication::sendToken(System.String)
extern "C"  WWW_t3134621005 * ServerCommunication_sendToken_m2819480524 (ServerCommunication_t2900333373 * __this, String_t* ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MagicTV.update.comm.ServerCommunication::WaitForRequestToken(UnityEngine.WWW)
extern "C"  Il2CppObject * ServerCommunication_WaitForRequestToken_m3880296410 (ServerCommunication_t2900333373 * __this, WWW_t3134621005 * ___www0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWW MagicTV.update.comm.ServerCommunication::sendTransformInfo(UnityEngine.GameObject,System.String)
extern "C"  WWW_t3134621005 * ServerCommunication_sendTransformInfo_m1053222187 (ServerCommunication_t2900333373 * __this, GameObject_t3674682005 * ___gm0, String_t* ___bundle_id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWW MagicTV.update.comm.ServerCommunication::sendVuforiaChromaquiInfo(System.String,System.String,System.Single,System.Single,System.Single,System.Single)
extern "C"  WWW_t3134621005 * ServerCommunication_sendVuforiaChromaquiInfo_m3417881490 (ServerCommunication_t2900333373 * __this, String_t* ___material_name0, String_t* ___bundle_id1, float ___float12, float ___float23, float ___float34, float ___float45, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MagicTV.update.comm.ServerCommunication::WaitForRequestMetas(UnityEngine.WWW)
extern "C"  Il2CppObject * ServerCommunication_WaitForRequestMetas_m2156354405 (ServerCommunication_t2900333373 * __this, WWW_t3134621005 * ___www0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.comm.ServerCommunication::dispatchResultToken(UnityEngine.WWW)
extern "C"  void ServerCommunication_dispatchResultToken_m2440897630 (ServerCommunication_t2900333373 * __this, WWW_t3134621005 * ___www0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.update.comm.ServerCommunication::dispatchResultMeta(UnityEngine.WWW)
extern "C"  void ServerCommunication_dispatchResultMeta_m2212254022 (ServerCommunication_t2900333373 * __this, WWW_t3134621005 * ___www0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
