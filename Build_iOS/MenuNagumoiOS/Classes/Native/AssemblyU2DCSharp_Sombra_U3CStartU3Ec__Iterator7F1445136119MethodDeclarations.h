﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Sombra/<Start>c__Iterator7F
struct U3CStartU3Ec__Iterator7F_t1445136119;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Sombra/<Start>c__Iterator7F::.ctor()
extern "C"  void U3CStartU3Ec__Iterator7F__ctor_m3451423316 (U3CStartU3Ec__Iterator7F_t1445136119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Sombra/<Start>c__Iterator7F::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator7F_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1505469182 (U3CStartU3Ec__Iterator7F_t1445136119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Sombra/<Start>c__Iterator7F::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator7F_System_Collections_IEnumerator_get_Current_m1222599314 (U3CStartU3Ec__Iterator7F_t1445136119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Sombra/<Start>c__Iterator7F::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator7F_MoveNext_m3108358432 (U3CStartU3Ec__Iterator7F_t1445136119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sombra/<Start>c__Iterator7F::Dispose()
extern "C"  void U3CStartU3Ec__Iterator7F_Dispose_m1001337233 (U3CStartU3Ec__Iterator7F_t1445136119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sombra/<Start>c__Iterator7F::Reset()
extern "C"  void U3CStartU3Ec__Iterator7F_Reset_m1097856257 (U3CStartU3Ec__Iterator7F_t1445136119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
