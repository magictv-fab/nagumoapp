﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.TestStatus
struct TestStatus_t2697759954;
// ICSharpCode.SharpZipLib.Zip.ZipFile
struct ZipFile_t2937401711;
// ICSharpCode.SharpZipLib.Zip.ZipEntry
struct ZipEntry_t3141689087;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipF2937401711.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Test3198385319.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_ZipE3141689087.h"

// System.Void ICSharpCode.SharpZipLib.Zip.TestStatus::.ctor(ICSharpCode.SharpZipLib.Zip.ZipFile)
extern "C"  void TestStatus__ctor_m2311824058 (TestStatus_t2697759954 * __this, ZipFile_t2937401711 * ___file0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Zip.TestOperation ICSharpCode.SharpZipLib.Zip.TestStatus::get_Operation()
extern "C"  int32_t TestStatus_get_Operation_m1605928281 (TestStatus_t2697759954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Zip.ZipFile ICSharpCode.SharpZipLib.Zip.TestStatus::get_File()
extern "C"  ZipFile_t2937401711 * TestStatus_get_File_m857620132 (TestStatus_t2697759954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Zip.ZipEntry ICSharpCode.SharpZipLib.Zip.TestStatus::get_Entry()
extern "C"  ZipEntry_t3141689087 * TestStatus_get_Entry_m3343444818 (TestStatus_t2697759954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.TestStatus::get_ErrorCount()
extern "C"  int32_t TestStatus_get_ErrorCount_m2114220297 (TestStatus_t2697759954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.TestStatus::get_BytesTested()
extern "C"  int64_t TestStatus_get_BytesTested_m3848084699 (TestStatus_t2697759954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Zip.TestStatus::get_EntryValid()
extern "C"  bool TestStatus_get_EntryValid_m444852658 (TestStatus_t2697759954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.TestStatus::AddError()
extern "C"  void TestStatus_AddError_m1855867954 (TestStatus_t2697759954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.TestStatus::SetOperation(ICSharpCode.SharpZipLib.Zip.TestOperation)
extern "C"  void TestStatus_SetOperation_m3830873227 (TestStatus_t2697759954 * __this, int32_t ___operation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.TestStatus::SetEntry(ICSharpCode.SharpZipLib.Zip.ZipEntry)
extern "C"  void TestStatus_SetEntry_m4293818924 (TestStatus_t2697759954 * __this, ZipEntry_t3141689087 * ___entry0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.TestStatus::SetBytesTested(System.Int64)
extern "C"  void TestStatus_SetBytesTested_m2331266711 (TestStatus_t2697759954 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
