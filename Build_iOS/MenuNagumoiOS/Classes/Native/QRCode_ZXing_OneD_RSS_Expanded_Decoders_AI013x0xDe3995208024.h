﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "QRCode_ZXing_OneD_RSS_Expanded_Decoders_AI01weight1167885677.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Expanded.Decoders.AI013x0xDecoder
struct  AI013x0xDecoder_t3995208024  : public AI01weightDecoder_t1167885677
{
public:

public:
};

struct AI013x0xDecoder_t3995208024_StaticFields
{
public:
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.AI013x0xDecoder::HEADER_SIZE
	int32_t ___HEADER_SIZE_3;
	// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.AI013x0xDecoder::WEIGHT_SIZE
	int32_t ___WEIGHT_SIZE_4;

public:
	inline static int32_t get_offset_of_HEADER_SIZE_3() { return static_cast<int32_t>(offsetof(AI013x0xDecoder_t3995208024_StaticFields, ___HEADER_SIZE_3)); }
	inline int32_t get_HEADER_SIZE_3() const { return ___HEADER_SIZE_3; }
	inline int32_t* get_address_of_HEADER_SIZE_3() { return &___HEADER_SIZE_3; }
	inline void set_HEADER_SIZE_3(int32_t value)
	{
		___HEADER_SIZE_3 = value;
	}

	inline static int32_t get_offset_of_WEIGHT_SIZE_4() { return static_cast<int32_t>(offsetof(AI013x0xDecoder_t3995208024_StaticFields, ___WEIGHT_SIZE_4)); }
	inline int32_t get_WEIGHT_SIZE_4() const { return ___WEIGHT_SIZE_4; }
	inline int32_t* get_address_of_WEIGHT_SIZE_4() { return &___WEIGHT_SIZE_4; }
	inline void set_WEIGHT_SIZE_4(int32_t value)
	{
		___WEIGHT_SIZE_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
