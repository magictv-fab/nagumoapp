﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Datamatrix.Encoder.EncoderContext
struct EncoderContext_t1774722223;
// System.String
struct String_t;
// ZXing.Dimension
struct Dimension_t1395692488;
// System.Text.StringBuilder
struct StringBuilder_t243639308;
// ZXing.Datamatrix.Encoder.SymbolInfo
struct SymbolInfo_t553159586;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "QRCode_ZXing_Datamatrix_Encoder_SymbolShapeHint2380532246.h"
#include "QRCode_ZXing_Dimension1395692488.h"

// System.Void ZXing.Datamatrix.Encoder.EncoderContext::.cctor()
extern "C"  void EncoderContext__cctor_m3733273280 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.EncoderContext::.ctor(System.String)
extern "C"  void EncoderContext__ctor_m2075003285 (EncoderContext_t1774722223 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.EncoderContext::setSymbolShape(ZXing.Datamatrix.Encoder.SymbolShapeHint)
extern "C"  void EncoderContext_setSymbolShape_m3694618393 (EncoderContext_t1774722223 * __this, int32_t ___shape0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.EncoderContext::setSizeConstraints(ZXing.Dimension,ZXing.Dimension)
extern "C"  void EncoderContext_setSizeConstraints_m3385694076 (EncoderContext_t1774722223 * __this, Dimension_t1395692488 * ___minSize0, Dimension_t1395692488 * ___maxSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.EncoderContext::setSkipAtEnd(System.Int32)
extern "C"  void EncoderContext_setSkipAtEnd_m22746287 (EncoderContext_t1774722223 * __this, int32_t ___count0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char ZXing.Datamatrix.Encoder.EncoderContext::get_CurrentChar()
extern "C"  Il2CppChar EncoderContext_get_CurrentChar_m1749056431 (EncoderContext_t1774722223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.EncoderContext::writeCodewords(System.String)
extern "C"  void EncoderContext_writeCodewords_m2818555278 (EncoderContext_t1774722223 * __this, String_t* ___codewords0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.EncoderContext::writeCodeword(System.Char)
extern "C"  void EncoderContext_writeCodeword_m1046931580 (EncoderContext_t1774722223 * __this, Il2CppChar ___codeword0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Encoder.EncoderContext::get_CodewordCount()
extern "C"  int32_t EncoderContext_get_CodewordCount_m4092014312 (EncoderContext_t1774722223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.EncoderContext::signalEncoderChange(System.Int32)
extern "C"  void EncoderContext_signalEncoderChange_m1529751104 (EncoderContext_t1774722223 * __this, int32_t ___encoding0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.EncoderContext::resetEncoderSignal()
extern "C"  void EncoderContext_resetEncoderSignal_m3893991180 (EncoderContext_t1774722223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Datamatrix.Encoder.EncoderContext::get_HasMoreCharacters()
extern "C"  bool EncoderContext_get_HasMoreCharacters_m1552748495 (EncoderContext_t1774722223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Encoder.EncoderContext::get_TotalMessageCharCount()
extern "C"  int32_t EncoderContext_get_TotalMessageCharCount_m455632390 (EncoderContext_t1774722223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Encoder.EncoderContext::get_RemainingCharacters()
extern "C"  int32_t EncoderContext_get_RemainingCharacters_m3010107856 (EncoderContext_t1774722223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.EncoderContext::updateSymbolInfo()
extern "C"  void EncoderContext_updateSymbolInfo_m1320222790 (EncoderContext_t1774722223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.EncoderContext::updateSymbolInfo(System.Int32)
extern "C"  void EncoderContext_updateSymbolInfo_m462584983 (EncoderContext_t1774722223 * __this, int32_t ___len0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.EncoderContext::resetSymbolInfo()
extern "C"  void EncoderContext_resetSymbolInfo_m1853615904 (EncoderContext_t1774722223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Encoder.EncoderContext::get_Pos()
extern "C"  int32_t EncoderContext_get_Pos_m97848356 (EncoderContext_t1774722223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Encoder.EncoderContext::set_Pos(System.Int32)
extern "C"  void EncoderContext_set_Pos_m1445175987 (EncoderContext_t1774722223 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder ZXing.Datamatrix.Encoder.EncoderContext::get_Codewords()
extern "C"  StringBuilder_t243639308 * EncoderContext_get_Codewords_m3532172073 (EncoderContext_t1774722223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Datamatrix.Encoder.SymbolInfo ZXing.Datamatrix.Encoder.EncoderContext::get_SymbolInfo()
extern "C"  SymbolInfo_t553159586 * EncoderContext_get_SymbolInfo_m2691266372 (EncoderContext_t1774722223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Encoder.EncoderContext::get_NewEncoding()
extern "C"  int32_t EncoderContext_get_NewEncoding_m143688227 (EncoderContext_t1774722223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.Datamatrix.Encoder.EncoderContext::get_Message()
extern "C"  String_t* EncoderContext_get_Message_m2089913004 (EncoderContext_t1774722223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
