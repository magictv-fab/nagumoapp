﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedList_1_Enu3528606100MethodDeclarations.h"

// System.Void System.Collections.Generic.LinkedList`1/Enumerator<ZXing.Aztec.Internal.Token>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Enumerator__ctor_m2038788326(__this, ___info0, ___context1, method) ((  void (*) (Enumerator_t2423997084 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Enumerator__ctor_m2035556075_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<ZXing.Aztec.Internal.Token>::.ctor(System.Collections.Generic.LinkedList`1<T>)
#define Enumerator__ctor_m4045307318(__this, ___parent0, method) ((  void (*) (Enumerator_t2423997084 *, LinkedList_1_t940842524 *, const MethodInfo*))Enumerator__ctor_m857368315_gshared)(__this, ___parent0, method)
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<ZXing.Aztec.Internal.Token>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3330934507(__this, method) ((  Il2CppObject * (*) (Enumerator_t2423997084 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1753810300_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<ZXing.Aztec.Internal.Token>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3211116341(__this, method) ((  void (*) (Enumerator_t2423997084 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4062113552_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<ZXing.Aztec.Internal.Token>::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Enumerator_System_Runtime_Serialization_ISerializable_GetObjectData_m3726659782(__this, ___info0, ___context1, method) ((  void (*) (Enumerator_t2423997084 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Enumerator_System_Runtime_Serialization_ISerializable_GetObjectData_m3585730209_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<ZXing.Aztec.Internal.Token>::System.Runtime.Serialization.IDeserializationCallback.OnDeserialization(System.Object)
#define Enumerator_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m3180119217(__this, ___sender0, method) ((  void (*) (Enumerator_t2423997084 *, Il2CppObject *, const MethodInfo*))Enumerator_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m407024268_gshared)(__this, ___sender0, method)
// T System.Collections.Generic.LinkedList`1/Enumerator<ZXing.Aztec.Internal.Token>::get_Current()
#define Enumerator_get_Current_m3223226564(__this, method) ((  Token_t3066207355 * (*) (Enumerator_t2423997084 *, const MethodInfo*))Enumerator_get_Current_m1124073047_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<ZXing.Aztec.Internal.Token>::MoveNext()
#define Enumerator_MoveNext_m3725979160(__this, method) ((  bool (*) (Enumerator_t2423997084 *, const MethodInfo*))Enumerator_MoveNext_m2358966120_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<ZXing.Aztec.Internal.Token>::Dispose()
#define Enumerator_Dispose_m2973042722(__this, method) ((  void (*) (Enumerator_t2423997084 *, const MethodInfo*))Enumerator_Dispose_m272587367_gshared)(__this, method)
