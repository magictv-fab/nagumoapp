﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ISampleAppUIEventHandler
struct ISampleAppUIEventHandler_t3468374354;

#include "codegen/il2cpp-codegen.h"

// System.Void ISampleAppUIEventHandler::.ctor()
extern "C"  void ISampleAppUIEventHandler__ctor_m1662237065 (ISampleAppUIEventHandler_t3468374354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
