﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetDrag
struct SetDrag_t3798420676;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetDrag::.ctor()
extern "C"  void SetDrag__ctor_m4051332850 (SetDrag_t3798420676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetDrag::Reset()
extern "C"  void SetDrag_Reset_m1697765791 (SetDrag_t3798420676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetDrag::OnEnter()
extern "C"  void SetDrag_OnEnter_m1482227273 (SetDrag_t3798420676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetDrag::OnUpdate()
extern "C"  void SetDrag_OnUpdate_m2132931866 (SetDrag_t3798420676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetDrag::DoSetDrag()
extern "C"  void SetDrag_DoSetDrag_m1898363771 (SetDrag_t3798420676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
