﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.RGBLuminanceSource
struct RGBLuminanceSource_t3258519836;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// ZXing.LuminanceSource
struct LuminanceSource_t1231523093;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_RGBLuminanceSource_BitmapFormat3665922245.h"

// System.Void ZXing.RGBLuminanceSource::.ctor(System.Int32,System.Int32)
extern "C"  void RGBLuminanceSource__ctor_m2991886683 (RGBLuminanceSource_t3258519836 * __this, int32_t ___width0, int32_t ___height1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.RGBLuminanceSource::.ctor(System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat)
extern "C"  void RGBLuminanceSource__ctor_m3721951397 (RGBLuminanceSource_t3258519836 * __this, ByteU5BU5D_t4260760469* ___rgbRawBytes0, int32_t ___width1, int32_t ___height2, int32_t ___bitmapFormat3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.LuminanceSource ZXing.RGBLuminanceSource::CreateLuminanceSource(System.Byte[],System.Int32,System.Int32)
extern "C"  LuminanceSource_t1231523093 * RGBLuminanceSource_CreateLuminanceSource_m2806820275 (RGBLuminanceSource_t3258519836 * __this, ByteU5BU5D_t4260760469* ___newLuminances0, int32_t ___width1, int32_t ___height2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.RGBLuminanceSource/BitmapFormat ZXing.RGBLuminanceSource::DetermineBitmapFormat(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t RGBLuminanceSource_DetermineBitmapFormat_m1245877845 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___rgbRawBytes0, int32_t ___width1, int32_t ___height2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.RGBLuminanceSource::CalculateLuminance(System.Byte[],ZXing.RGBLuminanceSource/BitmapFormat)
extern "C"  void RGBLuminanceSource_CalculateLuminance_m3916696453 (RGBLuminanceSource_t3258519836 * __this, ByteU5BU5D_t4260760469* ___rgbRawBytes0, int32_t ___bitmapFormat1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.RGBLuminanceSource::CalculateLuminanceRGB565(System.Byte[])
extern "C"  void RGBLuminanceSource_CalculateLuminanceRGB565_m2333399799 (RGBLuminanceSource_t3258519836 * __this, ByteU5BU5D_t4260760469* ___rgb565RawData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.RGBLuminanceSource::CalculateLuminanceRGB24(System.Byte[])
extern "C"  void RGBLuminanceSource_CalculateLuminanceRGB24_m1119619779 (RGBLuminanceSource_t3258519836 * __this, ByteU5BU5D_t4260760469* ___rgbRawBytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.RGBLuminanceSource::CalculateLuminanceBGR24(System.Byte[])
extern "C"  void RGBLuminanceSource_CalculateLuminanceBGR24_m297584323 (RGBLuminanceSource_t3258519836 * __this, ByteU5BU5D_t4260760469* ___rgbRawBytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.RGBLuminanceSource::CalculateLuminanceRGB32(System.Byte[])
extern "C"  void RGBLuminanceSource_CalculateLuminanceRGB32_m3493997830 (RGBLuminanceSource_t3258519836 * __this, ByteU5BU5D_t4260760469* ___rgbRawBytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.RGBLuminanceSource::CalculateLuminanceBGR32(System.Byte[])
extern "C"  void RGBLuminanceSource_CalculateLuminanceBGR32_m2671962374 (RGBLuminanceSource_t3258519836 * __this, ByteU5BU5D_t4260760469* ___rgbRawBytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.RGBLuminanceSource::CalculateLuminanceBGRA32(System.Byte[])
extern "C"  void RGBLuminanceSource_CalculateLuminanceBGRA32_m1011811531 (RGBLuminanceSource_t3258519836 * __this, ByteU5BU5D_t4260760469* ___rgbRawBytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.RGBLuminanceSource::CalculateLuminanceRGBA32(System.Byte[])
extern "C"  void RGBLuminanceSource_CalculateLuminanceRGBA32_m725106891 (RGBLuminanceSource_t3258519836 * __this, ByteU5BU5D_t4260760469* ___rgbRawBytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.RGBLuminanceSource::CalculateLuminanceARGB32(System.Byte[])
extern "C"  void RGBLuminanceSource_CalculateLuminanceARGB32_m300545075 (RGBLuminanceSource_t3258519836 * __this, ByteU5BU5D_t4260760469* ___rgbRawBytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
