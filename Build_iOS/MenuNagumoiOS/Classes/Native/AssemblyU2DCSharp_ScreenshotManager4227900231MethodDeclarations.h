﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScreenshotManager
struct ScreenshotManager_t4227900231;
// System.Action`1<UnityEngine.Texture2D>
struct Action_1_t4279924331;
// System.Action`1<System.String>
struct Action_1_t403047693;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "AssemblyU2DCSharp_ScreenshotManager_ImageType30728781.h"

// System.Void ScreenshotManager::.ctor()
extern "C"  void ScreenshotManager__ctor_m2671299076 (ScreenshotManager_t4227900231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenshotManager::.cctor()
extern "C"  void ScreenshotManager__cctor_m723796521 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenshotManager::add_OnScreenshotTaken(System.Action`1<UnityEngine.Texture2D>)
extern "C"  void ScreenshotManager_add_OnScreenshotTaken_m3924850792 (Il2CppObject * __this /* static, unused */, Action_1_t4279924331 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenshotManager::remove_OnScreenshotTaken(System.Action`1<UnityEngine.Texture2D>)
extern "C"  void ScreenshotManager_remove_OnScreenshotTaken_m3189087209 (Il2CppObject * __this /* static, unused */, Action_1_t4279924331 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenshotManager::add_OnScreenshotSaved(System.Action`1<System.String>)
extern "C"  void ScreenshotManager_add_OnScreenshotSaved_m1287076354 (Il2CppObject * __this /* static, unused */, Action_1_t403047693 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenshotManager::remove_OnScreenshotSaved(System.Action`1<System.String>)
extern "C"  void ScreenshotManager_remove_OnScreenshotSaved_m4040453763 (Il2CppObject * __this /* static, unused */, Action_1_t403047693 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenshotManager::add_OnImageSaved(System.Action`1<System.String>)
extern "C"  void ScreenshotManager_add_OnImageSaved_m4152581093 (Il2CppObject * __this /* static, unused */, Action_1_t403047693 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenshotManager::remove_OnImageSaved(System.Action`1<System.String>)
extern "C"  void ScreenshotManager_remove_OnImageSaved_m2711984836 (Il2CppObject * __this /* static, unused */, Action_1_t403047693 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ScreenshotManager::saveToGallery(System.String)
extern "C"  int32_t ScreenshotManager_saveToGallery_m1350967704 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ScreenshotManager ScreenshotManager::get_Instance()
extern "C"  ScreenshotManager_t4227900231 * ScreenshotManager_get_Instance_m2723457074 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenshotManager::Awake()
extern "C"  void ScreenshotManager_Awake_m2908904295 (ScreenshotManager_t4227900231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenshotManager::SaveScreenshot(System.String,System.String,System.String,UnityEngine.Rect)
extern "C"  void ScreenshotManager_SaveScreenshot_m3268692564 (Il2CppObject * __this /* static, unused */, String_t* ___fileName0, String_t* ___albumName1, String_t* ___fileType2, Rect_t4241904616  ___screenArea3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ScreenshotManager::GrabScreenshot(System.String,System.String,System.String,UnityEngine.Rect)
extern "C"  Il2CppObject * ScreenshotManager_GrabScreenshot_m1218038427 (ScreenshotManager_t4227900231 * __this, String_t* ___fileName0, String_t* ___albumName1, String_t* ___fileType2, Rect_t4241904616  ___screenArea3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenshotManager::SaveImage(UnityEngine.Texture2D,System.String,System.String)
extern "C"  void ScreenshotManager_SaveImage_m1565237908 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___texture0, String_t* ___fileName1, String_t* ___fileType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ScreenshotManager::Save(System.Byte[],System.String,System.String,ScreenshotManager/ImageType)
extern "C"  Il2CppObject * ScreenshotManager_Save_m531930795 (ScreenshotManager_t4227900231 * __this, ByteU5BU5D_t4260760469* ___bytes0, String_t* ___fileName1, String_t* ___path2, int32_t ___imageType3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ScreenshotManager::Wait(System.Single)
extern "C"  Il2CppObject * ScreenshotManager_Wait_m3757319454 (ScreenshotManager_t4227900231 * __this, float ___delay0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
