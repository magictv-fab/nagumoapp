﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FirstAccess103307284.h"
#include "AssemblyU2DCSharp_FirstAcessPopup1039578873.h"
#include "AssemblyU2DCSharp_HandAnim65440576.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_Extensions_Horizon885073351.h"
#include "AssemblyU2DCSharp_InfiniteHorizontalScroll3122519653.h"
#include "AssemblyU2DCSharp_InfiniteScroll1391012385.h"
#include "AssemblyU2DCSharp_InstantiateMagazine2106642558.h"
#include "AssemblyU2DCSharp_JC_Slider1089222343.h"
#include "AssemblyU2DCSharp_MinhasOfertasManager1225294387.h"
#include "AssemblyU2DCSharp_MinhasOfertasManager_U3CIMinhaOf4166432221.h"
#include "AssemblyU2DCSharp_MinhasOfertasManager_U3CILoadOfe1789407802.h"
#include "AssemblyU2DCSharp_MinhasOfertasManager_U3CILoadImg3364386919.h"
#include "AssemblyU2DCSharp_MinhasOfertasManager_U3CLoadLoad3716337497.h"
#include "AssemblyU2DCSharp_NotificationObj1964455916.h"
#include "AssemblyU2DCSharp_NotificationsManager3392303557.h"
#include "AssemblyU2DCSharp_NotificationsManager_U3CIAtualiz1703890513.h"
#include "AssemblyU2DCSharp_NotificationsScroll2084299189.h"
#include "AssemblyU2DCSharp_OfertaValues3488850643.h"
#include "AssemblyU2DCSharp_OfertaValues_U3CExcluirFavoritoU3856895931.h"
#include "AssemblyU2DCSharp_OfertaValues_U3CIFavoritarU3Ec__2289274213.h"
#include "AssemblyU2DCSharp_OpenURL401448613.h"
#include "AssemblyU2DCSharp_ScrollBallManager457008001.h"
#include "AssemblyU2DCSharp_ScrollRectTool3827539881.h"
#include "AssemblyU2DCSharp_ScrollRectTool_SelectChildren1389527649.h"
#include "AssemblyU2DCSharp_ScrollRectToolInfinity2171516145.h"
#include "AssemblyU2DCSharp_ScrollRectToolInfinity_SelectChi2120299545.h"
#include "AssemblyU2DCSharp_SelectCover1857328123.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_SetPropertyUtilit1171612705.h"
#include "AssemblyU2DCSharp_SharerPhoto2586188127.h"
#include "AssemblyU2DCSharp_SharerPhoto_U3CdelayU3Ec__Iterat3855746252.h"
#include "AssemblyU2DCSharp_TweenMove2172774588.h"
#include "AssemblyU2DCSharp_WebCamTextureToImage27496160.h"
#include "AssemblyU2DCSharp_nativeShareAndroidScript2018237170.h"
#include "AssemblyU2DCSharp_nativeShareAndroidScript_U3CShar3076450652.h"
#include "AssemblyU2DCSharp_nativeShareAndroidScript_U3CShowP693954555.h"
#include "AssemblyU2DCSharp_shareScriptAndroid2567206277.h"
#include "AssemblyU2DCSharp_shareScriptAndroid_U3CShareScree3116874859.h"
#include "AssemblyU2DCSharp_Area730_Examples_ExampleSceneCon2779674540.h"
#include "AssemblyU2DCSharp_Area730_Notifications_Samples1320174935.h"
#include "AssemblyU2DCSharp_CrossPlatformPushNotificationCon3726717364.h"
#include "AssemblyU2DCSharp_Area730_Notifications_AndroidNot2050093249.h"
#include "AssemblyU2DCSharp_Area730_Notifications_ColorUtils2048080214.h"
#include "AssemblyU2DCSharp_Area730_Notifications_DataHolder1724900414.h"
#include "AssemblyU2DCSharp_Area730_Notifications_Notificati2273303539.h"
#include "AssemblyU2DCSharp_Area730_Notifications_Notificati1374367998.h"
#include "AssemblyU2DCSharp_Area730_Notifications_Notificati3873938600.h"
#include "AssemblyU2DCSharp_Area730_Notifications_Notification76028059.h"
#include "AssemblyU2DCSharp_CaptureAndSaveCamera4072796851.h"
#include "AssemblyU2DCSharp_CaptureAndSaveScreen237891578.h"
#include "AssemblyU2DCSharp_PreviewAndSave2697337644.h"
#include "AssemblyU2DCSharp_AboutScreenView204522622.h"
#include "AssemblyU2DCSharp_AppManager3961229932.h"
#include "AssemblyU2DCSharp_AppManager_ViewType4151742434.h"
#include "AssemblyU2DCSharp_AppManager_U3CLoadAboutPageForFi2969058794.h"
#include "AssemblyU2DCSharp_ISampleAppUIElement2180050874.h"
#include "AssemblyU2DCSharp_ISampleAppUIEventHandler3468374354.h"
#include "AssemblyU2DCSharp_InputController2664602342.h"
#include "AssemblyU2DCSharp_SampleAppUIBox3692040448.h"
#include "AssemblyU2DCSharp_SampleAppUIButton4060007389.h"
#include "AssemblyU2DCSharp_SampleAppUICheckButton4241727631.h"
#include "AssemblyU2DCSharp_SampleAppUIConstants672344260.h"
#include "AssemblyU2DCSharp_SampleAppUILabel416684265.h"
#include "AssemblyU2DCSharp_SampleAppsUILayout3082371642.h"
#include "AssemblyU2DCSharp_SampleAppUIRadioButton3067317570.h"
#include "AssemblyU2DCSharp_SampleAppUIRect2784570703.h"
#include "AssemblyU2DCSharp_SampleInitErrorHandler2792208188.h"
#include "AssemblyU2DCSharp_SampleInitErrorHandler_ErrorData135901855.h"
#include "AssemblyU2DCSharp_SceneViewManager413752636.h"
#include "AssemblyU2DCSharp_SplashScreenView1797787416.h"
#include "AssemblyU2DCSharp_UnionAssets_FLE_EventDispatcher4168900583.h"
#include "AssemblyU2DCSharp_UnionAssets_FLE_EventDispatcherB1248907224.h"
#include "AssemblyU2DCSharp_UnionAssets_FLE_BaseEvent1449158927.h"
#include "AssemblyU2DCSharp_UnionAssets_FLE_CEvent44106931.h"
#include "AssemblyU2DCSharp_MNFeaturePreview1881393139.h"
#include "AssemblyU2DCSharp_MNUseExample1970404964.h"
#include "AssemblyU2DCSharp_MNP76495.h"
#include "AssemblyU2DCSharp_MNPopup1928680331.h"
#include "AssemblyU2DCSharp_MobileNativeDialog811616801.h"
#include "AssemblyU2DCSharp_MobileNativeMessage2985184846.h"
#include "AssemblyU2DCSharp_MobileNativeRateUs1205595255.h"
#include "AssemblyU2DCSharp_MNDialogResult3125317574.h"
#include "AssemblyU2DCSharp_MNAndroidNative3251910469.h"
#include "AssemblyU2DCSharp_MNProxyPool2453359369.h"
#include "AssemblyU2DCSharp_MNAndroidDialog2972443766.h"
#include "AssemblyU2DCSharp_MNAndroidMessage1251344025.h"
#include "AssemblyU2DCSharp_MNAndroidRateUsPopUp2446611808.h"
#include "AssemblyU2DCSharp_MNIOSDialog768531220.h"
#include "AssemblyU2DCSharp_MNIOSMessage1649531835.h"
#include "AssemblyU2DCSharp_MNIOSRateUsPopUp2151207298.h"
#include "AssemblyU2DCSharp_MNIOSNative1047997923.h"
#include "AssemblyU2DCSharp_MNWP8Dialog489019942.h"
#include "AssemblyU2DCSharp_MNWP8Message1574616809.h"
#include "AssemblyU2DCSharp_MNWP8RateUsPopUp4179652016.h"
#include "AssemblyU2DCSharp_BaseWP8Popup3692405086.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_ConsoleBa1938891126.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_LogView1467390519.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_MenuBase1761432360.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_AccessTok4120877346.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3300 = { sizeof (FirstAccess_t103307284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3300[3] = 
{
	FirstAccess_t103307284::get_offset_of_screenFirst_2(),
	FirstAccess_t103307284::get_offset_of_loadScreen_3(),
	FirstAccess_t103307284::get_offset_of_url_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3301 = { sizeof (FirstAcessPopup_t1039578873), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3302 = { sizeof (HandAnim_t65440576), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3303 = { sizeof (HorizontalScrollXSnap_t885073351), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3303[20] = 
{
	HorizontalScrollXSnap_t885073351::get_offset_of__screensContainer_2(),
	HorizontalScrollXSnap_t885073351::get_offset_of__screens_3(),
	HorizontalScrollXSnap_t885073351::get_offset_of__startingScreen_4(),
	HorizontalScrollXSnap_t885073351::get_offset_of__fastSwipeTimer_5(),
	HorizontalScrollXSnap_t885073351::get_offset_of__fastSwipeCounter_6(),
	HorizontalScrollXSnap_t885073351::get_offset_of__fastSwipeTarget_7(),
	HorizontalScrollXSnap_t885073351::get_offset_of__positions_8(),
	HorizontalScrollXSnap_t885073351::get_offset_of__scroll_rect_9(),
	HorizontalScrollXSnap_t885073351::get_offset_of__lerp_target_10(),
	HorizontalScrollXSnap_t885073351::get_offset_of__lerp_11(),
	HorizontalScrollXSnap_t885073351::get_offset_of__containerSize_12(),
	HorizontalScrollXSnap_t885073351::get_offset_of_Pagination_13(),
	HorizontalScrollXSnap_t885073351::get_offset_of_NextButton_14(),
	HorizontalScrollXSnap_t885073351::get_offset_of_PrevButton_15(),
	HorizontalScrollXSnap_t885073351::get_offset_of_UseFastSwipe_16(),
	HorizontalScrollXSnap_t885073351::get_offset_of_FastSwipeThreshold_17(),
	HorizontalScrollXSnap_t885073351::get_offset_of__startDrag_18(),
	HorizontalScrollXSnap_t885073351::get_offset_of__startPosition_19(),
	HorizontalScrollXSnap_t885073351::get_offset_of__currentScreen_20(),
	HorizontalScrollXSnap_t885073351::get_offset_of_fastSwipe_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3304 = { sizeof (InfiniteHorizontalScroll_t3122519653), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3305 = { sizeof (InfiniteScroll_t1391012385), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3305[7] = 
{
	InfiniteScroll_t1391012385::get_offset_of_initOnAwake_38(),
	InfiniteScroll_t1391012385::get_offset_of__t_39(),
	InfiniteScroll_t1391012385::get_offset_of_prefabItems_40(),
	InfiniteScroll_t1391012385::get_offset_of_itemTypeStart_41(),
	InfiniteScroll_t1391012385::get_offset_of_itemTypeEnd_42(),
	InfiniteScroll_t1391012385::get_offset_of_init_43(),
	InfiniteScroll_t1391012385::get_offset_of_dragOffset_44(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3306 = { sizeof (InstantiateMagazine_t2106642558), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3306[1] = 
{
	InstantiateMagazine_t2106642558::get_offset_of_magazineCoverObj_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3307 = { sizeof (JC_Slider_t1089222343), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3307[1] = 
{
	JC_Slider_t1089222343::get_offset_of_StepsIncrement_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3308 = { sizeof (MinhasOfertasManager_t1225294387), -1, sizeof(MinhasOfertasManager_t1225294387_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3308[21] = 
{
	MinhasOfertasManager_t1225294387_StaticFields::get_offset_of_ofertasData_2(),
	MinhasOfertasManager_t1225294387_StaticFields::get_offset_of_instance_3(),
	MinhasOfertasManager_t1225294387_StaticFields::get_offset_of_titleLst_4(),
	MinhasOfertasManager_t1225294387_StaticFields::get_offset_of_valueLst_5(),
	MinhasOfertasManager_t1225294387_StaticFields::get_offset_of_infoLst_6(),
	MinhasOfertasManager_t1225294387_StaticFields::get_offset_of_idLst_7(),
	MinhasOfertasManager_t1225294387_StaticFields::get_offset_of_validadeLst_8(),
	MinhasOfertasManager_t1225294387_StaticFields::get_offset_of_aquisicaoLst_9(),
	MinhasOfertasManager_t1225294387_StaticFields::get_offset_of_unidadesLst_10(),
	MinhasOfertasManager_t1225294387_StaticFields::get_offset_of_pesoLst_11(),
	MinhasOfertasManager_t1225294387_StaticFields::get_offset_of_imgLst_12(),
	MinhasOfertasManager_t1225294387::get_offset_of_loadingObj_13(),
	MinhasOfertasManager_t1225294387::get_offset_of_popup_14(),
	MinhasOfertasManager_t1225294387::get_offset_of_itemPrefab_15(),
	MinhasOfertasManager_t1225294387::get_offset_of_containerItens_16(),
	MinhasOfertasManager_t1225294387_StaticFields::get_offset_of_itensList_17(),
	MinhasOfertasManager_t1225294387::get_offset_of_scrollRect_18(),
	MinhasOfertasManager_t1225294387::get_offset_of_ofertaCounter_19(),
	MinhasOfertasManager_t1225294387::get_offset_of_currentItem_20(),
	MinhasOfertasManager_t1225294387_StaticFields::get_offset_of_currentJsonTxt_21(),
	MinhasOfertasManager_t1225294387_StaticFields::get_offset_of_loadedImgsCount_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3309 = { sizeof (U3CIMinhaOfertaRemoveU3Ec__Iterator1D_t4166432221), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3309[8] = 
{
	U3CIMinhaOfertaRemoveU3Ec__Iterator1D_t4166432221::get_offset_of_id_0(),
	U3CIMinhaOfertaRemoveU3Ec__Iterator1D_t4166432221::get_offset_of_U3CjsonU3E__0_1(),
	U3CIMinhaOfertaRemoveU3Ec__Iterator1D_t4166432221::get_offset_of_U3CheadersU3E__1_2(),
	U3CIMinhaOfertaRemoveU3Ec__Iterator1D_t4166432221::get_offset_of_U3CpDataU3E__2_3(),
	U3CIMinhaOfertaRemoveU3Ec__Iterator1D_t4166432221::get_offset_of_U3CwwwU3E__3_4(),
	U3CIMinhaOfertaRemoveU3Ec__Iterator1D_t4166432221::get_offset_of_U24PC_5(),
	U3CIMinhaOfertaRemoveU3Ec__Iterator1D_t4166432221::get_offset_of_U24current_6(),
	U3CIMinhaOfertaRemoveU3Ec__Iterator1D_t4166432221::get_offset_of_U3CU24U3Eid_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3310 = { sizeof (U3CILoadOfertasU3Ec__Iterator1E_t1789407802), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3310[13] = 
{
	U3CILoadOfertasU3Ec__Iterator1E_t1789407802::get_offset_of_U3CjsonU3E__0_0(),
	U3CILoadOfertasU3Ec__Iterator1E_t1789407802::get_offset_of_U3CheadersU3E__1_1(),
	U3CILoadOfertasU3Ec__Iterator1E_t1789407802::get_offset_of_U3CpDataU3E__2_2(),
	U3CILoadOfertasU3Ec__Iterator1E_t1789407802::get_offset_of_U3CwwwU3E__3_3(),
	U3CILoadOfertasU3Ec__Iterator1E_t1789407802::get_offset_of_U3CU24s_29U3E__4_4(),
	U3CILoadOfertasU3Ec__Iterator1E_t1789407802::get_offset_of_U3CobjU3E__5_5(),
	U3CILoadOfertasU3Ec__Iterator1E_t1789407802::get_offset_of_U3CmessageU3E__6_6(),
	U3CILoadOfertasU3Ec__Iterator1E_t1789407802::get_offset_of_U3CU24s_30U3E__7_7(),
	U3CILoadOfertasU3Ec__Iterator1E_t1789407802::get_offset_of_U3CU24s_31U3E__8_8(),
	U3CILoadOfertasU3Ec__Iterator1E_t1789407802::get_offset_of_U3CofertaDataU3E__9_9(),
	U3CILoadOfertasU3Ec__Iterator1E_t1789407802::get_offset_of_U24PC_10(),
	U3CILoadOfertasU3Ec__Iterator1E_t1789407802::get_offset_of_U24current_11(),
	U3CILoadOfertasU3Ec__Iterator1E_t1789407802::get_offset_of_U3CU3Ef__this_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3311 = { sizeof (U3CILoadImgsU3Ec__Iterator1F_t3364386919), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3311[9] = 
{
	U3CILoadImgsU3Ec__Iterator1F_t3364386919::get_offset_of_U3CiU3E__0_0(),
	U3CILoadImgsU3Ec__Iterator1F_t3364386919::get_offset_of_U3CU24s_32U3E__1_1(),
	U3CILoadImgsU3Ec__Iterator1F_t3364386919::get_offset_of_U3CU24s_33U3E__2_2(),
	U3CILoadImgsU3Ec__Iterator1F_t3364386919::get_offset_of_U3CofertaDataU3E__3_3(),
	U3CILoadImgsU3Ec__Iterator1F_t3364386919::get_offset_of_U3CwwwU3E__4_4(),
	U3CILoadImgsU3Ec__Iterator1F_t3364386919::get_offset_of_U3CsptU3E__5_5(),
	U3CILoadImgsU3Ec__Iterator1F_t3364386919::get_offset_of_U24PC_6(),
	U3CILoadImgsU3Ec__Iterator1F_t3364386919::get_offset_of_U24current_7(),
	U3CILoadImgsU3Ec__Iterator1F_t3364386919::get_offset_of_U3CU3Ef__this_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3312 = { sizeof (U3CLoadLoadedsU3Ec__Iterator20_t3716337497), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3312[3] = 
{
	U3CLoadLoadedsU3Ec__Iterator20_t3716337497::get_offset_of_U24PC_0(),
	U3CLoadLoadedsU3Ec__Iterator20_t3716337497::get_offset_of_U24current_1(),
	U3CLoadLoadedsU3Ec__Iterator20_t3716337497::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3313 = { sizeof (NotificationObj_t1964455916), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3313[6] = 
{
	NotificationObj_t1964455916::get_offset_of_titleText_2(),
	NotificationObj_t1964455916::get_offset_of_textText_3(),
	NotificationObj_t1964455916::get_offset_of_dateText_4(),
	NotificationObj_t1964455916::get_offset_of_title_5(),
	NotificationObj_t1964455916::get_offset_of_text_6(),
	NotificationObj_t1964455916::get_offset_of_dateTime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3314 = { sizeof (NotificationsManager_t3392303557), -1, sizeof(NotificationsManager_t3392303557_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3314[3] = 
{
	NotificationsManager_t3392303557_StaticFields::get_offset_of_oneSignalDeviceKey_2(),
	NotificationsManager_t3392303557::get_offset_of_oneSignalKey_3(),
	NotificationsManager_t3392303557_StaticFields::get_offset_of_extraMessage_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3315 = { sizeof (U3CIAtualizaTokenU3Ec__Iterator21_t1703890513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3315[8] = 
{
	U3CIAtualizaTokenU3Ec__Iterator21_t1703890513::get_offset_of_id_0(),
	U3CIAtualizaTokenU3Ec__Iterator21_t1703890513::get_offset_of_U3CjsonU3E__0_1(),
	U3CIAtualizaTokenU3Ec__Iterator21_t1703890513::get_offset_of_U3CheadersU3E__1_2(),
	U3CIAtualizaTokenU3Ec__Iterator21_t1703890513::get_offset_of_U3CpDataU3E__2_3(),
	U3CIAtualizaTokenU3Ec__Iterator21_t1703890513::get_offset_of_U3CwwwU3E__3_4(),
	U3CIAtualizaTokenU3Ec__Iterator21_t1703890513::get_offset_of_U24PC_5(),
	U3CIAtualizaTokenU3Ec__Iterator21_t1703890513::get_offset_of_U24current_6(),
	U3CIAtualizaTokenU3Ec__Iterator21_t1703890513::get_offset_of_U3CU24U3Eid_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3316 = { sizeof (NotificationsScroll_t2084299189), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3316[5] = 
{
	NotificationsScroll_t2084299189::get_offset_of_notificatonObj_2(),
	NotificationsScroll_t2084299189::get_offset_of_notificationContainer_3(),
	NotificationsScroll_t2084299189::get_offset_of_notificationsManager_4(),
	NotificationsScroll_t2084299189::get_offset_of_objsList_5(),
	NotificationsScroll_t2084299189::get_offset_of_idNotificationsLst_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3317 = { sizeof (OfertaValues_t3488850643), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3317[21] = 
{
	OfertaValues_t3488850643::get_offset_of_title_2(),
	OfertaValues_t3488850643::get_offset_of_info_3(),
	OfertaValues_t3488850643::get_offset_of_value_4(),
	OfertaValues_t3488850643::get_offset_of_validade_5(),
	OfertaValues_t3488850643::get_offset_of_unidades_6(),
	OfertaValues_t3488850643::get_offset_of_aquisicao_7(),
	OfertaValues_t3488850643::get_offset_of_img_8(),
	OfertaValues_t3488850643::get_offset_of_favoritoImg_9(),
	OfertaValues_t3488850643::get_offset_of_btn_10(),
	OfertaValues_t3488850643::get_offset_of_id_11(),
	OfertaValues_t3488850643::get_offset_of_saveImageSource_12(),
	OfertaValues_t3488850643::get_offset_of_favoritou_13(),
	OfertaValues_t3488850643::get_offset_of_favoritoSprite_14(),
	OfertaValues_t3488850643::get_offset_of_desfavoritouSprite_15(),
	OfertaValues_t3488850643::get_offset_of_favoritouObj_16(),
	OfertaValues_t3488850643::get_offset_of_btnFavoritado_17(),
	OfertaValues_t3488850643::get_offset_of_btnFavoritar_18(),
	OfertaValues_t3488850643::get_offset_of_popupSemConexao_19(),
	OfertaValues_t3488850643::get_offset_of_showPopup_20(),
	OfertaValues_t3488850643::get_offset_of_link_21(),
	OfertaValues_t3488850643::get_offset_of_imgLink_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3318 = { sizeof (U3CExcluirFavoritoU3Ec__Iterator22_t3856895931), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3318[8] = 
{
	U3CExcluirFavoritoU3Ec__Iterator22_t3856895931::get_offset_of_id_0(),
	U3CExcluirFavoritoU3Ec__Iterator22_t3856895931::get_offset_of_U3CjsonU3E__0_1(),
	U3CExcluirFavoritoU3Ec__Iterator22_t3856895931::get_offset_of_U3CheadersU3E__1_2(),
	U3CExcluirFavoritoU3Ec__Iterator22_t3856895931::get_offset_of_U3CpDataU3E__2_3(),
	U3CExcluirFavoritoU3Ec__Iterator22_t3856895931::get_offset_of_U3CwwwU3E__3_4(),
	U3CExcluirFavoritoU3Ec__Iterator22_t3856895931::get_offset_of_U24PC_5(),
	U3CExcluirFavoritoU3Ec__Iterator22_t3856895931::get_offset_of_U24current_6(),
	U3CExcluirFavoritoU3Ec__Iterator22_t3856895931::get_offset_of_U3CU24U3Eid_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3319 = { sizeof (U3CIFavoritarU3Ec__Iterator23_t2289274213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3319[9] = 
{
	U3CIFavoritarU3Ec__Iterator23_t2289274213::get_offset_of_id_0(),
	U3CIFavoritarU3Ec__Iterator23_t2289274213::get_offset_of_U3CjsonU3E__0_1(),
	U3CIFavoritarU3Ec__Iterator23_t2289274213::get_offset_of_U3CheadersU3E__1_2(),
	U3CIFavoritarU3Ec__Iterator23_t2289274213::get_offset_of_U3CpDataU3E__2_3(),
	U3CIFavoritarU3Ec__Iterator23_t2289274213::get_offset_of_U3CwwwU3E__3_4(),
	U3CIFavoritarU3Ec__Iterator23_t2289274213::get_offset_of_U24PC_5(),
	U3CIFavoritarU3Ec__Iterator23_t2289274213::get_offset_of_U24current_6(),
	U3CIFavoritarU3Ec__Iterator23_t2289274213::get_offset_of_U3CU24U3Eid_7(),
	U3CIFavoritarU3Ec__Iterator23_t2289274213::get_offset_of_U3CU3Ef__this_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3320 = { sizeof (OpenURL_t401448613), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3320[1] = 
{
	OpenURL_t401448613::get_offset_of_url_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3321 = { sizeof (ScrollBallManager_t457008001), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3321[4] = 
{
	ScrollBallManager_t457008001::get_offset_of_scrollRect_2(),
	ScrollBallManager_t457008001::get_offset_of_xPos_3(),
	ScrollBallManager_t457008001::get_offset_of_xSize_4(),
	ScrollBallManager_t457008001::get_offset_of_scrollbar_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3322 = { sizeof (ScrollRectTool_t3827539881), -1, sizeof(ScrollRectTool_t3827539881_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3322[20] = 
{
	ScrollRectTool_t3827539881_StaticFields::get_offset_of_instance_2(),
	ScrollRectTool_t3827539881::get_offset_of_changeColorScale_3(),
	ScrollRectTool_t3827539881::get_offset_of_snapMinVel_4(),
	ScrollRectTool_t3827539881::get_offset_of_rotMultiplayer_5(),
	ScrollRectTool_t3827539881::get_offset_of_selectChildren_6(),
	ScrollRectTool_t3827539881::get_offset_of_scaleFront_7(),
	ScrollRectTool_t3827539881::get_offset_of_scaleBack_8(),
	ScrollRectTool_t3827539881::get_offset_of_sideCells_9(),
	ScrollRectTool_t3827539881::get_offset_of_center_10(),
	ScrollRectTool_t3827539881::get_offset_of_backCells_11(),
	ScrollRectTool_t3827539881::get_offset_of_grid_12(),
	ScrollRectTool_t3827539881::get_offset_of_rect_13(),
	ScrollRectTool_t3827539881::get_offset_of_scrollRect_14(),
	ScrollRectTool_t3827539881::get_offset_of_targetPos_15(),
	ScrollRectTool_t3827539881::get_offset_of_done_16(),
	ScrollRectTool_t3827539881::get_offset_of_t_17(),
	ScrollRectTool_t3827539881::get_offset_of_desaceleration_18(),
	ScrollRectTool_t3827539881::get_offset_of_tmpChild_19(),
	ScrollRectTool_t3827539881::get_offset_of_fixCelSize_20(),
	ScrollRectTool_t3827539881::get_offset_of_fixPair_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3323 = { sizeof (SelectChildren_t1389527649), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3324 = { sizeof (ScrollRectToolInfinity_t2171516145), -1, sizeof(ScrollRectToolInfinity_t2171516145_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3324[5] = 
{
	ScrollRectToolInfinity_t2171516145_StaticFields::get_offset_of_selectChildren_2(),
	ScrollRectToolInfinity_t2171516145::get_offset_of_snapMinVel_3(),
	ScrollRectToolInfinity_t2171516145::get_offset_of_rect_4(),
	ScrollRectToolInfinity_t2171516145::get_offset_of_scrollRect_5(),
	ScrollRectToolInfinity_t2171516145::get_offset_of_desaceleration_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3325 = { sizeof (SelectChildren_t2120299545), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3326 = { sizeof (SelectCover_t1857328123), -1, sizeof(SelectCover_t1857328123_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3326[2] = 
{
	SelectCover_t1857328123_StaticFields::get_offset_of_clickedName_2(),
	SelectCover_t1857328123_StaticFields::get_offset_of_clickedEdition_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3327 = { sizeof (SetPropertyUtility_t1171612706), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3328 = { sizeof (SharerPhoto_t2586188127), -1, sizeof(SharerPhoto_t2586188127_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3328[7] = 
{
	SharerPhoto_t2586188127_StaticFields::get_offset_of_awardIndex_2(),
	SharerPhoto_t2586188127::get_offset_of_frameLst_3(),
	SharerPhoto_t2586188127::get_offset_of_tex_4(),
	SharerPhoto_t2586188127::get_offset_of_imageRefCut_5(),
	SharerPhoto_t2586188127::get_offset_of_take_6(),
	SharerPhoto_t2586188127::get_offset_of_parabensPanel_7(),
	SharerPhoto_t2586188127::get_offset_of_loadingPanel_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3329 = { sizeof (U3CdelayU3Ec__Iterator24_t3855746252), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3329[3] = 
{
	U3CdelayU3Ec__Iterator24_t3855746252::get_offset_of_U24PC_0(),
	U3CdelayU3Ec__Iterator24_t3855746252::get_offset_of_U24current_1(),
	U3CdelayU3Ec__Iterator24_t3855746252::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3330 = { sizeof (TweenMove_t2172774588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3330[5] = 
{
	TweenMove_t2172774588::get_offset_of_easetype_2(),
	TweenMove_t2172774588::get_offset_of_position_3(),
	TweenMove_t2172774588::get_offset_of_time_4(),
	TweenMove_t2172774588::get_offset_of_startPositon_5(),
	TweenMove_t2172774588::get_offset_of_flipflop_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3331 = { sizeof (WebCamTextureToImage_t27496160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3331[1] = 
{
	WebCamTextureToImage_t27496160::get_offset_of_webCamTexture_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3332 = { sizeof (nativeShareAndroidScript_t2018237170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3332[5] = 
{
	nativeShareAndroidScript_t2018237170::get_offset_of_CanvasShareObj_2(),
	nativeShareAndroidScript_t2018237170::get_offset_of_isProcessing_3(),
	nativeShareAndroidScript_t2018237170::get_offset_of_isFocus_4(),
	nativeShareAndroidScript_t2018237170::get_offset_of_marginImage_5(),
	nativeShareAndroidScript_t2018237170::get_offset_of_marginImageBottom_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3333 = { sizeof (U3CShareScreenshotU3Ec__Iterator25_t3076450652), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3333[13] = 
{
	U3CShareScreenshotU3Ec__Iterator25_t3076450652::get_offset_of_U3CtexU3E__0_0(),
	U3CShareScreenshotU3Ec__Iterator25_t3076450652::get_offset_of_U3CdestinationU3E__1_1(),
	U3CShareScreenshotU3Ec__Iterator25_t3076450652::get_offset_of_U3CbytesU3E__2_2(),
	U3CShareScreenshotU3Ec__Iterator25_t3076450652::get_offset_of_U3CintentClassU3E__3_3(),
	U3CShareScreenshotU3Ec__Iterator25_t3076450652::get_offset_of_U3CintentObjectU3E__4_4(),
	U3CShareScreenshotU3Ec__Iterator25_t3076450652::get_offset_of_U3CuriClassU3E__5_5(),
	U3CShareScreenshotU3Ec__Iterator25_t3076450652::get_offset_of_U3CuriObjectU3E__6_6(),
	U3CShareScreenshotU3Ec__Iterator25_t3076450652::get_offset_of_U3CunityU3E__7_7(),
	U3CShareScreenshotU3Ec__Iterator25_t3076450652::get_offset_of_U3CcurrentActivityU3E__8_8(),
	U3CShareScreenshotU3Ec__Iterator25_t3076450652::get_offset_of_U3CchooserU3E__9_9(),
	U3CShareScreenshotU3Ec__Iterator25_t3076450652::get_offset_of_U24PC_10(),
	U3CShareScreenshotU3Ec__Iterator25_t3076450652::get_offset_of_U24current_11(),
	U3CShareScreenshotU3Ec__Iterator25_t3076450652::get_offset_of_U3CU3Ef__this_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3334 = { sizeof (U3CShowPanelOkU3Ec__Iterator26_t693954555), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3334[2] = 
{
	U3CShowPanelOkU3Ec__Iterator26_t693954555::get_offset_of_U24PC_0(),
	U3CShowPanelOkU3Ec__Iterator26_t693954555::get_offset_of_U24current_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3335 = { sizeof (shareScriptAndroid_t2567206277), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3335[3] = 
{
	shareScriptAndroid_t2567206277::get_offset_of_CanvasShareObj_2(),
	shareScriptAndroid_t2567206277::get_offset_of_isProcessing_3(),
	shareScriptAndroid_t2567206277::get_offset_of_isFocus_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3336 = { sizeof (U3CShareScreenshotU3Ec__Iterator27_t3116874859), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3336[11] = 
{
	U3CShareScreenshotU3Ec__Iterator27_t3116874859::get_offset_of_U3CdestinationU3E__0_0(),
	U3CShareScreenshotU3Ec__Iterator27_t3116874859::get_offset_of_U3CintentClassU3E__1_1(),
	U3CShareScreenshotU3Ec__Iterator27_t3116874859::get_offset_of_U3CintentObjectU3E__2_2(),
	U3CShareScreenshotU3Ec__Iterator27_t3116874859::get_offset_of_U3CuriClassU3E__3_3(),
	U3CShareScreenshotU3Ec__Iterator27_t3116874859::get_offset_of_U3CuriObjectU3E__4_4(),
	U3CShareScreenshotU3Ec__Iterator27_t3116874859::get_offset_of_U3CunityU3E__5_5(),
	U3CShareScreenshotU3Ec__Iterator27_t3116874859::get_offset_of_U3CcurrentActivityU3E__6_6(),
	U3CShareScreenshotU3Ec__Iterator27_t3116874859::get_offset_of_U3CchooserU3E__7_7(),
	U3CShareScreenshotU3Ec__Iterator27_t3116874859::get_offset_of_U24PC_8(),
	U3CShareScreenshotU3Ec__Iterator27_t3116874859::get_offset_of_U24current_9(),
	U3CShareScreenshotU3Ec__Iterator27_t3116874859::get_offset_of_U3CU3Ef__this_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3337 = { sizeof (ExampleSceneController_t2779674540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3337[21] = 
{
	ExampleSceneController_t2779674540::get_offset_of_versionLabel_2(),
	ExampleSceneController_t2779674540::get_offset_of_titlefield_3(),
	ExampleSceneController_t2779674540::get_offset_of_bodyField_4(),
	ExampleSceneController_t2779674540::get_offset_of_tickerField_5(),
	ExampleSceneController_t2779674540::get_offset_of_delayField_6(),
	ExampleSceneController_t2779674540::get_offset_of_idField_7(),
	ExampleSceneController_t2779674540::get_offset_of_intervalField_8(),
	ExampleSceneController_t2779674540::get_offset_of_alertOnce_9(),
	ExampleSceneController_t2779674540::get_offset_of_sound_10(),
	ExampleSceneController_t2779674540::get_offset_of_vibro_11(),
	ExampleSceneController_t2779674540::get_offset_of_autoCancel_12(),
	ExampleSceneController_t2779674540::get_offset_of_repeating_13(),
	ExampleSceneController_t2779674540::get_offset_of_colorField_14(),
	ExampleSceneController_t2779674540::get_offset_of_title_15(),
	ExampleSceneController_t2779674540::get_offset_of_body_16(),
	ExampleSceneController_t2779674540::get_offset_of_ticker_17(),
	ExampleSceneController_t2779674540::get_offset_of_delay_18(),
	ExampleSceneController_t2779674540::get_offset_of_id_19(),
	ExampleSceneController_t2779674540::get_offset_of_interval_20(),
	ExampleSceneController_t2779674540::get_offset_of_flags_21(),
	ExampleSceneController_t2779674540::get_offset_of_color_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3338 = { sizeof (Samples_t1320174935), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3339 = { sizeof (CrossPlatformPushNotificationController_t3726717364), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3339[3] = 
{
	CrossPlatformPushNotificationController_t3726717364::get_offset_of__IOSOneSignalId_2(),
	CrossPlatformPushNotificationController_t3726717364::get_offset_of__androidOneSignalId_3(),
	CrossPlatformPushNotificationController_t3726717364::get_offset_of__gmsAppId_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3340 = { sizeof (AndroidNotifications_t2050093249), -1, sizeof(AndroidNotifications_t2050093249_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3340[1] = 
{
	AndroidNotifications_t2050093249_StaticFields::get_offset_of__dataHolder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3341 = { sizeof (ColorUtils_t2048080214), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3342 = { sizeof (DataHolder_t1724900414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3342[2] = 
{
	DataHolder_t1724900414::get_offset_of_notifications_2(),
	DataHolder_t1724900414::get_offset_of_unityClass_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3343 = { sizeof (Notification_t2273303539), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3343[19] = 
{
	Notification_t2273303539::get_offset_of__id_0(),
	Notification_t2273303539::get_offset_of__smallIcon_1(),
	Notification_t2273303539::get_offset_of__largeIcon_2(),
	Notification_t2273303539::get_offset_of__defaults_3(),
	Notification_t2273303539::get_offset_of__autoCancel_4(),
	Notification_t2273303539::get_offset_of__sound_5(),
	Notification_t2273303539::get_offset_of__ticker_6(),
	Notification_t2273303539::get_offset_of__title_7(),
	Notification_t2273303539::get_offset_of__body_8(),
	Notification_t2273303539::get_offset_of__vibratePattern_9(),
	Notification_t2273303539::get_offset_of__when_10(),
	Notification_t2273303539::get_offset_of__delay_11(),
	Notification_t2273303539::get_offset_of__isRepeating_12(),
	Notification_t2273303539::get_offset_of__interval_13(),
	Notification_t2273303539::get_offset_of__number_14(),
	Notification_t2273303539::get_offset_of__alertOnce_15(),
	Notification_t2273303539::get_offset_of__color_16(),
	Notification_t2273303539::get_offset_of__group_17(),
	Notification_t2273303539::get_offset_of__sortKey_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3344 = { sizeof (NotificationBuilder_t1374367998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3344[23] = 
{
	0,
	0,
	0,
	0,
	NotificationBuilder_t1374367998::get_offset_of__id_4(),
	NotificationBuilder_t1374367998::get_offset_of__smallIcon_5(),
	NotificationBuilder_t1374367998::get_offset_of__largeIcon_6(),
	NotificationBuilder_t1374367998::get_offset_of__defaults_7(),
	NotificationBuilder_t1374367998::get_offset_of__autoCancel_8(),
	NotificationBuilder_t1374367998::get_offset_of__sound_9(),
	NotificationBuilder_t1374367998::get_offset_of__ticker_10(),
	NotificationBuilder_t1374367998::get_offset_of__title_11(),
	NotificationBuilder_t1374367998::get_offset_of__body_12(),
	NotificationBuilder_t1374367998::get_offset_of__vibratePattern_13(),
	NotificationBuilder_t1374367998::get_offset_of__when_14(),
	NotificationBuilder_t1374367998::get_offset_of__delay_15(),
	NotificationBuilder_t1374367998::get_offset_of__isRepeating_16(),
	NotificationBuilder_t1374367998::get_offset_of__interval_17(),
	NotificationBuilder_t1374367998::get_offset_of__number_18(),
	NotificationBuilder_t1374367998::get_offset_of__alertOnce_19(),
	NotificationBuilder_t1374367998::get_offset_of__color_20(),
	NotificationBuilder_t1374367998::get_offset_of__group_21(),
	NotificationBuilder_t1374367998::get_offset_of__sortKey_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3345 = { sizeof (NotificationInstance_t3873938600), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3345[25] = 
{
	NotificationInstance_t3873938600::get_offset_of_name_0(),
	NotificationInstance_t3873938600::get_offset_of_smallIcon_1(),
	NotificationInstance_t3873938600::get_offset_of_largeIcon_2(),
	NotificationInstance_t3873938600::get_offset_of_title_3(),
	NotificationInstance_t3873938600::get_offset_of_body_4(),
	NotificationInstance_t3873938600::get_offset_of_ticker_5(),
	NotificationInstance_t3873938600::get_offset_of_autoCancel_6(),
	NotificationInstance_t3873938600::get_offset_of_isRepeating_7(),
	NotificationInstance_t3873938600::get_offset_of_intervalHours_8(),
	NotificationInstance_t3873938600::get_offset_of_intervalMinutes_9(),
	NotificationInstance_t3873938600::get_offset_of_intervalSeconds_10(),
	NotificationInstance_t3873938600::get_offset_of_alertOnce_11(),
	NotificationInstance_t3873938600::get_offset_of_number_12(),
	NotificationInstance_t3873938600::get_offset_of_delayHours_13(),
	NotificationInstance_t3873938600::get_offset_of_delayMinutes_14(),
	NotificationInstance_t3873938600::get_offset_of_delaySeconds_15(),
	NotificationInstance_t3873938600::get_offset_of_defaultSound_16(),
	NotificationInstance_t3873938600::get_offset_of_defaultVibrate_17(),
	NotificationInstance_t3873938600::get_offset_of_soundFile_18(),
	NotificationInstance_t3873938600::get_offset_of_vibroPattern_19(),
	NotificationInstance_t3873938600::get_offset_of_group_20(),
	NotificationInstance_t3873938600::get_offset_of_sortKey_21(),
	NotificationInstance_t3873938600::get_offset_of_hasColor_22(),
	NotificationInstance_t3873938600::get_offset_of_color_23(),
	NotificationInstance_t3873938600::get_offset_of_id_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3346 = { sizeof (NotificationTracker_t76028059), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3346[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3347 = { sizeof (CaptureAndSaveCamera_t4072796851), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3348 = { sizeof (CaptureAndSaveScreen_t237891578), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3349 = { sizeof (PreviewAndSave_t2697337644), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3350 = { sizeof (AboutScreenView_t204522622), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3350[14] = 
{
	0,
	0,
	AboutScreenView_t204522622::get_offset_of_m_AboutText_2(),
	AboutScreenView_t204522622::get_offset_of_OnStartButtonTapped_3(),
	AboutScreenView_t204522622::get_offset_of_mAboutTitleBgStyle_4(),
	AboutScreenView_t204522622::get_offset_of_mOKButtonBgStyle_5(),
	AboutScreenView_t204522622::get_offset_of_mTitle_6(),
	AboutScreenView_t204522622::get_offset_of_mUISkin_7(),
	AboutScreenView_t204522622::get_offset_of_mButtonGUIStyles_8(),
	AboutScreenView_t204522622::get_offset_of_mScrollPosition_9(),
	AboutScreenView_t204522622::get_offset_of_mStartButtonAreaHeight_10(),
	AboutScreenView_t204522622::get_offset_of_mAboutTitleHeight_11(),
	AboutScreenView_t204522622::get_offset_of_mLastTouchPosition_12(),
	AboutScreenView_t204522622::get_offset_of_mBox_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3351 = { sizeof (AppManager_t3961229932), -1, sizeof(AppManager_t3961229932_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3351[6] = 
{
	AppManager_t3961229932::get_offset_of_TitleForAboutPage_2(),
	AppManager_t3961229932::get_offset_of_m_UIEventHandler_3(),
	AppManager_t3961229932_StaticFields::get_offset_of_mActiveViewType_4(),
	AppManager_t3961229932::get_offset_of_mSplashView_5(),
	AppManager_t3961229932::get_offset_of_mAboutView_6(),
	AppManager_t3961229932::get_offset_of_mSecondsVisible_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3352 = { sizeof (ViewType_t4151742434)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3352[5] = 
{
	ViewType_t4151742434::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3353 = { sizeof (U3CLoadAboutPageForFirstTimeU3Ec__Iterator28_t2969058794), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3353[3] = 
{
	U3CLoadAboutPageForFirstTimeU3Ec__Iterator28_t2969058794::get_offset_of_U24PC_0(),
	U3CLoadAboutPageForFirstTimeU3Ec__Iterator28_t2969058794::get_offset_of_U24current_1(),
	U3CLoadAboutPageForFirstTimeU3Ec__Iterator28_t2969058794::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3354 = { sizeof (ISampleAppUIElement_t2180050874), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3355 = { sizeof (ISampleAppUIEventHandler_t3468374354), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3356 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3357 = { sizeof (InputController_t2664602342), -1, sizeof(InputController_t2664602342_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3357[13] = 
{
	InputController_t2664602342_StaticFields::get_offset_of_GUIInput_0(),
	InputController_t2664602342_StaticFields::get_offset_of_DoubleTapped_1(),
	InputController_t2664602342_StaticFields::get_offset_of_SingleTapped_2(),
	InputController_t2664602342_StaticFields::get_offset_of_BackButtonTapped_3(),
	InputController_t2664602342_StaticFields::get_offset_of_timeSinceBackEventDispatched_4(),
	InputController_t2664602342_StaticFields::get_offset_of_backButtonEventDispached_5(),
	InputController_t2664602342_StaticFields::get_offset_of_tapEventDispatched_6(),
	InputController_t2664602342_StaticFields::get_offset_of_mMillisecondsSinceLastTap_7(),
	InputController_t2664602342_StaticFields::get_offset_of_MAX_TAP_MILLISEC_8(),
	InputController_t2664602342_StaticFields::get_offset_of_MAX_TAP_DISTANCE_SCREEN_SPACE_9(),
	InputController_t2664602342_StaticFields::get_offset_of_mWaitingForSecondTap_10(),
	InputController_t2664602342_StaticFields::get_offset_of_mFirstTapPosition_11(),
	InputController_t2664602342_StaticFields::get_offset_of_mFirstTapTime_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3358 = { sizeof (SampleAppUIBox_t3692040448), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3358[2] = 
{
	SampleAppUIBox_t3692040448::get_offset_of_mRect_0(),
	SampleAppUIBox_t3692040448::get_offset_of_mStyle_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3359 = { sizeof (SampleAppUIButton_t4060007389), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3359[5] = 
{
	SampleAppUIButton_t4060007389::get_offset_of_mButtonImage_0(),
	SampleAppUIButton_t4060007389::get_offset_of_mRect_1(),
	SampleAppUIButton_t4060007389::get_offset_of_mStyle_2(),
	SampleAppUIButton_t4060007389::get_offset_of_mTitle_3(),
	SampleAppUIButton_t4060007389::get_offset_of_TappedOn_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3360 = { sizeof (SampleAppUICheckButton_t4241727631), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3360[8] = 
{
	SampleAppUICheckButton_t4241727631::get_offset_of_mRect_0(),
	SampleAppUICheckButton_t4241727631::get_offset_of_mTappedOn_1(),
	SampleAppUICheckButton_t4241727631::get_offset_of_mSelected_2(),
	SampleAppUICheckButton_t4241727631::get_offset_of_mStyle_3(),
	SampleAppUICheckButton_t4241727631::get_offset_of_mHeight_4(),
	SampleAppUICheckButton_t4241727631::get_offset_of_mWidth_5(),
	SampleAppUICheckButton_t4241727631::get_offset_of_mTitle_6(),
	SampleAppUICheckButton_t4241727631::get_offset_of_TappedOn_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3361 = { sizeof (SampleAppUIConstants_t672344260), -1, sizeof(SampleAppUIConstants_t672344260_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3361[90] = 
{
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_RectLabelOne_0(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_RectLabelAbout_1(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_RectOptionOne_2(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_RectOptionTwo_3(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_RectOptionThree_4(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_RectLabelTwo_5(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_RectOptionFour_6(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_RectOptionFive_7(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_RectLabelThree_8(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_RectOptionSix_9(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_RectOptionSeven_10(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_RectOptionEight_11(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_RectReset_12(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_CloseButtonRect_13(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_BoxRect_14(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_RectLabelFour_15(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_RectOptionTen_16(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_RectOptionEleven_17(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_RectLabelFive_18(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_RectOptionTwelve_19(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_RectOptionThirteen_20(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_RectOptionFourteen_21(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_RectOptionFifteen_22(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_RectOptionSixteen_23(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_RectLabelSix_24(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_RectOptionsSvnteen_25(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_RectOptionsEighteen_26(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_MainBackground_27(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_ImageTargetLabelStyle_28(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_CylinderTargetLabelStyle_29(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_MultiTargetLabelStyle_30(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_FrameMarkerLabelStyle_31(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_TextRecognitionLabelStyle_32(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_VideoPlaybackLabelStyle_33(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_VirtualButtonsLabelStyle_34(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_OcclusionManagementStyle_35(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_BackgroundTextureStyle_36(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_UDTTextureStyle_37(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_Books_38(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_CloudRecognition_39(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_AboutLableStyle_40(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_ButtonsLabelStyle_41(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_ExtendedTrackingStyleOff_42(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_ExtendedTrackingStyleOn_43(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_CameraFlashStyleOff_44(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_CameraFlashStyleOn_45(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_AutoFocusStyleOn_46(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_AutoFocusStyleOff_47(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_PlayFullscreenModeOn_48(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_PlayFullscreenModeOff_49(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_CameraLabelStyle_50(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_CameraFacingFrontStyleOn_51(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_CameraFacingFrontStyleOff_52(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_CameraFacingRearStyleOn_53(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_CameraFacingRearStyleOff_54(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_StonesAndChipsStyleOn_55(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_StonesAndChipsStyleOff_56(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_TarmacOn_57(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_TarmacOff_58(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_DatasetLabelStyle_59(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_closeButtonStyleOff_60(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_closeButtonStyleOn_61(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_CharacterModeStyleOn_62(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_CharacterModeStyleOff_63(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_TrackerOff_64(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_TrackerOn_65(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_MeshUpdatesOff_66(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_MeshUpdatesOn_67(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_SmartTerrainLabel_68(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_Reset_69(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_YellowButtonStyleOn_70(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_YellowButtonStyleOff_71(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_RedButtonStyleOn_72(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_RedButtonStyleOff_73(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_GreenButtonStyleOn_74(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_GreenButtonStyleOff_75(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_BlueButtonStyleOn_76(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_BlueButtonStyleOff_77(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_AboutTitleForImageTgt_78(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_AboutTitleFoMultiTgt_79(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_AboutTitleForCylinderTgt_80(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_AboutTitleForFrameMarkers_81(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_AboutTitleForUDT_82(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_AboutTitleForTextReco_83(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_AboutTitleForCloudReco_84(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_AboutTitleForBooks_85(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_AboutTitleForVirtualBtns_86(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_AboutTitleForVideoBg_87(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_AboutTitleForVideoPb_88(),
	SampleAppUIConstants_t672344260_StaticFields::get_offset_of_AboutTitleForOcclusionMgt_89(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3362 = { sizeof (SampleAppUILabel_t416684265), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3362[5] = 
{
	SampleAppUILabel_t416684265::get_offset_of_mRect_0(),
	SampleAppUILabel_t416684265::get_offset_of_mStyle_1(),
	SampleAppUILabel_t416684265::get_offset_of_mTitle_2(),
	SampleAppUILabel_t416684265::get_offset_of_mWidth_3(),
	SampleAppUILabel_t416684265::get_offset_of_mHeight_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3363 = { sizeof (SampleAppsUILayout_t3082371642), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3363[8] = 
{
	SampleAppsUILayout_t3082371642::get_offset_of_GUIElements_0(),
	SampleAppsUILayout_t3082371642::get_offset_of_mIndex_1(),
	SampleAppsUILayout_t3082371642::get_offset_of_mStyleHeader_2(),
	SampleAppsUILayout_t3082371642::get_offset_of_mStyleAboutButton_3(),
	SampleAppsUILayout_t3082371642::get_offset_of_mStyleSlider_4(),
	SampleAppsUILayout_t3082371642::get_offset_of_mStyleToggle_5(),
	SampleAppsUILayout_t3082371642::get_offset_of_mStyleCloseButton_6(),
	SampleAppsUILayout_t3082371642::get_offset_of_mStyleGroupLabel_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3364 = { sizeof (SampleAppUIRadioButton_t3067317570), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3364[10] = 
{
	SampleAppUIRadioButton_t3067317570::get_offset_of_mIndex_0(),
	SampleAppUIRadioButton_t3067317570::get_offset_of_mOptionsTapped_1(),
	SampleAppUIRadioButton_t3067317570::get_offset_of_mOptionsSelected_2(),
	SampleAppUIRadioButton_t3067317570::get_offset_of_mRect_3(),
	SampleAppUIRadioButton_t3067317570::get_offset_of_mTappedOn_4(),
	SampleAppUIRadioButton_t3067317570::get_offset_of_mStyle_5(),
	SampleAppUIRadioButton_t3067317570::get_offset_of_mWidth_6(),
	SampleAppUIRadioButton_t3067317570::get_offset_of_mHeight_7(),
	SampleAppUIRadioButton_t3067317570::get_offset_of_titleList_8(),
	SampleAppUIRadioButton_t3067317570::get_offset_of_TappedOnOption_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3365 = { sizeof (SampleAppUIRect_t2784570703), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3365[4] = 
{
	SampleAppUIRect_t2784570703::get_offset_of_mX_0(),
	SampleAppUIRect_t2784570703::get_offset_of_mY_1(),
	SampleAppUIRect_t2784570703::get_offset_of_mWidth_2(),
	SampleAppUIRect_t2784570703::get_offset_of_mHeight_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3366 = { sizeof (SampleInitErrorHandler_t2792208188), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3366[6] = 
{
	SampleInitErrorHandler_t2792208188::get_offset_of_mErrorTitleMessage_2(),
	SampleInitErrorHandler_t2792208188::get_offset_of_mErrorBodyMessage_3(),
	SampleInitErrorHandler_t2792208188::get_offset_of_mErrorOkButton_4(),
	SampleInitErrorHandler_t2792208188::get_offset_of_mCurrentError_5(),
	SampleInitErrorHandler_t2792208188::get_offset_of_mManager_6(),
	SampleInitErrorHandler_t2792208188::get_offset_of_mErrorcode_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3367 = { sizeof (ErrorData_t135901855)+ sizeof (Il2CppObject), sizeof(ErrorData_t135901855_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3367[2] = 
{
	ErrorData_t135901855::get_offset_of_Title_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ErrorData_t135901855::get_offset_of_Text_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3368 = { sizeof (SceneViewManager_t413752636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3368[3] = 
{
	SceneViewManager_t413752636::get_offset_of_mAppManager_2(),
	SceneViewManager_t413752636::get_offset_of_mPopUpMsg_3(),
	SceneViewManager_t413752636::get_offset_of_mErrorOccurred_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3369 = { sizeof (SplashScreenView_t1797787416), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3369[4] = 
{
	SplashScreenView_t1797787416::get_offset_of_mAndroidPotrait_0(),
	SplashScreenView_t1797787416::get_offset_of_mWindowsPlayModeTexture_1(),
	SplashScreenView_t1797787416::get_offset_of_m_SplashStyle_2(),
	SplashScreenView_t1797787416::get_offset_of_iPhoneImages_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3370 = { sizeof (EventDispatcher_t4168900583), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3370[2] = 
{
	EventDispatcher_t4168900583::get_offset_of_listners_2(),
	EventDispatcher_t4168900583::get_offset_of_dataListners_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3371 = { sizeof (EventDispatcherBase_t1248907224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3371[2] = 
{
	EventDispatcherBase_t1248907224::get_offset_of_listners_0(),
	EventDispatcherBase_t1248907224::get_offset_of_dataListners_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3372 = { sizeof (BaseEvent_t1449158927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3372[14] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3373 = { sizeof (CEvent_t44106931), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3373[7] = 
{
	CEvent_t44106931::get_offset_of__id_0(),
	CEvent_t44106931::get_offset_of__name_1(),
	CEvent_t44106931::get_offset_of__data_2(),
	CEvent_t44106931::get_offset_of__dispatcher_3(),
	CEvent_t44106931::get_offset_of__isStoped_4(),
	CEvent_t44106931::get_offset_of__isLocked_5(),
	CEvent_t44106931::get_offset_of__currentTarget_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3374 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3375 = { sizeof (MNFeaturePreview_t1881393139), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3375[10] = 
{
	MNFeaturePreview_t1881393139::get_offset_of_style_2(),
	MNFeaturePreview_t1881393139::get_offset_of_buttonWidth_3(),
	MNFeaturePreview_t1881393139::get_offset_of_buttonHeight_4(),
	MNFeaturePreview_t1881393139::get_offset_of_StartY_5(),
	MNFeaturePreview_t1881393139::get_offset_of_StartX_6(),
	MNFeaturePreview_t1881393139::get_offset_of_XStartPos_7(),
	MNFeaturePreview_t1881393139::get_offset_of_YStartPos_8(),
	MNFeaturePreview_t1881393139::get_offset_of_XButtonStep_9(),
	MNFeaturePreview_t1881393139::get_offset_of_YButtonStep_10(),
	MNFeaturePreview_t1881393139::get_offset_of_YLableStep_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3376 = { sizeof (MNUseExample_t1970404964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3376[2] = 
{
	MNUseExample_t1970404964::get_offset_of_appleId_12(),
	MNUseExample_t1970404964::get_offset_of_apdroidAppUrl_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3377 = { sizeof (MNP_t76495), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3378 = { sizeof (MNPopup_t1928680331), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3378[2] = 
{
	MNPopup_t1928680331::get_offset_of_title_4(),
	MNPopup_t1928680331::get_offset_of_message_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3379 = { sizeof (MobileNativeDialog_t811616801), -1, sizeof(MobileNativeDialog_t811616801_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3379[2] = 
{
	MobileNativeDialog_t811616801::get_offset_of_OnComplete_2(),
	MobileNativeDialog_t811616801_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3380 = { sizeof (MobileNativeMessage_t2985184846), -1, sizeof(MobileNativeMessage_t2985184846_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3380[2] = 
{
	MobileNativeMessage_t2985184846::get_offset_of_OnComplete_2(),
	MobileNativeMessage_t2985184846_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3381 = { sizeof (MobileNativeRateUs_t1205595255), -1, sizeof(MobileNativeRateUs_t1205595255_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3381[9] = 
{
	MobileNativeRateUs_t1205595255::get_offset_of_title_2(),
	MobileNativeRateUs_t1205595255::get_offset_of_message_3(),
	MobileNativeRateUs_t1205595255::get_offset_of_yes_4(),
	MobileNativeRateUs_t1205595255::get_offset_of_later_5(),
	MobileNativeRateUs_t1205595255::get_offset_of_no_6(),
	MobileNativeRateUs_t1205595255::get_offset_of_url_7(),
	MobileNativeRateUs_t1205595255::get_offset_of_appleId_8(),
	MobileNativeRateUs_t1205595255::get_offset_of_OnComplete_9(),
	MobileNativeRateUs_t1205595255_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3382 = { sizeof (MNDialogResult_t3125317574)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3382[7] = 
{
	MNDialogResult_t3125317574::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3383 = { sizeof (MNAndroidNative_t3251910469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3383[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3384 = { sizeof (MNProxyPool_t2453359369), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3385 = { sizeof (MNAndroidDialog_t2972443766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3385[2] = 
{
	MNAndroidDialog_t2972443766::get_offset_of_yes_6(),
	MNAndroidDialog_t2972443766::get_offset_of_no_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3386 = { sizeof (MNAndroidMessage_t1251344025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3386[1] = 
{
	MNAndroidMessage_t1251344025::get_offset_of_ok_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3387 = { sizeof (MNAndroidRateUsPopUp_t2446611808), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3387[4] = 
{
	MNAndroidRateUsPopUp_t2446611808::get_offset_of_yes_6(),
	MNAndroidRateUsPopUp_t2446611808::get_offset_of_later_7(),
	MNAndroidRateUsPopUp_t2446611808::get_offset_of_no_8(),
	MNAndroidRateUsPopUp_t2446611808::get_offset_of_url_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3388 = { sizeof (MNIOSDialog_t768531220), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3388[2] = 
{
	MNIOSDialog_t768531220::get_offset_of_yes_6(),
	MNIOSDialog_t768531220::get_offset_of_no_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3389 = { sizeof (MNIOSMessage_t1649531835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3389[1] = 
{
	MNIOSMessage_t1649531835::get_offset_of_ok_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3390 = { sizeof (MNIOSRateUsPopUp_t2151207298), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3390[4] = 
{
	MNIOSRateUsPopUp_t2151207298::get_offset_of_rate_6(),
	MNIOSRateUsPopUp_t2151207298::get_offset_of_remind_7(),
	MNIOSRateUsPopUp_t2151207298::get_offset_of_declined_8(),
	MNIOSRateUsPopUp_t2151207298::get_offset_of_appleId_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3391 = { sizeof (MNIOSNative_t1047997923), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3392 = { sizeof (MNWP8Dialog_t489019942), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3393 = { sizeof (MNWP8Message_t1574616809), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3394 = { sizeof (MNWP8RateUsPopUp_t4179652016), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3395 = { sizeof (BaseWP8Popup_t3692405086), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3395[2] = 
{
	BaseWP8Popup_t3692405086::get_offset_of_title_4(),
	BaseWP8Popup_t3692405086::get_offset_of_message_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3396 = { sizeof (ConsoleBase_t1938891126), -1, sizeof(ConsoleBase_t1938891126_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3396[15] = 
{
	0,
	ConsoleBase_t1938891126_StaticFields::get_offset_of_ButtonHeight_3(),
	ConsoleBase_t1938891126_StaticFields::get_offset_of_MainWindowWidth_4(),
	ConsoleBase_t1938891126_StaticFields::get_offset_of_MainWindowFullWidth_5(),
	ConsoleBase_t1938891126_StaticFields::get_offset_of_MarginFix_6(),
	ConsoleBase_t1938891126_StaticFields::get_offset_of_menuStack_7(),
	ConsoleBase_t1938891126::get_offset_of_status_8(),
	ConsoleBase_t1938891126::get_offset_of_lastResponse_9(),
	ConsoleBase_t1938891126::get_offset_of_scrollPosition_10(),
	ConsoleBase_t1938891126::get_offset_of_scaleFactor_11(),
	ConsoleBase_t1938891126::get_offset_of_textStyle_12(),
	ConsoleBase_t1938891126::get_offset_of_buttonStyle_13(),
	ConsoleBase_t1938891126::get_offset_of_textInputStyle_14(),
	ConsoleBase_t1938891126::get_offset_of_labelStyle_15(),
	ConsoleBase_t1938891126::get_offset_of_U3CLastResponseTextureU3Ek__BackingField_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3397 = { sizeof (LogView_t1467390519), -1, sizeof(LogView_t1467390519_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3397[2] = 
{
	LogView_t1467390519_StaticFields::get_offset_of_datePatt_17(),
	LogView_t1467390519_StaticFields::get_offset_of_events_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3398 = { sizeof (MenuBase_t1761432360), -1, sizeof(MenuBase_t1761432360_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3398[1] = 
{
	MenuBase_t1761432360_StaticFields::get_offset_of_shareDialogMode_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3399 = { sizeof (AccessTokenMenu_t4120877346), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
