﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.globals.DeviceFileInfoJson/<populateIndexedBundles>c__AnonStorey9B/<populateIndexedBundles>c__AnonStorey9C
struct U3CpopulateIndexedBundlesU3Ec__AnonStorey9C_t2872216120;
// MagicTV.vo.BundleVO
struct BundleVO_t1984518073;
// MagicTV.vo.MetadataVO
struct MetadataVO_t2511256998;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_vo_BundleVO1984518073.h"
#include "AssemblyU2DCSharp_MagicTV_vo_MetadataVO2511256998.h"

// System.Void MagicTV.globals.DeviceFileInfoJson/<populateIndexedBundles>c__AnonStorey9B/<populateIndexedBundles>c__AnonStorey9C::.ctor()
extern "C"  void U3CpopulateIndexedBundlesU3Ec__AnonStorey9C__ctor_m30168419 (U3CpopulateIndexedBundlesU3Ec__AnonStorey9C_t2872216120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicTV.globals.DeviceFileInfoJson/<populateIndexedBundles>c__AnonStorey9B/<populateIndexedBundles>c__AnonStorey9C::<>m__49(MagicTV.vo.BundleVO)
extern "C"  bool U3CpopulateIndexedBundlesU3Ec__AnonStorey9C_U3CU3Em__49_m3180314532 (U3CpopulateIndexedBundlesU3Ec__AnonStorey9C_t2872216120 * __this, BundleVO_t1984518073 * ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicTV.globals.DeviceFileInfoJson/<populateIndexedBundles>c__AnonStorey9B/<populateIndexedBundles>c__AnonStorey9C::<>m__4A(MagicTV.vo.MetadataVO)
extern "C"  bool U3CpopulateIndexedBundlesU3Ec__AnonStorey9C_U3CU3Em__4A_m797504335 (U3CpopulateIndexedBundlesU3Ec__AnonStorey9C_t2872216120 * __this, MetadataVO_t2511256998 * ___m0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
