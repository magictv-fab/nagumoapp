﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.BoolTest
struct  BoolTest_t1752966980  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.BoolTest::boolVariable
	FsmBool_t1075959796 * ___boolVariable_9;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.BoolTest::isTrue
	FsmEvent_t2133468028 * ___isTrue_10;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.BoolTest::isFalse
	FsmEvent_t2133468028 * ___isFalse_11;
	// System.Boolean HutongGames.PlayMaker.Actions.BoolTest::everyFrame
	bool ___everyFrame_12;

public:
	inline static int32_t get_offset_of_boolVariable_9() { return static_cast<int32_t>(offsetof(BoolTest_t1752966980, ___boolVariable_9)); }
	inline FsmBool_t1075959796 * get_boolVariable_9() const { return ___boolVariable_9; }
	inline FsmBool_t1075959796 ** get_address_of_boolVariable_9() { return &___boolVariable_9; }
	inline void set_boolVariable_9(FsmBool_t1075959796 * value)
	{
		___boolVariable_9 = value;
		Il2CppCodeGenWriteBarrier(&___boolVariable_9, value);
	}

	inline static int32_t get_offset_of_isTrue_10() { return static_cast<int32_t>(offsetof(BoolTest_t1752966980, ___isTrue_10)); }
	inline FsmEvent_t2133468028 * get_isTrue_10() const { return ___isTrue_10; }
	inline FsmEvent_t2133468028 ** get_address_of_isTrue_10() { return &___isTrue_10; }
	inline void set_isTrue_10(FsmEvent_t2133468028 * value)
	{
		___isTrue_10 = value;
		Il2CppCodeGenWriteBarrier(&___isTrue_10, value);
	}

	inline static int32_t get_offset_of_isFalse_11() { return static_cast<int32_t>(offsetof(BoolTest_t1752966980, ___isFalse_11)); }
	inline FsmEvent_t2133468028 * get_isFalse_11() const { return ___isFalse_11; }
	inline FsmEvent_t2133468028 ** get_address_of_isFalse_11() { return &___isFalse_11; }
	inline void set_isFalse_11(FsmEvent_t2133468028 * value)
	{
		___isFalse_11 = value;
		Il2CppCodeGenWriteBarrier(&___isFalse_11, value);
	}

	inline static int32_t get_offset_of_everyFrame_12() { return static_cast<int32_t>(offsetof(BoolTest_t1752966980, ___everyFrame_12)); }
	inline bool get_everyFrame_12() const { return ___everyFrame_12; }
	inline bool* get_address_of_everyFrame_12() { return &___everyFrame_12; }
	inline void set_everyFrame_12(bool value)
	{
		___everyFrame_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
