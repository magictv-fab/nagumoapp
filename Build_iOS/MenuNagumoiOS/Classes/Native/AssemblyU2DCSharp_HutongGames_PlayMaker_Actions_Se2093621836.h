﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t3871136040;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// PlayMakerAnimatorIKProxy
struct PlayMakerAnimatorIKProxy_t1024752181;
// UnityEngine.Animator
struct Animator_t2776330603;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "UnityEngine_UnityEngine_AvatarIKGoal2036631794.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAnimatorIKGoal
struct  SetAnimatorIKGoal_t2093621836  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAnimatorIKGoal::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// UnityEngine.AvatarIKGoal HutongGames.PlayMaker.Actions.SetAnimatorIKGoal::iKGoal
	int32_t ___iKGoal_10;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.SetAnimatorIKGoal::goal
	FsmGameObject_t1697147867 * ___goal_11;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SetAnimatorIKGoal::position
	FsmVector3_t533912882 * ___position_12;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.SetAnimatorIKGoal::rotation
	FsmQuaternion_t3871136040 * ___rotation_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetAnimatorIKGoal::positionWeight
	FsmFloat_t2134102846 * ___positionWeight_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetAnimatorIKGoal::rotationWeight
	FsmFloat_t2134102846 * ___rotationWeight_15;
	// System.Boolean HutongGames.PlayMaker.Actions.SetAnimatorIKGoal::everyFrame
	bool ___everyFrame_16;
	// PlayMakerAnimatorIKProxy HutongGames.PlayMaker.Actions.SetAnimatorIKGoal::_animatorProxy
	PlayMakerAnimatorIKProxy_t1024752181 * ____animatorProxy_17;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.SetAnimatorIKGoal::_animator
	Animator_t2776330603 * ____animator_18;
	// UnityEngine.Transform HutongGames.PlayMaker.Actions.SetAnimatorIKGoal::_transform
	Transform_t1659122786 * ____transform_19;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(SetAnimatorIKGoal_t2093621836, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_iKGoal_10() { return static_cast<int32_t>(offsetof(SetAnimatorIKGoal_t2093621836, ___iKGoal_10)); }
	inline int32_t get_iKGoal_10() const { return ___iKGoal_10; }
	inline int32_t* get_address_of_iKGoal_10() { return &___iKGoal_10; }
	inline void set_iKGoal_10(int32_t value)
	{
		___iKGoal_10 = value;
	}

	inline static int32_t get_offset_of_goal_11() { return static_cast<int32_t>(offsetof(SetAnimatorIKGoal_t2093621836, ___goal_11)); }
	inline FsmGameObject_t1697147867 * get_goal_11() const { return ___goal_11; }
	inline FsmGameObject_t1697147867 ** get_address_of_goal_11() { return &___goal_11; }
	inline void set_goal_11(FsmGameObject_t1697147867 * value)
	{
		___goal_11 = value;
		Il2CppCodeGenWriteBarrier(&___goal_11, value);
	}

	inline static int32_t get_offset_of_position_12() { return static_cast<int32_t>(offsetof(SetAnimatorIKGoal_t2093621836, ___position_12)); }
	inline FsmVector3_t533912882 * get_position_12() const { return ___position_12; }
	inline FsmVector3_t533912882 ** get_address_of_position_12() { return &___position_12; }
	inline void set_position_12(FsmVector3_t533912882 * value)
	{
		___position_12 = value;
		Il2CppCodeGenWriteBarrier(&___position_12, value);
	}

	inline static int32_t get_offset_of_rotation_13() { return static_cast<int32_t>(offsetof(SetAnimatorIKGoal_t2093621836, ___rotation_13)); }
	inline FsmQuaternion_t3871136040 * get_rotation_13() const { return ___rotation_13; }
	inline FsmQuaternion_t3871136040 ** get_address_of_rotation_13() { return &___rotation_13; }
	inline void set_rotation_13(FsmQuaternion_t3871136040 * value)
	{
		___rotation_13 = value;
		Il2CppCodeGenWriteBarrier(&___rotation_13, value);
	}

	inline static int32_t get_offset_of_positionWeight_14() { return static_cast<int32_t>(offsetof(SetAnimatorIKGoal_t2093621836, ___positionWeight_14)); }
	inline FsmFloat_t2134102846 * get_positionWeight_14() const { return ___positionWeight_14; }
	inline FsmFloat_t2134102846 ** get_address_of_positionWeight_14() { return &___positionWeight_14; }
	inline void set_positionWeight_14(FsmFloat_t2134102846 * value)
	{
		___positionWeight_14 = value;
		Il2CppCodeGenWriteBarrier(&___positionWeight_14, value);
	}

	inline static int32_t get_offset_of_rotationWeight_15() { return static_cast<int32_t>(offsetof(SetAnimatorIKGoal_t2093621836, ___rotationWeight_15)); }
	inline FsmFloat_t2134102846 * get_rotationWeight_15() const { return ___rotationWeight_15; }
	inline FsmFloat_t2134102846 ** get_address_of_rotationWeight_15() { return &___rotationWeight_15; }
	inline void set_rotationWeight_15(FsmFloat_t2134102846 * value)
	{
		___rotationWeight_15 = value;
		Il2CppCodeGenWriteBarrier(&___rotationWeight_15, value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(SetAnimatorIKGoal_t2093621836, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}

	inline static int32_t get_offset_of__animatorProxy_17() { return static_cast<int32_t>(offsetof(SetAnimatorIKGoal_t2093621836, ____animatorProxy_17)); }
	inline PlayMakerAnimatorIKProxy_t1024752181 * get__animatorProxy_17() const { return ____animatorProxy_17; }
	inline PlayMakerAnimatorIKProxy_t1024752181 ** get_address_of__animatorProxy_17() { return &____animatorProxy_17; }
	inline void set__animatorProxy_17(PlayMakerAnimatorIKProxy_t1024752181 * value)
	{
		____animatorProxy_17 = value;
		Il2CppCodeGenWriteBarrier(&____animatorProxy_17, value);
	}

	inline static int32_t get_offset_of__animator_18() { return static_cast<int32_t>(offsetof(SetAnimatorIKGoal_t2093621836, ____animator_18)); }
	inline Animator_t2776330603 * get__animator_18() const { return ____animator_18; }
	inline Animator_t2776330603 ** get_address_of__animator_18() { return &____animator_18; }
	inline void set__animator_18(Animator_t2776330603 * value)
	{
		____animator_18 = value;
		Il2CppCodeGenWriteBarrier(&____animator_18, value);
	}

	inline static int32_t get_offset_of__transform_19() { return static_cast<int32_t>(offsetof(SetAnimatorIKGoal_t2093621836, ____transform_19)); }
	inline Transform_t1659122786 * get__transform_19() const { return ____transform_19; }
	inline Transform_t1659122786 ** get_address_of__transform_19() { return &____transform_19; }
	inline void set__transform_19(Transform_t1659122786 * value)
	{
		____transform_19 = value;
		Il2CppCodeGenWriteBarrier(&____transform_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
