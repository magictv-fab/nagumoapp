﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DragPanel
struct DragPanel_t1678257424;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1848751023;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1848751023.h"

// System.Void DragPanel::.ctor()
extern "C"  void DragPanel__ctor_m1334614619 (DragPanel_t1678257424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DragPanel::Awake()
extern "C"  void DragPanel_Awake_m1572219838 (DragPanel_t1678257424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DragPanel::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C"  void DragPanel_OnPointerDown_m656432389 (DragPanel_t1678257424 * __this, PointerEventData_t1848751023 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DragPanel::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void DragPanel_OnDrag_m2538909826 (DragPanel_t1678257424 * __this, PointerEventData_t1848751023 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DragPanel::ClampToWindow()
extern "C"  void DragPanel_ClampToWindow_m2386593311 (DragPanel_t1678257424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
