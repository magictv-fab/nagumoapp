﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AudioSource
struct AudioSource_t1740077639;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AttachedAudio
struct  AttachedAudio_t711869554  : public Il2CppObject
{
public:
	// UnityEngine.AudioSource AttachedAudio::attachedAudioSource
	AudioSource_t1740077639 * ___attachedAudioSource_0;
	// System.Single AttachedAudio::frameIndex
	float ___frameIndex_1;
	// System.Single AttachedAudio::fps
	float ___fps_2;
	// System.Boolean AttachedAudio::togglePlay
	bool ___togglePlay_3;

public:
	inline static int32_t get_offset_of_attachedAudioSource_0() { return static_cast<int32_t>(offsetof(AttachedAudio_t711869554, ___attachedAudioSource_0)); }
	inline AudioSource_t1740077639 * get_attachedAudioSource_0() const { return ___attachedAudioSource_0; }
	inline AudioSource_t1740077639 ** get_address_of_attachedAudioSource_0() { return &___attachedAudioSource_0; }
	inline void set_attachedAudioSource_0(AudioSource_t1740077639 * value)
	{
		___attachedAudioSource_0 = value;
		Il2CppCodeGenWriteBarrier(&___attachedAudioSource_0, value);
	}

	inline static int32_t get_offset_of_frameIndex_1() { return static_cast<int32_t>(offsetof(AttachedAudio_t711869554, ___frameIndex_1)); }
	inline float get_frameIndex_1() const { return ___frameIndex_1; }
	inline float* get_address_of_frameIndex_1() { return &___frameIndex_1; }
	inline void set_frameIndex_1(float value)
	{
		___frameIndex_1 = value;
	}

	inline static int32_t get_offset_of_fps_2() { return static_cast<int32_t>(offsetof(AttachedAudio_t711869554, ___fps_2)); }
	inline float get_fps_2() const { return ___fps_2; }
	inline float* get_address_of_fps_2() { return &___fps_2; }
	inline void set_fps_2(float value)
	{
		___fps_2 = value;
	}

	inline static int32_t get_offset_of_togglePlay_3() { return static_cast<int32_t>(offsetof(AttachedAudio_t711869554, ___togglePlay_3)); }
	inline bool get_togglePlay_3() const { return ___togglePlay_3; }
	inline bool* get_address_of_togglePlay_3() { return &___togglePlay_3; }
	inline void set_togglePlay_3(bool value)
	{
		___togglePlay_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
