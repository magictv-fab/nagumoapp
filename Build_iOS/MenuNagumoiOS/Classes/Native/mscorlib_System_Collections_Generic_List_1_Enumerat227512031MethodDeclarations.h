﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.WWW>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3237817703(__this, ___l0, method) ((  void (*) (Enumerator_t227512031 *, List_1_t207839261 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.WWW>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3672564363(__this, method) ((  void (*) (Enumerator_t227512031 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.WWW>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4124127415(__this, method) ((  Il2CppObject * (*) (Enumerator_t227512031 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.WWW>::Dispose()
#define Enumerator_Dispose_m3336717580(__this, method) ((  void (*) (Enumerator_t227512031 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.WWW>::VerifyState()
#define Enumerator_VerifyState_m3155467845(__this, method) ((  void (*) (Enumerator_t227512031 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.WWW>::MoveNext()
#define Enumerator_MoveNext_m3455707511(__this, method) ((  bool (*) (Enumerator_t227512031 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.WWW>::get_Current()
#define Enumerator_get_Current_m818492028(__this, method) ((  WWW_t3134621005 * (*) (Enumerator_t227512031 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
