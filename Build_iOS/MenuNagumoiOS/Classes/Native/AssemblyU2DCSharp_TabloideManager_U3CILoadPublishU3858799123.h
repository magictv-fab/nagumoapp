﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// UnityEngine.WWW
struct WWW_t3134621005;
// TabloideData[]
struct TabloideDataU5BU5D_t684230357;
// TabloideData
struct TabloideData_t1867721404;
// System.Object
struct Il2CppObject;
// TabloideManager
struct TabloideManager_t3674153819;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TabloideManager/<ILoadPublish>c__Iterator80
struct  U3CILoadPublishU3Ec__Iterator80_t3858799123  : public Il2CppObject
{
public:
	// System.String TabloideManager/<ILoadPublish>c__Iterator80::<json>__0
	String_t* ___U3CjsonU3E__0_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> TabloideManager/<ILoadPublish>c__Iterator80::<headers>__1
	Dictionary_2_t827649927 * ___U3CheadersU3E__1_1;
	// System.Byte[] TabloideManager/<ILoadPublish>c__Iterator80::<pData>__2
	ByteU5BU5D_t4260760469* ___U3CpDataU3E__2_2;
	// UnityEngine.WWW TabloideManager/<ILoadPublish>c__Iterator80::<www>__3
	WWW_t3134621005 * ___U3CwwwU3E__3_3;
	// System.String TabloideManager/<ILoadPublish>c__Iterator80::<message>__4
	String_t* ___U3CmessageU3E__4_4;
	// TabloideData[] TabloideManager/<ILoadPublish>c__Iterator80::<$s_347>__5
	TabloideDataU5BU5D_t684230357* ___U3CU24s_347U3E__5_5;
	// System.Int32 TabloideManager/<ILoadPublish>c__Iterator80::<$s_348>__6
	int32_t ___U3CU24s_348U3E__6_6;
	// TabloideData TabloideManager/<ILoadPublish>c__Iterator80::<tabloideData>__7
	TabloideData_t1867721404 * ___U3CtabloideDataU3E__7_7;
	// System.Int32 TabloideManager/<ILoadPublish>c__Iterator80::$PC
	int32_t ___U24PC_8;
	// System.Object TabloideManager/<ILoadPublish>c__Iterator80::$current
	Il2CppObject * ___U24current_9;
	// TabloideManager TabloideManager/<ILoadPublish>c__Iterator80::<>f__this
	TabloideManager_t3674153819 * ___U3CU3Ef__this_10;

public:
	inline static int32_t get_offset_of_U3CjsonU3E__0_0() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator80_t3858799123, ___U3CjsonU3E__0_0)); }
	inline String_t* get_U3CjsonU3E__0_0() const { return ___U3CjsonU3E__0_0; }
	inline String_t** get_address_of_U3CjsonU3E__0_0() { return &___U3CjsonU3E__0_0; }
	inline void set_U3CjsonU3E__0_0(String_t* value)
	{
		___U3CjsonU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjsonU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CheadersU3E__1_1() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator80_t3858799123, ___U3CheadersU3E__1_1)); }
	inline Dictionary_2_t827649927 * get_U3CheadersU3E__1_1() const { return ___U3CheadersU3E__1_1; }
	inline Dictionary_2_t827649927 ** get_address_of_U3CheadersU3E__1_1() { return &___U3CheadersU3E__1_1; }
	inline void set_U3CheadersU3E__1_1(Dictionary_2_t827649927 * value)
	{
		___U3CheadersU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CheadersU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CpDataU3E__2_2() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator80_t3858799123, ___U3CpDataU3E__2_2)); }
	inline ByteU5BU5D_t4260760469* get_U3CpDataU3E__2_2() const { return ___U3CpDataU3E__2_2; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CpDataU3E__2_2() { return &___U3CpDataU3E__2_2; }
	inline void set_U3CpDataU3E__2_2(ByteU5BU5D_t4260760469* value)
	{
		___U3CpDataU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpDataU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__3_3() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator80_t3858799123, ___U3CwwwU3E__3_3)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__3_3() const { return ___U3CwwwU3E__3_3; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__3_3() { return &___U3CwwwU3E__3_3; }
	inline void set_U3CwwwU3E__3_3(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CmessageU3E__4_4() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator80_t3858799123, ___U3CmessageU3E__4_4)); }
	inline String_t* get_U3CmessageU3E__4_4() const { return ___U3CmessageU3E__4_4; }
	inline String_t** get_address_of_U3CmessageU3E__4_4() { return &___U3CmessageU3E__4_4; }
	inline void set_U3CmessageU3E__4_4(String_t* value)
	{
		___U3CmessageU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmessageU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U3CU24s_347U3E__5_5() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator80_t3858799123, ___U3CU24s_347U3E__5_5)); }
	inline TabloideDataU5BU5D_t684230357* get_U3CU24s_347U3E__5_5() const { return ___U3CU24s_347U3E__5_5; }
	inline TabloideDataU5BU5D_t684230357** get_address_of_U3CU24s_347U3E__5_5() { return &___U3CU24s_347U3E__5_5; }
	inline void set_U3CU24s_347U3E__5_5(TabloideDataU5BU5D_t684230357* value)
	{
		___U3CU24s_347U3E__5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_347U3E__5_5, value);
	}

	inline static int32_t get_offset_of_U3CU24s_348U3E__6_6() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator80_t3858799123, ___U3CU24s_348U3E__6_6)); }
	inline int32_t get_U3CU24s_348U3E__6_6() const { return ___U3CU24s_348U3E__6_6; }
	inline int32_t* get_address_of_U3CU24s_348U3E__6_6() { return &___U3CU24s_348U3E__6_6; }
	inline void set_U3CU24s_348U3E__6_6(int32_t value)
	{
		___U3CU24s_348U3E__6_6 = value;
	}

	inline static int32_t get_offset_of_U3CtabloideDataU3E__7_7() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator80_t3858799123, ___U3CtabloideDataU3E__7_7)); }
	inline TabloideData_t1867721404 * get_U3CtabloideDataU3E__7_7() const { return ___U3CtabloideDataU3E__7_7; }
	inline TabloideData_t1867721404 ** get_address_of_U3CtabloideDataU3E__7_7() { return &___U3CtabloideDataU3E__7_7; }
	inline void set_U3CtabloideDataU3E__7_7(TabloideData_t1867721404 * value)
	{
		___U3CtabloideDataU3E__7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtabloideDataU3E__7_7, value);
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator80_t3858799123, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator80_t3858799123, ___U24current_9)); }
	inline Il2CppObject * get_U24current_9() const { return ___U24current_9; }
	inline Il2CppObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(Il2CppObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_10() { return static_cast<int32_t>(offsetof(U3CILoadPublishU3Ec__Iterator80_t3858799123, ___U3CU3Ef__this_10)); }
	inline TabloideManager_t3674153819 * get_U3CU3Ef__this_10() const { return ___U3CU3Ef__this_10; }
	inline TabloideManager_t3674153819 ** get_address_of_U3CU3Ef__this_10() { return &___U3CU3Ef__this_10; }
	inline void set_U3CU3Ef__this_10(TabloideManager_t3674153819 * value)
	{
		___U3CU3Ef__this_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
