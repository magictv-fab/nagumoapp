﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AddForce
struct AddForce_t2783319922;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AddForce::.ctor()
extern "C"  void AddForce__ctor_m276996916 (AddForce_t2783319922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddForce::Reset()
extern "C"  void AddForce_Reset_m2218397153 (AddForce_t2783319922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddForce::Awake()
extern "C"  void AddForce_Awake_m514602135 (AddForce_t2783319922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddForce::OnEnter()
extern "C"  void AddForce_OnEnter_m3592759819 (AddForce_t2783319922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddForce::OnFixedUpdate()
extern "C"  void AddForce_OnFixedUpdate_m2424491472 (AddForce_t2783319922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddForce::DoAddForce()
extern "C"  void AddForce_DoAddForce_m207894405 (AddForce_t2783319922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
