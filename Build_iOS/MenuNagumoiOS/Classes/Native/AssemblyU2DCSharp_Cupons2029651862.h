﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t538875265;
// UnityEngine.Sprite
struct Sprite_t3199167241;
// UnityEngine.UI.RawImage
struct RawImage_t821930207;
// UnityEngine.UI.Text
struct Text_t9039225;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// QRCodeEncodeController
struct QRCodeEncodeController_t2317858336;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cupons
struct  Cupons_t2029651862  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.Image Cupons::container
	Image_t538875265 * ___container_2;
	// UnityEngine.Sprite Cupons::cupom1
	Sprite_t3199167241 * ___cupom1_3;
	// UnityEngine.Sprite Cupons::cupom2
	Sprite_t3199167241 * ___cupom2_4;
	// UnityEngine.Sprite Cupons::cupom3
	Sprite_t3199167241 * ___cupom3_5;
	// UnityEngine.Sprite Cupons::cupom5
	Sprite_t3199167241 * ___cupom5_6;
	// UnityEngine.Sprite Cupons::cupom10
	Sprite_t3199167241 * ___cupom10_7;
	// UnityEngine.Sprite Cupons::cupom50
	Sprite_t3199167241 * ___cupom50_8;
	// UnityEngine.Sprite Cupons::cupom100
	Sprite_t3199167241 * ___cupom100_9;
	// UnityEngine.Sprite Cupons::cupom200
	Sprite_t3199167241 * ___cupom200_10;
	// UnityEngine.Sprite Cupons::cupom500
	Sprite_t3199167241 * ___cupom500_11;
	// UnityEngine.UI.RawImage Cupons::qrCodeImage
	RawImage_t821930207 * ___qrCodeImage_12;
	// System.Int32 Cupons::cupomValue
	int32_t ___cupomValue_13;
	// UnityEngine.UI.Text Cupons::codeText
	Text_t9039225 * ___codeText_14;
	// UnityEngine.UI.Text Cupons::dateText
	Text_t9039225 * ___dateText_15;
	// System.String Cupons::code
	String_t* ___code_16;
	// UnityEngine.GameObject Cupons::usedObj
	GameObject_t3674682005 * ___usedObj_17;
	// UnityEngine.GameObject Cupons::expiredObj
	GameObject_t3674682005 * ___expiredObj_18;
	// System.Boolean Cupons::used
	bool ___used_19;
	// UnityEngine.UI.Image Cupons::barcodeSprite
	Image_t538875265 * ___barcodeSprite_20;
	// System.String Cupons::dataExpira
	String_t* ___dataExpira_21;
	// System.Int32 Cupons::cupomValueTabloid
	int32_t ___cupomValueTabloid_22;
	// QRCodeEncodeController Cupons::e_qrController
	QRCodeEncodeController_t2317858336 * ___e_qrController_23;

public:
	inline static int32_t get_offset_of_container_2() { return static_cast<int32_t>(offsetof(Cupons_t2029651862, ___container_2)); }
	inline Image_t538875265 * get_container_2() const { return ___container_2; }
	inline Image_t538875265 ** get_address_of_container_2() { return &___container_2; }
	inline void set_container_2(Image_t538875265 * value)
	{
		___container_2 = value;
		Il2CppCodeGenWriteBarrier(&___container_2, value);
	}

	inline static int32_t get_offset_of_cupom1_3() { return static_cast<int32_t>(offsetof(Cupons_t2029651862, ___cupom1_3)); }
	inline Sprite_t3199167241 * get_cupom1_3() const { return ___cupom1_3; }
	inline Sprite_t3199167241 ** get_address_of_cupom1_3() { return &___cupom1_3; }
	inline void set_cupom1_3(Sprite_t3199167241 * value)
	{
		___cupom1_3 = value;
		Il2CppCodeGenWriteBarrier(&___cupom1_3, value);
	}

	inline static int32_t get_offset_of_cupom2_4() { return static_cast<int32_t>(offsetof(Cupons_t2029651862, ___cupom2_4)); }
	inline Sprite_t3199167241 * get_cupom2_4() const { return ___cupom2_4; }
	inline Sprite_t3199167241 ** get_address_of_cupom2_4() { return &___cupom2_4; }
	inline void set_cupom2_4(Sprite_t3199167241 * value)
	{
		___cupom2_4 = value;
		Il2CppCodeGenWriteBarrier(&___cupom2_4, value);
	}

	inline static int32_t get_offset_of_cupom3_5() { return static_cast<int32_t>(offsetof(Cupons_t2029651862, ___cupom3_5)); }
	inline Sprite_t3199167241 * get_cupom3_5() const { return ___cupom3_5; }
	inline Sprite_t3199167241 ** get_address_of_cupom3_5() { return &___cupom3_5; }
	inline void set_cupom3_5(Sprite_t3199167241 * value)
	{
		___cupom3_5 = value;
		Il2CppCodeGenWriteBarrier(&___cupom3_5, value);
	}

	inline static int32_t get_offset_of_cupom5_6() { return static_cast<int32_t>(offsetof(Cupons_t2029651862, ___cupom5_6)); }
	inline Sprite_t3199167241 * get_cupom5_6() const { return ___cupom5_6; }
	inline Sprite_t3199167241 ** get_address_of_cupom5_6() { return &___cupom5_6; }
	inline void set_cupom5_6(Sprite_t3199167241 * value)
	{
		___cupom5_6 = value;
		Il2CppCodeGenWriteBarrier(&___cupom5_6, value);
	}

	inline static int32_t get_offset_of_cupom10_7() { return static_cast<int32_t>(offsetof(Cupons_t2029651862, ___cupom10_7)); }
	inline Sprite_t3199167241 * get_cupom10_7() const { return ___cupom10_7; }
	inline Sprite_t3199167241 ** get_address_of_cupom10_7() { return &___cupom10_7; }
	inline void set_cupom10_7(Sprite_t3199167241 * value)
	{
		___cupom10_7 = value;
		Il2CppCodeGenWriteBarrier(&___cupom10_7, value);
	}

	inline static int32_t get_offset_of_cupom50_8() { return static_cast<int32_t>(offsetof(Cupons_t2029651862, ___cupom50_8)); }
	inline Sprite_t3199167241 * get_cupom50_8() const { return ___cupom50_8; }
	inline Sprite_t3199167241 ** get_address_of_cupom50_8() { return &___cupom50_8; }
	inline void set_cupom50_8(Sprite_t3199167241 * value)
	{
		___cupom50_8 = value;
		Il2CppCodeGenWriteBarrier(&___cupom50_8, value);
	}

	inline static int32_t get_offset_of_cupom100_9() { return static_cast<int32_t>(offsetof(Cupons_t2029651862, ___cupom100_9)); }
	inline Sprite_t3199167241 * get_cupom100_9() const { return ___cupom100_9; }
	inline Sprite_t3199167241 ** get_address_of_cupom100_9() { return &___cupom100_9; }
	inline void set_cupom100_9(Sprite_t3199167241 * value)
	{
		___cupom100_9 = value;
		Il2CppCodeGenWriteBarrier(&___cupom100_9, value);
	}

	inline static int32_t get_offset_of_cupom200_10() { return static_cast<int32_t>(offsetof(Cupons_t2029651862, ___cupom200_10)); }
	inline Sprite_t3199167241 * get_cupom200_10() const { return ___cupom200_10; }
	inline Sprite_t3199167241 ** get_address_of_cupom200_10() { return &___cupom200_10; }
	inline void set_cupom200_10(Sprite_t3199167241 * value)
	{
		___cupom200_10 = value;
		Il2CppCodeGenWriteBarrier(&___cupom200_10, value);
	}

	inline static int32_t get_offset_of_cupom500_11() { return static_cast<int32_t>(offsetof(Cupons_t2029651862, ___cupom500_11)); }
	inline Sprite_t3199167241 * get_cupom500_11() const { return ___cupom500_11; }
	inline Sprite_t3199167241 ** get_address_of_cupom500_11() { return &___cupom500_11; }
	inline void set_cupom500_11(Sprite_t3199167241 * value)
	{
		___cupom500_11 = value;
		Il2CppCodeGenWriteBarrier(&___cupom500_11, value);
	}

	inline static int32_t get_offset_of_qrCodeImage_12() { return static_cast<int32_t>(offsetof(Cupons_t2029651862, ___qrCodeImage_12)); }
	inline RawImage_t821930207 * get_qrCodeImage_12() const { return ___qrCodeImage_12; }
	inline RawImage_t821930207 ** get_address_of_qrCodeImage_12() { return &___qrCodeImage_12; }
	inline void set_qrCodeImage_12(RawImage_t821930207 * value)
	{
		___qrCodeImage_12 = value;
		Il2CppCodeGenWriteBarrier(&___qrCodeImage_12, value);
	}

	inline static int32_t get_offset_of_cupomValue_13() { return static_cast<int32_t>(offsetof(Cupons_t2029651862, ___cupomValue_13)); }
	inline int32_t get_cupomValue_13() const { return ___cupomValue_13; }
	inline int32_t* get_address_of_cupomValue_13() { return &___cupomValue_13; }
	inline void set_cupomValue_13(int32_t value)
	{
		___cupomValue_13 = value;
	}

	inline static int32_t get_offset_of_codeText_14() { return static_cast<int32_t>(offsetof(Cupons_t2029651862, ___codeText_14)); }
	inline Text_t9039225 * get_codeText_14() const { return ___codeText_14; }
	inline Text_t9039225 ** get_address_of_codeText_14() { return &___codeText_14; }
	inline void set_codeText_14(Text_t9039225 * value)
	{
		___codeText_14 = value;
		Il2CppCodeGenWriteBarrier(&___codeText_14, value);
	}

	inline static int32_t get_offset_of_dateText_15() { return static_cast<int32_t>(offsetof(Cupons_t2029651862, ___dateText_15)); }
	inline Text_t9039225 * get_dateText_15() const { return ___dateText_15; }
	inline Text_t9039225 ** get_address_of_dateText_15() { return &___dateText_15; }
	inline void set_dateText_15(Text_t9039225 * value)
	{
		___dateText_15 = value;
		Il2CppCodeGenWriteBarrier(&___dateText_15, value);
	}

	inline static int32_t get_offset_of_code_16() { return static_cast<int32_t>(offsetof(Cupons_t2029651862, ___code_16)); }
	inline String_t* get_code_16() const { return ___code_16; }
	inline String_t** get_address_of_code_16() { return &___code_16; }
	inline void set_code_16(String_t* value)
	{
		___code_16 = value;
		Il2CppCodeGenWriteBarrier(&___code_16, value);
	}

	inline static int32_t get_offset_of_usedObj_17() { return static_cast<int32_t>(offsetof(Cupons_t2029651862, ___usedObj_17)); }
	inline GameObject_t3674682005 * get_usedObj_17() const { return ___usedObj_17; }
	inline GameObject_t3674682005 ** get_address_of_usedObj_17() { return &___usedObj_17; }
	inline void set_usedObj_17(GameObject_t3674682005 * value)
	{
		___usedObj_17 = value;
		Il2CppCodeGenWriteBarrier(&___usedObj_17, value);
	}

	inline static int32_t get_offset_of_expiredObj_18() { return static_cast<int32_t>(offsetof(Cupons_t2029651862, ___expiredObj_18)); }
	inline GameObject_t3674682005 * get_expiredObj_18() const { return ___expiredObj_18; }
	inline GameObject_t3674682005 ** get_address_of_expiredObj_18() { return &___expiredObj_18; }
	inline void set_expiredObj_18(GameObject_t3674682005 * value)
	{
		___expiredObj_18 = value;
		Il2CppCodeGenWriteBarrier(&___expiredObj_18, value);
	}

	inline static int32_t get_offset_of_used_19() { return static_cast<int32_t>(offsetof(Cupons_t2029651862, ___used_19)); }
	inline bool get_used_19() const { return ___used_19; }
	inline bool* get_address_of_used_19() { return &___used_19; }
	inline void set_used_19(bool value)
	{
		___used_19 = value;
	}

	inline static int32_t get_offset_of_barcodeSprite_20() { return static_cast<int32_t>(offsetof(Cupons_t2029651862, ___barcodeSprite_20)); }
	inline Image_t538875265 * get_barcodeSprite_20() const { return ___barcodeSprite_20; }
	inline Image_t538875265 ** get_address_of_barcodeSprite_20() { return &___barcodeSprite_20; }
	inline void set_barcodeSprite_20(Image_t538875265 * value)
	{
		___barcodeSprite_20 = value;
		Il2CppCodeGenWriteBarrier(&___barcodeSprite_20, value);
	}

	inline static int32_t get_offset_of_dataExpira_21() { return static_cast<int32_t>(offsetof(Cupons_t2029651862, ___dataExpira_21)); }
	inline String_t* get_dataExpira_21() const { return ___dataExpira_21; }
	inline String_t** get_address_of_dataExpira_21() { return &___dataExpira_21; }
	inline void set_dataExpira_21(String_t* value)
	{
		___dataExpira_21 = value;
		Il2CppCodeGenWriteBarrier(&___dataExpira_21, value);
	}

	inline static int32_t get_offset_of_cupomValueTabloid_22() { return static_cast<int32_t>(offsetof(Cupons_t2029651862, ___cupomValueTabloid_22)); }
	inline int32_t get_cupomValueTabloid_22() const { return ___cupomValueTabloid_22; }
	inline int32_t* get_address_of_cupomValueTabloid_22() { return &___cupomValueTabloid_22; }
	inline void set_cupomValueTabloid_22(int32_t value)
	{
		___cupomValueTabloid_22 = value;
	}

	inline static int32_t get_offset_of_e_qrController_23() { return static_cast<int32_t>(offsetof(Cupons_t2029651862, ___e_qrController_23)); }
	inline QRCodeEncodeController_t2317858336 * get_e_qrController_23() const { return ___e_qrController_23; }
	inline QRCodeEncodeController_t2317858336 ** get_address_of_e_qrController_23() { return &___e_qrController_23; }
	inline void set_e_qrController_23(QRCodeEncodeController_t2317858336 * value)
	{
		___e_qrController_23 = value;
		Il2CppCodeGenWriteBarrier(&___e_qrController_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
