﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.ArrayList
struct ArrayList_t3948406897;
// ICSharpCode.SharpZipLib.Checksums.Crc32
struct Crc32_t3523361801;
// ICSharpCode.SharpZipLib.Zip.ZipEntry
struct ZipEntry_t3141689087;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Compr537202536.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Comp3174865049.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_UseZ3006992774.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.ZipOutputStream
struct  ZipOutputStream_t2362341588  : public DeflaterOutputStream_t537202536
{
public:
	// System.Collections.ArrayList ICSharpCode.SharpZipLib.Zip.ZipOutputStream::entries
	ArrayList_t3948406897 * ___entries_11;
	// ICSharpCode.SharpZipLib.Checksums.Crc32 ICSharpCode.SharpZipLib.Zip.ZipOutputStream::crc
	Crc32_t3523361801 * ___crc_12;
	// ICSharpCode.SharpZipLib.Zip.ZipEntry ICSharpCode.SharpZipLib.Zip.ZipOutputStream::curEntry
	ZipEntry_t3141689087 * ___curEntry_13;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipOutputStream::defaultCompressionLevel
	int32_t ___defaultCompressionLevel_14;
	// ICSharpCode.SharpZipLib.Zip.CompressionMethod ICSharpCode.SharpZipLib.Zip.ZipOutputStream::curMethod
	int32_t ___curMethod_15;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipOutputStream::size
	int64_t ___size_16;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipOutputStream::offset
	int64_t ___offset_17;
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.ZipOutputStream::zipComment
	ByteU5BU5D_t4260760469* ___zipComment_18;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipOutputStream::patchEntryHeader
	bool ___patchEntryHeader_19;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipOutputStream::crcPatchPos
	int64_t ___crcPatchPos_20;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipOutputStream::sizePatchPos
	int64_t ___sizePatchPos_21;
	// ICSharpCode.SharpZipLib.Zip.UseZip64 ICSharpCode.SharpZipLib.Zip.ZipOutputStream::useZip64_
	int32_t ___useZip64__22;

public:
	inline static int32_t get_offset_of_entries_11() { return static_cast<int32_t>(offsetof(ZipOutputStream_t2362341588, ___entries_11)); }
	inline ArrayList_t3948406897 * get_entries_11() const { return ___entries_11; }
	inline ArrayList_t3948406897 ** get_address_of_entries_11() { return &___entries_11; }
	inline void set_entries_11(ArrayList_t3948406897 * value)
	{
		___entries_11 = value;
		Il2CppCodeGenWriteBarrier(&___entries_11, value);
	}

	inline static int32_t get_offset_of_crc_12() { return static_cast<int32_t>(offsetof(ZipOutputStream_t2362341588, ___crc_12)); }
	inline Crc32_t3523361801 * get_crc_12() const { return ___crc_12; }
	inline Crc32_t3523361801 ** get_address_of_crc_12() { return &___crc_12; }
	inline void set_crc_12(Crc32_t3523361801 * value)
	{
		___crc_12 = value;
		Il2CppCodeGenWriteBarrier(&___crc_12, value);
	}

	inline static int32_t get_offset_of_curEntry_13() { return static_cast<int32_t>(offsetof(ZipOutputStream_t2362341588, ___curEntry_13)); }
	inline ZipEntry_t3141689087 * get_curEntry_13() const { return ___curEntry_13; }
	inline ZipEntry_t3141689087 ** get_address_of_curEntry_13() { return &___curEntry_13; }
	inline void set_curEntry_13(ZipEntry_t3141689087 * value)
	{
		___curEntry_13 = value;
		Il2CppCodeGenWriteBarrier(&___curEntry_13, value);
	}

	inline static int32_t get_offset_of_defaultCompressionLevel_14() { return static_cast<int32_t>(offsetof(ZipOutputStream_t2362341588, ___defaultCompressionLevel_14)); }
	inline int32_t get_defaultCompressionLevel_14() const { return ___defaultCompressionLevel_14; }
	inline int32_t* get_address_of_defaultCompressionLevel_14() { return &___defaultCompressionLevel_14; }
	inline void set_defaultCompressionLevel_14(int32_t value)
	{
		___defaultCompressionLevel_14 = value;
	}

	inline static int32_t get_offset_of_curMethod_15() { return static_cast<int32_t>(offsetof(ZipOutputStream_t2362341588, ___curMethod_15)); }
	inline int32_t get_curMethod_15() const { return ___curMethod_15; }
	inline int32_t* get_address_of_curMethod_15() { return &___curMethod_15; }
	inline void set_curMethod_15(int32_t value)
	{
		___curMethod_15 = value;
	}

	inline static int32_t get_offset_of_size_16() { return static_cast<int32_t>(offsetof(ZipOutputStream_t2362341588, ___size_16)); }
	inline int64_t get_size_16() const { return ___size_16; }
	inline int64_t* get_address_of_size_16() { return &___size_16; }
	inline void set_size_16(int64_t value)
	{
		___size_16 = value;
	}

	inline static int32_t get_offset_of_offset_17() { return static_cast<int32_t>(offsetof(ZipOutputStream_t2362341588, ___offset_17)); }
	inline int64_t get_offset_17() const { return ___offset_17; }
	inline int64_t* get_address_of_offset_17() { return &___offset_17; }
	inline void set_offset_17(int64_t value)
	{
		___offset_17 = value;
	}

	inline static int32_t get_offset_of_zipComment_18() { return static_cast<int32_t>(offsetof(ZipOutputStream_t2362341588, ___zipComment_18)); }
	inline ByteU5BU5D_t4260760469* get_zipComment_18() const { return ___zipComment_18; }
	inline ByteU5BU5D_t4260760469** get_address_of_zipComment_18() { return &___zipComment_18; }
	inline void set_zipComment_18(ByteU5BU5D_t4260760469* value)
	{
		___zipComment_18 = value;
		Il2CppCodeGenWriteBarrier(&___zipComment_18, value);
	}

	inline static int32_t get_offset_of_patchEntryHeader_19() { return static_cast<int32_t>(offsetof(ZipOutputStream_t2362341588, ___patchEntryHeader_19)); }
	inline bool get_patchEntryHeader_19() const { return ___patchEntryHeader_19; }
	inline bool* get_address_of_patchEntryHeader_19() { return &___patchEntryHeader_19; }
	inline void set_patchEntryHeader_19(bool value)
	{
		___patchEntryHeader_19 = value;
	}

	inline static int32_t get_offset_of_crcPatchPos_20() { return static_cast<int32_t>(offsetof(ZipOutputStream_t2362341588, ___crcPatchPos_20)); }
	inline int64_t get_crcPatchPos_20() const { return ___crcPatchPos_20; }
	inline int64_t* get_address_of_crcPatchPos_20() { return &___crcPatchPos_20; }
	inline void set_crcPatchPos_20(int64_t value)
	{
		___crcPatchPos_20 = value;
	}

	inline static int32_t get_offset_of_sizePatchPos_21() { return static_cast<int32_t>(offsetof(ZipOutputStream_t2362341588, ___sizePatchPos_21)); }
	inline int64_t get_sizePatchPos_21() const { return ___sizePatchPos_21; }
	inline int64_t* get_address_of_sizePatchPos_21() { return &___sizePatchPos_21; }
	inline void set_sizePatchPos_21(int64_t value)
	{
		___sizePatchPos_21 = value;
	}

	inline static int32_t get_offset_of_useZip64__22() { return static_cast<int32_t>(offsetof(ZipOutputStream_t2362341588, ___useZip64__22)); }
	inline int32_t get_useZip64__22() const { return ___useZip64__22; }
	inline int32_t* get_address_of_useZip64__22() { return &___useZip64__22; }
	inline void set_useZip64__22(int32_t value)
	{
		___useZip64__22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
