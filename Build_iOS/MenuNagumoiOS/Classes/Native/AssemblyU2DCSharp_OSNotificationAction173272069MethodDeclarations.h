﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OSNotificationAction
struct OSNotificationAction_t173272069;

#include "codegen/il2cpp-codegen.h"

// System.Void OSNotificationAction::.ctor()
extern "C"  void OSNotificationAction__ctor_m1944921142 (OSNotificationAction_t173272069 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
