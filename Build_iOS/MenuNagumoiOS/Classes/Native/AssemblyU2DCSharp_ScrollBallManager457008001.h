﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.ScrollRect
struct ScrollRect_t3606982749;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t2601556940;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScrollBallManager
struct  ScrollBallManager_t457008001  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.ScrollRect ScrollBallManager::scrollRect
	ScrollRect_t3606982749 * ___scrollRect_2;
	// System.Single ScrollBallManager::xPos
	float ___xPos_3;
	// System.Single ScrollBallManager::xSize
	float ___xSize_4;
	// UnityEngine.UI.Scrollbar ScrollBallManager::scrollbar
	Scrollbar_t2601556940 * ___scrollbar_5;

public:
	inline static int32_t get_offset_of_scrollRect_2() { return static_cast<int32_t>(offsetof(ScrollBallManager_t457008001, ___scrollRect_2)); }
	inline ScrollRect_t3606982749 * get_scrollRect_2() const { return ___scrollRect_2; }
	inline ScrollRect_t3606982749 ** get_address_of_scrollRect_2() { return &___scrollRect_2; }
	inline void set_scrollRect_2(ScrollRect_t3606982749 * value)
	{
		___scrollRect_2 = value;
		Il2CppCodeGenWriteBarrier(&___scrollRect_2, value);
	}

	inline static int32_t get_offset_of_xPos_3() { return static_cast<int32_t>(offsetof(ScrollBallManager_t457008001, ___xPos_3)); }
	inline float get_xPos_3() const { return ___xPos_3; }
	inline float* get_address_of_xPos_3() { return &___xPos_3; }
	inline void set_xPos_3(float value)
	{
		___xPos_3 = value;
	}

	inline static int32_t get_offset_of_xSize_4() { return static_cast<int32_t>(offsetof(ScrollBallManager_t457008001, ___xSize_4)); }
	inline float get_xSize_4() const { return ___xSize_4; }
	inline float* get_address_of_xSize_4() { return &___xSize_4; }
	inline void set_xSize_4(float value)
	{
		___xSize_4 = value;
	}

	inline static int32_t get_offset_of_scrollbar_5() { return static_cast<int32_t>(offsetof(ScrollBallManager_t457008001, ___scrollbar_5)); }
	inline Scrollbar_t2601556940 * get_scrollbar_5() const { return ___scrollbar_5; }
	inline Scrollbar_t2601556940 ** get_address_of_scrollbar_5() { return &___scrollbar_5; }
	inline void set_scrollbar_5(Scrollbar_t2601556940 * value)
	{
		___scrollbar_5 = value;
		Il2CppCodeGenWriteBarrier(&___scrollbar_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
