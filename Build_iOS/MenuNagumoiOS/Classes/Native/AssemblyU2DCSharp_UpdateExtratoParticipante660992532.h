﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// barGraphController
struct barGraphController_t2598333175;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UpdateExtratoParticipante
struct  UpdateExtratoParticipante_t660992532  : public MonoBehaviour_t667441552
{
public:
	// barGraphController UpdateExtratoParticipante::BGController
	barGraphController_t2598333175 * ___BGController_2;
	// UnityEngine.GameObject UpdateExtratoParticipante::loadingObj
	GameObject_t3674682005 * ___loadingObj_4;

public:
	inline static int32_t get_offset_of_BGController_2() { return static_cast<int32_t>(offsetof(UpdateExtratoParticipante_t660992532, ___BGController_2)); }
	inline barGraphController_t2598333175 * get_BGController_2() const { return ___BGController_2; }
	inline barGraphController_t2598333175 ** get_address_of_BGController_2() { return &___BGController_2; }
	inline void set_BGController_2(barGraphController_t2598333175 * value)
	{
		___BGController_2 = value;
		Il2CppCodeGenWriteBarrier(&___BGController_2, value);
	}

	inline static int32_t get_offset_of_loadingObj_4() { return static_cast<int32_t>(offsetof(UpdateExtratoParticipante_t660992532, ___loadingObj_4)); }
	inline GameObject_t3674682005 * get_loadingObj_4() const { return ___loadingObj_4; }
	inline GameObject_t3674682005 ** get_address_of_loadingObj_4() { return &___loadingObj_4; }
	inline void set_loadingObj_4(GameObject_t3674682005 * value)
	{
		___loadingObj_4 = value;
		Il2CppCodeGenWriteBarrier(&___loadingObj_4, value);
	}
};

struct UpdateExtratoParticipante_t660992532_StaticFields
{
public:
	// System.Boolean UpdateExtratoParticipante::onUpdate
	bool ___onUpdate_3;

public:
	inline static int32_t get_offset_of_onUpdate_3() { return static_cast<int32_t>(offsetof(UpdateExtratoParticipante_t660992532_StaticFields, ___onUpdate_3)); }
	inline bool get_onUpdate_3() const { return ___onUpdate_3; }
	inline bool* get_address_of_onUpdate_3() { return &___onUpdate_3; }
	inline void set_onUpdate_3(bool value)
	{
		___onUpdate_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
