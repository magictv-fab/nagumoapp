﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern "C" void DelegatePInvokeWrapper_AppDomainInitializer_t691583313 ();
extern "C" void DelegatePInvokeWrapper_Swapper_t4166107989 ();
extern "C" void DelegatePInvokeWrapper_InternalCancelHandler_t2258117980 ();
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t1428404869 ();
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t2583486074 ();
extern "C" void DelegatePInvokeWrapper_CrossContextDelegate_t651537830 ();
extern "C" void DelegatePInvokeWrapper_CallbackHandler_t1474775431 ();
extern "C" void DelegatePInvokeWrapper_ThreadStart_t124146534 ();
extern "C" void DelegatePInvokeWrapper_CharGetter_t4120438118 ();
extern "C" void DelegatePInvokeWrapper_ReadMethod_t1873379884 ();
extern "C" void DelegatePInvokeWrapper_UnmanagedReadOrWrite_t2055733333 ();
extern "C" void DelegatePInvokeWrapper_WriteMethod_t3250749483 ();
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t3891738222 ();
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t1637408689 ();
extern "C" void DelegatePInvokeWrapper_SocketAsyncCall_t742231849 ();
extern "C" void DelegatePInvokeWrapper_CostDelegate_t1292950321 ();
extern "C" void DelegatePInvokeWrapper_Action_t3771233898 ();
extern "C" void DelegatePInvokeWrapper_AndroidJavaRunnable_t1289602340 ();
extern "C" void DelegatePInvokeWrapper_LogCallback_t2984951347 ();
extern "C" void DelegatePInvokeWrapper_PCMReaderCallback_t83861602 ();
extern "C" void DelegatePInvokeWrapper_PCMSetPositionCallback_t4244274966 ();
extern "C" void DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t1377657005 ();
extern "C" void DelegatePInvokeWrapper_WillRenderCanvases_t4247149838 ();
extern "C" void DelegatePInvokeWrapper_StateChanged_t2578300556 ();
extern "C" void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t581305515 ();
extern "C" void DelegatePInvokeWrapper_UnityAction_t594794173 ();
extern "C" void DelegatePInvokeWrapper_FontTextureRebuildCallback_t4168056797 ();
extern "C" void DelegatePInvokeWrapper_WindowFunction_t2749288659 ();
extern "C" void DelegatePInvokeWrapper_SkinChangedDelegate_t4002872878 ();
extern "C" void DelegatePInvokeWrapper_BannerFailedToLoadDelegate_t2244784594 ();
extern "C" void DelegatePInvokeWrapper_BannerWasClickedDelegate_t3510840818 ();
extern "C" void DelegatePInvokeWrapper_BannerWasLoadedDelegate_t3604098244 ();
extern "C" void DelegatePInvokeWrapper_InterstitialWasLoadedDelegate_t3068494210 ();
extern "C" void DelegatePInvokeWrapper_InterstitialWasViewedDelegate_t507319425 ();
extern "C" void DelegatePInvokeWrapper_OnError_t1516540506 ();
extern "C" void DelegatePInvokeWrapper_OnSuccess_t1013330901 ();
extern "C" void DelegatePInvokeWrapper_EasyWebCamStartedDelegate_t3886828347 ();
extern "C" void DelegatePInvokeWrapper_EasyWebCamStopedDelegate_t2264447425 ();
extern "C" void DelegatePInvokeWrapper_EasyWebCamUpdateDelegate_t1731309161 ();
extern "C" void DelegatePInvokeWrapper_OnValidateInput_t3952708057 ();
extern "C" void DelegatePInvokeWrapper_OnChangeVisible_t527731977 ();
extern "C" void DelegatePInvokeWrapper_OnAwakeEventHandler_t1806304177 ();
extern "C" void DelegatePInvokeWrapper_OnPauseEventHandler_t3609693858 ();
extern "C" void DelegatePInvokeWrapper_OnWifiInfoEventHandler_t558195666 ();
extern "C" void DelegatePInvokeWrapper_OnCompleteEventHandler_t2318258528 ();
extern "C" void DelegatePInvokeWrapper_OnErrorEventHandler_t1722022561 ();
extern "C" void DelegatePInvokeWrapper_OnProgressEventHandler_t3695678548 ();
extern "C" void DelegatePInvokeWrapper_TurnChange_t3311189389 ();
extern "C" void DelegatePInvokeWrapper_OnFinishedEvent_t3844636377 ();
extern "C" void DelegatePInvokeWrapper_OnReadyEventHandlerandler_t2819726214 ();
extern "C" void DelegatePInvokeWrapper_OnTrack_t1498492137 ();
extern "C" void DelegatePInvokeWrapper_OnMoveHandler_t662891892 ();
extern "C" void DelegatePInvokeWrapper_OnVoidEventHandler_t1904900265 ();
extern "C" void DelegatePInvokeWrapper_OnRotateComplete_t2396770826 ();
extern "C" void DelegatePInvokeWrapper_OnChangeEventHandler_t2629944389 ();
extern "C" void DelegatePInvokeWrapper_VoidEvent_t1756309665 ();
extern "C" void DelegatePInvokeWrapper_OnScreenOrientationEventHandler_t449305768 ();
extern "C" void DelegatePInvokeWrapper_OnComponentIsReadyEventHandler_t1240798929 ();
extern "C" void DelegatePInvokeWrapper_OnReadyEventHandler_t1972392543 ();
extern "C" void DelegatePInvokeWrapper_OnCronEventHandler_t343176941 ();
extern "C" void DelegatePInvokeWrapper_OnStartErrorEventHandler_t726677771 ();
extern "C" void DelegatePInvokeWrapper_OnProgressEventHandler_t1956336554 ();
extern "C" void DelegatePInvokeWrapper_OnVoidEvent_t2058934555 ();
extern "C" void DelegatePInvokeWrapper_OnEvent_t2458123614 ();
extern "C" void DelegatePInvokeWrapper_OnLoadCompleted_t2465843575 ();
extern "C" void DelegatePInvokeWrapper_OnComplete_t3814375362 ();
extern "C" void DelegatePInvokeWrapper_OnDLLLoaded_t970356664 ();
extern "C" void DelegatePInvokeWrapper_HideUnityDelegate_t3175190102 ();
extern "C" void DelegatePInvokeWrapper_InitDelegate_t5726901 ();
extern "C" void DelegatePInvokeWrapper_Delegate_escapeString_t2694597681 ();
extern "C" void DelegatePInvokeWrapper_EasingFunction_t524263911 ();
extern "C" void DelegatePInvokeWrapper_ConfirmOverwriteDelegate_t2877285688 ();
extern "C" void DelegatePInvokeWrapper_ReadDataHandler_t2108073026 ();
extern "C" void DelegatePInvokeWrapper_InAppEventDispatcherOnOff_t3474551315 ();
extern "C" void DelegatePInvokeWrapper_InAppEventDispatcherWithString_t1910714196 ();
extern "C" void DelegatePInvokeWrapper_OnTransformAxisEventHandler_t3424591110 ();
extern "C" void DelegatePInvokeWrapper_ApplyTween_t882368618 ();
extern "C" void DelegatePInvokeWrapper_EasingFunction_t1323017328 ();
extern "C" void DelegatePInvokeWrapper_OnBool_t3660731593 ();
extern "C" void DelegatePInvokeWrapper_OnUpdateRequestErrorEventHandler_t1521989041 ();
extern "C" void DelegatePInvokeWrapper_OnLoadAnswerEventHandler_t3624955211 ();
extern "C" void DelegatePInvokeWrapper_OnVoidEventHandler_t284867202 ();
extern "C" void DelegatePInvokeWrapper_OnARToggleOnOff_t2139692132 ();
extern "C" void DelegatePInvokeWrapper_OnARWantToDoSomething_t4073481420 ();
extern "C" void DelegatePInvokeWrapper_OnStringEvent_t3419556962 ();
extern "C" void DelegatePInvokeWrapper_OnVoidEventHandler_t622442222 ();
extern "C" void DelegatePInvokeWrapper_OnSomething_t1610687155 ();
extern "C" void DelegatePInvokeWrapper_OnString_t2272041080 ();
extern "C" void DelegatePInvokeWrapper_OnChangeAnToPerspectiveEventHandler_t2702236419 ();
extern "C" void DelegatePInvokeWrapper_OnChangeToPerspectiveEventHandler_t1587175024 ();
extern "C" void DelegatePInvokeWrapper_OnIntBoolEventHandler_t587466104 ();
extern "C" void DelegatePInvokeWrapper_OnStringEventHandler_t2329146386 ();
extern "C" void DelegatePInvokeWrapper_OnVoidEventHandler_t2512456021 ();
extern "C" void DelegatePInvokeWrapper_OnChangeToAnStateEventHandler_t630243546 ();
extern "C" void DelegatePInvokeWrapper_OnChangeToStateEventHandler_t4233189127 ();
extern "C" void DelegatePInvokeWrapper_OnFloatEventHandler_t1243657927 ();
extern "C" void DelegatePInvokeWrapper_OnMetaEventHandler_t4203203482 ();
extern "C" void DelegatePInvokeWrapper_OnStringEventHandler_t1157926502 ();
extern "C" void DelegatePInvokeWrapper_OnInAppProgressEventHandler_t3139301368 ();
extern "C" void DelegatePInvokeWrapper_OnBooleanEventHandler_t2032344769 ();
extern "C" void DelegatePInvokeWrapper_ScreenShotCompleteDelegate_t509502638 ();
extern "C" void DelegatePInvokeWrapper_ClickOkPromptDelegate_t3728636616 ();
extern "C" void DelegatePInvokeWrapper_CallbackImagePicked_t554520473 ();
extern "C" void DelegatePInvokeWrapper_IdsAvailableCallback_t3679464791 ();
extern "C" void DelegatePInvokeWrapper_OnLogoutEmailSuccess_t3366364849 ();
extern "C" void DelegatePInvokeWrapper_OnSetEmailSuccess_t2952831753 ();
extern "C" void DelegatePInvokeWrapper_PromptForPushNotificationsUserResponse_t2193543734 ();
extern "C" void DelegatePInvokeWrapper_OnEvent_t314892251 ();
extern "C" void DelegatePInvokeWrapper_QRScanFinished_t1583106503 ();
extern "C" void DelegatePInvokeWrapper_OnCompleteEventHandler_t2690410097 ();
extern "C" void DelegatePInvokeWrapper_SelectChildren_t1389527649 ();
extern "C" void DelegatePInvokeWrapper_SelectChildren_t2120299545 ();
extern "C" void DelegatePInvokeWrapper_OnPlayDelegate_t3097072301 ();
extern "C" void DelegatePInvokeWrapper_OnBundleIDEventHandler_t3610444662 ();
extern "C" void DelegatePInvokeWrapper_OnTransformEventHandler_t2999119763 ();
extern "C" void DelegatePInvokeWrapper_EventHandlerFunction_t2672185540 ();
extern "C" void DelegatePInvokeWrapper_PageSnapChange_t3629032906 ();
extern "C" void DelegatePInvokeWrapper_UnitySendMessageDelegate_t2158631822 ();
extern const Il2CppMethodPointer g_DelegateWrappersManagedToNative[118] = 
{
	DelegatePInvokeWrapper_AppDomainInitializer_t691583313,
	DelegatePInvokeWrapper_Swapper_t4166107989,
	DelegatePInvokeWrapper_InternalCancelHandler_t2258117980,
	DelegatePInvokeWrapper_ReadDelegate_t1428404869,
	DelegatePInvokeWrapper_WriteDelegate_t2583486074,
	DelegatePInvokeWrapper_CrossContextDelegate_t651537830,
	DelegatePInvokeWrapper_CallbackHandler_t1474775431,
	DelegatePInvokeWrapper_ThreadStart_t124146534,
	DelegatePInvokeWrapper_CharGetter_t4120438118,
	DelegatePInvokeWrapper_ReadMethod_t1873379884,
	DelegatePInvokeWrapper_UnmanagedReadOrWrite_t2055733333,
	DelegatePInvokeWrapper_WriteMethod_t3250749483,
	DelegatePInvokeWrapper_ReadDelegate_t3891738222,
	DelegatePInvokeWrapper_WriteDelegate_t1637408689,
	DelegatePInvokeWrapper_SocketAsyncCall_t742231849,
	DelegatePInvokeWrapper_CostDelegate_t1292950321,
	DelegatePInvokeWrapper_Action_t3771233898,
	DelegatePInvokeWrapper_AndroidJavaRunnable_t1289602340,
	DelegatePInvokeWrapper_LogCallback_t2984951347,
	DelegatePInvokeWrapper_PCMReaderCallback_t83861602,
	DelegatePInvokeWrapper_PCMSetPositionCallback_t4244274966,
	DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t1377657005,
	DelegatePInvokeWrapper_WillRenderCanvases_t4247149838,
	DelegatePInvokeWrapper_StateChanged_t2578300556,
	DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t581305515,
	DelegatePInvokeWrapper_UnityAction_t594794173,
	DelegatePInvokeWrapper_FontTextureRebuildCallback_t4168056797,
	DelegatePInvokeWrapper_WindowFunction_t2749288659,
	DelegatePInvokeWrapper_SkinChangedDelegate_t4002872878,
	DelegatePInvokeWrapper_BannerFailedToLoadDelegate_t2244784594,
	DelegatePInvokeWrapper_BannerWasClickedDelegate_t3510840818,
	DelegatePInvokeWrapper_BannerWasLoadedDelegate_t3604098244,
	DelegatePInvokeWrapper_InterstitialWasLoadedDelegate_t3068494210,
	DelegatePInvokeWrapper_InterstitialWasViewedDelegate_t507319425,
	DelegatePInvokeWrapper_OnError_t1516540506,
	DelegatePInvokeWrapper_OnSuccess_t1013330901,
	DelegatePInvokeWrapper_EasyWebCamStartedDelegate_t3886828347,
	DelegatePInvokeWrapper_EasyWebCamStopedDelegate_t2264447425,
	DelegatePInvokeWrapper_EasyWebCamUpdateDelegate_t1731309161,
	DelegatePInvokeWrapper_OnValidateInput_t3952708057,
	DelegatePInvokeWrapper_OnChangeVisible_t527731977,
	DelegatePInvokeWrapper_OnAwakeEventHandler_t1806304177,
	DelegatePInvokeWrapper_OnPauseEventHandler_t3609693858,
	DelegatePInvokeWrapper_OnWifiInfoEventHandler_t558195666,
	DelegatePInvokeWrapper_OnCompleteEventHandler_t2318258528,
	DelegatePInvokeWrapper_OnErrorEventHandler_t1722022561,
	DelegatePInvokeWrapper_OnProgressEventHandler_t3695678548,
	DelegatePInvokeWrapper_TurnChange_t3311189389,
	DelegatePInvokeWrapper_OnFinishedEvent_t3844636377,
	DelegatePInvokeWrapper_OnReadyEventHandlerandler_t2819726214,
	DelegatePInvokeWrapper_OnTrack_t1498492137,
	DelegatePInvokeWrapper_OnMoveHandler_t662891892,
	DelegatePInvokeWrapper_OnVoidEventHandler_t1904900265,
	DelegatePInvokeWrapper_OnRotateComplete_t2396770826,
	DelegatePInvokeWrapper_OnChangeEventHandler_t2629944389,
	DelegatePInvokeWrapper_VoidEvent_t1756309665,
	DelegatePInvokeWrapper_OnScreenOrientationEventHandler_t449305768,
	DelegatePInvokeWrapper_OnComponentIsReadyEventHandler_t1240798929,
	DelegatePInvokeWrapper_OnReadyEventHandler_t1972392543,
	DelegatePInvokeWrapper_OnCronEventHandler_t343176941,
	DelegatePInvokeWrapper_OnStartErrorEventHandler_t726677771,
	DelegatePInvokeWrapper_OnProgressEventHandler_t1956336554,
	DelegatePInvokeWrapper_OnVoidEvent_t2058934555,
	DelegatePInvokeWrapper_OnEvent_t2458123614,
	DelegatePInvokeWrapper_OnLoadCompleted_t2465843575,
	DelegatePInvokeWrapper_OnComplete_t3814375362,
	DelegatePInvokeWrapper_OnDLLLoaded_t970356664,
	DelegatePInvokeWrapper_HideUnityDelegate_t3175190102,
	DelegatePInvokeWrapper_InitDelegate_t5726901,
	DelegatePInvokeWrapper_Delegate_escapeString_t2694597681,
	DelegatePInvokeWrapper_EasingFunction_t524263911,
	DelegatePInvokeWrapper_ConfirmOverwriteDelegate_t2877285688,
	DelegatePInvokeWrapper_ReadDataHandler_t2108073026,
	DelegatePInvokeWrapper_InAppEventDispatcherOnOff_t3474551315,
	DelegatePInvokeWrapper_InAppEventDispatcherWithString_t1910714196,
	DelegatePInvokeWrapper_OnTransformAxisEventHandler_t3424591110,
	DelegatePInvokeWrapper_ApplyTween_t882368618,
	DelegatePInvokeWrapper_EasingFunction_t1323017328,
	DelegatePInvokeWrapper_OnBool_t3660731593,
	DelegatePInvokeWrapper_OnUpdateRequestErrorEventHandler_t1521989041,
	DelegatePInvokeWrapper_OnLoadAnswerEventHandler_t3624955211,
	DelegatePInvokeWrapper_OnVoidEventHandler_t284867202,
	DelegatePInvokeWrapper_OnARToggleOnOff_t2139692132,
	DelegatePInvokeWrapper_OnARWantToDoSomething_t4073481420,
	DelegatePInvokeWrapper_OnStringEvent_t3419556962,
	DelegatePInvokeWrapper_OnVoidEventHandler_t622442222,
	DelegatePInvokeWrapper_OnSomething_t1610687155,
	DelegatePInvokeWrapper_OnString_t2272041080,
	DelegatePInvokeWrapper_OnChangeAnToPerspectiveEventHandler_t2702236419,
	DelegatePInvokeWrapper_OnChangeToPerspectiveEventHandler_t1587175024,
	DelegatePInvokeWrapper_OnIntBoolEventHandler_t587466104,
	DelegatePInvokeWrapper_OnStringEventHandler_t2329146386,
	DelegatePInvokeWrapper_OnVoidEventHandler_t2512456021,
	DelegatePInvokeWrapper_OnChangeToAnStateEventHandler_t630243546,
	DelegatePInvokeWrapper_OnChangeToStateEventHandler_t4233189127,
	DelegatePInvokeWrapper_OnFloatEventHandler_t1243657927,
	DelegatePInvokeWrapper_OnMetaEventHandler_t4203203482,
	DelegatePInvokeWrapper_OnStringEventHandler_t1157926502,
	DelegatePInvokeWrapper_OnInAppProgressEventHandler_t3139301368,
	DelegatePInvokeWrapper_OnBooleanEventHandler_t2032344769,
	DelegatePInvokeWrapper_ScreenShotCompleteDelegate_t509502638,
	DelegatePInvokeWrapper_ClickOkPromptDelegate_t3728636616,
	DelegatePInvokeWrapper_CallbackImagePicked_t554520473,
	DelegatePInvokeWrapper_IdsAvailableCallback_t3679464791,
	DelegatePInvokeWrapper_OnLogoutEmailSuccess_t3366364849,
	DelegatePInvokeWrapper_OnSetEmailSuccess_t2952831753,
	DelegatePInvokeWrapper_PromptForPushNotificationsUserResponse_t2193543734,
	DelegatePInvokeWrapper_OnEvent_t314892251,
	DelegatePInvokeWrapper_QRScanFinished_t1583106503,
	DelegatePInvokeWrapper_OnCompleteEventHandler_t2690410097,
	DelegatePInvokeWrapper_SelectChildren_t1389527649,
	DelegatePInvokeWrapper_SelectChildren_t2120299545,
	DelegatePInvokeWrapper_OnPlayDelegate_t3097072301,
	DelegatePInvokeWrapper_OnBundleIDEventHandler_t3610444662,
	DelegatePInvokeWrapper_OnTransformEventHandler_t2999119763,
	DelegatePInvokeWrapper_EventHandlerFunction_t2672185540,
	DelegatePInvokeWrapper_PageSnapChange_t3629032906,
	DelegatePInvokeWrapper_UnitySendMessageDelegate_t2158631822,
};
