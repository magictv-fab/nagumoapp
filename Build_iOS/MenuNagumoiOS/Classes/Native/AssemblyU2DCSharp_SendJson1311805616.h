﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.InputField
struct InputField_t609046876;
// UnityEngine.UI.Text
struct Text_t9039225;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SendJson
struct  SendJson_t1311805616  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.InputField SendJson::jsonInput
	InputField_t609046876 * ___jsonInput_2;
	// UnityEngine.UI.Text SendJson::resp
	Text_t9039225 * ___resp_3;
	// System.String SendJson::url
	String_t* ___url_4;

public:
	inline static int32_t get_offset_of_jsonInput_2() { return static_cast<int32_t>(offsetof(SendJson_t1311805616, ___jsonInput_2)); }
	inline InputField_t609046876 * get_jsonInput_2() const { return ___jsonInput_2; }
	inline InputField_t609046876 ** get_address_of_jsonInput_2() { return &___jsonInput_2; }
	inline void set_jsonInput_2(InputField_t609046876 * value)
	{
		___jsonInput_2 = value;
		Il2CppCodeGenWriteBarrier(&___jsonInput_2, value);
	}

	inline static int32_t get_offset_of_resp_3() { return static_cast<int32_t>(offsetof(SendJson_t1311805616, ___resp_3)); }
	inline Text_t9039225 * get_resp_3() const { return ___resp_3; }
	inline Text_t9039225 ** get_address_of_resp_3() { return &___resp_3; }
	inline void set_resp_3(Text_t9039225 * value)
	{
		___resp_3 = value;
		Il2CppCodeGenWriteBarrier(&___resp_3, value);
	}

	inline static int32_t get_offset_of_url_4() { return static_cast<int32_t>(offsetof(SendJson_t1311805616, ___url_4)); }
	inline String_t* get_url_4() const { return ___url_4; }
	inline String_t** get_address_of_url_4() { return &___url_4; }
	inline void set_url_4(String_t* value)
	{
		___url_4 = value;
		Il2CppCodeGenWriteBarrier(&___url_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
