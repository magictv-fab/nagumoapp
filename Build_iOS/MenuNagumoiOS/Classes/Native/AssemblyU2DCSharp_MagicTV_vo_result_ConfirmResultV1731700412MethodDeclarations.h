﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.vo.result.ConfirmResultVO
struct ConfirmResultVO_t1731700412;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.vo.result.ConfirmResultVO::.ctor()
extern "C"  void ConfirmResultVO__ctor_m2600693456 (ConfirmResultVO_t1731700412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.vo.result.ConfirmResultVO::.cctor()
extern "C"  void ConfirmResultVO__cctor_m2829989597 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
