﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.MSIReader
struct MSIReader_t2057678968;
// ZXing.Result
struct Result_t2610723219;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>
struct IDictionary_2_t759989846;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"
#include "mscorlib_System_String7231557.h"

// System.Void ZXing.OneD.MSIReader::.ctor(System.Boolean)
extern "C"  void MSIReader__ctor_m3375086418 (MSIReader_t2057678968 * __this, bool ___usingCheckDigit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Result ZXing.OneD.MSIReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  Result_t2610723219 * MSIReader_decodeRow_m2594321382 (MSIReader_t2057678968 * __this, int32_t ___rowNumber0, BitArray_t4163851164 * ___row1, Il2CppObject* ___hints2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.OneD.MSIReader::findStartPattern(ZXing.Common.BitArray,System.Int32[])
extern "C"  Int32U5BU5D_t3230847821* MSIReader_findStartPattern_m1419800036 (MSIReader_t2057678968 * __this, BitArray_t4163851164 * ___row0, Int32U5BU5D_t3230847821* ___counters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.OneD.MSIReader::findEndPattern(ZXing.Common.BitArray,System.Int32,System.Int32[])
extern "C"  Int32U5BU5D_t3230847821* MSIReader_findEndPattern_m1869664552 (MSIReader_t2057678968 * __this, BitArray_t4163851164 * ___row0, int32_t ___rowOffset1, Int32U5BU5D_t3230847821* ___counters2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.MSIReader::calculateAverageCounterWidth(System.Int32[],System.Int32)
extern "C"  void MSIReader_calculateAverageCounterWidth_m2335315902 (MSIReader_t2057678968 * __this, Int32U5BU5D_t3230847821* ___counters0, int32_t ___patternLength1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.MSIReader::toPattern(System.Int32[],System.Int32)
extern "C"  int32_t MSIReader_toPattern_m460167400 (MSIReader_t2057678968 * __this, Int32U5BU5D_t3230847821* ___counters0, int32_t ___patternLength1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.MSIReader::patternToChar(System.Int32,System.Char&)
extern "C"  bool MSIReader_patternToChar_m2517938782 (Il2CppObject * __this /* static, unused */, int32_t ___pattern0, Il2CppChar* ___c1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.MSIReader::CalculateChecksumLuhn(System.String)
extern "C"  int32_t MSIReader_CalculateChecksumLuhn_m564328063 (Il2CppObject * __this /* static, unused */, String_t* ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.MSIReader::.cctor()
extern "C"  void MSIReader__cctor_m3970129394 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
