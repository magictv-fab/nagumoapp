﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniWebView/OnWebContentProcessTerminatedDelegate
struct OnWebContentProcessTerminatedDelegate_t1791934381;
// System.Object
struct Il2CppObject;
// UniWebView
struct UniWebView_t424341801;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_UniWebView424341801.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void UniWebView/OnWebContentProcessTerminatedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void OnWebContentProcessTerminatedDelegate__ctor_m2136160724 (OnWebContentProcessTerminatedDelegate_t1791934381 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView/OnWebContentProcessTerminatedDelegate::Invoke(UniWebView)
extern "C"  void OnWebContentProcessTerminatedDelegate_Invoke_m1121220837 (OnWebContentProcessTerminatedDelegate_t1791934381 * __this, UniWebView_t424341801 * ___webView0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult UniWebView/OnWebContentProcessTerminatedDelegate::BeginInvoke(UniWebView,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnWebContentProcessTerminatedDelegate_BeginInvoke_m3416557074 (OnWebContentProcessTerminatedDelegate_t1791934381 * __this, UniWebView_t424341801 * ___webView0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebView/OnWebContentProcessTerminatedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void OnWebContentProcessTerminatedDelegate_EndInvoke_m2488579556 (OnWebContentProcessTerminatedDelegate_t1791934381 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
