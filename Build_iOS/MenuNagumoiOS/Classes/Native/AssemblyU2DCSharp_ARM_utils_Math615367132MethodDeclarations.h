﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Single[]
struct SingleU5BU5D_t2316563989;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// UnityEngine.Quaternion[]
struct QuaternionU5BU5D_t2399324759;
// System.Collections.Generic.List`1<System.Double>
struct List_1_t941444821;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

// System.Single ARM.utils.Math::getMedian(System.Int32[])
extern "C"  float Math_getMedian_m3212802917 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ARM.utils.Math::getMeanAngle(System.Single[])
extern "C"  float Math_getMeanAngle_m1093227013 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t2316563989* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ARM.utils.Math::getMedianAngle(System.Single[])
extern "C"  float Math_getMedianAngle_m3996756682 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t2316563989* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARM.utils.Math::getQuadrantOfAngle(System.Single)
extern "C"  int32_t Math_getQuadrantOfAngle_m1392374163 (Il2CppObject * __this /* static, unused */, float ___angle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ARM.utils.Math::normalizeAngle(System.Single)
extern "C"  float Math_normalizeAngle_m4145120569 (Il2CppObject * __this /* static, unused */, float ___angle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ARM.utils.Math::getMedian(System.Single[])
extern "C"  float Math_getMedian_m306373011 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t2316563989* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.utils.Math::getMedian(UnityEngine.Vector3[])
extern "C"  Vector3_t4282066566  Math_getMedian_m1701343599 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t215400611* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ARM.utils.Math::getMean(System.Single[])
extern "C"  float Math_getMean_m2578473336 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t2316563989* ___positions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.utils.Math::getMean(UnityEngine.Vector3[])
extern "C"  Vector3_t4282066566  Math_getMean_m1136854292 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t215400611* ___positions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion ARM.utils.Math::getMean(UnityEngine.Quaternion[])
extern "C"  Quaternion_t1553702882  Math_getMean_m1326364382 (Il2CppObject * __this /* static, unused */, QuaternionU5BU5D_t2399324759* ___positions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double ARM.utils.Math::getStandardDeviation(System.Collections.Generic.List`1<System.Double>)
extern "C"  double Math_getStandardDeviation_m1319533957 (Il2CppObject * __this /* static, unused */, List_1_t941444821 * ___doubleList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.utils.Math::InvertPosition(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Math_InvertPosition_m644092320 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion ARM.utils.Math::InvertRotation(UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  Math_InvertRotation_m2805535495 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___rotation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
