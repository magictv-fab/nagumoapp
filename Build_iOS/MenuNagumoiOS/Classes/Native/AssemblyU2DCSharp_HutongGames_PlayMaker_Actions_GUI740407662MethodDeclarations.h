﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutBox
struct GUILayoutBox_t740407662;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutBox::.ctor()
extern "C"  void GUILayoutBox__ctor_m382447800 (GUILayoutBox_t740407662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBox::Reset()
extern "C"  void GUILayoutBox_Reset_m2323848037 (GUILayoutBox_t740407662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBox::OnGUI()
extern "C"  void GUILayoutBox_OnGUI_m4172813746 (GUILayoutBox_t740407662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
