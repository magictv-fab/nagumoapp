﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.Common.ReedSolomon.ReedSolomonDecoder
struct ReedSolomonDecoder_t4166992619;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Maxicode.Internal.Decoder
struct  Decoder_t3124105626  : public Il2CppObject
{
public:
	// ZXing.Common.ReedSolomon.ReedSolomonDecoder ZXing.Maxicode.Internal.Decoder::rsDecoder
	ReedSolomonDecoder_t4166992619 * ___rsDecoder_0;

public:
	inline static int32_t get_offset_of_rsDecoder_0() { return static_cast<int32_t>(offsetof(Decoder_t3124105626, ___rsDecoder_0)); }
	inline ReedSolomonDecoder_t4166992619 * get_rsDecoder_0() const { return ___rsDecoder_0; }
	inline ReedSolomonDecoder_t4166992619 ** get_address_of_rsDecoder_0() { return &___rsDecoder_0; }
	inline void set_rsDecoder_0(ReedSolomonDecoder_t4166992619 * value)
	{
		___rsDecoder_0 = value;
		Il2CppCodeGenWriteBarrier(&___rsDecoder_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
