﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmColor
struct FsmColor_t2131419205;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAmbientLight
struct  SetAmbientLight_t3560184814  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.SetAmbientLight::ambientColor
	FsmColor_t2131419205 * ___ambientColor_9;
	// System.Boolean HutongGames.PlayMaker.Actions.SetAmbientLight::everyFrame
	bool ___everyFrame_10;

public:
	inline static int32_t get_offset_of_ambientColor_9() { return static_cast<int32_t>(offsetof(SetAmbientLight_t3560184814, ___ambientColor_9)); }
	inline FsmColor_t2131419205 * get_ambientColor_9() const { return ___ambientColor_9; }
	inline FsmColor_t2131419205 ** get_address_of_ambientColor_9() { return &___ambientColor_9; }
	inline void set_ambientColor_9(FsmColor_t2131419205 * value)
	{
		___ambientColor_9 = value;
		Il2CppCodeGenWriteBarrier(&___ambientColor_9, value);
	}

	inline static int32_t get_offset_of_everyFrame_10() { return static_cast<int32_t>(offsetof(SetAmbientLight_t3560184814, ___everyFrame_10)); }
	inline bool get_everyFrame_10() const { return ___everyFrame_10; }
	inline bool* get_address_of_everyFrame_10() { return &___everyFrame_10; }
	inline void set_everyFrame_10(bool value)
	{
		___everyFrame_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
