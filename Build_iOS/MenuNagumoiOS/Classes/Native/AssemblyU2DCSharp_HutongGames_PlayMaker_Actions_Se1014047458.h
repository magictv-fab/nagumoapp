﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co2052155886.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetVisibility
struct  SetVisibility_t1014047458  : public ComponentAction_1_t2052155886
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetVisibility::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetVisibility::toggle
	FsmBool_t1075959796 * ___toggle_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetVisibility::visible
	FsmBool_t1075959796 * ___visible_13;
	// System.Boolean HutongGames.PlayMaker.Actions.SetVisibility::resetOnExit
	bool ___resetOnExit_14;
	// System.Boolean HutongGames.PlayMaker.Actions.SetVisibility::initialVisibility
	bool ___initialVisibility_15;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(SetVisibility_t1014047458, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_toggle_12() { return static_cast<int32_t>(offsetof(SetVisibility_t1014047458, ___toggle_12)); }
	inline FsmBool_t1075959796 * get_toggle_12() const { return ___toggle_12; }
	inline FsmBool_t1075959796 ** get_address_of_toggle_12() { return &___toggle_12; }
	inline void set_toggle_12(FsmBool_t1075959796 * value)
	{
		___toggle_12 = value;
		Il2CppCodeGenWriteBarrier(&___toggle_12, value);
	}

	inline static int32_t get_offset_of_visible_13() { return static_cast<int32_t>(offsetof(SetVisibility_t1014047458, ___visible_13)); }
	inline FsmBool_t1075959796 * get_visible_13() const { return ___visible_13; }
	inline FsmBool_t1075959796 ** get_address_of_visible_13() { return &___visible_13; }
	inline void set_visible_13(FsmBool_t1075959796 * value)
	{
		___visible_13 = value;
		Il2CppCodeGenWriteBarrier(&___visible_13, value);
	}

	inline static int32_t get_offset_of_resetOnExit_14() { return static_cast<int32_t>(offsetof(SetVisibility_t1014047458, ___resetOnExit_14)); }
	inline bool get_resetOnExit_14() const { return ___resetOnExit_14; }
	inline bool* get_address_of_resetOnExit_14() { return &___resetOnExit_14; }
	inline void set_resetOnExit_14(bool value)
	{
		___resetOnExit_14 = value;
	}

	inline static int32_t get_offset_of_initialVisibility_15() { return static_cast<int32_t>(offsetof(SetVisibility_t1014047458, ___initialVisibility_15)); }
	inline bool get_initialVisibility_15() const { return ___initialVisibility_15; }
	inline bool* get_address_of_initialVisibility_15() { return &___initialVisibility_15; }
	inline void set_initialVisibility_15(bool value)
	{
		___initialVisibility_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
