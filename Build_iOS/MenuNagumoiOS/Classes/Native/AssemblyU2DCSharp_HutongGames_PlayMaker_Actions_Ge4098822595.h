﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetSubstring
struct  GetSubstring_t4098822595  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetSubstring::stringVariable
	FsmString_t952858651 * ___stringVariable_9;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetSubstring::startIndex
	FsmInt_t1596138449 * ___startIndex_10;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetSubstring::length
	FsmInt_t1596138449 * ___length_11;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetSubstring::storeResult
	FsmString_t952858651 * ___storeResult_12;
	// System.Boolean HutongGames.PlayMaker.Actions.GetSubstring::everyFrame
	bool ___everyFrame_13;

public:
	inline static int32_t get_offset_of_stringVariable_9() { return static_cast<int32_t>(offsetof(GetSubstring_t4098822595, ___stringVariable_9)); }
	inline FsmString_t952858651 * get_stringVariable_9() const { return ___stringVariable_9; }
	inline FsmString_t952858651 ** get_address_of_stringVariable_9() { return &___stringVariable_9; }
	inline void set_stringVariable_9(FsmString_t952858651 * value)
	{
		___stringVariable_9 = value;
		Il2CppCodeGenWriteBarrier(&___stringVariable_9, value);
	}

	inline static int32_t get_offset_of_startIndex_10() { return static_cast<int32_t>(offsetof(GetSubstring_t4098822595, ___startIndex_10)); }
	inline FsmInt_t1596138449 * get_startIndex_10() const { return ___startIndex_10; }
	inline FsmInt_t1596138449 ** get_address_of_startIndex_10() { return &___startIndex_10; }
	inline void set_startIndex_10(FsmInt_t1596138449 * value)
	{
		___startIndex_10 = value;
		Il2CppCodeGenWriteBarrier(&___startIndex_10, value);
	}

	inline static int32_t get_offset_of_length_11() { return static_cast<int32_t>(offsetof(GetSubstring_t4098822595, ___length_11)); }
	inline FsmInt_t1596138449 * get_length_11() const { return ___length_11; }
	inline FsmInt_t1596138449 ** get_address_of_length_11() { return &___length_11; }
	inline void set_length_11(FsmInt_t1596138449 * value)
	{
		___length_11 = value;
		Il2CppCodeGenWriteBarrier(&___length_11, value);
	}

	inline static int32_t get_offset_of_storeResult_12() { return static_cast<int32_t>(offsetof(GetSubstring_t4098822595, ___storeResult_12)); }
	inline FsmString_t952858651 * get_storeResult_12() const { return ___storeResult_12; }
	inline FsmString_t952858651 ** get_address_of_storeResult_12() { return &___storeResult_12; }
	inline void set_storeResult_12(FsmString_t952858651 * value)
	{
		___storeResult_12 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_12, value);
	}

	inline static int32_t get_offset_of_everyFrame_13() { return static_cast<int32_t>(offsetof(GetSubstring_t4098822595, ___everyFrame_13)); }
	inline bool get_everyFrame_13() const { return ___everyFrame_13; }
	inline bool* get_address_of_everyFrame_13() { return &___everyFrame_13; }
	inline void set_everyFrame_13(bool value)
	{
		___everyFrame_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
