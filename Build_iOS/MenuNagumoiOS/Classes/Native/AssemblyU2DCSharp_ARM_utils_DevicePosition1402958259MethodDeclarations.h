﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.utils.DevicePosition
struct DevicePosition_t1402958259;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

// System.Void ARM.utils.DevicePosition::.ctor()
extern "C"  void DevicePosition__ctor_m3871351117 (DevicePosition_t1402958259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.utils.DevicePosition::start()
extern "C"  void DevicePosition_start_m1153835629 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARM.utils.DevicePosition::getVector()
extern "C"  Vector3_t4282066566  DevicePosition_getVector_m3543515722 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion ARM.utils.DevicePosition::getQuaternion()
extern "C"  Quaternion_t1553702882  DevicePosition_getQuaternion_m942458007 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
