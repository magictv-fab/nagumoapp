﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Extensions.HorizontalScrollSnap
struct HorizontalScrollSnap_t2651831999;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2662109048;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1848751023;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1848751023.h"

// System.Void UnityEngine.UI.Extensions.HorizontalScrollSnap::.ctor()
extern "C"  void HorizontalScrollSnap__ctor_m305574857 (HorizontalScrollSnap_t2651831999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.HorizontalScrollSnap::Start()
extern "C"  void HorizontalScrollSnap_Start_m3547679945 (HorizontalScrollSnap_t2651831999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.HorizontalScrollSnap::Update()
extern "C"  void HorizontalScrollSnap_Update_m2609748068 (HorizontalScrollSnap_t2651831999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.Extensions.HorizontalScrollSnap::IsRectMovingSlowerThanThreshold(System.Single)
extern "C"  bool HorizontalScrollSnap_IsRectMovingSlowerThanThreshold_m1419432216 (HorizontalScrollSnap_t2651831999 * __this, float ___startingSpeed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.HorizontalScrollSnap::DistributePages()
extern "C"  void HorizontalScrollSnap_DistributePages_m2105885450 (HorizontalScrollSnap_t2651831999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.HorizontalScrollSnap::AddChild(UnityEngine.GameObject)
extern "C"  void HorizontalScrollSnap_AddChild_m1470271630 (HorizontalScrollSnap_t2651831999 * __this, GameObject_t3674682005 * ___GO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.HorizontalScrollSnap::AddChild(UnityEngine.GameObject,System.Boolean)
extern "C"  void HorizontalScrollSnap_AddChild_m1729418543 (HorizontalScrollSnap_t2651831999 * __this, GameObject_t3674682005 * ___GO0, bool ___WorldPositionStays1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.HorizontalScrollSnap::RemoveChild(System.Int32,UnityEngine.GameObject&)
extern "C"  void HorizontalScrollSnap_RemoveChild_m3440790590 (HorizontalScrollSnap_t2651831999 * __this, int32_t ___index0, GameObject_t3674682005 ** ___ChildRemoved1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.HorizontalScrollSnap::RemoveAllChildren(UnityEngine.GameObject[]&)
extern "C"  void HorizontalScrollSnap_RemoveAllChildren_m4075449043 (HorizontalScrollSnap_t2651831999 * __this, GameObjectU5BU5D_t2662109048** ___ChildrenRemoved0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.HorizontalScrollSnap::SetScrollContainerPosition()
extern "C"  void HorizontalScrollSnap_SetScrollContainerPosition_m832269526 (HorizontalScrollSnap_t2651831999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.HorizontalScrollSnap::UpdateLayout()
extern "C"  void HorizontalScrollSnap_UpdateLayout_m1572830958 (HorizontalScrollSnap_t2651831999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.HorizontalScrollSnap::OnRectTransformDimensionsChange()
extern "C"  void HorizontalScrollSnap_OnRectTransformDimensionsChange_m2941773357 (HorizontalScrollSnap_t2651831999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.HorizontalScrollSnap::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void HorizontalScrollSnap_OnEndDrag_m1543169959 (HorizontalScrollSnap_t2651831999 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
