﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<ZXing.ResultMetadataType>
struct DefaultComparer_t1371143639;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_ResultMetadataType2923366972.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<ZXing.ResultMetadataType>::.ctor()
extern "C"  void DefaultComparer__ctor_m4221270812_gshared (DefaultComparer_t1371143639 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m4221270812(__this, method) ((  void (*) (DefaultComparer_t1371143639 *, const MethodInfo*))DefaultComparer__ctor_m4221270812_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<ZXing.ResultMetadataType>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m381345879_gshared (DefaultComparer_t1371143639 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m381345879(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1371143639 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m381345879_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<ZXing.ResultMetadataType>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1566775089_gshared (DefaultComparer_t1371143639 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1566775089(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1371143639 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m1566775089_gshared)(__this, ___x0, ___y1, method)
