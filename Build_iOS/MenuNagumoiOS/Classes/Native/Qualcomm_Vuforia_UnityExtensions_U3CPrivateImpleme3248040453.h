﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"
#include "Qualcomm_Vuforia_UnityExtensions_U3CPrivateImpleme3454134163.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{7F9F85B4-C683-4C34-B525-19C32F1AC11B}
struct  U3CPrivateImplementationDetailsU3EU7B7F9F85B4U2DC683U2D4C34U2DB525U2D19C32F1AC11BU7D_t3248040453  : public Il2CppObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3EU7B7F9F85B4U2DC683U2D4C34U2DB525U2D19C32F1AC11BU7D_t3248040453_StaticFields
{
public:
	// <PrivateImplementationDetails>{7F9F85B4-C683-4C34-B525-19C32F1AC11B}/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>{7F9F85B4-C683-4C34-B525-19C32F1AC11B}::$$method0x60008ee-1
	__StaticArrayInitTypeSizeU3D24_t3454134163  ___U24U24method0x60008eeU2D1_0;

public:
	inline static int32_t get_offset_of_U24U24method0x60008eeU2D1_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B7F9F85B4U2DC683U2D4C34U2DB525U2D19C32F1AC11BU7D_t3248040453_StaticFields, ___U24U24method0x60008eeU2D1_0)); }
	inline __StaticArrayInitTypeSizeU3D24_t3454134163  get_U24U24method0x60008eeU2D1_0() const { return ___U24U24method0x60008eeU2D1_0; }
	inline __StaticArrayInitTypeSizeU3D24_t3454134163 * get_address_of_U24U24method0x60008eeU2D1_0() { return &___U24U24method0x60008eeU2D1_0; }
	inline void set_U24U24method0x60008eeU2D1_0(__StaticArrayInitTypeSizeU3D24_t3454134163  value)
	{
		___U24U24method0x60008eeU2D1_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
