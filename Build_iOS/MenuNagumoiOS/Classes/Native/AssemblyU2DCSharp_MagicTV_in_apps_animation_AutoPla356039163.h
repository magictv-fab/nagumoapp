﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AmIVisible
struct AmIVisible_t1175015477;

#include "AssemblyU2DCSharp_ARM_animation_AnimatorController975235909.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.in_apps.animation.AutoPlayController
struct  AutoPlayController_t356039163  : public AnimatorController_t975235909
{
public:
	// AmIVisible MagicTV.in_apps.animation.AutoPlayController::_amIVisible
	AmIVisible_t1175015477 * ____amIVisible_11;

public:
	inline static int32_t get_offset_of__amIVisible_11() { return static_cast<int32_t>(offsetof(AutoPlayController_t356039163, ____amIVisible_11)); }
	inline AmIVisible_t1175015477 * get__amIVisible_11() const { return ____amIVisible_11; }
	inline AmIVisible_t1175015477 ** get_address_of__amIVisible_11() { return &____amIVisible_11; }
	inline void set__amIVisible_11(AmIVisible_t1175015477 * value)
	{
		____amIVisible_11 = value;
		Il2CppCodeGenWriteBarrier(&____amIVisible_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
