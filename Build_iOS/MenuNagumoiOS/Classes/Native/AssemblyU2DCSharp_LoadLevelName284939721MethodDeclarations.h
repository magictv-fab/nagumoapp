﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoadLevelName
struct LoadLevelName_t284939721;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void LoadLevelName::.ctor()
extern "C"  void LoadLevelName__ctor_m2833776066 (LoadLevelName_t284939721 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadLevelName::Start()
extern "C"  void LoadLevelName_Start_m1780913858 (LoadLevelName_t284939721 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadLevelName::Update()
extern "C"  void LoadLevelName_Update_m3674574219 (LoadLevelName_t284939721 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadLevelName::BackButton()
extern "C"  void LoadLevelName_BackButton_m2958634811 (LoadLevelName_t284939721 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadLevelName::LoadLevel(System.String)
extern "C"  void LoadLevelName_LoadLevel_m3392386468 (LoadLevelName_t284939721 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadLevelName::DoceNovembroLevel(System.Int32)
extern "C"  void LoadLevelName_DoceNovembroLevel_m3298263462 (LoadLevelName_t284939721 * __this, int32_t ___indexPremio0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadLevelName::LoadLevelAcougue()
extern "C"  void LoadLevelName_LoadLevelAcougue_m660082707 (LoadLevelName_t284939721 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadLevelName::LoadLevelCenaMain(System.Boolean)
extern "C"  void LoadLevelName_LoadLevelCenaMain_m825917219 (LoadLevelName_t284939721 * __this, bool ___isAcougue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LoadLevelName::GoLevel()
extern "C"  Il2CppObject * LoadLevelName_GoLevel_m4042264036 (LoadLevelName_t284939721 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadLevelName::LoadLevelAdditive(System.String)
extern "C"  void LoadLevelName_LoadLevelAdditive_m4001860920 (LoadLevelName_t284939721 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LoadLevelName::GoLevelAdditive()
extern "C"  Il2CppObject * LoadLevelName_GoLevelAdditive_m2697744336 (LoadLevelName_t284939721 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadLevelName::Exit()
extern "C"  void LoadLevelName_Exit_m2015877344 (LoadLevelName_t284939721 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
