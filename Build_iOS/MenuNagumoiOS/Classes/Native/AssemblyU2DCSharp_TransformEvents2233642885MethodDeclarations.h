﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TransformEvents
struct TransformEvents_t2233642885;
// TransformEvents/OnBundleIDEventHandler
struct OnBundleIDEventHandler_t3610444662;
// TransformEvents/OnInAppEventHandler
struct OnInAppEventHandler_t2177803875;
// MagicTV.abstracts.InAppAbstract
struct InAppAbstract_t382673128;
// TransformEvents/OnTransformEventHandler
struct OnTransformEventHandler_t2999119763;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TransformEvents_OnBundleIDEventH3610444662.h"
#include "AssemblyU2DCSharp_TransformEvents_OnInAppEventHand2177803875.h"
#include "AssemblyU2DCSharp_MagicTV_abstracts_InAppAbstract382673128.h"
#include "AssemblyU2DCSharp_TransformEvents_OnTransformEvent2999119763.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void TransformEvents::.ctor()
extern "C"  void TransformEvents__ctor_m297909638 (TransformEvents_t2233642885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransformEvents::AddOnBundleIDEventHandler(TransformEvents/OnBundleIDEventHandler)
extern "C"  void TransformEvents_AddOnBundleIDEventHandler_m4188604603 (Il2CppObject * __this /* static, unused */, OnBundleIDEventHandler_t3610444662 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransformEvents::RemoveOnBundleIDEventHandler(TransformEvents/OnBundleIDEventHandler)
extern "C"  void TransformEvents_RemoveOnBundleIDEventHandler_m834234040 (Il2CppObject * __this /* static, unused */, OnBundleIDEventHandler_t3610444662 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransformEvents::RaiseBundleID(System.Int32)
extern "C"  void TransformEvents_RaiseBundleID_m2154525790 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransformEvents::AddOnInAppEventHandler(TransformEvents/OnInAppEventHandler)
extern "C"  void TransformEvents_AddOnInAppEventHandler_m1036961349 (Il2CppObject * __this /* static, unused */, OnInAppEventHandler_t2177803875 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransformEvents::RemoveOnInAppEventHandler(TransformEvents/OnInAppEventHandler)
extern "C"  void TransformEvents_RemoveOnInAppEventHandler_m3909658114 (Il2CppObject * __this /* static, unused */, OnInAppEventHandler_t2177803875 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransformEvents::RaiseInApp(MagicTV.abstracts.InAppAbstract)
extern "C"  void TransformEvents_RaiseInApp_m2832097378 (Il2CppObject * __this /* static, unused */, InAppAbstract_t382673128 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransformEvents::AddOnPositionEventHandler(TransformEvents/OnTransformEventHandler)
extern "C"  void TransformEvents_AddOnPositionEventHandler_m3382347298 (Il2CppObject * __this /* static, unused */, OnTransformEventHandler_t2999119763 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransformEvents::RemoveOnPositionEventHandler(TransformEvents/OnTransformEventHandler)
extern "C"  void TransformEvents_RemoveOnPositionEventHandler_m2476074949 (Il2CppObject * __this /* static, unused */, OnTransformEventHandler_t2999119763 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransformEvents::RaiseOnPosition(UnityEngine.Vector3)
extern "C"  void TransformEvents_RaiseOnPosition_m260368833 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransformEvents::AddOnRotationEventHandler(TransformEvents/OnTransformEventHandler)
extern "C"  void TransformEvents_AddOnRotationEventHandler_m3889355501 (Il2CppObject * __this /* static, unused */, OnTransformEventHandler_t2999119763 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransformEvents::RemoveOnRotationEventHandler(TransformEvents/OnTransformEventHandler)
extern "C"  void TransformEvents_RemoveOnRotationEventHandler_m2983083152 (Il2CppObject * __this /* static, unused */, OnTransformEventHandler_t2999119763 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransformEvents::RaiseOnRotation(UnityEngine.Vector3)
extern "C"  void TransformEvents_RaiseOnRotation_m2360027788 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransformEvents::AddOnScaleEventHandler(TransformEvents/OnTransformEventHandler)
extern "C"  void TransformEvents_AddOnScaleEventHandler_m502896071 (Il2CppObject * __this /* static, unused */, OnTransformEventHandler_t2999119763 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransformEvents::RemoveOnScaleEventHandler(TransformEvents/OnTransformEventHandler)
extern "C"  void TransformEvents_RemoveOnScaleEventHandler_m3583201028 (Il2CppObject * __this /* static, unused */, OnTransformEventHandler_t2999119763 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransformEvents::RaiseOnScale(UnityEngine.Vector3)
extern "C"  void TransformEvents_RaiseOnScale_m2334938460 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
