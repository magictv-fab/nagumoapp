﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.Perspective,System.Object>
struct KeyCollection_t4137643543;
// System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>
struct Dictionary_2_t2510884092;
// System.Collections.Generic.IEnumerator`1<MagicTV.globals.Perspective>
struct IEnumerator_1_t2556418851;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// MagicTV.globals.Perspective[]
struct PerspectiveU5BU5D_t1573652303;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_globals_Perspective644553802.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3125820146.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.Perspective,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m2058062697_gshared (KeyCollection_t4137643543 * __this, Dictionary_2_t2510884092 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m2058062697(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t4137643543 *, Dictionary_2_t2510884092 *, const MethodInfo*))KeyCollection__ctor_m2058062697_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.Perspective,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2538115149_gshared (KeyCollection_t4137643543 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2538115149(__this, ___item0, method) ((  void (*) (KeyCollection_t4137643543 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2538115149_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.Perspective,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1562404100_gshared (KeyCollection_t4137643543 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1562404100(__this, method) ((  void (*) (KeyCollection_t4137643543 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1562404100_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.Perspective,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m597680733_gshared (KeyCollection_t4137643543 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m597680733(__this, ___item0, method) ((  bool (*) (KeyCollection_t4137643543 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m597680733_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.Perspective,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2857060098_gshared (KeyCollection_t4137643543 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2857060098(__this, ___item0, method) ((  bool (*) (KeyCollection_t4137643543 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2857060098_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.Perspective,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3234332160_gshared (KeyCollection_t4137643543 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3234332160(__this, method) ((  Il2CppObject* (*) (KeyCollection_t4137643543 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3234332160_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.Perspective,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m3642680182_gshared (KeyCollection_t4137643543 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m3642680182(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t4137643543 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3642680182_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.Perspective,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m188384753_gshared (KeyCollection_t4137643543 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m188384753(__this, method) ((  Il2CppObject * (*) (KeyCollection_t4137643543 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m188384753_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.Perspective,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1726625406_gshared (KeyCollection_t4137643543 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1726625406(__this, method) ((  bool (*) (KeyCollection_t4137643543 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1726625406_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.Perspective,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2726864048_gshared (KeyCollection_t4137643543 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2726864048(__this, method) ((  bool (*) (KeyCollection_t4137643543 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2726864048_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.Perspective,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m2244505756_gshared (KeyCollection_t4137643543 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m2244505756(__this, method) ((  Il2CppObject * (*) (KeyCollection_t4137643543 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m2244505756_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.Perspective,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m613996062_gshared (KeyCollection_t4137643543 * __this, PerspectiveU5BU5D_t1573652303* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m613996062(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t4137643543 *, PerspectiveU5BU5D_t1573652303*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m613996062_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.Perspective,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3125820146  KeyCollection_GetEnumerator_m3434083777_gshared (KeyCollection_t4137643543 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m3434083777(__this, method) ((  Enumerator_t3125820146  (*) (KeyCollection_t4137643543 *, const MethodInfo*))KeyCollection_GetEnumerator_m3434083777_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<MagicTV.globals.Perspective,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m2713534454_gshared (KeyCollection_t4137643543 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m2713534454(__this, method) ((  int32_t (*) (KeyCollection_t4137643543 *, const MethodInfo*))KeyCollection_get_Count_m2713534454_gshared)(__this, method)
