﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScreenshotManagerGUI/<Save>c__Iterator2C
struct  U3CSaveU3Ec__Iterator2C_t3387133346  : public Il2CppObject
{
public:
	// System.Boolean ScreenshotManagerGUI/<Save>c__Iterator2C::<photoSaved>__0
	bool ___U3CphotoSavedU3E__0_0;
	// System.String ScreenshotManagerGUI/<Save>c__Iterator2C::<date>__1
	String_t* ___U3CdateU3E__1_1;
	// System.String ScreenshotManagerGUI/<Save>c__Iterator2C::fileName
	String_t* ___fileName_2;
	// System.String ScreenshotManagerGUI/<Save>c__Iterator2C::<screenshotFilename>__2
	String_t* ___U3CscreenshotFilenameU3E__2_3;
	// System.String ScreenshotManagerGUI/<Save>c__Iterator2C::<path>__3
	String_t* ___U3CpathU3E__3_4;
	// System.Boolean ScreenshotManagerGUI/<Save>c__Iterator2C::callback
	bool ___callback_5;
	// System.Int32 ScreenshotManagerGUI/<Save>c__Iterator2C::$PC
	int32_t ___U24PC_6;
	// System.Object ScreenshotManagerGUI/<Save>c__Iterator2C::$current
	Il2CppObject * ___U24current_7;
	// System.String ScreenshotManagerGUI/<Save>c__Iterator2C::<$>fileName
	String_t* ___U3CU24U3EfileName_8;
	// System.Boolean ScreenshotManagerGUI/<Save>c__Iterator2C::<$>callback
	bool ___U3CU24U3Ecallback_9;

public:
	inline static int32_t get_offset_of_U3CphotoSavedU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSaveU3Ec__Iterator2C_t3387133346, ___U3CphotoSavedU3E__0_0)); }
	inline bool get_U3CphotoSavedU3E__0_0() const { return ___U3CphotoSavedU3E__0_0; }
	inline bool* get_address_of_U3CphotoSavedU3E__0_0() { return &___U3CphotoSavedU3E__0_0; }
	inline void set_U3CphotoSavedU3E__0_0(bool value)
	{
		___U3CphotoSavedU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CdateU3E__1_1() { return static_cast<int32_t>(offsetof(U3CSaveU3Ec__Iterator2C_t3387133346, ___U3CdateU3E__1_1)); }
	inline String_t* get_U3CdateU3E__1_1() const { return ___U3CdateU3E__1_1; }
	inline String_t** get_address_of_U3CdateU3E__1_1() { return &___U3CdateU3E__1_1; }
	inline void set_U3CdateU3E__1_1(String_t* value)
	{
		___U3CdateU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdateU3E__1_1, value);
	}

	inline static int32_t get_offset_of_fileName_2() { return static_cast<int32_t>(offsetof(U3CSaveU3Ec__Iterator2C_t3387133346, ___fileName_2)); }
	inline String_t* get_fileName_2() const { return ___fileName_2; }
	inline String_t** get_address_of_fileName_2() { return &___fileName_2; }
	inline void set_fileName_2(String_t* value)
	{
		___fileName_2 = value;
		Il2CppCodeGenWriteBarrier(&___fileName_2, value);
	}

	inline static int32_t get_offset_of_U3CscreenshotFilenameU3E__2_3() { return static_cast<int32_t>(offsetof(U3CSaveU3Ec__Iterator2C_t3387133346, ___U3CscreenshotFilenameU3E__2_3)); }
	inline String_t* get_U3CscreenshotFilenameU3E__2_3() const { return ___U3CscreenshotFilenameU3E__2_3; }
	inline String_t** get_address_of_U3CscreenshotFilenameU3E__2_3() { return &___U3CscreenshotFilenameU3E__2_3; }
	inline void set_U3CscreenshotFilenameU3E__2_3(String_t* value)
	{
		___U3CscreenshotFilenameU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CscreenshotFilenameU3E__2_3, value);
	}

	inline static int32_t get_offset_of_U3CpathU3E__3_4() { return static_cast<int32_t>(offsetof(U3CSaveU3Ec__Iterator2C_t3387133346, ___U3CpathU3E__3_4)); }
	inline String_t* get_U3CpathU3E__3_4() const { return ___U3CpathU3E__3_4; }
	inline String_t** get_address_of_U3CpathU3E__3_4() { return &___U3CpathU3E__3_4; }
	inline void set_U3CpathU3E__3_4(String_t* value)
	{
		___U3CpathU3E__3_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpathU3E__3_4, value);
	}

	inline static int32_t get_offset_of_callback_5() { return static_cast<int32_t>(offsetof(U3CSaveU3Ec__Iterator2C_t3387133346, ___callback_5)); }
	inline bool get_callback_5() const { return ___callback_5; }
	inline bool* get_address_of_callback_5() { return &___callback_5; }
	inline void set_callback_5(bool value)
	{
		___callback_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CSaveU3Ec__Iterator2C_t3387133346, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CSaveU3Ec__Iterator2C_t3387133346, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EfileName_8() { return static_cast<int32_t>(offsetof(U3CSaveU3Ec__Iterator2C_t3387133346, ___U3CU24U3EfileName_8)); }
	inline String_t* get_U3CU24U3EfileName_8() const { return ___U3CU24U3EfileName_8; }
	inline String_t** get_address_of_U3CU24U3EfileName_8() { return &___U3CU24U3EfileName_8; }
	inline void set_U3CU24U3EfileName_8(String_t* value)
	{
		___U3CU24U3EfileName_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EfileName_8, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ecallback_9() { return static_cast<int32_t>(offsetof(U3CSaveU3Ec__Iterator2C_t3387133346, ___U3CU24U3Ecallback_9)); }
	inline bool get_U3CU24U3Ecallback_9() const { return ___U3CU24U3Ecallback_9; }
	inline bool* get_address_of_U3CU24U3Ecallback_9() { return &___U3CU24U3Ecallback_9; }
	inline void set_U3CU24U3Ecallback_9(bool value)
	{
		___U3CU24U3Ecallback_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
