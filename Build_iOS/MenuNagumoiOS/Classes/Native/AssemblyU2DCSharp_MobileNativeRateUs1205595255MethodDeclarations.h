﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MobileNativeRateUs
struct MobileNativeRateUs_t1205595255;
// System.String
struct String_t;
// UnionAssets.FLE.CEvent
struct CEvent_t44106931;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_UnionAssets_FLE_CEvent44106931.h"
#include "AssemblyU2DCSharp_MNDialogResult3125317574.h"

// System.Void MobileNativeRateUs::.ctor(System.String,System.String)
extern "C"  void MobileNativeRateUs__ctor_m1121062714 (MobileNativeRateUs_t1205595255 * __this, String_t* ___title0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MobileNativeRateUs::.ctor(System.String,System.String,System.String,System.String,System.String)
extern "C"  void MobileNativeRateUs__ctor_m1329747054 (MobileNativeRateUs_t1205595255 * __this, String_t* ___title0, String_t* ___message1, String_t* ___yes2, String_t* ___later3, String_t* ___no4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MobileNativeRateUs::SetAndroidAppUrl(System.String)
extern "C"  void MobileNativeRateUs_SetAndroidAppUrl_m2291738567 (MobileNativeRateUs_t1205595255 * __this, String_t* ____url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MobileNativeRateUs::SetAppleId(System.String)
extern "C"  void MobileNativeRateUs_SetAppleId_m2870185935 (MobileNativeRateUs_t1205595255 * __this, String_t* ____appleId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MobileNativeRateUs::Start()
extern "C"  void MobileNativeRateUs_Start_m978699012 (MobileNativeRateUs_t1205595255 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MobileNativeRateUs::OnCompleteListener(UnionAssets.FLE.CEvent)
extern "C"  void MobileNativeRateUs_OnCompleteListener_m2116968842 (MobileNativeRateUs_t1205595255 * __this, CEvent_t44106931 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MobileNativeRateUs::<OnComplete>m__B(MNDialogResult)
extern "C"  void MobileNativeRateUs_U3COnCompleteU3Em__B_m286561241 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
