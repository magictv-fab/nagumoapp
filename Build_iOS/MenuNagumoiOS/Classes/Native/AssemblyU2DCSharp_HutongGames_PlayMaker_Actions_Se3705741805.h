﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co2322045418.h"
#include "UnityEngine_UnityEngine_Space4209342076.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetVelocity
struct  SetVelocity_t3705741805  : public ComponentAction_1_t2322045418
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetVelocity::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SetVelocity::vector
	FsmVector3_t533912882 * ___vector_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetVelocity::x
	FsmFloat_t2134102846 * ___x_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetVelocity::y
	FsmFloat_t2134102846 * ___y_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetVelocity::z
	FsmFloat_t2134102846 * ___z_15;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.SetVelocity::space
	int32_t ___space_16;
	// System.Boolean HutongGames.PlayMaker.Actions.SetVelocity::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(SetVelocity_t3705741805, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_vector_12() { return static_cast<int32_t>(offsetof(SetVelocity_t3705741805, ___vector_12)); }
	inline FsmVector3_t533912882 * get_vector_12() const { return ___vector_12; }
	inline FsmVector3_t533912882 ** get_address_of_vector_12() { return &___vector_12; }
	inline void set_vector_12(FsmVector3_t533912882 * value)
	{
		___vector_12 = value;
		Il2CppCodeGenWriteBarrier(&___vector_12, value);
	}

	inline static int32_t get_offset_of_x_13() { return static_cast<int32_t>(offsetof(SetVelocity_t3705741805, ___x_13)); }
	inline FsmFloat_t2134102846 * get_x_13() const { return ___x_13; }
	inline FsmFloat_t2134102846 ** get_address_of_x_13() { return &___x_13; }
	inline void set_x_13(FsmFloat_t2134102846 * value)
	{
		___x_13 = value;
		Il2CppCodeGenWriteBarrier(&___x_13, value);
	}

	inline static int32_t get_offset_of_y_14() { return static_cast<int32_t>(offsetof(SetVelocity_t3705741805, ___y_14)); }
	inline FsmFloat_t2134102846 * get_y_14() const { return ___y_14; }
	inline FsmFloat_t2134102846 ** get_address_of_y_14() { return &___y_14; }
	inline void set_y_14(FsmFloat_t2134102846 * value)
	{
		___y_14 = value;
		Il2CppCodeGenWriteBarrier(&___y_14, value);
	}

	inline static int32_t get_offset_of_z_15() { return static_cast<int32_t>(offsetof(SetVelocity_t3705741805, ___z_15)); }
	inline FsmFloat_t2134102846 * get_z_15() const { return ___z_15; }
	inline FsmFloat_t2134102846 ** get_address_of_z_15() { return &___z_15; }
	inline void set_z_15(FsmFloat_t2134102846 * value)
	{
		___z_15 = value;
		Il2CppCodeGenWriteBarrier(&___z_15, value);
	}

	inline static int32_t get_offset_of_space_16() { return static_cast<int32_t>(offsetof(SetVelocity_t3705741805, ___space_16)); }
	inline int32_t get_space_16() const { return ___space_16; }
	inline int32_t* get_address_of_space_16() { return &___space_16; }
	inline void set_space_16(int32_t value)
	{
		___space_16 = value;
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(SetVelocity_t3705741805, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
