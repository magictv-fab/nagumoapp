﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Extensions.ScrollRectEx/<OnInitializePotentialDrag>c__AnonStorey8B
struct U3COnInitializePotentialDragU3Ec__AnonStorey8B_t2330403566;
// UnityEngine.EventSystems.IInitializePotentialDragHandler
struct IInitializePotentialDragHandler_t220416063;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.Extensions.ScrollRectEx/<OnInitializePotentialDrag>c__AnonStorey8B::.ctor()
extern "C"  void U3COnInitializePotentialDragU3Ec__AnonStorey8B__ctor_m3632182845 (U3COnInitializePotentialDragU3Ec__AnonStorey8B_t2330403566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.ScrollRectEx/<OnInitializePotentialDrag>c__AnonStorey8B::<>m__0(UnityEngine.EventSystems.IInitializePotentialDragHandler)
extern "C"  void U3COnInitializePotentialDragU3Ec__AnonStorey8B_U3CU3Em__0_m2921405320 (U3COnInitializePotentialDragU3Ec__AnonStorey8B_t2330403566 * __this, Il2CppObject * ___parent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
