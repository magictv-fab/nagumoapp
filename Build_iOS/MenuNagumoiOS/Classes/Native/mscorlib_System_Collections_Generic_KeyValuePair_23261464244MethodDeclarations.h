﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,ZXing.Common.CharacterSetECI>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1412130714(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3261464244 *, String_t*, CharacterSetECI_t2542265168 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,ZXing.Common.CharacterSetECI>::get_Key()
#define KeyValuePair_2_get_Key_m82304782(__this, method) ((  String_t* (*) (KeyValuePair_2_t3261464244 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,ZXing.Common.CharacterSetECI>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m359930447(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3261464244 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,ZXing.Common.CharacterSetECI>::get_Value()
#define KeyValuePair_2_get_Value_m3212829134(__this, method) ((  CharacterSetECI_t2542265168 * (*) (KeyValuePair_2_t3261464244 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,ZXing.Common.CharacterSetECI>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2727063247(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3261464244 *, CharacterSetECI_t2542265168 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,ZXing.Common.CharacterSetECI>::ToString()
#define KeyValuePair_2_ToString_m3378560627(__this, method) ((  String_t* (*) (KeyValuePair_2_t3261464244 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
