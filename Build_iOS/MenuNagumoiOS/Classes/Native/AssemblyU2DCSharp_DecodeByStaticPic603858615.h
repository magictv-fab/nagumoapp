﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// UnityEngine.UI.Text
struct Text_t9039225;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DecodeByStaticPic
struct  DecodeByStaticPic_t603858615  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Texture2D DecodeByStaticPic::targetTex
	Texture2D_t3884108195 * ___targetTex_2;
	// UnityEngine.UI.Text DecodeByStaticPic::resultText
	Text_t9039225 * ___resultText_3;

public:
	inline static int32_t get_offset_of_targetTex_2() { return static_cast<int32_t>(offsetof(DecodeByStaticPic_t603858615, ___targetTex_2)); }
	inline Texture2D_t3884108195 * get_targetTex_2() const { return ___targetTex_2; }
	inline Texture2D_t3884108195 ** get_address_of_targetTex_2() { return &___targetTex_2; }
	inline void set_targetTex_2(Texture2D_t3884108195 * value)
	{
		___targetTex_2 = value;
		Il2CppCodeGenWriteBarrier(&___targetTex_2, value);
	}

	inline static int32_t get_offset_of_resultText_3() { return static_cast<int32_t>(offsetof(DecodeByStaticPic_t603858615, ___resultText_3)); }
	inline Text_t9039225 * get_resultText_3() const { return ___resultText_3; }
	inline Text_t9039225 ** get_address_of_resultText_3() { return &___resultText_3; }
	inline void set_resultText_3(Text_t9039225 * value)
	{
		___resultText_3 = value;
		Il2CppCodeGenWriteBarrier(&___resultText_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
