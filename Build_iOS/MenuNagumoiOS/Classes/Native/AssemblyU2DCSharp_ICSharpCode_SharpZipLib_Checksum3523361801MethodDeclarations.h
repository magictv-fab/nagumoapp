﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Checksums.Crc32
struct Crc32_t3523361801;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"

// System.Void ICSharpCode.SharpZipLib.Checksums.Crc32::.ctor()
extern "C"  void Crc32__ctor_m2324925223 (Crc32_t3523361801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Checksums.Crc32::.cctor()
extern "C"  void Crc32__cctor_m2871108966 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 ICSharpCode.SharpZipLib.Checksums.Crc32::ComputeCrc32(System.UInt32,System.Byte)
extern "C"  uint32_t Crc32_ComputeCrc32_m2469023201 (Il2CppObject * __this /* static, unused */, uint32_t ___oldCrc0, uint8_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Checksums.Crc32::get_Value()
extern "C"  int64_t Crc32_get_Value_m2296638364 (Crc32_t3523361801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Checksums.Crc32::set_Value(System.Int64)
extern "C"  void Crc32_set_Value_m2651134955 (Crc32_t3523361801 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Checksums.Crc32::Reset()
extern "C"  void Crc32_Reset_m4266325460 (Crc32_t3523361801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Checksums.Crc32::Update(System.Int32)
extern "C"  void Crc32_Update_m4002310423 (Crc32_t3523361801 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Checksums.Crc32::Update(System.Byte[])
extern "C"  void Crc32_Update_m2211431619 (Crc32_t3523361801 * __this, ByteU5BU5D_t4260760469* ___buffer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Checksums.Crc32::Update(System.Byte[],System.Int32,System.Int32)
extern "C"  void Crc32_Update_m3973340259 (Crc32_t3523361801 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
