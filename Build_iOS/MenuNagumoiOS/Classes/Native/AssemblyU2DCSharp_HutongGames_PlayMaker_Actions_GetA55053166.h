﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// UnityEngine.Animator
struct Animator_t2776330603;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorIsHuman
struct  GetAnimatorIsHuman_t55053166  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorIsHuman::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetAnimatorIsHuman::isHuman
	FsmBool_t1075959796 * ___isHuman_10;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorIsHuman::isHumanEvent
	FsmEvent_t2133468028 * ___isHumanEvent_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorIsHuman::isGenericEvent
	FsmEvent_t2133468028 * ___isGenericEvent_12;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorIsHuman::_animator
	Animator_t2776330603 * ____animator_13;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(GetAnimatorIsHuman_t55053166, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_isHuman_10() { return static_cast<int32_t>(offsetof(GetAnimatorIsHuman_t55053166, ___isHuman_10)); }
	inline FsmBool_t1075959796 * get_isHuman_10() const { return ___isHuman_10; }
	inline FsmBool_t1075959796 ** get_address_of_isHuman_10() { return &___isHuman_10; }
	inline void set_isHuman_10(FsmBool_t1075959796 * value)
	{
		___isHuman_10 = value;
		Il2CppCodeGenWriteBarrier(&___isHuman_10, value);
	}

	inline static int32_t get_offset_of_isHumanEvent_11() { return static_cast<int32_t>(offsetof(GetAnimatorIsHuman_t55053166, ___isHumanEvent_11)); }
	inline FsmEvent_t2133468028 * get_isHumanEvent_11() const { return ___isHumanEvent_11; }
	inline FsmEvent_t2133468028 ** get_address_of_isHumanEvent_11() { return &___isHumanEvent_11; }
	inline void set_isHumanEvent_11(FsmEvent_t2133468028 * value)
	{
		___isHumanEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___isHumanEvent_11, value);
	}

	inline static int32_t get_offset_of_isGenericEvent_12() { return static_cast<int32_t>(offsetof(GetAnimatorIsHuman_t55053166, ___isGenericEvent_12)); }
	inline FsmEvent_t2133468028 * get_isGenericEvent_12() const { return ___isGenericEvent_12; }
	inline FsmEvent_t2133468028 ** get_address_of_isGenericEvent_12() { return &___isGenericEvent_12; }
	inline void set_isGenericEvent_12(FsmEvent_t2133468028 * value)
	{
		___isGenericEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___isGenericEvent_12, value);
	}

	inline static int32_t get_offset_of__animator_13() { return static_cast<int32_t>(offsetof(GetAnimatorIsHuman_t55053166, ____animator_13)); }
	inline Animator_t2776330603 * get__animator_13() const { return ____animator_13; }
	inline Animator_t2776330603 ** get_address_of__animator_13() { return &____animator_13; }
	inline void set__animator_13(Animator_t2776330603 * value)
	{
		____animator_13 = value;
		Il2CppCodeGenWriteBarrier(&____animator_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
