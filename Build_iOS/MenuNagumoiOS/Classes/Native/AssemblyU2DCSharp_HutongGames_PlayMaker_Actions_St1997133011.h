﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.StartServer
struct  StartServer_t1997133011  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.StartServer::connections
	FsmInt_t1596138449 * ___connections_9;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.StartServer::listenPort
	FsmInt_t1596138449 * ___listenPort_10;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.StartServer::incomingPassword
	FsmString_t952858651 * ___incomingPassword_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.StartServer::useNAT
	FsmBool_t1075959796 * ___useNAT_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.StartServer::useSecurityLayer
	FsmBool_t1075959796 * ___useSecurityLayer_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.StartServer::runInBackground
	FsmBool_t1075959796 * ___runInBackground_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.StartServer::errorEvent
	FsmEvent_t2133468028 * ___errorEvent_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.StartServer::errorString
	FsmString_t952858651 * ___errorString_16;

public:
	inline static int32_t get_offset_of_connections_9() { return static_cast<int32_t>(offsetof(StartServer_t1997133011, ___connections_9)); }
	inline FsmInt_t1596138449 * get_connections_9() const { return ___connections_9; }
	inline FsmInt_t1596138449 ** get_address_of_connections_9() { return &___connections_9; }
	inline void set_connections_9(FsmInt_t1596138449 * value)
	{
		___connections_9 = value;
		Il2CppCodeGenWriteBarrier(&___connections_9, value);
	}

	inline static int32_t get_offset_of_listenPort_10() { return static_cast<int32_t>(offsetof(StartServer_t1997133011, ___listenPort_10)); }
	inline FsmInt_t1596138449 * get_listenPort_10() const { return ___listenPort_10; }
	inline FsmInt_t1596138449 ** get_address_of_listenPort_10() { return &___listenPort_10; }
	inline void set_listenPort_10(FsmInt_t1596138449 * value)
	{
		___listenPort_10 = value;
		Il2CppCodeGenWriteBarrier(&___listenPort_10, value);
	}

	inline static int32_t get_offset_of_incomingPassword_11() { return static_cast<int32_t>(offsetof(StartServer_t1997133011, ___incomingPassword_11)); }
	inline FsmString_t952858651 * get_incomingPassword_11() const { return ___incomingPassword_11; }
	inline FsmString_t952858651 ** get_address_of_incomingPassword_11() { return &___incomingPassword_11; }
	inline void set_incomingPassword_11(FsmString_t952858651 * value)
	{
		___incomingPassword_11 = value;
		Il2CppCodeGenWriteBarrier(&___incomingPassword_11, value);
	}

	inline static int32_t get_offset_of_useNAT_12() { return static_cast<int32_t>(offsetof(StartServer_t1997133011, ___useNAT_12)); }
	inline FsmBool_t1075959796 * get_useNAT_12() const { return ___useNAT_12; }
	inline FsmBool_t1075959796 ** get_address_of_useNAT_12() { return &___useNAT_12; }
	inline void set_useNAT_12(FsmBool_t1075959796 * value)
	{
		___useNAT_12 = value;
		Il2CppCodeGenWriteBarrier(&___useNAT_12, value);
	}

	inline static int32_t get_offset_of_useSecurityLayer_13() { return static_cast<int32_t>(offsetof(StartServer_t1997133011, ___useSecurityLayer_13)); }
	inline FsmBool_t1075959796 * get_useSecurityLayer_13() const { return ___useSecurityLayer_13; }
	inline FsmBool_t1075959796 ** get_address_of_useSecurityLayer_13() { return &___useSecurityLayer_13; }
	inline void set_useSecurityLayer_13(FsmBool_t1075959796 * value)
	{
		___useSecurityLayer_13 = value;
		Il2CppCodeGenWriteBarrier(&___useSecurityLayer_13, value);
	}

	inline static int32_t get_offset_of_runInBackground_14() { return static_cast<int32_t>(offsetof(StartServer_t1997133011, ___runInBackground_14)); }
	inline FsmBool_t1075959796 * get_runInBackground_14() const { return ___runInBackground_14; }
	inline FsmBool_t1075959796 ** get_address_of_runInBackground_14() { return &___runInBackground_14; }
	inline void set_runInBackground_14(FsmBool_t1075959796 * value)
	{
		___runInBackground_14 = value;
		Il2CppCodeGenWriteBarrier(&___runInBackground_14, value);
	}

	inline static int32_t get_offset_of_errorEvent_15() { return static_cast<int32_t>(offsetof(StartServer_t1997133011, ___errorEvent_15)); }
	inline FsmEvent_t2133468028 * get_errorEvent_15() const { return ___errorEvent_15; }
	inline FsmEvent_t2133468028 ** get_address_of_errorEvent_15() { return &___errorEvent_15; }
	inline void set_errorEvent_15(FsmEvent_t2133468028 * value)
	{
		___errorEvent_15 = value;
		Il2CppCodeGenWriteBarrier(&___errorEvent_15, value);
	}

	inline static int32_t get_offset_of_errorString_16() { return static_cast<int32_t>(offsetof(StartServer_t1997133011, ___errorString_16)); }
	inline FsmString_t952858651 * get_errorString_16() const { return ___errorString_16; }
	inline FsmString_t952858651 ** get_address_of_errorString_16() { return &___errorString_16; }
	inline void set_errorString_16(FsmString_t952858651 * value)
	{
		___errorString_16 = value;
		Il2CppCodeGenWriteBarrier(&___errorString_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
