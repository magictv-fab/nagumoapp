﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.PDF417.Internal.BarcodeMetadata
struct BarcodeMetadata_t443526333;
// ZXing.PDF417.Internal.DetectionResultColumn[]
struct DetectionResultColumnU5BU5D_t3380244739;
// ZXing.PDF417.Internal.BoundingBox
struct BoundingBox_t242606069;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.DetectionResult
struct  DetectionResult_t1261579248  : public Il2CppObject
{
public:
	// ZXing.PDF417.Internal.BarcodeMetadata ZXing.PDF417.Internal.DetectionResult::<Metadata>k__BackingField
	BarcodeMetadata_t443526333 * ___U3CMetadataU3Ek__BackingField_0;
	// ZXing.PDF417.Internal.DetectionResultColumn[] ZXing.PDF417.Internal.DetectionResult::<DetectionResultColumns>k__BackingField
	DetectionResultColumnU5BU5D_t3380244739* ___U3CDetectionResultColumnsU3Ek__BackingField_1;
	// ZXing.PDF417.Internal.BoundingBox ZXing.PDF417.Internal.DetectionResult::<Box>k__BackingField
	BoundingBox_t242606069 * ___U3CBoxU3Ek__BackingField_2;
	// System.Int32 ZXing.PDF417.Internal.DetectionResult::<ColumnCount>k__BackingField
	int32_t ___U3CColumnCountU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CMetadataU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DetectionResult_t1261579248, ___U3CMetadataU3Ek__BackingField_0)); }
	inline BarcodeMetadata_t443526333 * get_U3CMetadataU3Ek__BackingField_0() const { return ___U3CMetadataU3Ek__BackingField_0; }
	inline BarcodeMetadata_t443526333 ** get_address_of_U3CMetadataU3Ek__BackingField_0() { return &___U3CMetadataU3Ek__BackingField_0; }
	inline void set_U3CMetadataU3Ek__BackingField_0(BarcodeMetadata_t443526333 * value)
	{
		___U3CMetadataU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CMetadataU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CDetectionResultColumnsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DetectionResult_t1261579248, ___U3CDetectionResultColumnsU3Ek__BackingField_1)); }
	inline DetectionResultColumnU5BU5D_t3380244739* get_U3CDetectionResultColumnsU3Ek__BackingField_1() const { return ___U3CDetectionResultColumnsU3Ek__BackingField_1; }
	inline DetectionResultColumnU5BU5D_t3380244739** get_address_of_U3CDetectionResultColumnsU3Ek__BackingField_1() { return &___U3CDetectionResultColumnsU3Ek__BackingField_1; }
	inline void set_U3CDetectionResultColumnsU3Ek__BackingField_1(DetectionResultColumnU5BU5D_t3380244739* value)
	{
		___U3CDetectionResultColumnsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDetectionResultColumnsU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CBoxU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DetectionResult_t1261579248, ___U3CBoxU3Ek__BackingField_2)); }
	inline BoundingBox_t242606069 * get_U3CBoxU3Ek__BackingField_2() const { return ___U3CBoxU3Ek__BackingField_2; }
	inline BoundingBox_t242606069 ** get_address_of_U3CBoxU3Ek__BackingField_2() { return &___U3CBoxU3Ek__BackingField_2; }
	inline void set_U3CBoxU3Ek__BackingField_2(BoundingBox_t242606069 * value)
	{
		___U3CBoxU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CBoxU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CColumnCountU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(DetectionResult_t1261579248, ___U3CColumnCountU3Ek__BackingField_3)); }
	inline int32_t get_U3CColumnCountU3Ek__BackingField_3() const { return ___U3CColumnCountU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CColumnCountU3Ek__BackingField_3() { return &___U3CColumnCountU3Ek__BackingField_3; }
	inline void set_U3CColumnCountU3Ek__BackingField_3(int32_t value)
	{
		___U3CColumnCountU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
