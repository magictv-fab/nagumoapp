﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GUIStyleState1997423985.h"
#include "UnityEngine_UnityEngine_FontStyle3350479768.h"
#include "UnityEngine_UnityEngine_ImagePosition2091083802.h"
#include "UnityEngine_UnityEngine_GUIStyle2990928826.h"
#include "UnityEngine_UnityEngine_TextClipping3924524051.h"
#include "UnityEngine_UnityEngine_ExitGUIException2922874134.h"
#include "UnityEngine_UnityEngine_FocusType2235102504.h"
#include "UnityEngine_UnityEngine_GUIUtility1028319349.h"
#include "UnityEngine_UnityEngine_GUIClip3370872417.h"
#include "UnityEngine_UnityEngine_SliderState1233388262.h"
#include "UnityEngine_UnityEngine_SliderHandler783692703.h"
#include "UnityEngine_UnityEngine_GUITargetAttribute2337826580.h"
#include "UnityEngine_UnityEngine_TextEditor319394238.h"
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnappin1841135060.h"
#include "UnityEngine_UnityEngine_TextEditor_CharacterType1769114053.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp4145961110.h"
#include "UnityEngine_UnityEngine_Internal_DrawArguments1587375252.h"
#include "UnityEngine_UnityEngine_Internal_DrawWithTextSelec4294668057.h"
#include "UnityEngine_UnityEngine_JsonUtility4196051482.h"
#include "UnityEngine_UnityEngine_AndroidJavaException125722146.h"
#include "UnityEngine_UnityEngine_AndroidJavaRunnableProxy1013435428.h"
#include "UnityEngine_UnityEngine_AndroidJavaProxy1828457281.h"
#include "UnityEngine_UnityEngine_AndroidReflection1838616560.h"
#include "UnityEngine_UnityEngine__AndroidJNIHelper2775494809.h"
#include "UnityEngine_UnityEngine_AndroidJNISafe3064662183.h"
#include "UnityEngine_AOT_MonoPInvokeCallbackAttribute1825429660.h"
#include "UnityEngine_UnityEngine_WrapperlessIcall2494346367.h"
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttri3576128422.h"
#include "UnityEngine_UnityEngine_AttributeHelperEngine4212589506.h"
#include "UnityEngine_UnityEngine_DisallowMultipleComponent62111112.h"
#include "UnityEngine_UnityEngine_RequireComponent1687166108.h"
#include "UnityEngine_UnityEngine_AddComponentMenu813859935.h"
#include "UnityEngine_UnityEngine_ExecuteInEditMode3132250205.h"
#include "UnityEngine_UnityEngine_HideInInspector2952493798.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_SetupCoroutine1459791391.h"
#include "UnityEngine_UnityEngine_WritableAttribute2171443922.h"
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssembly1696890055.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_657441114.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2242891083.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3481375915.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2181296590.h"
#include "UnityEngine_UnityEngine_Resolution1578306928.h"
#include "UnityEngine_UnityEngine_LightType1292142182.h"
#include "UnityEngine_UnityEngine_CameraClearFlags2093155523.h"
#include "UnityEngine_UnityEngine_ColorSpace161844263.h"
#include "UnityEngine_UnityEngine_ScreenOrientation1849668026.h"
#include "UnityEngine_UnityEngine_FilterMode1625068031.h"
#include "UnityEngine_UnityEngine_TextureWrapMode1899634046.h"
#include "UnityEngine_UnityEngine_NPOTSupport1787002238.h"
#include "UnityEngine_UnityEngine_TextureFormat4189619560.h"
#include "UnityEngine_UnityEngine_RenderTextureFormat2841883826.h"
#include "UnityEngine_UnityEngine_RenderTextureReadWrite2502600968.h"
#include "UnityEngine_UnityEngine_Rendering_CompareFunction2661816155.h"
#include "UnityEngine_UnityEngine_Rendering_ColorWriteMask3214369688.h"
#include "UnityEngine_UnityEngine_Rendering_StencilOp3324967291.h"
#include "UnityEngine_UnityEngine_GUIStateObjects1371753044.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Local1307362368.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserP2280656072.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achiev344600729.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achie2116066607.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score3396031228.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leade1185876199.h"
#include "UnityEngine_UnityEngine_SendMouseEvents3965481900.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo3209134097.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState1609153288.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope1608660171.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope1305796361.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range1533311935.h"
#include "UnityEngine_UnityEngine_PropertyAttribute3531521085.h"
#include "UnityEngine_UnityEngine_TooltipAttribute1877437789.h"
#include "UnityEngine_UnityEngine_SpaceAttribute1515135354.h"
#include "UnityEngine_UnityEngine_HeaderAttribute1964924101.h"
#include "UnityEngine_UnityEngine_RangeAttribute912008995.h"
#include "UnityEngine_UnityEngine_TextAreaAttribute641061976.h"
#include "UnityEngine_UnityEngine_SelectionBaseAttribute204085987.h"
#include "UnityEngine_UnityEngine_StackTraceUtility4217621253.h"
#include "UnityEngine_UnityEngine_UnityException3473321374.h"
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttr1486273289.h"
#include "UnityEngine_UnityEngine_StateMachineBehaviour759180893.h"
#include "UnityEngine_UnityEngine_SystemClock4036018645.h"
#include "UnityEngine_UnityEngine_TrackedReference2089686725.h"
#include "UnityEngine_UnityEngine_UnityAPICompatibilityVersi2574965573.h"
#include "UnityEngine_UnityEngine_Events_PersistentListenerM3900400668.h"
#include "UnityEngine_UnityEngine_Events_ArgumentCache1171347191.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall1559630662.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall1277370263.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { sizeof (GUIStyleState_t1997423985), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2300[3] = 
{
	GUIStyleState_t1997423985::get_offset_of_m_Ptr_0(),
	GUIStyleState_t1997423985::get_offset_of_m_SourceStyle_1(),
	GUIStyleState_t1997423985::get_offset_of_m_Background_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (FontStyle_t3350479768)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2301[5] = 
{
	FontStyle_t3350479768::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (ImagePosition_t2091083802)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2302[5] = 
{
	ImagePosition_t2091083802::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (GUIStyle_t2990928826), -1, sizeof(GUIStyle_t2990928826_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2303[16] = 
{
	GUIStyle_t2990928826::get_offset_of_m_Ptr_0(),
	GUIStyle_t2990928826::get_offset_of_m_Normal_1(),
	GUIStyle_t2990928826::get_offset_of_m_Hover_2(),
	GUIStyle_t2990928826::get_offset_of_m_Active_3(),
	GUIStyle_t2990928826::get_offset_of_m_Focused_4(),
	GUIStyle_t2990928826::get_offset_of_m_OnNormal_5(),
	GUIStyle_t2990928826::get_offset_of_m_OnHover_6(),
	GUIStyle_t2990928826::get_offset_of_m_OnActive_7(),
	GUIStyle_t2990928826::get_offset_of_m_OnFocused_8(),
	GUIStyle_t2990928826::get_offset_of_m_Border_9(),
	GUIStyle_t2990928826::get_offset_of_m_Padding_10(),
	GUIStyle_t2990928826::get_offset_of_m_Margin_11(),
	GUIStyle_t2990928826::get_offset_of_m_Overflow_12(),
	GUIStyle_t2990928826::get_offset_of_m_FontInternal_13(),
	GUIStyle_t2990928826_StaticFields::get_offset_of_showKeyboardFocus_14(),
	GUIStyle_t2990928826_StaticFields::get_offset_of_s_None_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (TextClipping_t3924524051)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2304[3] = 
{
	TextClipping_t3924524051::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { sizeof (ExitGUIException_t2922874134), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (FocusType_t2235102504)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2306[4] = 
{
	FocusType_t2235102504::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { sizeof (GUIUtility_t1028319349), -1, sizeof(GUIUtility_t1028319349_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2307[4] = 
{
	GUIUtility_t1028319349_StaticFields::get_offset_of_s_SkinMode_0(),
	GUIUtility_t1028319349_StaticFields::get_offset_of_s_OriginalID_1(),
	GUIUtility_t1028319349_StaticFields::get_offset_of_s_EditorScreenPointOffset_2(),
	GUIUtility_t1028319349_StaticFields::get_offset_of_s_HasKeyboardFocus_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { sizeof (GUIClip_t3370872417), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { sizeof (SliderState_t1233388262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2309[3] = 
{
	SliderState_t1233388262::get_offset_of_dragStartPos_0(),
	SliderState_t1233388262::get_offset_of_dragStartValue_1(),
	SliderState_t1233388262::get_offset_of_isDragging_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { sizeof (SliderHandler_t783692703)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2310[9] = 
{
	SliderHandler_t783692703::get_offset_of_position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SliderHandler_t783692703::get_offset_of_currentValue_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SliderHandler_t783692703::get_offset_of_size_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SliderHandler_t783692703::get_offset_of_start_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SliderHandler_t783692703::get_offset_of_end_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SliderHandler_t783692703::get_offset_of_slider_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SliderHandler_t783692703::get_offset_of_thumb_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SliderHandler_t783692703::get_offset_of_horiz_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SliderHandler_t783692703::get_offset_of_id_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { sizeof (GUITargetAttribute_t2337826580), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2311[1] = 
{
	GUITargetAttribute_t2337826580::get_offset_of_displayMask_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { sizeof (TextEditor_t319394238), -1, sizeof(TextEditor_t319394238_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2312[24] = 
{
	TextEditor_t319394238::get_offset_of_keyboardOnScreen_0(),
	TextEditor_t319394238::get_offset_of_controlID_1(),
	TextEditor_t319394238::get_offset_of_style_2(),
	TextEditor_t319394238::get_offset_of_multiline_3(),
	TextEditor_t319394238::get_offset_of_hasHorizontalCursorPos_4(),
	TextEditor_t319394238::get_offset_of_isPasswordField_5(),
	TextEditor_t319394238::get_offset_of_m_HasFocus_6(),
	TextEditor_t319394238::get_offset_of_scrollOffset_7(),
	TextEditor_t319394238::get_offset_of_m_Content_8(),
	TextEditor_t319394238::get_offset_of_m_Position_9(),
	TextEditor_t319394238::get_offset_of_m_CursorIndex_10(),
	TextEditor_t319394238::get_offset_of_m_SelectIndex_11(),
	TextEditor_t319394238::get_offset_of_m_RevealCursor_12(),
	TextEditor_t319394238::get_offset_of_graphicalCursorPos_13(),
	TextEditor_t319394238::get_offset_of_graphicalSelectCursorPos_14(),
	TextEditor_t319394238::get_offset_of_m_MouseDragSelectsWholeWords_15(),
	TextEditor_t319394238::get_offset_of_m_DblClickInitPos_16(),
	TextEditor_t319394238::get_offset_of_m_DblClickSnap_17(),
	TextEditor_t319394238::get_offset_of_m_bJustSelected_18(),
	TextEditor_t319394238::get_offset_of_m_iAltCursorPos_19(),
	TextEditor_t319394238::get_offset_of_oldText_20(),
	TextEditor_t319394238::get_offset_of_oldPos_21(),
	TextEditor_t319394238::get_offset_of_oldSelectPos_22(),
	TextEditor_t319394238_StaticFields::get_offset_of_s_Keyactions_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { sizeof (DblClickSnapping_t1841135060)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2313[3] = 
{
	DblClickSnapping_t1841135060::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { sizeof (CharacterType_t1769114053)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2314[5] = 
{
	CharacterType_t1769114053::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { sizeof (TextEditOp_t4145961110)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2315[51] = 
{
	TextEditOp_t4145961110::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { sizeof (Internal_DrawArguments_t1587375252)+ sizeof (Il2CppObject), sizeof(Internal_DrawArguments_t1587375252_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2316[6] = 
{
	Internal_DrawArguments_t1587375252::get_offset_of_target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t1587375252::get_offset_of_position_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t1587375252::get_offset_of_isHover_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t1587375252::get_offset_of_isActive_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t1587375252::get_offset_of_on_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t1587375252::get_offset_of_hasKeyboardFocus_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { sizeof (Internal_DrawWithTextSelectionArguments_t4294668057)+ sizeof (Il2CppObject), sizeof(Internal_DrawWithTextSelectionArguments_t4294668057_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2317[11] = 
{
	Internal_DrawWithTextSelectionArguments_t4294668057::get_offset_of_target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t4294668057::get_offset_of_position_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t4294668057::get_offset_of_firstPos_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t4294668057::get_offset_of_lastPos_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t4294668057::get_offset_of_cursorColor_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t4294668057::get_offset_of_selectionColor_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t4294668057::get_offset_of_isHover_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t4294668057::get_offset_of_isActive_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t4294668057::get_offset_of_on_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t4294668057::get_offset_of_hasKeyboardFocus_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t4294668057::get_offset_of_drawSelectionAsComposition_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { sizeof (JsonUtility_t4196051482), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { sizeof (AndroidJavaException_t125722146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2319[1] = 
{
	AndroidJavaException_t125722146::get_offset_of_mJavaStackTrace_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { sizeof (AndroidJavaRunnableProxy_t1013435428), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2320[1] = 
{
	AndroidJavaRunnableProxy_t1013435428::get_offset_of_mRunnable_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { sizeof (AndroidJavaProxy_t1828457281), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2321[1] = 
{
	AndroidJavaProxy_t1828457281::get_offset_of_javaInterface_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { sizeof (AndroidReflection_t1838616560), -1, sizeof(AndroidReflection_t1838616560_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2322[5] = 
{
	AndroidReflection_t1838616560_StaticFields::get_offset_of_s_ReflectionHelperClass_0(),
	AndroidReflection_t1838616560_StaticFields::get_offset_of_s_ReflectionHelperGetConstructorID_1(),
	AndroidReflection_t1838616560_StaticFields::get_offset_of_s_ReflectionHelperGetMethodID_2(),
	AndroidReflection_t1838616560_StaticFields::get_offset_of_s_ReflectionHelperGetFieldID_3(),
	AndroidReflection_t1838616560_StaticFields::get_offset_of_s_ReflectionHelperNewProxyInstance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { sizeof (_AndroidJNIHelper_t2775494809), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (AndroidJNISafe_t3064662183), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { sizeof (MonoPInvokeCallbackAttribute_t1825429660), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { sizeof (WrapperlessIcall_t2494346367), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { sizeof (IL2CPPStructAlignmentAttribute_t3576128422), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2327[1] = 
{
	IL2CPPStructAlignmentAttribute_t3576128422::get_offset_of_Align_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { sizeof (AttributeHelperEngine_t4212589506), -1, sizeof(AttributeHelperEngine_t4212589506_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2328[3] = 
{
	AttributeHelperEngine_t4212589506_StaticFields::get_offset_of__disallowMultipleComponentArray_0(),
	AttributeHelperEngine_t4212589506_StaticFields::get_offset_of__executeInEditModeArray_1(),
	AttributeHelperEngine_t4212589506_StaticFields::get_offset_of__requireComponentArray_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { sizeof (DisallowMultipleComponent_t62111112), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { sizeof (RequireComponent_t1687166108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2330[3] = 
{
	RequireComponent_t1687166108::get_offset_of_m_Type0_0(),
	RequireComponent_t1687166108::get_offset_of_m_Type1_1(),
	RequireComponent_t1687166108::get_offset_of_m_Type2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (AddComponentMenu_t813859935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2331[2] = 
{
	AddComponentMenu_t813859935::get_offset_of_m_AddComponentMenu_0(),
	AddComponentMenu_t813859935::get_offset_of_m_Ordering_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (ExecuteInEditMode_t3132250205), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (HideInInspector_t2952493798), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2334[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { sizeof (Color_t4194546905)+ sizeof (Il2CppObject), sizeof(Color_t4194546905_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2335[4] = 
{
	Color_t4194546905::get_offset_of_r_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color_t4194546905::get_offset_of_g_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color_t4194546905::get_offset_of_b_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color_t4194546905::get_offset_of_a_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { sizeof (SetupCoroutine_t1459791391), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { sizeof (WritableAttribute_t2171443922), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { sizeof (AssemblyIsEditorAssembly_t1696890055), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { sizeof (GcUserProfileData_t657441114)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2339[4] = 
{
	GcUserProfileData_t657441114::get_offset_of_userName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcUserProfileData_t657441114::get_offset_of_userID_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcUserProfileData_t657441114::get_offset_of_isFriend_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcUserProfileData_t657441114::get_offset_of_image_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2340 = { sizeof (GcAchievementDescriptionData_t2242891083)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2340[7] = 
{
	GcAchievementDescriptionData_t2242891083::get_offset_of_m_Identifier_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t2242891083::get_offset_of_m_Title_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t2242891083::get_offset_of_m_Image_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t2242891083::get_offset_of_m_AchievedDescription_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t2242891083::get_offset_of_m_UnachievedDescription_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t2242891083::get_offset_of_m_Hidden_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t2242891083::get_offset_of_m_Points_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2341 = { sizeof (GcAchievementData_t3481375915)+ sizeof (Il2CppObject), sizeof(GcAchievementData_t3481375915_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2341[5] = 
{
	GcAchievementData_t3481375915::get_offset_of_m_Identifier_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementData_t3481375915::get_offset_of_m_PercentCompleted_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementData_t3481375915::get_offset_of_m_Completed_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementData_t3481375915::get_offset_of_m_Hidden_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementData_t3481375915::get_offset_of_m_LastReportedDate_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2342 = { sizeof (GcScoreData_t2181296590)+ sizeof (Il2CppObject), sizeof(GcScoreData_t2181296590_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2342[7] = 
{
	GcScoreData_t2181296590::get_offset_of_m_Category_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t2181296590::get_offset_of_m_ValueLow_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t2181296590::get_offset_of_m_ValueHigh_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t2181296590::get_offset_of_m_Date_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t2181296590::get_offset_of_m_FormattedValue_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t2181296590::get_offset_of_m_PlayerID_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t2181296590::get_offset_of_m_Rank_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2343 = { sizeof (Resolution_t1578306928)+ sizeof (Il2CppObject), sizeof(Resolution_t1578306928_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2343[3] = 
{
	Resolution_t1578306928::get_offset_of_m_Width_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resolution_t1578306928::get_offset_of_m_Height_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resolution_t1578306928::get_offset_of_m_RefreshRate_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2344 = { sizeof (LightType_t1292142182)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2344[5] = 
{
	LightType_t1292142182::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2345 = { sizeof (CameraClearFlags_t2093155523)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2345[6] = 
{
	CameraClearFlags_t2093155523::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2346 = { sizeof (ColorSpace_t161844263)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2346[4] = 
{
	ColorSpace_t161844263::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2347 = { sizeof (ScreenOrientation_t1849668026)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2347[8] = 
{
	ScreenOrientation_t1849668026::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2348 = { sizeof (FilterMode_t1625068031)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2348[4] = 
{
	FilterMode_t1625068031::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2349 = { sizeof (TextureWrapMode_t1899634046)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2349[3] = 
{
	TextureWrapMode_t1899634046::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2350 = { sizeof (NPOTSupport_t1787002238)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2350[4] = 
{
	NPOTSupport_t1787002238::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2351 = { sizeof (TextureFormat_t4189619560)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2351[47] = 
{
	TextureFormat_t4189619560::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2352 = { sizeof (RenderTextureFormat_t2841883826)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2352[20] = 
{
	RenderTextureFormat_t2841883826::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2353 = { sizeof (RenderTextureReadWrite_t2502600968)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2353[4] = 
{
	RenderTextureReadWrite_t2502600968::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2354 = { sizeof (CompareFunction_t2661816155)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2354[10] = 
{
	CompareFunction_t2661816155::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2355 = { sizeof (ColorWriteMask_t3214369688)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2355[6] = 
{
	ColorWriteMask_t3214369688::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2356 = { sizeof (StencilOp_t3324967291)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2356[9] = 
{
	StencilOp_t3324967291::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2357 = { sizeof (GUIStateObjects_t1371753044), -1, sizeof(GUIStateObjects_t1371753044_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2357[1] = 
{
	GUIStateObjects_t1371753044_StaticFields::get_offset_of_s_StateCache_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2358 = { sizeof (LocalUser_t1307362368), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2358[3] = 
{
	LocalUser_t1307362368::get_offset_of_m_Friends_5(),
	LocalUser_t1307362368::get_offset_of_m_Authenticated_6(),
	LocalUser_t1307362368::get_offset_of_m_Underage_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2359 = { sizeof (UserProfile_t2280656072), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2359[5] = 
{
	UserProfile_t2280656072::get_offset_of_m_UserName_0(),
	UserProfile_t2280656072::get_offset_of_m_ID_1(),
	UserProfile_t2280656072::get_offset_of_m_IsFriend_2(),
	UserProfile_t2280656072::get_offset_of_m_State_3(),
	UserProfile_t2280656072::get_offset_of_m_Image_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2360 = { sizeof (Achievement_t344600729), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2360[5] = 
{
	Achievement_t344600729::get_offset_of_m_Completed_0(),
	Achievement_t344600729::get_offset_of_m_Hidden_1(),
	Achievement_t344600729::get_offset_of_m_LastReportedDate_2(),
	Achievement_t344600729::get_offset_of_U3CidU3Ek__BackingField_3(),
	Achievement_t344600729::get_offset_of_U3CpercentCompletedU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2361 = { sizeof (AchievementDescription_t2116066607), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2361[7] = 
{
	AchievementDescription_t2116066607::get_offset_of_m_Title_0(),
	AchievementDescription_t2116066607::get_offset_of_m_Image_1(),
	AchievementDescription_t2116066607::get_offset_of_m_AchievedDescription_2(),
	AchievementDescription_t2116066607::get_offset_of_m_UnachievedDescription_3(),
	AchievementDescription_t2116066607::get_offset_of_m_Hidden_4(),
	AchievementDescription_t2116066607::get_offset_of_m_Points_5(),
	AchievementDescription_t2116066607::get_offset_of_U3CidU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2362 = { sizeof (Score_t3396031228), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2362[6] = 
{
	Score_t3396031228::get_offset_of_m_Date_0(),
	Score_t3396031228::get_offset_of_m_FormattedValue_1(),
	Score_t3396031228::get_offset_of_m_UserID_2(),
	Score_t3396031228::get_offset_of_m_Rank_3(),
	Score_t3396031228::get_offset_of_U3CleaderboardIDU3Ek__BackingField_4(),
	Score_t3396031228::get_offset_of_U3CvalueU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2363 = { sizeof (Leaderboard_t1185876199), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2363[10] = 
{
	Leaderboard_t1185876199::get_offset_of_m_Loading_0(),
	Leaderboard_t1185876199::get_offset_of_m_LocalUserScore_1(),
	Leaderboard_t1185876199::get_offset_of_m_MaxRange_2(),
	Leaderboard_t1185876199::get_offset_of_m_Scores_3(),
	Leaderboard_t1185876199::get_offset_of_m_Title_4(),
	Leaderboard_t1185876199::get_offset_of_m_UserIDs_5(),
	Leaderboard_t1185876199::get_offset_of_U3CidU3Ek__BackingField_6(),
	Leaderboard_t1185876199::get_offset_of_U3CuserScopeU3Ek__BackingField_7(),
	Leaderboard_t1185876199::get_offset_of_U3CrangeU3Ek__BackingField_8(),
	Leaderboard_t1185876199::get_offset_of_U3CtimeScopeU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2364 = { sizeof (SendMouseEvents_t3965481900), -1, sizeof(SendMouseEvents_t3965481900_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2364[5] = 
{
	SendMouseEvents_t3965481900_StaticFields::get_offset_of_s_MouseUsed_0(),
	SendMouseEvents_t3965481900_StaticFields::get_offset_of_m_LastHit_1(),
	SendMouseEvents_t3965481900_StaticFields::get_offset_of_m_MouseDownHit_2(),
	SendMouseEvents_t3965481900_StaticFields::get_offset_of_m_CurrentHit_3(),
	SendMouseEvents_t3965481900_StaticFields::get_offset_of_m_Cameras_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2365 = { sizeof (HitInfo_t3209134097)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2365[2] = 
{
	HitInfo_t3209134097::get_offset_of_target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HitInfo_t3209134097::get_offset_of_camera_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2366 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2367 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2368 = { sizeof (UserState_t1609153288)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2368[6] = 
{
	UserState_t1609153288::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2369 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2370 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2371 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2372 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2373 = { sizeof (UserScope_t1608660171)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2373[3] = 
{
	UserScope_t1608660171::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2374 = { sizeof (TimeScope_t1305796361)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2374[4] = 
{
	TimeScope_t1305796361::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2375 = { sizeof (Range_t1533311935)+ sizeof (Il2CppObject), sizeof(Range_t1533311935_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2375[2] = 
{
	Range_t1533311935::get_offset_of_from_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Range_t1533311935::get_offset_of_count_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2376 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2377 = { sizeof (PropertyAttribute_t3531521085), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2378 = { sizeof (TooltipAttribute_t1877437789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2378[1] = 
{
	TooltipAttribute_t1877437789::get_offset_of_tooltip_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2379 = { sizeof (SpaceAttribute_t1515135354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2379[1] = 
{
	SpaceAttribute_t1515135354::get_offset_of_height_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2380 = { sizeof (HeaderAttribute_t1964924101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2380[1] = 
{
	HeaderAttribute_t1964924101::get_offset_of_header_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2381 = { sizeof (RangeAttribute_t912008995), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2381[2] = 
{
	RangeAttribute_t912008995::get_offset_of_min_0(),
	RangeAttribute_t912008995::get_offset_of_max_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2382 = { sizeof (TextAreaAttribute_t641061976), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2382[2] = 
{
	TextAreaAttribute_t641061976::get_offset_of_minLines_0(),
	TextAreaAttribute_t641061976::get_offset_of_maxLines_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2383 = { sizeof (SelectionBaseAttribute_t204085987), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2384 = { sizeof (StackTraceUtility_t4217621253), -1, sizeof(StackTraceUtility_t4217621253_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2384[1] = 
{
	StackTraceUtility_t4217621253_StaticFields::get_offset_of_projectFolder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2385 = { sizeof (UnityException_t3473321374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2385[2] = 
{
	0,
	UnityException_t3473321374::get_offset_of_unityStackTrace_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2386 = { sizeof (SharedBetweenAnimatorsAttribute_t1486273289), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2387 = { sizeof (StateMachineBehaviour_t759180893), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2388 = { sizeof (SystemClock_t4036018645), -1, sizeof(SystemClock_t4036018645_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2388[1] = 
{
	SystemClock_t4036018645_StaticFields::get_offset_of_s_Epoch_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2389 = { sizeof (TrackedReference_t2089686725), sizeof(TrackedReference_t2089686725_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2389[1] = 
{
	TrackedReference_t2089686725::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2390 = { sizeof (UnityAPICompatibilityVersionAttribute_t2574965573), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2390[1] = 
{
	UnityAPICompatibilityVersionAttribute_t2574965573::get_offset_of__version_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2391 = { sizeof (PersistentListenerMode_t3900400668)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2391[8] = 
{
	PersistentListenerMode_t3900400668::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2392 = { sizeof (ArgumentCache_t1171347191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2392[6] = 
{
	ArgumentCache_t1171347191::get_offset_of_m_ObjectArgument_0(),
	ArgumentCache_t1171347191::get_offset_of_m_ObjectArgumentAssemblyTypeName_1(),
	ArgumentCache_t1171347191::get_offset_of_m_IntArgument_2(),
	ArgumentCache_t1171347191::get_offset_of_m_FloatArgument_3(),
	ArgumentCache_t1171347191::get_offset_of_m_StringArgument_4(),
	ArgumentCache_t1171347191::get_offset_of_m_BoolArgument_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2393 = { sizeof (BaseInvokableCall_t1559630662), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2394 = { sizeof (InvokableCall_t1277370263), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2394[1] = 
{
	InvokableCall_t1277370263::get_offset_of_Delegate_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2395 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2395[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2396 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2396[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2397 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2397[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2398 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2398[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2399 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2399[1] = 
{
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
