﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChangeObj
struct ChangeObj_t3459212039;

#include "codegen/il2cpp-codegen.h"

// System.Void ChangeObj::.ctor()
extern "C"  void ChangeObj__ctor_m1217210948 (ChangeObj_t3459212039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeObj::Start()
extern "C"  void ChangeObj_Start_m164348740 (ChangeObj_t3459212039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeObj::Update()
extern "C"  void ChangeObj_Update_m805695817 (ChangeObj_t3459212039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeObj::ButtonChange()
extern "C"  void ChangeObj_ButtonChange_m2936429698 (ChangeObj_t3459212039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
