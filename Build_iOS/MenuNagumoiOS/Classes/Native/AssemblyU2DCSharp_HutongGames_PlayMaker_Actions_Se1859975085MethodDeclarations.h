﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFsmMaterial
struct SetFsmMaterial_t1859975085;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFsmMaterial::.ctor()
extern "C"  void SetFsmMaterial__ctor_m1912431449 (SetFsmMaterial_t1859975085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmMaterial::Reset()
extern "C"  void SetFsmMaterial_Reset_m3853831686 (SetFsmMaterial_t1859975085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmMaterial::OnEnter()
extern "C"  void SetFsmMaterial_OnEnter_m3287315696 (SetFsmMaterial_t1859975085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmMaterial::DoSetFsmBool()
extern "C"  void SetFsmMaterial_DoSetFsmBool_m235560062 (SetFsmMaterial_t1859975085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmMaterial::OnUpdate()
extern "C"  void SetFsmMaterial_OnUpdate_m2256098131 (SetFsmMaterial_t1859975085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
