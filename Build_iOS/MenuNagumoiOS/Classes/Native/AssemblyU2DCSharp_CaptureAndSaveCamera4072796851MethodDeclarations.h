﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CaptureAndSaveCamera
struct CaptureAndSaveCamera_t4072796851;

#include "codegen/il2cpp-codegen.h"

// System.Void CaptureAndSaveCamera::.ctor()
extern "C"  void CaptureAndSaveCamera__ctor_m3922772296 (CaptureAndSaveCamera_t4072796851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
