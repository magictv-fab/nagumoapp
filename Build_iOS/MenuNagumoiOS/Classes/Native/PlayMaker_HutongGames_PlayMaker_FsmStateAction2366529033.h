﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// HutongGames.PlayMaker.FsmState
struct FsmState_t2146334067;
// HutongGames.PlayMaker.Fsm
struct Fsm_t1527112426;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmStateAction
struct  FsmStateAction_t2366529033  : public Il2CppObject
{
public:
	// System.String HutongGames.PlayMaker.FsmStateAction::name
	String_t* ___name_0;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::enabled
	bool ___enabled_1;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::isOpen
	bool ___isOpen_2;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::active
	bool ___active_3;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::finished
	bool ___finished_4;
	// UnityEngine.GameObject HutongGames.PlayMaker.FsmStateAction::owner
	GameObject_t3674682005 * ___owner_5;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.FsmStateAction::fsmState
	FsmState_t2146334067 * ___fsmState_6;
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.FsmStateAction::fsm
	Fsm_t1527112426 * ___fsm_7;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::<Entered>k__BackingField
	bool ___U3CEnteredU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(FsmStateAction_t2366529033, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_enabled_1() { return static_cast<int32_t>(offsetof(FsmStateAction_t2366529033, ___enabled_1)); }
	inline bool get_enabled_1() const { return ___enabled_1; }
	inline bool* get_address_of_enabled_1() { return &___enabled_1; }
	inline void set_enabled_1(bool value)
	{
		___enabled_1 = value;
	}

	inline static int32_t get_offset_of_isOpen_2() { return static_cast<int32_t>(offsetof(FsmStateAction_t2366529033, ___isOpen_2)); }
	inline bool get_isOpen_2() const { return ___isOpen_2; }
	inline bool* get_address_of_isOpen_2() { return &___isOpen_2; }
	inline void set_isOpen_2(bool value)
	{
		___isOpen_2 = value;
	}

	inline static int32_t get_offset_of_active_3() { return static_cast<int32_t>(offsetof(FsmStateAction_t2366529033, ___active_3)); }
	inline bool get_active_3() const { return ___active_3; }
	inline bool* get_address_of_active_3() { return &___active_3; }
	inline void set_active_3(bool value)
	{
		___active_3 = value;
	}

	inline static int32_t get_offset_of_finished_4() { return static_cast<int32_t>(offsetof(FsmStateAction_t2366529033, ___finished_4)); }
	inline bool get_finished_4() const { return ___finished_4; }
	inline bool* get_address_of_finished_4() { return &___finished_4; }
	inline void set_finished_4(bool value)
	{
		___finished_4 = value;
	}

	inline static int32_t get_offset_of_owner_5() { return static_cast<int32_t>(offsetof(FsmStateAction_t2366529033, ___owner_5)); }
	inline GameObject_t3674682005 * get_owner_5() const { return ___owner_5; }
	inline GameObject_t3674682005 ** get_address_of_owner_5() { return &___owner_5; }
	inline void set_owner_5(GameObject_t3674682005 * value)
	{
		___owner_5 = value;
		Il2CppCodeGenWriteBarrier(&___owner_5, value);
	}

	inline static int32_t get_offset_of_fsmState_6() { return static_cast<int32_t>(offsetof(FsmStateAction_t2366529033, ___fsmState_6)); }
	inline FsmState_t2146334067 * get_fsmState_6() const { return ___fsmState_6; }
	inline FsmState_t2146334067 ** get_address_of_fsmState_6() { return &___fsmState_6; }
	inline void set_fsmState_6(FsmState_t2146334067 * value)
	{
		___fsmState_6 = value;
		Il2CppCodeGenWriteBarrier(&___fsmState_6, value);
	}

	inline static int32_t get_offset_of_fsm_7() { return static_cast<int32_t>(offsetof(FsmStateAction_t2366529033, ___fsm_7)); }
	inline Fsm_t1527112426 * get_fsm_7() const { return ___fsm_7; }
	inline Fsm_t1527112426 ** get_address_of_fsm_7() { return &___fsm_7; }
	inline void set_fsm_7(Fsm_t1527112426 * value)
	{
		___fsm_7 = value;
		Il2CppCodeGenWriteBarrier(&___fsm_7, value);
	}

	inline static int32_t get_offset_of_U3CEnteredU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(FsmStateAction_t2366529033, ___U3CEnteredU3Ek__BackingField_8)); }
	inline bool get_U3CEnteredU3Ek__BackingField_8() const { return ___U3CEnteredU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CEnteredU3Ek__BackingField_8() { return &___U3CEnteredU3Ek__BackingField_8; }
	inline void set_U3CEnteredU3Ek__BackingField_8(bool value)
	{
		___U3CEnteredU3Ek__BackingField_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
