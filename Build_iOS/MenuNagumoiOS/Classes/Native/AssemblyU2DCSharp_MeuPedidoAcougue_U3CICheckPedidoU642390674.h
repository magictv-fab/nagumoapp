﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// UnityEngine.WWW
struct WWW_t3134621005;
// MeuPedidoData
struct MeuPedidoData_t2688356908;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1151101739;
// PedidoData[]
struct PedidoDataU5BU5D_t3795515318;
// PedidoData
struct PedidoData_t3443010735;
// CarnesData[]
struct CarnesDataU5BU5D_t2965870023;
// CarnesData
struct CarnesData_t3441984818;
// System.Exception
struct Exception_t3991598821;
// System.Object
struct Il2CppObject;
// MeuPedidoAcougue
struct MeuPedidoAcougue_t2271273197;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MeuPedidoAcougue/<ICheckPedido>c__Iterator67
struct  U3CICheckPedidoU3Ec__Iterator67_t642390674  : public Il2CppObject
{
public:
	// System.String MeuPedidoAcougue/<ICheckPedido>c__Iterator67::<json>__0
	String_t* ___U3CjsonU3E__0_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> MeuPedidoAcougue/<ICheckPedido>c__Iterator67::<headers>__1
	Dictionary_2_t827649927 * ___U3CheadersU3E__1_1;
	// System.Byte[] MeuPedidoAcougue/<ICheckPedido>c__Iterator67::<pData>__2
	ByteU5BU5D_t4260760469* ___U3CpDataU3E__2_2;
	// UnityEngine.WWW MeuPedidoAcougue/<ICheckPedido>c__Iterator67::<www>__3
	WWW_t3134621005 * ___U3CwwwU3E__3_3;
	// System.String MeuPedidoAcougue/<ICheckPedido>c__Iterator67::<message>__4
	String_t* ___U3CmessageU3E__4_4;
	// MeuPedidoData MeuPedidoAcougue/<ICheckPedido>c__Iterator67::<meuPedidoData>__5
	MeuPedidoData_t2688356908 * ___U3CmeuPedidoDataU3E__5_5;
	// System.Int32 MeuPedidoAcougue/<ICheckPedido>c__Iterator67::<avaliou>__6
	int32_t ___U3CavaliouU3E__6_6;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> MeuPedidoAcougue/<ICheckPedido>c__Iterator67::<statusDict>__7
	Dictionary_2_t1151101739 * ___U3CstatusDictU3E__7_7;
	// PedidoData[] MeuPedidoAcougue/<ICheckPedido>c__Iterator67::<$s_321>__8
	PedidoDataU5BU5D_t3795515318* ___U3CU24s_321U3E__8_8;
	// System.Int32 MeuPedidoAcougue/<ICheckPedido>c__Iterator67::<$s_322>__9
	int32_t ___U3CU24s_322U3E__9_9;
	// PedidoData MeuPedidoAcougue/<ICheckPedido>c__Iterator67::<pedidoData>__10
	PedidoData_t3443010735 * ___U3CpedidoDataU3E__10_10;
	// System.String MeuPedidoAcougue/<ICheckPedido>c__Iterator67::<info>__11
	String_t* ___U3CinfoU3E__11_11;
	// CarnesData[] MeuPedidoAcougue/<ICheckPedido>c__Iterator67::<$s_323>__12
	CarnesDataU5BU5D_t2965870023* ___U3CU24s_323U3E__12_12;
	// System.Int32 MeuPedidoAcougue/<ICheckPedido>c__Iterator67::<$s_324>__13
	int32_t ___U3CU24s_324U3E__13_13;
	// CarnesData MeuPedidoAcougue/<ICheckPedido>c__Iterator67::<carneData>__14
	CarnesData_t3441984818 * ___U3CcarneDataU3E__14_14;
	// System.Boolean MeuPedidoAcougue/<ICheckPedido>c__Iterator67::isUpdate
	bool ___isUpdate_15;
	// System.Exception MeuPedidoAcougue/<ICheckPedido>c__Iterator67::<ex>__15
	Exception_t3991598821 * ___U3CexU3E__15_16;
	// System.Int32 MeuPedidoAcougue/<ICheckPedido>c__Iterator67::$PC
	int32_t ___U24PC_17;
	// System.Object MeuPedidoAcougue/<ICheckPedido>c__Iterator67::$current
	Il2CppObject * ___U24current_18;
	// System.Boolean MeuPedidoAcougue/<ICheckPedido>c__Iterator67::<$>isUpdate
	bool ___U3CU24U3EisUpdate_19;
	// MeuPedidoAcougue MeuPedidoAcougue/<ICheckPedido>c__Iterator67::<>f__this
	MeuPedidoAcougue_t2271273197 * ___U3CU3Ef__this_20;

public:
	inline static int32_t get_offset_of_U3CjsonU3E__0_0() { return static_cast<int32_t>(offsetof(U3CICheckPedidoU3Ec__Iterator67_t642390674, ___U3CjsonU3E__0_0)); }
	inline String_t* get_U3CjsonU3E__0_0() const { return ___U3CjsonU3E__0_0; }
	inline String_t** get_address_of_U3CjsonU3E__0_0() { return &___U3CjsonU3E__0_0; }
	inline void set_U3CjsonU3E__0_0(String_t* value)
	{
		___U3CjsonU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjsonU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CheadersU3E__1_1() { return static_cast<int32_t>(offsetof(U3CICheckPedidoU3Ec__Iterator67_t642390674, ___U3CheadersU3E__1_1)); }
	inline Dictionary_2_t827649927 * get_U3CheadersU3E__1_1() const { return ___U3CheadersU3E__1_1; }
	inline Dictionary_2_t827649927 ** get_address_of_U3CheadersU3E__1_1() { return &___U3CheadersU3E__1_1; }
	inline void set_U3CheadersU3E__1_1(Dictionary_2_t827649927 * value)
	{
		___U3CheadersU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CheadersU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CpDataU3E__2_2() { return static_cast<int32_t>(offsetof(U3CICheckPedidoU3Ec__Iterator67_t642390674, ___U3CpDataU3E__2_2)); }
	inline ByteU5BU5D_t4260760469* get_U3CpDataU3E__2_2() const { return ___U3CpDataU3E__2_2; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CpDataU3E__2_2() { return &___U3CpDataU3E__2_2; }
	inline void set_U3CpDataU3E__2_2(ByteU5BU5D_t4260760469* value)
	{
		___U3CpDataU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpDataU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__3_3() { return static_cast<int32_t>(offsetof(U3CICheckPedidoU3Ec__Iterator67_t642390674, ___U3CwwwU3E__3_3)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__3_3() const { return ___U3CwwwU3E__3_3; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__3_3() { return &___U3CwwwU3E__3_3; }
	inline void set_U3CwwwU3E__3_3(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CmessageU3E__4_4() { return static_cast<int32_t>(offsetof(U3CICheckPedidoU3Ec__Iterator67_t642390674, ___U3CmessageU3E__4_4)); }
	inline String_t* get_U3CmessageU3E__4_4() const { return ___U3CmessageU3E__4_4; }
	inline String_t** get_address_of_U3CmessageU3E__4_4() { return &___U3CmessageU3E__4_4; }
	inline void set_U3CmessageU3E__4_4(String_t* value)
	{
		___U3CmessageU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmessageU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U3CmeuPedidoDataU3E__5_5() { return static_cast<int32_t>(offsetof(U3CICheckPedidoU3Ec__Iterator67_t642390674, ___U3CmeuPedidoDataU3E__5_5)); }
	inline MeuPedidoData_t2688356908 * get_U3CmeuPedidoDataU3E__5_5() const { return ___U3CmeuPedidoDataU3E__5_5; }
	inline MeuPedidoData_t2688356908 ** get_address_of_U3CmeuPedidoDataU3E__5_5() { return &___U3CmeuPedidoDataU3E__5_5; }
	inline void set_U3CmeuPedidoDataU3E__5_5(MeuPedidoData_t2688356908 * value)
	{
		___U3CmeuPedidoDataU3E__5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmeuPedidoDataU3E__5_5, value);
	}

	inline static int32_t get_offset_of_U3CavaliouU3E__6_6() { return static_cast<int32_t>(offsetof(U3CICheckPedidoU3Ec__Iterator67_t642390674, ___U3CavaliouU3E__6_6)); }
	inline int32_t get_U3CavaliouU3E__6_6() const { return ___U3CavaliouU3E__6_6; }
	inline int32_t* get_address_of_U3CavaliouU3E__6_6() { return &___U3CavaliouU3E__6_6; }
	inline void set_U3CavaliouU3E__6_6(int32_t value)
	{
		___U3CavaliouU3E__6_6 = value;
	}

	inline static int32_t get_offset_of_U3CstatusDictU3E__7_7() { return static_cast<int32_t>(offsetof(U3CICheckPedidoU3Ec__Iterator67_t642390674, ___U3CstatusDictU3E__7_7)); }
	inline Dictionary_2_t1151101739 * get_U3CstatusDictU3E__7_7() const { return ___U3CstatusDictU3E__7_7; }
	inline Dictionary_2_t1151101739 ** get_address_of_U3CstatusDictU3E__7_7() { return &___U3CstatusDictU3E__7_7; }
	inline void set_U3CstatusDictU3E__7_7(Dictionary_2_t1151101739 * value)
	{
		___U3CstatusDictU3E__7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CstatusDictU3E__7_7, value);
	}

	inline static int32_t get_offset_of_U3CU24s_321U3E__8_8() { return static_cast<int32_t>(offsetof(U3CICheckPedidoU3Ec__Iterator67_t642390674, ___U3CU24s_321U3E__8_8)); }
	inline PedidoDataU5BU5D_t3795515318* get_U3CU24s_321U3E__8_8() const { return ___U3CU24s_321U3E__8_8; }
	inline PedidoDataU5BU5D_t3795515318** get_address_of_U3CU24s_321U3E__8_8() { return &___U3CU24s_321U3E__8_8; }
	inline void set_U3CU24s_321U3E__8_8(PedidoDataU5BU5D_t3795515318* value)
	{
		___U3CU24s_321U3E__8_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_321U3E__8_8, value);
	}

	inline static int32_t get_offset_of_U3CU24s_322U3E__9_9() { return static_cast<int32_t>(offsetof(U3CICheckPedidoU3Ec__Iterator67_t642390674, ___U3CU24s_322U3E__9_9)); }
	inline int32_t get_U3CU24s_322U3E__9_9() const { return ___U3CU24s_322U3E__9_9; }
	inline int32_t* get_address_of_U3CU24s_322U3E__9_9() { return &___U3CU24s_322U3E__9_9; }
	inline void set_U3CU24s_322U3E__9_9(int32_t value)
	{
		___U3CU24s_322U3E__9_9 = value;
	}

	inline static int32_t get_offset_of_U3CpedidoDataU3E__10_10() { return static_cast<int32_t>(offsetof(U3CICheckPedidoU3Ec__Iterator67_t642390674, ___U3CpedidoDataU3E__10_10)); }
	inline PedidoData_t3443010735 * get_U3CpedidoDataU3E__10_10() const { return ___U3CpedidoDataU3E__10_10; }
	inline PedidoData_t3443010735 ** get_address_of_U3CpedidoDataU3E__10_10() { return &___U3CpedidoDataU3E__10_10; }
	inline void set_U3CpedidoDataU3E__10_10(PedidoData_t3443010735 * value)
	{
		___U3CpedidoDataU3E__10_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpedidoDataU3E__10_10, value);
	}

	inline static int32_t get_offset_of_U3CinfoU3E__11_11() { return static_cast<int32_t>(offsetof(U3CICheckPedidoU3Ec__Iterator67_t642390674, ___U3CinfoU3E__11_11)); }
	inline String_t* get_U3CinfoU3E__11_11() const { return ___U3CinfoU3E__11_11; }
	inline String_t** get_address_of_U3CinfoU3E__11_11() { return &___U3CinfoU3E__11_11; }
	inline void set_U3CinfoU3E__11_11(String_t* value)
	{
		___U3CinfoU3E__11_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CinfoU3E__11_11, value);
	}

	inline static int32_t get_offset_of_U3CU24s_323U3E__12_12() { return static_cast<int32_t>(offsetof(U3CICheckPedidoU3Ec__Iterator67_t642390674, ___U3CU24s_323U3E__12_12)); }
	inline CarnesDataU5BU5D_t2965870023* get_U3CU24s_323U3E__12_12() const { return ___U3CU24s_323U3E__12_12; }
	inline CarnesDataU5BU5D_t2965870023** get_address_of_U3CU24s_323U3E__12_12() { return &___U3CU24s_323U3E__12_12; }
	inline void set_U3CU24s_323U3E__12_12(CarnesDataU5BU5D_t2965870023* value)
	{
		___U3CU24s_323U3E__12_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_323U3E__12_12, value);
	}

	inline static int32_t get_offset_of_U3CU24s_324U3E__13_13() { return static_cast<int32_t>(offsetof(U3CICheckPedidoU3Ec__Iterator67_t642390674, ___U3CU24s_324U3E__13_13)); }
	inline int32_t get_U3CU24s_324U3E__13_13() const { return ___U3CU24s_324U3E__13_13; }
	inline int32_t* get_address_of_U3CU24s_324U3E__13_13() { return &___U3CU24s_324U3E__13_13; }
	inline void set_U3CU24s_324U3E__13_13(int32_t value)
	{
		___U3CU24s_324U3E__13_13 = value;
	}

	inline static int32_t get_offset_of_U3CcarneDataU3E__14_14() { return static_cast<int32_t>(offsetof(U3CICheckPedidoU3Ec__Iterator67_t642390674, ___U3CcarneDataU3E__14_14)); }
	inline CarnesData_t3441984818 * get_U3CcarneDataU3E__14_14() const { return ___U3CcarneDataU3E__14_14; }
	inline CarnesData_t3441984818 ** get_address_of_U3CcarneDataU3E__14_14() { return &___U3CcarneDataU3E__14_14; }
	inline void set_U3CcarneDataU3E__14_14(CarnesData_t3441984818 * value)
	{
		___U3CcarneDataU3E__14_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcarneDataU3E__14_14, value);
	}

	inline static int32_t get_offset_of_isUpdate_15() { return static_cast<int32_t>(offsetof(U3CICheckPedidoU3Ec__Iterator67_t642390674, ___isUpdate_15)); }
	inline bool get_isUpdate_15() const { return ___isUpdate_15; }
	inline bool* get_address_of_isUpdate_15() { return &___isUpdate_15; }
	inline void set_isUpdate_15(bool value)
	{
		___isUpdate_15 = value;
	}

	inline static int32_t get_offset_of_U3CexU3E__15_16() { return static_cast<int32_t>(offsetof(U3CICheckPedidoU3Ec__Iterator67_t642390674, ___U3CexU3E__15_16)); }
	inline Exception_t3991598821 * get_U3CexU3E__15_16() const { return ___U3CexU3E__15_16; }
	inline Exception_t3991598821 ** get_address_of_U3CexU3E__15_16() { return &___U3CexU3E__15_16; }
	inline void set_U3CexU3E__15_16(Exception_t3991598821 * value)
	{
		___U3CexU3E__15_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CexU3E__15_16, value);
	}

	inline static int32_t get_offset_of_U24PC_17() { return static_cast<int32_t>(offsetof(U3CICheckPedidoU3Ec__Iterator67_t642390674, ___U24PC_17)); }
	inline int32_t get_U24PC_17() const { return ___U24PC_17; }
	inline int32_t* get_address_of_U24PC_17() { return &___U24PC_17; }
	inline void set_U24PC_17(int32_t value)
	{
		___U24PC_17 = value;
	}

	inline static int32_t get_offset_of_U24current_18() { return static_cast<int32_t>(offsetof(U3CICheckPedidoU3Ec__Iterator67_t642390674, ___U24current_18)); }
	inline Il2CppObject * get_U24current_18() const { return ___U24current_18; }
	inline Il2CppObject ** get_address_of_U24current_18() { return &___U24current_18; }
	inline void set_U24current_18(Il2CppObject * value)
	{
		___U24current_18 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_18, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EisUpdate_19() { return static_cast<int32_t>(offsetof(U3CICheckPedidoU3Ec__Iterator67_t642390674, ___U3CU24U3EisUpdate_19)); }
	inline bool get_U3CU24U3EisUpdate_19() const { return ___U3CU24U3EisUpdate_19; }
	inline bool* get_address_of_U3CU24U3EisUpdate_19() { return &___U3CU24U3EisUpdate_19; }
	inline void set_U3CU24U3EisUpdate_19(bool value)
	{
		___U3CU24U3EisUpdate_19 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_20() { return static_cast<int32_t>(offsetof(U3CICheckPedidoU3Ec__Iterator67_t642390674, ___U3CU3Ef__this_20)); }
	inline MeuPedidoAcougue_t2271273197 * get_U3CU3Ef__this_20() const { return ___U3CU3Ef__this_20; }
	inline MeuPedidoAcougue_t2271273197 ** get_address_of_U3CU3Ef__this_20() { return &___U3CU3Ef__this_20; }
	inline void set_U3CU3Ef__this_20(MeuPedidoAcougue_t2271273197 * value)
	{
		___U3CU3Ef__this_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
