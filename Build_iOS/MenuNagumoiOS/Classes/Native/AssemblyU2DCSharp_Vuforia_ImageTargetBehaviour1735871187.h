﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t4179556250;

#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTarg2347335889.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetBehaviour
struct  ImageTargetBehaviour_t1735871187  : public ImageTargetAbstractBehaviour_t2347335889
{
public:
	// Vuforia.TrackableBehaviour Vuforia.ImageTargetBehaviour::mTrackableBehaviour
	TrackableBehaviour_t4179556250 * ___mTrackableBehaviour_24;
	// System.Boolean Vuforia.ImageTargetBehaviour::_registred
	bool ____registred_25;

public:
	inline static int32_t get_offset_of_mTrackableBehaviour_24() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t1735871187, ___mTrackableBehaviour_24)); }
	inline TrackableBehaviour_t4179556250 * get_mTrackableBehaviour_24() const { return ___mTrackableBehaviour_24; }
	inline TrackableBehaviour_t4179556250 ** get_address_of_mTrackableBehaviour_24() { return &___mTrackableBehaviour_24; }
	inline void set_mTrackableBehaviour_24(TrackableBehaviour_t4179556250 * value)
	{
		___mTrackableBehaviour_24 = value;
		Il2CppCodeGenWriteBarrier(&___mTrackableBehaviour_24, value);
	}

	inline static int32_t get_offset_of__registred_25() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t1735871187, ____registred_25)); }
	inline bool get__registred_25() const { return ____registred_25; }
	inline bool* get_address_of__registred_25() { return &____registred_25; }
	inline void set__registred_25(bool value)
	{
		____registred_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
