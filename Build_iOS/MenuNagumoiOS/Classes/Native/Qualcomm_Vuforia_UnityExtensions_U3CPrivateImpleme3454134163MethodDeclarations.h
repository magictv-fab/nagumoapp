﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// <PrivateImplementationDetails>{7F9F85B4-C683-4C34-B525-19C32F1AC11B}/__StaticArrayInitTypeSize=24
struct __StaticArrayInitTypeSizeU3D24_t3454134163;
struct __StaticArrayInitTypeSizeU3D24_t3454134163_marshaled_pinvoke;
struct __StaticArrayInitTypeSizeU3D24_t3454134163_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct __StaticArrayInitTypeSizeU3D24_t3454134163;
struct __StaticArrayInitTypeSizeU3D24_t3454134163_marshaled_pinvoke;

extern "C" void __StaticArrayInitTypeSizeU3D24_t3454134163_marshal_pinvoke(const __StaticArrayInitTypeSizeU3D24_t3454134163& unmarshaled, __StaticArrayInitTypeSizeU3D24_t3454134163_marshaled_pinvoke& marshaled);
extern "C" void __StaticArrayInitTypeSizeU3D24_t3454134163_marshal_pinvoke_back(const __StaticArrayInitTypeSizeU3D24_t3454134163_marshaled_pinvoke& marshaled, __StaticArrayInitTypeSizeU3D24_t3454134163& unmarshaled);
extern "C" void __StaticArrayInitTypeSizeU3D24_t3454134163_marshal_pinvoke_cleanup(__StaticArrayInitTypeSizeU3D24_t3454134163_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct __StaticArrayInitTypeSizeU3D24_t3454134163;
struct __StaticArrayInitTypeSizeU3D24_t3454134163_marshaled_com;

extern "C" void __StaticArrayInitTypeSizeU3D24_t3454134163_marshal_com(const __StaticArrayInitTypeSizeU3D24_t3454134163& unmarshaled, __StaticArrayInitTypeSizeU3D24_t3454134163_marshaled_com& marshaled);
extern "C" void __StaticArrayInitTypeSizeU3D24_t3454134163_marshal_com_back(const __StaticArrayInitTypeSizeU3D24_t3454134163_marshaled_com& marshaled, __StaticArrayInitTypeSizeU3D24_t3454134163& unmarshaled);
extern "C" void __StaticArrayInitTypeSizeU3D24_t3454134163_marshal_com_cleanup(__StaticArrayInitTypeSizeU3D24_t3454134163_marshaled_com& marshaled);
