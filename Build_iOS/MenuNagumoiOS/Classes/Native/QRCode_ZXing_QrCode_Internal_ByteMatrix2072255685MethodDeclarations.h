﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.QrCode.Internal.ByteMatrix
struct ByteMatrix_t2072255685;
// System.Byte[][]
struct ByteU5BU5DU5BU5D_t2421305976;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void ZXing.QrCode.Internal.ByteMatrix::.ctor(System.Int32,System.Int32)
extern "C"  void ByteMatrix__ctor_m2265712687 (ByteMatrix_t2072255685 * __this, int32_t ___width0, int32_t ___height1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.QrCode.Internal.ByteMatrix::get_Height()
extern "C"  int32_t ByteMatrix_get_Height_m3514195851 (ByteMatrix_t2072255685 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.QrCode.Internal.ByteMatrix::get_Width()
extern "C"  int32_t ByteMatrix_get_Width_m3699339716 (ByteMatrix_t2072255685 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.QrCode.Internal.ByteMatrix::get_Item(System.Int32,System.Int32)
extern "C"  int32_t ByteMatrix_get_Item_m2064229327 (ByteMatrix_t2072255685 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.ByteMatrix::set_Item(System.Int32,System.Int32,System.Int32)
extern "C"  void ByteMatrix_set_Item_m845013006 (ByteMatrix_t2072255685 * __this, int32_t ___x0, int32_t ___y1, int32_t ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[][] ZXing.QrCode.Internal.ByteMatrix::get_Array()
extern "C"  ByteU5BU5DU5BU5D_t2421305976* ByteMatrix_get_Array_m1937957853 (ByteMatrix_t2072255685 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.ByteMatrix::set(System.Int32,System.Int32,System.Boolean)
extern "C"  void ByteMatrix_set_m798954894 (ByteMatrix_t2072255685 * __this, int32_t ___x0, int32_t ___y1, bool ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.ByteMatrix::clear(System.Byte)
extern "C"  void ByteMatrix_clear_m3603529481 (ByteMatrix_t2072255685 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.QrCode.Internal.ByteMatrix::ToString()
extern "C"  String_t* ByteMatrix_ToString_m205739676 (ByteMatrix_t2072255685 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
