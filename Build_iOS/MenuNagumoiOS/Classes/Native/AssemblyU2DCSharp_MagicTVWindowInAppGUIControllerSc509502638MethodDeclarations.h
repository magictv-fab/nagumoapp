﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTVWindowInAppGUIControllerScript/ScreenShotCompleteDelegate
struct ScreenShotCompleteDelegate_t509502638;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void MagicTVWindowInAppGUIControllerScript/ScreenShotCompleteDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ScreenShotCompleteDelegate__ctor_m1118397013 (ScreenShotCompleteDelegate_t509502638 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowInAppGUIControllerScript/ScreenShotCompleteDelegate::Invoke(System.String)
extern "C"  void ScreenShotCompleteDelegate_Invoke_m309709939 (ScreenShotCompleteDelegate_t509502638 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult MagicTVWindowInAppGUIControllerScript/ScreenShotCompleteDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ScreenShotCompleteDelegate_BeginInvoke_m3953535480 (ScreenShotCompleteDelegate_t509502638 * __this, String_t* ___path0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTVWindowInAppGUIControllerScript/ScreenShotCompleteDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void ScreenShotCompleteDelegate_EndInvoke_m407717605 (ScreenShotCompleteDelegate_t509502638 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
