﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ApplicationManager
struct ApplicationManager_t3252667485;

#include "codegen/il2cpp-codegen.h"

// System.Void ApplicationManager::.ctor()
extern "C"  void ApplicationManager__ctor_m3127956702 (ApplicationManager_t3252667485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ApplicationManager::Quit()
extern "C"  void ApplicationManager_Quit_m287936309 (ApplicationManager_t3252667485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
