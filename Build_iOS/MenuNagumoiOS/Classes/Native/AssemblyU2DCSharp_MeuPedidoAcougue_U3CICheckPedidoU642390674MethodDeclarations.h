﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MeuPedidoAcougue/<ICheckPedido>c__Iterator67
struct U3CICheckPedidoU3Ec__Iterator67_t642390674;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MeuPedidoAcougue/<ICheckPedido>c__Iterator67::.ctor()
extern "C"  void U3CICheckPedidoU3Ec__Iterator67__ctor_m137313353 (U3CICheckPedidoU3Ec__Iterator67_t642390674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MeuPedidoAcougue/<ICheckPedido>c__Iterator67::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CICheckPedidoU3Ec__Iterator67_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m508506547 (U3CICheckPedidoU3Ec__Iterator67_t642390674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MeuPedidoAcougue/<ICheckPedido>c__Iterator67::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CICheckPedidoU3Ec__Iterator67_System_Collections_IEnumerator_get_Current_m2609445703 (U3CICheckPedidoU3Ec__Iterator67_t642390674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MeuPedidoAcougue/<ICheckPedido>c__Iterator67::MoveNext()
extern "C"  bool U3CICheckPedidoU3Ec__Iterator67_MoveNext_m1324313331 (U3CICheckPedidoU3Ec__Iterator67_t642390674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MeuPedidoAcougue/<ICheckPedido>c__Iterator67::Dispose()
extern "C"  void U3CICheckPedidoU3Ec__Iterator67_Dispose_m3007396422 (U3CICheckPedidoU3Ec__Iterator67_t642390674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MeuPedidoAcougue/<ICheckPedido>c__Iterator67::Reset()
extern "C"  void U3CICheckPedidoU3Ec__Iterator67_Reset_m2078713590 (U3CICheckPedidoU3Ec__Iterator67_t642390674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
