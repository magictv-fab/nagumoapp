﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.CharacterController
struct CharacterController_t1618060635;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "UnityEngine_UnityEngine_Space4209342076.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ControllerSimpleMove
struct  ControllerSimpleMove_t1596918407  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.ControllerSimpleMove::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.ControllerSimpleMove::moveVector
	FsmVector3_t533912882 * ___moveVector_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ControllerSimpleMove::speed
	FsmFloat_t2134102846 * ___speed_11;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.ControllerSimpleMove::space
	int32_t ___space_12;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ControllerSimpleMove::previousGo
	GameObject_t3674682005 * ___previousGo_13;
	// UnityEngine.CharacterController HutongGames.PlayMaker.Actions.ControllerSimpleMove::controller
	CharacterController_t1618060635 * ___controller_14;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(ControllerSimpleMove_t1596918407, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_moveVector_10() { return static_cast<int32_t>(offsetof(ControllerSimpleMove_t1596918407, ___moveVector_10)); }
	inline FsmVector3_t533912882 * get_moveVector_10() const { return ___moveVector_10; }
	inline FsmVector3_t533912882 ** get_address_of_moveVector_10() { return &___moveVector_10; }
	inline void set_moveVector_10(FsmVector3_t533912882 * value)
	{
		___moveVector_10 = value;
		Il2CppCodeGenWriteBarrier(&___moveVector_10, value);
	}

	inline static int32_t get_offset_of_speed_11() { return static_cast<int32_t>(offsetof(ControllerSimpleMove_t1596918407, ___speed_11)); }
	inline FsmFloat_t2134102846 * get_speed_11() const { return ___speed_11; }
	inline FsmFloat_t2134102846 ** get_address_of_speed_11() { return &___speed_11; }
	inline void set_speed_11(FsmFloat_t2134102846 * value)
	{
		___speed_11 = value;
		Il2CppCodeGenWriteBarrier(&___speed_11, value);
	}

	inline static int32_t get_offset_of_space_12() { return static_cast<int32_t>(offsetof(ControllerSimpleMove_t1596918407, ___space_12)); }
	inline int32_t get_space_12() const { return ___space_12; }
	inline int32_t* get_address_of_space_12() { return &___space_12; }
	inline void set_space_12(int32_t value)
	{
		___space_12 = value;
	}

	inline static int32_t get_offset_of_previousGo_13() { return static_cast<int32_t>(offsetof(ControllerSimpleMove_t1596918407, ___previousGo_13)); }
	inline GameObject_t3674682005 * get_previousGo_13() const { return ___previousGo_13; }
	inline GameObject_t3674682005 ** get_address_of_previousGo_13() { return &___previousGo_13; }
	inline void set_previousGo_13(GameObject_t3674682005 * value)
	{
		___previousGo_13 = value;
		Il2CppCodeGenWriteBarrier(&___previousGo_13, value);
	}

	inline static int32_t get_offset_of_controller_14() { return static_cast<int32_t>(offsetof(ControllerSimpleMove_t1596918407, ___controller_14)); }
	inline CharacterController_t1618060635 * get_controller_14() const { return ___controller_14; }
	inline CharacterController_t1618060635 ** get_address_of_controller_14() { return &___controller_14; }
	inline void set_controller_14(CharacterController_t1618060635 * value)
	{
		___controller_14 = value;
		Il2CppCodeGenWriteBarrier(&___controller_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
