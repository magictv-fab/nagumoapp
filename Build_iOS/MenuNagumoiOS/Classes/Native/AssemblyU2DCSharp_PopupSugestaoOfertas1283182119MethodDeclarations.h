﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PopupSugestaoOfertas
struct PopupSugestaoOfertas_t1283182119;

#include "codegen/il2cpp-codegen.h"

// System.Void PopupSugestaoOfertas::.ctor()
extern "C"  void PopupSugestaoOfertas__ctor_m4114909268 (PopupSugestaoOfertas_t1283182119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupSugestaoOfertas::OnEnable()
extern "C"  void PopupSugestaoOfertas_OnEnable_m2377019058 (PopupSugestaoOfertas_t1283182119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupSugestaoOfertas::Update()
extern "C"  void PopupSugestaoOfertas_Update_m440030521 (PopupSugestaoOfertas_t1283182119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupSugestaoOfertas::FecharPopup()
extern "C"  void PopupSugestaoOfertas_FecharPopup_m2033031177 (PopupSugestaoOfertas_t1283182119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
