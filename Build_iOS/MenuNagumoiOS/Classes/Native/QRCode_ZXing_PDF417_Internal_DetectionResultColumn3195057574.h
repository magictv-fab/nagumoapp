﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.PDF417.Internal.BoundingBox
struct BoundingBox_t242606069;
// ZXing.PDF417.Internal.Codeword[]
struct CodewordU5BU5D_t399100150;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.DetectionResultColumn
struct  DetectionResultColumn_t3195057574  : public Il2CppObject
{
public:
	// ZXing.PDF417.Internal.BoundingBox ZXing.PDF417.Internal.DetectionResultColumn::<Box>k__BackingField
	BoundingBox_t242606069 * ___U3CBoxU3Ek__BackingField_0;
	// ZXing.PDF417.Internal.Codeword[] ZXing.PDF417.Internal.DetectionResultColumn::<Codewords>k__BackingField
	CodewordU5BU5D_t399100150* ___U3CCodewordsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CBoxU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DetectionResultColumn_t3195057574, ___U3CBoxU3Ek__BackingField_0)); }
	inline BoundingBox_t242606069 * get_U3CBoxU3Ek__BackingField_0() const { return ___U3CBoxU3Ek__BackingField_0; }
	inline BoundingBox_t242606069 ** get_address_of_U3CBoxU3Ek__BackingField_0() { return &___U3CBoxU3Ek__BackingField_0; }
	inline void set_U3CBoxU3Ek__BackingField_0(BoundingBox_t242606069 * value)
	{
		___U3CBoxU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CBoxU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CCodewordsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DetectionResultColumn_t3195057574, ___U3CCodewordsU3Ek__BackingField_1)); }
	inline CodewordU5BU5D_t399100150* get_U3CCodewordsU3Ek__BackingField_1() const { return ___U3CCodewordsU3Ek__BackingField_1; }
	inline CodewordU5BU5D_t399100150** get_address_of_U3CCodewordsU3Ek__BackingField_1() { return &___U3CCodewordsU3Ek__BackingField_1; }
	inline void set_U3CCodewordsU3Ek__BackingField_1(CodewordU5BU5D_t399100150* value)
	{
		___U3CCodewordsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCodewordsU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
