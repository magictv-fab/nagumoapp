﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Common.ReedSolomon.GenericGF
struct GenericGF_t2563420960;
// ZXing.Common.ReedSolomon.GenericGFPoly
struct GenericGFPoly_t755870220;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void ZXing.Common.ReedSolomon.GenericGF::.ctor(System.Int32,System.Int32,System.Int32)
extern "C"  void GenericGF__ctor_m1201533730 (GenericGF_t2563420960 * __this, int32_t ___primitive0, int32_t ___size1, int32_t ___genBase2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.ReedSolomon.GenericGFPoly ZXing.Common.ReedSolomon.GenericGF::get_Zero()
extern "C"  GenericGFPoly_t755870220 * GenericGF_get_Zero_m2117778987 (GenericGF_t2563420960 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.ReedSolomon.GenericGFPoly ZXing.Common.ReedSolomon.GenericGF::get_One()
extern "C"  GenericGFPoly_t755870220 * GenericGF_get_One_m196957477 (GenericGF_t2563420960 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.ReedSolomon.GenericGFPoly ZXing.Common.ReedSolomon.GenericGF::buildMonomial(System.Int32,System.Int32)
extern "C"  GenericGFPoly_t755870220 * GenericGF_buildMonomial_m154997542 (GenericGF_t2563420960 * __this, int32_t ___degree0, int32_t ___coefficient1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Common.ReedSolomon.GenericGF::addOrSubtract(System.Int32,System.Int32)
extern "C"  int32_t GenericGF_addOrSubtract_m606347277 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Common.ReedSolomon.GenericGF::exp(System.Int32)
extern "C"  int32_t GenericGF_exp_m1157646543 (GenericGF_t2563420960 * __this, int32_t ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Common.ReedSolomon.GenericGF::log(System.Int32)
extern "C"  int32_t GenericGF_log_m1657655734 (GenericGF_t2563420960 * __this, int32_t ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Common.ReedSolomon.GenericGF::inverse(System.Int32)
extern "C"  int32_t GenericGF_inverse_m4016860322 (GenericGF_t2563420960 * __this, int32_t ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Common.ReedSolomon.GenericGF::multiply(System.Int32,System.Int32)
extern "C"  int32_t GenericGF_multiply_m795996481 (GenericGF_t2563420960 * __this, int32_t ___a0, int32_t ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Common.ReedSolomon.GenericGF::get_Size()
extern "C"  int32_t GenericGF_get_Size_m1853846859 (GenericGF_t2563420960 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Common.ReedSolomon.GenericGF::get_GeneratorBase()
extern "C"  int32_t GenericGF_get_GeneratorBase_m1597502940 (GenericGF_t2563420960 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.Common.ReedSolomon.GenericGF::ToString()
extern "C"  String_t* GenericGF_ToString_m2767027714 (GenericGF_t2563420960 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Common.ReedSolomon.GenericGF::.cctor()
extern "C"  void GenericGF__cctor_m554541404 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
