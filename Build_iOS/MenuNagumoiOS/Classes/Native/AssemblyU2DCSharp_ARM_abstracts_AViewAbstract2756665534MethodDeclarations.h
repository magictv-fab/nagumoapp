﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.abstracts.AViewAbstract
struct AViewAbstract_t2756665534;

#include "codegen/il2cpp-codegen.h"

// System.Void ARM.abstracts.AViewAbstract::.ctor()
extern "C"  void AViewAbstract__ctor_m2356140818 (AViewAbstract_t2756665534 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.AViewAbstract::init()
extern "C"  void AViewAbstract_init_m3714620642 (AViewAbstract_t2756665534 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.AViewAbstract::Start()
extern "C"  void AViewAbstract_Start_m1303278610 (AViewAbstract_t2756665534 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.AViewAbstract::Show()
extern "C"  void AViewAbstract_Show_m3079419823 (AViewAbstract_t2756665534 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.AViewAbstract::Hide()
extern "C"  void AViewAbstract_Hide_m2765077684 (AViewAbstract_t2756665534 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
