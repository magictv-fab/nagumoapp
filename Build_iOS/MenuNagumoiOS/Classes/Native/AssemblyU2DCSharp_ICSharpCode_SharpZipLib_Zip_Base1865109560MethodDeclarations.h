﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.BaseArchiveStorage
struct BaseArchiveStorage_t1865109560;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_File4232823638.h"

// System.Void ICSharpCode.SharpZipLib.Zip.BaseArchiveStorage::.ctor(ICSharpCode.SharpZipLib.Zip.FileUpdateMode)
extern "C"  void BaseArchiveStorage__ctor_m1962682477 (BaseArchiveStorage_t1865109560 * __this, int32_t ___updateMode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Zip.FileUpdateMode ICSharpCode.SharpZipLib.Zip.BaseArchiveStorage::get_UpdateMode()
extern "C"  int32_t BaseArchiveStorage_get_UpdateMode_m2785739709 (BaseArchiveStorage_t1865109560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
