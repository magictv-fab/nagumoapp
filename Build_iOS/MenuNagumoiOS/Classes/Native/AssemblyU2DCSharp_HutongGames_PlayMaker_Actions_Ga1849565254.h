﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GameObjectIsChildOf
struct  GameObjectIsChildOf_t1849565254  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GameObjectIsChildOf::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GameObjectIsChildOf::isChildOf
	FsmGameObject_t1697147867 * ___isChildOf_10;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GameObjectIsChildOf::trueEvent
	FsmEvent_t2133468028 * ___trueEvent_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GameObjectIsChildOf::falseEvent
	FsmEvent_t2133468028 * ___falseEvent_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GameObjectIsChildOf::storeResult
	FsmBool_t1075959796 * ___storeResult_13;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(GameObjectIsChildOf_t1849565254, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_isChildOf_10() { return static_cast<int32_t>(offsetof(GameObjectIsChildOf_t1849565254, ___isChildOf_10)); }
	inline FsmGameObject_t1697147867 * get_isChildOf_10() const { return ___isChildOf_10; }
	inline FsmGameObject_t1697147867 ** get_address_of_isChildOf_10() { return &___isChildOf_10; }
	inline void set_isChildOf_10(FsmGameObject_t1697147867 * value)
	{
		___isChildOf_10 = value;
		Il2CppCodeGenWriteBarrier(&___isChildOf_10, value);
	}

	inline static int32_t get_offset_of_trueEvent_11() { return static_cast<int32_t>(offsetof(GameObjectIsChildOf_t1849565254, ___trueEvent_11)); }
	inline FsmEvent_t2133468028 * get_trueEvent_11() const { return ___trueEvent_11; }
	inline FsmEvent_t2133468028 ** get_address_of_trueEvent_11() { return &___trueEvent_11; }
	inline void set_trueEvent_11(FsmEvent_t2133468028 * value)
	{
		___trueEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___trueEvent_11, value);
	}

	inline static int32_t get_offset_of_falseEvent_12() { return static_cast<int32_t>(offsetof(GameObjectIsChildOf_t1849565254, ___falseEvent_12)); }
	inline FsmEvent_t2133468028 * get_falseEvent_12() const { return ___falseEvent_12; }
	inline FsmEvent_t2133468028 ** get_address_of_falseEvent_12() { return &___falseEvent_12; }
	inline void set_falseEvent_12(FsmEvent_t2133468028 * value)
	{
		___falseEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___falseEvent_12, value);
	}

	inline static int32_t get_offset_of_storeResult_13() { return static_cast<int32_t>(offsetof(GameObjectIsChildOf_t1849565254, ___storeResult_13)); }
	inline FsmBool_t1075959796 * get_storeResult_13() const { return ___storeResult_13; }
	inline FsmBool_t1075959796 ** get_address_of_storeResult_13() { return &___storeResult_13; }
	inline void set_storeResult_13(FsmBool_t1075959796 * value)
	{
		___storeResult_13 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
