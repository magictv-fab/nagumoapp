﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FavoritosManager/<ILoadPublish>c__Iterator53
struct U3CILoadPublishU3Ec__Iterator53_t1296913502;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void FavoritosManager/<ILoadPublish>c__Iterator53::.ctor()
extern "C"  void U3CILoadPublishU3Ec__Iterator53__ctor_m423774205 (U3CILoadPublishU3Ec__Iterator53_t1296913502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FavoritosManager/<ILoadPublish>c__Iterator53::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CILoadPublishU3Ec__Iterator53_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2632060543 (U3CILoadPublishU3Ec__Iterator53_t1296913502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FavoritosManager/<ILoadPublish>c__Iterator53::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CILoadPublishU3Ec__Iterator53_System_Collections_IEnumerator_get_Current_m3307663379 (U3CILoadPublishU3Ec__Iterator53_t1296913502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FavoritosManager/<ILoadPublish>c__Iterator53::MoveNext()
extern "C"  bool U3CILoadPublishU3Ec__Iterator53_MoveNext_m1179538111 (U3CILoadPublishU3Ec__Iterator53_t1296913502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FavoritosManager/<ILoadPublish>c__Iterator53::Dispose()
extern "C"  void U3CILoadPublishU3Ec__Iterator53_Dispose_m3418368250 (U3CILoadPublishU3Ec__Iterator53_t1296913502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FavoritosManager/<ILoadPublish>c__Iterator53::Reset()
extern "C"  void U3CILoadPublishU3Ec__Iterator53_Reset_m2365174442 (U3CILoadPublishU3Ec__Iterator53_t1296913502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
