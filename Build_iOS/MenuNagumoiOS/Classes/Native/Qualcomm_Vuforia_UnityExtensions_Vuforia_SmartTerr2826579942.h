﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Vuforia.SmartTerrainTrackable
struct SmartTerrainTrackable_t1276145027;
// UnityEngine.MeshFilter
struct MeshFilter_t3839065225;
// UnityEngine.MeshCollider
struct MeshCollider_t2667653125;

#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Trackable4179556250.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.SmartTerrainTrackableBehaviour
struct  SmartTerrainTrackableBehaviour_t2826579942  : public TrackableBehaviour_t4179556250
{
public:
	// Vuforia.SmartTerrainTrackable Vuforia.SmartTerrainTrackableBehaviour::mSmartTerrainTrackable
	Il2CppObject * ___mSmartTerrainTrackable_9;
	// System.Boolean Vuforia.SmartTerrainTrackableBehaviour::mDisableAutomaticUpdates
	bool ___mDisableAutomaticUpdates_10;
	// UnityEngine.MeshFilter Vuforia.SmartTerrainTrackableBehaviour::mMeshFilterToUpdate
	MeshFilter_t3839065225 * ___mMeshFilterToUpdate_11;
	// UnityEngine.MeshCollider Vuforia.SmartTerrainTrackableBehaviour::mMeshColliderToUpdate
	MeshCollider_t2667653125 * ___mMeshColliderToUpdate_12;

public:
	inline static int32_t get_offset_of_mSmartTerrainTrackable_9() { return static_cast<int32_t>(offsetof(SmartTerrainTrackableBehaviour_t2826579942, ___mSmartTerrainTrackable_9)); }
	inline Il2CppObject * get_mSmartTerrainTrackable_9() const { return ___mSmartTerrainTrackable_9; }
	inline Il2CppObject ** get_address_of_mSmartTerrainTrackable_9() { return &___mSmartTerrainTrackable_9; }
	inline void set_mSmartTerrainTrackable_9(Il2CppObject * value)
	{
		___mSmartTerrainTrackable_9 = value;
		Il2CppCodeGenWriteBarrier(&___mSmartTerrainTrackable_9, value);
	}

	inline static int32_t get_offset_of_mDisableAutomaticUpdates_10() { return static_cast<int32_t>(offsetof(SmartTerrainTrackableBehaviour_t2826579942, ___mDisableAutomaticUpdates_10)); }
	inline bool get_mDisableAutomaticUpdates_10() const { return ___mDisableAutomaticUpdates_10; }
	inline bool* get_address_of_mDisableAutomaticUpdates_10() { return &___mDisableAutomaticUpdates_10; }
	inline void set_mDisableAutomaticUpdates_10(bool value)
	{
		___mDisableAutomaticUpdates_10 = value;
	}

	inline static int32_t get_offset_of_mMeshFilterToUpdate_11() { return static_cast<int32_t>(offsetof(SmartTerrainTrackableBehaviour_t2826579942, ___mMeshFilterToUpdate_11)); }
	inline MeshFilter_t3839065225 * get_mMeshFilterToUpdate_11() const { return ___mMeshFilterToUpdate_11; }
	inline MeshFilter_t3839065225 ** get_address_of_mMeshFilterToUpdate_11() { return &___mMeshFilterToUpdate_11; }
	inline void set_mMeshFilterToUpdate_11(MeshFilter_t3839065225 * value)
	{
		___mMeshFilterToUpdate_11 = value;
		Il2CppCodeGenWriteBarrier(&___mMeshFilterToUpdate_11, value);
	}

	inline static int32_t get_offset_of_mMeshColliderToUpdate_12() { return static_cast<int32_t>(offsetof(SmartTerrainTrackableBehaviour_t2826579942, ___mMeshColliderToUpdate_12)); }
	inline MeshCollider_t2667653125 * get_mMeshColliderToUpdate_12() const { return ___mMeshColliderToUpdate_12; }
	inline MeshCollider_t2667653125 ** get_address_of_mMeshColliderToUpdate_12() { return &___mMeshColliderToUpdate_12; }
	inline void set_mMeshColliderToUpdate_12(MeshCollider_t2667653125 * value)
	{
		___mMeshColliderToUpdate_12 = value;
		Il2CppCodeGenWriteBarrier(&___mMeshColliderToUpdate_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
