﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutFloatLabel
struct GUILayoutFloatLabel_t2180325291;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutFloatLabel::.ctor()
extern "C"  void GUILayoutFloatLabel__ctor_m216781803 (GUILayoutFloatLabel_t2180325291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutFloatLabel::Reset()
extern "C"  void GUILayoutFloatLabel_Reset_m2158182040 (GUILayoutFloatLabel_t2180325291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutFloatLabel::OnGUI()
extern "C"  void GUILayoutFloatLabel_OnGUI_m4007147749 (GUILayoutFloatLabel_t2180325291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
