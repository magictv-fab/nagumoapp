﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.BoolOperator
struct BoolOperator_t3039326166;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.BoolOperator::.ctor()
extern "C"  void BoolOperator__ctor_m3368529744 (BoolOperator_t3039326166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolOperator::Reset()
extern "C"  void BoolOperator_Reset_m1014962685 (BoolOperator_t3039326166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolOperator::OnEnter()
extern "C"  void BoolOperator_OnEnter_m2438438695 (BoolOperator_t3039326166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolOperator::OnUpdate()
extern "C"  void BoolOperator_OnUpdate_m1710714876 (BoolOperator_t3039326166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolOperator::DoBoolOperator()
extern "C"  void BoolOperator_DoBoolOperator_m4208792525 (BoolOperator_t3039326166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
