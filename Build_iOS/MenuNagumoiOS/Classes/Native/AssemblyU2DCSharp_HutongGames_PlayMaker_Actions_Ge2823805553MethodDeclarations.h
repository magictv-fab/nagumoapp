﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo
struct GetAnimatorCurrentStateInfo_t2823805553;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::.ctor()
extern "C"  void GetAnimatorCurrentStateInfo__ctor_m3574624229 (GetAnimatorCurrentStateInfo_t2823805553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::Reset()
extern "C"  void GetAnimatorCurrentStateInfo_Reset_m1221057170 (GetAnimatorCurrentStateInfo_t2823805553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::OnEnter()
extern "C"  void GetAnimatorCurrentStateInfo_OnEnter_m2926743164 (GetAnimatorCurrentStateInfo_t2823805553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::OnUpdate()
extern "C"  void GetAnimatorCurrentStateInfo_OnUpdate_m3963251527 (GetAnimatorCurrentStateInfo_t2823805553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::OnAnimatorMoveEvent()
extern "C"  void GetAnimatorCurrentStateInfo_OnAnimatorMoveEvent_m4219685574 (GetAnimatorCurrentStateInfo_t2823805553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::GetLayerInfo()
extern "C"  void GetAnimatorCurrentStateInfo_GetLayerInfo_m2468317032 (GetAnimatorCurrentStateInfo_t2823805553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::OnExit()
extern "C"  void GetAnimatorCurrentStateInfo_OnExit_m657520028 (GetAnimatorCurrentStateInfo_t2823805553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
