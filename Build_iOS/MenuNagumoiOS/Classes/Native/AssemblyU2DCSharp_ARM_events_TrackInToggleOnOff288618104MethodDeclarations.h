﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.events.TrackInToggleOnOff
struct TrackInToggleOnOff_t288618104;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ARM.events.TrackInToggleOnOff::.ctor()
extern "C"  void TrackInToggleOnOff__ctor_m3310819900 (TrackInToggleOnOff_t288618104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.events.TrackInToggleOnOff::Start()
extern "C"  void TrackInToggleOnOff_Start_m2257957692 (TrackInToggleOnOff_t288618104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.events.TrackInToggleOnOff::trackOutHandler(System.String)
extern "C"  void TrackInToggleOnOff_trackOutHandler_m2901838433 (TrackInToggleOnOff_t288618104 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.events.TrackInToggleOnOff::trackInHandler(System.String)
extern "C"  void TrackInToggleOnOff_trackInHandler_m818480320 (TrackInToggleOnOff_t288618104 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
