﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24186358450MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3234979958(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3490093394 *, int32_t, InAppEventDispatcherOnOff_t3474551315 *, const MethodInfo*))KeyValuePair_2__ctor_m4287554263_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::get_Key()
#define KeyValuePair_2_get_Key_m2447326898(__this, method) ((  int32_t (*) (KeyValuePair_2_t3490093394 *, const MethodInfo*))KeyValuePair_2_get_Key_m965051057_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m652511219(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3490093394 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m1467337970_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::get_Value()
#define KeyValuePair_2_get_Value_m1307219058(__this, method) ((  InAppEventDispatcherOnOff_t3474551315 * (*) (KeyValuePair_2_t3490093394 *, const MethodInfo*))KeyValuePair_2_get_Value_m3958869269_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2231675507(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3490093394 *, InAppEventDispatcherOnOff_t3474551315 *, const MethodInfo*))KeyValuePair_2_set_Value_m3512555506_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::ToString()
#define KeyValuePair_2_ToString_m2363753807(__this, method) ((  String_t* (*) (KeyValuePair_2_t3490093394 *, const MethodInfo*))KeyValuePair_2_ToString_m1044167254_gshared)(__this, method)
