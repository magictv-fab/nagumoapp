﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString[]
struct FsmStringU5BU5D_t2523845914;
// HutongGames.PlayMaker.FsmFloat[]
struct FsmFloatU5BU5D_t2945380875;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.PlayerPrefsGetFloat
struct  PlayerPrefsGetFloat_t1405771171  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.Actions.PlayerPrefsGetFloat::keys
	FsmStringU5BU5D_t2523845914* ___keys_9;
	// HutongGames.PlayMaker.FsmFloat[] HutongGames.PlayMaker.Actions.PlayerPrefsGetFloat::variables
	FsmFloatU5BU5D_t2945380875* ___variables_10;

public:
	inline static int32_t get_offset_of_keys_9() { return static_cast<int32_t>(offsetof(PlayerPrefsGetFloat_t1405771171, ___keys_9)); }
	inline FsmStringU5BU5D_t2523845914* get_keys_9() const { return ___keys_9; }
	inline FsmStringU5BU5D_t2523845914** get_address_of_keys_9() { return &___keys_9; }
	inline void set_keys_9(FsmStringU5BU5D_t2523845914* value)
	{
		___keys_9 = value;
		Il2CppCodeGenWriteBarrier(&___keys_9, value);
	}

	inline static int32_t get_offset_of_variables_10() { return static_cast<int32_t>(offsetof(PlayerPrefsGetFloat_t1405771171, ___variables_10)); }
	inline FsmFloatU5BU5D_t2945380875* get_variables_10() const { return ___variables_10; }
	inline FsmFloatU5BU5D_t2945380875** get_address_of_variables_10() { return &___variables_10; }
	inline void set_variables_10(FsmFloatU5BU5D_t2945380875* value)
	{
		___variables_10 = value;
		Il2CppCodeGenWriteBarrier(&___variables_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
