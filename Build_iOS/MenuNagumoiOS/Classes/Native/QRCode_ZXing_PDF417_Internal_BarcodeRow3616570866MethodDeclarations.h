﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.PDF417.Internal.BarcodeRow
struct BarcodeRow_t3616570866;
// System.SByte[]
struct SByteU5BU5D_t2505034988;

#include "codegen/il2cpp-codegen.h"

// System.Void ZXing.PDF417.Internal.BarcodeRow::.ctor(System.Int32)
extern "C"  void BarcodeRow__ctor_m267850173 (BarcodeRow_t3616570866 * __this, int32_t ___width0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.BarcodeRow::set(System.Int32,System.Boolean)
extern "C"  void BarcodeRow_set_m3203187904 (BarcodeRow_t3616570866 * __this, int32_t ___x0, bool ___black1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.PDF417.Internal.BarcodeRow::addBar(System.Boolean,System.Int32)
extern "C"  void BarcodeRow_addBar_m2381568982 (BarcodeRow_t3616570866 * __this, bool ___black0, int32_t ___width1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.SByte[] ZXing.PDF417.Internal.BarcodeRow::getScaledRow(System.Int32)
extern "C"  SByteU5BU5D_t2505034988* BarcodeRow_getScaledRow_m706988018 (BarcodeRow_t3616570866 * __this, int32_t ___scale0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
