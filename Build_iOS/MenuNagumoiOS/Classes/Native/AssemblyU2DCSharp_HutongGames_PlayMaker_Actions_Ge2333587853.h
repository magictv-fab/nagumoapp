﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetVectorLength
struct  GetVectorLength_t2333587853  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetVectorLength::vector3
	FsmVector3_t533912882 * ___vector3_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetVectorLength::storeLength
	FsmFloat_t2134102846 * ___storeLength_10;

public:
	inline static int32_t get_offset_of_vector3_9() { return static_cast<int32_t>(offsetof(GetVectorLength_t2333587853, ___vector3_9)); }
	inline FsmVector3_t533912882 * get_vector3_9() const { return ___vector3_9; }
	inline FsmVector3_t533912882 ** get_address_of_vector3_9() { return &___vector3_9; }
	inline void set_vector3_9(FsmVector3_t533912882 * value)
	{
		___vector3_9 = value;
		Il2CppCodeGenWriteBarrier(&___vector3_9, value);
	}

	inline static int32_t get_offset_of_storeLength_10() { return static_cast<int32_t>(offsetof(GetVectorLength_t2333587853, ___storeLength_10)); }
	inline FsmFloat_t2134102846 * get_storeLength_10() const { return ___storeLength_10; }
	inline FsmFloat_t2134102846 ** get_address_of_storeLength_10() { return &___storeLength_10; }
	inline void set_storeLength_10(FsmFloat_t2134102846 * value)
	{
		___storeLength_10 = value;
		Il2CppCodeGenWriteBarrier(&___storeLength_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
