﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// UnityEngine.Animator
struct Animator_t2776330603;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAnimatorTrigger
struct  SetAnimatorTrigger_t2149117047  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAnimatorTrigger::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetAnimatorTrigger::trigger
	FsmString_t952858651 * ___trigger_10;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.SetAnimatorTrigger::_animator
	Animator_t2776330603 * ____animator_11;
	// System.Int32 HutongGames.PlayMaker.Actions.SetAnimatorTrigger::_paramID
	int32_t ____paramID_12;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(SetAnimatorTrigger_t2149117047, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_trigger_10() { return static_cast<int32_t>(offsetof(SetAnimatorTrigger_t2149117047, ___trigger_10)); }
	inline FsmString_t952858651 * get_trigger_10() const { return ___trigger_10; }
	inline FsmString_t952858651 ** get_address_of_trigger_10() { return &___trigger_10; }
	inline void set_trigger_10(FsmString_t952858651 * value)
	{
		___trigger_10 = value;
		Il2CppCodeGenWriteBarrier(&___trigger_10, value);
	}

	inline static int32_t get_offset_of__animator_11() { return static_cast<int32_t>(offsetof(SetAnimatorTrigger_t2149117047, ____animator_11)); }
	inline Animator_t2776330603 * get__animator_11() const { return ____animator_11; }
	inline Animator_t2776330603 ** get_address_of__animator_11() { return &____animator_11; }
	inline void set__animator_11(Animator_t2776330603 * value)
	{
		____animator_11 = value;
		Il2CppCodeGenWriteBarrier(&____animator_11, value);
	}

	inline static int32_t get_offset_of__paramID_12() { return static_cast<int32_t>(offsetof(SetAnimatorTrigger_t2149117047, ____paramID_12)); }
	inline int32_t get__paramID_12() const { return ____paramID_12; }
	inline int32_t* get_address_of__paramID_12() { return &____paramID_12; }
	inline void set__paramID_12(int32_t value)
	{
		____paramID_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
