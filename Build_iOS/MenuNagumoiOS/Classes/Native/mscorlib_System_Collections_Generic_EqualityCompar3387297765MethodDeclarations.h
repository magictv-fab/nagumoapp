﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<MagicTV.globals.Perspective>
struct DefaultComparer_t3387297765;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_globals_Perspective644553802.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<MagicTV.globals.Perspective>::.ctor()
extern "C"  void DefaultComparer__ctor_m2923104701_gshared (DefaultComparer_t3387297765 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2923104701(__this, method) ((  void (*) (DefaultComparer_t3387297765 *, const MethodInfo*))DefaultComparer__ctor_m2923104701_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<MagicTV.globals.Perspective>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1661634382_gshared (DefaultComparer_t3387297765 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1661634382(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3387297765 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m1661634382_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<MagicTV.globals.Perspective>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2234427918_gshared (DefaultComparer_t3387297765 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m2234427918(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3387297765 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m2234427918_gshared)(__this, ___x0, ___y1, method)
