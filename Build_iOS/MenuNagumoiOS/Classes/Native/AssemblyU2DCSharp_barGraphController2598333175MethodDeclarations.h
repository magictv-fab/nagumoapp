﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// barGraphController
struct barGraphController_t2598333175;

#include "codegen/il2cpp-codegen.h"

// System.Void barGraphController::.ctor()
extern "C"  void barGraphController__ctor_m601488260 (barGraphController_t2598333175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void barGraphController::Start()
extern "C"  void barGraphController_Start_m3843593348 (barGraphController_t2598333175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void barGraphController::Update()
extern "C"  void barGraphController_Update_m3193128969 (barGraphController_t2598333175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void barGraphController::updateBarGraphText(System.Single)
extern "C"  void barGraphController_updateBarGraphText_m2223779450 (barGraphController_t2598333175 * __this, float ___valor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void barGraphController::updateFillBar(System.Single)
extern "C"  void barGraphController_updateFillBar_m4170244034 (barGraphController_t2598333175 * __this, float ___valor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
