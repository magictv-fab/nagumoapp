﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.UI.InputField
struct InputField_t609046876;
// UnityEngine.UI.Toggle
struct Toggle_t110812896;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Area730.Examples.ExampleSceneController
struct  ExampleSceneController_t2779674540  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.Text Area730.Examples.ExampleSceneController::versionLabel
	Text_t9039225 * ___versionLabel_2;
	// UnityEngine.UI.InputField Area730.Examples.ExampleSceneController::titlefield
	InputField_t609046876 * ___titlefield_3;
	// UnityEngine.UI.InputField Area730.Examples.ExampleSceneController::bodyField
	InputField_t609046876 * ___bodyField_4;
	// UnityEngine.UI.InputField Area730.Examples.ExampleSceneController::tickerField
	InputField_t609046876 * ___tickerField_5;
	// UnityEngine.UI.InputField Area730.Examples.ExampleSceneController::delayField
	InputField_t609046876 * ___delayField_6;
	// UnityEngine.UI.InputField Area730.Examples.ExampleSceneController::idField
	InputField_t609046876 * ___idField_7;
	// UnityEngine.UI.InputField Area730.Examples.ExampleSceneController::intervalField
	InputField_t609046876 * ___intervalField_8;
	// UnityEngine.UI.Toggle Area730.Examples.ExampleSceneController::alertOnce
	Toggle_t110812896 * ___alertOnce_9;
	// UnityEngine.UI.Toggle Area730.Examples.ExampleSceneController::sound
	Toggle_t110812896 * ___sound_10;
	// UnityEngine.UI.Toggle Area730.Examples.ExampleSceneController::vibro
	Toggle_t110812896 * ___vibro_11;
	// UnityEngine.UI.Toggle Area730.Examples.ExampleSceneController::autoCancel
	Toggle_t110812896 * ___autoCancel_12;
	// UnityEngine.UI.Toggle Area730.Examples.ExampleSceneController::repeating
	Toggle_t110812896 * ___repeating_13;
	// UnityEngine.UI.InputField Area730.Examples.ExampleSceneController::colorField
	InputField_t609046876 * ___colorField_14;
	// System.String Area730.Examples.ExampleSceneController::title
	String_t* ___title_15;
	// System.String Area730.Examples.ExampleSceneController::body
	String_t* ___body_16;
	// System.String Area730.Examples.ExampleSceneController::ticker
	String_t* ___ticker_17;
	// System.Int32 Area730.Examples.ExampleSceneController::delay
	int32_t ___delay_18;
	// System.Int32 Area730.Examples.ExampleSceneController::id
	int32_t ___id_19;
	// System.Int32 Area730.Examples.ExampleSceneController::interval
	int32_t ___interval_20;
	// System.Int32 Area730.Examples.ExampleSceneController::flags
	int32_t ___flags_21;
	// System.String Area730.Examples.ExampleSceneController::color
	String_t* ___color_22;

public:
	inline static int32_t get_offset_of_versionLabel_2() { return static_cast<int32_t>(offsetof(ExampleSceneController_t2779674540, ___versionLabel_2)); }
	inline Text_t9039225 * get_versionLabel_2() const { return ___versionLabel_2; }
	inline Text_t9039225 ** get_address_of_versionLabel_2() { return &___versionLabel_2; }
	inline void set_versionLabel_2(Text_t9039225 * value)
	{
		___versionLabel_2 = value;
		Il2CppCodeGenWriteBarrier(&___versionLabel_2, value);
	}

	inline static int32_t get_offset_of_titlefield_3() { return static_cast<int32_t>(offsetof(ExampleSceneController_t2779674540, ___titlefield_3)); }
	inline InputField_t609046876 * get_titlefield_3() const { return ___titlefield_3; }
	inline InputField_t609046876 ** get_address_of_titlefield_3() { return &___titlefield_3; }
	inline void set_titlefield_3(InputField_t609046876 * value)
	{
		___titlefield_3 = value;
		Il2CppCodeGenWriteBarrier(&___titlefield_3, value);
	}

	inline static int32_t get_offset_of_bodyField_4() { return static_cast<int32_t>(offsetof(ExampleSceneController_t2779674540, ___bodyField_4)); }
	inline InputField_t609046876 * get_bodyField_4() const { return ___bodyField_4; }
	inline InputField_t609046876 ** get_address_of_bodyField_4() { return &___bodyField_4; }
	inline void set_bodyField_4(InputField_t609046876 * value)
	{
		___bodyField_4 = value;
		Il2CppCodeGenWriteBarrier(&___bodyField_4, value);
	}

	inline static int32_t get_offset_of_tickerField_5() { return static_cast<int32_t>(offsetof(ExampleSceneController_t2779674540, ___tickerField_5)); }
	inline InputField_t609046876 * get_tickerField_5() const { return ___tickerField_5; }
	inline InputField_t609046876 ** get_address_of_tickerField_5() { return &___tickerField_5; }
	inline void set_tickerField_5(InputField_t609046876 * value)
	{
		___tickerField_5 = value;
		Il2CppCodeGenWriteBarrier(&___tickerField_5, value);
	}

	inline static int32_t get_offset_of_delayField_6() { return static_cast<int32_t>(offsetof(ExampleSceneController_t2779674540, ___delayField_6)); }
	inline InputField_t609046876 * get_delayField_6() const { return ___delayField_6; }
	inline InputField_t609046876 ** get_address_of_delayField_6() { return &___delayField_6; }
	inline void set_delayField_6(InputField_t609046876 * value)
	{
		___delayField_6 = value;
		Il2CppCodeGenWriteBarrier(&___delayField_6, value);
	}

	inline static int32_t get_offset_of_idField_7() { return static_cast<int32_t>(offsetof(ExampleSceneController_t2779674540, ___idField_7)); }
	inline InputField_t609046876 * get_idField_7() const { return ___idField_7; }
	inline InputField_t609046876 ** get_address_of_idField_7() { return &___idField_7; }
	inline void set_idField_7(InputField_t609046876 * value)
	{
		___idField_7 = value;
		Il2CppCodeGenWriteBarrier(&___idField_7, value);
	}

	inline static int32_t get_offset_of_intervalField_8() { return static_cast<int32_t>(offsetof(ExampleSceneController_t2779674540, ___intervalField_8)); }
	inline InputField_t609046876 * get_intervalField_8() const { return ___intervalField_8; }
	inline InputField_t609046876 ** get_address_of_intervalField_8() { return &___intervalField_8; }
	inline void set_intervalField_8(InputField_t609046876 * value)
	{
		___intervalField_8 = value;
		Il2CppCodeGenWriteBarrier(&___intervalField_8, value);
	}

	inline static int32_t get_offset_of_alertOnce_9() { return static_cast<int32_t>(offsetof(ExampleSceneController_t2779674540, ___alertOnce_9)); }
	inline Toggle_t110812896 * get_alertOnce_9() const { return ___alertOnce_9; }
	inline Toggle_t110812896 ** get_address_of_alertOnce_9() { return &___alertOnce_9; }
	inline void set_alertOnce_9(Toggle_t110812896 * value)
	{
		___alertOnce_9 = value;
		Il2CppCodeGenWriteBarrier(&___alertOnce_9, value);
	}

	inline static int32_t get_offset_of_sound_10() { return static_cast<int32_t>(offsetof(ExampleSceneController_t2779674540, ___sound_10)); }
	inline Toggle_t110812896 * get_sound_10() const { return ___sound_10; }
	inline Toggle_t110812896 ** get_address_of_sound_10() { return &___sound_10; }
	inline void set_sound_10(Toggle_t110812896 * value)
	{
		___sound_10 = value;
		Il2CppCodeGenWriteBarrier(&___sound_10, value);
	}

	inline static int32_t get_offset_of_vibro_11() { return static_cast<int32_t>(offsetof(ExampleSceneController_t2779674540, ___vibro_11)); }
	inline Toggle_t110812896 * get_vibro_11() const { return ___vibro_11; }
	inline Toggle_t110812896 ** get_address_of_vibro_11() { return &___vibro_11; }
	inline void set_vibro_11(Toggle_t110812896 * value)
	{
		___vibro_11 = value;
		Il2CppCodeGenWriteBarrier(&___vibro_11, value);
	}

	inline static int32_t get_offset_of_autoCancel_12() { return static_cast<int32_t>(offsetof(ExampleSceneController_t2779674540, ___autoCancel_12)); }
	inline Toggle_t110812896 * get_autoCancel_12() const { return ___autoCancel_12; }
	inline Toggle_t110812896 ** get_address_of_autoCancel_12() { return &___autoCancel_12; }
	inline void set_autoCancel_12(Toggle_t110812896 * value)
	{
		___autoCancel_12 = value;
		Il2CppCodeGenWriteBarrier(&___autoCancel_12, value);
	}

	inline static int32_t get_offset_of_repeating_13() { return static_cast<int32_t>(offsetof(ExampleSceneController_t2779674540, ___repeating_13)); }
	inline Toggle_t110812896 * get_repeating_13() const { return ___repeating_13; }
	inline Toggle_t110812896 ** get_address_of_repeating_13() { return &___repeating_13; }
	inline void set_repeating_13(Toggle_t110812896 * value)
	{
		___repeating_13 = value;
		Il2CppCodeGenWriteBarrier(&___repeating_13, value);
	}

	inline static int32_t get_offset_of_colorField_14() { return static_cast<int32_t>(offsetof(ExampleSceneController_t2779674540, ___colorField_14)); }
	inline InputField_t609046876 * get_colorField_14() const { return ___colorField_14; }
	inline InputField_t609046876 ** get_address_of_colorField_14() { return &___colorField_14; }
	inline void set_colorField_14(InputField_t609046876 * value)
	{
		___colorField_14 = value;
		Il2CppCodeGenWriteBarrier(&___colorField_14, value);
	}

	inline static int32_t get_offset_of_title_15() { return static_cast<int32_t>(offsetof(ExampleSceneController_t2779674540, ___title_15)); }
	inline String_t* get_title_15() const { return ___title_15; }
	inline String_t** get_address_of_title_15() { return &___title_15; }
	inline void set_title_15(String_t* value)
	{
		___title_15 = value;
		Il2CppCodeGenWriteBarrier(&___title_15, value);
	}

	inline static int32_t get_offset_of_body_16() { return static_cast<int32_t>(offsetof(ExampleSceneController_t2779674540, ___body_16)); }
	inline String_t* get_body_16() const { return ___body_16; }
	inline String_t** get_address_of_body_16() { return &___body_16; }
	inline void set_body_16(String_t* value)
	{
		___body_16 = value;
		Il2CppCodeGenWriteBarrier(&___body_16, value);
	}

	inline static int32_t get_offset_of_ticker_17() { return static_cast<int32_t>(offsetof(ExampleSceneController_t2779674540, ___ticker_17)); }
	inline String_t* get_ticker_17() const { return ___ticker_17; }
	inline String_t** get_address_of_ticker_17() { return &___ticker_17; }
	inline void set_ticker_17(String_t* value)
	{
		___ticker_17 = value;
		Il2CppCodeGenWriteBarrier(&___ticker_17, value);
	}

	inline static int32_t get_offset_of_delay_18() { return static_cast<int32_t>(offsetof(ExampleSceneController_t2779674540, ___delay_18)); }
	inline int32_t get_delay_18() const { return ___delay_18; }
	inline int32_t* get_address_of_delay_18() { return &___delay_18; }
	inline void set_delay_18(int32_t value)
	{
		___delay_18 = value;
	}

	inline static int32_t get_offset_of_id_19() { return static_cast<int32_t>(offsetof(ExampleSceneController_t2779674540, ___id_19)); }
	inline int32_t get_id_19() const { return ___id_19; }
	inline int32_t* get_address_of_id_19() { return &___id_19; }
	inline void set_id_19(int32_t value)
	{
		___id_19 = value;
	}

	inline static int32_t get_offset_of_interval_20() { return static_cast<int32_t>(offsetof(ExampleSceneController_t2779674540, ___interval_20)); }
	inline int32_t get_interval_20() const { return ___interval_20; }
	inline int32_t* get_address_of_interval_20() { return &___interval_20; }
	inline void set_interval_20(int32_t value)
	{
		___interval_20 = value;
	}

	inline static int32_t get_offset_of_flags_21() { return static_cast<int32_t>(offsetof(ExampleSceneController_t2779674540, ___flags_21)); }
	inline int32_t get_flags_21() const { return ___flags_21; }
	inline int32_t* get_address_of_flags_21() { return &___flags_21; }
	inline void set_flags_21(int32_t value)
	{
		___flags_21 = value;
	}

	inline static int32_t get_offset_of_color_22() { return static_cast<int32_t>(offsetof(ExampleSceneController_t2779674540, ___color_22)); }
	inline String_t* get_color_22() const { return ___color_22; }
	inline String_t** get_address_of_color_22() { return &___color_22; }
	inline void set_color_22(String_t* value)
	{
		___color_22 = value;
		Il2CppCodeGenWriteBarrier(&___color_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
