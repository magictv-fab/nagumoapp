﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AMR.device.DeviceAppStateMachineEvents
struct DeviceAppStateMachineEvents_t922942516;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// AMR.device.DeviceAppStateMachineEvents/OnPauseEventHandler
struct OnPauseEventHandler_t3609693858;
// AMR.device.DeviceAppStateMachineEvents/OnAwakeEventHandler
struct OnAwakeEventHandler_t1806304177;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AMR_device_DeviceAppStateMachineE922942516.h"
#include "AssemblyU2DCSharp_AMR_device_DeviceAppStateMachine3609693858.h"
#include "AssemblyU2DCSharp_AMR_device_DeviceAppStateMachine1806304177.h"

// System.Void AMR.device.DeviceAppStateMachineEvents::.ctor()
extern "C"  void DeviceAppStateMachineEvents__ctor_m949902289 (DeviceAppStateMachineEvents_t922942516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject AMR.device.DeviceAppStateMachineEvents::get_SelfGameObject()
extern "C"  GameObject_t3674682005 * DeviceAppStateMachineEvents_get_SelfGameObject_m2031315406 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AMR.device.DeviceAppStateMachineEvents AMR.device.DeviceAppStateMachineEvents::get_IntanceDeviceAppStateMachineEvents()
extern "C"  DeviceAppStateMachineEvents_t922942516 * DeviceAppStateMachineEvents_get_IntanceDeviceAppStateMachineEvents_m1513554475 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AMR.device.DeviceAppStateMachineEvents::set_IntanceDeviceAppStateMachineEvents(AMR.device.DeviceAppStateMachineEvents)
extern "C"  void DeviceAppStateMachineEvents_set_IntanceDeviceAppStateMachineEvents_m1453468258 (Il2CppObject * __this /* static, unused */, DeviceAppStateMachineEvents_t922942516 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AMR.device.DeviceAppStateMachineEvents::AddPauseEventHandler(AMR.device.DeviceAppStateMachineEvents/OnPauseEventHandler)
extern "C"  void DeviceAppStateMachineEvents_AddPauseEventHandler_m1601880246 (Il2CppObject * __this /* static, unused */, OnPauseEventHandler_t3609693858 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AMR.device.DeviceAppStateMachineEvents::RemovePauseEventHandler(AMR.device.DeviceAppStateMachineEvents/OnPauseEventHandler)
extern "C"  void DeviceAppStateMachineEvents_RemovePauseEventHandler_m1377671375 (Il2CppObject * __this /* static, unused */, OnPauseEventHandler_t3609693858 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AMR.device.DeviceAppStateMachineEvents::RaisePause()
extern "C"  void DeviceAppStateMachineEvents_RaisePause_m1218641085 (DeviceAppStateMachineEvents_t922942516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AMR.device.DeviceAppStateMachineEvents::AddResumeEventHandler(AMR.device.DeviceAppStateMachineEvents/OnPauseEventHandler)
extern "C"  void DeviceAppStateMachineEvents_AddResumeEventHandler_m1422524587 (Il2CppObject * __this /* static, unused */, OnPauseEventHandler_t3609693858 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AMR.device.DeviceAppStateMachineEvents::RemoveResumeEventHandler(AMR.device.DeviceAppStateMachineEvents/OnPauseEventHandler)
extern "C"  void DeviceAppStateMachineEvents_RemoveResumeEventHandler_m3061984178 (Il2CppObject * __this /* static, unused */, OnPauseEventHandler_t3609693858 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AMR.device.DeviceAppStateMachineEvents::RaiseResume()
extern "C"  void DeviceAppStateMachineEvents_RaiseResume_m1808721768 (DeviceAppStateMachineEvents_t922942516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AMR.device.DeviceAppStateMachineEvents::AddAwakeEventHandler(AMR.device.DeviceAppStateMachineEvents/OnAwakeEventHandler)
extern "C"  void DeviceAppStateMachineEvents_AddAwakeEventHandler_m537414166 (Il2CppObject * __this /* static, unused */, OnAwakeEventHandler_t1806304177 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AMR.device.DeviceAppStateMachineEvents::RemoveAwakeEventHandler(AMR.device.DeviceAppStateMachineEvents/OnAwakeEventHandler)
extern "C"  void DeviceAppStateMachineEvents_RemoveAwakeEventHandler_m313205295 (DeviceAppStateMachineEvents_t922942516 * __this, OnAwakeEventHandler_t1806304177 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AMR.device.DeviceAppStateMachineEvents::RaiseAwake()
extern "C"  void DeviceAppStateMachineEvents_RaiseAwake_m1402120332 (DeviceAppStateMachineEvents_t922942516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AMR.device.DeviceAppStateMachineEvents::set_IOSPauseToleranceInMilliseconds(System.Int32)
extern "C"  void DeviceAppStateMachineEvents_set_IOSPauseToleranceInMilliseconds_m1354601470 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AMR.device.DeviceAppStateMachineEvents::get_IOSPauseToleranceInMilliseconds()
extern "C"  int32_t DeviceAppStateMachineEvents_get_IOSPauseToleranceInMilliseconds_m56059091 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AMR.device.DeviceAppStateMachineEvents::startBackGroundTimerCount()
extern "C"  void DeviceAppStateMachineEvents_startBackGroundTimerCount_m650367945 (DeviceAppStateMachineEvents_t922942516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AMR.device.DeviceAppStateMachineEvents::checkIOSWasOnBackground()
extern "C"  void DeviceAppStateMachineEvents_checkIOSWasOnBackground_m3557328544 (DeviceAppStateMachineEvents_t922942516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AMR.device.DeviceAppStateMachineEvents::Start()
extern "C"  void DeviceAppStateMachineEvents_Start_m4192007377 (DeviceAppStateMachineEvents_t922942516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AMR.device.DeviceAppStateMachineEvents::Update()
extern "C"  void DeviceAppStateMachineEvents_Update_m1109061980 (DeviceAppStateMachineEvents_t922942516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AMR.device.DeviceAppStateMachineEvents::OnApplicationPause(System.Boolean)
extern "C"  void DeviceAppStateMachineEvents_OnApplicationPause_m935506063 (DeviceAppStateMachineEvents_t922942516 * __this, bool ___pauseStatus0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AMR.device.DeviceAppStateMachineEvents::OnApplicationFocus(System.Boolean)
extern "C"  void DeviceAppStateMachineEvents_OnApplicationFocus_m3776306033 (DeviceAppStateMachineEvents_t922942516 * __this, bool ___focusStatus0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
