﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityEngine.SocialPlatforms.Impl.Achievement
struct Achievement_t344600729;
// System.String
struct String_t;
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
struct AchievementDescription_t2116066607;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// UnityEngine.SocialPlatforms.Impl.Leaderboard
struct Leaderboard_t1185876199;
// UnityEngine.SocialPlatforms.IScore
struct IScore_t4279057999;
// UnityEngine.SocialPlatforms.IScore[]
struct IScoreU5BU5D_t250104726;
// System.String[]
struct StringU5BU5D_t4054002952;
// UnityEngine.SocialPlatforms.Impl.LocalUser
struct LocalUser_t1307362368;
// UnityEngine.SocialPlatforms.IUserProfile[]
struct IUserProfileU5BU5D_t3419104218;
// UnityEngine.SocialPlatforms.Impl.Score
struct Score_t3396031228;
// UnityEngine.SocialPlatforms.Impl.UserProfile
struct UserProfile_t2280656072;
// UnityEngine.SpaceAttribute
struct SpaceAttribute_t1515135354;
// UnityEngine.SphereCollider
struct SphereCollider_t111527973;
// UnityEngine.Sprite
struct Sprite_t3199167241;
// System.Object
struct Il2CppObject;
// System.Diagnostics.StackTrace
struct StackTrace_t1047871261;
// UnityEngine.StateMachineBehaviour
struct StateMachineBehaviour_t759180893;
// UnityEngine.Animator
struct Animator_t2776330603;
// UnityEngine.Experimental.Director.AnimatorControllerPlayable
struct AnimatorControllerPlayable_t3906681469;
// UnityEngine.TextAreaAttribute
struct TextAreaAttribute_t641061976;
// UnityEngine.TextAsset
struct TextAsset_t3836129977;
// UnityEngine.TextEditor
struct TextEditor_t319394238;
// UnityEngine.Event
struct Event_t4196595728;
// UnityEngine.TextGenerator
struct TextGenerator_t538854556;
// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t1433993036;
// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t1187093738;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t1317283468;
// System.Collections.Generic.IList`1<UnityEngine.UIVertex>
struct IList_1_t2643745119;
// System.Collections.Generic.IList`1<UnityEngine.UICharInfo>
struct IList_1_t2760454687;
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo>
struct IList_1_t2513555389;
// UnityEngine.Font
struct Font_t4241557075;
// UnityEngine.TextMesh
struct TextMesh_t2567681854;
// UnityEngine.Texture
struct Texture_t2526458961;
// UnityEngine.Color[]
struct ColorU5BU5D_t2441545636;
// UnityEngine.Color32[]
struct Color32U5BU5D_t2960766953;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t1877437789;
// UnityEngine.TouchScreenKeyboard
struct TouchScreenKeyboard_t1858258760;
// UnityEngine.TrackedReference
struct TrackedReference_t2089686725;
// UnityEngine.Transform
struct Transform_t1659122786;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.Transform/Enumerator
struct Enumerator_t3875554846;
// System.UnhandledExceptionEventArgs
struct UnhandledExceptionEventArgs_t3134093121;
// System.Exception
struct Exception_t3991598821;
// UnityEngine.UnityAPICompatibilityVersionAttribute
struct UnityAPICompatibilityVersionAttribute_t2574965573;
// UnityEngine.UnityException
struct UnityException_t3473321374;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t2372756133;
// UnityEngine.WaitForFixedUpdate
struct WaitForFixedUpdate_t2130080621;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t1615819279;
// UnityEngine.WaitUntil
struct WaitUntil_t3918096351;
// System.Func`1<System.Boolean>
struct Func_1_t1601960292;
// UnityEngine.WebCamTexture
struct WebCamTexture_t1290350902;
// UnityEngine.WebCamDevice[]
struct WebCamDeviceU5BU5D_t3721690872;
// UnityEngine.WrapperlessIcall
struct WrapperlessIcall_t2494346367;
// UnityEngine.WritableAttribute
struct WritableAttribute_t2171443922;
// UnityEngine.WWW
struct WWW_t3134621005;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.Text.Encoding
struct Encoding_t2012439129;
// UnityEngine.AudioClip
struct AudioClip_t794140988;
// UnityEngine.AssetBundle
struct AssetBundle_t2070959688;
// UnityEngine.YieldInstruction
struct YieldInstruction_t2048002629;
// UnityEngineInternal.GenericStack
struct GenericStack_t931085639;
// System.Delegate
struct Delegate_t3310234105;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Type
struct Type_t;
// UnityEngineInternal.TypeInferenceRuleAttribute
struct TypeInferenceRuleAttribute_t1657757719;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achiev344600729.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achiev344600729MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_Void2863195528.h"
#include "mscorlib_System_Double3868226565.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "mscorlib_System_DateTime4283661327MethodDeclarations.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achie2116066607.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achie2116066607MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "mscorlib_System_Int321153838500.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leade1185876199.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leade1185876199MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score3396031228MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range1533311935.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range1533311935MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope1608660171.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope1305796361.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score3396031228.h"
#include "mscorlib_System_Int641153838595.h"
#include "mscorlib_System_UInt3224667981.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Local1307362368.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Local1307362368MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserP2280656072MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserP2280656072.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState1609153288.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope1305796361MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope1608660171MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState1609153288MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SortingLayer3376264497.h"
#include "UnityEngine_UnityEngine_SortingLayer3376264497MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Space4209342076.h"
#include "UnityEngine_UnityEngine_Space4209342076MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SpaceAttribute1515135354.h"
#include "UnityEngine_UnityEngine_SpaceAttribute1515135354MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PropertyAttribute3531521085MethodDeclarations.h"
#include "mscorlib_System_Single4291918972.h"
#include "UnityEngine_UnityEngine_SphereCollider111527973.h"
#include "UnityEngine_UnityEngine_SphereCollider111527973MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Sprite3199167241.h"
#include "UnityEngine_UnityEngine_Sprite3199167241MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3071478659MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Vector44282066567MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "UnityEngine_UnityEngine_SpriteMeshType2570694128.h"
#include "UnityEngine_UnityEngine_SpriteMeshType2570694128MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SpriteRenderer2548470764.h"
#include "UnityEngine_UnityEngine_SpriteRenderer2548470764MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Sprites_DataUtility1448936472.h"
#include "UnityEngine_UnityEngine_Sprites_DataUtility1448936472MethodDeclarations.h"
#include "UnityEngine_UnityEngine_StackTraceUtility4217621253.h"
#include "UnityEngine_UnityEngine_StackTraceUtility4217621253MethodDeclarations.h"
#include "mscorlib_System_Diagnostics_StackTrace1047871261MethodDeclarations.h"
#include "mscorlib_System_Diagnostics_StackTrace1047871261.h"
#include "mscorlib_System_ArgumentException928607144MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder243639308MethodDeclarations.h"
#include "mscorlib_System_Exception3991598821MethodDeclarations.h"
#include "mscorlib_System_Exception3991598821.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "mscorlib_System_ArgumentException928607144.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898.h"
#include "mscorlib_System_Char2862622538.h"
#include "mscorlib_System_Diagnostics_StackFrame1034942277.h"
#include "mscorlib_System_Reflection_MethodBase318515428.h"
#include "mscorlib_System_Reflection_ParameterInfo2235474049.h"
#include "mscorlib_System_Diagnostics_StackFrame1034942277MethodDeclarations.h"
#include "mscorlib_System_Type2863145774MethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodBase318515428MethodDeclarations.h"
#include "mscorlib_System_Reflection_ParameterInfo2235474049MethodDeclarations.h"
#include "mscorlib_System_Int321153838500MethodDeclarations.h"
#include "UnityEngine_UnityEngine_StateMachineBehaviour759180893.h"
#include "UnityEngine_UnityEngine_StateMachineBehaviour759180893MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScriptableObject2970544072MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animator2776330603.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo323110318.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim3906681469.h"
#include "UnityEngine_UnityEngine_SystemClock4036018645.h"
#include "UnityEngine_UnityEngine_SystemClock4036018645MethodDeclarations.h"
#include "mscorlib_System_DateTimeKind1472618179.h"
#include "UnityEngine_UnityEngine_SystemInfo3820892225.h"
#include "UnityEngine_UnityEngine_SystemInfo3820892225MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NPOTSupport1787002238.h"
#include "UnityEngine_UnityEngine_DeviceType3959528308.h"
#include "UnityEngine_UnityEngine_TextAnchor213922566.h"
#include "UnityEngine_UnityEngine_TextAnchor213922566MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextAreaAttribute641061976.h"
#include "UnityEngine_UnityEngine_TextAreaAttribute641061976MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextAsset3836129977.h"
#include "UnityEngine_UnityEngine_TextAsset3836129977MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextClipping3924524051.h"
#include "UnityEngine_UnityEngine_TextClipping3924524051MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextEditor319394238.h"
#include "UnityEngine_UnityEngine_TextEditor319394238MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyle2990928826MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector24282066565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIContent2094828418MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyle2990928826.h"
#include "UnityEngine_UnityEngine_GUIContent2094828418.h"
#include "UnityEngine_UnityEngine_Rect4241904616MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Event4196595728.h"
#include "UnityEngine_UnityEngine_Event4196595728MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1390811265MethodDeclarations.h"
#include "UnityEngine_UnityEngine_EventModifiers4195406918.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp4145961110.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1390811265.h"
#include "mscorlib_System_Char2862622538MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf4203372500MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnappin1841135060.h"
#include "UnityEngine_UnityEngine_TextEditor_CharacterType1769114053.h"
#include "UnityEngine_UnityEngine_EventType637886954.h"
#include "UnityEngine_UnityEngine_RectOffset3056157787MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectOffset3056157787.h"
#include "UnityEngine_UnityEngine_Input4200062272MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug4195163081MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIUtility1028319349MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application2856536070MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform3050318497.h"
#include "UnityEngine_UnityEngine_TextEditor_CharacterType1769114053MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnappin1841135060MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp4145961110MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextGenerationSettings1923005356.h"
#include "UnityEngine_UnityEngine_TextGenerationSettings1923005356MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_FontStyle3350479768.h"
#include "UnityEngine_UnityEngine_HorizontalWrapMode2918974229.h"
#include "UnityEngine_UnityEngine_VerticalWrapMode1147493927.h"
#include "UnityEngine_UnityEngine_Font4241557075.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "UnityEngine_UnityEngine_Color4194546905MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextGenerator538854556.h"
#include "UnityEngine_UnityEngine_TextGenerator538854556MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1317283468MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1433993036MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1187093738MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1317283468.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1433993036.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1187093738.h"
#include "UnityEngine_UnityEngine_Font4241557075MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextMesh2567681854.h"
#include "UnityEngine_UnityEngine_TextMesh2567681854MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "UnityEngine_UnityEngine_Texture2526458961MethodDeclarations.h"
#include "UnityEngine_UnityEngine_FilterMode1625068031.h"
#include "UnityEngine_UnityEngine_TextureWrapMode1899634046.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_IntPtr4010401971MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextureFormat4189619560.h"
#include "UnityEngine_UnityEngine_Color32598853688.h"
#include "mscorlib_System_Byte2862609660.h"
#include "UnityEngine_UnityEngine_TextureFormat4189619560MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextureWrapMode1899634046MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time4241968337.h"
#include "UnityEngine_UnityEngine_Time4241968337MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TooltipAttribute1877437789.h"
#include "UnityEngine_UnityEngine_TooltipAttribute1877437789MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch4210255029.h"
#include "UnityEngine_UnityEngine_Touch4210255029MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchPhase1567063616.h"
#include "UnityEngine_UnityEngine_TouchType970257423.h"
#include "UnityEngine_UnityEngine_TouchPhase1567063616MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard1858258760.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard1858258760MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboardType2604324130.h"
#include "mscorlib_System_Convert1363677321MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard_Interna705488572.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard_Interna705488572MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboardType2604324130MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchType970257423MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TrackedReference2089686725.h"
#include "UnityEngine_UnityEngine_TrackedReference2089686725MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UnityEngine_Transform1659122786MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform972643934.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "UnityEngine_UnityEngine_Component3501516275MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform_Enumerator3875554846MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform_Enumerator3875554846.h"
#include "UnityEngine_UnityEngine_UICharInfo65807484.h"
#include "UnityEngine_UnityEngine_UICharInfo65807484MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UILineInfo4113875482.h"
#include "UnityEngine_UnityEngine_UILineInfo4113875482MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UIVertex4244065212.h"
#include "UnityEngine_UnityEngine_UIVertex4244065212MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32598853688MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UnhandledExceptionHandler1700300692.h"
#include "UnityEngine_UnityEngine_UnhandledExceptionHandler1700300692MethodDeclarations.h"
#include "mscorlib_System_AppDomain3575612635MethodDeclarations.h"
#include "mscorlib_System_UnhandledExceptionEventHandler2544755120MethodDeclarations.h"
#include "mscorlib_System_AppDomain3575612635.h"
#include "mscorlib_System_UnhandledExceptionEventArgs3134093121.h"
#include "mscorlib_System_UnhandledExceptionEventHandler2544755120.h"
#include "mscorlib_System_UnhandledExceptionEventArgs3134093121MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UnityAPICompatibilityVersi2574965573.h"
#include "UnityEngine_UnityEngine_UnityAPICompatibilityVersi2574965573MethodDeclarations.h"
#include "mscorlib_System_Attribute2523058482MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UnityException3473321374.h"
#include "UnityEngine_UnityEngine_UnityException3473321374MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "UnityEngine_UnityEngine_UnityString3369712284.h"
#include "UnityEngine_UnityEngine_UnityString3369712284MethodDeclarations.h"
#include "mscorlib_System_IndexOutOfRangeException3456360697MethodDeclarations.h"
#include "mscorlib_System_IndexOutOfRangeException3456360697.h"
#include "mscorlib_System_Single4291918972MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf4203372500.h"
#include "UnityEngine_UnityEngine_VerticalWrapMode1147493927MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame2372756133.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame2372756133MethodDeclarations.h"
#include "UnityEngine_UnityEngine_YieldInstruction2048002629MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForFixedUpdate2130080621.h"
#include "UnityEngine_UnityEngine_WaitForFixedUpdate2130080621MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1615819279.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1615819279MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitUntil3918096351.h"
#include "UnityEngine_UnityEngine_WaitUntil3918096351MethodDeclarations.h"
#include "System_Core_System_Func_1_gen1601960292.h"
#include "UnityEngine_UnityEngine_CustomYieldInstruction2666549910MethodDeclarations.h"
#include "System_Core_System_Func_1_gen1601960292MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WebCamDevice3274004757.h"
#include "UnityEngine_UnityEngine_WebCamDevice3274004757MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WebCamTexture1290350902.h"
#include "UnityEngine_UnityEngine_WebCamTexture1290350902MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WrapMode1491636113.h"
#include "UnityEngine_UnityEngine_WrapMode1491636113MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WrapperlessIcall2494346367.h"
#include "UnityEngine_UnityEngine_WrapperlessIcall2494346367MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WritableAttribute2171443922.h"
#include "UnityEngine_UnityEngine_WritableAttribute2171443922MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWW3134621005.h"
#include "UnityEngine_UnityEngine_WWW3134621005MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWWForm461342257.h"
#include "UnityEngine_UnityEngine_WWWForm461342257MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge827649927.h"
#include "UnityEngine_UnityEngine_Hash128346790303.h"
#include "mscorlib_System_Text_Encoding2012439129MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding2012439129.h"
#include "UnityEngine_UnityEngine_WWWTranscoder609724394MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge827649927MethodDeclarations.h"
#include "mscorlib_System_StringComparison4173268078.h"
#include "UnityEngine_UnityEngine_AudioClip794140988.h"
#include "UnityEngine_UnityEngine_AudioType794660134.h"
#include "UnityEngine_UnityEngine_AssetBundle2070959688.h"
#include "UnityEngine_UnityEngine_Hash128346790303MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_726430633.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2144973319.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2144973319MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_726430633MethodDeclarations.h"
#include "mscorlib_System_StringComparer4230573202MethodDeclarations.h"
#include "mscorlib_System_IO_StringReader4061477668MethodDeclarations.h"
#include "mscorlib_System_IO_StringReader4061477668.h"
#include "mscorlib_System_StringComparer4230573202.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1333978725MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1375417109MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random3156561159MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1333978725.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1375417109.h"
#include "mscorlib_System_IO_MemoryStream418716369MethodDeclarations.h"
#include "mscorlib_System_IO_MemoryStream418716369.h"
#include "UnityEngine_UnityEngine_WWWTranscoder609724394.h"
#include "UnityEngine_UnityEngine_YieldInstruction2048002629.h"
#include "UnityEngine_UnityEngineInternal_GenericStack931085639.h"
#include "UnityEngine_UnityEngineInternal_GenericStack931085639MethodDeclarations.h"
#include "mscorlib_System_Collections_Stack1761758306MethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_MathfInternal4096243933.h"
#include "UnityEngine_UnityEngineInternal_MathfInternal4096243933MethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_NetFxCoreExtension2541795172.h"
#include "UnityEngine_UnityEngineInternal_NetFxCoreExtension2541795172MethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodInfo318736065.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "mscorlib_System_Delegate3310234105MethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleA1657757719.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleA1657757719MethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules2889237774.h"
#include "mscorlib_System_Enum2862688501MethodDeclarations.h"
#include "mscorlib_System_Enum2862688501.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules2889237774MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor(System.String,System.Double,System.Boolean,System.Boolean,System.DateTime)
extern "C"  void Achievement__ctor_m377036415 (Achievement_t344600729 * __this, String_t* ___id0, double ___percentCompleted1, bool ___completed2, bool ___hidden3, DateTime_t4283661327  ___lastReportedDate4, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___id0;
		Achievement_set_id_m3123429815(__this, L_0, /*hidden argument*/NULL);
		double L_1 = ___percentCompleted1;
		Achievement_set_percentCompleted_m1005987436(__this, L_1, /*hidden argument*/NULL);
		bool L_2 = ___completed2;
		__this->set_m_Completed_0(L_2);
		bool L_3 = ___hidden3;
		__this->set_m_Hidden_1(L_3);
		DateTime_t4283661327  L_4 = ___lastReportedDate4;
		__this->set_m_LastReportedDate_2(L_4);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor(System.String,System.Double)
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern const uint32_t Achievement__ctor_m2960680429_MetadataUsageId;
extern "C"  void Achievement__ctor_m2960680429 (Achievement_t344600729 * __this, String_t* ___id0, double ___percent1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Achievement__ctor_m2960680429_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___id0;
		Achievement_set_id_m3123429815(__this, L_0, /*hidden argument*/NULL);
		double L_1 = ___percent1;
		Achievement_set_percentCompleted_m1005987436(__this, L_1, /*hidden argument*/NULL);
		__this->set_m_Hidden_1((bool)0);
		__this->set_m_Completed_0((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t4283661327_il2cpp_TypeInfo_var);
		DateTime_t4283661327  L_2 = ((DateTime_t4283661327_StaticFields*)DateTime_t4283661327_il2cpp_TypeInfo_var->static_fields)->get_MinValue_13();
		__this->set_m_LastReportedDate_2(L_2);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor()
extern Il2CppCodeGenString* _stringLiteral4010126410;
extern const uint32_t Achievement__ctor_m3345265521_MetadataUsageId;
extern "C"  void Achievement__ctor_m3345265521 (Achievement_t344600729 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Achievement__ctor_m3345265521_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Achievement__ctor_m2960680429(__this, _stringLiteral4010126410, (0.0), /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Achievement::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral32179;
extern const uint32_t Achievement_ToString_m2974157186_MetadataUsageId;
extern "C"  String_t* Achievement_ToString_m2974157186 (Achievement_t344600729 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Achievement_ToString_m2974157186_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)9)));
		String_t* L_1 = Achievement_get_id_m1680539866(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		ObjectU5BU5D_t1108656482* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, _stringLiteral32179);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_3 = L_2;
		double L_4 = Achievement_get_percentCompleted_m2492759109(__this, /*hidden argument*/NULL);
		double L_5 = L_4;
		Il2CppObject * L_6 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_5);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_6);
		ObjectU5BU5D_t1108656482* L_7 = L_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, _stringLiteral32179);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_8 = L_7;
		bool L_9 = Achievement_get_completed_m3689853355(__this, /*hidden argument*/NULL);
		bool L_10 = L_9;
		Il2CppObject * L_11 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_11);
		ObjectU5BU5D_t1108656482* L_12 = L_8;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 5);
		ArrayElementTypeCheck (L_12, _stringLiteral32179);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_13 = L_12;
		bool L_14 = Achievement_get_hidden_m2954555244(__this, /*hidden argument*/NULL);
		bool L_15 = L_14;
		Il2CppObject * L_16 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 6);
		ArrayElementTypeCheck (L_13, L_16);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)L_16);
		ObjectU5BU5D_t1108656482* L_17 = L_13;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 7);
		ArrayElementTypeCheck (L_17, _stringLiteral32179);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_18 = L_17;
		DateTime_t4283661327  L_19 = Achievement_get_lastReportedDate_m445111052(__this, /*hidden argument*/NULL);
		DateTime_t4283661327  L_20 = L_19;
		Il2CppObject * L_21 = Box(DateTime_t4283661327_il2cpp_TypeInfo_var, &L_20);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 8);
		ArrayElementTypeCheck (L_18, L_21);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)L_21);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m3016520001(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		return L_22;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Achievement::get_id()
extern "C"  String_t* Achievement_get_id_m1680539866 (Achievement_t344600729 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CidU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_id(System.String)
extern "C"  void Achievement_set_id_m3123429815 (Achievement_t344600729 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CidU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Double UnityEngine.SocialPlatforms.Impl.Achievement::get_percentCompleted()
extern "C"  double Achievement_get_percentCompleted_m2492759109 (Achievement_t344600729 * __this, const MethodInfo* method)
{
	{
		double L_0 = __this->get_U3CpercentCompletedU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_percentCompleted(System.Double)
extern "C"  void Achievement_set_percentCompleted_m1005987436 (Achievement_t344600729 * __this, double ___value0, const MethodInfo* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3CpercentCompletedU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_completed()
extern "C"  bool Achievement_get_completed_m3689853355 (Achievement_t344600729 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_Completed_0();
		return L_0;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_hidden()
extern "C"  bool Achievement_get_hidden_m2954555244 (Achievement_t344600729 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_Hidden_1();
		return L_0;
	}
}
// System.DateTime UnityEngine.SocialPlatforms.Impl.Achievement::get_lastReportedDate()
extern "C"  DateTime_t4283661327  Achievement_get_lastReportedDate_m445111052 (Achievement_t344600729 * __this, const MethodInfo* method)
{
	{
		DateTime_t4283661327  L_0 = __this->get_m_LastReportedDate_2();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::.ctor(System.String,System.String,UnityEngine.Texture2D,System.String,System.String,System.Boolean,System.Int32)
extern "C"  void AchievementDescription__ctor_m3032164909 (AchievementDescription_t2116066607 * __this, String_t* ___id0, String_t* ___title1, Texture2D_t3884108195 * ___image2, String_t* ___achievedDescription3, String_t* ___unachievedDescription4, bool ___hidden5, int32_t ___points6, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___id0;
		AchievementDescription_set_id_m2215766207(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___title1;
		__this->set_m_Title_0(L_1);
		Texture2D_t3884108195 * L_2 = ___image2;
		__this->set_m_Image_1(L_2);
		String_t* L_3 = ___achievedDescription3;
		__this->set_m_AchievedDescription_2(L_3);
		String_t* L_4 = ___unachievedDescription4;
		__this->set_m_UnachievedDescription_3(L_4);
		bool L_5 = ___hidden5;
		__this->set_m_Hidden_4(L_5);
		int32_t L_6 = ___points6;
		__this->set_m_Points_5(L_6);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral32179;
extern const uint32_t AchievementDescription_ToString_m1633092820_MetadataUsageId;
extern "C"  String_t* AchievementDescription_ToString_m1633092820 (AchievementDescription_t2116066607 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AchievementDescription_ToString_m1633092820_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)11)));
		String_t* L_1 = AchievementDescription_get_id_m3162941612(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		ObjectU5BU5D_t1108656482* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, _stringLiteral32179);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_3 = L_2;
		String_t* L_4 = AchievementDescription_get_title_m1294089737(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_4);
		ObjectU5BU5D_t1108656482* L_5 = L_3;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, _stringLiteral32179);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_6 = L_5;
		String_t* L_7 = AchievementDescription_get_achievedDescription_m4248956218(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_7);
		ObjectU5BU5D_t1108656482* L_8 = L_6;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 5);
		ArrayElementTypeCheck (L_8, _stringLiteral32179);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_9 = L_8;
		String_t* L_10 = AchievementDescription_get_unachievedDescription_m3429874753(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 6);
		ArrayElementTypeCheck (L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)L_10);
		ObjectU5BU5D_t1108656482* L_11 = L_9;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 7);
		ArrayElementTypeCheck (L_11, _stringLiteral32179);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_12 = L_11;
		int32_t L_13 = AchievementDescription_get_points_m4158769271(__this, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		Il2CppObject * L_15 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 8);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)L_15);
		ObjectU5BU5D_t1108656482* L_16 = L_12;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, ((int32_t)9));
		ArrayElementTypeCheck (L_16, _stringLiteral32179);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_17 = L_16;
		bool L_18 = AchievementDescription_get_hidden_m734162136(__this, /*hidden argument*/NULL);
		bool L_19 = L_18;
		Il2CppObject * L_20 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, ((int32_t)10));
		ArrayElementTypeCheck (L_17, L_20);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Il2CppObject *)L_20);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m3016520001(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		return L_21;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::SetImage(UnityEngine.Texture2D)
extern "C"  void AchievementDescription_SetImage_m1092175896 (AchievementDescription_t2116066607 * __this, Texture2D_t3884108195 * ___image0, const MethodInfo* method)
{
	{
		Texture2D_t3884108195 * L_0 = ___image0;
		__this->set_m_Image_1(L_0);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_id()
extern "C"  String_t* AchievementDescription_get_id_m3162941612 (AchievementDescription_t2116066607 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CidU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::set_id(System.String)
extern "C"  void AchievementDescription_set_id_m2215766207 (AchievementDescription_t2116066607 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CidU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_title()
extern "C"  String_t* AchievementDescription_get_title_m1294089737 (AchievementDescription_t2116066607 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_Title_0();
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_achievedDescription()
extern "C"  String_t* AchievementDescription_get_achievedDescription_m4248956218 (AchievementDescription_t2116066607 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_AchievedDescription_2();
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_unachievedDescription()
extern "C"  String_t* AchievementDescription_get_unachievedDescription_m3429874753 (AchievementDescription_t2116066607 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_UnachievedDescription_3();
		return L_0;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_hidden()
extern "C"  bool AchievementDescription_get_hidden_m734162136 (AchievementDescription_t2116066607 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_Hidden_4();
		return L_0;
	}
}
// System.Int32 UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_points()
extern "C"  int32_t AchievementDescription_get_points_m4158769271 (AchievementDescription_t2116066607 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Points_5();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::.ctor()
extern Il2CppClass* Score_t3396031228_il2cpp_TypeInfo_var;
extern Il2CppClass* ScoreU5BU5D_t2926278037_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3624438231;
extern const uint32_t Leaderboard__ctor_m596857571_MetadataUsageId;
extern "C"  void Leaderboard__ctor_m596857571 (Leaderboard_t1185876199 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Leaderboard__ctor_m596857571_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Leaderboard_set_id_m2022535593(__this, _stringLiteral3624438231, /*hidden argument*/NULL);
		Range_t1533311935  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Range__ctor_m872630735(&L_0, 1, ((int32_t)10), /*hidden argument*/NULL);
		Leaderboard_set_range_m2830170470(__this, L_0, /*hidden argument*/NULL);
		Leaderboard_set_userScope_m3914830286(__this, 0, /*hidden argument*/NULL);
		Leaderboard_set_timeScope_m3669793618(__this, 2, /*hidden argument*/NULL);
		__this->set_m_Loading_0((bool)0);
		Score_t3396031228 * L_1 = (Score_t3396031228 *)il2cpp_codegen_object_new(Score_t3396031228_il2cpp_TypeInfo_var);
		Score__ctor_m113497156(L_1, _stringLiteral3624438231, (((int64_t)((int64_t)0))), /*hidden argument*/NULL);
		__this->set_m_LocalUserScore_1(L_1);
		__this->set_m_MaxRange_2(0);
		__this->set_m_Scores_3((IScoreU5BU5D_t250104726*)((ScoreU5BU5D_t2926278037*)SZArrayNew(ScoreU5BU5D_t2926278037_il2cpp_TypeInfo_var, (uint32_t)0)));
		__this->set_m_Title_4(_stringLiteral3624438231);
		__this->set_m_UserIDs_5(((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)0)));
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t24667981_il2cpp_TypeInfo_var;
extern Il2CppClass* UserScope_t1608660171_il2cpp_TypeInfo_var;
extern Il2CppClass* TimeScope_t1305796361_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral69499590;
extern Il2CppCodeGenString* _stringLiteral1411513442;
extern Il2CppCodeGenString* _stringLiteral1472989374;
extern Il2CppCodeGenString* _stringLiteral3534372753;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral2271991173;
extern Il2CppCodeGenString* _stringLiteral778717479;
extern Il2CppCodeGenString* _stringLiteral4163059537;
extern Il2CppCodeGenString* _stringLiteral978617427;
extern Il2CppCodeGenString* _stringLiteral1548524389;
extern const uint32_t Leaderboard_ToString_m114482384_MetadataUsageId;
extern "C"  String_t* Leaderboard_ToString_m114482384 (Leaderboard_t1185876199 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Leaderboard_ToString_m114482384_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Range_t1533311935  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Range_t1533311935  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)20)));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral69499590);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral69499590);
		ObjectU5BU5D_t1108656482* L_1 = L_0;
		String_t* L_2 = Leaderboard_get_id_m2379239336(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_2);
		ObjectU5BU5D_t1108656482* L_3 = L_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, _stringLiteral1411513442);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral1411513442);
		ObjectU5BU5D_t1108656482* L_4 = L_3;
		String_t* L_5 = __this->get_m_Title_4();
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 3);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_5);
		ObjectU5BU5D_t1108656482* L_6 = L_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, _stringLiteral1472989374);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral1472989374);
		ObjectU5BU5D_t1108656482* L_7 = L_6;
		bool L_8 = __this->get_m_Loading_0();
		bool L_9 = L_8;
		Il2CppObject * L_10 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 5);
		ArrayElementTypeCheck (L_7, L_10);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_10);
		ObjectU5BU5D_t1108656482* L_11 = L_7;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 6);
		ArrayElementTypeCheck (L_11, _stringLiteral3534372753);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral3534372753);
		ObjectU5BU5D_t1108656482* L_12 = L_11;
		Range_t1533311935  L_13 = Leaderboard_get_range_m234965965(__this, /*hidden argument*/NULL);
		V_0 = L_13;
		int32_t L_14 = (&V_0)->get_from_0();
		int32_t L_15 = L_14;
		Il2CppObject * L_16 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 7);
		ArrayElementTypeCheck (L_12, L_16);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)L_16);
		ObjectU5BU5D_t1108656482* L_17 = L_12;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 8);
		ArrayElementTypeCheck (L_17, _stringLiteral44);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral44);
		ObjectU5BU5D_t1108656482* L_18 = L_17;
		Range_t1533311935  L_19 = Leaderboard_get_range_m234965965(__this, /*hidden argument*/NULL);
		V_1 = L_19;
		int32_t L_20 = (&V_1)->get_count_1();
		int32_t L_21 = L_20;
		Il2CppObject * L_22 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_21);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)9));
		ArrayElementTypeCheck (L_18, L_22);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)L_22);
		ObjectU5BU5D_t1108656482* L_23 = L_18;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, ((int32_t)10));
		ArrayElementTypeCheck (L_23, _stringLiteral2271991173);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Il2CppObject *)_stringLiteral2271991173);
		ObjectU5BU5D_t1108656482* L_24 = L_23;
		uint32_t L_25 = __this->get_m_MaxRange_2();
		uint32_t L_26 = L_25;
		Il2CppObject * L_27 = Box(UInt32_t24667981_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, ((int32_t)11));
		ArrayElementTypeCheck (L_24, L_27);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (Il2CppObject *)L_27);
		ObjectU5BU5D_t1108656482* L_28 = L_24;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, ((int32_t)12));
		ArrayElementTypeCheck (L_28, _stringLiteral778717479);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (Il2CppObject *)_stringLiteral778717479);
		ObjectU5BU5D_t1108656482* L_29 = L_28;
		IScoreU5BU5D_t250104726* L_30 = __this->get_m_Scores_3();
		NullCheck(L_30);
		int32_t L_31 = (((int32_t)((int32_t)(((Il2CppArray *)L_30)->max_length))));
		Il2CppObject * L_32 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_31);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, ((int32_t)13));
		ArrayElementTypeCheck (L_29, L_32);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (Il2CppObject *)L_32);
		ObjectU5BU5D_t1108656482* L_33 = L_29;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, ((int32_t)14));
		ArrayElementTypeCheck (L_33, _stringLiteral4163059537);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (Il2CppObject *)_stringLiteral4163059537);
		ObjectU5BU5D_t1108656482* L_34 = L_33;
		int32_t L_35 = Leaderboard_get_userScope_m3547770469(__this, /*hidden argument*/NULL);
		int32_t L_36 = L_35;
		Il2CppObject * L_37 = Box(UserScope_t1608660171_il2cpp_TypeInfo_var, &L_36);
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, ((int32_t)15));
		ArrayElementTypeCheck (L_34, L_37);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (Il2CppObject *)L_37);
		ObjectU5BU5D_t1108656482* L_38 = L_34;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, ((int32_t)16));
		ArrayElementTypeCheck (L_38, _stringLiteral978617427);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (Il2CppObject *)_stringLiteral978617427);
		ObjectU5BU5D_t1108656482* L_39 = L_38;
		int32_t L_40 = Leaderboard_get_timeScope_m2356081377(__this, /*hidden argument*/NULL);
		int32_t L_41 = L_40;
		Il2CppObject * L_42 = Box(TimeScope_t1305796361_il2cpp_TypeInfo_var, &L_41);
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, ((int32_t)17));
		ArrayElementTypeCheck (L_39, L_42);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (Il2CppObject *)L_42);
		ObjectU5BU5D_t1108656482* L_43 = L_39;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, ((int32_t)18));
		ArrayElementTypeCheck (L_43, _stringLiteral1548524389);
		(L_43)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (Il2CppObject *)_stringLiteral1548524389);
		ObjectU5BU5D_t1108656482* L_44 = L_43;
		StringU5BU5D_t4054002952* L_45 = __this->get_m_UserIDs_5();
		NullCheck(L_45);
		int32_t L_46 = (((int32_t)((int32_t)(((Il2CppArray *)L_45)->max_length))));
		Il2CppObject * L_47 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_46);
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)19));
		ArrayElementTypeCheck (L_44, L_47);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)19)), (Il2CppObject *)L_47);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_48 = String_Concat_m3016520001(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		return L_48;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetLocalUserScore(UnityEngine.SocialPlatforms.IScore)
extern "C"  void Leaderboard_SetLocalUserScore_m700491556 (Leaderboard_t1185876199 * __this, Il2CppObject * ___score0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___score0;
		__this->set_m_LocalUserScore_1(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetMaxRange(System.UInt32)
extern "C"  void Leaderboard_SetMaxRange_m3779908734 (Leaderboard_t1185876199 * __this, uint32_t ___maxRange0, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___maxRange0;
		__this->set_m_MaxRange_2(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetScores(UnityEngine.SocialPlatforms.IScore[])
extern "C"  void Leaderboard_SetScores_m1463319879 (Leaderboard_t1185876199 * __this, IScoreU5BU5D_t250104726* ___scores0, const MethodInfo* method)
{
	{
		IScoreU5BU5D_t250104726* L_0 = ___scores0;
		__this->set_m_Scores_3(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetTitle(System.String)
extern "C"  void Leaderboard_SetTitle_m771163339 (Leaderboard_t1185876199 * __this, String_t* ___title0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___title0;
		__this->set_m_Title_4(L_0);
		return;
	}
}
// System.String[] UnityEngine.SocialPlatforms.Impl.Leaderboard::GetUserFilter()
extern "C"  StringU5BU5D_t4054002952* Leaderboard_GetUserFilter_m3119905721 (Leaderboard_t1185876199 * __this, const MethodInfo* method)
{
	{
		StringU5BU5D_t4054002952* L_0 = __this->get_m_UserIDs_5();
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::get_id()
extern "C"  String_t* Leaderboard_get_id_m2379239336 (Leaderboard_t1185876199 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CidU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_id(System.String)
extern "C"  void Leaderboard_set_id_m2022535593 (Leaderboard_t1185876199 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CidU3Ek__BackingField_6(L_0);
		return;
	}
}
// UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_userScope()
extern "C"  int32_t Leaderboard_get_userScope_m3547770469 (Leaderboard_t1185876199 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CuserScopeU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_userScope(UnityEngine.SocialPlatforms.UserScope)
extern "C"  void Leaderboard_set_userScope_m3914830286 (Leaderboard_t1185876199 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CuserScopeU3Ek__BackingField_7(L_0);
		return;
	}
}
// UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.Impl.Leaderboard::get_range()
extern "C"  Range_t1533311935  Leaderboard_get_range_m234965965 (Leaderboard_t1185876199 * __this, const MethodInfo* method)
{
	{
		Range_t1533311935  L_0 = __this->get_U3CrangeU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_range(UnityEngine.SocialPlatforms.Range)
extern "C"  void Leaderboard_set_range_m2830170470 (Leaderboard_t1185876199 * __this, Range_t1533311935  ___value0, const MethodInfo* method)
{
	{
		Range_t1533311935  L_0 = ___value0;
		__this->set_U3CrangeU3Ek__BackingField_8(L_0);
		return;
	}
}
// UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_timeScope()
extern "C"  int32_t Leaderboard_get_timeScope_m2356081377 (Leaderboard_t1185876199 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CtimeScopeU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_timeScope(UnityEngine.SocialPlatforms.TimeScope)
extern "C"  void Leaderboard_set_timeScope_m3669793618 (Leaderboard_t1185876199 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CtimeScopeU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::.ctor()
extern Il2CppClass* UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var;
extern const uint32_t LocalUser__ctor_m1052633066_MetadataUsageId;
extern "C"  void LocalUser__ctor_m1052633066 (LocalUser_t1307362368 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LocalUser__ctor_m1052633066_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UserProfile__ctor_m1280449570(__this, /*hidden argument*/NULL);
		__this->set_m_Friends_5((IUserProfileU5BU5D_t3419104218*)((UserProfileU5BU5D_t2378268441*)SZArrayNew(UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var, (uint32_t)0)));
		__this->set_m_Authenticated_6((bool)0);
		__this->set_m_Underage_7((bool)0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetFriends(UnityEngine.SocialPlatforms.IUserProfile[])
extern "C"  void LocalUser_SetFriends_m3475409220 (LocalUser_t1307362368 * __this, IUserProfileU5BU5D_t3419104218* ___friends0, const MethodInfo* method)
{
	{
		IUserProfileU5BU5D_t3419104218* L_0 = ___friends0;
		__this->set_m_Friends_5(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetAuthenticated(System.Boolean)
extern "C"  void LocalUser_SetAuthenticated_m653377406 (LocalUser_t1307362368 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_Authenticated_6(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetUnderage(System.Boolean)
extern "C"  void LocalUser_SetUnderage_m2968368872 (LocalUser_t1307362368 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_Underage_7(L_0);
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.LocalUser::get_authenticated()
extern "C"  bool LocalUser_get_authenticated_m3657159816 (LocalUser_t1307362368 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_Authenticated_6();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Score::.ctor(System.String,System.Int64)
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral48;
extern const uint32_t Score__ctor_m113497156_MetadataUsageId;
extern "C"  void Score__ctor_m113497156 (Score_t3396031228 * __this, String_t* ___leaderboardID0, int64_t ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Score__ctor_m113497156_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___leaderboardID0;
		int64_t L_1 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t4283661327_il2cpp_TypeInfo_var);
		DateTime_t4283661327  L_2 = DateTime_get_Now_m1812131422(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		Score__ctor_m3768037481(__this, L_0, L_1, _stringLiteral48, L_2, L_3, (-1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Score::.ctor(System.String,System.Int64,System.String,System.DateTime,System.String,System.Int32)
extern "C"  void Score__ctor_m3768037481 (Score_t3396031228 * __this, String_t* ___leaderboardID0, int64_t ___value1, String_t* ___userID2, DateTime_t4283661327  ___date3, String_t* ___formattedValue4, int32_t ___rank5, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___leaderboardID0;
		Score_set_leaderboardID_m3646875387(__this, L_0, /*hidden argument*/NULL);
		int64_t L_1 = ___value1;
		Score_set_value_m2229956626(__this, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___userID2;
		__this->set_m_UserID_2(L_2);
		DateTime_t4283661327  L_3 = ___date3;
		__this->set_m_Date_0(L_3);
		String_t* L_4 = ___formattedValue4;
		__this->set_m_FormattedValue_1(L_4);
		int32_t L_5 = ___rank5;
		__this->set_m_Rank_3(L_5);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Score::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t1153838595_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2642717173;
extern Il2CppCodeGenString* _stringLiteral1871350441;
extern Il2CppCodeGenString* _stringLiteral188508522;
extern Il2CppCodeGenString* _stringLiteral3481064940;
extern Il2CppCodeGenString* _stringLiteral1272038458;
extern const uint32_t Score_ToString_m3380639973_MetadataUsageId;
extern "C"  String_t* Score_ToString_m3380639973 (Score_t3396031228 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Score_ToString_m3380639973_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10)));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral2642717173);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2642717173);
		ObjectU5BU5D_t1108656482* L_1 = L_0;
		int32_t L_2 = __this->get_m_Rank_3();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t1108656482* L_5 = L_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, _stringLiteral1871350441);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral1871350441);
		ObjectU5BU5D_t1108656482* L_6 = L_5;
		int64_t L_7 = Score_get_value_m3381234835(__this, /*hidden argument*/NULL);
		int64_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int64_t1153838595_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_9);
		ObjectU5BU5D_t1108656482* L_10 = L_6;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 4);
		ArrayElementTypeCheck (L_10, _stringLiteral188508522);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral188508522);
		ObjectU5BU5D_t1108656482* L_11 = L_10;
		String_t* L_12 = Score_get_leaderboardID_m1097777176(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 5);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_12);
		ObjectU5BU5D_t1108656482* L_13 = L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 6);
		ArrayElementTypeCheck (L_13, _stringLiteral3481064940);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral3481064940);
		ObjectU5BU5D_t1108656482* L_14 = L_13;
		String_t* L_15 = __this->get_m_UserID_2();
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 7);
		ArrayElementTypeCheck (L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)L_15);
		ObjectU5BU5D_t1108656482* L_16 = L_14;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 8);
		ArrayElementTypeCheck (L_16, _stringLiteral1272038458);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral1272038458);
		ObjectU5BU5D_t1108656482* L_17 = L_16;
		DateTime_t4283661327  L_18 = __this->get_m_Date_0();
		DateTime_t4283661327  L_19 = L_18;
		Il2CppObject * L_20 = Box(DateTime_t4283661327_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, ((int32_t)9));
		ArrayElementTypeCheck (L_17, L_20);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)L_20);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m3016520001(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		return L_21;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Score::get_leaderboardID()
extern "C"  String_t* Score_get_leaderboardID_m1097777176 (Score_t3396031228 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CleaderboardIDU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Score::set_leaderboardID(System.String)
extern "C"  void Score_set_leaderboardID_m3646875387 (Score_t3396031228 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CleaderboardIDU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Int64 UnityEngine.SocialPlatforms.Impl.Score::get_value()
extern "C"  int64_t Score_get_value_m3381234835 (Score_t3396031228 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = __this->get_U3CvalueU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Score::set_value(System.Int64)
extern "C"  void Score_set_value_m2229956626 (Score_t3396031228 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		int64_t L_0 = ___value0;
		__this->set_U3CvalueU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::.ctor()
extern Il2CppClass* Texture2D_t3884108195_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3378765435;
extern Il2CppCodeGenString* _stringLiteral48;
extern const uint32_t UserProfile__ctor_m1280449570_MetadataUsageId;
extern "C"  void UserProfile__ctor_m1280449570 (UserProfile_t2280656072 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UserProfile__ctor_m1280449570_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		__this->set_m_UserName_0(_stringLiteral3378765435);
		__this->set_m_ID_1(_stringLiteral48);
		__this->set_m_IsFriend_2((bool)0);
		__this->set_m_State_3(3);
		Texture2D_t3884108195 * L_0 = (Texture2D_t3884108195 *)il2cpp_codegen_object_new(Texture2D_t3884108195_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1883511258(L_0, ((int32_t)32), ((int32_t)32), /*hidden argument*/NULL);
		__this->set_m_Image_4(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::.ctor(System.String,System.String,System.Boolean,UnityEngine.SocialPlatforms.UserState,UnityEngine.Texture2D)
extern "C"  void UserProfile__ctor_m2682768015 (UserProfile_t2280656072 * __this, String_t* ___name0, String_t* ___id1, bool ___friend2, int32_t ___state3, Texture2D_t3884108195 * ___image4, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set_m_UserName_0(L_0);
		String_t* L_1 = ___id1;
		__this->set_m_ID_1(L_1);
		bool L_2 = ___friend2;
		__this->set_m_IsFriend_2(L_2);
		int32_t L_3 = ___state3;
		__this->set_m_State_3(L_3);
		Texture2D_t3884108195 * L_4 = ___image4;
		__this->set_m_Image_4(L_4);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* UserState_t1609153288_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral32179;
extern const uint32_t UserProfile_ToString_m2563774257_MetadataUsageId;
extern "C"  String_t* UserProfile_ToString_m2563774257 (UserProfile_t2280656072 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UserProfile_ToString_m2563774257_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)7));
		String_t* L_1 = UserProfile_get_id_m2095754825(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		ObjectU5BU5D_t1108656482* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, _stringLiteral32179);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_3 = L_2;
		String_t* L_4 = UserProfile_get_userName_m3149753764(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_4);
		ObjectU5BU5D_t1108656482* L_5 = L_3;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, _stringLiteral32179);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_6 = L_5;
		bool L_7 = UserProfile_get_isFriend_m1712941209(__this, /*hidden argument*/NULL);
		bool L_8 = L_7;
		Il2CppObject * L_9 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_9);
		ObjectU5BU5D_t1108656482* L_10 = L_6;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 5);
		ArrayElementTypeCheck (L_10, _stringLiteral32179);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_11 = L_10;
		int32_t L_12 = UserProfile_get_state_m1340538601(__this, /*hidden argument*/NULL);
		int32_t L_13 = L_12;
		Il2CppObject * L_14 = Box(UserState_t1609153288_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 6);
		ArrayElementTypeCheck (L_11, L_14);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)L_14);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m3016520001(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		return L_15;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetUserName(System.String)
extern "C"  void UserProfile_SetUserName_m914181770 (UserProfile_t2280656072 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		__this->set_m_UserName_0(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetUserID(System.String)
extern "C"  void UserProfile_SetUserID_m1515238170 (UserProfile_t2280656072 * __this, String_t* ___id0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___id0;
		__this->set_m_ID_1(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetImage(UnityEngine.Texture2D)
extern "C"  void UserProfile_SetImage_m1928130753 (UserProfile_t2280656072 * __this, Texture2D_t3884108195 * ___image0, const MethodInfo* method)
{
	{
		Texture2D_t3884108195 * L_0 = ___image0;
		__this->set_m_Image_4(L_0);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_userName()
extern "C"  String_t* UserProfile_get_userName_m3149753764 (UserProfile_t2280656072 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_UserName_0();
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_id()
extern "C"  String_t* UserProfile_get_id_m2095754825 (UserProfile_t2280656072 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_ID_1();
		return L_0;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.UserProfile::get_isFriend()
extern "C"  bool UserProfile_get_isFriend_m1712941209 (UserProfile_t2280656072 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_IsFriend_2();
		return L_0;
	}
}
// UnityEngine.SocialPlatforms.UserState UnityEngine.SocialPlatforms.Impl.UserProfile::get_state()
extern "C"  int32_t UserProfile_get_state_m1340538601 (UserProfile_t2280656072 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_State_3();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Range::.ctor(System.Int32,System.Int32)
extern "C"  void Range__ctor_m872630735 (Range_t1533311935 * __this, int32_t ___fromValue0, int32_t ___valueCount1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___fromValue0;
		__this->set_from_0(L_0);
		int32_t L_1 = ___valueCount1;
		__this->set_count_1(L_1);
		return;
	}
}
extern "C"  void Range__ctor_m872630735_AdjustorThunk (Il2CppObject * __this, int32_t ___fromValue0, int32_t ___valueCount1, const MethodInfo* method)
{
	Range_t1533311935 * _thisAdjusted = reinterpret_cast<Range_t1533311935 *>(__this + 1);
	Range__ctor_m872630735(_thisAdjusted, ___fromValue0, ___valueCount1, method);
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.Range
extern "C" void Range_t1533311935_marshal_pinvoke(const Range_t1533311935& unmarshaled, Range_t1533311935_marshaled_pinvoke& marshaled)
{
	marshaled.___from_0 = unmarshaled.get_from_0();
	marshaled.___count_1 = unmarshaled.get_count_1();
}
extern "C" void Range_t1533311935_marshal_pinvoke_back(const Range_t1533311935_marshaled_pinvoke& marshaled, Range_t1533311935& unmarshaled)
{
	int32_t unmarshaled_from_temp_0 = 0;
	unmarshaled_from_temp_0 = marshaled.___from_0;
	unmarshaled.set_from_0(unmarshaled_from_temp_0);
	int32_t unmarshaled_count_temp_1 = 0;
	unmarshaled_count_temp_1 = marshaled.___count_1;
	unmarshaled.set_count_1(unmarshaled_count_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.Range
extern "C" void Range_t1533311935_marshal_pinvoke_cleanup(Range_t1533311935_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.Range
extern "C" void Range_t1533311935_marshal_com(const Range_t1533311935& unmarshaled, Range_t1533311935_marshaled_com& marshaled)
{
	marshaled.___from_0 = unmarshaled.get_from_0();
	marshaled.___count_1 = unmarshaled.get_count_1();
}
extern "C" void Range_t1533311935_marshal_com_back(const Range_t1533311935_marshaled_com& marshaled, Range_t1533311935& unmarshaled)
{
	int32_t unmarshaled_from_temp_0 = 0;
	unmarshaled_from_temp_0 = marshaled.___from_0;
	unmarshaled.set_from_0(unmarshaled_from_temp_0);
	int32_t unmarshaled_count_temp_1 = 0;
	unmarshaled_count_temp_1 = marshaled.___count_1;
	unmarshaled.set_count_1(unmarshaled_count_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.Range
extern "C" void Range_t1533311935_marshal_com_cleanup(Range_t1533311935_marshaled_com& marshaled)
{
}
// System.Int32 UnityEngine.SortingLayer::GetLayerValueFromID(System.Int32)
extern "C"  int32_t SortingLayer_GetLayerValueFromID_m2560954442 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method)
{
	typedef int32_t (*SortingLayer_GetLayerValueFromID_m2560954442_ftn) (int32_t);
	static SortingLayer_GetLayerValueFromID_m2560954442_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SortingLayer_GetLayerValueFromID_m2560954442_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SortingLayer::GetLayerValueFromID(System.Int32)");
	return _il2cpp_icall_func(___id0);
}
// Conversion methods for marshalling of: UnityEngine.SortingLayer
extern "C" void SortingLayer_t3376264497_marshal_pinvoke(const SortingLayer_t3376264497& unmarshaled, SortingLayer_t3376264497_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Id_0 = unmarshaled.get_m_Id_0();
}
extern "C" void SortingLayer_t3376264497_marshal_pinvoke_back(const SortingLayer_t3376264497_marshaled_pinvoke& marshaled, SortingLayer_t3376264497& unmarshaled)
{
	int32_t unmarshaled_m_Id_temp_0 = 0;
	unmarshaled_m_Id_temp_0 = marshaled.___m_Id_0;
	unmarshaled.set_m_Id_0(unmarshaled_m_Id_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.SortingLayer
extern "C" void SortingLayer_t3376264497_marshal_pinvoke_cleanup(SortingLayer_t3376264497_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SortingLayer
extern "C" void SortingLayer_t3376264497_marshal_com(const SortingLayer_t3376264497& unmarshaled, SortingLayer_t3376264497_marshaled_com& marshaled)
{
	marshaled.___m_Id_0 = unmarshaled.get_m_Id_0();
}
extern "C" void SortingLayer_t3376264497_marshal_com_back(const SortingLayer_t3376264497_marshaled_com& marshaled, SortingLayer_t3376264497& unmarshaled)
{
	int32_t unmarshaled_m_Id_temp_0 = 0;
	unmarshaled_m_Id_temp_0 = marshaled.___m_Id_0;
	unmarshaled.set_m_Id_0(unmarshaled_m_Id_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.SortingLayer
extern "C" void SortingLayer_t3376264497_marshal_com_cleanup(SortingLayer_t3376264497_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.SpaceAttribute::.ctor()
extern "C"  void SpaceAttribute__ctor_m3636842103 (SpaceAttribute_t1515135354 * __this, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m1741701746(__this, /*hidden argument*/NULL);
		__this->set_height_0((8.0f));
		return;
	}
}
// System.Void UnityEngine.SpaceAttribute::.ctor(System.Single)
extern "C"  void SpaceAttribute__ctor_m2299833492 (SpaceAttribute_t1515135354 * __this, float ___height0, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m1741701746(__this, /*hidden argument*/NULL);
		float L_0 = ___height0;
		__this->set_height_0(L_0);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.SphereCollider::get_center()
extern "C"  Vector3_t4282066566  SphereCollider_get_center_m2231335056 (SphereCollider_t111527973 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		SphereCollider_INTERNAL_get_center_m848738269(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.SphereCollider::set_center(UnityEngine.Vector3)
extern "C"  void SphereCollider_set_center_m453923599 (SphereCollider_t111527973 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		SphereCollider_INTERNAL_set_center_m2497295953(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SphereCollider::INTERNAL_get_center(UnityEngine.Vector3&)
extern "C"  void SphereCollider_INTERNAL_get_center_m848738269 (SphereCollider_t111527973 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*SphereCollider_INTERNAL_get_center_m848738269_ftn) (SphereCollider_t111527973 *, Vector3_t4282066566 *);
	static SphereCollider_INTERNAL_get_center_m848738269_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SphereCollider_INTERNAL_get_center_m848738269_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SphereCollider::INTERNAL_get_center(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.SphereCollider::INTERNAL_set_center(UnityEngine.Vector3&)
extern "C"  void SphereCollider_INTERNAL_set_center_m2497295953 (SphereCollider_t111527973 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*SphereCollider_INTERNAL_set_center_m2497295953_ftn) (SphereCollider_t111527973 *, Vector3_t4282066566 *);
	static SphereCollider_INTERNAL_set_center_m2497295953_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SphereCollider_INTERNAL_set_center_m2497295953_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SphereCollider::INTERNAL_set_center(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.SphereCollider::get_radius()
extern "C"  float SphereCollider_get_radius_m3477839903 (SphereCollider_t111527973 * __this, const MethodInfo* method)
{
	typedef float (*SphereCollider_get_radius_m3477839903_ftn) (SphereCollider_t111527973 *);
	static SphereCollider_get_radius_m3477839903_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SphereCollider_get_radius_m3477839903_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SphereCollider::get_radius()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.SphereCollider::set_radius(System.Single)
extern "C"  void SphereCollider_set_radius_m1893499780 (SphereCollider_t111527973 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*SphereCollider_set_radius_m1893499780_ftn) (SphereCollider_t111527973 *, float);
	static SphereCollider_set_radius_m1893499780_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SphereCollider_set_radius_m1893499780_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SphereCollider::set_radius(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Sprite::.ctor()
extern "C"  void Sprite__ctor_m156427976 (Sprite_t3199167241 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m570634126(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Sprite UnityEngine.Sprite::Create(UnityEngine.Texture2D,UnityEngine.Rect,UnityEngine.Vector2,System.Single)
extern "C"  Sprite_t3199167241 * Sprite_Create_m2242007923 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___texture0, Rect_t4241904616  ___rect1, Vector2_t4282066565  ___pivot2, float ___pixelsPerUnit3, const MethodInfo* method)
{
	Vector4_t4282066567  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	uint32_t V_2 = 0;
	{
		Vector4_t4282066567  L_0 = Vector4_get_zero_m3835647092(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 1;
		V_2 = 0;
		Texture2D_t3884108195 * L_1 = ___texture0;
		float L_2 = ___pixelsPerUnit3;
		uint32_t L_3 = V_2;
		int32_t L_4 = V_1;
		Sprite_t3199167241 * L_5 = Sprite_INTERNAL_CALL_Create_m1601644917(NULL /*static, unused*/, L_1, (&___rect1), (&___pivot2), L_2, L_3, L_4, (&V_0), /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.Sprite UnityEngine.Sprite::Create(UnityEngine.Texture2D,UnityEngine.Rect,UnityEngine.Vector2)
extern "C"  Sprite_t3199167241 * Sprite_Create_m278903054 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___texture0, Rect_t4241904616  ___rect1, Vector2_t4282066565  ___pivot2, const MethodInfo* method)
{
	Vector4_t4282066567  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	uint32_t V_2 = 0;
	float V_3 = 0.0f;
	{
		Vector4_t4282066567  L_0 = Vector4_get_zero_m3835647092(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 1;
		V_2 = 0;
		V_3 = (100.0f);
		Texture2D_t3884108195 * L_1 = ___texture0;
		float L_2 = V_3;
		uint32_t L_3 = V_2;
		int32_t L_4 = V_1;
		Sprite_t3199167241 * L_5 = Sprite_INTERNAL_CALL_Create_m1601644917(NULL /*static, unused*/, L_1, (&___rect1), (&___pivot2), L_2, L_3, L_4, (&V_0), /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.Sprite UnityEngine.Sprite::INTERNAL_CALL_Create(UnityEngine.Texture2D,UnityEngine.Rect&,UnityEngine.Vector2&,System.Single,System.UInt32,UnityEngine.SpriteMeshType,UnityEngine.Vector4&)
extern "C"  Sprite_t3199167241 * Sprite_INTERNAL_CALL_Create_m1601644917 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___texture0, Rect_t4241904616 * ___rect1, Vector2_t4282066565 * ___pivot2, float ___pixelsPerUnit3, uint32_t ___extrude4, int32_t ___meshType5, Vector4_t4282066567 * ___border6, const MethodInfo* method)
{
	typedef Sprite_t3199167241 * (*Sprite_INTERNAL_CALL_Create_m1601644917_ftn) (Texture2D_t3884108195 *, Rect_t4241904616 *, Vector2_t4282066565 *, float, uint32_t, int32_t, Vector4_t4282066567 *);
	static Sprite_INTERNAL_CALL_Create_m1601644917_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_INTERNAL_CALL_Create_m1601644917_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::INTERNAL_CALL_Create(UnityEngine.Texture2D,UnityEngine.Rect&,UnityEngine.Vector2&,System.Single,System.UInt32,UnityEngine.SpriteMeshType,UnityEngine.Vector4&)");
	return _il2cpp_icall_func(___texture0, ___rect1, ___pivot2, ___pixelsPerUnit3, ___extrude4, ___meshType5, ___border6);
}
// UnityEngine.Rect UnityEngine.Sprite::get_rect()
extern "C"  Rect_t4241904616  Sprite_get_rect_m132626493 (Sprite_t3199167241 * __this, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Sprite_INTERNAL_get_rect_m853825042(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t4241904616  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Sprite::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C"  void Sprite_INTERNAL_get_rect_m853825042 (Sprite_t3199167241 * __this, Rect_t4241904616 * ___value0, const MethodInfo* method)
{
	typedef void (*Sprite_INTERNAL_get_rect_m853825042_ftn) (Sprite_t3199167241 *, Rect_t4241904616 *);
	static Sprite_INTERNAL_get_rect_m853825042_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_INTERNAL_get_rect_m853825042_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::INTERNAL_get_rect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Sprite::get_pixelsPerUnit()
extern "C"  float Sprite_get_pixelsPerUnit_m2438708453 (Sprite_t3199167241 * __this, const MethodInfo* method)
{
	typedef float (*Sprite_get_pixelsPerUnit_m2438708453_ftn) (Sprite_t3199167241 *);
	static Sprite_get_pixelsPerUnit_m2438708453_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_get_pixelsPerUnit_m2438708453_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::get_pixelsPerUnit()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Texture2D UnityEngine.Sprite::get_texture()
extern "C"  Texture2D_t3884108195 * Sprite_get_texture_m1481489947 (Sprite_t3199167241 * __this, const MethodInfo* method)
{
	typedef Texture2D_t3884108195 * (*Sprite_get_texture_m1481489947_ftn) (Sprite_t3199167241 *);
	static Sprite_get_texture_m1481489947_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_get_texture_m1481489947_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::get_texture()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Texture2D UnityEngine.Sprite::get_associatedAlphaSplitTexture()
extern "C"  Texture2D_t3884108195 * Sprite_get_associatedAlphaSplitTexture_m4226901177 (Sprite_t3199167241 * __this, const MethodInfo* method)
{
	typedef Texture2D_t3884108195 * (*Sprite_get_associatedAlphaSplitTexture_m4226901177_ftn) (Sprite_t3199167241 *);
	static Sprite_get_associatedAlphaSplitTexture_m4226901177_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_get_associatedAlphaSplitTexture_m4226901177_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::get_associatedAlphaSplitTexture()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Rect UnityEngine.Sprite::get_textureRect()
extern "C"  Rect_t4241904616  Sprite_get_textureRect_m3946160520 (Sprite_t3199167241 * __this, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Sprite_INTERNAL_get_textureRect_m1939414807(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t4241904616  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Sprite::INTERNAL_get_textureRect(UnityEngine.Rect&)
extern "C"  void Sprite_INTERNAL_get_textureRect_m1939414807 (Sprite_t3199167241 * __this, Rect_t4241904616 * ___value0, const MethodInfo* method)
{
	typedef void (*Sprite_INTERNAL_get_textureRect_m1939414807_ftn) (Sprite_t3199167241 *, Rect_t4241904616 *);
	static Sprite_INTERNAL_get_textureRect_m1939414807_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_INTERNAL_get_textureRect_m1939414807_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::INTERNAL_get_textureRect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector4 UnityEngine.Sprite::get_border()
extern "C"  Vector4_t4282066567  Sprite_get_border_m1562752938 (Sprite_t3199167241 * __this, const MethodInfo* method)
{
	Vector4_t4282066567  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Sprite_INTERNAL_get_border_m855941073(__this, (&V_0), /*hidden argument*/NULL);
		Vector4_t4282066567  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Sprite::INTERNAL_get_border(UnityEngine.Vector4&)
extern "C"  void Sprite_INTERNAL_get_border_m855941073 (Sprite_t3199167241 * __this, Vector4_t4282066567 * ___value0, const MethodInfo* method)
{
	typedef void (*Sprite_INTERNAL_get_border_m855941073_ftn) (Sprite_t3199167241 *, Vector4_t4282066567 *);
	static Sprite_INTERNAL_get_border_m855941073_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_INTERNAL_get_border_m855941073_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::INTERNAL_get_border(UnityEngine.Vector4&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetInnerUV(UnityEngine.Sprite)
extern "C"  Vector4_t4282066567  DataUtility_GetInnerUV_m860985145 (Il2CppObject * __this /* static, unused */, Sprite_t3199167241 * ___sprite0, const MethodInfo* method)
{
	Vector4_t4282066567  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Sprite_t3199167241 * L_0 = ___sprite0;
		DataUtility_INTERNAL_CALL_GetInnerUV_m4066645897(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		Vector4_t4282066567  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetInnerUV(UnityEngine.Sprite,UnityEngine.Vector4&)
extern "C"  void DataUtility_INTERNAL_CALL_GetInnerUV_m4066645897 (Il2CppObject * __this /* static, unused */, Sprite_t3199167241 * ___sprite0, Vector4_t4282066567 * ___value1, const MethodInfo* method)
{
	typedef void (*DataUtility_INTERNAL_CALL_GetInnerUV_m4066645897_ftn) (Sprite_t3199167241 *, Vector4_t4282066567 *);
	static DataUtility_INTERNAL_CALL_GetInnerUV_m4066645897_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DataUtility_INTERNAL_CALL_GetInnerUV_m4066645897_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetInnerUV(UnityEngine.Sprite,UnityEngine.Vector4&)");
	_il2cpp_icall_func(___sprite0, ___value1);
}
// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetOuterUV(UnityEngine.Sprite)
extern "C"  Vector4_t4282066567  DataUtility_GetOuterUV_m2974486430 (Il2CppObject * __this /* static, unused */, Sprite_t3199167241 * ___sprite0, const MethodInfo* method)
{
	Vector4_t4282066567  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Sprite_t3199167241 * L_0 = ___sprite0;
		DataUtility_INTERNAL_CALL_GetOuterUV_m3695722564(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		Vector4_t4282066567  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetOuterUV(UnityEngine.Sprite,UnityEngine.Vector4&)
extern "C"  void DataUtility_INTERNAL_CALL_GetOuterUV_m3695722564 (Il2CppObject * __this /* static, unused */, Sprite_t3199167241 * ___sprite0, Vector4_t4282066567 * ___value1, const MethodInfo* method)
{
	typedef void (*DataUtility_INTERNAL_CALL_GetOuterUV_m3695722564_ftn) (Sprite_t3199167241 *, Vector4_t4282066567 *);
	static DataUtility_INTERNAL_CALL_GetOuterUV_m3695722564_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DataUtility_INTERNAL_CALL_GetOuterUV_m3695722564_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetOuterUV(UnityEngine.Sprite,UnityEngine.Vector4&)");
	_il2cpp_icall_func(___sprite0, ___value1);
}
// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetPadding(UnityEngine.Sprite)
extern "C"  Vector4_t4282066567  DataUtility_GetPadding_m3826542291 (Il2CppObject * __this /* static, unused */, Sprite_t3199167241 * ___sprite0, const MethodInfo* method)
{
	Vector4_t4282066567  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Sprite_t3199167241 * L_0 = ___sprite0;
		DataUtility_INTERNAL_CALL_GetPadding_m3174755375(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		Vector4_t4282066567  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetPadding(UnityEngine.Sprite,UnityEngine.Vector4&)
extern "C"  void DataUtility_INTERNAL_CALL_GetPadding_m3174755375 (Il2CppObject * __this /* static, unused */, Sprite_t3199167241 * ___sprite0, Vector4_t4282066567 * ___value1, const MethodInfo* method)
{
	typedef void (*DataUtility_INTERNAL_CALL_GetPadding_m3174755375_ftn) (Sprite_t3199167241 *, Vector4_t4282066567 *);
	static DataUtility_INTERNAL_CALL_GetPadding_m3174755375_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DataUtility_INTERNAL_CALL_GetPadding_m3174755375_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetPadding(UnityEngine.Sprite,UnityEngine.Vector4&)");
	_il2cpp_icall_func(___sprite0, ___value1);
}
// UnityEngine.Vector2 UnityEngine.Sprites.DataUtility::GetMinSize(UnityEngine.Sprite)
extern "C"  Vector2_t4282066565  DataUtility_GetMinSize_m1313355571 (Il2CppObject * __this /* static, unused */, Sprite_t3199167241 * ___sprite0, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Sprite_t3199167241 * L_0 = ___sprite0;
		DataUtility_Internal_GetMinSize_m885438560(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Sprites.DataUtility::Internal_GetMinSize(UnityEngine.Sprite,UnityEngine.Vector2&)
extern "C"  void DataUtility_Internal_GetMinSize_m885438560 (Il2CppObject * __this /* static, unused */, Sprite_t3199167241 * ___sprite0, Vector2_t4282066565 * ___output1, const MethodInfo* method)
{
	typedef void (*DataUtility_Internal_GetMinSize_m885438560_ftn) (Sprite_t3199167241 *, Vector2_t4282066565 *);
	static DataUtility_Internal_GetMinSize_m885438560_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DataUtility_Internal_GetMinSize_m885438560_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprites.DataUtility::Internal_GetMinSize(UnityEngine.Sprite,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___sprite0, ___output1);
}
// System.Void UnityEngine.StackTraceUtility::.cctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StackTraceUtility_t4217621253_il2cpp_TypeInfo_var;
extern const uint32_t StackTraceUtility__cctor_m1486031939_MetadataUsageId;
extern "C"  void StackTraceUtility__cctor_m1486031939 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility__cctor_m1486031939_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		((StackTraceUtility_t4217621253_StaticFields*)StackTraceUtility_t4217621253_il2cpp_TypeInfo_var->static_fields)->set_projectFolder_0(L_0);
		return;
	}
}
// System.Void UnityEngine.StackTraceUtility::SetProjectFolder(System.String)
extern Il2CppClass* StackTraceUtility_t4217621253_il2cpp_TypeInfo_var;
extern const uint32_t StackTraceUtility_SetProjectFolder_m3541316899_MetadataUsageId;
extern "C"  void StackTraceUtility_SetProjectFolder_m3541316899 (Il2CppObject * __this /* static, unused */, String_t* ___folder0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_SetProjectFolder_m3541316899_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___folder0;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t4217621253_il2cpp_TypeInfo_var);
		((StackTraceUtility_t4217621253_StaticFields*)StackTraceUtility_t4217621253_il2cpp_TypeInfo_var->static_fields)->set_projectFolder_0(L_0);
		return;
	}
}
// System.String UnityEngine.StackTraceUtility::ExtractStackTrace()
extern Il2CppClass* StackTrace_t1047871261_il2cpp_TypeInfo_var;
extern Il2CppClass* StackTraceUtility_t4217621253_il2cpp_TypeInfo_var;
extern const uint32_t StackTraceUtility_ExtractStackTrace_m235366505_MetadataUsageId;
extern "C"  String_t* StackTraceUtility_ExtractStackTrace_m235366505 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_ExtractStackTrace_m235366505_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StackTrace_t1047871261 * V_0 = NULL;
	String_t* V_1 = NULL;
	{
		StackTrace_t1047871261 * L_0 = (StackTrace_t1047871261 *)il2cpp_codegen_object_new(StackTrace_t1047871261_il2cpp_TypeInfo_var);
		StackTrace__ctor_m449371190(L_0, 1, (bool)1, /*hidden argument*/NULL);
		V_0 = L_0;
		StackTrace_t1047871261 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t4217621253_il2cpp_TypeInfo_var);
		String_t* L_2 = StackTraceUtility_ExtractFormattedStackTrace_m3996939365(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = String_ToString_m1382284457(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		String_t* L_4 = V_1;
		return L_4;
	}
}
// System.Boolean UnityEngine.StackTraceUtility::IsSystemStacktraceType(System.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4118889292;
extern Il2CppCodeGenString* _stringLiteral108037399;
extern Il2CppCodeGenString* _stringLiteral4222249919;
extern Il2CppCodeGenString* _stringLiteral235002098;
extern Il2CppCodeGenString* _stringLiteral2264296596;
extern Il2CppCodeGenString* _stringLiteral4231872978;
extern const uint32_t StackTraceUtility_IsSystemStacktraceType_m295158192_MetadataUsageId;
extern "C"  bool StackTraceUtility_IsSystemStacktraceType_m295158192 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_IsSystemStacktraceType_m295158192_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___name0;
		V_0 = ((String_t*)CastclassSealed(L_0, String_t_il2cpp_TypeInfo_var));
		String_t* L_1 = V_0;
		NullCheck(L_1);
		bool L_2 = String_StartsWith_m1500793453(L_1, _stringLiteral4118889292, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0064;
		}
	}
	{
		String_t* L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = String_StartsWith_m1500793453(L_3, _stringLiteral108037399, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0064;
		}
	}
	{
		String_t* L_5 = V_0;
		NullCheck(L_5);
		bool L_6 = String_StartsWith_m1500793453(L_5, _stringLiteral4222249919, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0064;
		}
	}
	{
		String_t* L_7 = V_0;
		NullCheck(L_7);
		bool L_8 = String_StartsWith_m1500793453(L_7, _stringLiteral235002098, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0064;
		}
	}
	{
		String_t* L_9 = V_0;
		NullCheck(L_9);
		bool L_10 = String_StartsWith_m1500793453(L_9, _stringLiteral2264296596, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0064;
		}
	}
	{
		String_t* L_11 = V_0;
		NullCheck(L_11);
		bool L_12 = String_StartsWith_m1500793453(L_11, _stringLiteral4231872978, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_12));
		goto IL_0065;
	}

IL_0064:
	{
		G_B7_0 = 1;
	}

IL_0065:
	{
		return (bool)G_B7_0;
	}
}
// System.Void UnityEngine.StackTraceUtility::ExtractStringFromExceptionInternal(System.Object,System.String&,System.String&)
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppClass* StringBuilder_t243639308_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StackTrace_t1047871261_il2cpp_TypeInfo_var;
extern Il2CppClass* StackTraceUtility_t4217621253_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3176792989;
extern Il2CppCodeGenString* _stringLiteral4051978937;
extern Il2CppCodeGenString* _stringLiteral10;
extern Il2CppCodeGenString* _stringLiteral1830;
extern Il2CppCodeGenString* _stringLiteral3343166721;
extern const uint32_t StackTraceUtility_ExtractStringFromExceptionInternal_m803331050_MetadataUsageId;
extern "C"  void StackTraceUtility_ExtractStringFromExceptionInternal_m803331050 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___exceptiono0, String_t** ___message1, String_t** ___stackTrace2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_ExtractStringFromExceptionInternal_m803331050_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * V_0 = NULL;
	StringBuilder_t243639308 * V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	StackTrace_t1047871261 * V_5 = NULL;
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___exceptiono0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentException_t928607144 * L_1 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_1, _stringLiteral3176792989, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = ___exceptiono0;
		V_0 = ((Exception_t3991598821 *)IsInstClass(L_2, Exception_t3991598821_il2cpp_TypeInfo_var));
		Exception_t3991598821 * L_3 = V_0;
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		ArgumentException_t928607144 * L_4 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_4, _stringLiteral4051978937, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0029:
	{
		Exception_t3991598821 * L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_5);
		if (L_6)
		{
			goto IL_003e;
		}
	}
	{
		G_B7_0 = ((int32_t)512);
		goto IL_004b;
	}

IL_003e:
	{
		Exception_t3991598821 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_7);
		NullCheck(L_8);
		int32_t L_9 = String_get_Length_m2979997331(L_8, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)((int32_t)L_9*(int32_t)2));
	}

IL_004b:
	{
		StringBuilder_t243639308 * L_10 = (StringBuilder_t243639308 *)il2cpp_codegen_object_new(StringBuilder_t243639308_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3624398269(L_10, G_B7_0, /*hidden argument*/NULL);
		V_1 = L_10;
		String_t** L_11 = ___message1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		*((Il2CppObject **)(L_11)) = (Il2CppObject *)L_12;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_11), (Il2CppObject *)L_12);
		String_t* L_13 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_2 = L_13;
		goto IL_00ff;
	}

IL_0063:
	{
		String_t* L_14 = V_2;
		NullCheck(L_14);
		int32_t L_15 = String_get_Length_m2979997331(L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_007a;
		}
	}
	{
		Exception_t3991598821 * L_16 = V_0;
		NullCheck(L_16);
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_16);
		V_2 = L_17;
		goto IL_008c;
	}

IL_007a:
	{
		Exception_t3991598821 * L_18 = V_0;
		NullCheck(L_18);
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_18);
		String_t* L_20 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m1825781833(NULL /*static, unused*/, L_19, _stringLiteral10, L_20, /*hidden argument*/NULL);
		V_2 = L_21;
	}

IL_008c:
	{
		Exception_t3991598821 * L_22 = V_0;
		NullCheck(L_22);
		Type_t * L_23 = Exception_GetType_m913902486(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		String_t* L_24 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_23);
		V_3 = L_24;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_4 = L_25;
		Exception_t3991598821 * L_26 = V_0;
		NullCheck(L_26);
		String_t* L_27 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_26);
		if (!L_27)
		{
			goto IL_00b2;
		}
	}
	{
		Exception_t3991598821 * L_28 = V_0;
		NullCheck(L_28);
		String_t* L_29 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_28);
		V_4 = L_29;
	}

IL_00b2:
	{
		String_t* L_30 = V_4;
		NullCheck(L_30);
		String_t* L_31 = String_Trim_m1030489823(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		int32_t L_32 = String_get_Length_m2979997331(L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_00d8;
		}
	}
	{
		String_t* L_33 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_34 = String_Concat_m138640077(NULL /*static, unused*/, L_33, _stringLiteral1830, /*hidden argument*/NULL);
		V_3 = L_34;
		String_t* L_35 = V_3;
		String_t* L_36 = V_4;
		String_t* L_37 = String_Concat_m138640077(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		V_3 = L_37;
	}

IL_00d8:
	{
		String_t** L_38 = ___message1;
		String_t* L_39 = V_3;
		*((Il2CppObject **)(L_38)) = (Il2CppObject *)L_39;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_38), (Il2CppObject *)L_39);
		Exception_t3991598821 * L_40 = V_0;
		NullCheck(L_40);
		Exception_t3991598821 * L_41 = Exception_get_InnerException_m1427945535(L_40, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_00f8;
		}
	}
	{
		String_t* L_42 = V_3;
		String_t* L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_44 = String_Concat_m2933632197(NULL /*static, unused*/, _stringLiteral3343166721, L_42, _stringLiteral10, L_43, /*hidden argument*/NULL);
		V_2 = L_44;
	}

IL_00f8:
	{
		Exception_t3991598821 * L_45 = V_0;
		NullCheck(L_45);
		Exception_t3991598821 * L_46 = Exception_get_InnerException_m1427945535(L_45, /*hidden argument*/NULL);
		V_0 = L_46;
	}

IL_00ff:
	{
		Exception_t3991598821 * L_47 = V_0;
		if (L_47)
		{
			goto IL_0063;
		}
	}
	{
		StringBuilder_t243639308 * L_48 = V_1;
		String_t* L_49 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_50 = String_Concat_m138640077(NULL /*static, unused*/, L_49, _stringLiteral10, /*hidden argument*/NULL);
		NullCheck(L_48);
		StringBuilder_Append_m3898090075(L_48, L_50, /*hidden argument*/NULL);
		StackTrace_t1047871261 * L_51 = (StackTrace_t1047871261 *)il2cpp_codegen_object_new(StackTrace_t1047871261_il2cpp_TypeInfo_var);
		StackTrace__ctor_m449371190(L_51, 1, (bool)1, /*hidden argument*/NULL);
		V_5 = L_51;
		StringBuilder_t243639308 * L_52 = V_1;
		StackTrace_t1047871261 * L_53 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t4217621253_il2cpp_TypeInfo_var);
		String_t* L_54 = StackTraceUtility_ExtractFormattedStackTrace_m3996939365(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		NullCheck(L_52);
		StringBuilder_Append_m3898090075(L_52, L_54, /*hidden argument*/NULL);
		String_t** L_55 = ___stackTrace2;
		StringBuilder_t243639308 * L_56 = V_1;
		NullCheck(L_56);
		String_t* L_57 = StringBuilder_ToString_m350379841(L_56, /*hidden argument*/NULL);
		*((Il2CppObject **)(L_55)) = (Il2CppObject *)L_57;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_55), (Il2CppObject *)L_57);
		return;
	}
}
// System.String UnityEngine.StackTraceUtility::PostprocessStacktrace(System.String,System.Boolean)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t3324145743_il2cpp_TypeInfo_var;
extern Il2CppClass* StringBuilder_t243639308_il2cpp_TypeInfo_var;
extern Il2CppClass* StackTraceUtility_t4217621253_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3162923440;
extern Il2CppCodeGenString* _stringLiteral3880156305;
extern Il2CppCodeGenString* _stringLiteral994875;
extern Il2CppCodeGenString* _stringLiteral1444255587;
extern Il2CppCodeGenString* _stringLiteral3385676414;
extern Il2CppCodeGenString* _stringLiteral2053922246;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral93;
extern Il2CppCodeGenString* _stringLiteral96845;
extern Il2CppCodeGenString* _stringLiteral89059;
extern Il2CppCodeGenString* _stringLiteral1627007362;
extern Il2CppCodeGenString* _stringLiteral30610331;
extern Il2CppCodeGenString* _stringLiteral30841157;
extern Il2CppCodeGenString* _stringLiteral41;
extern Il2CppCodeGenString* _stringLiteral10;
extern const uint32_t StackTraceUtility_PostprocessStacktrace_m556856210_MetadataUsageId;
extern "C"  String_t* StackTraceUtility_PostprocessStacktrace_m556856210 (Il2CppObject * __this /* static, unused */, String_t* ___oldString0, bool ___stripEngineInternalInformation1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_PostprocessStacktrace_m556856210_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t4054002952* V_0 = NULL;
	StringBuilder_t243639308 * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	String_t* V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	{
		String_t* L_0 = ___oldString0;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_1;
	}

IL_000c:
	{
		String_t* L_2 = ___oldString0;
		CharU5BU5D_t3324145743* L_3 = ((CharU5BU5D_t3324145743*)SZArrayNew(CharU5BU5D_t3324145743_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)10));
		NullCheck(L_2);
		StringU5BU5D_t4054002952* L_4 = String_Split_m290179486(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = ___oldString0;
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m2979997331(L_5, /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_7 = (StringBuilder_t243639308 *)il2cpp_codegen_object_new(StringBuilder_t243639308_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3624398269(L_7, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		V_2 = 0;
		goto IL_0040;
	}

IL_0031:
	{
		StringU5BU5D_t4054002952* L_8 = V_0;
		int32_t L_9 = V_2;
		StringU5BU5D_t4054002952* L_10 = V_0;
		int32_t L_11 = V_2;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		String_t* L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck(L_13);
		String_t* L_14 = String_Trim_m1030489823(L_13, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		ArrayElementTypeCheck (L_8, L_14);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (String_t*)L_14);
		int32_t L_15 = V_2;
		V_2 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0040:
	{
		int32_t L_16 = V_2;
		StringU5BU5D_t4054002952* L_17 = V_0;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_17)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		V_3 = 0;
		goto IL_0265;
	}

IL_0050:
	{
		StringU5BU5D_t4054002952* L_18 = V_0;
		int32_t L_19 = V_3;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		int32_t L_20 = L_19;
		String_t* L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		V_4 = L_21;
		String_t* L_22 = V_4;
		NullCheck(L_22);
		int32_t L_23 = String_get_Length_m2979997331(L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0070;
		}
	}
	{
		String_t* L_24 = V_4;
		NullCheck(L_24);
		Il2CppChar L_25 = String_get_Chars_m3015341861(L_24, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_25) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0075;
		}
	}

IL_0070:
	{
		goto IL_0261;
	}

IL_0075:
	{
		String_t* L_26 = V_4;
		NullCheck(L_26);
		bool L_27 = String_StartsWith_m1500793453(L_26, _stringLiteral3162923440, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_008b;
		}
	}
	{
		goto IL_0261;
	}

IL_008b:
	{
		bool L_28 = ___stripEngineInternalInformation1;
		if (!L_28)
		{
			goto IL_00a7;
		}
	}
	{
		String_t* L_29 = V_4;
		NullCheck(L_29);
		bool L_30 = String_StartsWith_m1500793453(L_29, _stringLiteral3880156305, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_00a7;
		}
	}
	{
		goto IL_026e;
	}

IL_00a7:
	{
		bool L_31 = ___stripEngineInternalInformation1;
		if (!L_31)
		{
			goto IL_00fa;
		}
	}
	{
		int32_t L_32 = V_3;
		StringU5BU5D_t4054002952* L_33 = V_0;
		NullCheck(L_33);
		if ((((int32_t)L_32) >= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_33)->max_length))))-(int32_t)1)))))
		{
			goto IL_00fa;
		}
	}
	{
		String_t* L_34 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t4217621253_il2cpp_TypeInfo_var);
		bool L_35 = StackTraceUtility_IsSystemStacktraceType_m295158192(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_00fa;
		}
	}
	{
		StringU5BU5D_t4054002952* L_36 = V_0;
		int32_t L_37 = V_3;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, ((int32_t)((int32_t)L_37+(int32_t)1)));
		int32_t L_38 = ((int32_t)((int32_t)L_37+(int32_t)1));
		String_t* L_39 = (L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_38));
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t4217621253_il2cpp_TypeInfo_var);
		bool L_40 = StackTraceUtility_IsSystemStacktraceType_m295158192(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_00d8;
		}
	}
	{
		goto IL_0261;
	}

IL_00d8:
	{
		String_t* L_41 = V_4;
		NullCheck(L_41);
		int32_t L_42 = String_IndexOf_m1476794331(L_41, _stringLiteral994875, /*hidden argument*/NULL);
		V_5 = L_42;
		int32_t L_43 = V_5;
		if ((((int32_t)L_43) == ((int32_t)(-1))))
		{
			goto IL_00fa;
		}
	}
	{
		String_t* L_44 = V_4;
		int32_t L_45 = V_5;
		NullCheck(L_44);
		String_t* L_46 = String_Substring_m675079568(L_44, 0, L_45, /*hidden argument*/NULL);
		V_4 = L_46;
	}

IL_00fa:
	{
		String_t* L_47 = V_4;
		NullCheck(L_47);
		int32_t L_48 = String_IndexOf_m1476794331(L_47, _stringLiteral1444255587, /*hidden argument*/NULL);
		if ((((int32_t)L_48) == ((int32_t)(-1))))
		{
			goto IL_0111;
		}
	}
	{
		goto IL_0261;
	}

IL_0111:
	{
		String_t* L_49 = V_4;
		NullCheck(L_49);
		int32_t L_50 = String_IndexOf_m1476794331(L_49, _stringLiteral3385676414, /*hidden argument*/NULL);
		if ((((int32_t)L_50) == ((int32_t)(-1))))
		{
			goto IL_0128;
		}
	}
	{
		goto IL_0261;
	}

IL_0128:
	{
		String_t* L_51 = V_4;
		NullCheck(L_51);
		int32_t L_52 = String_IndexOf_m1476794331(L_51, _stringLiteral2053922246, /*hidden argument*/NULL);
		if ((((int32_t)L_52) == ((int32_t)(-1))))
		{
			goto IL_013f;
		}
	}
	{
		goto IL_0261;
	}

IL_013f:
	{
		bool L_53 = ___stripEngineInternalInformation1;
		if (!L_53)
		{
			goto IL_016c;
		}
	}
	{
		String_t* L_54 = V_4;
		NullCheck(L_54);
		bool L_55 = String_StartsWith_m1500793453(L_54, _stringLiteral91, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_016c;
		}
	}
	{
		String_t* L_56 = V_4;
		NullCheck(L_56);
		bool L_57 = String_EndsWith_m2265568550(L_56, _stringLiteral93, /*hidden argument*/NULL);
		if (!L_57)
		{
			goto IL_016c;
		}
	}
	{
		goto IL_0261;
	}

IL_016c:
	{
		String_t* L_58 = V_4;
		NullCheck(L_58);
		bool L_59 = String_StartsWith_m1500793453(L_58, _stringLiteral96845, /*hidden argument*/NULL);
		if (!L_59)
		{
			goto IL_0188;
		}
	}
	{
		String_t* L_60 = V_4;
		NullCheck(L_60);
		String_t* L_61 = String_Remove_m242090629(L_60, 0, 3, /*hidden argument*/NULL);
		V_4 = L_61;
	}

IL_0188:
	{
		String_t* L_62 = V_4;
		NullCheck(L_62);
		int32_t L_63 = String_IndexOf_m1476794331(L_62, _stringLiteral89059, /*hidden argument*/NULL);
		V_6 = L_63;
		V_7 = (-1);
		int32_t L_64 = V_6;
		if ((((int32_t)L_64) == ((int32_t)(-1))))
		{
			goto IL_01b1;
		}
	}
	{
		String_t* L_65 = V_4;
		int32_t L_66 = V_6;
		NullCheck(L_65);
		int32_t L_67 = String_IndexOf_m1991631068(L_65, _stringLiteral93, L_66, /*hidden argument*/NULL);
		V_7 = L_67;
	}

IL_01b1:
	{
		int32_t L_68 = V_6;
		if ((((int32_t)L_68) == ((int32_t)(-1))))
		{
			goto IL_01d4;
		}
	}
	{
		int32_t L_69 = V_7;
		int32_t L_70 = V_6;
		if ((((int32_t)L_69) <= ((int32_t)L_70)))
		{
			goto IL_01d4;
		}
	}
	{
		String_t* L_71 = V_4;
		int32_t L_72 = V_6;
		int32_t L_73 = V_7;
		int32_t L_74 = V_6;
		NullCheck(L_71);
		String_t* L_75 = String_Remove_m242090629(L_71, L_72, ((int32_t)((int32_t)((int32_t)((int32_t)L_73-(int32_t)L_74))+(int32_t)1)), /*hidden argument*/NULL);
		V_4 = L_75;
	}

IL_01d4:
	{
		String_t* L_76 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_77 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_76);
		String_t* L_78 = String_Replace_m2915759397(L_76, _stringLiteral1627007362, L_77, /*hidden argument*/NULL);
		V_4 = L_78;
		String_t* L_79 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t4217621253_il2cpp_TypeInfo_var);
		String_t* L_80 = ((StackTraceUtility_t4217621253_StaticFields*)StackTraceUtility_t4217621253_il2cpp_TypeInfo_var->static_fields)->get_projectFolder_0();
		String_t* L_81 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_79);
		String_t* L_82 = String_Replace_m2915759397(L_79, L_80, L_81, /*hidden argument*/NULL);
		V_4 = L_82;
		String_t* L_83 = V_4;
		NullCheck(L_83);
		String_t* L_84 = String_Replace_m3369701083(L_83, ((int32_t)92), ((int32_t)47), /*hidden argument*/NULL);
		V_4 = L_84;
		String_t* L_85 = V_4;
		NullCheck(L_85);
		int32_t L_86 = String_LastIndexOf_m2747144337(L_85, _stringLiteral30610331, /*hidden argument*/NULL);
		V_8 = L_86;
		int32_t L_87 = V_8;
		if ((((int32_t)L_87) == ((int32_t)(-1))))
		{
			goto IL_024e;
		}
	}
	{
		String_t* L_88 = V_4;
		int32_t L_89 = V_8;
		NullCheck(L_88);
		String_t* L_90 = String_Remove_m242090629(L_88, L_89, 5, /*hidden argument*/NULL);
		V_4 = L_90;
		String_t* L_91 = V_4;
		int32_t L_92 = V_8;
		NullCheck(L_91);
		String_t* L_93 = String_Insert_m3926397187(L_91, L_92, _stringLiteral30841157, /*hidden argument*/NULL);
		V_4 = L_93;
		String_t* L_94 = V_4;
		String_t* L_95 = V_4;
		NullCheck(L_95);
		int32_t L_96 = String_get_Length_m2979997331(L_95, /*hidden argument*/NULL);
		NullCheck(L_94);
		String_t* L_97 = String_Insert_m3926397187(L_94, L_96, _stringLiteral41, /*hidden argument*/NULL);
		V_4 = L_97;
	}

IL_024e:
	{
		StringBuilder_t243639308 * L_98 = V_1;
		String_t* L_99 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_100 = String_Concat_m138640077(NULL /*static, unused*/, L_99, _stringLiteral10, /*hidden argument*/NULL);
		NullCheck(L_98);
		StringBuilder_Append_m3898090075(L_98, L_100, /*hidden argument*/NULL);
	}

IL_0261:
	{
		int32_t L_101 = V_3;
		V_3 = ((int32_t)((int32_t)L_101+(int32_t)1));
	}

IL_0265:
	{
		int32_t L_102 = V_3;
		StringU5BU5D_t4054002952* L_103 = V_0;
		NullCheck(L_103);
		if ((((int32_t)L_102) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_103)->max_length)))))))
		{
			goto IL_0050;
		}
	}

IL_026e:
	{
		StringBuilder_t243639308 * L_104 = V_1;
		NullCheck(L_104);
		String_t* L_105 = StringBuilder_ToString_m350379841(L_104, /*hidden argument*/NULL);
		return L_105;
	}
}
// System.String UnityEngine.StackTraceUtility::ExtractFormattedStackTrace(System.Diagnostics.StackTrace)
extern Il2CppClass* StringBuilder_t243639308_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StackTraceUtility_t4217621253_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral46;
extern Il2CppCodeGenString* _stringLiteral58;
extern Il2CppCodeGenString* _stringLiteral40;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral41;
extern Il2CppCodeGenString* _stringLiteral65906227;
extern Il2CppCodeGenString* _stringLiteral3328621047;
extern Il2CppCodeGenString* _stringLiteral2281497008;
extern Il2CppCodeGenString* _stringLiteral3703338489;
extern Il2CppCodeGenString* _stringLiteral1970626406;
extern Il2CppCodeGenString* _stringLiteral3747019304;
extern Il2CppCodeGenString* _stringLiteral30841157;
extern Il2CppCodeGenString* _stringLiteral10;
extern const uint32_t StackTraceUtility_ExtractFormattedStackTrace_m3996939365_MetadataUsageId;
extern "C"  String_t* StackTraceUtility_ExtractFormattedStackTrace_m3996939365 (Il2CppObject * __this /* static, unused */, StackTrace_t1047871261 * ___stackTrace0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_ExtractFormattedStackTrace_m3996939365_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t243639308 * V_0 = NULL;
	int32_t V_1 = 0;
	StackFrame_t1034942277 * V_2 = NULL;
	MethodBase_t318515428 * V_3 = NULL;
	Type_t * V_4 = NULL;
	String_t* V_5 = NULL;
	int32_t V_6 = 0;
	ParameterInfoU5BU5D_t2015293532* V_7 = NULL;
	bool V_8 = false;
	String_t* V_9 = NULL;
	bool V_10 = false;
	int32_t V_11 = 0;
	int32_t G_B24_0 = 0;
	int32_t G_B26_0 = 0;
	{
		StringBuilder_t243639308 * L_0 = (StringBuilder_t243639308 *)il2cpp_codegen_object_new(StringBuilder_t243639308_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3624398269(L_0, ((int32_t)255), /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 0;
		goto IL_0257;
	}

IL_0012:
	{
		StackTrace_t1047871261 * L_1 = ___stackTrace0;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		StackFrame_t1034942277 * L_3 = VirtFuncInvoker1< StackFrame_t1034942277 *, int32_t >::Invoke(5 /* System.Diagnostics.StackFrame System.Diagnostics.StackTrace::GetFrame(System.Int32) */, L_1, L_2);
		V_2 = L_3;
		StackFrame_t1034942277 * L_4 = V_2;
		NullCheck(L_4);
		MethodBase_t318515428 * L_5 = VirtFuncInvoker0< MethodBase_t318515428 * >::Invoke(7 /* System.Reflection.MethodBase System.Diagnostics.StackFrame::GetMethod() */, L_4);
		V_3 = L_5;
		MethodBase_t318515428 * L_6 = V_3;
		if (L_6)
		{
			goto IL_002c;
		}
	}
	{
		goto IL_0253;
	}

IL_002c:
	{
		MethodBase_t318515428 * L_7 = V_3;
		NullCheck(L_7);
		Type_t * L_8 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_7);
		V_4 = L_8;
		Type_t * L_9 = V_4;
		if (L_9)
		{
			goto IL_0040;
		}
	}
	{
		goto IL_0253;
	}

IL_0040:
	{
		Type_t * L_10 = V_4;
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_10);
		V_5 = L_11;
		String_t* L_12 = V_5;
		if (!L_12)
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_13 = V_5;
		NullCheck(L_13);
		int32_t L_14 = String_get_Length_m2979997331(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0071;
		}
	}
	{
		StringBuilder_t243639308 * L_15 = V_0;
		String_t* L_16 = V_5;
		NullCheck(L_15);
		StringBuilder_Append_m3898090075(L_15, L_16, /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_17 = V_0;
		NullCheck(L_17);
		StringBuilder_Append_m3898090075(L_17, _stringLiteral46, /*hidden argument*/NULL);
	}

IL_0071:
	{
		StringBuilder_t243639308 * L_18 = V_0;
		Type_t * L_19 = V_4;
		NullCheck(L_19);
		String_t* L_20 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_19);
		NullCheck(L_18);
		StringBuilder_Append_m3898090075(L_18, L_20, /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_21 = V_0;
		NullCheck(L_21);
		StringBuilder_Append_m3898090075(L_21, _stringLiteral58, /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_22 = V_0;
		MethodBase_t318515428 * L_23 = V_3;
		NullCheck(L_23);
		String_t* L_24 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_23);
		NullCheck(L_22);
		StringBuilder_Append_m3898090075(L_22, L_24, /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_25 = V_0;
		NullCheck(L_25);
		StringBuilder_Append_m3898090075(L_25, _stringLiteral40, /*hidden argument*/NULL);
		V_6 = 0;
		MethodBase_t318515428 * L_26 = V_3;
		NullCheck(L_26);
		ParameterInfoU5BU5D_t2015293532* L_27 = VirtFuncInvoker0< ParameterInfoU5BU5D_t2015293532* >::Invoke(15 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_26);
		V_7 = L_27;
		V_8 = (bool)1;
		goto IL_00ee;
	}

IL_00b7:
	{
		bool L_28 = V_8;
		if (L_28)
		{
			goto IL_00cf;
		}
	}
	{
		StringBuilder_t243639308 * L_29 = V_0;
		NullCheck(L_29);
		StringBuilder_Append_m3898090075(L_29, _stringLiteral1396, /*hidden argument*/NULL);
		goto IL_00d2;
	}

IL_00cf:
	{
		V_8 = (bool)0;
	}

IL_00d2:
	{
		StringBuilder_t243639308 * L_30 = V_0;
		ParameterInfoU5BU5D_t2015293532* L_31 = V_7;
		int32_t L_32 = V_6;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		int32_t L_33 = L_32;
		ParameterInfo_t2235474049 * L_34 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_33));
		NullCheck(L_34);
		Type_t * L_35 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_34);
		NullCheck(L_35);
		String_t* L_36 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_35);
		NullCheck(L_30);
		StringBuilder_Append_m3898090075(L_30, L_36, /*hidden argument*/NULL);
		int32_t L_37 = V_6;
		V_6 = ((int32_t)((int32_t)L_37+(int32_t)1));
	}

IL_00ee:
	{
		int32_t L_38 = V_6;
		ParameterInfoU5BU5D_t2015293532* L_39 = V_7;
		NullCheck(L_39);
		if ((((int32_t)L_38) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_39)->max_length)))))))
		{
			goto IL_00b7;
		}
	}
	{
		StringBuilder_t243639308 * L_40 = V_0;
		NullCheck(L_40);
		StringBuilder_Append_m3898090075(L_40, _stringLiteral41, /*hidden argument*/NULL);
		StackFrame_t1034942277 * L_41 = V_2;
		NullCheck(L_41);
		String_t* L_42 = VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String System.Diagnostics.StackFrame::GetFileName() */, L_41);
		V_9 = L_42;
		String_t* L_43 = V_9;
		if (!L_43)
		{
			goto IL_0247;
		}
	}
	{
		Type_t * L_44 = V_4;
		NullCheck(L_44);
		String_t* L_45 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_44);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_46 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_45, _stringLiteral65906227, /*hidden argument*/NULL);
		if (!L_46)
		{
			goto IL_0140;
		}
	}
	{
		Type_t * L_47 = V_4;
		NullCheck(L_47);
		String_t* L_48 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_47);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_49 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_48, _stringLiteral3328621047, /*hidden argument*/NULL);
		if (L_49)
		{
			goto IL_01c4;
		}
	}

IL_0140:
	{
		Type_t * L_50 = V_4;
		NullCheck(L_50);
		String_t* L_51 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_50);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_52 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_51, _stringLiteral2281497008, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_016c;
		}
	}
	{
		Type_t * L_53 = V_4;
		NullCheck(L_53);
		String_t* L_54 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_53);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_55 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_54, _stringLiteral3328621047, /*hidden argument*/NULL);
		if (L_55)
		{
			goto IL_01c4;
		}
	}

IL_016c:
	{
		Type_t * L_56 = V_4;
		NullCheck(L_56);
		String_t* L_57 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_56);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_58 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_57, _stringLiteral3703338489, /*hidden argument*/NULL);
		if (!L_58)
		{
			goto IL_0198;
		}
	}
	{
		Type_t * L_59 = V_4;
		NullCheck(L_59);
		String_t* L_60 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_59);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_61 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_60, _stringLiteral3328621047, /*hidden argument*/NULL);
		if (L_61)
		{
			goto IL_01c4;
		}
	}

IL_0198:
	{
		Type_t * L_62 = V_4;
		NullCheck(L_62);
		String_t* L_63 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_62);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_64 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_63, _stringLiteral1970626406, /*hidden argument*/NULL);
		if (!L_64)
		{
			goto IL_01c1;
		}
	}
	{
		Type_t * L_65 = V_4;
		NullCheck(L_65);
		String_t* L_66 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_65);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_67 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_66, _stringLiteral3747019304, /*hidden argument*/NULL);
		G_B24_0 = ((int32_t)(L_67));
		goto IL_01c2;
	}

IL_01c1:
	{
		G_B24_0 = 0;
	}

IL_01c2:
	{
		G_B26_0 = G_B24_0;
		goto IL_01c5;
	}

IL_01c4:
	{
		G_B26_0 = 1;
	}

IL_01c5:
	{
		V_10 = (bool)G_B26_0;
		bool L_68 = V_10;
		if (L_68)
		{
			goto IL_0247;
		}
	}
	{
		StringBuilder_t243639308 * L_69 = V_0;
		NullCheck(L_69);
		StringBuilder_Append_m3898090075(L_69, _stringLiteral30841157, /*hidden argument*/NULL);
		String_t* L_70 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t4217621253_il2cpp_TypeInfo_var);
		String_t* L_71 = ((StackTraceUtility_t4217621253_StaticFields*)StackTraceUtility_t4217621253_il2cpp_TypeInfo_var->static_fields)->get_projectFolder_0();
		NullCheck(L_70);
		bool L_72 = String_StartsWith_m1500793453(L_70, L_71, /*hidden argument*/NULL);
		if (!L_72)
		{
			goto IL_0210;
		}
	}
	{
		String_t* L_73 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t4217621253_il2cpp_TypeInfo_var);
		String_t* L_74 = ((StackTraceUtility_t4217621253_StaticFields*)StackTraceUtility_t4217621253_il2cpp_TypeInfo_var->static_fields)->get_projectFolder_0();
		NullCheck(L_74);
		int32_t L_75 = String_get_Length_m2979997331(L_74, /*hidden argument*/NULL);
		String_t* L_76 = V_9;
		NullCheck(L_76);
		int32_t L_77 = String_get_Length_m2979997331(L_76, /*hidden argument*/NULL);
		String_t* L_78 = ((StackTraceUtility_t4217621253_StaticFields*)StackTraceUtility_t4217621253_il2cpp_TypeInfo_var->static_fields)->get_projectFolder_0();
		NullCheck(L_78);
		int32_t L_79 = String_get_Length_m2979997331(L_78, /*hidden argument*/NULL);
		NullCheck(L_73);
		String_t* L_80 = String_Substring_m675079568(L_73, L_75, ((int32_t)((int32_t)L_77-(int32_t)L_79)), /*hidden argument*/NULL);
		V_9 = L_80;
	}

IL_0210:
	{
		StringBuilder_t243639308 * L_81 = V_0;
		String_t* L_82 = V_9;
		NullCheck(L_81);
		StringBuilder_Append_m3898090075(L_81, L_82, /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_83 = V_0;
		NullCheck(L_83);
		StringBuilder_Append_m3898090075(L_83, _stringLiteral58, /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_84 = V_0;
		StackFrame_t1034942277 * L_85 = V_2;
		NullCheck(L_85);
		int32_t L_86 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Diagnostics.StackFrame::GetFileLineNumber() */, L_85);
		V_11 = L_86;
		String_t* L_87 = Int32_ToString_m1286526384((&V_11), /*hidden argument*/NULL);
		NullCheck(L_84);
		StringBuilder_Append_m3898090075(L_84, L_87, /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_88 = V_0;
		NullCheck(L_88);
		StringBuilder_Append_m3898090075(L_88, _stringLiteral41, /*hidden argument*/NULL);
	}

IL_0247:
	{
		StringBuilder_t243639308 * L_89 = V_0;
		NullCheck(L_89);
		StringBuilder_Append_m3898090075(L_89, _stringLiteral10, /*hidden argument*/NULL);
	}

IL_0253:
	{
		int32_t L_90 = V_1;
		V_1 = ((int32_t)((int32_t)L_90+(int32_t)1));
	}

IL_0257:
	{
		int32_t L_91 = V_1;
		StackTrace_t1047871261 * L_92 = ___stackTrace0;
		NullCheck(L_92);
		int32_t L_93 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Diagnostics.StackTrace::get_FrameCount() */, L_92);
		if ((((int32_t)L_91) < ((int32_t)L_93)))
		{
			goto IL_0012;
		}
	}
	{
		StringBuilder_t243639308 * L_94 = V_0;
		NullCheck(L_94);
		String_t* L_95 = StringBuilder_ToString_m350379841(L_94, /*hidden argument*/NULL);
		return L_95;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::.ctor()
extern "C"  void StateMachineBehaviour__ctor_m3149540562 (StateMachineBehaviour_t759180893 * __this, const MethodInfo* method)
{
	{
		ScriptableObject__ctor_m1827087273(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern "C"  void StateMachineBehaviour_OnStateEnter_m2858660338 (StateMachineBehaviour_t759180893 * __this, Animator_t2776330603 * ___animator0, AnimatorStateInfo_t323110318  ___stateInfo1, int32_t ___layerIndex2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateUpdate(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern "C"  void StateMachineBehaviour_OnStateUpdate_m2829799871 (StateMachineBehaviour_t759180893 * __this, Animator_t2776330603 * ___animator0, AnimatorStateInfo_t323110318  ___stateInfo1, int32_t ___layerIndex2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern "C"  void StateMachineBehaviour_OnStateExit_m922939338 (StateMachineBehaviour_t759180893 * __this, Animator_t2776330603 * ___animator0, AnimatorStateInfo_t323110318  ___stateInfo1, int32_t ___layerIndex2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateMove(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern "C"  void StateMachineBehaviour_OnStateMove_m834394999 (StateMachineBehaviour_t759180893 * __this, Animator_t2776330603 * ___animator0, AnimatorStateInfo_t323110318  ___stateInfo1, int32_t ___layerIndex2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateIK(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern "C"  void StateMachineBehaviour_OnStateIK_m2478904358 (StateMachineBehaviour_t759180893 * __this, Animator_t2776330603 * ___animator0, AnimatorStateInfo_t323110318  ___stateInfo1, int32_t ___layerIndex2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineEnter(UnityEngine.Animator,System.Int32)
extern "C"  void StateMachineBehaviour_OnStateMachineEnter_m3756430466 (StateMachineBehaviour_t759180893 * __this, Animator_t2776330603 * ___animator0, int32_t ___stateMachinePathHash1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineExit(UnityEngine.Animator,System.Int32)
extern "C"  void StateMachineBehaviour_OnStateMachineExit_m4198306608 (StateMachineBehaviour_t759180893 * __this, Animator_t2776330603 * ___animator0, int32_t ___stateMachinePathHash1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32,UnityEngine.Experimental.Director.AnimatorControllerPlayable)
extern "C"  void StateMachineBehaviour_OnStateEnter_m1932811376 (StateMachineBehaviour_t759180893 * __this, Animator_t2776330603 * ___animator0, AnimatorStateInfo_t323110318  ___stateInfo1, int32_t ___layerIndex2, AnimatorControllerPlayable_t3906681469 * ___controller3, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateUpdate(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32,UnityEngine.Experimental.Director.AnimatorControllerPlayable)
extern "C"  void StateMachineBehaviour_OnStateUpdate_m2812313539 (StateMachineBehaviour_t759180893 * __this, Animator_t2776330603 * ___animator0, AnimatorStateInfo_t323110318  ___stateInfo1, int32_t ___layerIndex2, AnimatorControllerPlayable_t3906681469 * ___controller3, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32,UnityEngine.Experimental.Director.AnimatorControllerPlayable)
extern "C"  void StateMachineBehaviour_OnStateExit_m2956750232 (StateMachineBehaviour_t759180893 * __this, Animator_t2776330603 * ___animator0, AnimatorStateInfo_t323110318  ___stateInfo1, int32_t ___layerIndex2, AnimatorControllerPlayable_t3906681469 * ___controller3, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateMove(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32,UnityEngine.Experimental.Director.AnimatorControllerPlayable)
extern "C"  void StateMachineBehaviour_OnStateMove_m2938949899 (StateMachineBehaviour_t759180893 * __this, Animator_t2776330603 * ___animator0, AnimatorStateInfo_t323110318  ___stateInfo1, int32_t ___layerIndex2, AnimatorControllerPlayable_t3906681469 * ___controller3, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateIK(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32,UnityEngine.Experimental.Director.AnimatorControllerPlayable)
extern "C"  void StateMachineBehaviour_OnStateIK_m3126912700 (StateMachineBehaviour_t759180893 * __this, Animator_t2776330603 * ___animator0, AnimatorStateInfo_t323110318  ___stateInfo1, int32_t ___layerIndex2, AnimatorControllerPlayable_t3906681469 * ___controller3, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineEnter(UnityEngine.Animator,System.Int32,UnityEngine.Experimental.Director.AnimatorControllerPlayable)
extern "C"  void StateMachineBehaviour_OnStateMachineEnter_m2392556512 (StateMachineBehaviour_t759180893 * __this, Animator_t2776330603 * ___animator0, int32_t ___stateMachinePathHash1, AnimatorControllerPlayable_t3906681469 * ___controller2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineExit(UnityEngine.Animator,System.Int32,UnityEngine.Experimental.Director.AnimatorControllerPlayable)
extern "C"  void StateMachineBehaviour_OnStateMachineExit_m2976228338 (StateMachineBehaviour_t759180893 * __this, Animator_t2776330603 * ___animator0, int32_t ___stateMachinePathHash1, AnimatorControllerPlayable_t3906681469 * ___controller2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.SystemClock::.cctor()
extern Il2CppClass* SystemClock_t4036018645_il2cpp_TypeInfo_var;
extern const uint32_t SystemClock__cctor_m3660846035_MetadataUsageId;
extern "C"  void SystemClock__cctor_m3660846035 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SystemClock__cctor_m3660846035_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DateTime_t4283661327  L_0;
		memset(&L_0, 0, sizeof(L_0));
		DateTime__ctor_m3805233578(&L_0, ((int32_t)1970), 1, 1, 0, 0, 0, 1, /*hidden argument*/NULL);
		((SystemClock_t4036018645_StaticFields*)SystemClock_t4036018645_il2cpp_TypeInfo_var->static_fields)->set_s_Epoch_0(L_0);
		return;
	}
}
// System.DateTime UnityEngine.SystemClock::get_now()
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern const uint32_t SystemClock_get_now_m175136990_MetadataUsageId;
extern "C"  DateTime_t4283661327  SystemClock_get_now_m175136990 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SystemClock_get_now_m175136990_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t4283661327_il2cpp_TypeInfo_var);
		DateTime_t4283661327  L_0 = DateTime_get_Now_m1812131422(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String UnityEngine.SystemInfo::get_operatingSystem()
extern "C"  String_t* SystemInfo_get_operatingSystem_m2538828082 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*SystemInfo_get_operatingSystem_m2538828082_ftn) ();
	static SystemInfo_get_operatingSystem_m2538828082_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_operatingSystem_m2538828082_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_operatingSystem()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.SystemInfo::get_processorType()
extern "C"  String_t* SystemInfo_get_processorType_m3719165582 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*SystemInfo_get_processorType_m3719165582_ftn) ();
	static SystemInfo_get_processorType_m3719165582_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_processorType_m3719165582_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_processorType()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.SystemInfo::get_processorCount()
extern "C"  int32_t SystemInfo_get_processorCount_m3548598394 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*SystemInfo_get_processorCount_m3548598394_ftn) ();
	static SystemInfo_get_processorCount_m3548598394_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_processorCount_m3548598394_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_processorCount()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.SystemInfo::get_systemMemorySize()
extern "C"  int32_t SystemInfo_get_systemMemorySize_m114183438 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*SystemInfo_get_systemMemorySize_m114183438_ftn) ();
	static SystemInfo_get_systemMemorySize_m114183438_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_systemMemorySize_m114183438_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_systemMemorySize()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.SystemInfo::get_graphicsMemorySize()
extern "C"  int32_t SystemInfo_get_graphicsMemorySize_m4144186474 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*SystemInfo_get_graphicsMemorySize_m4144186474_ftn) ();
	static SystemInfo_get_graphicsMemorySize_m4144186474_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_graphicsMemorySize_m4144186474_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_graphicsMemorySize()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.SystemInfo::get_graphicsDeviceName()
extern "C"  String_t* SystemInfo_get_graphicsDeviceName_m4186183660 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*SystemInfo_get_graphicsDeviceName_m4186183660_ftn) ();
	static SystemInfo_get_graphicsDeviceName_m4186183660_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_graphicsDeviceName_m4186183660_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_graphicsDeviceName()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.SystemInfo::get_graphicsDeviceVendor()
extern "C"  String_t* SystemInfo_get_graphicsDeviceVendor_m3175628649 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*SystemInfo_get_graphicsDeviceVendor_m3175628649_ftn) ();
	static SystemInfo_get_graphicsDeviceVendor_m3175628649_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_graphicsDeviceVendor_m3175628649_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_graphicsDeviceVendor()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.SystemInfo::get_graphicsDeviceID()
extern "C"  int32_t SystemInfo_get_graphicsDeviceID_m2460390329 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*SystemInfo_get_graphicsDeviceID_m2460390329_ftn) ();
	static SystemInfo_get_graphicsDeviceID_m2460390329_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_graphicsDeviceID_m2460390329_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_graphicsDeviceID()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.SystemInfo::get_graphicsDeviceVendorID()
extern "C"  int32_t SystemInfo_get_graphicsDeviceVendorID_m3448682337 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*SystemInfo_get_graphicsDeviceVendorID_m3448682337_ftn) ();
	static SystemInfo_get_graphicsDeviceVendorID_m3448682337_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_graphicsDeviceVendorID_m3448682337_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_graphicsDeviceVendorID()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.SystemInfo::get_graphicsDeviceVersion()
extern "C"  String_t* SystemInfo_get_graphicsDeviceVersion_m3634129081 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*SystemInfo_get_graphicsDeviceVersion_m3634129081_ftn) ();
	static SystemInfo_get_graphicsDeviceVersion_m3634129081_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_graphicsDeviceVersion_m3634129081_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_graphicsDeviceVersion()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.SystemInfo::get_graphicsShaderLevel()
extern "C"  int32_t SystemInfo_get_graphicsShaderLevel_m1169417593 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*SystemInfo_get_graphicsShaderLevel_m1169417593_ftn) ();
	static SystemInfo_get_graphicsShaderLevel_m1169417593_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_graphicsShaderLevel_m1169417593_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_graphicsShaderLevel()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.SystemInfo::get_graphicsPixelFillrate()
extern "C"  int32_t SystemInfo_get_graphicsPixelFillrate_m2941685667 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return (-1);
	}
}
// System.Boolean UnityEngine.SystemInfo::get_supportsShadows()
extern "C"  bool SystemInfo_get_supportsShadows_m1667108142 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*SystemInfo_get_supportsShadows_m1667108142_ftn) ();
	static SystemInfo_get_supportsShadows_m1667108142_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_supportsShadows_m1667108142_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_supportsShadows()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.SystemInfo::get_supportsRenderTextures()
extern "C"  bool SystemInfo_get_supportsRenderTextures_m3098351893 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*SystemInfo_get_supportsRenderTextures_m3098351893_ftn) ();
	static SystemInfo_get_supportsRenderTextures_m3098351893_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_supportsRenderTextures_m3098351893_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_supportsRenderTextures()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.SystemInfo::get_supportsImageEffects()
extern "C"  bool SystemInfo_get_supportsImageEffects_m2392300814 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*SystemInfo_get_supportsImageEffects_m2392300814_ftn) ();
	static SystemInfo_get_supportsImageEffects_m2392300814_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_supportsImageEffects_m2392300814_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_supportsImageEffects()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.SystemInfo::get_supports3DTextures()
extern "C"  bool SystemInfo_get_supports3DTextures_m3554473744 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*SystemInfo_get_supports3DTextures_m3554473744_ftn) ();
	static SystemInfo_get_supports3DTextures_m3554473744_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_supports3DTextures_m3554473744_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_supports3DTextures()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.SystemInfo::get_supportsComputeShaders()
extern "C"  bool SystemInfo_get_supportsComputeShaders_m940660062 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*SystemInfo_get_supportsComputeShaders_m940660062_ftn) ();
	static SystemInfo_get_supportsComputeShaders_m940660062_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_supportsComputeShaders_m940660062_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_supportsComputeShaders()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.SystemInfo::get_supportsInstancing()
extern "C"  bool SystemInfo_get_supportsInstancing_m2109416633 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*SystemInfo_get_supportsInstancing_m2109416633_ftn) ();
	static SystemInfo_get_supportsInstancing_m2109416633_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_supportsInstancing_m2109416633_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_supportsInstancing()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.SystemInfo::get_supportedRenderTargetCount()
extern "C"  int32_t SystemInfo_get_supportedRenderTargetCount_m2248175383 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*SystemInfo_get_supportedRenderTargetCount_m2248175383_ftn) ();
	static SystemInfo_get_supportedRenderTargetCount_m2248175383_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_supportedRenderTargetCount_m2248175383_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_supportedRenderTargetCount()");
	return _il2cpp_icall_func();
}
// UnityEngine.NPOTSupport UnityEngine.SystemInfo::get_npotSupport()
extern "C"  int32_t SystemInfo_get_npotSupport_m2935155019 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*SystemInfo_get_npotSupport_m2935155019_ftn) ();
	static SystemInfo_get_npotSupport_m2935155019_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_npotSupport_m2935155019_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_npotSupport()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.SystemInfo::get_deviceUniqueIdentifier()
extern "C"  String_t* SystemInfo_get_deviceUniqueIdentifier_m983206480 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*SystemInfo_get_deviceUniqueIdentifier_m983206480_ftn) ();
	static SystemInfo_get_deviceUniqueIdentifier_m983206480_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_deviceUniqueIdentifier_m983206480_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_deviceUniqueIdentifier()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.SystemInfo::get_deviceName()
extern "C"  String_t* SystemInfo_get_deviceName_m3161260225 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*SystemInfo_get_deviceName_m3161260225_ftn) ();
	static SystemInfo_get_deviceName_m3161260225_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_deviceName_m3161260225_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_deviceName()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.SystemInfo::get_deviceModel()
extern "C"  String_t* SystemInfo_get_deviceModel_m3014844565 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*SystemInfo_get_deviceModel_m3014844565_ftn) ();
	static SystemInfo_get_deviceModel_m3014844565_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_deviceModel_m3014844565_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_deviceModel()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.SystemInfo::get_supportsAccelerometer()
extern "C"  bool SystemInfo_get_supportsAccelerometer_m1352574890 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*SystemInfo_get_supportsAccelerometer_m1352574890_ftn) ();
	static SystemInfo_get_supportsAccelerometer_m1352574890_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_supportsAccelerometer_m1352574890_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_supportsAccelerometer()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.SystemInfo::get_supportsGyroscope()
extern "C"  bool SystemInfo_get_supportsGyroscope_m1121004512 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*SystemInfo_get_supportsGyroscope_m1121004512_ftn) ();
	static SystemInfo_get_supportsGyroscope_m1121004512_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_supportsGyroscope_m1121004512_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_supportsGyroscope()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.SystemInfo::get_supportsLocationService()
extern "C"  bool SystemInfo_get_supportsLocationService_m2852518363 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*SystemInfo_get_supportsLocationService_m2852518363_ftn) ();
	static SystemInfo_get_supportsLocationService_m2852518363_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_supportsLocationService_m2852518363_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_supportsLocationService()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.SystemInfo::get_supportsVibration()
extern "C"  bool SystemInfo_get_supportsVibration_m259686957 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*SystemInfo_get_supportsVibration_m259686957_ftn) ();
	static SystemInfo_get_supportsVibration_m259686957_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_supportsVibration_m259686957_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_supportsVibration()");
	return _il2cpp_icall_func();
}
// UnityEngine.DeviceType UnityEngine.SystemInfo::get_deviceType()
extern "C"  int32_t SystemInfo_get_deviceType_m2827604277 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*SystemInfo_get_deviceType_m2827604277_ftn) ();
	static SystemInfo_get_deviceType_m2827604277_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_deviceType_m2827604277_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_deviceType()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.SystemInfo::get_maxTextureSize()
extern "C"  int32_t SystemInfo_get_maxTextureSize_m1524512213 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*SystemInfo_get_maxTextureSize_m1524512213_ftn) ();
	static SystemInfo_get_maxTextureSize_m1524512213_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_maxTextureSize_m1524512213_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_maxTextureSize()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.TextAreaAttribute::.ctor(System.Int32,System.Int32)
extern "C"  void TextAreaAttribute__ctor_m2577573455 (TextAreaAttribute_t641061976 * __this, int32_t ___minLines0, int32_t ___maxLines1, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m1741701746(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___minLines0;
		__this->set_minLines_0(L_0);
		int32_t L_1 = ___maxLines1;
		__this->set_maxLines_1(L_1);
		return;
	}
}
// System.String UnityEngine.TextAsset::get_text()
extern "C"  String_t* TextAsset_get_text_m655578209 (TextAsset_t3836129977 * __this, const MethodInfo* method)
{
	typedef String_t* (*TextAsset_get_text_m655578209_ftn) (TextAsset_t3836129977 *);
	static TextAsset_get_text_m655578209_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextAsset_get_text_m655578209_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextAsset::get_text()");
	return _il2cpp_icall_func(__this);
}
// System.String UnityEngine.TextAsset::ToString()
extern "C"  String_t* TextAsset_ToString_m3220326647 (TextAsset_t3836129977 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = TextAsset_get_text_m655578209(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.TextEditor::.ctor()
extern Il2CppClass* GUIStyle_t2990928826_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIContent_t2094828418_il2cpp_TypeInfo_var;
extern const uint32_t TextEditor__ctor_m1029296947_MetadataUsageId;
extern "C"  void TextEditor__ctor_m1029296947 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextEditor__ctor_m1029296947_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t2990928826_il2cpp_TypeInfo_var);
		GUIStyle_t2990928826 * L_0 = GUIStyle_get_none_m2767659632(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_style_2(L_0);
		Vector2_t4282066565  L_1 = Vector2_get_zero_m199872368(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_scrollOffset_7(L_1);
		GUIContent_t2094828418 * L_2 = (GUIContent_t2094828418 *)il2cpp_codegen_object_new(GUIContent_t2094828418_il2cpp_TypeInfo_var);
		GUIContent__ctor_m923375087(L_2, /*hidden argument*/NULL);
		__this->set_m_Content_8(L_2);
		__this->set_m_iAltCursorPos_19((-1));
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.TextEditor::get_text()
extern "C"  String_t* TextEditor_get_text_m2661970474 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	{
		GUIContent_t2094828418 * L_0 = __this->get_m_Content_8();
		NullCheck(L_0);
		String_t* L_1 = GUIContent_get_text_m3944801774(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.TextEditor::set_text(System.String)
extern "C"  void TextEditor_set_text_m2873269991 (TextEditor_t319394238 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		GUIContent_t2094828418 * L_0 = __this->get_m_Content_8();
		String_t* L_1 = ___value0;
		NullCheck(L_0);
		GUIContent_set_text_m1575840163(L_0, L_1, /*hidden argument*/NULL);
		int32_t* L_2 = __this->get_address_of_m_CursorIndex_10();
		TextEditor_ClampTextIndex_m707900960(__this, L_2, /*hidden argument*/NULL);
		int32_t* L_3 = __this->get_address_of_m_SelectIndex_11();
		TextEditor_ClampTextIndex_m707900960(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Rect UnityEngine.TextEditor::get_position()
extern "C"  Rect_t4241904616  TextEditor_get_position_m2783529335 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	{
		Rect_t4241904616  L_0 = __this->get_m_Position_9();
		return L_0;
	}
}
// System.Void UnityEngine.TextEditor::set_position(UnityEngine.Rect)
extern "C"  void TextEditor_set_position_m1757326908 (TextEditor_t319394238 * __this, Rect_t4241904616  ___value0, const MethodInfo* method)
{
	{
		Rect_t4241904616  L_0 = __this->get_m_Position_9();
		Rect_t4241904616  L_1 = ___value0;
		bool L_2 = Rect_op_Equality_m1552341101(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Rect_t4241904616  L_3 = ___value0;
		__this->set_m_Position_9(L_3);
		TextEditor_UpdateScrollOffset_m3069173402(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.TextEditor::get_cursorIndex()
extern "C"  int32_t TextEditor_get_cursorIndex_m3258858212 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_CursorIndex_10();
		return L_0;
	}
}
// System.Void UnityEngine.TextEditor::set_cursorIndex(System.Int32)
extern "C"  void TextEditor_set_cursorIndex_m3839283393 (TextEditor_t319394238 * __this, int32_t ___value0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_CursorIndex_10();
		V_0 = L_0;
		int32_t L_1 = ___value0;
		__this->set_m_CursorIndex_10(L_1);
		int32_t* L_2 = __this->get_address_of_m_CursorIndex_10();
		TextEditor_ClampTextIndex_m707900960(__this, L_2, /*hidden argument*/NULL);
		int32_t L_3 = __this->get_m_CursorIndex_10();
		int32_t L_4 = V_0;
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_m_RevealCursor_12((bool)1);
	}

IL_002d:
	{
		return;
	}
}
// System.Int32 UnityEngine.TextEditor::get_selectIndex()
extern "C"  int32_t TextEditor_get_selectIndex_m847319678 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_SelectIndex_11();
		return L_0;
	}
}
// System.Void UnityEngine.TextEditor::set_selectIndex(System.Int32)
extern "C"  void TextEditor_set_selectIndex_m2271285595 (TextEditor_t319394238 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_m_SelectIndex_11(L_0);
		int32_t* L_1 = __this->get_address_of_m_SelectIndex_11();
		TextEditor_ClampTextIndex_m707900960(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::ClearCursorPos()
extern "C"  void TextEditor_ClearCursorPos_m3987638978 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	{
		__this->set_hasHorizontalCursorPos_4((bool)0);
		__this->set_m_iAltCursorPos_19((-1));
		return;
	}
}
// System.Void UnityEngine.TextEditor::OnFocus()
extern "C"  void TextEditor_OnFocus_m1604527178 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_multiline_3();
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		V_0 = 0;
		int32_t L_1 = V_0;
		TextEditor_set_selectIndex_m2271285595(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = V_0;
		TextEditor_set_cursorIndex_m3839283393(__this, L_2, /*hidden argument*/NULL);
		goto IL_0026;
	}

IL_0020:
	{
		TextEditor_SelectAll_m2943368726(__this, /*hidden argument*/NULL);
	}

IL_0026:
	{
		__this->set_m_HasFocus_6((bool)1);
		return;
	}
}
// System.Void UnityEngine.TextEditor::OnLostFocus()
extern "C"  void TextEditor_OnLostFocus_m1599980550 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	{
		__this->set_m_HasFocus_6((bool)0);
		Vector2_t4282066565  L_0 = Vector2_get_zero_m199872368(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_scrollOffset_7(L_0);
		return;
	}
}
// System.Void UnityEngine.TextEditor::GrabGraphicalCursorPos()
extern "C"  void TextEditor_GrabGraphicalCursorPos_m192984232 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_hasHorizontalCursorPos_4();
		if (L_0)
		{
			goto IL_0058;
		}
	}
	{
		GUIStyle_t2990928826 * L_1 = __this->get_style_2();
		Rect_t4241904616  L_2 = TextEditor_get_position_m2783529335(__this, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_3 = __this->get_m_Content_8();
		int32_t L_4 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector2_t4282066565  L_5 = GUIStyle_GetCursorPixelPosition_m296157110(L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		__this->set_graphicalCursorPos_13(L_5);
		GUIStyle_t2990928826 * L_6 = __this->get_style_2();
		Rect_t4241904616  L_7 = TextEditor_get_position_m2783529335(__this, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_8 = __this->get_m_Content_8();
		int32_t L_9 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector2_t4282066565  L_10 = GUIStyle_GetCursorPixelPosition_m296157110(L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		__this->set_graphicalSelectCursorPos_14(L_10);
		__this->set_hasHorizontalCursorPos_4((bool)0);
	}

IL_0058:
	{
		return;
	}
}
// System.Boolean UnityEngine.TextEditor::HandleKeyEvent(UnityEngine.Event)
extern Il2CppClass* TextEditor_t319394238_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m92653271_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m2037749410_MethodInfo_var;
extern const uint32_t TextEditor_HandleKeyEvent_m1630561141_MetadataUsageId;
extern "C"  bool TextEditor_HandleKeyEvent_m1630561141 (TextEditor_t319394238 * __this, Event_t4196595728 * ___e0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_HandleKeyEvent_m1630561141_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		TextEditor_InitKeyActions_m2904712959(__this, /*hidden argument*/NULL);
		Event_t4196595728 * L_0 = ___e0;
		NullCheck(L_0);
		int32_t L_1 = Event_get_modifiers_m4020990886(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Event_t4196595728 * L_2 = ___e0;
		Event_t4196595728 * L_3 = L_2;
		NullCheck(L_3);
		int32_t L_4 = Event_get_modifiers_m4020990886(L_3, /*hidden argument*/NULL);
		NullCheck(L_3);
		Event_set_modifiers_m755733059(L_3, ((int32_t)((int32_t)L_4&(int32_t)((int32_t)-33))), /*hidden argument*/NULL);
		Dictionary_2_t1390811265 * L_5 = ((TextEditor_t319394238_StaticFields*)TextEditor_t319394238_il2cpp_TypeInfo_var->static_fields)->get_s_Keyactions_23();
		Event_t4196595728 * L_6 = ___e0;
		NullCheck(L_5);
		bool L_7 = Dictionary_2_ContainsKey_m92653271(L_5, L_6, /*hidden argument*/Dictionary_2_ContainsKey_m92653271_MethodInfo_var);
		if (!L_7)
		{
			goto IL_0049;
		}
	}
	{
		Dictionary_2_t1390811265 * L_8 = ((TextEditor_t319394238_StaticFields*)TextEditor_t319394238_il2cpp_TypeInfo_var->static_fields)->get_s_Keyactions_23();
		Event_t4196595728 * L_9 = ___e0;
		NullCheck(L_8);
		int32_t L_10 = Dictionary_2_get_Item_m2037749410(L_8, L_9, /*hidden argument*/Dictionary_2_get_Item_m2037749410_MethodInfo_var);
		V_1 = L_10;
		int32_t L_11 = V_1;
		TextEditor_PerformOperation_m1276141663(__this, L_11, /*hidden argument*/NULL);
		Event_t4196595728 * L_12 = ___e0;
		int32_t L_13 = V_0;
		NullCheck(L_12);
		Event_set_modifiers_m755733059(L_12, L_13, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0049:
	{
		Event_t4196595728 * L_14 = ___e0;
		int32_t L_15 = V_0;
		NullCheck(L_14);
		Event_set_modifiers_m755733059(L_14, L_15, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Boolean UnityEngine.TextEditor::DeleteLineBack()
extern "C"  bool TextEditor_DeleteLineBack_m4050675933 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		bool L_0 = TextEditor_get_hasSelection_m1781822098(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		TextEditor_DeleteSelection_m1201672204(__this, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0014:
	{
		int32_t L_1 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		V_1 = L_2;
		goto IL_003e;
	}

IL_0022:
	{
		String_t* L_3 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_4 = V_1;
		NullCheck(L_3);
		Il2CppChar L_5 = String_get_Chars_m3015341861(L_3, L_4, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_6 = V_1;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
		goto IL_0048;
	}

IL_003e:
	{
		int32_t L_7 = V_1;
		int32_t L_8 = L_7;
		V_1 = ((int32_t)((int32_t)L_8-(int32_t)1));
		if (L_8)
		{
			goto IL_0022;
		}
	}

IL_0048:
	{
		int32_t L_9 = V_1;
		if ((!(((uint32_t)L_9) == ((uint32_t)(-1)))))
		{
			goto IL_0051;
		}
	}
	{
		V_0 = 0;
	}

IL_0051:
	{
		int32_t L_10 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_11 = V_0;
		if ((((int32_t)L_10) == ((int32_t)L_11)))
		{
			goto IL_008e;
		}
	}
	{
		GUIContent_t2094828418 * L_12 = __this->get_m_Content_8();
		String_t* L_13 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_14 = V_0;
		int32_t L_15 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_16 = V_0;
		NullCheck(L_13);
		String_t* L_17 = String_Remove_m242090629(L_13, L_14, ((int32_t)((int32_t)L_15-(int32_t)L_16)), /*hidden argument*/NULL);
		NullCheck(L_12);
		GUIContent_set_text_m1575840163(L_12, L_17, /*hidden argument*/NULL);
		int32_t L_18 = V_0;
		V_2 = L_18;
		int32_t L_19 = V_2;
		TextEditor_set_cursorIndex_m3839283393(__this, L_19, /*hidden argument*/NULL);
		int32_t L_20 = V_2;
		TextEditor_set_selectIndex_m2271285595(__this, L_20, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_008e:
	{
		return (bool)0;
	}
}
// System.Boolean UnityEngine.TextEditor::DeleteWordBack()
extern "C"  bool TextEditor_DeleteWordBack_m1423005555 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		bool L_0 = TextEditor_get_hasSelection_m1781822098(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		TextEditor_DeleteSelection_m1201672204(__this, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0014:
	{
		int32_t L_1 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_2 = TextEditor_FindEndOfPreviousWord_m2038394172(__this, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_005e;
		}
	}
	{
		GUIContent_t2094828418 * L_5 = __this->get_m_Content_8();
		String_t* L_6 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_7 = V_0;
		int32_t L_8 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_9 = V_0;
		NullCheck(L_6);
		String_t* L_10 = String_Remove_m242090629(L_6, L_7, ((int32_t)((int32_t)L_8-(int32_t)L_9)), /*hidden argument*/NULL);
		NullCheck(L_5);
		GUIContent_set_text_m1575840163(L_5, L_10, /*hidden argument*/NULL);
		int32_t L_11 = V_0;
		V_1 = L_11;
		int32_t L_12 = V_1;
		TextEditor_set_cursorIndex_m3839283393(__this, L_12, /*hidden argument*/NULL);
		int32_t L_13 = V_1;
		TextEditor_set_selectIndex_m2271285595(__this, L_13, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_005e:
	{
		return (bool)0;
	}
}
// System.Boolean UnityEngine.TextEditor::DeleteWordForward()
extern "C"  bool TextEditor_DeleteWordForward_m2247051099 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = TextEditor_get_hasSelection_m1781822098(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		TextEditor_DeleteSelection_m1201672204(__this, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0014:
	{
		int32_t L_1 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_2 = TextEditor_FindStartOfNextWord_m4132487039(__this, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		String_t* L_4 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_m2979997331(L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_3) >= ((int32_t)L_5)))
		{
			goto IL_005d;
		}
	}
	{
		GUIContent_t2094828418 * L_6 = __this->get_m_Content_8();
		String_t* L_7 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_8 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_9 = V_0;
		int32_t L_10 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		String_t* L_11 = String_Remove_m242090629(L_7, L_8, ((int32_t)((int32_t)L_9-(int32_t)L_10)), /*hidden argument*/NULL);
		NullCheck(L_6);
		GUIContent_set_text_m1575840163(L_6, L_11, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_005d:
	{
		return (bool)0;
	}
}
// System.Boolean UnityEngine.TextEditor::Delete()
extern "C"  bool TextEditor_Delete_m2591383938 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	{
		bool L_0 = TextEditor_get_hasSelection_m1781822098(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		TextEditor_DeleteSelection_m1201672204(__this, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0014:
	{
		int32_t L_1 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		String_t* L_2 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m2979997331(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0049;
		}
	}
	{
		GUIContent_t2094828418 * L_4 = __this->get_m_Content_8();
		String_t* L_5 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_7 = String_Remove_m242090629(L_5, L_6, 1, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIContent_set_text_m1575840163(L_4, L_7, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0049:
	{
		return (bool)0;
	}
}
// System.Boolean UnityEngine.TextEditor::Backspace()
extern "C"  bool TextEditor_Backspace_m2697740394 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = TextEditor_get_hasSelection_m1781822098(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		TextEditor_DeleteSelection_m1201672204(__this, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0014:
	{
		int32_t L_1 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_005e;
		}
	}
	{
		GUIContent_t2094828418 * L_2 = __this->get_m_Content_8();
		String_t* L_3 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_4 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_5 = String_Remove_m242090629(L_3, ((int32_t)((int32_t)L_4-(int32_t)1)), 1, /*hidden argument*/NULL);
		NullCheck(L_2);
		GUIContent_set_text_m1575840163(L_2, L_5, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_6-(int32_t)1));
		int32_t L_7 = V_0;
		TextEditor_set_cursorIndex_m3839283393(__this, L_7, /*hidden argument*/NULL);
		int32_t L_8 = V_0;
		TextEditor_set_selectIndex_m2271285595(__this, L_8, /*hidden argument*/NULL);
		TextEditor_ClearCursorPos_m3987638978(__this, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_005e:
	{
		return (bool)0;
	}
}
// System.Void UnityEngine.TextEditor::SelectAll()
extern "C"  void TextEditor_SelectAll_m2943368726 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	{
		TextEditor_set_cursorIndex_m3839283393(__this, 0, /*hidden argument*/NULL);
		String_t* L_0 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m2979997331(L_0, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2271285595(__this, L_1, /*hidden argument*/NULL);
		TextEditor_ClearCursorPos_m3987638978(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectNone()
extern "C"  void TextEditor_SelectNone_m1425185029 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2271285595(__this, L_0, /*hidden argument*/NULL);
		TextEditor_ClearCursorPos_m3987638978(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.TextEditor::get_hasSelection()
extern "C"  bool TextEditor_get_hasSelection_m1781822098 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		return (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UnityEngine.TextEditor::DeleteSelection()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t TextEditor_DeleteSelection_m1201672204_MetadataUsageId;
extern "C"  bool TextEditor_DeleteSelection_m1201672204 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_DeleteSelection_m1201672204_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0013;
		}
	}
	{
		return (bool)0;
	}

IL_0013:
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) >= ((int32_t)L_3)))
		{
			goto IL_007a;
		}
	}
	{
		GUIContent_t2094828418 * L_4 = __this->get_m_Content_8();
		String_t* L_5 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_7 = String_Substring_m675079568(L_5, 0, L_6, /*hidden argument*/NULL);
		String_t* L_8 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_9 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		String_t* L_10 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		int32_t L_11 = String_get_Length_m2979997331(L_10, /*hidden argument*/NULL);
		int32_t L_12 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		String_t* L_13 = String_Substring_m675079568(L_8, L_9, ((int32_t)((int32_t)L_11-(int32_t)L_12)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m138640077(NULL /*static, unused*/, L_7, L_13, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIContent_set_text_m1575840163(L_4, L_14, /*hidden argument*/NULL);
		int32_t L_15 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2271285595(__this, L_15, /*hidden argument*/NULL);
		goto IL_00cb;
	}

IL_007a:
	{
		GUIContent_t2094828418 * L_16 = __this->get_m_Content_8();
		String_t* L_17 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_18 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		String_t* L_19 = String_Substring_m675079568(L_17, 0, L_18, /*hidden argument*/NULL);
		String_t* L_20 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_21 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		String_t* L_22 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		int32_t L_23 = String_get_Length_m2979997331(L_22, /*hidden argument*/NULL);
		int32_t L_24 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		String_t* L_25 = String_Substring_m675079568(L_20, L_21, ((int32_t)((int32_t)L_23-(int32_t)L_24)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m138640077(NULL /*static, unused*/, L_19, L_25, /*hidden argument*/NULL);
		NullCheck(L_16);
		GUIContent_set_text_m1575840163(L_16, L_26, /*hidden argument*/NULL);
		int32_t L_27 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, L_27, /*hidden argument*/NULL);
	}

IL_00cb:
	{
		TextEditor_ClearCursorPos_m3987638978(__this, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Void UnityEngine.TextEditor::ReplaceSelection(System.String)
extern "C"  void TextEditor_ReplaceSelection_m1188267705 (TextEditor_t319394238 * __this, String_t* ___replace0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_DeleteSelection_m1201672204(__this, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_0 = __this->get_m_Content_8();
		String_t* L_1 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_2 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		String_t* L_3 = ___replace0;
		NullCheck(L_1);
		String_t* L_4 = String_Insert_m3926397187(L_1, L_2, L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		GUIContent_set_text_m1575840163(L_0, L_4, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		String_t* L_6 = ___replace0;
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m2979997331(L_6, /*hidden argument*/NULL);
		int32_t L_8 = ((int32_t)((int32_t)L_5+(int32_t)L_7));
		V_0 = L_8;
		TextEditor_set_cursorIndex_m3839283393(__this, L_8, /*hidden argument*/NULL);
		int32_t L_9 = V_0;
		TextEditor_set_selectIndex_m2271285595(__this, L_9, /*hidden argument*/NULL);
		TextEditor_ClearCursorPos_m3987638978(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::Insert(System.Char)
extern "C"  void TextEditor_Insert_m1131670643 (TextEditor_t319394238 * __this, Il2CppChar ___c0, const MethodInfo* method)
{
	{
		String_t* L_0 = Char_ToString_m2089191214((&___c0), /*hidden argument*/NULL);
		TextEditor_ReplaceSelection_m1188267705(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveRight()
extern "C"  void TextEditor_MoveRight_m4087700060 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	{
		TextEditor_ClearCursorPos_m3987638978(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, ((int32_t)((int32_t)L_2+(int32_t)1)), /*hidden argument*/NULL);
		TextEditor_DetectFocusChange_m993735446(__this, /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2271285595(__this, L_3, /*hidden argument*/NULL);
		goto IL_006a;
	}

IL_003c:
	{
		int32_t L_4 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)L_5)))
		{
			goto IL_005e;
		}
	}
	{
		int32_t L_6 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, L_6, /*hidden argument*/NULL);
		goto IL_006a;
	}

IL_005e:
	{
		int32_t L_7 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2271285595(__this, L_7, /*hidden argument*/NULL);
	}

IL_006a:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveLeft()
extern "C"  void TextEditor_MoveLeft_m2173129001 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0030;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, ((int32_t)((int32_t)L_2-(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2271285595(__this, L_3, /*hidden argument*/NULL);
		goto IL_005e;
	}

IL_0030:
	{
		int32_t L_4 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)L_5)))
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_6 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2271285595(__this, L_6, /*hidden argument*/NULL);
		goto IL_005e;
	}

IL_0052:
	{
		int32_t L_7 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, L_7, /*hidden argument*/NULL);
	}

IL_005e:
	{
		TextEditor_ClearCursorPos_m3987638978(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveUp()
extern "C"  void TextEditor_MoveUp_m2143317757 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2271285595(__this, L_2, /*hidden argument*/NULL);
		goto IL_002e;
	}

IL_0022:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, L_3, /*hidden argument*/NULL);
	}

IL_002e:
	{
		TextEditor_GrabGraphicalCursorPos_m192984232(__this, /*hidden argument*/NULL);
		Vector2_t4282066565 * L_4 = __this->get_address_of_graphicalCursorPos_13();
		Vector2_t4282066565 * L_5 = L_4;
		float L_6 = L_5->get_y_2();
		L_5->set_y_2(((float)((float)L_6-(float)(1.0f))));
		GUIStyle_t2990928826 * L_7 = __this->get_style_2();
		Rect_t4241904616  L_8 = TextEditor_get_position_m2783529335(__this, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_9 = __this->get_m_Content_8();
		Vector2_t4282066565  L_10 = __this->get_graphicalCursorPos_13();
		NullCheck(L_7);
		int32_t L_11 = GUIStyle_GetCursorStringIndex_m3223359742(L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		int32_t L_12 = V_0;
		TextEditor_set_selectIndex_m2271285595(__this, L_12, /*hidden argument*/NULL);
		int32_t L_13 = V_0;
		TextEditor_set_cursorIndex_m3839283393(__this, L_13, /*hidden argument*/NULL);
		int32_t L_14 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_14) > ((int32_t)0)))
		{
			goto IL_0089;
		}
	}
	{
		TextEditor_ClearCursorPos_m3987638978(__this, /*hidden argument*/NULL);
	}

IL_0089:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveDown()
extern "C"  void TextEditor_MoveDown_m1953831684 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2271285595(__this, L_2, /*hidden argument*/NULL);
		goto IL_002e;
	}

IL_0022:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, L_3, /*hidden argument*/NULL);
	}

IL_002e:
	{
		TextEditor_GrabGraphicalCursorPos_m192984232(__this, /*hidden argument*/NULL);
		Vector2_t4282066565 * L_4 = __this->get_address_of_graphicalCursorPos_13();
		Vector2_t4282066565 * L_5 = L_4;
		float L_6 = L_5->get_y_2();
		GUIStyle_t2990928826 * L_7 = __this->get_style_2();
		NullCheck(L_7);
		float L_8 = GUIStyle_get_lineHeight_m3977255453(L_7, /*hidden argument*/NULL);
		L_5->set_y_2(((float)((float)L_6+(float)((float)((float)L_8+(float)(5.0f))))));
		GUIStyle_t2990928826 * L_9 = __this->get_style_2();
		Rect_t4241904616  L_10 = TextEditor_get_position_m2783529335(__this, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_11 = __this->get_m_Content_8();
		Vector2_t4282066565  L_12 = __this->get_graphicalCursorPos_13();
		NullCheck(L_9);
		int32_t L_13 = GUIStyle_GetCursorStringIndex_m3223359742(L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		int32_t L_14 = V_0;
		TextEditor_set_selectIndex_m2271285595(__this, L_14, /*hidden argument*/NULL);
		int32_t L_15 = V_0;
		TextEditor_set_cursorIndex_m3839283393(__this, L_15, /*hidden argument*/NULL);
		int32_t L_16 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		String_t* L_17 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		int32_t L_18 = String_get_Length_m2979997331(L_17, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_16) == ((uint32_t)L_18))))
		{
			goto IL_009f;
		}
	}
	{
		TextEditor_ClearCursorPos_m3987638978(__this, /*hidden argument*/NULL);
	}

IL_009f:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveLineStart()
extern "C"  void TextEditor_MoveLineStart_m2661644558 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_2 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0022;
	}

IL_001c:
	{
		int32_t L_3 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
	}

IL_0022:
	{
		V_0 = G_B3_0;
		int32_t L_4 = V_0;
		V_1 = L_4;
		goto IL_0050;
	}

IL_002a:
	{
		String_t* L_5 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_6 = V_1;
		NullCheck(L_5);
		Il2CppChar L_7 = String_get_Chars_m3015341861(L_5, L_6, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0050;
		}
	}
	{
		int32_t L_8 = V_1;
		V_2 = ((int32_t)((int32_t)L_8+(int32_t)1));
		int32_t L_9 = V_2;
		TextEditor_set_cursorIndex_m3839283393(__this, L_9, /*hidden argument*/NULL);
		int32_t L_10 = V_2;
		TextEditor_set_selectIndex_m2271285595(__this, L_10, /*hidden argument*/NULL);
		return;
	}

IL_0050:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = L_11;
		V_1 = ((int32_t)((int32_t)L_12-(int32_t)1));
		if (L_12)
		{
			goto IL_002a;
		}
	}
	{
		V_2 = 0;
		int32_t L_13 = V_2;
		TextEditor_set_cursorIndex_m3839283393(__this, L_13, /*hidden argument*/NULL);
		int32_t L_14 = V_2;
		TextEditor_set_selectIndex_m2271285595(__this, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveLineEnd()
extern "C"  void TextEditor_MoveLineEnd_m2429882887 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_2 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0022;
	}

IL_001c:
	{
		int32_t L_3 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
	}

IL_0022:
	{
		V_0 = G_B3_0;
		int32_t L_4 = V_0;
		V_1 = L_4;
		String_t* L_5 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m2979997331(L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		goto IL_005e;
	}

IL_0036:
	{
		String_t* L_7 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_8 = V_1;
		NullCheck(L_7);
		Il2CppChar L_9 = String_get_Chars_m3015341861(L_7, L_8, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_005a;
		}
	}
	{
		int32_t L_10 = V_1;
		V_3 = L_10;
		int32_t L_11 = V_3;
		TextEditor_set_cursorIndex_m3839283393(__this, L_11, /*hidden argument*/NULL);
		int32_t L_12 = V_3;
		TextEditor_set_selectIndex_m2271285595(__this, L_12, /*hidden argument*/NULL);
		return;
	}

IL_005a:
	{
		int32_t L_13 = V_1;
		V_1 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_005e:
	{
		int32_t L_14 = V_1;
		int32_t L_15 = V_2;
		if ((((int32_t)L_14) < ((int32_t)L_15)))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_16 = V_2;
		V_3 = L_16;
		int32_t L_17 = V_3;
		TextEditor_set_cursorIndex_m3839283393(__this, L_17, /*hidden argument*/NULL);
		int32_t L_18 = V_3;
		TextEditor_set_selectIndex_m2271285595(__this, L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveGraphicalLineStart()
extern "C"  void TextEditor_MoveGraphicalLineStart_m1209754461 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	TextEditor_t319394238 * G_B2_0 = NULL;
	TextEditor_t319394238 * G_B2_1 = NULL;
	TextEditor_t319394238 * G_B1_0 = NULL;
	TextEditor_t319394238 * G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	TextEditor_t319394238 * G_B3_1 = NULL;
	TextEditor_t319394238 * G_B3_2 = NULL;
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		G_B1_1 = __this;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			G_B2_0 = __this;
			G_B2_1 = __this;
			goto IL_001e;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0024;
	}

IL_001e:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0024:
	{
		NullCheck(G_B3_1);
		int32_t L_4 = TextEditor_GetGraphicalLineStart_m2255929555(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_0;
		TextEditor_set_selectIndex_m2271285595(__this, L_5, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		NullCheck(G_B3_2);
		TextEditor_set_cursorIndex_m3839283393(G_B3_2, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveGraphicalLineEnd()
extern "C"  void TextEditor_MoveGraphicalLineEnd_m537871382 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	TextEditor_t319394238 * G_B2_0 = NULL;
	TextEditor_t319394238 * G_B2_1 = NULL;
	TextEditor_t319394238 * G_B1_0 = NULL;
	TextEditor_t319394238 * G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	TextEditor_t319394238 * G_B3_1 = NULL;
	TextEditor_t319394238 * G_B3_2 = NULL;
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		G_B1_1 = __this;
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			G_B2_0 = __this;
			G_B2_1 = __this;
			goto IL_001e;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0024;
	}

IL_001e:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0024:
	{
		NullCheck(G_B3_1);
		int32_t L_4 = TextEditor_GetGraphicalLineEnd_m3638753612(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_0;
		TextEditor_set_selectIndex_m2271285595(__this, L_5, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		NullCheck(G_B3_2);
		TextEditor_set_cursorIndex_m3839283393(G_B3_2, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveTextStart()
extern "C"  void TextEditor_MoveTextStart_m1434665397 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		TextEditor_set_cursorIndex_m3839283393(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = V_0;
		TextEditor_set_selectIndex_m2271285595(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveTextEnd()
extern "C"  void TextEditor_MoveTextEnd_m3188381806 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		String_t* L_0 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m2979997331(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		TextEditor_set_cursorIndex_m3839283393(__this, L_2, /*hidden argument*/NULL);
		int32_t L_3 = V_0;
		TextEditor_set_selectIndex_m2271285595(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.TextEditor::IndexOfEndOfLine(System.Int32)
extern "C"  int32_t TextEditor_IndexOfEndOfLine_m1755139103 (TextEditor_t319394238 * __this, int32_t ___startIndex0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___startIndex0;
		NullCheck(L_0);
		int32_t L_2 = String_IndexOf_m204546721(L_0, ((int32_t)10), L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_4 = V_0;
		G_B3_0 = L_4;
		goto IL_0027;
	}

IL_001c:
	{
		String_t* L_5 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m2979997331(L_5, /*hidden argument*/NULL);
		G_B3_0 = L_6;
	}

IL_0027:
	{
		return G_B3_0;
	}
}
// System.Void UnityEngine.TextEditor::MoveParagraphForward()
extern "C"  void TextEditor_MoveParagraphForward_m2816529689 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	TextEditor_t319394238 * G_B2_0 = NULL;
	TextEditor_t319394238 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	TextEditor_t319394238 * G_B3_1 = NULL;
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			G_B2_0 = __this;
			goto IL_001d;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0023;
	}

IL_001d:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_0023:
	{
		NullCheck(G_B3_1);
		TextEditor_set_cursorIndex_m3839283393(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		int32_t L_4 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		String_t* L_5 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m2979997331(L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_4) >= ((int32_t)L_6)))
		{
			goto IL_005b;
		}
	}
	{
		int32_t L_7 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_8 = TextEditor_IndexOfEndOfLine_m1755139103(__this, ((int32_t)((int32_t)L_7+(int32_t)1)), /*hidden argument*/NULL);
		V_0 = L_8;
		int32_t L_9 = V_0;
		TextEditor_set_cursorIndex_m3839283393(__this, L_9, /*hidden argument*/NULL);
		int32_t L_10 = V_0;
		TextEditor_set_selectIndex_m2271285595(__this, L_10, /*hidden argument*/NULL);
	}

IL_005b:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveParagraphBackward()
extern "C"  void TextEditor_MoveParagraphBackward_m4210809841 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	TextEditor_t319394238 * G_B2_0 = NULL;
	TextEditor_t319394238 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	TextEditor_t319394238 * G_B3_1 = NULL;
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			G_B2_0 = __this;
			goto IL_001d;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0023;
	}

IL_001d:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_0023:
	{
		NullCheck(G_B3_1);
		TextEditor_set_cursorIndex_m3839283393(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		int32_t L_4 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)1)))
		{
			goto IL_005f;
		}
	}
	{
		String_t* L_5 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_7 = String_LastIndexOf_m902083627(L_5, ((int32_t)10), ((int32_t)((int32_t)L_6-(int32_t)2)), /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
		int32_t L_8 = V_0;
		TextEditor_set_cursorIndex_m3839283393(__this, L_8, /*hidden argument*/NULL);
		int32_t L_9 = V_0;
		TextEditor_set_selectIndex_m2271285595(__this, L_9, /*hidden argument*/NULL);
		goto IL_006f;
	}

IL_005f:
	{
		V_0 = 0;
		int32_t L_10 = V_0;
		TextEditor_set_cursorIndex_m3839283393(__this, L_10, /*hidden argument*/NULL);
		int32_t L_11 = V_0;
		TextEditor_set_selectIndex_m2271285595(__this, L_11, /*hidden argument*/NULL);
	}

IL_006f:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveCursorToPosition(UnityEngine.Vector2)
extern "C"  void TextEditor_MoveCursorToPosition_m541380254 (TextEditor_t319394238 * __this, Vector2_t4282066565  ___cursorPosition0, const MethodInfo* method)
{
	{
		GUIStyle_t2990928826 * L_0 = __this->get_style_2();
		Rect_t4241904616  L_1 = TextEditor_get_position_m2783529335(__this, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_2 = __this->get_m_Content_8();
		Vector2_t4282066565  L_3 = ___cursorPosition0;
		Vector2_t4282066565  L_4 = __this->get_scrollOffset_7();
		Vector2_t4282066565  L_5 = Vector2_op_Addition_m1173049553(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_6 = GUIStyle_GetCursorStringIndex_m3223359742(L_0, L_1, L_2, L_5, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2271285595(__this, L_6, /*hidden argument*/NULL);
		Event_t4196595728 * L_7 = Event_get_current_m238587645(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Event_get_shift_m643038780(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0044;
		}
	}
	{
		int32_t L_9 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, L_9, /*hidden argument*/NULL);
	}

IL_0044:
	{
		TextEditor_DetectFocusChange_m993735446(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectToPosition(UnityEngine.Vector2)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t TextEditor_SelectToPosition_m406326665_MetadataUsageId;
extern "C"  void TextEditor_SelectToPosition_m406326665 (TextEditor_t319394238 * __this, Vector2_t4282066565  ___cursorPosition0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_SelectToPosition_m406326665_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_m_MouseDragSelectsWholeWords_15();
		if (L_0)
		{
			goto IL_0039;
		}
	}
	{
		GUIStyle_t2990928826 * L_1 = __this->get_style_2();
		Rect_t4241904616  L_2 = TextEditor_get_position_m2783529335(__this, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_3 = __this->get_m_Content_8();
		Vector2_t4282066565  L_4 = ___cursorPosition0;
		Vector2_t4282066565  L_5 = __this->get_scrollOffset_7();
		Vector2_t4282066565  L_6 = Vector2_op_Addition_m1173049553(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_7 = GUIStyle_GetCursorStringIndex_m3223359742(L_1, L_2, L_3, L_6, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, L_7, /*hidden argument*/NULL);
		goto IL_0193;
	}

IL_0039:
	{
		GUIStyle_t2990928826 * L_8 = __this->get_style_2();
		Rect_t4241904616  L_9 = TextEditor_get_position_m2783529335(__this, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_10 = __this->get_m_Content_8();
		Vector2_t4282066565  L_11 = ___cursorPosition0;
		Vector2_t4282066565  L_12 = __this->get_scrollOffset_7();
		Vector2_t4282066565  L_13 = Vector2_op_Addition_m1173049553(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		NullCheck(L_8);
		int32_t L_14 = GUIStyle_GetCursorStringIndex_m3223359742(L_8, L_9, L_10, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
		uint8_t L_15 = __this->get_m_DblClickSnap_17();
		if (L_15)
		{
			goto IL_00e1;
		}
	}
	{
		int32_t L_16 = V_0;
		int32_t L_17 = __this->get_m_DblClickInitPos_16();
		if ((((int32_t)L_16) >= ((int32_t)L_17)))
		{
			goto IL_009a;
		}
	}
	{
		int32_t L_18 = V_0;
		int32_t L_19 = TextEditor_FindEndOfClassification_m3906171638(__this, L_18, (-1), /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, L_19, /*hidden argument*/NULL);
		int32_t L_20 = __this->get_m_DblClickInitPos_16();
		int32_t L_21 = TextEditor_FindEndOfClassification_m3906171638(__this, L_20, 1, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2271285595(__this, L_21, /*hidden argument*/NULL);
		goto IL_00dc;
	}

IL_009a:
	{
		int32_t L_22 = V_0;
		String_t* L_23 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		NullCheck(L_23);
		int32_t L_24 = String_get_Length_m2979997331(L_23, /*hidden argument*/NULL);
		if ((((int32_t)L_22) < ((int32_t)L_24)))
		{
			goto IL_00b9;
		}
	}
	{
		String_t* L_25 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		NullCheck(L_25);
		int32_t L_26 = String_get_Length_m2979997331(L_25, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_26-(int32_t)1));
	}

IL_00b9:
	{
		int32_t L_27 = V_0;
		int32_t L_28 = TextEditor_FindEndOfClassification_m3906171638(__this, L_27, 1, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, L_28, /*hidden argument*/NULL);
		int32_t L_29 = __this->get_m_DblClickInitPos_16();
		int32_t L_30 = TextEditor_FindEndOfClassification_m3906171638(__this, ((int32_t)((int32_t)L_29-(int32_t)1)), (-1), /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2271285595(__this, L_30, /*hidden argument*/NULL);
	}

IL_00dc:
	{
		goto IL_0193;
	}

IL_00e1:
	{
		int32_t L_31 = V_0;
		int32_t L_32 = __this->get_m_DblClickInitPos_16();
		if ((((int32_t)L_31) >= ((int32_t)L_32)))
		{
			goto IL_013c;
		}
	}
	{
		int32_t L_33 = V_0;
		if ((((int32_t)L_33) <= ((int32_t)0)))
		{
			goto IL_0117;
		}
	}
	{
		String_t* L_34 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_35 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_36 = Mathf_Max_m2911193737(NULL /*static, unused*/, 0, ((int32_t)((int32_t)L_35-(int32_t)2)), /*hidden argument*/NULL);
		NullCheck(L_34);
		int32_t L_37 = String_LastIndexOf_m902083627(L_34, ((int32_t)10), L_36, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, ((int32_t)((int32_t)L_37+(int32_t)1)), /*hidden argument*/NULL);
		goto IL_011e;
	}

IL_0117:
	{
		TextEditor_set_cursorIndex_m3839283393(__this, 0, /*hidden argument*/NULL);
	}

IL_011e:
	{
		String_t* L_38 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_39 = __this->get_m_DblClickInitPos_16();
		NullCheck(L_38);
		int32_t L_40 = String_LastIndexOf_m902083627(L_38, ((int32_t)10), L_39, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2271285595(__this, L_40, /*hidden argument*/NULL);
		goto IL_0193;
	}

IL_013c:
	{
		int32_t L_41 = V_0;
		String_t* L_42 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		NullCheck(L_42);
		int32_t L_43 = String_get_Length_m2979997331(L_42, /*hidden argument*/NULL);
		if ((((int32_t)L_41) >= ((int32_t)L_43)))
		{
			goto IL_015f;
		}
	}
	{
		int32_t L_44 = V_0;
		int32_t L_45 = TextEditor_IndexOfEndOfLine_m1755139103(__this, L_44, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, L_45, /*hidden argument*/NULL);
		goto IL_0170;
	}

IL_015f:
	{
		String_t* L_46 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		NullCheck(L_46);
		int32_t L_47 = String_get_Length_m2979997331(L_46, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, L_47, /*hidden argument*/NULL);
	}

IL_0170:
	{
		String_t* L_48 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_49 = __this->get_m_DblClickInitPos_16();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_50 = Mathf_Max_m2911193737(NULL /*static, unused*/, 0, ((int32_t)((int32_t)L_49-(int32_t)2)), /*hidden argument*/NULL);
		NullCheck(L_48);
		int32_t L_51 = String_LastIndexOf_m902083627(L_48, ((int32_t)10), L_50, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2271285595(__this, ((int32_t)((int32_t)L_51+(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0193:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectLeft()
extern "C"  void TextEditor_SelectLeft_m1358467604 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_m_bJustSelected_18();
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_1 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_2 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)L_2)))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_3 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		TextEditor_set_selectIndex_m2271285595(__this, L_5, /*hidden argument*/NULL);
	}

IL_0036:
	{
		__this->set_m_bJustSelected_18((bool)0);
		int32_t L_6 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, ((int32_t)((int32_t)L_6-(int32_t)1)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectRight()
extern "C"  void TextEditor_SelectRight_m308033233 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_m_bJustSelected_18();
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_1 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_2 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) >= ((int32_t)L_2)))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_3 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		TextEditor_set_selectIndex_m2271285595(__this, L_5, /*hidden argument*/NULL);
	}

IL_0036:
	{
		__this->set_m_bJustSelected_18((bool)0);
		int32_t L_6 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, ((int32_t)((int32_t)L_6+(int32_t)1)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectUp()
extern "C"  void TextEditor_SelectUp_m2312302248 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	{
		TextEditor_GrabGraphicalCursorPos_m192984232(__this, /*hidden argument*/NULL);
		Vector2_t4282066565 * L_0 = __this->get_address_of_graphicalCursorPos_13();
		Vector2_t4282066565 * L_1 = L_0;
		float L_2 = L_1->get_y_2();
		L_1->set_y_2(((float)((float)L_2-(float)(1.0f))));
		GUIStyle_t2990928826 * L_3 = __this->get_style_2();
		Rect_t4241904616  L_4 = TextEditor_get_position_m2783529335(__this, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_5 = __this->get_m_Content_8();
		Vector2_t4282066565  L_6 = __this->get_graphicalCursorPos_13();
		NullCheck(L_3);
		int32_t L_7 = GUIStyle_GetCursorStringIndex_m3223359742(L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectDown()
extern "C"  void TextEditor_SelectDown_m1139170287 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	{
		TextEditor_GrabGraphicalCursorPos_m192984232(__this, /*hidden argument*/NULL);
		Vector2_t4282066565 * L_0 = __this->get_address_of_graphicalCursorPos_13();
		Vector2_t4282066565 * L_1 = L_0;
		float L_2 = L_1->get_y_2();
		GUIStyle_t2990928826 * L_3 = __this->get_style_2();
		NullCheck(L_3);
		float L_4 = GUIStyle_get_lineHeight_m3977255453(L_3, /*hidden argument*/NULL);
		L_1->set_y_2(((float)((float)L_2+(float)((float)((float)L_4+(float)(5.0f))))));
		GUIStyle_t2990928826 * L_5 = __this->get_style_2();
		Rect_t4241904616  L_6 = TextEditor_get_position_m2783529335(__this, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_7 = __this->get_m_Content_8();
		Vector2_t4282066565  L_8 = __this->get_graphicalCursorPos_13();
		NullCheck(L_5);
		int32_t L_9 = GUIStyle_GetCursorStringIndex_m3223359742(L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectTextEnd()
extern "C"  void TextEditor_SelectTextEnd_m175926179 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m2979997331(L_0, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectTextStart()
extern "C"  void TextEditor_SelectTextStart_m1272765354 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	{
		TextEditor_set_cursorIndex_m3839283393(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MouseDragSelectsWholeWords(System.Boolean)
extern "C"  void TextEditor_MouseDragSelectsWholeWords_m3387444856 (TextEditor_t319394238 * __this, bool ___on0, const MethodInfo* method)
{
	{
		bool L_0 = ___on0;
		__this->set_m_MouseDragSelectsWholeWords_15(L_0);
		int32_t L_1 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		__this->set_m_DblClickInitPos_16(L_1);
		return;
	}
}
// System.Void UnityEngine.TextEditor::DblClickSnap(UnityEngine.TextEditor/DblClickSnapping)
extern "C"  void TextEditor_DblClickSnap_m1086666601 (TextEditor_t319394238 * __this, uint8_t ___snapping0, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___snapping0;
		__this->set_m_DblClickSnap_17(L_0);
		return;
	}
}
// System.Int32 UnityEngine.TextEditor::GetGraphicalLineStart(System.Int32)
extern "C"  int32_t TextEditor_GetGraphicalLineStart_m2255929555 (TextEditor_t319394238 * __this, int32_t ___p0, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		GUIStyle_t2990928826 * L_0 = __this->get_style_2();
		Rect_t4241904616  L_1 = TextEditor_get_position_m2783529335(__this, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_2 = __this->get_m_Content_8();
		int32_t L_3 = ___p0;
		NullCheck(L_0);
		Vector2_t4282066565  L_4 = GUIStyle_GetCursorPixelPosition_m296157110(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		(&V_0)->set_x_1((0.0f));
		GUIStyle_t2990928826 * L_5 = __this->get_style_2();
		Rect_t4241904616  L_6 = TextEditor_get_position_m2783529335(__this, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_7 = __this->get_m_Content_8();
		Vector2_t4282066565  L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = GUIStyle_GetCursorStringIndex_m3223359742(L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Int32 UnityEngine.TextEditor::GetGraphicalLineEnd(System.Int32)
extern "C"  int32_t TextEditor_GetGraphicalLineEnd_m3638753612 (TextEditor_t319394238 * __this, int32_t ___p0, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		GUIStyle_t2990928826 * L_0 = __this->get_style_2();
		Rect_t4241904616  L_1 = TextEditor_get_position_m2783529335(__this, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_2 = __this->get_m_Content_8();
		int32_t L_3 = ___p0;
		NullCheck(L_0);
		Vector2_t4282066565  L_4 = GUIStyle_GetCursorPixelPosition_m296157110(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Vector2_t4282066565 * L_5 = (&V_0);
		float L_6 = L_5->get_x_1();
		L_5->set_x_1(((float)((float)L_6+(float)(5000.0f))));
		GUIStyle_t2990928826 * L_7 = __this->get_style_2();
		Rect_t4241904616  L_8 = TextEditor_get_position_m2783529335(__this, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_9 = __this->get_m_Content_8();
		Vector2_t4282066565  L_10 = V_0;
		NullCheck(L_7);
		int32_t L_11 = GUIStyle_GetCursorStringIndex_m3223359742(L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Int32 UnityEngine.TextEditor::FindNextSeperator(System.Int32)
extern "C"  int32_t TextEditor_FindNextSeperator_m2382945623 (TextEditor_t319394238 * __this, int32_t ___startPos0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		String_t* L_0 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m2979997331(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0016;
	}

IL_0011:
	{
		int32_t L_2 = ___startPos0;
		___startPos0 = ((int32_t)((int32_t)L_2+(int32_t)1));
	}

IL_0016:
	{
		int32_t L_3 = ___startPos0;
		int32_t L_4 = V_0;
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		String_t* L_5 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_6 = ___startPos0;
		NullCheck(L_5);
		Il2CppChar L_7 = String_get_Chars_m3015341861(L_5, L_6, /*hidden argument*/NULL);
		bool L_8 = TextEditor_isLetterLikeChar_m985832713(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0011;
		}
	}

IL_0033:
	{
		goto IL_003d;
	}

IL_0038:
	{
		int32_t L_9 = ___startPos0;
		___startPos0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_003d:
	{
		int32_t L_10 = ___startPos0;
		int32_t L_11 = V_0;
		if ((((int32_t)L_10) >= ((int32_t)L_11)))
		{
			goto IL_005a;
		}
	}
	{
		String_t* L_12 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_13 = ___startPos0;
		NullCheck(L_12);
		Il2CppChar L_14 = String_get_Chars_m3015341861(L_12, L_13, /*hidden argument*/NULL);
		bool L_15 = TextEditor_isLetterLikeChar_m985832713(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_0038;
		}
	}

IL_005a:
	{
		int32_t L_16 = ___startPos0;
		return L_16;
	}
}
// System.Boolean UnityEngine.TextEditor::isLetterLikeChar(System.Char)
extern Il2CppClass* Char_t2862622538_il2cpp_TypeInfo_var;
extern const uint32_t TextEditor_isLetterLikeChar_m985832713_MetadataUsageId;
extern "C"  bool TextEditor_isLetterLikeChar_m985832713 (Il2CppObject * __this /* static, unused */, Il2CppChar ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_isLetterLikeChar_m985832713_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		Il2CppChar L_0 = ___c0;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2862622538_il2cpp_TypeInfo_var);
		bool L_1 = Char_IsLetterOrDigit_m2290383044(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		Il2CppChar L_2 = ___c0;
		G_B3_0 = ((((int32_t)L_2) == ((int32_t)((int32_t)39)))? 1 : 0);
		goto IL_0013;
	}

IL_0012:
	{
		G_B3_0 = 1;
	}

IL_0013:
	{
		return (bool)G_B3_0;
	}
}
// System.Int32 UnityEngine.TextEditor::FindPrevSeperator(System.Int32)
extern "C"  int32_t TextEditor_FindPrevSeperator_m1092722199 (TextEditor_t319394238 * __this, int32_t ___startPos0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___startPos0;
		___startPos0 = ((int32_t)((int32_t)L_0-(int32_t)1));
		goto IL_000f;
	}

IL_000a:
	{
		int32_t L_1 = ___startPos0;
		___startPos0 = ((int32_t)((int32_t)L_1-(int32_t)1));
	}

IL_000f:
	{
		int32_t L_2 = ___startPos0;
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_002c;
		}
	}
	{
		String_t* L_3 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_4 = ___startPos0;
		NullCheck(L_3);
		Il2CppChar L_5 = String_get_Chars_m3015341861(L_3, L_4, /*hidden argument*/NULL);
		bool L_6 = TextEditor_isLetterLikeChar_m985832713(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_000a;
		}
	}

IL_002c:
	{
		goto IL_0036;
	}

IL_0031:
	{
		int32_t L_7 = ___startPos0;
		___startPos0 = ((int32_t)((int32_t)L_7-(int32_t)1));
	}

IL_0036:
	{
		int32_t L_8 = ___startPos0;
		if ((((int32_t)L_8) < ((int32_t)0)))
		{
			goto IL_0053;
		}
	}
	{
		String_t* L_9 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_10 = ___startPos0;
		NullCheck(L_9);
		Il2CppChar L_11 = String_get_Chars_m3015341861(L_9, L_10, /*hidden argument*/NULL);
		bool L_12 = TextEditor_isLetterLikeChar_m985832713(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0031;
		}
	}

IL_0053:
	{
		int32_t L_13 = ___startPos0;
		return ((int32_t)((int32_t)L_13+(int32_t)1));
	}
}
// System.Void UnityEngine.TextEditor::MoveWordRight()
extern "C"  void TextEditor_MoveWordRight_m1611060338 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	TextEditor_t319394238 * G_B2_0 = NULL;
	TextEditor_t319394238 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	TextEditor_t319394238 * G_B3_1 = NULL;
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			G_B2_0 = __this;
			goto IL_001d;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0023;
	}

IL_001d:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_0023:
	{
		NullCheck(G_B3_1);
		TextEditor_set_cursorIndex_m3839283393(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		int32_t L_4 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_FindNextSeperator_m2382945623(__this, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		int32_t L_6 = V_0;
		TextEditor_set_selectIndex_m2271285595(__this, L_6, /*hidden argument*/NULL);
		int32_t L_7 = V_0;
		TextEditor_set_cursorIndex_m3839283393(__this, L_7, /*hidden argument*/NULL);
		TextEditor_ClearCursorPos_m3987638978(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveToStartOfNextWord()
extern "C"  void TextEditor_MoveToStartOfNextWord_m1275371419 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_ClearCursorPos_m3987638978(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_001e;
		}
	}
	{
		TextEditor_MoveRight_m4087700060(__this, /*hidden argument*/NULL);
		return;
	}

IL_001e:
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_FindStartOfNextWord_m4132487039(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		TextEditor_set_selectIndex_m2271285595(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		TextEditor_set_cursorIndex_m3839283393(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveToEndOfPreviousWord()
extern "C"  void TextEditor_MoveToEndOfPreviousWord_m4107655576 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_ClearCursorPos_m3987638978(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_001e;
		}
	}
	{
		TextEditor_MoveLeft_m2173129001(__this, /*hidden argument*/NULL);
		return;
	}

IL_001e:
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_FindEndOfPreviousWord_m2038394172(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		TextEditor_set_selectIndex_m2271285595(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		TextEditor_set_cursorIndex_m3839283393(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectToStartOfNextWord()
extern "C"  void TextEditor_SelectToStartOfNextWord_m2771612816 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	{
		TextEditor_ClearCursorPos_m3987638978(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_FindStartOfNextWord_m4132487039(__this, L_0, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectToEndOfPreviousWord()
extern "C"  void TextEditor_SelectToEndOfPreviousWord_m3181593933 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	{
		TextEditor_ClearCursorPos_m3987638978(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_FindEndOfPreviousWord_m2038394172(__this, L_0, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.TextEditor/CharacterType UnityEngine.TextEditor::ClassifyChar(System.Char)
extern Il2CppClass* Char_t2862622538_il2cpp_TypeInfo_var;
extern const uint32_t TextEditor_ClassifyChar_m3839786120_MetadataUsageId;
extern "C"  int32_t TextEditor_ClassifyChar_m3839786120 (TextEditor_t319394238 * __this, Il2CppChar ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_ClassifyChar_m3839786120_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppChar L_0 = ___c0;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2862622538_il2cpp_TypeInfo_var);
		bool L_1 = Char_IsWhiteSpace_m2745315955(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return (int32_t)(3);
	}

IL_000d:
	{
		Il2CppChar L_2 = ___c0;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2862622538_il2cpp_TypeInfo_var);
		bool L_3 = Char_IsLetterOrDigit_m2290383044(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0020;
		}
	}
	{
		Il2CppChar L_4 = ___c0;
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)39)))))
		{
			goto IL_0022;
		}
	}

IL_0020:
	{
		return (int32_t)(0);
	}

IL_0022:
	{
		return (int32_t)(1);
	}
}
// System.Int32 UnityEngine.TextEditor::FindStartOfNextWord(System.Int32)
extern Il2CppClass* Char_t2862622538_il2cpp_TypeInfo_var;
extern const uint32_t TextEditor_FindStartOfNextWord_m4132487039_MetadataUsageId;
extern "C"  int32_t TextEditor_FindStartOfNextWord_m4132487039 (TextEditor_t319394238 * __this, int32_t ___p0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_FindStartOfNextWord_m4132487039_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Il2CppChar V_1 = 0x0;
	int32_t V_2 = 0;
	{
		String_t* L_0 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m2979997331(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = ___p0;
		int32_t L_3 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_4 = ___p0;
		return L_4;
	}

IL_0015:
	{
		String_t* L_5 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_6 = ___p0;
		NullCheck(L_5);
		Il2CppChar L_7 = String_get_Chars_m3015341861(L_5, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		Il2CppChar L_8 = V_1;
		int32_t L_9 = TextEditor_ClassifyChar_m3839786120(__this, L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		int32_t L_10 = V_2;
		if ((((int32_t)L_10) == ((int32_t)3)))
		{
			goto IL_0064;
		}
	}
	{
		int32_t L_11 = ___p0;
		___p0 = ((int32_t)((int32_t)L_11+(int32_t)1));
		goto IL_0040;
	}

IL_003b:
	{
		int32_t L_12 = ___p0;
		___p0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0040:
	{
		int32_t L_13 = ___p0;
		int32_t L_14 = V_0;
		if ((((int32_t)L_13) >= ((int32_t)L_14)))
		{
			goto IL_005f;
		}
	}
	{
		String_t* L_15 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_16 = ___p0;
		NullCheck(L_15);
		Il2CppChar L_17 = String_get_Chars_m3015341861(L_15, L_16, /*hidden argument*/NULL);
		int32_t L_18 = TextEditor_ClassifyChar_m3839786120(__this, L_17, /*hidden argument*/NULL);
		int32_t L_19 = V_2;
		if ((((int32_t)L_18) == ((int32_t)L_19)))
		{
			goto IL_003b;
		}
	}

IL_005f:
	{
		goto IL_0078;
	}

IL_0064:
	{
		Il2CppChar L_20 = V_1;
		if ((((int32_t)L_20) == ((int32_t)((int32_t)9))))
		{
			goto IL_0074;
		}
	}
	{
		Il2CppChar L_21 = V_1;
		if ((!(((uint32_t)L_21) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0078;
		}
	}

IL_0074:
	{
		int32_t L_22 = ___p0;
		return ((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0078:
	{
		int32_t L_23 = ___p0;
		int32_t L_24 = V_0;
		if ((!(((uint32_t)L_23) == ((uint32_t)L_24))))
		{
			goto IL_0081;
		}
	}
	{
		int32_t L_25 = ___p0;
		return L_25;
	}

IL_0081:
	{
		String_t* L_26 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_27 = ___p0;
		NullCheck(L_26);
		Il2CppChar L_28 = String_get_Chars_m3015341861(L_26, L_27, /*hidden argument*/NULL);
		V_1 = L_28;
		Il2CppChar L_29 = V_1;
		if ((!(((uint32_t)L_29) == ((uint32_t)((int32_t)32)))))
		{
			goto IL_00c2;
		}
	}
	{
		goto IL_00a0;
	}

IL_009b:
	{
		int32_t L_30 = ___p0;
		___p0 = ((int32_t)((int32_t)L_30+(int32_t)1));
	}

IL_00a0:
	{
		int32_t L_31 = ___p0;
		int32_t L_32 = V_0;
		if ((((int32_t)L_31) >= ((int32_t)L_32)))
		{
			goto IL_00bd;
		}
	}
	{
		String_t* L_33 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_34 = ___p0;
		NullCheck(L_33);
		Il2CppChar L_35 = String_get_Chars_m3015341861(L_33, L_34, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2862622538_il2cpp_TypeInfo_var);
		bool L_36 = Char_IsWhiteSpace_m2745315955(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		if (L_36)
		{
			goto IL_009b;
		}
	}

IL_00bd:
	{
		goto IL_00d4;
	}

IL_00c2:
	{
		Il2CppChar L_37 = V_1;
		if ((((int32_t)L_37) == ((int32_t)((int32_t)9))))
		{
			goto IL_00d2;
		}
	}
	{
		Il2CppChar L_38 = V_1;
		if ((!(((uint32_t)L_38) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_00d4;
		}
	}

IL_00d2:
	{
		int32_t L_39 = ___p0;
		return L_39;
	}

IL_00d4:
	{
		int32_t L_40 = ___p0;
		return L_40;
	}
}
// System.Int32 UnityEngine.TextEditor::FindEndOfPreviousWord(System.Int32)
extern "C"  int32_t TextEditor_FindEndOfPreviousWord_m2038394172 (TextEditor_t319394238 * __this, int32_t ___p0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___p0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		int32_t L_1 = ___p0;
		return L_1;
	}

IL_0008:
	{
		int32_t L_2 = ___p0;
		___p0 = ((int32_t)((int32_t)L_2-(int32_t)1));
		goto IL_0017;
	}

IL_0012:
	{
		int32_t L_3 = ___p0;
		___p0 = ((int32_t)((int32_t)L_3-(int32_t)1));
	}

IL_0017:
	{
		int32_t L_4 = ___p0;
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0031;
		}
	}
	{
		String_t* L_5 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_6 = ___p0;
		NullCheck(L_5);
		Il2CppChar L_7 = String_get_Chars_m3015341861(L_5, L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_7) == ((int32_t)((int32_t)32))))
		{
			goto IL_0012;
		}
	}

IL_0031:
	{
		String_t* L_8 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_9 = ___p0;
		NullCheck(L_8);
		Il2CppChar L_10 = String_get_Chars_m3015341861(L_8, L_9, /*hidden argument*/NULL);
		int32_t L_11 = TextEditor_ClassifyChar_m3839786120(__this, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		int32_t L_12 = V_0;
		if ((((int32_t)L_12) == ((int32_t)3)))
		{
			goto IL_0076;
		}
	}
	{
		goto IL_0055;
	}

IL_0050:
	{
		int32_t L_13 = ___p0;
		___p0 = ((int32_t)((int32_t)L_13-(int32_t)1));
	}

IL_0055:
	{
		int32_t L_14 = ___p0;
		if ((((int32_t)L_14) <= ((int32_t)0)))
		{
			goto IL_0076;
		}
	}
	{
		String_t* L_15 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_16 = ___p0;
		NullCheck(L_15);
		Il2CppChar L_17 = String_get_Chars_m3015341861(L_15, ((int32_t)((int32_t)L_16-(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_18 = TextEditor_ClassifyChar_m3839786120(__this, L_17, /*hidden argument*/NULL);
		int32_t L_19 = V_0;
		if ((((int32_t)L_18) == ((int32_t)L_19)))
		{
			goto IL_0050;
		}
	}

IL_0076:
	{
		int32_t L_20 = ___p0;
		return L_20;
	}
}
// System.Void UnityEngine.TextEditor::MoveWordLeft()
extern "C"  void TextEditor_MoveWordLeft_m4171447379 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	TextEditor_t319394238 * G_B2_0 = NULL;
	TextEditor_t319394238 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	TextEditor_t319394238 * G_B3_1 = NULL;
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			G_B2_0 = __this;
			goto IL_001d;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0023;
	}

IL_001d:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_0023:
	{
		NullCheck(G_B3_1);
		TextEditor_set_cursorIndex_m3839283393(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		int32_t L_4 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_FindPrevSeperator_m1092722199(__this, L_4, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, L_5, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2271285595(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectWordRight()
extern "C"  void TextEditor_SelectWordRight_m1449160295 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	TextEditor_t319394238 * G_B3_0 = NULL;
	TextEditor_t319394238 * G_B2_0 = NULL;
	int32_t G_B4_0 = 0;
	TextEditor_t319394238 * G_B4_1 = NULL;
	{
		TextEditor_ClearCursorPos_m3987638978(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_2 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) >= ((int32_t)L_2)))
		{
			goto IL_0060;
		}
	}
	{
		int32_t L_3 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2271285595(__this, L_3, /*hidden argument*/NULL);
		TextEditor_MoveWordRight_m1611060338(__this, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		TextEditor_set_selectIndex_m2271285595(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		G_B2_0 = __this;
		if ((((int32_t)L_5) >= ((int32_t)L_6)))
		{
			G_B3_0 = __this;
			goto IL_0054;
		}
	}
	{
		int32_t L_7 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		G_B4_0 = L_7;
		G_B4_1 = G_B2_0;
		goto IL_005a;
	}

IL_0054:
	{
		int32_t L_8 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		G_B4_0 = L_8;
		G_B4_1 = G_B3_0;
	}

IL_005a:
	{
		NullCheck(G_B4_1);
		TextEditor_set_cursorIndex_m3839283393(G_B4_1, G_B4_0, /*hidden argument*/NULL);
		return;
	}

IL_0060:
	{
		int32_t L_9 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2271285595(__this, L_9, /*hidden argument*/NULL);
		TextEditor_MoveWordRight_m1611060338(__this, /*hidden argument*/NULL);
		int32_t L_10 = V_0;
		TextEditor_set_selectIndex_m2271285595(__this, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectWordLeft()
extern "C"  void TextEditor_SelectWordLeft_m979636158 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	TextEditor_t319394238 * G_B3_0 = NULL;
	TextEditor_t319394238 * G_B2_0 = NULL;
	int32_t G_B4_0 = 0;
	TextEditor_t319394238 * G_B4_1 = NULL;
	{
		TextEditor_ClearCursorPos_m3987638978(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_2 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)L_2)))
		{
			goto IL_0060;
		}
	}
	{
		int32_t L_3 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2271285595(__this, L_3, /*hidden argument*/NULL);
		TextEditor_MoveWordLeft_m4171447379(__this, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		TextEditor_set_selectIndex_m2271285595(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		G_B2_0 = __this;
		if ((((int32_t)L_5) <= ((int32_t)L_6)))
		{
			G_B3_0 = __this;
			goto IL_0054;
		}
	}
	{
		int32_t L_7 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		G_B4_0 = L_7;
		G_B4_1 = G_B2_0;
		goto IL_005a;
	}

IL_0054:
	{
		int32_t L_8 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		G_B4_0 = L_8;
		G_B4_1 = G_B3_0;
	}

IL_005a:
	{
		NullCheck(G_B4_1);
		TextEditor_set_cursorIndex_m3839283393(G_B4_1, G_B4_0, /*hidden argument*/NULL);
		return;
	}

IL_0060:
	{
		int32_t L_9 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2271285595(__this, L_9, /*hidden argument*/NULL);
		TextEditor_MoveWordLeft_m4171447379(__this, /*hidden argument*/NULL);
		int32_t L_10 = V_0;
		TextEditor_set_selectIndex_m2271285595(__this, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::ExpandSelectGraphicalLineStart()
extern "C"  void TextEditor_ExpandSelectGraphicalLineStart_m4278286146 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_ClearCursorPos_m3987638978(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_GetGraphicalLineStart_m2255929555(__this, L_2, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, L_3, /*hidden argument*/NULL);
		goto IL_004e;
	}

IL_002e:
	{
		int32_t L_4 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_GetGraphicalLineStart_m2255929555(__this, L_5, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, L_6, /*hidden argument*/NULL);
		int32_t L_7 = V_0;
		TextEditor_set_selectIndex_m2271285595(__this, L_7, /*hidden argument*/NULL);
	}

IL_004e:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::ExpandSelectGraphicalLineEnd()
extern "C"  void TextEditor_ExpandSelectGraphicalLineEnd_m885198139 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_ClearCursorPos_m3987638978(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_GetGraphicalLineEnd_m3638753612(__this, L_2, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, L_3, /*hidden argument*/NULL);
		goto IL_004e;
	}

IL_002e:
	{
		int32_t L_4 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_GetGraphicalLineEnd_m3638753612(__this, L_5, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, L_6, /*hidden argument*/NULL);
		int32_t L_7 = V_0;
		TextEditor_set_selectIndex_m2271285595(__this, L_7, /*hidden argument*/NULL);
	}

IL_004e:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectGraphicalLineStart()
extern "C"  void TextEditor_SelectGraphicalLineStart_m348597512 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	{
		TextEditor_ClearCursorPos_m3987638978(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_GetGraphicalLineStart_m2255929555(__this, L_0, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectGraphicalLineEnd()
extern "C"  void TextEditor_SelectGraphicalLineEnd_m31947905 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	{
		TextEditor_ClearCursorPos_m3987638978(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_GetGraphicalLineEnd_m3638753612(__this, L_0, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectParagraphForward()
extern "C"  void TextEditor_SelectParagraphForward_m2310606212 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		TextEditor_ClearCursorPos_m3987638978(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_0) < ((int32_t)L_1))? 1 : 0);
		int32_t L_2 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		String_t* L_3 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m2979997331(L_3, /*hidden argument*/NULL);
		if ((((int32_t)L_2) >= ((int32_t)L_4)))
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_5 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_IndexOfEndOfLine_m1755139103(__this, ((int32_t)((int32_t)L_5+(int32_t)1)), /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, L_6, /*hidden argument*/NULL);
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_8 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_9 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_8) <= ((int32_t)L_9)))
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_10 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, L_10, /*hidden argument*/NULL);
	}

IL_0062:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectParagraphBackward()
extern "C"  void TextEditor_SelectParagraphBackward_m1412083942 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t V_1 = 0;
	{
		TextEditor_ClearCursorPos_m3987638978(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_0) > ((int32_t)L_1))? 1 : 0);
		int32_t L_2 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_0066;
		}
	}
	{
		String_t* L_3 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_4 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_5 = String_LastIndexOf_m902083627(L_3, ((int32_t)10), ((int32_t)((int32_t)L_4-(int32_t)2)), /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, ((int32_t)((int32_t)L_5+(int32_t)1)), /*hidden argument*/NULL);
		bool L_6 = V_0;
		if (!L_6)
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_7 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_8 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_7) >= ((int32_t)L_8)))
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_9 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, L_9, /*hidden argument*/NULL);
	}

IL_0061:
	{
		goto IL_0076;
	}

IL_0066:
	{
		V_1 = 0;
		int32_t L_10 = V_1;
		TextEditor_set_cursorIndex_m3839283393(__this, L_10, /*hidden argument*/NULL);
		int32_t L_11 = V_1;
		TextEditor_set_selectIndex_m2271285595(__this, L_11, /*hidden argument*/NULL);
	}

IL_0076:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectCurrentWord()
extern "C"  void TextEditor_SelectCurrentWord_m3270822936 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_ClearCursorPos_m3987638978(__this, /*hidden argument*/NULL);
		String_t* L_0 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m2979997331(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2271285595(__this, L_2, /*hidden argument*/NULL);
		int32_t L_3 = V_0;
		if (L_3)
		{
			goto IL_0025;
		}
	}
	{
		return;
	}

IL_0025:
	{
		int32_t L_4 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		if ((((int32_t)L_4) < ((int32_t)L_5)))
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_6 = V_0;
		TextEditor_set_cursorIndex_m3839283393(__this, ((int32_t)((int32_t)L_6-(int32_t)1)), /*hidden argument*/NULL);
	}

IL_003a:
	{
		int32_t L_7 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		int32_t L_8 = V_0;
		if ((((int32_t)L_7) < ((int32_t)L_8)))
		{
			goto IL_0054;
		}
	}
	{
		int32_t L_9 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2271285595(__this, ((int32_t)((int32_t)L_9-(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0054:
	{
		int32_t L_10 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_11 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_10) >= ((int32_t)L_11)))
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_12 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_13 = TextEditor_FindEndOfClassification_m3906171638(__this, L_12, (-1), /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, L_13, /*hidden argument*/NULL);
		int32_t L_14 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		int32_t L_15 = TextEditor_FindEndOfClassification_m3906171638(__this, L_14, 1, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2271285595(__this, L_15, /*hidden argument*/NULL);
		goto IL_00b6;
	}

IL_0090:
	{
		int32_t L_16 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_17 = TextEditor_FindEndOfClassification_m3906171638(__this, L_16, 1, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, L_17, /*hidden argument*/NULL);
		int32_t L_18 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		int32_t L_19 = TextEditor_FindEndOfClassification_m3906171638(__this, L_18, (-1), /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2271285595(__this, L_19, /*hidden argument*/NULL);
	}

IL_00b6:
	{
		__this->set_m_bJustSelected_18((bool)1);
		return;
	}
}
// System.Int32 UnityEngine.TextEditor::FindEndOfClassification(System.Int32,System.Int32)
extern "C"  int32_t TextEditor_FindEndOfClassification_m3906171638 (TextEditor_t319394238 * __this, int32_t ___p0, int32_t ___dir1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		String_t* L_0 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m2979997331(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = ___p0;
		int32_t L_3 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)L_3)))
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_4 = ___p0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001c;
		}
	}

IL_001a:
	{
		int32_t L_5 = ___p0;
		return L_5;
	}

IL_001c:
	{
		String_t* L_6 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_7 = ___p0;
		NullCheck(L_6);
		Il2CppChar L_8 = String_get_Chars_m3015341861(L_6, L_7, /*hidden argument*/NULL);
		int32_t L_9 = TextEditor_ClassifyChar_m3839786120(__this, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
	}

IL_002f:
	{
		int32_t L_10 = ___p0;
		int32_t L_11 = ___dir1;
		___p0 = ((int32_t)((int32_t)L_10+(int32_t)L_11));
		int32_t L_12 = ___p0;
		if ((((int32_t)L_12) >= ((int32_t)0)))
		{
			goto IL_003d;
		}
	}
	{
		return 0;
	}

IL_003d:
	{
		int32_t L_13 = ___p0;
		int32_t L_14 = V_0;
		if ((((int32_t)L_13) < ((int32_t)L_14)))
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_15 = V_0;
		return L_15;
	}

IL_0046:
	{
		String_t* L_16 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_17 = ___p0;
		NullCheck(L_16);
		Il2CppChar L_18 = String_get_Chars_m3015341861(L_16, L_17, /*hidden argument*/NULL);
		int32_t L_19 = TextEditor_ClassifyChar_m3839786120(__this, L_18, /*hidden argument*/NULL);
		int32_t L_20 = V_1;
		if ((((int32_t)L_19) == ((int32_t)L_20)))
		{
			goto IL_002f;
		}
	}
	{
		int32_t L_21 = ___dir1;
		if ((!(((uint32_t)L_21) == ((uint32_t)1))))
		{
			goto IL_0067;
		}
	}
	{
		int32_t L_22 = ___p0;
		return L_22;
	}

IL_0067:
	{
		int32_t L_23 = ___p0;
		return ((int32_t)((int32_t)L_23+(int32_t)1));
	}
}
// System.Void UnityEngine.TextEditor::SelectCurrentParagraph()
extern "C"  void TextEditor_SelectCurrentParagraph_m469508226 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_ClearCursorPos_m3987638978(__this, /*hidden argument*/NULL);
		String_t* L_0 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m2979997331(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_3 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)L_3)))
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_4 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_IndexOfEndOfLine_m1755139103(__this, L_4, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m3839283393(__this, ((int32_t)((int32_t)L_5+(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0032:
	{
		int32_t L_6 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_005a;
		}
	}
	{
		String_t* L_7 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_8 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		int32_t L_9 = String_LastIndexOf_m902083627(L_7, ((int32_t)10), ((int32_t)((int32_t)L_8-(int32_t)1)), /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2271285595(__this, ((int32_t)((int32_t)L_9+(int32_t)1)), /*hidden argument*/NULL);
	}

IL_005a:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::UpdateScrollOffsetIfNeeded()
extern "C"  void TextEditor_UpdateScrollOffsetIfNeeded_m3936207980 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	{
		Event_t4196595728 * L_0 = Event_get_current_m238587645(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m2209939250(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)7)))
		{
			goto IL_0026;
		}
	}
	{
		Event_t4196595728 * L_2 = Event_get_current_m238587645(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Event_get_type_m2209939250(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_3) == ((int32_t)8)))
		{
			goto IL_0026;
		}
	}
	{
		TextEditor_UpdateScrollOffset_m3069173402(__this, /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::UpdateScrollOffset()
extern "C"  void TextEditor_UpdateScrollOffset_m3069173402 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Rect_t4241904616  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t4282066565  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t4241904616  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t4241904616  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector2_t4282066565  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Rect_t4241904616  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Rect_t4241904616  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector2_t4282066565 * G_B19_0 = NULL;
	Vector2_t4282066565 * G_B18_0 = NULL;
	float G_B20_0 = 0.0f;
	Vector2_t4282066565 * G_B20_1 = NULL;
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		GUIStyle_t2990928826 * L_1 = __this->get_style_2();
		Rect_t4241904616  L_2 = TextEditor_get_position_m2783529335(__this, /*hidden argument*/NULL);
		V_3 = L_2;
		float L_3 = Rect_get_width_m2824209432((&V_3), /*hidden argument*/NULL);
		Rect_t4241904616  L_4 = TextEditor_get_position_m2783529335(__this, /*hidden argument*/NULL);
		V_4 = L_4;
		float L_5 = Rect_get_height_m2154960823((&V_4), /*hidden argument*/NULL);
		Rect_t4241904616  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Rect__ctor_m3291325233(&L_6, (0.0f), (0.0f), L_3, L_5, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_7 = __this->get_m_Content_8();
		int32_t L_8 = V_0;
		NullCheck(L_1);
		Vector2_t4282066565  L_9 = GUIStyle_GetCursorPixelPosition_m296157110(L_1, L_6, L_7, L_8, /*hidden argument*/NULL);
		__this->set_graphicalCursorPos_13(L_9);
		GUIStyle_t2990928826 * L_10 = __this->get_style_2();
		NullCheck(L_10);
		RectOffset_t3056157787 * L_11 = GUIStyle_get_padding_m3072941276(L_10, /*hidden argument*/NULL);
		Rect_t4241904616  L_12 = TextEditor_get_position_m2783529335(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Rect_t4241904616  L_13 = RectOffset_Remove_m843726027(L_11, L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		GUIStyle_t2990928826 * L_14 = __this->get_style_2();
		GUIContent_t2094828418 * L_15 = __this->get_m_Content_8();
		NullCheck(L_14);
		Vector2_t4282066565  L_16 = GUIStyle_CalcSize_m2299809257(L_14, L_15, /*hidden argument*/NULL);
		V_5 = L_16;
		float L_17 = (&V_5)->get_x_1();
		GUIStyle_t2990928826 * L_18 = __this->get_style_2();
		GUIContent_t2094828418 * L_19 = __this->get_m_Content_8();
		Rect_t4241904616  L_20 = TextEditor_get_position_m2783529335(__this, /*hidden argument*/NULL);
		V_6 = L_20;
		float L_21 = Rect_get_width_m2824209432((&V_6), /*hidden argument*/NULL);
		NullCheck(L_18);
		float L_22 = GUIStyle_CalcHeight_m3565424293(L_18, L_19, L_21, /*hidden argument*/NULL);
		Vector2__ctor_m1517109030((&V_2), L_17, L_22, /*hidden argument*/NULL);
		float L_23 = (&V_2)->get_x_1();
		Rect_t4241904616  L_24 = TextEditor_get_position_m2783529335(__this, /*hidden argument*/NULL);
		V_7 = L_24;
		float L_25 = Rect_get_width_m2824209432((&V_7), /*hidden argument*/NULL);
		if ((!(((float)L_23) < ((float)L_25))))
		{
			goto IL_00d3;
		}
	}
	{
		Vector2_t4282066565 * L_26 = __this->get_address_of_scrollOffset_7();
		L_26->set_x_1((0.0f));
		goto IL_017a;
	}

IL_00d3:
	{
		bool L_27 = __this->get_m_RevealCursor_12();
		if (!L_27)
		{
			goto IL_017a;
		}
	}
	{
		Vector2_t4282066565 * L_28 = __this->get_address_of_graphicalCursorPos_13();
		float L_29 = L_28->get_x_1();
		Vector2_t4282066565 * L_30 = __this->get_address_of_scrollOffset_7();
		float L_31 = L_30->get_x_1();
		float L_32 = Rect_get_width_m2824209432((&V_1), /*hidden argument*/NULL);
		if ((!(((float)((float)((float)L_29+(float)(1.0f)))) > ((float)((float)((float)L_31+(float)L_32))))))
		{
			goto IL_0125;
		}
	}
	{
		Vector2_t4282066565 * L_33 = __this->get_address_of_scrollOffset_7();
		Vector2_t4282066565 * L_34 = __this->get_address_of_graphicalCursorPos_13();
		float L_35 = L_34->get_x_1();
		float L_36 = Rect_get_width_m2824209432((&V_1), /*hidden argument*/NULL);
		L_33->set_x_1(((float)((float)L_35-(float)L_36)));
	}

IL_0125:
	{
		Vector2_t4282066565 * L_37 = __this->get_address_of_graphicalCursorPos_13();
		float L_38 = L_37->get_x_1();
		Vector2_t4282066565 * L_39 = __this->get_address_of_scrollOffset_7();
		float L_40 = L_39->get_x_1();
		GUIStyle_t2990928826 * L_41 = __this->get_style_2();
		NullCheck(L_41);
		RectOffset_t3056157787 * L_42 = GUIStyle_get_padding_m3072941276(L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		int32_t L_43 = RectOffset_get_left_m4104523390(L_42, /*hidden argument*/NULL);
		if ((!(((float)L_38) < ((float)((float)((float)L_40+(float)(((float)((float)L_43)))))))))
		{
			goto IL_017a;
		}
	}
	{
		Vector2_t4282066565 * L_44 = __this->get_address_of_scrollOffset_7();
		Vector2_t4282066565 * L_45 = __this->get_address_of_graphicalCursorPos_13();
		float L_46 = L_45->get_x_1();
		GUIStyle_t2990928826 * L_47 = __this->get_style_2();
		NullCheck(L_47);
		RectOffset_t3056157787 * L_48 = GUIStyle_get_padding_m3072941276(L_47, /*hidden argument*/NULL);
		NullCheck(L_48);
		int32_t L_49 = RectOffset_get_left_m4104523390(L_48, /*hidden argument*/NULL);
		L_44->set_x_1(((float)((float)L_46-(float)(((float)((float)L_49))))));
	}

IL_017a:
	{
		float L_50 = (&V_2)->get_y_2();
		float L_51 = Rect_get_height_m2154960823((&V_1), /*hidden argument*/NULL);
		if ((!(((float)L_50) < ((float)L_51))))
		{
			goto IL_01a2;
		}
	}
	{
		Vector2_t4282066565 * L_52 = __this->get_address_of_scrollOffset_7();
		L_52->set_y_2((0.0f));
		goto IL_027f;
	}

IL_01a2:
	{
		bool L_53 = __this->get_m_RevealCursor_12();
		if (!L_53)
		{
			goto IL_027f;
		}
	}
	{
		Vector2_t4282066565 * L_54 = __this->get_address_of_graphicalCursorPos_13();
		float L_55 = L_54->get_y_2();
		GUIStyle_t2990928826 * L_56 = __this->get_style_2();
		NullCheck(L_56);
		float L_57 = GUIStyle_get_lineHeight_m3977255453(L_56, /*hidden argument*/NULL);
		Vector2_t4282066565 * L_58 = __this->get_address_of_scrollOffset_7();
		float L_59 = L_58->get_y_2();
		float L_60 = Rect_get_height_m2154960823((&V_1), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_61 = __this->get_style_2();
		NullCheck(L_61);
		RectOffset_t3056157787 * L_62 = GUIStyle_get_padding_m3072941276(L_61, /*hidden argument*/NULL);
		NullCheck(L_62);
		int32_t L_63 = RectOffset_get_top_m140097312(L_62, /*hidden argument*/NULL);
		if ((!(((float)((float)((float)L_55+(float)L_57))) > ((float)((float)((float)((float)((float)L_59+(float)L_60))+(float)(((float)((float)L_63)))))))))
		{
			goto IL_022a;
		}
	}
	{
		Vector2_t4282066565 * L_64 = __this->get_address_of_scrollOffset_7();
		Vector2_t4282066565 * L_65 = __this->get_address_of_graphicalCursorPos_13();
		float L_66 = L_65->get_y_2();
		float L_67 = Rect_get_height_m2154960823((&V_1), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_68 = __this->get_style_2();
		NullCheck(L_68);
		RectOffset_t3056157787 * L_69 = GUIStyle_get_padding_m3072941276(L_68, /*hidden argument*/NULL);
		NullCheck(L_69);
		int32_t L_70 = RectOffset_get_top_m140097312(L_69, /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_71 = __this->get_style_2();
		NullCheck(L_71);
		float L_72 = GUIStyle_get_lineHeight_m3977255453(L_71, /*hidden argument*/NULL);
		L_64->set_y_2(((float)((float)((float)((float)((float)((float)L_66-(float)L_67))-(float)(((float)((float)L_70)))))+(float)L_72)));
	}

IL_022a:
	{
		Vector2_t4282066565 * L_73 = __this->get_address_of_graphicalCursorPos_13();
		float L_74 = L_73->get_y_2();
		Vector2_t4282066565 * L_75 = __this->get_address_of_scrollOffset_7();
		float L_76 = L_75->get_y_2();
		GUIStyle_t2990928826 * L_77 = __this->get_style_2();
		NullCheck(L_77);
		RectOffset_t3056157787 * L_78 = GUIStyle_get_padding_m3072941276(L_77, /*hidden argument*/NULL);
		NullCheck(L_78);
		int32_t L_79 = RectOffset_get_top_m140097312(L_78, /*hidden argument*/NULL);
		if ((!(((float)L_74) < ((float)((float)((float)L_76+(float)(((float)((float)L_79)))))))))
		{
			goto IL_027f;
		}
	}
	{
		Vector2_t4282066565 * L_80 = __this->get_address_of_scrollOffset_7();
		Vector2_t4282066565 * L_81 = __this->get_address_of_graphicalCursorPos_13();
		float L_82 = L_81->get_y_2();
		GUIStyle_t2990928826 * L_83 = __this->get_style_2();
		NullCheck(L_83);
		RectOffset_t3056157787 * L_84 = GUIStyle_get_padding_m3072941276(L_83, /*hidden argument*/NULL);
		NullCheck(L_84);
		int32_t L_85 = RectOffset_get_top_m140097312(L_84, /*hidden argument*/NULL);
		L_80->set_y_2(((float)((float)L_82-(float)(((float)((float)L_85))))));
	}

IL_027f:
	{
		Vector2_t4282066565 * L_86 = __this->get_address_of_scrollOffset_7();
		float L_87 = L_86->get_y_2();
		if ((!(((float)L_87) > ((float)(0.0f)))))
		{
			goto IL_02f1;
		}
	}
	{
		float L_88 = (&V_2)->get_y_2();
		Vector2_t4282066565 * L_89 = __this->get_address_of_scrollOffset_7();
		float L_90 = L_89->get_y_2();
		float L_91 = Rect_get_height_m2154960823((&V_1), /*hidden argument*/NULL);
		if ((!(((float)((float)((float)L_88-(float)L_90))) < ((float)L_91))))
		{
			goto IL_02f1;
		}
	}
	{
		Vector2_t4282066565 * L_92 = __this->get_address_of_scrollOffset_7();
		float L_93 = (&V_2)->get_y_2();
		float L_94 = Rect_get_height_m2154960823((&V_1), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_95 = __this->get_style_2();
		NullCheck(L_95);
		RectOffset_t3056157787 * L_96 = GUIStyle_get_padding_m3072941276(L_95, /*hidden argument*/NULL);
		NullCheck(L_96);
		int32_t L_97 = RectOffset_get_top_m140097312(L_96, /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_98 = __this->get_style_2();
		NullCheck(L_98);
		RectOffset_t3056157787 * L_99 = GUIStyle_get_padding_m3072941276(L_98, /*hidden argument*/NULL);
		NullCheck(L_99);
		int32_t L_100 = RectOffset_get_bottom_m2106858018(L_99, /*hidden argument*/NULL);
		L_92->set_y_2(((float)((float)((float)((float)((float)((float)L_93-(float)L_94))-(float)(((float)((float)L_97)))))-(float)(((float)((float)L_100))))));
	}

IL_02f1:
	{
		Vector2_t4282066565 * L_101 = __this->get_address_of_scrollOffset_7();
		Vector2_t4282066565 * L_102 = __this->get_address_of_scrollOffset_7();
		float L_103 = L_102->get_y_2();
		G_B18_0 = L_101;
		if ((!(((float)L_103) < ((float)(0.0f)))))
		{
			G_B19_0 = L_101;
			goto IL_0316;
		}
	}
	{
		G_B20_0 = (0.0f);
		G_B20_1 = G_B18_0;
		goto IL_0321;
	}

IL_0316:
	{
		Vector2_t4282066565 * L_104 = __this->get_address_of_scrollOffset_7();
		float L_105 = L_104->get_y_2();
		G_B20_0 = L_105;
		G_B20_1 = G_B19_0;
	}

IL_0321:
	{
		G_B20_1->set_y_2(G_B20_0);
		__this->set_m_RevealCursor_12((bool)0);
		return;
	}
}
// System.Void UnityEngine.TextEditor::DrawCursor(System.String)
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t TextEditor_DrawCursor_m4225527287_MetadataUsageId;
extern "C"  void TextEditor_DrawCursor_m4225527287 (TextEditor_t319394238 * __this, String_t* ___newText0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_DrawCursor_m4225527287_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	Vector2_t4282066565  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t4241904616  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t4241904616  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Rect_t4241904616  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Rect_t4241904616  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		String_t* L_0 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		String_t* L_2 = Input_get_compositionString_m1541052002(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m2979997331(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_005e;
		}
	}
	{
		GUIContent_t2094828418 * L_4 = __this->get_m_Content_8();
		String_t* L_5 = ___newText0;
		int32_t L_6 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_7 = String_Substring_m675079568(L_5, 0, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		String_t* L_8 = Input_get_compositionString_m1541052002(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_9 = ___newText0;
		int32_t L_10 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		String_t* L_11 = String_Substring_m2809233063(L_9, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m1825781833(NULL /*static, unused*/, L_7, L_8, L_11, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIContent_set_text_m1575840163(L_4, L_12, /*hidden argument*/NULL);
		int32_t L_13 = V_1;
		String_t* L_14 = Input_get_compositionString_m1541052002(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_14);
		int32_t L_15 = String_get_Length_m2979997331(L_14, /*hidden argument*/NULL);
		V_1 = ((int32_t)((int32_t)L_13+(int32_t)L_15));
		goto IL_006a;
	}

IL_005e:
	{
		GUIContent_t2094828418 * L_16 = __this->get_m_Content_8();
		String_t* L_17 = ___newText0;
		NullCheck(L_16);
		GUIContent_set_text_m1575840163(L_16, L_17, /*hidden argument*/NULL);
	}

IL_006a:
	{
		GUIStyle_t2990928826 * L_18 = __this->get_style_2();
		Rect_t4241904616  L_19 = TextEditor_get_position_m2783529335(__this, /*hidden argument*/NULL);
		V_3 = L_19;
		float L_20 = Rect_get_width_m2824209432((&V_3), /*hidden argument*/NULL);
		Rect_t4241904616  L_21 = TextEditor_get_position_m2783529335(__this, /*hidden argument*/NULL);
		V_4 = L_21;
		float L_22 = Rect_get_height_m2154960823((&V_4), /*hidden argument*/NULL);
		Rect_t4241904616  L_23;
		memset(&L_23, 0, sizeof(L_23));
		Rect__ctor_m3291325233(&L_23, (0.0f), (0.0f), L_20, L_22, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_24 = __this->get_m_Content_8();
		int32_t L_25 = V_1;
		NullCheck(L_18);
		Vector2_t4282066565  L_26 = GUIStyle_GetCursorPixelPosition_m296157110(L_18, L_23, L_24, L_25, /*hidden argument*/NULL);
		__this->set_graphicalCursorPos_13(L_26);
		GUIStyle_t2990928826 * L_27 = __this->get_style_2();
		NullCheck(L_27);
		Vector2_t4282066565  L_28 = GUIStyle_get_contentOffset_m2092220605(L_27, /*hidden argument*/NULL);
		V_2 = L_28;
		GUIStyle_t2990928826 * L_29 = __this->get_style_2();
		GUIStyle_t2990928826 * L_30 = L_29;
		NullCheck(L_30);
		Vector2_t4282066565  L_31 = GUIStyle_get_contentOffset_m2092220605(L_30, /*hidden argument*/NULL);
		Vector2_t4282066565  L_32 = __this->get_scrollOffset_7();
		Vector2_t4282066565  L_33 = Vector2_op_Subtraction_m2097149401(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		GUIStyle_set_contentOffset_m3385864054(L_30, L_33, /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_34 = __this->get_style_2();
		Vector2_t4282066565  L_35 = __this->get_scrollOffset_7();
		NullCheck(L_34);
		GUIStyle_set_Internal_clipOffset_m411599389(L_34, L_35, /*hidden argument*/NULL);
		Vector2_t4282066565  L_36 = __this->get_graphicalCursorPos_13();
		Rect_t4241904616  L_37 = TextEditor_get_position_m2783529335(__this, /*hidden argument*/NULL);
		V_5 = L_37;
		float L_38 = Rect_get_x_m982385354((&V_5), /*hidden argument*/NULL);
		Rect_t4241904616  L_39 = TextEditor_get_position_m2783529335(__this, /*hidden argument*/NULL);
		V_6 = L_39;
		float L_40 = Rect_get_y_m982386315((&V_6), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_41 = __this->get_style_2();
		NullCheck(L_41);
		float L_42 = GUIStyle_get_lineHeight_m3977255453(L_41, /*hidden argument*/NULL);
		Vector2_t4282066565  L_43;
		memset(&L_43, 0, sizeof(L_43));
		Vector2__ctor_m1517109030(&L_43, L_38, ((float)((float)L_40+(float)L_42)), /*hidden argument*/NULL);
		Vector2_t4282066565  L_44 = Vector2_op_Addition_m1173049553(NULL /*static, unused*/, L_36, L_43, /*hidden argument*/NULL);
		Vector2_t4282066565  L_45 = __this->get_scrollOffset_7();
		Vector2_t4282066565  L_46 = Vector2_op_Subtraction_m2097149401(NULL /*static, unused*/, L_44, L_45, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		Input_set_compositionCursorPos_m3225375732(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		String_t* L_47 = Input_get_compositionString_m1541052002(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_47);
		int32_t L_48 = String_get_Length_m2979997331(L_47, /*hidden argument*/NULL);
		if ((((int32_t)L_48) <= ((int32_t)0)))
		{
			goto IL_017b;
		}
	}
	{
		GUIStyle_t2990928826 * L_49 = __this->get_style_2();
		Rect_t4241904616  L_50 = TextEditor_get_position_m2783529335(__this, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_51 = __this->get_m_Content_8();
		int32_t L_52 = __this->get_controlID_1();
		int32_t L_53 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_54 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		String_t* L_55 = Input_get_compositionString_m1541052002(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_55);
		int32_t L_56 = String_get_Length_m2979997331(L_55, /*hidden argument*/NULL);
		NullCheck(L_49);
		GUIStyle_DrawWithTextSelection_m469195656(L_49, L_50, L_51, L_52, L_53, ((int32_t)((int32_t)L_54+(int32_t)L_56)), (bool)1, /*hidden argument*/NULL);
		goto IL_01a4;
	}

IL_017b:
	{
		GUIStyle_t2990928826 * L_57 = __this->get_style_2();
		Rect_t4241904616  L_58 = TextEditor_get_position_m2783529335(__this, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_59 = __this->get_m_Content_8();
		int32_t L_60 = __this->get_controlID_1();
		int32_t L_61 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_62 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		NullCheck(L_57);
		GUIStyle_DrawWithTextSelection_m984171349(L_57, L_58, L_59, L_60, L_61, L_62, /*hidden argument*/NULL);
	}

IL_01a4:
	{
		int32_t L_63 = __this->get_m_iAltCursorPos_19();
		if ((((int32_t)L_63) == ((int32_t)(-1))))
		{
			goto IL_01d3;
		}
	}
	{
		GUIStyle_t2990928826 * L_64 = __this->get_style_2();
		Rect_t4241904616  L_65 = TextEditor_get_position_m2783529335(__this, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_66 = __this->get_m_Content_8();
		int32_t L_67 = __this->get_controlID_1();
		int32_t L_68 = __this->get_m_iAltCursorPos_19();
		NullCheck(L_64);
		GUIStyle_DrawCursor_m4169664613(L_64, L_65, L_66, L_67, L_68, /*hidden argument*/NULL);
	}

IL_01d3:
	{
		GUIStyle_t2990928826 * L_69 = __this->get_style_2();
		Vector2_t4282066565  L_70 = V_2;
		NullCheck(L_69);
		GUIStyle_set_contentOffset_m3385864054(L_69, L_70, /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_71 = __this->get_style_2();
		Vector2_t4282066565  L_72 = Vector2_get_zero_m199872368(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_71);
		GUIStyle_set_Internal_clipOffset_m411599389(L_71, L_72, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_73 = __this->get_m_Content_8();
		String_t* L_74 = V_0;
		NullCheck(L_73);
		GUIContent_set_text_m1575840163(L_73, L_74, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.TextEditor::PerformOperation(UnityEngine.TextEditor/TextEditOp)
extern Il2CppClass* TextEditOp_t4145961110_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2341361039;
extern const uint32_t TextEditor_PerformOperation_m1276141663_MetadataUsageId;
extern "C"  bool TextEditor_PerformOperation_m1276141663 (TextEditor_t319394238 * __this, int32_t ___operation0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_PerformOperation_m1276141663_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		__this->set_m_RevealCursor_12((bool)1);
		int32_t L_0 = ___operation0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_00cc;
		}
		if (L_1 == 1)
		{
			goto IL_00d7;
		}
		if (L_1 == 2)
		{
			goto IL_00e2;
		}
		if (L_1 == 3)
		{
			goto IL_00ed;
		}
		if (L_1 == 4)
		{
			goto IL_00f8;
		}
		if (L_1 == 5)
		{
			goto IL_0103;
		}
		if (L_1 == 6)
		{
			goto IL_013a;
		}
		if (L_1 == 7)
		{
			goto IL_0145;
		}
		if (L_1 == 8)
		{
			goto IL_027e;
		}
		if (L_1 == 9)
		{
			goto IL_027e;
		}
		if (L_1 == 10)
		{
			goto IL_0166;
		}
		if (L_1 == 11)
		{
			goto IL_0171;
		}
		if (L_1 == 12)
		{
			goto IL_012f;
		}
		if (L_1 == 13)
		{
			goto IL_010e;
		}
		if (L_1 == 14)
		{
			goto IL_0150;
		}
		if (L_1 == 15)
		{
			goto IL_015b;
		}
		if (L_1 == 16)
		{
			goto IL_0119;
		}
		if (L_1 == 17)
		{
			goto IL_0124;
		}
		if (L_1 == 18)
		{
			goto IL_017c;
		}
		if (L_1 == 19)
		{
			goto IL_0187;
		}
		if (L_1 == 20)
		{
			goto IL_0192;
		}
		if (L_1 == 21)
		{
			goto IL_019d;
		}
		if (L_1 == 22)
		{
			goto IL_01d4;
		}
		if (L_1 == 23)
		{
			goto IL_01df;
		}
		if (L_1 == 24)
		{
			goto IL_027e;
		}
		if (L_1 == 25)
		{
			goto IL_027e;
		}
		if (L_1 == 26)
		{
			goto IL_01ea;
		}
		if (L_1 == 27)
		{
			goto IL_01f5;
		}
		if (L_1 == 28)
		{
			goto IL_0216;
		}
		if (L_1 == 29)
		{
			goto IL_0221;
		}
		if (L_1 == 30)
		{
			goto IL_01b3;
		}
		if (L_1 == 31)
		{
			goto IL_01a8;
		}
		if (L_1 == 32)
		{
			goto IL_01be;
		}
		if (L_1 == 33)
		{
			goto IL_01c9;
		}
		if (L_1 == 34)
		{
			goto IL_020b;
		}
		if (L_1 == 35)
		{
			goto IL_0200;
		}
		if (L_1 == 36)
		{
			goto IL_022c;
		}
		if (L_1 == 37)
		{
			goto IL_0233;
		}
		if (L_1 == 38)
		{
			goto IL_0269;
		}
		if (L_1 == 39)
		{
			goto IL_0277;
		}
		if (L_1 == 40)
		{
			goto IL_0270;
		}
		if (L_1 == 41)
		{
			goto IL_023a;
		}
		if (L_1 == 42)
		{
			goto IL_0241;
		}
		if (L_1 == 43)
		{
			goto IL_024c;
		}
		if (L_1 == 44)
		{
			goto IL_0253;
		}
		if (L_1 == 45)
		{
			goto IL_025e;
		}
	}
	{
		goto IL_027e;
	}

IL_00cc:
	{
		TextEditor_MoveLeft_m2173129001(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_00d7:
	{
		TextEditor_MoveRight_m4087700060(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_00e2:
	{
		TextEditor_MoveUp_m2143317757(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_00ed:
	{
		TextEditor_MoveDown_m1953831684(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_00f8:
	{
		TextEditor_MoveLineStart_m2661644558(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0103:
	{
		TextEditor_MoveLineEnd_m2429882887(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_010e:
	{
		TextEditor_MoveWordRight_m1611060338(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0119:
	{
		TextEditor_MoveToStartOfNextWord_m1275371419(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0124:
	{
		TextEditor_MoveToEndOfPreviousWord_m4107655576(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_012f:
	{
		TextEditor_MoveWordLeft_m4171447379(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_013a:
	{
		TextEditor_MoveTextStart_m1434665397(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0145:
	{
		TextEditor_MoveTextEnd_m3188381806(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0150:
	{
		TextEditor_MoveParagraphForward_m2816529689(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_015b:
	{
		TextEditor_MoveParagraphBackward_m4210809841(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0166:
	{
		TextEditor_MoveGraphicalLineStart_m1209754461(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0171:
	{
		TextEditor_MoveGraphicalLineEnd_m537871382(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_017c:
	{
		TextEditor_SelectLeft_m1358467604(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0187:
	{
		TextEditor_SelectRight_m308033233(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0192:
	{
		TextEditor_SelectUp_m2312302248(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_019d:
	{
		TextEditor_SelectDown_m1139170287(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_01a8:
	{
		TextEditor_SelectWordRight_m1449160295(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_01b3:
	{
		TextEditor_SelectWordLeft_m979636158(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_01be:
	{
		TextEditor_SelectToEndOfPreviousWord_m3181593933(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_01c9:
	{
		TextEditor_SelectToStartOfNextWord_m2771612816(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_01d4:
	{
		TextEditor_SelectTextStart_m1272765354(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_01df:
	{
		TextEditor_SelectTextEnd_m175926179(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_01ea:
	{
		TextEditor_ExpandSelectGraphicalLineStart_m4278286146(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_01f5:
	{
		TextEditor_ExpandSelectGraphicalLineEnd_m885198139(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0200:
	{
		TextEditor_SelectParagraphForward_m2310606212(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_020b:
	{
		TextEditor_SelectParagraphBackward_m1412083942(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0216:
	{
		TextEditor_SelectGraphicalLineStart_m348597512(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0221:
	{
		TextEditor_SelectGraphicalLineEnd_m31947905(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_022c:
	{
		bool L_2 = TextEditor_Delete_m2591383938(__this, /*hidden argument*/NULL);
		return L_2;
	}

IL_0233:
	{
		bool L_3 = TextEditor_Backspace_m2697740394(__this, /*hidden argument*/NULL);
		return L_3;
	}

IL_023a:
	{
		bool L_4 = TextEditor_Cut_m3622060269(__this, /*hidden argument*/NULL);
		return L_4;
	}

IL_0241:
	{
		TextEditor_Copy_m645385702(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_024c:
	{
		bool L_5 = TextEditor_Paste_m4252838526(__this, /*hidden argument*/NULL);
		return L_5;
	}

IL_0253:
	{
		TextEditor_SelectAll_m2943368726(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_025e:
	{
		TextEditor_SelectNone_m1425185029(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0269:
	{
		bool L_6 = TextEditor_DeleteWordBack_m1423005555(__this, /*hidden argument*/NULL);
		return L_6;
	}

IL_0270:
	{
		bool L_7 = TextEditor_DeleteLineBack_m4050675933(__this, /*hidden argument*/NULL);
		return L_7;
	}

IL_0277:
	{
		bool L_8 = TextEditor_DeleteWordForward_m2247051099(__this, /*hidden argument*/NULL);
		return L_8;
	}

IL_027e:
	{
		int32_t L_9 = ___operation0;
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(TextEditOp_t4145961110_il2cpp_TypeInfo_var, &L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral2341361039, L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0298:
	{
		return (bool)0;
	}
}
// System.Void UnityEngine.TextEditor::SaveBackup()
extern "C"  void TextEditor_SaveBackup_m1589758352 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		__this->set_oldText_20(L_0);
		int32_t L_1 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		__this->set_oldPos_21(L_1);
		int32_t L_2 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		__this->set_oldSelectPos_22(L_2);
		return;
	}
}
// System.Boolean UnityEngine.TextEditor::Cut()
extern "C"  bool TextEditor_Cut_m3622060269 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_isPasswordField_5();
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		TextEditor_Copy_m645385702(__this, /*hidden argument*/NULL);
		bool L_1 = TextEditor_DeleteSelection_m1201672204(__this, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.TextEditor::Copy()
extern Il2CppClass* GUIUtility_t1028319349_il2cpp_TypeInfo_var;
extern const uint32_t TextEditor_Copy_m645385702_MetadataUsageId;
extern "C"  void TextEditor_Copy_m645385702 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_Copy_m645385702_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		int32_t L_0 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		bool L_2 = __this->get_isPasswordField_5();
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		return;
	}

IL_001e:
	{
		int32_t L_3 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_4 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0053;
		}
	}
	{
		String_t* L_5 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_7 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		int32_t L_8 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_9 = String_Substring_m675079568(L_5, L_6, ((int32_t)((int32_t)L_7-(int32_t)L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0072;
	}

IL_0053:
	{
		String_t* L_10 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		int32_t L_11 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		int32_t L_12 = TextEditor_get_cursorIndex_m3258858212(__this, /*hidden argument*/NULL);
		int32_t L_13 = TextEditor_get_selectIndex_m847319678(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		String_t* L_14 = String_Substring_m675079568(L_10, L_11, ((int32_t)((int32_t)L_12-(int32_t)L_13)), /*hidden argument*/NULL);
		V_0 = L_14;
	}

IL_0072:
	{
		String_t* L_15 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		GUIUtility_set_systemCopyBuffer_m2088074937(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.TextEditor::ReplaceNewlinesWithSpaces(System.String)
extern Il2CppCodeGenString* _stringLiteral413;
extern Il2CppCodeGenString* _stringLiteral32;
extern const uint32_t TextEditor_ReplaceNewlinesWithSpaces_m4176827598_MetadataUsageId;
extern "C"  String_t* TextEditor_ReplaceNewlinesWithSpaces_m4176827598 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_ReplaceNewlinesWithSpaces_m4176827598_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___value0;
		NullCheck(L_0);
		String_t* L_1 = String_Replace_m2915759397(L_0, _stringLiteral413, _stringLiteral32, /*hidden argument*/NULL);
		___value0 = L_1;
		String_t* L_2 = ___value0;
		NullCheck(L_2);
		String_t* L_3 = String_Replace_m3369701083(L_2, ((int32_t)10), ((int32_t)32), /*hidden argument*/NULL);
		___value0 = L_3;
		String_t* L_4 = ___value0;
		NullCheck(L_4);
		String_t* L_5 = String_Replace_m3369701083(L_4, ((int32_t)13), ((int32_t)32), /*hidden argument*/NULL);
		___value0 = L_5;
		String_t* L_6 = ___value0;
		return L_6;
	}
}
// System.Boolean UnityEngine.TextEditor::Paste()
extern Il2CppClass* GUIUtility_t1028319349_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t TextEditor_Paste_m4252838526_MetadataUsageId;
extern "C"  bool TextEditor_Paste_m4252838526 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_Paste_m4252838526_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		String_t* L_0 = GUIUtility_get_systemCopyBuffer_m1467578008(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_3 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0031;
		}
	}
	{
		bool L_4 = __this->get_multiline_3();
		if (L_4)
		{
			goto IL_0028;
		}
	}
	{
		String_t* L_5 = V_0;
		String_t* L_6 = TextEditor_ReplaceNewlinesWithSpaces_m4176827598(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
	}

IL_0028:
	{
		String_t* L_7 = V_0;
		TextEditor_ReplaceSelection_m1188267705(__this, L_7, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0031:
	{
		return (bool)0;
	}
}
// System.Void UnityEngine.TextEditor::MapKey(System.String,UnityEngine.TextEditor/TextEditOp)
extern Il2CppClass* TextEditor_t319394238_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m1048138207_MethodInfo_var;
extern const uint32_t TextEditor_MapKey_m368710468_MetadataUsageId;
extern "C"  void TextEditor_MapKey_m368710468 (Il2CppObject * __this /* static, unused */, String_t* ___key0, int32_t ___action1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_MapKey_m368710468_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1390811265 * L_0 = ((TextEditor_t319394238_StaticFields*)TextEditor_t319394238_il2cpp_TypeInfo_var->static_fields)->get_s_Keyactions_23();
		String_t* L_1 = ___key0;
		Event_t4196595728 * L_2 = Event_KeyboardEvent_m1236547202(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		int32_t L_3 = ___action1;
		NullCheck(L_0);
		Dictionary_2_set_Item_m1048138207(L_0, L_2, L_3, /*hidden argument*/Dictionary_2_set_Item_m1048138207_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.TextEditor::InitKeyActions()
extern Il2CppClass* TextEditor_t319394238_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t1390811265_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2397409902_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3317767;
extern Il2CppCodeGenString* _stringLiteral108511772;
extern Il2CppCodeGenString* _stringLiteral3739;
extern Il2CppCodeGenString* _stringLiteral3089570;
extern Il2CppCodeGenString* _stringLiteral35641002;
extern Il2CppCodeGenString* _stringLiteral1110532057;
extern Il2CppCodeGenString* _stringLiteral37374;
extern Il2CppCodeGenString* _stringLiteral35412805;
extern Il2CppCodeGenString* _stringLiteral2959508907;
extern Il2CppCodeGenString* _stringLiteral1353507967;
extern Il2CppCodeGenString* _stringLiteral3065469884;
extern Il2CppCodeGenString* _stringLiteral77103;
extern Il2CppCodeGenString* _stringLiteral90128741;
extern Il2CppCodeGenString* _stringLiteral2799651966;
extern Il2CppCodeGenString* _stringLiteral38411565;
extern Il2CppCodeGenString* _stringLiteral1196419510;
extern Il2CppCodeGenString* _stringLiteral40257;
extern Il2CppCodeGenString* _stringLiteral38183368;
extern Il2CppCodeGenString* _stringLiteral37488044;
extern Il2CppCodeGenString* _stringLiteral1167790359;
extern Il2CppCodeGenString* _stringLiteral39296;
extern Il2CppCodeGenString* _stringLiteral37259847;
extern Il2CppCodeGenString* _stringLiteral35531650;
extern Il2CppCodeGenString* _stringLiteral1143256;
extern Il2CppCodeGenString* _stringLiteral1092149026;
extern Il2CppCodeGenString* _stringLiteral3797509729;
extern Il2CppCodeGenString* _stringLiteral1136758;
extern Il2CppCodeGenString* _stringLiteral1091920829;
extern Il2CppCodeGenString* _stringLiteral1040431850;
extern Il2CppCodeGenString* _stringLiteral2194277273;
extern Il2CppCodeGenString* _stringLiteral1082942;
extern Il2CppCodeGenString* _stringLiteral1040203653;
extern Il2CppCodeGenString* _stringLiteral1039508329;
extern Il2CppCodeGenString* _stringLiteral2165648122;
extern Il2CppCodeGenString* _stringLiteral1081981;
extern Il2CppCodeGenString* _stringLiteral1039280132;
extern Il2CppCodeGenString* _stringLiteral1244;
extern Il2CppCodeGenString* _stringLiteral1267;
extern Il2CppCodeGenString* _stringLiteral1246;
extern Il2CppCodeGenString* _stringLiteral1265;
extern Il2CppCodeGenString* _stringLiteral3014;
extern Il2CppCodeGenString* _stringLiteral3018;
extern Il2CppCodeGenString* _stringLiteral3012;
extern Il2CppCodeGenString* _stringLiteral3016;
extern Il2CppCodeGenString* _stringLiteral3011;
extern Il2CppCodeGenString* _stringLiteral3015;
extern Il2CppCodeGenString* _stringLiteral2324910417;
extern Il2CppCodeGenString* _stringLiteral2475929369;
extern Il2CppCodeGenString* _stringLiteral2672442874;
extern Il2CppCodeGenString* _stringLiteral3208415;
extern Il2CppCodeGenString* _stringLiteral100571;
extern Il2CppCodeGenString* _stringLiteral94073;
extern Il2CppCodeGenString* _stringLiteral89900544;
extern Il2CppCodeGenString* _stringLiteral485509001;
extern Il2CppCodeGenString* _stringLiteral61107681;
extern Il2CppCodeGenString* _stringLiteral3034;
extern Il2CppCodeGenString* _stringLiteral3013;
extern Il2CppCodeGenString* _stringLiteral3032;
extern Il2CppCodeGenString* _stringLiteral3957366670;
extern Il2CppCodeGenString* _stringLiteral637174935;
extern Il2CppCodeGenString* _stringLiteral4109032604;
extern const uint32_t TextEditor_InitKeyActions_m2904712959_MetadataUsageId;
extern "C"  void TextEditor_InitKeyActions_m2904712959 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_InitKeyActions_m2904712959_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1390811265 * L_0 = ((TextEditor_t319394238_StaticFields*)TextEditor_t319394238_il2cpp_TypeInfo_var->static_fields)->get_s_Keyactions_23();
		if (!L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		Dictionary_2_t1390811265 * L_1 = (Dictionary_2_t1390811265 *)il2cpp_codegen_object_new(Dictionary_2_t1390811265_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2397409902(L_1, /*hidden argument*/Dictionary_2__ctor_m2397409902_MethodInfo_var);
		((TextEditor_t319394238_StaticFields*)TextEditor_t319394238_il2cpp_TypeInfo_var->static_fields)->set_s_Keyactions_23(L_1);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral3317767, 0, /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral108511772, 1, /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral3739, 2, /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral3089570, 3, /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral35641002, ((int32_t)18), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral1110532057, ((int32_t)19), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral37374, ((int32_t)20), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral35412805, ((int32_t)21), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral2959508907, ((int32_t)36), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral1353507967, ((int32_t)37), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral3065469884, ((int32_t)37), /*hidden argument*/NULL);
		int32_t L_2 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_00e0;
		}
	}
	{
		int32_t L_3 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_3) == ((int32_t)3)))
		{
			goto IL_00e0;
		}
	}
	{
		int32_t L_4 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_4) == ((int32_t)4)))
		{
			goto IL_00e0;
		}
	}
	{
		int32_t L_5 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_00e0;
		}
	}
	{
		int32_t L_6 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)17)))))
		{
			goto IL_029b;
		}
	}
	{
		String_t* L_7 = SystemInfo_get_operatingSystem_m2538828082(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = String_StartsWith_m1500793453(L_7, _stringLiteral77103, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_029b;
		}
	}

IL_00e0:
	{
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral90128741, ((int32_t)10), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral2799651966, ((int32_t)11), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral38411565, ((int32_t)12), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral1196419510, ((int32_t)13), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral40257, ((int32_t)15), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral38183368, ((int32_t)14), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral37488044, ((int32_t)10), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral1167790359, ((int32_t)11), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral39296, 6, /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral37259847, 7, /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral35531650, ((int32_t)22), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral1143256, ((int32_t)23), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral1092149026, ((int32_t)26), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral3797509729, ((int32_t)27), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral1136758, ((int32_t)34), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral1091920829, ((int32_t)35), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral1040431850, ((int32_t)30), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral2194277273, ((int32_t)31), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral1082942, ((int32_t)34), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral1040203653, ((int32_t)35), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral1039508329, ((int32_t)26), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral2165648122, ((int32_t)27), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral1081981, ((int32_t)22), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral1039280132, ((int32_t)23), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral1244, ((int32_t)44), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral1267, ((int32_t)41), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral1246, ((int32_t)42), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral1265, ((int32_t)43), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral3014, ((int32_t)36), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral3018, ((int32_t)37), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral3012, 0, /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral3016, 1, /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral3011, 4, /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral3015, 5, /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral2324910417, ((int32_t)39), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral2475929369, ((int32_t)38), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral2672442874, ((int32_t)40), /*hidden argument*/NULL);
		goto IL_03d3;
	}

IL_029b:
	{
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral3208415, ((int32_t)10), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral100571, ((int32_t)11), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral37488044, ((int32_t)12), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral1167790359, ((int32_t)13), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral39296, ((int32_t)15), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral37259847, ((int32_t)14), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral90128741, ((int32_t)17), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral2799651966, ((int32_t)16), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral94073, ((int32_t)15), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral89900544, ((int32_t)14), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral1092149026, ((int32_t)32), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral3797509729, ((int32_t)33), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral1136758, ((int32_t)34), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral1091920829, ((int32_t)35), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral35531650, ((int32_t)28), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral1143256, ((int32_t)29), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral485509001, ((int32_t)39), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral61107681, ((int32_t)38), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral2672442874, ((int32_t)40), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral3011, ((int32_t)44), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral3034, ((int32_t)41), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral3013, ((int32_t)42), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral3032, ((int32_t)43), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral3957366670, ((int32_t)41), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral637174935, ((int32_t)42), /*hidden argument*/NULL);
		TextEditor_MapKey_m368710468(NULL /*static, unused*/, _stringLiteral4109032604, ((int32_t)43), /*hidden argument*/NULL);
	}

IL_03d3:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::DetectFocusChange()
extern Il2CppClass* GUIUtility_t1028319349_il2cpp_TypeInfo_var;
extern const uint32_t TextEditor_DetectFocusChange_m993735446_MetadataUsageId;
extern "C"  void TextEditor_DetectFocusChange_m993735446 (TextEditor_t319394238 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_DetectFocusChange_m993735446_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_m_HasFocus_6();
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_1 = __this->get_controlID_1();
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		int32_t L_2 = GUIUtility_get_keyboardControl_m1277835431(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_0021;
		}
	}
	{
		TextEditor_OnLostFocus_m1599980550(__this, /*hidden argument*/NULL);
	}

IL_0021:
	{
		bool L_3 = __this->get_m_HasFocus_6();
		if (L_3)
		{
			goto IL_0042;
		}
	}
	{
		int32_t L_4 = __this->get_controlID_1();
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		int32_t L_5 = GUIUtility_get_keyboardControl_m1277835431(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_0042;
		}
	}
	{
		TextEditor_OnFocus_m1604527178(__this, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::ClampTextIndex(System.Int32&)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t TextEditor_ClampTextIndex_m707900960_MetadataUsageId;
extern "C"  void TextEditor_ClampTextIndex_m707900960 (TextEditor_t319394238 * __this, int32_t* ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_ClampTextIndex_m707900960_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = ___index0;
		int32_t* L_1 = ___index0;
		String_t* L_2 = TextEditor_get_text_m2661970474(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m2979997331(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_4 = Mathf_Clamp_m510460741(NULL /*static, unused*/, (*((int32_t*)L_1)), 0, L_3, /*hidden argument*/NULL);
		*((int32_t*)(L_0)) = (int32_t)L_4;
		return;
	}
}
// System.Boolean UnityEngine.TextGenerationSettings::CompareColors(UnityEngine.Color,UnityEngine.Color)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t TextGenerationSettings_CompareColors_m2801108564_MetadataUsageId;
extern "C"  bool TextGenerationSettings_CompareColors_m2801108564 (TextGenerationSettings_t1923005356 * __this, Color_t4194546905  ___left0, Color_t4194546905  ___right1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextGenerationSettings_CompareColors_m2801108564_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B5_0 = 0;
	{
		float L_0 = (&___left0)->get_r_0();
		float L_1 = (&___right1)->get_r_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		bool L_2 = Mathf_Approximately_m1395529776(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_005d;
		}
	}
	{
		float L_3 = (&___left0)->get_g_1();
		float L_4 = (&___right1)->get_g_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		bool L_5 = Mathf_Approximately_m1395529776(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_005d;
		}
	}
	{
		float L_6 = (&___left0)->get_b_2();
		float L_7 = (&___right1)->get_b_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		bool L_8 = Mathf_Approximately_m1395529776(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005d;
		}
	}
	{
		float L_9 = (&___left0)->get_a_3();
		float L_10 = (&___right1)->get_a_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		bool L_11 = Mathf_Approximately_m1395529776(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_11));
		goto IL_005e;
	}

IL_005d:
	{
		G_B5_0 = 0;
	}

IL_005e:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool TextGenerationSettings_CompareColors_m2801108564_AdjustorThunk (Il2CppObject * __this, Color_t4194546905  ___left0, Color_t4194546905  ___right1, const MethodInfo* method)
{
	TextGenerationSettings_t1923005356 * _thisAdjusted = reinterpret_cast<TextGenerationSettings_t1923005356 *>(__this + 1);
	return TextGenerationSettings_CompareColors_m2801108564(_thisAdjusted, ___left0, ___right1, method);
}
// System.Boolean UnityEngine.TextGenerationSettings::CompareVector2(UnityEngine.Vector2,UnityEngine.Vector2)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t TextGenerationSettings_CompareVector2_m332687775_MetadataUsageId;
extern "C"  bool TextGenerationSettings_CompareVector2_m332687775 (TextGenerationSettings_t1923005356 * __this, Vector2_t4282066565  ___left0, Vector2_t4282066565  ___right1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextGenerationSettings_CompareVector2_m332687775_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		float L_0 = (&___left0)->get_x_1();
		float L_1 = (&___right1)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		bool L_2 = Mathf_Approximately_m1395529776(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		float L_3 = (&___left0)->get_y_2();
		float L_4 = (&___right1)->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		bool L_5 = Mathf_Approximately_m1395529776(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002e;
	}

IL_002d:
	{
		G_B3_0 = 0;
	}

IL_002e:
	{
		return (bool)G_B3_0;
	}
}
extern "C"  bool TextGenerationSettings_CompareVector2_m332687775_AdjustorThunk (Il2CppObject * __this, Vector2_t4282066565  ___left0, Vector2_t4282066565  ___right1, const MethodInfo* method)
{
	TextGenerationSettings_t1923005356 * _thisAdjusted = reinterpret_cast<TextGenerationSettings_t1923005356 *>(__this + 1);
	return TextGenerationSettings_CompareVector2_m332687775(_thisAdjusted, ___left0, ___right1, method);
}
// System.Boolean UnityEngine.TextGenerationSettings::Equals(UnityEngine.TextGenerationSettings)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t TextGenerationSettings_Equals_m3950599493_MetadataUsageId;
extern "C"  bool TextGenerationSettings_Equals_m3950599493 (TextGenerationSettings_t1923005356 * __this, TextGenerationSettings_t1923005356  ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextGenerationSettings_Equals_m3950599493_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B21_0 = 0;
	{
		Color_t4194546905  L_0 = __this->get_color_1();
		Color_t4194546905  L_1 = (&___other0)->get_color_1();
		bool L_2 = TextGenerationSettings_CompareColors_m2801108564(__this, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0186;
		}
	}
	{
		int32_t L_3 = __this->get_fontSize_2();
		int32_t L_4 = (&___other0)->get_fontSize_2();
		if ((!(((uint32_t)L_3) == ((uint32_t)L_4))))
		{
			goto IL_0186;
		}
	}
	{
		float L_5 = __this->get_scaleFactor_5();
		float L_6 = (&___other0)->get_scaleFactor_5();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		bool L_7 = Mathf_Approximately_m1395529776(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0186;
		}
	}
	{
		int32_t L_8 = __this->get_resizeTextMinSize_10();
		int32_t L_9 = (&___other0)->get_resizeTextMinSize_10();
		if ((!(((uint32_t)L_8) == ((uint32_t)L_9))))
		{
			goto IL_0186;
		}
	}
	{
		int32_t L_10 = __this->get_resizeTextMaxSize_11();
		int32_t L_11 = (&___other0)->get_resizeTextMaxSize_11();
		if ((!(((uint32_t)L_10) == ((uint32_t)L_11))))
		{
			goto IL_0186;
		}
	}
	{
		float L_12 = __this->get_lineSpacing_3();
		float L_13 = (&___other0)->get_lineSpacing_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		bool L_14 = Mathf_Approximately_m1395529776(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0186;
		}
	}
	{
		int32_t L_15 = __this->get_fontStyle_6();
		int32_t L_16 = (&___other0)->get_fontStyle_6();
		if ((!(((uint32_t)L_15) == ((uint32_t)L_16))))
		{
			goto IL_0186;
		}
	}
	{
		bool L_17 = __this->get_richText_4();
		bool L_18 = (&___other0)->get_richText_4();
		if ((!(((uint32_t)L_17) == ((uint32_t)L_18))))
		{
			goto IL_0186;
		}
	}
	{
		int32_t L_19 = __this->get_textAnchor_7();
		int32_t L_20 = (&___other0)->get_textAnchor_7();
		if ((!(((uint32_t)L_19) == ((uint32_t)L_20))))
		{
			goto IL_0186;
		}
	}
	{
		bool L_21 = __this->get_alignByGeometry_8();
		bool L_22 = (&___other0)->get_alignByGeometry_8();
		if ((!(((uint32_t)L_21) == ((uint32_t)L_22))))
		{
			goto IL_0186;
		}
	}
	{
		bool L_23 = __this->get_resizeTextForBestFit_9();
		bool L_24 = (&___other0)->get_resizeTextForBestFit_9();
		if ((!(((uint32_t)L_23) == ((uint32_t)L_24))))
		{
			goto IL_0186;
		}
	}
	{
		int32_t L_25 = __this->get_resizeTextMinSize_10();
		int32_t L_26 = (&___other0)->get_resizeTextMinSize_10();
		if ((!(((uint32_t)L_25) == ((uint32_t)L_26))))
		{
			goto IL_0186;
		}
	}
	{
		int32_t L_27 = __this->get_resizeTextMaxSize_11();
		int32_t L_28 = (&___other0)->get_resizeTextMaxSize_11();
		if ((!(((uint32_t)L_27) == ((uint32_t)L_28))))
		{
			goto IL_0186;
		}
	}
	{
		bool L_29 = __this->get_resizeTextForBestFit_9();
		bool L_30 = (&___other0)->get_resizeTextForBestFit_9();
		if ((!(((uint32_t)L_29) == ((uint32_t)L_30))))
		{
			goto IL_0186;
		}
	}
	{
		bool L_31 = __this->get_updateBounds_12();
		bool L_32 = (&___other0)->get_updateBounds_12();
		if ((!(((uint32_t)L_31) == ((uint32_t)L_32))))
		{
			goto IL_0186;
		}
	}
	{
		int32_t L_33 = __this->get_horizontalOverflow_14();
		int32_t L_34 = (&___other0)->get_horizontalOverflow_14();
		if ((!(((uint32_t)L_33) == ((uint32_t)L_34))))
		{
			goto IL_0186;
		}
	}
	{
		int32_t L_35 = __this->get_verticalOverflow_13();
		int32_t L_36 = (&___other0)->get_verticalOverflow_13();
		if ((!(((uint32_t)L_35) == ((uint32_t)L_36))))
		{
			goto IL_0186;
		}
	}
	{
		Vector2_t4282066565  L_37 = __this->get_generationExtents_15();
		Vector2_t4282066565  L_38 = (&___other0)->get_generationExtents_15();
		bool L_39 = TextGenerationSettings_CompareVector2_m332687775(__this, L_37, L_38, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_0186;
		}
	}
	{
		Vector2_t4282066565  L_40 = __this->get_pivot_16();
		Vector2_t4282066565  L_41 = (&___other0)->get_pivot_16();
		bool L_42 = TextGenerationSettings_CompareVector2_m332687775(__this, L_40, L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_0186;
		}
	}
	{
		Font_t4241557075 * L_43 = __this->get_font_0();
		Font_t4241557075 * L_44 = (&___other0)->get_font_0();
		bool L_45 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		G_B21_0 = ((int32_t)(L_45));
		goto IL_0187;
	}

IL_0186:
	{
		G_B21_0 = 0;
	}

IL_0187:
	{
		return (bool)G_B21_0;
	}
}
extern "C"  bool TextGenerationSettings_Equals_m3950599493_AdjustorThunk (Il2CppObject * __this, TextGenerationSettings_t1923005356  ___other0, const MethodInfo* method)
{
	TextGenerationSettings_t1923005356 * _thisAdjusted = reinterpret_cast<TextGenerationSettings_t1923005356 *>(__this + 1);
	return TextGenerationSettings_Equals_m3950599493(_thisAdjusted, ___other0, method);
}
// Conversion methods for marshalling of: UnityEngine.TextGenerationSettings
extern "C" void TextGenerationSettings_t1923005356_marshal_pinvoke(const TextGenerationSettings_t1923005356& unmarshaled, TextGenerationSettings_t1923005356_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___font_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'font' of type 'TextGenerationSettings': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___font_0Exception);
}
extern "C" void TextGenerationSettings_t1923005356_marshal_pinvoke_back(const TextGenerationSettings_t1923005356_marshaled_pinvoke& marshaled, TextGenerationSettings_t1923005356& unmarshaled)
{
	Il2CppCodeGenException* ___font_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'font' of type 'TextGenerationSettings': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___font_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.TextGenerationSettings
extern "C" void TextGenerationSettings_t1923005356_marshal_pinvoke_cleanup(TextGenerationSettings_t1923005356_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.TextGenerationSettings
extern "C" void TextGenerationSettings_t1923005356_marshal_com(const TextGenerationSettings_t1923005356& unmarshaled, TextGenerationSettings_t1923005356_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___font_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'font' of type 'TextGenerationSettings': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___font_0Exception);
}
extern "C" void TextGenerationSettings_t1923005356_marshal_com_back(const TextGenerationSettings_t1923005356_marshaled_com& marshaled, TextGenerationSettings_t1923005356& unmarshaled)
{
	Il2CppCodeGenException* ___font_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'font' of type 'TextGenerationSettings': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___font_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.TextGenerationSettings
extern "C" void TextGenerationSettings_t1923005356_marshal_com_cleanup(TextGenerationSettings_t1923005356_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.TextGenerator::.ctor()
extern "C"  void TextGenerator__ctor_m3994909811 (TextGenerator_t538854556 * __this, const MethodInfo* method)
{
	{
		TextGenerator__ctor_m1563237700(__this, ((int32_t)50), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::.ctor(System.Int32)
extern Il2CppClass* List_1_t1317283468_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1433993036_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1187093738_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1637285730_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m2496259298_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m363550720_MethodInfo_var;
extern const uint32_t TextGenerator__ctor_m1563237700_MetadataUsageId;
extern "C"  void TextGenerator__ctor_m1563237700 (TextGenerator_t538854556 * __this, int32_t ___initialCapacity0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextGenerator__ctor_m1563237700_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___initialCapacity0;
		List_1_t1317283468 * L_1 = (List_1_t1317283468 *)il2cpp_codegen_object_new(List_1_t1317283468_il2cpp_TypeInfo_var);
		List_1__ctor_m1637285730(L_1, ((int32_t)((int32_t)((int32_t)((int32_t)L_0+(int32_t)1))*(int32_t)4)), /*hidden argument*/List_1__ctor_m1637285730_MethodInfo_var);
		__this->set_m_Verts_5(L_1);
		int32_t L_2 = ___initialCapacity0;
		List_1_t1433993036 * L_3 = (List_1_t1433993036 *)il2cpp_codegen_object_new(List_1_t1433993036_il2cpp_TypeInfo_var);
		List_1__ctor_m2496259298(L_3, ((int32_t)((int32_t)L_2+(int32_t)1)), /*hidden argument*/List_1__ctor_m2496259298_MethodInfo_var);
		__this->set_m_Characters_6(L_3);
		List_1_t1187093738 * L_4 = (List_1_t1187093738 *)il2cpp_codegen_object_new(List_1_t1187093738_il2cpp_TypeInfo_var);
		List_1__ctor_m363550720(L_4, ((int32_t)20), /*hidden argument*/List_1__ctor_m363550720_MethodInfo_var);
		__this->set_m_Lines_7(L_4);
		TextGenerator_Init_m1881520001(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::System.IDisposable.Dispose()
extern "C"  void TextGenerator_System_IDisposable_Dispose_m3586435340 (TextGenerator_t538854556 * __this, const MethodInfo* method)
{
	{
		TextGenerator_Dispose_cpp_m959647828(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::Finalize()
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t TextGenerator_Finalize_m3211881231_MetadataUsageId;
extern "C"  void TextGenerator_Finalize_m3211881231 (TextGenerator_t538854556 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextGenerator_Finalize_m3211881231_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, __this);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m3027285644(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0012:
	{
		return;
	}
}
// UnityEngine.TextGenerationSettings UnityEngine.TextGenerator::ValidatedSettings(UnityEngine.TextGenerationSettings)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral217203965;
extern Il2CppCodeGenString* _stringLiteral3822004709;
extern const uint32_t TextGenerator_ValidatedSettings_m1098807955_MetadataUsageId;
extern "C"  TextGenerationSettings_t1923005356  TextGenerator_ValidatedSettings_m1098807955 (TextGenerator_t538854556 * __this, TextGenerationSettings_t1923005356  ___settings0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextGenerator_ValidatedSettings_m1098807955_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Font_t4241557075 * L_0 = (&___settings0)->get_font_0();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		Font_t4241557075 * L_2 = (&___settings0)->get_font_0();
		NullCheck(L_2);
		bool L_3 = Font_get_dynamic_m3880144684(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0025;
		}
	}
	{
		TextGenerationSettings_t1923005356  L_4 = ___settings0;
		return L_4;
	}

IL_0025:
	{
		int32_t L_5 = (&___settings0)->get_fontSize_2();
		if (L_5)
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_6 = (&___settings0)->get_fontStyle_6();
		if (!L_6)
		{
			goto IL_0057;
		}
	}

IL_003d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3123317694(NULL /*static, unused*/, _stringLiteral217203965, /*hidden argument*/NULL);
		(&___settings0)->set_fontSize_2(0);
		(&___settings0)->set_fontStyle_6(0);
	}

IL_0057:
	{
		bool L_7 = (&___settings0)->get_resizeTextForBestFit_9();
		if (!L_7)
		{
			goto IL_0075;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3123317694(NULL /*static, unused*/, _stringLiteral3822004709, /*hidden argument*/NULL);
		(&___settings0)->set_resizeTextForBestFit_9((bool)0);
	}

IL_0075:
	{
		TextGenerationSettings_t1923005356  L_8 = ___settings0;
		return L_8;
	}
}
// System.Void UnityEngine.TextGenerator::Invalidate()
extern "C"  void TextGenerator_Invalidate_m620450028 (TextGenerator_t538854556 * __this, const MethodInfo* method)
{
	{
		__this->set_m_HasGenerated_3((bool)0);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::GetCharacters(System.Collections.Generic.List`1<UnityEngine.UICharInfo>)
extern "C"  void TextGenerator_GetCharacters_m2483060112 (TextGenerator_t538854556 * __this, List_1_t1433993036 * ___characters0, const MethodInfo* method)
{
	{
		List_1_t1433993036 * L_0 = ___characters0;
		TextGenerator_GetCharactersInternal_m3189630150(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::GetLines(System.Collections.Generic.List`1<UnityEngine.UILineInfo>)
extern "C"  void TextGenerator_GetLines_m2441503301 (TextGenerator_t538854556 * __this, List_1_t1187093738 * ___lines0, const MethodInfo* method)
{
	{
		List_1_t1187093738 * L_0 = ___lines0;
		TextGenerator_GetLinesInternal_m702225117(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::GetVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern "C"  void TextGenerator_GetVertices_m1097518625 (TextGenerator_t538854556 * __this, List_1_t1317283468 * ___vertices0, const MethodInfo* method)
{
	{
		List_1_t1317283468 * L_0 = ___vertices0;
		TextGenerator_GetVerticesInternal_m2005679319(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.TextGenerator::GetPreferredWidth(System.String,UnityEngine.TextGenerationSettings)
extern "C"  float TextGenerator_GetPreferredWidth_m1618543389 (TextGenerator_t538854556 * __this, String_t* ___str0, TextGenerationSettings_t1923005356  ___settings1, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		(&___settings1)->set_horizontalOverflow_14(1);
		(&___settings1)->set_verticalOverflow_13(1);
		(&___settings1)->set_updateBounds_12((bool)1);
		String_t* L_0 = ___str0;
		TextGenerationSettings_t1923005356  L_1 = ___settings1;
		TextGenerator_Populate_m953583418(__this, L_0, L_1, /*hidden argument*/NULL);
		Rect_t4241904616  L_2 = TextGenerator_get_rectExtents_m2200526529(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_width_m2824209432((&V_0), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Single UnityEngine.TextGenerator::GetPreferredHeight(System.String,UnityEngine.TextGenerationSettings)
extern "C"  float TextGenerator_GetPreferredHeight_m1770778044 (TextGenerator_t538854556 * __this, String_t* ___str0, TextGenerationSettings_t1923005356  ___settings1, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		(&___settings1)->set_verticalOverflow_13(1);
		(&___settings1)->set_updateBounds_12((bool)1);
		String_t* L_0 = ___str0;
		TextGenerationSettings_t1923005356  L_1 = ___settings1;
		TextGenerator_Populate_m953583418(__this, L_0, L_1, /*hidden argument*/NULL);
		Rect_t4241904616  L_2 = TextGenerator_get_rectExtents_m2200526529(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_height_m2154960823((&V_0), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.TextGenerator::Populate(System.String,UnityEngine.TextGenerationSettings)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t TextGenerator_Populate_m953583418_MetadataUsageId;
extern "C"  bool TextGenerator_Populate_m953583418 (TextGenerator_t538854556 * __this, String_t* ___str0, TextGenerationSettings_t1923005356  ___settings1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextGenerator_Populate_m953583418_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_m_HasGenerated_3();
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		String_t* L_1 = ___str0;
		String_t* L_2 = __this->get_m_LastString_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0035;
		}
	}
	{
		TextGenerationSettings_t1923005356  L_4 = __this->get_m_LastSettings_2();
		bool L_5 = TextGenerationSettings_Equals_m3950599493((&___settings1), L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		bool L_6 = __this->get_m_LastValid_4();
		return L_6;
	}

IL_0035:
	{
		String_t* L_7 = ___str0;
		TextGenerationSettings_t1923005356  L_8 = ___settings1;
		bool L_9 = TextGenerator_PopulateAlways_m4138837161(__this, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Boolean UnityEngine.TextGenerator::PopulateAlways(System.String,UnityEngine.TextGenerationSettings)
extern "C"  bool TextGenerator_PopulateAlways_m4138837161 (TextGenerator_t538854556 * __this, String_t* ___str0, TextGenerationSettings_t1923005356  ___settings1, const MethodInfo* method)
{
	TextGenerationSettings_t1923005356  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		String_t* L_0 = ___str0;
		__this->set_m_LastString_1(L_0);
		__this->set_m_HasGenerated_3((bool)1);
		__this->set_m_CachedVerts_8((bool)0);
		__this->set_m_CachedCharacters_9((bool)0);
		__this->set_m_CachedLines_10((bool)0);
		TextGenerationSettings_t1923005356  L_1 = ___settings1;
		__this->set_m_LastSettings_2(L_1);
		TextGenerationSettings_t1923005356  L_2 = ___settings1;
		TextGenerationSettings_t1923005356  L_3 = TextGenerator_ValidatedSettings_m1098807955(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = ___str0;
		Font_t4241557075 * L_5 = (&V_0)->get_font_0();
		Color_t4194546905  L_6 = (&V_0)->get_color_1();
		int32_t L_7 = (&V_0)->get_fontSize_2();
		float L_8 = (&V_0)->get_scaleFactor_5();
		float L_9 = (&V_0)->get_lineSpacing_3();
		int32_t L_10 = (&V_0)->get_fontStyle_6();
		bool L_11 = (&V_0)->get_richText_4();
		bool L_12 = (&V_0)->get_resizeTextForBestFit_9();
		int32_t L_13 = (&V_0)->get_resizeTextMinSize_10();
		int32_t L_14 = (&V_0)->get_resizeTextMaxSize_11();
		int32_t L_15 = (&V_0)->get_verticalOverflow_13();
		int32_t L_16 = (&V_0)->get_horizontalOverflow_14();
		bool L_17 = (&V_0)->get_updateBounds_12();
		int32_t L_18 = (&V_0)->get_textAnchor_7();
		Vector2_t4282066565  L_19 = (&V_0)->get_generationExtents_15();
		Vector2_t4282066565  L_20 = (&V_0)->get_pivot_16();
		bool L_21 = (&V_0)->get_generateOutOfBounds_17();
		bool L_22 = (&V_0)->get_alignByGeometry_8();
		bool L_23 = TextGenerator_Populate_Internal_m1773862789(__this, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, L_18, L_19, L_20, L_21, L_22, /*hidden argument*/NULL);
		__this->set_m_LastValid_4(L_23);
		bool L_24 = __this->get_m_LastValid_4();
		return L_24;
	}
}
// System.Collections.Generic.IList`1<UnityEngine.UIVertex> UnityEngine.TextGenerator::get_verts()
extern "C"  Il2CppObject* TextGenerator_get_verts_m1179011229 (TextGenerator_t538854556 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_CachedVerts_8();
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t1317283468 * L_1 = __this->get_m_Verts_5();
		TextGenerator_GetVertices_m1097518625(__this, L_1, /*hidden argument*/NULL);
		__this->set_m_CachedVerts_8((bool)1);
	}

IL_001e:
	{
		List_1_t1317283468 * L_2 = __this->get_m_Verts_5();
		return L_2;
	}
}
// System.Collections.Generic.IList`1<UnityEngine.UICharInfo> UnityEngine.TextGenerator::get_characters()
extern "C"  Il2CppObject* TextGenerator_get_characters_m3420670449 (TextGenerator_t538854556 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_CachedCharacters_9();
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t1433993036 * L_1 = __this->get_m_Characters_6();
		TextGenerator_GetCharacters_m2483060112(__this, L_1, /*hidden argument*/NULL);
		__this->set_m_CachedCharacters_9((bool)1);
	}

IL_001e:
	{
		List_1_t1433993036 * L_2 = __this->get_m_Characters_6();
		return L_2;
	}
}
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo> UnityEngine.TextGenerator::get_lines()
extern "C"  Il2CppObject* TextGenerator_get_lines_m3197542168 (TextGenerator_t538854556 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_CachedLines_10();
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t1187093738 * L_1 = __this->get_m_Lines_7();
		TextGenerator_GetLines_m2441503301(__this, L_1, /*hidden argument*/NULL);
		__this->set_m_CachedLines_10((bool)1);
	}

IL_001e:
	{
		List_1_t1187093738 * L_2 = __this->get_m_Lines_7();
		return L_2;
	}
}
// System.Void UnityEngine.TextGenerator::Init()
extern "C"  void TextGenerator_Init_m1881520001 (TextGenerator_t538854556 * __this, const MethodInfo* method)
{
	typedef void (*TextGenerator_Init_m1881520001_ftn) (TextGenerator_t538854556 *);
	static TextGenerator_Init_m1881520001_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_Init_m1881520001_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::Dispose_cpp()
extern "C"  void TextGenerator_Dispose_cpp_m959647828 (TextGenerator_t538854556 * __this, const MethodInfo* method)
{
	typedef void (*TextGenerator_Dispose_cpp_m959647828_ftn) (TextGenerator_t538854556 *);
	static TextGenerator_Dispose_cpp_m959647828_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_Dispose_cpp_m959647828_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::Dispose_cpp()");
	_il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.TextGenerator::Populate_Internal(System.String,UnityEngine.Font,UnityEngine.Color,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,UnityEngine.VerticalWrapMode,UnityEngine.HorizontalWrapMode,System.Boolean,UnityEngine.TextAnchor,UnityEngine.Vector2,UnityEngine.Vector2,System.Boolean,System.Boolean)
extern "C"  bool TextGenerator_Populate_Internal_m1773862789 (TextGenerator_t538854556 * __this, String_t* ___str0, Font_t4241557075 * ___font1, Color_t4194546905  ___color2, int32_t ___fontSize3, float ___scaleFactor4, float ___lineSpacing5, int32_t ___style6, bool ___richText7, bool ___resizeTextForBestFit8, int32_t ___resizeTextMinSize9, int32_t ___resizeTextMaxSize10, int32_t ___verticalOverFlow11, int32_t ___horizontalOverflow12, bool ___updateBounds13, int32_t ___anchor14, Vector2_t4282066565  ___extents15, Vector2_t4282066565  ___pivot16, bool ___generateOutOfBounds17, bool ___alignByGeometry18, const MethodInfo* method)
{
	{
		String_t* L_0 = ___str0;
		Font_t4241557075 * L_1 = ___font1;
		Color_t4194546905  L_2 = ___color2;
		int32_t L_3 = ___fontSize3;
		float L_4 = ___scaleFactor4;
		float L_5 = ___lineSpacing5;
		int32_t L_6 = ___style6;
		bool L_7 = ___richText7;
		bool L_8 = ___resizeTextForBestFit8;
		int32_t L_9 = ___resizeTextMinSize9;
		int32_t L_10 = ___resizeTextMaxSize10;
		int32_t L_11 = ___verticalOverFlow11;
		int32_t L_12 = ___horizontalOverflow12;
		bool L_13 = ___updateBounds13;
		int32_t L_14 = ___anchor14;
		float L_15 = (&___extents15)->get_x_1();
		float L_16 = (&___extents15)->get_y_2();
		float L_17 = (&___pivot16)->get_x_1();
		float L_18 = (&___pivot16)->get_y_2();
		bool L_19 = ___generateOutOfBounds17;
		bool L_20 = ___alignByGeometry18;
		bool L_21 = TextGenerator_Populate_Internal_cpp_m3249805403(__this, L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, L_18, L_19, L_20, /*hidden argument*/NULL);
		return L_21;
	}
}
// System.Boolean UnityEngine.TextGenerator::Populate_Internal_cpp(System.String,UnityEngine.Font,UnityEngine.Color,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean,System.Boolean)
extern "C"  bool TextGenerator_Populate_Internal_cpp_m3249805403 (TextGenerator_t538854556 * __this, String_t* ___str0, Font_t4241557075 * ___font1, Color_t4194546905  ___color2, int32_t ___fontSize3, float ___scaleFactor4, float ___lineSpacing5, int32_t ___style6, bool ___richText7, bool ___resizeTextForBestFit8, int32_t ___resizeTextMinSize9, int32_t ___resizeTextMaxSize10, int32_t ___verticalOverFlow11, int32_t ___horizontalOverflow12, bool ___updateBounds13, int32_t ___anchor14, float ___extentsX15, float ___extentsY16, float ___pivotX17, float ___pivotY18, bool ___generateOutOfBounds19, bool ___alignByGeometry20, const MethodInfo* method)
{
	{
		String_t* L_0 = ___str0;
		Font_t4241557075 * L_1 = ___font1;
		int32_t L_2 = ___fontSize3;
		float L_3 = ___scaleFactor4;
		float L_4 = ___lineSpacing5;
		int32_t L_5 = ___style6;
		bool L_6 = ___richText7;
		bool L_7 = ___resizeTextForBestFit8;
		int32_t L_8 = ___resizeTextMinSize9;
		int32_t L_9 = ___resizeTextMaxSize10;
		int32_t L_10 = ___verticalOverFlow11;
		int32_t L_11 = ___horizontalOverflow12;
		bool L_12 = ___updateBounds13;
		int32_t L_13 = ___anchor14;
		float L_14 = ___extentsX15;
		float L_15 = ___extentsY16;
		float L_16 = ___pivotX17;
		float L_17 = ___pivotY18;
		bool L_18 = ___generateOutOfBounds19;
		bool L_19 = ___alignByGeometry20;
		bool L_20 = TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m2939372809(NULL /*static, unused*/, __this, L_0, L_1, (&___color2), L_2, L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, L_18, L_19, /*hidden argument*/NULL);
		return L_20;
	}
}
// System.Boolean UnityEngine.TextGenerator::INTERNAL_CALL_Populate_Internal_cpp(UnityEngine.TextGenerator,System.String,UnityEngine.Font,UnityEngine.Color&,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean,System.Boolean)
extern "C"  bool TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m2939372809 (Il2CppObject * __this /* static, unused */, TextGenerator_t538854556 * ___self0, String_t* ___str1, Font_t4241557075 * ___font2, Color_t4194546905 * ___color3, int32_t ___fontSize4, float ___scaleFactor5, float ___lineSpacing6, int32_t ___style7, bool ___richText8, bool ___resizeTextForBestFit9, int32_t ___resizeTextMinSize10, int32_t ___resizeTextMaxSize11, int32_t ___verticalOverFlow12, int32_t ___horizontalOverflow13, bool ___updateBounds14, int32_t ___anchor15, float ___extentsX16, float ___extentsY17, float ___pivotX18, float ___pivotY19, bool ___generateOutOfBounds20, bool ___alignByGeometry21, const MethodInfo* method)
{
	typedef bool (*TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m2939372809_ftn) (TextGenerator_t538854556 *, String_t*, Font_t4241557075 *, Color_t4194546905 *, int32_t, float, float, int32_t, bool, bool, int32_t, int32_t, int32_t, int32_t, bool, int32_t, float, float, float, float, bool, bool);
	static TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m2939372809_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m2939372809_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::INTERNAL_CALL_Populate_Internal_cpp(UnityEngine.TextGenerator,System.String,UnityEngine.Font,UnityEngine.Color&,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean,System.Boolean)");
	return _il2cpp_icall_func(___self0, ___str1, ___font2, ___color3, ___fontSize4, ___scaleFactor5, ___lineSpacing6, ___style7, ___richText8, ___resizeTextForBestFit9, ___resizeTextMinSize10, ___resizeTextMaxSize11, ___verticalOverFlow12, ___horizontalOverflow13, ___updateBounds14, ___anchor15, ___extentsX16, ___extentsY17, ___pivotX18, ___pivotY19, ___generateOutOfBounds20, ___alignByGeometry21);
}
// UnityEngine.Rect UnityEngine.TextGenerator::get_rectExtents()
extern "C"  Rect_t4241904616  TextGenerator_get_rectExtents_m2200526529 (TextGenerator_t538854556 * __this, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		TextGenerator_INTERNAL_get_rectExtents_m4152002332(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t4241904616  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.TextGenerator::INTERNAL_get_rectExtents(UnityEngine.Rect&)
extern "C"  void TextGenerator_INTERNAL_get_rectExtents_m4152002332 (TextGenerator_t538854556 * __this, Rect_t4241904616 * ___value0, const MethodInfo* method)
{
	typedef void (*TextGenerator_INTERNAL_get_rectExtents_m4152002332_ftn) (TextGenerator_t538854556 *, Rect_t4241904616 *);
	static TextGenerator_INTERNAL_get_rectExtents_m4152002332_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_INTERNAL_get_rectExtents_m4152002332_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::INTERNAL_get_rectExtents(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.TextGenerator::get_vertexCount()
extern "C"  int32_t TextGenerator_get_vertexCount_m818659347 (TextGenerator_t538854556 * __this, const MethodInfo* method)
{
	typedef int32_t (*TextGenerator_get_vertexCount_m818659347_ftn) (TextGenerator_t538854556 *);
	static TextGenerator_get_vertexCount_m818659347_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_vertexCount_m818659347_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_vertexCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::GetVerticesInternal(System.Object)
extern "C"  void TextGenerator_GetVerticesInternal_m2005679319 (TextGenerator_t538854556 * __this, Il2CppObject * ___vertices0, const MethodInfo* method)
{
	typedef void (*TextGenerator_GetVerticesInternal_m2005679319_ftn) (TextGenerator_t538854556 *, Il2CppObject *);
	static TextGenerator_GetVerticesInternal_m2005679319_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetVerticesInternal_m2005679319_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetVerticesInternal(System.Object)");
	_il2cpp_icall_func(__this, ___vertices0);
}
// System.Int32 UnityEngine.TextGenerator::get_characterCount()
extern "C"  int32_t TextGenerator_get_characterCount_m1694678400 (TextGenerator_t538854556 * __this, const MethodInfo* method)
{
	typedef int32_t (*TextGenerator_get_characterCount_m1694678400_ftn) (TextGenerator_t538854556 *);
	static TextGenerator_get_characterCount_m1694678400_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_characterCount_m1694678400_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_characterCount()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.TextGenerator::get_characterCountVisible()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t TextGenerator_get_characterCountVisible_m2621262708_MetadataUsageId;
extern "C"  int32_t TextGenerator_get_characterCountVisible_m2621262708 (TextGenerator_t538854556 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextGenerator_get_characterCountVisible_m2621262708_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = __this->get_m_LastString_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_0036;
	}

IL_0016:
	{
		String_t* L_2 = __this->get_m_LastString_1();
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m2979997331(L_2, /*hidden argument*/NULL);
		int32_t L_4 = TextGenerator_get_vertexCount_m818659347(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_5 = Mathf_Max_m2911193737(NULL /*static, unused*/, 0, ((int32_t)((int32_t)((int32_t)((int32_t)L_4-(int32_t)4))/(int32_t)4)), /*hidden argument*/NULL);
		int32_t L_6 = Mathf_Min_m2413438171(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		G_B3_0 = L_6;
	}

IL_0036:
	{
		return G_B3_0;
	}
}
// System.Void UnityEngine.TextGenerator::GetCharactersInternal(System.Object)
extern "C"  void TextGenerator_GetCharactersInternal_m3189630150 (TextGenerator_t538854556 * __this, Il2CppObject * ___characters0, const MethodInfo* method)
{
	typedef void (*TextGenerator_GetCharactersInternal_m3189630150_ftn) (TextGenerator_t538854556 *, Il2CppObject *);
	static TextGenerator_GetCharactersInternal_m3189630150_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetCharactersInternal_m3189630150_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetCharactersInternal(System.Object)");
	_il2cpp_icall_func(__this, ___characters0);
}
// System.Int32 UnityEngine.TextGenerator::get_lineCount()
extern "C"  int32_t TextGenerator_get_lineCount_m3121044867 (TextGenerator_t538854556 * __this, const MethodInfo* method)
{
	typedef int32_t (*TextGenerator_get_lineCount_m3121044867_ftn) (TextGenerator_t538854556 *);
	static TextGenerator_get_lineCount_m3121044867_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_lineCount_m3121044867_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_lineCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::GetLinesInternal(System.Object)
extern "C"  void TextGenerator_GetLinesInternal_m702225117 (TextGenerator_t538854556 * __this, Il2CppObject * ___lines0, const MethodInfo* method)
{
	typedef void (*TextGenerator_GetLinesInternal_m702225117_ftn) (TextGenerator_t538854556 *, Il2CppObject *);
	static TextGenerator_GetLinesInternal_m702225117_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetLinesInternal_m702225117_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetLinesInternal(System.Object)");
	_il2cpp_icall_func(__this, ___lines0);
}
// Conversion methods for marshalling of: UnityEngine.TextGenerator
extern "C" void TextGenerator_t538854556_marshal_pinvoke(const TextGenerator_t538854556& unmarshaled, TextGenerator_t538854556_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_LastSettings_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_LastSettings' of type 'TextGenerator'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_LastSettings_2Exception);
}
extern "C" void TextGenerator_t538854556_marshal_pinvoke_back(const TextGenerator_t538854556_marshaled_pinvoke& marshaled, TextGenerator_t538854556& unmarshaled)
{
	Il2CppCodeGenException* ___m_LastSettings_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_LastSettings' of type 'TextGenerator'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_LastSettings_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.TextGenerator
extern "C" void TextGenerator_t538854556_marshal_pinvoke_cleanup(TextGenerator_t538854556_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.TextGenerator
extern "C" void TextGenerator_t538854556_marshal_com(const TextGenerator_t538854556& unmarshaled, TextGenerator_t538854556_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_LastSettings_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_LastSettings' of type 'TextGenerator'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_LastSettings_2Exception);
}
extern "C" void TextGenerator_t538854556_marshal_com_back(const TextGenerator_t538854556_marshaled_com& marshaled, TextGenerator_t538854556& unmarshaled)
{
	Il2CppCodeGenException* ___m_LastSettings_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_LastSettings' of type 'TextGenerator'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_LastSettings_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.TextGenerator
extern "C" void TextGenerator_t538854556_marshal_com_cleanup(TextGenerator_t538854556_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.TextMesh::set_text(System.String)
extern "C"  void TextMesh_set_text_m3628430759 (TextMesh_t2567681854 * __this, String_t* ___value0, const MethodInfo* method)
{
	typedef void (*TextMesh_set_text_m3628430759_ftn) (TextMesh_t2567681854 *, String_t*);
	static TextMesh_set_text_m3628430759_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextMesh_set_text_m3628430759_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextMesh::set_text(System.String)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Texture::.ctor()
extern "C"  void Texture__ctor_m516856734 (Texture_t2526458961 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m570634126(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Texture::Internal_GetWidth(UnityEngine.Texture)
extern "C"  int32_t Texture_Internal_GetWidth_m1143336192 (Il2CppObject * __this /* static, unused */, Texture_t2526458961 * ___mono0, const MethodInfo* method)
{
	typedef int32_t (*Texture_Internal_GetWidth_m1143336192_ftn) (Texture_t2526458961 *);
	static Texture_Internal_GetWidth_m1143336192_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_Internal_GetWidth_m1143336192_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::Internal_GetWidth(UnityEngine.Texture)");
	return _il2cpp_icall_func(___mono0);
}
// System.Int32 UnityEngine.Texture::Internal_GetHeight(UnityEngine.Texture)
extern "C"  int32_t Texture_Internal_GetHeight_m1065663213 (Il2CppObject * __this /* static, unused */, Texture_t2526458961 * ___mono0, const MethodInfo* method)
{
	typedef int32_t (*Texture_Internal_GetHeight_m1065663213_ftn) (Texture_t2526458961 *);
	static Texture_Internal_GetHeight_m1065663213_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_Internal_GetHeight_m1065663213_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::Internal_GetHeight(UnityEngine.Texture)");
	return _il2cpp_icall_func(___mono0);
}
// System.Int32 UnityEngine.Texture::get_width()
extern "C"  int32_t Texture_get_width_m1557399609 (Texture_t2526458961 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Texture_Internal_GetWidth_m1143336192(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Texture::set_width(System.Int32)
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4188032821;
extern const uint32_t Texture_set_width_m3343740566_MetadataUsageId;
extern "C"  void Texture_set_width_m3343740566 (Texture_t2526458961 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Texture_set_width_m3343740566_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t3991598821 * L_0 = (Exception_t3991598821 *)il2cpp_codegen_object_new(Exception_t3991598821_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_0, _stringLiteral4188032821, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 UnityEngine.Texture::get_height()
extern "C"  int32_t Texture_get_height_m1538561974 (Texture_t2526458961 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Texture_Internal_GetHeight_m1065663213(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Texture::set_height(System.Int32)
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4188032821;
extern const uint32_t Texture_set_height_m1897594619_MetadataUsageId;
extern "C"  void Texture_set_height_m1897594619 (Texture_t2526458961 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Texture_set_height_m1897594619_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t3991598821 * L_0 = (Exception_t3991598821 *)il2cpp_codegen_object_new(Exception_t3991598821_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_0, _stringLiteral4188032821, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UnityEngine.Texture::set_filterMode(UnityEngine.FilterMode)
extern "C"  void Texture_set_filterMode_m3842701708 (Texture_t2526458961 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Texture_set_filterMode_m3842701708_ftn) (Texture_t2526458961 *, int32_t);
	static Texture_set_filterMode_m3842701708_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_set_filterMode_m3842701708_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::set_filterMode(UnityEngine.FilterMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Texture::set_wrapMode(UnityEngine.TextureWrapMode)
extern "C"  void Texture_set_wrapMode_m3720633937 (Texture_t2526458961 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Texture_set_wrapMode_m3720633937_ftn) (Texture_t2526458961 *, int32_t);
	static Texture_set_wrapMode_m3720633937_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_set_wrapMode_m3720633937_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::set_wrapMode(UnityEngine.TextureWrapMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.Texture::GetNativeTextureID()
extern "C"  int32_t Texture_GetNativeTextureID_m994822479 (Texture_t2526458961 * __this, const MethodInfo* method)
{
	typedef int32_t (*Texture_GetNativeTextureID_m994822479_ftn) (Texture_t2526458961 *);
	static Texture_GetNativeTextureID_m994822479_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_GetNativeTextureID_m994822479_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::GetNativeTextureID()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t Texture2D__ctor_m1883511258_MetadataUsageId;
extern "C"  void Texture2D__ctor_m1883511258 (Texture2D_t3884108195 * __this, int32_t ___width0, int32_t ___height1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Texture2D__ctor_m1883511258_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture__ctor_m516856734(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		IntPtr_t L_2 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		Texture2D_Internal_Create_m457592211(NULL /*static, unused*/, __this, L_0, L_1, 5, (bool)1, (bool)0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t Texture2D__ctor_m3705883154_MetadataUsageId;
extern "C"  void Texture2D__ctor_m3705883154 (Texture2D_t3884108195 * __this, int32_t ___width0, int32_t ___height1, int32_t ___format2, bool ___mipmap3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Texture2D__ctor_m3705883154_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture__ctor_m516856734(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		int32_t L_2 = ___format2;
		bool L_3 = ___mipmap3;
		IntPtr_t L_4 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		Texture2D_Internal_Create_m457592211(NULL /*static, unused*/, __this, L_0, L_1, L_2, L_3, (bool)0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
extern "C"  void Texture2D_Internal_Create_m457592211 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___mono0, int32_t ___width1, int32_t ___height2, int32_t ___format3, bool ___mipmap4, bool ___linear5, IntPtr_t ___nativeTex6, const MethodInfo* method)
{
	typedef void (*Texture2D_Internal_Create_m457592211_ftn) (Texture2D_t3884108195 *, int32_t, int32_t, int32_t, bool, bool, IntPtr_t);
	static Texture2D_Internal_Create_m457592211_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_Internal_Create_m457592211_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)");
	_il2cpp_icall_func(___mono0, ___width1, ___height2, ___format3, ___mipmap4, ___linear5, ___nativeTex6);
}
// UnityEngine.TextureFormat UnityEngine.Texture2D::get_format()
extern "C"  int32_t Texture2D_get_format_m863889216 (Texture2D_t3884108195 * __this, const MethodInfo* method)
{
	typedef int32_t (*Texture2D_get_format_m863889216_ftn) (Texture2D_t3884108195 *);
	static Texture2D_get_format_m863889216_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_get_format_m863889216_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::get_format()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Texture2D UnityEngine.Texture2D::get_whiteTexture()
extern "C"  Texture2D_t3884108195 * Texture2D_get_whiteTexture_m1214146742 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef Texture2D_t3884108195 * (*Texture2D_get_whiteTexture_m1214146742_ftn) ();
	static Texture2D_get_whiteTexture_m1214146742_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_get_whiteTexture_m1214146742_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::get_whiteTexture()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Texture2D::SetPixel(System.Int32,System.Int32,UnityEngine.Color)
extern "C"  void Texture2D_SetPixel_m378278602 (Texture2D_t3884108195 * __this, int32_t ___x0, int32_t ___y1, Color_t4194546905  ___color2, const MethodInfo* method)
{
	{
		int32_t L_0 = ___x0;
		int32_t L_1 = ___y1;
		Texture2D_INTERNAL_CALL_SetPixel_m40008491(NULL /*static, unused*/, __this, L_0, L_1, (&___color2), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::INTERNAL_CALL_SetPixel(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.Color&)
extern "C"  void Texture2D_INTERNAL_CALL_SetPixel_m40008491 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___self0, int32_t ___x1, int32_t ___y2, Color_t4194546905 * ___color3, const MethodInfo* method)
{
	typedef void (*Texture2D_INTERNAL_CALL_SetPixel_m40008491_ftn) (Texture2D_t3884108195 *, int32_t, int32_t, Color_t4194546905 *);
	static Texture2D_INTERNAL_CALL_SetPixel_m40008491_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_INTERNAL_CALL_SetPixel_m40008491_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::INTERNAL_CALL_SetPixel(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.Color&)");
	_il2cpp_icall_func(___self0, ___x1, ___y2, ___color3);
}
// UnityEngine.Color UnityEngine.Texture2D::GetPixel(System.Int32,System.Int32)
extern "C"  Color_t4194546905  Texture2D_GetPixel_m380515877 (Texture2D_t3884108195 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method)
{
	Color_t4194546905  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___x0;
		int32_t L_1 = ___y1;
		Texture2D_INTERNAL_CALL_GetPixel_m3914095287(NULL /*static, unused*/, __this, L_0, L_1, (&V_0), /*hidden argument*/NULL);
		Color_t4194546905  L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Texture2D::INTERNAL_CALL_GetPixel(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.Color&)
extern "C"  void Texture2D_INTERNAL_CALL_GetPixel_m3914095287 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___self0, int32_t ___x1, int32_t ___y2, Color_t4194546905 * ___value3, const MethodInfo* method)
{
	typedef void (*Texture2D_INTERNAL_CALL_GetPixel_m3914095287_ftn) (Texture2D_t3884108195 *, int32_t, int32_t, Color_t4194546905 *);
	static Texture2D_INTERNAL_CALL_GetPixel_m3914095287_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_INTERNAL_CALL_GetPixel_m3914095287_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::INTERNAL_CALL_GetPixel(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.Color&)");
	_il2cpp_icall_func(___self0, ___x1, ___y2, ___value3);
}
// UnityEngine.Color UnityEngine.Texture2D::GetPixelBilinear(System.Single,System.Single)
extern "C"  Color_t4194546905  Texture2D_GetPixelBilinear_m2169326019 (Texture2D_t3884108195 * __this, float ___u0, float ___v1, const MethodInfo* method)
{
	Color_t4194546905  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___u0;
		float L_1 = ___v1;
		Texture2D_INTERNAL_CALL_GetPixelBilinear_m2056702809(NULL /*static, unused*/, __this, L_0, L_1, (&V_0), /*hidden argument*/NULL);
		Color_t4194546905  L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Texture2D::INTERNAL_CALL_GetPixelBilinear(UnityEngine.Texture2D,System.Single,System.Single,UnityEngine.Color&)
extern "C"  void Texture2D_INTERNAL_CALL_GetPixelBilinear_m2056702809 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___self0, float ___u1, float ___v2, Color_t4194546905 * ___value3, const MethodInfo* method)
{
	typedef void (*Texture2D_INTERNAL_CALL_GetPixelBilinear_m2056702809_ftn) (Texture2D_t3884108195 *, float, float, Color_t4194546905 *);
	static Texture2D_INTERNAL_CALL_GetPixelBilinear_m2056702809_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_INTERNAL_CALL_GetPixelBilinear_m2056702809_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::INTERNAL_CALL_GetPixelBilinear(UnityEngine.Texture2D,System.Single,System.Single,UnityEngine.Color&)");
	_il2cpp_icall_func(___self0, ___u1, ___v2, ___value3);
}
// System.Void UnityEngine.Texture2D::SetPixels(UnityEngine.Color[])
extern "C"  void Texture2D_SetPixels_m1289331147 (Texture2D_t3884108195 * __this, ColorU5BU5D_t2441545636* ___colors0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		ColorU5BU5D_t2441545636* L_0 = ___colors0;
		int32_t L_1 = V_0;
		Texture2D_SetPixels_m2710961388(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::SetPixels(UnityEngine.Color[],System.Int32)
extern "C"  void Texture2D_SetPixels_m2710961388 (Texture2D_t3884108195 * __this, ColorU5BU5D_t2441545636* ___colors0, int32_t ___miplevel1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, __this);
		int32_t L_1 = ___miplevel1;
		V_0 = ((int32_t)((int32_t)L_0>>(int32_t)((int32_t)((int32_t)L_1&(int32_t)((int32_t)31)))));
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		V_0 = 1;
	}

IL_0015:
	{
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, __this);
		int32_t L_4 = ___miplevel1;
		V_1 = ((int32_t)((int32_t)L_3>>(int32_t)((int32_t)((int32_t)L_4&(int32_t)((int32_t)31)))));
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) >= ((int32_t)1)))
		{
			goto IL_002a;
		}
	}
	{
		V_1 = 1;
	}

IL_002a:
	{
		int32_t L_6 = V_0;
		int32_t L_7 = V_1;
		ColorU5BU5D_t2441545636* L_8 = ___colors0;
		int32_t L_9 = ___miplevel1;
		Texture2D_SetPixels_m3304189612(__this, 0, 0, L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::SetPixels(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[],System.Int32)
extern "C"  void Texture2D_SetPixels_m3304189612 (Texture2D_t3884108195 * __this, int32_t ___x0, int32_t ___y1, int32_t ___blockWidth2, int32_t ___blockHeight3, ColorU5BU5D_t2441545636* ___colors4, int32_t ___miplevel5, const MethodInfo* method)
{
	typedef void (*Texture2D_SetPixels_m3304189612_ftn) (Texture2D_t3884108195 *, int32_t, int32_t, int32_t, int32_t, ColorU5BU5D_t2441545636*, int32_t);
	static Texture2D_SetPixels_m3304189612_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_SetPixels_m3304189612_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::SetPixels(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[],System.Int32)");
	_il2cpp_icall_func(__this, ___x0, ___y1, ___blockWidth2, ___blockHeight3, ___colors4, ___miplevel5);
}
// System.Void UnityEngine.Texture2D::SetAllPixels32(UnityEngine.Color32[],System.Int32)
extern "C"  void Texture2D_SetAllPixels32_m2090735797 (Texture2D_t3884108195 * __this, Color32U5BU5D_t2960766953* ___colors0, int32_t ___miplevel1, const MethodInfo* method)
{
	typedef void (*Texture2D_SetAllPixels32_m2090735797_ftn) (Texture2D_t3884108195 *, Color32U5BU5D_t2960766953*, int32_t);
	static Texture2D_SetAllPixels32_m2090735797_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_SetAllPixels32_m2090735797_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::SetAllPixels32(UnityEngine.Color32[],System.Int32)");
	_il2cpp_icall_func(__this, ___colors0, ___miplevel1);
}
// System.Void UnityEngine.Texture2D::SetPixels32(UnityEngine.Color32[])
extern "C"  void Texture2D_SetPixels32_m1350049485 (Texture2D_t3884108195 * __this, Color32U5BU5D_t2960766953* ___colors0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		Color32U5BU5D_t2960766953* L_0 = ___colors0;
		int32_t L_1 = V_0;
		Texture2D_SetPixels32_m4259722026(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::SetPixels32(UnityEngine.Color32[],System.Int32)
extern "C"  void Texture2D_SetPixels32_m4259722026 (Texture2D_t3884108195 * __this, Color32U5BU5D_t2960766953* ___colors0, int32_t ___miplevel1, const MethodInfo* method)
{
	{
		Color32U5BU5D_t2960766953* L_0 = ___colors0;
		int32_t L_1 = ___miplevel1;
		Texture2D_SetAllPixels32_m2090735797(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Texture2D::LoadImage(System.Byte[],System.Boolean)
extern "C"  bool Texture2D_LoadImage_m1576144569 (Texture2D_t3884108195 * __this, ByteU5BU5D_t4260760469* ___data0, bool ___markNonReadable1, const MethodInfo* method)
{
	typedef bool (*Texture2D_LoadImage_m1576144569_ftn) (Texture2D_t3884108195 *, ByteU5BU5D_t4260760469*, bool);
	static Texture2D_LoadImage_m1576144569_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_LoadImage_m1576144569_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::LoadImage(System.Byte[],System.Boolean)");
	return _il2cpp_icall_func(__this, ___data0, ___markNonReadable1);
}
// System.Boolean UnityEngine.Texture2D::LoadImage(System.Byte[])
extern "C"  bool Texture2D_LoadImage_m2186196036 (Texture2D_t3884108195 * __this, ByteU5BU5D_t4260760469* ___data0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		ByteU5BU5D_t4260760469* L_0 = ___data0;
		bool L_1 = V_0;
		bool L_2 = Texture2D_LoadImage_m1576144569(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Color[] UnityEngine.Texture2D::GetPixels()
extern "C"  ColorU5BU5D_t2441545636* Texture2D_GetPixels_m1759701362 (Texture2D_t3884108195 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		ColorU5BU5D_t2441545636* L_1 = Texture2D_GetPixels_m3014919107(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Color[] UnityEngine.Texture2D::GetPixels(System.Int32)
extern "C"  ColorU5BU5D_t2441545636* Texture2D_GetPixels_m3014919107 (Texture2D_t3884108195 * __this, int32_t ___miplevel0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, __this);
		int32_t L_1 = ___miplevel0;
		V_0 = ((int32_t)((int32_t)L_0>>(int32_t)((int32_t)((int32_t)L_1&(int32_t)((int32_t)31)))));
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		V_0 = 1;
	}

IL_0015:
	{
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, __this);
		int32_t L_4 = ___miplevel0;
		V_1 = ((int32_t)((int32_t)L_3>>(int32_t)((int32_t)((int32_t)L_4&(int32_t)((int32_t)31)))));
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) >= ((int32_t)1)))
		{
			goto IL_002a;
		}
	}
	{
		V_1 = 1;
	}

IL_002a:
	{
		int32_t L_6 = V_0;
		int32_t L_7 = V_1;
		int32_t L_8 = ___miplevel0;
		ColorU5BU5D_t2441545636* L_9 = Texture2D_GetPixels_m871852803(__this, 0, 0, L_6, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// UnityEngine.Color[] UnityEngine.Texture2D::GetPixels(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  ColorU5BU5D_t2441545636* Texture2D_GetPixels_m871852803 (Texture2D_t3884108195 * __this, int32_t ___x0, int32_t ___y1, int32_t ___blockWidth2, int32_t ___blockHeight3, int32_t ___miplevel4, const MethodInfo* method)
{
	typedef ColorU5BU5D_t2441545636* (*Texture2D_GetPixels_m871852803_ftn) (Texture2D_t3884108195 *, int32_t, int32_t, int32_t, int32_t, int32_t);
	static Texture2D_GetPixels_m871852803_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_GetPixels_m871852803_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::GetPixels(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)");
	return _il2cpp_icall_func(__this, ___x0, ___y1, ___blockWidth2, ___blockHeight3, ___miplevel4);
}
// UnityEngine.Color32[] UnityEngine.Texture2D::GetPixels32(System.Int32)
extern "C"  Color32U5BU5D_t2960766953* Texture2D_GetPixels32_m327121251 (Texture2D_t3884108195 * __this, int32_t ___miplevel0, const MethodInfo* method)
{
	typedef Color32U5BU5D_t2960766953* (*Texture2D_GetPixels32_m327121251_ftn) (Texture2D_t3884108195 *, int32_t);
	static Texture2D_GetPixels32_m327121251_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_GetPixels32_m327121251_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::GetPixels32(System.Int32)");
	return _il2cpp_icall_func(__this, ___miplevel0);
}
// UnityEngine.Color32[] UnityEngine.Texture2D::GetPixels32()
extern "C"  Color32U5BU5D_t2960766953* Texture2D_GetPixels32_m4076457746 (Texture2D_t3884108195 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		Color32U5BU5D_t2960766953* L_1 = Texture2D_GetPixels32_m327121251(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)
extern "C"  void Texture2D_Apply_m2754532430 (Texture2D_t3884108195 * __this, bool ___updateMipmaps0, bool ___makeNoLongerReadable1, const MethodInfo* method)
{
	typedef void (*Texture2D_Apply_m2754532430_ftn) (Texture2D_t3884108195 *, bool, bool);
	static Texture2D_Apply_m2754532430_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_Apply_m2754532430_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)");
	_il2cpp_icall_func(__this, ___updateMipmaps0, ___makeNoLongerReadable1);
}
// System.Void UnityEngine.Texture2D::Apply()
extern "C"  void Texture2D_Apply_m1364130776 (Texture2D_t3884108195 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	bool V_1 = false;
	{
		V_0 = (bool)0;
		V_1 = (bool)1;
		bool L_0 = V_1;
		bool L_1 = V_0;
		Texture2D_Apply_m2754532430(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Texture2D::Resize(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern "C"  bool Texture2D_Resize_m1334821900 (Texture2D_t3884108195 * __this, int32_t ___width0, int32_t ___height1, int32_t ___format2, bool ___hasMipMap3, const MethodInfo* method)
{
	typedef bool (*Texture2D_Resize_m1334821900_ftn) (Texture2D_t3884108195 *, int32_t, int32_t, int32_t, bool);
	static Texture2D_Resize_m1334821900_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_Resize_m1334821900_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::Resize(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)");
	return _il2cpp_icall_func(__this, ___width0, ___height1, ___format2, ___hasMipMap3);
}
// System.Boolean UnityEngine.Texture2D::Resize(System.Int32,System.Int32)
extern "C"  bool Texture2D_Resize_m3471718048 (Texture2D_t3884108195 * __this, int32_t ___width0, int32_t ___height1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		bool L_2 = Texture2D_Internal_ResizeWH_m3271797007(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Texture2D::Internal_ResizeWH(System.Int32,System.Int32)
extern "C"  bool Texture2D_Internal_ResizeWH_m3271797007 (Texture2D_t3884108195 * __this, int32_t ___width0, int32_t ___height1, const MethodInfo* method)
{
	typedef bool (*Texture2D_Internal_ResizeWH_m3271797007_ftn) (Texture2D_t3884108195 *, int32_t, int32_t);
	static Texture2D_Internal_ResizeWH_m3271797007_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_Internal_ResizeWH_m3271797007_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::Internal_ResizeWH(System.Int32,System.Int32)");
	return _il2cpp_icall_func(__this, ___width0, ___height1);
}
// System.Void UnityEngine.Texture2D::ReadPixels(UnityEngine.Rect,System.Int32,System.Int32,System.Boolean)
extern "C"  void Texture2D_ReadPixels_m2828402557 (Texture2D_t3884108195 * __this, Rect_t4241904616  ___source0, int32_t ___destX1, int32_t ___destY2, bool ___recalculateMipMaps3, const MethodInfo* method)
{
	{
		int32_t L_0 = ___destX1;
		int32_t L_1 = ___destY2;
		bool L_2 = ___recalculateMipMaps3;
		Texture2D_INTERNAL_CALL_ReadPixels_m388307610(NULL /*static, unused*/, __this, (&___source0), L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::ReadPixels(UnityEngine.Rect,System.Int32,System.Int32)
extern "C"  void Texture2D_ReadPixels_m1334301696 (Texture2D_t3884108195 * __this, Rect_t4241904616  ___source0, int32_t ___destX1, int32_t ___destY2, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)1;
		int32_t L_0 = ___destX1;
		int32_t L_1 = ___destY2;
		bool L_2 = V_0;
		Texture2D_INTERNAL_CALL_ReadPixels_m388307610(NULL /*static, unused*/, __this, (&___source0), L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::INTERNAL_CALL_ReadPixels(UnityEngine.Texture2D,UnityEngine.Rect&,System.Int32,System.Int32,System.Boolean)
extern "C"  void Texture2D_INTERNAL_CALL_ReadPixels_m388307610 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___self0, Rect_t4241904616 * ___source1, int32_t ___destX2, int32_t ___destY3, bool ___recalculateMipMaps4, const MethodInfo* method)
{
	typedef void (*Texture2D_INTERNAL_CALL_ReadPixels_m388307610_ftn) (Texture2D_t3884108195 *, Rect_t4241904616 *, int32_t, int32_t, bool);
	static Texture2D_INTERNAL_CALL_ReadPixels_m388307610_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_INTERNAL_CALL_ReadPixels_m388307610_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::INTERNAL_CALL_ReadPixels(UnityEngine.Texture2D,UnityEngine.Rect&,System.Int32,System.Int32,System.Boolean)");
	_il2cpp_icall_func(___self0, ___source1, ___destX2, ___destY3, ___recalculateMipMaps4);
}
// System.Byte[] UnityEngine.Texture2D::EncodeToPNG()
extern "C"  ByteU5BU5D_t4260760469* Texture2D_EncodeToPNG_m2464495756 (Texture2D_t3884108195 * __this, const MethodInfo* method)
{
	typedef ByteU5BU5D_t4260760469* (*Texture2D_EncodeToPNG_m2464495756_ftn) (Texture2D_t3884108195 *);
	static Texture2D_EncodeToPNG_m2464495756_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_EncodeToPNG_m2464495756_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::EncodeToPNG()");
	return _il2cpp_icall_func(__this);
}
// System.Byte[] UnityEngine.Texture2D::EncodeToJPG(System.Int32)
extern "C"  ByteU5BU5D_t4260760469* Texture2D_EncodeToJPG_m1915683733 (Texture2D_t3884108195 * __this, int32_t ___quality0, const MethodInfo* method)
{
	typedef ByteU5BU5D_t4260760469* (*Texture2D_EncodeToJPG_m1915683733_ftn) (Texture2D_t3884108195 *, int32_t);
	static Texture2D_EncodeToJPG_m1915683733_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_EncodeToJPG_m1915683733_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::EncodeToJPG(System.Int32)");
	return _il2cpp_icall_func(__this, ___quality0);
}
// System.Byte[] UnityEngine.Texture2D::EncodeToJPG()
extern "C"  ByteU5BU5D_t4260760469* Texture2D_EncodeToJPG_m2459014212 (Texture2D_t3884108195 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t4260760469* L_0 = Texture2D_EncodeToJPG_m1915683733(__this, ((int32_t)75), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.Time::get_time()
extern "C"  float Time_get_time_m342192902 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_time_m342192902_ftn) ();
	static Time_get_time_m342192902_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_time_m342192902_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_time()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_timeSinceLevelLoad()
extern "C"  float Time_get_timeSinceLevelLoad_m441028310 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_timeSinceLevelLoad_m441028310_ftn) ();
	static Time_get_timeSinceLevelLoad_m441028310_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_timeSinceLevelLoad_m441028310_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_timeSinceLevelLoad()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m2741110510 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_deltaTime_m2741110510_ftn) ();
	static Time_get_deltaTime_m2741110510_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_deltaTime_m2741110510_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_deltaTime()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_unscaledTime()
extern "C"  float Time_get_unscaledTime_m319114521 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_unscaledTime_m319114521_ftn) ();
	static Time_get_unscaledTime_m319114521_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_unscaledTime_m319114521_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_unscaledTime()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_unscaledDeltaTime()
extern "C"  float Time_get_unscaledDeltaTime_m285638843 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_unscaledDeltaTime_m285638843_ftn) ();
	static Time_get_unscaledDeltaTime_m285638843_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_unscaledDeltaTime_m285638843_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_unscaledDeltaTime()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Time::set_fixedDeltaTime(System.Single)
extern "C"  void Time_set_fixedDeltaTime_m2294328761 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*Time_set_fixedDeltaTime_m2294328761_ftn) (float);
	static Time_set_fixedDeltaTime_m2294328761_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_set_fixedDeltaTime_m2294328761_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::set_fixedDeltaTime(System.Single)");
	_il2cpp_icall_func(___value0);
}
// System.Single UnityEngine.Time::get_smoothDeltaTime()
extern "C"  float Time_get_smoothDeltaTime_m1119418976 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_smoothDeltaTime_m1119418976_ftn) ();
	static Time_get_smoothDeltaTime_m1119418976_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_smoothDeltaTime_m1119418976_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_smoothDeltaTime()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_timeScale()
extern "C"  float Time_get_timeScale_m1970669766 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_timeScale_m1970669766_ftn) ();
	static Time_get_timeScale_m1970669766_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_timeScale_m1970669766_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_timeScale()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Time::set_timeScale(System.Single)
extern "C"  void Time_set_timeScale_m1848691981 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*Time_set_timeScale_m1848691981_ftn) (float);
	static Time_set_timeScale_m1848691981_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_set_timeScale_m1848691981_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::set_timeScale(System.Single)");
	_il2cpp_icall_func(___value0);
}
// System.Int32 UnityEngine.Time::get_frameCount()
extern "C"  int32_t Time_get_frameCount_m3434184975 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Time_get_frameCount_m3434184975_ftn) ();
	static Time_get_frameCount_m3434184975_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_frameCount_m3434184975_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_frameCount()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_realtimeSinceStartup()
extern "C"  float Time_get_realtimeSinceStartup_m2972554983 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_realtimeSinceStartup_m2972554983_ftn) ();
	static Time_get_realtimeSinceStartup_m2972554983_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_realtimeSinceStartup_m2972554983_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_realtimeSinceStartup()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
extern "C"  void TooltipAttribute__ctor_m3341446606 (TooltipAttribute_t1877437789 * __this, String_t* ___tooltip0, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m1741701746(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___tooltip0;
		__this->set_tooltip_0(L_0);
		return;
	}
}
// System.Int32 UnityEngine.Touch::get_fingerId()
extern "C"  int32_t Touch_get_fingerId_m1427167959 (Touch_t4210255029 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_FingerId_0();
		return L_0;
	}
}
extern "C"  int32_t Touch_get_fingerId_m1427167959_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Touch_t4210255029 * _thisAdjusted = reinterpret_cast<Touch_t4210255029 *>(__this + 1);
	return Touch_get_fingerId_m1427167959(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
extern "C"  Vector2_t4282066565  Touch_get_position_m1943849441 (Touch_t4210255029 * __this, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0 = __this->get_m_Position_1();
		return L_0;
	}
}
extern "C"  Vector2_t4282066565  Touch_get_position_m1943849441_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Touch_t4210255029 * _thisAdjusted = reinterpret_cast<Touch_t4210255029 *>(__this + 1);
	return Touch_get_position_m1943849441(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Touch::get_deltaPosition()
extern "C"  Vector2_t4282066565  Touch_get_deltaPosition_m3983677995 (Touch_t4210255029 * __this, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0 = __this->get_m_PositionDelta_3();
		return L_0;
	}
}
extern "C"  Vector2_t4282066565  Touch_get_deltaPosition_m3983677995_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Touch_t4210255029 * _thisAdjusted = reinterpret_cast<Touch_t4210255029 *>(__this + 1);
	return Touch_get_deltaPosition_m3983677995(_thisAdjusted, method);
}
// System.Single UnityEngine.Touch::get_deltaTime()
extern "C"  float Touch_get_deltaTime_m2685662144 (Touch_t4210255029 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_TimeDelta_4();
		return L_0;
	}
}
extern "C"  float Touch_get_deltaTime_m2685662144_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Touch_t4210255029 * _thisAdjusted = reinterpret_cast<Touch_t4210255029 *>(__this + 1);
	return Touch_get_deltaTime_m2685662144(_thisAdjusted, method);
}
// System.Int32 UnityEngine.Touch::get_tapCount()
extern "C"  int32_t Touch_get_tapCount_m2141151647 (Touch_t4210255029 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_TapCount_5();
		return L_0;
	}
}
extern "C"  int32_t Touch_get_tapCount_m2141151647_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Touch_t4210255029 * _thisAdjusted = reinterpret_cast<Touch_t4210255029 *>(__this + 1);
	return Touch_get_tapCount_m2141151647(_thisAdjusted, method);
}
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
extern "C"  int32_t Touch_get_phase_m3314549414 (Touch_t4210255029 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Phase_6();
		return L_0;
	}
}
extern "C"  int32_t Touch_get_phase_m3314549414_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Touch_t4210255029 * _thisAdjusted = reinterpret_cast<Touch_t4210255029 *>(__this + 1);
	return Touch_get_phase_m3314549414(_thisAdjusted, method);
}
// UnityEngine.TouchType UnityEngine.Touch::get_type()
extern "C"  int32_t Touch_get_type_m1428398012 (Touch_t4210255029 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Type_7();
		return L_0;
	}
}
extern "C"  int32_t Touch_get_type_m1428398012_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Touch_t4210255029 * _thisAdjusted = reinterpret_cast<Touch_t4210255029 *>(__this + 1);
	return Touch_get_type_m1428398012(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.Touch
extern "C" void Touch_t4210255029_marshal_pinvoke(const Touch_t4210255029& unmarshaled, Touch_t4210255029_marshaled_pinvoke& marshaled)
{
	marshaled.___m_FingerId_0 = unmarshaled.get_m_FingerId_0();
	Vector2_t4282066565_marshal_pinvoke(unmarshaled.get_m_Position_1(), marshaled.___m_Position_1);
	Vector2_t4282066565_marshal_pinvoke(unmarshaled.get_m_RawPosition_2(), marshaled.___m_RawPosition_2);
	Vector2_t4282066565_marshal_pinvoke(unmarshaled.get_m_PositionDelta_3(), marshaled.___m_PositionDelta_3);
	marshaled.___m_TimeDelta_4 = unmarshaled.get_m_TimeDelta_4();
	marshaled.___m_TapCount_5 = unmarshaled.get_m_TapCount_5();
	marshaled.___m_Phase_6 = unmarshaled.get_m_Phase_6();
	marshaled.___m_Type_7 = unmarshaled.get_m_Type_7();
	marshaled.___m_Pressure_8 = unmarshaled.get_m_Pressure_8();
	marshaled.___m_maximumPossiblePressure_9 = unmarshaled.get_m_maximumPossiblePressure_9();
	marshaled.___m_Radius_10 = unmarshaled.get_m_Radius_10();
	marshaled.___m_RadiusVariance_11 = unmarshaled.get_m_RadiusVariance_11();
	marshaled.___m_AltitudeAngle_12 = unmarshaled.get_m_AltitudeAngle_12();
	marshaled.___m_AzimuthAngle_13 = unmarshaled.get_m_AzimuthAngle_13();
}
extern "C" void Touch_t4210255029_marshal_pinvoke_back(const Touch_t4210255029_marshaled_pinvoke& marshaled, Touch_t4210255029& unmarshaled)
{
	int32_t unmarshaled_m_FingerId_temp_0 = 0;
	unmarshaled_m_FingerId_temp_0 = marshaled.___m_FingerId_0;
	unmarshaled.set_m_FingerId_0(unmarshaled_m_FingerId_temp_0);
	Vector2_t4282066565  unmarshaled_m_Position_temp_1;
	memset(&unmarshaled_m_Position_temp_1, 0, sizeof(unmarshaled_m_Position_temp_1));
	Vector2_t4282066565_marshal_pinvoke_back(marshaled.___m_Position_1, unmarshaled_m_Position_temp_1);
	unmarshaled.set_m_Position_1(unmarshaled_m_Position_temp_1);
	Vector2_t4282066565  unmarshaled_m_RawPosition_temp_2;
	memset(&unmarshaled_m_RawPosition_temp_2, 0, sizeof(unmarshaled_m_RawPosition_temp_2));
	Vector2_t4282066565_marshal_pinvoke_back(marshaled.___m_RawPosition_2, unmarshaled_m_RawPosition_temp_2);
	unmarshaled.set_m_RawPosition_2(unmarshaled_m_RawPosition_temp_2);
	Vector2_t4282066565  unmarshaled_m_PositionDelta_temp_3;
	memset(&unmarshaled_m_PositionDelta_temp_3, 0, sizeof(unmarshaled_m_PositionDelta_temp_3));
	Vector2_t4282066565_marshal_pinvoke_back(marshaled.___m_PositionDelta_3, unmarshaled_m_PositionDelta_temp_3);
	unmarshaled.set_m_PositionDelta_3(unmarshaled_m_PositionDelta_temp_3);
	float unmarshaled_m_TimeDelta_temp_4 = 0.0f;
	unmarshaled_m_TimeDelta_temp_4 = marshaled.___m_TimeDelta_4;
	unmarshaled.set_m_TimeDelta_4(unmarshaled_m_TimeDelta_temp_4);
	int32_t unmarshaled_m_TapCount_temp_5 = 0;
	unmarshaled_m_TapCount_temp_5 = marshaled.___m_TapCount_5;
	unmarshaled.set_m_TapCount_5(unmarshaled_m_TapCount_temp_5);
	int32_t unmarshaled_m_Phase_temp_6 = 0;
	unmarshaled_m_Phase_temp_6 = marshaled.___m_Phase_6;
	unmarshaled.set_m_Phase_6(unmarshaled_m_Phase_temp_6);
	int32_t unmarshaled_m_Type_temp_7 = 0;
	unmarshaled_m_Type_temp_7 = marshaled.___m_Type_7;
	unmarshaled.set_m_Type_7(unmarshaled_m_Type_temp_7);
	float unmarshaled_m_Pressure_temp_8 = 0.0f;
	unmarshaled_m_Pressure_temp_8 = marshaled.___m_Pressure_8;
	unmarshaled.set_m_Pressure_8(unmarshaled_m_Pressure_temp_8);
	float unmarshaled_m_maximumPossiblePressure_temp_9 = 0.0f;
	unmarshaled_m_maximumPossiblePressure_temp_9 = marshaled.___m_maximumPossiblePressure_9;
	unmarshaled.set_m_maximumPossiblePressure_9(unmarshaled_m_maximumPossiblePressure_temp_9);
	float unmarshaled_m_Radius_temp_10 = 0.0f;
	unmarshaled_m_Radius_temp_10 = marshaled.___m_Radius_10;
	unmarshaled.set_m_Radius_10(unmarshaled_m_Radius_temp_10);
	float unmarshaled_m_RadiusVariance_temp_11 = 0.0f;
	unmarshaled_m_RadiusVariance_temp_11 = marshaled.___m_RadiusVariance_11;
	unmarshaled.set_m_RadiusVariance_11(unmarshaled_m_RadiusVariance_temp_11);
	float unmarshaled_m_AltitudeAngle_temp_12 = 0.0f;
	unmarshaled_m_AltitudeAngle_temp_12 = marshaled.___m_AltitudeAngle_12;
	unmarshaled.set_m_AltitudeAngle_12(unmarshaled_m_AltitudeAngle_temp_12);
	float unmarshaled_m_AzimuthAngle_temp_13 = 0.0f;
	unmarshaled_m_AzimuthAngle_temp_13 = marshaled.___m_AzimuthAngle_13;
	unmarshaled.set_m_AzimuthAngle_13(unmarshaled_m_AzimuthAngle_temp_13);
}
// Conversion method for clean up from marshalling of: UnityEngine.Touch
extern "C" void Touch_t4210255029_marshal_pinvoke_cleanup(Touch_t4210255029_marshaled_pinvoke& marshaled)
{
	Vector2_t4282066565_marshal_pinvoke_cleanup(marshaled.___m_Position_1);
	Vector2_t4282066565_marshal_pinvoke_cleanup(marshaled.___m_RawPosition_2);
	Vector2_t4282066565_marshal_pinvoke_cleanup(marshaled.___m_PositionDelta_3);
}
// Conversion methods for marshalling of: UnityEngine.Touch
extern "C" void Touch_t4210255029_marshal_com(const Touch_t4210255029& unmarshaled, Touch_t4210255029_marshaled_com& marshaled)
{
	marshaled.___m_FingerId_0 = unmarshaled.get_m_FingerId_0();
	Vector2_t4282066565_marshal_com(unmarshaled.get_m_Position_1(), marshaled.___m_Position_1);
	Vector2_t4282066565_marshal_com(unmarshaled.get_m_RawPosition_2(), marshaled.___m_RawPosition_2);
	Vector2_t4282066565_marshal_com(unmarshaled.get_m_PositionDelta_3(), marshaled.___m_PositionDelta_3);
	marshaled.___m_TimeDelta_4 = unmarshaled.get_m_TimeDelta_4();
	marshaled.___m_TapCount_5 = unmarshaled.get_m_TapCount_5();
	marshaled.___m_Phase_6 = unmarshaled.get_m_Phase_6();
	marshaled.___m_Type_7 = unmarshaled.get_m_Type_7();
	marshaled.___m_Pressure_8 = unmarshaled.get_m_Pressure_8();
	marshaled.___m_maximumPossiblePressure_9 = unmarshaled.get_m_maximumPossiblePressure_9();
	marshaled.___m_Radius_10 = unmarshaled.get_m_Radius_10();
	marshaled.___m_RadiusVariance_11 = unmarshaled.get_m_RadiusVariance_11();
	marshaled.___m_AltitudeAngle_12 = unmarshaled.get_m_AltitudeAngle_12();
	marshaled.___m_AzimuthAngle_13 = unmarshaled.get_m_AzimuthAngle_13();
}
extern "C" void Touch_t4210255029_marshal_com_back(const Touch_t4210255029_marshaled_com& marshaled, Touch_t4210255029& unmarshaled)
{
	int32_t unmarshaled_m_FingerId_temp_0 = 0;
	unmarshaled_m_FingerId_temp_0 = marshaled.___m_FingerId_0;
	unmarshaled.set_m_FingerId_0(unmarshaled_m_FingerId_temp_0);
	Vector2_t4282066565  unmarshaled_m_Position_temp_1;
	memset(&unmarshaled_m_Position_temp_1, 0, sizeof(unmarshaled_m_Position_temp_1));
	Vector2_t4282066565_marshal_com_back(marshaled.___m_Position_1, unmarshaled_m_Position_temp_1);
	unmarshaled.set_m_Position_1(unmarshaled_m_Position_temp_1);
	Vector2_t4282066565  unmarshaled_m_RawPosition_temp_2;
	memset(&unmarshaled_m_RawPosition_temp_2, 0, sizeof(unmarshaled_m_RawPosition_temp_2));
	Vector2_t4282066565_marshal_com_back(marshaled.___m_RawPosition_2, unmarshaled_m_RawPosition_temp_2);
	unmarshaled.set_m_RawPosition_2(unmarshaled_m_RawPosition_temp_2);
	Vector2_t4282066565  unmarshaled_m_PositionDelta_temp_3;
	memset(&unmarshaled_m_PositionDelta_temp_3, 0, sizeof(unmarshaled_m_PositionDelta_temp_3));
	Vector2_t4282066565_marshal_com_back(marshaled.___m_PositionDelta_3, unmarshaled_m_PositionDelta_temp_3);
	unmarshaled.set_m_PositionDelta_3(unmarshaled_m_PositionDelta_temp_3);
	float unmarshaled_m_TimeDelta_temp_4 = 0.0f;
	unmarshaled_m_TimeDelta_temp_4 = marshaled.___m_TimeDelta_4;
	unmarshaled.set_m_TimeDelta_4(unmarshaled_m_TimeDelta_temp_4);
	int32_t unmarshaled_m_TapCount_temp_5 = 0;
	unmarshaled_m_TapCount_temp_5 = marshaled.___m_TapCount_5;
	unmarshaled.set_m_TapCount_5(unmarshaled_m_TapCount_temp_5);
	int32_t unmarshaled_m_Phase_temp_6 = 0;
	unmarshaled_m_Phase_temp_6 = marshaled.___m_Phase_6;
	unmarshaled.set_m_Phase_6(unmarshaled_m_Phase_temp_6);
	int32_t unmarshaled_m_Type_temp_7 = 0;
	unmarshaled_m_Type_temp_7 = marshaled.___m_Type_7;
	unmarshaled.set_m_Type_7(unmarshaled_m_Type_temp_7);
	float unmarshaled_m_Pressure_temp_8 = 0.0f;
	unmarshaled_m_Pressure_temp_8 = marshaled.___m_Pressure_8;
	unmarshaled.set_m_Pressure_8(unmarshaled_m_Pressure_temp_8);
	float unmarshaled_m_maximumPossiblePressure_temp_9 = 0.0f;
	unmarshaled_m_maximumPossiblePressure_temp_9 = marshaled.___m_maximumPossiblePressure_9;
	unmarshaled.set_m_maximumPossiblePressure_9(unmarshaled_m_maximumPossiblePressure_temp_9);
	float unmarshaled_m_Radius_temp_10 = 0.0f;
	unmarshaled_m_Radius_temp_10 = marshaled.___m_Radius_10;
	unmarshaled.set_m_Radius_10(unmarshaled_m_Radius_temp_10);
	float unmarshaled_m_RadiusVariance_temp_11 = 0.0f;
	unmarshaled_m_RadiusVariance_temp_11 = marshaled.___m_RadiusVariance_11;
	unmarshaled.set_m_RadiusVariance_11(unmarshaled_m_RadiusVariance_temp_11);
	float unmarshaled_m_AltitudeAngle_temp_12 = 0.0f;
	unmarshaled_m_AltitudeAngle_temp_12 = marshaled.___m_AltitudeAngle_12;
	unmarshaled.set_m_AltitudeAngle_12(unmarshaled_m_AltitudeAngle_temp_12);
	float unmarshaled_m_AzimuthAngle_temp_13 = 0.0f;
	unmarshaled_m_AzimuthAngle_temp_13 = marshaled.___m_AzimuthAngle_13;
	unmarshaled.set_m_AzimuthAngle_13(unmarshaled_m_AzimuthAngle_temp_13);
}
// Conversion method for clean up from marshalling of: UnityEngine.Touch
extern "C" void Touch_t4210255029_marshal_com_cleanup(Touch_t4210255029_marshaled_com& marshaled)
{
	Vector2_t4282066565_marshal_com_cleanup(marshaled.___m_Position_1);
	Vector2_t4282066565_marshal_com_cleanup(marshaled.___m_RawPosition_2);
	Vector2_t4282066565_marshal_com_cleanup(marshaled.___m_PositionDelta_3);
}
// System.Void UnityEngine.TouchScreenKeyboard::.ctor(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String)
extern Il2CppClass* TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572_il2cpp_TypeInfo_var;
extern Il2CppClass* TouchScreenKeyboardType_t2604324130_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern const uint32_t TouchScreenKeyboard__ctor_m3607055310_MetadataUsageId;
extern "C"  void TouchScreenKeyboard__ctor_m3607055310 (TouchScreenKeyboard_t1858258760 * __this, String_t* ___text0, int32_t ___keyboardType1, bool ___autocorrection2, bool ___multiline3, bool ___secure4, bool ___alert5, String_t* ___textPlaceholder6, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TouchScreenKeyboard__ctor_m3607055310_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Initobj (TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_0 = ___keyboardType1;
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(TouchScreenKeyboardType_t2604324130_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		uint32_t L_3 = Convert_ToUInt32_m2991910559(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		(&V_0)->set_keyboardType_0(L_3);
		bool L_4 = ___autocorrection2;
		uint32_t L_5 = Convert_ToUInt32_m175516108(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		(&V_0)->set_autocorrection_1(L_5);
		bool L_6 = ___multiline3;
		uint32_t L_7 = Convert_ToUInt32_m175516108(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		(&V_0)->set_multiline_2(L_7);
		bool L_8 = ___secure4;
		uint32_t L_9 = Convert_ToUInt32_m175516108(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		(&V_0)->set_secure_3(L_9);
		bool L_10 = ___alert5;
		uint32_t L_11 = Convert_ToUInt32_m175516108(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		(&V_0)->set_alert_4(L_11);
		String_t* L_12 = ___text0;
		String_t* L_13 = ___textPlaceholder6;
		TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m3839791280(__this, (&V_0), L_12, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TouchScreenKeyboard::Destroy()
extern "C"  void TouchScreenKeyboard_Destroy_m3865079199 (TouchScreenKeyboard_t1858258760 * __this, const MethodInfo* method)
{
	typedef void (*TouchScreenKeyboard_Destroy_m3865079199_ftn) (TouchScreenKeyboard_t1858258760 *);
	static TouchScreenKeyboard_Destroy_m3865079199_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_Destroy_m3865079199_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::Destroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TouchScreenKeyboard::Finalize()
extern "C"  void TouchScreenKeyboard_Finalize_m541415163 (TouchScreenKeyboard_t1858258760 * __this, const MethodInfo* method)
{
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		TouchScreenKeyboard_Destroy_m3865079199(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m3027285644(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.TouchScreenKeyboard::TouchScreenKeyboard_InternalConstructorHelper(UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments&,System.String,System.String)
extern "C"  void TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m3839791280 (TouchScreenKeyboard_t1858258760 * __this, TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572 * ___arguments0, String_t* ___text1, String_t* ___textPlaceholder2, const MethodInfo* method)
{
	typedef void (*TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m3839791280_ftn) (TouchScreenKeyboard_t1858258760 *, TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572 *, String_t*, String_t*);
	static TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m3839791280_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m3839791280_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::TouchScreenKeyboard_InternalConstructorHelper(UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments&,System.String,System.String)");
	_il2cpp_icall_func(__this, ___arguments0, ___text1, ___textPlaceholder2);
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_isSupported()
extern "C"  bool TouchScreenKeyboard_get_isSupported_m2472329766 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		V_1 = L_1;
		int32_t L_2 = V_1;
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 0)
		{
			goto IL_0068;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 1)
		{
			goto IL_0068;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 2)
		{
			goto IL_0068;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 3)
		{
			goto IL_0066;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 4)
		{
			goto IL_0066;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 5)
		{
			goto IL_0066;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 6)
		{
			goto IL_0049;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 7)
		{
			goto IL_0049;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 8)
		{
			goto IL_0066;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 9)
		{
			goto IL_0049;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 10)
		{
			goto IL_0049;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 11)
		{
			goto IL_0049;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 12)
		{
			goto IL_0066;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 13)
		{
			goto IL_0066;
		}
	}

IL_0049:
	{
		int32_t L_3 = V_1;
		if (((int32_t)((int32_t)L_3-(int32_t)8)) == 0)
		{
			goto IL_0066;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)8)) == 1)
		{
			goto IL_006a;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)8)) == 2)
		{
			goto IL_006a;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)8)) == 3)
		{
			goto IL_0066;
		}
	}
	{
		goto IL_006a;
	}

IL_0066:
	{
		return (bool)1;
	}

IL_0068:
	{
		return (bool)0;
	}

IL_006a:
	{
		return (bool)0;
	}
}
// UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t TouchScreenKeyboard_Open_m3070776627_MetadataUsageId;
extern "C"  TouchScreenKeyboard_t1858258760 * TouchScreenKeyboard_Open_m3070776627 (Il2CppObject * __this /* static, unused */, String_t* ___text0, int32_t ___keyboardType1, bool ___autocorrection2, bool ___multiline3, bool ___secure4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TouchScreenKeyboard_Open_m3070776627_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		V_1 = (bool)0;
		String_t* L_1 = ___text0;
		int32_t L_2 = ___keyboardType1;
		bool L_3 = ___autocorrection2;
		bool L_4 = ___multiline3;
		bool L_5 = ___secure4;
		bool L_6 = V_1;
		String_t* L_7 = V_0;
		TouchScreenKeyboard_t1858258760 * L_8 = TouchScreenKeyboard_Open_m3970504870(NULL /*static, unused*/, L_1, L_2, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t TouchScreenKeyboard_Open_m751274250_MetadataUsageId;
extern "C"  TouchScreenKeyboard_t1858258760 * TouchScreenKeyboard_Open_m751274250 (Il2CppObject * __this /* static, unused */, String_t* ___text0, int32_t ___keyboardType1, bool ___autocorrection2, bool ___multiline3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TouchScreenKeyboard_Open_m751274250_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		V_1 = (bool)0;
		V_2 = (bool)0;
		String_t* L_1 = ___text0;
		int32_t L_2 = ___keyboardType1;
		bool L_3 = ___autocorrection2;
		bool L_4 = ___multiline3;
		bool L_5 = V_2;
		bool L_6 = V_1;
		String_t* L_7 = V_0;
		TouchScreenKeyboard_t1858258760 * L_8 = TouchScreenKeyboard_Open_m3970504870(NULL /*static, unused*/, L_1, L_2, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String)
extern Il2CppClass* TouchScreenKeyboard_t1858258760_il2cpp_TypeInfo_var;
extern const uint32_t TouchScreenKeyboard_Open_m3970504870_MetadataUsageId;
extern "C"  TouchScreenKeyboard_t1858258760 * TouchScreenKeyboard_Open_m3970504870 (Il2CppObject * __this /* static, unused */, String_t* ___text0, int32_t ___keyboardType1, bool ___autocorrection2, bool ___multiline3, bool ___secure4, bool ___alert5, String_t* ___textPlaceholder6, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TouchScreenKeyboard_Open_m3970504870_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___text0;
		int32_t L_1 = ___keyboardType1;
		bool L_2 = ___autocorrection2;
		bool L_3 = ___multiline3;
		bool L_4 = ___secure4;
		bool L_5 = ___alert5;
		String_t* L_6 = ___textPlaceholder6;
		TouchScreenKeyboard_t1858258760 * L_7 = (TouchScreenKeyboard_t1858258760 *)il2cpp_codegen_object_new(TouchScreenKeyboard_t1858258760_il2cpp_TypeInfo_var);
		TouchScreenKeyboard__ctor_m3607055310(L_7, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.String UnityEngine.TouchScreenKeyboard::get_text()
extern "C"  String_t* TouchScreenKeyboard_get_text_m2874389744 (TouchScreenKeyboard_t1858258760 * __this, const MethodInfo* method)
{
	typedef String_t* (*TouchScreenKeyboard_get_text_m2874389744_ftn) (TouchScreenKeyboard_t1858258760 *);
	static TouchScreenKeyboard_get_text_m2874389744_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_text_m2874389744_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_text()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TouchScreenKeyboard::set_text(System.String)
extern "C"  void TouchScreenKeyboard_set_text_m1654889403 (TouchScreenKeyboard_t1858258760 * __this, String_t* ___value0, const MethodInfo* method)
{
	typedef void (*TouchScreenKeyboard_set_text_m1654889403_ftn) (TouchScreenKeyboard_t1858258760 *, String_t*);
	static TouchScreenKeyboard_set_text_m1654889403_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_set_text_m1654889403_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::set_text(System.String)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.TouchScreenKeyboard::set_hideInput(System.Boolean)
extern "C"  void TouchScreenKeyboard_set_hideInput_m2518551559 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*TouchScreenKeyboard_set_hideInput_m2518551559_ftn) (bool);
	static TouchScreenKeyboard_set_hideInput_m2518551559_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_set_hideInput_m2518551559_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::set_hideInput(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_active()
extern "C"  bool TouchScreenKeyboard_get_active_m4287082086 (TouchScreenKeyboard_t1858258760 * __this, const MethodInfo* method)
{
	typedef bool (*TouchScreenKeyboard_get_active_m4287082086_ftn) (TouchScreenKeyboard_t1858258760 *);
	static TouchScreenKeyboard_get_active_m4287082086_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_active_m4287082086_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_active()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TouchScreenKeyboard::set_active(System.Boolean)
extern "C"  void TouchScreenKeyboard_set_active_m3667959159 (TouchScreenKeyboard_t1858258760 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*TouchScreenKeyboard_set_active_m3667959159_ftn) (TouchScreenKeyboard_t1858258760 *, bool);
	static TouchScreenKeyboard_set_active_m3667959159_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_set_active_m3667959159_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::set_active(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_done()
extern "C"  bool TouchScreenKeyboard_get_done_m25268130 (TouchScreenKeyboard_t1858258760 * __this, const MethodInfo* method)
{
	typedef bool (*TouchScreenKeyboard_get_done_m25268130_ftn) (TouchScreenKeyboard_t1858258760 *);
	static TouchScreenKeyboard_get_done_m25268130_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_done_m25268130_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_done()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_wasCanceled()
extern "C"  bool TouchScreenKeyboard_get_wasCanceled_m3910688420 (TouchScreenKeyboard_t1858258760 * __this, const MethodInfo* method)
{
	typedef bool (*TouchScreenKeyboard_get_wasCanceled_m3910688420_ftn) (TouchScreenKeyboard_t1858258760 *);
	static TouchScreenKeyboard_get_wasCanceled_m3910688420_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_wasCanceled_m3910688420_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_wasCanceled()");
	return _il2cpp_icall_func(__this);
}
// Conversion methods for marshalling of: UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments
extern "C" void TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572_marshal_pinvoke(const TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572& unmarshaled, TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572_marshaled_pinvoke& marshaled)
{
	marshaled.___keyboardType_0 = unmarshaled.get_keyboardType_0();
	marshaled.___autocorrection_1 = unmarshaled.get_autocorrection_1();
	marshaled.___multiline_2 = unmarshaled.get_multiline_2();
	marshaled.___secure_3 = unmarshaled.get_secure_3();
	marshaled.___alert_4 = unmarshaled.get_alert_4();
}
extern "C" void TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572_marshal_pinvoke_back(const TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572_marshaled_pinvoke& marshaled, TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572& unmarshaled)
{
	uint32_t unmarshaled_keyboardType_temp_0 = 0;
	unmarshaled_keyboardType_temp_0 = marshaled.___keyboardType_0;
	unmarshaled.set_keyboardType_0(unmarshaled_keyboardType_temp_0);
	uint32_t unmarshaled_autocorrection_temp_1 = 0;
	unmarshaled_autocorrection_temp_1 = marshaled.___autocorrection_1;
	unmarshaled.set_autocorrection_1(unmarshaled_autocorrection_temp_1);
	uint32_t unmarshaled_multiline_temp_2 = 0;
	unmarshaled_multiline_temp_2 = marshaled.___multiline_2;
	unmarshaled.set_multiline_2(unmarshaled_multiline_temp_2);
	uint32_t unmarshaled_secure_temp_3 = 0;
	unmarshaled_secure_temp_3 = marshaled.___secure_3;
	unmarshaled.set_secure_3(unmarshaled_secure_temp_3);
	uint32_t unmarshaled_alert_temp_4 = 0;
	unmarshaled_alert_temp_4 = marshaled.___alert_4;
	unmarshaled.set_alert_4(unmarshaled_alert_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments
extern "C" void TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572_marshal_pinvoke_cleanup(TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments
extern "C" void TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572_marshal_com(const TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572& unmarshaled, TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572_marshaled_com& marshaled)
{
	marshaled.___keyboardType_0 = unmarshaled.get_keyboardType_0();
	marshaled.___autocorrection_1 = unmarshaled.get_autocorrection_1();
	marshaled.___multiline_2 = unmarshaled.get_multiline_2();
	marshaled.___secure_3 = unmarshaled.get_secure_3();
	marshaled.___alert_4 = unmarshaled.get_alert_4();
}
extern "C" void TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572_marshal_com_back(const TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572_marshaled_com& marshaled, TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572& unmarshaled)
{
	uint32_t unmarshaled_keyboardType_temp_0 = 0;
	unmarshaled_keyboardType_temp_0 = marshaled.___keyboardType_0;
	unmarshaled.set_keyboardType_0(unmarshaled_keyboardType_temp_0);
	uint32_t unmarshaled_autocorrection_temp_1 = 0;
	unmarshaled_autocorrection_temp_1 = marshaled.___autocorrection_1;
	unmarshaled.set_autocorrection_1(unmarshaled_autocorrection_temp_1);
	uint32_t unmarshaled_multiline_temp_2 = 0;
	unmarshaled_multiline_temp_2 = marshaled.___multiline_2;
	unmarshaled.set_multiline_2(unmarshaled_multiline_temp_2);
	uint32_t unmarshaled_secure_temp_3 = 0;
	unmarshaled_secure_temp_3 = marshaled.___secure_3;
	unmarshaled.set_secure_3(unmarshaled_secure_temp_3);
	uint32_t unmarshaled_alert_temp_4 = 0;
	unmarshaled_alert_temp_4 = marshaled.___alert_4;
	unmarshaled.set_alert_4(unmarshaled_alert_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments
extern "C" void TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572_marshal_com_cleanup(TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572_marshaled_com& marshaled)
{
}
// System.Boolean UnityEngine.TrackedReference::Equals(System.Object)
extern Il2CppClass* TrackedReference_t2089686725_il2cpp_TypeInfo_var;
extern const uint32_t TrackedReference_Equals_m732758423_MetadataUsageId;
extern "C"  bool TrackedReference_Equals_m732758423 (TrackedReference_t2089686725 * __this, Il2CppObject * ___o0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TrackedReference_Equals_m732758423_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___o0;
		bool L_1 = TrackedReference_op_Equality_m4125598506(NULL /*static, unused*/, ((TrackedReference_t2089686725 *)IsInstClass(L_0, TrackedReference_t2089686725_il2cpp_TypeInfo_var)), __this, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 UnityEngine.TrackedReference::GetHashCode()
extern "C"  int32_t TrackedReference_GetHashCode_m894516347 (TrackedReference_t2089686725 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = __this->get_m_Ptr_0();
		int32_t L_1 = IntPtr_op_Explicit_m1500672818(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.TrackedReference::op_Equality(UnityEngine.TrackedReference,UnityEngine.TrackedReference)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t TrackedReference_op_Equality_m4125598506_MetadataUsageId;
extern "C"  bool TrackedReference_op_Equality_m4125598506 (Il2CppObject * __this /* static, unused */, TrackedReference_t2089686725 * ___x0, TrackedReference_t2089686725 * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TrackedReference_op_Equality_m4125598506_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		TrackedReference_t2089686725 * L_0 = ___x0;
		V_0 = L_0;
		TrackedReference_t2089686725 * L_1 = ___y1;
		V_1 = L_1;
		Il2CppObject * L_2 = V_1;
		if (L_2)
		{
			goto IL_0012;
		}
	}
	{
		Il2CppObject * L_3 = V_0;
		if (L_3)
		{
			goto IL_0012;
		}
	}
	{
		return (bool)1;
	}

IL_0012:
	{
		Il2CppObject * L_4 = V_1;
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		TrackedReference_t2089686725 * L_5 = ___x0;
		NullCheck(L_5);
		IntPtr_t L_6 = L_5->get_m_Ptr_0();
		IntPtr_t L_7 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_8 = IntPtr_op_Equality_m72843924(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0029:
	{
		Il2CppObject * L_9 = V_0;
		if (L_9)
		{
			goto IL_0040;
		}
	}
	{
		TrackedReference_t2089686725 * L_10 = ___y1;
		NullCheck(L_10);
		IntPtr_t L_11 = L_10->get_m_Ptr_0();
		IntPtr_t L_12 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_13 = IntPtr_op_Equality_m72843924(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		return L_13;
	}

IL_0040:
	{
		TrackedReference_t2089686725 * L_14 = ___x0;
		NullCheck(L_14);
		IntPtr_t L_15 = L_14->get_m_Ptr_0();
		TrackedReference_t2089686725 * L_16 = ___y1;
		NullCheck(L_16);
		IntPtr_t L_17 = L_16->get_m_Ptr_0();
		bool L_18 = IntPtr_op_Equality_m72843924(NULL /*static, unused*/, L_15, L_17, /*hidden argument*/NULL);
		return L_18;
	}
}
// System.Boolean UnityEngine.TrackedReference::op_Inequality(UnityEngine.TrackedReference,UnityEngine.TrackedReference)
extern "C"  bool TrackedReference_op_Inequality_m2008054821 (Il2CppObject * __this /* static, unused */, TrackedReference_t2089686725 * ___x0, TrackedReference_t2089686725 * ___y1, const MethodInfo* method)
{
	{
		TrackedReference_t2089686725 * L_0 = ___x0;
		TrackedReference_t2089686725 * L_1 = ___y1;
		bool L_2 = TrackedReference_op_Equality_m4125598506(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.TrackedReference
extern "C" void TrackedReference_t2089686725_marshal_pinvoke(const TrackedReference_t2089686725& unmarshaled, TrackedReference_t2089686725_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void TrackedReference_t2089686725_marshal_pinvoke_back(const TrackedReference_t2089686725_marshaled_pinvoke& marshaled, TrackedReference_t2089686725& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.TrackedReference
extern "C" void TrackedReference_t2089686725_marshal_pinvoke_cleanup(TrackedReference_t2089686725_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.TrackedReference
extern "C" void TrackedReference_t2089686725_marshal_com(const TrackedReference_t2089686725& unmarshaled, TrackedReference_t2089686725_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void TrackedReference_t2089686725_marshal_com_back(const TrackedReference_t2089686725_marshaled_com& marshaled, TrackedReference_t2089686725& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.TrackedReference
extern "C" void TrackedReference_t2089686725_marshal_com_cleanup(TrackedReference_t2089686725_marshaled_com& marshaled)
{
}
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t4282066566  Transform_get_position_m2211398607 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_INTERNAL_get_position_m1705230066(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m3111394108 (Transform_t1659122786 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_position_m1126232166(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_position_m1705230066 (Transform_t1659122786 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_position_m1705230066_ftn) (Transform_t1659122786 *, Vector3_t4282066566 *);
	static Transform_INTERNAL_get_position_m1705230066_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_position_m1705230066_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_set_position_m1126232166 (Transform_t1659122786 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_position_m1126232166_ftn) (Transform_t1659122786 *, Vector3_t4282066566 *);
	static Transform_INTERNAL_set_position_m1126232166_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_position_m1126232166_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
extern "C"  Vector3_t4282066566  Transform_get_localPosition_m668140784 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_INTERNAL_get_localPosition_m2703574131(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C"  void Transform_set_localPosition_m3504330903 (Transform_t1659122786 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_localPosition_m221305727(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_localPosition_m2703574131 (Transform_t1659122786 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localPosition_m2703574131_ftn) (Transform_t1659122786 *, Vector3_t4282066566 *);
	static Transform_INTERNAL_get_localPosition_m2703574131_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localPosition_m2703574131_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_set_localPosition_m221305727 (Transform_t1659122786 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_localPosition_m221305727_ftn) (Transform_t1659122786 *, Vector3_t4282066566 *);
	static Transform_INTERNAL_set_localPosition_m221305727_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localPosition_m221305727_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_eulerAngles()
extern "C"  Vector3_t4282066566  Transform_get_eulerAngles_m1058084741 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_t1553702882  L_0 = Transform_get_rotation_m11483428(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_t4282066566  L_1 = Quaternion_get_eulerAngles_m997303795((&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Transform::set_eulerAngles(UnityEngine.Vector3)
extern "C"  void Transform_set_eulerAngles_m1704681314 (Transform_t1659122786 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = ___value0;
		Quaternion_t1553702882  L_1 = Quaternion_Euler_m1940911101(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Transform_set_rotation_m1525803229(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localEulerAngles()
extern "C"  Vector3_t4282066566  Transform_get_localEulerAngles_m3489183428 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_INTERNAL_get_localEulerAngles_m2659489127(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_localEulerAngles(UnityEngine.Vector3)
extern "C"  void Transform_set_localEulerAngles_m3898859559 (Transform_t1659122786 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_localEulerAngles_m4148368091(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localEulerAngles(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_localEulerAngles_m2659489127 (Transform_t1659122786 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localEulerAngles_m2659489127_ftn) (Transform_t1659122786 *, Vector3_t4282066566 *);
	static Transform_INTERNAL_get_localEulerAngles_m2659489127_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localEulerAngles_m2659489127_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localEulerAngles(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localEulerAngles(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_set_localEulerAngles_m4148368091 (Transform_t1659122786 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_localEulerAngles_m4148368091_ftn) (Transform_t1659122786 *, Vector3_t4282066566 *);
	static Transform_INTERNAL_set_localEulerAngles_m4148368091_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localEulerAngles_m4148368091_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localEulerAngles(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_right()
extern "C"  Vector3_t4282066566  Transform_get_right_m2070836824 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	{
		Quaternion_t1553702882  L_0 = Transform_get_rotation_m11483428(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_1 = Vector3_get_right_m4015137012(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_2 = Quaternion_op_Multiply_m3771288979(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::get_up()
extern "C"  Vector3_t4282066566  Transform_get_up_m297874561 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	{
		Quaternion_t1553702882  L_0 = Transform_get_rotation_m11483428(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_1 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_2 = Quaternion_op_Multiply_m3771288979(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Transform::set_up(UnityEngine.Vector3)
extern "C"  void Transform_set_up_m2430652106 (Transform_t1659122786 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_1 = ___value0;
		Quaternion_t1553702882  L_2 = Quaternion_FromToRotation_m2335489018(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Transform_set_rotation_m1525803229(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
extern "C"  Vector3_t4282066566  Transform_get_forward_m877665793 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	{
		Quaternion_t1553702882  L_0 = Transform_get_rotation_m11483428(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_1 = Vector3_get_forward_m1039372701(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_2 = Quaternion_op_Multiply_m3771288979(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C"  Quaternion_t1553702882  Transform_get_rotation_m11483428 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_INTERNAL_get_rotation_m2389720173(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_rotation_m1525803229 (Transform_t1659122786 * __this, Quaternion_t1553702882  ___value0, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_rotation_m2051942009(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_get_rotation_m2389720173 (Transform_t1659122786 * __this, Quaternion_t1553702882 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_rotation_m2389720173_ftn) (Transform_t1659122786 *, Quaternion_t1553702882 *);
	static Transform_INTERNAL_get_rotation_m2389720173_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_rotation_m2389720173_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_rotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_set_rotation_m2051942009 (Transform_t1659122786 * __this, Quaternion_t1553702882 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_rotation_m2051942009_ftn) (Transform_t1659122786 *, Quaternion_t1553702882 *);
	static Transform_INTERNAL_set_rotation_m2051942009_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_rotation_m2051942009_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Quaternion UnityEngine.Transform::get_localRotation()
extern "C"  Quaternion_t1553702882  Transform_get_localRotation_m3343229381 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_INTERNAL_get_localRotation_m1409235788(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_localRotation_m3719981474 (Transform_t1659122786 * __this, Quaternion_t1553702882  ___value0, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_localRotation_m2898114752(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_get_localRotation_m1409235788 (Transform_t1659122786 * __this, Quaternion_t1553702882 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localRotation_m1409235788_ftn) (Transform_t1659122786 *, Quaternion_t1553702882 *);
	static Transform_INTERNAL_get_localRotation_m1409235788_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localRotation_m1409235788_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_set_localRotation_m2898114752 (Transform_t1659122786 * __this, Quaternion_t1553702882 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_localRotation_m2898114752_ftn) (Transform_t1659122786 *, Quaternion_t1553702882 *);
	static Transform_INTERNAL_set_localRotation_m2898114752_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localRotation_m2898114752_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
extern "C"  Vector3_t4282066566  Transform_get_localScale_m3886572677 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_INTERNAL_get_localScale_m1534477480(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C"  void Transform_set_localScale_m310756934 (Transform_t1659122786 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_localScale_m3463244060(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_localScale_m1534477480 (Transform_t1659122786 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localScale_m1534477480_ftn) (Transform_t1659122786 *, Vector3_t4282066566 *);
	static Transform_INTERNAL_get_localScale_m1534477480_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localScale_m1534477480_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_set_localScale_m3463244060 (Transform_t1659122786 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_localScale_m3463244060_ftn) (Transform_t1659122786 *, Vector3_t4282066566 *);
	static Transform_INTERNAL_set_localScale_m3463244060_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localScale_m3463244060_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C"  Transform_t1659122786 * Transform_get_parent_m2236876972 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	{
		Transform_t1659122786 * L_0 = Transform_get_parentInternal_m3763475785(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
extern Il2CppClass* RectTransform_t972643934_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral509676363;
extern const uint32_t Transform_set_parent_m3231272063_MetadataUsageId;
extern "C"  void Transform_set_parent_m3231272063 (Transform_t1659122786 * __this, Transform_t1659122786 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_set_parent_m3231272063_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		if (!((RectTransform_t972643934 *)IsInstSealed(__this, RectTransform_t972643934_il2cpp_TypeInfo_var)))
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogWarning_m4097176146(NULL /*static, unused*/, _stringLiteral509676363, __this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		Transform_t1659122786 * L_0 = ___value0;
		Transform_set_parentInternal_m1254352386(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform UnityEngine.Transform::get_parentInternal()
extern "C"  Transform_t1659122786 * Transform_get_parentInternal_m3763475785 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	typedef Transform_t1659122786 * (*Transform_get_parentInternal_m3763475785_ftn) (Transform_t1659122786 *);
	static Transform_get_parentInternal_m3763475785_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_parentInternal_m3763475785_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_parentInternal()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)
extern "C"  void Transform_set_parentInternal_m1254352386 (Transform_t1659122786 * __this, Transform_t1659122786 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_set_parentInternal_m1254352386_ftn) (Transform_t1659122786 *, Transform_t1659122786 *);
	static Transform_set_parentInternal_m1254352386_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_set_parentInternal_m1254352386_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform)
extern "C"  void Transform_SetParent_m3449663462 (Transform_t1659122786 * __this, Transform_t1659122786 * ___parent0, const MethodInfo* method)
{
	{
		Transform_t1659122786 * L_0 = ___parent0;
		Transform_SetParent_m263985879(__this, L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)
extern "C"  void Transform_SetParent_m263985879 (Transform_t1659122786 * __this, Transform_t1659122786 * ___parent0, bool ___worldPositionStays1, const MethodInfo* method)
{
	typedef void (*Transform_SetParent_m263985879_ftn) (Transform_t1659122786 *, Transform_t1659122786 *, bool);
	static Transform_SetParent_m263985879_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_SetParent_m263985879_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)");
	_il2cpp_icall_func(__this, ___parent0, ___worldPositionStays1);
}
// UnityEngine.Matrix4x4 UnityEngine.Transform::get_worldToLocalMatrix()
extern "C"  Matrix4x4_t1651859333  Transform_get_worldToLocalMatrix_m3792395652 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	Matrix4x4_t1651859333  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_INTERNAL_get_worldToLocalMatrix_m3861589095(__this, (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t1651859333  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Transform_INTERNAL_get_worldToLocalMatrix_m3861589095 (Transform_t1659122786 * __this, Matrix4x4_t1651859333 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_worldToLocalMatrix_m3861589095_ftn) (Transform_t1659122786 *, Matrix4x4_t1651859333 *);
	static Transform_INTERNAL_get_worldToLocalMatrix_m3861589095_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_worldToLocalMatrix_m3861589095_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Matrix4x4 UnityEngine.Transform::get_localToWorldMatrix()
extern "C"  Matrix4x4_t1651859333  Transform_get_localToWorldMatrix_m3571020210 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	Matrix4x4_t1651859333  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_INTERNAL_get_localToWorldMatrix_m3747334677(__this, (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t1651859333  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localToWorldMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Transform_INTERNAL_get_localToWorldMatrix_m3747334677 (Transform_t1659122786 * __this, Matrix4x4_t1651859333 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localToWorldMatrix_m3747334677_ftn) (Transform_t1659122786 *, Matrix4x4_t1651859333 *);
	static Transform_INTERNAL_get_localToWorldMatrix_m3747334677_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localToWorldMatrix_m3747334677_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localToWorldMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3)
extern "C"  void Transform_Translate_m2849099360 (Transform_t1659122786 * __this, Vector3_t4282066566  ___translation0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 1;
		Vector3_t4282066566  L_0 = ___translation0;
		int32_t L_1 = V_0;
		Transform_Translate_m1056984957(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3,UnityEngine.Space)
extern "C"  void Transform_Translate_m1056984957 (Transform_t1659122786 * __this, Vector3_t4282066566  ___translation0, int32_t ___relativeTo1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___relativeTo1;
		if (L_0)
		{
			goto IL_001d;
		}
	}
	{
		Vector3_t4282066566  L_1 = Transform_get_position_m2211398607(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_2 = ___translation0;
		Vector3_t4282066566  L_3 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Transform_set_position_m3111394108(__this, L_3, /*hidden argument*/NULL);
		goto IL_0035;
	}

IL_001d:
	{
		Vector3_t4282066566  L_4 = Transform_get_position_m2211398607(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_5 = ___translation0;
		Vector3_t4282066566  L_6 = Transform_TransformDirection_m83001769(__this, L_5, /*hidden argument*/NULL);
		Vector3_t4282066566  L_7 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		Transform_set_position_m3111394108(__this, L_7, /*hidden argument*/NULL);
	}

IL_0035:
	{
		return;
	}
}
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3)
extern "C"  void Transform_Rotate_m637363399 (Transform_t1659122786 * __this, Vector3_t4282066566  ___eulerAngles0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 1;
		Vector3_t4282066566  L_0 = ___eulerAngles0;
		int32_t L_1 = V_0;
		Transform_Rotate_m3141515812(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3,UnityEngine.Space)
extern "C"  void Transform_Rotate_m3141515812 (Transform_t1659122786 * __this, Vector3_t4282066566  ___eulerAngles0, int32_t ___relativeTo1, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___eulerAngles0)->get_x_1();
		float L_1 = (&___eulerAngles0)->get_y_2();
		float L_2 = (&___eulerAngles0)->get_z_3();
		Quaternion_t1553702882  L_3 = Quaternion_Euler_m1204688217(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = ___relativeTo1;
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0039;
		}
	}
	{
		Quaternion_t1553702882  L_5 = Transform_get_localRotation_m3343229381(__this, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_6 = V_0;
		Quaternion_t1553702882  L_7 = Quaternion_op_Multiply_m3077481361(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		Transform_set_localRotation_m3719981474(__this, L_7, /*hidden argument*/NULL);
		goto IL_0066;
	}

IL_0039:
	{
		Quaternion_t1553702882  L_8 = Transform_get_rotation_m11483428(__this, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_9 = Transform_get_rotation_m11483428(__this, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_10 = Quaternion_Inverse_m3542515566(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_11 = V_0;
		Quaternion_t1553702882  L_12 = Quaternion_op_Multiply_m3077481361(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_13 = Transform_get_rotation_m11483428(__this, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_14 = Quaternion_op_Multiply_m3077481361(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_15 = Quaternion_op_Multiply_m3077481361(NULL /*static, unused*/, L_8, L_14, /*hidden argument*/NULL);
		Transform_set_rotation_m1525803229(__this, L_15, /*hidden argument*/NULL);
	}

IL_0066:
	{
		return;
	}
}
// System.Void UnityEngine.Transform::RotateAroundInternal(UnityEngine.Vector3,System.Single)
extern "C"  void Transform_RotateAroundInternal_m3394506018 (Transform_t1659122786 * __this, Vector3_t4282066566  ___axis0, float ___angle1, const MethodInfo* method)
{
	{
		float L_0 = ___angle1;
		Transform_INTERNAL_CALL_RotateAroundInternal_m4163317180(NULL /*static, unused*/, __this, (&___axis0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_RotateAroundInternal(UnityEngine.Transform,UnityEngine.Vector3&,System.Single)
extern "C"  void Transform_INTERNAL_CALL_RotateAroundInternal_m4163317180 (Il2CppObject * __this /* static, unused */, Transform_t1659122786 * ___self0, Vector3_t4282066566 * ___axis1, float ___angle2, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_CALL_RotateAroundInternal_m4163317180_ftn) (Transform_t1659122786 *, Vector3_t4282066566 *, float);
	static Transform_INTERNAL_CALL_RotateAroundInternal_m4163317180_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_RotateAroundInternal_m4163317180_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_RotateAroundInternal(UnityEngine.Transform,UnityEngine.Vector3&,System.Single)");
	_il2cpp_icall_func(___self0, ___axis1, ___angle2);
}
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3,System.Single)
extern "C"  void Transform_Rotate_m4090005356 (Transform_t1659122786 * __this, Vector3_t4282066566  ___axis0, float ___angle1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 1;
		Vector3_t4282066566  L_0 = ___axis0;
		float L_1 = ___angle1;
		int32_t L_2 = V_0;
		Transform_Rotate_m4229389705(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3,System.Single,UnityEngine.Space)
extern "C"  void Transform_Rotate_m4229389705 (Transform_t1659122786 * __this, Vector3_t4282066566  ___axis0, float ___angle1, int32_t ___relativeTo2, const MethodInfo* method)
{
	{
		int32_t L_0 = ___relativeTo2;
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0025;
		}
	}
	{
		Transform_t1659122786 * L_1 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_2 = ___axis0;
		NullCheck(L_1);
		Vector3_t4282066566  L_3 = Transform_TransformDirection_m83001769(L_1, L_2, /*hidden argument*/NULL);
		float L_4 = ___angle1;
		Transform_RotateAroundInternal_m3394506018(__this, L_3, ((float)((float)L_4*(float)(0.0174532924f))), /*hidden argument*/NULL);
		goto IL_0033;
	}

IL_0025:
	{
		Vector3_t4282066566  L_5 = ___axis0;
		float L_6 = ___angle1;
		Transform_RotateAroundInternal_m3394506018(__this, L_5, ((float)((float)L_6*(float)(0.0174532924f))), /*hidden argument*/NULL);
	}

IL_0033:
	{
		return;
	}
}
// System.Void UnityEngine.Transform::RotateAround(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  void Transform_RotateAround_m2745906802 (Transform_t1659122786 * __this, Vector3_t4282066566  ___point0, Vector3_t4282066566  ___axis1, float ___angle2, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t1553702882  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Vector3_t4282066566  L_0 = Transform_get_position_m2211398607(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___angle2;
		Vector3_t4282066566  L_2 = ___axis1;
		Quaternion_t1553702882  L_3 = Quaternion_AngleAxis_m644124247(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		Vector3_t4282066566  L_4 = V_0;
		Vector3_t4282066566  L_5 = ___point0;
		Vector3_t4282066566  L_6 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		Quaternion_t1553702882  L_7 = V_1;
		Vector3_t4282066566  L_8 = V_2;
		Vector3_t4282066566  L_9 = Quaternion_op_Multiply_m3771288979(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		Vector3_t4282066566  L_10 = ___point0;
		Vector3_t4282066566  L_11 = V_2;
		Vector3_t4282066566  L_12 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		Vector3_t4282066566  L_13 = V_0;
		Transform_set_position_m3111394108(__this, L_13, /*hidden argument*/NULL);
		Vector3_t4282066566  L_14 = ___axis1;
		float L_15 = ___angle2;
		Transform_RotateAroundInternal_m3394506018(__this, L_14, ((float)((float)L_15*(float)(0.0174532924f))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Transform)
extern "C"  void Transform_LookAt_m2663225588 (Transform_t1659122786 * __this, Transform_t1659122786 * ___target0, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t4282066566  L_0 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Transform_t1659122786 * L_1 = ___target0;
		Vector3_t4282066566  L_2 = V_0;
		Transform_LookAt_m3252223111(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Transform,UnityEngine.Vector3)
extern "C"  void Transform_LookAt_m3252223111 (Transform_t1659122786 * __this, Transform_t1659122786 * ___target0, Vector3_t4282066566  ___worldUp1, const MethodInfo* method)
{
	{
		Transform_t1659122786 * L_0 = ___target0;
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		Transform_t1659122786 * L_2 = ___target0;
		NullCheck(L_2);
		Vector3_t4282066566  L_3 = Transform_get_position_m2211398607(L_2, /*hidden argument*/NULL);
		Vector3_t4282066566  L_4 = ___worldUp1;
		Transform_LookAt_m2054691043(__this, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Transform_LookAt_m2054691043 (Transform_t1659122786 * __this, Vector3_t4282066566  ___worldPosition0, Vector3_t4282066566  ___worldUp1, const MethodInfo* method)
{
	{
		Transform_INTERNAL_CALL_LookAt_m302711157(NULL /*static, unused*/, __this, (&___worldPosition0), (&___worldUp1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Vector3)
extern "C"  void Transform_LookAt_m724138832 (Transform_t1659122786 * __this, Vector3_t4282066566  ___worldPosition0, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t4282066566  L_0 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Transform_INTERNAL_CALL_LookAt_m302711157(NULL /*static, unused*/, __this, (&___worldPosition0), (&V_0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_LookAt(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_LookAt_m302711157 (Il2CppObject * __this /* static, unused */, Transform_t1659122786 * ___self0, Vector3_t4282066566 * ___worldPosition1, Vector3_t4282066566 * ___worldUp2, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_CALL_LookAt_m302711157_ftn) (Transform_t1659122786 *, Vector3_t4282066566 *, Vector3_t4282066566 *);
	static Transform_INTERNAL_CALL_LookAt_m302711157_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_LookAt_m302711157_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_LookAt(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___worldPosition1, ___worldUp2);
}
// UnityEngine.Vector3 UnityEngine.Transform::TransformDirection(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Transform_TransformDirection_m83001769 (Transform_t1659122786 * __this, Vector3_t4282066566  ___direction0, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_INTERNAL_CALL_TransformDirection_m3704788692(NULL /*static, unused*/, __this, (&___direction0), (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_TransformDirection(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_TransformDirection_m3704788692 (Il2CppObject * __this /* static, unused */, Transform_t1659122786 * ___self0, Vector3_t4282066566 * ___direction1, Vector3_t4282066566 * ___value2, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_CALL_TransformDirection_m3704788692_ftn) (Transform_t1659122786 *, Vector3_t4282066566 *, Vector3_t4282066566 *);
	static Transform_INTERNAL_CALL_TransformDirection_m3704788692_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_TransformDirection_m3704788692_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_TransformDirection(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___direction1, ___value2);
}
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformDirection(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Transform_InverseTransformDirection_m416562129 (Transform_t1659122786 * __this, Vector3_t4282066566  ___direction0, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_INTERNAL_CALL_InverseTransformDirection_m1169769038(NULL /*static, unused*/, __this, (&___direction0), (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_InverseTransformDirection(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_InverseTransformDirection_m1169769038 (Il2CppObject * __this /* static, unused */, Transform_t1659122786 * ___self0, Vector3_t4282066566 * ___direction1, Vector3_t4282066566 * ___value2, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_CALL_InverseTransformDirection_m1169769038_ftn) (Transform_t1659122786 *, Vector3_t4282066566 *, Vector3_t4282066566 *);
	static Transform_INTERNAL_CALL_InverseTransformDirection_m1169769038_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_InverseTransformDirection_m1169769038_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_InverseTransformDirection(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___direction1, ___value2);
}
// UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Transform_TransformPoint_m437395512 (Transform_t1659122786 * __this, Vector3_t4282066566  ___position0, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_INTERNAL_CALL_TransformPoint_m1435510371(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_TransformPoint_m1435510371 (Il2CppObject * __this /* static, unused */, Transform_t1659122786 * ___self0, Vector3_t4282066566 * ___position1, Vector3_t4282066566 * ___value2, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_CALL_TransformPoint_m1435510371_ftn) (Transform_t1659122786 *, Vector3_t4282066566 *, Vector3_t4282066566 *);
	static Transform_INTERNAL_CALL_TransformPoint_m1435510371_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_TransformPoint_m1435510371_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Transform_InverseTransformPoint_m1626812000 (Transform_t1659122786 * __this, Vector3_t4282066566  ___position0, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_INTERNAL_CALL_InverseTransformPoint_m1782292189(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_InverseTransformPoint_m1782292189 (Il2CppObject * __this /* static, unused */, Transform_t1659122786 * ___self0, Vector3_t4282066566 * ___position1, Vector3_t4282066566 * ___value2, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_CALL_InverseTransformPoint_m1782292189_ftn) (Transform_t1659122786 *, Vector3_t4282066566 *, Vector3_t4282066566 *);
	static Transform_INTERNAL_CALL_InverseTransformPoint_m1782292189_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_InverseTransformPoint_m1782292189_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Transform UnityEngine.Transform::get_root()
extern "C"  Transform_t1659122786 * Transform_get_root_m1064615716 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	typedef Transform_t1659122786 * (*Transform_get_root_m1064615716_ftn) (Transform_t1659122786 *);
	static Transform_get_root_m1064615716_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_root_m1064615716_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_root()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C"  int32_t Transform_get_childCount_m2107810675 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	typedef int32_t (*Transform_get_childCount_m2107810675_ftn) (Transform_t1659122786 *);
	static Transform_get_childCount_m2107810675_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_childCount_m2107810675_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_childCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Transform::DetachChildren()
extern "C"  void Transform_DetachChildren_m1928114025 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	typedef void (*Transform_DetachChildren_m1928114025_ftn) (Transform_t1659122786 *);
	static Transform_DetachChildren_m1928114025_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_DetachChildren_m1928114025_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::DetachChildren()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Transform::SetAsFirstSibling()
extern "C"  void Transform_SetAsFirstSibling_m1746075601 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	typedef void (*Transform_SetAsFirstSibling_m1746075601_ftn) (Transform_t1659122786 *);
	static Transform_SetAsFirstSibling_m1746075601_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_SetAsFirstSibling_m1746075601_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::SetAsFirstSibling()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Transform::SetAsLastSibling()
extern "C"  void Transform_SetAsLastSibling_m3507604527 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	typedef void (*Transform_SetAsLastSibling_m3507604527_ftn) (Transform_t1659122786 *);
	static Transform_SetAsLastSibling_m3507604527_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_SetAsLastSibling_m3507604527_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::SetAsLastSibling()");
	_il2cpp_icall_func(__this);
}
// UnityEngine.Transform UnityEngine.Transform::Find(System.String)
extern "C"  Transform_t1659122786 * Transform_Find_m3950449392 (Transform_t1659122786 * __this, String_t* ___name0, const MethodInfo* method)
{
	typedef Transform_t1659122786 * (*Transform_Find_m3950449392_ftn) (Transform_t1659122786 *, String_t*);
	static Transform_Find_m3950449392_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_Find_m3950449392_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::Find(System.String)");
	return _il2cpp_icall_func(__this, ___name0);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_lossyScale()
extern "C"  Vector3_t4282066566  Transform_get_lossyScale_m3749612506 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_INTERNAL_get_lossyScale_m2289856381(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_lossyScale(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_lossyScale_m2289856381 (Transform_t1659122786 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_lossyScale_m2289856381_ftn) (Transform_t1659122786 *, Vector3_t4282066566 *);
	static Transform_INTERNAL_get_lossyScale_m2289856381_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_lossyScale_m2289856381_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_lossyScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Transform::IsChildOf(UnityEngine.Transform)
extern "C"  bool Transform_IsChildOf_m3321648579 (Transform_t1659122786 * __this, Transform_t1659122786 * ___parent0, const MethodInfo* method)
{
	typedef bool (*Transform_IsChildOf_m3321648579_ftn) (Transform_t1659122786 *, Transform_t1659122786 *);
	static Transform_IsChildOf_m3321648579_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_IsChildOf_m3321648579_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::IsChildOf(UnityEngine.Transform)");
	return _il2cpp_icall_func(__this, ___parent0);
}
// UnityEngine.Transform UnityEngine.Transform::FindChild(System.String)
extern "C"  Transform_t1659122786 * Transform_FindChild_m2149912886 (Transform_t1659122786 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		Transform_t1659122786 * L_1 = Transform_Find_m3950449392(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
extern Il2CppClass* Enumerator_t3875554846_il2cpp_TypeInfo_var;
extern const uint32_t Transform_GetEnumerator_m688365631_MetadataUsageId;
extern "C"  Il2CppObject * Transform_GetEnumerator_m688365631 (Transform_t1659122786 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_GetEnumerator_m688365631_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t3875554846 * L_0 = (Enumerator_t3875554846 *)il2cpp_codegen_object_new(Enumerator_t3875554846_il2cpp_TypeInfo_var);
		Enumerator__ctor_m856668951(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C"  Transform_t1659122786 * Transform_GetChild_m4040462992 (Transform_t1659122786 * __this, int32_t ___index0, const MethodInfo* method)
{
	typedef Transform_t1659122786 * (*Transform_GetChild_m4040462992_ftn) (Transform_t1659122786 *, int32_t);
	static Transform_GetChild_m4040462992_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_GetChild_m4040462992_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::GetChild(System.Int32)");
	return _il2cpp_icall_func(__this, ___index0);
}
// System.Void UnityEngine.Transform/Enumerator::.ctor(UnityEngine.Transform)
extern "C"  void Enumerator__ctor_m856668951 (Enumerator_t3875554846 * __this, Transform_t1659122786 * ___outer0, const MethodInfo* method)
{
	{
		__this->set_currentIndex_1((-1));
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Transform_t1659122786 * L_0 = ___outer0;
		__this->set_outer_0(L_0);
		return;
	}
}
// System.Object UnityEngine.Transform/Enumerator::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m1838268607 (Enumerator_t3875554846 * __this, const MethodInfo* method)
{
	{
		Transform_t1659122786 * L_0 = __this->get_outer_0();
		int32_t L_1 = __this->get_currentIndex_1();
		NullCheck(L_0);
		Transform_t1659122786 * L_2 = Transform_GetChild_m4040462992(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Transform/Enumerator::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1095530856 (Enumerator_t3875554846 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Transform_t1659122786 * L_0 = __this->get_outer_0();
		NullCheck(L_0);
		int32_t L_1 = Transform_get_childCount_m2107810675(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = __this->get_currentIndex_1();
		int32_t L_3 = ((int32_t)((int32_t)L_2+(int32_t)1));
		V_1 = L_3;
		__this->set_currentIndex_1(L_3);
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		return (bool)((((int32_t)L_4) < ((int32_t)L_5))? 1 : 0);
	}
}
// System.Void UnityEngine.Transform/Enumerator::Reset()
extern "C"  void Enumerator_Reset_m258411155 (Enumerator_t3875554846 * __this, const MethodInfo* method)
{
	{
		__this->set_currentIndex_1((-1));
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.UICharInfo
extern "C" void UICharInfo_t65807484_marshal_pinvoke(const UICharInfo_t65807484& unmarshaled, UICharInfo_t65807484_marshaled_pinvoke& marshaled)
{
	Vector2_t4282066565_marshal_pinvoke(unmarshaled.get_cursorPos_0(), marshaled.___cursorPos_0);
	marshaled.___charWidth_1 = unmarshaled.get_charWidth_1();
}
extern "C" void UICharInfo_t65807484_marshal_pinvoke_back(const UICharInfo_t65807484_marshaled_pinvoke& marshaled, UICharInfo_t65807484& unmarshaled)
{
	Vector2_t4282066565  unmarshaled_cursorPos_temp_0;
	memset(&unmarshaled_cursorPos_temp_0, 0, sizeof(unmarshaled_cursorPos_temp_0));
	Vector2_t4282066565_marshal_pinvoke_back(marshaled.___cursorPos_0, unmarshaled_cursorPos_temp_0);
	unmarshaled.set_cursorPos_0(unmarshaled_cursorPos_temp_0);
	float unmarshaled_charWidth_temp_1 = 0.0f;
	unmarshaled_charWidth_temp_1 = marshaled.___charWidth_1;
	unmarshaled.set_charWidth_1(unmarshaled_charWidth_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.UICharInfo
extern "C" void UICharInfo_t65807484_marshal_pinvoke_cleanup(UICharInfo_t65807484_marshaled_pinvoke& marshaled)
{
	Vector2_t4282066565_marshal_pinvoke_cleanup(marshaled.___cursorPos_0);
}
// Conversion methods for marshalling of: UnityEngine.UICharInfo
extern "C" void UICharInfo_t65807484_marshal_com(const UICharInfo_t65807484& unmarshaled, UICharInfo_t65807484_marshaled_com& marshaled)
{
	Vector2_t4282066565_marshal_com(unmarshaled.get_cursorPos_0(), marshaled.___cursorPos_0);
	marshaled.___charWidth_1 = unmarshaled.get_charWidth_1();
}
extern "C" void UICharInfo_t65807484_marshal_com_back(const UICharInfo_t65807484_marshaled_com& marshaled, UICharInfo_t65807484& unmarshaled)
{
	Vector2_t4282066565  unmarshaled_cursorPos_temp_0;
	memset(&unmarshaled_cursorPos_temp_0, 0, sizeof(unmarshaled_cursorPos_temp_0));
	Vector2_t4282066565_marshal_com_back(marshaled.___cursorPos_0, unmarshaled_cursorPos_temp_0);
	unmarshaled.set_cursorPos_0(unmarshaled_cursorPos_temp_0);
	float unmarshaled_charWidth_temp_1 = 0.0f;
	unmarshaled_charWidth_temp_1 = marshaled.___charWidth_1;
	unmarshaled.set_charWidth_1(unmarshaled_charWidth_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.UICharInfo
extern "C" void UICharInfo_t65807484_marshal_com_cleanup(UICharInfo_t65807484_marshaled_com& marshaled)
{
	Vector2_t4282066565_marshal_com_cleanup(marshaled.___cursorPos_0);
}
// Conversion methods for marshalling of: UnityEngine.UILineInfo
extern "C" void UILineInfo_t4113875482_marshal_pinvoke(const UILineInfo_t4113875482& unmarshaled, UILineInfo_t4113875482_marshaled_pinvoke& marshaled)
{
	marshaled.___startCharIdx_0 = unmarshaled.get_startCharIdx_0();
	marshaled.___height_1 = unmarshaled.get_height_1();
	marshaled.___topY_2 = unmarshaled.get_topY_2();
}
extern "C" void UILineInfo_t4113875482_marshal_pinvoke_back(const UILineInfo_t4113875482_marshaled_pinvoke& marshaled, UILineInfo_t4113875482& unmarshaled)
{
	int32_t unmarshaled_startCharIdx_temp_0 = 0;
	unmarshaled_startCharIdx_temp_0 = marshaled.___startCharIdx_0;
	unmarshaled.set_startCharIdx_0(unmarshaled_startCharIdx_temp_0);
	int32_t unmarshaled_height_temp_1 = 0;
	unmarshaled_height_temp_1 = marshaled.___height_1;
	unmarshaled.set_height_1(unmarshaled_height_temp_1);
	float unmarshaled_topY_temp_2 = 0.0f;
	unmarshaled_topY_temp_2 = marshaled.___topY_2;
	unmarshaled.set_topY_2(unmarshaled_topY_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.UILineInfo
extern "C" void UILineInfo_t4113875482_marshal_pinvoke_cleanup(UILineInfo_t4113875482_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.UILineInfo
extern "C" void UILineInfo_t4113875482_marshal_com(const UILineInfo_t4113875482& unmarshaled, UILineInfo_t4113875482_marshaled_com& marshaled)
{
	marshaled.___startCharIdx_0 = unmarshaled.get_startCharIdx_0();
	marshaled.___height_1 = unmarshaled.get_height_1();
	marshaled.___topY_2 = unmarshaled.get_topY_2();
}
extern "C" void UILineInfo_t4113875482_marshal_com_back(const UILineInfo_t4113875482_marshaled_com& marshaled, UILineInfo_t4113875482& unmarshaled)
{
	int32_t unmarshaled_startCharIdx_temp_0 = 0;
	unmarshaled_startCharIdx_temp_0 = marshaled.___startCharIdx_0;
	unmarshaled.set_startCharIdx_0(unmarshaled_startCharIdx_temp_0);
	int32_t unmarshaled_height_temp_1 = 0;
	unmarshaled_height_temp_1 = marshaled.___height_1;
	unmarshaled.set_height_1(unmarshaled_height_temp_1);
	float unmarshaled_topY_temp_2 = 0.0f;
	unmarshaled_topY_temp_2 = marshaled.___topY_2;
	unmarshaled.set_topY_2(unmarshaled_topY_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.UILineInfo
extern "C" void UILineInfo_t4113875482_marshal_com_cleanup(UILineInfo_t4113875482_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.UIVertex::.cctor()
extern Il2CppClass* UIVertex_t4244065212_il2cpp_TypeInfo_var;
extern const uint32_t UIVertex__cctor_m2134470424_MetadataUsageId;
extern "C"  void UIVertex__cctor_m2134470424 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIVertex__cctor_m2134470424_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UIVertex_t4244065212  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color32_t598853688  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color32__ctor_m576906339(&L_0, ((int32_t)255), ((int32_t)255), ((int32_t)255), ((int32_t)255), /*hidden argument*/NULL);
		((UIVertex_t4244065212_StaticFields*)UIVertex_t4244065212_il2cpp_TypeInfo_var->static_fields)->set_s_DefaultColor_6(L_0);
		Vector4_t4282066567  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector4__ctor_m2441427762(&L_1, (1.0f), (0.0f), (0.0f), (-1.0f), /*hidden argument*/NULL);
		((UIVertex_t4244065212_StaticFields*)UIVertex_t4244065212_il2cpp_TypeInfo_var->static_fields)->set_s_DefaultTangent_7(L_1);
		Initobj (UIVertex_t4244065212_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t4282066566  L_2 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->set_position_0(L_2);
		Vector3_t4282066566  L_3 = Vector3_get_back_m1326515313(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->set_normal_1(L_3);
		Vector4_t4282066567  L_4 = ((UIVertex_t4244065212_StaticFields*)UIVertex_t4244065212_il2cpp_TypeInfo_var->static_fields)->get_s_DefaultTangent_7();
		(&V_0)->set_tangent_5(L_4);
		Color32_t598853688  L_5 = ((UIVertex_t4244065212_StaticFields*)UIVertex_t4244065212_il2cpp_TypeInfo_var->static_fields)->get_s_DefaultColor_6();
		(&V_0)->set_color_2(L_5);
		Vector2_t4282066565  L_6 = Vector2_get_zero_m199872368(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->set_uv0_3(L_6);
		Vector2_t4282066565  L_7 = Vector2_get_zero_m199872368(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->set_uv1_4(L_7);
		UIVertex_t4244065212  L_8 = V_0;
		((UIVertex_t4244065212_StaticFields*)UIVertex_t4244065212_il2cpp_TypeInfo_var->static_fields)->set_simpleVert_8(L_8);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.UIVertex
extern "C" void UIVertex_t4244065212_marshal_pinvoke(const UIVertex_t4244065212& unmarshaled, UIVertex_t4244065212_marshaled_pinvoke& marshaled)
{
	Vector3_t4282066566_marshal_pinvoke(unmarshaled.get_position_0(), marshaled.___position_0);
	Vector3_t4282066566_marshal_pinvoke(unmarshaled.get_normal_1(), marshaled.___normal_1);
	Color32_t598853688_marshal_pinvoke(unmarshaled.get_color_2(), marshaled.___color_2);
	Vector2_t4282066565_marshal_pinvoke(unmarshaled.get_uv0_3(), marshaled.___uv0_3);
	Vector2_t4282066565_marshal_pinvoke(unmarshaled.get_uv1_4(), marshaled.___uv1_4);
	Vector4_t4282066567_marshal_pinvoke(unmarshaled.get_tangent_5(), marshaled.___tangent_5);
}
extern "C" void UIVertex_t4244065212_marshal_pinvoke_back(const UIVertex_t4244065212_marshaled_pinvoke& marshaled, UIVertex_t4244065212& unmarshaled)
{
	Vector3_t4282066566  unmarshaled_position_temp_0;
	memset(&unmarshaled_position_temp_0, 0, sizeof(unmarshaled_position_temp_0));
	Vector3_t4282066566_marshal_pinvoke_back(marshaled.___position_0, unmarshaled_position_temp_0);
	unmarshaled.set_position_0(unmarshaled_position_temp_0);
	Vector3_t4282066566  unmarshaled_normal_temp_1;
	memset(&unmarshaled_normal_temp_1, 0, sizeof(unmarshaled_normal_temp_1));
	Vector3_t4282066566_marshal_pinvoke_back(marshaled.___normal_1, unmarshaled_normal_temp_1);
	unmarshaled.set_normal_1(unmarshaled_normal_temp_1);
	Color32_t598853688  unmarshaled_color_temp_2;
	memset(&unmarshaled_color_temp_2, 0, sizeof(unmarshaled_color_temp_2));
	Color32_t598853688_marshal_pinvoke_back(marshaled.___color_2, unmarshaled_color_temp_2);
	unmarshaled.set_color_2(unmarshaled_color_temp_2);
	Vector2_t4282066565  unmarshaled_uv0_temp_3;
	memset(&unmarshaled_uv0_temp_3, 0, sizeof(unmarshaled_uv0_temp_3));
	Vector2_t4282066565_marshal_pinvoke_back(marshaled.___uv0_3, unmarshaled_uv0_temp_3);
	unmarshaled.set_uv0_3(unmarshaled_uv0_temp_3);
	Vector2_t4282066565  unmarshaled_uv1_temp_4;
	memset(&unmarshaled_uv1_temp_4, 0, sizeof(unmarshaled_uv1_temp_4));
	Vector2_t4282066565_marshal_pinvoke_back(marshaled.___uv1_4, unmarshaled_uv1_temp_4);
	unmarshaled.set_uv1_4(unmarshaled_uv1_temp_4);
	Vector4_t4282066567  unmarshaled_tangent_temp_5;
	memset(&unmarshaled_tangent_temp_5, 0, sizeof(unmarshaled_tangent_temp_5));
	Vector4_t4282066567_marshal_pinvoke_back(marshaled.___tangent_5, unmarshaled_tangent_temp_5);
	unmarshaled.set_tangent_5(unmarshaled_tangent_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.UIVertex
extern "C" void UIVertex_t4244065212_marshal_pinvoke_cleanup(UIVertex_t4244065212_marshaled_pinvoke& marshaled)
{
	Vector3_t4282066566_marshal_pinvoke_cleanup(marshaled.___position_0);
	Vector3_t4282066566_marshal_pinvoke_cleanup(marshaled.___normal_1);
	Color32_t598853688_marshal_pinvoke_cleanup(marshaled.___color_2);
	Vector2_t4282066565_marshal_pinvoke_cleanup(marshaled.___uv0_3);
	Vector2_t4282066565_marshal_pinvoke_cleanup(marshaled.___uv1_4);
	Vector4_t4282066567_marshal_pinvoke_cleanup(marshaled.___tangent_5);
}
// Conversion methods for marshalling of: UnityEngine.UIVertex
extern "C" void UIVertex_t4244065212_marshal_com(const UIVertex_t4244065212& unmarshaled, UIVertex_t4244065212_marshaled_com& marshaled)
{
	Vector3_t4282066566_marshal_com(unmarshaled.get_position_0(), marshaled.___position_0);
	Vector3_t4282066566_marshal_com(unmarshaled.get_normal_1(), marshaled.___normal_1);
	Color32_t598853688_marshal_com(unmarshaled.get_color_2(), marshaled.___color_2);
	Vector2_t4282066565_marshal_com(unmarshaled.get_uv0_3(), marshaled.___uv0_3);
	Vector2_t4282066565_marshal_com(unmarshaled.get_uv1_4(), marshaled.___uv1_4);
	Vector4_t4282066567_marshal_com(unmarshaled.get_tangent_5(), marshaled.___tangent_5);
}
extern "C" void UIVertex_t4244065212_marshal_com_back(const UIVertex_t4244065212_marshaled_com& marshaled, UIVertex_t4244065212& unmarshaled)
{
	Vector3_t4282066566  unmarshaled_position_temp_0;
	memset(&unmarshaled_position_temp_0, 0, sizeof(unmarshaled_position_temp_0));
	Vector3_t4282066566_marshal_com_back(marshaled.___position_0, unmarshaled_position_temp_0);
	unmarshaled.set_position_0(unmarshaled_position_temp_0);
	Vector3_t4282066566  unmarshaled_normal_temp_1;
	memset(&unmarshaled_normal_temp_1, 0, sizeof(unmarshaled_normal_temp_1));
	Vector3_t4282066566_marshal_com_back(marshaled.___normal_1, unmarshaled_normal_temp_1);
	unmarshaled.set_normal_1(unmarshaled_normal_temp_1);
	Color32_t598853688  unmarshaled_color_temp_2;
	memset(&unmarshaled_color_temp_2, 0, sizeof(unmarshaled_color_temp_2));
	Color32_t598853688_marshal_com_back(marshaled.___color_2, unmarshaled_color_temp_2);
	unmarshaled.set_color_2(unmarshaled_color_temp_2);
	Vector2_t4282066565  unmarshaled_uv0_temp_3;
	memset(&unmarshaled_uv0_temp_3, 0, sizeof(unmarshaled_uv0_temp_3));
	Vector2_t4282066565_marshal_com_back(marshaled.___uv0_3, unmarshaled_uv0_temp_3);
	unmarshaled.set_uv0_3(unmarshaled_uv0_temp_3);
	Vector2_t4282066565  unmarshaled_uv1_temp_4;
	memset(&unmarshaled_uv1_temp_4, 0, sizeof(unmarshaled_uv1_temp_4));
	Vector2_t4282066565_marshal_com_back(marshaled.___uv1_4, unmarshaled_uv1_temp_4);
	unmarshaled.set_uv1_4(unmarshaled_uv1_temp_4);
	Vector4_t4282066567  unmarshaled_tangent_temp_5;
	memset(&unmarshaled_tangent_temp_5, 0, sizeof(unmarshaled_tangent_temp_5));
	Vector4_t4282066567_marshal_com_back(marshaled.___tangent_5, unmarshaled_tangent_temp_5);
	unmarshaled.set_tangent_5(unmarshaled_tangent_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.UIVertex
extern "C" void UIVertex_t4244065212_marshal_com_cleanup(UIVertex_t4244065212_marshaled_com& marshaled)
{
	Vector3_t4282066566_marshal_com_cleanup(marshaled.___position_0);
	Vector3_t4282066566_marshal_com_cleanup(marshaled.___normal_1);
	Color32_t598853688_marshal_com_cleanup(marshaled.___color_2);
	Vector2_t4282066565_marshal_com_cleanup(marshaled.___uv0_3);
	Vector2_t4282066565_marshal_com_cleanup(marshaled.___uv1_4);
	Vector4_t4282066567_marshal_com_cleanup(marshaled.___tangent_5);
}
// System.Void UnityEngine.UnhandledExceptionHandler::RegisterUECatcher()
extern Il2CppClass* UnhandledExceptionEventHandler_t2544755120_il2cpp_TypeInfo_var;
extern const MethodInfo* UnhandledExceptionHandler_HandleUnhandledException_m4033241923_MethodInfo_var;
extern const uint32_t UnhandledExceptionHandler_RegisterUECatcher_m143130254_MetadataUsageId;
extern "C"  void UnhandledExceptionHandler_RegisterUECatcher_m143130254 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnhandledExceptionHandler_RegisterUECatcher_m143130254_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AppDomain_t3575612635 * L_0 = AppDomain_get_CurrentDomain_m3448347417(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)UnhandledExceptionHandler_HandleUnhandledException_m4033241923_MethodInfo_var);
		UnhandledExceptionEventHandler_t2544755120 * L_2 = (UnhandledExceptionEventHandler_t2544755120 *)il2cpp_codegen_object_new(UnhandledExceptionEventHandler_t2544755120_il2cpp_TypeInfo_var);
		UnhandledExceptionEventHandler__ctor_m2560140041(L_2, NULL, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		AppDomain_add_UnhandledException_m1729490677(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::HandleUnhandledException(System.Object,System.UnhandledExceptionEventArgs)
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2454016344;
extern const uint32_t UnhandledExceptionHandler_HandleUnhandledException_m4033241923_MetadataUsageId;
extern "C"  void UnhandledExceptionHandler_HandleUnhandledException_m4033241923 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___sender0, UnhandledExceptionEventArgs_t3134093121 * ___args1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnhandledExceptionHandler_HandleUnhandledException_m4033241923_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * V_0 = NULL;
	{
		UnhandledExceptionEventArgs_t3134093121 * L_0 = ___args1;
		NullCheck(L_0);
		Il2CppObject * L_1 = UnhandledExceptionEventArgs_get_ExceptionObject_m3487900308(L_0, /*hidden argument*/NULL);
		V_0 = ((Exception_t3991598821 *)IsInstClass(L_1, Exception_t3991598821_il2cpp_TypeInfo_var));
		Exception_t3991598821 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		Exception_t3991598821 * L_3 = V_0;
		UnhandledExceptionHandler_PrintException_m3142367935(NULL /*static, unused*/, _stringLiteral2454016344, L_3, /*hidden argument*/NULL);
	}

IL_001d:
	{
		UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m1608070048(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::PrintException(System.String,System.Exception)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3111784619;
extern const uint32_t UnhandledExceptionHandler_PrintException_m3142367935_MetadataUsageId;
extern "C"  void UnhandledExceptionHandler_PrintException_m3142367935 (Il2CppObject * __this /* static, unused */, String_t* ___title0, Exception_t3991598821 * ___e1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnhandledExceptionHandler_PrintException_m3142367935_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t3991598821 * L_0 = ___e1;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogException_m248970745(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Exception_t3991598821 * L_1 = ___e1;
		NullCheck(L_1);
		Exception_t3991598821 * L_2 = Exception_get_InnerException_m1427945535(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		Exception_t3991598821 * L_3 = ___e1;
		NullCheck(L_3);
		Exception_t3991598821 * L_4 = Exception_get_InnerException_m1427945535(L_3, /*hidden argument*/NULL);
		UnhandledExceptionHandler_PrintException_m3142367935(NULL /*static, unused*/, _stringLiteral3111784619, L_4, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::NativeUnhandledExceptionHandler()
extern "C"  void UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m1608070048 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m1608070048_ftn) ();
	static UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m1608070048_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m1608070048_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.UnhandledExceptionHandler::NativeUnhandledExceptionHandler()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.UnityAPICompatibilityVersionAttribute::.ctor(System.String)
extern "C"  void UnityAPICompatibilityVersionAttribute__ctor_m60867672 (UnityAPICompatibilityVersionAttribute_t2574965573 * __this, String_t* ___version0, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___version0;
		__this->set__version_0(L_0);
		return;
	}
}
// System.Void UnityEngine.UnityException::.ctor()
extern Il2CppCodeGenString* _stringLiteral3200128946;
extern const uint32_t UnityException__ctor_m1176480467_MetadataUsageId;
extern "C"  void UnityException__ctor_m1176480467 (UnityException_t3473321374 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityException__ctor_m1176480467_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception__ctor_m3870771296(__this, _stringLiteral3200128946, /*hidden argument*/NULL);
		Exception_set_HResult_m3566571225(__this, ((int32_t)-2147467261), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityException::.ctor(System.String)
extern "C"  void UnityException__ctor_m743662351 (UnityException_t3473321374 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception__ctor_m3870771296(__this, L_0, /*hidden argument*/NULL);
		Exception_set_HResult_m3566571225(__this, ((int32_t)-2147467261), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityException::.ctor(System.String,System.Exception)
extern "C"  void UnityException__ctor_m2780758535 (UnityException_t3473321374 * __this, String_t* ___message0, Exception_t3991598821 * ___innerException1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t3991598821 * L_1 = ___innerException1;
		Exception__ctor_m1328171222(__this, L_0, L_1, /*hidden argument*/NULL);
		Exception_set_HResult_m3566571225(__this, ((int32_t)-2147467261), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void UnityException__ctor_m2001840532 (UnityException_t3473321374 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t2185721892 * L_0 = ___info0;
		StreamingContext_t2761351129  L_1 = ___context1;
		Exception__ctor_m3602014243(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.UnityString::Format(System.String,System.Object[])
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t UnityString_Format_m427603113_MetadataUsageId;
extern "C"  String_t* UnityString_Format_m427603113 (Il2CppObject * __this /* static, unused */, String_t* ___fmt0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityString_Format_m427603113_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___fmt0;
		ObjectU5BU5D_t1108656482* L_1 = ___args1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m4050103162(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m1517109030 (Vector2_t4282066565 * __this, float ___x0, float ___y1, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_1(L_0);
		float L_1 = ___y1;
		__this->set_y_2(L_1);
		return;
	}
}
extern "C"  void Vector2__ctor_m1517109030_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, const MethodInfo* method)
{
	Vector2_t4282066565 * _thisAdjusted = reinterpret_cast<Vector2_t4282066565 *>(__this + 1);
	Vector2__ctor_m1517109030(_thisAdjusted, ___x0, ___y1, method);
}
// System.Single UnityEngine.Vector2::get_Item(System.Int32)
extern Il2CppClass* IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3517435145;
extern const uint32_t Vector2_get_Item_m2185542843_MetadataUsageId;
extern "C"  float Vector2_get_Item_m2185542843 (Vector2_t4282066565 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector2_get_Item_m2185542843_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_001b;
		}
	}
	{
		goto IL_0022;
	}

IL_0014:
	{
		float L_3 = __this->get_x_1();
		return L_3;
	}

IL_001b:
	{
		float L_4 = __this->get_y_2();
		return L_4;
	}

IL_0022:
	{
		IndexOutOfRangeException_t3456360697 * L_5 = (IndexOutOfRangeException_t3456360697 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1621772274(L_5, _stringLiteral3517435145, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}
}
extern "C"  float Vector2_get_Item_m2185542843_AdjustorThunk (Il2CppObject * __this, int32_t ___index0, const MethodInfo* method)
{
	Vector2_t4282066565 * _thisAdjusted = reinterpret_cast<Vector2_t4282066565 *>(__this + 1);
	return Vector2_get_Item_m2185542843(_thisAdjusted, ___index0, method);
}
// System.Void UnityEngine.Vector2::set_Item(System.Int32,System.Single)
extern Il2CppClass* IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3517435145;
extern const uint32_t Vector2_set_Item_m2767519328_MetadataUsageId;
extern "C"  void Vector2_set_Item_m2767519328 (Vector2_t4282066565 * __this, int32_t ___index0, float ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector2_set_Item_m2767519328_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0020;
		}
	}
	{
		goto IL_002c;
	}

IL_0014:
	{
		float L_3 = ___value1;
		__this->set_x_1(L_3);
		goto IL_0037;
	}

IL_0020:
	{
		float L_4 = ___value1;
		__this->set_y_2(L_4);
		goto IL_0037;
	}

IL_002c:
	{
		IndexOutOfRangeException_t3456360697 * L_5 = (IndexOutOfRangeException_t3456360697 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1621772274(L_5, _stringLiteral3517435145, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0037:
	{
		return;
	}
}
extern "C"  void Vector2_set_Item_m2767519328_AdjustorThunk (Il2CppObject * __this, int32_t ___index0, float ___value1, const MethodInfo* method)
{
	Vector2_t4282066565 * _thisAdjusted = reinterpret_cast<Vector2_t4282066565 *>(__this + 1);
	Vector2_set_Item_m2767519328(_thisAdjusted, ___index0, ___value1, method);
}
// System.Void UnityEngine.Vector2::Set(System.Single,System.Single)
extern "C"  void Vector2_Set_m3617333350 (Vector2_t4282066565 * __this, float ___new_x0, float ___new_y1, const MethodInfo* method)
{
	{
		float L_0 = ___new_x0;
		__this->set_x_1(L_0);
		float L_1 = ___new_y1;
		__this->set_y_2(L_1);
		return;
	}
}
extern "C"  void Vector2_Set_m3617333350_AdjustorThunk (Il2CppObject * __this, float ___new_x0, float ___new_y1, const MethodInfo* method)
{
	Vector2_t4282066565 * _thisAdjusted = reinterpret_cast<Vector2_t4282066565 *>(__this + 1);
	Vector2_Set_m3617333350(_thisAdjusted, ___new_x0, ___new_y1, method);
}
// UnityEngine.Vector2 UnityEngine.Vector2::Lerp(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Vector2_Lerp_m235172797_MetadataUsageId;
extern "C"  Vector2_t4282066565  Vector2_Lerp_m235172797 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___a0, Vector2_t4282066565  ___b1, float ___t2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector2_Lerp_m235172797_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___t2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m2272733930(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		___t2 = L_1;
		float L_2 = (&___a0)->get_x_1();
		float L_3 = (&___b1)->get_x_1();
		float L_4 = (&___a0)->get_x_1();
		float L_5 = ___t2;
		float L_6 = (&___a0)->get_y_2();
		float L_7 = (&___b1)->get_y_2();
		float L_8 = (&___a0)->get_y_2();
		float L_9 = ___t2;
		Vector2_t4282066565  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector2__ctor_m1517109030(&L_10, ((float)((float)L_2+(float)((float)((float)((float)((float)L_3-(float)L_4))*(float)L_5)))), ((float)((float)L_6+(float)((float)((float)((float)((float)L_7-(float)L_8))*(float)L_9)))), /*hidden argument*/NULL);
		return L_10;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::Scale(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t4282066565  Vector2_Scale_m1743563745 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___a0, Vector2_t4282066565  ___b1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		Vector2_t4282066565  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m1517109030(&L_4, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void UnityEngine.Vector2::Normalize()
extern "C"  void Vector2_Normalize_m195575125 (Vector2_t4282066565 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = Vector2_get_magnitude_m1987058139(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = V_0;
		if ((!(((float)L_1) > ((float)(1.0E-05f)))))
		{
			goto IL_0029;
		}
	}
	{
		float L_2 = V_0;
		Vector2_t4282066565  L_3 = Vector2_op_Division_m747627697(NULL /*static, unused*/, (*(Vector2_t4282066565 *)__this), L_2, /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)__this) = L_3;
		goto IL_0034;
	}

IL_0029:
	{
		Vector2_t4282066565  L_4 = Vector2_get_zero_m199872368(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)__this) = L_4;
	}

IL_0034:
	{
		return;
	}
}
extern "C"  void Vector2_Normalize_m195575125_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector2_t4282066565 * _thisAdjusted = reinterpret_cast<Vector2_t4282066565 *>(__this + 1);
	Vector2_Normalize_m195575125(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_normalized()
extern "C"  Vector2_t4282066565  Vector2_get_normalized_m123128511 (Vector2_t4282066565 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = __this->get_x_1();
		float L_1 = __this->get_y_2();
		Vector2__ctor_m1517109030((&V_0), L_0, L_1, /*hidden argument*/NULL);
		Vector2_Normalize_m195575125((&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_2 = V_0;
		return L_2;
	}
}
extern "C"  Vector2_t4282066565  Vector2_get_normalized_m123128511_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector2_t4282066565 * _thisAdjusted = reinterpret_cast<Vector2_t4282066565 *>(__this + 1);
	return Vector2_get_normalized_m123128511(_thisAdjusted, method);
}
// System.String UnityEngine.Vector2::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3928503634;
extern const uint32_t Vector2_ToString_m3859776067_MetadataUsageId;
extern "C"  String_t* Vector2_ToString_m3859776067 (Vector2_t4282066565 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector2_ToString_m3859776067_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)2));
		float L_1 = __this->get_x_1();
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_0;
		float L_5 = __this->get_y_2();
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		String_t* L_8 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral3928503634, L_4, /*hidden argument*/NULL);
		return L_8;
	}
}
extern "C"  String_t* Vector2_ToString_m3859776067_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector2_t4282066565 * _thisAdjusted = reinterpret_cast<Vector2_t4282066565 *>(__this + 1);
	return Vector2_ToString_m3859776067(_thisAdjusted, method);
}
// System.Int32 UnityEngine.Vector2::GetHashCode()
extern "C"  int32_t Vector2_GetHashCode_m128434585 (Vector2_t4282066565 * __this, const MethodInfo* method)
{
	{
		float* L_0 = __this->get_address_of_x_1();
		int32_t L_1 = Single_GetHashCode_m65342520(L_0, /*hidden argument*/NULL);
		float* L_2 = __this->get_address_of_y_2();
		int32_t L_3 = Single_GetHashCode_m65342520(L_2, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))));
	}
}
extern "C"  int32_t Vector2_GetHashCode_m128434585_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector2_t4282066565 * _thisAdjusted = reinterpret_cast<Vector2_t4282066565 *>(__this + 1);
	return Vector2_GetHashCode_m128434585(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Vector2::Equals(System.Object)
extern Il2CppClass* Vector2_t4282066565_il2cpp_TypeInfo_var;
extern const uint32_t Vector2_Equals_m3404198849_MetadataUsageId;
extern "C"  bool Vector2_Equals_m3404198849 (Vector2_t4282066565 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector2_Equals_m3404198849_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t G_B5_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Vector2_t4282066565_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(Vector2_t4282066565 *)((Vector2_t4282066565 *)UnBox (L_1, Vector2_t4282066565_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_x_1();
		float L_3 = (&V_0)->get_x_1();
		bool L_4 = Single_Equals_m2110115959(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003f;
		}
	}
	{
		float* L_5 = __this->get_address_of_y_2();
		float L_6 = (&V_0)->get_y_2();
		bool L_7 = Single_Equals_m2110115959(L_5, L_6, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_7));
		goto IL_0040;
	}

IL_003f:
	{
		G_B5_0 = 0;
	}

IL_0040:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool Vector2_Equals_m3404198849_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Vector2_t4282066565 * _thisAdjusted = reinterpret_cast<Vector2_t4282066565 *>(__this + 1);
	return Vector2_Equals_m3404198849(_thisAdjusted, ___other0, method);
}
// System.Single UnityEngine.Vector2::Dot(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  float Vector2_Dot_m2437602225 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___lhs0, Vector2_t4282066565  ___rhs1, const MethodInfo* method)
{
	{
		float L_0 = (&___lhs0)->get_x_1();
		float L_1 = (&___rhs1)->get_x_1();
		float L_2 = (&___lhs0)->get_y_2();
		float L_3 = (&___rhs1)->get_y_2();
		return ((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))));
	}
}
// System.Single UnityEngine.Vector2::get_magnitude()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Vector2_get_magnitude_m1987058139_MetadataUsageId;
extern "C"  float Vector2_get_magnitude_m1987058139 (Vector2_t4282066565 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector2_get_magnitude_m1987058139_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_x_1();
		float L_1 = __this->get_x_1();
		float L_2 = __this->get_y_2();
		float L_3 = __this->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_4 = sqrtf(((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3)))));
		return L_4;
	}
}
extern "C"  float Vector2_get_magnitude_m1987058139_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector2_t4282066565 * _thisAdjusted = reinterpret_cast<Vector2_t4282066565 *>(__this + 1);
	return Vector2_get_magnitude_m1987058139(_thisAdjusted, method);
}
// System.Single UnityEngine.Vector2::get_sqrMagnitude()
extern "C"  float Vector2_get_sqrMagnitude_m996072851 (Vector2_t4282066565 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_x_1();
		float L_1 = __this->get_x_1();
		float L_2 = __this->get_y_2();
		float L_3 = __this->get_y_2();
		return ((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))));
	}
}
extern "C"  float Vector2_get_sqrMagnitude_m996072851_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector2_t4282066565 * _thisAdjusted = reinterpret_cast<Vector2_t4282066565 *>(__this + 1);
	return Vector2_get_sqrMagnitude_m996072851(_thisAdjusted, method);
}
// System.Single UnityEngine.Vector2::Angle(UnityEngine.Vector2,UnityEngine.Vector2)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Vector2_Angle_m3950144551_MetadataUsageId;
extern "C"  float Vector2_Angle_m3950144551 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___from0, Vector2_t4282066565  ___to1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector2_Angle_m3950144551_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2_t4282066565  L_0 = Vector2_get_normalized_m123128511((&___from0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_1 = Vector2_get_normalized_m123128511((&___to1), /*hidden argument*/NULL);
		float L_2 = Vector2_Dot_m2437602225(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_2, (-1.0f), (1.0f), /*hidden argument*/NULL);
		float L_4 = acosf(L_3);
		return ((float)((float)L_4*(float)(57.29578f)));
	}
}
// System.Single UnityEngine.Vector2::Distance(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  float Vector2_Distance_m340609291 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___a0, Vector2_t4282066565  ___b1, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t4282066565  L_0 = ___a0;
		Vector2_t4282066565  L_1 = ___b1;
		Vector2_t4282066565  L_2 = Vector2_op_Subtraction_m2097149401(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Vector2_get_magnitude_m1987058139((&V_0), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Single UnityEngine.Vector2::SqrMagnitude(UnityEngine.Vector2)
extern "C"  float Vector2_SqrMagnitude_m4007443280 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___a0, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___a0)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___a0)->get_y_2();
		return ((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))));
	}
}
// System.Single UnityEngine.Vector2::SqrMagnitude()
extern "C"  float Vector2_SqrMagnitude_m1478729770 (Vector2_t4282066565 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_x_1();
		float L_1 = __this->get_x_1();
		float L_2 = __this->get_y_2();
		float L_3 = __this->get_y_2();
		return ((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))));
	}
}
extern "C"  float Vector2_SqrMagnitude_m1478729770_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector2_t4282066565 * _thisAdjusted = reinterpret_cast<Vector2_t4282066565 *>(__this + 1);
	return Vector2_SqrMagnitude_m1478729770(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C"  Vector2_t4282066565  Vector2_get_zero_m199872368 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector2__ctor_m1517109030(&L_0, (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_one()
extern "C"  Vector2_t4282066565  Vector2_get_one_m2767488832 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector2__ctor_m1517109030(&L_0, (1.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_up()
extern "C"  Vector2_t4282066565  Vector2_get_up_m1197831267 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector2__ctor_m1517109030(&L_0, (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_right()
extern "C"  Vector2_t4282066565  Vector2_get_right_m3495203638 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector2__ctor_m1517109030(&L_0, (1.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t4282066565  Vector2_op_Addition_m1173049553 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___a0, Vector2_t4282066565  ___b1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		Vector2_t4282066565  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m1517109030(&L_4, ((float)((float)L_0+(float)L_1)), ((float)((float)L_2+(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t4282066565  Vector2_op_Subtraction_m2097149401 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___a0, Vector2_t4282066565  ___b1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		Vector2_t4282066565  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m1517109030(&L_4, ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_UnaryNegation(UnityEngine.Vector2)
extern "C"  Vector2_t4282066565  Vector2_op_UnaryNegation_m1730705317 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___a0, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___a0)->get_y_2();
		Vector2_t4282066565  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m1517109030(&L_2, ((-L_0)), ((-L_1)), /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t4282066565  Vector2_op_Multiply_m1738245082 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___a0, float ___d1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = ___d1;
		float L_2 = (&___a0)->get_y_2();
		float L_3 = ___d1;
		Vector2_t4282066565  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m1517109030(&L_4, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Division(UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t4282066565  Vector2_op_Division_m747627697 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___a0, float ___d1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = ___d1;
		float L_2 = (&___a0)->get_y_2();
		float L_3 = ___d1;
		Vector2_t4282066565  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m1517109030(&L_4, ((float)((float)L_0/(float)L_1)), ((float)((float)L_2/(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean UnityEngine.Vector2::op_Equality(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  bool Vector2_op_Equality_m1927481448 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___lhs0, Vector2_t4282066565  ___rhs1, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0 = ___lhs0;
		Vector2_t4282066565  L_1 = ___rhs1;
		Vector2_t4282066565  L_2 = Vector2_op_Subtraction_m2097149401(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector2_SqrMagnitude_m4007443280(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return (bool)((((float)L_3) < ((float)(9.99999944E-11f)))? 1 : 0);
	}
}
// System.Boolean UnityEngine.Vector2::op_Inequality(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  bool Vector2_op_Inequality_m1638984867 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___lhs0, Vector2_t4282066565  ___rhs1, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0 = ___lhs0;
		Vector2_t4282066565  L_1 = ___rhs1;
		Vector2_t4282066565  L_2 = Vector2_op_Subtraction_m2097149401(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector2_SqrMagnitude_m4007443280(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return (bool)((((int32_t)((!(((float)L_3) >= ((float)(9.99999944E-11f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector2_t4282066565  Vector2_op_Implicit_m4083860659 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___v0, const MethodInfo* method)
{
	{
		float L_0 = (&___v0)->get_x_1();
		float L_1 = (&___v0)->get_y_2();
		Vector2_t4282066565  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m1517109030(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C"  Vector3_t4282066566  Vector2_op_Implicit_m482286037 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___v0, const MethodInfo* method)
{
	{
		float L_0 = (&___v0)->get_x_1();
		float L_1 = (&___v0)->get_y_2();
		Vector3_t4282066566  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector3__ctor_m2926210380(&L_2, L_0, L_1, (0.0f), /*hidden argument*/NULL);
		return L_2;
	}
}
// Conversion methods for marshalling of: UnityEngine.Vector2
extern "C" void Vector2_t4282066565_marshal_pinvoke(const Vector2_t4282066565& unmarshaled, Vector2_t4282066565_marshaled_pinvoke& marshaled)
{
	marshaled.___x_1 = unmarshaled.get_x_1();
	marshaled.___y_2 = unmarshaled.get_y_2();
}
extern "C" void Vector2_t4282066565_marshal_pinvoke_back(const Vector2_t4282066565_marshaled_pinvoke& marshaled, Vector2_t4282066565& unmarshaled)
{
	float unmarshaled_x_temp_0 = 0.0f;
	unmarshaled_x_temp_0 = marshaled.___x_1;
	unmarshaled.set_x_1(unmarshaled_x_temp_0);
	float unmarshaled_y_temp_1 = 0.0f;
	unmarshaled_y_temp_1 = marshaled.___y_2;
	unmarshaled.set_y_2(unmarshaled_y_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Vector2
extern "C" void Vector2_t4282066565_marshal_pinvoke_cleanup(Vector2_t4282066565_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Vector2
extern "C" void Vector2_t4282066565_marshal_com(const Vector2_t4282066565& unmarshaled, Vector2_t4282066565_marshaled_com& marshaled)
{
	marshaled.___x_1 = unmarshaled.get_x_1();
	marshaled.___y_2 = unmarshaled.get_y_2();
}
extern "C" void Vector2_t4282066565_marshal_com_back(const Vector2_t4282066565_marshaled_com& marshaled, Vector2_t4282066565& unmarshaled)
{
	float unmarshaled_x_temp_0 = 0.0f;
	unmarshaled_x_temp_0 = marshaled.___x_1;
	unmarshaled.set_x_1(unmarshaled_x_temp_0);
	float unmarshaled_y_temp_1 = 0.0f;
	unmarshaled_y_temp_1 = marshaled.___y_2;
	unmarshaled.set_y_2(unmarshaled_y_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Vector2
extern "C" void Vector2_t4282066565_marshal_com_cleanup(Vector2_t4282066565_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m2926210380 (Vector3_t4282066566 * __this, float ___x0, float ___y1, float ___z2, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_1(L_0);
		float L_1 = ___y1;
		__this->set_y_2(L_1);
		float L_2 = ___z2;
		__this->set_z_3(L_2);
		return;
	}
}
extern "C"  void Vector3__ctor_m2926210380_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, float ___z2, const MethodInfo* method)
{
	Vector3_t4282066566 * _thisAdjusted = reinterpret_cast<Vector3_t4282066566 *>(__this + 1);
	Vector3__ctor_m2926210380(_thisAdjusted, ___x0, ___y1, ___z2, method);
}
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single)
extern "C"  void Vector3__ctor_m1846874791 (Vector3_t4282066566 * __this, float ___x0, float ___y1, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_1(L_0);
		float L_1 = ___y1;
		__this->set_y_2(L_1);
		__this->set_z_3((0.0f));
		return;
	}
}
extern "C"  void Vector3__ctor_m1846874791_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, const MethodInfo* method)
{
	Vector3_t4282066566 * _thisAdjusted = reinterpret_cast<Vector3_t4282066566 *>(__this + 1);
	Vector3__ctor_m1846874791(_thisAdjusted, ___x0, ___y1, method);
}
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Vector3_Lerp_m650470329_MetadataUsageId;
extern "C"  Vector3_t4282066566  Vector3_Lerp_m650470329 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, Vector3_t4282066566  ___b1, float ___t2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Lerp_m650470329_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___t2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m2272733930(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		___t2 = L_1;
		float L_2 = (&___a0)->get_x_1();
		float L_3 = (&___b1)->get_x_1();
		float L_4 = (&___a0)->get_x_1();
		float L_5 = ___t2;
		float L_6 = (&___a0)->get_y_2();
		float L_7 = (&___b1)->get_y_2();
		float L_8 = (&___a0)->get_y_2();
		float L_9 = ___t2;
		float L_10 = (&___a0)->get_z_3();
		float L_11 = (&___b1)->get_z_3();
		float L_12 = (&___a0)->get_z_3();
		float L_13 = ___t2;
		Vector3_t4282066566  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m2926210380(&L_14, ((float)((float)L_2+(float)((float)((float)((float)((float)L_3-(float)L_4))*(float)L_5)))), ((float)((float)L_6+(float)((float)((float)((float)((float)L_7-(float)L_8))*(float)L_9)))), ((float)((float)L_10+(float)((float)((float)((float)((float)L_11-(float)L_12))*(float)L_13)))), /*hidden argument*/NULL);
		return L_14;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::MoveTowards(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t4282066566  Vector3_MoveTowards_m2405650085 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___current0, Vector3_t4282066566  ___target1, float ___maxDistanceDelta2, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		Vector3_t4282066566  L_0 = ___target1;
		Vector3_t4282066566  L_1 = ___current0;
		Vector3_t4282066566  L_2 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Vector3_get_magnitude_m989985786((&V_0), /*hidden argument*/NULL);
		V_1 = L_3;
		float L_4 = V_1;
		float L_5 = ___maxDistanceDelta2;
		if ((((float)L_4) <= ((float)L_5)))
		{
			goto IL_0022;
		}
	}
	{
		float L_6 = V_1;
		if ((!(((float)L_6) == ((float)(0.0f)))))
		{
			goto IL_0024;
		}
	}

IL_0022:
	{
		Vector3_t4282066566  L_7 = ___target1;
		return L_7;
	}

IL_0024:
	{
		Vector3_t4282066566  L_8 = ___current0;
		Vector3_t4282066566  L_9 = V_0;
		float L_10 = V_1;
		Vector3_t4282066566  L_11 = Vector3_op_Division_m4277988370(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		float L_12 = ___maxDistanceDelta2;
		Vector3_t4282066566  L_13 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		Vector3_t4282066566  L_14 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_8, L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::RotateTowards(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern "C"  Vector3_t4282066566  Vector3_RotateTowards_m2666123092 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___current0, Vector3_t4282066566  ___target1, float ___maxRadiansDelta2, float ___maxMagnitudeDelta3, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___maxRadiansDelta2;
		float L_1 = ___maxMagnitudeDelta3;
		Vector3_INTERNAL_CALL_RotateTowards_m3264305586(NULL /*static, unused*/, (&___current0), (&___target1), L_0, L_1, (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Vector3::INTERNAL_CALL_RotateTowards(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Single,UnityEngine.Vector3&)
extern "C"  void Vector3_INTERNAL_CALL_RotateTowards_m3264305586 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___current0, Vector3_t4282066566 * ___target1, float ___maxRadiansDelta2, float ___maxMagnitudeDelta3, Vector3_t4282066566 * ___value4, const MethodInfo* method)
{
	typedef void (*Vector3_INTERNAL_CALL_RotateTowards_m3264305586_ftn) (Vector3_t4282066566 *, Vector3_t4282066566 *, float, float, Vector3_t4282066566 *);
	static Vector3_INTERNAL_CALL_RotateTowards_m3264305586_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Vector3_INTERNAL_CALL_RotateTowards_m3264305586_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Vector3::INTERNAL_CALL_RotateTowards(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Single,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___current0, ___target1, ___maxRadiansDelta2, ___maxMagnitudeDelta3, ___value4);
}
// System.Single UnityEngine.Vector3::get_Item(System.Int32)
extern Il2CppClass* IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral965278184;
extern const uint32_t Vector3_get_Item_m108333500_MetadataUsageId;
extern "C"  float Vector3_get_Item_m108333500 (Vector3_t4282066566 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3_get_Item_m108333500_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0019;
		}
		if (L_1 == 1)
		{
			goto IL_0020;
		}
		if (L_1 == 2)
		{
			goto IL_0027;
		}
	}
	{
		goto IL_002e;
	}

IL_0019:
	{
		float L_2 = __this->get_x_1();
		return L_2;
	}

IL_0020:
	{
		float L_3 = __this->get_y_2();
		return L_3;
	}

IL_0027:
	{
		float L_4 = __this->get_z_3();
		return L_4;
	}

IL_002e:
	{
		IndexOutOfRangeException_t3456360697 * L_5 = (IndexOutOfRangeException_t3456360697 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1621772274(L_5, _stringLiteral965278184, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}
}
extern "C"  float Vector3_get_Item_m108333500_AdjustorThunk (Il2CppObject * __this, int32_t ___index0, const MethodInfo* method)
{
	Vector3_t4282066566 * _thisAdjusted = reinterpret_cast<Vector3_t4282066566 *>(__this + 1);
	return Vector3_get_Item_m108333500(_thisAdjusted, ___index0, method);
}
// System.Void UnityEngine.Vector3::set_Item(System.Int32,System.Single)
extern Il2CppClass* IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral965278184;
extern const uint32_t Vector3_set_Item_m1844835745_MetadataUsageId;
extern "C"  void Vector3_set_Item_m1844835745 (Vector3_t4282066566 * __this, int32_t ___index0, float ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3_set_Item_m1844835745_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0019;
		}
		if (L_1 == 1)
		{
			goto IL_0025;
		}
		if (L_1 == 2)
		{
			goto IL_0031;
		}
	}
	{
		goto IL_003d;
	}

IL_0019:
	{
		float L_2 = ___value1;
		__this->set_x_1(L_2);
		goto IL_0048;
	}

IL_0025:
	{
		float L_3 = ___value1;
		__this->set_y_2(L_3);
		goto IL_0048;
	}

IL_0031:
	{
		float L_4 = ___value1;
		__this->set_z_3(L_4);
		goto IL_0048;
	}

IL_003d:
	{
		IndexOutOfRangeException_t3456360697 * L_5 = (IndexOutOfRangeException_t3456360697 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1621772274(L_5, _stringLiteral965278184, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0048:
	{
		return;
	}
}
extern "C"  void Vector3_set_Item_m1844835745_AdjustorThunk (Il2CppObject * __this, int32_t ___index0, float ___value1, const MethodInfo* method)
{
	Vector3_t4282066566 * _thisAdjusted = reinterpret_cast<Vector3_t4282066566 *>(__this + 1);
	Vector3_set_Item_m1844835745(_thisAdjusted, ___index0, ___value1, method);
}
// UnityEngine.Vector3 UnityEngine.Vector3::Scale(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Vector3_Scale_m3746402337 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, Vector3_t4282066566  ___b1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___b1)->get_z_3();
		Vector3_t4282066566  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m2926210380(&L_6, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::Cross(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Vector3_Cross_m2894122475 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___lhs0, Vector3_t4282066566  ___rhs1, const MethodInfo* method)
{
	{
		float L_0 = (&___lhs0)->get_y_2();
		float L_1 = (&___rhs1)->get_z_3();
		float L_2 = (&___lhs0)->get_z_3();
		float L_3 = (&___rhs1)->get_y_2();
		float L_4 = (&___lhs0)->get_z_3();
		float L_5 = (&___rhs1)->get_x_1();
		float L_6 = (&___lhs0)->get_x_1();
		float L_7 = (&___rhs1)->get_z_3();
		float L_8 = (&___lhs0)->get_x_1();
		float L_9 = (&___rhs1)->get_y_2();
		float L_10 = (&___lhs0)->get_y_2();
		float L_11 = (&___rhs1)->get_x_1();
		Vector3_t4282066566  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector3__ctor_m2926210380(&L_12, ((float)((float)((float)((float)L_0*(float)L_1))-(float)((float)((float)L_2*(float)L_3)))), ((float)((float)((float)((float)L_4*(float)L_5))-(float)((float)((float)L_6*(float)L_7)))), ((float)((float)((float)((float)L_8*(float)L_9))-(float)((float)((float)L_10*(float)L_11)))), /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Int32 UnityEngine.Vector3::GetHashCode()
extern "C"  int32_t Vector3_GetHashCode_m3912867704 (Vector3_t4282066566 * __this, const MethodInfo* method)
{
	{
		float* L_0 = __this->get_address_of_x_1();
		int32_t L_1 = Single_GetHashCode_m65342520(L_0, /*hidden argument*/NULL);
		float* L_2 = __this->get_address_of_y_2();
		int32_t L_3 = Single_GetHashCode_m65342520(L_2, /*hidden argument*/NULL);
		float* L_4 = __this->get_address_of_z_3();
		int32_t L_5 = Single_GetHashCode_m65342520(L_4, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))));
	}
}
extern "C"  int32_t Vector3_GetHashCode_m3912867704_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector3_t4282066566 * _thisAdjusted = reinterpret_cast<Vector3_t4282066566 *>(__this + 1);
	return Vector3_GetHashCode_m3912867704(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Vector3::Equals(System.Object)
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern const uint32_t Vector3_Equals_m3337192096_MetadataUsageId;
extern "C"  bool Vector3_Equals_m3337192096 (Vector3_t4282066566 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Equals_m3337192096_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t G_B6_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Vector3_t4282066566_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(Vector3_t4282066566 *)((Vector3_t4282066566 *)UnBox (L_1, Vector3_t4282066566_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_x_1();
		float L_3 = (&V_0)->get_x_1();
		bool L_4 = Single_Equals_m2110115959(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0056;
		}
	}
	{
		float* L_5 = __this->get_address_of_y_2();
		float L_6 = (&V_0)->get_y_2();
		bool L_7 = Single_Equals_m2110115959(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0056;
		}
	}
	{
		float* L_8 = __this->get_address_of_z_3();
		float L_9 = (&V_0)->get_z_3();
		bool L_10 = Single_Equals_m2110115959(L_8, L_9, /*hidden argument*/NULL);
		G_B6_0 = ((int32_t)(L_10));
		goto IL_0057;
	}

IL_0056:
	{
		G_B6_0 = 0;
	}

IL_0057:
	{
		return (bool)G_B6_0;
	}
}
extern "C"  bool Vector3_Equals_m3337192096_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Vector3_t4282066566 * _thisAdjusted = reinterpret_cast<Vector3_t4282066566 *>(__this + 1);
	return Vector3_Equals_m3337192096(_thisAdjusted, ___other0, method);
}
// UnityEngine.Vector3 UnityEngine.Vector3::Reflect(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Vector3_Reflect_m2873550862 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___inDirection0, Vector3_t4282066566  ___inNormal1, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = ___inNormal1;
		Vector3_t4282066566  L_1 = ___inDirection0;
		float L_2 = Vector3_Dot_m2370485424(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Vector3_t4282066566  L_3 = ___inNormal1;
		Vector3_t4282066566  L_4 = Vector3_op_Multiply_m3809076219(NULL /*static, unused*/, ((float)((float)(-2.0f)*(float)L_2)), L_3, /*hidden argument*/NULL);
		Vector3_t4282066566  L_5 = ___inDirection0;
		Vector3_t4282066566  L_6 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::Normalize(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Vector3_Normalize_m3047997355 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		Vector3_t4282066566  L_0 = ___value0;
		float L_1 = Vector3_Magnitude_m995314358(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = V_0;
		if ((!(((float)L_2) > ((float)(1.0E-05f)))))
		{
			goto IL_001a;
		}
	}
	{
		Vector3_t4282066566  L_3 = ___value0;
		float L_4 = V_0;
		Vector3_t4282066566  L_5 = Vector3_op_Division_m4277988370(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_001a:
	{
		Vector3_t4282066566  L_6 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.Vector3::Normalize()
extern "C"  void Vector3_Normalize_m3984983796 (Vector3_t4282066566 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = Vector3_Magnitude_m995314358(NULL /*static, unused*/, (*(Vector3_t4282066566 *)__this), /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = V_0;
		if ((!(((float)L_1) > ((float)(1.0E-05f)))))
		{
			goto IL_002e;
		}
	}
	{
		float L_2 = V_0;
		Vector3_t4282066566  L_3 = Vector3_op_Division_m4277988370(NULL /*static, unused*/, (*(Vector3_t4282066566 *)__this), L_2, /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)__this) = L_3;
		goto IL_0039;
	}

IL_002e:
	{
		Vector3_t4282066566  L_4 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)__this) = L_4;
	}

IL_0039:
	{
		return;
	}
}
extern "C"  void Vector3_Normalize_m3984983796_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector3_t4282066566 * _thisAdjusted = reinterpret_cast<Vector3_t4282066566 *>(__this + 1);
	Vector3_Normalize_m3984983796(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
extern "C"  Vector3_t4282066566  Vector3_get_normalized_m2650940353 (Vector3_t4282066566 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = Vector3_Normalize_m3047997355(NULL /*static, unused*/, (*(Vector3_t4282066566 *)__this), /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  Vector3_t4282066566  Vector3_get_normalized_m2650940353_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector3_t4282066566 * _thisAdjusted = reinterpret_cast<Vector3_t4282066566 *>(__this + 1);
	return Vector3_get_normalized_m2650940353(_thisAdjusted, method);
}
// System.String UnityEngine.Vector3::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1732534255;
extern const uint32_t Vector3_ToString_m3566373060_MetadataUsageId;
extern "C"  String_t* Vector3_ToString_m3566373060 (Vector3_t4282066566 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3_ToString_m3566373060_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)3));
		float L_1 = __this->get_x_1();
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_0;
		float L_5 = __this->get_y_2();
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t1108656482* L_8 = L_4;
		float L_9 = __this->get_z_3();
		float L_10 = L_9;
		Il2CppObject * L_11 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		String_t* L_12 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral1732534255, L_8, /*hidden argument*/NULL);
		return L_12;
	}
}
extern "C"  String_t* Vector3_ToString_m3566373060_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector3_t4282066566 * _thisAdjusted = reinterpret_cast<Vector3_t4282066566 *>(__this + 1);
	return Vector3_ToString_m3566373060(_thisAdjusted, method);
}
// System.Single UnityEngine.Vector3::Dot(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Vector3_Dot_m2370485424 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___lhs0, Vector3_t4282066566  ___rhs1, const MethodInfo* method)
{
	{
		float L_0 = (&___lhs0)->get_x_1();
		float L_1 = (&___rhs1)->get_x_1();
		float L_2 = (&___lhs0)->get_y_2();
		float L_3 = (&___rhs1)->get_y_2();
		float L_4 = (&___lhs0)->get_z_3();
		float L_5 = (&___rhs1)->get_z_3();
		return ((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))));
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::Project(UnityEngine.Vector3,UnityEngine.Vector3)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Vector3_Project_m2128087858_MetadataUsageId;
extern "C"  Vector3_t4282066566  Vector3_Project_m2128087858 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___vector0, Vector3_t4282066566  ___onNormal1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Project_m2128087858_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		Vector3_t4282066566  L_0 = ___onNormal1;
		Vector3_t4282066566  L_1 = ___onNormal1;
		float L_2 = Vector3_Dot_m2370485424(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_4 = ((Mathf_t4203372500_StaticFields*)Mathf_t4203372500_il2cpp_TypeInfo_var->static_fields)->get_Epsilon_0();
		if ((!(((float)L_3) < ((float)L_4))))
		{
			goto IL_0019;
		}
	}
	{
		Vector3_t4282066566  L_5 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_5;
	}

IL_0019:
	{
		Vector3_t4282066566  L_6 = ___onNormal1;
		Vector3_t4282066566  L_7 = ___vector0;
		Vector3_t4282066566  L_8 = ___onNormal1;
		float L_9 = Vector3_Dot_m2370485424(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		Vector3_t4282066566  L_10 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_6, L_9, /*hidden argument*/NULL);
		float L_11 = V_0;
		Vector3_t4282066566  L_12 = Vector3_op_Division_m4277988370(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Single UnityEngine.Vector3::Angle(UnityEngine.Vector3,UnityEngine.Vector3)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Vector3_Angle_m1904328934_MetadataUsageId;
extern "C"  float Vector3_Angle_m1904328934 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___from0, Vector3_t4282066566  ___to1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Angle_m1904328934_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector3_t4282066566  L_0 = Vector3_get_normalized_m2650940353((&___from0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_1 = Vector3_get_normalized_m2650940353((&___to1), /*hidden argument*/NULL);
		float L_2 = Vector3_Dot_m2370485424(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_2, (-1.0f), (1.0f), /*hidden argument*/NULL);
		float L_4 = acosf(L_3);
		return ((float)((float)L_4*(float)(57.29578f)));
	}
}
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Vector3_Distance_m3366690344_MetadataUsageId;
extern "C"  float Vector3_Distance_m3366690344 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, Vector3_t4282066566  ___b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Distance_m3366690344_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___b1)->get_z_3();
		Vector3__ctor_m2926210380((&V_0), ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), /*hidden argument*/NULL);
		float L_6 = (&V_0)->get_x_1();
		float L_7 = (&V_0)->get_x_1();
		float L_8 = (&V_0)->get_y_2();
		float L_9 = (&V_0)->get_y_2();
		float L_10 = (&V_0)->get_z_3();
		float L_11 = (&V_0)->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_12 = sqrtf(((float)((float)((float)((float)((float)((float)L_6*(float)L_7))+(float)((float)((float)L_8*(float)L_9))))+(float)((float)((float)L_10*(float)L_11)))));
		return L_12;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::ClampMagnitude(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t4282066566  Vector3_ClampMagnitude_m4004286216 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___vector0, float ___maxLength1, const MethodInfo* method)
{
	{
		float L_0 = Vector3_get_sqrMagnitude_m1207423764((&___vector0), /*hidden argument*/NULL);
		float L_1 = ___maxLength1;
		float L_2 = ___maxLength1;
		if ((!(((float)L_0) > ((float)((float)((float)L_1*(float)L_2))))))
		{
			goto IL_001d;
		}
	}
	{
		Vector3_t4282066566  L_3 = Vector3_get_normalized_m2650940353((&___vector0), /*hidden argument*/NULL);
		float L_4 = ___maxLength1;
		Vector3_t4282066566  L_5 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_001d:
	{
		Vector3_t4282066566  L_6 = ___vector0;
		return L_6;
	}
}
// System.Single UnityEngine.Vector3::Magnitude(UnityEngine.Vector3)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Vector3_Magnitude_m995314358_MetadataUsageId;
extern "C"  float Vector3_Magnitude_m995314358 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Magnitude_m995314358_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___a0)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___a0)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___a0)->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_6 = sqrtf(((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5)))));
		return L_6;
	}
}
// System.Single UnityEngine.Vector3::get_magnitude()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Vector3_get_magnitude_m989985786_MetadataUsageId;
extern "C"  float Vector3_get_magnitude_m989985786 (Vector3_t4282066566 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3_get_magnitude_m989985786_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_x_1();
		float L_1 = __this->get_x_1();
		float L_2 = __this->get_y_2();
		float L_3 = __this->get_y_2();
		float L_4 = __this->get_z_3();
		float L_5 = __this->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_6 = sqrtf(((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5)))));
		return L_6;
	}
}
extern "C"  float Vector3_get_magnitude_m989985786_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector3_t4282066566 * _thisAdjusted = reinterpret_cast<Vector3_t4282066566 *>(__this + 1);
	return Vector3_get_magnitude_m989985786(_thisAdjusted, method);
}
// System.Single UnityEngine.Vector3::SqrMagnitude(UnityEngine.Vector3)
extern "C"  float Vector3_SqrMagnitude_m1662776270 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___a0)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___a0)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___a0)->get_z_3();
		return ((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))));
	}
}
// System.Single UnityEngine.Vector3::get_sqrMagnitude()
extern "C"  float Vector3_get_sqrMagnitude_m1207423764 (Vector3_t4282066566 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_x_1();
		float L_1 = __this->get_x_1();
		float L_2 = __this->get_y_2();
		float L_3 = __this->get_y_2();
		float L_4 = __this->get_z_3();
		float L_5 = __this->get_z_3();
		return ((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))));
	}
}
extern "C"  float Vector3_get_sqrMagnitude_m1207423764_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector3_t4282066566 * _thisAdjusted = reinterpret_cast<Vector3_t4282066566 *>(__this + 1);
	return Vector3_get_sqrMagnitude_m1207423764(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Vector3::Min(UnityEngine.Vector3,UnityEngine.Vector3)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Vector3_Min_m10392601_MetadataUsageId;
extern "C"  Vector3_t4282066566  Vector3_Min_m10392601 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___lhs0, Vector3_t4282066566  ___rhs1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Min_m10392601_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (&___lhs0)->get_x_1();
		float L_1 = (&___rhs1)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Min_m2322067385(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = (&___lhs0)->get_y_2();
		float L_4 = (&___rhs1)->get_y_2();
		float L_5 = Mathf_Min_m2322067385(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = (&___lhs0)->get_z_3();
		float L_7 = (&___rhs1)->get_z_3();
		float L_8 = Mathf_Min_m2322067385(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Vector3_t4282066566  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m2926210380(&L_9, L_2, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::Max(UnityEngine.Vector3,UnityEngine.Vector3)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Vector3_Max_m545730887_MetadataUsageId;
extern "C"  Vector3_t4282066566  Vector3_Max_m545730887 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___lhs0, Vector3_t4282066566  ___rhs1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Max_m545730887_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (&___lhs0)->get_x_1();
		float L_1 = (&___rhs1)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Max_m3923796455(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = (&___lhs0)->get_y_2();
		float L_4 = (&___rhs1)->get_y_2();
		float L_5 = Mathf_Max_m3923796455(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = (&___lhs0)->get_z_3();
		float L_7 = (&___rhs1)->get_z_3();
		float L_8 = Mathf_Max_m3923796455(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Vector3_t4282066566  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m2926210380(&L_9, L_2, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C"  Vector3_t4282066566  Vector3_get_zero_m2017759730 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m2926210380(&L_0, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_one()
extern "C"  Vector3_t4282066566  Vector3_get_one_m886467710 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m2926210380(&L_0, (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern "C"  Vector3_t4282066566  Vector3_get_forward_m1039372701 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m2926210380(&L_0, (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_back()
extern "C"  Vector3_t4282066566  Vector3_get_back_m1326515313 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m2926210380(&L_0, (0.0f), (0.0f), (-1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
extern "C"  Vector3_t4282066566  Vector3_get_up_m4046647141 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m2926210380(&L_0, (0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_down()
extern "C"  Vector3_t4282066566  Vector3_get_down_m1397301612 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m2926210380(&L_0, (0.0f), (-1.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_left()
extern "C"  Vector3_t4282066566  Vector3_get_left_m1616598929 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m2926210380(&L_0, (-1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_right()
extern "C"  Vector3_t4282066566  Vector3_get_right_m4015137012 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m2926210380(&L_0, (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Vector3_op_Addition_m695438225 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, Vector3_t4282066566  ___b1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___b1)->get_z_3();
		Vector3_t4282066566  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m2926210380(&L_6, ((float)((float)L_0+(float)L_1)), ((float)((float)L_2+(float)L_3)), ((float)((float)L_4+(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Vector3_op_Subtraction_m2842958165 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, Vector3_t4282066566  ___b1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___b1)->get_z_3();
		Vector3_t4282066566  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m2926210380(&L_6, ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_UnaryNegation(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Vector3_op_UnaryNegation_m3293197314 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___a0)->get_y_2();
		float L_2 = (&___a0)->get_z_3();
		Vector3_t4282066566  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m2926210380(&L_3, ((-L_0)), ((-L_1)), ((-L_2)), /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t4282066566  Vector3_op_Multiply_m973638459 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, float ___d1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = ___d1;
		float L_2 = (&___a0)->get_y_2();
		float L_3 = ___d1;
		float L_4 = (&___a0)->get_z_3();
		float L_5 = ___d1;
		Vector3_t4282066566  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m2926210380(&L_6, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(System.Single,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Vector3_op_Multiply_m3809076219 (Il2CppObject * __this /* static, unused */, float ___d0, Vector3_t4282066566  ___a1, const MethodInfo* method)
{
	{
		float L_0 = (&___a1)->get_x_1();
		float L_1 = ___d0;
		float L_2 = (&___a1)->get_y_2();
		float L_3 = ___d0;
		float L_4 = (&___a1)->get_z_3();
		float L_5 = ___d0;
		Vector3_t4282066566  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m2926210380(&L_6, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Division(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t4282066566  Vector3_op_Division_m4277988370 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___a0, float ___d1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = ___d1;
		float L_2 = (&___a0)->get_y_2();
		float L_3 = ___d1;
		float L_4 = (&___a0)->get_z_3();
		float L_5 = ___d1;
		Vector3_t4282066566  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m2926210380(&L_6, ((float)((float)L_0/(float)L_1)), ((float)((float)L_2/(float)L_3)), ((float)((float)L_4/(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean UnityEngine.Vector3::op_Equality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Vector3_op_Equality_m582817895 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___lhs0, Vector3_t4282066566  ___rhs1, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = ___lhs0;
		Vector3_t4282066566  L_1 = ___rhs1;
		Vector3_t4282066566  L_2 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector3_SqrMagnitude_m1662776270(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return (bool)((((float)L_3) < ((float)(9.99999944E-11f)))? 1 : 0);
	}
}
// System.Boolean UnityEngine.Vector3::op_Inequality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Vector3_op_Inequality_m231387234 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___lhs0, Vector3_t4282066566  ___rhs1, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = ___lhs0;
		Vector3_t4282066566  L_1 = ___rhs1;
		Vector3_t4282066566  L_2 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector3_SqrMagnitude_m1662776270(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return (bool)((((int32_t)((!(((float)L_3) >= ((float)(9.99999944E-11f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.Vector3
extern "C" void Vector3_t4282066566_marshal_pinvoke(const Vector3_t4282066566& unmarshaled, Vector3_t4282066566_marshaled_pinvoke& marshaled)
{
	marshaled.___x_1 = unmarshaled.get_x_1();
	marshaled.___y_2 = unmarshaled.get_y_2();
	marshaled.___z_3 = unmarshaled.get_z_3();
}
extern "C" void Vector3_t4282066566_marshal_pinvoke_back(const Vector3_t4282066566_marshaled_pinvoke& marshaled, Vector3_t4282066566& unmarshaled)
{
	float unmarshaled_x_temp_0 = 0.0f;
	unmarshaled_x_temp_0 = marshaled.___x_1;
	unmarshaled.set_x_1(unmarshaled_x_temp_0);
	float unmarshaled_y_temp_1 = 0.0f;
	unmarshaled_y_temp_1 = marshaled.___y_2;
	unmarshaled.set_y_2(unmarshaled_y_temp_1);
	float unmarshaled_z_temp_2 = 0.0f;
	unmarshaled_z_temp_2 = marshaled.___z_3;
	unmarshaled.set_z_3(unmarshaled_z_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.Vector3
extern "C" void Vector3_t4282066566_marshal_pinvoke_cleanup(Vector3_t4282066566_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Vector3
extern "C" void Vector3_t4282066566_marshal_com(const Vector3_t4282066566& unmarshaled, Vector3_t4282066566_marshaled_com& marshaled)
{
	marshaled.___x_1 = unmarshaled.get_x_1();
	marshaled.___y_2 = unmarshaled.get_y_2();
	marshaled.___z_3 = unmarshaled.get_z_3();
}
extern "C" void Vector3_t4282066566_marshal_com_back(const Vector3_t4282066566_marshaled_com& marshaled, Vector3_t4282066566& unmarshaled)
{
	float unmarshaled_x_temp_0 = 0.0f;
	unmarshaled_x_temp_0 = marshaled.___x_1;
	unmarshaled.set_x_1(unmarshaled_x_temp_0);
	float unmarshaled_y_temp_1 = 0.0f;
	unmarshaled_y_temp_1 = marshaled.___y_2;
	unmarshaled.set_y_2(unmarshaled_y_temp_1);
	float unmarshaled_z_temp_2 = 0.0f;
	unmarshaled_z_temp_2 = marshaled.___z_3;
	unmarshaled.set_z_3(unmarshaled_z_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.Vector3
extern "C" void Vector3_t4282066566_marshal_com_cleanup(Vector3_t4282066566_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Vector4__ctor_m2441427762 (Vector4_t4282066567 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_1(L_0);
		float L_1 = ___y1;
		__this->set_y_2(L_1);
		float L_2 = ___z2;
		__this->set_z_3(L_2);
		float L_3 = ___w3;
		__this->set_w_4(L_3);
		return;
	}
}
extern "C"  void Vector4__ctor_m2441427762_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, float ___z2, float ___w3, const MethodInfo* method)
{
	Vector4_t4282066567 * _thisAdjusted = reinterpret_cast<Vector4_t4282066567 *>(__this + 1);
	Vector4__ctor_m2441427762(_thisAdjusted, ___x0, ___y1, ___z2, ___w3, method);
}
// System.Single UnityEngine.Vector4::get_Item(System.Int32)
extern Il2CppClass* IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2708088519;
extern const uint32_t Vector4_get_Item_m2326091453_MetadataUsageId;
extern "C"  float Vector4_get_Item_m2326091453 (Vector4_t4282066567 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector4_get_Item_m2326091453_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_001d;
		}
		if (L_1 == 1)
		{
			goto IL_0024;
		}
		if (L_1 == 2)
		{
			goto IL_002b;
		}
		if (L_1 == 3)
		{
			goto IL_0032;
		}
	}
	{
		goto IL_0039;
	}

IL_001d:
	{
		float L_2 = __this->get_x_1();
		return L_2;
	}

IL_0024:
	{
		float L_3 = __this->get_y_2();
		return L_3;
	}

IL_002b:
	{
		float L_4 = __this->get_z_3();
		return L_4;
	}

IL_0032:
	{
		float L_5 = __this->get_w_4();
		return L_5;
	}

IL_0039:
	{
		IndexOutOfRangeException_t3456360697 * L_6 = (IndexOutOfRangeException_t3456360697 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1621772274(L_6, _stringLiteral2708088519, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}
}
extern "C"  float Vector4_get_Item_m2326091453_AdjustorThunk (Il2CppObject * __this, int32_t ___index0, const MethodInfo* method)
{
	Vector4_t4282066567 * _thisAdjusted = reinterpret_cast<Vector4_t4282066567 *>(__this + 1);
	return Vector4_get_Item_m2326091453(_thisAdjusted, ___index0, method);
}
// System.Void UnityEngine.Vector4::set_Item(System.Int32,System.Single)
extern Il2CppClass* IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2708088519;
extern const uint32_t Vector4_set_Item_m922152162_MetadataUsageId;
extern "C"  void Vector4_set_Item_m922152162 (Vector4_t4282066567 * __this, int32_t ___index0, float ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector4_set_Item_m922152162_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_001d;
		}
		if (L_1 == 1)
		{
			goto IL_0029;
		}
		if (L_1 == 2)
		{
			goto IL_0035;
		}
		if (L_1 == 3)
		{
			goto IL_0041;
		}
	}
	{
		goto IL_004d;
	}

IL_001d:
	{
		float L_2 = ___value1;
		__this->set_x_1(L_2);
		goto IL_0058;
	}

IL_0029:
	{
		float L_3 = ___value1;
		__this->set_y_2(L_3);
		goto IL_0058;
	}

IL_0035:
	{
		float L_4 = ___value1;
		__this->set_z_3(L_4);
		goto IL_0058;
	}

IL_0041:
	{
		float L_5 = ___value1;
		__this->set_w_4(L_5);
		goto IL_0058;
	}

IL_004d:
	{
		IndexOutOfRangeException_t3456360697 * L_6 = (IndexOutOfRangeException_t3456360697 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1621772274(L_6, _stringLiteral2708088519, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0058:
	{
		return;
	}
}
extern "C"  void Vector4_set_Item_m922152162_AdjustorThunk (Il2CppObject * __this, int32_t ___index0, float ___value1, const MethodInfo* method)
{
	Vector4_t4282066567 * _thisAdjusted = reinterpret_cast<Vector4_t4282066567 *>(__this + 1);
	Vector4_set_Item_m922152162(_thisAdjusted, ___index0, ___value1, method);
}
// System.Int32 UnityEngine.Vector4::GetHashCode()
extern "C"  int32_t Vector4_GetHashCode_m3402333527 (Vector4_t4282066567 * __this, const MethodInfo* method)
{
	{
		float* L_0 = __this->get_address_of_x_1();
		int32_t L_1 = Single_GetHashCode_m65342520(L_0, /*hidden argument*/NULL);
		float* L_2 = __this->get_address_of_y_2();
		int32_t L_3 = Single_GetHashCode_m65342520(L_2, /*hidden argument*/NULL);
		float* L_4 = __this->get_address_of_z_3();
		int32_t L_5 = Single_GetHashCode_m65342520(L_4, /*hidden argument*/NULL);
		float* L_6 = __this->get_address_of_w_4();
		int32_t L_7 = Single_GetHashCode_m65342520(L_6, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
	}
}
extern "C"  int32_t Vector4_GetHashCode_m3402333527_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector4_t4282066567 * _thisAdjusted = reinterpret_cast<Vector4_t4282066567 *>(__this + 1);
	return Vector4_GetHashCode_m3402333527(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Vector4::Equals(System.Object)
extern Il2CppClass* Vector4_t4282066567_il2cpp_TypeInfo_var;
extern const uint32_t Vector4_Equals_m3270185343_MetadataUsageId;
extern "C"  bool Vector4_Equals_m3270185343 (Vector4_t4282066567 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector4_Equals_m3270185343_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector4_t4282066567  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Vector4_t4282066567_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(Vector4_t4282066567 *)((Vector4_t4282066567 *)UnBox (L_1, Vector4_t4282066567_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_x_1();
		float L_3 = (&V_0)->get_x_1();
		bool L_4 = Single_Equals_m2110115959(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_006d;
		}
	}
	{
		float* L_5 = __this->get_address_of_y_2();
		float L_6 = (&V_0)->get_y_2();
		bool L_7 = Single_Equals_m2110115959(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006d;
		}
	}
	{
		float* L_8 = __this->get_address_of_z_3();
		float L_9 = (&V_0)->get_z_3();
		bool L_10 = Single_Equals_m2110115959(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006d;
		}
	}
	{
		float* L_11 = __this->get_address_of_w_4();
		float L_12 = (&V_0)->get_w_4();
		bool L_13 = Single_Equals_m2110115959(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_006e;
	}

IL_006d:
	{
		G_B7_0 = 0;
	}

IL_006e:
	{
		return (bool)G_B7_0;
	}
}
extern "C"  bool Vector4_Equals_m3270185343_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Vector4_t4282066567 * _thisAdjusted = reinterpret_cast<Vector4_t4282066567 *>(__this + 1);
	return Vector4_Equals_m3270185343(_thisAdjusted, ___other0, method);
}
// System.String UnityEngine.Vector4::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral843281963;
extern const uint32_t Vector4_ToString_m3272970053_MetadataUsageId;
extern "C"  String_t* Vector4_ToString_m3272970053 (Vector4_t4282066567 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector4_ToString_m3272970053_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = __this->get_x_1();
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_0;
		float L_5 = __this->get_y_2();
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t1108656482* L_8 = L_4;
		float L_9 = __this->get_z_3();
		float L_10 = L_9;
		Il2CppObject * L_11 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t1108656482* L_12 = L_8;
		float L_13 = __this->get_w_4();
		float L_14 = L_13;
		Il2CppObject * L_15 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral843281963, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
extern "C"  String_t* Vector4_ToString_m3272970053_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector4_t4282066567 * _thisAdjusted = reinterpret_cast<Vector4_t4282066567 *>(__this + 1);
	return Vector4_ToString_m3272970053(_thisAdjusted, method);
}
// System.Single UnityEngine.Vector4::Dot(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  float Vector4_Dot_m2303368623 (Il2CppObject * __this /* static, unused */, Vector4_t4282066567  ___a0, Vector4_t4282066567  ___b1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___b1)->get_z_3();
		float L_6 = (&___a0)->get_w_4();
		float L_7 = (&___b1)->get_w_4();
		return ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7))));
	}
}
// System.Single UnityEngine.Vector4::Distance(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  float Vector4_Distance_m2097804101 (Il2CppObject * __this /* static, unused */, Vector4_t4282066567  ___a0, Vector4_t4282066567  ___b1, const MethodInfo* method)
{
	{
		Vector4_t4282066567  L_0 = ___a0;
		Vector4_t4282066567  L_1 = ___b1;
		Vector4_t4282066567  L_2 = Vector4_op_Subtraction_m3588766929(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector4_Magnitude_m3106604758(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Single UnityEngine.Vector4::Magnitude(UnityEngine.Vector4)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Vector4_Magnitude_m3106604758_MetadataUsageId;
extern "C"  float Vector4_Magnitude_m3106604758 (Il2CppObject * __this /* static, unused */, Vector4_t4282066567  ___a0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector4_Magnitude_m3106604758_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector4_t4282066567  L_0 = ___a0;
		Vector4_t4282066567  L_1 = ___a0;
		float L_2 = Vector4_Dot_m2303368623(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_3 = sqrtf(L_2);
		return L_3;
	}
}
// System.Single UnityEngine.Vector4::SqrMagnitude(UnityEngine.Vector4)
extern "C"  float Vector4_SqrMagnitude_m3613076556 (Il2CppObject * __this /* static, unused */, Vector4_t4282066567  ___a0, const MethodInfo* method)
{
	{
		Vector4_t4282066567  L_0 = ___a0;
		Vector4_t4282066567  L_1 = ___a0;
		float L_2 = Vector4_Dot_m2303368623(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Single UnityEngine.Vector4::get_sqrMagnitude()
extern "C"  float Vector4_get_sqrMagnitude_m1418774677 (Vector4_t4282066567 * __this, const MethodInfo* method)
{
	{
		float L_0 = Vector4_Dot_m2303368623(NULL /*static, unused*/, (*(Vector4_t4282066567 *)__this), (*(Vector4_t4282066567 *)__this), /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  float Vector4_get_sqrMagnitude_m1418774677_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector4_t4282066567 * _thisAdjusted = reinterpret_cast<Vector4_t4282066567 *>(__this + 1);
	return Vector4_get_sqrMagnitude_m1418774677(_thisAdjusted, method);
}
// UnityEngine.Vector4 UnityEngine.Vector4::get_zero()
extern "C"  Vector4_t4282066567  Vector4_get_zero_m3835647092 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector4_t4282066567  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector4__ctor_m2441427762(&L_0, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector4 UnityEngine.Vector4::op_Subtraction(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  Vector4_t4282066567  Vector4_op_Subtraction_m3588766929 (Il2CppObject * __this /* static, unused */, Vector4_t4282066567  ___a0, Vector4_t4282066567  ___b1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___b1)->get_z_3();
		float L_6 = (&___a0)->get_w_4();
		float L_7 = (&___b1)->get_w_4();
		Vector4_t4282066567  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector4__ctor_m2441427762(&L_8, ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), ((float)((float)L_6-(float)L_7)), /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Vector4 UnityEngine.Vector4::op_Division(UnityEngine.Vector4,System.Single)
extern "C"  Vector4_t4282066567  Vector4_op_Division_m3513381747 (Il2CppObject * __this /* static, unused */, Vector4_t4282066567  ___a0, float ___d1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = ___d1;
		float L_2 = (&___a0)->get_y_2();
		float L_3 = ___d1;
		float L_4 = (&___a0)->get_z_3();
		float L_5 = ___d1;
		float L_6 = (&___a0)->get_w_4();
		float L_7 = ___d1;
		Vector4_t4282066567  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector4__ctor_m2441427762(&L_8, ((float)((float)L_0/(float)L_1)), ((float)((float)L_2/(float)L_3)), ((float)((float)L_4/(float)L_5)), ((float)((float)L_6/(float)L_7)), /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Boolean UnityEngine.Vector4::op_Equality(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  bool Vector4_op_Equality_m3533121638 (Il2CppObject * __this /* static, unused */, Vector4_t4282066567  ___lhs0, Vector4_t4282066567  ___rhs1, const MethodInfo* method)
{
	{
		Vector4_t4282066567  L_0 = ___lhs0;
		Vector4_t4282066567  L_1 = ___rhs1;
		Vector4_t4282066567  L_2 = Vector4_op_Subtraction_m3588766929(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector4_SqrMagnitude_m3613076556(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return (bool)((((float)L_3) < ((float)(9.99999944E-11f)))? 1 : 0);
	}
}
// UnityEngine.Vector4 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector4_t4282066567  Vector4_op_Implicit_m331673271 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___v0, const MethodInfo* method)
{
	{
		float L_0 = (&___v0)->get_x_1();
		float L_1 = (&___v0)->get_y_2();
		float L_2 = (&___v0)->get_z_3();
		Vector4_t4282066567  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector4__ctor_m2441427762(&L_3, L_0, L_1, L_2, (0.0f), /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector4)
extern "C"  Vector3_t4282066566  Vector4_op_Implicit_m3933247893 (Il2CppObject * __this /* static, unused */, Vector4_t4282066567  ___v0, const MethodInfo* method)
{
	{
		float L_0 = (&___v0)->get_x_1();
		float L_1 = (&___v0)->get_y_2();
		float L_2 = (&___v0)->get_z_3();
		Vector3_t4282066566  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m2926210380(&L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// Conversion methods for marshalling of: UnityEngine.Vector4
extern "C" void Vector4_t4282066567_marshal_pinvoke(const Vector4_t4282066567& unmarshaled, Vector4_t4282066567_marshaled_pinvoke& marshaled)
{
	marshaled.___x_1 = unmarshaled.get_x_1();
	marshaled.___y_2 = unmarshaled.get_y_2();
	marshaled.___z_3 = unmarshaled.get_z_3();
	marshaled.___w_4 = unmarshaled.get_w_4();
}
extern "C" void Vector4_t4282066567_marshal_pinvoke_back(const Vector4_t4282066567_marshaled_pinvoke& marshaled, Vector4_t4282066567& unmarshaled)
{
	float unmarshaled_x_temp_0 = 0.0f;
	unmarshaled_x_temp_0 = marshaled.___x_1;
	unmarshaled.set_x_1(unmarshaled_x_temp_0);
	float unmarshaled_y_temp_1 = 0.0f;
	unmarshaled_y_temp_1 = marshaled.___y_2;
	unmarshaled.set_y_2(unmarshaled_y_temp_1);
	float unmarshaled_z_temp_2 = 0.0f;
	unmarshaled_z_temp_2 = marshaled.___z_3;
	unmarshaled.set_z_3(unmarshaled_z_temp_2);
	float unmarshaled_w_temp_3 = 0.0f;
	unmarshaled_w_temp_3 = marshaled.___w_4;
	unmarshaled.set_w_4(unmarshaled_w_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Vector4
extern "C" void Vector4_t4282066567_marshal_pinvoke_cleanup(Vector4_t4282066567_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Vector4
extern "C" void Vector4_t4282066567_marshal_com(const Vector4_t4282066567& unmarshaled, Vector4_t4282066567_marshaled_com& marshaled)
{
	marshaled.___x_1 = unmarshaled.get_x_1();
	marshaled.___y_2 = unmarshaled.get_y_2();
	marshaled.___z_3 = unmarshaled.get_z_3();
	marshaled.___w_4 = unmarshaled.get_w_4();
}
extern "C" void Vector4_t4282066567_marshal_com_back(const Vector4_t4282066567_marshaled_com& marshaled, Vector4_t4282066567& unmarshaled)
{
	float unmarshaled_x_temp_0 = 0.0f;
	unmarshaled_x_temp_0 = marshaled.___x_1;
	unmarshaled.set_x_1(unmarshaled_x_temp_0);
	float unmarshaled_y_temp_1 = 0.0f;
	unmarshaled_y_temp_1 = marshaled.___y_2;
	unmarshaled.set_y_2(unmarshaled_y_temp_1);
	float unmarshaled_z_temp_2 = 0.0f;
	unmarshaled_z_temp_2 = marshaled.___z_3;
	unmarshaled.set_z_3(unmarshaled_z_temp_2);
	float unmarshaled_w_temp_3 = 0.0f;
	unmarshaled_w_temp_3 = marshaled.___w_4;
	unmarshaled.set_w_4(unmarshaled_w_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Vector4
extern "C" void Vector4_t4282066567_marshal_com_cleanup(Vector4_t4282066567_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.WaitForEndOfFrame::.ctor()
extern "C"  void WaitForEndOfFrame__ctor_m4124201226 (WaitForEndOfFrame_t2372756133 * __this, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m539393484(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WaitForFixedUpdate::.ctor()
extern "C"  void WaitForFixedUpdate__ctor_m2916734308 (WaitForFixedUpdate_t2130080621 * __this, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m539393484(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C"  void WaitForSeconds__ctor_m3184996201 (WaitForSeconds_t1615819279 * __this, float ___seconds0, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m539393484(__this, /*hidden argument*/NULL);
		float L_0 = ___seconds0;
		__this->set_m_Seconds_0(L_0);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.WaitForSeconds
extern "C" void WaitForSeconds_t1615819279_marshal_pinvoke(const WaitForSeconds_t1615819279& unmarshaled, WaitForSeconds_t1615819279_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Seconds_0 = unmarshaled.get_m_Seconds_0();
}
extern "C" void WaitForSeconds_t1615819279_marshal_pinvoke_back(const WaitForSeconds_t1615819279_marshaled_pinvoke& marshaled, WaitForSeconds_t1615819279& unmarshaled)
{
	float unmarshaled_m_Seconds_temp_0 = 0.0f;
	unmarshaled_m_Seconds_temp_0 = marshaled.___m_Seconds_0;
	unmarshaled.set_m_Seconds_0(unmarshaled_m_Seconds_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.WaitForSeconds
extern "C" void WaitForSeconds_t1615819279_marshal_pinvoke_cleanup(WaitForSeconds_t1615819279_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.WaitForSeconds
extern "C" void WaitForSeconds_t1615819279_marshal_com(const WaitForSeconds_t1615819279& unmarshaled, WaitForSeconds_t1615819279_marshaled_com& marshaled)
{
	marshaled.___m_Seconds_0 = unmarshaled.get_m_Seconds_0();
}
extern "C" void WaitForSeconds_t1615819279_marshal_com_back(const WaitForSeconds_t1615819279_marshaled_com& marshaled, WaitForSeconds_t1615819279& unmarshaled)
{
	float unmarshaled_m_Seconds_temp_0 = 0.0f;
	unmarshaled_m_Seconds_temp_0 = marshaled.___m_Seconds_0;
	unmarshaled.set_m_Seconds_0(unmarshaled_m_Seconds_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.WaitForSeconds
extern "C" void WaitForSeconds_t1615819279_marshal_com_cleanup(WaitForSeconds_t1615819279_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.WaitUntil::.ctor(System.Func`1<System.Boolean>)
extern "C"  void WaitUntil__ctor_m1048429269 (WaitUntil_t3918096351 * __this, Func_1_t1601960292 * ___predicate0, const MethodInfo* method)
{
	{
		CustomYieldInstruction__ctor_m2800749531(__this, /*hidden argument*/NULL);
		Func_1_t1601960292 * L_0 = ___predicate0;
		__this->set_m_Predicate_0(L_0);
		return;
	}
}
// System.Boolean UnityEngine.WaitUntil::get_keepWaiting()
extern const MethodInfo* Func_1_Invoke_m1113859879_MethodInfo_var;
extern const uint32_t WaitUntil_get_keepWaiting_m4082018771_MetadataUsageId;
extern "C"  bool WaitUntil_get_keepWaiting_m4082018771 (WaitUntil_t3918096351 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WaitUntil_get_keepWaiting_m4082018771_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Func_1_t1601960292 * L_0 = __this->get_m_Predicate_0();
		NullCheck(L_0);
		bool L_1 = Func_1_Invoke_m1113859879(L_0, /*hidden argument*/Func_1_Invoke_m1113859879_MethodInfo_var);
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}
}
// System.String UnityEngine.WebCamDevice::get_name()
extern "C"  String_t* WebCamDevice_get_name_m2875559007 (WebCamDevice_t3274004757 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_Name_0();
		return L_0;
	}
}
extern "C"  String_t* WebCamDevice_get_name_m2875559007_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	WebCamDevice_t3274004757 * _thisAdjusted = reinterpret_cast<WebCamDevice_t3274004757 *>(__this + 1);
	return WebCamDevice_get_name_m2875559007(_thisAdjusted, method);
}
// System.Boolean UnityEngine.WebCamDevice::get_isFrontFacing()
extern "C"  bool WebCamDevice_get_isFrontFacing_m3402023620 (WebCamDevice_t3274004757 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Flags_1();
		return (bool)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)1))) == ((int32_t)1))? 1 : 0);
	}
}
extern "C"  bool WebCamDevice_get_isFrontFacing_m3402023620_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	WebCamDevice_t3274004757 * _thisAdjusted = reinterpret_cast<WebCamDevice_t3274004757 *>(__this + 1);
	return WebCamDevice_get_isFrontFacing_m3402023620(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.WebCamDevice
extern "C" void WebCamDevice_t3274004757_marshal_pinvoke(const WebCamDevice_t3274004757& unmarshaled, WebCamDevice_t3274004757_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Name_0 = il2cpp_codegen_marshal_string(unmarshaled.get_m_Name_0());
	marshaled.___m_Flags_1 = unmarshaled.get_m_Flags_1();
}
extern "C" void WebCamDevice_t3274004757_marshal_pinvoke_back(const WebCamDevice_t3274004757_marshaled_pinvoke& marshaled, WebCamDevice_t3274004757& unmarshaled)
{
	unmarshaled.set_m_Name_0(il2cpp_codegen_marshal_string_result(marshaled.___m_Name_0));
	int32_t unmarshaled_m_Flags_temp_1 = 0;
	unmarshaled_m_Flags_temp_1 = marshaled.___m_Flags_1;
	unmarshaled.set_m_Flags_1(unmarshaled_m_Flags_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.WebCamDevice
extern "C" void WebCamDevice_t3274004757_marshal_pinvoke_cleanup(WebCamDevice_t3274004757_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_Name_0);
	marshaled.___m_Name_0 = NULL;
}
// Conversion methods for marshalling of: UnityEngine.WebCamDevice
extern "C" void WebCamDevice_t3274004757_marshal_com(const WebCamDevice_t3274004757& unmarshaled, WebCamDevice_t3274004757_marshaled_com& marshaled)
{
	marshaled.___m_Name_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_m_Name_0());
	marshaled.___m_Flags_1 = unmarshaled.get_m_Flags_1();
}
extern "C" void WebCamDevice_t3274004757_marshal_com_back(const WebCamDevice_t3274004757_marshaled_com& marshaled, WebCamDevice_t3274004757& unmarshaled)
{
	unmarshaled.set_m_Name_0(il2cpp_codegen_marshal_bstring_result(marshaled.___m_Name_0));
	int32_t unmarshaled_m_Flags_temp_1 = 0;
	unmarshaled_m_Flags_temp_1 = marshaled.___m_Flags_1;
	unmarshaled.set_m_Flags_1(unmarshaled_m_Flags_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.WebCamDevice
extern "C" void WebCamDevice_t3274004757_marshal_com_cleanup(WebCamDevice_t3274004757_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___m_Name_0);
	marshaled.___m_Name_0 = NULL;
}
// System.Void UnityEngine.WebCamTexture::.ctor(System.String,System.Int32,System.Int32,System.Int32)
extern "C"  void WebCamTexture__ctor_m1622497486 (WebCamTexture_t1290350902 * __this, String_t* ___deviceName0, int32_t ___requestedWidth1, int32_t ___requestedHeight2, int32_t ___requestedFPS3, const MethodInfo* method)
{
	{
		Texture__ctor_m516856734(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___deviceName0;
		int32_t L_1 = ___requestedWidth1;
		int32_t L_2 = ___requestedHeight2;
		int32_t L_3 = ___requestedFPS3;
		WebCamTexture_Internal_CreateWebCamTexture_m384251711(NULL /*static, unused*/, __this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WebCamTexture::.ctor(System.String)
extern "C"  void WebCamTexture__ctor_m3625407113 (WebCamTexture_t1290350902 * __this, String_t* ___deviceName0, const MethodInfo* method)
{
	{
		Texture__ctor_m516856734(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___deviceName0;
		WebCamTexture_Internal_CreateWebCamTexture_m384251711(NULL /*static, unused*/, __this, L_0, 0, 0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WebCamTexture::.ctor(System.Int32,System.Int32)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t WebCamTexture__ctor_m1800008749_MetadataUsageId;
extern "C"  void WebCamTexture__ctor_m1800008749 (WebCamTexture_t1290350902 * __this, int32_t ___requestedWidth0, int32_t ___requestedHeight1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebCamTexture__ctor_m1800008749_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture__ctor_m516856734(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		int32_t L_1 = ___requestedWidth0;
		int32_t L_2 = ___requestedHeight1;
		WebCamTexture_Internal_CreateWebCamTexture_m384251711(NULL /*static, unused*/, __this, L_0, L_1, L_2, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WebCamTexture::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t WebCamTexture__ctor_m755637529_MetadataUsageId;
extern "C"  void WebCamTexture__ctor_m755637529 (WebCamTexture_t1290350902 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebCamTexture__ctor_m755637529_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture__ctor_m516856734(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		WebCamTexture_Internal_CreateWebCamTexture_m384251711(NULL /*static, unused*/, __this, L_0, 0, 0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WebCamTexture::Internal_CreateWebCamTexture(UnityEngine.WebCamTexture,System.String,System.Int32,System.Int32,System.Int32)
extern "C"  void WebCamTexture_Internal_CreateWebCamTexture_m384251711 (Il2CppObject * __this /* static, unused */, WebCamTexture_t1290350902 * ___self0, String_t* ___scriptingDevice1, int32_t ___requestedWidth2, int32_t ___requestedHeight3, int32_t ___maxFramerate4, const MethodInfo* method)
{
	typedef void (*WebCamTexture_Internal_CreateWebCamTexture_m384251711_ftn) (WebCamTexture_t1290350902 *, String_t*, int32_t, int32_t, int32_t);
	static WebCamTexture_Internal_CreateWebCamTexture_m384251711_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_Internal_CreateWebCamTexture_m384251711_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::Internal_CreateWebCamTexture(UnityEngine.WebCamTexture,System.String,System.Int32,System.Int32,System.Int32)");
	_il2cpp_icall_func(___self0, ___scriptingDevice1, ___requestedWidth2, ___requestedHeight3, ___maxFramerate4);
}
// System.Void UnityEngine.WebCamTexture::Play()
extern "C"  void WebCamTexture_Play_m2252445503 (WebCamTexture_t1290350902 * __this, const MethodInfo* method)
{
	{
		WebCamTexture_INTERNAL_CALL_Play_m3478980075(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WebCamTexture::INTERNAL_CALL_Play(UnityEngine.WebCamTexture)
extern "C"  void WebCamTexture_INTERNAL_CALL_Play_m3478980075 (Il2CppObject * __this /* static, unused */, WebCamTexture_t1290350902 * ___self0, const MethodInfo* method)
{
	typedef void (*WebCamTexture_INTERNAL_CALL_Play_m3478980075_ftn) (WebCamTexture_t1290350902 *);
	static WebCamTexture_INTERNAL_CALL_Play_m3478980075_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_INTERNAL_CALL_Play_m3478980075_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::INTERNAL_CALL_Play(UnityEngine.WebCamTexture)");
	_il2cpp_icall_func(___self0);
}
// System.Void UnityEngine.WebCamTexture::Pause()
extern "C"  void WebCamTexture_Pause_m809763501 (WebCamTexture_t1290350902 * __this, const MethodInfo* method)
{
	{
		WebCamTexture_INTERNAL_CALL_Pause_m4232847003(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WebCamTexture::INTERNAL_CALL_Pause(UnityEngine.WebCamTexture)
extern "C"  void WebCamTexture_INTERNAL_CALL_Pause_m4232847003 (Il2CppObject * __this /* static, unused */, WebCamTexture_t1290350902 * ___self0, const MethodInfo* method)
{
	typedef void (*WebCamTexture_INTERNAL_CALL_Pause_m4232847003_ftn) (WebCamTexture_t1290350902 *);
	static WebCamTexture_INTERNAL_CALL_Pause_m4232847003_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_INTERNAL_CALL_Pause_m4232847003_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::INTERNAL_CALL_Pause(UnityEngine.WebCamTexture)");
	_il2cpp_icall_func(___self0);
}
// System.Void UnityEngine.WebCamTexture::Stop()
extern "C"  void WebCamTexture_Stop_m2346129549 (WebCamTexture_t1290350902 * __this, const MethodInfo* method)
{
	{
		WebCamTexture_INTERNAL_CALL_Stop_m3733059677(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WebCamTexture::INTERNAL_CALL_Stop(UnityEngine.WebCamTexture)
extern "C"  void WebCamTexture_INTERNAL_CALL_Stop_m3733059677 (Il2CppObject * __this /* static, unused */, WebCamTexture_t1290350902 * ___self0, const MethodInfo* method)
{
	typedef void (*WebCamTexture_INTERNAL_CALL_Stop_m3733059677_ftn) (WebCamTexture_t1290350902 *);
	static WebCamTexture_INTERNAL_CALL_Stop_m3733059677_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_INTERNAL_CALL_Stop_m3733059677_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::INTERNAL_CALL_Stop(UnityEngine.WebCamTexture)");
	_il2cpp_icall_func(___self0);
}
// System.Boolean UnityEngine.WebCamTexture::get_isPlaying()
extern "C"  bool WebCamTexture_get_isPlaying_m1109067512 (WebCamTexture_t1290350902 * __this, const MethodInfo* method)
{
	typedef bool (*WebCamTexture_get_isPlaying_m1109067512_ftn) (WebCamTexture_t1290350902 *);
	static WebCamTexture_get_isPlaying_m1109067512_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_get_isPlaying_m1109067512_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::get_isPlaying()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.WebCamTexture::set_deviceName(System.String)
extern "C"  void WebCamTexture_set_deviceName_m1209257657 (WebCamTexture_t1290350902 * __this, String_t* ___value0, const MethodInfo* method)
{
	typedef void (*WebCamTexture_set_deviceName_m1209257657_ftn) (WebCamTexture_t1290350902 *, String_t*);
	static WebCamTexture_set_deviceName_m1209257657_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_set_deviceName_m1209257657_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::set_deviceName(System.String)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.WebCamTexture::set_requestedFPS(System.Single)
extern "C"  void WebCamTexture_set_requestedFPS_m644257608 (WebCamTexture_t1290350902 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*WebCamTexture_set_requestedFPS_m644257608_ftn) (WebCamTexture_t1290350902 *, float);
	static WebCamTexture_set_requestedFPS_m644257608_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_set_requestedFPS_m644257608_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::set_requestedFPS(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.WebCamTexture::set_requestedWidth(System.Int32)
extern "C"  void WebCamTexture_set_requestedWidth_m3687101425 (WebCamTexture_t1290350902 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*WebCamTexture_set_requestedWidth_m3687101425_ftn) (WebCamTexture_t1290350902 *, int32_t);
	static WebCamTexture_set_requestedWidth_m3687101425_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_set_requestedWidth_m3687101425_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::set_requestedWidth(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.WebCamTexture::set_requestedHeight(System.Int32)
extern "C"  void WebCamTexture_set_requestedHeight_m3951846656 (WebCamTexture_t1290350902 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*WebCamTexture_set_requestedHeight_m3951846656_ftn) (WebCamTexture_t1290350902 *, int32_t);
	static WebCamTexture_set_requestedHeight_m3951846656_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_set_requestedHeight_m3951846656_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::set_requestedHeight(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.WebCamDevice[] UnityEngine.WebCamTexture::get_devices()
extern "C"  WebCamDeviceU5BU5D_t3721690872* WebCamTexture_get_devices_m3738445840 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef WebCamDeviceU5BU5D_t3721690872* (*WebCamTexture_get_devices_m3738445840_ftn) ();
	static WebCamTexture_get_devices_m3738445840_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_get_devices_m3738445840_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::get_devices()");
	return _il2cpp_icall_func();
}
// UnityEngine.Color[] UnityEngine.WebCamTexture::GetPixels()
extern "C"  ColorU5BU5D_t2441545636* WebCamTexture_GetPixels_m2104673983 (WebCamTexture_t1290350902 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, __this);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, __this);
		ColorU5BU5D_t2441545636* L_2 = WebCamTexture_GetPixels_m3429338087(__this, 0, 0, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Color[] UnityEngine.WebCamTexture::GetPixels(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  ColorU5BU5D_t2441545636* WebCamTexture_GetPixels_m3429338087 (WebCamTexture_t1290350902 * __this, int32_t ___x0, int32_t ___y1, int32_t ___blockWidth2, int32_t ___blockHeight3, const MethodInfo* method)
{
	typedef ColorU5BU5D_t2441545636* (*WebCamTexture_GetPixels_m3429338087_ftn) (WebCamTexture_t1290350902 *, int32_t, int32_t, int32_t, int32_t);
	static WebCamTexture_GetPixels_m3429338087_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_GetPixels_m3429338087_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::GetPixels(System.Int32,System.Int32,System.Int32,System.Int32)");
	return _il2cpp_icall_func(__this, ___x0, ___y1, ___blockWidth2, ___blockHeight3);
}
// UnityEngine.Color32[] UnityEngine.WebCamTexture::GetPixels32(UnityEngine.Color32[])
extern "C"  Color32U5BU5D_t2960766953* WebCamTexture_GetPixels32_m2023060102 (WebCamTexture_t1290350902 * __this, Color32U5BU5D_t2960766953* ___colors0, const MethodInfo* method)
{
	typedef Color32U5BU5D_t2960766953* (*WebCamTexture_GetPixels32_m2023060102_ftn) (WebCamTexture_t1290350902 *, Color32U5BU5D_t2960766953*);
	static WebCamTexture_GetPixels32_m2023060102_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_GetPixels32_m2023060102_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::GetPixels32(UnityEngine.Color32[])");
	return _il2cpp_icall_func(__this, ___colors0);
}
// UnityEngine.Color32[] UnityEngine.WebCamTexture::GetPixels32()
extern "C"  Color32U5BU5D_t2960766953* WebCamTexture_GetPixels32_m3040478367 (WebCamTexture_t1290350902 * __this, const MethodInfo* method)
{
	Color32U5BU5D_t2960766953* V_0 = NULL;
	{
		V_0 = (Color32U5BU5D_t2960766953*)NULL;
		Color32U5BU5D_t2960766953* L_0 = V_0;
		Color32U5BU5D_t2960766953* L_1 = WebCamTexture_GetPixels32_m2023060102(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.WebCamTexture::get_didUpdateThisFrame()
extern "C"  bool WebCamTexture_get_didUpdateThisFrame_m2572205429 (WebCamTexture_t1290350902 * __this, const MethodInfo* method)
{
	typedef bool (*WebCamTexture_get_didUpdateThisFrame_m2572205429_ftn) (WebCamTexture_t1290350902 *);
	static WebCamTexture_get_didUpdateThisFrame_m2572205429_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_get_didUpdateThisFrame_m2572205429_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::get_didUpdateThisFrame()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.WrapperlessIcall::.ctor()
extern "C"  void WrapperlessIcall__ctor_m1888400594 (WrapperlessIcall_t2494346367 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WritableAttribute::.ctor()
extern "C"  void WritableAttribute__ctor_m2205809533 (WritableAttribute_t2171443922 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WWW::.ctor(System.String)
extern "C"  void WWW__ctor_m1985874080 (WWW_t3134621005 * __this, String_t* ___url0, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___url0;
		WWW_InitWWW_m3594284248(__this, L_0, (ByteU5BU5D_t4260760469*)(ByteU5BU5D_t4260760469*)NULL, (StringU5BU5D_t4054002952*)(StringU5BU5D_t4054002952*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WWW::.ctor(System.String,UnityEngine.WWWForm)
extern "C"  void WWW__ctor_m3203953640 (WWW_t3134621005 * __this, String_t* ___url0, WWWForm_t461342257 * ___form1, const MethodInfo* method)
{
	StringU5BU5D_t4054002952* V_0 = NULL;
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		WWWForm_t461342257 * L_0 = ___form1;
		NullCheck(L_0);
		Dictionary_2_t827649927 * L_1 = WWWForm_get_headers_m370408569(L_0, /*hidden argument*/NULL);
		StringU5BU5D_t4054002952* L_2 = WWW_FlattenedHeadersFrom_m3229619487(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = ___url0;
		WWWForm_t461342257 * L_4 = ___form1;
		NullCheck(L_4);
		ByteU5BU5D_t4260760469* L_5 = WWWForm_get_data_m2893811951(L_4, /*hidden argument*/NULL);
		StringU5BU5D_t4054002952* L_6 = V_0;
		WWW_InitWWW_m3594284248(__this, L_3, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WWW::.ctor(System.String,System.Byte[])
extern "C"  void WWW__ctor_m2485385635 (WWW_t3134621005 * __this, String_t* ___url0, ByteU5BU5D_t4260760469* ___postData1, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___url0;
		ByteU5BU5D_t4260760469* L_1 = ___postData1;
		WWW_InitWWW_m3594284248(__this, L_0, L_1, (StringU5BU5D_t4054002952*)(StringU5BU5D_t4054002952*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WWW::.ctor(System.String,System.Byte[],System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void WWW__ctor_m4133031156 (WWW_t3134621005 * __this, String_t* ___url0, ByteU5BU5D_t4260760469* ___postData1, Dictionary_2_t827649927 * ___headers2, const MethodInfo* method)
{
	StringU5BU5D_t4054002952* V_0 = NULL;
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Dictionary_2_t827649927 * L_0 = ___headers2;
		StringU5BU5D_t4054002952* L_1 = WWW_FlattenedHeadersFrom_m3229619487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = ___url0;
		ByteU5BU5D_t4260760469* L_3 = ___postData1;
		StringU5BU5D_t4054002952* L_4 = V_0;
		WWW_InitWWW_m3594284248(__this, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WWW::.ctor(System.String,UnityEngine.Hash128,System.UInt32)
extern "C"  void WWW__ctor_m649419502 (WWW_t3134621005 * __this, String_t* ___url0, Hash128_t346790303  ___hash1, uint32_t ___crc2, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___url0;
		uint32_t L_1 = ___crc2;
		WWW_INTERNAL_CALL_WWW_m2558106608(NULL /*static, unused*/, __this, L_0, (&___hash1), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WWW::Dispose()
extern "C"  void WWW_Dispose_m2446678367 (WWW_t3134621005 * __this, const MethodInfo* method)
{
	{
		WWW_DestroyWWW_m300967382(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WWW::Finalize()
extern "C"  void WWW_Finalize_m1793349504 (WWW_t3134621005 * __this, const MethodInfo* method)
{
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		WWW_DestroyWWW_m300967382(__this, (bool)0, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m3027285644(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.WWW::DestroyWWW(System.Boolean)
extern "C"  void WWW_DestroyWWW_m300967382 (WWW_t3134621005 * __this, bool ___cancel0, const MethodInfo* method)
{
	typedef void (*WWW_DestroyWWW_m300967382_ftn) (WWW_t3134621005 *, bool);
	static WWW_DestroyWWW_m300967382_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_DestroyWWW_m300967382_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::DestroyWWW(System.Boolean)");
	_il2cpp_icall_func(__this, ___cancel0);
}
// System.Void UnityEngine.WWW::InitWWW(System.String,System.Byte[],System.String[])
extern "C"  void WWW_InitWWW_m3594284248 (WWW_t3134621005 * __this, String_t* ___url0, ByteU5BU5D_t4260760469* ___postData1, StringU5BU5D_t4054002952* ___iHeaders2, const MethodInfo* method)
{
	typedef void (*WWW_InitWWW_m3594284248_ftn) (WWW_t3134621005 *, String_t*, ByteU5BU5D_t4260760469*, StringU5BU5D_t4054002952*);
	static WWW_InitWWW_m3594284248_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_InitWWW_m3594284248_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::InitWWW(System.String,System.Byte[],System.String[])");
	_il2cpp_icall_func(__this, ___url0, ___postData1, ___iHeaders2);
}
// System.String UnityEngine.WWW::EscapeURL(System.String)
extern Il2CppClass* Encoding_t2012439129_il2cpp_TypeInfo_var;
extern const uint32_t WWW_EscapeURL_m1167392721_MetadataUsageId;
extern "C"  String_t* WWW_EscapeURL_m1167392721 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWW_EscapeURL_m1167392721_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Encoding_t2012439129 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t2012439129_il2cpp_TypeInfo_var);
		Encoding_t2012439129 * L_0 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = ___s0;
		Encoding_t2012439129 * L_2 = V_0;
		String_t* L_3 = WWW_EscapeURL_m1690274784(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String UnityEngine.WWW::EscapeURL(System.String,System.Text.Encoding)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WWWTranscoder_t609724394_il2cpp_TypeInfo_var;
extern const uint32_t WWW_EscapeURL_m1690274784_MetadataUsageId;
extern "C"  String_t* WWW_EscapeURL_m1690274784 (Il2CppObject * __this /* static, unused */, String_t* ___s0, Encoding_t2012439129 * ___e1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWW_EscapeURL_m1690274784_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___s0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_0008:
	{
		String_t* L_1 = ___s0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_3 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}

IL_001e:
	{
		Encoding_t2012439129 * L_5 = ___e1;
		if (L_5)
		{
			goto IL_0026;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_0026:
	{
		String_t* L_6 = ___s0;
		Encoding_t2012439129 * L_7 = ___e1;
		IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t609724394_il2cpp_TypeInfo_var);
		String_t* L_8 = WWWTranscoder_URLEncode_m3301913818(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.String UnityEngine.WWW::UnEscapeURL(System.String)
extern Il2CppClass* Encoding_t2012439129_il2cpp_TypeInfo_var;
extern const uint32_t WWW_UnEscapeURL_m1534650378_MetadataUsageId;
extern "C"  String_t* WWW_UnEscapeURL_m1534650378 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWW_UnEscapeURL_m1534650378_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Encoding_t2012439129 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t2012439129_il2cpp_TypeInfo_var);
		Encoding_t2012439129 * L_0 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = ___s0;
		Encoding_t2012439129 * L_2 = V_0;
		String_t* L_3 = WWW_UnEscapeURL_m262353223(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String UnityEngine.WWW::UnEscapeURL(System.String,System.Text.Encoding)
extern Il2CppClass* WWWTranscoder_t609724394_il2cpp_TypeInfo_var;
extern const uint32_t WWW_UnEscapeURL_m262353223_MetadataUsageId;
extern "C"  String_t* WWW_UnEscapeURL_m262353223 (Il2CppObject * __this /* static, unused */, String_t* ___s0, Encoding_t2012439129 * ___e1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWW_UnEscapeURL_m262353223_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___s0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_0008:
	{
		String_t* L_1 = ___s0;
		NullCheck(L_1);
		int32_t L_2 = String_IndexOf_m2775210486(L_1, ((int32_t)37), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_0026;
		}
	}
	{
		String_t* L_3 = ___s0;
		NullCheck(L_3);
		int32_t L_4 = String_IndexOf_m2775210486(L_3, ((int32_t)43), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_0026;
		}
	}
	{
		String_t* L_5 = ___s0;
		return L_5;
	}

IL_0026:
	{
		String_t* L_6 = ___s0;
		Encoding_t2012439129 * L_7 = ___e1;
		IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t609724394_il2cpp_TypeInfo_var);
		String_t* L_8 = WWWTranscoder_URLDecode_m3418913970(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.WWW::get_responseHeaders()
extern Il2CppClass* UnityException_t3473321374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral331141550;
extern const uint32_t WWW_get_responseHeaders_m2488150044_MetadataUsageId;
extern "C"  Dictionary_2_t827649927 * WWW_get_responseHeaders_m2488150044 (WWW_t3134621005 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWW_get_responseHeaders_m2488150044_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = WWW_get_isDone_m634060017(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		UnityException_t3473321374 * L_1 = (UnityException_t3473321374 *)il2cpp_codegen_object_new(UnityException_t3473321374_il2cpp_TypeInfo_var);
		UnityException__ctor_m743662351(L_1, _stringLiteral331141550, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		String_t* L_2 = WWW_get_responseHeadersString_m2464460368(__this, /*hidden argument*/NULL);
		Dictionary_2_t827649927 * L_3 = WWW_ParseHTTPHeaderString_m3695887721(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String UnityEngine.WWW::get_responseHeadersString()
extern "C"  String_t* WWW_get_responseHeadersString_m2464460368 (WWW_t3134621005 * __this, const MethodInfo* method)
{
	typedef String_t* (*WWW_get_responseHeadersString_m2464460368_ftn) (WWW_t3134621005 *);
	static WWW_get_responseHeadersString_m2464460368_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_get_responseHeadersString_m2464460368_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::get_responseHeadersString()");
	return _il2cpp_icall_func(__this);
}
// System.String UnityEngine.WWW::get_text()
extern Il2CppClass* UnityException_t3473321374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral962042027;
extern const uint32_t WWW_get_text_m4216049525_MetadataUsageId;
extern "C"  String_t* WWW_get_text_m4216049525 (WWW_t3134621005 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWW_get_text_m4216049525_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t4260760469* V_0 = NULL;
	{
		bool L_0 = WWW_get_isDone_m634060017(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		UnityException_t3473321374 * L_1 = (UnityException_t3473321374 *)il2cpp_codegen_object_new(UnityException_t3473321374_il2cpp_TypeInfo_var);
		UnityException__ctor_m743662351(L_1, _stringLiteral962042027, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		ByteU5BU5D_t4260760469* L_2 = WWW_get_bytes_m2080623436(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		Encoding_t2012439129 * L_3 = WWW_GetTextEncoder_m1656865633(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t4260760469* L_4 = V_0;
		ByteU5BU5D_t4260760469* L_5 = V_0;
		NullCheck(L_5);
		NullCheck(L_3);
		String_t* L_6 = VirtFuncInvoker3< String_t*, ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(21 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_3, L_4, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length)))));
		return L_6;
	}
}
// System.Text.Encoding UnityEngine.WWW::get_DefaultEncoding()
extern Il2CppClass* Encoding_t2012439129_il2cpp_TypeInfo_var;
extern const uint32_t WWW_get_DefaultEncoding_m2507364293_MetadataUsageId;
extern "C"  Encoding_t2012439129 * WWW_get_DefaultEncoding_m2507364293 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWW_get_DefaultEncoding_m2507364293_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t2012439129_il2cpp_TypeInfo_var);
		Encoding_t2012439129 * L_0 = Encoding_get_ASCII_m1425378925(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Text.Encoding UnityEngine.WWW::GetTextEncoder()
extern Il2CppClass* CharU5BU5D_t3324145743_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t2012439129_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m2295372330_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3132504110;
extern Il2CppCodeGenString* _stringLiteral739074380;
extern Il2CppCodeGenString* _stringLiteral3424733699;
extern Il2CppCodeGenString* _stringLiteral39;
extern const uint32_t WWW_GetTextEncoder_m1656865633_MetadataUsageId;
extern "C"  Encoding_t2012439129 * WWW_GetTextEncoder_m1656865633 (WWW_t3134621005 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWW_GetTextEncoder_m1656865633_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	int32_t V_4 = 0;
	Encoding_t2012439129 * V_5 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (String_t*)NULL;
		Dictionary_2_t827649927 * L_0 = WWW_get_responseHeaders_m2488150044(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = Dictionary_2_TryGetValue_m2295372330(L_0, _stringLiteral3132504110, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m2295372330_MethodInfo_var);
		if (!L_1)
		{
			goto IL_00b0;
		}
	}
	{
		String_t* L_2 = V_0;
		NullCheck(L_2);
		int32_t L_3 = String_IndexOf_m864002126(L_2, _stringLiteral739074380, 5, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) <= ((int32_t)(-1))))
		{
			goto IL_00b0;
		}
	}
	{
		String_t* L_5 = V_0;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		int32_t L_7 = String_IndexOf_m204546721(L_5, ((int32_t)61), L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		int32_t L_8 = V_2;
		if ((((int32_t)L_8) <= ((int32_t)(-1))))
		{
			goto IL_00b0;
		}
	}
	{
		String_t* L_9 = V_0;
		int32_t L_10 = V_2;
		NullCheck(L_9);
		String_t* L_11 = String_Substring_m2809233063(L_9, ((int32_t)((int32_t)L_10+(int32_t)1)), /*hidden argument*/NULL);
		NullCheck(L_11);
		String_t* L_12 = String_Trim_m1030489823(L_11, /*hidden argument*/NULL);
		CharU5BU5D_t3324145743* L_13 = ((CharU5BU5D_t3324145743*)SZArrayNew(CharU5BU5D_t3324145743_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 0);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)39));
		CharU5BU5D_t3324145743* L_14 = L_13;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 1);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppChar)((int32_t)34));
		NullCheck(L_12);
		String_t* L_15 = String_Trim_m1469603388(L_12, L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		String_t* L_16 = String_Trim_m1030489823(L_15, /*hidden argument*/NULL);
		V_3 = L_16;
		String_t* L_17 = V_3;
		NullCheck(L_17);
		int32_t L_18 = String_IndexOf_m2775210486(L_17, ((int32_t)59), /*hidden argument*/NULL);
		V_4 = L_18;
		int32_t L_19 = V_4;
		if ((((int32_t)L_19) <= ((int32_t)(-1))))
		{
			goto IL_0083;
		}
	}
	{
		String_t* L_20 = V_3;
		int32_t L_21 = V_4;
		NullCheck(L_20);
		String_t* L_22 = String_Substring_m675079568(L_20, 0, L_21, /*hidden argument*/NULL);
		V_3 = L_22;
	}

IL_0083:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_23 = V_3;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t2012439129_il2cpp_TypeInfo_var);
			Encoding_t2012439129 * L_24 = Encoding_GetEncoding_m4050696948(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
			V_5 = L_24;
			goto IL_00b6;
		}

IL_0090:
		{
			; // IL_0090: leave IL_00b0
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0095;
		throw e;
	}

CATCH_0095:
	{ // begin catch(System.Exception)
		String_t* L_25 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral3424733699, L_25, _stringLiteral39, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		goto IL_00b0;
	} // end catch (depth: 1)

IL_00b0:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t2012439129_il2cpp_TypeInfo_var);
		Encoding_t2012439129 * L_27 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_27;
	}

IL_00b6:
	{
		Encoding_t2012439129 * L_28 = V_5;
		return L_28;
	}
}
// System.Byte[] UnityEngine.WWW::get_bytes()
extern "C"  ByteU5BU5D_t4260760469* WWW_get_bytes_m2080623436 (WWW_t3134621005 * __this, const MethodInfo* method)
{
	typedef ByteU5BU5D_t4260760469* (*WWW_get_bytes_m2080623436_ftn) (WWW_t3134621005 *);
	static WWW_get_bytes_m2080623436_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_get_bytes_m2080623436_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::get_bytes()");
	return _il2cpp_icall_func(__this);
}
// System.String UnityEngine.WWW::get_error()
extern "C"  String_t* WWW_get_error_m1787423074 (WWW_t3134621005 * __this, const MethodInfo* method)
{
	typedef String_t* (*WWW_get_error_m1787423074_ftn) (WWW_t3134621005 *);
	static WWW_get_error_m1787423074_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_get_error_m1787423074_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::get_error()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Texture2D UnityEngine.WWW::GetTexture(System.Boolean)
extern "C"  Texture2D_t3884108195 * WWW_GetTexture_m1996665057 (WWW_t3134621005 * __this, bool ___markNonReadable0, const MethodInfo* method)
{
	typedef Texture2D_t3884108195 * (*WWW_GetTexture_m1996665057_ftn) (WWW_t3134621005 *, bool);
	static WWW_GetTexture_m1996665057_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_GetTexture_m1996665057_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::GetTexture(System.Boolean)");
	return _il2cpp_icall_func(__this, ___markNonReadable0);
}
// UnityEngine.Texture2D UnityEngine.WWW::get_texture()
extern "C"  Texture2D_t3884108195 * WWW_get_texture_m2854732303 (WWW_t3134621005 * __this, const MethodInfo* method)
{
	{
		Texture2D_t3884108195 * L_0 = WWW_GetTexture_m1996665057(__this, (bool)0, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.AudioClip UnityEngine.WWW::GetAudioClip(System.Boolean)
extern "C"  AudioClip_t794140988 * WWW_GetAudioClip_m163276389 (WWW_t3134621005 * __this, bool ___threeD0, const MethodInfo* method)
{
	{
		bool L_0 = ___threeD0;
		AudioClip_t794140988 * L_1 = WWW_GetAudioClip_m286832248(__this, L_0, (bool)0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.AudioClip UnityEngine.WWW::GetAudioClip(System.Boolean,System.Boolean)
extern "C"  AudioClip_t794140988 * WWW_GetAudioClip_m286832248 (WWW_t3134621005 * __this, bool ___threeD0, bool ___stream1, const MethodInfo* method)
{
	{
		bool L_0 = ___threeD0;
		bool L_1 = ___stream1;
		AudioClip_t794140988 * L_2 = WWW_GetAudioClip_m1447938731(__this, L_0, L_1, 0, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.AudioClip UnityEngine.WWW::GetAudioClip(System.Boolean,System.Boolean,UnityEngine.AudioType)
extern "C"  AudioClip_t794140988 * WWW_GetAudioClip_m1447938731 (WWW_t3134621005 * __this, bool ___threeD0, bool ___stream1, int32_t ___audioType2, const MethodInfo* method)
{
	{
		bool L_0 = ___threeD0;
		bool L_1 = ___stream1;
		int32_t L_2 = ___audioType2;
		AudioClip_t794140988 * L_3 = WWW_GetAudioClipInternal_m850700757(__this, L_0, L_1, (bool)0, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.AudioClip UnityEngine.WWW::GetAudioClipInternal(System.Boolean,System.Boolean,System.Boolean,UnityEngine.AudioType)
extern "C"  AudioClip_t794140988 * WWW_GetAudioClipInternal_m850700757 (WWW_t3134621005 * __this, bool ___threeD0, bool ___stream1, bool ___compressed2, int32_t ___audioType3, const MethodInfo* method)
{
	typedef AudioClip_t794140988 * (*WWW_GetAudioClipInternal_m850700757_ftn) (WWW_t3134621005 *, bool, bool, bool, int32_t);
	static WWW_GetAudioClipInternal_m850700757_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_GetAudioClipInternal_m850700757_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::GetAudioClipInternal(System.Boolean,System.Boolean,System.Boolean,UnityEngine.AudioType)");
	return _il2cpp_icall_func(__this, ___threeD0, ___stream1, ___compressed2, ___audioType3);
}
// System.Void UnityEngine.WWW::LoadImageIntoTexture(UnityEngine.Texture2D)
extern "C"  void WWW_LoadImageIntoTexture_m1974117364 (WWW_t3134621005 * __this, Texture2D_t3884108195 * ___tex0, const MethodInfo* method)
{
	typedef void (*WWW_LoadImageIntoTexture_m1974117364_ftn) (WWW_t3134621005 *, Texture2D_t3884108195 *);
	static WWW_LoadImageIntoTexture_m1974117364_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_LoadImageIntoTexture_m1974117364_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::LoadImageIntoTexture(UnityEngine.Texture2D)");
	_il2cpp_icall_func(__this, ___tex0);
}
// System.Boolean UnityEngine.WWW::get_isDone()
extern "C"  bool WWW_get_isDone_m634060017 (WWW_t3134621005 * __this, const MethodInfo* method)
{
	typedef bool (*WWW_get_isDone_m634060017_ftn) (WWW_t3134621005 *);
	static WWW_get_isDone_m634060017_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_get_isDone_m634060017_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::get_isDone()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.WWW::get_progress()
extern "C"  float WWW_get_progress_m3186358572 (WWW_t3134621005 * __this, const MethodInfo* method)
{
	typedef float (*WWW_get_progress_m3186358572_ftn) (WWW_t3134621005 *);
	static WWW_get_progress_m3186358572_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_get_progress_m3186358572_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::get_progress()");
	return _il2cpp_icall_func(__this);
}
// System.String UnityEngine.WWW::get_url()
extern "C"  String_t* WWW_get_url_m4155171145 (WWW_t3134621005 * __this, const MethodInfo* method)
{
	typedef String_t* (*WWW_get_url_m4155171145_ftn) (WWW_t3134621005 *);
	static WWW_get_url_m4155171145_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_get_url_m4155171145_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::get_url()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.AssetBundle UnityEngine.WWW::get_assetBundle()
extern "C"  AssetBundle_t2070959688 * WWW_get_assetBundle_m2674678273 (WWW_t3134621005 * __this, const MethodInfo* method)
{
	typedef AssetBundle_t2070959688 * (*WWW_get_assetBundle_m2674678273_ftn) (WWW_t3134621005 *);
	static WWW_get_assetBundle_m2674678273_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_get_assetBundle_m2674678273_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::get_assetBundle()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.WWW::INTERNAL_CALL_WWW(UnityEngine.WWW,System.String,UnityEngine.Hash128&,System.UInt32)
extern "C"  void WWW_INTERNAL_CALL_WWW_m2558106608 (Il2CppObject * __this /* static, unused */, WWW_t3134621005 * ___self0, String_t* ___url1, Hash128_t346790303 * ___hash2, uint32_t ___crc3, const MethodInfo* method)
{
	typedef void (*WWW_INTERNAL_CALL_WWW_m2558106608_ftn) (WWW_t3134621005 *, String_t*, Hash128_t346790303 *, uint32_t);
	static WWW_INTERNAL_CALL_WWW_m2558106608_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_INTERNAL_CALL_WWW_m2558106608_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::INTERNAL_CALL_WWW(UnityEngine.WWW,System.String,UnityEngine.Hash128&,System.UInt32)");
	_il2cpp_icall_func(___self0, ___url1, ___hash2, ___crc3);
}
// UnityEngine.WWW UnityEngine.WWW::LoadFromCacheOrDownload(System.String,System.Int32)
extern "C"  WWW_t3134621005 * WWW_LoadFromCacheOrDownload_m197364101 (Il2CppObject * __this /* static, unused */, String_t* ___url0, int32_t ___version1, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	{
		V_0 = 0;
		String_t* L_0 = ___url0;
		int32_t L_1 = ___version1;
		uint32_t L_2 = V_0;
		WWW_t3134621005 * L_3 = WWW_LoadFromCacheOrDownload_m101644025(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.WWW UnityEngine.WWW::LoadFromCacheOrDownload(System.String,System.Int32,System.UInt32)
extern "C"  WWW_t3134621005 * WWW_LoadFromCacheOrDownload_m101644025 (Il2CppObject * __this /* static, unused */, String_t* ___url0, int32_t ___version1, uint32_t ___crc2, const MethodInfo* method)
{
	Hash128_t346790303  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___version1;
		Hash128__ctor_m425960966((&V_0), 0, 0, 0, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___url0;
		Hash128_t346790303  L_2 = V_0;
		uint32_t L_3 = ___crc2;
		WWW_t3134621005 * L_4 = WWW_LoadFromCacheOrDownload_m2606851680(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.WWW UnityEngine.WWW::LoadFromCacheOrDownload(System.String,UnityEngine.Hash128,System.UInt32)
extern Il2CppClass* WWW_t3134621005_il2cpp_TypeInfo_var;
extern const uint32_t WWW_LoadFromCacheOrDownload_m2606851680_MetadataUsageId;
extern "C"  WWW_t3134621005 * WWW_LoadFromCacheOrDownload_m2606851680 (Il2CppObject * __this /* static, unused */, String_t* ___url0, Hash128_t346790303  ___hash1, uint32_t ___crc2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWW_LoadFromCacheOrDownload_m2606851680_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___url0;
		Hash128_t346790303  L_1 = ___hash1;
		uint32_t L_2 = ___crc2;
		WWW_t3134621005 * L_3 = (WWW_t3134621005 *)il2cpp_codegen_object_new(WWW_t3134621005_il2cpp_TypeInfo_var);
		WWW__ctor_m649419502(L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String[] UnityEngine.WWW::FlattenedHeadersFrom(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t2144973319_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Count_m2614934137_MethodInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m2759194411_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2871721525_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1739472607_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m730091314_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2577713898_MethodInfo_var;
extern const uint32_t WWW_FlattenedHeadersFrom_m3229619487_MetadataUsageId;
extern "C"  StringU5BU5D_t4054002952* WWW_FlattenedHeadersFrom_m3229619487 (Il2CppObject * __this /* static, unused */, Dictionary_2_t827649927 * ___headers0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWW_FlattenedHeadersFrom_m3229619487_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t4054002952* V_0 = NULL;
	int32_t V_1 = 0;
	KeyValuePair_2_t726430633  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Enumerator_t2144973319  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t827649927 * L_0 = ___headers0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (StringU5BU5D_t4054002952*)NULL;
	}

IL_0008:
	{
		Dictionary_2_t827649927 * L_1 = ___headers0;
		NullCheck(L_1);
		int32_t L_2 = Dictionary_2_get_Count_m2614934137(L_1, /*hidden argument*/Dictionary_2_get_Count_m2614934137_MethodInfo_var);
		V_0 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)L_2*(int32_t)2))));
		V_1 = 0;
		Dictionary_2_t827649927 * L_3 = ___headers0;
		NullCheck(L_3);
		Enumerator_t2144973319  L_4 = Dictionary_2_GetEnumerator_m2759194411(L_3, /*hidden argument*/Dictionary_2_GetEnumerator_m2759194411_MethodInfo_var);
		V_3 = L_4;
	}

IL_001f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0052;
		}

IL_0024:
		{
			KeyValuePair_2_t726430633  L_5 = Enumerator_get_Current_m2871721525((&V_3), /*hidden argument*/Enumerator_get_Current_m2871721525_MethodInfo_var);
			V_2 = L_5;
			StringU5BU5D_t4054002952* L_6 = V_0;
			int32_t L_7 = V_1;
			int32_t L_8 = L_7;
			V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
			String_t* L_9 = KeyValuePair_2_get_Key_m1739472607((&V_2), /*hidden argument*/KeyValuePair_2_get_Key_m1739472607_MethodInfo_var);
			NullCheck(L_9);
			String_t* L_10 = String_ToString_m1382284457(L_9, /*hidden argument*/NULL);
			NullCheck(L_6);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_8);
			ArrayElementTypeCheck (L_6, L_10);
			(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (String_t*)L_10);
			StringU5BU5D_t4054002952* L_11 = V_0;
			int32_t L_12 = V_1;
			int32_t L_13 = L_12;
			V_1 = ((int32_t)((int32_t)L_13+(int32_t)1));
			String_t* L_14 = KeyValuePair_2_get_Value_m730091314((&V_2), /*hidden argument*/KeyValuePair_2_get_Value_m730091314_MethodInfo_var);
			NullCheck(L_14);
			String_t* L_15 = String_ToString_m1382284457(L_14, /*hidden argument*/NULL);
			NullCheck(L_11);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_13);
			ArrayElementTypeCheck (L_11, L_15);
			(L_11)->SetAt(static_cast<il2cpp_array_size_t>(L_13), (String_t*)L_15);
		}

IL_0052:
		{
			bool L_16 = Enumerator_MoveNext_m2577713898((&V_3), /*hidden argument*/Enumerator_MoveNext_m2577713898_MethodInfo_var);
			if (L_16)
			{
				goto IL_0024;
			}
		}

IL_005e:
		{
			IL2CPP_LEAVE(0x6F, FINALLY_0063);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0063;
	}

FINALLY_0063:
	{ // begin finally (depth: 1)
		Enumerator_t2144973319  L_17 = V_3;
		Enumerator_t2144973319  L_18 = L_17;
		Il2CppObject * L_19 = Box(Enumerator_t2144973319_il2cpp_TypeInfo_var, &L_18);
		NullCheck((Il2CppObject *)L_19);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_19);
		IL2CPP_END_FINALLY(99)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(99)
	{
		IL2CPP_JUMP_TBL(0x6F, IL_006f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_006f:
	{
		StringU5BU5D_t4054002952* L_20 = V_0;
		return L_20;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.WWW::ParseHTTPHeaderString(System.String)
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppClass* StringComparer_t4230573202_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t827649927_il2cpp_TypeInfo_var;
extern Il2CppClass* StringReader_t4061477668_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m495871664_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m439360998_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3329490176;
extern Il2CppCodeGenString* _stringLiteral2228360;
extern Il2CppCodeGenString* _stringLiteral2455815154;
extern Il2CppCodeGenString* _stringLiteral1830;
extern const uint32_t WWW_ParseHTTPHeaderString_m3695887721_MetadataUsageId;
extern "C"  Dictionary_2_t827649927 * WWW_ParseHTTPHeaderString_m3695887721 (Il2CppObject * __this /* static, unused */, String_t* ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWW_ParseHTTPHeaderString_m3695887721_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t827649927 * V_0 = NULL;
	StringReader_t4061477668 * V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	int32_t V_4 = 0;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	{
		String_t* L_0 = ___input0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentException_t928607144 * L_1 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_1, _stringLiteral3329490176, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringComparer_t4230573202_il2cpp_TypeInfo_var);
		StringComparer_t4230573202 * L_2 = StringComparer_get_OrdinalIgnoreCase_m2513153269(NULL /*static, unused*/, /*hidden argument*/NULL);
		Dictionary_2_t827649927 * L_3 = (Dictionary_2_t827649927 *)il2cpp_codegen_object_new(Dictionary_2_t827649927_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m495871664(L_3, L_2, /*hidden argument*/Dictionary_2__ctor_m495871664_MethodInfo_var);
		V_0 = L_3;
		String_t* L_4 = ___input0;
		StringReader_t4061477668 * L_5 = (StringReader_t4061477668 *)il2cpp_codegen_object_new(StringReader_t4061477668_il2cpp_TypeInfo_var);
		StringReader__ctor_m1181104909(L_5, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		V_2 = 0;
	}

IL_0025:
	{
		StringReader_t4061477668 * L_6 = V_1;
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(12 /* System.String System.IO.StringReader::ReadLine() */, L_6);
		V_3 = L_7;
		String_t* L_8 = V_3;
		if (L_8)
		{
			goto IL_0037;
		}
	}
	{
		goto IL_00a7;
	}

IL_0037:
	{
		int32_t L_9 = V_2;
		int32_t L_10 = L_9;
		V_2 = ((int32_t)((int32_t)L_10+(int32_t)1));
		if (L_10)
		{
			goto IL_0062;
		}
	}
	{
		String_t* L_11 = V_3;
		NullCheck(L_11);
		bool L_12 = String_StartsWith_m1500793453(L_11, _stringLiteral2228360, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0062;
		}
	}
	{
		Dictionary_2_t827649927 * L_13 = V_0;
		String_t* L_14 = V_3;
		NullCheck(L_13);
		Dictionary_2_set_Item_m439360998(L_13, _stringLiteral2455815154, L_14, /*hidden argument*/Dictionary_2_set_Item_m439360998_MethodInfo_var);
		goto IL_0025;
	}

IL_0062:
	{
		String_t* L_15 = V_3;
		NullCheck(L_15);
		int32_t L_16 = String_IndexOf_m1476794331(L_15, _stringLiteral1830, /*hidden argument*/NULL);
		V_4 = L_16;
		int32_t L_17 = V_4;
		if ((!(((uint32_t)L_17) == ((uint32_t)(-1)))))
		{
			goto IL_007c;
		}
	}
	{
		goto IL_0025;
	}

IL_007c:
	{
		String_t* L_18 = V_3;
		int32_t L_19 = V_4;
		NullCheck(L_18);
		String_t* L_20 = String_Substring_m675079568(L_18, 0, L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		String_t* L_21 = String_ToUpper_m1841663596(L_20, /*hidden argument*/NULL);
		V_5 = L_21;
		String_t* L_22 = V_3;
		int32_t L_23 = V_4;
		NullCheck(L_22);
		String_t* L_24 = String_Substring_m2809233063(L_22, ((int32_t)((int32_t)L_23+(int32_t)2)), /*hidden argument*/NULL);
		V_6 = L_24;
		Dictionary_2_t827649927 * L_25 = V_0;
		String_t* L_26 = V_5;
		String_t* L_27 = V_6;
		NullCheck(L_25);
		Dictionary_2_set_Item_m439360998(L_25, L_26, L_27, /*hidden argument*/Dictionary_2_set_Item_m439360998_MethodInfo_var);
		goto IL_0025;
	}

IL_00a7:
	{
		Dictionary_2_t827649927 * L_28 = V_0;
		return L_28;
	}
}
// System.Void UnityEngine.WWWForm::.ctor()
extern Il2CppClass* List_1_t1333978725_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1375417109_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2342623121_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m459821414_MethodInfo_var;
extern const uint32_t WWWForm__ctor_m1417930174_MetadataUsageId;
extern "C"  void WWWForm__ctor_m1417930174 (WWWForm_t461342257 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWWForm__ctor_m1417930174_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		List_1_t1333978725 * L_0 = (List_1_t1333978725 *)il2cpp_codegen_object_new(List_1_t1333978725_il2cpp_TypeInfo_var);
		List_1__ctor_m2342623121(L_0, /*hidden argument*/List_1__ctor_m2342623121_MethodInfo_var);
		__this->set_formData_0(L_0);
		List_1_t1375417109 * L_1 = (List_1_t1375417109 *)il2cpp_codegen_object_new(List_1_t1375417109_il2cpp_TypeInfo_var);
		List_1__ctor_m459821414(L_1, /*hidden argument*/List_1__ctor_m459821414_MethodInfo_var);
		__this->set_fieldNames_1(L_1);
		List_1_t1375417109 * L_2 = (List_1_t1375417109 *)il2cpp_codegen_object_new(List_1_t1375417109_il2cpp_TypeInfo_var);
		List_1__ctor_m459821414(L_2, /*hidden argument*/List_1__ctor_m459821414_MethodInfo_var);
		__this->set_fileNames_2(L_2);
		List_1_t1375417109 * L_3 = (List_1_t1375417109 *)il2cpp_codegen_object_new(List_1_t1375417109_il2cpp_TypeInfo_var);
		List_1__ctor_m459821414(L_3, /*hidden argument*/List_1__ctor_m459821414_MethodInfo_var);
		__this->set_types_3(L_3);
		__this->set_boundary_4(((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)((int32_t)40))));
		V_0 = 0;
		goto IL_0076;
	}

IL_0046:
	{
		int32_t L_4 = Random_Range_m75452833(NULL /*static, unused*/, ((int32_t)48), ((int32_t)110), /*hidden argument*/NULL);
		V_1 = L_4;
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) <= ((int32_t)((int32_t)57))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_6 = V_1;
		V_1 = ((int32_t)((int32_t)L_6+(int32_t)7));
	}

IL_005c:
	{
		int32_t L_7 = V_1;
		if ((((int32_t)L_7) <= ((int32_t)((int32_t)90))))
		{
			goto IL_0068;
		}
	}
	{
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)6));
	}

IL_0068:
	{
		ByteU5BU5D_t4260760469* L_9 = __this->get_boundary_4();
		int32_t L_10 = V_0;
		int32_t L_11 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (uint8_t)(((int32_t)((uint8_t)L_11))));
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0076:
	{
		int32_t L_13 = V_0;
		if ((((int32_t)L_13) < ((int32_t)((int32_t)40))))
		{
			goto IL_0046;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.WWWForm::AddField(System.String,System.String)
extern Il2CppClass* Encoding_t2012439129_il2cpp_TypeInfo_var;
extern const uint32_t WWWForm_AddField_m2890504319_MetadataUsageId;
extern "C"  void WWWForm_AddField_m2890504319 (WWWForm_t461342257 * __this, String_t* ___fieldName0, String_t* ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWWForm_AddField_m2890504319_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Encoding_t2012439129 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t2012439129_il2cpp_TypeInfo_var);
		Encoding_t2012439129 * L_0 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = ___fieldName0;
		String_t* L_2 = ___value1;
		Encoding_t2012439129 * L_3 = V_0;
		WWWForm_AddField_m3426667506(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WWWForm::AddField(System.String,System.String,System.Text.Encoding)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m4975193_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2036983937_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3907812548;
extern Il2CppCodeGenString* _stringLiteral34;
extern const uint32_t WWWForm_AddField_m3426667506_MetadataUsageId;
extern "C"  void WWWForm_AddField_m3426667506 (WWWForm_t461342257 * __this, String_t* ___fieldName0, String_t* ___value1, Encoding_t2012439129 * ___e2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWWForm_AddField_m3426667506_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1375417109 * L_0 = __this->get_fieldNames_1();
		String_t* L_1 = ___fieldName0;
		NullCheck(L_0);
		List_1_Add_m4975193(L_0, L_1, /*hidden argument*/List_1_Add_m4975193_MethodInfo_var);
		List_1_t1375417109 * L_2 = __this->get_fileNames_2();
		NullCheck(L_2);
		List_1_Add_m4975193(L_2, (String_t*)NULL, /*hidden argument*/List_1_Add_m4975193_MethodInfo_var);
		List_1_t1333978725 * L_3 = __this->get_formData_0();
		Encoding_t2012439129 * L_4 = ___e2;
		String_t* L_5 = ___value1;
		NullCheck(L_4);
		ByteU5BU5D_t4260760469* L_6 = VirtFuncInvoker1< ByteU5BU5D_t4260760469*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_4, L_5);
		NullCheck(L_3);
		List_1_Add_m2036983937(L_3, L_6, /*hidden argument*/List_1_Add_m2036983937_MethodInfo_var);
		List_1_t1375417109 * L_7 = __this->get_types_3();
		Encoding_t2012439129 * L_8 = ___e2;
		NullCheck(L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(24 /* System.String System.Text.Encoding::get_WebName() */, L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral3907812548, L_9, _stringLiteral34, /*hidden argument*/NULL);
		NullCheck(L_7);
		List_1_Add_m4975193(L_7, L_10, /*hidden argument*/List_1_Add_m4975193_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.WWWForm::AddBinaryData(System.String,System.Byte[],System.String)
extern "C"  void WWWForm_AddBinaryData_m3460911449 (WWWForm_t461342257 * __this, String_t* ___fieldName0, ByteU5BU5D_t4260760469* ___contents1, String_t* ___fileName2, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		V_0 = (String_t*)NULL;
		String_t* L_0 = ___fieldName0;
		ByteU5BU5D_t4260760469* L_1 = ___contents1;
		String_t* L_2 = ___fileName2;
		String_t* L_3 = V_0;
		WWWForm_AddBinaryData_m1424533973(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WWWForm::AddBinaryData(System.String,System.Byte[],System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m4975193_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2036983937_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1481531;
extern Il2CppCodeGenString* _stringLiteral1469609;
extern Il2CppCodeGenString* _stringLiteral3415708533;
extern Il2CppCodeGenString* _stringLiteral1178484637;
extern const uint32_t WWWForm_AddBinaryData_m1424533973_MetadataUsageId;
extern "C"  void WWWForm_AddBinaryData_m1424533973 (WWWForm_t461342257 * __this, String_t* ___fieldName0, ByteU5BU5D_t4260760469* ___contents1, String_t* ___fileName2, String_t* ___mimeType3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWWForm_AddBinaryData_m1424533973_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t G_B10_0 = 0;
	String_t* G_B13_0 = NULL;
	String_t* G_B12_0 = NULL;
	String_t* G_B14_0 = NULL;
	String_t* G_B14_1 = NULL;
	{
		__this->set_containsFiles_5((bool)1);
		ByteU5BU5D_t4260760469* L_0 = ___contents1;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) <= ((int32_t)8)))
		{
			goto IL_0062;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_1 = ___contents1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		int32_t L_2 = 0;
		uint8_t L_3 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)137)))))
		{
			goto IL_0062;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_4 = ___contents1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		int32_t L_5 = 1;
		uint8_t L_6 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)80)))))
		{
			goto IL_0062;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_7 = ___contents1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		int32_t L_8 = 2;
		uint8_t L_9 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)78)))))
		{
			goto IL_0062;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_10 = ___contents1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 3);
		int32_t L_11 = 3;
		uint8_t L_12 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		if ((!(((uint32_t)L_12) == ((uint32_t)((int32_t)71)))))
		{
			goto IL_0062;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_13 = ___contents1;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		int32_t L_14 = 4;
		uint8_t L_15 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_0062;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_16 = ___contents1;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 5);
		int32_t L_17 = 5;
		uint8_t L_18 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		if ((!(((uint32_t)L_18) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0062;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_19 = ___contents1;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 6);
		int32_t L_20 = 6;
		uint8_t L_21 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		if ((!(((uint32_t)L_21) == ((uint32_t)((int32_t)26)))))
		{
			goto IL_0062;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_22 = ___contents1;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 7);
		int32_t L_23 = 7;
		uint8_t L_24 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		G_B10_0 = ((((int32_t)L_24) == ((int32_t)((int32_t)10)))? 1 : 0);
		goto IL_0063;
	}

IL_0062:
	{
		G_B10_0 = 0;
	}

IL_0063:
	{
		V_0 = (bool)G_B10_0;
		String_t* L_25 = ___fileName2;
		if (L_25)
		{
			goto IL_0087;
		}
	}
	{
		String_t* L_26 = ___fieldName0;
		bool L_27 = V_0;
		G_B12_0 = L_26;
		if (!L_27)
		{
			G_B13_0 = L_26;
			goto IL_007b;
		}
	}
	{
		G_B14_0 = _stringLiteral1481531;
		G_B14_1 = G_B12_0;
		goto IL_0080;
	}

IL_007b:
	{
		G_B14_0 = _stringLiteral1469609;
		G_B14_1 = G_B13_0;
	}

IL_0080:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_28 = String_Concat_m138640077(NULL /*static, unused*/, G_B14_1, G_B14_0, /*hidden argument*/NULL);
		___fileName2 = L_28;
	}

IL_0087:
	{
		String_t* L_29 = ___mimeType3;
		if (L_29)
		{
			goto IL_00a7;
		}
	}
	{
		bool L_30 = V_0;
		if (!L_30)
		{
			goto IL_00a0;
		}
	}
	{
		___mimeType3 = _stringLiteral3415708533;
		goto IL_00a7;
	}

IL_00a0:
	{
		___mimeType3 = _stringLiteral1178484637;
	}

IL_00a7:
	{
		List_1_t1375417109 * L_31 = __this->get_fieldNames_1();
		String_t* L_32 = ___fieldName0;
		NullCheck(L_31);
		List_1_Add_m4975193(L_31, L_32, /*hidden argument*/List_1_Add_m4975193_MethodInfo_var);
		List_1_t1375417109 * L_33 = __this->get_fileNames_2();
		String_t* L_34 = ___fileName2;
		NullCheck(L_33);
		List_1_Add_m4975193(L_33, L_34, /*hidden argument*/List_1_Add_m4975193_MethodInfo_var);
		List_1_t1333978725 * L_35 = __this->get_formData_0();
		ByteU5BU5D_t4260760469* L_36 = ___contents1;
		NullCheck(L_35);
		List_1_Add_m2036983937(L_35, L_36, /*hidden argument*/List_1_Add_m2036983937_MethodInfo_var);
		List_1_t1375417109 * L_37 = __this->get_types_3();
		String_t* L_38 = ___mimeType3;
		NullCheck(L_37);
		List_1_Add_m4975193(L_37, L_38, /*hidden argument*/List_1_Add_m4975193_MethodInfo_var);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.WWWForm::get_headers()
extern Il2CppClass* Dictionary_2_t827649927_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t2012439129_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m640701813_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m439360998_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral949037134;
extern Il2CppCodeGenString* _stringLiteral2489289636;
extern Il2CppCodeGenString* _stringLiteral34;
extern Il2CppCodeGenString* _stringLiteral2809397470;
extern const uint32_t WWWForm_get_headers_m370408569_MetadataUsageId;
extern "C"  Dictionary_2_t827649927 * WWWForm_get_headers_m370408569 (WWWForm_t461342257 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWWForm_get_headers_m370408569_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t827649927 * V_0 = NULL;
	{
		Dictionary_2_t827649927 * L_0 = (Dictionary_2_t827649927 *)il2cpp_codegen_object_new(Dictionary_2_t827649927_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m640701813(L_0, /*hidden argument*/Dictionary_2__ctor_m640701813_MethodInfo_var);
		V_0 = L_0;
		bool L_1 = __this->get_containsFiles_5();
		if (!L_1)
		{
			goto IL_0049;
		}
	}
	{
		Dictionary_2_t827649927 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t2012439129_il2cpp_TypeInfo_var);
		Encoding_t2012439129 * L_3 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t4260760469* L_4 = __this->get_boundary_4();
		ByteU5BU5D_t4260760469* L_5 = __this->get_boundary_4();
		NullCheck(L_5);
		NullCheck(L_3);
		String_t* L_6 = VirtFuncInvoker3< String_t*, ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(21 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_3, L_4, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length)))));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2489289636, L_6, _stringLiteral34, /*hidden argument*/NULL);
		NullCheck(L_2);
		Dictionary_2_set_Item_m439360998(L_2, _stringLiteral949037134, L_7, /*hidden argument*/Dictionary_2_set_Item_m439360998_MethodInfo_var);
		goto IL_0059;
	}

IL_0049:
	{
		Dictionary_2_t827649927 * L_8 = V_0;
		NullCheck(L_8);
		Dictionary_2_set_Item_m439360998(L_8, _stringLiteral949037134, _stringLiteral2809397470, /*hidden argument*/Dictionary_2_set_Item_m439360998_MethodInfo_var);
	}

IL_0059:
	{
		Dictionary_2_t827649927 * L_9 = V_0;
		return L_9;
	}
}
// System.Byte[] UnityEngine.WWWForm::get_data()
extern Il2CppClass* MemoryStream_t418716369_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t2012439129_il2cpp_TypeInfo_var;
extern Il2CppClass* WWWTranscoder_t609724394_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m1424439513_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m816542880_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m4222766741_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1440;
extern Il2CppCodeGenString* _stringLiteral413;
extern Il2CppCodeGenString* _stringLiteral1491620852;
extern Il2CppCodeGenString* _stringLiteral3174832991;
extern Il2CppCodeGenString* _stringLiteral34;
extern Il2CppCodeGenString* _stringLiteral4209993937;
extern Il2CppCodeGenString* _stringLiteral1954;
extern Il2CppCodeGenString* _stringLiteral63117;
extern Il2CppCodeGenString* _stringLiteral2014;
extern Il2CppCodeGenString* _stringLiteral38;
extern Il2CppCodeGenString* _stringLiteral61;
extern const uint32_t WWWForm_get_data_m2893811951_MetadataUsageId;
extern "C"  ByteU5BU5D_t4260760469* WWWForm_get_data_m2893811951 (WWWForm_t461342257 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWWForm_get_data_m2893811951_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t4260760469* V_0 = NULL;
	ByteU5BU5D_t4260760469* V_1 = NULL;
	ByteU5BU5D_t4260760469* V_2 = NULL;
	ByteU5BU5D_t4260760469* V_3 = NULL;
	ByteU5BU5D_t4260760469* V_4 = NULL;
	ByteU5BU5D_t4260760469* V_5 = NULL;
	MemoryStream_t418716369 * V_6 = NULL;
	int32_t V_7 = 0;
	ByteU5BU5D_t4260760469* V_8 = NULL;
	String_t* V_9 = NULL;
	String_t* V_10 = NULL;
	ByteU5BU5D_t4260760469* V_11 = NULL;
	String_t* V_12 = NULL;
	ByteU5BU5D_t4260760469* V_13 = NULL;
	ByteU5BU5D_t4260760469* V_14 = NULL;
	ByteU5BU5D_t4260760469* V_15 = NULL;
	ByteU5BU5D_t4260760469* V_16 = NULL;
	MemoryStream_t418716369 * V_17 = NULL;
	int32_t V_18 = 0;
	ByteU5BU5D_t4260760469* V_19 = NULL;
	ByteU5BU5D_t4260760469* V_20 = NULL;
	ByteU5BU5D_t4260760469* V_21 = NULL;
	ByteU5BU5D_t4260760469* V_22 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = __this->get_containsFiles_5();
		if (!L_0)
		{
			goto IL_0311;
		}
	}
	{
		Encoding_t2012439129 * L_1 = WWW_get_DefaultEncoding_m2507364293(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		ByteU5BU5D_t4260760469* L_2 = VirtFuncInvoker1< ByteU5BU5D_t4260760469*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_1, _stringLiteral1440);
		V_0 = L_2;
		Encoding_t2012439129 * L_3 = WWW_get_DefaultEncoding_m2507364293(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		ByteU5BU5D_t4260760469* L_4 = VirtFuncInvoker1< ByteU5BU5D_t4260760469*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_3, _stringLiteral413);
		V_1 = L_4;
		Encoding_t2012439129 * L_5 = WWW_get_DefaultEncoding_m2507364293(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		ByteU5BU5D_t4260760469* L_6 = VirtFuncInvoker1< ByteU5BU5D_t4260760469*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_5, _stringLiteral1491620852);
		V_2 = L_6;
		Encoding_t2012439129 * L_7 = WWW_get_DefaultEncoding_m2507364293(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		ByteU5BU5D_t4260760469* L_8 = VirtFuncInvoker1< ByteU5BU5D_t4260760469*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_7, _stringLiteral3174832991);
		V_3 = L_8;
		Encoding_t2012439129 * L_9 = WWW_get_DefaultEncoding_m2507364293(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		ByteU5BU5D_t4260760469* L_10 = VirtFuncInvoker1< ByteU5BU5D_t4260760469*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_9, _stringLiteral34);
		V_4 = L_10;
		Encoding_t2012439129 * L_11 = WWW_get_DefaultEncoding_m2507364293(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		ByteU5BU5D_t4260760469* L_12 = VirtFuncInvoker1< ByteU5BU5D_t4260760469*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_11, _stringLiteral4209993937);
		V_5 = L_12;
		MemoryStream_t418716369 * L_13 = (MemoryStream_t418716369 *)il2cpp_codegen_object_new(MemoryStream_t418716369_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1061194329(L_13, ((int32_t)1024), /*hidden argument*/NULL);
		V_6 = L_13;
	}

IL_0079:
	try
	{ // begin try (depth: 1)
		{
			V_7 = 0;
			goto IL_0297;
		}

IL_0081:
		{
			MemoryStream_t418716369 * L_14 = V_6;
			ByteU5BU5D_t4260760469* L_15 = V_1;
			ByteU5BU5D_t4260760469* L_16 = V_1;
			NullCheck(L_16);
			NullCheck(L_14);
			VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(23 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_14, L_15, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length)))));
			MemoryStream_t418716369 * L_17 = V_6;
			ByteU5BU5D_t4260760469* L_18 = V_0;
			ByteU5BU5D_t4260760469* L_19 = V_0;
			NullCheck(L_19);
			NullCheck(L_17);
			VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(23 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_17, L_18, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_19)->max_length)))));
			MemoryStream_t418716369 * L_20 = V_6;
			ByteU5BU5D_t4260760469* L_21 = __this->get_boundary_4();
			ByteU5BU5D_t4260760469* L_22 = __this->get_boundary_4();
			NullCheck(L_22);
			NullCheck(L_20);
			VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(23 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_20, L_21, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_22)->max_length)))));
			MemoryStream_t418716369 * L_23 = V_6;
			ByteU5BU5D_t4260760469* L_24 = V_1;
			ByteU5BU5D_t4260760469* L_25 = V_1;
			NullCheck(L_25);
			NullCheck(L_23);
			VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(23 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_23, L_24, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_25)->max_length)))));
			MemoryStream_t418716369 * L_26 = V_6;
			ByteU5BU5D_t4260760469* L_27 = V_2;
			ByteU5BU5D_t4260760469* L_28 = V_2;
			NullCheck(L_28);
			NullCheck(L_26);
			VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(23 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_26, L_27, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_28)->max_length)))));
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t2012439129_il2cpp_TypeInfo_var);
			Encoding_t2012439129 * L_29 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
			List_1_t1375417109 * L_30 = __this->get_types_3();
			int32_t L_31 = V_7;
			NullCheck(L_30);
			String_t* L_32 = List_1_get_Item_m1424439513(L_30, L_31, /*hidden argument*/List_1_get_Item_m1424439513_MethodInfo_var);
			NullCheck(L_29);
			ByteU5BU5D_t4260760469* L_33 = VirtFuncInvoker1< ByteU5BU5D_t4260760469*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_29, L_32);
			V_8 = L_33;
			MemoryStream_t418716369 * L_34 = V_6;
			ByteU5BU5D_t4260760469* L_35 = V_8;
			ByteU5BU5D_t4260760469* L_36 = V_8;
			NullCheck(L_36);
			NullCheck(L_34);
			VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(23 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_34, L_35, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_36)->max_length)))));
			MemoryStream_t418716369 * L_37 = V_6;
			ByteU5BU5D_t4260760469* L_38 = V_1;
			ByteU5BU5D_t4260760469* L_39 = V_1;
			NullCheck(L_39);
			NullCheck(L_37);
			VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(23 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_37, L_38, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_39)->max_length)))));
			MemoryStream_t418716369 * L_40 = V_6;
			ByteU5BU5D_t4260760469* L_41 = V_3;
			ByteU5BU5D_t4260760469* L_42 = V_3;
			NullCheck(L_42);
			NullCheck(L_40);
			VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(23 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_40, L_41, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_42)->max_length)))));
			Encoding_t2012439129 * L_43 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
			NullCheck(L_43);
			String_t* L_44 = VirtFuncInvoker0< String_t* >::Invoke(23 /* System.String System.Text.Encoding::get_HeaderName() */, L_43);
			V_9 = L_44;
			List_1_t1375417109 * L_45 = __this->get_fieldNames_1();
			int32_t L_46 = V_7;
			NullCheck(L_45);
			String_t* L_47 = List_1_get_Item_m1424439513(L_45, L_46, /*hidden argument*/List_1_get_Item_m1424439513_MethodInfo_var);
			V_10 = L_47;
			String_t* L_48 = V_10;
			Encoding_t2012439129 * L_49 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t609724394_il2cpp_TypeInfo_var);
			bool L_50 = WWWTranscoder_SevenBitClean_m1805125217(NULL /*static, unused*/, L_48, L_49, /*hidden argument*/NULL);
			if (!L_50)
			{
				goto IL_0144;
			}
		}

IL_0132:
		{
			String_t* L_51 = V_10;
			NullCheck(L_51);
			int32_t L_52 = String_IndexOf_m1476794331(L_51, _stringLiteral1954, /*hidden argument*/NULL);
			if ((((int32_t)L_52) <= ((int32_t)(-1))))
			{
				goto IL_017d;
			}
		}

IL_0144:
		{
			StringU5BU5D_t4054002952* L_53 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
			NullCheck(L_53);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_53, 0);
			ArrayElementTypeCheck (L_53, _stringLiteral1954);
			(L_53)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1954);
			StringU5BU5D_t4054002952* L_54 = L_53;
			String_t* L_55 = V_9;
			NullCheck(L_54);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_54, 1);
			ArrayElementTypeCheck (L_54, L_55);
			(L_54)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_55);
			StringU5BU5D_t4054002952* L_56 = L_54;
			NullCheck(L_56);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_56, 2);
			ArrayElementTypeCheck (L_56, _stringLiteral63117);
			(L_56)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral63117);
			StringU5BU5D_t4054002952* L_57 = L_56;
			String_t* L_58 = V_10;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t2012439129_il2cpp_TypeInfo_var);
			Encoding_t2012439129 * L_59 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t609724394_il2cpp_TypeInfo_var);
			String_t* L_60 = WWWTranscoder_QPEncode_m1021207296(NULL /*static, unused*/, L_58, L_59, /*hidden argument*/NULL);
			NullCheck(L_57);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_57, 3);
			ArrayElementTypeCheck (L_57, L_60);
			(L_57)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_60);
			StringU5BU5D_t4054002952* L_61 = L_57;
			NullCheck(L_61);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_61, 4);
			ArrayElementTypeCheck (L_61, _stringLiteral2014);
			(L_61)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral2014);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_62 = String_Concat_m21867311(NULL /*static, unused*/, L_61, /*hidden argument*/NULL);
			V_10 = L_62;
		}

IL_017d:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t2012439129_il2cpp_TypeInfo_var);
			Encoding_t2012439129 * L_63 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
			String_t* L_64 = V_10;
			NullCheck(L_63);
			ByteU5BU5D_t4260760469* L_65 = VirtFuncInvoker1< ByteU5BU5D_t4260760469*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_63, L_64);
			V_11 = L_65;
			MemoryStream_t418716369 * L_66 = V_6;
			ByteU5BU5D_t4260760469* L_67 = V_11;
			ByteU5BU5D_t4260760469* L_68 = V_11;
			NullCheck(L_68);
			NullCheck(L_66);
			VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(23 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_66, L_67, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_68)->max_length)))));
			MemoryStream_t418716369 * L_69 = V_6;
			ByteU5BU5D_t4260760469* L_70 = V_4;
			ByteU5BU5D_t4260760469* L_71 = V_4;
			NullCheck(L_71);
			NullCheck(L_69);
			VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(23 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_69, L_70, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_71)->max_length)))));
			List_1_t1375417109 * L_72 = __this->get_fileNames_2();
			int32_t L_73 = V_7;
			NullCheck(L_72);
			String_t* L_74 = List_1_get_Item_m1424439513(L_72, L_73, /*hidden argument*/List_1_get_Item_m1424439513_MethodInfo_var);
			if (!L_74)
			{
				goto IL_025c;
			}
		}

IL_01b9:
		{
			List_1_t1375417109 * L_75 = __this->get_fileNames_2();
			int32_t L_76 = V_7;
			NullCheck(L_75);
			String_t* L_77 = List_1_get_Item_m1424439513(L_75, L_76, /*hidden argument*/List_1_get_Item_m1424439513_MethodInfo_var);
			V_12 = L_77;
			String_t* L_78 = V_12;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t2012439129_il2cpp_TypeInfo_var);
			Encoding_t2012439129 * L_79 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t609724394_il2cpp_TypeInfo_var);
			bool L_80 = WWWTranscoder_SevenBitClean_m1805125217(NULL /*static, unused*/, L_78, L_79, /*hidden argument*/NULL);
			if (!L_80)
			{
				goto IL_01eb;
			}
		}

IL_01d9:
		{
			String_t* L_81 = V_12;
			NullCheck(L_81);
			int32_t L_82 = String_IndexOf_m1476794331(L_81, _stringLiteral1954, /*hidden argument*/NULL);
			if ((((int32_t)L_82) <= ((int32_t)(-1))))
			{
				goto IL_0224;
			}
		}

IL_01eb:
		{
			StringU5BU5D_t4054002952* L_83 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
			NullCheck(L_83);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_83, 0);
			ArrayElementTypeCheck (L_83, _stringLiteral1954);
			(L_83)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1954);
			StringU5BU5D_t4054002952* L_84 = L_83;
			String_t* L_85 = V_9;
			NullCheck(L_84);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_84, 1);
			ArrayElementTypeCheck (L_84, L_85);
			(L_84)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_85);
			StringU5BU5D_t4054002952* L_86 = L_84;
			NullCheck(L_86);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_86, 2);
			ArrayElementTypeCheck (L_86, _stringLiteral63117);
			(L_86)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral63117);
			StringU5BU5D_t4054002952* L_87 = L_86;
			String_t* L_88 = V_12;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t2012439129_il2cpp_TypeInfo_var);
			Encoding_t2012439129 * L_89 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t609724394_il2cpp_TypeInfo_var);
			String_t* L_90 = WWWTranscoder_QPEncode_m1021207296(NULL /*static, unused*/, L_88, L_89, /*hidden argument*/NULL);
			NullCheck(L_87);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_87, 3);
			ArrayElementTypeCheck (L_87, L_90);
			(L_87)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_90);
			StringU5BU5D_t4054002952* L_91 = L_87;
			NullCheck(L_91);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_91, 4);
			ArrayElementTypeCheck (L_91, _stringLiteral2014);
			(L_91)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral2014);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_92 = String_Concat_m21867311(NULL /*static, unused*/, L_91, /*hidden argument*/NULL);
			V_12 = L_92;
		}

IL_0224:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t2012439129_il2cpp_TypeInfo_var);
			Encoding_t2012439129 * L_93 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
			String_t* L_94 = V_12;
			NullCheck(L_93);
			ByteU5BU5D_t4260760469* L_95 = VirtFuncInvoker1< ByteU5BU5D_t4260760469*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_93, L_94);
			V_13 = L_95;
			MemoryStream_t418716369 * L_96 = V_6;
			ByteU5BU5D_t4260760469* L_97 = V_5;
			ByteU5BU5D_t4260760469* L_98 = V_5;
			NullCheck(L_98);
			NullCheck(L_96);
			VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(23 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_96, L_97, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_98)->max_length)))));
			MemoryStream_t418716369 * L_99 = V_6;
			ByteU5BU5D_t4260760469* L_100 = V_13;
			ByteU5BU5D_t4260760469* L_101 = V_13;
			NullCheck(L_101);
			NullCheck(L_99);
			VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(23 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_99, L_100, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_101)->max_length)))));
			MemoryStream_t418716369 * L_102 = V_6;
			ByteU5BU5D_t4260760469* L_103 = V_4;
			ByteU5BU5D_t4260760469* L_104 = V_4;
			NullCheck(L_104);
			NullCheck(L_102);
			VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(23 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_102, L_103, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_104)->max_length)))));
		}

IL_025c:
		{
			MemoryStream_t418716369 * L_105 = V_6;
			ByteU5BU5D_t4260760469* L_106 = V_1;
			ByteU5BU5D_t4260760469* L_107 = V_1;
			NullCheck(L_107);
			NullCheck(L_105);
			VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(23 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_105, L_106, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_107)->max_length)))));
			MemoryStream_t418716369 * L_108 = V_6;
			ByteU5BU5D_t4260760469* L_109 = V_1;
			ByteU5BU5D_t4260760469* L_110 = V_1;
			NullCheck(L_110);
			NullCheck(L_108);
			VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(23 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_108, L_109, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_110)->max_length)))));
			List_1_t1333978725 * L_111 = __this->get_formData_0();
			int32_t L_112 = V_7;
			NullCheck(L_111);
			ByteU5BU5D_t4260760469* L_113 = List_1_get_Item_m816542880(L_111, L_112, /*hidden argument*/List_1_get_Item_m816542880_MethodInfo_var);
			V_14 = L_113;
			MemoryStream_t418716369 * L_114 = V_6;
			ByteU5BU5D_t4260760469* L_115 = V_14;
			ByteU5BU5D_t4260760469* L_116 = V_14;
			NullCheck(L_116);
			NullCheck(L_114);
			VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(23 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_114, L_115, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_116)->max_length)))));
			int32_t L_117 = V_7;
			V_7 = ((int32_t)((int32_t)L_117+(int32_t)1));
		}

IL_0297:
		{
			int32_t L_118 = V_7;
			List_1_t1333978725 * L_119 = __this->get_formData_0();
			NullCheck(L_119);
			int32_t L_120 = List_1_get_Count_m4222766741(L_119, /*hidden argument*/List_1_get_Count_m4222766741_MethodInfo_var);
			if ((((int32_t)L_118) < ((int32_t)L_120)))
			{
				goto IL_0081;
			}
		}

IL_02a9:
		{
			MemoryStream_t418716369 * L_121 = V_6;
			ByteU5BU5D_t4260760469* L_122 = V_1;
			ByteU5BU5D_t4260760469* L_123 = V_1;
			NullCheck(L_123);
			NullCheck(L_121);
			VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(23 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_121, L_122, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_123)->max_length)))));
			MemoryStream_t418716369 * L_124 = V_6;
			ByteU5BU5D_t4260760469* L_125 = V_0;
			ByteU5BU5D_t4260760469* L_126 = V_0;
			NullCheck(L_126);
			NullCheck(L_124);
			VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(23 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_124, L_125, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_126)->max_length)))));
			MemoryStream_t418716369 * L_127 = V_6;
			ByteU5BU5D_t4260760469* L_128 = __this->get_boundary_4();
			ByteU5BU5D_t4260760469* L_129 = __this->get_boundary_4();
			NullCheck(L_129);
			NullCheck(L_127);
			VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(23 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_127, L_128, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_129)->max_length)))));
			MemoryStream_t418716369 * L_130 = V_6;
			ByteU5BU5D_t4260760469* L_131 = V_0;
			ByteU5BU5D_t4260760469* L_132 = V_0;
			NullCheck(L_132);
			NullCheck(L_130);
			VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(23 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_130, L_131, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_132)->max_length)))));
			MemoryStream_t418716369 * L_133 = V_6;
			ByteU5BU5D_t4260760469* L_134 = V_1;
			ByteU5BU5D_t4260760469* L_135 = V_1;
			NullCheck(L_135);
			NullCheck(L_133);
			VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(23 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_133, L_134, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_135)->max_length)))));
			MemoryStream_t418716369 * L_136 = V_6;
			NullCheck(L_136);
			ByteU5BU5D_t4260760469* L_137 = VirtFuncInvoker0< ByteU5BU5D_t4260760469* >::Invoke(31 /* System.Byte[] System.IO.MemoryStream::ToArray() */, L_136);
			V_22 = L_137;
			IL2CPP_LEAVE(0x3F7, FINALLY_0302);
		}

IL_02fd:
		{
			; // IL_02fd: leave IL_0311
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0302;
	}

FINALLY_0302:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t418716369 * L_138 = V_6;
			if (!L_138)
			{
				goto IL_0310;
			}
		}

IL_0309:
		{
			MemoryStream_t418716369 * L_139 = V_6;
			NullCheck(L_139);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_139);
		}

IL_0310:
		{
			IL2CPP_END_FINALLY(770)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(770)
	{
		IL2CPP_JUMP_TBL(0x3F7, IL_03f7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0311:
	{
		Encoding_t2012439129 * L_140 = WWW_get_DefaultEncoding_m2507364293(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_140);
		ByteU5BU5D_t4260760469* L_141 = VirtFuncInvoker1< ByteU5BU5D_t4260760469*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_140, _stringLiteral38);
		V_15 = L_141;
		Encoding_t2012439129 * L_142 = WWW_get_DefaultEncoding_m2507364293(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_142);
		ByteU5BU5D_t4260760469* L_143 = VirtFuncInvoker1< ByteU5BU5D_t4260760469*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_142, _stringLiteral61);
		V_16 = L_143;
		MemoryStream_t418716369 * L_144 = (MemoryStream_t418716369 *)il2cpp_codegen_object_new(MemoryStream_t418716369_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1061194329(L_144, ((int32_t)1024), /*hidden argument*/NULL);
		V_17 = L_144;
	}

IL_033f:
	try
	{ // begin try (depth: 1)
		{
			V_18 = 0;
			goto IL_03c3;
		}

IL_0347:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t2012439129_il2cpp_TypeInfo_var);
			Encoding_t2012439129 * L_145 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
			List_1_t1375417109 * L_146 = __this->get_fieldNames_1();
			int32_t L_147 = V_18;
			NullCheck(L_146);
			String_t* L_148 = List_1_get_Item_m1424439513(L_146, L_147, /*hidden argument*/List_1_get_Item_m1424439513_MethodInfo_var);
			NullCheck(L_145);
			ByteU5BU5D_t4260760469* L_149 = VirtFuncInvoker1< ByteU5BU5D_t4260760469*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_145, L_148);
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t609724394_il2cpp_TypeInfo_var);
			ByteU5BU5D_t4260760469* L_150 = WWWTranscoder_URLEncode_m4039667511(NULL /*static, unused*/, L_149, /*hidden argument*/NULL);
			V_19 = L_150;
			List_1_t1333978725 * L_151 = __this->get_formData_0();
			int32_t L_152 = V_18;
			NullCheck(L_151);
			ByteU5BU5D_t4260760469* L_153 = List_1_get_Item_m816542880(L_151, L_152, /*hidden argument*/List_1_get_Item_m816542880_MethodInfo_var);
			V_20 = L_153;
			ByteU5BU5D_t4260760469* L_154 = V_20;
			ByteU5BU5D_t4260760469* L_155 = WWWTranscoder_URLEncode_m4039667511(NULL /*static, unused*/, L_154, /*hidden argument*/NULL);
			V_21 = L_155;
			int32_t L_156 = V_18;
			if ((((int32_t)L_156) <= ((int32_t)0)))
			{
				goto IL_0393;
			}
		}

IL_0385:
		{
			MemoryStream_t418716369 * L_157 = V_17;
			ByteU5BU5D_t4260760469* L_158 = V_15;
			ByteU5BU5D_t4260760469* L_159 = V_15;
			NullCheck(L_159);
			NullCheck(L_157);
			VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(23 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_157, L_158, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_159)->max_length)))));
		}

IL_0393:
		{
			MemoryStream_t418716369 * L_160 = V_17;
			ByteU5BU5D_t4260760469* L_161 = V_19;
			ByteU5BU5D_t4260760469* L_162 = V_19;
			NullCheck(L_162);
			NullCheck(L_160);
			VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(23 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_160, L_161, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_162)->max_length)))));
			MemoryStream_t418716369 * L_163 = V_17;
			ByteU5BU5D_t4260760469* L_164 = V_16;
			ByteU5BU5D_t4260760469* L_165 = V_16;
			NullCheck(L_165);
			NullCheck(L_163);
			VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(23 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_163, L_164, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_165)->max_length)))));
			MemoryStream_t418716369 * L_166 = V_17;
			ByteU5BU5D_t4260760469* L_167 = V_21;
			ByteU5BU5D_t4260760469* L_168 = V_21;
			NullCheck(L_168);
			NullCheck(L_166);
			VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(23 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_166, L_167, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_168)->max_length)))));
			int32_t L_169 = V_18;
			V_18 = ((int32_t)((int32_t)L_169+(int32_t)1));
		}

IL_03c3:
		{
			int32_t L_170 = V_18;
			List_1_t1333978725 * L_171 = __this->get_formData_0();
			NullCheck(L_171);
			int32_t L_172 = List_1_get_Count_m4222766741(L_171, /*hidden argument*/List_1_get_Count_m4222766741_MethodInfo_var);
			if ((((int32_t)L_170) < ((int32_t)L_172)))
			{
				goto IL_0347;
			}
		}

IL_03d5:
		{
			MemoryStream_t418716369 * L_173 = V_17;
			NullCheck(L_173);
			ByteU5BU5D_t4260760469* L_174 = VirtFuncInvoker0< ByteU5BU5D_t4260760469* >::Invoke(31 /* System.Byte[] System.IO.MemoryStream::ToArray() */, L_173);
			V_22 = L_174;
			IL2CPP_LEAVE(0x3F7, FINALLY_03e8);
		}

IL_03e3:
		{
			; // IL_03e3: leave IL_03f7
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_03e8;
	}

FINALLY_03e8:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t418716369 * L_175 = V_17;
			if (!L_175)
			{
				goto IL_03f6;
			}
		}

IL_03ef:
		{
			MemoryStream_t418716369 * L_176 = V_17;
			NullCheck(L_176);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_176);
		}

IL_03f6:
		{
			IL2CPP_END_FINALLY(1000)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1000)
	{
		IL2CPP_JUMP_TBL(0x3F7, IL_03f7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_03f7:
	{
		ByteU5BU5D_t4260760469* L_177 = V_22;
		return L_177;
	}
}
// System.Void UnityEngine.WWWTranscoder::.cctor()
extern Il2CppClass* WWWTranscoder_t609724394_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3633740456;
extern Il2CppCodeGenString* _stringLiteral285443752;
extern Il2CppCodeGenString* _stringLiteral2318011663;
extern Il2CppCodeGenString* _stringLiteral4217153853;
extern const uint32_t WWWTranscoder__cctor_m1486747496_MetadataUsageId;
extern "C"  void WWWTranscoder__cctor_m1486747496 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWWTranscoder__cctor_m1486747496_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Encoding_t2012439129 * L_0 = WWW_get_DefaultEncoding_m2507364293(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		ByteU5BU5D_t4260760469* L_1 = VirtFuncInvoker1< ByteU5BU5D_t4260760469*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_0, _stringLiteral3633740456);
		((WWWTranscoder_t609724394_StaticFields*)WWWTranscoder_t609724394_il2cpp_TypeInfo_var->static_fields)->set_ucHexChars_0(L_1);
		Encoding_t2012439129 * L_2 = WWW_get_DefaultEncoding_m2507364293(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		ByteU5BU5D_t4260760469* L_3 = VirtFuncInvoker1< ByteU5BU5D_t4260760469*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_2, _stringLiteral285443752);
		((WWWTranscoder_t609724394_StaticFields*)WWWTranscoder_t609724394_il2cpp_TypeInfo_var->static_fields)->set_lcHexChars_1(L_3);
		((WWWTranscoder_t609724394_StaticFields*)WWWTranscoder_t609724394_il2cpp_TypeInfo_var->static_fields)->set_urlEscapeChar_2(((int32_t)37));
		((WWWTranscoder_t609724394_StaticFields*)WWWTranscoder_t609724394_il2cpp_TypeInfo_var->static_fields)->set_urlSpace_3(((int32_t)43));
		Encoding_t2012439129 * L_4 = WWW_get_DefaultEncoding_m2507364293(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		ByteU5BU5D_t4260760469* L_5 = VirtFuncInvoker1< ByteU5BU5D_t4260760469*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_4, _stringLiteral2318011663);
		((WWWTranscoder_t609724394_StaticFields*)WWWTranscoder_t609724394_il2cpp_TypeInfo_var->static_fields)->set_urlForbidden_4(L_5);
		((WWWTranscoder_t609724394_StaticFields*)WWWTranscoder_t609724394_il2cpp_TypeInfo_var->static_fields)->set_qpEscapeChar_5(((int32_t)61));
		((WWWTranscoder_t609724394_StaticFields*)WWWTranscoder_t609724394_il2cpp_TypeInfo_var->static_fields)->set_qpSpace_6(((int32_t)95));
		Encoding_t2012439129 * L_6 = WWW_get_DefaultEncoding_m2507364293(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		ByteU5BU5D_t4260760469* L_7 = VirtFuncInvoker1< ByteU5BU5D_t4260760469*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_6, _stringLiteral4217153853);
		((WWWTranscoder_t609724394_StaticFields*)WWWTranscoder_t609724394_il2cpp_TypeInfo_var->static_fields)->set_qpForbidden_7(L_7);
		return;
	}
}
// System.Byte UnityEngine.WWWTranscoder::Hex2Byte(System.Byte[],System.Int32)
extern "C"  uint8_t WWWTranscoder_Hex2Byte_m2137781440 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___b0, int32_t ___offset1, const MethodInfo* method)
{
	uint8_t V_0 = 0x0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		int32_t L_0 = ___offset1;
		V_1 = L_0;
		goto IL_0071;
	}

IL_0009:
	{
		uint8_t L_1 = V_0;
		V_0 = (((int32_t)((uint8_t)((int32_t)((int32_t)L_1*(int32_t)((int32_t)16))))));
		ByteU5BU5D_t4260760469* L_2 = ___b0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		uint8_t L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_2 = L_5;
		int32_t L_6 = V_2;
		if ((((int32_t)L_6) < ((int32_t)((int32_t)48))))
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_7 = V_2;
		if ((((int32_t)L_7) > ((int32_t)((int32_t)57))))
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_8 = V_2;
		V_2 = ((int32_t)((int32_t)L_8-(int32_t)((int32_t)48)));
		goto IL_005c;
	}

IL_002d:
	{
		int32_t L_9 = V_2;
		if ((((int32_t)L_9) < ((int32_t)((int32_t)65))))
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_10 = V_2;
		if ((((int32_t)L_10) > ((int32_t)((int32_t)75))))
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_11 = V_2;
		V_2 = ((int32_t)((int32_t)L_11-(int32_t)((int32_t)55)));
		goto IL_005c;
	}

IL_0047:
	{
		int32_t L_12 = V_2;
		if ((((int32_t)L_12) < ((int32_t)((int32_t)97))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_13 = V_2;
		if ((((int32_t)L_13) > ((int32_t)((int32_t)102))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_14 = V_2;
		V_2 = ((int32_t)((int32_t)L_14-(int32_t)((int32_t)87)));
	}

IL_005c:
	{
		int32_t L_15 = V_2;
		if ((((int32_t)L_15) <= ((int32_t)((int32_t)15))))
		{
			goto IL_0067;
		}
	}
	{
		return ((int32_t)63);
	}

IL_0067:
	{
		uint8_t L_16 = V_0;
		int32_t L_17 = V_2;
		V_0 = (((int32_t)((uint8_t)((int32_t)((int32_t)L_16+(int32_t)(((int32_t)((uint8_t)L_17))))))));
		int32_t L_18 = V_1;
		V_1 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0071:
	{
		int32_t L_19 = V_1;
		int32_t L_20 = ___offset1;
		if ((((int32_t)L_19) < ((int32_t)((int32_t)((int32_t)L_20+(int32_t)2)))))
		{
			goto IL_0009;
		}
	}
	{
		uint8_t L_21 = V_0;
		return L_21;
	}
}
// System.Byte[] UnityEngine.WWWTranscoder::Byte2Hex(System.Byte,System.Byte[])
extern Il2CppClass* ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var;
extern const uint32_t WWWTranscoder_Byte2Hex_m2414999496_MetadataUsageId;
extern "C"  ByteU5BU5D_t4260760469* WWWTranscoder_Byte2Hex_m2414999496 (Il2CppObject * __this /* static, unused */, uint8_t ___b0, ByteU5BU5D_t4260760469* ___hexChars1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWWTranscoder_Byte2Hex_m2414999496_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t4260760469* V_0 = NULL;
	{
		V_0 = ((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)2));
		ByteU5BU5D_t4260760469* L_0 = V_0;
		ByteU5BU5D_t4260760469* L_1 = ___hexChars1;
		uint8_t L_2 = ___b0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, ((int32_t)((int32_t)L_2>>(int32_t)4)));
		int32_t L_3 = ((int32_t)((int32_t)L_2>>(int32_t)4));
		uint8_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint8_t)L_4);
		ByteU5BU5D_t4260760469* L_5 = V_0;
		ByteU5BU5D_t4260760469* L_6 = ___hexChars1;
		uint8_t L_7 = ___b0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, ((int32_t)((int32_t)L_7&(int32_t)((int32_t)15))));
		int32_t L_8 = ((int32_t)((int32_t)L_7&(int32_t)((int32_t)15)));
		uint8_t L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (uint8_t)L_9);
		ByteU5BU5D_t4260760469* L_10 = V_0;
		return L_10;
	}
}
// System.String UnityEngine.WWWTranscoder::URLEncode(System.String,System.Text.Encoding)
extern Il2CppClass* WWWTranscoder_t609724394_il2cpp_TypeInfo_var;
extern const uint32_t WWWTranscoder_URLEncode_m3301913818_MetadataUsageId;
extern "C"  String_t* WWWTranscoder_URLEncode_m3301913818 (Il2CppObject * __this /* static, unused */, String_t* ___toEncode0, Encoding_t2012439129 * ___e1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWWTranscoder_URLEncode_m3301913818_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t4260760469* V_0 = NULL;
	{
		Encoding_t2012439129 * L_0 = ___e1;
		String_t* L_1 = ___toEncode0;
		NullCheck(L_0);
		ByteU5BU5D_t4260760469* L_2 = VirtFuncInvoker1< ByteU5BU5D_t4260760469*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_0, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t609724394_il2cpp_TypeInfo_var);
		uint8_t L_3 = ((WWWTranscoder_t609724394_StaticFields*)WWWTranscoder_t609724394_il2cpp_TypeInfo_var->static_fields)->get_urlEscapeChar_2();
		uint8_t L_4 = ((WWWTranscoder_t609724394_StaticFields*)WWWTranscoder_t609724394_il2cpp_TypeInfo_var->static_fields)->get_urlSpace_3();
		ByteU5BU5D_t4260760469* L_5 = ((WWWTranscoder_t609724394_StaticFields*)WWWTranscoder_t609724394_il2cpp_TypeInfo_var->static_fields)->get_urlForbidden_4();
		ByteU5BU5D_t4260760469* L_6 = WWWTranscoder_Encode_m2677436946(NULL /*static, unused*/, L_2, L_3, L_4, L_5, (bool)0, /*hidden argument*/NULL);
		V_0 = L_6;
		Encoding_t2012439129 * L_7 = WWW_get_DefaultEncoding_m2507364293(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t4260760469* L_8 = V_0;
		ByteU5BU5D_t4260760469* L_9 = V_0;
		NullCheck(L_9);
		NullCheck(L_7);
		String_t* L_10 = VirtFuncInvoker3< String_t*, ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(21 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_7, L_8, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))));
		return L_10;
	}
}
// System.Byte[] UnityEngine.WWWTranscoder::URLEncode(System.Byte[])
extern Il2CppClass* WWWTranscoder_t609724394_il2cpp_TypeInfo_var;
extern const uint32_t WWWTranscoder_URLEncode_m4039667511_MetadataUsageId;
extern "C"  ByteU5BU5D_t4260760469* WWWTranscoder_URLEncode_m4039667511 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___toEncode0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWWTranscoder_URLEncode_m4039667511_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t4260760469* L_0 = ___toEncode0;
		IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t609724394_il2cpp_TypeInfo_var);
		uint8_t L_1 = ((WWWTranscoder_t609724394_StaticFields*)WWWTranscoder_t609724394_il2cpp_TypeInfo_var->static_fields)->get_urlEscapeChar_2();
		uint8_t L_2 = ((WWWTranscoder_t609724394_StaticFields*)WWWTranscoder_t609724394_il2cpp_TypeInfo_var->static_fields)->get_urlSpace_3();
		ByteU5BU5D_t4260760469* L_3 = ((WWWTranscoder_t609724394_StaticFields*)WWWTranscoder_t609724394_il2cpp_TypeInfo_var->static_fields)->get_urlForbidden_4();
		ByteU5BU5D_t4260760469* L_4 = WWWTranscoder_Encode_m2677436946(NULL /*static, unused*/, L_0, L_1, L_2, L_3, (bool)0, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.String UnityEngine.WWWTranscoder::QPEncode(System.String,System.Text.Encoding)
extern Il2CppClass* WWWTranscoder_t609724394_il2cpp_TypeInfo_var;
extern const uint32_t WWWTranscoder_QPEncode_m1021207296_MetadataUsageId;
extern "C"  String_t* WWWTranscoder_QPEncode_m1021207296 (Il2CppObject * __this /* static, unused */, String_t* ___toEncode0, Encoding_t2012439129 * ___e1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWWTranscoder_QPEncode_m1021207296_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t4260760469* V_0 = NULL;
	{
		Encoding_t2012439129 * L_0 = ___e1;
		String_t* L_1 = ___toEncode0;
		NullCheck(L_0);
		ByteU5BU5D_t4260760469* L_2 = VirtFuncInvoker1< ByteU5BU5D_t4260760469*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_0, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t609724394_il2cpp_TypeInfo_var);
		uint8_t L_3 = ((WWWTranscoder_t609724394_StaticFields*)WWWTranscoder_t609724394_il2cpp_TypeInfo_var->static_fields)->get_qpEscapeChar_5();
		uint8_t L_4 = ((WWWTranscoder_t609724394_StaticFields*)WWWTranscoder_t609724394_il2cpp_TypeInfo_var->static_fields)->get_qpSpace_6();
		ByteU5BU5D_t4260760469* L_5 = ((WWWTranscoder_t609724394_StaticFields*)WWWTranscoder_t609724394_il2cpp_TypeInfo_var->static_fields)->get_qpForbidden_7();
		ByteU5BU5D_t4260760469* L_6 = WWWTranscoder_Encode_m2677436946(NULL /*static, unused*/, L_2, L_3, L_4, L_5, (bool)1, /*hidden argument*/NULL);
		V_0 = L_6;
		Encoding_t2012439129 * L_7 = WWW_get_DefaultEncoding_m2507364293(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t4260760469* L_8 = V_0;
		ByteU5BU5D_t4260760469* L_9 = V_0;
		NullCheck(L_9);
		NullCheck(L_7);
		String_t* L_10 = VirtFuncInvoker3< String_t*, ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(21 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_7, L_8, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))));
		return L_10;
	}
}
// System.Byte[] UnityEngine.WWWTranscoder::Encode(System.Byte[],System.Byte,System.Byte,System.Byte[],System.Boolean)
extern Il2CppClass* MemoryStream_t418716369_il2cpp_TypeInfo_var;
extern Il2CppClass* WWWTranscoder_t609724394_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t WWWTranscoder_Encode_m2677436946_MetadataUsageId;
extern "C"  ByteU5BU5D_t4260760469* WWWTranscoder_Encode_m2677436946 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___input0, uint8_t ___escapeChar1, uint8_t ___space2, ByteU5BU5D_t4260760469* ___forbidden3, bool ___uppercase4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWWTranscoder_Encode_m2677436946_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MemoryStream_t418716369 * V_0 = NULL;
	int32_t V_1 = 0;
	ByteU5BU5D_t4260760469* V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B9_0 = 0;
	MemoryStream_t418716369 * G_B9_1 = NULL;
	int32_t G_B8_0 = 0;
	MemoryStream_t418716369 * G_B8_1 = NULL;
	ByteU5BU5D_t4260760469* G_B10_0 = NULL;
	int32_t G_B10_1 = 0;
	MemoryStream_t418716369 * G_B10_2 = NULL;
	{
		ByteU5BU5D_t4260760469* L_0 = ___input0;
		NullCheck(L_0);
		MemoryStream_t418716369 * L_1 = (MemoryStream_t418716369 *)il2cpp_codegen_object_new(MemoryStream_t418716369_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1061194329(L_1, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))*(int32_t)2)), /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		{
			V_1 = 0;
			goto IL_0089;
		}

IL_0012:
		{
			ByteU5BU5D_t4260760469* L_2 = ___input0;
			int32_t L_3 = V_1;
			NullCheck(L_2);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
			int32_t L_4 = L_3;
			uint8_t L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
			if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)32)))))
			{
				goto IL_0028;
			}
		}

IL_001c:
		{
			MemoryStream_t418716369 * L_6 = V_0;
			uint8_t L_7 = ___space2;
			NullCheck(L_6);
			VirtActionInvoker1< uint8_t >::Invoke(24 /* System.Void System.IO.MemoryStream::WriteByte(System.Byte) */, L_6, L_7);
			goto IL_0085;
		}

IL_0028:
		{
			ByteU5BU5D_t4260760469* L_8 = ___input0;
			int32_t L_9 = V_1;
			NullCheck(L_8);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
			int32_t L_10 = L_9;
			uint8_t L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
			if ((((int32_t)L_11) < ((int32_t)((int32_t)32))))
			{
				goto IL_004a;
			}
		}

IL_0032:
		{
			ByteU5BU5D_t4260760469* L_12 = ___input0;
			int32_t L_13 = V_1;
			NullCheck(L_12);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
			int32_t L_14 = L_13;
			uint8_t L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
			if ((((int32_t)L_15) > ((int32_t)((int32_t)126))))
			{
				goto IL_004a;
			}
		}

IL_003c:
		{
			ByteU5BU5D_t4260760469* L_16 = ___forbidden3;
			ByteU5BU5D_t4260760469* L_17 = ___input0;
			int32_t L_18 = V_1;
			NullCheck(L_17);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
			int32_t L_19 = L_18;
			uint8_t L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t609724394_il2cpp_TypeInfo_var);
			bool L_21 = WWWTranscoder_ByteArrayContains_m3244919989(NULL /*static, unused*/, L_16, L_20, /*hidden argument*/NULL);
			if (!L_21)
			{
				goto IL_007c;
			}
		}

IL_004a:
		{
			MemoryStream_t418716369 * L_22 = V_0;
			uint8_t L_23 = ___escapeChar1;
			NullCheck(L_22);
			VirtActionInvoker1< uint8_t >::Invoke(24 /* System.Void System.IO.MemoryStream::WriteByte(System.Byte) */, L_22, L_23);
			MemoryStream_t418716369 * L_24 = V_0;
			ByteU5BU5D_t4260760469* L_25 = ___input0;
			int32_t L_26 = V_1;
			NullCheck(L_25);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_26);
			int32_t L_27 = L_26;
			uint8_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
			bool L_29 = ___uppercase4;
			G_B8_0 = ((int32_t)(L_28));
			G_B8_1 = L_24;
			if (!L_29)
			{
				G_B9_0 = ((int32_t)(L_28));
				G_B9_1 = L_24;
				goto IL_0066;
			}
		}

IL_005c:
		{
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t609724394_il2cpp_TypeInfo_var);
			ByteU5BU5D_t4260760469* L_30 = ((WWWTranscoder_t609724394_StaticFields*)WWWTranscoder_t609724394_il2cpp_TypeInfo_var->static_fields)->get_ucHexChars_0();
			G_B10_0 = L_30;
			G_B10_1 = G_B8_0;
			G_B10_2 = G_B8_1;
			goto IL_006b;
		}

IL_0066:
		{
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t609724394_il2cpp_TypeInfo_var);
			ByteU5BU5D_t4260760469* L_31 = ((WWWTranscoder_t609724394_StaticFields*)WWWTranscoder_t609724394_il2cpp_TypeInfo_var->static_fields)->get_lcHexChars_1();
			G_B10_0 = L_31;
			G_B10_1 = G_B9_0;
			G_B10_2 = G_B9_1;
		}

IL_006b:
		{
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t609724394_il2cpp_TypeInfo_var);
			ByteU5BU5D_t4260760469* L_32 = WWWTranscoder_Byte2Hex_m2414999496(NULL /*static, unused*/, G_B10_1, G_B10_0, /*hidden argument*/NULL);
			NullCheck(G_B10_2);
			VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(23 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, G_B10_2, L_32, 0, 2);
			goto IL_0085;
		}

IL_007c:
		{
			MemoryStream_t418716369 * L_33 = V_0;
			ByteU5BU5D_t4260760469* L_34 = ___input0;
			int32_t L_35 = V_1;
			NullCheck(L_34);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_34, L_35);
			int32_t L_36 = L_35;
			uint8_t L_37 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_36));
			NullCheck(L_33);
			VirtActionInvoker1< uint8_t >::Invoke(24 /* System.Void System.IO.MemoryStream::WriteByte(System.Byte) */, L_33, L_37);
		}

IL_0085:
		{
			int32_t L_38 = V_1;
			V_1 = ((int32_t)((int32_t)L_38+(int32_t)1));
		}

IL_0089:
		{
			int32_t L_39 = V_1;
			ByteU5BU5D_t4260760469* L_40 = ___input0;
			NullCheck(L_40);
			if ((((int32_t)L_39) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_40)->max_length)))))))
			{
				goto IL_0012;
			}
		}

IL_0092:
		{
			MemoryStream_t418716369 * L_41 = V_0;
			NullCheck(L_41);
			ByteU5BU5D_t4260760469* L_42 = VirtFuncInvoker0< ByteU5BU5D_t4260760469* >::Invoke(31 /* System.Byte[] System.IO.MemoryStream::ToArray() */, L_41);
			V_2 = L_42;
			IL2CPP_LEAVE(0xB0, FINALLY_00a3);
		}

IL_009e:
		{
			; // IL_009e: leave IL_00b0
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_00a3;
	}

FINALLY_00a3:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t418716369 * L_43 = V_0;
			if (!L_43)
			{
				goto IL_00af;
			}
		}

IL_00a9:
		{
			MemoryStream_t418716369 * L_44 = V_0;
			NullCheck(L_44);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_44);
		}

IL_00af:
		{
			IL2CPP_END_FINALLY(163)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(163)
	{
		IL2CPP_JUMP_TBL(0xB0, IL_00b0)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00b0:
	{
		ByteU5BU5D_t4260760469* L_45 = V_2;
		return L_45;
	}
}
// System.Boolean UnityEngine.WWWTranscoder::ByteArrayContains(System.Byte[],System.Byte)
extern "C"  bool WWWTranscoder_ByteArrayContains_m3244919989 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___array0, uint8_t ___b1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		ByteU5BU5D_t4260760469* L_0 = ___array0;
		NullCheck(L_0);
		V_0 = (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
		V_1 = 0;
		goto IL_001a;
	}

IL_000b:
	{
		ByteU5BU5D_t4260760469* L_1 = ___array0;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		uint8_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		uint8_t L_5 = ___b1;
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_0016;
		}
	}
	{
		return (bool)1;
	}

IL_0016:
	{
		int32_t L_6 = V_1;
		V_1 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_001a:
	{
		int32_t L_7 = V_1;
		int32_t L_8 = V_0;
		if ((((int32_t)L_7) < ((int32_t)L_8)))
		{
			goto IL_000b;
		}
	}
	{
		return (bool)0;
	}
}
// System.String UnityEngine.WWWTranscoder::URLDecode(System.String,System.Text.Encoding)
extern Il2CppClass* WWWTranscoder_t609724394_il2cpp_TypeInfo_var;
extern const uint32_t WWWTranscoder_URLDecode_m3418913970_MetadataUsageId;
extern "C"  String_t* WWWTranscoder_URLDecode_m3418913970 (Il2CppObject * __this /* static, unused */, String_t* ___toEncode0, Encoding_t2012439129 * ___e1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWWTranscoder_URLDecode_m3418913970_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t4260760469* V_0 = NULL;
	{
		Encoding_t2012439129 * L_0 = WWW_get_DefaultEncoding_m2507364293(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ___toEncode0;
		NullCheck(L_0);
		ByteU5BU5D_t4260760469* L_2 = VirtFuncInvoker1< ByteU5BU5D_t4260760469*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_0, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t609724394_il2cpp_TypeInfo_var);
		uint8_t L_3 = ((WWWTranscoder_t609724394_StaticFields*)WWWTranscoder_t609724394_il2cpp_TypeInfo_var->static_fields)->get_urlEscapeChar_2();
		uint8_t L_4 = ((WWWTranscoder_t609724394_StaticFields*)WWWTranscoder_t609724394_il2cpp_TypeInfo_var->static_fields)->get_urlSpace_3();
		ByteU5BU5D_t4260760469* L_5 = WWWTranscoder_Decode_m2839418928(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		Encoding_t2012439129 * L_6 = ___e1;
		ByteU5BU5D_t4260760469* L_7 = V_0;
		ByteU5BU5D_t4260760469* L_8 = V_0;
		NullCheck(L_8);
		NullCheck(L_6);
		String_t* L_9 = VirtFuncInvoker3< String_t*, ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(21 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_6, L_7, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))));
		return L_9;
	}
}
// System.Byte[] UnityEngine.WWWTranscoder::Decode(System.Byte[],System.Byte,System.Byte)
extern Il2CppClass* MemoryStream_t418716369_il2cpp_TypeInfo_var;
extern Il2CppClass* WWWTranscoder_t609724394_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t WWWTranscoder_Decode_m2839418928_MetadataUsageId;
extern "C"  ByteU5BU5D_t4260760469* WWWTranscoder_Decode_m2839418928 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___input0, uint8_t ___escapeChar1, uint8_t ___space2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWWTranscoder_Decode_m2839418928_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MemoryStream_t418716369 * V_0 = NULL;
	int32_t V_1 = 0;
	ByteU5BU5D_t4260760469* V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ByteU5BU5D_t4260760469* L_0 = ___input0;
		NullCheck(L_0);
		MemoryStream_t418716369 * L_1 = (MemoryStream_t418716369 *)il2cpp_codegen_object_new(MemoryStream_t418716369_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1061194329(L_1, (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length)))), /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_0009:
	try
	{ // begin try (depth: 1)
		{
			V_1 = 0;
			goto IL_0061;
		}

IL_0010:
		{
			ByteU5BU5D_t4260760469* L_2 = ___input0;
			int32_t L_3 = V_1;
			NullCheck(L_2);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
			int32_t L_4 = L_3;
			uint8_t L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
			uint8_t L_6 = ___space2;
			if ((!(((uint32_t)L_5) == ((uint32_t)L_6))))
			{
				goto IL_0026;
			}
		}

IL_0019:
		{
			MemoryStream_t418716369 * L_7 = V_0;
			NullCheck(L_7);
			VirtActionInvoker1< uint8_t >::Invoke(24 /* System.Void System.IO.MemoryStream::WriteByte(System.Byte) */, L_7, ((int32_t)32));
			goto IL_005d;
		}

IL_0026:
		{
			ByteU5BU5D_t4260760469* L_8 = ___input0;
			int32_t L_9 = V_1;
			NullCheck(L_8);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
			int32_t L_10 = L_9;
			uint8_t L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
			uint8_t L_12 = ___escapeChar1;
			if ((!(((uint32_t)L_11) == ((uint32_t)L_12))))
			{
				goto IL_0054;
			}
		}

IL_002f:
		{
			int32_t L_13 = V_1;
			ByteU5BU5D_t4260760469* L_14 = ___input0;
			NullCheck(L_14);
			if ((((int32_t)((int32_t)((int32_t)L_13+(int32_t)2))) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
			{
				goto IL_0054;
			}
		}

IL_003a:
		{
			int32_t L_15 = V_1;
			V_1 = ((int32_t)((int32_t)L_15+(int32_t)1));
			MemoryStream_t418716369 * L_16 = V_0;
			ByteU5BU5D_t4260760469* L_17 = ___input0;
			int32_t L_18 = V_1;
			int32_t L_19 = L_18;
			V_1 = ((int32_t)((int32_t)L_19+(int32_t)1));
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t609724394_il2cpp_TypeInfo_var);
			uint8_t L_20 = WWWTranscoder_Hex2Byte_m2137781440(NULL /*static, unused*/, L_17, L_19, /*hidden argument*/NULL);
			NullCheck(L_16);
			VirtActionInvoker1< uint8_t >::Invoke(24 /* System.Void System.IO.MemoryStream::WriteByte(System.Byte) */, L_16, L_20);
			goto IL_005d;
		}

IL_0054:
		{
			MemoryStream_t418716369 * L_21 = V_0;
			ByteU5BU5D_t4260760469* L_22 = ___input0;
			int32_t L_23 = V_1;
			NullCheck(L_22);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
			int32_t L_24 = L_23;
			uint8_t L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
			NullCheck(L_21);
			VirtActionInvoker1< uint8_t >::Invoke(24 /* System.Void System.IO.MemoryStream::WriteByte(System.Byte) */, L_21, L_25);
		}

IL_005d:
		{
			int32_t L_26 = V_1;
			V_1 = ((int32_t)((int32_t)L_26+(int32_t)1));
		}

IL_0061:
		{
			int32_t L_27 = V_1;
			ByteU5BU5D_t4260760469* L_28 = ___input0;
			NullCheck(L_28);
			if ((((int32_t)L_27) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_28)->max_length)))))))
			{
				goto IL_0010;
			}
		}

IL_006a:
		{
			MemoryStream_t418716369 * L_29 = V_0;
			NullCheck(L_29);
			ByteU5BU5D_t4260760469* L_30 = VirtFuncInvoker0< ByteU5BU5D_t4260760469* >::Invoke(31 /* System.Byte[] System.IO.MemoryStream::ToArray() */, L_29);
			V_2 = L_30;
			IL2CPP_LEAVE(0x88, FINALLY_007b);
		}

IL_0076:
		{
			; // IL_0076: leave IL_0088
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_007b;
	}

FINALLY_007b:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t418716369 * L_31 = V_0;
			if (!L_31)
			{
				goto IL_0087;
			}
		}

IL_0081:
		{
			MemoryStream_t418716369 * L_32 = V_0;
			NullCheck(L_32);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_32);
		}

IL_0087:
		{
			IL2CPP_END_FINALLY(123)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(123)
	{
		IL2CPP_JUMP_TBL(0x88, IL_0088)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0088:
	{
		ByteU5BU5D_t4260760469* L_33 = V_2;
		return L_33;
	}
}
// System.Boolean UnityEngine.WWWTranscoder::SevenBitClean(System.String,System.Text.Encoding)
extern Il2CppClass* WWWTranscoder_t609724394_il2cpp_TypeInfo_var;
extern const uint32_t WWWTranscoder_SevenBitClean_m1805125217_MetadataUsageId;
extern "C"  bool WWWTranscoder_SevenBitClean_m1805125217 (Il2CppObject * __this /* static, unused */, String_t* ___s0, Encoding_t2012439129 * ___e1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWWTranscoder_SevenBitClean_m1805125217_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Encoding_t2012439129 * L_0 = ___e1;
		String_t* L_1 = ___s0;
		NullCheck(L_0);
		ByteU5BU5D_t4260760469* L_2 = VirtFuncInvoker1< ByteU5BU5D_t4260760469*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_0, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t609724394_il2cpp_TypeInfo_var);
		bool L_3 = WWWTranscoder_SevenBitClean_m945043319(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.WWWTranscoder::SevenBitClean(System.Byte[])
extern "C"  bool WWWTranscoder_SevenBitClean_m945043319 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___input0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0021;
	}

IL_0007:
	{
		ByteU5BU5D_t4260760469* L_0 = ___input0;
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		uint8_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		if ((((int32_t)L_3) < ((int32_t)((int32_t)32))))
		{
			goto IL_001b;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_4 = ___input0;
		int32_t L_5 = V_0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		uint8_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		if ((((int32_t)L_7) <= ((int32_t)((int32_t)126))))
		{
			goto IL_001d;
		}
	}

IL_001b:
	{
		return (bool)0;
	}

IL_001d:
	{
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0021:
	{
		int32_t L_9 = V_0;
		ByteU5BU5D_t4260760469* L_10 = ___input0;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_0007;
		}
	}
	{
		return (bool)1;
	}
}
// System.Void UnityEngine.YieldInstruction::.ctor()
extern "C"  void YieldInstruction__ctor_m539393484 (YieldInstruction_t2048002629 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.YieldInstruction
extern "C" void YieldInstruction_t2048002629_marshal_pinvoke(const YieldInstruction_t2048002629& unmarshaled, YieldInstruction_t2048002629_marshaled_pinvoke& marshaled)
{
}
extern "C" void YieldInstruction_t2048002629_marshal_pinvoke_back(const YieldInstruction_t2048002629_marshaled_pinvoke& marshaled, YieldInstruction_t2048002629& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngine.YieldInstruction
extern "C" void YieldInstruction_t2048002629_marshal_pinvoke_cleanup(YieldInstruction_t2048002629_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.YieldInstruction
extern "C" void YieldInstruction_t2048002629_marshal_com(const YieldInstruction_t2048002629& unmarshaled, YieldInstruction_t2048002629_marshaled_com& marshaled)
{
}
extern "C" void YieldInstruction_t2048002629_marshal_com_back(const YieldInstruction_t2048002629_marshaled_com& marshaled, YieldInstruction_t2048002629& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngine.YieldInstruction
extern "C" void YieldInstruction_t2048002629_marshal_com_cleanup(YieldInstruction_t2048002629_marshaled_com& marshaled)
{
}
// System.Void UnityEngineInternal.GenericStack::.ctor()
extern "C"  void GenericStack__ctor_m2328546233 (GenericStack_t931085639 * __this, const MethodInfo* method)
{
	{
		Stack__ctor_m1821673314(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngineInternal.MathfInternal::.cctor()
extern Il2CppClass* MathfInternal_t4096243933_il2cpp_TypeInfo_var;
extern const uint32_t MathfInternal__cctor_m2600550988_MetadataUsageId;
extern "C"  void MathfInternal__cctor_m2600550988 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MathfInternal__cctor_m2600550988_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		il2cpp_codegen_memory_barrier();
		((MathfInternal_t4096243933_StaticFields*)MathfInternal_t4096243933_il2cpp_TypeInfo_var->static_fields)->set_FloatMinNormal_0((1.17549435E-38f));
		il2cpp_codegen_memory_barrier();
		((MathfInternal_t4096243933_StaticFields*)MathfInternal_t4096243933_il2cpp_TypeInfo_var->static_fields)->set_FloatMinDenormal_1((1.401298E-45f));
		((MathfInternal_t4096243933_StaticFields*)MathfInternal_t4096243933_il2cpp_TypeInfo_var->static_fields)->set_IsFlushToZeroEnabled_2((bool)1);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngineInternal.MathfInternal
extern "C" void MathfInternal_t4096243933_marshal_pinvoke(const MathfInternal_t4096243933& unmarshaled, MathfInternal_t4096243933_marshaled_pinvoke& marshaled)
{
}
extern "C" void MathfInternal_t4096243933_marshal_pinvoke_back(const MathfInternal_t4096243933_marshaled_pinvoke& marshaled, MathfInternal_t4096243933& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngineInternal.MathfInternal
extern "C" void MathfInternal_t4096243933_marshal_pinvoke_cleanup(MathfInternal_t4096243933_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngineInternal.MathfInternal
extern "C" void MathfInternal_t4096243933_marshal_com(const MathfInternal_t4096243933& unmarshaled, MathfInternal_t4096243933_marshaled_com& marshaled)
{
}
extern "C" void MathfInternal_t4096243933_marshal_com_back(const MathfInternal_t4096243933_marshaled_com& marshaled, MathfInternal_t4096243933& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngineInternal.MathfInternal
extern "C" void MathfInternal_t4096243933_marshal_com_cleanup(MathfInternal_t4096243933_marshaled_com& marshaled)
{
}
// System.Delegate UnityEngineInternal.NetFxCoreExtensions::CreateDelegate(System.Reflection.MethodInfo,System.Type,System.Object)
extern "C"  Delegate_t3310234105 * NetFxCoreExtensions_CreateDelegate_m3408605866 (Il2CppObject * __this /* static, unused */, MethodInfo_t * ___self0, Type_t * ___delegateType1, Il2CppObject * ___target2, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___delegateType1;
		Il2CppObject * L_1 = ___target2;
		MethodInfo_t * L_2 = ___self0;
		Delegate_t3310234105 * L_3 = Delegate_CreateDelegate_m3460497746(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Reflection.MethodInfo UnityEngineInternal.NetFxCoreExtensions::GetMethodInfo(System.Delegate)
extern "C"  MethodInfo_t * NetFxCoreExtensions_GetMethodInfo_m1628849205 (Il2CppObject * __this /* static, unused */, Delegate_t3310234105 * ___self0, const MethodInfo* method)
{
	{
		Delegate_t3310234105 * L_0 = ___self0;
		NullCheck(L_0);
		MethodInfo_t * L_1 = Delegate_get_Method_m669548326(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(UnityEngineInternal.TypeInferenceRules)
extern Il2CppClass* TypeInferenceRules_t2889237774_il2cpp_TypeInfo_var;
extern const uint32_t TypeInferenceRuleAttribute__ctor_m1168575159_MetadataUsageId;
extern "C"  void TypeInferenceRuleAttribute__ctor_m1168575159 (TypeInferenceRuleAttribute_t1657757719 * __this, int32_t ___rule0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeInferenceRuleAttribute__ctor_m1168575159_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___rule0;
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(TypeInferenceRules_t2889237774_il2cpp_TypeInfo_var, &L_1);
		NullCheck((Enum_t2862688501 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_2);
		TypeInferenceRuleAttribute__ctor_m2173394041(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(System.String)
extern "C"  void TypeInferenceRuleAttribute__ctor_m2173394041 (TypeInferenceRuleAttribute_t1657757719 * __this, String_t* ___rule0, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___rule0;
		__this->set__rule_0(L_0);
		return;
	}
}
// System.String UnityEngineInternal.TypeInferenceRuleAttribute::ToString()
extern "C"  String_t* TypeInferenceRuleAttribute_ToString_m318752778 (TypeInferenceRuleAttribute_t1657757719 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__rule_0();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
