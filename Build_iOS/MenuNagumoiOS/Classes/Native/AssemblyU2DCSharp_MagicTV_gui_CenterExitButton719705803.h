﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.gui.CenterExitButton
struct  CenterExitButton_t719705803  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject MagicTV.gui.CenterExitButton::buttonCenter
	GameObject_t3674682005 * ___buttonCenter_3;
	// UnityEngine.GameObject MagicTV.gui.CenterExitButton::buttonOriginExit
	GameObject_t3674682005 * ___buttonOriginExit_4;

public:
	inline static int32_t get_offset_of_buttonCenter_3() { return static_cast<int32_t>(offsetof(CenterExitButton_t719705803, ___buttonCenter_3)); }
	inline GameObject_t3674682005 * get_buttonCenter_3() const { return ___buttonCenter_3; }
	inline GameObject_t3674682005 ** get_address_of_buttonCenter_3() { return &___buttonCenter_3; }
	inline void set_buttonCenter_3(GameObject_t3674682005 * value)
	{
		___buttonCenter_3 = value;
		Il2CppCodeGenWriteBarrier(&___buttonCenter_3, value);
	}

	inline static int32_t get_offset_of_buttonOriginExit_4() { return static_cast<int32_t>(offsetof(CenterExitButton_t719705803, ___buttonOriginExit_4)); }
	inline GameObject_t3674682005 * get_buttonOriginExit_4() const { return ___buttonOriginExit_4; }
	inline GameObject_t3674682005 ** get_address_of_buttonOriginExit_4() { return &___buttonOriginExit_4; }
	inline void set_buttonOriginExit_4(GameObject_t3674682005 * value)
	{
		___buttonOriginExit_4 = value;
		Il2CppCodeGenWriteBarrier(&___buttonOriginExit_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
