﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.utils.bytes.ByteListAbstract
struct ByteListAbstract_t980263662;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "AssemblyU2DCSharp_ARM_events_ChangeEventAbstract369667058.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ByteCompareEvents
struct  ByteCompareEvents_t284362262  : public ChangeEventAbstract_t369667058
{
public:
	// ARM.utils.bytes.ByteListAbstract ByteCompareEvents::ByteListSource
	ByteListAbstract_t980263662 * ___ByteListSource_3;
	// System.Single ByteCompareEvents::CaptureRate
	float ___CaptureRate_4;
	// System.Byte[] ByteCompareEvents::_historyBytes
	ByteU5BU5D_t4260760469* ____historyBytes_5;

public:
	inline static int32_t get_offset_of_ByteListSource_3() { return static_cast<int32_t>(offsetof(ByteCompareEvents_t284362262, ___ByteListSource_3)); }
	inline ByteListAbstract_t980263662 * get_ByteListSource_3() const { return ___ByteListSource_3; }
	inline ByteListAbstract_t980263662 ** get_address_of_ByteListSource_3() { return &___ByteListSource_3; }
	inline void set_ByteListSource_3(ByteListAbstract_t980263662 * value)
	{
		___ByteListSource_3 = value;
		Il2CppCodeGenWriteBarrier(&___ByteListSource_3, value);
	}

	inline static int32_t get_offset_of_CaptureRate_4() { return static_cast<int32_t>(offsetof(ByteCompareEvents_t284362262, ___CaptureRate_4)); }
	inline float get_CaptureRate_4() const { return ___CaptureRate_4; }
	inline float* get_address_of_CaptureRate_4() { return &___CaptureRate_4; }
	inline void set_CaptureRate_4(float value)
	{
		___CaptureRate_4 = value;
	}

	inline static int32_t get_offset_of__historyBytes_5() { return static_cast<int32_t>(offsetof(ByteCompareEvents_t284362262, ____historyBytes_5)); }
	inline ByteU5BU5D_t4260760469* get__historyBytes_5() const { return ____historyBytes_5; }
	inline ByteU5BU5D_t4260760469** get_address_of__historyBytes_5() { return &____historyBytes_5; }
	inline void set__historyBytes_5(ByteU5BU5D_t4260760469* value)
	{
		____historyBytes_5 = value;
		Il2CppCodeGenWriteBarrier(&____historyBytes_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
