﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.vo.RequestImagesResultVO
struct RequestImagesResultVO_t1706607919;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.vo.RequestImagesResultVO::.ctor()
extern "C"  void RequestImagesResultVO__ctor_m2210795828 (RequestImagesResultVO_t1706607919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.vo.RequestImagesResultVO::ToString()
extern "C"  String_t* RequestImagesResultVO_ToString_m4231429919 (RequestImagesResultVO_t1706607919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
