﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.device.DeviceControll
struct DeviceControll_t2606264131;

#include "codegen/il2cpp-codegen.h"

// System.Void ARM.device.DeviceControll::.ctor()
extern "C"  void DeviceControll__ctor_m3682509344 (DeviceControll_t2606264131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.DeviceControll::lightToggle(System.Boolean)
extern "C"  void DeviceControll_lightToggle_m1576394399 (Il2CppObject * __this /* static, unused */, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.device.DeviceControll::vibrate()
extern "C"  void DeviceControll_vibrate_m1694966221 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
