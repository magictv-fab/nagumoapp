﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MinhasOfertasManager/<ILoadOfertas>c__Iterator1E
struct U3CILoadOfertasU3Ec__Iterator1E_t1789407802;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MinhasOfertasManager/<ILoadOfertas>c__Iterator1E::.ctor()
extern "C"  void U3CILoadOfertasU3Ec__Iterator1E__ctor_m1974105505 (U3CILoadOfertasU3Ec__Iterator1E_t1789407802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MinhasOfertasManager/<ILoadOfertas>c__Iterator1E::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CILoadOfertasU3Ec__Iterator1E_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1081662683 (U3CILoadOfertasU3Ec__Iterator1E_t1789407802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MinhasOfertasManager/<ILoadOfertas>c__Iterator1E::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CILoadOfertasU3Ec__Iterator1E_System_Collections_IEnumerator_get_Current_m4269667951 (U3CILoadOfertasU3Ec__Iterator1E_t1789407802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MinhasOfertasManager/<ILoadOfertas>c__Iterator1E::MoveNext()
extern "C"  bool U3CILoadOfertasU3Ec__Iterator1E_MoveNext_m1855789211 (U3CILoadOfertasU3Ec__Iterator1E_t1789407802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MinhasOfertasManager/<ILoadOfertas>c__Iterator1E::Dispose()
extern "C"  void U3CILoadOfertasU3Ec__Iterator1E_Dispose_m2933095838 (U3CILoadOfertasU3Ec__Iterator1E_t1789407802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MinhasOfertasManager/<ILoadOfertas>c__Iterator1E::Reset()
extern "C"  void U3CILoadOfertasU3Ec__Iterator1E_Reset_m3915505742 (U3CILoadOfertasU3Ec__Iterator1E_t1789407802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
