﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<ZXing.Aztec.Internal.State>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1962367538(__this, ___l0, method) ((  void (*) (Enumerator_t158314661 *, List_1_t138641891 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ZXing.Aztec.Internal.State>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2886771424(__this, method) ((  void (*) (Enumerator_t158314661 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<ZXing.Aztec.Internal.State>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1384945558(__this, method) ((  Il2CppObject * (*) (Enumerator_t158314661 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ZXing.Aztec.Internal.State>::Dispose()
#define Enumerator_Dispose_m3210744983(__this, method) ((  void (*) (Enumerator_t158314661 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ZXing.Aztec.Internal.State>::VerifyState()
#define Enumerator_VerifyState_m2595860560(__this, method) ((  void (*) (Enumerator_t158314661 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<ZXing.Aztec.Internal.State>::MoveNext()
#define Enumerator_MoveNext_m2175655001(__this, method) ((  bool (*) (Enumerator_t158314661 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<ZXing.Aztec.Internal.State>::get_Current()
#define Enumerator_get_Current_m1856533347(__this, method) ((  State_t3065423635 * (*) (Enumerator_t158314661 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
