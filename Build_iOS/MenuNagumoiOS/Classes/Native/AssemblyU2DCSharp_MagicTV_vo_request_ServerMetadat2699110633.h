﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<MagicTV.vo.MetadataVO>
struct List_1_t3879442550;

#include "AssemblyU2DCSharp_MagicTV_vo_request_ServerRequestPa47984066.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.vo.request.ServerMetadataParamsVO
struct  ServerMetadataParamsVO_t2699110633  : public ServerRequestParamsUpdateVO_t47984066
{
public:
	// System.Collections.Generic.List`1<MagicTV.vo.MetadataVO> MagicTV.vo.request.ServerMetadataParamsVO::metas
	List_1_t3879442550 * ___metas_6;

public:
	inline static int32_t get_offset_of_metas_6() { return static_cast<int32_t>(offsetof(ServerMetadataParamsVO_t2699110633, ___metas_6)); }
	inline List_1_t3879442550 * get_metas_6() const { return ___metas_6; }
	inline List_1_t3879442550 ** get_address_of_metas_6() { return &___metas_6; }
	inline void set_metas_6(List_1_t3879442550 * value)
	{
		___metas_6 = value;
		Il2CppCodeGenWriteBarrier(&___metas_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
