﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// System.Text.RegularExpressions.Regex
struct Regex_t2161232213;
// System.ComponentModel.PropertyDescriptor
struct PropertyDescriptor_t2073374448;
// System.ComponentModel.MemberDescriptor
struct MemberDescriptor_t2617136693;
// System.ComponentModel.TypeDescriptionProvider
struct TypeDescriptionProvider_t3543085017;
// System.Collections.Generic.LinkedList`1<System.ComponentModel.TypeDescriptionProvider>
struct LinkedList_1_t1417720186;
// System.ComponentModel.WeakObjectWrapper
struct WeakObjectWrapper_t1518976226;
// System.Net.Cookie
struct Cookie_t2033273982;
// System.Net.IPAddress
struct IPAddress_t3525271463;
// System.Net.NetworkInformation.NetworkInterface
struct NetworkInterface_t3597375525;
// System.Net.NetworkInformation.LinuxNetworkInterface
struct LinuxNetworkInterface_t908270265;
// System.Net.NetworkInformation.MacOsNetworkInterface
struct MacOsNetworkInterface_t1851933528;
// System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES
struct Win32_IP_ADAPTER_ADDRESSES_t3597816152;
// System.Text.RegularExpressions.Capture
struct Capture_t754001812;
// System.Text.RegularExpressions.Group
struct Group_t2151468941;

#include "mscorlib_System_Array1146569071.h"
#include "System_System_Text_RegularExpressions_Regex2161232213.h"
#include "System_System_ComponentModel_PropertyDescriptor2073374448.h"
#include "System_System_ComponentModel_MemberDescriptor2617136693.h"
#include "System_System_ComponentModel_TypeDescriptionProvid3543085017.h"
#include "System_System_Collections_Generic_LinkedList_1_gen1417720186.h"
#include "System_System_ComponentModel_WeakObjectWrapper1518976226.h"
#include "System_System_Net_Cookie2033273982.h"
#include "System_System_Net_IPAddress3525271463.h"
#include "System_System_Net_NetworkInformation_NetworkInterf3597375525.h"
#include "System_System_Net_NetworkInformation_LinuxNetworkIn908270265.h"
#include "System_System_Net_NetworkInformation_MacOsNetworkI1851933528.h"
#include "System_System_Net_NetworkInformation_Win32_IP_ADAP3597816152.h"
#include "System_System_Security_Cryptography_X509Certificate766901931.h"
#include "System_System_Text_RegularExpressions_Capture754001812.h"
#include "System_System_Text_RegularExpressions_Group2151468941.h"
#include "System_System_Text_RegularExpressions_Mark3811539797.h"
#include "System_System_Uri_UriScheme1290668975.h"

#pragma once
// System.Text.RegularExpressions.Regex[]
struct RegexU5BU5D_t3722511800  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Regex_t2161232213 * m_Items[1];

public:
	inline Regex_t2161232213 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Regex_t2161232213 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Regex_t2161232213 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.ComponentModel.PropertyDescriptor[]
struct PropertyDescriptorU5BU5D_t917702481  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PropertyDescriptor_t2073374448 * m_Items[1];

public:
	inline PropertyDescriptor_t2073374448 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PropertyDescriptor_t2073374448 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PropertyDescriptor_t2073374448 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.ComponentModel.MemberDescriptor[]
struct MemberDescriptorU5BU5D_t1625508184  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MemberDescriptor_t2617136693 * m_Items[1];

public:
	inline MemberDescriptor_t2617136693 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MemberDescriptor_t2617136693 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MemberDescriptor_t2617136693 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.ComponentModel.TypeDescriptionProvider[]
struct TypeDescriptionProviderU5BU5D_t4220469412  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TypeDescriptionProvider_t3543085017 * m_Items[1];

public:
	inline TypeDescriptionProvider_t3543085017 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TypeDescriptionProvider_t3543085017 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TypeDescriptionProvider_t3543085017 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.LinkedList`1<System.ComponentModel.TypeDescriptionProvider>[]
struct LinkedList_1U5BU5D_t1670055775  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LinkedList_1_t1417720186 * m_Items[1];

public:
	inline LinkedList_1_t1417720186 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline LinkedList_1_t1417720186 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, LinkedList_1_t1417720186 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.ComponentModel.WeakObjectWrapper[]
struct WeakObjectWrapperU5BU5D_t3390019927  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) WeakObjectWrapper_t1518976226 * m_Items[1];

public:
	inline WeakObjectWrapper_t1518976226 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline WeakObjectWrapper_t1518976226 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, WeakObjectWrapper_t1518976226 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Net.Cookie[]
struct CookieU5BU5D_t1520446411  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Cookie_t2033273982 * m_Items[1];

public:
	inline Cookie_t2033273982 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Cookie_t2033273982 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Cookie_t2033273982 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Net.IPAddress[]
struct IPAddressU5BU5D_t1215594974  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) IPAddress_t3525271463 * m_Items[1];

public:
	inline IPAddress_t3525271463 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline IPAddress_t3525271463 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, IPAddress_t3525271463 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Net.NetworkInformation.NetworkInterface[]
struct NetworkInterfaceU5BU5D_t611619240  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) NetworkInterface_t3597375525 * m_Items[1];

public:
	inline NetworkInterface_t3597375525 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline NetworkInterface_t3597375525 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, NetworkInterface_t3597375525 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Net.NetworkInformation.LinuxNetworkInterface[]
struct LinuxNetworkInterfaceU5BU5D_t3684363844  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LinuxNetworkInterface_t908270265 * m_Items[1];

public:
	inline LinuxNetworkInterface_t908270265 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline LinuxNetworkInterface_t908270265 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, LinuxNetworkInterface_t908270265 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Net.NetworkInformation.MacOsNetworkInterface[]
struct MacOsNetworkInterfaceU5BU5D_t10308681  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MacOsNetworkInterface_t1851933528 * m_Items[1];

public:
	inline MacOsNetworkInterface_t1851933528 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MacOsNetworkInterface_t1851933528 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MacOsNetworkInterface_t1851933528 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES[]
struct Win32_IP_ADAPTER_ADDRESSESU5BU5D_t3677701705  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Win32_IP_ADAPTER_ADDRESSES_t3597816152 * m_Items[1];

public:
	inline Win32_IP_ADAPTER_ADDRESSES_t3597816152 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Win32_IP_ADAPTER_ADDRESSES_t3597816152 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Win32_IP_ADAPTER_ADDRESSES_t3597816152 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Security.Cryptography.X509Certificates.X509ChainStatus[]
struct X509ChainStatusU5BU5D_t2899776074  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) X509ChainStatus_t766901931  m_Items[1];

public:
	inline X509ChainStatus_t766901931  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline X509ChainStatus_t766901931 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, X509ChainStatus_t766901931  value)
	{
		m_Items[index] = value;
	}
};
// System.Text.RegularExpressions.Capture[]
struct CaptureU5BU5D_t3823141789  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Capture_t754001812 * m_Items[1];

public:
	inline Capture_t754001812 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Capture_t754001812 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Capture_t754001812 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Text.RegularExpressions.Group[]
struct GroupU5BU5D_t1259259808  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Group_t2151468941 * m_Items[1];

public:
	inline Group_t2151468941 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Group_t2151468941 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Group_t2151468941 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Text.RegularExpressions.Mark[]
struct MarkU5BU5D_t581716920  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Mark_t3811539797  m_Items[1];

public:
	inline Mark_t3811539797  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Mark_t3811539797 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Mark_t3811539797  value)
	{
		m_Items[index] = value;
	}
};
// System.Uri/UriScheme[]
struct UriSchemeU5BU5D_t4293835958  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UriScheme_t1290668975  m_Items[1];

public:
	inline UriScheme_t1290668975  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UriScheme_t1290668975 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UriScheme_t1290668975  value)
	{
		m_Items[index] = value;
	}
};
