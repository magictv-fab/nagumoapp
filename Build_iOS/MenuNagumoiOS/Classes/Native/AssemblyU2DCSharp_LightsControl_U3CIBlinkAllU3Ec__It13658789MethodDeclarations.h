﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LightsControl/<IBlinkAll>c__Iterator5F
struct U3CIBlinkAllU3Ec__Iterator5F_t13658789;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void LightsControl/<IBlinkAll>c__Iterator5F::.ctor()
extern "C"  void U3CIBlinkAllU3Ec__Iterator5F__ctor_m3547186070 (U3CIBlinkAllU3Ec__Iterator5F_t13658789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LightsControl/<IBlinkAll>c__Iterator5F::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CIBlinkAllU3Ec__Iterator5F_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4204289094 (U3CIBlinkAllU3Ec__Iterator5F_t13658789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LightsControl/<IBlinkAll>c__Iterator5F::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CIBlinkAllU3Ec__Iterator5F_System_Collections_IEnumerator_get_Current_m1504160730 (U3CIBlinkAllU3Ec__Iterator5F_t13658789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LightsControl/<IBlinkAll>c__Iterator5F::MoveNext()
extern "C"  bool U3CIBlinkAllU3Ec__Iterator5F_MoveNext_m1882692486 (U3CIBlinkAllU3Ec__Iterator5F_t13658789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightsControl/<IBlinkAll>c__Iterator5F::Dispose()
extern "C"  void U3CIBlinkAllU3Ec__Iterator5F_Dispose_m2835030611 (U3CIBlinkAllU3Ec__Iterator5F_t13658789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightsControl/<IBlinkAll>c__Iterator5F::Reset()
extern "C"  void U3CIBlinkAllU3Ec__Iterator5F_Reset_m1193619011 (U3CIBlinkAllU3Ec__Iterator5F_t13658789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
