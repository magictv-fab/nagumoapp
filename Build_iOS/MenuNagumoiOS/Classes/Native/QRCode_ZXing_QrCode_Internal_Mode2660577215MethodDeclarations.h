﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.QrCode.Internal.Mode
struct Mode_t2660577215;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.String
struct String_t;
// ZXing.QrCode.Internal.Version
struct Version_t1953509534;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "QRCode_ZXing_QrCode_Internal_Version1953509534.h"

// System.Void ZXing.QrCode.Internal.Mode::.ctor(System.Int32[],System.Int32,System.String)
extern "C"  void Mode__ctor_m1092282567 (Mode_t2660577215 * __this, Int32U5BU5D_t3230847821* ___characterCountBitsForVersions0, int32_t ___bits1, String_t* ___name2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.QrCode.Internal.Mode ZXing.QrCode.Internal.Mode::forBits(System.Int32)
extern "C"  Mode_t2660577215 * Mode_forBits_m3735414044 (Il2CppObject * __this /* static, unused */, int32_t ___bits0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.QrCode.Internal.Mode::getCharacterCountBits(ZXing.QrCode.Internal.Version)
extern "C"  int32_t Mode_getCharacterCountBits_m1186043814 (Mode_t2660577215 * __this, Version_t1953509534 * ___version0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.QrCode.Internal.Mode::get_Bits()
extern "C"  int32_t Mode_get_Bits_m1605040356 (Mode_t2660577215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.QrCode.Internal.Mode::ToString()
extern "C"  String_t* Mode_ToString_m3620458198 (Mode_t2660577215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.Mode::.cctor()
extern "C"  void Mode__cctor_m2579022896 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
