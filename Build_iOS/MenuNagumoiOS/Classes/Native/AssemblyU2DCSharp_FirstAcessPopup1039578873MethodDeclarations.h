﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FirstAcessPopup
struct FirstAcessPopup_t1039578873;

#include "codegen/il2cpp-codegen.h"

// System.Void FirstAcessPopup::.ctor()
extern "C"  void FirstAcessPopup__ctor_m3133986962 (FirstAcessPopup_t1039578873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FirstAcessPopup::Awake()
extern "C"  void FirstAcessPopup_Awake_m3371592181 (FirstAcessPopup_t1039578873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FirstAcessPopup::Close()
extern "C"  void FirstAcessPopup_Close_m549879208 (FirstAcessPopup_t1039578873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FirstAcessPopup::Update()
extern "C"  void FirstAcessPopup_Update_m96210107 (FirstAcessPopup_t1039578873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
