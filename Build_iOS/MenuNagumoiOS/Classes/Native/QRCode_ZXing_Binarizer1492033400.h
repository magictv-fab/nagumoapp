﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.LuminanceSource
struct LuminanceSource_t1231523093;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Binarizer
struct  Binarizer_t1492033400  : public Il2CppObject
{
public:
	// ZXing.LuminanceSource ZXing.Binarizer::source
	LuminanceSource_t1231523093 * ___source_0;

public:
	inline static int32_t get_offset_of_source_0() { return static_cast<int32_t>(offsetof(Binarizer_t1492033400, ___source_0)); }
	inline LuminanceSource_t1231523093 * get_source_0() const { return ___source_0; }
	inline LuminanceSource_t1231523093 ** get_address_of_source_0() { return &___source_0; }
	inline void set_source_0(LuminanceSource_t1231523093 * value)
	{
		___source_0 = value;
		Il2CppCodeGenWriteBarrier(&___source_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
