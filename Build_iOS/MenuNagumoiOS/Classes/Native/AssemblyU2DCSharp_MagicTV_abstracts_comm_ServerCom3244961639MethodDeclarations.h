﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.abstracts.comm.ServerCommunicationAbstract
struct ServerCommunicationAbstract_t3244961639;
// MagicTV.abstracts.comm.ServerCommunicationAbstract/OnUpdateRequestCompleteEventHandler
struct OnUpdateRequestCompleteEventHandler_t239303760;
// MagicTV.vo.RevisionInfoVO
struct RevisionInfoVO_t1983749152;
// MagicTV.abstracts.comm.ServerCommunicationAbstract/OnUpdateRequestErrorEventHandler
struct OnUpdateRequestErrorEventHandler_t1521989041;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_abstracts_comm_ServerComm239303760.h"
#include "AssemblyU2DCSharp_MagicTV_vo_RevisionInfoVO1983749152.h"
#include "AssemblyU2DCSharp_MagicTV_abstracts_comm_ServerCom1521989041.h"
#include "mscorlib_System_String7231557.h"

// System.Void MagicTV.abstracts.comm.ServerCommunicationAbstract::.ctor()
extern "C"  void ServerCommunicationAbstract__ctor_m3062178476 (ServerCommunicationAbstract_t3244961639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.comm.ServerCommunicationAbstract::AddUpdateRequestCompleteEventHandler(MagicTV.abstracts.comm.ServerCommunicationAbstract/OnUpdateRequestCompleteEventHandler)
extern "C"  void ServerCommunicationAbstract_AddUpdateRequestCompleteEventHandler_m2902117558 (ServerCommunicationAbstract_t3244961639 * __this, OnUpdateRequestCompleteEventHandler_t239303760 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.comm.ServerCommunicationAbstract::RemoveUpdateRequestCompleteEventHandler(MagicTV.abstracts.comm.ServerCommunicationAbstract/OnUpdateRequestCompleteEventHandler)
extern "C"  void ServerCommunicationAbstract_RemoveUpdateRequestCompleteEventHandler_m3250921765 (ServerCommunicationAbstract_t3244961639 * __this, OnUpdateRequestCompleteEventHandler_t239303760 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.comm.ServerCommunicationAbstract::RaiseUpdateRequestComplete(MagicTV.vo.RevisionInfoVO)
extern "C"  void ServerCommunicationAbstract_RaiseUpdateRequestComplete_m4099999727 (ServerCommunicationAbstract_t3244961639 * __this, RevisionInfoVO_t1983749152 * ___revisionInfoVO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.comm.ServerCommunicationAbstract::AddUpdateRequestErrorEventHandler(MagicTV.abstracts.comm.ServerCommunicationAbstract/OnUpdateRequestErrorEventHandler)
extern "C"  void ServerCommunicationAbstract_AddUpdateRequestErrorEventHandler_m2769543972 (ServerCommunicationAbstract_t3244961639 * __this, OnUpdateRequestErrorEventHandler_t1521989041 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.comm.ServerCommunicationAbstract::RemoveUpdateRequestErrorEventHandler(MagicTV.abstracts.comm.ServerCommunicationAbstract/OnUpdateRequestErrorEventHandler)
extern "C"  void ServerCommunicationAbstract_RemoveUpdateRequestErrorEventHandler_m3818256083 (ServerCommunicationAbstract_t3244961639 * __this, OnUpdateRequestErrorEventHandler_t1521989041 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.comm.ServerCommunicationAbstract::RaiseUpdateRequestError(System.String)
extern "C"  void ServerCommunicationAbstract_RaiseUpdateRequestError_m3294923978 (ServerCommunicationAbstract_t3244961639 * __this, String_t* ___descriptionError0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.comm.ServerCommunicationAbstract::RaiseUpdateTokenComplete(System.Boolean)
extern "C"  void ServerCommunicationAbstract_RaiseUpdateTokenComplete_m1821634956 (ServerCommunicationAbstract_t3244961639 * __this, bool ___success0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
