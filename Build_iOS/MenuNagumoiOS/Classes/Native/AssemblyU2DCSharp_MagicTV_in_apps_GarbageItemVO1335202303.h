﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.animation.ViewTimeControllAbstract
struct ViewTimeControllAbstract_t2357615493;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.in_apps.GarbageItemVO
struct  GarbageItemVO_t1335202303  : public Il2CppObject
{
public:
	// ARM.animation.ViewTimeControllAbstract MagicTV.in_apps.GarbageItemVO::presentation
	ViewTimeControllAbstract_t2357615493 * ___presentation_0;
	// System.Single MagicTV.in_apps.GarbageItemVO::timerCount
	float ___timerCount_1;
	// System.Boolean MagicTV.in_apps.GarbageItemVO::isTrash
	bool ___isTrash_2;
	// System.Boolean MagicTV.in_apps.GarbageItemVO::isDied
	bool ___isDied_3;

public:
	inline static int32_t get_offset_of_presentation_0() { return static_cast<int32_t>(offsetof(GarbageItemVO_t1335202303, ___presentation_0)); }
	inline ViewTimeControllAbstract_t2357615493 * get_presentation_0() const { return ___presentation_0; }
	inline ViewTimeControllAbstract_t2357615493 ** get_address_of_presentation_0() { return &___presentation_0; }
	inline void set_presentation_0(ViewTimeControllAbstract_t2357615493 * value)
	{
		___presentation_0 = value;
		Il2CppCodeGenWriteBarrier(&___presentation_0, value);
	}

	inline static int32_t get_offset_of_timerCount_1() { return static_cast<int32_t>(offsetof(GarbageItemVO_t1335202303, ___timerCount_1)); }
	inline float get_timerCount_1() const { return ___timerCount_1; }
	inline float* get_address_of_timerCount_1() { return &___timerCount_1; }
	inline void set_timerCount_1(float value)
	{
		___timerCount_1 = value;
	}

	inline static int32_t get_offset_of_isTrash_2() { return static_cast<int32_t>(offsetof(GarbageItemVO_t1335202303, ___isTrash_2)); }
	inline bool get_isTrash_2() const { return ___isTrash_2; }
	inline bool* get_address_of_isTrash_2() { return &___isTrash_2; }
	inline void set_isTrash_2(bool value)
	{
		___isTrash_2 = value;
	}

	inline static int32_t get_offset_of_isDied_3() { return static_cast<int32_t>(offsetof(GarbageItemVO_t1335202303, ___isDied_3)); }
	inline bool get_isDied_3() const { return ___isDied_3; }
	inline bool* get_address_of_isDied_3() { return &___isDied_3; }
	inline void set_isDied_3(bool value)
	{
		___isDied_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
