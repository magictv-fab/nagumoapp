﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ARM_abstracts_components_General3900398046.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.abstracts.screens.GeneralScreenAbstract
struct  GeneralScreenAbstract_t2842388060  : public GeneralComponentsAbstract_t3900398046
{
public:
	// System.Boolean MagicTV.abstracts.screens.GeneralScreenAbstract::_isVisible
	bool ____isVisible_4;

public:
	inline static int32_t get_offset_of__isVisible_4() { return static_cast<int32_t>(offsetof(GeneralScreenAbstract_t2842388060, ____isVisible_4)); }
	inline bool get__isVisible_4() const { return ____isVisible_4; }
	inline bool* get_address_of__isVisible_4() { return &____isVisible_4; }
	inline void set__isVisible_4(bool value)
	{
		____isVisible_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
