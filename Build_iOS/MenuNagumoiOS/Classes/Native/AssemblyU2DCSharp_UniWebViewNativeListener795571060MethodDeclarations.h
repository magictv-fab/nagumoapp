﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniWebViewNativeListener
struct UniWebViewNativeListener_t795571060;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UniWebViewNativeListener795571060.h"
#include "mscorlib_System_String7231557.h"

// System.Void UniWebViewNativeListener::.ctor()
extern "C"  void UniWebViewNativeListener__ctor_m693026215 (UniWebViewNativeListener_t795571060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewNativeListener::.cctor()
extern "C"  void UniWebViewNativeListener__cctor_m3821847270 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewNativeListener::AddListener(UniWebViewNativeListener)
extern "C"  void UniWebViewNativeListener_AddListener_m1007533830 (Il2CppObject * __this /* static, unused */, UniWebViewNativeListener_t795571060 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewNativeListener::RemoveListener(System.String)
extern "C"  void UniWebViewNativeListener_RemoveListener_m970096141 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniWebViewNativeListener UniWebViewNativeListener::GetListener(System.String)
extern "C"  UniWebViewNativeListener_t795571060 * UniWebViewNativeListener_GetListener_m1254123324 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UniWebViewNativeListener::get_Name()
extern "C"  String_t* UniWebViewNativeListener_get_Name_m1366243892 (UniWebViewNativeListener_t795571060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewNativeListener::PageStarted(System.String)
extern "C"  void UniWebViewNativeListener_PageStarted_m1711808043 (UniWebViewNativeListener_t795571060 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewNativeListener::PageFinished(System.String)
extern "C"  void UniWebViewNativeListener_PageFinished_m2951358756 (UniWebViewNativeListener_t795571060 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewNativeListener::PageErrorReceived(System.String)
extern "C"  void UniWebViewNativeListener_PageErrorReceived_m2441738595 (UniWebViewNativeListener_t795571060 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewNativeListener::ShowTransitionFinished(System.String)
extern "C"  void UniWebViewNativeListener_ShowTransitionFinished_m841233185 (UniWebViewNativeListener_t795571060 * __this, String_t* ___identifer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewNativeListener::HideTransitionFinished(System.String)
extern "C"  void UniWebViewNativeListener_HideTransitionFinished_m379669884 (UniWebViewNativeListener_t795571060 * __this, String_t* ___identifer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewNativeListener::AnimateToFinished(System.String)
extern "C"  void UniWebViewNativeListener_AnimateToFinished_m3494175439 (UniWebViewNativeListener_t795571060 * __this, String_t* ___identifer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewNativeListener::AddJavaScriptFinished(System.String)
extern "C"  void UniWebViewNativeListener_AddJavaScriptFinished_m486046909 (UniWebViewNativeListener_t795571060 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewNativeListener::EvalJavaScriptFinished(System.String)
extern "C"  void UniWebViewNativeListener_EvalJavaScriptFinished_m496678442 (UniWebViewNativeListener_t795571060 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewNativeListener::MessageReceived(System.String)
extern "C"  void UniWebViewNativeListener_MessageReceived_m2872303957 (UniWebViewNativeListener_t795571060 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewNativeListener::WebViewKeyDown(System.String)
extern "C"  void UniWebViewNativeListener_WebViewKeyDown_m2633188381 (UniWebViewNativeListener_t795571060 * __this, String_t* ___keyCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewNativeListener::WebViewDone(System.String)
extern "C"  void UniWebViewNativeListener_WebViewDone_m3847132866 (UniWebViewNativeListener_t795571060 * __this, String_t* ___param0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniWebViewNativeListener::WebContentProcessDidTerminate(System.String)
extern "C"  void UniWebViewNativeListener_WebContentProcessDidTerminate_m1411322097 (UniWebViewNativeListener_t795571060 * __this, String_t* ___param0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
