﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Cadastro/<GoLevel>c__Iterator4B
struct U3CGoLevelU3Ec__Iterator4B_t3037222391;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Cadastro/<GoLevel>c__Iterator4B::.ctor()
extern "C"  void U3CGoLevelU3Ec__Iterator4B__ctor_m2134927188 (U3CGoLevelU3Ec__Iterator4B_t3037222391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Cadastro/<GoLevel>c__Iterator4B::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGoLevelU3Ec__Iterator4B_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m160135294 (U3CGoLevelU3Ec__Iterator4B_t3037222391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Cadastro/<GoLevel>c__Iterator4B::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGoLevelU3Ec__Iterator4B_System_Collections_IEnumerator_get_Current_m3792320530 (U3CGoLevelU3Ec__Iterator4B_t3037222391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Cadastro/<GoLevel>c__Iterator4B::MoveNext()
extern "C"  bool U3CGoLevelU3Ec__Iterator4B_MoveNext_m3163063840 (U3CGoLevelU3Ec__Iterator4B_t3037222391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cadastro/<GoLevel>c__Iterator4B::Dispose()
extern "C"  void U3CGoLevelU3Ec__Iterator4B_Dispose_m2863910545 (U3CGoLevelU3Ec__Iterator4B_t3037222391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cadastro/<GoLevel>c__Iterator4B::Reset()
extern "C"  void U3CGoLevelU3Ec__Iterator4B_Reset_m4076327425 (U3CGoLevelU3Ec__Iterator4B_t3037222391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
