﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.QrCode.Internal.FinderPattern
struct FinderPattern_t4119758992;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.FinderPatternInfo
struct  FinderPatternInfo_t3964498398  : public Il2CppObject
{
public:
	// ZXing.QrCode.Internal.FinderPattern ZXing.QrCode.Internal.FinderPatternInfo::bottomLeft
	FinderPattern_t4119758992 * ___bottomLeft_0;
	// ZXing.QrCode.Internal.FinderPattern ZXing.QrCode.Internal.FinderPatternInfo::topLeft
	FinderPattern_t4119758992 * ___topLeft_1;
	// ZXing.QrCode.Internal.FinderPattern ZXing.QrCode.Internal.FinderPatternInfo::topRight
	FinderPattern_t4119758992 * ___topRight_2;

public:
	inline static int32_t get_offset_of_bottomLeft_0() { return static_cast<int32_t>(offsetof(FinderPatternInfo_t3964498398, ___bottomLeft_0)); }
	inline FinderPattern_t4119758992 * get_bottomLeft_0() const { return ___bottomLeft_0; }
	inline FinderPattern_t4119758992 ** get_address_of_bottomLeft_0() { return &___bottomLeft_0; }
	inline void set_bottomLeft_0(FinderPattern_t4119758992 * value)
	{
		___bottomLeft_0 = value;
		Il2CppCodeGenWriteBarrier(&___bottomLeft_0, value);
	}

	inline static int32_t get_offset_of_topLeft_1() { return static_cast<int32_t>(offsetof(FinderPatternInfo_t3964498398, ___topLeft_1)); }
	inline FinderPattern_t4119758992 * get_topLeft_1() const { return ___topLeft_1; }
	inline FinderPattern_t4119758992 ** get_address_of_topLeft_1() { return &___topLeft_1; }
	inline void set_topLeft_1(FinderPattern_t4119758992 * value)
	{
		___topLeft_1 = value;
		Il2CppCodeGenWriteBarrier(&___topLeft_1, value);
	}

	inline static int32_t get_offset_of_topRight_2() { return static_cast<int32_t>(offsetof(FinderPatternInfo_t3964498398, ___topRight_2)); }
	inline FinderPattern_t4119758992 * get_topRight_2() const { return ___topRight_2; }
	inline FinderPattern_t4119758992 ** get_address_of_topRight_2() { return &___topRight_2; }
	inline void set_topRight_2(FinderPattern_t4119758992 * value)
	{
		___topRight_2 = value;
		Il2CppCodeGenWriteBarrier(&___topRight_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
