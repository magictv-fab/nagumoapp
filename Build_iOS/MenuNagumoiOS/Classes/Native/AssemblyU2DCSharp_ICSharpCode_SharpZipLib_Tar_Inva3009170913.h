﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Tar_TarE3143306048.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Tar.InvalidHeaderException
struct  InvalidHeaderException_t3009170913  : public TarException_t3143306048
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
