﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.OneD.RSS.FinderPattern
struct FinderPattern_t3366792524;

#include "QRCode_ZXing_OneD_RSS_DataCharacter770728801.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.RSS.Pair
struct  Pair_t1045409632  : public DataCharacter_t770728801
{
public:
	// ZXing.OneD.RSS.FinderPattern ZXing.OneD.RSS.Pair::<FinderPattern>k__BackingField
	FinderPattern_t3366792524 * ___U3CFinderPatternU3Ek__BackingField_2;
	// System.Int32 ZXing.OneD.RSS.Pair::<Count>k__BackingField
	int32_t ___U3CCountU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CFinderPatternU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Pair_t1045409632, ___U3CFinderPatternU3Ek__BackingField_2)); }
	inline FinderPattern_t3366792524 * get_U3CFinderPatternU3Ek__BackingField_2() const { return ___U3CFinderPatternU3Ek__BackingField_2; }
	inline FinderPattern_t3366792524 ** get_address_of_U3CFinderPatternU3Ek__BackingField_2() { return &___U3CFinderPatternU3Ek__BackingField_2; }
	inline void set_U3CFinderPatternU3Ek__BackingField_2(FinderPattern_t3366792524 * value)
	{
		___U3CFinderPatternU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CFinderPatternU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CCountU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Pair_t1045409632, ___U3CCountU3Ek__BackingField_3)); }
	inline int32_t get_U3CCountU3Ek__BackingField_3() const { return ___U3CCountU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CCountU3Ek__BackingField_3() { return &___U3CCountU3Ek__BackingField_3; }
	inline void set_U3CCountU3Ek__BackingField_3(int32_t value)
	{
		___U3CCountU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
