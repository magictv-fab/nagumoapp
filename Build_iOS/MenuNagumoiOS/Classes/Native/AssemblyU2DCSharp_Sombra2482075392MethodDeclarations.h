﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Sombra
struct Sombra_t2482075392;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void Sombra::.ctor()
extern "C"  void Sombra__ctor_m3138112923 (Sombra_t2482075392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Sombra::Start()
extern "C"  Il2CppObject * Sombra_Start_m284725075 (Sombra_t2482075392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sombra::Update()
extern "C"  void Sombra_Update_m224114898 (Sombra_t2482075392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
