﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FakeDeviceFileInfo/<getBundles>c__AnonStorey9F
struct U3CgetBundlesU3Ec__AnonStorey9F_t2310048435;
// MagicTV.vo.BundleVO
struct BundleVO_t1984518073;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_vo_BundleVO1984518073.h"

// System.Void FakeDeviceFileInfo/<getBundles>c__AnonStorey9F::.ctor()
extern "C"  void U3CgetBundlesU3Ec__AnonStorey9F__ctor_m3605281608 (U3CgetBundlesU3Ec__AnonStorey9F_t2310048435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FakeDeviceFileInfo/<getBundles>c__AnonStorey9F::<>m__4B(MagicTV.vo.BundleVO)
extern "C"  bool U3CgetBundlesU3Ec__AnonStorey9F_U3CU3Em__4B_m2696123510 (U3CgetBundlesU3Ec__AnonStorey9F_t2310048435 * __this, BundleVO_t1984518073 * ___bx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
