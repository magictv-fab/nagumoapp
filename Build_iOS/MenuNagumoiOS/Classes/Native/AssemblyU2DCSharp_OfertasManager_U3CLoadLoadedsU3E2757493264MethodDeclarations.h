﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertasManager/<LoadLoadeds>c__Iterator6C
struct U3CLoadLoadedsU3Ec__Iterator6C_t2757493264;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OfertasManager/<LoadLoadeds>c__Iterator6C::.ctor()
extern "C"  void U3CLoadLoadedsU3Ec__Iterator6C__ctor_m504022363 (U3CLoadLoadedsU3Ec__Iterator6C_t2757493264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManager/<LoadLoadeds>c__Iterator6C::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadLoadedsU3Ec__Iterator6C_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2777106199 (U3CLoadLoadedsU3Ec__Iterator6C_t2757493264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManager/<LoadLoadeds>c__Iterator6C::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadLoadedsU3Ec__Iterator6C_System_Collections_IEnumerator_get_Current_m952129707 (U3CLoadLoadedsU3Ec__Iterator6C_t2757493264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OfertasManager/<LoadLoadeds>c__Iterator6C::MoveNext()
extern "C"  bool U3CLoadLoadedsU3Ec__Iterator6C_MoveNext_m3763931065 (U3CLoadLoadedsU3Ec__Iterator6C_t2757493264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManager/<LoadLoadeds>c__Iterator6C::Dispose()
extern "C"  void U3CLoadLoadedsU3Ec__Iterator6C_Dispose_m3227436760 (U3CLoadLoadedsU3Ec__Iterator6C_t2757493264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManager/<LoadLoadeds>c__Iterator6C::Reset()
extern "C"  void U3CLoadLoadedsU3Ec__Iterator6C_Reset_m2445422600 (U3CLoadLoadedsU3Ec__Iterator6C_t2757493264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
