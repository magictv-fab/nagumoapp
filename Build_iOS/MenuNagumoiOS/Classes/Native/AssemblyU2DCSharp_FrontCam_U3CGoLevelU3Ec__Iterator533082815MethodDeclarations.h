﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FrontCam/<GoLevel>c__Iterator58
struct U3CGoLevelU3Ec__Iterator58_t533082815;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void FrontCam/<GoLevel>c__Iterator58::.ctor()
extern "C"  void U3CGoLevelU3Ec__Iterator58__ctor_m3485100940 (U3CGoLevelU3Ec__Iterator58_t533082815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FrontCam/<GoLevel>c__Iterator58::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGoLevelU3Ec__Iterator58_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m894412614 (U3CGoLevelU3Ec__Iterator58_t533082815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FrontCam/<GoLevel>c__Iterator58::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGoLevelU3Ec__Iterator58_System_Collections_IEnumerator_get_Current_m3394565850 (U3CGoLevelU3Ec__Iterator58_t533082815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FrontCam/<GoLevel>c__Iterator58::MoveNext()
extern "C"  bool U3CGoLevelU3Ec__Iterator58_MoveNext_m3820582632 (U3CGoLevelU3Ec__Iterator58_t533082815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FrontCam/<GoLevel>c__Iterator58::Dispose()
extern "C"  void U3CGoLevelU3Ec__Iterator58_Dispose_m3300762825 (U3CGoLevelU3Ec__Iterator58_t533082815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FrontCam/<GoLevel>c__Iterator58::Reset()
extern "C"  void U3CGoLevelU3Ec__Iterator58_Reset_m1131533881 (U3CGoLevelU3Ec__Iterator58_t533082815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
