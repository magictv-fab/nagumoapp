﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GameObjectHasChildren
struct  GameObjectHasChildren_t3474747126  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GameObjectHasChildren::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GameObjectHasChildren::trueEvent
	FsmEvent_t2133468028 * ___trueEvent_10;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GameObjectHasChildren::falseEvent
	FsmEvent_t2133468028 * ___falseEvent_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GameObjectHasChildren::storeResult
	FsmBool_t1075959796 * ___storeResult_12;
	// System.Boolean HutongGames.PlayMaker.Actions.GameObjectHasChildren::everyFrame
	bool ___everyFrame_13;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(GameObjectHasChildren_t3474747126, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_trueEvent_10() { return static_cast<int32_t>(offsetof(GameObjectHasChildren_t3474747126, ___trueEvent_10)); }
	inline FsmEvent_t2133468028 * get_trueEvent_10() const { return ___trueEvent_10; }
	inline FsmEvent_t2133468028 ** get_address_of_trueEvent_10() { return &___trueEvent_10; }
	inline void set_trueEvent_10(FsmEvent_t2133468028 * value)
	{
		___trueEvent_10 = value;
		Il2CppCodeGenWriteBarrier(&___trueEvent_10, value);
	}

	inline static int32_t get_offset_of_falseEvent_11() { return static_cast<int32_t>(offsetof(GameObjectHasChildren_t3474747126, ___falseEvent_11)); }
	inline FsmEvent_t2133468028 * get_falseEvent_11() const { return ___falseEvent_11; }
	inline FsmEvent_t2133468028 ** get_address_of_falseEvent_11() { return &___falseEvent_11; }
	inline void set_falseEvent_11(FsmEvent_t2133468028 * value)
	{
		___falseEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___falseEvent_11, value);
	}

	inline static int32_t get_offset_of_storeResult_12() { return static_cast<int32_t>(offsetof(GameObjectHasChildren_t3474747126, ___storeResult_12)); }
	inline FsmBool_t1075959796 * get_storeResult_12() const { return ___storeResult_12; }
	inline FsmBool_t1075959796 ** get_address_of_storeResult_12() { return &___storeResult_12; }
	inline void set_storeResult_12(FsmBool_t1075959796 * value)
	{
		___storeResult_12 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_12, value);
	}

	inline static int32_t get_offset_of_everyFrame_13() { return static_cast<int32_t>(offsetof(GameObjectHasChildren_t3474747126, ___everyFrame_13)); }
	inline bool get_everyFrame_13() const { return ___everyFrame_13; }
	inline bool* get_address_of_everyFrame_13() { return &___everyFrame_13; }
	inline void set_everyFrame_13(bool value)
	{
		___everyFrame_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
