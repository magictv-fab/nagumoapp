﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<MagicTV.globals.Perspective,System.Object>
struct Dictionary_2_t2510884092;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va442717500.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<MagicTV.globals.Perspective,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m4218311554_gshared (Enumerator_t442717500 * __this, Dictionary_2_t2510884092 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m4218311554(__this, ___host0, method) ((  void (*) (Enumerator_t442717500 *, Dictionary_2_t2510884092 *, const MethodInfo*))Enumerator__ctor_m4218311554_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<MagicTV.globals.Perspective,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m836226591_gshared (Enumerator_t442717500 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m836226591(__this, method) ((  Il2CppObject * (*) (Enumerator_t442717500 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m836226591_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<MagicTV.globals.Perspective,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m343134195_gshared (Enumerator_t442717500 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m343134195(__this, method) ((  void (*) (Enumerator_t442717500 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m343134195_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<MagicTV.globals.Perspective,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3259883172_gshared (Enumerator_t442717500 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3259883172(__this, method) ((  void (*) (Enumerator_t442717500 *, const MethodInfo*))Enumerator_Dispose_m3259883172_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<MagicTV.globals.Perspective,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3548565215_gshared (Enumerator_t442717500 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3548565215(__this, method) ((  bool (*) (Enumerator_t442717500 *, const MethodInfo*))Enumerator_MoveNext_m3548565215_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<MagicTV.globals.Perspective,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m2656210499_gshared (Enumerator_t442717500 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2656210499(__this, method) ((  Il2CppObject * (*) (Enumerator_t442717500 *, const MethodInfo*))Enumerator_get_Current_m2656210499_gshared)(__this, method)
