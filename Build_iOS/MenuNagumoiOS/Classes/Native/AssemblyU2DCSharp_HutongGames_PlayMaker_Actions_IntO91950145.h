﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_In1437297806.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.IntOperator
struct  IntOperator_t91950145  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.IntOperator::integer1
	FsmInt_t1596138449 * ___integer1_9;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.IntOperator::integer2
	FsmInt_t1596138449 * ___integer2_10;
	// HutongGames.PlayMaker.Actions.IntOperator/Operation HutongGames.PlayMaker.Actions.IntOperator::operation
	int32_t ___operation_11;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.IntOperator::storeResult
	FsmInt_t1596138449 * ___storeResult_12;
	// System.Boolean HutongGames.PlayMaker.Actions.IntOperator::everyFrame
	bool ___everyFrame_13;

public:
	inline static int32_t get_offset_of_integer1_9() { return static_cast<int32_t>(offsetof(IntOperator_t91950145, ___integer1_9)); }
	inline FsmInt_t1596138449 * get_integer1_9() const { return ___integer1_9; }
	inline FsmInt_t1596138449 ** get_address_of_integer1_9() { return &___integer1_9; }
	inline void set_integer1_9(FsmInt_t1596138449 * value)
	{
		___integer1_9 = value;
		Il2CppCodeGenWriteBarrier(&___integer1_9, value);
	}

	inline static int32_t get_offset_of_integer2_10() { return static_cast<int32_t>(offsetof(IntOperator_t91950145, ___integer2_10)); }
	inline FsmInt_t1596138449 * get_integer2_10() const { return ___integer2_10; }
	inline FsmInt_t1596138449 ** get_address_of_integer2_10() { return &___integer2_10; }
	inline void set_integer2_10(FsmInt_t1596138449 * value)
	{
		___integer2_10 = value;
		Il2CppCodeGenWriteBarrier(&___integer2_10, value);
	}

	inline static int32_t get_offset_of_operation_11() { return static_cast<int32_t>(offsetof(IntOperator_t91950145, ___operation_11)); }
	inline int32_t get_operation_11() const { return ___operation_11; }
	inline int32_t* get_address_of_operation_11() { return &___operation_11; }
	inline void set_operation_11(int32_t value)
	{
		___operation_11 = value;
	}

	inline static int32_t get_offset_of_storeResult_12() { return static_cast<int32_t>(offsetof(IntOperator_t91950145, ___storeResult_12)); }
	inline FsmInt_t1596138449 * get_storeResult_12() const { return ___storeResult_12; }
	inline FsmInt_t1596138449 ** get_address_of_storeResult_12() { return &___storeResult_12; }
	inline void set_storeResult_12(FsmInt_t1596138449 * value)
	{
		___storeResult_12 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_12, value);
	}

	inline static int32_t get_offset_of_everyFrame_13() { return static_cast<int32_t>(offsetof(IntOperator_t91950145, ___everyFrame_13)); }
	inline bool get_everyFrame_13() const { return ___everyFrame_13; }
	inline bool* get_address_of_everyFrame_13() { return &___everyFrame_13; }
	inline void set_everyFrame_13(bool value)
	{
		___everyFrame_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
