﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.components.GroupComponentsManager
struct GroupComponentsManager_t3788191382;
// ARM.abstracts.ProgressEventsAbstract[]
struct ProgressEventsAbstractU5BU5D_t3177844181;

#include "AssemblyU2DCSharp_ARM_abstracts_ProgressEventsAbst2129719228.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.components.GroupProgressComponentsManager
struct  GroupProgressComponentsManager_t1430043881  : public ProgressEventsAbstract_t2129719228
{
public:
	// ARM.components.GroupComponentsManager ARM.components.GroupProgressComponentsManager::groupComponentManager
	GroupComponentsManager_t3788191382 * ___groupComponentManager_14;
	// ARM.abstracts.ProgressEventsAbstract[] ARM.components.GroupProgressComponentsManager::_components
	ProgressEventsAbstractU5BU5D_t3177844181* ____components_15;

public:
	inline static int32_t get_offset_of_groupComponentManager_14() { return static_cast<int32_t>(offsetof(GroupProgressComponentsManager_t1430043881, ___groupComponentManager_14)); }
	inline GroupComponentsManager_t3788191382 * get_groupComponentManager_14() const { return ___groupComponentManager_14; }
	inline GroupComponentsManager_t3788191382 ** get_address_of_groupComponentManager_14() { return &___groupComponentManager_14; }
	inline void set_groupComponentManager_14(GroupComponentsManager_t3788191382 * value)
	{
		___groupComponentManager_14 = value;
		Il2CppCodeGenWriteBarrier(&___groupComponentManager_14, value);
	}

	inline static int32_t get_offset_of__components_15() { return static_cast<int32_t>(offsetof(GroupProgressComponentsManager_t1430043881, ____components_15)); }
	inline ProgressEventsAbstractU5BU5D_t3177844181* get__components_15() const { return ____components_15; }
	inline ProgressEventsAbstractU5BU5D_t3177844181** get_address_of__components_15() { return &____components_15; }
	inline void set__components_15(ProgressEventsAbstractU5BU5D_t3177844181* value)
	{
		____components_15 = value;
		Il2CppCodeGenWriteBarrier(&____components_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
