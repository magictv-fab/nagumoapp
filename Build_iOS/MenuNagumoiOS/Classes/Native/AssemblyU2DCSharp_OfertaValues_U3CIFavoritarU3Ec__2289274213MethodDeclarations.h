﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertaValues/<IFavoritar>c__Iterator23
struct U3CIFavoritarU3Ec__Iterator23_t2289274213;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OfertaValues/<IFavoritar>c__Iterator23::.ctor()
extern "C"  void U3CIFavoritarU3Ec__Iterator23__ctor_m725989590 (U3CIFavoritarU3Ec__Iterator23_t2289274213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertaValues/<IFavoritar>c__Iterator23::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CIFavoritarU3Ec__Iterator23_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m841953030 (U3CIFavoritarU3Ec__Iterator23_t2289274213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertaValues/<IFavoritar>c__Iterator23::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CIFavoritarU3Ec__Iterator23_System_Collections_IEnumerator_get_Current_m1354497690 (U3CIFavoritarU3Ec__Iterator23_t2289274213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OfertaValues/<IFavoritar>c__Iterator23::MoveNext()
extern "C"  bool U3CIFavoritarU3Ec__Iterator23_MoveNext_m3833372230 (U3CIFavoritarU3Ec__Iterator23_t2289274213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertaValues/<IFavoritar>c__Iterator23::Dispose()
extern "C"  void U3CIFavoritarU3Ec__Iterator23_Dispose_m1789577107 (U3CIFavoritarU3Ec__Iterator23_t2289274213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertaValues/<IFavoritar>c__Iterator23::Reset()
extern "C"  void U3CIFavoritarU3Ec__Iterator23_Reset_m2667389827 (U3CIFavoritarU3Ec__Iterator23_t2289274213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
