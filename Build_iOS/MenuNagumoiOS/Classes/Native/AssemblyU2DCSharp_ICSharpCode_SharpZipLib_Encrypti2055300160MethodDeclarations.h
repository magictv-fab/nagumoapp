﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Encryption.PkzipClassicDecryptCryptoTransform
struct PkzipClassicDecryptCryptoTransform_t2055300160;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"

// System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicDecryptCryptoTransform::.ctor(System.Byte[])
extern "C"  void PkzipClassicDecryptCryptoTransform__ctor_m4048362466 (PkzipClassicDecryptCryptoTransform_t2055300160 * __this, ByteU5BU5D_t4260760469* ___keyBlock0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ICSharpCode.SharpZipLib.Encryption.PkzipClassicDecryptCryptoTransform::TransformFinalBlock(System.Byte[],System.Int32,System.Int32)
extern "C"  ByteU5BU5D_t4260760469* PkzipClassicDecryptCryptoTransform_TransformFinalBlock_m684228055 (PkzipClassicDecryptCryptoTransform_t2055300160 * __this, ByteU5BU5D_t4260760469* ___inputBuffer0, int32_t ___inputOffset1, int32_t ___inputCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Encryption.PkzipClassicDecryptCryptoTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32)
extern "C"  int32_t PkzipClassicDecryptCryptoTransform_TransformBlock_m3151674299 (PkzipClassicDecryptCryptoTransform_t2055300160 * __this, ByteU5BU5D_t4260760469* ___inputBuffer0, int32_t ___inputOffset1, int32_t ___inputCount2, ByteU5BU5D_t4260760469* ___outputBuffer3, int32_t ___outputOffset4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Encryption.PkzipClassicDecryptCryptoTransform::get_CanReuseTransform()
extern "C"  bool PkzipClassicDecryptCryptoTransform_get_CanReuseTransform_m1680802552 (PkzipClassicDecryptCryptoTransform_t2055300160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Encryption.PkzipClassicDecryptCryptoTransform::get_InputBlockSize()
extern "C"  int32_t PkzipClassicDecryptCryptoTransform_get_InputBlockSize_m1185704860 (PkzipClassicDecryptCryptoTransform_t2055300160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Encryption.PkzipClassicDecryptCryptoTransform::get_OutputBlockSize()
extern "C"  int32_t PkzipClassicDecryptCryptoTransform_get_OutputBlockSize_m1142378679 (PkzipClassicDecryptCryptoTransform_t2055300160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Encryption.PkzipClassicDecryptCryptoTransform::get_CanTransformMultipleBlocks()
extern "C"  bool PkzipClassicDecryptCryptoTransform_get_CanTransformMultipleBlocks_m3692663044 (PkzipClassicDecryptCryptoTransform_t2055300160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicDecryptCryptoTransform::Dispose()
extern "C"  void PkzipClassicDecryptCryptoTransform_Dispose_m4044711364 (PkzipClassicDecryptCryptoTransform_t2055300160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
