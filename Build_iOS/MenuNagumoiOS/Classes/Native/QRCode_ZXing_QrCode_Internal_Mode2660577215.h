﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.QrCode.Internal.Mode
struct Mode_t2660577215;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.Mode
struct  Mode_t2660577215  : public Il2CppObject
{
public:
	// System.Int32[] ZXing.QrCode.Internal.Mode::characterCountBitsForVersions
	Int32U5BU5D_t3230847821* ___characterCountBitsForVersions_10;
	// System.Int32 ZXing.QrCode.Internal.Mode::bits
	int32_t ___bits_11;
	// System.String ZXing.QrCode.Internal.Mode::name
	String_t* ___name_12;

public:
	inline static int32_t get_offset_of_characterCountBitsForVersions_10() { return static_cast<int32_t>(offsetof(Mode_t2660577215, ___characterCountBitsForVersions_10)); }
	inline Int32U5BU5D_t3230847821* get_characterCountBitsForVersions_10() const { return ___characterCountBitsForVersions_10; }
	inline Int32U5BU5D_t3230847821** get_address_of_characterCountBitsForVersions_10() { return &___characterCountBitsForVersions_10; }
	inline void set_characterCountBitsForVersions_10(Int32U5BU5D_t3230847821* value)
	{
		___characterCountBitsForVersions_10 = value;
		Il2CppCodeGenWriteBarrier(&___characterCountBitsForVersions_10, value);
	}

	inline static int32_t get_offset_of_bits_11() { return static_cast<int32_t>(offsetof(Mode_t2660577215, ___bits_11)); }
	inline int32_t get_bits_11() const { return ___bits_11; }
	inline int32_t* get_address_of_bits_11() { return &___bits_11; }
	inline void set_bits_11(int32_t value)
	{
		___bits_11 = value;
	}

	inline static int32_t get_offset_of_name_12() { return static_cast<int32_t>(offsetof(Mode_t2660577215, ___name_12)); }
	inline String_t* get_name_12() const { return ___name_12; }
	inline String_t** get_address_of_name_12() { return &___name_12; }
	inline void set_name_12(String_t* value)
	{
		___name_12 = value;
		Il2CppCodeGenWriteBarrier(&___name_12, value);
	}
};

struct Mode_t2660577215_StaticFields
{
public:
	// ZXing.QrCode.Internal.Mode ZXing.QrCode.Internal.Mode::TERMINATOR
	Mode_t2660577215 * ___TERMINATOR_0;
	// ZXing.QrCode.Internal.Mode ZXing.QrCode.Internal.Mode::NUMERIC
	Mode_t2660577215 * ___NUMERIC_1;
	// ZXing.QrCode.Internal.Mode ZXing.QrCode.Internal.Mode::ALPHANUMERIC
	Mode_t2660577215 * ___ALPHANUMERIC_2;
	// ZXing.QrCode.Internal.Mode ZXing.QrCode.Internal.Mode::STRUCTURED_APPEND
	Mode_t2660577215 * ___STRUCTURED_APPEND_3;
	// ZXing.QrCode.Internal.Mode ZXing.QrCode.Internal.Mode::BYTE
	Mode_t2660577215 * ___BYTE_4;
	// ZXing.QrCode.Internal.Mode ZXing.QrCode.Internal.Mode::ECI
	Mode_t2660577215 * ___ECI_5;
	// ZXing.QrCode.Internal.Mode ZXing.QrCode.Internal.Mode::KANJI
	Mode_t2660577215 * ___KANJI_6;
	// ZXing.QrCode.Internal.Mode ZXing.QrCode.Internal.Mode::FNC1_FIRST_POSITION
	Mode_t2660577215 * ___FNC1_FIRST_POSITION_7;
	// ZXing.QrCode.Internal.Mode ZXing.QrCode.Internal.Mode::FNC1_SECOND_POSITION
	Mode_t2660577215 * ___FNC1_SECOND_POSITION_8;
	// ZXing.QrCode.Internal.Mode ZXing.QrCode.Internal.Mode::HANZI
	Mode_t2660577215 * ___HANZI_9;

public:
	inline static int32_t get_offset_of_TERMINATOR_0() { return static_cast<int32_t>(offsetof(Mode_t2660577215_StaticFields, ___TERMINATOR_0)); }
	inline Mode_t2660577215 * get_TERMINATOR_0() const { return ___TERMINATOR_0; }
	inline Mode_t2660577215 ** get_address_of_TERMINATOR_0() { return &___TERMINATOR_0; }
	inline void set_TERMINATOR_0(Mode_t2660577215 * value)
	{
		___TERMINATOR_0 = value;
		Il2CppCodeGenWriteBarrier(&___TERMINATOR_0, value);
	}

	inline static int32_t get_offset_of_NUMERIC_1() { return static_cast<int32_t>(offsetof(Mode_t2660577215_StaticFields, ___NUMERIC_1)); }
	inline Mode_t2660577215 * get_NUMERIC_1() const { return ___NUMERIC_1; }
	inline Mode_t2660577215 ** get_address_of_NUMERIC_1() { return &___NUMERIC_1; }
	inline void set_NUMERIC_1(Mode_t2660577215 * value)
	{
		___NUMERIC_1 = value;
		Il2CppCodeGenWriteBarrier(&___NUMERIC_1, value);
	}

	inline static int32_t get_offset_of_ALPHANUMERIC_2() { return static_cast<int32_t>(offsetof(Mode_t2660577215_StaticFields, ___ALPHANUMERIC_2)); }
	inline Mode_t2660577215 * get_ALPHANUMERIC_2() const { return ___ALPHANUMERIC_2; }
	inline Mode_t2660577215 ** get_address_of_ALPHANUMERIC_2() { return &___ALPHANUMERIC_2; }
	inline void set_ALPHANUMERIC_2(Mode_t2660577215 * value)
	{
		___ALPHANUMERIC_2 = value;
		Il2CppCodeGenWriteBarrier(&___ALPHANUMERIC_2, value);
	}

	inline static int32_t get_offset_of_STRUCTURED_APPEND_3() { return static_cast<int32_t>(offsetof(Mode_t2660577215_StaticFields, ___STRUCTURED_APPEND_3)); }
	inline Mode_t2660577215 * get_STRUCTURED_APPEND_3() const { return ___STRUCTURED_APPEND_3; }
	inline Mode_t2660577215 ** get_address_of_STRUCTURED_APPEND_3() { return &___STRUCTURED_APPEND_3; }
	inline void set_STRUCTURED_APPEND_3(Mode_t2660577215 * value)
	{
		___STRUCTURED_APPEND_3 = value;
		Il2CppCodeGenWriteBarrier(&___STRUCTURED_APPEND_3, value);
	}

	inline static int32_t get_offset_of_BYTE_4() { return static_cast<int32_t>(offsetof(Mode_t2660577215_StaticFields, ___BYTE_4)); }
	inline Mode_t2660577215 * get_BYTE_4() const { return ___BYTE_4; }
	inline Mode_t2660577215 ** get_address_of_BYTE_4() { return &___BYTE_4; }
	inline void set_BYTE_4(Mode_t2660577215 * value)
	{
		___BYTE_4 = value;
		Il2CppCodeGenWriteBarrier(&___BYTE_4, value);
	}

	inline static int32_t get_offset_of_ECI_5() { return static_cast<int32_t>(offsetof(Mode_t2660577215_StaticFields, ___ECI_5)); }
	inline Mode_t2660577215 * get_ECI_5() const { return ___ECI_5; }
	inline Mode_t2660577215 ** get_address_of_ECI_5() { return &___ECI_5; }
	inline void set_ECI_5(Mode_t2660577215 * value)
	{
		___ECI_5 = value;
		Il2CppCodeGenWriteBarrier(&___ECI_5, value);
	}

	inline static int32_t get_offset_of_KANJI_6() { return static_cast<int32_t>(offsetof(Mode_t2660577215_StaticFields, ___KANJI_6)); }
	inline Mode_t2660577215 * get_KANJI_6() const { return ___KANJI_6; }
	inline Mode_t2660577215 ** get_address_of_KANJI_6() { return &___KANJI_6; }
	inline void set_KANJI_6(Mode_t2660577215 * value)
	{
		___KANJI_6 = value;
		Il2CppCodeGenWriteBarrier(&___KANJI_6, value);
	}

	inline static int32_t get_offset_of_FNC1_FIRST_POSITION_7() { return static_cast<int32_t>(offsetof(Mode_t2660577215_StaticFields, ___FNC1_FIRST_POSITION_7)); }
	inline Mode_t2660577215 * get_FNC1_FIRST_POSITION_7() const { return ___FNC1_FIRST_POSITION_7; }
	inline Mode_t2660577215 ** get_address_of_FNC1_FIRST_POSITION_7() { return &___FNC1_FIRST_POSITION_7; }
	inline void set_FNC1_FIRST_POSITION_7(Mode_t2660577215 * value)
	{
		___FNC1_FIRST_POSITION_7 = value;
		Il2CppCodeGenWriteBarrier(&___FNC1_FIRST_POSITION_7, value);
	}

	inline static int32_t get_offset_of_FNC1_SECOND_POSITION_8() { return static_cast<int32_t>(offsetof(Mode_t2660577215_StaticFields, ___FNC1_SECOND_POSITION_8)); }
	inline Mode_t2660577215 * get_FNC1_SECOND_POSITION_8() const { return ___FNC1_SECOND_POSITION_8; }
	inline Mode_t2660577215 ** get_address_of_FNC1_SECOND_POSITION_8() { return &___FNC1_SECOND_POSITION_8; }
	inline void set_FNC1_SECOND_POSITION_8(Mode_t2660577215 * value)
	{
		___FNC1_SECOND_POSITION_8 = value;
		Il2CppCodeGenWriteBarrier(&___FNC1_SECOND_POSITION_8, value);
	}

	inline static int32_t get_offset_of_HANZI_9() { return static_cast<int32_t>(offsetof(Mode_t2660577215_StaticFields, ___HANZI_9)); }
	inline Mode_t2660577215 * get_HANZI_9() const { return ___HANZI_9; }
	inline Mode_t2660577215 ** get_address_of_HANZI_9() { return &___HANZI_9; }
	inline void set_HANZI_9(Mode_t2660577215 * value)
	{
		___HANZI_9 = value;
		Il2CppCodeGenWriteBarrier(&___HANZI_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
