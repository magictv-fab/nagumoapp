﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::.ctor()
#define List_1__ctor_m942288672(__this, method) ((  void (*) (List_1_t2046686999 *, const MethodInfo*))List_1__ctor_m574172797_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m573994490(__this, ___collection0, method) ((  void (*) (List_1_t2046686999 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1160795371_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::.ctor(System.Int32)
#define List_1__ctor_m2530684726(__this, ___capacity0, method) ((  void (*) (List_1_t2046686999 *, int32_t, const MethodInfo*))List_1__ctor_m3643386469_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::.ctor(T[],System.Int32)
#define List_1__ctor_m1698703552(__this, ___data0, ___size1, method) ((  void (*) (List_1_t2046686999 *, ILoadLevelEventHandlerU5BU5D_t2382241470*, int32_t, const MethodInfo*))List_1__ctor_m4134761583_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::.cctor()
#define List_1__cctor_m2357432040(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3826137881_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m239977775(__this, method) ((  Il2CppObject* (*) (List_1_t2046686999 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m394654591(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2046686999 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m2086094798(__this, method) ((  Il2CppObject * (*) (List_1_t2046686999 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m3594652335(__this, ___item0, method) ((  int32_t (*) (List_1_t2046686999 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3794749222_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m2792380529(__this, ___item0, method) ((  bool (*) (List_1_t2046686999 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2659633254_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m722424199(__this, ___item0, method) ((  int32_t (*) (List_1_t2046686999 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3431692926_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m2899877242(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2046686999 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2067529129_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m3802574190(__this, ___item0, method) ((  void (*) (List_1_t2046686999 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1644145887_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1350643506(__this, method) ((  bool (*) (List_1_t2046686999 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2876726603(__this, method) ((  bool (*) (List_1_t2046686999 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3867536694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m313791165(__this, method) ((  Il2CppObject * (*) (List_1_t2046686999 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m4093341856(__this, method) ((  bool (*) (List_1_t2046686999 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m432946261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m2802809433(__this, method) ((  bool (*) (List_1_t2046686999 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2961826820_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m1971210500(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t2046686999 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3985478825_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m4257174225(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2046686999 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3234554688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::Add(T)
#define List_1_Add_m636649488(__this, ___item0, method) ((  void (*) (List_1_t2046686999 *, Il2CppObject *, const MethodInfo*))List_1_Add_m268533613_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m3580199861(__this, ___newCount0, method) ((  void (*) (List_1_t2046686999 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4122600870_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m1914613010(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t2046686999 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m590371457_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m2366752435(__this, ___collection0, method) ((  void (*) (List_1_t2046686999 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2478449828_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m1627724659(__this, ___enumerable0, method) ((  void (*) (List_1_t2046686999 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1739422052_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m1361000260(__this, ___collection0, method) ((  void (*) (List_1_t2046686999 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2229151411_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::AsReadOnly()
#define List_1_AsReadOnly_m2127243457(__this, method) ((  ReadOnlyCollection_1_t2235578983 * (*) (List_1_t2046686999 *, const MethodInfo*))List_1_AsReadOnly_m769820182_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::Clear()
#define List_1_Clear_m1238508944(__this, method) ((  void (*) (List_1_t2046686999 *, const MethodInfo*))List_1_Clear_m454602559_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::Contains(T)
#define List_1_Contains_m3761085954(__this, ___item0, method) ((  bool (*) (List_1_t2046686999 *, Il2CppObject *, const MethodInfo*))List_1_Contains_m4186092781_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m1485001706(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2046686999 *, ILoadLevelEventHandlerU5BU5D_t2382241470*, int32_t, const MethodInfo*))List_1_CopyTo_m3988356635_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::Find(System.Predicate`1<T>)
#define List_1_Find_m476009116(__this, ___match0, method) ((  Il2CppObject * (*) (List_1_t2046686999 *, Predicate_1_t289558330 *, const MethodInfo*))List_1_Find_m3379773421_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m334336569(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t289558330 *, const MethodInfo*))List_1_CheckMatch_m3390394152_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::FindAll(System.Predicate`1<T>)
#define List_1_FindAll_m1964784583(__this, ___match0, method) ((  List_1_t2046686999 * (*) (List_1_t2046686999 *, Predicate_1_t289558330 *, const MethodInfo*))List_1_FindAll_m754611998_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::FindAllStackBits(System.Predicate`1<T>)
#define List_1_FindAllStackBits_m1425669427(__this, ___match0, method) ((  List_1_t2046686999 * (*) (List_1_t2046686999 *, Predicate_1_t289558330 *, const MethodInfo*))List_1_FindAllStackBits_m2296615868_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::FindAllList(System.Predicate`1<T>)
#define List_1_FindAllList_m2148810697(__this, ___match0, method) ((  List_1_t2046686999 * (*) (List_1_t2046686999 *, Predicate_1_t289558330 *, const MethodInfo*))List_1_FindAllList_m1562834848_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m2748040726(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t2046686999 *, int32_t, int32_t, Predicate_1_t289558330 *, const MethodInfo*))List_1_GetIndex_m4275988045_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m2395786349(__this, ___action0, method) ((  void (*) (List_1_t2046686999 *, Action_1_t1074317583 *, const MethodInfo*))List_1_ForEach_m2030348444_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::GetEnumerator()
#define List_1_GetEnumerator_m2116953573(__this, method) ((  Enumerator_t2066359769  (*) (List_1_t2046686999 *, const MethodInfo*))List_1_GetEnumerator_m1919240000_gshared)(__this, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::GetRange(System.Int32,System.Int32)
#define List_1_GetRange_m891149763(__this, ___index0, ___count1, method) ((  List_1_t2046686999 * (*) (List_1_t2046686999 *, int32_t, int32_t, const MethodInfo*))List_1_GetRange_m4200734924_gshared)(__this, ___index0, ___count1, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::IndexOf(T)
#define List_1_IndexOf_m1948207670(__this, ___item0, method) ((  int32_t (*) (List_1_t2046686999 *, Il2CppObject *, const MethodInfo*))List_1_IndexOf_m1752303327_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m1214619713(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t2046686999 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3807054194_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m1231368890(__this, ___index0, method) ((  void (*) (List_1_t2046686999 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3734723819_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::Insert(System.Int32,T)
#define List_1_Insert_m3487128801(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2046686999 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_Insert_m3427163986_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m2938058902(__this, ___collection0, method) ((  void (*) (List_1_t2046686999 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2905071175_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::Remove(T)
#define List_1_Remove_m1684695085(__this, ___item0, method) ((  bool (*) (List_1_t2046686999 *, Il2CppObject *, const MethodInfo*))List_1_Remove_m2747911208_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m1019012473(__this, ___match0, method) ((  int32_t (*) (List_1_t2046686999 *, Predicate_1_t289558330 *, const MethodInfo*))List_1_RemoveAll_m2933443938_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m1360981671(__this, ___index0, method) ((  void (*) (List_1_t2046686999 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1301016856_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m3253640394(__this, ___index0, ___count1, method) ((  void (*) (List_1_t2046686999 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m856857915_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::Reverse()
#define List_1_Reverse_m2163841125(__this, method) ((  void (*) (List_1_t2046686999 *, const MethodInfo*))List_1_Reverse_m449081940_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::Sort()
#define List_1_Sort_m1471023453(__this, method) ((  void (*) (List_1_t2046686999 *, const MethodInfo*))List_1_Sort_m1168641486_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m1918478631(__this, ___comparer0, method) ((  void (*) (List_1_t2046686999 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3726677974_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m2635858416(__this, ___comparison0, method) ((  void (*) (List_1_t2046686999 *, Comparison_1_t3689829930 *, const MethodInfo*))List_1_Sort_m4192185249_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::ToArray()
#define List_1_ToArray_m3418346302(__this, method) ((  ILoadLevelEventHandlerU5BU5D_t2382241470* (*) (List_1_t2046686999 *, const MethodInfo*))List_1_ToArray_m238588755_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::TrimExcess()
#define List_1_TrimExcess_m2501242678(__this, method) ((  void (*) (List_1_t2046686999 *, const MethodInfo*))List_1_TrimExcess_m2451380967_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::TrueForAll(System.Predicate`1<T>)
#define List_1_TrueForAll_m357519474(__this, ___match0, method) ((  bool (*) (List_1_t2046686999 *, Predicate_1_t289558330 *, const MethodInfo*))List_1_TrueForAll_m2945946909_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::get_Capacity()
#define List_1_get_Capacity_m1115895654(__this, method) ((  int32_t (*) (List_1_t2046686999 *, const MethodInfo*))List_1_get_Capacity_m543520655_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m790388679(__this, ___value0, method) ((  void (*) (List_1_t2046686999 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1332789688_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::get_Count()
#define List_1_get_Count_m82203141(__this, method) ((  int32_t (*) (List_1_t2046686999 *, const MethodInfo*))List_1_get_Count_m2594154675_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::get_Item(System.Int32)
#define List_1_get_Item_m1640035981(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t2046686999 *, int32_t, const MethodInfo*))List_1_get_Item_m850128002_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::set_Item(System.Int32,T)
#define List_1_set_Item_m2865883512(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2046686999 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_set_Item_m1074271145_gshared)(__this, ___index0, ___value1, method)
