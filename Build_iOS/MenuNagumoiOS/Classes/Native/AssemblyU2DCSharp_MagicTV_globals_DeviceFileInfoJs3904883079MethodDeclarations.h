﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.globals.DeviceFileInfoJson/<DoReadFile>c__Iterator38
struct U3CDoReadFileU3Ec__Iterator38_t3904883079;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.globals.DeviceFileInfoJson/<DoReadFile>c__Iterator38::.ctor()
extern "C"  void U3CDoReadFileU3Ec__Iterator38__ctor_m1663000820 (U3CDoReadFileU3Ec__Iterator38_t3904883079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MagicTV.globals.DeviceFileInfoJson/<DoReadFile>c__Iterator38::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoReadFileU3Ec__Iterator38_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m787060712 (U3CDoReadFileU3Ec__Iterator38_t3904883079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MagicTV.globals.DeviceFileInfoJson/<DoReadFile>c__Iterator38::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoReadFileU3Ec__Iterator38_System_Collections_IEnumerator_get_Current_m634940284 (U3CDoReadFileU3Ec__Iterator38_t3904883079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicTV.globals.DeviceFileInfoJson/<DoReadFile>c__Iterator38::MoveNext()
extern "C"  bool U3CDoReadFileU3Ec__Iterator38_MoveNext_m1431738984 (U3CDoReadFileU3Ec__Iterator38_t3904883079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.DeviceFileInfoJson/<DoReadFile>c__Iterator38::Dispose()
extern "C"  void U3CDoReadFileU3Ec__Iterator38_Dispose_m314236977 (U3CDoReadFileU3Ec__Iterator38_t3904883079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.DeviceFileInfoJson/<DoReadFile>c__Iterator38::Reset()
extern "C"  void U3CDoReadFileU3Ec__Iterator38_Reset_m3604401057 (U3CDoReadFileU3Ec__Iterator38_t3904883079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
