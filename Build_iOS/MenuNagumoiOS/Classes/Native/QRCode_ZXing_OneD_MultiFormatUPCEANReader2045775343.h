﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.OneD.UPCEANReader[]
struct UPCEANReaderU5BU5D_t2863360746;

#include "QRCode_ZXing_OneD_OneDReader3436042911.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.MultiFormatUPCEANReader
struct  MultiFormatUPCEANReader_t2045775343  : public OneDReader_t3436042911
{
public:
	// ZXing.OneD.UPCEANReader[] ZXing.OneD.MultiFormatUPCEANReader::readers
	UPCEANReaderU5BU5D_t2863360746* ___readers_2;

public:
	inline static int32_t get_offset_of_readers_2() { return static_cast<int32_t>(offsetof(MultiFormatUPCEANReader_t2045775343, ___readers_2)); }
	inline UPCEANReaderU5BU5D_t2863360746* get_readers_2() const { return ___readers_2; }
	inline UPCEANReaderU5BU5D_t2863360746** get_address_of_readers_2() { return &___readers_2; }
	inline void set_readers_2(UPCEANReaderU5BU5D_t2863360746* value)
	{
		___readers_2 = value;
		Il2CppCodeGenWriteBarrier(&___readers_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
