﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GerarCupom/<IGerar>c__Iterator5C
struct U3CIGerarU3Ec__Iterator5C_t2374326817;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GerarCupom/<IGerar>c__Iterator5C::.ctor()
extern "C"  void U3CIGerarU3Ec__Iterator5C__ctor_m1654485402 (U3CIGerarU3Ec__Iterator5C_t2374326817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GerarCupom/<IGerar>c__Iterator5C::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CIGerarU3Ec__Iterator5C_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3351999618 (U3CIGerarU3Ec__Iterator5C_t2374326817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GerarCupom/<IGerar>c__Iterator5C::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CIGerarU3Ec__Iterator5C_System_Collections_IEnumerator_get_Current_m120241174 (U3CIGerarU3Ec__Iterator5C_t2374326817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GerarCupom/<IGerar>c__Iterator5C::MoveNext()
extern "C"  bool U3CIGerarU3Ec__Iterator5C_MoveNext_m2967819778 (U3CIGerarU3Ec__Iterator5C_t2374326817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GerarCupom/<IGerar>c__Iterator5C::Dispose()
extern "C"  void U3CIGerarU3Ec__Iterator5C_Dispose_m720854871 (U3CIGerarU3Ec__Iterator5C_t2374326817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GerarCupom/<IGerar>c__Iterator5C::Reset()
extern "C"  void U3CIGerarU3Ec__Iterator5C_Reset_m3595885639 (U3CIGerarU3Ec__Iterator5C_t2374326817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
