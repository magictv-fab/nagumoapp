﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedListNode_13787551078MethodDeclarations.h"

// System.Void System.Collections.Generic.LinkedListNode`1<ZXing.Aztec.Internal.Token>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
#define LinkedListNode_1__ctor_m3580100159(__this, ___list0, ___value1, method) ((  void (*) (LinkedListNode_1_t2682942062 *, LinkedList_1_t940842524 *, Token_t3066207355 *, const MethodInfo*))LinkedListNode_1__ctor_m648136130_gshared)(__this, ___list0, ___value1, method)
// System.Void System.Collections.Generic.LinkedListNode`1<ZXing.Aztec.Internal.Token>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedListNode_1__ctor_m39289055(__this, ___list0, ___value1, ___previousNode2, ___nextNode3, method) ((  void (*) (LinkedListNode_1_t2682942062 *, LinkedList_1_t940842524 *, Token_t3066207355 *, LinkedListNode_1_t2682942062 *, LinkedListNode_1_t2682942062 *, const MethodInfo*))LinkedListNode_1__ctor_m448391458_gshared)(__this, ___list0, ___value1, ___previousNode2, ___nextNode3, method)
// System.Void System.Collections.Generic.LinkedListNode`1<ZXing.Aztec.Internal.Token>::Detach()
#define LinkedListNode_1_Detach_m2486358785(__this, method) ((  void (*) (LinkedListNode_1_t2682942062 *, const MethodInfo*))LinkedListNode_1_Detach_m3406254942_gshared)(__this, method)
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<ZXing.Aztec.Internal.Token>::get_List()
#define LinkedListNode_1_get_List_m3638634405(__this, method) ((  LinkedList_1_t940842524 * (*) (LinkedListNode_1_t2682942062 *, const MethodInfo*))LinkedListNode_1_get_List_m3467110818_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<ZXing.Aztec.Internal.Token>::get_Next()
#define LinkedListNode_1_get_Next_m3685871320(__this, method) ((  LinkedListNode_1_t2682942062 * (*) (LinkedListNode_1_t2682942062 *, const MethodInfo*))LinkedListNode_1_get_Next_m1427618777_gshared)(__this, method)
// T System.Collections.Generic.LinkedListNode`1<ZXing.Aztec.Internal.Token>::get_Value()
#define LinkedListNode_1_get_Value_m3013381307(__this, method) ((  Token_t3066207355 * (*) (LinkedListNode_1_t2682942062 *, const MethodInfo*))LinkedListNode_1_get_Value_m702633824_gshared)(__this, method)
