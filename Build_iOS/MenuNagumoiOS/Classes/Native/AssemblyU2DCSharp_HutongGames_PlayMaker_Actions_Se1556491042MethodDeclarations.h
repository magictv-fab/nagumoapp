﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SendEventByName
struct SendEventByName_t1556491042;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SendEventByName::.ctor()
extern "C"  void SendEventByName__ctor_m1783722580 (SendEventByName_t1556491042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SendEventByName::Reset()
extern "C"  void SendEventByName_Reset_m3725122817 (SendEventByName_t1556491042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SendEventByName::OnEnter()
extern "C"  void SendEventByName_OnEnter_m4152144171 (SendEventByName_t1556491042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SendEventByName::OnUpdate()
extern "C"  void SendEventByName_OnUpdate_m3295977080 (SendEventByName_t1556491042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
