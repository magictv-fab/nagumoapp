﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetChildNum
struct  GetChildNum_t3319553230  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetChildNum::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetChildNum::childIndex
	FsmInt_t1596138449 * ___childIndex_10;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetChildNum::store
	FsmGameObject_t1697147867 * ___store_11;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(GetChildNum_t3319553230, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_childIndex_10() { return static_cast<int32_t>(offsetof(GetChildNum_t3319553230, ___childIndex_10)); }
	inline FsmInt_t1596138449 * get_childIndex_10() const { return ___childIndex_10; }
	inline FsmInt_t1596138449 ** get_address_of_childIndex_10() { return &___childIndex_10; }
	inline void set_childIndex_10(FsmInt_t1596138449 * value)
	{
		___childIndex_10 = value;
		Il2CppCodeGenWriteBarrier(&___childIndex_10, value);
	}

	inline static int32_t get_offset_of_store_11() { return static_cast<int32_t>(offsetof(GetChildNum_t3319553230, ___store_11)); }
	inline FsmGameObject_t1697147867 * get_store_11() const { return ___store_11; }
	inline FsmGameObject_t1697147867 ** get_address_of_store_11() { return &___store_11; }
	inline void set_store_11(FsmGameObject_t1697147867 * value)
	{
		___store_11 = value;
		Il2CppCodeGenWriteBarrier(&___store_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
