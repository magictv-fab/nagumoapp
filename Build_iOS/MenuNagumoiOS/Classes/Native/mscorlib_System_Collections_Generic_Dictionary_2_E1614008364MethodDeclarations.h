﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3363211663MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1670879530(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1614008364 *, Dictionary_2_t296684972 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2125982391(__this, method) ((  Il2CppObject * (*) (Enumerator_t1614008364 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1122849099(__this, method) ((  void (*) (Enumerator_t1614008364 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3181160596(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t1614008364 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1774453331(__this, method) ((  Il2CppObject * (*) (Enumerator_t1614008364 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1196070949(__this, method) ((  Il2CppObject * (*) (Enumerator_t1614008364 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action>::MoveNext()
#define Enumerator_MoveNext_m3938641079(__this, method) ((  bool (*) (Enumerator_t1614008364 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action>::get_Current()
#define Enumerator_get_Current_m4205927961(__this, method) ((  KeyValuePair_2_t195465678  (*) (Enumerator_t1614008364 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1544239940(__this, method) ((  String_t* (*) (Enumerator_t1614008364 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2160565992(__this, method) ((  Action_t3771233898 * (*) (Enumerator_t1614008364 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action>::Reset()
#define Enumerator_Reset_m3597931644(__this, method) ((  void (*) (Enumerator_t1614008364 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action>::VerifyState()
#define Enumerator_VerifyState_m4255979397(__this, method) ((  void (*) (Enumerator_t1614008364 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3932206509(__this, method) ((  void (*) (Enumerator_t1614008364 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Action>::Dispose()
#define Enumerator_Dispose_m2687065676(__this, method) ((  void (*) (Enumerator_t1614008364 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
