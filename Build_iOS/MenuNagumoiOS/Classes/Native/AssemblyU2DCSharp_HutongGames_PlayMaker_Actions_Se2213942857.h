﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// PlayMakerAnimatorIKProxy
struct PlayMakerAnimatorIKProxy_t1024752181;
// UnityEngine.Animator
struct Animator_t2776330603;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAnimatorLookAt
struct  SetAnimatorLookAt_t2213942857  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAnimatorLookAt::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.SetAnimatorLookAt::target
	FsmGameObject_t1697147867 * ___target_10;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SetAnimatorLookAt::targetPosition
	FsmVector3_t533912882 * ___targetPosition_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetAnimatorLookAt::weight
	FsmFloat_t2134102846 * ___weight_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetAnimatorLookAt::bodyWeight
	FsmFloat_t2134102846 * ___bodyWeight_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetAnimatorLookAt::headWeight
	FsmFloat_t2134102846 * ___headWeight_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetAnimatorLookAt::eyesWeight
	FsmFloat_t2134102846 * ___eyesWeight_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetAnimatorLookAt::clampWeight
	FsmFloat_t2134102846 * ___clampWeight_16;
	// System.Boolean HutongGames.PlayMaker.Actions.SetAnimatorLookAt::everyFrame
	bool ___everyFrame_17;
	// PlayMakerAnimatorIKProxy HutongGames.PlayMaker.Actions.SetAnimatorLookAt::_animatorProxy
	PlayMakerAnimatorIKProxy_t1024752181 * ____animatorProxy_18;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.SetAnimatorLookAt::_animator
	Animator_t2776330603 * ____animator_19;
	// UnityEngine.Transform HutongGames.PlayMaker.Actions.SetAnimatorLookAt::_transform
	Transform_t1659122786 * ____transform_20;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(SetAnimatorLookAt_t2213942857, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_target_10() { return static_cast<int32_t>(offsetof(SetAnimatorLookAt_t2213942857, ___target_10)); }
	inline FsmGameObject_t1697147867 * get_target_10() const { return ___target_10; }
	inline FsmGameObject_t1697147867 ** get_address_of_target_10() { return &___target_10; }
	inline void set_target_10(FsmGameObject_t1697147867 * value)
	{
		___target_10 = value;
		Il2CppCodeGenWriteBarrier(&___target_10, value);
	}

	inline static int32_t get_offset_of_targetPosition_11() { return static_cast<int32_t>(offsetof(SetAnimatorLookAt_t2213942857, ___targetPosition_11)); }
	inline FsmVector3_t533912882 * get_targetPosition_11() const { return ___targetPosition_11; }
	inline FsmVector3_t533912882 ** get_address_of_targetPosition_11() { return &___targetPosition_11; }
	inline void set_targetPosition_11(FsmVector3_t533912882 * value)
	{
		___targetPosition_11 = value;
		Il2CppCodeGenWriteBarrier(&___targetPosition_11, value);
	}

	inline static int32_t get_offset_of_weight_12() { return static_cast<int32_t>(offsetof(SetAnimatorLookAt_t2213942857, ___weight_12)); }
	inline FsmFloat_t2134102846 * get_weight_12() const { return ___weight_12; }
	inline FsmFloat_t2134102846 ** get_address_of_weight_12() { return &___weight_12; }
	inline void set_weight_12(FsmFloat_t2134102846 * value)
	{
		___weight_12 = value;
		Il2CppCodeGenWriteBarrier(&___weight_12, value);
	}

	inline static int32_t get_offset_of_bodyWeight_13() { return static_cast<int32_t>(offsetof(SetAnimatorLookAt_t2213942857, ___bodyWeight_13)); }
	inline FsmFloat_t2134102846 * get_bodyWeight_13() const { return ___bodyWeight_13; }
	inline FsmFloat_t2134102846 ** get_address_of_bodyWeight_13() { return &___bodyWeight_13; }
	inline void set_bodyWeight_13(FsmFloat_t2134102846 * value)
	{
		___bodyWeight_13 = value;
		Il2CppCodeGenWriteBarrier(&___bodyWeight_13, value);
	}

	inline static int32_t get_offset_of_headWeight_14() { return static_cast<int32_t>(offsetof(SetAnimatorLookAt_t2213942857, ___headWeight_14)); }
	inline FsmFloat_t2134102846 * get_headWeight_14() const { return ___headWeight_14; }
	inline FsmFloat_t2134102846 ** get_address_of_headWeight_14() { return &___headWeight_14; }
	inline void set_headWeight_14(FsmFloat_t2134102846 * value)
	{
		___headWeight_14 = value;
		Il2CppCodeGenWriteBarrier(&___headWeight_14, value);
	}

	inline static int32_t get_offset_of_eyesWeight_15() { return static_cast<int32_t>(offsetof(SetAnimatorLookAt_t2213942857, ___eyesWeight_15)); }
	inline FsmFloat_t2134102846 * get_eyesWeight_15() const { return ___eyesWeight_15; }
	inline FsmFloat_t2134102846 ** get_address_of_eyesWeight_15() { return &___eyesWeight_15; }
	inline void set_eyesWeight_15(FsmFloat_t2134102846 * value)
	{
		___eyesWeight_15 = value;
		Il2CppCodeGenWriteBarrier(&___eyesWeight_15, value);
	}

	inline static int32_t get_offset_of_clampWeight_16() { return static_cast<int32_t>(offsetof(SetAnimatorLookAt_t2213942857, ___clampWeight_16)); }
	inline FsmFloat_t2134102846 * get_clampWeight_16() const { return ___clampWeight_16; }
	inline FsmFloat_t2134102846 ** get_address_of_clampWeight_16() { return &___clampWeight_16; }
	inline void set_clampWeight_16(FsmFloat_t2134102846 * value)
	{
		___clampWeight_16 = value;
		Il2CppCodeGenWriteBarrier(&___clampWeight_16, value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(SetAnimatorLookAt_t2213942857, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}

	inline static int32_t get_offset_of__animatorProxy_18() { return static_cast<int32_t>(offsetof(SetAnimatorLookAt_t2213942857, ____animatorProxy_18)); }
	inline PlayMakerAnimatorIKProxy_t1024752181 * get__animatorProxy_18() const { return ____animatorProxy_18; }
	inline PlayMakerAnimatorIKProxy_t1024752181 ** get_address_of__animatorProxy_18() { return &____animatorProxy_18; }
	inline void set__animatorProxy_18(PlayMakerAnimatorIKProxy_t1024752181 * value)
	{
		____animatorProxy_18 = value;
		Il2CppCodeGenWriteBarrier(&____animatorProxy_18, value);
	}

	inline static int32_t get_offset_of__animator_19() { return static_cast<int32_t>(offsetof(SetAnimatorLookAt_t2213942857, ____animator_19)); }
	inline Animator_t2776330603 * get__animator_19() const { return ____animator_19; }
	inline Animator_t2776330603 ** get_address_of__animator_19() { return &____animator_19; }
	inline void set__animator_19(Animator_t2776330603 * value)
	{
		____animator_19 = value;
		Il2CppCodeGenWriteBarrier(&____animator_19, value);
	}

	inline static int32_t get_offset_of__transform_20() { return static_cast<int32_t>(offsetof(SetAnimatorLookAt_t2213942857, ____transform_20)); }
	inline Transform_t1659122786 * get__transform_20() const { return ____transform_20; }
	inline Transform_t1659122786 ** get_address_of__transform_20() { return &____transform_20; }
	inline void set__transform_20(Transform_t1659122786 * value)
	{
		____transform_20 = value;
		Il2CppCodeGenWriteBarrier(&____transform_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
