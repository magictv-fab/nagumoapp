﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OfertasManagerCRM_PraVoce/<ILoadImgs>c__IteratorE
struct U3CILoadImgsU3Ec__IteratorE_t3089220910;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OfertasManagerCRM_PraVoce/<ILoadImgs>c__IteratorE::.ctor()
extern "C"  void U3CILoadImgsU3Ec__IteratorE__ctor_m1543781373 (U3CILoadImgsU3Ec__IteratorE_t3089220910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManagerCRM_PraVoce/<ILoadImgs>c__IteratorE::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CILoadImgsU3Ec__IteratorE_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2239682229 (U3CILoadImgsU3Ec__IteratorE_t3089220910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OfertasManagerCRM_PraVoce/<ILoadImgs>c__IteratorE::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CILoadImgsU3Ec__IteratorE_System_Collections_IEnumerator_get_Current_m5694537 (U3CILoadImgsU3Ec__IteratorE_t3089220910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OfertasManagerCRM_PraVoce/<ILoadImgs>c__IteratorE::MoveNext()
extern "C"  bool U3CILoadImgsU3Ec__IteratorE_MoveNext_m3880577495 (U3CILoadImgsU3Ec__IteratorE_t3089220910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_PraVoce/<ILoadImgs>c__IteratorE::Dispose()
extern "C"  void U3CILoadImgsU3Ec__IteratorE_Dispose_m1708465402 (U3CILoadImgsU3Ec__IteratorE_t3089220910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OfertasManagerCRM_PraVoce/<ILoadImgs>c__IteratorE::Reset()
extern "C"  void U3CILoadImgsU3Ec__IteratorE_Reset_m3485181610 (U3CILoadImgsU3Ec__IteratorE_t3089220910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
