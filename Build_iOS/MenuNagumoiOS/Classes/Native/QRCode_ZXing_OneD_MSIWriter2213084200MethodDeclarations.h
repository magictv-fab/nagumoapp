﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.MSIWriter
struct MSIWriter_t2213084200;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// System.String
struct String_t;
// System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>
struct IDictionary_2_t3297010830;
// System.Boolean[]
struct BooleanU5BU5D_t3456302923;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "QRCode_ZXing_BarcodeFormat4201805817.h"

// ZXing.Common.BitMatrix ZXing.OneD.MSIWriter::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern "C"  BitMatrix_t1058711404 * MSIWriter_encode_m4079087019 (MSIWriter_t2213084200 * __this, String_t* ___contents0, int32_t ___format1, int32_t ___width2, int32_t ___height3, Il2CppObject* ___hints4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean[] ZXing.OneD.MSIWriter::encode(System.String)
extern "C"  BooleanU5BU5D_t3456302923* MSIWriter_encode_m3736512353 (MSIWriter_t2213084200 * __this, String_t* ___contents0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.MSIWriter::.ctor()
extern "C"  void MSIWriter__ctor_m3182521195 (MSIWriter_t2213084200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.MSIWriter::.cctor()
extern "C"  void MSIWriter__cctor_m3686780322 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
