﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// ZXing.Datamatrix.Encoder.SymbolInfo
struct SymbolInfo_t553159586;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "QRCode_ZXing_Datamatrix_Encoder_SymbolInfo553159586.h"

// System.Void ZXing.Datamatrix.Encoder.ErrorCorrection::.cctor()
extern "C"  void ErrorCorrection__cctor_m2435554797 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.Datamatrix.Encoder.ErrorCorrection::encodeECC200(System.String,ZXing.Datamatrix.Encoder.SymbolInfo)
extern "C"  String_t* ErrorCorrection_encodeECC200_m3691662483 (Il2CppObject * __this /* static, unused */, String_t* ___codewords0, SymbolInfo_t553159586 * ___symbolInfo1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.Datamatrix.Encoder.ErrorCorrection::createECCBlock(System.String,System.Int32)
extern "C"  String_t* ErrorCorrection_createECCBlock_m770781568 (Il2CppObject * __this /* static, unused */, String_t* ___codewords0, int32_t ___numECWords1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.Datamatrix.Encoder.ErrorCorrection::createECCBlock(System.String,System.Int32,System.Int32,System.Int32)
extern "C"  String_t* ErrorCorrection_createECCBlock_m3282052832 (Il2CppObject * __this /* static, unused */, String_t* ___codewords0, int32_t ___start1, int32_t ___len2, int32_t ___numECWords3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
