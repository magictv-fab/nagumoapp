﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.abstracts.screens.LoadScreenCircle
struct LoadScreenCircle_t3455683822;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MagicTV.abstracts.screens.LoadScreenCircle::.ctor()
extern "C"  void LoadScreenCircle__ctor_m3693221878 (LoadScreenCircle_t3455683822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.LoadScreenCircle::prepare()
extern "C"  void LoadScreenCircle_prepare_m2470218683 (LoadScreenCircle_t3455683822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.LoadScreenCircle::Start()
extern "C"  void LoadScreenCircle_Start_m2640359670 (LoadScreenCircle_t3455683822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.LoadScreenCircle::setMessage(System.String)
extern "C"  void LoadScreenCircle_setMessage_m715784495 (LoadScreenCircle_t3455683822 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.LoadScreenCircle::setPercentLoaded(System.Single)
extern "C"  void LoadScreenCircle_setPercentLoaded_m2428126485 (LoadScreenCircle_t3455683822 * __this, float ___total0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.LoadScreenCircle::hide()
extern "C"  void LoadScreenCircle_hide_m399206192 (LoadScreenCircle_t3455683822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.LoadScreenCircle::HideNow()
extern "C"  void LoadScreenCircle_HideNow_m2028486024 (LoadScreenCircle_t3455683822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.screens.LoadScreenCircle::show()
extern "C"  void LoadScreenCircle_show_m713548331 (LoadScreenCircle_t3455683822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
