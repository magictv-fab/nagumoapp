﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameControllerExample
struct GameControllerExample_t2194515004;
// OSSubscriptionStateChanges
struct OSSubscriptionStateChanges_t52612275;
// OSPermissionStateChanges
struct OSPermissionStateChanges_t1134260069;
// OSEmailSubscriptionStateChanges
struct OSEmailSubscriptionStateChanges_t721307367;
// OSNotification
struct OSNotification_t892481775;
// OSNotificationOpenedResult
struct OSNotificationOpenedResult_t158818421;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t696267445;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OSSubscriptionStateChanges52612275.h"
#include "AssemblyU2DCSharp_OSPermissionStateChanges1134260069.h"
#include "AssemblyU2DCSharp_OSEmailSubscriptionStateChanges721307367.h"
#include "AssemblyU2DCSharp_OSNotification892481775.h"
#include "AssemblyU2DCSharp_OSNotificationOpenedResult158818421.h"
#include "mscorlib_System_String7231557.h"

// System.Void GameControllerExample::.ctor()
extern "C"  void GameControllerExample__ctor_m3840432303 (GameControllerExample_t2194515004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameControllerExample::.cctor()
extern "C"  void GameControllerExample__cctor_m2607188190 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameControllerExample::Start()
extern "C"  void GameControllerExample_Start_m2787570095 (GameControllerExample_t2194515004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameControllerExample::OneSignal_subscriptionObserver(OSSubscriptionStateChanges)
extern "C"  void GameControllerExample_OneSignal_subscriptionObserver_m302891110 (GameControllerExample_t2194515004 * __this, OSSubscriptionStateChanges_t52612275 * ___stateChanges0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameControllerExample::OneSignal_permissionObserver(OSPermissionStateChanges)
extern "C"  void GameControllerExample_OneSignal_permissionObserver_m201157990 (GameControllerExample_t2194515004 * __this, OSPermissionStateChanges_t1134260069 * ___stateChanges0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameControllerExample::OneSignal_emailSubscriptionObserver(OSEmailSubscriptionStateChanges)
extern "C"  void GameControllerExample_OneSignal_emailSubscriptionObserver_m1670749248 (GameControllerExample_t2194515004 * __this, OSEmailSubscriptionStateChanges_t721307367 * ___stateChanges0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameControllerExample::HandleNotificationReceived(OSNotification)
extern "C"  void GameControllerExample_HandleNotificationReceived_m2139856346 (Il2CppObject * __this /* static, unused */, OSNotification_t892481775 * ___notification0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameControllerExample::HandleNotificationOpened(OSNotificationOpenedResult)
extern "C"  void GameControllerExample_HandleNotificationOpened_m894220124 (Il2CppObject * __this /* static, unused */, OSNotificationOpenedResult_t158818421 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameControllerExample::OnGUI()
extern "C"  void GameControllerExample_OnGUI_m3335830953 (GameControllerExample_t2194515004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameControllerExample::<OnGUI>m__76(System.String,System.String)
extern "C"  void GameControllerExample_U3COnGUIU3Em__76_m873933723 (Il2CppObject * __this /* static, unused */, String_t* ___userId0, String_t* ___pushToken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameControllerExample::<OnGUI>m__77(System.String,System.String)
extern "C"  void GameControllerExample_U3COnGUIU3Em__77_m885249850 (Il2CppObject * __this /* static, unused */, String_t* ___userId0, String_t* ___pushToken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameControllerExample::<OnGUI>m__78()
extern "C"  void GameControllerExample_U3COnGUIU3Em__78_m434267589 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameControllerExample::<OnGUI>m__79(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void GameControllerExample_U3COnGUIU3Em__79_m3068721509 (Il2CppObject * __this /* static, unused */, Dictionary_2_t696267445 * ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameControllerExample::<OnGUI>m__7A()
extern "C"  void GameControllerExample_U3COnGUIU3Em__7A_m434276238 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameControllerExample::<OnGUI>m__7B(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void GameControllerExample_U3COnGUIU3Em__7B_m3010610862 (Il2CppObject * __this /* static, unused */, Dictionary_2_t696267445 * ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameControllerExample::<OnGUI>m__7C(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void GameControllerExample_U3COnGUIU3Em__7C_m2526935535 (Il2CppObject * __this /* static, unused */, Dictionary_2_t696267445 * ___responseSuccess0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameControllerExample::<OnGUI>m__7D(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void GameControllerExample_U3COnGUIU3Em__7D_m2043260208 (Il2CppObject * __this /* static, unused */, Dictionary_2_t696267445 * ___responseFailure0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
