﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString[]
struct FsmStringU5BU5D_t2523845914;
// HutongGames.PlayMaker.FsmFloat[]
struct FsmFloatU5BU5D_t2945380875;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SelectRandomString
struct  SelectRandomString_t2007679544  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.Actions.SelectRandomString::strings
	FsmStringU5BU5D_t2523845914* ___strings_9;
	// HutongGames.PlayMaker.FsmFloat[] HutongGames.PlayMaker.Actions.SelectRandomString::weights
	FsmFloatU5BU5D_t2945380875* ___weights_10;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SelectRandomString::storeString
	FsmString_t952858651 * ___storeString_11;

public:
	inline static int32_t get_offset_of_strings_9() { return static_cast<int32_t>(offsetof(SelectRandomString_t2007679544, ___strings_9)); }
	inline FsmStringU5BU5D_t2523845914* get_strings_9() const { return ___strings_9; }
	inline FsmStringU5BU5D_t2523845914** get_address_of_strings_9() { return &___strings_9; }
	inline void set_strings_9(FsmStringU5BU5D_t2523845914* value)
	{
		___strings_9 = value;
		Il2CppCodeGenWriteBarrier(&___strings_9, value);
	}

	inline static int32_t get_offset_of_weights_10() { return static_cast<int32_t>(offsetof(SelectRandomString_t2007679544, ___weights_10)); }
	inline FsmFloatU5BU5D_t2945380875* get_weights_10() const { return ___weights_10; }
	inline FsmFloatU5BU5D_t2945380875** get_address_of_weights_10() { return &___weights_10; }
	inline void set_weights_10(FsmFloatU5BU5D_t2945380875* value)
	{
		___weights_10 = value;
		Il2CppCodeGenWriteBarrier(&___weights_10, value);
	}

	inline static int32_t get_offset_of_storeString_11() { return static_cast<int32_t>(offsetof(SelectRandomString_t2007679544, ___storeString_11)); }
	inline FsmString_t952858651 * get_storeString_11() const { return ___storeString_11; }
	inline FsmString_t952858651 ** get_address_of_storeString_11() { return &___storeString_11; }
	inline void set_storeString_11(FsmString_t952858651 * value)
	{
		___storeString_11 = value;
		Il2CppCodeGenWriteBarrier(&___storeString_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
