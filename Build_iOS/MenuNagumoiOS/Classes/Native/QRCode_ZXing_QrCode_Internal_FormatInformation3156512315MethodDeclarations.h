﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.QrCode.Internal.FormatInformation
struct FormatInformation_t3156512315;
// ZXing.QrCode.Internal.ErrorCorrectionLevel
struct ErrorCorrectionLevel_t1225927610;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void ZXing.QrCode.Internal.FormatInformation::.ctor(System.Int32)
extern "C"  void FormatInformation__ctor_m1450507650 (FormatInformation_t3156512315 * __this, int32_t ___formatInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.QrCode.Internal.FormatInformation::numBitsDiffering(System.Int32,System.Int32)
extern "C"  int32_t FormatInformation_numBitsDiffering_m2839155485 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.QrCode.Internal.FormatInformation ZXing.QrCode.Internal.FormatInformation::decodeFormatInformation(System.Int32,System.Int32)
extern "C"  FormatInformation_t3156512315 * FormatInformation_decodeFormatInformation_m2231343159 (Il2CppObject * __this /* static, unused */, int32_t ___maskedFormatInfo10, int32_t ___maskedFormatInfo21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.QrCode.Internal.FormatInformation ZXing.QrCode.Internal.FormatInformation::doDecodeFormatInformation(System.Int32,System.Int32)
extern "C"  FormatInformation_t3156512315 * FormatInformation_doDecodeFormatInformation_m2182675938 (Il2CppObject * __this /* static, unused */, int32_t ___maskedFormatInfo10, int32_t ___maskedFormatInfo21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.QrCode.Internal.ErrorCorrectionLevel ZXing.QrCode.Internal.FormatInformation::get_ErrorCorrectionLevel()
extern "C"  ErrorCorrectionLevel_t1225927610 * FormatInformation_get_ErrorCorrectionLevel_m3203771670 (FormatInformation_t3156512315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte ZXing.QrCode.Internal.FormatInformation::get_DataMask()
extern "C"  uint8_t FormatInformation_get_DataMask_m4123422630 (FormatInformation_t3156512315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.QrCode.Internal.FormatInformation::GetHashCode()
extern "C"  int32_t FormatInformation_GetHashCode_m1615437422 (FormatInformation_t3156512315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.QrCode.Internal.FormatInformation::Equals(System.Object)
extern "C"  bool FormatInformation_Equals_m3379407574 (FormatInformation_t3156512315 * __this, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.FormatInformation::.cctor()
extern "C"  void FormatInformation__cctor_m3757553692 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
