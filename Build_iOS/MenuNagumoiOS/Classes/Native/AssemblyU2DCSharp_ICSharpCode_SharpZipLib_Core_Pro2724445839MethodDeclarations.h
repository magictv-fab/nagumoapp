﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Core.ProgressHandler
struct ProgressHandler_t2724445839;
// System.Object
struct Il2CppObject;
// ICSharpCode.SharpZipLib.Core.ProgressEventArgs
struct ProgressEventArgs_t2021439644;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Core_Pro2021439644.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ICSharpCode.SharpZipLib.Core.ProgressHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void ProgressHandler__ctor_m2625226214 (ProgressHandler_t2724445839 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.ProgressHandler::Invoke(System.Object,ICSharpCode.SharpZipLib.Core.ProgressEventArgs)
extern "C"  void ProgressHandler_Invoke_m3151537702 (ProgressHandler_t2724445839 * __this, Il2CppObject * ___sender0, ProgressEventArgs_t2021439644 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ICSharpCode.SharpZipLib.Core.ProgressHandler::BeginInvoke(System.Object,ICSharpCode.SharpZipLib.Core.ProgressEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ProgressHandler_BeginInvoke_m1704425045 (ProgressHandler_t2724445839 * __this, Il2CppObject * ___sender0, ProgressEventArgs_t2021439644 * ___e1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Core.ProgressHandler::EndInvoke(System.IAsyncResult)
extern "C"  void ProgressHandler_EndInvoke_m179837686 (ProgressHandler_t2724445839 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
