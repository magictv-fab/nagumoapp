﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.vo.RevisionInfoVO
struct RevisionInfoVO_t1983749152;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.vo.RevisionInfoVO::.ctor()
extern "C"  void RevisionInfoVO__ctor_m111396915 (RevisionInfoVO_t1983749152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.vo.RevisionInfoVO::ToString()
extern "C"  String_t* RevisionInfoVO_ToString_m2670301658 (RevisionInfoVO_t1983749152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
