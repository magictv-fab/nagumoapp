﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TBEasyWebCam.EasyWebCam
struct EasyWebCam_t134290121;
// TBEasyWebCam.CallBack.EasyWebCamStartedDelegate
struct EasyWebCamStartedDelegate_t3886828347;
// TBEasyWebCam.CallBack.EasyWebCamUpdateDelegate
struct EasyWebCamUpdateDelegate_t1731309161;
// TBEasyWebCam.CallBack.EasyWebCamStopedDelegate
struct EasyWebCamStopedDelegate_t2264447425;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;

#include "codegen/il2cpp-codegen.h"
#include "EasyWebCamLib_TBEasyWebCam_CallBack_EasyWebCamStar3886828347.h"
#include "EasyWebCamLib_TBEasyWebCam_CallBack_EasyWebCamUpda1731309161.h"
#include "EasyWebCamLib_TBEasyWebCam_CallBack_EasyWebCamStop2264447425.h"
#include "EasyWebCamLib_TBEasyWebCam_Setting_ResolutionMode805264687.h"
#include "EasyWebCamLib_TBEasyWebCam_Setting_FocusMode2918518777.h"
#include "EasyWebCamLib_TBEasyWebCam_Setting_FlashMode142630673.h"
#include "EasyWebCamLib_TBEasyWebCam_Setting_TorchMode3286129949.h"

// System.Void TBEasyWebCam.EasyWebCam::.ctor()
extern "C"  void EasyWebCam__ctor_m2942642827 (EasyWebCam_t134290121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBEasyWebCam.EasyWebCam::.cctor()
extern "C"  void EasyWebCam__cctor_m545518210 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBEasyWebCam.EasyWebCam::add_OnPreviewStart(TBEasyWebCam.CallBack.EasyWebCamStartedDelegate)
extern "C"  void EasyWebCam_add_OnPreviewStart_m1627481599 (Il2CppObject * __this /* static, unused */, EasyWebCamStartedDelegate_t3886828347 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBEasyWebCam.EasyWebCam::remove_OnPreviewStart(TBEasyWebCam.CallBack.EasyWebCamStartedDelegate)
extern "C"  void EasyWebCam_remove_OnPreviewStart_m1913703250 (Il2CppObject * __this /* static, unused */, EasyWebCamStartedDelegate_t3886828347 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBEasyWebCam.EasyWebCam::add_OnPreviewUpdate(TBEasyWebCam.CallBack.EasyWebCamUpdateDelegate)
extern "C"  void EasyWebCam_add_OnPreviewUpdate_m3392434732 (Il2CppObject * __this /* static, unused */, EasyWebCamUpdateDelegate_t1731309161 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBEasyWebCam.EasyWebCam::remove_OnPreviewUpdate(TBEasyWebCam.CallBack.EasyWebCamUpdateDelegate)
extern "C"  void EasyWebCam_remove_OnPreviewUpdate_m3678656383 (Il2CppObject * __this /* static, unused */, EasyWebCamUpdateDelegate_t1731309161 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBEasyWebCam.EasyWebCam::add_OnPreviewStoped(TBEasyWebCam.CallBack.EasyWebCamStopedDelegate)
extern "C"  void EasyWebCam_add_OnPreviewStoped_m1761453356 (Il2CppObject * __this /* static, unused */, EasyWebCamStopedDelegate_t2264447425 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBEasyWebCam.EasyWebCam::remove_OnPreviewStoped(TBEasyWebCam.CallBack.EasyWebCamStopedDelegate)
extern "C"  void EasyWebCam_remove_OnPreviewStoped_m2047675007 (Il2CppObject * __this /* static, unused */, EasyWebCamStopedDelegate_t2264447425 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D TBEasyWebCam.EasyWebCam::get_WebCamPreview()
extern "C"  Texture2D_t3884108195 * EasyWebCam_get_WebCamPreview_m3750636714 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBEasyWebCam.EasyWebCam::Awake()
extern "C"  void EasyWebCam_Awake_m3180248046 (EasyWebCam_t134290121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBEasyWebCam.EasyWebCam::Start()
extern "C"  void EasyWebCam_Start_m1889780619 (EasyWebCam_t134290121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBEasyWebCam.EasyWebCam::PreviewStart()
extern "C"  void EasyWebCam_PreviewStart_m1812874323 (EasyWebCam_t134290121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBEasyWebCam.EasyWebCam::Update()
extern "C"  void EasyWebCam_Update_m2754476514 (EasyWebCam_t134290121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBEasyWebCam.EasyWebCam::Play()
extern "C"  void EasyWebCam_Play_m2322994061 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBEasyWebCam.EasyWebCam::Stop()
extern "C"  void EasyWebCam_Stop_m2416678107 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBEasyWebCam.EasyWebCam::setPreviewResolution(TBEasyWebCam.Setting.ResolutionMode)
extern "C"  void EasyWebCam_setPreviewResolution_m1440641591 (Il2CppObject * __this /* static, unused */, uint8_t ___resolutionMode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBEasyWebCam.EasyWebCam::TakePhoto()
extern "C"  void EasyWebCam_TakePhoto_m591790804 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBEasyWebCam.EasyWebCam::setFocusMode(TBEasyWebCam.Setting.FocusMode)
extern "C"  void EasyWebCam_setFocusMode_m442608696 (Il2CppObject * __this /* static, unused */, int32_t ___paramode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBEasyWebCam.EasyWebCam::setFlashMode(TBEasyWebCam.Setting.FlashMode)
extern "C"  void EasyWebCam_setFlashMode_m3741921080 (Il2CppObject * __this /* static, unused */, int32_t ___paramode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBEasyWebCam.EasyWebCam::setTorchMode(TBEasyWebCam.Setting.TorchMode)
extern "C"  void EasyWebCam_setTorchMode_m2305671352 (Il2CppObject * __this /* static, unused */, int32_t ___paramode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TBEasyWebCam.EasyWebCam::Width()
extern "C"  int32_t EasyWebCam_Width_m1315556861 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TBEasyWebCam.EasyWebCam::Height()
extern "C"  int32_t EasyWebCam_Height_m2631371378 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TBEasyWebCam.EasyWebCam::getFrame()
extern "C"  int32_t EasyWebCam_getFrame_m254258786 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBEasyWebCam.EasyWebCam::OnPause(System.Boolean)
extern "C"  void EasyWebCam_OnPause_m1689652407 (EasyWebCam_t134290121 * __this, bool ___isPaused0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBEasyWebCam.EasyWebCam::OnRelease()
extern "C"  void EasyWebCam_OnRelease_m3945036497 (EasyWebCam_t134290121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBEasyWebCam.EasyWebCam::Release()
extern "C"  void EasyWebCam_Release_m1528285168 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TBEasyWebCam.EasyWebCam::isPlaying()
extern "C"  bool EasyWebCam_isPlaying_m2062268193 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
