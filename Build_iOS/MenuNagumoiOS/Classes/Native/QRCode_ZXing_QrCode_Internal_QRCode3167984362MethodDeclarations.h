﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.QrCode.Internal.QRCode
struct QRCode_t3167984362;
// ZXing.QrCode.Internal.Mode
struct Mode_t2660577215;
// ZXing.QrCode.Internal.ErrorCorrectionLevel
struct ErrorCorrectionLevel_t1225927610;
// ZXing.QrCode.Internal.Version
struct Version_t1953509534;
// ZXing.QrCode.Internal.ByteMatrix
struct ByteMatrix_t2072255685;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_QrCode_Internal_Mode2660577215.h"
#include "QRCode_ZXing_QrCode_Internal_ErrorCorrectionLevel1225927610.h"
#include "QRCode_ZXing_QrCode_Internal_Version1953509534.h"
#include "QRCode_ZXing_QrCode_Internal_ByteMatrix2072255685.h"

// System.Void ZXing.QrCode.Internal.QRCode::.ctor()
extern "C"  void QRCode__ctor_m2991854162 (QRCode_t3167984362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.QrCode.Internal.Mode ZXing.QrCode.Internal.QRCode::get_Mode()
extern "C"  Mode_t2660577215 * QRCode_get_Mode_m3243740605 (QRCode_t3167984362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.QRCode::set_Mode(ZXing.QrCode.Internal.Mode)
extern "C"  void QRCode_set_Mode_m1987037748 (QRCode_t3167984362 * __this, Mode_t2660577215 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.QrCode.Internal.ErrorCorrectionLevel ZXing.QrCode.Internal.QRCode::get_ECLevel()
extern "C"  ErrorCorrectionLevel_t1225927610 * QRCode_get_ECLevel_m1306289193 (QRCode_t3167984362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.QRCode::set_ECLevel(ZXing.QrCode.Internal.ErrorCorrectionLevel)
extern "C"  void QRCode_set_ECLevel_m54984704 (QRCode_t3167984362 * __this, ErrorCorrectionLevel_t1225927610 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.QrCode.Internal.Version ZXing.QrCode.Internal.QRCode::get_Version()
extern "C"  Version_t1953509534 * QRCode_get_Version_m3105339881 (QRCode_t3167984362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.QRCode::set_Version(ZXing.QrCode.Internal.Version)
extern "C"  void QRCode_set_Version_m1014611722 (QRCode_t3167984362 * __this, Version_t1953509534 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.QrCode.Internal.QRCode::get_MaskPattern()
extern "C"  int32_t QRCode_get_MaskPattern_m1946589309 (QRCode_t3167984362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.QRCode::set_MaskPattern(System.Int32)
extern "C"  void QRCode_set_MaskPattern_m3188463720 (QRCode_t3167984362 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.QrCode.Internal.ByteMatrix ZXing.QrCode.Internal.QRCode::get_Matrix()
extern "C"  ByteMatrix_t2072255685 * QRCode_get_Matrix_m1327850901 (QRCode_t3167984362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.QRCode::set_Matrix(ZXing.QrCode.Internal.ByteMatrix)
extern "C"  void QRCode_set_Matrix_m3694272076 (QRCode_t3167984362 * __this, ByteMatrix_t2072255685 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.QrCode.Internal.QRCode::ToString()
extern "C"  String_t* QRCode_ToString_m3162927809 (QRCode_t3167984362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.QrCode.Internal.QRCode::isValidMaskPattern(System.Int32)
extern "C"  bool QRCode_isValidMaskPattern_m3280486729 (Il2CppObject * __this /* static, unused */, int32_t ___maskPattern0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.QRCode::.cctor()
extern "C"  void QRCode__cctor_m2071069595 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
