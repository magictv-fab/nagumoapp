﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CarrosselController
struct CarrosselController_t3044697396;

#include "codegen/il2cpp-codegen.h"

// System.Void CarrosselController::.ctor()
extern "C"  void CarrosselController__ctor_m242239671 (CarrosselController_t3044697396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CarrosselController::Start()
extern "C"  void CarrosselController_Start_m3484344759 (CarrosselController_t3044697396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CarrosselController::Update()
extern "C"  void CarrosselController_Update_m646357302 (CarrosselController_t3044697396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
