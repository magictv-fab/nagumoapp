﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmTemplateControl
struct FsmTemplateControl_t2786508133;
// HutongGames.PlayMaker.Fsm
struct Fsm_t1527112426;
// FsmTemplate
struct FsmTemplate_t1237263802;
// HutongGames.PlayMaker.FsmVarOverride[]
struct FsmVarOverrideU5BU5D_t3759009944;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm1527112426.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTemplateControl2786508133.h"
#include "PlayMaker_FsmTemplate1237263802.h"

// System.Int32 HutongGames.PlayMaker.FsmTemplateControl::get_ID()
extern "C"  int32_t FsmTemplateControl_get_ID_m412961676 (FsmTemplateControl_t2786508133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTemplateControl::set_ID(System.Int32)
extern "C"  void FsmTemplateControl_set_ID_m583624959 (FsmTemplateControl_t2786508133 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.FsmTemplateControl::get_RunFsm()
extern "C"  Fsm_t1527112426 * FsmTemplateControl_get_RunFsm_m18082387 (FsmTemplateControl_t2786508133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTemplateControl::set_RunFsm(HutongGames.PlayMaker.Fsm)
extern "C"  void FsmTemplateControl_set_RunFsm_m236454806 (FsmTemplateControl_t2786508133 * __this, Fsm_t1527112426 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTemplateControl::.ctor()
extern "C"  void FsmTemplateControl__ctor_m4040770958 (FsmTemplateControl_t2786508133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTemplateControl::.ctor(HutongGames.PlayMaker.FsmTemplateControl)
extern "C"  void FsmTemplateControl__ctor_m2898585279 (FsmTemplateControl_t2786508133 * __this, FsmTemplateControl_t2786508133 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTemplateControl::SetFsmTemplate(FsmTemplate)
extern "C"  void FsmTemplateControl_SetFsmTemplate_m1445422826 (FsmTemplateControl_t2786508133 * __this, FsmTemplate_t1237263802 * ___template0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.FsmTemplateControl::InstantiateFsm()
extern "C"  Fsm_t1527112426 * FsmTemplateControl_InstantiateFsm_m2092517515 (FsmTemplateControl_t2786508133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVarOverride[] HutongGames.PlayMaker.FsmTemplateControl::CopyOverrides(HutongGames.PlayMaker.FsmTemplateControl)
extern "C"  FsmVarOverrideU5BU5D_t3759009944* FsmTemplateControl_CopyOverrides_m2409630991 (Il2CppObject * __this /* static, unused */, FsmTemplateControl_t2786508133 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTemplateControl::ClearOverrides()
extern "C"  void FsmTemplateControl_ClearOverrides_m3550343056 (FsmTemplateControl_t2786508133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTemplateControl::UpdateOverrides()
extern "C"  void FsmTemplateControl_UpdateOverrides_m3856959690 (FsmTemplateControl_t2786508133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTemplateControl::UpdateValues()
extern "C"  void FsmTemplateControl_UpdateValues_m2558089601 (FsmTemplateControl_t2786508133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTemplateControl::ApplyOverrides(HutongGames.PlayMaker.Fsm)
extern "C"  void FsmTemplateControl_ApplyOverrides_m1401694351 (FsmTemplateControl_t2786508133 * __this, Fsm_t1527112426 * ___overrideFsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
