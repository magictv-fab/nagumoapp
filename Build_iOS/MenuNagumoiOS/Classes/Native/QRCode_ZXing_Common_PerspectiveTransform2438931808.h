﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.PerspectiveTransform
struct  PerspectiveTransform_t2438931808  : public Il2CppObject
{
public:
	// System.Single ZXing.Common.PerspectiveTransform::a11
	float ___a11_0;
	// System.Single ZXing.Common.PerspectiveTransform::a12
	float ___a12_1;
	// System.Single ZXing.Common.PerspectiveTransform::a13
	float ___a13_2;
	// System.Single ZXing.Common.PerspectiveTransform::a21
	float ___a21_3;
	// System.Single ZXing.Common.PerspectiveTransform::a22
	float ___a22_4;
	// System.Single ZXing.Common.PerspectiveTransform::a23
	float ___a23_5;
	// System.Single ZXing.Common.PerspectiveTransform::a31
	float ___a31_6;
	// System.Single ZXing.Common.PerspectiveTransform::a32
	float ___a32_7;
	// System.Single ZXing.Common.PerspectiveTransform::a33
	float ___a33_8;

public:
	inline static int32_t get_offset_of_a11_0() { return static_cast<int32_t>(offsetof(PerspectiveTransform_t2438931808, ___a11_0)); }
	inline float get_a11_0() const { return ___a11_0; }
	inline float* get_address_of_a11_0() { return &___a11_0; }
	inline void set_a11_0(float value)
	{
		___a11_0 = value;
	}

	inline static int32_t get_offset_of_a12_1() { return static_cast<int32_t>(offsetof(PerspectiveTransform_t2438931808, ___a12_1)); }
	inline float get_a12_1() const { return ___a12_1; }
	inline float* get_address_of_a12_1() { return &___a12_1; }
	inline void set_a12_1(float value)
	{
		___a12_1 = value;
	}

	inline static int32_t get_offset_of_a13_2() { return static_cast<int32_t>(offsetof(PerspectiveTransform_t2438931808, ___a13_2)); }
	inline float get_a13_2() const { return ___a13_2; }
	inline float* get_address_of_a13_2() { return &___a13_2; }
	inline void set_a13_2(float value)
	{
		___a13_2 = value;
	}

	inline static int32_t get_offset_of_a21_3() { return static_cast<int32_t>(offsetof(PerspectiveTransform_t2438931808, ___a21_3)); }
	inline float get_a21_3() const { return ___a21_3; }
	inline float* get_address_of_a21_3() { return &___a21_3; }
	inline void set_a21_3(float value)
	{
		___a21_3 = value;
	}

	inline static int32_t get_offset_of_a22_4() { return static_cast<int32_t>(offsetof(PerspectiveTransform_t2438931808, ___a22_4)); }
	inline float get_a22_4() const { return ___a22_4; }
	inline float* get_address_of_a22_4() { return &___a22_4; }
	inline void set_a22_4(float value)
	{
		___a22_4 = value;
	}

	inline static int32_t get_offset_of_a23_5() { return static_cast<int32_t>(offsetof(PerspectiveTransform_t2438931808, ___a23_5)); }
	inline float get_a23_5() const { return ___a23_5; }
	inline float* get_address_of_a23_5() { return &___a23_5; }
	inline void set_a23_5(float value)
	{
		___a23_5 = value;
	}

	inline static int32_t get_offset_of_a31_6() { return static_cast<int32_t>(offsetof(PerspectiveTransform_t2438931808, ___a31_6)); }
	inline float get_a31_6() const { return ___a31_6; }
	inline float* get_address_of_a31_6() { return &___a31_6; }
	inline void set_a31_6(float value)
	{
		___a31_6 = value;
	}

	inline static int32_t get_offset_of_a32_7() { return static_cast<int32_t>(offsetof(PerspectiveTransform_t2438931808, ___a32_7)); }
	inline float get_a32_7() const { return ___a32_7; }
	inline float* get_address_of_a32_7() { return &___a32_7; }
	inline void set_a32_7(float value)
	{
		___a32_7 = value;
	}

	inline static int32_t get_offset_of_a33_8() { return static_cast<int32_t>(offsetof(PerspectiveTransform_t2438931808, ___a33_8)); }
	inline float get_a33_8() const { return ___a33_8; }
	inline float* get_address_of_a33_8() { return &___a33_8; }
	inline void set_a33_8(float value)
	{
		___a33_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
