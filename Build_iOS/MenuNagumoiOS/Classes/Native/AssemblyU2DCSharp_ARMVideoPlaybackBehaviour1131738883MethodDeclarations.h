﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARMVideoPlaybackBehaviour
struct ARMVideoPlaybackBehaviour_t1131738883;
// ARMVideoPlaybackBehaviour/OnVoidEvent
struct OnVoidEvent_t2058934555;
// VideoPlayerHelper
struct VideoPlayerHelper_t638710250;
// UnityEngine.Texture
struct Texture_t2526458961;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ARMVideoPlaybackBehaviour_OnVoid2058934555.h"
#include "AssemblyU2DCSharp_VideoPlayerHelper_MediaState2586048626.h"
#include "AssemblyU2DCSharp_VideoPlayerHelper_MediaType3131497273.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"

// System.Void ARMVideoPlaybackBehaviour::.ctor()
extern "C"  void ARMVideoPlaybackBehaviour__ctor_m880902600 (ARMVideoPlaybackBehaviour_t1131738883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVideoPlaybackBehaviour::AddYouCanShowMe(ARMVideoPlaybackBehaviour/OnVoidEvent)
extern "C"  void ARMVideoPlaybackBehaviour_AddYouCanShowMe_m616717034 (ARMVideoPlaybackBehaviour_t1131738883 * __this, OnVoidEvent_t2058934555 * ___eventHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVideoPlaybackBehaviour::RaiseYouCanShowMe()
extern "C"  void ARMVideoPlaybackBehaviour_RaiseYouCanShowMe_m3716209912 (ARMVideoPlaybackBehaviour_t1131738883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VideoPlayerHelper ARMVideoPlaybackBehaviour::get_VideoPlayer()
extern "C"  VideoPlayerHelper_t638710250 * ARMVideoPlaybackBehaviour_get_VideoPlayer_m2692202722 (ARMVideoPlaybackBehaviour_t1131738883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VideoPlayerHelper/MediaState ARMVideoPlaybackBehaviour::get_CurrentState()
extern "C"  int32_t ARMVideoPlaybackBehaviour_get_CurrentState_m3648677490 (ARMVideoPlaybackBehaviour_t1131738883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VideoPlayerHelper/MediaType ARMVideoPlaybackBehaviour::get_MediaType()
extern "C"  int32_t ARMVideoPlaybackBehaviour_get_MediaType_m3756867893 (ARMVideoPlaybackBehaviour_t1131738883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVideoPlaybackBehaviour::set_MediaType(VideoPlayerHelper/MediaType)
extern "C"  void ARMVideoPlaybackBehaviour_set_MediaType_m3404809778 (ARMVideoPlaybackBehaviour_t1131738883 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture ARMVideoPlaybackBehaviour::get_KeyframeTexture()
extern "C"  Texture_t2526458961 * ARMVideoPlaybackBehaviour_get_KeyframeTexture_m814948377 (ARMVideoPlaybackBehaviour_t1131738883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVideoPlaybackBehaviour::set_KeyframeTexture(UnityEngine.Texture)
extern "C"  void ARMVideoPlaybackBehaviour_set_KeyframeTexture_m3416091800 (ARMVideoPlaybackBehaviour_t1131738883 * __this, Texture_t2526458961 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVideoPlaybackBehaviour::Start()
extern "C"  void ARMVideoPlaybackBehaviour_Start_m4123007688 (ARMVideoPlaybackBehaviour_t1131738883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVideoPlaybackBehaviour::Init()
extern "C"  void ARMVideoPlaybackBehaviour_Init_m2335257484 (ARMVideoPlaybackBehaviour_t1131738883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVideoPlaybackBehaviour::OnRenderObject()
extern "C"  void ARMVideoPlaybackBehaviour_OnRenderObject_m3748305840 (ARMVideoPlaybackBehaviour_t1131738883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVideoPlaybackBehaviour::OnDestroy()
extern "C"  void ARMVideoPlaybackBehaviour_OnDestroy_m3232576193 (ARMVideoPlaybackBehaviour_t1131738883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVideoPlaybackBehaviour::IniciaVideo()
extern "C"  void ARMVideoPlaybackBehaviour_IniciaVideo_m4206555786 (ARMVideoPlaybackBehaviour_t1131738883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVideoPlaybackBehaviour::PreparaVideo()
extern "C"  void ARMVideoPlaybackBehaviour_PreparaVideo_m2095282868 (ARMVideoPlaybackBehaviour_t1131738883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVideoPlaybackBehaviour::InitVideoTexture()
extern "C"  void ARMVideoPlaybackBehaviour_InitVideoTexture_m4035340108 (ARMVideoPlaybackBehaviour_t1131738883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVideoPlaybackBehaviour::Loop()
extern "C"  void ARMVideoPlaybackBehaviour_Loop_m2422243360 (ARMVideoPlaybackBehaviour_t1131738883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVideoPlaybackBehaviour::Play()
extern "C"  void ARMVideoPlaybackBehaviour_Play_m2533580976 (ARMVideoPlaybackBehaviour_t1131738883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVideoPlaybackBehaviour::DelayParaOPrimeiroPlay()
extern "C"  void ARMVideoPlaybackBehaviour_DelayParaOPrimeiroPlay_m2345008163 (ARMVideoPlaybackBehaviour_t1131738883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVideoPlaybackBehaviour::BasePlay()
extern "C"  void ARMVideoPlaybackBehaviour_BasePlay_m1762065761 (ARMVideoPlaybackBehaviour_t1131738883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVideoPlaybackBehaviour::Stop()
extern "C"  void ARMVideoPlaybackBehaviour_Stop_m2627265022 (ARMVideoPlaybackBehaviour_t1131738883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVideoPlaybackBehaviour::Pause()
extern "C"  void ARMVideoPlaybackBehaviour_Pause_m935028572 (ARMVideoPlaybackBehaviour_t1131738883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVideoPlaybackBehaviour::VideoPause()
extern "C"  void ARMVideoPlaybackBehaviour_VideoPause_m4277575095 (ARMVideoPlaybackBehaviour_t1131738883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVideoPlaybackBehaviour::HandleStateChange(VideoPlayerHelper/MediaState)
extern "C"  void ARMVideoPlaybackBehaviour_HandleStateChange_m573997645 (ARMVideoPlaybackBehaviour_t1131738883 * __this, int32_t ___newState0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
