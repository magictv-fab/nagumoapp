﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.MouseLook
struct MouseLook_t696688690;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmFloat2134102846.h"

// System.Void HutongGames.PlayMaker.Actions.MouseLook::.ctor()
extern "C"  void MouseLook__ctor_m4153572676 (MouseLook_t696688690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MouseLook::Reset()
extern "C"  void MouseLook_Reset_m1800005617 (MouseLook_t696688690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MouseLook::OnEnter()
extern "C"  void MouseLook_OnEnter_m950452251 (MouseLook_t696688690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MouseLook::OnUpdate()
extern "C"  void MouseLook_OnUpdate_m2827775368 (MouseLook_t696688690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MouseLook::DoMouseLook()
extern "C"  void MouseLook_DoMouseLook_m1084452859 (MouseLook_t696688690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.MouseLook::GetXRotation()
extern "C"  float MouseLook_GetXRotation_m1436289940 (MouseLook_t696688690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.MouseLook::GetYRotation()
extern "C"  float MouseLook_GetYRotation_m3934305877 (MouseLook_t696688690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.MouseLook::ClampAngle(System.Single,HutongGames.PlayMaker.FsmFloat,HutongGames.PlayMaker.FsmFloat)
extern "C"  float MouseLook_ClampAngle_m1957514079 (Il2CppObject * __this /* static, unused */, float ___angle0, FsmFloat_t2134102846 * ___min1, FsmFloat_t2134102846 * ___max2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
