﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DeviceFileInfoDAO
struct DeviceFileInfoDAO_t152212114;
// MagicTV.vo.ResultRevisionVO
struct ResultRevisionVO_t2445597391;
// MagicTV.vo.BundleVO[]
struct BundleVOU5BU5D_t2162892100;
// MagicTV.vo.FileVO[]
struct FileVOU5BU5D_t573513762;
// MagicTV.vo.UrlInfoVO[]
struct UrlInfoVOU5BU5D_t1681515545;
// MagicTV.vo.BundleVO
struct BundleVO_t1984518073;
// MagicTV.vo.FileVO
struct FileVO_t1935348659;
// MagicTV.vo.MetadataVO[]
struct MetadataVOU5BU5D_t4238035203;
// System.String
struct String_t;
// MagicTV.vo.MetadataVO
struct MetadataVO_t2511256998;
// MagicTV.vo.UrlInfoVO
struct UrlInfoVO_t1761987528;
// MagicTV.vo.KeyValueVO
struct KeyValueVO_t1780100105;
// MagicTV.vo.KeyValueVO[]
struct KeyValueVOU5BU5D_t1359699380;
// ReturnDataVO
struct ReturnDataVO_t544837971;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t3308144514;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_MagicTV_vo_MetadataVO2511256998.h"
#include "AssemblyU2DCSharp_MagicTV_vo_KeyValueVO1780100105.h"
#include "AssemblyU2DCSharp_MagicTV_vo_ResultRevisionVO2445597391.h"
#include "AssemblyU2DCSharp_MagicTV_vo_BundleVO1984518073.h"
#include "AssemblyU2DCSharp_MagicTV_vo_FileVO1935348659.h"
#include "AssemblyU2DCSharp_MagicTV_vo_UrlInfoVO1761987528.h"

// System.Void DeviceFileInfoDAO::.ctor()
extern "C"  void DeviceFileInfoDAO__ctor_m3613908761 (DeviceFileInfoDAO_t152212114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceFileInfoDAO::prepare()
extern "C"  void DeviceFileInfoDAO_prepare_m3559724574 (DeviceFileInfoDAO_t152212114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceFileInfoDAO::Update()
extern "C"  void DeviceFileInfoDAO_Update_m2088883988 (DeviceFileInfoDAO_t152212114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceFileInfoDAO::UpdateDataBase()
extern "C"  void DeviceFileInfoDAO_UpdateDataBase_m881163023 (DeviceFileInfoDAO_t152212114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceFileInfoDAO::ResetdataBase()
extern "C"  void DeviceFileInfoDAO_ResetdataBase_m810827681 (DeviceFileInfoDAO_t152212114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.ResultRevisionVO DeviceFileInfoDAO::SelectCurrentResultRevisionVO()
extern "C"  ResultRevisionVO_t2445597391 * DeviceFileInfoDAO_SelectCurrentResultRevisionVO_m1371317777 (DeviceFileInfoDAO_t152212114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.ResultRevisionVO DeviceFileInfoDAO::SelectResultRevisionVOByRevision(System.Int32)
extern "C"  ResultRevisionVO_t2445597391 * DeviceFileInfoDAO_SelectResultRevisionVOByRevision_m1477488303 (DeviceFileInfoDAO_t152212114 * __this, int32_t ___revision0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.BundleVO[] DeviceFileInfoDAO::SelectBundlesVOByRevision(System.Int32)
extern "C"  BundleVOU5BU5D_t2162892100* DeviceFileInfoDAO_SelectBundlesVOByRevision_m1549571656 (DeviceFileInfoDAO_t152212114 * __this, int32_t ___revision0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.FileVO[] DeviceFileInfoDAO::SelectFilesVOByBundleId(System.Int32)
extern "C"  FileVOU5BU5D_t573513762* DeviceFileInfoDAO_SelectFilesVOByBundleId_m771486710 (DeviceFileInfoDAO_t152212114 * __this, int32_t ___bundle_id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.UrlInfoVO[] DeviceFileInfoDAO::SelectUrlInfoVOByFilesId(System.Int32)
extern "C"  UrlInfoVOU5BU5D_t1681515545* DeviceFileInfoDAO_SelectUrlInfoVOByFilesId_m2807957154 (DeviceFileInfoDAO_t152212114 * __this, int32_t ___file_id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.BundleVO DeviceFileInfoDAO::SelectBundlesVOById(System.Int32)
extern "C"  BundleVO_t1984518073 * DeviceFileInfoDAO_SelectBundlesVOById_m2346217578 (DeviceFileInfoDAO_t152212114 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.FileVO DeviceFileInfoDAO::SelectFilesVOById(System.Int32)
extern "C"  FileVO_t1935348659 * DeviceFileInfoDAO_SelectFilesVOById_m1179913014 (DeviceFileInfoDAO_t152212114 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.MetadataVO[] DeviceFileInfoDAO::SelectBundleMetadataVOByBundleId(System.Int32)
extern "C"  MetadataVOU5BU5D_t4238035203* DeviceFileInfoDAO_SelectBundleMetadataVOByBundleId_m2723693827 (DeviceFileInfoDAO_t152212114 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.MetadataVO[] DeviceFileInfoDAO::SelectAppMetadataVOByRevisionId(System.Int32)
extern "C"  MetadataVOU5BU5D_t4238035203* DeviceFileInfoDAO_SelectAppMetadataVOByRevisionId_m1423278517 (DeviceFileInfoDAO_t152212114 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.MetadataVO[] DeviceFileInfoDAO::SelectMetadataVOByTableAndTableId(System.String,System.Int32)
extern "C"  MetadataVOU5BU5D_t4238035203* DeviceFileInfoDAO_SelectMetadataVOByTableAndTableId_m2824774756 (DeviceFileInfoDAO_t152212114 * __this, String_t* ___tabela0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.MetadataVO DeviceFileInfoDAO::SelectAppMetadataVOByMetadataVO(MagicTV.vo.MetadataVO)
extern "C"  MetadataVO_t2511256998 * DeviceFileInfoDAO_SelectAppMetadataVOByMetadataVO_m680901276 (DeviceFileInfoDAO_t152212114 * __this, MetadataVO_t2511256998 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.MetadataVO DeviceFileInfoDAO::SelectBundleMetadataVOByMetadataVO(MagicTV.vo.MetadataVO)
extern "C"  MetadataVO_t2511256998 * DeviceFileInfoDAO_SelectBundleMetadataVOByMetadataVO_m3705896537 (DeviceFileInfoDAO_t152212114 * __this, MetadataVO_t2511256998 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.MetadataVO DeviceFileInfoDAO::SelectBundleMetadataVOById(System.Int32)
extern "C"  MetadataVO_t2511256998 * DeviceFileInfoDAO_SelectBundleMetadataVOById_m2495427519 (DeviceFileInfoDAO_t152212114 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.MetadataVO DeviceFileInfoDAO::SelectAppMetadataVOById(System.Int32)
extern "C"  MetadataVO_t2511256998 * DeviceFileInfoDAO_SelectAppMetadataVOById_m452803868 (DeviceFileInfoDAO_t152212114 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.UrlInfoVO DeviceFileInfoDAO::SelectUrlInfoVOById(System.Int32)
extern "C"  UrlInfoVO_t1761987528 * DeviceFileInfoDAO_SelectUrlInfoVOById_m1561498609 (DeviceFileInfoDAO_t152212114 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.UrlInfoVO[] DeviceFileInfoDAO::SelectDeactiveUrlInfoVO()
extern "C"  UrlInfoVOU5BU5D_t1681515545* DeviceFileInfoDAO_SelectDeactiveUrlInfoVO_m638185477 (DeviceFileInfoDAO_t152212114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.KeyValueVO DeviceFileInfoDAO::SelectKeyValueVOById(System.Int32)
extern "C"  KeyValueVO_t1780100105 * DeviceFileInfoDAO_SelectKeyValueVOById_m3648200867 (DeviceFileInfoDAO_t152212114 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.KeyValueVO[] DeviceFileInfoDAO::SelectKeyValueVOByKey(System.String)
extern "C"  KeyValueVOU5BU5D_t1359699380* DeviceFileInfoDAO_SelectKeyValueVOByKey_m1278590906 (DeviceFileInfoDAO_t152212114 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.KeyValueVO[] DeviceFileInfoDAO::SelectKeyValueVO(MagicTV.vo.KeyValueVO)
extern "C"  KeyValueVOU5BU5D_t1359699380* DeviceFileInfoDAO_SelectKeyValueVO_m2918407983 (DeviceFileInfoDAO_t152212114 * __this, KeyValueVO_t1780100105 * ___vo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::SelectResultRevision(System.String[],System.String[],System.String[])
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_SelectResultRevision_m1678719103 (DeviceFileInfoDAO_t152212114 * __this, StringU5BU5D_t4054002952* ___col0, StringU5BU5D_t4054002952* ___operation1, StringU5BU5D_t4054002952* ___values2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::SelectBundle(System.String[],System.String[],System.String[])
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_SelectBundle_m3121548373 (DeviceFileInfoDAO_t152212114 * __this, StringU5BU5D_t4054002952* ___col0, StringU5BU5D_t4054002952* ___operation1, StringU5BU5D_t4054002952* ___values2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::SelectFile(System.String[],System.String[],System.String[])
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_SelectFile_m638978971 (DeviceFileInfoDAO_t152212114 * __this, StringU5BU5D_t4054002952* ___col0, StringU5BU5D_t4054002952* ___operation1, StringU5BU5D_t4054002952* ___values2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::SelectAppMetadata(System.String[],System.String[],System.String[])
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_SelectAppMetadata_m3769111007 (DeviceFileInfoDAO_t152212114 * __this, StringU5BU5D_t4054002952* ___col0, StringU5BU5D_t4054002952* ___operation1, StringU5BU5D_t4054002952* ___values2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::SelectBundleMetadata(System.String[],System.String[],System.String[])
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_SelectBundleMetadata_m2584284902 (DeviceFileInfoDAO_t152212114 * __this, StringU5BU5D_t4054002952* ___col0, StringU5BU5D_t4054002952* ___operation1, StringU5BU5D_t4054002952* ___values2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::SelectUrlInfo(System.String[],System.String[],System.String[])
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_SelectUrlInfo_m613552434 (DeviceFileInfoDAO_t152212114 * __this, StringU5BU5D_t4054002952* ___col0, StringU5BU5D_t4054002952* ___operation1, StringU5BU5D_t4054002952* ___values2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::SelectKeyValue(System.String[],System.String[],System.String[])
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_SelectKeyValue_m3664635781 (DeviceFileInfoDAO_t152212114 * __this, StringU5BU5D_t4054002952* ___col0, StringU5BU5D_t4054002952* ___operation1, StringU5BU5D_t4054002952* ___values2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::SelectAll(System.String)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_SelectAll_m3928329212 (DeviceFileInfoDAO_t152212114 * __this, String_t* ___tableName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::SelectWhere(System.String,System.String[],System.String[],System.String[],System.String[])
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_SelectWhere_m3720926814 (DeviceFileInfoDAO_t152212114 * __this, String_t* ___tableName0, StringU5BU5D_t4054002952* ___items1, StringU5BU5D_t4054002952* ___col2, StringU5BU5D_t4054002952* ___operation3, StringU5BU5D_t4054002952* ___values4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::InsertResultRevisionVO(MagicTV.vo.ResultRevisionVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_InsertResultRevisionVO_m906088416 (DeviceFileInfoDAO_t152212114 * __this, ResultRevisionVO_t2445597391 * ___revisionResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::InsertBundleVO(MagicTV.vo.BundleVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_InsertBundleVO_m1616614540 (DeviceFileInfoDAO_t152212114 * __this, BundleVO_t1984518073 * ___bundle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::InsertFileVO(MagicTV.vo.FileVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_InsertFileVO_m4110155224 (DeviceFileInfoDAO_t152212114 * __this, FileVO_t1935348659 * ___file0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::InsertBundleMetadataVO(MagicTV.vo.MetadataVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_InsertBundleMetadataVO_m3638348240 (DeviceFileInfoDAO_t152212114 * __this, MetadataVO_t2511256998 * ___metadata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::InsertAppMetadataVO(MagicTV.vo.MetadataVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_InsertAppMetadataVO_m3488183139 (DeviceFileInfoDAO_t152212114 * __this, MetadataVO_t2511256998 * ___metadata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::InsertUrlInfoVO(MagicTV.vo.UrlInfoVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_InsertUrlInfoVO_m1819810526 (DeviceFileInfoDAO_t152212114 * __this, UrlInfoVO_t1761987528 * ___urlInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::InsertKeyValueVO(MagicTV.vo.KeyValueVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_InsertKeyValueVO_m45507500 (DeviceFileInfoDAO_t152212114 * __this, KeyValueVO_t1780100105 * ___keyvalue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::DeleteVO(MagicTV.vo.ResultRevisionVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_DeleteVO_m728475942 (DeviceFileInfoDAO_t152212114 * __this, ResultRevisionVO_t2445597391 * ___vo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::DeleteVO(MagicTV.vo.BundleVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_DeleteVO_m2132807036 (DeviceFileInfoDAO_t152212114 * __this, BundleVO_t1984518073 * ___vo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::DeleteAppMetadataVO(MagicTV.vo.MetadataVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_DeleteAppMetadataVO_m4081853781 (DeviceFileInfoDAO_t152212114 * __this, MetadataVO_t2511256998 * ___vo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::DeleteBundleMetadataVO(MagicTV.vo.MetadataVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_DeleteBundleMetadataVO_m3005119134 (DeviceFileInfoDAO_t152212114 * __this, MetadataVO_t2511256998 * ___vo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::DeleteVO(MagicTV.vo.FileVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_DeleteVO_m1383383490 (DeviceFileInfoDAO_t152212114 * __this, FileVO_t1935348659 * ___vo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::DeleteVO(MagicTV.vo.UrlInfoVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_DeleteVO_m1761633733 (DeviceFileInfoDAO_t152212114 * __this, UrlInfoVO_t1761987528 * ___vo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::DeleteVO(MagicTV.vo.KeyValueVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_DeleteVO_m2805142060 (DeviceFileInfoDAO_t152212114 * __this, KeyValueVO_t1780100105 * ___vo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::DeleteVO(MagicTV.vo.BundleVO[])
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_DeleteVO_m928212378 (DeviceFileInfoDAO_t152212114 * __this, BundleVOU5BU5D_t2162892100* ___vo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::DeleteAppMetadataVO(MagicTV.vo.MetadataVO[])
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_DeleteAppMetadataVO_m1356393267 (DeviceFileInfoDAO_t152212114 * __this, MetadataVOU5BU5D_t4238035203* ___vo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::DeleteBundleMetadataVO(MagicTV.vo.MetadataVO[])
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_DeleteBundleMetadataVO_m1701515836 (DeviceFileInfoDAO_t152212114 * __this, MetadataVOU5BU5D_t4238035203* ___vo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::DeleteVO(MagicTV.vo.FileVO[])
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_DeleteVO_m2286690400 (DeviceFileInfoDAO_t152212114 * __this, FileVOU5BU5D_t573513762* ___vo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::DeleteVO(MagicTV.vo.UrlInfoVO[])
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_DeleteVO_m712953763 (DeviceFileInfoDAO_t152212114 * __this, UrlInfoVOU5BU5D_t1681515545* ___vo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::DeleteWhereIdIsDifferent(System.String,System.String[])
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_DeleteWhereIdIsDifferent_m3232688003 (DeviceFileInfoDAO_t152212114 * __this, String_t* ___tableName0, StringU5BU5D_t4054002952* ___values1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::DeleteWhereAppMetadataIdIsDifferent(System.String,System.String[])
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_DeleteWhereAppMetadataIdIsDifferent_m3415893595 (DeviceFileInfoDAO_t152212114 * __this, String_t* ___table0, StringU5BU5D_t4054002952* ___values1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::DeleteWhereBundleMetadataIdIsDifferent(System.String,System.String[])
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_DeleteWhereBundleMetadataIdIsDifferent_m2832306932 (DeviceFileInfoDAO_t152212114 * __this, String_t* ___table0, StringU5BU5D_t4054002952* ___values1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::DeactiveFileUrlWhereIdIsDifferent(System.String,System.String[])
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_DeactiveFileUrlWhereIdIsDifferent_m1157595586 (DeviceFileInfoDAO_t152212114 * __this, String_t* ___tableName0, StringU5BU5D_t4054002952* ___values1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::UpdateResultRevisionVO(MagicTV.vo.ResultRevisionVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_UpdateResultRevisionVO_m1888678864 (DeviceFileInfoDAO_t152212114 * __this, ResultRevisionVO_t2445597391 * ___revisionResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::UpdateBundleVO(MagicTV.vo.BundleVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_UpdateBundleVO_m1276287100 (DeviceFileInfoDAO_t152212114 * __this, BundleVO_t1984518073 * ___bundle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::UpdateFileVO(MagicTV.vo.FileVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_UpdateFileVO_m422738376 (DeviceFileInfoDAO_t152212114 * __this, FileVO_t1935348659 * ___file0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::UpdateAppMetadataVO(MagicTV.vo.MetadataVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_UpdateAppMetadataVO_m2666733939 (DeviceFileInfoDAO_t152212114 * __this, MetadataVO_t2511256998 * ___metadata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::UpdateBundleMetadataVO(MagicTV.vo.MetadataVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_UpdateBundleMetadataVO_m273916352 (DeviceFileInfoDAO_t152212114 * __this, MetadataVO_t2511256998 * ___metadata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::UpdateUrlInfoVO(MagicTV.vo.UrlInfoVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_UpdateUrlInfoVO_m1182655182 (DeviceFileInfoDAO_t152212114 * __this, UrlInfoVO_t1761987528 * ___urlInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::DeactiveAllUrls()
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_DeactiveAllUrls_m3426642143 (DeviceFileInfoDAO_t152212114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::UpdateKeyValueVO(MagicTV.vo.KeyValueVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_UpdateKeyValueVO_m1919545244 (DeviceFileInfoDAO_t152212114 * __this, KeyValueVO_t1780100105 * ___metadata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::CommitResultRevisionVO(MagicTV.vo.ResultRevisionVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_CommitResultRevisionVO_m1111023138 (DeviceFileInfoDAO_t152212114 * __this, ResultRevisionVO_t2445597391 * ____obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::CommitBandleVO(MagicTV.vo.BundleVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_CommitBandleVO_m4132885346 (DeviceFileInfoDAO_t152212114 * __this, BundleVO_t1984518073 * ____obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::CommitFileVO(MagicTV.vo.FileVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_CommitFileVO_m50544922 (DeviceFileInfoDAO_t152212114 * __this, FileVO_t1935348659 * ____obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::CommitBundleMetadataVO(MagicTV.vo.MetadataVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_CommitBundleMetadataVO_m3859034514 (DeviceFileInfoDAO_t152212114 * __this, MetadataVO_t2511256998 * ____obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::CommitAppMetadataVO(MagicTV.vo.MetadataVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_CommitAppMetadataVO_m26669793 (DeviceFileInfoDAO_t152212114 * __this, MetadataVO_t2511256998 * ____obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::CommitUrlInfoVO(MagicTV.vo.UrlInfoVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_CommitUrlInfoVO_m2115789984 (DeviceFileInfoDAO_t152212114 * __this, UrlInfoVO_t1761987528 * ____obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::CommiteKeyValueVO(MagicTV.vo.KeyValueVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_CommiteKeyValueVO_m3731505911 (DeviceFileInfoDAO_t152212114 * __this, KeyValueVO_t1780100105 * ____obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceFileInfoDAO::UpdateAllUrlsToDelete()
extern "C"  void DeviceFileInfoDAO_UpdateAllUrlsToDelete_m3247802617 (DeviceFileInfoDAO_t152212114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReturnDataVO DeviceFileInfoDAO::Commit(MagicTV.vo.ResultRevisionVO)
extern "C"  ReturnDataVO_t544837971 * DeviceFileInfoDAO_Commit_m3866017395 (DeviceFileInfoDAO_t152212114 * __this, ResultRevisionVO_t2445597391 * ____revisionResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DeviceFileInfoDAO::<Commit>m__18(MagicTV.vo.BundleVO)
extern "C"  String_t* DeviceFileInfoDAO_U3CCommitU3Em__18_m2938347038 (Il2CppObject * __this /* static, unused */, BundleVO_t1984518073 * ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DeviceFileInfoDAO::<Commit>m__19(MagicTV.vo.MetadataVO)
extern "C"  String_t* DeviceFileInfoDAO_U3CCommitU3Em__19_m664023856 (Il2CppObject * __this /* static, unused */, MetadataVO_t2511256998 * ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.String> DeviceFileInfoDAO::<Commit>m__1A(MagicTV.vo.BundleVO)
extern "C"  Il2CppObject* DeviceFileInfoDAO_U3CCommitU3Em__1A_m620582652 (Il2CppObject * __this /* static, unused */, BundleVO_t1984518073 * ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.String> DeviceFileInfoDAO::<Commit>m__1B(MagicTV.vo.BundleVO)
extern "C"  Il2CppObject* DeviceFileInfoDAO_U3CCommitU3Em__1B_m2877493659 (Il2CppObject * __this /* static, unused */, BundleVO_t1984518073 * ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.String> DeviceFileInfoDAO::<Commit>m__1C(MagicTV.vo.BundleVO)
extern "C"  Il2CppObject* DeviceFileInfoDAO_U3CCommitU3Em__1C_m839437370 (Il2CppObject * __this /* static, unused */, BundleVO_t1984518073 * ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DeviceFileInfoDAO::<Commit>m__1D(MagicTV.vo.FileVO)
extern "C"  String_t* DeviceFileInfoDAO_U3CCommitU3Em__1D_m4120780632 (Il2CppObject * __this /* static, unused */, FileVO_t1935348659 * ___f0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.String> DeviceFileInfoDAO::<Commit>m__1E(MagicTV.vo.FileVO)
extern "C"  Il2CppObject* DeviceFileInfoDAO_U3CCommitU3Em__1E_m4171089086 (Il2CppObject * __this /* static, unused */, FileVO_t1935348659 * ___f0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DeviceFileInfoDAO::<Commit>m__1F(MagicTV.vo.MetadataVO)
extern "C"  String_t* DeviceFileInfoDAO_U3CCommitU3Em__1F_m4087903363 (Il2CppObject * __this /* static, unused */, MetadataVO_t2511256998 * ___m0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DeviceFileInfoDAO::<Commit>m__20(MagicTV.vo.UrlInfoVO)
extern "C"  String_t* DeviceFileInfoDAO_U3CCommitU3Em__20_m3823349306 (Il2CppObject * __this /* static, unused */, UrlInfoVO_t1761987528 * ___u0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
