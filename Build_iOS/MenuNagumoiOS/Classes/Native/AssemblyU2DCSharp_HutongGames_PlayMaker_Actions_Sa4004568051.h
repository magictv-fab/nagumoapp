﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmAnimationCurve
struct FsmAnimationCurve_t2685995989;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SampleCurve
struct  SampleCurve_t4004568051  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.SampleCurve::curve
	FsmAnimationCurve_t2685995989 * ___curve_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SampleCurve::sampleAt
	FsmFloat_t2134102846 * ___sampleAt_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SampleCurve::storeValue
	FsmFloat_t2134102846 * ___storeValue_11;
	// System.Boolean HutongGames.PlayMaker.Actions.SampleCurve::everyFrame
	bool ___everyFrame_12;

public:
	inline static int32_t get_offset_of_curve_9() { return static_cast<int32_t>(offsetof(SampleCurve_t4004568051, ___curve_9)); }
	inline FsmAnimationCurve_t2685995989 * get_curve_9() const { return ___curve_9; }
	inline FsmAnimationCurve_t2685995989 ** get_address_of_curve_9() { return &___curve_9; }
	inline void set_curve_9(FsmAnimationCurve_t2685995989 * value)
	{
		___curve_9 = value;
		Il2CppCodeGenWriteBarrier(&___curve_9, value);
	}

	inline static int32_t get_offset_of_sampleAt_10() { return static_cast<int32_t>(offsetof(SampleCurve_t4004568051, ___sampleAt_10)); }
	inline FsmFloat_t2134102846 * get_sampleAt_10() const { return ___sampleAt_10; }
	inline FsmFloat_t2134102846 ** get_address_of_sampleAt_10() { return &___sampleAt_10; }
	inline void set_sampleAt_10(FsmFloat_t2134102846 * value)
	{
		___sampleAt_10 = value;
		Il2CppCodeGenWriteBarrier(&___sampleAt_10, value);
	}

	inline static int32_t get_offset_of_storeValue_11() { return static_cast<int32_t>(offsetof(SampleCurve_t4004568051, ___storeValue_11)); }
	inline FsmFloat_t2134102846 * get_storeValue_11() const { return ___storeValue_11; }
	inline FsmFloat_t2134102846 ** get_address_of_storeValue_11() { return &___storeValue_11; }
	inline void set_storeValue_11(FsmFloat_t2134102846 * value)
	{
		___storeValue_11 = value;
		Il2CppCodeGenWriteBarrier(&___storeValue_11, value);
	}

	inline static int32_t get_offset_of_everyFrame_12() { return static_cast<int32_t>(offsetof(SampleCurve_t4004568051, ___everyFrame_12)); }
	inline bool get_everyFrame_12() const { return ___everyFrame_12; }
	inline bool* get_address_of_everyFrame_12() { return &___everyFrame_12; }
	inline void set_everyFrame_12(bool value)
	{
		___everyFrame_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
