﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InApp.SimpleEvents.BussolaCameraBehavior
struct BussolaCameraBehavior_t1897102310;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void InApp.SimpleEvents.BussolaCameraBehavior::.ctor()
extern "C"  void BussolaCameraBehavior__ctor_m2677680612 (BussolaCameraBehavior_t1897102310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// InApp.SimpleEvents.BussolaCameraBehavior InApp.SimpleEvents.BussolaCameraBehavior::GetInstance()
extern "C"  BussolaCameraBehavior_t1897102310 * BussolaCameraBehavior_GetInstance_m651467207 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.SimpleEvents.BussolaCameraBehavior::Start()
extern "C"  void BussolaCameraBehavior_Start_m1624818404 (BussolaCameraBehavior_t1897102310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.SimpleEvents.BussolaCameraBehavior::configByString(System.String)
extern "C"  void BussolaCameraBehavior_configByString_m4024044824 (BussolaCameraBehavior_t1897102310 * __this, String_t* ___config0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.SimpleEvents.BussolaCameraBehavior::TurnOn()
extern "C"  void BussolaCameraBehavior_TurnOn_m1942043484 (BussolaCameraBehavior_t1897102310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.SimpleEvents.BussolaCameraBehavior::TurnOff()
extern "C"  void BussolaCameraBehavior_TurnOff_m73627124 (BussolaCameraBehavior_t1897102310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.SimpleEvents.BussolaCameraBehavior::Reset()
extern "C"  void BussolaCameraBehavior_Reset_m324113553 (BussolaCameraBehavior_t1897102310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
