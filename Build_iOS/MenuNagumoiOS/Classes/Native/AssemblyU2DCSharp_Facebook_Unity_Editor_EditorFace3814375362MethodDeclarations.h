﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.Editor.EditorFacebookMockDialog/OnComplete
struct OnComplete_t3814375362;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void Facebook.Unity.Editor.EditorFacebookMockDialog/OnComplete::.ctor(System.Object,System.IntPtr)
extern "C"  void OnComplete__ctor_m2148463257 (OnComplete_t3814375362 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebookMockDialog/OnComplete::Invoke(System.String)
extern "C"  void OnComplete_Invoke_m1534533039 (OnComplete_t3814375362 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Facebook.Unity.Editor.EditorFacebookMockDialog/OnComplete::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnComplete_BeginInvoke_m266695356 (OnComplete_t3814375362 * __this, String_t* ___result0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebookMockDialog/OnComplete::EndInvoke(System.IAsyncResult)
extern "C"  void OnComplete_EndInvoke_m3731724585 (OnComplete_t3814375362 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
