﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Core_Sca1070518092.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Core.DirectoryEventArgs
struct  DirectoryEventArgs_t2626056408  : public ScanEventArgs_t1070518092
{
public:
	// System.Boolean ICSharpCode.SharpZipLib.Core.DirectoryEventArgs::hasMatchingFiles_
	bool ___hasMatchingFiles__3;

public:
	inline static int32_t get_offset_of_hasMatchingFiles__3() { return static_cast<int32_t>(offsetof(DirectoryEventArgs_t2626056408, ___hasMatchingFiles__3)); }
	inline bool get_hasMatchingFiles__3() const { return ___hasMatchingFiles__3; }
	inline bool* get_address_of_hasMatchingFiles__3() { return &___hasMatchingFiles__3; }
	inline void set_hasMatchingFiles__3(bool value)
	{
		___hasMatchingFiles__3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
