﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MagicApplicationSettings
struct MagicApplicationSettings_t2410043078;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// LoadMainScene
struct LoadMainScene_t1678736365;
// System.Action
struct Action_t3771233898;
// System.Func`2<System.IO.FileInfo,System.Boolean>
struct Func_2_t3035545473;
// System.Func`2<System.String,System.Boolean>
struct Func_2_t3730860138;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicApplicationSettings
struct  MagicApplicationSettings_t2410043078  : public MonoBehaviour_t667441552
{
public:
	// System.String MagicApplicationSettings::GoogleAnalitcsID
	String_t* ___GoogleAnalitcsID_6;
	// System.String MagicApplicationSettings::urlToGetInfo
	String_t* ___urlToGetInfo_7;
	// System.String MagicApplicationSettings::urlToRegisterDevice
	String_t* ___urlToRegisterDevice_8;
	// System.String MagicApplicationSettings::urlToGetPing
	String_t* ___urlToGetPing_9;
	// System.String MagicApplicationSettings::app
	String_t* ___app_10;
	// System.Int32 MagicApplicationSettings::timeoutToleranceMiliseconds
	int32_t ___timeoutToleranceMiliseconds_11;
	// System.Boolean MagicApplicationSettings::AppReady
	bool ___AppReady_12;
	// LoadMainScene MagicApplicationSettings::LoadMainScene
	LoadMainScene_t1678736365 * ___LoadMainScene_13;
	// System.Action MagicApplicationSettings::onAppReady
	Action_t3771233898 * ___onAppReady_14;

public:
	inline static int32_t get_offset_of_GoogleAnalitcsID_6() { return static_cast<int32_t>(offsetof(MagicApplicationSettings_t2410043078, ___GoogleAnalitcsID_6)); }
	inline String_t* get_GoogleAnalitcsID_6() const { return ___GoogleAnalitcsID_6; }
	inline String_t** get_address_of_GoogleAnalitcsID_6() { return &___GoogleAnalitcsID_6; }
	inline void set_GoogleAnalitcsID_6(String_t* value)
	{
		___GoogleAnalitcsID_6 = value;
		Il2CppCodeGenWriteBarrier(&___GoogleAnalitcsID_6, value);
	}

	inline static int32_t get_offset_of_urlToGetInfo_7() { return static_cast<int32_t>(offsetof(MagicApplicationSettings_t2410043078, ___urlToGetInfo_7)); }
	inline String_t* get_urlToGetInfo_7() const { return ___urlToGetInfo_7; }
	inline String_t** get_address_of_urlToGetInfo_7() { return &___urlToGetInfo_7; }
	inline void set_urlToGetInfo_7(String_t* value)
	{
		___urlToGetInfo_7 = value;
		Il2CppCodeGenWriteBarrier(&___urlToGetInfo_7, value);
	}

	inline static int32_t get_offset_of_urlToRegisterDevice_8() { return static_cast<int32_t>(offsetof(MagicApplicationSettings_t2410043078, ___urlToRegisterDevice_8)); }
	inline String_t* get_urlToRegisterDevice_8() const { return ___urlToRegisterDevice_8; }
	inline String_t** get_address_of_urlToRegisterDevice_8() { return &___urlToRegisterDevice_8; }
	inline void set_urlToRegisterDevice_8(String_t* value)
	{
		___urlToRegisterDevice_8 = value;
		Il2CppCodeGenWriteBarrier(&___urlToRegisterDevice_8, value);
	}

	inline static int32_t get_offset_of_urlToGetPing_9() { return static_cast<int32_t>(offsetof(MagicApplicationSettings_t2410043078, ___urlToGetPing_9)); }
	inline String_t* get_urlToGetPing_9() const { return ___urlToGetPing_9; }
	inline String_t** get_address_of_urlToGetPing_9() { return &___urlToGetPing_9; }
	inline void set_urlToGetPing_9(String_t* value)
	{
		___urlToGetPing_9 = value;
		Il2CppCodeGenWriteBarrier(&___urlToGetPing_9, value);
	}

	inline static int32_t get_offset_of_app_10() { return static_cast<int32_t>(offsetof(MagicApplicationSettings_t2410043078, ___app_10)); }
	inline String_t* get_app_10() const { return ___app_10; }
	inline String_t** get_address_of_app_10() { return &___app_10; }
	inline void set_app_10(String_t* value)
	{
		___app_10 = value;
		Il2CppCodeGenWriteBarrier(&___app_10, value);
	}

	inline static int32_t get_offset_of_timeoutToleranceMiliseconds_11() { return static_cast<int32_t>(offsetof(MagicApplicationSettings_t2410043078, ___timeoutToleranceMiliseconds_11)); }
	inline int32_t get_timeoutToleranceMiliseconds_11() const { return ___timeoutToleranceMiliseconds_11; }
	inline int32_t* get_address_of_timeoutToleranceMiliseconds_11() { return &___timeoutToleranceMiliseconds_11; }
	inline void set_timeoutToleranceMiliseconds_11(int32_t value)
	{
		___timeoutToleranceMiliseconds_11 = value;
	}

	inline static int32_t get_offset_of_AppReady_12() { return static_cast<int32_t>(offsetof(MagicApplicationSettings_t2410043078, ___AppReady_12)); }
	inline bool get_AppReady_12() const { return ___AppReady_12; }
	inline bool* get_address_of_AppReady_12() { return &___AppReady_12; }
	inline void set_AppReady_12(bool value)
	{
		___AppReady_12 = value;
	}

	inline static int32_t get_offset_of_LoadMainScene_13() { return static_cast<int32_t>(offsetof(MagicApplicationSettings_t2410043078, ___LoadMainScene_13)); }
	inline LoadMainScene_t1678736365 * get_LoadMainScene_13() const { return ___LoadMainScene_13; }
	inline LoadMainScene_t1678736365 ** get_address_of_LoadMainScene_13() { return &___LoadMainScene_13; }
	inline void set_LoadMainScene_13(LoadMainScene_t1678736365 * value)
	{
		___LoadMainScene_13 = value;
		Il2CppCodeGenWriteBarrier(&___LoadMainScene_13, value);
	}

	inline static int32_t get_offset_of_onAppReady_14() { return static_cast<int32_t>(offsetof(MagicApplicationSettings_t2410043078, ___onAppReady_14)); }
	inline Action_t3771233898 * get_onAppReady_14() const { return ___onAppReady_14; }
	inline Action_t3771233898 ** get_address_of_onAppReady_14() { return &___onAppReady_14; }
	inline void set_onAppReady_14(Action_t3771233898 * value)
	{
		___onAppReady_14 = value;
		Il2CppCodeGenWriteBarrier(&___onAppReady_14, value);
	}
};

struct MagicApplicationSettings_t2410043078_StaticFields
{
public:
	// MagicApplicationSettings MagicApplicationSettings::Instance
	MagicApplicationSettings_t2410043078 * ___Instance_2;
	// System.String MagicApplicationSettings::SOFTWARE_VERSION_NAME
	String_t* ___SOFTWARE_VERSION_NAME_3;
	// System.String MagicApplicationSettings::AppVersionNumber
	String_t* ___AppVersionNumber_4;
	// System.Collections.Generic.List`1<System.String> MagicApplicationSettings::safeFolders
	List_1_t1375417109 * ___safeFolders_5;
	// System.Func`2<System.IO.FileInfo,System.Boolean> MagicApplicationSettings::<>f__am$cacheD
	Func_2_t3035545473 * ___U3CU3Ef__amU24cacheD_15;
	// System.Func`2<System.String,System.Boolean> MagicApplicationSettings::<>f__am$cacheE
	Func_2_t3730860138 * ___U3CU3Ef__amU24cacheE_16;

public:
	inline static int32_t get_offset_of_Instance_2() { return static_cast<int32_t>(offsetof(MagicApplicationSettings_t2410043078_StaticFields, ___Instance_2)); }
	inline MagicApplicationSettings_t2410043078 * get_Instance_2() const { return ___Instance_2; }
	inline MagicApplicationSettings_t2410043078 ** get_address_of_Instance_2() { return &___Instance_2; }
	inline void set_Instance_2(MagicApplicationSettings_t2410043078 * value)
	{
		___Instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_2, value);
	}

	inline static int32_t get_offset_of_SOFTWARE_VERSION_NAME_3() { return static_cast<int32_t>(offsetof(MagicApplicationSettings_t2410043078_StaticFields, ___SOFTWARE_VERSION_NAME_3)); }
	inline String_t* get_SOFTWARE_VERSION_NAME_3() const { return ___SOFTWARE_VERSION_NAME_3; }
	inline String_t** get_address_of_SOFTWARE_VERSION_NAME_3() { return &___SOFTWARE_VERSION_NAME_3; }
	inline void set_SOFTWARE_VERSION_NAME_3(String_t* value)
	{
		___SOFTWARE_VERSION_NAME_3 = value;
		Il2CppCodeGenWriteBarrier(&___SOFTWARE_VERSION_NAME_3, value);
	}

	inline static int32_t get_offset_of_AppVersionNumber_4() { return static_cast<int32_t>(offsetof(MagicApplicationSettings_t2410043078_StaticFields, ___AppVersionNumber_4)); }
	inline String_t* get_AppVersionNumber_4() const { return ___AppVersionNumber_4; }
	inline String_t** get_address_of_AppVersionNumber_4() { return &___AppVersionNumber_4; }
	inline void set_AppVersionNumber_4(String_t* value)
	{
		___AppVersionNumber_4 = value;
		Il2CppCodeGenWriteBarrier(&___AppVersionNumber_4, value);
	}

	inline static int32_t get_offset_of_safeFolders_5() { return static_cast<int32_t>(offsetof(MagicApplicationSettings_t2410043078_StaticFields, ___safeFolders_5)); }
	inline List_1_t1375417109 * get_safeFolders_5() const { return ___safeFolders_5; }
	inline List_1_t1375417109 ** get_address_of_safeFolders_5() { return &___safeFolders_5; }
	inline void set_safeFolders_5(List_1_t1375417109 * value)
	{
		___safeFolders_5 = value;
		Il2CppCodeGenWriteBarrier(&___safeFolders_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheD_15() { return static_cast<int32_t>(offsetof(MagicApplicationSettings_t2410043078_StaticFields, ___U3CU3Ef__amU24cacheD_15)); }
	inline Func_2_t3035545473 * get_U3CU3Ef__amU24cacheD_15() const { return ___U3CU3Ef__amU24cacheD_15; }
	inline Func_2_t3035545473 ** get_address_of_U3CU3Ef__amU24cacheD_15() { return &___U3CU3Ef__amU24cacheD_15; }
	inline void set_U3CU3Ef__amU24cacheD_15(Func_2_t3035545473 * value)
	{
		___U3CU3Ef__amU24cacheD_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheD_15, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheE_16() { return static_cast<int32_t>(offsetof(MagicApplicationSettings_t2410043078_StaticFields, ___U3CU3Ef__amU24cacheE_16)); }
	inline Func_2_t3730860138 * get_U3CU3Ef__amU24cacheE_16() const { return ___U3CU3Ef__amU24cacheE_16; }
	inline Func_2_t3730860138 ** get_address_of_U3CU3Ef__amU24cacheE_16() { return &___U3CU3Ef__amU24cacheE_16; }
	inline void set_U3CU3Ef__amU24cacheE_16(Func_2_t3730860138 * value)
	{
		___U3CU3Ef__amU24cacheE_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheE_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
