﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// UnityEngine.WWW
struct WWW_t3134621005;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FacebookMain/<ISendJsonEnviaSeCompartilhou>c__Iterator4F
struct  U3CISendJsonEnviaSeCompartilhouU3Ec__Iterator4F_t867600582  : public Il2CppObject
{
public:
	// System.String FacebookMain/<ISendJsonEnviaSeCompartilhou>c__Iterator4F::<json>__0
	String_t* ___U3CjsonU3E__0_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> FacebookMain/<ISendJsonEnviaSeCompartilhou>c__Iterator4F::<headers>__1
	Dictionary_2_t827649927 * ___U3CheadersU3E__1_1;
	// System.Byte[] FacebookMain/<ISendJsonEnviaSeCompartilhou>c__Iterator4F::<pData>__2
	ByteU5BU5D_t4260760469* ___U3CpDataU3E__2_2;
	// UnityEngine.WWW FacebookMain/<ISendJsonEnviaSeCompartilhou>c__Iterator4F::<www>__3
	WWW_t3134621005 * ___U3CwwwU3E__3_3;
	// System.Int32 FacebookMain/<ISendJsonEnviaSeCompartilhou>c__Iterator4F::$PC
	int32_t ___U24PC_4;
	// System.Object FacebookMain/<ISendJsonEnviaSeCompartilhou>c__Iterator4F::$current
	Il2CppObject * ___U24current_5;

public:
	inline static int32_t get_offset_of_U3CjsonU3E__0_0() { return static_cast<int32_t>(offsetof(U3CISendJsonEnviaSeCompartilhouU3Ec__Iterator4F_t867600582, ___U3CjsonU3E__0_0)); }
	inline String_t* get_U3CjsonU3E__0_0() const { return ___U3CjsonU3E__0_0; }
	inline String_t** get_address_of_U3CjsonU3E__0_0() { return &___U3CjsonU3E__0_0; }
	inline void set_U3CjsonU3E__0_0(String_t* value)
	{
		___U3CjsonU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjsonU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CheadersU3E__1_1() { return static_cast<int32_t>(offsetof(U3CISendJsonEnviaSeCompartilhouU3Ec__Iterator4F_t867600582, ___U3CheadersU3E__1_1)); }
	inline Dictionary_2_t827649927 * get_U3CheadersU3E__1_1() const { return ___U3CheadersU3E__1_1; }
	inline Dictionary_2_t827649927 ** get_address_of_U3CheadersU3E__1_1() { return &___U3CheadersU3E__1_1; }
	inline void set_U3CheadersU3E__1_1(Dictionary_2_t827649927 * value)
	{
		___U3CheadersU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CheadersU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CpDataU3E__2_2() { return static_cast<int32_t>(offsetof(U3CISendJsonEnviaSeCompartilhouU3Ec__Iterator4F_t867600582, ___U3CpDataU3E__2_2)); }
	inline ByteU5BU5D_t4260760469* get_U3CpDataU3E__2_2() const { return ___U3CpDataU3E__2_2; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CpDataU3E__2_2() { return &___U3CpDataU3E__2_2; }
	inline void set_U3CpDataU3E__2_2(ByteU5BU5D_t4260760469* value)
	{
		___U3CpDataU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpDataU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__3_3() { return static_cast<int32_t>(offsetof(U3CISendJsonEnviaSeCompartilhouU3Ec__Iterator4F_t867600582, ___U3CwwwU3E__3_3)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__3_3() const { return ___U3CwwwU3E__3_3; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__3_3() { return &___U3CwwwU3E__3_3; }
	inline void set_U3CwwwU3E__3_3(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CISendJsonEnviaSeCompartilhouU3Ec__Iterator4F_t867600582, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CISendJsonEnviaSeCompartilhouU3Ec__Iterator4F_t867600582, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
