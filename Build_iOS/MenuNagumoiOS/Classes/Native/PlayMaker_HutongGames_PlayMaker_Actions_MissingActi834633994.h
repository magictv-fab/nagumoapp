﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.MissingAction
struct  MissingAction_t834633994  : public FsmStateAction_t2366529033
{
public:
	// System.String HutongGames.PlayMaker.Actions.MissingAction::actionName
	String_t* ___actionName_9;

public:
	inline static int32_t get_offset_of_actionName_9() { return static_cast<int32_t>(offsetof(MissingAction_t834633994, ___actionName_9)); }
	inline String_t* get_actionName_9() const { return ___actionName_9; }
	inline String_t** get_address_of_actionName_9() { return &___actionName_9; }
	inline void set_actionName_9(String_t* value)
	{
		___actionName_9 = value;
		Il2CppCodeGenWriteBarrier(&___actionName_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
