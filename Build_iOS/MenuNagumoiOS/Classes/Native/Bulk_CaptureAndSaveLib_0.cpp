﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// CaptureAndSave
struct CaptureAndSave_t700313070;
// System.String
struct String_t;
// UnityEngine.Camera
struct Camera_t2727095145;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// CaptureAndSave/<GetScreenPixels>c__Iterator1
struct U3CGetScreenPixelsU3Ec__Iterator1_t3827175978;
// System.Object
struct Il2CppObject;
// CaptureAndSave/<SaveToAlbum>c__Iterator0
struct U3CSaveToAlbumU3Ec__Iterator0_t371193057;
// CaptureAndSaveEventListener
struct CaptureAndSaveEventListener_t2941400608;
// CaptureAndSaveEventListener/OnError
struct OnError_t1516540506;
// CaptureAndSaveEventListener/OnSuccess
struct OnSuccess_t1013330901;
// CaptureAndSaveEventListener/OnScreenShot
struct OnScreenShot_t3427048564;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "CaptureAndSaveLib_U3CModuleU3E86524790.h"
#include "CaptureAndSaveLib_U3CModuleU3E86524790MethodDeclarations.h"
#include "CaptureAndSaveLib_CaptureAndSave700313070.h"
#include "CaptureAndSaveLib_CaptureAndSave700313070MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "CaptureAndSaveLib_ImageType1125820181.h"
#include "mscorlib_System_Int321153838500.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "mscorlib_System_Boolean476798718.h"
#include "CaptureAndSaveLib_WatermarkAlignment1574127103.h"
#include "CaptureAndSaveLib_CaptureAndSaveEventListener_OnEr1516540506MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen3187157168MethodDeclarations.h"
#include "CaptureAndSaveLib_CaptureAndSaveEventListener2941400608.h"
#include "CaptureAndSaveLib_CaptureAndSaveEventListener2941400608MethodDeclarations.h"
#include "CaptureAndSaveLib_CaptureAndSaveEventListener_OnEr1516540506.h"
#include "UnityEngine_UnityEngine_Coroutine3621161934.h"
#include "UnityEngine_UnityEngine_Object3071478659MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195MethodDeclarations.h"
#include "mscorlib_System_IO_File667612524MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources2918352667MethodDeclarations.h"
#include "mscorlib_System_GC1614687344MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application2856536070MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3501516275MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005MethodDeclarations.h"
#include "CaptureAndSaveLib_CaptureAndSaveEventListener_OnSu1013330901MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Byte2862609660.h"
#include "mscorlib_System_Exception3991598821.h"
#include "UnityEngine_UnityEngine_AsyncOperation3699081103.h"
#include "UnityEngine_UnityEngine_RuntimePlatform3050318497.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_SendMessageOptions3856946179.h"
#include "CaptureAndSaveLib_CaptureAndSaveEventListener_OnSu1013330901.h"
#include "mscorlib_System_Exception3991598821MethodDeclarations.h"
#include "CaptureAndSaveLib_CaptureAndSave_U3CSaveToAlbumU3Ec371193057MethodDeclarations.h"
#include "CaptureAndSaveLib_CaptureAndSave_U3CSaveToAlbumU3Ec371193057.h"
#include "mscorlib_System_DateTime4283661327MethodDeclarations.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_Int641153838595.h"
#include "mscorlib_System_Environment4152990825MethodDeclarations.h"
#include "mscorlib_System_IO_Path667902997MethodDeclarations.h"
#include "mscorlib_System_IO_Directory1148685675MethodDeclarations.h"
#include "mscorlib_System_Environment_SpecialFolder124599404.h"
#include "mscorlib_System_IO_DirectoryInfo89154617.h"
#include "UnityEngine_UnityEngine_TextureFormat4189619560.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Rect4241904616MethodDeclarations.h"
#include "mscorlib_System_Single4291918972.h"
#include "CaptureAndSaveLib_CaptureAndSave_U3CGetScreenPixel3827175978MethodDeclarations.h"
#include "CaptureAndSaveLib_CaptureAndSave_U3CGetScreenPixel3827175978.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera2727095145MethodDeclarations.h"
#include "CaptureAndSaveLib_CaptureAndSaveEventListener_OnSc3427048564MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"
#include "CaptureAndSaveLib_CaptureAndSaveEventListener_OnSc3427048564.h"
#include "CallNative_CallNative3421361205MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2526458961MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "UnityEngine_UnityEngine_Color4194546905MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame2372756133MethodDeclarations.h"
#include "mscorlib_System_UInt3224667981.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame2372756133.h"
#include "mscorlib_System_NotSupportedException1732551818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1732551818.h"
#include "mscorlib_System_Delegate3310234105MethodDeclarations.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"
#include "CaptureAndSaveLib_ImageType1125820181MethodDeclarations.h"
#include "CaptureAndSaveLib_WatermarkAlignment1574127103MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CaptureAndSave::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1577017734;
extern Il2CppCodeGenString* _stringLiteral3629492053;
extern const uint32_t CaptureAndSave__ctor_m2839589910_MetadataUsageId;
extern "C"  void CaptureAndSave__ctor_m2839589910 (CaptureAndSave_t700313070 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSave__ctor_m2839589910_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_FILENAME_PREFIX_2(_stringLiteral1577017734);
		__this->set_ALBUM_NAME_3(_stringLiteral3629492053);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_androidImagePath_4(L_0);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_absAlbumPath_5(L_1);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CaptureAndSave::SetAlbumPath(System.String)
extern "C"  void CaptureAndSave_SetAlbumPath_m1200194850 (CaptureAndSave_t700313070 * __this, String_t* ___albumPath0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___albumPath0;
		__this->set_absAlbumPath_5(L_0);
		return;
	}
}
// System.Void CaptureAndSave::CaptureAndSaveToAlbum(System.Int32,System.Int32,UnityEngine.Camera,ImageType)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t CaptureAndSave_CaptureAndSaveToAlbum_m1006194421_MetadataUsageId;
extern "C"  void CaptureAndSave_CaptureAndSaveToAlbum_m1006194421 (CaptureAndSave_t700313070 * __this, int32_t ___targetWidth0, int32_t ___targetHeight1, Camera_t2727095145 * ___camera2, int32_t ___imgType3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSave_CaptureAndSaveToAlbum_m1006194421_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___targetWidth0;
		int32_t L_1 = ___targetHeight1;
		Camera_t2727095145 * L_2 = ___camera2;
		int32_t L_3 = ___imgType3;
		Texture2D_t3884108195 * L_4 = CaptureAndSave_GetScreenShot_m2602002446(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		int32_t L_6 = ___imgType3;
		CaptureAndSave_SaveTexture_m1816178388(__this, L_4, L_5, L_6, (bool)1, (bool)1, (Texture2D_t3884108195 *)NULL, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CaptureAndSave::CaptureAndSaveToAlbum(System.Int32,System.Int32,UnityEngine.Camera,ImageType,UnityEngine.Texture2D,WatermarkAlignment)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t CaptureAndSave_CaptureAndSaveToAlbum_m1564711100_MetadataUsageId;
extern "C"  void CaptureAndSave_CaptureAndSaveToAlbum_m1564711100 (CaptureAndSave_t700313070 * __this, int32_t ___targetWidth0, int32_t ___targetHeight1, Camera_t2727095145 * ___camera2, int32_t ___imgType3, Texture2D_t3884108195 * ___watermark4, int32_t ___align5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSave_CaptureAndSaveToAlbum_m1564711100_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___targetWidth0;
		int32_t L_1 = ___targetHeight1;
		Camera_t2727095145 * L_2 = ___camera2;
		int32_t L_3 = ___imgType3;
		Texture2D_t3884108195 * L_4 = CaptureAndSave_GetScreenShot_m2602002446(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		int32_t L_6 = ___imgType3;
		Texture2D_t3884108195 * L_7 = ___watermark4;
		int32_t L_8 = ___align5;
		CaptureAndSave_SaveTexture_m1816178388(__this, L_4, L_5, L_6, (bool)1, (bool)1, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CaptureAndSave::CaptureAndSaveAtPath(System.Int32,System.Int32,UnityEngine.Camera,System.String,ImageType)
extern "C"  void CaptureAndSave_CaptureAndSaveAtPath_m2689293131 (CaptureAndSave_t700313070 * __this, int32_t ___targetWidth0, int32_t ___targetHeight1, Camera_t2727095145 * ___camera2, String_t* ___path3, int32_t ___imgType4, const MethodInfo* method)
{
	{
		int32_t L_0 = ___targetWidth0;
		int32_t L_1 = ___targetHeight1;
		Camera_t2727095145 * L_2 = ___camera2;
		int32_t L_3 = ___imgType4;
		Texture2D_t3884108195 * L_4 = CaptureAndSave_GetScreenShot_m2602002446(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		String_t* L_5 = ___path3;
		int32_t L_6 = ___imgType4;
		CaptureAndSave_SaveTexture_m1816178388(__this, L_4, L_5, L_6, (bool)1, (bool)0, (Texture2D_t3884108195 *)NULL, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CaptureAndSave::CaptureAndSaveAtPath(System.Int32,System.Int32,UnityEngine.Camera,System.String,ImageType,UnityEngine.Texture2D,WatermarkAlignment)
extern "C"  void CaptureAndSave_CaptureAndSaveAtPath_m3081692454 (CaptureAndSave_t700313070 * __this, int32_t ___targetWidth0, int32_t ___targetHeight1, Camera_t2727095145 * ___camera2, String_t* ___path3, int32_t ___imgType4, Texture2D_t3884108195 * ___watermark5, int32_t ___align6, const MethodInfo* method)
{
	{
		int32_t L_0 = ___targetWidth0;
		int32_t L_1 = ___targetHeight1;
		Camera_t2727095145 * L_2 = ___camera2;
		int32_t L_3 = ___imgType4;
		Texture2D_t3884108195 * L_4 = CaptureAndSave_GetScreenShot_m2602002446(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		String_t* L_5 = ___path3;
		int32_t L_6 = ___imgType4;
		Texture2D_t3884108195 * L_7 = ___watermark5;
		int32_t L_8 = ___align6;
		CaptureAndSave_SaveTexture_m1816178388(__this, L_4, L_5, L_6, (bool)1, (bool)0, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CaptureAndSave::CaptureAndSaveAtPath(System.String,ImageType)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2243241132;
extern const uint32_t CaptureAndSave_CaptureAndSaveAtPath_m3439077541_MetadataUsageId;
extern "C"  void CaptureAndSave_CaptureAndSaveAtPath_m3439077541 (CaptureAndSave_t700313070 * __this, String_t* ___path0, int32_t ___imgType1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSave_CaptureAndSaveAtPath_m3439077541_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___path0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		OnError_t1516540506 * L_2 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		OnError_t1516540506 * L_3 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		NullCheck(L_3);
		OnError_Invoke_m3553004494(L_3, _stringLiteral2243241132, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}

IL_0025:
	{
		int32_t L_4 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_6 = ___path0;
		int32_t L_7 = ___imgType1;
		Il2CppObject * L_8 = CaptureAndSave_SaveToAlbum_m1026814172(__this, 0, 0, L_4, L_5, L_6, L_7, (bool)0, (Texture2D_t3884108195 *)NULL, 0, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CaptureAndSave::CaptureAndSaveAtPath(System.String,ImageType,UnityEngine.Texture2D,WatermarkAlignment)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2243241132;
extern const uint32_t CaptureAndSave_CaptureAndSaveAtPath_m907896076_MetadataUsageId;
extern "C"  void CaptureAndSave_CaptureAndSaveAtPath_m907896076 (CaptureAndSave_t700313070 * __this, String_t* ___path0, int32_t ___imgType1, Texture2D_t3884108195 * ___watermark2, int32_t ___align3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSave_CaptureAndSaveAtPath_m907896076_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___path0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		OnError_t1516540506 * L_2 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		OnError_t1516540506 * L_3 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		NullCheck(L_3);
		OnError_Invoke_m3553004494(L_3, _stringLiteral2243241132, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}

IL_0025:
	{
		int32_t L_4 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_6 = ___path0;
		int32_t L_7 = ___imgType1;
		Texture2D_t3884108195 * L_8 = ___watermark2;
		int32_t L_9 = ___align3;
		Il2CppObject * L_10 = CaptureAndSave_SaveToAlbum_m1026814172(__this, 0, 0, L_4, L_5, L_6, L_7, (bool)0, L_8, L_9, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CaptureAndSave::CaptureAndSaveAtPath(System.Int32,System.Int32,System.Int32,System.Int32,System.String,ImageType)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2243241132;
extern const uint32_t CaptureAndSave_CaptureAndSaveAtPath_m3109496421_MetadataUsageId;
extern "C"  void CaptureAndSave_CaptureAndSaveAtPath_m3109496421 (CaptureAndSave_t700313070 * __this, int32_t ___x0, int32_t ___y1, int32_t ___width2, int32_t ___height3, String_t* ___path4, int32_t ___imgType5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSave_CaptureAndSaveAtPath_m3109496421_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___path4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		OnError_t1516540506 * L_2 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		OnError_t1516540506 * L_3 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		NullCheck(L_3);
		OnError_Invoke_m3553004494(L_3, _stringLiteral2243241132, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}

IL_0026:
	{
		int32_t L_4 = ___x0;
		int32_t L_5 = ___y1;
		int32_t L_6 = ___width2;
		int32_t L_7 = ___height3;
		String_t* L_8 = ___path4;
		int32_t L_9 = ___imgType5;
		Il2CppObject * L_10 = CaptureAndSave_SaveToAlbum_m1026814172(__this, L_4, L_5, L_6, L_7, L_8, L_9, (bool)0, (Texture2D_t3884108195 *)NULL, 0, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CaptureAndSave::CaptureAndSaveAtPath(System.Int32,System.Int32,System.Int32,System.Int32,System.String,ImageType,UnityEngine.Texture2D,WatermarkAlignment)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2243241132;
extern const uint32_t CaptureAndSave_CaptureAndSaveAtPath_m141221708_MetadataUsageId;
extern "C"  void CaptureAndSave_CaptureAndSaveAtPath_m141221708 (CaptureAndSave_t700313070 * __this, int32_t ___x0, int32_t ___y1, int32_t ___width2, int32_t ___height3, String_t* ___path4, int32_t ___imgType5, Texture2D_t3884108195 * ___watermark6, int32_t ___align7, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSave_CaptureAndSaveAtPath_m141221708_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___path4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		OnError_t1516540506 * L_2 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		OnError_t1516540506 * L_3 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		NullCheck(L_3);
		OnError_Invoke_m3553004494(L_3, _stringLiteral2243241132, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}

IL_0026:
	{
		int32_t L_4 = ___x0;
		int32_t L_5 = ___y1;
		int32_t L_6 = ___width2;
		int32_t L_7 = ___height3;
		String_t* L_8 = ___path4;
		int32_t L_9 = ___imgType5;
		Texture2D_t3884108195 * L_10 = ___watermark6;
		int32_t L_11 = ___align7;
		Il2CppObject * L_12 = CaptureAndSave_SaveToAlbum_m1026814172(__this, L_4, L_5, L_6, L_7, L_8, L_9, (bool)0, L_10, L_11, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CaptureAndSave::SaveTextureAtPath(UnityEngine.Texture2D,System.String,ImageType)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2243241132;
extern const uint32_t CaptureAndSave_SaveTextureAtPath_m808158949_MetadataUsageId;
extern "C"  void CaptureAndSave_SaveTextureAtPath_m808158949 (CaptureAndSave_t700313070 * __this, Texture2D_t3884108195 * ___tex2D0, String_t* ___path1, int32_t ___imgType2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSave_SaveTextureAtPath_m808158949_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___path1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		OnError_t1516540506 * L_2 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		OnError_t1516540506 * L_3 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		NullCheck(L_3);
		OnError_Invoke_m3553004494(L_3, _stringLiteral2243241132, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}

IL_0025:
	{
		Texture2D_t3884108195 * L_4 = ___tex2D0;
		String_t* L_5 = ___path1;
		int32_t L_6 = ___imgType2;
		CaptureAndSave_SaveTexture_m1816178388(__this, L_4, L_5, L_6, (bool)0, (bool)0, (Texture2D_t3884108195 *)NULL, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CaptureAndSave::SaveTextureAtPath(UnityEngine.Texture2D,System.String,ImageType,UnityEngine.Texture2D,WatermarkAlignment)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2243241132;
extern const uint32_t CaptureAndSave_SaveTextureAtPath_m4137488076_MetadataUsageId;
extern "C"  void CaptureAndSave_SaveTextureAtPath_m4137488076 (CaptureAndSave_t700313070 * __this, Texture2D_t3884108195 * ___tex2D0, String_t* ___path1, int32_t ___imgType2, Texture2D_t3884108195 * ___watermark3, int32_t ___align4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSave_SaveTextureAtPath_m4137488076_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___path1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		OnError_t1516540506 * L_2 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		OnError_t1516540506 * L_3 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		NullCheck(L_3);
		OnError_Invoke_m3553004494(L_3, _stringLiteral2243241132, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}

IL_0025:
	{
		Texture2D_t3884108195 * L_4 = ___tex2D0;
		String_t* L_5 = ___path1;
		int32_t L_6 = ___imgType2;
		Texture2D_t3884108195 * L_7 = ___watermark3;
		int32_t L_8 = ___align4;
		CaptureAndSave_SaveTexture_m1816178388(__this, L_4, L_5, L_6, (bool)0, (bool)0, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CaptureAndSave::SaveTextureToGallery(UnityEngine.Texture2D,ImageType)
extern Il2CppClass* CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral918492790;
extern const uint32_t CaptureAndSave_SaveTextureToGallery_m1530839788_MetadataUsageId;
extern "C"  void CaptureAndSave_SaveTextureToGallery_m1530839788 (CaptureAndSave_t700313070 * __this, Texture2D_t3884108195 * ___tex2D0, int32_t ___imgType1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSave_SaveTextureToGallery_m1530839788_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture2D_t3884108195 * L_0 = ___tex2D0;
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		OnError_t1516540506 * L_2 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		OnError_t1516540506 * L_3 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		NullCheck(L_3);
		OnError_Invoke_m3553004494(L_3, _stringLiteral918492790, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}

IL_0026:
	{
		Texture2D_t3884108195 * L_4 = ___tex2D0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		int32_t L_6 = ___imgType1;
		CaptureAndSave_SaveTexture_m1816178388(__this, L_4, L_5, L_6, (bool)0, (bool)1, (Texture2D_t3884108195 *)NULL, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CaptureAndSave::SaveTextureToGallery(UnityEngine.Texture2D,ImageType,UnityEngine.Texture2D,WatermarkAlignment)
extern Il2CppClass* CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral918492790;
extern const uint32_t CaptureAndSave_SaveTextureToGallery_m153208485_MetadataUsageId;
extern "C"  void CaptureAndSave_SaveTextureToGallery_m153208485 (CaptureAndSave_t700313070 * __this, Texture2D_t3884108195 * ___tex2D0, int32_t ___imgType1, Texture2D_t3884108195 * ___watermark2, int32_t ___align3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSave_SaveTextureToGallery_m153208485_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture2D_t3884108195 * L_0 = ___tex2D0;
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		OnError_t1516540506 * L_2 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		OnError_t1516540506 * L_3 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		NullCheck(L_3);
		OnError_Invoke_m3553004494(L_3, _stringLiteral918492790, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}

IL_0026:
	{
		Texture2D_t3884108195 * L_4 = ___tex2D0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		int32_t L_6 = ___imgType1;
		Texture2D_t3884108195 * L_7 = ___watermark2;
		int32_t L_8 = ___align3;
		CaptureAndSave_SaveTexture_m1816178388(__this, L_4, L_5, L_6, (bool)0, (bool)1, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CaptureAndSave::CaptureAndSaveToAlbum(ImageType)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t CaptureAndSave_CaptureAndSaveToAlbum_m3265519171_MetadataUsageId;
extern "C"  void CaptureAndSave_CaptureAndSaveToAlbum_m3265519171 (CaptureAndSave_t700313070 * __this, int32_t ___imgType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSave_CaptureAndSaveToAlbum_m3265519171_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		int32_t L_3 = ___imgType0;
		Il2CppObject * L_4 = CaptureAndSave_SaveToAlbum_m1026814172(__this, 0, 0, L_0, L_1, L_2, L_3, (bool)1, (Texture2D_t3884108195 *)NULL, 0, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CaptureAndSave::CaptureAndSaveToAlbum(ImageType,UnityEngine.Texture2D,WatermarkAlignment)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t CaptureAndSave_CaptureAndSaveToAlbum_m2807830830_MetadataUsageId;
extern "C"  void CaptureAndSave_CaptureAndSaveToAlbum_m2807830830 (CaptureAndSave_t700313070 * __this, int32_t ___imgType0, Texture2D_t3884108195 * ___watermark1, int32_t ___align2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSave_CaptureAndSaveToAlbum_m2807830830_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		int32_t L_3 = ___imgType0;
		Texture2D_t3884108195 * L_4 = ___watermark1;
		int32_t L_5 = ___align2;
		Il2CppObject * L_6 = CaptureAndSave_SaveToAlbum_m1026814172(__this, 0, 0, L_0, L_1, L_2, L_3, (bool)1, L_4, L_5, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CaptureAndSave::CaptureAndSaveToAlbum(System.Int32,System.Int32,System.Int32,System.Int32,ImageType)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t CaptureAndSave_CaptureAndSaveToAlbum_m390648579_MetadataUsageId;
extern "C"  void CaptureAndSave_CaptureAndSaveToAlbum_m390648579 (CaptureAndSave_t700313070 * __this, int32_t ___x0, int32_t ___y1, int32_t ___width2, int32_t ___height3, int32_t ___imgType4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSave_CaptureAndSaveToAlbum_m390648579_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___x0;
		int32_t L_1 = ___y1;
		int32_t L_2 = ___width2;
		int32_t L_3 = ___height3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		int32_t L_5 = ___imgType4;
		Il2CppObject * L_6 = CaptureAndSave_SaveToAlbum_m1026814172(__this, L_0, L_1, L_2, L_3, L_4, L_5, (bool)1, (Texture2D_t3884108195 *)NULL, 0, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CaptureAndSave::CaptureAndSaveToAlbum(System.Int32,System.Int32,System.Int32,System.Int32,ImageType,UnityEngine.Texture2D,WatermarkAlignment)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t CaptureAndSave_CaptureAndSaveToAlbum_m3720445038_MetadataUsageId;
extern "C"  void CaptureAndSave_CaptureAndSaveToAlbum_m3720445038 (CaptureAndSave_t700313070 * __this, int32_t ___x0, int32_t ___y1, int32_t ___width2, int32_t ___height3, int32_t ___imgType4, Texture2D_t3884108195 * ___watermark5, int32_t ___align6, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSave_CaptureAndSaveToAlbum_m3720445038_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___x0;
		int32_t L_1 = ___y1;
		int32_t L_2 = ___width2;
		int32_t L_3 = ___height3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		int32_t L_5 = ___imgType4;
		Texture2D_t3884108195 * L_6 = ___watermark5;
		int32_t L_7 = ___align6;
		Il2CppObject * L_8 = CaptureAndSave_SaveToAlbum_m1026814172(__this, L_0, L_1, L_2, L_3, L_4, L_5, (bool)1, L_6, L_7, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CaptureAndSave::SaveTexture(UnityEngine.Texture2D,System.String,ImageType,System.Boolean,System.Boolean,UnityEngine.Texture2D,WatermarkAlignment)
extern Il2CppClass* CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1477602327;
extern const uint32_t CaptureAndSave_SaveTexture_m1816178388_MetadataUsageId;
extern "C"  void CaptureAndSave_SaveTexture_m1816178388 (CaptureAndSave_t700313070 * __this, Texture2D_t3884108195 * ___tex2D0, String_t* ___path1, int32_t ___imgType2, bool ___destroy3, bool ___saveToGallery4, Texture2D_t3884108195 * ___watermark5, int32_t ___align6, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSave_SaveTexture_m1816178388_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t4260760469* V_0 = NULL;
	Texture2D_t3884108195 * V_1 = NULL;
	String_t* V_2 = NULL;
	Exception_t3991598821 * V_3 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			V_1 = (Texture2D_t3884108195 *)NULL;
			Texture2D_t3884108195 * L_0 = ___watermark5;
			bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
			if (!L_1)
			{
				goto IL_003a;
			}
		}

IL_000f:
		{
			Texture2D_t3884108195 * L_2 = ___tex2D0;
			Texture2D_t3884108195 * L_3 = ___watermark5;
			int32_t L_4 = ___align6;
			Texture2D_t3884108195 * L_5 = CaptureAndSave_AddWatermark_m277847075(__this, L_2, L_3, L_4, /*hidden argument*/NULL);
			V_1 = L_5;
			int32_t L_6 = ___imgType2;
			if ((!(((uint32_t)L_6) == ((uint32_t)1))))
			{
				goto IL_002e;
			}
		}

IL_0022:
		{
			Texture2D_t3884108195 * L_7 = V_1;
			NullCheck(L_7);
			ByteU5BU5D_t4260760469* L_8 = Texture2D_EncodeToPNG_m2464495756(L_7, /*hidden argument*/NULL);
			V_0 = L_8;
			goto IL_0035;
		}

IL_002e:
		{
			Texture2D_t3884108195 * L_9 = V_1;
			NullCheck(L_9);
			ByteU5BU5D_t4260760469* L_10 = Texture2D_EncodeToJPG_m2459014212(L_9, /*hidden argument*/NULL);
			V_0 = L_10;
		}

IL_0035:
		{
			goto IL_0054;
		}

IL_003a:
		{
			int32_t L_11 = ___imgType2;
			if ((!(((uint32_t)L_11) == ((uint32_t)1))))
			{
				goto IL_004d;
			}
		}

IL_0041:
		{
			Texture2D_t3884108195 * L_12 = ___tex2D0;
			NullCheck(L_12);
			ByteU5BU5D_t4260760469* L_13 = Texture2D_EncodeToPNG_m2464495756(L_12, /*hidden argument*/NULL);
			V_0 = L_13;
			goto IL_0054;
		}

IL_004d:
		{
			Texture2D_t3884108195 * L_14 = ___tex2D0;
			NullCheck(L_14);
			ByteU5BU5D_t4260760469* L_15 = Texture2D_EncodeToJPG_m2459014212(L_14, /*hidden argument*/NULL);
			V_0 = L_15;
		}

IL_0054:
		{
			String_t* L_16 = ___path1;
			int32_t L_17 = ___imgType2;
			String_t* L_18 = CaptureAndSave_GetFullPath_m3173349982(__this, L_16, L_17, /*hidden argument*/NULL);
			V_2 = L_18;
			String_t* L_19 = V_2;
			ByteU5BU5D_t4260760469* L_20 = V_0;
			File_WriteAllBytes_m2419938065(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
			bool L_21 = ___destroy3;
			if (!L_21)
			{
				goto IL_0074;
			}
		}

IL_006b:
		{
			Texture2D_t3884108195 * L_22 = ___tex2D0;
			Object_Destroy_m176400816(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
			___tex2D0 = (Texture2D_t3884108195 *)NULL;
		}

IL_0074:
		{
			Texture2D_t3884108195 * L_23 = V_1;
			bool L_24 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
			if (!L_24)
			{
				goto IL_0085;
			}
		}

IL_007f:
		{
			Texture2D_t3884108195 * L_25 = V_1;
			Object_Destroy_m176400816(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		}

IL_0085:
		{
			V_1 = (Texture2D_t3884108195 *)NULL;
			V_0 = (ByteU5BU5D_t4260760469*)NULL;
			Resources_UnloadUnusedAssets_m3831138427(NULL /*static, unused*/, /*hidden argument*/NULL);
			GC_Collect_m1459080321(NULL /*static, unused*/, /*hidden argument*/NULL);
			bool L_26 = ___saveToGallery4;
			if (!L_26)
			{
				goto IL_00b2;
			}
		}

IL_009b:
		{
			int32_t L_27 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((!(((uint32_t)L_27) == ((uint32_t)8))))
			{
				goto IL_00b2;
			}
		}

IL_00a6:
		{
			String_t* L_28 = V_2;
			CaptureAndSave_MoveImageToCameraRoll_m2078478887(__this, L_28, /*hidden argument*/NULL);
			goto IL_0106;
		}

IL_00b2:
		{
			bool L_29 = ___saveToGallery4;
			if (!L_29)
			{
				goto IL_00f1;
			}
		}

IL_00b9:
		{
			int32_t L_30 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((!(((uint32_t)L_30) == ((uint32_t)((int32_t)11)))))
			{
				goto IL_00f1;
			}
		}

IL_00c5:
		{
			GameObject_t3674682005 * L_31 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
			String_t* L_32 = V_2;
			NullCheck(L_31);
			GameObject_SendMessage_m423373689(L_31, _stringLiteral1477602327, L_32, 1, /*hidden argument*/NULL);
			OnSuccess_t1013330901 * L_33 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onSuccessInvoker_1();
			if (!L_33)
			{
				goto IL_00ec;
			}
		}

IL_00e1:
		{
			OnSuccess_t1013330901 * L_34 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onSuccessInvoker_1();
			String_t* L_35 = V_2;
			NullCheck(L_34);
			OnSuccess_Invoke_m629490739(L_34, L_35, /*hidden argument*/NULL);
		}

IL_00ec:
		{
			goto IL_0106;
		}

IL_00f1:
		{
			OnSuccess_t1013330901 * L_36 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onSuccessInvoker_1();
			if (!L_36)
			{
				goto IL_0106;
			}
		}

IL_00fb:
		{
			OnSuccess_t1013330901 * L_37 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onSuccessInvoker_1();
			String_t* L_38 = V_2;
			NullCheck(L_37);
			OnSuccess_Invoke_m629490739(L_37, L_38, /*hidden argument*/NULL);
		}

IL_0106:
		{
			goto IL_012b;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_010b;
		throw e;
	}

CATCH_010b:
	{ // begin catch(System.Exception)
		{
			V_3 = ((Exception_t3991598821 *)__exception_local);
			OnError_t1516540506 * L_39 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
			if (!L_39)
			{
				goto IL_0126;
			}
		}

IL_0116:
		{
			OnError_t1516540506 * L_40 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
			Exception_t3991598821 * L_41 = V_3;
			NullCheck(L_41);
			String_t* L_42 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_41);
			NullCheck(L_40);
			OnError_Invoke_m3553004494(L_40, L_42, /*hidden argument*/NULL);
		}

IL_0126:
		{
			goto IL_012b;
		}
	} // end catch (depth: 1)

IL_012b:
	{
		return;
	}
}
// System.Collections.IEnumerator CaptureAndSave::SaveToAlbum(System.Int32,System.Int32,System.Int32,System.Int32,System.String,ImageType,System.Boolean,UnityEngine.Texture2D,WatermarkAlignment)
extern Il2CppClass* U3CSaveToAlbumU3Ec__Iterator0_t371193057_il2cpp_TypeInfo_var;
extern const uint32_t CaptureAndSave_SaveToAlbum_m1026814172_MetadataUsageId;
extern "C"  Il2CppObject * CaptureAndSave_SaveToAlbum_m1026814172 (CaptureAndSave_t700313070 * __this, int32_t ___x0, int32_t ___y1, int32_t ___width2, int32_t ___height3, String_t* ___path4, int32_t ___imgType5, bool ___saveToGallery6, Texture2D_t3884108195 * ___watermark7, int32_t ___align8, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSave_SaveToAlbum_m1026814172_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSaveToAlbumU3Ec__Iterator0_t371193057 * V_0 = NULL;
	{
		U3CSaveToAlbumU3Ec__Iterator0_t371193057 * L_0 = (U3CSaveToAlbumU3Ec__Iterator0_t371193057 *)il2cpp_codegen_object_new(U3CSaveToAlbumU3Ec__Iterator0_t371193057_il2cpp_TypeInfo_var);
		U3CSaveToAlbumU3Ec__Iterator0__ctor_m321072963(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSaveToAlbumU3Ec__Iterator0_t371193057 * L_1 = V_0;
		int32_t L_2 = ___x0;
		NullCheck(L_1);
		L_1->set_x_0(L_2);
		U3CSaveToAlbumU3Ec__Iterator0_t371193057 * L_3 = V_0;
		int32_t L_4 = ___y1;
		NullCheck(L_3);
		L_3->set_y_1(L_4);
		U3CSaveToAlbumU3Ec__Iterator0_t371193057 * L_5 = V_0;
		int32_t L_6 = ___width2;
		NullCheck(L_5);
		L_5->set_width_2(L_6);
		U3CSaveToAlbumU3Ec__Iterator0_t371193057 * L_7 = V_0;
		int32_t L_8 = ___height3;
		NullCheck(L_7);
		L_7->set_height_3(L_8);
		U3CSaveToAlbumU3Ec__Iterator0_t371193057 * L_9 = V_0;
		int32_t L_10 = ___imgType5;
		NullCheck(L_9);
		L_9->set_imgType_4(L_10);
		U3CSaveToAlbumU3Ec__Iterator0_t371193057 * L_11 = V_0;
		String_t* L_12 = ___path4;
		NullCheck(L_11);
		L_11->set_path_6(L_12);
		U3CSaveToAlbumU3Ec__Iterator0_t371193057 * L_13 = V_0;
		bool L_14 = ___saveToGallery6;
		NullCheck(L_13);
		L_13->set_saveToGallery_7(L_14);
		U3CSaveToAlbumU3Ec__Iterator0_t371193057 * L_15 = V_0;
		Texture2D_t3884108195 * L_16 = ___watermark7;
		NullCheck(L_15);
		L_15->set_watermark_8(L_16);
		U3CSaveToAlbumU3Ec__Iterator0_t371193057 * L_17 = V_0;
		int32_t L_18 = ___align8;
		NullCheck(L_17);
		L_17->set_align_9(L_18);
		U3CSaveToAlbumU3Ec__Iterator0_t371193057 * L_19 = V_0;
		int32_t L_20 = ___x0;
		NullCheck(L_19);
		L_19->set_U3CU24U3Ex_12(L_20);
		U3CSaveToAlbumU3Ec__Iterator0_t371193057 * L_21 = V_0;
		int32_t L_22 = ___y1;
		NullCheck(L_21);
		L_21->set_U3CU24U3Ey_13(L_22);
		U3CSaveToAlbumU3Ec__Iterator0_t371193057 * L_23 = V_0;
		int32_t L_24 = ___width2;
		NullCheck(L_23);
		L_23->set_U3CU24U3Ewidth_14(L_24);
		U3CSaveToAlbumU3Ec__Iterator0_t371193057 * L_25 = V_0;
		int32_t L_26 = ___height3;
		NullCheck(L_25);
		L_25->set_U3CU24U3Eheight_15(L_26);
		U3CSaveToAlbumU3Ec__Iterator0_t371193057 * L_27 = V_0;
		int32_t L_28 = ___imgType5;
		NullCheck(L_27);
		L_27->set_U3CU24U3EimgType_16(L_28);
		U3CSaveToAlbumU3Ec__Iterator0_t371193057 * L_29 = V_0;
		String_t* L_30 = ___path4;
		NullCheck(L_29);
		L_29->set_U3CU24U3Epath_17(L_30);
		U3CSaveToAlbumU3Ec__Iterator0_t371193057 * L_31 = V_0;
		bool L_32 = ___saveToGallery6;
		NullCheck(L_31);
		L_31->set_U3CU24U3EsaveToGallery_18(L_32);
		U3CSaveToAlbumU3Ec__Iterator0_t371193057 * L_33 = V_0;
		Texture2D_t3884108195 * L_34 = ___watermark7;
		NullCheck(L_33);
		L_33->set_U3CU24U3Ewatermark_19(L_34);
		U3CSaveToAlbumU3Ec__Iterator0_t371193057 * L_35 = V_0;
		int32_t L_36 = ___align8;
		NullCheck(L_35);
		L_35->set_U3CU24U3Ealign_20(L_36);
		U3CSaveToAlbumU3Ec__Iterator0_t371193057 * L_37 = V_0;
		NullCheck(L_37);
		L_37->set_U3CU3Ef__this_21(__this);
		U3CSaveToAlbumU3Ec__Iterator0_t371193057 * L_38 = V_0;
		return L_38;
	}
}
// System.String CaptureAndSave::GetFileName(ImageType)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t1153838595_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral95;
extern Il2CppCodeGenString* _stringLiteral1475827;
extern Il2CppCodeGenString* _stringLiteral1481531;
extern const uint32_t CaptureAndSave_GetFileName_m569272239_MetadataUsageId;
extern "C"  String_t* CaptureAndSave_GetFileName_m569272239 (CaptureAndSave_t700313070 * __this, int32_t ___imgType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSave_GetFileName_m569272239_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	DateTime_t4283661327  V_1;
	memset(&V_1, 0, sizeof(V_1));
	DateTime_t4283661327  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		String_t* L_1 = __this->get_FILENAME_PREFIX_2();
		bool L_2 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_3;
		goto IL_0032;
	}

IL_0021:
	{
		String_t* L_4 = __this->get_FILENAME_PREFIX_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m138640077(NULL /*static, unused*/, L_4, _stringLiteral95, /*hidden argument*/NULL);
		V_0 = L_5;
	}

IL_0032:
	{
		int32_t L_6 = ___imgType0;
		if (L_6)
		{
			goto IL_005b;
		}
	}
	{
		String_t* L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t4283661327_il2cpp_TypeInfo_var);
		DateTime_t4283661327  L_8 = DateTime_get_Now_m1812131422(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_8;
		int64_t L_9 = DateTime_ToFileTime_m3189666065((&V_1), /*hidden argument*/NULL);
		int64_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Int64_t1153838595_il2cpp_TypeInfo_var, &L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m2809334143(NULL /*static, unused*/, L_7, L_11, _stringLiteral1475827, /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0080;
	}

IL_005b:
	{
		int32_t L_13 = ___imgType0;
		if ((!(((uint32_t)L_13) == ((uint32_t)1))))
		{
			goto IL_0080;
		}
	}
	{
		String_t* L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t4283661327_il2cpp_TypeInfo_var);
		DateTime_t4283661327  L_15 = DateTime_get_Now_m1812131422(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_15;
		int64_t L_16 = DateTime_ToFileTime_m3189666065((&V_2), /*hidden argument*/NULL);
		int64_t L_17 = L_16;
		Il2CppObject * L_18 = Box(Int64_t1153838595_il2cpp_TypeInfo_var, &L_17);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m2809334143(NULL /*static, unused*/, L_14, L_18, _stringLiteral1481531, /*hidden argument*/NULL);
		V_0 = L_19;
	}

IL_0080:
	{
		String_t* L_20 = V_0;
		return L_20;
	}
}
// System.String CaptureAndSave::GetFullPath(System.String,ImageType)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Path_t667902997_il2cpp_TypeInfo_var;
extern const uint32_t CaptureAndSave_GetFullPath_m3173349982_MetadataUsageId;
extern "C"  String_t* CaptureAndSave_GetFullPath_m3173349982 (CaptureAndSave_t700313070 * __this, String_t* ___path0, int32_t ___imgType1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSave_GetFullPath_m3173349982_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	{
		String_t* L_0 = ___path0;
		V_0 = L_0;
		String_t* L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_012b;
		}
	}
	{
		int32_t L_3 = ___imgType1;
		String_t* L_4 = CaptureAndSave_GetFileName_m569272239(__this, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		String_t* L_5 = __this->get_absAlbumPath_5();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0102;
		}
	}
	{
		int32_t L_7 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_7) == ((int32_t)2)))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_8 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_8) == ((int32_t)1)))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_9 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_9) == ((int32_t)((int32_t)13))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_10 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_11 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_11) == ((uint32_t)7))))
		{
			goto IL_00ac;
		}
	}

IL_005c:
	{
		String_t* L_12 = __this->get_ALBUM_NAME_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_13 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		String_t* L_14 = Environment_GetFolderPath_m4247985398(NULL /*static, unused*/, ((int32_t)39), /*hidden argument*/NULL);
		String_t* L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t667902997_il2cpp_TypeInfo_var);
		String_t* L_16 = Path_Combine_m4122812896(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_00ac;
	}

IL_007f:
	{
		String_t* L_17 = Environment_GetFolderPath_m4247985398(NULL /*static, unused*/, ((int32_t)39), /*hidden argument*/NULL);
		String_t* L_18 = __this->get_ALBUM_NAME_3();
		IL2CPP_RUNTIME_CLASS_INIT(Path_t667902997_il2cpp_TypeInfo_var);
		String_t* L_19 = Path_Combine_m4122812896(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		V_2 = L_19;
		String_t* L_20 = V_2;
		bool L_21 = Directory_Exists_m4117375188(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		if (L_21)
		{
			goto IL_00a4;
		}
	}
	{
		String_t* L_22 = V_2;
		Directory_CreateDirectory_m677877474(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
	}

IL_00a4:
	{
		String_t* L_23 = V_2;
		String_t* L_24 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t667902997_il2cpp_TypeInfo_var);
		String_t* L_25 = Path_Combine_m4122812896(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		V_0 = L_25;
	}

IL_00ac:
	{
		int32_t L_26 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_26) == ((uint32_t)8))))
		{
			goto IL_00c3;
		}
	}
	{
		String_t* L_27 = Application_get_persistentDataPath_m2554537447(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_28 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t667902997_il2cpp_TypeInfo_var);
		String_t* L_29 = Path_Combine_m4122812896(NULL /*static, unused*/, L_27, L_28, /*hidden argument*/NULL);
		V_0 = L_29;
	}

IL_00c3:
	{
		int32_t L_30 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_30) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_00fd;
		}
	}
	{
		String_t* L_31 = __this->get_androidImagePath_4();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_32 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_00f0;
		}
	}
	{
		String_t* L_33 = Application_get_persistentDataPath_m2554537447(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_34 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t667902997_il2cpp_TypeInfo_var);
		String_t* L_35 = Path_Combine_m4122812896(NULL /*static, unused*/, L_33, L_34, /*hidden argument*/NULL);
		V_0 = L_35;
		goto IL_00fd;
	}

IL_00f0:
	{
		String_t* L_36 = __this->get_androidImagePath_4();
		String_t* L_37 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t667902997_il2cpp_TypeInfo_var);
		String_t* L_38 = Path_Combine_m4122812896(NULL /*static, unused*/, L_36, L_37, /*hidden argument*/NULL);
		V_0 = L_38;
	}

IL_00fd:
	{
		goto IL_012b;
	}

IL_0102:
	{
		String_t* L_39 = __this->get_absAlbumPath_5();
		bool L_40 = Directory_Exists_m4117375188(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		if (L_40)
		{
			goto IL_011e;
		}
	}
	{
		String_t* L_41 = __this->get_absAlbumPath_5();
		Directory_CreateDirectory_m677877474(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
	}

IL_011e:
	{
		String_t* L_42 = __this->get_absAlbumPath_5();
		String_t* L_43 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t667902997_il2cpp_TypeInfo_var);
		String_t* L_44 = Path_Combine_m4122812896(NULL /*static, unused*/, L_42, L_43, /*hidden argument*/NULL);
		V_0 = L_44;
	}

IL_012b:
	{
		String_t* L_45 = V_0;
		return L_45;
	}
}
// UnityEngine.Texture2D CaptureAndSave::GetPixels(System.Int32,System.Int32,System.Int32,System.Int32,ImageType)
extern Il2CppClass* Texture2D_t3884108195_il2cpp_TypeInfo_var;
extern const uint32_t CaptureAndSave_GetPixels_m1882752963_MetadataUsageId;
extern "C"  Texture2D_t3884108195 * CaptureAndSave_GetPixels_m1882752963 (CaptureAndSave_t700313070 * __this, int32_t ___x0, int32_t ___y1, int32_t ___width2, int32_t ___height3, int32_t ___imgType4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSave_GetPixels_m1882752963_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Texture2D_t3884108195 * V_1 = NULL;
	{
		int32_t L_0 = ___imgType4;
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_000f;
		}
	}
	{
		V_0 = 5;
		goto IL_0011;
	}

IL_000f:
	{
		V_0 = 3;
	}

IL_0011:
	{
		int32_t L_1 = ___width2;
		int32_t L_2 = ___height3;
		int32_t L_3 = V_0;
		Texture2D_t3884108195 * L_4 = (Texture2D_t3884108195 *)il2cpp_codegen_object_new(Texture2D_t3884108195_il2cpp_TypeInfo_var);
		Texture2D__ctor_m3705883154(L_4, L_1, L_2, L_3, (bool)0, /*hidden argument*/NULL);
		V_1 = L_4;
		Texture2D_t3884108195 * L_5 = V_1;
		int32_t L_6 = ___x0;
		int32_t L_7 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_8 = ___height3;
		int32_t L_9 = ___y1;
		int32_t L_10 = ___width2;
		int32_t L_11 = ___height3;
		Rect_t4241904616  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Rect__ctor_m3291325233(&L_12, (((float)((float)L_6))), (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_7-(int32_t)L_8))-(int32_t)L_9))))), (((float)((float)L_10))), (((float)((float)L_11))), /*hidden argument*/NULL);
		NullCheck(L_5);
		Texture2D_ReadPixels_m1334301696(L_5, L_12, 0, 0, /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_13 = V_1;
		NullCheck(L_13);
		Texture2D_Apply_m1364130776(L_13, /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_14 = V_1;
		return L_14;
	}
}
// System.Collections.IEnumerator CaptureAndSave::GetScreenPixels(System.Int32,System.Int32,System.Int32,System.Int32,ImageType,UnityEngine.Texture2D,WatermarkAlignment)
extern Il2CppClass* U3CGetScreenPixelsU3Ec__Iterator1_t3827175978_il2cpp_TypeInfo_var;
extern const uint32_t CaptureAndSave_GetScreenPixels_m3796987421_MetadataUsageId;
extern "C"  Il2CppObject * CaptureAndSave_GetScreenPixels_m3796987421 (CaptureAndSave_t700313070 * __this, int32_t ___x0, int32_t ___y1, int32_t ___width2, int32_t ___height3, int32_t ___imgType4, Texture2D_t3884108195 * ___watermark5, int32_t ___align6, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSave_GetScreenPixels_m3796987421_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetScreenPixelsU3Ec__Iterator1_t3827175978 * V_0 = NULL;
	{
		U3CGetScreenPixelsU3Ec__Iterator1_t3827175978 * L_0 = (U3CGetScreenPixelsU3Ec__Iterator1_t3827175978 *)il2cpp_codegen_object_new(U3CGetScreenPixelsU3Ec__Iterator1_t3827175978_il2cpp_TypeInfo_var);
		U3CGetScreenPixelsU3Ec__Iterator1__ctor_m3292122010(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetScreenPixelsU3Ec__Iterator1_t3827175978 * L_1 = V_0;
		int32_t L_2 = ___imgType4;
		NullCheck(L_1);
		L_1->set_imgType_0(L_2);
		U3CGetScreenPixelsU3Ec__Iterator1_t3827175978 * L_3 = V_0;
		int32_t L_4 = ___width2;
		NullCheck(L_3);
		L_3->set_width_2(L_4);
		U3CGetScreenPixelsU3Ec__Iterator1_t3827175978 * L_5 = V_0;
		int32_t L_6 = ___height3;
		NullCheck(L_5);
		L_5->set_height_3(L_6);
		U3CGetScreenPixelsU3Ec__Iterator1_t3827175978 * L_7 = V_0;
		int32_t L_8 = ___x0;
		NullCheck(L_7);
		L_7->set_x_4(L_8);
		U3CGetScreenPixelsU3Ec__Iterator1_t3827175978 * L_9 = V_0;
		int32_t L_10 = ___y1;
		NullCheck(L_9);
		L_9->set_y_5(L_10);
		U3CGetScreenPixelsU3Ec__Iterator1_t3827175978 * L_11 = V_0;
		Texture2D_t3884108195 * L_12 = ___watermark5;
		NullCheck(L_11);
		L_11->set_watermark_6(L_12);
		U3CGetScreenPixelsU3Ec__Iterator1_t3827175978 * L_13 = V_0;
		int32_t L_14 = ___align6;
		NullCheck(L_13);
		L_13->set_align_8(L_14);
		U3CGetScreenPixelsU3Ec__Iterator1_t3827175978 * L_15 = V_0;
		int32_t L_16 = ___imgType4;
		NullCheck(L_15);
		L_15->set_U3CU24U3EimgType_11(L_16);
		U3CGetScreenPixelsU3Ec__Iterator1_t3827175978 * L_17 = V_0;
		int32_t L_18 = ___width2;
		NullCheck(L_17);
		L_17->set_U3CU24U3Ewidth_12(L_18);
		U3CGetScreenPixelsU3Ec__Iterator1_t3827175978 * L_19 = V_0;
		int32_t L_20 = ___height3;
		NullCheck(L_19);
		L_19->set_U3CU24U3Eheight_13(L_20);
		U3CGetScreenPixelsU3Ec__Iterator1_t3827175978 * L_21 = V_0;
		int32_t L_22 = ___x0;
		NullCheck(L_21);
		L_21->set_U3CU24U3Ex_14(L_22);
		U3CGetScreenPixelsU3Ec__Iterator1_t3827175978 * L_23 = V_0;
		int32_t L_24 = ___y1;
		NullCheck(L_23);
		L_23->set_U3CU24U3Ey_15(L_24);
		U3CGetScreenPixelsU3Ec__Iterator1_t3827175978 * L_25 = V_0;
		Texture2D_t3884108195 * L_26 = ___watermark5;
		NullCheck(L_25);
		L_25->set_U3CU24U3Ewatermark_16(L_26);
		U3CGetScreenPixelsU3Ec__Iterator1_t3827175978 * L_27 = V_0;
		int32_t L_28 = ___align6;
		NullCheck(L_27);
		L_27->set_U3CU24U3Ealign_17(L_28);
		U3CGetScreenPixelsU3Ec__Iterator1_t3827175978 * L_29 = V_0;
		NullCheck(L_29);
		L_29->set_U3CU3Ef__this_18(__this);
		U3CGetScreenPixelsU3Ec__Iterator1_t3827175978 * L_30 = V_0;
		return L_30;
	}
}
// UnityEngine.Texture2D CaptureAndSave::GetScreenShot(System.Int32,System.Int32,UnityEngine.Camera,ImageType)
extern "C"  Texture2D_t3884108195 * CaptureAndSave_GetScreenShot_m2602002446 (CaptureAndSave_t700313070 * __this, int32_t ___targetWidth0, int32_t ___targetHeight1, Camera_t2727095145 * ___camera2, int32_t ___imgType3, const MethodInfo* method)
{
	{
		int32_t L_0 = ___targetWidth0;
		int32_t L_1 = ___targetHeight1;
		Camera_t2727095145 * L_2 = ___camera2;
		int32_t L_3 = ___imgType3;
		Texture2D_t3884108195 * L_4 = CaptureAndSave_GetScreenShotFromCamera_m3302824084(__this, L_0, L_1, L_2, L_3, (Texture2D_t3884108195 *)NULL, 0, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Texture2D CaptureAndSave::GetScreenShot(System.Int32,System.Int32,UnityEngine.Camera,ImageType,UnityEngine.Texture2D,WatermarkAlignment)
extern "C"  Texture2D_t3884108195 * CaptureAndSave_GetScreenShot_m3002585027 (CaptureAndSave_t700313070 * __this, int32_t ___targetWidth0, int32_t ___targetHeight1, Camera_t2727095145 * ___camera2, int32_t ___imgType3, Texture2D_t3884108195 * ___watermark4, int32_t ___align5, const MethodInfo* method)
{
	{
		int32_t L_0 = ___targetWidth0;
		int32_t L_1 = ___targetHeight1;
		Camera_t2727095145 * L_2 = ___camera2;
		int32_t L_3 = ___imgType3;
		Texture2D_t3884108195 * L_4 = ___watermark4;
		int32_t L_5 = ___align5;
		Texture2D_t3884108195 * L_6 = CaptureAndSave_GetScreenShotFromCamera_m3302824084(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Texture2D CaptureAndSave::GetScreenShotFromCamera(System.Int32,System.Int32,UnityEngine.Camera,ImageType,UnityEngine.Texture2D,WatermarkAlignment)
extern Il2CppClass* RenderTexture_t1963041563_il2cpp_TypeInfo_var;
extern Il2CppClass* Texture2D_t3884108195_il2cpp_TypeInfo_var;
extern Il2CppClass* CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var;
extern const uint32_t CaptureAndSave_GetScreenShotFromCamera_m3302824084_MetadataUsageId;
extern "C"  Texture2D_t3884108195 * CaptureAndSave_GetScreenShotFromCamera_m3302824084 (CaptureAndSave_t700313070 * __this, int32_t ___targetWidth0, int32_t ___targetHeight1, Camera_t2727095145 * ___camera2, int32_t ___imgType3, Texture2D_t3884108195 * ___watermark4, int32_t ___align5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSave_GetScreenShotFromCamera_m3302824084_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	RenderTexture_t1963041563 * V_2 = NULL;
	RenderTexture_t1963041563 * V_3 = NULL;
	RenderTexture_t1963041563 * V_4 = NULL;
	int32_t V_5 = 0;
	Texture2D_t3884108195 * V_6 = NULL;
	Texture2D_t3884108195 * V_7 = NULL;
	{
		int32_t L_0 = ___targetWidth0;
		V_0 = L_0;
		int32_t L_1 = ___targetHeight1;
		V_1 = L_1;
		int32_t L_2 = V_0;
		int32_t L_3 = V_1;
		RenderTexture_t1963041563 * L_4 = (RenderTexture_t1963041563 *)il2cpp_codegen_object_new(RenderTexture_t1963041563_il2cpp_TypeInfo_var);
		RenderTexture__ctor_m591418693(L_4, L_2, L_3, ((int32_t)24), /*hidden argument*/NULL);
		V_2 = L_4;
		RenderTexture_t1963041563 * L_5 = RenderTexture_get_active_m1725644858(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_5;
		Camera_t2727095145 * L_6 = ___camera2;
		NullCheck(L_6);
		RenderTexture_t1963041563 * L_7 = Camera_get_targetTexture_m1468336738(L_6, /*hidden argument*/NULL);
		V_4 = L_7;
		Camera_t2727095145 * L_8 = ___camera2;
		RenderTexture_t1963041563 * L_9 = V_2;
		NullCheck(L_8);
		Camera_set_targetTexture_m671169649(L_8, L_9, /*hidden argument*/NULL);
		int32_t L_10 = ___imgType3;
		if ((!(((uint32_t)L_10) == ((uint32_t)1))))
		{
			goto IL_0033;
		}
	}
	{
		V_5 = 5;
		goto IL_0036;
	}

IL_0033:
	{
		V_5 = 3;
	}

IL_0036:
	{
		int32_t L_11 = V_0;
		int32_t L_12 = V_1;
		int32_t L_13 = V_5;
		Texture2D_t3884108195 * L_14 = (Texture2D_t3884108195 *)il2cpp_codegen_object_new(Texture2D_t3884108195_il2cpp_TypeInfo_var);
		Texture2D__ctor_m3705883154(L_14, L_11, L_12, L_13, (bool)0, /*hidden argument*/NULL);
		V_6 = L_14;
		Camera_t2727095145 * L_15 = ___camera2;
		NullCheck(L_15);
		Camera_Render_m945002290(L_15, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_16 = V_2;
		RenderTexture_set_active_m1002947377(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_17 = V_6;
		int32_t L_18 = V_0;
		int32_t L_19 = V_1;
		Rect_t4241904616  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Rect__ctor_m3291325233(&L_20, (0.0f), (0.0f), (((float)((float)L_18))), (((float)((float)L_19))), /*hidden argument*/NULL);
		NullCheck(L_17);
		Texture2D_ReadPixels_m1334301696(L_17, L_20, 0, 0, /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_21 = V_6;
		NullCheck(L_21);
		Texture2D_Apply_m1364130776(L_21, /*hidden argument*/NULL);
		Camera_t2727095145 * L_22 = ___camera2;
		RenderTexture_t1963041563 * L_23 = V_4;
		NullCheck(L_22);
		Camera_set_targetTexture_m671169649(L_22, L_23, /*hidden argument*/NULL);
		RenderTexture_t1963041563 * L_24 = V_3;
		RenderTexture_set_active_m1002947377(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_25 = ___watermark4;
		bool L_26 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_25, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00a8;
		}
	}
	{
		Texture2D_t3884108195 * L_27 = V_6;
		V_7 = L_27;
		Texture2D_t3884108195 * L_28 = V_6;
		Texture2D_t3884108195 * L_29 = ___watermark4;
		int32_t L_30 = ___align5;
		Texture2D_t3884108195 * L_31 = CaptureAndSave_AddWatermark_m277847075(__this, L_28, L_29, L_30, /*hidden argument*/NULL);
		V_6 = L_31;
		Texture2D_t3884108195 * L_32 = V_7;
		Object_Destroy_m176400816(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		V_7 = (Texture2D_t3884108195 *)NULL;
	}

IL_00a8:
	{
		OnScreenShot_t3427048564 * L_33 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onScreenShotInvoker_2();
		if (!L_33)
		{
			goto IL_00be;
		}
	}
	{
		OnScreenShot_t3427048564 * L_34 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onScreenShotInvoker_2();
		Texture2D_t3884108195 * L_35 = V_6;
		NullCheck(L_34);
		OnScreenShot_Invoke_m3599849904(L_34, L_35, /*hidden argument*/NULL);
	}

IL_00be:
	{
		Texture2D_t3884108195 * L_36 = V_6;
		return L_36;
	}
}
// System.Void CaptureAndSave::GetScreenShot(System.Int32,System.Int32,System.Int32,System.Int32,ImageType)
extern "C"  void CaptureAndSave_GetScreenShot_m2957471437 (CaptureAndSave_t700313070 * __this, int32_t ___x0, int32_t ___y1, int32_t ___width2, int32_t ___height3, int32_t ___imgType4, const MethodInfo* method)
{
	{
		int32_t L_0 = ___width2;
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_1 = ___height3;
		if (L_1)
		{
			goto IL_000e;
		}
	}

IL_000d:
	{
		return;
	}

IL_000e:
	{
		int32_t L_2 = ___x0;
		int32_t L_3 = ___y1;
		int32_t L_4 = ___width2;
		int32_t L_5 = ___height3;
		int32_t L_6 = ___imgType4;
		Il2CppObject * L_7 = CaptureAndSave_GetScreenPixels_m3796987421(__this, L_2, L_3, L_4, L_5, L_6, (Texture2D_t3884108195 *)NULL, 0, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CaptureAndSave::GetScreenShot(System.Int32,System.Int32,System.Int32,System.Int32,ImageType,UnityEngine.Texture2D,WatermarkAlignment)
extern "C"  void CaptureAndSave_GetScreenShot_m394865636 (CaptureAndSave_t700313070 * __this, int32_t ___x0, int32_t ___y1, int32_t ___width2, int32_t ___height3, int32_t ___imgType4, Texture2D_t3884108195 * ___watermark5, int32_t ___align6, const MethodInfo* method)
{
	{
		int32_t L_0 = ___width2;
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_1 = ___height3;
		if (L_1)
		{
			goto IL_000e;
		}
	}

IL_000d:
	{
		return;
	}

IL_000e:
	{
		int32_t L_2 = ___x0;
		int32_t L_3 = ___y1;
		int32_t L_4 = ___width2;
		int32_t L_5 = ___height3;
		int32_t L_6 = ___imgType4;
		Texture2D_t3884108195 * L_7 = ___watermark5;
		int32_t L_8 = ___align6;
		Il2CppObject * L_9 = CaptureAndSave_GetScreenPixels_m3796987421(__this, L_2, L_3, L_4, L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CaptureAndSave::GetFullScreenShot(ImageType)
extern "C"  void CaptureAndSave_GetFullScreenShot_m2596668478 (CaptureAndSave_t700313070 * __this, int32_t ___imgType0, const MethodInfo* method)
{
	{
		int32_t L_0 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = ___imgType0;
		Il2CppObject * L_3 = CaptureAndSave_GetScreenPixels_m3796987421(__this, 0, 0, L_0, L_1, L_2, (Texture2D_t3884108195 *)NULL, 0, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CaptureAndSave::GetFullScreenShot(ImageType,UnityEngine.Texture2D,WatermarkAlignment)
extern "C"  void CaptureAndSave_GetFullScreenShot_m3194136979 (CaptureAndSave_t700313070 * __this, int32_t ___imgType0, Texture2D_t3884108195 * ___watermark1, int32_t ___align2, const MethodInfo* method)
{
	{
		int32_t L_0 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = ___imgType0;
		Texture2D_t3884108195 * L_3 = ___watermark1;
		int32_t L_4 = ___align2;
		Il2CppObject * L_5 = CaptureAndSave_GetScreenPixels_m3796987421(__this, 0, 0, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CaptureAndSave::CopyImageToCameraRoll(System.String)
extern Il2CppClass* Path_t667902997_il2cpp_TypeInfo_var;
extern Il2CppClass* CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1477602327;
extern Il2CppCodeGenString* _stringLiteral2381718370;
extern Il2CppCodeGenString* _stringLiteral3275765239;
extern const uint32_t CaptureAndSave_CopyImageToCameraRoll_m1468462539_MetadataUsageId;
extern "C"  void CaptureAndSave_CopyImageToCameraRoll_m1468462539 (CaptureAndSave_t700313070 * __this, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSave_CopyImageToCameraRoll_m1468462539_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Exception_t3991598821 * V_1 = NULL;
	String_t* V_2 = NULL;
	Exception_t3991598821 * V_3 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = ___path0;
		String_t* L_2 = __this->get_ALBUM_NAME_3();
		CallNative_CopyImageToCameraRoll_m2433959120(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		goto IL_0179;
	}

IL_001c:
	{
		int32_t L_3 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_00a0;
		}
	}

IL_0028:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_4 = __this->get_androidImagePath_4();
			String_t* L_5 = ___path0;
			IL2CPP_RUNTIME_CLASS_INIT(Path_t667902997_il2cpp_TypeInfo_var);
			String_t* L_6 = Path_GetFileName_m26786182(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
			String_t* L_7 = Path_Combine_m4122812896(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
			V_0 = L_7;
			String_t* L_8 = ___path0;
			String_t* L_9 = V_0;
			File_Copy_m4182716978(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
			GameObject_t3674682005 * L_10 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
			String_t* L_11 = V_0;
			NullCheck(L_10);
			GameObject_SendMessage_m423373689(L_10, _stringLiteral1477602327, L_11, 1, /*hidden argument*/NULL);
			OnSuccess_t1013330901 * L_12 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onSuccessInvoker_1();
			if (!L_12)
			{
				goto IL_006c;
			}
		}

IL_005d:
		{
			OnSuccess_t1013330901 * L_13 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onSuccessInvoker_1();
			NullCheck(L_13);
			OnSuccess_Invoke_m629490739(L_13, _stringLiteral2381718370, /*hidden argument*/NULL);
		}

IL_006c:
		{
			goto IL_009b;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0071;
		throw e;
	}

CATCH_0071:
	{ // begin catch(System.Exception)
		{
			V_1 = ((Exception_t3991598821 *)__exception_local);
			OnError_t1516540506 * L_14 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
			if (!L_14)
			{
				goto IL_0096;
			}
		}

IL_007c:
		{
			OnError_t1516540506 * L_15 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
			Exception_t3991598821 * L_16 = V_1;
			NullCheck(L_16);
			String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_16);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_18 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3275765239, L_17, /*hidden argument*/NULL);
			NullCheck(L_15);
			OnError_Invoke_m3553004494(L_15, L_18, /*hidden argument*/NULL);
		}

IL_0096:
		{
			goto IL_009b;
		}
	} // end catch (depth: 1)

IL_009b:
	{
		goto IL_0179;
	}

IL_00a0:
	{
		int32_t L_19 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_19) == ((int32_t)2)))
		{
			goto IL_00d7;
		}
	}
	{
		int32_t L_20 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_20) == ((int32_t)1)))
		{
			goto IL_00d7;
		}
	}
	{
		int32_t L_21 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_21) == ((int32_t)((int32_t)13))))
		{
			goto IL_00d7;
		}
	}
	{
		int32_t L_22 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00d7;
		}
	}
	{
		int32_t L_23 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_23) == ((uint32_t)7))))
		{
			goto IL_0179;
		}
	}

IL_00d7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_24 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_2 = L_24;
		String_t* L_25 = __this->get_ALBUM_NAME_3();
		bool L_26 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00fa;
		}
	}
	{
		String_t* L_27 = Environment_GetFolderPath_m4247985398(NULL /*static, unused*/, ((int32_t)39), /*hidden argument*/NULL);
		V_2 = L_27;
		goto IL_011f;
	}

IL_00fa:
	{
		String_t* L_28 = Environment_GetFolderPath_m4247985398(NULL /*static, unused*/, ((int32_t)39), /*hidden argument*/NULL);
		String_t* L_29 = __this->get_ALBUM_NAME_3();
		IL2CPP_RUNTIME_CLASS_INIT(Path_t667902997_il2cpp_TypeInfo_var);
		String_t* L_30 = Path_Combine_m4122812896(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/NULL);
		V_2 = L_30;
		String_t* L_31 = V_2;
		bool L_32 = Directory_Exists_m4117375188(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		if (L_32)
		{
			goto IL_011f;
		}
	}
	{
		String_t* L_33 = V_2;
		Directory_CreateDirectory_m677877474(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
	}

IL_011f:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_34 = ___path0;
			String_t* L_35 = V_2;
			String_t* L_36 = ___path0;
			IL2CPP_RUNTIME_CLASS_INIT(Path_t667902997_il2cpp_TypeInfo_var);
			String_t* L_37 = Path_GetFileName_m26786182(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
			String_t* L_38 = Path_Combine_m4122812896(NULL /*static, unused*/, L_35, L_37, /*hidden argument*/NULL);
			File_Copy_m4182716978(NULL /*static, unused*/, L_34, L_38, /*hidden argument*/NULL);
			OnSuccess_t1013330901 * L_39 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onSuccessInvoker_1();
			if (!L_39)
			{
				goto IL_014a;
			}
		}

IL_013b:
		{
			OnSuccess_t1013330901 * L_40 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onSuccessInvoker_1();
			NullCheck(L_40);
			OnSuccess_Invoke_m629490739(L_40, _stringLiteral2381718370, /*hidden argument*/NULL);
		}

IL_014a:
		{
			goto IL_0179;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_014f;
		throw e;
	}

CATCH_014f:
	{ // begin catch(System.Exception)
		{
			V_3 = ((Exception_t3991598821 *)__exception_local);
			OnError_t1516540506 * L_41 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
			if (!L_41)
			{
				goto IL_0174;
			}
		}

IL_015a:
		{
			OnError_t1516540506 * L_42 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
			Exception_t3991598821 * L_43 = V_3;
			NullCheck(L_43);
			String_t* L_44 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_43);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_45 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3275765239, L_44, /*hidden argument*/NULL);
			NullCheck(L_42);
			OnError_Invoke_m3553004494(L_42, L_45, /*hidden argument*/NULL);
		}

IL_0174:
		{
			goto IL_0179;
		}
	} // end catch (depth: 1)

IL_0179:
	{
		return;
	}
}
// System.Void CaptureAndSave::CopyVideoToCameraRoll(System.String)
extern Il2CppClass* Path_t667902997_il2cpp_TypeInfo_var;
extern Il2CppClass* CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2562156408;
extern Il2CppCodeGenString* _stringLiteral1477602327;
extern Il2CppCodeGenString* _stringLiteral1249293954;
extern Il2CppCodeGenString* _stringLiteral1801649879;
extern const uint32_t CaptureAndSave_CopyVideoToCameraRoll_m99063467_MetadataUsageId;
extern "C"  void CaptureAndSave_CopyVideoToCameraRoll_m99063467 (CaptureAndSave_t700313070 * __this, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSave_CopyVideoToCameraRoll_m99063467_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Exception_t3991598821 * V_1 = NULL;
	String_t* V_2 = NULL;
	Exception_t3991598821 * V_3 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = ___path0;
		String_t* L_2 = __this->get_ALBUM_NAME_3();
		CallNative_CopyVideoToCameraRoll_m1507384752(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		goto IL_0181;
	}

IL_001c:
	{
		int32_t L_3 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_00c5;
		}
	}

IL_0028:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_4 = __this->get_androidImagePath_4();
			V_0 = L_4;
			String_t* L_5 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(Path_t667902997_il2cpp_TypeInfo_var);
			String_t* L_6 = Path_GetDirectoryName_m1772680861(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
			String_t* L_7 = Path_Combine_m4122812896(NULL /*static, unused*/, L_6, _stringLiteral2562156408, /*hidden argument*/NULL);
			V_0 = L_7;
			String_t* L_8 = V_0;
			bool L_9 = Directory_Exists_m4117375188(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
			if (L_9)
			{
				goto IL_0052;
			}
		}

IL_004b:
		{
			String_t* L_10 = V_0;
			Directory_CreateDirectory_m677877474(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		}

IL_0052:
		{
			String_t* L_11 = V_0;
			String_t* L_12 = ___path0;
			IL2CPP_RUNTIME_CLASS_INIT(Path_t667902997_il2cpp_TypeInfo_var);
			String_t* L_13 = Path_GetFileName_m26786182(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
			String_t* L_14 = Path_Combine_m4122812896(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/NULL);
			V_0 = L_14;
			String_t* L_15 = ___path0;
			String_t* L_16 = V_0;
			File_Copy_m4182716978(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
			GameObject_t3674682005 * L_17 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
			String_t* L_18 = V_0;
			NullCheck(L_17);
			GameObject_SendMessage_m423373689(L_17, _stringLiteral1477602327, L_18, 1, /*hidden argument*/NULL);
			OnSuccess_t1013330901 * L_19 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onSuccessInvoker_1();
			if (!L_19)
			{
				goto IL_0091;
			}
		}

IL_0082:
		{
			OnSuccess_t1013330901 * L_20 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onSuccessInvoker_1();
			NullCheck(L_20);
			OnSuccess_Invoke_m629490739(L_20, _stringLiteral1249293954, /*hidden argument*/NULL);
		}

IL_0091:
		{
			goto IL_00c0;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0096;
		throw e;
	}

CATCH_0096:
	{ // begin catch(System.Exception)
		{
			V_1 = ((Exception_t3991598821 *)__exception_local);
			OnError_t1516540506 * L_21 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
			if (!L_21)
			{
				goto IL_00bb;
			}
		}

IL_00a1:
		{
			OnError_t1516540506 * L_22 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
			Exception_t3991598821 * L_23 = V_1;
			NullCheck(L_23);
			String_t* L_24 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_23);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_25 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1801649879, L_24, /*hidden argument*/NULL);
			NullCheck(L_22);
			OnError_Invoke_m3553004494(L_22, L_25, /*hidden argument*/NULL);
		}

IL_00bb:
		{
			goto IL_00c0;
		}
	} // end catch (depth: 1)

IL_00c0:
	{
		goto IL_0181;
	}

IL_00c5:
	{
		int32_t L_26 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_26) == ((int32_t)2)))
		{
			goto IL_00fc;
		}
	}
	{
		int32_t L_27 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_27) == ((int32_t)1)))
		{
			goto IL_00fc;
		}
	}
	{
		int32_t L_28 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_28) == ((int32_t)((int32_t)13))))
		{
			goto IL_00fc;
		}
	}
	{
		int32_t L_29 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00fc;
		}
	}
	{
		int32_t L_30 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_30) == ((uint32_t)7))))
		{
			goto IL_0181;
		}
	}

IL_00fc:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_31 = Environment_GetFolderPath_m4247985398(NULL /*static, unused*/, ((int32_t)39), /*hidden argument*/NULL);
			V_2 = L_31;
			String_t* L_32 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(Path_t667902997_il2cpp_TypeInfo_var);
			String_t* L_33 = Path_GetDirectoryName_m1772680861(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
			String_t* L_34 = Path_Combine_m4122812896(NULL /*static, unused*/, L_33, _stringLiteral2562156408, /*hidden argument*/NULL);
			V_2 = L_34;
			String_t* L_35 = V_2;
			bool L_36 = Directory_Exists_m4117375188(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
			if (L_36)
			{
				goto IL_0127;
			}
		}

IL_0120:
		{
			String_t* L_37 = V_2;
			Directory_CreateDirectory_m677877474(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		}

IL_0127:
		{
			String_t* L_38 = ___path0;
			String_t* L_39 = V_2;
			String_t* L_40 = ___path0;
			IL2CPP_RUNTIME_CLASS_INIT(Path_t667902997_il2cpp_TypeInfo_var);
			String_t* L_41 = Path_GetFileName_m26786182(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
			String_t* L_42 = Path_Combine_m4122812896(NULL /*static, unused*/, L_39, L_41, /*hidden argument*/NULL);
			File_Copy_m4182716978(NULL /*static, unused*/, L_38, L_42, /*hidden argument*/NULL);
			OnSuccess_t1013330901 * L_43 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onSuccessInvoker_1();
			if (!L_43)
			{
				goto IL_0152;
			}
		}

IL_0143:
		{
			OnSuccess_t1013330901 * L_44 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onSuccessInvoker_1();
			NullCheck(L_44);
			OnSuccess_Invoke_m629490739(L_44, _stringLiteral1249293954, /*hidden argument*/NULL);
		}

IL_0152:
		{
			goto IL_0181;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0157;
		throw e;
	}

CATCH_0157:
	{ // begin catch(System.Exception)
		{
			V_3 = ((Exception_t3991598821 *)__exception_local);
			OnError_t1516540506 * L_45 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
			if (!L_45)
			{
				goto IL_017c;
			}
		}

IL_0162:
		{
			OnError_t1516540506 * L_46 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
			Exception_t3991598821 * L_47 = V_3;
			NullCheck(L_47);
			String_t* L_48 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_47);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_49 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1801649879, L_48, /*hidden argument*/NULL);
			NullCheck(L_46);
			OnError_Invoke_m3553004494(L_46, L_49, /*hidden argument*/NULL);
		}

IL_017c:
		{
			goto IL_0181;
		}
	} // end catch (depth: 1)

IL_0181:
	{
		return;
	}
}
// System.Void CaptureAndSave::MoveImageToCameraRoll(System.String)
extern Il2CppClass* Path_t667902997_il2cpp_TypeInfo_var;
extern Il2CppClass* CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1477602327;
extern Il2CppCodeGenString* _stringLiteral2381718370;
extern Il2CppCodeGenString* _stringLiteral3275765239;
extern const uint32_t CaptureAndSave_MoveImageToCameraRoll_m2078478887_MetadataUsageId;
extern "C"  void CaptureAndSave_MoveImageToCameraRoll_m2078478887 (CaptureAndSave_t700313070 * __this, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSave_MoveImageToCameraRoll_m2078478887_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Exception_t3991598821 * V_1 = NULL;
	String_t* V_2 = NULL;
	Exception_t3991598821 * V_3 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = ___path0;
		String_t* L_2 = __this->get_ALBUM_NAME_3();
		CallNative_MoveImageToCameraRoll_m786993196(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		goto IL_017b;
	}

IL_001c:
	{
		int32_t L_3 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_00a0;
		}
	}

IL_0028:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_4 = __this->get_androidImagePath_4();
			String_t* L_5 = ___path0;
			IL2CPP_RUNTIME_CLASS_INIT(Path_t667902997_il2cpp_TypeInfo_var);
			String_t* L_6 = Path_GetFileName_m26786182(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
			String_t* L_7 = Path_Combine_m4122812896(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
			V_0 = L_7;
			String_t* L_8 = ___path0;
			String_t* L_9 = V_0;
			File_Move_m1404293974(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
			GameObject_t3674682005 * L_10 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
			String_t* L_11 = V_0;
			NullCheck(L_10);
			GameObject_SendMessage_m423373689(L_10, _stringLiteral1477602327, L_11, 1, /*hidden argument*/NULL);
			OnSuccess_t1013330901 * L_12 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onSuccessInvoker_1();
			if (!L_12)
			{
				goto IL_006c;
			}
		}

IL_005d:
		{
			OnSuccess_t1013330901 * L_13 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onSuccessInvoker_1();
			NullCheck(L_13);
			OnSuccess_Invoke_m629490739(L_13, _stringLiteral2381718370, /*hidden argument*/NULL);
		}

IL_006c:
		{
			goto IL_009b;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0071;
		throw e;
	}

CATCH_0071:
	{ // begin catch(System.Exception)
		{
			V_1 = ((Exception_t3991598821 *)__exception_local);
			OnError_t1516540506 * L_14 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
			if (!L_14)
			{
				goto IL_0096;
			}
		}

IL_007c:
		{
			OnError_t1516540506 * L_15 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
			Exception_t3991598821 * L_16 = V_1;
			NullCheck(L_16);
			String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_16);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_18 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3275765239, L_17, /*hidden argument*/NULL);
			NullCheck(L_15);
			OnError_Invoke_m3553004494(L_15, L_18, /*hidden argument*/NULL);
		}

IL_0096:
		{
			goto IL_009b;
		}
	} // end catch (depth: 1)

IL_009b:
	{
		goto IL_017b;
	}

IL_00a0:
	{
		int32_t L_19 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_19) == ((int32_t)2)))
		{
			goto IL_00d7;
		}
	}
	{
		int32_t L_20 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_20) == ((int32_t)1)))
		{
			goto IL_00d7;
		}
	}
	{
		int32_t L_21 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_21) == ((int32_t)((int32_t)13))))
		{
			goto IL_00d7;
		}
	}
	{
		int32_t L_22 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00d7;
		}
	}
	{
		int32_t L_23 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_23) == ((uint32_t)7))))
		{
			goto IL_017b;
		}
	}

IL_00d7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_24 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_2 = L_24;
		String_t* L_25 = __this->get_ALBUM_NAME_3();
		bool L_26 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00fa;
		}
	}
	{
		String_t* L_27 = Environment_GetFolderPath_m4247985398(NULL /*static, unused*/, ((int32_t)39), /*hidden argument*/NULL);
		V_2 = L_27;
		goto IL_011f;
	}

IL_00fa:
	{
		String_t* L_28 = Environment_GetFolderPath_m4247985398(NULL /*static, unused*/, ((int32_t)39), /*hidden argument*/NULL);
		String_t* L_29 = __this->get_ALBUM_NAME_3();
		IL2CPP_RUNTIME_CLASS_INIT(Path_t667902997_il2cpp_TypeInfo_var);
		String_t* L_30 = Path_Combine_m4122812896(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/NULL);
		V_2 = L_30;
		String_t* L_31 = V_2;
		bool L_32 = Directory_Exists_m4117375188(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		if (L_32)
		{
			goto IL_011f;
		}
	}
	{
		String_t* L_33 = V_2;
		Directory_CreateDirectory_m677877474(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
	}

IL_011f:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_34 = V_2;
			String_t* L_35 = ___path0;
			IL2CPP_RUNTIME_CLASS_INIT(Path_t667902997_il2cpp_TypeInfo_var);
			String_t* L_36 = Path_GetFileName_m26786182(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
			String_t* L_37 = Path_Combine_m4122812896(NULL /*static, unused*/, L_34, L_36, /*hidden argument*/NULL);
			V_2 = L_37;
			String_t* L_38 = ___path0;
			String_t* L_39 = V_2;
			File_Move_m1404293974(NULL /*static, unused*/, L_38, L_39, /*hidden argument*/NULL);
			OnSuccess_t1013330901 * L_40 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onSuccessInvoker_1();
			if (!L_40)
			{
				goto IL_014c;
			}
		}

IL_013d:
		{
			OnSuccess_t1013330901 * L_41 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onSuccessInvoker_1();
			NullCheck(L_41);
			OnSuccess_Invoke_m629490739(L_41, _stringLiteral2381718370, /*hidden argument*/NULL);
		}

IL_014c:
		{
			goto IL_017b;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0151;
		throw e;
	}

CATCH_0151:
	{ // begin catch(System.Exception)
		{
			V_3 = ((Exception_t3991598821 *)__exception_local);
			OnError_t1516540506 * L_42 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
			if (!L_42)
			{
				goto IL_0176;
			}
		}

IL_015c:
		{
			OnError_t1516540506 * L_43 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
			Exception_t3991598821 * L_44 = V_3;
			NullCheck(L_44);
			String_t* L_45 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_44);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_46 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3275765239, L_45, /*hidden argument*/NULL);
			NullCheck(L_43);
			OnError_Invoke_m3553004494(L_43, L_46, /*hidden argument*/NULL);
		}

IL_0176:
		{
			goto IL_017b;
		}
	} // end catch (depth: 1)

IL_017b:
	{
		return;
	}
}
// System.Void CaptureAndSave::MoveVideoToCameraRoll(System.String)
extern Il2CppClass* Path_t667902997_il2cpp_TypeInfo_var;
extern Il2CppClass* CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2562156408;
extern Il2CppCodeGenString* _stringLiteral1477602327;
extern Il2CppCodeGenString* _stringLiteral1249293954;
extern Il2CppCodeGenString* _stringLiteral1801649879;
extern const uint32_t CaptureAndSave_MoveVideoToCameraRoll_m709079815_MetadataUsageId;
extern "C"  void CaptureAndSave_MoveVideoToCameraRoll_m709079815 (CaptureAndSave_t700313070 * __this, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSave_MoveVideoToCameraRoll_m709079815_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Exception_t3991598821 * V_1 = NULL;
	String_t* V_2 = NULL;
	Exception_t3991598821 * V_3 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = ___path0;
		String_t* L_2 = __this->get_ALBUM_NAME_3();
		CallNative_MoveVideoToCameraRoll_m4155386124(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		goto IL_0181;
	}

IL_001c:
	{
		int32_t L_3 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_00c5;
		}
	}

IL_0028:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_4 = __this->get_androidImagePath_4();
			V_0 = L_4;
			String_t* L_5 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(Path_t667902997_il2cpp_TypeInfo_var);
			String_t* L_6 = Path_GetDirectoryName_m1772680861(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
			String_t* L_7 = Path_Combine_m4122812896(NULL /*static, unused*/, L_6, _stringLiteral2562156408, /*hidden argument*/NULL);
			V_0 = L_7;
			String_t* L_8 = V_0;
			bool L_9 = Directory_Exists_m4117375188(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
			if (L_9)
			{
				goto IL_0052;
			}
		}

IL_004b:
		{
			String_t* L_10 = V_0;
			Directory_CreateDirectory_m677877474(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		}

IL_0052:
		{
			String_t* L_11 = V_0;
			String_t* L_12 = ___path0;
			IL2CPP_RUNTIME_CLASS_INIT(Path_t667902997_il2cpp_TypeInfo_var);
			String_t* L_13 = Path_GetFileName_m26786182(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
			String_t* L_14 = Path_Combine_m4122812896(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/NULL);
			V_0 = L_14;
			String_t* L_15 = ___path0;
			String_t* L_16 = V_0;
			File_Move_m1404293974(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
			GameObject_t3674682005 * L_17 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
			String_t* L_18 = V_0;
			NullCheck(L_17);
			GameObject_SendMessage_m423373689(L_17, _stringLiteral1477602327, L_18, 1, /*hidden argument*/NULL);
			OnSuccess_t1013330901 * L_19 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onSuccessInvoker_1();
			if (!L_19)
			{
				goto IL_0091;
			}
		}

IL_0082:
		{
			OnSuccess_t1013330901 * L_20 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onSuccessInvoker_1();
			NullCheck(L_20);
			OnSuccess_Invoke_m629490739(L_20, _stringLiteral1249293954, /*hidden argument*/NULL);
		}

IL_0091:
		{
			goto IL_00c0;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0096;
		throw e;
	}

CATCH_0096:
	{ // begin catch(System.Exception)
		{
			V_1 = ((Exception_t3991598821 *)__exception_local);
			OnError_t1516540506 * L_21 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
			if (!L_21)
			{
				goto IL_00bb;
			}
		}

IL_00a1:
		{
			OnError_t1516540506 * L_22 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
			Exception_t3991598821 * L_23 = V_1;
			NullCheck(L_23);
			String_t* L_24 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_23);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_25 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1801649879, L_24, /*hidden argument*/NULL);
			NullCheck(L_22);
			OnError_Invoke_m3553004494(L_22, L_25, /*hidden argument*/NULL);
		}

IL_00bb:
		{
			goto IL_00c0;
		}
	} // end catch (depth: 1)

IL_00c0:
	{
		goto IL_0181;
	}

IL_00c5:
	{
		int32_t L_26 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_26) == ((int32_t)2)))
		{
			goto IL_00fc;
		}
	}
	{
		int32_t L_27 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_27) == ((int32_t)1)))
		{
			goto IL_00fc;
		}
	}
	{
		int32_t L_28 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_28) == ((int32_t)((int32_t)13))))
		{
			goto IL_00fc;
		}
	}
	{
		int32_t L_29 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00fc;
		}
	}
	{
		int32_t L_30 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_30) == ((uint32_t)7))))
		{
			goto IL_0181;
		}
	}

IL_00fc:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_31 = Environment_GetFolderPath_m4247985398(NULL /*static, unused*/, ((int32_t)39), /*hidden argument*/NULL);
			V_2 = L_31;
			String_t* L_32 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(Path_t667902997_il2cpp_TypeInfo_var);
			String_t* L_33 = Path_GetDirectoryName_m1772680861(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
			String_t* L_34 = Path_Combine_m4122812896(NULL /*static, unused*/, L_33, _stringLiteral2562156408, /*hidden argument*/NULL);
			V_2 = L_34;
			String_t* L_35 = V_2;
			bool L_36 = Directory_Exists_m4117375188(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
			if (L_36)
			{
				goto IL_0127;
			}
		}

IL_0120:
		{
			String_t* L_37 = V_2;
			Directory_CreateDirectory_m677877474(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		}

IL_0127:
		{
			String_t* L_38 = ___path0;
			String_t* L_39 = V_2;
			String_t* L_40 = ___path0;
			IL2CPP_RUNTIME_CLASS_INIT(Path_t667902997_il2cpp_TypeInfo_var);
			String_t* L_41 = Path_GetFileName_m26786182(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
			String_t* L_42 = Path_Combine_m4122812896(NULL /*static, unused*/, L_39, L_41, /*hidden argument*/NULL);
			File_Move_m1404293974(NULL /*static, unused*/, L_38, L_42, /*hidden argument*/NULL);
			OnSuccess_t1013330901 * L_43 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onSuccessInvoker_1();
			if (!L_43)
			{
				goto IL_0152;
			}
		}

IL_0143:
		{
			OnSuccess_t1013330901 * L_44 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onSuccessInvoker_1();
			NullCheck(L_44);
			OnSuccess_Invoke_m629490739(L_44, _stringLiteral1249293954, /*hidden argument*/NULL);
		}

IL_0152:
		{
			goto IL_0181;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0157;
		throw e;
	}

CATCH_0157:
	{ // begin catch(System.Exception)
		{
			V_3 = ((Exception_t3991598821 *)__exception_local);
			OnError_t1516540506 * L_45 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
			if (!L_45)
			{
				goto IL_017c;
			}
		}

IL_0162:
		{
			OnError_t1516540506 * L_46 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
			Exception_t3991598821 * L_47 = V_3;
			NullCheck(L_47);
			String_t* L_48 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_47);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_49 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1801649879, L_48, /*hidden argument*/NULL);
			NullCheck(L_46);
			OnError_Invoke_m3553004494(L_46, L_49, /*hidden argument*/NULL);
		}

IL_017c:
		{
			goto IL_0181;
		}
	} // end catch (depth: 1)

IL_0181:
	{
		return;
	}
}
// UnityEngine.Texture2D CaptureAndSave::AddWatermark(UnityEngine.Texture2D,UnityEngine.Texture2D,WatermarkAlignment)
extern "C"  Texture2D_t3884108195 * CaptureAndSave_AddWatermark_m277847075 (CaptureAndSave_t700313070 * __this, Texture2D_t3884108195 * ___background0, Texture2D_t3884108195 * ___watermark1, int32_t ___align2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		int32_t L_0 = ___align2;
		V_2 = L_0;
		int32_t L_1 = V_2;
		if (L_1 == 0)
		{
			goto IL_0043;
		}
		if (L_1 == 1)
		{
			goto IL_0058;
		}
		if (L_1 == 2)
		{
			goto IL_0025;
		}
		if (L_1 == 3)
		{
			goto IL_002e;
		}
		if (L_1 == 4)
		{
			goto IL_0079;
		}
	}
	{
		goto IL_009e;
	}

IL_0025:
	{
		V_0 = 0;
		V_1 = 0;
		goto IL_009e;
	}

IL_002e:
	{
		Texture2D_t3884108195 * L_2 = ___background0;
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_2);
		Texture2D_t3884108195 * L_4 = ___watermark1;
		NullCheck(L_4);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_4);
		V_0 = ((int32_t)((int32_t)L_3-(int32_t)L_5));
		V_1 = 0;
		goto IL_009e;
	}

IL_0043:
	{
		V_0 = 0;
		Texture2D_t3884108195 * L_6 = ___background0;
		NullCheck(L_6);
		int32_t L_7 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_6);
		Texture2D_t3884108195 * L_8 = ___watermark1;
		NullCheck(L_8);
		int32_t L_9 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_8);
		V_1 = ((int32_t)((int32_t)L_7-(int32_t)L_9));
		goto IL_009e;
	}

IL_0058:
	{
		Texture2D_t3884108195 * L_10 = ___background0;
		NullCheck(L_10);
		int32_t L_11 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_10);
		Texture2D_t3884108195 * L_12 = ___watermark1;
		NullCheck(L_12);
		int32_t L_13 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_12);
		V_0 = ((int32_t)((int32_t)L_11-(int32_t)L_13));
		Texture2D_t3884108195 * L_14 = ___background0;
		NullCheck(L_14);
		int32_t L_15 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_14);
		Texture2D_t3884108195 * L_16 = ___watermark1;
		NullCheck(L_16);
		int32_t L_17 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_16);
		V_1 = ((int32_t)((int32_t)L_15-(int32_t)L_17));
		goto IL_009e;
	}

IL_0079:
	{
		Texture2D_t3884108195 * L_18 = ___background0;
		NullCheck(L_18);
		int32_t L_19 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_18);
		Texture2D_t3884108195 * L_20 = ___watermark1;
		NullCheck(L_20);
		int32_t L_21 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_20);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_19-(int32_t)L_21))/(int32_t)2));
		Texture2D_t3884108195 * L_22 = ___background0;
		NullCheck(L_22);
		int32_t L_23 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_22);
		Texture2D_t3884108195 * L_24 = ___watermark1;
		NullCheck(L_24);
		int32_t L_25 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_24);
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)L_23-(int32_t)L_25))/(int32_t)2));
		goto IL_009e;
	}

IL_009e:
	{
		Texture2D_t3884108195 * L_26 = ___background0;
		Texture2D_t3884108195 * L_27 = ___watermark1;
		int32_t L_28 = V_0;
		int32_t L_29 = V_1;
		Texture2D_t3884108195 * L_30 = CaptureAndSave_CombineTexture_m3511564587(__this, L_26, L_27, L_28, L_29, /*hidden argument*/NULL);
		return L_30;
	}
}
// UnityEngine.Texture2D CaptureAndSave::AddWatermark(UnityEngine.Texture2D,UnityEngine.Texture2D,System.Int32,System.Int32)
extern "C"  Texture2D_t3884108195 * CaptureAndSave_AddWatermark_m66560740 (CaptureAndSave_t700313070 * __this, Texture2D_t3884108195 * ___background0, Texture2D_t3884108195 * ___watermark1, int32_t ___startX2, int32_t ___startY3, const MethodInfo* method)
{
	{
		Texture2D_t3884108195 * L_0 = ___background0;
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_0);
		int32_t L_2 = ___startY3;
		Texture2D_t3884108195 * L_3 = ___watermark1;
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_3);
		___startY3 = ((int32_t)((int32_t)((int32_t)((int32_t)L_1-(int32_t)L_2))-(int32_t)L_4));
		Texture2D_t3884108195 * L_5 = ___background0;
		Texture2D_t3884108195 * L_6 = ___watermark1;
		int32_t L_7 = ___startX2;
		int32_t L_8 = ___startY3;
		Texture2D_t3884108195 * L_9 = CaptureAndSave_CombineTexture_m3511564587(__this, L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// UnityEngine.Texture2D CaptureAndSave::CombineTexture(UnityEngine.Texture2D,UnityEngine.Texture2D,System.Int32,System.Int32)
extern Il2CppClass* Texture2D_t3884108195_il2cpp_TypeInfo_var;
extern const uint32_t CaptureAndSave_CombineTexture_m3511564587_MetadataUsageId;
extern "C"  Texture2D_t3884108195 * CaptureAndSave_CombineTexture_m3511564587 (CaptureAndSave_t700313070 * __this, Texture2D_t3884108195 * ___background0, Texture2D_t3884108195 * ___watermark1, int32_t ___startX2, int32_t ___startY3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSave_CombineTexture_m3511564587_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Texture2D_t3884108195 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Color_t4194546905  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Color_t4194546905  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Color_t4194546905  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Color_t4194546905  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		Texture2D_t3884108195 * L_0 = ___background0;
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_0);
		Texture2D_t3884108195 * L_2 = ___background0;
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_2);
		Texture2D_t3884108195 * L_4 = (Texture2D_t3884108195 *)il2cpp_codegen_object_new(Texture2D_t3884108195_il2cpp_TypeInfo_var);
		Texture2D__ctor_m3705883154(L_4, L_1, L_3, 5, (bool)0, /*hidden argument*/NULL);
		V_0 = L_4;
		V_1 = 0;
		goto IL_00b4;
	}

IL_001b:
	{
		V_2 = 0;
		goto IL_00a4;
	}

IL_0022:
	{
		int32_t L_5 = V_1;
		int32_t L_6 = ___startX2;
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_7 = V_1;
		int32_t L_8 = ___startX2;
		Texture2D_t3884108195 * L_9 = ___watermark1;
		NullCheck(L_9);
		int32_t L_10 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_9);
		if ((((int32_t)L_7) > ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_11 = V_2;
		int32_t L_12 = ___startY3;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_13 = V_2;
		int32_t L_14 = ___startY3;
		Texture2D_t3884108195 * L_15 = ___watermark1;
		NullCheck(L_15);
		int32_t L_16 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_15);
		if ((((int32_t)L_13) > ((int32_t)((int32_t)((int32_t)L_14+(int32_t)L_16)))))
		{
			goto IL_008c;
		}
	}
	{
		Texture2D_t3884108195 * L_17 = ___background0;
		int32_t L_18 = V_1;
		int32_t L_19 = V_2;
		NullCheck(L_17);
		Color_t4194546905  L_20 = Texture2D_GetPixel_m380515877(L_17, L_18, L_19, /*hidden argument*/NULL);
		V_3 = L_20;
		Texture2D_t3884108195 * L_21 = ___watermark1;
		int32_t L_22 = V_1;
		int32_t L_23 = ___startX2;
		int32_t L_24 = V_2;
		int32_t L_25 = ___startY3;
		NullCheck(L_21);
		Color_t4194546905  L_26 = Texture2D_GetPixel_m380515877(L_21, ((int32_t)((int32_t)L_22-(int32_t)L_23)), ((int32_t)((int32_t)L_24-(int32_t)L_25)), /*hidden argument*/NULL);
		V_4 = L_26;
		Color_t4194546905  L_27 = V_3;
		Color_t4194546905  L_28 = V_4;
		float L_29 = (&V_4)->get_a_3();
		Color_t4194546905  L_30 = Color_Lerp_m3494628845(NULL /*static, unused*/, L_27, L_28, ((float)((float)L_29/(float)(1.0f))), /*hidden argument*/NULL);
		V_5 = L_30;
		Texture2D_t3884108195 * L_31 = V_0;
		int32_t L_32 = V_1;
		int32_t L_33 = V_2;
		Color_t4194546905  L_34 = V_5;
		NullCheck(L_31);
		Texture2D_SetPixel_m378278602(L_31, L_32, L_33, L_34, /*hidden argument*/NULL);
		goto IL_00a0;
	}

IL_008c:
	{
		Texture2D_t3884108195 * L_35 = ___background0;
		int32_t L_36 = V_1;
		int32_t L_37 = V_2;
		NullCheck(L_35);
		Color_t4194546905  L_38 = Texture2D_GetPixel_m380515877(L_35, L_36, L_37, /*hidden argument*/NULL);
		V_6 = L_38;
		Texture2D_t3884108195 * L_39 = V_0;
		int32_t L_40 = V_1;
		int32_t L_41 = V_2;
		Color_t4194546905  L_42 = V_6;
		NullCheck(L_39);
		Texture2D_SetPixel_m378278602(L_39, L_40, L_41, L_42, /*hidden argument*/NULL);
	}

IL_00a0:
	{
		int32_t L_43 = V_2;
		V_2 = ((int32_t)((int32_t)L_43+(int32_t)1));
	}

IL_00a4:
	{
		int32_t L_44 = V_2;
		Texture2D_t3884108195 * L_45 = ___background0;
		NullCheck(L_45);
		int32_t L_46 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_45);
		if ((((int32_t)L_44) < ((int32_t)L_46)))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_47 = V_1;
		V_1 = ((int32_t)((int32_t)L_47+(int32_t)1));
	}

IL_00b4:
	{
		int32_t L_48 = V_1;
		Texture2D_t3884108195 * L_49 = ___background0;
		NullCheck(L_49);
		int32_t L_50 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_49);
		if ((((int32_t)L_48) < ((int32_t)L_50)))
		{
			goto IL_001b;
		}
	}
	{
		Texture2D_t3884108195 * L_51 = V_0;
		NullCheck(L_51);
		Texture2D_Apply_m1364130776(L_51, /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_52 = V_0;
		return L_52;
	}
}
// System.Void CaptureAndSave::AndroidGalleryPath(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Path_t667902997_il2cpp_TypeInfo_var;
extern const uint32_t CaptureAndSave_AndroidGalleryPath_m2832616556_MetadataUsageId;
extern "C"  void CaptureAndSave_AndroidGalleryPath_m2832616556 (CaptureAndSave_t700313070 * __this, String_t* ___androidPath0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSave_AndroidGalleryPath_m2832616556_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_ALBUM_NAME_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_2 = ___androidPath0;
		String_t* L_3 = __this->get_ALBUM_NAME_3();
		IL2CPP_RUNTIME_CLASS_INIT(Path_t667902997_il2cpp_TypeInfo_var);
		String_t* L_4 = Path_Combine_m4122812896(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		__this->set_androidImagePath_4(L_4);
		goto IL_002e;
	}

IL_0027:
	{
		String_t* L_5 = ___androidPath0;
		__this->set_androidImagePath_4(L_5);
	}

IL_002e:
	{
		String_t* L_6 = __this->get_androidImagePath_4();
		bool L_7 = Directory_Exists_m4117375188(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_004a;
		}
	}
	{
		String_t* L_8 = __this->get_androidImagePath_4();
		Directory_CreateDirectory_m677877474(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_004a:
	{
		return;
	}
}
// System.Void CaptureAndSave::OnError(System.String)
extern Il2CppClass* CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var;
extern const uint32_t CaptureAndSave_OnError_m608607365_MetadataUsageId;
extern "C"  void CaptureAndSave_OnError_m608607365 (CaptureAndSave_t700313070 * __this, String_t* ___err0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSave_OnError_m608607365_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		OnError_t1516540506 * L_0 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		OnError_t1516540506 * L_1 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		String_t* L_2 = ___err0;
		NullCheck(L_1);
		OnError_Invoke_m3553004494(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void CaptureAndSave::OnComplete(System.String)
extern Il2CppClass* CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var;
extern const uint32_t CaptureAndSave_OnComplete_m3721315804_MetadataUsageId;
extern "C"  void CaptureAndSave_OnComplete_m3721315804 (CaptureAndSave_t700313070 * __this, String_t* ___msg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSave_OnComplete_m3721315804_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		OnSuccess_t1013330901 * L_0 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onSuccessInvoker_1();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		OnSuccess_t1013330901 * L_1 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onSuccessInvoker_1();
		String_t* L_2 = ___msg0;
		NullCheck(L_1);
		OnSuccess_Invoke_m629490739(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void CaptureAndSave/<GetScreenPixels>c__Iterator1::.ctor()
extern "C"  void U3CGetScreenPixelsU3Ec__Iterator1__ctor_m3292122010 (U3CGetScreenPixelsU3Ec__Iterator1_t3827175978 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object CaptureAndSave/<GetScreenPixels>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetScreenPixelsU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1637417282 (U3CGetScreenPixelsU3Ec__Iterator1_t3827175978 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_10();
		return L_0;
	}
}
// System.Object CaptureAndSave/<GetScreenPixels>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetScreenPixelsU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m153733334 (U3CGetScreenPixelsU3Ec__Iterator1_t3827175978 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_10();
		return L_0;
	}
}
// System.Boolean CaptureAndSave/<GetScreenPixels>c__Iterator1::MoveNext()
extern Il2CppClass* WaitForEndOfFrame_t2372756133_il2cpp_TypeInfo_var;
extern Il2CppClass* Texture2D_t3884108195_il2cpp_TypeInfo_var;
extern Il2CppClass* CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetScreenPixelsU3Ec__Iterator1_MoveNext_m676718516_MetadataUsageId;
extern "C"  bool U3CGetScreenPixelsU3Ec__Iterator1_MoveNext_m676718516 (U3CGetScreenPixelsU3Ec__Iterator1_t3827175978 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetScreenPixelsU3Ec__Iterator1_MoveNext_m676718516_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_9();
		V_0 = L_0;
		__this->set_U24PC_9((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0038;
		}
	}
	{
		goto IL_0151;
	}

IL_0021:
	{
		WaitForEndOfFrame_t2372756133 * L_2 = (WaitForEndOfFrame_t2372756133 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t2372756133_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m4124201226(L_2, /*hidden argument*/NULL);
		__this->set_U24current_10(L_2);
		__this->set_U24PC_9(1);
		goto IL_0153;
	}

IL_0038:
	{
		int32_t L_3 = __this->get_imgType_0();
		if ((!(((uint32_t)L_3) == ((uint32_t)1))))
		{
			goto IL_0050;
		}
	}
	{
		__this->set_U3CtFormatU3E__0_1(5);
		goto IL_0057;
	}

IL_0050:
	{
		__this->set_U3CtFormatU3E__0_1(3);
	}

IL_0057:
	{
		CaptureAndSave_t700313070 * L_4 = __this->get_U3CU3Ef__this_18();
		int32_t L_5 = __this->get_width_2();
		int32_t L_6 = __this->get_height_3();
		int32_t L_7 = __this->get_U3CtFormatU3E__0_1();
		Texture2D_t3884108195 * L_8 = (Texture2D_t3884108195 *)il2cpp_codegen_object_new(Texture2D_t3884108195_il2cpp_TypeInfo_var);
		Texture2D__ctor_m3705883154(L_8, L_5, L_6, L_7, (bool)0, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_tex_6(L_8);
		CaptureAndSave_t700313070 * L_9 = __this->get_U3CU3Ef__this_18();
		NullCheck(L_9);
		Texture2D_t3884108195 * L_10 = L_9->get_tex_6();
		int32_t L_11 = __this->get_x_4();
		int32_t L_12 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_13 = __this->get_height_3();
		int32_t L_14 = __this->get_y_5();
		int32_t L_15 = __this->get_width_2();
		int32_t L_16 = __this->get_height_3();
		Rect_t4241904616  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Rect__ctor_m3291325233(&L_17, (((float)((float)L_11))), (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_12-(int32_t)L_13))-(int32_t)L_14))))), (((float)((float)L_15))), (((float)((float)L_16))), /*hidden argument*/NULL);
		NullCheck(L_10);
		Texture2D_ReadPixels_m1334301696(L_10, L_17, 0, 0, /*hidden argument*/NULL);
		CaptureAndSave_t700313070 * L_18 = __this->get_U3CU3Ef__this_18();
		NullCheck(L_18);
		Texture2D_t3884108195 * L_19 = L_18->get_tex_6();
		NullCheck(L_19);
		Texture2D_Apply_m1364130776(L_19, /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_20 = __this->get_watermark_6();
		bool L_21 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_20, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_012b;
		}
	}
	{
		CaptureAndSave_t700313070 * L_22 = __this->get_U3CU3Ef__this_18();
		NullCheck(L_22);
		Texture2D_t3884108195 * L_23 = L_22->get_tex_6();
		__this->set_U3CtempU3E__1_7(L_23);
		CaptureAndSave_t700313070 * L_24 = __this->get_U3CU3Ef__this_18();
		CaptureAndSave_t700313070 * L_25 = __this->get_U3CU3Ef__this_18();
		CaptureAndSave_t700313070 * L_26 = __this->get_U3CU3Ef__this_18();
		NullCheck(L_26);
		Texture2D_t3884108195 * L_27 = L_26->get_tex_6();
		Texture2D_t3884108195 * L_28 = __this->get_watermark_6();
		int32_t L_29 = __this->get_align_8();
		NullCheck(L_25);
		Texture2D_t3884108195 * L_30 = CaptureAndSave_AddWatermark_m277847075(L_25, L_27, L_28, L_29, /*hidden argument*/NULL);
		NullCheck(L_24);
		L_24->set_tex_6(L_30);
		Texture2D_t3884108195 * L_31 = __this->get_U3CtempU3E__1_7();
		Object_Destroy_m176400816(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		__this->set_U3CtempU3E__1_7((Texture2D_t3884108195 *)NULL);
	}

IL_012b:
	{
		OnScreenShot_t3427048564 * L_32 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onScreenShotInvoker_2();
		if (!L_32)
		{
			goto IL_014a;
		}
	}
	{
		OnScreenShot_t3427048564 * L_33 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onScreenShotInvoker_2();
		CaptureAndSave_t700313070 * L_34 = __this->get_U3CU3Ef__this_18();
		NullCheck(L_34);
		Texture2D_t3884108195 * L_35 = L_34->get_tex_6();
		NullCheck(L_33);
		OnScreenShot_Invoke_m3599849904(L_33, L_35, /*hidden argument*/NULL);
	}

IL_014a:
	{
		__this->set_U24PC_9((-1));
	}

IL_0151:
	{
		return (bool)0;
	}

IL_0153:
	{
		return (bool)1;
	}
}
// System.Void CaptureAndSave/<GetScreenPixels>c__Iterator1::Dispose()
extern "C"  void U3CGetScreenPixelsU3Ec__Iterator1_Dispose_m2531604823 (U3CGetScreenPixelsU3Ec__Iterator1_t3827175978 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_9((-1));
		return;
	}
}
// System.Void CaptureAndSave/<GetScreenPixels>c__Iterator1::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetScreenPixelsU3Ec__Iterator1_Reset_m938554951_MetadataUsageId;
extern "C"  void U3CGetScreenPixelsU3Ec__Iterator1_Reset_m938554951 (U3CGetScreenPixelsU3Ec__Iterator1_t3827175978 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetScreenPixelsU3Ec__Iterator1_Reset_m938554951_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void CaptureAndSave/<SaveToAlbum>c__Iterator0::.ctor()
extern "C"  void U3CSaveToAlbumU3Ec__Iterator0__ctor_m321072963 (U3CSaveToAlbumU3Ec__Iterator0_t371193057 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object CaptureAndSave/<SaveToAlbum>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSaveToAlbumU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m914667449 (U3CSaveToAlbumU3Ec__Iterator0_t371193057 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_11();
		return L_0;
	}
}
// System.Object CaptureAndSave/<SaveToAlbum>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSaveToAlbumU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1655131469 (U3CSaveToAlbumU3Ec__Iterator0_t371193057 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_11();
		return L_0;
	}
}
// System.Boolean CaptureAndSave/<SaveToAlbum>c__Iterator0::MoveNext()
extern Il2CppClass* WaitForEndOfFrame_t2372756133_il2cpp_TypeInfo_var;
extern Il2CppClass* CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral946707300;
extern const uint32_t U3CSaveToAlbumU3Ec__Iterator0_MoveNext_m135567979_MetadataUsageId;
extern "C"  bool U3CSaveToAlbumU3Ec__Iterator0_MoveNext_m135567979 (U3CSaveToAlbumU3Ec__Iterator0_t371193057 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CSaveToAlbumU3Ec__Iterator0_MoveNext_m135567979_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_10();
		V_0 = L_0;
		__this->set_U24PC_10((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_003c;
		}
		if (L_1 == 2)
		{
			goto IL_0082;
		}
	}
	{
		goto IL_00e8;
	}

IL_0025:
	{
		WaitForEndOfFrame_t2372756133 * L_2 = (WaitForEndOfFrame_t2372756133 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t2372756133_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m4124201226(L_2, /*hidden argument*/NULL);
		__this->set_U24current_11(L_2);
		__this->set_U24PC_10(1);
		goto IL_00ea;
	}

IL_003c:
	{
		CaptureAndSave_t700313070 * L_3 = __this->get_U3CU3Ef__this_21();
		int32_t L_4 = __this->get_x_0();
		int32_t L_5 = __this->get_y_1();
		int32_t L_6 = __this->get_width_2();
		int32_t L_7 = __this->get_height_3();
		int32_t L_8 = __this->get_imgType_4();
		NullCheck(L_3);
		Texture2D_t3884108195 * L_9 = CaptureAndSave_GetPixels_m1882752963(L_3, L_4, L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		__this->set_U3CtU3E__0_5(L_9);
		WaitForEndOfFrame_t2372756133 * L_10 = (WaitForEndOfFrame_t2372756133 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t2372756133_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m4124201226(L_10, /*hidden argument*/NULL);
		__this->set_U24current_11(L_10);
		__this->set_U24PC_10(2);
		goto IL_00ea;
	}

IL_0082:
	{
		Texture2D_t3884108195 * L_11 = __this->get_U3CtU3E__0_5();
		bool L_12 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_11, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00b1;
		}
	}
	{
		OnError_t1516540506 * L_13 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		if (!L_13)
		{
			goto IL_00ac;
		}
	}
	{
		OnError_t1516540506 * L_14 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		NullCheck(L_14);
		OnError_Invoke_m3553004494(L_14, _stringLiteral946707300, /*hidden argument*/NULL);
	}

IL_00ac:
	{
		goto IL_00e8;
	}

IL_00b1:
	{
		CaptureAndSave_t700313070 * L_15 = __this->get_U3CU3Ef__this_21();
		Texture2D_t3884108195 * L_16 = __this->get_U3CtU3E__0_5();
		String_t* L_17 = __this->get_path_6();
		int32_t L_18 = __this->get_imgType_4();
		bool L_19 = __this->get_saveToGallery_7();
		Texture2D_t3884108195 * L_20 = __this->get_watermark_8();
		int32_t L_21 = __this->get_align_9();
		NullCheck(L_15);
		CaptureAndSave_SaveTexture_m1816178388(L_15, L_16, L_17, L_18, (bool)1, L_19, L_20, L_21, /*hidden argument*/NULL);
		__this->set_U24PC_10((-1));
	}

IL_00e8:
	{
		return (bool)0;
	}

IL_00ea:
	{
		return (bool)1;
	}
}
// System.Void CaptureAndSave/<SaveToAlbum>c__Iterator0::Dispose()
extern "C"  void U3CSaveToAlbumU3Ec__Iterator0_Dispose_m3506722496 (U3CSaveToAlbumU3Ec__Iterator0_t371193057 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_10((-1));
		return;
	}
}
// System.Void CaptureAndSave/<SaveToAlbum>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CSaveToAlbumU3Ec__Iterator0_Reset_m2262473200_MetadataUsageId;
extern "C"  void U3CSaveToAlbumU3Ec__Iterator0_Reset_m2262473200 (U3CSaveToAlbumU3Ec__Iterator0_t371193057 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CSaveToAlbumU3Ec__Iterator0_Reset_m2262473200_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void CaptureAndSaveEventListener::.ctor()
extern "C"  void CaptureAndSaveEventListener__ctor_m3649768002 (CaptureAndSaveEventListener_t2941400608 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CaptureAndSaveEventListener::add_onError(CaptureAndSaveEventListener/OnError)
extern "C"  void CaptureAndSaveEventListener_add_onError_m3586673325 (Il2CppObject * __this /* static, unused */, OnError_t1516540506 * ___value0, const MethodInfo* method)
{
	{
		OnError_t1516540506 * L_0 = ___value0;
		CaptureAndSaveEventListener_AddHandler_onError_m928399587(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CaptureAndSaveEventListener::remove_onError(CaptureAndSaveEventListener/OnError)
extern "C"  void CaptureAndSaveEventListener_remove_onError_m2299742024 (Il2CppObject * __this /* static, unused */, OnError_t1516540506 * ___value0, const MethodInfo* method)
{
	{
		OnError_t1516540506 * L_0 = ___value0;
		CaptureAndSaveEventListener_RemoveHandler_onError_m926266408(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CaptureAndSaveEventListener::add_onSuccess(CaptureAndSaveEventListener/OnSuccess)
extern "C"  void CaptureAndSaveEventListener_add_onSuccess_m31473463 (Il2CppObject * __this /* static, unused */, OnSuccess_t1013330901 * ___value0, const MethodInfo* method)
{
	{
		OnSuccess_t1013330901 * L_0 = ___value0;
		CaptureAndSaveEventListener_AddHandler_onSuccess_m1652304493(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CaptureAndSaveEventListener::remove_onSuccess(CaptureAndSaveEventListener/OnSuccess)
extern "C"  void CaptureAndSaveEventListener_remove_onSuccess_m3889526354 (Il2CppObject * __this /* static, unused */, OnSuccess_t1013330901 * ___value0, const MethodInfo* method)
{
	{
		OnSuccess_t1013330901 * L_0 = ___value0;
		CaptureAndSaveEventListener_RemoveHandler_onSuccess_m3006690098(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CaptureAndSaveEventListener::add_onScreenShot(CaptureAndSaveEventListener/OnScreenShot)
extern "C"  void CaptureAndSaveEventListener_add_onScreenShot_m407281169 (Il2CppObject * __this /* static, unused */, OnScreenShot_t3427048564 * ___value0, const MethodInfo* method)
{
	{
		OnScreenShot_t3427048564 * L_0 = ___value0;
		CaptureAndSaveEventListener_AddHandler_onScreenShot_m3650537671(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CaptureAndSaveEventListener::remove_onScreenShot(CaptureAndSaveEventListener/OnScreenShot)
extern "C"  void CaptureAndSaveEventListener_remove_onScreenShot_m4252947692 (Il2CppObject * __this /* static, unused */, OnScreenShot_t3427048564 * ___value0, const MethodInfo* method)
{
	{
		OnScreenShot_t3427048564 * L_0 = ___value0;
		CaptureAndSaveEventListener_RemoveHandler_onScreenShot_m2896603596(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CaptureAndSaveEventListener::AddHandler_onError(CaptureAndSaveEventListener/OnError)
extern Il2CppClass* CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var;
extern Il2CppClass* OnError_t1516540506_il2cpp_TypeInfo_var;
extern const uint32_t CaptureAndSaveEventListener_AddHandler_onError_m928399587_MetadataUsageId;
extern "C"  void CaptureAndSaveEventListener_AddHandler_onError_m928399587 (Il2CppObject * __this /* static, unused */, OnError_t1516540506 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSaveEventListener_AddHandler_onError_m928399587_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		OnError_t1516540506 * L_0 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		OnError_t1516540506 * L_1 = ___value0;
		Delegate_t3310234105 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->set_onErrorInvoker_0(((OnError_t1516540506 *)CastclassSealed(L_2, OnError_t1516540506_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void CaptureAndSaveEventListener::RemoveHandler_onError(CaptureAndSaveEventListener/OnError)
extern Il2CppClass* CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var;
extern Il2CppClass* OnError_t1516540506_il2cpp_TypeInfo_var;
extern const uint32_t CaptureAndSaveEventListener_RemoveHandler_onError_m926266408_MetadataUsageId;
extern "C"  void CaptureAndSaveEventListener_RemoveHandler_onError_m926266408 (Il2CppObject * __this /* static, unused */, OnError_t1516540506 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSaveEventListener_RemoveHandler_onError_m926266408_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		OnError_t1516540506 * L_0 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		OnError_t1516540506 * L_1 = ___value0;
		Delegate_t3310234105 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->set_onErrorInvoker_0(((OnError_t1516540506 *)CastclassSealed(L_2, OnError_t1516540506_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void CaptureAndSaveEventListener::AddHandler_onSuccess(CaptureAndSaveEventListener/OnSuccess)
extern Il2CppClass* CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var;
extern Il2CppClass* OnSuccess_t1013330901_il2cpp_TypeInfo_var;
extern const uint32_t CaptureAndSaveEventListener_AddHandler_onSuccess_m1652304493_MetadataUsageId;
extern "C"  void CaptureAndSaveEventListener_AddHandler_onSuccess_m1652304493 (Il2CppObject * __this /* static, unused */, OnSuccess_t1013330901 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSaveEventListener_AddHandler_onSuccess_m1652304493_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		OnSuccess_t1013330901 * L_0 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onSuccessInvoker_1();
		OnSuccess_t1013330901 * L_1 = ___value0;
		Delegate_t3310234105 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->set_onSuccessInvoker_1(((OnSuccess_t1013330901 *)CastclassSealed(L_2, OnSuccess_t1013330901_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void CaptureAndSaveEventListener::RemoveHandler_onSuccess(CaptureAndSaveEventListener/OnSuccess)
extern Il2CppClass* CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var;
extern Il2CppClass* OnSuccess_t1013330901_il2cpp_TypeInfo_var;
extern const uint32_t CaptureAndSaveEventListener_RemoveHandler_onSuccess_m3006690098_MetadataUsageId;
extern "C"  void CaptureAndSaveEventListener_RemoveHandler_onSuccess_m3006690098 (Il2CppObject * __this /* static, unused */, OnSuccess_t1013330901 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSaveEventListener_RemoveHandler_onSuccess_m3006690098_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		OnSuccess_t1013330901 * L_0 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onSuccessInvoker_1();
		OnSuccess_t1013330901 * L_1 = ___value0;
		Delegate_t3310234105 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->set_onSuccessInvoker_1(((OnSuccess_t1013330901 *)CastclassSealed(L_2, OnSuccess_t1013330901_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void CaptureAndSaveEventListener::AddHandler_onScreenShot(CaptureAndSaveEventListener/OnScreenShot)
extern Il2CppClass* CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var;
extern Il2CppClass* OnScreenShot_t3427048564_il2cpp_TypeInfo_var;
extern const uint32_t CaptureAndSaveEventListener_AddHandler_onScreenShot_m3650537671_MetadataUsageId;
extern "C"  void CaptureAndSaveEventListener_AddHandler_onScreenShot_m3650537671 (Il2CppObject * __this /* static, unused */, OnScreenShot_t3427048564 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSaveEventListener_AddHandler_onScreenShot_m3650537671_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		OnScreenShot_t3427048564 * L_0 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onScreenShotInvoker_2();
		OnScreenShot_t3427048564 * L_1 = ___value0;
		Delegate_t3310234105 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->set_onScreenShotInvoker_2(((OnScreenShot_t3427048564 *)CastclassSealed(L_2, OnScreenShot_t3427048564_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void CaptureAndSaveEventListener::RemoveHandler_onScreenShot(CaptureAndSaveEventListener/OnScreenShot)
extern Il2CppClass* CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var;
extern Il2CppClass* OnScreenShot_t3427048564_il2cpp_TypeInfo_var;
extern const uint32_t CaptureAndSaveEventListener_RemoveHandler_onScreenShot_m2896603596_MetadataUsageId;
extern "C"  void CaptureAndSaveEventListener_RemoveHandler_onScreenShot_m2896603596 (Il2CppObject * __this /* static, unused */, OnScreenShot_t3427048564 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CaptureAndSaveEventListener_RemoveHandler_onScreenShot_m2896603596_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		OnScreenShot_t3427048564 * L_0 = ((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->get_onScreenShotInvoker_2();
		OnScreenShot_t3427048564 * L_1 = ___value0;
		Delegate_t3310234105 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((CaptureAndSaveEventListener_t2941400608_StaticFields*)CaptureAndSaveEventListener_t2941400608_il2cpp_TypeInfo_var->static_fields)->set_onScreenShotInvoker_2(((OnScreenShot_t3427048564 *)CastclassSealed(L_2, OnScreenShot_t3427048564_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void CaptureAndSaveEventListener/OnError::.ctor(System.Object,System.IntPtr)
extern "C"  void OnError__ctor_m3869093594 (OnError_t1516540506 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void CaptureAndSaveEventListener/OnError::Invoke(System.String)
extern "C"  void OnError_Invoke_m3553004494 (OnError_t1516540506 * __this, String_t* ___err0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnError_Invoke_m3553004494((OnError_t1516540506 *)__this->get_prev_9(),___err0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___err0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___err0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___err0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___err0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___err0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_OnError_t1516540506 (OnError_t1516540506 * __this, String_t* ___err0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___err0' to native representation
	char* ____err0_marshaled = NULL;
	____err0_marshaled = il2cpp_codegen_marshal_string(___err0);

	// Native function invocation
	il2cppPInvokeFunc(____err0_marshaled);

	// Marshaling cleanup of parameter '___err0' native representation
	il2cpp_codegen_marshal_free(____err0_marshaled);
	____err0_marshaled = NULL;

}
// System.IAsyncResult CaptureAndSaveEventListener/OnError::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnError_BeginInvoke_m829483483 (OnError_t1516540506 * __this, String_t* ___err0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___err0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void CaptureAndSaveEventListener/OnError::EndInvoke(System.IAsyncResult)
extern "C"  void OnError_EndInvoke_m1920900586 (OnError_t1516540506 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void CaptureAndSaveEventListener/OnScreenShot::.ctor(System.Object,System.IntPtr)
extern "C"  void OnScreenShot__ctor_m954799378 (OnScreenShot_t3427048564 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void CaptureAndSaveEventListener/OnScreenShot::Invoke(UnityEngine.Texture2D)
extern "C"  void OnScreenShot_Invoke_m3599849904 (OnScreenShot_t3427048564 * __this, Texture2D_t3884108195 * ___tex2D0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnScreenShot_Invoke_m3599849904((OnScreenShot_t3427048564 *)__this->get_prev_9(),___tex2D0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Texture2D_t3884108195 * ___tex2D0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___tex2D0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Texture2D_t3884108195 * ___tex2D0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___tex2D0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___tex2D0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult CaptureAndSaveEventListener/OnScreenShot::BeginInvoke(UnityEngine.Texture2D,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnScreenShot_BeginInvoke_m3553517505 (OnScreenShot_t3427048564 * __this, Texture2D_t3884108195 * ___tex2D0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___tex2D0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void CaptureAndSaveEventListener/OnScreenShot::EndInvoke(System.IAsyncResult)
extern "C"  void OnScreenShot_EndInvoke_m3408257058 (OnScreenShot_t3427048564 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void CaptureAndSaveEventListener/OnSuccess::.ctor(System.Object,System.IntPtr)
extern "C"  void OnSuccess__ctor_m1594524309 (OnSuccess_t1013330901 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void CaptureAndSaveEventListener/OnSuccess::Invoke(System.String)
extern "C"  void OnSuccess_Invoke_m629490739 (OnSuccess_t1013330901 * __this, String_t* ___msg0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnSuccess_Invoke_m629490739((OnSuccess_t1013330901 *)__this->get_prev_9(),___msg0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___msg0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___msg0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___msg0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___msg0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___msg0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_OnSuccess_t1013330901 (OnSuccess_t1013330901 * __this, String_t* ___msg0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___msg0' to native representation
	char* ____msg0_marshaled = NULL;
	____msg0_marshaled = il2cpp_codegen_marshal_string(___msg0);

	// Native function invocation
	il2cppPInvokeFunc(____msg0_marshaled);

	// Marshaling cleanup of parameter '___msg0' native representation
	il2cpp_codegen_marshal_free(____msg0_marshaled);
	____msg0_marshaled = NULL;

}
// System.IAsyncResult CaptureAndSaveEventListener/OnSuccess::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnSuccess_BeginInvoke_m211601472 (OnSuccess_t1013330901 * __this, String_t* ___msg0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___msg0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void CaptureAndSaveEventListener/OnSuccess::EndInvoke(System.IAsyncResult)
extern "C"  void OnSuccess_EndInvoke_m1623754533 (OnSuccess_t1013330901 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
