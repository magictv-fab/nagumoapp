﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_InApp_DirectorActions480721875.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InApp.DirectorMeta
struct  DirectorMeta_t2276429143  : public Il2CppObject
{
public:
	// System.Int32 InApp.DirectorMeta::BundleID
	int32_t ___BundleID_0;
	// InApp.DirectorActions InApp.DirectorMeta::Action
	int32_t ___Action_1;
	// System.Single InApp.DirectorMeta::Delay
	float ___Delay_2;

public:
	inline static int32_t get_offset_of_BundleID_0() { return static_cast<int32_t>(offsetof(DirectorMeta_t2276429143, ___BundleID_0)); }
	inline int32_t get_BundleID_0() const { return ___BundleID_0; }
	inline int32_t* get_address_of_BundleID_0() { return &___BundleID_0; }
	inline void set_BundleID_0(int32_t value)
	{
		___BundleID_0 = value;
	}

	inline static int32_t get_offset_of_Action_1() { return static_cast<int32_t>(offsetof(DirectorMeta_t2276429143, ___Action_1)); }
	inline int32_t get_Action_1() const { return ___Action_1; }
	inline int32_t* get_address_of_Action_1() { return &___Action_1; }
	inline void set_Action_1(int32_t value)
	{
		___Action_1 = value;
	}

	inline static int32_t get_offset_of_Delay_2() { return static_cast<int32_t>(offsetof(DirectorMeta_t2276429143, ___Delay_2)); }
	inline float get_Delay_2() const { return ___Delay_2; }
	inline float* get_address_of_Delay_2() { return &___Delay_2; }
	inline void set_Delay_2(float value)
	{
		___Delay_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
