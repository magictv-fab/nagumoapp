﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.Aztec.Internal.State
struct State_t3065423635;
// ZXing.Aztec.Internal.Token
struct Token_t3066207355;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Aztec.Internal.State
struct  State_t3065423635  : public Il2CppObject
{
public:
	// System.Int32 ZXing.Aztec.Internal.State::mode
	int32_t ___mode_1;
	// ZXing.Aztec.Internal.Token ZXing.Aztec.Internal.State::token
	Token_t3066207355 * ___token_2;
	// System.Int32 ZXing.Aztec.Internal.State::binaryShiftByteCount
	int32_t ___binaryShiftByteCount_3;
	// System.Int32 ZXing.Aztec.Internal.State::bitCount
	int32_t ___bitCount_4;

public:
	inline static int32_t get_offset_of_mode_1() { return static_cast<int32_t>(offsetof(State_t3065423635, ___mode_1)); }
	inline int32_t get_mode_1() const { return ___mode_1; }
	inline int32_t* get_address_of_mode_1() { return &___mode_1; }
	inline void set_mode_1(int32_t value)
	{
		___mode_1 = value;
	}

	inline static int32_t get_offset_of_token_2() { return static_cast<int32_t>(offsetof(State_t3065423635, ___token_2)); }
	inline Token_t3066207355 * get_token_2() const { return ___token_2; }
	inline Token_t3066207355 ** get_address_of_token_2() { return &___token_2; }
	inline void set_token_2(Token_t3066207355 * value)
	{
		___token_2 = value;
		Il2CppCodeGenWriteBarrier(&___token_2, value);
	}

	inline static int32_t get_offset_of_binaryShiftByteCount_3() { return static_cast<int32_t>(offsetof(State_t3065423635, ___binaryShiftByteCount_3)); }
	inline int32_t get_binaryShiftByteCount_3() const { return ___binaryShiftByteCount_3; }
	inline int32_t* get_address_of_binaryShiftByteCount_3() { return &___binaryShiftByteCount_3; }
	inline void set_binaryShiftByteCount_3(int32_t value)
	{
		___binaryShiftByteCount_3 = value;
	}

	inline static int32_t get_offset_of_bitCount_4() { return static_cast<int32_t>(offsetof(State_t3065423635, ___bitCount_4)); }
	inline int32_t get_bitCount_4() const { return ___bitCount_4; }
	inline int32_t* get_address_of_bitCount_4() { return &___bitCount_4; }
	inline void set_bitCount_4(int32_t value)
	{
		___bitCount_4 = value;
	}
};

struct State_t3065423635_StaticFields
{
public:
	// ZXing.Aztec.Internal.State ZXing.Aztec.Internal.State::INITIAL_STATE
	State_t3065423635 * ___INITIAL_STATE_0;

public:
	inline static int32_t get_offset_of_INITIAL_STATE_0() { return static_cast<int32_t>(offsetof(State_t3065423635_StaticFields, ___INITIAL_STATE_0)); }
	inline State_t3065423635 * get_INITIAL_STATE_0() const { return ___INITIAL_STATE_0; }
	inline State_t3065423635 ** get_address_of_INITIAL_STATE_0() { return &___INITIAL_STATE_0; }
	inline void set_INITIAL_STATE_0(State_t3065423635 * value)
	{
		___INITIAL_STATE_0 = value;
		Il2CppCodeGenWriteBarrier(&___INITIAL_STATE_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
