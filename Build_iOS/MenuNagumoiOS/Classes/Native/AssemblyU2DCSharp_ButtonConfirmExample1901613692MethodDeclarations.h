﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ButtonConfirmExample
struct ButtonConfirmExample_t1901613692;

#include "codegen/il2cpp-codegen.h"

// System.Void ButtonConfirmExample::.ctor()
extern "C"  void ButtonConfirmExample__ctor_m2191463327 (ButtonConfirmExample_t1901613692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonConfirmExample::Click()
extern "C"  void ButtonConfirmExample_Click_m3896310853 (ButtonConfirmExample_t1901613692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonConfirmExample::OkClick()
extern "C"  void ButtonConfirmExample_OkClick_m2373023689 (ButtonConfirmExample_t1901613692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonConfirmExample::CancelClick()
extern "C"  void ButtonConfirmExample_CancelClick_m2135706475 (ButtonConfirmExample_t1901613692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
