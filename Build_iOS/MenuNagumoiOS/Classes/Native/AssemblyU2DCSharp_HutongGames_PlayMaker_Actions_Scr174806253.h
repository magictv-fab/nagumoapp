﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ScreenToWorldPoint
struct  ScreenToWorldPoint_t174806253  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.ScreenToWorldPoint::screenVector
	FsmVector3_t533912882 * ___screenVector_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ScreenToWorldPoint::screenX
	FsmFloat_t2134102846 * ___screenX_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ScreenToWorldPoint::screenY
	FsmFloat_t2134102846 * ___screenY_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ScreenToWorldPoint::screenZ
	FsmFloat_t2134102846 * ___screenZ_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ScreenToWorldPoint::normalized
	FsmBool_t1075959796 * ___normalized_13;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.ScreenToWorldPoint::storeWorldVector
	FsmVector3_t533912882 * ___storeWorldVector_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ScreenToWorldPoint::storeWorldX
	FsmFloat_t2134102846 * ___storeWorldX_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ScreenToWorldPoint::storeWorldY
	FsmFloat_t2134102846 * ___storeWorldY_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ScreenToWorldPoint::storeWorldZ
	FsmFloat_t2134102846 * ___storeWorldZ_17;
	// System.Boolean HutongGames.PlayMaker.Actions.ScreenToWorldPoint::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_screenVector_9() { return static_cast<int32_t>(offsetof(ScreenToWorldPoint_t174806253, ___screenVector_9)); }
	inline FsmVector3_t533912882 * get_screenVector_9() const { return ___screenVector_9; }
	inline FsmVector3_t533912882 ** get_address_of_screenVector_9() { return &___screenVector_9; }
	inline void set_screenVector_9(FsmVector3_t533912882 * value)
	{
		___screenVector_9 = value;
		Il2CppCodeGenWriteBarrier(&___screenVector_9, value);
	}

	inline static int32_t get_offset_of_screenX_10() { return static_cast<int32_t>(offsetof(ScreenToWorldPoint_t174806253, ___screenX_10)); }
	inline FsmFloat_t2134102846 * get_screenX_10() const { return ___screenX_10; }
	inline FsmFloat_t2134102846 ** get_address_of_screenX_10() { return &___screenX_10; }
	inline void set_screenX_10(FsmFloat_t2134102846 * value)
	{
		___screenX_10 = value;
		Il2CppCodeGenWriteBarrier(&___screenX_10, value);
	}

	inline static int32_t get_offset_of_screenY_11() { return static_cast<int32_t>(offsetof(ScreenToWorldPoint_t174806253, ___screenY_11)); }
	inline FsmFloat_t2134102846 * get_screenY_11() const { return ___screenY_11; }
	inline FsmFloat_t2134102846 ** get_address_of_screenY_11() { return &___screenY_11; }
	inline void set_screenY_11(FsmFloat_t2134102846 * value)
	{
		___screenY_11 = value;
		Il2CppCodeGenWriteBarrier(&___screenY_11, value);
	}

	inline static int32_t get_offset_of_screenZ_12() { return static_cast<int32_t>(offsetof(ScreenToWorldPoint_t174806253, ___screenZ_12)); }
	inline FsmFloat_t2134102846 * get_screenZ_12() const { return ___screenZ_12; }
	inline FsmFloat_t2134102846 ** get_address_of_screenZ_12() { return &___screenZ_12; }
	inline void set_screenZ_12(FsmFloat_t2134102846 * value)
	{
		___screenZ_12 = value;
		Il2CppCodeGenWriteBarrier(&___screenZ_12, value);
	}

	inline static int32_t get_offset_of_normalized_13() { return static_cast<int32_t>(offsetof(ScreenToWorldPoint_t174806253, ___normalized_13)); }
	inline FsmBool_t1075959796 * get_normalized_13() const { return ___normalized_13; }
	inline FsmBool_t1075959796 ** get_address_of_normalized_13() { return &___normalized_13; }
	inline void set_normalized_13(FsmBool_t1075959796 * value)
	{
		___normalized_13 = value;
		Il2CppCodeGenWriteBarrier(&___normalized_13, value);
	}

	inline static int32_t get_offset_of_storeWorldVector_14() { return static_cast<int32_t>(offsetof(ScreenToWorldPoint_t174806253, ___storeWorldVector_14)); }
	inline FsmVector3_t533912882 * get_storeWorldVector_14() const { return ___storeWorldVector_14; }
	inline FsmVector3_t533912882 ** get_address_of_storeWorldVector_14() { return &___storeWorldVector_14; }
	inline void set_storeWorldVector_14(FsmVector3_t533912882 * value)
	{
		___storeWorldVector_14 = value;
		Il2CppCodeGenWriteBarrier(&___storeWorldVector_14, value);
	}

	inline static int32_t get_offset_of_storeWorldX_15() { return static_cast<int32_t>(offsetof(ScreenToWorldPoint_t174806253, ___storeWorldX_15)); }
	inline FsmFloat_t2134102846 * get_storeWorldX_15() const { return ___storeWorldX_15; }
	inline FsmFloat_t2134102846 ** get_address_of_storeWorldX_15() { return &___storeWorldX_15; }
	inline void set_storeWorldX_15(FsmFloat_t2134102846 * value)
	{
		___storeWorldX_15 = value;
		Il2CppCodeGenWriteBarrier(&___storeWorldX_15, value);
	}

	inline static int32_t get_offset_of_storeWorldY_16() { return static_cast<int32_t>(offsetof(ScreenToWorldPoint_t174806253, ___storeWorldY_16)); }
	inline FsmFloat_t2134102846 * get_storeWorldY_16() const { return ___storeWorldY_16; }
	inline FsmFloat_t2134102846 ** get_address_of_storeWorldY_16() { return &___storeWorldY_16; }
	inline void set_storeWorldY_16(FsmFloat_t2134102846 * value)
	{
		___storeWorldY_16 = value;
		Il2CppCodeGenWriteBarrier(&___storeWorldY_16, value);
	}

	inline static int32_t get_offset_of_storeWorldZ_17() { return static_cast<int32_t>(offsetof(ScreenToWorldPoint_t174806253, ___storeWorldZ_17)); }
	inline FsmFloat_t2134102846 * get_storeWorldZ_17() const { return ___storeWorldZ_17; }
	inline FsmFloat_t2134102846 ** get_address_of_storeWorldZ_17() { return &___storeWorldZ_17; }
	inline void set_storeWorldZ_17(FsmFloat_t2134102846 * value)
	{
		___storeWorldZ_17 = value;
		Il2CppCodeGenWriteBarrier(&___storeWorldZ_17, value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(ScreenToWorldPoint_t174806253, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
