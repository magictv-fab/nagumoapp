﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3055477407.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUIVerticalSlider
struct  GUIVerticalSlider_t4042007136  : public GUIAction_t3055477407
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUIVerticalSlider::floatVariable
	FsmFloat_t2134102846 * ___floatVariable_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUIVerticalSlider::topValue
	FsmFloat_t2134102846 * ___topValue_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUIVerticalSlider::bottomValue
	FsmFloat_t2134102846 * ___bottomValue_18;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUIVerticalSlider::sliderStyle
	FsmString_t952858651 * ___sliderStyle_19;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUIVerticalSlider::thumbStyle
	FsmString_t952858651 * ___thumbStyle_20;

public:
	inline static int32_t get_offset_of_floatVariable_16() { return static_cast<int32_t>(offsetof(GUIVerticalSlider_t4042007136, ___floatVariable_16)); }
	inline FsmFloat_t2134102846 * get_floatVariable_16() const { return ___floatVariable_16; }
	inline FsmFloat_t2134102846 ** get_address_of_floatVariable_16() { return &___floatVariable_16; }
	inline void set_floatVariable_16(FsmFloat_t2134102846 * value)
	{
		___floatVariable_16 = value;
		Il2CppCodeGenWriteBarrier(&___floatVariable_16, value);
	}

	inline static int32_t get_offset_of_topValue_17() { return static_cast<int32_t>(offsetof(GUIVerticalSlider_t4042007136, ___topValue_17)); }
	inline FsmFloat_t2134102846 * get_topValue_17() const { return ___topValue_17; }
	inline FsmFloat_t2134102846 ** get_address_of_topValue_17() { return &___topValue_17; }
	inline void set_topValue_17(FsmFloat_t2134102846 * value)
	{
		___topValue_17 = value;
		Il2CppCodeGenWriteBarrier(&___topValue_17, value);
	}

	inline static int32_t get_offset_of_bottomValue_18() { return static_cast<int32_t>(offsetof(GUIVerticalSlider_t4042007136, ___bottomValue_18)); }
	inline FsmFloat_t2134102846 * get_bottomValue_18() const { return ___bottomValue_18; }
	inline FsmFloat_t2134102846 ** get_address_of_bottomValue_18() { return &___bottomValue_18; }
	inline void set_bottomValue_18(FsmFloat_t2134102846 * value)
	{
		___bottomValue_18 = value;
		Il2CppCodeGenWriteBarrier(&___bottomValue_18, value);
	}

	inline static int32_t get_offset_of_sliderStyle_19() { return static_cast<int32_t>(offsetof(GUIVerticalSlider_t4042007136, ___sliderStyle_19)); }
	inline FsmString_t952858651 * get_sliderStyle_19() const { return ___sliderStyle_19; }
	inline FsmString_t952858651 ** get_address_of_sliderStyle_19() { return &___sliderStyle_19; }
	inline void set_sliderStyle_19(FsmString_t952858651 * value)
	{
		___sliderStyle_19 = value;
		Il2CppCodeGenWriteBarrier(&___sliderStyle_19, value);
	}

	inline static int32_t get_offset_of_thumbStyle_20() { return static_cast<int32_t>(offsetof(GUIVerticalSlider_t4042007136, ___thumbStyle_20)); }
	inline FsmString_t952858651 * get_thumbStyle_20() const { return ___thumbStyle_20; }
	inline FsmString_t952858651 ** get_address_of_thumbStyle_20() { return &___thumbStyle_20; }
	inline void set_thumbStyle_20(FsmString_t952858651 * value)
	{
		___thumbStyle_20 = value;
		Il2CppCodeGenWriteBarrier(&___thumbStyle_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
