﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector3RotateTowards
struct  Vector3RotateTowards_t705544633  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3RotateTowards::currentDirection
	FsmVector3_t533912882 * ___currentDirection_9;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3RotateTowards::targetDirection
	FsmVector3_t533912882 * ___targetDirection_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector3RotateTowards::rotateSpeed
	FsmFloat_t2134102846 * ___rotateSpeed_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector3RotateTowards::maxMagnitude
	FsmFloat_t2134102846 * ___maxMagnitude_12;

public:
	inline static int32_t get_offset_of_currentDirection_9() { return static_cast<int32_t>(offsetof(Vector3RotateTowards_t705544633, ___currentDirection_9)); }
	inline FsmVector3_t533912882 * get_currentDirection_9() const { return ___currentDirection_9; }
	inline FsmVector3_t533912882 ** get_address_of_currentDirection_9() { return &___currentDirection_9; }
	inline void set_currentDirection_9(FsmVector3_t533912882 * value)
	{
		___currentDirection_9 = value;
		Il2CppCodeGenWriteBarrier(&___currentDirection_9, value);
	}

	inline static int32_t get_offset_of_targetDirection_10() { return static_cast<int32_t>(offsetof(Vector3RotateTowards_t705544633, ___targetDirection_10)); }
	inline FsmVector3_t533912882 * get_targetDirection_10() const { return ___targetDirection_10; }
	inline FsmVector3_t533912882 ** get_address_of_targetDirection_10() { return &___targetDirection_10; }
	inline void set_targetDirection_10(FsmVector3_t533912882 * value)
	{
		___targetDirection_10 = value;
		Il2CppCodeGenWriteBarrier(&___targetDirection_10, value);
	}

	inline static int32_t get_offset_of_rotateSpeed_11() { return static_cast<int32_t>(offsetof(Vector3RotateTowards_t705544633, ___rotateSpeed_11)); }
	inline FsmFloat_t2134102846 * get_rotateSpeed_11() const { return ___rotateSpeed_11; }
	inline FsmFloat_t2134102846 ** get_address_of_rotateSpeed_11() { return &___rotateSpeed_11; }
	inline void set_rotateSpeed_11(FsmFloat_t2134102846 * value)
	{
		___rotateSpeed_11 = value;
		Il2CppCodeGenWriteBarrier(&___rotateSpeed_11, value);
	}

	inline static int32_t get_offset_of_maxMagnitude_12() { return static_cast<int32_t>(offsetof(Vector3RotateTowards_t705544633, ___maxMagnitude_12)); }
	inline FsmFloat_t2134102846 * get_maxMagnitude_12() const { return ___maxMagnitude_12; }
	inline FsmFloat_t2134102846 ** get_address_of_maxMagnitude_12() { return &___maxMagnitude_12; }
	inline void set_maxMagnitude_12(FsmFloat_t2134102846 * value)
	{
		___maxMagnitude_12 = value;
		Il2CppCodeGenWriteBarrier(&___maxMagnitude_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
