﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4084788485.h"
#include "mscorlib_System_Array1146569071.h"
#include "QRCode_ZXing_Aztec_Internal_Decoder_Table1007478513.h"

// System.Void System.Array/InternalEnumerator`1<ZXing.Aztec.Internal.Decoder/Table>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3470786498_gshared (InternalEnumerator_1_t4084788485 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3470786498(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4084788485 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3470786498_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<ZXing.Aztec.Internal.Decoder/Table>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m406604446_gshared (InternalEnumerator_1_t4084788485 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m406604446(__this, method) ((  void (*) (InternalEnumerator_1_t4084788485 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m406604446_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<ZXing.Aztec.Internal.Decoder/Table>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3432473482_gshared (InternalEnumerator_1_t4084788485 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3432473482(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4084788485 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3432473482_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<ZXing.Aztec.Internal.Decoder/Table>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m591655961_gshared (InternalEnumerator_1_t4084788485 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m591655961(__this, method) ((  void (*) (InternalEnumerator_1_t4084788485 *, const MethodInfo*))InternalEnumerator_1_Dispose_m591655961_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<ZXing.Aztec.Internal.Decoder/Table>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4097703434_gshared (InternalEnumerator_1_t4084788485 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m4097703434(__this, method) ((  bool (*) (InternalEnumerator_1_t4084788485 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m4097703434_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<ZXing.Aztec.Internal.Decoder/Table>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m3931497481_gshared (InternalEnumerator_1_t4084788485 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3931497481(__this, method) ((  int32_t (*) (InternalEnumerator_1_t4084788485 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3931497481_gshared)(__this, method)
