﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetProceduralColor
struct SetProceduralColor_t3246619020;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetProceduralColor::.ctor()
extern "C"  void SetProceduralColor__ctor_m1187954778 (SetProceduralColor_t3246619020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProceduralColor::Reset()
extern "C"  void SetProceduralColor_Reset_m3129355015 (SetProceduralColor_t3246619020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProceduralColor::OnEnter()
extern "C"  void SetProceduralColor_OnEnter_m2849936817 (SetProceduralColor_t3246619020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProceduralColor::OnUpdate()
extern "C"  void SetProceduralColor_OnUpdate_m1582254770 (SetProceduralColor_t3246619020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProceduralColor::DoSetProceduralFloat()
extern "C"  void SetProceduralColor_DoSetProceduralFloat_m1698426290 (SetProceduralColor_t3246619020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
