﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InfiniteHorizontalScroll
struct InfiniteHorizontalScroll_t3122519653;
// UnityEngine.RectTransform
struct RectTransform_t972643934;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RectTransform972643934.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void InfiniteHorizontalScroll::.ctor()
extern "C"  void InfiniteHorizontalScroll__ctor_m3906408918 (InfiniteHorizontalScroll_t3122519653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single InfiniteHorizontalScroll::GetSize(UnityEngine.RectTransform)
extern "C"  float InfiniteHorizontalScroll_GetSize_m594541698 (InfiniteHorizontalScroll_t3122519653 * __this, RectTransform_t972643934 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single InfiniteHorizontalScroll::GetDimension(UnityEngine.Vector2)
extern "C"  float InfiniteHorizontalScroll_GetDimension_m4023819344 (InfiniteHorizontalScroll_t3122519653 * __this, Vector2_t4282066565  ___vector0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 InfiniteHorizontalScroll::GetVector(System.Single)
extern "C"  Vector2_t4282066565  InfiniteHorizontalScroll_GetVector_m3756221433 (InfiniteHorizontalScroll_t3122519653 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single InfiniteHorizontalScroll::GetPos(UnityEngine.RectTransform)
extern "C"  float InfiniteHorizontalScroll_GetPos_m2396392553 (InfiniteHorizontalScroll_t3122519653 * __this, RectTransform_t972643934 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 InfiniteHorizontalScroll::OneOrMinusOne()
extern "C"  int32_t InfiniteHorizontalScroll_OneOrMinusOne_m147503045 (InfiniteHorizontalScroll_t3122519653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
