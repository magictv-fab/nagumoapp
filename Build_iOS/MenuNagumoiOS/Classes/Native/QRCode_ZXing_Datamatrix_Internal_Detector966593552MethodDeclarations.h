﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Datamatrix.Internal.Detector
struct Detector_t966593552;
// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// ZXing.Common.DetectorResult
struct DetectorResult_t3846928147;
// ZXing.ResultPoint
struct ResultPoint_t1538592853;
// System.Collections.Generic.IDictionary`2<ZXing.ResultPoint,System.Int32>
struct IDictionary_2_t2988258903;
// ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions
struct ResultPointsAndTransitions_t1206419768;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitMatrix1058711404.h"
#include "QRCode_ZXing_ResultPoint1538592853.h"

// System.Void ZXing.Datamatrix.Internal.Detector::.ctor(ZXing.Common.BitMatrix)
extern "C"  void Detector__ctor_m4279347304 (Detector_t966593552 * __this, BitMatrix_t1058711404 * ___image0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.DetectorResult ZXing.Datamatrix.Internal.Detector::detect()
extern "C"  DetectorResult_t3846928147 * Detector_detect_m2493788321 (Detector_t966593552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.ResultPoint ZXing.Datamatrix.Internal.Detector::correctTopRightRectangular(ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,System.Int32,System.Int32)
extern "C"  ResultPoint_t1538592853 * Detector_correctTopRightRectangular_m346250900 (Detector_t966593552 * __this, ResultPoint_t1538592853 * ___bottomLeft0, ResultPoint_t1538592853 * ___bottomRight1, ResultPoint_t1538592853 * ___topLeft2, ResultPoint_t1538592853 * ___topRight3, int32_t ___dimensionTop4, int32_t ___dimensionRight5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.ResultPoint ZXing.Datamatrix.Internal.Detector::correctTopRight(ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,System.Int32)
extern "C"  ResultPoint_t1538592853 * Detector_correctTopRight_m135283879 (Detector_t966593552 * __this, ResultPoint_t1538592853 * ___bottomLeft0, ResultPoint_t1538592853 * ___bottomRight1, ResultPoint_t1538592853 * ___topLeft2, ResultPoint_t1538592853 * ___topRight3, int32_t ___dimension4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.Datamatrix.Internal.Detector::isValid(ZXing.ResultPoint)
extern "C"  bool Detector_isValid_m625231600 (Detector_t966593552 * __this, ResultPoint_t1538592853 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Internal.Detector::distance(ZXing.ResultPoint,ZXing.ResultPoint)
extern "C"  int32_t Detector_distance_m3025811522 (Il2CppObject * __this /* static, unused */, ResultPoint_t1538592853 * ___a0, ResultPoint_t1538592853 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Internal.Detector::increment(System.Collections.Generic.IDictionary`2<ZXing.ResultPoint,System.Int32>,ZXing.ResultPoint)
extern "C"  void Detector_increment_m84340603 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___table0, ResultPoint_t1538592853 * ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Common.BitMatrix ZXing.Datamatrix.Internal.Detector::sampleGrid(ZXing.Common.BitMatrix,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,System.Int32,System.Int32)
extern "C"  BitMatrix_t1058711404 * Detector_sampleGrid_m1722210198 (Il2CppObject * __this /* static, unused */, BitMatrix_t1058711404 * ___image0, ResultPoint_t1538592853 * ___topLeft1, ResultPoint_t1538592853 * ___bottomLeft2, ResultPoint_t1538592853 * ___bottomRight3, ResultPoint_t1538592853 * ___topRight4, int32_t ___dimensionX5, int32_t ___dimensionY6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Datamatrix.Internal.Detector/ResultPointsAndTransitions ZXing.Datamatrix.Internal.Detector::transitionsBetween(ZXing.ResultPoint,ZXing.ResultPoint)
extern "C"  ResultPointsAndTransitions_t1206419768 * Detector_transitionsBetween_m2174460032 (Detector_t966593552 * __this, ResultPoint_t1538592853 * ___from0, ResultPoint_t1538592853 * ___to1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
