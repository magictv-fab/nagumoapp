﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkGetNetworkDisconnectionInfos
struct NetworkGetNetworkDisconnectionInfos_t2164373997;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkGetNetworkDisconnectionInfos::.ctor()
extern "C"  void NetworkGetNetworkDisconnectionInfos__ctor_m4274312169 (NetworkGetNetworkDisconnectionInfos_t2164373997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetNetworkDisconnectionInfos::Reset()
extern "C"  void NetworkGetNetworkDisconnectionInfos_Reset_m1920745110 (NetworkGetNetworkDisconnectionInfos_t2164373997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetNetworkDisconnectionInfos::OnEnter()
extern "C"  void NetworkGetNetworkDisconnectionInfos_OnEnter_m1016988032 (NetworkGetNetworkDisconnectionInfos_t2164373997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetNetworkDisconnectionInfos::doGetNetworkDisconnectionInfo()
extern "C"  void NetworkGetNetworkDisconnectionInfos_doGetNetworkDisconnectionInfo_m2595342206 (NetworkGetNetworkDisconnectionInfos_t2164373997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
