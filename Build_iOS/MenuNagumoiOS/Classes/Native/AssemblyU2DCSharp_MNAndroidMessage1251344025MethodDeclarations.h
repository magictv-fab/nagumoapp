﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MNAndroidMessage
struct MNAndroidMessage_t1251344025;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MNAndroidMessage::.ctor()
extern "C"  void MNAndroidMessage__ctor_m2474738722 (MNAndroidMessage_t1251344025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MNAndroidMessage MNAndroidMessage::Create(System.String,System.String)
extern "C"  MNAndroidMessage_t1251344025 * MNAndroidMessage_Create_m2484642972 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MNAndroidMessage MNAndroidMessage::Create(System.String,System.String,System.String)
extern "C"  MNAndroidMessage_t1251344025 * MNAndroidMessage_Create_m3965160408 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___message1, String_t* ___ok2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNAndroidMessage::init()
extern "C"  void MNAndroidMessage_init_m670405074 (MNAndroidMessage_t1251344025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNAndroidMessage::onPopUpCallBack(System.String)
extern "C"  void MNAndroidMessage_onPopUpCallBack_m4158301296 (MNAndroidMessage_t1251344025 * __this, String_t* ___buttonIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
