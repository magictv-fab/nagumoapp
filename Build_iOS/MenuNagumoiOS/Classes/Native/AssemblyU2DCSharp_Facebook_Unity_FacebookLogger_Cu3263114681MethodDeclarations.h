﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.FacebookLogger/CustomLogger
struct CustomLogger_t3263114681;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void Facebook.Unity.FacebookLogger/CustomLogger::.ctor()
extern "C"  void CustomLogger__ctor_m773548802 (CustomLogger_t3263114681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookLogger/CustomLogger::Log(System.String)
extern "C"  void CustomLogger_Log_m831114078 (CustomLogger_t3263114681 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookLogger/CustomLogger::Info(System.String)
extern "C"  void CustomLogger_Info_m3290595794 (CustomLogger_t3263114681 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookLogger/CustomLogger::Warn(System.String)
extern "C"  void CustomLogger_Warn_m4000311546 (CustomLogger_t3263114681 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.FacebookLogger/CustomLogger::Error(System.String)
extern "C"  void CustomLogger_Error_m1988747226 (CustomLogger_t3263114681 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
