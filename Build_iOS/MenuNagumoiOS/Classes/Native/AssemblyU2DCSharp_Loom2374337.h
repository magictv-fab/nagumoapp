﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Loom
struct Loom_t2374337;
// System.Collections.Generic.List`1<System.Action>
struct List_1_t844452154;
// System.Collections.Generic.List`1<Loom/DelayedQueueItem>
struct List_1_t292984064;
// System.Func`2<Loom/DelayedQueueItem,System.Boolean>
struct Func_2_t341959283;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Loom
struct  Loom_t2374337  : public MonoBehaviour_t667441552
{
public:
	// System.Collections.Generic.List`1<System.Action> Loom::_actions
	List_1_t844452154 * ____actions_3;
	// System.Collections.Generic.List`1<Loom/DelayedQueueItem> Loom::_delayed
	List_1_t292984064 * ____delayed_4;
	// System.Collections.Generic.List`1<System.Action> Loom::toBeRun
	List_1_t844452154 * ___toBeRun_5;
	// System.Collections.Generic.List`1<Loom/DelayedQueueItem> Loom::toBeDelayed
	List_1_t292984064 * ___toBeDelayed_6;

public:
	inline static int32_t get_offset_of__actions_3() { return static_cast<int32_t>(offsetof(Loom_t2374337, ____actions_3)); }
	inline List_1_t844452154 * get__actions_3() const { return ____actions_3; }
	inline List_1_t844452154 ** get_address_of__actions_3() { return &____actions_3; }
	inline void set__actions_3(List_1_t844452154 * value)
	{
		____actions_3 = value;
		Il2CppCodeGenWriteBarrier(&____actions_3, value);
	}

	inline static int32_t get_offset_of__delayed_4() { return static_cast<int32_t>(offsetof(Loom_t2374337, ____delayed_4)); }
	inline List_1_t292984064 * get__delayed_4() const { return ____delayed_4; }
	inline List_1_t292984064 ** get_address_of__delayed_4() { return &____delayed_4; }
	inline void set__delayed_4(List_1_t292984064 * value)
	{
		____delayed_4 = value;
		Il2CppCodeGenWriteBarrier(&____delayed_4, value);
	}

	inline static int32_t get_offset_of_toBeRun_5() { return static_cast<int32_t>(offsetof(Loom_t2374337, ___toBeRun_5)); }
	inline List_1_t844452154 * get_toBeRun_5() const { return ___toBeRun_5; }
	inline List_1_t844452154 ** get_address_of_toBeRun_5() { return &___toBeRun_5; }
	inline void set_toBeRun_5(List_1_t844452154 * value)
	{
		___toBeRun_5 = value;
		Il2CppCodeGenWriteBarrier(&___toBeRun_5, value);
	}

	inline static int32_t get_offset_of_toBeDelayed_6() { return static_cast<int32_t>(offsetof(Loom_t2374337, ___toBeDelayed_6)); }
	inline List_1_t292984064 * get_toBeDelayed_6() const { return ___toBeDelayed_6; }
	inline List_1_t292984064 ** get_address_of_toBeDelayed_6() { return &___toBeDelayed_6; }
	inline void set_toBeDelayed_6(List_1_t292984064 * value)
	{
		___toBeDelayed_6 = value;
		Il2CppCodeGenWriteBarrier(&___toBeDelayed_6, value);
	}
};

struct Loom_t2374337_StaticFields
{
public:
	// Loom Loom::_current
	Loom_t2374337 * ____current_2;
	// System.Func`2<Loom/DelayedQueueItem,System.Boolean> Loom::<>f__am$cache5
	Func_2_t341959283 * ___U3CU3Ef__amU24cache5_7;

public:
	inline static int32_t get_offset_of__current_2() { return static_cast<int32_t>(offsetof(Loom_t2374337_StaticFields, ____current_2)); }
	inline Loom_t2374337 * get__current_2() const { return ____current_2; }
	inline Loom_t2374337 ** get_address_of__current_2() { return &____current_2; }
	inline void set__current_2(Loom_t2374337 * value)
	{
		____current_2 = value;
		Il2CppCodeGenWriteBarrier(&____current_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_7() { return static_cast<int32_t>(offsetof(Loom_t2374337_StaticFields, ___U3CU3Ef__amU24cache5_7)); }
	inline Func_2_t341959283 * get_U3CU3Ef__amU24cache5_7() const { return ___U3CU3Ef__amU24cache5_7; }
	inline Func_2_t341959283 ** get_address_of_U3CU3Ef__amU24cache5_7() { return &___U3CU3Ef__amU24cache5_7; }
	inline void set_U3CU3Ef__amU24cache5_7(Func_2_t341959283 * value)
	{
		___U3CU3Ef__amU24cache5_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
