﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PublishManager/<ILoadImgs>c__Iterator79
struct U3CILoadImgsU3Ec__Iterator79_t1709434345;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PublishManager/<ILoadImgs>c__Iterator79::.ctor()
extern "C"  void U3CILoadImgsU3Ec__Iterator79__ctor_m3155771298 (U3CILoadImgsU3Ec__Iterator79_t1709434345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PublishManager/<ILoadImgs>c__Iterator79::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CILoadImgsU3Ec__Iterator79_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3216087536 (U3CILoadImgsU3Ec__Iterator79_t1709434345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PublishManager/<ILoadImgs>c__Iterator79::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CILoadImgsU3Ec__Iterator79_System_Collections_IEnumerator_get_Current_m2578808196 (U3CILoadImgsU3Ec__Iterator79_t1709434345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PublishManager/<ILoadImgs>c__Iterator79::MoveNext()
extern "C"  bool U3CILoadImgsU3Ec__Iterator79_MoveNext_m933533458 (U3CILoadImgsU3Ec__Iterator79_t1709434345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PublishManager/<ILoadImgs>c__Iterator79::Dispose()
extern "C"  void U3CILoadImgsU3Ec__Iterator79_Dispose_m347589471 (U3CILoadImgsU3Ec__Iterator79_t1709434345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PublishManager/<ILoadImgs>c__Iterator79::Reset()
extern "C"  void U3CILoadImgsU3Ec__Iterator79_Reset_m802204239 (U3CILoadImgsU3Ec__Iterator79_t1709434345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
