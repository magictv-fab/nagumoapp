﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1619369899MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m2650624833(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t923104843 *, Dictionary_2_t3591312688 *, const MethodInfo*))KeyCollection__ctor_m4128360304_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m465518965(__this, ___item0, method) ((  void (*) (KeyCollection_t923104843 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3905989606_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m778125356(__this, method) ((  void (*) (KeyCollection_t923104843 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m888967901_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m436204921(__this, ___item0, method) ((  bool (*) (KeyCollection_t923104843 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1383295204_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m680358174(__this, ___item0, method) ((  bool (*) (KeyCollection_t923104843 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m596427593_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2541071742(__this, method) ((  Il2CppObject* (*) (KeyCollection_t923104843 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4173551769_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m627589790(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t923104843 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m4231009231_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3681848493(__this, method) ((  Il2CppObject * (*) (KeyCollection_t923104843 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m4128769802_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1167192730(__this, method) ((  bool (*) (KeyCollection_t923104843 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m787887941_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3754660812(__this, method) ((  bool (*) (KeyCollection_t923104843 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m412262967_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m2367627262(__this, method) ((  Il2CppObject * (*) (KeyCollection_t923104843 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3401403171_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m2554330614(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t923104843 *, EventNamesU5BU5D_t3350345955*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m1970859557_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::GetEnumerator()
#define KeyCollection_GetEnumerator_m2368034115(__this, method) ((  Enumerator_t4206248742  (*) (KeyCollection_t923104843 *, const MethodInfo*))KeyCollection_GetEnumerator_m3315239560_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff>::get_Count()
#define KeyCollection_get_Count_m2921568646(__this, method) ((  int32_t (*) (KeyCollection_t923104843 *, const MethodInfo*))KeyCollection_get_Count_m3602700541_gshared)(__this, method)
