﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARMVuforiaInitializationErrorHandler
struct ARMVuforiaInitializationErrorHandler_t811017192;

#include "codegen/il2cpp-codegen.h"
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUni432217960.h"

// System.Void ARMVuforiaInitializationErrorHandler::.ctor()
extern "C"  void ARMVuforiaInitializationErrorHandler__ctor_m2478828467 (ARMVuforiaInitializationErrorHandler_t811017192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVuforiaInitializationErrorHandler::cameraPrefesShortCut()
extern "C"  void ARMVuforiaInitializationErrorHandler_cameraPrefesShortCut_m3529175021 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVuforiaInitializationErrorHandler::Awake()
extern "C"  void ARMVuforiaInitializationErrorHandler_Awake_m2716433686 (ARMVuforiaInitializationErrorHandler_t811017192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVuforiaInitializationErrorHandler::OnGUI()
extern "C"  void ARMVuforiaInitializationErrorHandler_OnGUI_m1974227117 (ARMVuforiaInitializationErrorHandler_t811017192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVuforiaInitializationErrorHandler::OnDestroy()
extern "C"  void ARMVuforiaInitializationErrorHandler_OnDestroy_m629059372 (ARMVuforiaInitializationErrorHandler_t811017192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVuforiaInitializationErrorHandler::DrawWindowContent(System.Int32)
extern "C"  void ARMVuforiaInitializationErrorHandler_DrawWindowContent_m2158977895 (ARMVuforiaInitializationErrorHandler_t811017192 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVuforiaInitializationErrorHandler::ShowXCodeConfigButton()
extern "C"  void ARMVuforiaInitializationErrorHandler_ShowXCodeConfigButton_m520390797 (ARMVuforiaInitializationErrorHandler_t811017192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVuforiaInitializationErrorHandler::RestartApplication()
extern "C"  void ARMVuforiaInitializationErrorHandler_RestartApplication_m1538162354 (ARMVuforiaInitializationErrorHandler_t811017192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVuforiaInitializationErrorHandler::SetErrorCode(Vuforia.VuforiaUnity/InitError)
extern "C"  void ARMVuforiaInitializationErrorHandler_SetErrorCode_m1466135708 (ARMVuforiaInitializationErrorHandler_t811017192 * __this, int32_t ___errorCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVuforiaInitializationErrorHandler::SetErrorOccurred(System.Boolean)
extern "C"  void ARMVuforiaInitializationErrorHandler_SetErrorOccurred_m1273547507 (ARMVuforiaInitializationErrorHandler_t811017192 * __this, bool ___errorOccurred0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVuforiaInitializationErrorHandler::OnQCARInitializationError(Vuforia.VuforiaUnity/InitError)
extern "C"  void ARMVuforiaInitializationErrorHandler_OnQCARInitializationError_m1761516079 (ARMVuforiaInitializationErrorHandler_t811017192 * __this, int32_t ___initError0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMVuforiaInitializationErrorHandler::OnApplicationPause(System.Boolean)
extern "C"  void ARMVuforiaInitializationErrorHandler_OnApplicationPause_m1269318509 (ARMVuforiaInitializationErrorHandler_t811017192 * __this, bool ___pauseState0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
