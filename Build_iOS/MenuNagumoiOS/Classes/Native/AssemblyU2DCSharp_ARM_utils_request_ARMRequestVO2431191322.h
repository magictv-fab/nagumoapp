﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// ARM.utils.request.IRequestParameter
struct IRequestParameter_t2549190997;
// UnityEngine.WWW
struct WWW_t3134621005;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.utils.request.ARMRequestVO
struct  ARMRequestVO_t2431191322  : public Il2CppObject
{
public:
	// System.Boolean ARM.utils.request.ARMRequestVO::UseUnityCache
	bool ___UseUnityCache_0;
	// System.Int32 ARM.utils.request.ARMRequestVO::UnityCacheVersion
	int32_t ___UnityCacheVersion_1;
	// System.String ARM.utils.request.ARMRequestVO::Url
	String_t* ___Url_2;
	// System.Int32 ARM.utils.request.ARMRequestVO::TotalAttempts
	int32_t ___TotalAttempts_3;
	// UnityEngine.WWWForm ARM.utils.request.ARMRequestVO::PostData
	WWWForm_t461342257 * ___PostData_4;
	// System.Single ARM.utils.request.ARMRequestVO::Weight
	float ___Weight_5;
	// ARM.utils.request.IRequestParameter ARM.utils.request.ARMRequestVO::MetaData
	Il2CppObject * ___MetaData_6;
	// System.Int32 ARM.utils.request.ARMRequestVO::CurrentAttempt
	int32_t ___CurrentAttempt_7;
	// UnityEngine.WWW ARM.utils.request.ARMRequestVO::Request
	WWW_t3134621005 * ___Request_8;

public:
	inline static int32_t get_offset_of_UseUnityCache_0() { return static_cast<int32_t>(offsetof(ARMRequestVO_t2431191322, ___UseUnityCache_0)); }
	inline bool get_UseUnityCache_0() const { return ___UseUnityCache_0; }
	inline bool* get_address_of_UseUnityCache_0() { return &___UseUnityCache_0; }
	inline void set_UseUnityCache_0(bool value)
	{
		___UseUnityCache_0 = value;
	}

	inline static int32_t get_offset_of_UnityCacheVersion_1() { return static_cast<int32_t>(offsetof(ARMRequestVO_t2431191322, ___UnityCacheVersion_1)); }
	inline int32_t get_UnityCacheVersion_1() const { return ___UnityCacheVersion_1; }
	inline int32_t* get_address_of_UnityCacheVersion_1() { return &___UnityCacheVersion_1; }
	inline void set_UnityCacheVersion_1(int32_t value)
	{
		___UnityCacheVersion_1 = value;
	}

	inline static int32_t get_offset_of_Url_2() { return static_cast<int32_t>(offsetof(ARMRequestVO_t2431191322, ___Url_2)); }
	inline String_t* get_Url_2() const { return ___Url_2; }
	inline String_t** get_address_of_Url_2() { return &___Url_2; }
	inline void set_Url_2(String_t* value)
	{
		___Url_2 = value;
		Il2CppCodeGenWriteBarrier(&___Url_2, value);
	}

	inline static int32_t get_offset_of_TotalAttempts_3() { return static_cast<int32_t>(offsetof(ARMRequestVO_t2431191322, ___TotalAttempts_3)); }
	inline int32_t get_TotalAttempts_3() const { return ___TotalAttempts_3; }
	inline int32_t* get_address_of_TotalAttempts_3() { return &___TotalAttempts_3; }
	inline void set_TotalAttempts_3(int32_t value)
	{
		___TotalAttempts_3 = value;
	}

	inline static int32_t get_offset_of_PostData_4() { return static_cast<int32_t>(offsetof(ARMRequestVO_t2431191322, ___PostData_4)); }
	inline WWWForm_t461342257 * get_PostData_4() const { return ___PostData_4; }
	inline WWWForm_t461342257 ** get_address_of_PostData_4() { return &___PostData_4; }
	inline void set_PostData_4(WWWForm_t461342257 * value)
	{
		___PostData_4 = value;
		Il2CppCodeGenWriteBarrier(&___PostData_4, value);
	}

	inline static int32_t get_offset_of_Weight_5() { return static_cast<int32_t>(offsetof(ARMRequestVO_t2431191322, ___Weight_5)); }
	inline float get_Weight_5() const { return ___Weight_5; }
	inline float* get_address_of_Weight_5() { return &___Weight_5; }
	inline void set_Weight_5(float value)
	{
		___Weight_5 = value;
	}

	inline static int32_t get_offset_of_MetaData_6() { return static_cast<int32_t>(offsetof(ARMRequestVO_t2431191322, ___MetaData_6)); }
	inline Il2CppObject * get_MetaData_6() const { return ___MetaData_6; }
	inline Il2CppObject ** get_address_of_MetaData_6() { return &___MetaData_6; }
	inline void set_MetaData_6(Il2CppObject * value)
	{
		___MetaData_6 = value;
		Il2CppCodeGenWriteBarrier(&___MetaData_6, value);
	}

	inline static int32_t get_offset_of_CurrentAttempt_7() { return static_cast<int32_t>(offsetof(ARMRequestVO_t2431191322, ___CurrentAttempt_7)); }
	inline int32_t get_CurrentAttempt_7() const { return ___CurrentAttempt_7; }
	inline int32_t* get_address_of_CurrentAttempt_7() { return &___CurrentAttempt_7; }
	inline void set_CurrentAttempt_7(int32_t value)
	{
		___CurrentAttempt_7 = value;
	}

	inline static int32_t get_offset_of_Request_8() { return static_cast<int32_t>(offsetof(ARMRequestVO_t2431191322, ___Request_8)); }
	inline WWW_t3134621005 * get_Request_8() const { return ___Request_8; }
	inline WWW_t3134621005 ** get_address_of_Request_8() { return &___Request_8; }
	inline void set_Request_8(WWW_t3134621005 * value)
	{
		___Request_8 = value;
		Il2CppCodeGenWriteBarrier(&___Request_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
