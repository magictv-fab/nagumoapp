﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// PlayMakerAnimatorMoveProxy
struct PlayMakerAnimatorMoveProxy_t4175490694;
// UnityEngine.Animator
struct Animator_t2776330603;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo
struct  GetAnimatorCurrentTransitionInfo_t114989767  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::layerIndex
	FsmInt_t1596138449 * ___layerIndex_10;
	// System.Boolean HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::everyFrame
	bool ___everyFrame_11;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::name
	FsmString_t952858651 * ___name_12;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::nameHash
	FsmInt_t1596138449 * ___nameHash_13;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::userNameHash
	FsmInt_t1596138449 * ___userNameHash_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::normalizedTime
	FsmFloat_t2134102846 * ___normalizedTime_15;
	// PlayMakerAnimatorMoveProxy HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::_animatorProxy
	PlayMakerAnimatorMoveProxy_t4175490694 * ____animatorProxy_16;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::_animator
	Animator_t2776330603 * ____animator_17;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfo_t114989767, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_layerIndex_10() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfo_t114989767, ___layerIndex_10)); }
	inline FsmInt_t1596138449 * get_layerIndex_10() const { return ___layerIndex_10; }
	inline FsmInt_t1596138449 ** get_address_of_layerIndex_10() { return &___layerIndex_10; }
	inline void set_layerIndex_10(FsmInt_t1596138449 * value)
	{
		___layerIndex_10 = value;
		Il2CppCodeGenWriteBarrier(&___layerIndex_10, value);
	}

	inline static int32_t get_offset_of_everyFrame_11() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfo_t114989767, ___everyFrame_11)); }
	inline bool get_everyFrame_11() const { return ___everyFrame_11; }
	inline bool* get_address_of_everyFrame_11() { return &___everyFrame_11; }
	inline void set_everyFrame_11(bool value)
	{
		___everyFrame_11 = value;
	}

	inline static int32_t get_offset_of_name_12() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfo_t114989767, ___name_12)); }
	inline FsmString_t952858651 * get_name_12() const { return ___name_12; }
	inline FsmString_t952858651 ** get_address_of_name_12() { return &___name_12; }
	inline void set_name_12(FsmString_t952858651 * value)
	{
		___name_12 = value;
		Il2CppCodeGenWriteBarrier(&___name_12, value);
	}

	inline static int32_t get_offset_of_nameHash_13() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfo_t114989767, ___nameHash_13)); }
	inline FsmInt_t1596138449 * get_nameHash_13() const { return ___nameHash_13; }
	inline FsmInt_t1596138449 ** get_address_of_nameHash_13() { return &___nameHash_13; }
	inline void set_nameHash_13(FsmInt_t1596138449 * value)
	{
		___nameHash_13 = value;
		Il2CppCodeGenWriteBarrier(&___nameHash_13, value);
	}

	inline static int32_t get_offset_of_userNameHash_14() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfo_t114989767, ___userNameHash_14)); }
	inline FsmInt_t1596138449 * get_userNameHash_14() const { return ___userNameHash_14; }
	inline FsmInt_t1596138449 ** get_address_of_userNameHash_14() { return &___userNameHash_14; }
	inline void set_userNameHash_14(FsmInt_t1596138449 * value)
	{
		___userNameHash_14 = value;
		Il2CppCodeGenWriteBarrier(&___userNameHash_14, value);
	}

	inline static int32_t get_offset_of_normalizedTime_15() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfo_t114989767, ___normalizedTime_15)); }
	inline FsmFloat_t2134102846 * get_normalizedTime_15() const { return ___normalizedTime_15; }
	inline FsmFloat_t2134102846 ** get_address_of_normalizedTime_15() { return &___normalizedTime_15; }
	inline void set_normalizedTime_15(FsmFloat_t2134102846 * value)
	{
		___normalizedTime_15 = value;
		Il2CppCodeGenWriteBarrier(&___normalizedTime_15, value);
	}

	inline static int32_t get_offset_of__animatorProxy_16() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfo_t114989767, ____animatorProxy_16)); }
	inline PlayMakerAnimatorMoveProxy_t4175490694 * get__animatorProxy_16() const { return ____animatorProxy_16; }
	inline PlayMakerAnimatorMoveProxy_t4175490694 ** get_address_of__animatorProxy_16() { return &____animatorProxy_16; }
	inline void set__animatorProxy_16(PlayMakerAnimatorMoveProxy_t4175490694 * value)
	{
		____animatorProxy_16 = value;
		Il2CppCodeGenWriteBarrier(&____animatorProxy_16, value);
	}

	inline static int32_t get_offset_of__animator_17() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfo_t114989767, ____animator_17)); }
	inline Animator_t2776330603 * get__animator_17() const { return ____animator_17; }
	inline Animator_t2776330603 ** get_address_of__animator_17() { return &____animator_17; }
	inline void set__animator_17(Animator_t2776330603 * value)
	{
		____animator_17 = value;
		Il2CppCodeGenWriteBarrier(&____animator_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
