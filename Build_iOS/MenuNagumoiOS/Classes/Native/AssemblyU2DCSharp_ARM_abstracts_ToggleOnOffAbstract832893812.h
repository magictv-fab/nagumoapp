﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.abstracts.ToggleOnOffAbstract/TurnChange
struct TurnChange_t3311189389;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.abstracts.ToggleOnOffAbstract
struct  ToggleOnOffAbstract_t832893812  : public MonoBehaviour_t667441552
{
public:
	// ARM.abstracts.ToggleOnOffAbstract/TurnChange ARM.abstracts.ToggleOnOffAbstract::onTurnOn
	TurnChange_t3311189389 * ___onTurnOn_2;
	// ARM.abstracts.ToggleOnOffAbstract/TurnChange ARM.abstracts.ToggleOnOffAbstract::onTurnOff
	TurnChange_t3311189389 * ___onTurnOff_3;
	// System.Boolean ARM.abstracts.ToggleOnOffAbstract::activeByDefault
	bool ___activeByDefault_4;
	// System.Boolean ARM.abstracts.ToggleOnOffAbstract::_isActive
	bool ____isActive_5;
	// System.Boolean ARM.abstracts.ToggleOnOffAbstract::DebugIsOn
	bool ___DebugIsOn_6;

public:
	inline static int32_t get_offset_of_onTurnOn_2() { return static_cast<int32_t>(offsetof(ToggleOnOffAbstract_t832893812, ___onTurnOn_2)); }
	inline TurnChange_t3311189389 * get_onTurnOn_2() const { return ___onTurnOn_2; }
	inline TurnChange_t3311189389 ** get_address_of_onTurnOn_2() { return &___onTurnOn_2; }
	inline void set_onTurnOn_2(TurnChange_t3311189389 * value)
	{
		___onTurnOn_2 = value;
		Il2CppCodeGenWriteBarrier(&___onTurnOn_2, value);
	}

	inline static int32_t get_offset_of_onTurnOff_3() { return static_cast<int32_t>(offsetof(ToggleOnOffAbstract_t832893812, ___onTurnOff_3)); }
	inline TurnChange_t3311189389 * get_onTurnOff_3() const { return ___onTurnOff_3; }
	inline TurnChange_t3311189389 ** get_address_of_onTurnOff_3() { return &___onTurnOff_3; }
	inline void set_onTurnOff_3(TurnChange_t3311189389 * value)
	{
		___onTurnOff_3 = value;
		Il2CppCodeGenWriteBarrier(&___onTurnOff_3, value);
	}

	inline static int32_t get_offset_of_activeByDefault_4() { return static_cast<int32_t>(offsetof(ToggleOnOffAbstract_t832893812, ___activeByDefault_4)); }
	inline bool get_activeByDefault_4() const { return ___activeByDefault_4; }
	inline bool* get_address_of_activeByDefault_4() { return &___activeByDefault_4; }
	inline void set_activeByDefault_4(bool value)
	{
		___activeByDefault_4 = value;
	}

	inline static int32_t get_offset_of__isActive_5() { return static_cast<int32_t>(offsetof(ToggleOnOffAbstract_t832893812, ____isActive_5)); }
	inline bool get__isActive_5() const { return ____isActive_5; }
	inline bool* get_address_of__isActive_5() { return &____isActive_5; }
	inline void set__isActive_5(bool value)
	{
		____isActive_5 = value;
	}

	inline static int32_t get_offset_of_DebugIsOn_6() { return static_cast<int32_t>(offsetof(ToggleOnOffAbstract_t832893812, ___DebugIsOn_6)); }
	inline bool get_DebugIsOn_6() const { return ___DebugIsOn_6; }
	inline bool* get_address_of_DebugIsOn_6() { return &___DebugIsOn_6; }
	inline void set_DebugIsOn_6(bool value)
	{
		___DebugIsOn_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
