﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.abstracts.internet.InternetInfoAbstract
struct InternetInfoAbstract_t823636095;
// ARM.abstracts.internet.InternetInfoAbstract/OnWifiInfoEventHandler
struct OnWifiInfoEventHandler_t558195666;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ARM_abstracts_internet_InternetIn558195666.h"

// System.Void ARM.abstracts.internet.InternetInfoAbstract::.ctor()
extern "C"  void InternetInfoAbstract__ctor_m3202323740 (InternetInfoAbstract_t823636095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.internet.InternetInfoAbstract::AddWifiInfoEventHandler(ARM.abstracts.internet.InternetInfoAbstract/OnWifiInfoEventHandler)
extern "C"  void InternetInfoAbstract_AddWifiInfoEventHandler_m3673222332 (InternetInfoAbstract_t823636095 * __this, OnWifiInfoEventHandler_t558195666 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.internet.InternetInfoAbstract::RemoveWifiInfoEventHandler(ARM.abstracts.internet.InternetInfoAbstract/OnWifiInfoEventHandler)
extern "C"  void InternetInfoAbstract_RemoveWifiInfoEventHandler_m3119499149 (InternetInfoAbstract_t823636095 * __this, OnWifiInfoEventHandler_t558195666 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.abstracts.internet.InternetInfoAbstract::RaiseWifiInfo(System.Boolean)
extern "C"  void InternetInfoAbstract_RaiseWifiInfo_m1937494176 (InternetInfoAbstract_t823636095 * __this, bool ___has0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARM.abstracts.internet.InternetInfoAbstract::hasWifi()
extern "C"  bool InternetInfoAbstract_hasWifi_m2723197149 (InternetInfoAbstract_t823636095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
