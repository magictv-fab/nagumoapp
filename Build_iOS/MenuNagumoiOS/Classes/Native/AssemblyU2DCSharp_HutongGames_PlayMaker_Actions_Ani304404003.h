﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmAnimationCurve
struct FsmAnimationCurve_t2685995989;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AnimateFloat
struct  AnimateFloat_t304404003  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.AnimateFloat::animCurve
	FsmAnimationCurve_t2685995989 * ___animCurve_9;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AnimateFloat::floatVariable
	FsmFloat_t2134102846 * ___floatVariable_10;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.AnimateFloat::finishEvent
	FsmEvent_t2133468028 * ___finishEvent_11;
	// System.Boolean HutongGames.PlayMaker.Actions.AnimateFloat::realTime
	bool ___realTime_12;
	// System.Single HutongGames.PlayMaker.Actions.AnimateFloat::startTime
	float ___startTime_13;
	// System.Single HutongGames.PlayMaker.Actions.AnimateFloat::currentTime
	float ___currentTime_14;
	// System.Single HutongGames.PlayMaker.Actions.AnimateFloat::endTime
	float ___endTime_15;
	// System.Boolean HutongGames.PlayMaker.Actions.AnimateFloat::looping
	bool ___looping_16;

public:
	inline static int32_t get_offset_of_animCurve_9() { return static_cast<int32_t>(offsetof(AnimateFloat_t304404003, ___animCurve_9)); }
	inline FsmAnimationCurve_t2685995989 * get_animCurve_9() const { return ___animCurve_9; }
	inline FsmAnimationCurve_t2685995989 ** get_address_of_animCurve_9() { return &___animCurve_9; }
	inline void set_animCurve_9(FsmAnimationCurve_t2685995989 * value)
	{
		___animCurve_9 = value;
		Il2CppCodeGenWriteBarrier(&___animCurve_9, value);
	}

	inline static int32_t get_offset_of_floatVariable_10() { return static_cast<int32_t>(offsetof(AnimateFloat_t304404003, ___floatVariable_10)); }
	inline FsmFloat_t2134102846 * get_floatVariable_10() const { return ___floatVariable_10; }
	inline FsmFloat_t2134102846 ** get_address_of_floatVariable_10() { return &___floatVariable_10; }
	inline void set_floatVariable_10(FsmFloat_t2134102846 * value)
	{
		___floatVariable_10 = value;
		Il2CppCodeGenWriteBarrier(&___floatVariable_10, value);
	}

	inline static int32_t get_offset_of_finishEvent_11() { return static_cast<int32_t>(offsetof(AnimateFloat_t304404003, ___finishEvent_11)); }
	inline FsmEvent_t2133468028 * get_finishEvent_11() const { return ___finishEvent_11; }
	inline FsmEvent_t2133468028 ** get_address_of_finishEvent_11() { return &___finishEvent_11; }
	inline void set_finishEvent_11(FsmEvent_t2133468028 * value)
	{
		___finishEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___finishEvent_11, value);
	}

	inline static int32_t get_offset_of_realTime_12() { return static_cast<int32_t>(offsetof(AnimateFloat_t304404003, ___realTime_12)); }
	inline bool get_realTime_12() const { return ___realTime_12; }
	inline bool* get_address_of_realTime_12() { return &___realTime_12; }
	inline void set_realTime_12(bool value)
	{
		___realTime_12 = value;
	}

	inline static int32_t get_offset_of_startTime_13() { return static_cast<int32_t>(offsetof(AnimateFloat_t304404003, ___startTime_13)); }
	inline float get_startTime_13() const { return ___startTime_13; }
	inline float* get_address_of_startTime_13() { return &___startTime_13; }
	inline void set_startTime_13(float value)
	{
		___startTime_13 = value;
	}

	inline static int32_t get_offset_of_currentTime_14() { return static_cast<int32_t>(offsetof(AnimateFloat_t304404003, ___currentTime_14)); }
	inline float get_currentTime_14() const { return ___currentTime_14; }
	inline float* get_address_of_currentTime_14() { return &___currentTime_14; }
	inline void set_currentTime_14(float value)
	{
		___currentTime_14 = value;
	}

	inline static int32_t get_offset_of_endTime_15() { return static_cast<int32_t>(offsetof(AnimateFloat_t304404003, ___endTime_15)); }
	inline float get_endTime_15() const { return ___endTime_15; }
	inline float* get_address_of_endTime_15() { return &___endTime_15; }
	inline void set_endTime_15(float value)
	{
		___endTime_15 = value;
	}

	inline static int32_t get_offset_of_looping_16() { return static_cast<int32_t>(offsetof(AnimateFloat_t304404003, ___looping_16)); }
	inline bool get_looping_16() const { return ___looping_16; }
	inline bool* get_address_of_looping_16() { return &___looping_16; }
	inline void set_looping_16(bool value)
	{
		___looping_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
