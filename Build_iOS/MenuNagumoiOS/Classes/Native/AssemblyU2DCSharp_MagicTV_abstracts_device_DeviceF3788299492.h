﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// MagicTV.abstracts.device.DeviceFileInfoAbstract
struct DeviceFileInfoAbstract_t3788299492;

#include "AssemblyU2DCSharp_ARM_abstracts_components_General3900398046.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.abstracts.device.DeviceFileInfoAbstract
struct  DeviceFileInfoAbstract_t3788299492  : public GeneralComponentsAbstract_t3900398046
{
public:
	// System.Collections.Generic.List`1<System.String> MagicTV.abstracts.device.DeviceFileInfoAbstract::tracks
	List_1_t1375417109 * ___tracks_4;

public:
	inline static int32_t get_offset_of_tracks_4() { return static_cast<int32_t>(offsetof(DeviceFileInfoAbstract_t3788299492, ___tracks_4)); }
	inline List_1_t1375417109 * get_tracks_4() const { return ___tracks_4; }
	inline List_1_t1375417109 ** get_address_of_tracks_4() { return &___tracks_4; }
	inline void set_tracks_4(List_1_t1375417109 * value)
	{
		___tracks_4 = value;
		Il2CppCodeGenWriteBarrier(&___tracks_4, value);
	}
};

struct DeviceFileInfoAbstract_t3788299492_StaticFields
{
public:
	// MagicTV.abstracts.device.DeviceFileInfoAbstract MagicTV.abstracts.device.DeviceFileInfoAbstract::instance
	DeviceFileInfoAbstract_t3788299492 * ___instance_5;

public:
	inline static int32_t get_offset_of_instance_5() { return static_cast<int32_t>(offsetof(DeviceFileInfoAbstract_t3788299492_StaticFields, ___instance_5)); }
	inline DeviceFileInfoAbstract_t3788299492 * get_instance_5() const { return ___instance_5; }
	inline DeviceFileInfoAbstract_t3788299492 ** get_address_of_instance_5() { return &___instance_5; }
	inline void set_instance_5(DeviceFileInfoAbstract_t3788299492 * value)
	{
		___instance_5 = value;
		Il2CppCodeGenWriteBarrier(&___instance_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
