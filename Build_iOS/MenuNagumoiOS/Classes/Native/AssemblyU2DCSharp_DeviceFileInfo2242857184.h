﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MagicTV.vo.ResultRevisionVO
struct ResultRevisionVO_t2445597391;
// DeviceFileInfoDAO
struct DeviceFileInfoDAO_t152212114;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>>
struct Dictionary_2_t4173121995;
// System.Collections.Generic.Dictionary`2<System.String,MagicTV.vo.BundleVO[]>
struct Dictionary_2_t2983310470;
// DeviceFileInfo
struct DeviceFileInfo_t2242857184;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Func`2<MagicTV.vo.BundleVO,System.Collections.Generic.IEnumerable`1<MagicTV.vo.MetadataVO>>
struct Func_2_t2880153227;
// System.Func`2<MagicTV.vo.MetadataVO,System.Boolean>
struct Func_2_t3914892389;
// System.Func`2<MagicTV.vo.BundleVO,MagicTV.vo.BundleVO>
struct Func_2_t3347468641;
// System.Func`2<MagicTV.vo.BundleVO,System.Boolean>
struct Func_2_t1839749286;
// System.Func`2<MagicTV.vo.FileVO,System.Boolean>
struct Func_2_t250370948;
// System.Func`2<MagicTV.vo.FileVO,MagicTV.vo.FileVO>
struct Func_2_t1708920889;
// System.Func`2<MagicTV.vo.UrlInfoVO,System.Boolean>
struct Func_2_t1358372731;
// System.Func`2<MagicTV.vo.UrlInfoVO,MagicTV.vo.UrlInfoVO>
struct Func_2_t2643561541;

#include "AssemblyU2DCSharp_MagicTV_abstracts_device_DeviceF3788299492.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeviceFileInfo
struct  DeviceFileInfo_t2242857184  : public DeviceFileInfoAbstract_t3788299492
{
public:
	// MagicTV.vo.ResultRevisionVO DeviceFileInfo::_ResultRevisionVO
	ResultRevisionVO_t2445597391 * ____ResultRevisionVO_6;
	// DeviceFileInfoDAO DeviceFileInfo::_DeviceFileInfoDAO
	DeviceFileInfoDAO_t152212114 * ____DeviceFileInfoDAO_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<MagicTV.vo.BundleVO>> DeviceFileInfo::_indexedBundles
	Dictionary_2_t4173121995 * ____indexedBundles_8;
	// System.Collections.Generic.Dictionary`2<System.String,MagicTV.vo.BundleVO[]> DeviceFileInfo::_indexedBundlesArray
	Dictionary_2_t2983310470 * ____indexedBundlesArray_9;
	// System.String DeviceFileInfo::hashVarName
	String_t* ___hashVarName_11;
	// System.Boolean DeviceFileInfo::_debugClicked
	bool ____debugClicked_12;
	// System.String DeviceFileInfo::KEY_TOTAL_IMAGE
	String_t* ___KEY_TOTAL_IMAGE_13;
	// System.String DeviceFileInfo::KEY_LOCAL_IMAGE_URL_
	String_t* ___KEY_LOCAL_IMAGE_URL__14;
	// System.String DeviceFileInfo::TAG_LAST_INDEX_IMAGE_SORTED
	String_t* ___TAG_LAST_INDEX_IMAGE_SORTED_15;
	// System.String[] DeviceFileInfo::initialImages
	StringU5BU5D_t4054002952* ___initialImages_16;

public:
	inline static int32_t get_offset_of__ResultRevisionVO_6() { return static_cast<int32_t>(offsetof(DeviceFileInfo_t2242857184, ____ResultRevisionVO_6)); }
	inline ResultRevisionVO_t2445597391 * get__ResultRevisionVO_6() const { return ____ResultRevisionVO_6; }
	inline ResultRevisionVO_t2445597391 ** get_address_of__ResultRevisionVO_6() { return &____ResultRevisionVO_6; }
	inline void set__ResultRevisionVO_6(ResultRevisionVO_t2445597391 * value)
	{
		____ResultRevisionVO_6 = value;
		Il2CppCodeGenWriteBarrier(&____ResultRevisionVO_6, value);
	}

	inline static int32_t get_offset_of__DeviceFileInfoDAO_7() { return static_cast<int32_t>(offsetof(DeviceFileInfo_t2242857184, ____DeviceFileInfoDAO_7)); }
	inline DeviceFileInfoDAO_t152212114 * get__DeviceFileInfoDAO_7() const { return ____DeviceFileInfoDAO_7; }
	inline DeviceFileInfoDAO_t152212114 ** get_address_of__DeviceFileInfoDAO_7() { return &____DeviceFileInfoDAO_7; }
	inline void set__DeviceFileInfoDAO_7(DeviceFileInfoDAO_t152212114 * value)
	{
		____DeviceFileInfoDAO_7 = value;
		Il2CppCodeGenWriteBarrier(&____DeviceFileInfoDAO_7, value);
	}

	inline static int32_t get_offset_of__indexedBundles_8() { return static_cast<int32_t>(offsetof(DeviceFileInfo_t2242857184, ____indexedBundles_8)); }
	inline Dictionary_2_t4173121995 * get__indexedBundles_8() const { return ____indexedBundles_8; }
	inline Dictionary_2_t4173121995 ** get_address_of__indexedBundles_8() { return &____indexedBundles_8; }
	inline void set__indexedBundles_8(Dictionary_2_t4173121995 * value)
	{
		____indexedBundles_8 = value;
		Il2CppCodeGenWriteBarrier(&____indexedBundles_8, value);
	}

	inline static int32_t get_offset_of__indexedBundlesArray_9() { return static_cast<int32_t>(offsetof(DeviceFileInfo_t2242857184, ____indexedBundlesArray_9)); }
	inline Dictionary_2_t2983310470 * get__indexedBundlesArray_9() const { return ____indexedBundlesArray_9; }
	inline Dictionary_2_t2983310470 ** get_address_of__indexedBundlesArray_9() { return &____indexedBundlesArray_9; }
	inline void set__indexedBundlesArray_9(Dictionary_2_t2983310470 * value)
	{
		____indexedBundlesArray_9 = value;
		Il2CppCodeGenWriteBarrier(&____indexedBundlesArray_9, value);
	}

	inline static int32_t get_offset_of_hashVarName_11() { return static_cast<int32_t>(offsetof(DeviceFileInfo_t2242857184, ___hashVarName_11)); }
	inline String_t* get_hashVarName_11() const { return ___hashVarName_11; }
	inline String_t** get_address_of_hashVarName_11() { return &___hashVarName_11; }
	inline void set_hashVarName_11(String_t* value)
	{
		___hashVarName_11 = value;
		Il2CppCodeGenWriteBarrier(&___hashVarName_11, value);
	}

	inline static int32_t get_offset_of__debugClicked_12() { return static_cast<int32_t>(offsetof(DeviceFileInfo_t2242857184, ____debugClicked_12)); }
	inline bool get__debugClicked_12() const { return ____debugClicked_12; }
	inline bool* get_address_of__debugClicked_12() { return &____debugClicked_12; }
	inline void set__debugClicked_12(bool value)
	{
		____debugClicked_12 = value;
	}

	inline static int32_t get_offset_of_KEY_TOTAL_IMAGE_13() { return static_cast<int32_t>(offsetof(DeviceFileInfo_t2242857184, ___KEY_TOTAL_IMAGE_13)); }
	inline String_t* get_KEY_TOTAL_IMAGE_13() const { return ___KEY_TOTAL_IMAGE_13; }
	inline String_t** get_address_of_KEY_TOTAL_IMAGE_13() { return &___KEY_TOTAL_IMAGE_13; }
	inline void set_KEY_TOTAL_IMAGE_13(String_t* value)
	{
		___KEY_TOTAL_IMAGE_13 = value;
		Il2CppCodeGenWriteBarrier(&___KEY_TOTAL_IMAGE_13, value);
	}

	inline static int32_t get_offset_of_KEY_LOCAL_IMAGE_URL__14() { return static_cast<int32_t>(offsetof(DeviceFileInfo_t2242857184, ___KEY_LOCAL_IMAGE_URL__14)); }
	inline String_t* get_KEY_LOCAL_IMAGE_URL__14() const { return ___KEY_LOCAL_IMAGE_URL__14; }
	inline String_t** get_address_of_KEY_LOCAL_IMAGE_URL__14() { return &___KEY_LOCAL_IMAGE_URL__14; }
	inline void set_KEY_LOCAL_IMAGE_URL__14(String_t* value)
	{
		___KEY_LOCAL_IMAGE_URL__14 = value;
		Il2CppCodeGenWriteBarrier(&___KEY_LOCAL_IMAGE_URL__14, value);
	}

	inline static int32_t get_offset_of_TAG_LAST_INDEX_IMAGE_SORTED_15() { return static_cast<int32_t>(offsetof(DeviceFileInfo_t2242857184, ___TAG_LAST_INDEX_IMAGE_SORTED_15)); }
	inline String_t* get_TAG_LAST_INDEX_IMAGE_SORTED_15() const { return ___TAG_LAST_INDEX_IMAGE_SORTED_15; }
	inline String_t** get_address_of_TAG_LAST_INDEX_IMAGE_SORTED_15() { return &___TAG_LAST_INDEX_IMAGE_SORTED_15; }
	inline void set_TAG_LAST_INDEX_IMAGE_SORTED_15(String_t* value)
	{
		___TAG_LAST_INDEX_IMAGE_SORTED_15 = value;
		Il2CppCodeGenWriteBarrier(&___TAG_LAST_INDEX_IMAGE_SORTED_15, value);
	}

	inline static int32_t get_offset_of_initialImages_16() { return static_cast<int32_t>(offsetof(DeviceFileInfo_t2242857184, ___initialImages_16)); }
	inline StringU5BU5D_t4054002952* get_initialImages_16() const { return ___initialImages_16; }
	inline StringU5BU5D_t4054002952** get_address_of_initialImages_16() { return &___initialImages_16; }
	inline void set_initialImages_16(StringU5BU5D_t4054002952* value)
	{
		___initialImages_16 = value;
		Il2CppCodeGenWriteBarrier(&___initialImages_16, value);
	}
};

struct DeviceFileInfo_t2242857184_StaticFields
{
public:
	// DeviceFileInfo DeviceFileInfo::instance
	DeviceFileInfo_t2242857184 * ___instance_10;
	// System.Func`2<MagicTV.vo.BundleVO,System.Collections.Generic.IEnumerable`1<MagicTV.vo.MetadataVO>> DeviceFileInfo::<>f__am$cacheB
	Func_2_t2880153227 * ___U3CU3Ef__amU24cacheB_17;
	// System.Func`2<MagicTV.vo.MetadataVO,System.Boolean> DeviceFileInfo::<>f__am$cacheC
	Func_2_t3914892389 * ___U3CU3Ef__amU24cacheC_18;
	// System.Func`2<MagicTV.vo.BundleVO,MagicTV.vo.BundleVO> DeviceFileInfo::<>f__am$cacheD
	Func_2_t3347468641 * ___U3CU3Ef__amU24cacheD_19;
	// System.Func`2<MagicTV.vo.BundleVO,System.Boolean> DeviceFileInfo::<>f__am$cacheE
	Func_2_t1839749286 * ___U3CU3Ef__amU24cacheE_20;
	// System.Func`2<MagicTV.vo.FileVO,System.Boolean> DeviceFileInfo::<>f__am$cacheF
	Func_2_t250370948 * ___U3CU3Ef__amU24cacheF_21;
	// System.Func`2<MagicTV.vo.FileVO,MagicTV.vo.FileVO> DeviceFileInfo::<>f__am$cache10
	Func_2_t1708920889 * ___U3CU3Ef__amU24cache10_22;
	// System.Func`2<MagicTV.vo.FileVO,System.Boolean> DeviceFileInfo::<>f__am$cache11
	Func_2_t250370948 * ___U3CU3Ef__amU24cache11_23;
	// System.Func`2<MagicTV.vo.UrlInfoVO,System.Boolean> DeviceFileInfo::<>f__am$cache12
	Func_2_t1358372731 * ___U3CU3Ef__amU24cache12_24;
	// System.Func`2<MagicTV.vo.UrlInfoVO,MagicTV.vo.UrlInfoVO> DeviceFileInfo::<>f__am$cache13
	Func_2_t2643561541 * ___U3CU3Ef__amU24cache13_25;

public:
	inline static int32_t get_offset_of_instance_10() { return static_cast<int32_t>(offsetof(DeviceFileInfo_t2242857184_StaticFields, ___instance_10)); }
	inline DeviceFileInfo_t2242857184 * get_instance_10() const { return ___instance_10; }
	inline DeviceFileInfo_t2242857184 ** get_address_of_instance_10() { return &___instance_10; }
	inline void set_instance_10(DeviceFileInfo_t2242857184 * value)
	{
		___instance_10 = value;
		Il2CppCodeGenWriteBarrier(&___instance_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_17() { return static_cast<int32_t>(offsetof(DeviceFileInfo_t2242857184_StaticFields, ___U3CU3Ef__amU24cacheB_17)); }
	inline Func_2_t2880153227 * get_U3CU3Ef__amU24cacheB_17() const { return ___U3CU3Ef__amU24cacheB_17; }
	inline Func_2_t2880153227 ** get_address_of_U3CU3Ef__amU24cacheB_17() { return &___U3CU3Ef__amU24cacheB_17; }
	inline void set_U3CU3Ef__amU24cacheB_17(Func_2_t2880153227 * value)
	{
		___U3CU3Ef__amU24cacheB_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheB_17, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheC_18() { return static_cast<int32_t>(offsetof(DeviceFileInfo_t2242857184_StaticFields, ___U3CU3Ef__amU24cacheC_18)); }
	inline Func_2_t3914892389 * get_U3CU3Ef__amU24cacheC_18() const { return ___U3CU3Ef__amU24cacheC_18; }
	inline Func_2_t3914892389 ** get_address_of_U3CU3Ef__amU24cacheC_18() { return &___U3CU3Ef__amU24cacheC_18; }
	inline void set_U3CU3Ef__amU24cacheC_18(Func_2_t3914892389 * value)
	{
		___U3CU3Ef__amU24cacheC_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheC_18, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheD_19() { return static_cast<int32_t>(offsetof(DeviceFileInfo_t2242857184_StaticFields, ___U3CU3Ef__amU24cacheD_19)); }
	inline Func_2_t3347468641 * get_U3CU3Ef__amU24cacheD_19() const { return ___U3CU3Ef__amU24cacheD_19; }
	inline Func_2_t3347468641 ** get_address_of_U3CU3Ef__amU24cacheD_19() { return &___U3CU3Ef__amU24cacheD_19; }
	inline void set_U3CU3Ef__amU24cacheD_19(Func_2_t3347468641 * value)
	{
		___U3CU3Ef__amU24cacheD_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheD_19, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheE_20() { return static_cast<int32_t>(offsetof(DeviceFileInfo_t2242857184_StaticFields, ___U3CU3Ef__amU24cacheE_20)); }
	inline Func_2_t1839749286 * get_U3CU3Ef__amU24cacheE_20() const { return ___U3CU3Ef__amU24cacheE_20; }
	inline Func_2_t1839749286 ** get_address_of_U3CU3Ef__amU24cacheE_20() { return &___U3CU3Ef__amU24cacheE_20; }
	inline void set_U3CU3Ef__amU24cacheE_20(Func_2_t1839749286 * value)
	{
		___U3CU3Ef__amU24cacheE_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheE_20, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheF_21() { return static_cast<int32_t>(offsetof(DeviceFileInfo_t2242857184_StaticFields, ___U3CU3Ef__amU24cacheF_21)); }
	inline Func_2_t250370948 * get_U3CU3Ef__amU24cacheF_21() const { return ___U3CU3Ef__amU24cacheF_21; }
	inline Func_2_t250370948 ** get_address_of_U3CU3Ef__amU24cacheF_21() { return &___U3CU3Ef__amU24cacheF_21; }
	inline void set_U3CU3Ef__amU24cacheF_21(Func_2_t250370948 * value)
	{
		___U3CU3Ef__amU24cacheF_21 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheF_21, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache10_22() { return static_cast<int32_t>(offsetof(DeviceFileInfo_t2242857184_StaticFields, ___U3CU3Ef__amU24cache10_22)); }
	inline Func_2_t1708920889 * get_U3CU3Ef__amU24cache10_22() const { return ___U3CU3Ef__amU24cache10_22; }
	inline Func_2_t1708920889 ** get_address_of_U3CU3Ef__amU24cache10_22() { return &___U3CU3Ef__amU24cache10_22; }
	inline void set_U3CU3Ef__amU24cache10_22(Func_2_t1708920889 * value)
	{
		___U3CU3Ef__amU24cache10_22 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache10_22, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache11_23() { return static_cast<int32_t>(offsetof(DeviceFileInfo_t2242857184_StaticFields, ___U3CU3Ef__amU24cache11_23)); }
	inline Func_2_t250370948 * get_U3CU3Ef__amU24cache11_23() const { return ___U3CU3Ef__amU24cache11_23; }
	inline Func_2_t250370948 ** get_address_of_U3CU3Ef__amU24cache11_23() { return &___U3CU3Ef__amU24cache11_23; }
	inline void set_U3CU3Ef__amU24cache11_23(Func_2_t250370948 * value)
	{
		___U3CU3Ef__amU24cache11_23 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache11_23, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache12_24() { return static_cast<int32_t>(offsetof(DeviceFileInfo_t2242857184_StaticFields, ___U3CU3Ef__amU24cache12_24)); }
	inline Func_2_t1358372731 * get_U3CU3Ef__amU24cache12_24() const { return ___U3CU3Ef__amU24cache12_24; }
	inline Func_2_t1358372731 ** get_address_of_U3CU3Ef__amU24cache12_24() { return &___U3CU3Ef__amU24cache12_24; }
	inline void set_U3CU3Ef__amU24cache12_24(Func_2_t1358372731 * value)
	{
		___U3CU3Ef__amU24cache12_24 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache12_24, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache13_25() { return static_cast<int32_t>(offsetof(DeviceFileInfo_t2242857184_StaticFields, ___U3CU3Ef__amU24cache13_25)); }
	inline Func_2_t2643561541 * get_U3CU3Ef__amU24cache13_25() const { return ___U3CU3Ef__amU24cache13_25; }
	inline Func_2_t2643561541 ** get_address_of_U3CU3Ef__amU24cache13_25() { return &___U3CU3Ef__amU24cache13_25; }
	inline void set_U3CU3Ef__amU24cache13_25(Func_2_t2643561541 * value)
	{
		___U3CU3Ef__amU24cache13_25 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache13_25, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
