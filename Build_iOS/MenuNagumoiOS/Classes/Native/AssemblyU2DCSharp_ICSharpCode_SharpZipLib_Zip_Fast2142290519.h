﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t4260760469;
// ICSharpCode.SharpZipLib.Zip.ZipOutputStream
struct ZipOutputStream_t2362341588;
// ICSharpCode.SharpZipLib.Zip.ZipFile
struct ZipFile_t2937401711;
// System.String
struct String_t;
// ICSharpCode.SharpZipLib.Core.NameFilter
struct NameFilter_t123192849;
// ICSharpCode.SharpZipLib.Zip.FastZip/ConfirmOverwriteDelegate
struct ConfirmOverwriteDelegate_t2877285688;
// ICSharpCode.SharpZipLib.Zip.FastZipEvents
struct FastZipEvents_t381075024;
// ICSharpCode.SharpZipLib.Zip.IEntryFactory
struct IEntryFactory_t131367667;
// ICSharpCode.SharpZipLib.Core.INameTransform
struct INameTransform_t2173030;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_Fast1147420323.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Zip_UseZ3006992774.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.FastZip
struct  FastZip_t2142290519  : public Il2CppObject
{
public:
	// System.Boolean ICSharpCode.SharpZipLib.Zip.FastZip::continueRunning_
	bool ___continueRunning__0;
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.FastZip::buffer_
	ByteU5BU5D_t4260760469* ___buffer__1;
	// ICSharpCode.SharpZipLib.Zip.ZipOutputStream ICSharpCode.SharpZipLib.Zip.FastZip::outputStream_
	ZipOutputStream_t2362341588 * ___outputStream__2;
	// ICSharpCode.SharpZipLib.Zip.ZipFile ICSharpCode.SharpZipLib.Zip.FastZip::zipFile_
	ZipFile_t2937401711 * ___zipFile__3;
	// System.String ICSharpCode.SharpZipLib.Zip.FastZip::sourceDirectory_
	String_t* ___sourceDirectory__4;
	// ICSharpCode.SharpZipLib.Core.NameFilter ICSharpCode.SharpZipLib.Zip.FastZip::fileFilter_
	NameFilter_t123192849 * ___fileFilter__5;
	// ICSharpCode.SharpZipLib.Core.NameFilter ICSharpCode.SharpZipLib.Zip.FastZip::directoryFilter_
	NameFilter_t123192849 * ___directoryFilter__6;
	// ICSharpCode.SharpZipLib.Zip.FastZip/Overwrite ICSharpCode.SharpZipLib.Zip.FastZip::overwrite_
	int32_t ___overwrite__7;
	// ICSharpCode.SharpZipLib.Zip.FastZip/ConfirmOverwriteDelegate ICSharpCode.SharpZipLib.Zip.FastZip::confirmDelegate_
	ConfirmOverwriteDelegate_t2877285688 * ___confirmDelegate__8;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.FastZip::restoreDateTimeOnExtract_
	bool ___restoreDateTimeOnExtract__9;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.FastZip::restoreAttributesOnExtract_
	bool ___restoreAttributesOnExtract__10;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.FastZip::createEmptyDirectories_
	bool ___createEmptyDirectories__11;
	// ICSharpCode.SharpZipLib.Zip.FastZipEvents ICSharpCode.SharpZipLib.Zip.FastZip::events_
	FastZipEvents_t381075024 * ___events__12;
	// ICSharpCode.SharpZipLib.Zip.IEntryFactory ICSharpCode.SharpZipLib.Zip.FastZip::entryFactory_
	Il2CppObject * ___entryFactory__13;
	// ICSharpCode.SharpZipLib.Core.INameTransform ICSharpCode.SharpZipLib.Zip.FastZip::extractNameTransform_
	Il2CppObject * ___extractNameTransform__14;
	// ICSharpCode.SharpZipLib.Zip.UseZip64 ICSharpCode.SharpZipLib.Zip.FastZip::useZip64_
	int32_t ___useZip64__15;
	// System.String ICSharpCode.SharpZipLib.Zip.FastZip::password_
	String_t* ___password__16;

public:
	inline static int32_t get_offset_of_continueRunning__0() { return static_cast<int32_t>(offsetof(FastZip_t2142290519, ___continueRunning__0)); }
	inline bool get_continueRunning__0() const { return ___continueRunning__0; }
	inline bool* get_address_of_continueRunning__0() { return &___continueRunning__0; }
	inline void set_continueRunning__0(bool value)
	{
		___continueRunning__0 = value;
	}

	inline static int32_t get_offset_of_buffer__1() { return static_cast<int32_t>(offsetof(FastZip_t2142290519, ___buffer__1)); }
	inline ByteU5BU5D_t4260760469* get_buffer__1() const { return ___buffer__1; }
	inline ByteU5BU5D_t4260760469** get_address_of_buffer__1() { return &___buffer__1; }
	inline void set_buffer__1(ByteU5BU5D_t4260760469* value)
	{
		___buffer__1 = value;
		Il2CppCodeGenWriteBarrier(&___buffer__1, value);
	}

	inline static int32_t get_offset_of_outputStream__2() { return static_cast<int32_t>(offsetof(FastZip_t2142290519, ___outputStream__2)); }
	inline ZipOutputStream_t2362341588 * get_outputStream__2() const { return ___outputStream__2; }
	inline ZipOutputStream_t2362341588 ** get_address_of_outputStream__2() { return &___outputStream__2; }
	inline void set_outputStream__2(ZipOutputStream_t2362341588 * value)
	{
		___outputStream__2 = value;
		Il2CppCodeGenWriteBarrier(&___outputStream__2, value);
	}

	inline static int32_t get_offset_of_zipFile__3() { return static_cast<int32_t>(offsetof(FastZip_t2142290519, ___zipFile__3)); }
	inline ZipFile_t2937401711 * get_zipFile__3() const { return ___zipFile__3; }
	inline ZipFile_t2937401711 ** get_address_of_zipFile__3() { return &___zipFile__3; }
	inline void set_zipFile__3(ZipFile_t2937401711 * value)
	{
		___zipFile__3 = value;
		Il2CppCodeGenWriteBarrier(&___zipFile__3, value);
	}

	inline static int32_t get_offset_of_sourceDirectory__4() { return static_cast<int32_t>(offsetof(FastZip_t2142290519, ___sourceDirectory__4)); }
	inline String_t* get_sourceDirectory__4() const { return ___sourceDirectory__4; }
	inline String_t** get_address_of_sourceDirectory__4() { return &___sourceDirectory__4; }
	inline void set_sourceDirectory__4(String_t* value)
	{
		___sourceDirectory__4 = value;
		Il2CppCodeGenWriteBarrier(&___sourceDirectory__4, value);
	}

	inline static int32_t get_offset_of_fileFilter__5() { return static_cast<int32_t>(offsetof(FastZip_t2142290519, ___fileFilter__5)); }
	inline NameFilter_t123192849 * get_fileFilter__5() const { return ___fileFilter__5; }
	inline NameFilter_t123192849 ** get_address_of_fileFilter__5() { return &___fileFilter__5; }
	inline void set_fileFilter__5(NameFilter_t123192849 * value)
	{
		___fileFilter__5 = value;
		Il2CppCodeGenWriteBarrier(&___fileFilter__5, value);
	}

	inline static int32_t get_offset_of_directoryFilter__6() { return static_cast<int32_t>(offsetof(FastZip_t2142290519, ___directoryFilter__6)); }
	inline NameFilter_t123192849 * get_directoryFilter__6() const { return ___directoryFilter__6; }
	inline NameFilter_t123192849 ** get_address_of_directoryFilter__6() { return &___directoryFilter__6; }
	inline void set_directoryFilter__6(NameFilter_t123192849 * value)
	{
		___directoryFilter__6 = value;
		Il2CppCodeGenWriteBarrier(&___directoryFilter__6, value);
	}

	inline static int32_t get_offset_of_overwrite__7() { return static_cast<int32_t>(offsetof(FastZip_t2142290519, ___overwrite__7)); }
	inline int32_t get_overwrite__7() const { return ___overwrite__7; }
	inline int32_t* get_address_of_overwrite__7() { return &___overwrite__7; }
	inline void set_overwrite__7(int32_t value)
	{
		___overwrite__7 = value;
	}

	inline static int32_t get_offset_of_confirmDelegate__8() { return static_cast<int32_t>(offsetof(FastZip_t2142290519, ___confirmDelegate__8)); }
	inline ConfirmOverwriteDelegate_t2877285688 * get_confirmDelegate__8() const { return ___confirmDelegate__8; }
	inline ConfirmOverwriteDelegate_t2877285688 ** get_address_of_confirmDelegate__8() { return &___confirmDelegate__8; }
	inline void set_confirmDelegate__8(ConfirmOverwriteDelegate_t2877285688 * value)
	{
		___confirmDelegate__8 = value;
		Il2CppCodeGenWriteBarrier(&___confirmDelegate__8, value);
	}

	inline static int32_t get_offset_of_restoreDateTimeOnExtract__9() { return static_cast<int32_t>(offsetof(FastZip_t2142290519, ___restoreDateTimeOnExtract__9)); }
	inline bool get_restoreDateTimeOnExtract__9() const { return ___restoreDateTimeOnExtract__9; }
	inline bool* get_address_of_restoreDateTimeOnExtract__9() { return &___restoreDateTimeOnExtract__9; }
	inline void set_restoreDateTimeOnExtract__9(bool value)
	{
		___restoreDateTimeOnExtract__9 = value;
	}

	inline static int32_t get_offset_of_restoreAttributesOnExtract__10() { return static_cast<int32_t>(offsetof(FastZip_t2142290519, ___restoreAttributesOnExtract__10)); }
	inline bool get_restoreAttributesOnExtract__10() const { return ___restoreAttributesOnExtract__10; }
	inline bool* get_address_of_restoreAttributesOnExtract__10() { return &___restoreAttributesOnExtract__10; }
	inline void set_restoreAttributesOnExtract__10(bool value)
	{
		___restoreAttributesOnExtract__10 = value;
	}

	inline static int32_t get_offset_of_createEmptyDirectories__11() { return static_cast<int32_t>(offsetof(FastZip_t2142290519, ___createEmptyDirectories__11)); }
	inline bool get_createEmptyDirectories__11() const { return ___createEmptyDirectories__11; }
	inline bool* get_address_of_createEmptyDirectories__11() { return &___createEmptyDirectories__11; }
	inline void set_createEmptyDirectories__11(bool value)
	{
		___createEmptyDirectories__11 = value;
	}

	inline static int32_t get_offset_of_events__12() { return static_cast<int32_t>(offsetof(FastZip_t2142290519, ___events__12)); }
	inline FastZipEvents_t381075024 * get_events__12() const { return ___events__12; }
	inline FastZipEvents_t381075024 ** get_address_of_events__12() { return &___events__12; }
	inline void set_events__12(FastZipEvents_t381075024 * value)
	{
		___events__12 = value;
		Il2CppCodeGenWriteBarrier(&___events__12, value);
	}

	inline static int32_t get_offset_of_entryFactory__13() { return static_cast<int32_t>(offsetof(FastZip_t2142290519, ___entryFactory__13)); }
	inline Il2CppObject * get_entryFactory__13() const { return ___entryFactory__13; }
	inline Il2CppObject ** get_address_of_entryFactory__13() { return &___entryFactory__13; }
	inline void set_entryFactory__13(Il2CppObject * value)
	{
		___entryFactory__13 = value;
		Il2CppCodeGenWriteBarrier(&___entryFactory__13, value);
	}

	inline static int32_t get_offset_of_extractNameTransform__14() { return static_cast<int32_t>(offsetof(FastZip_t2142290519, ___extractNameTransform__14)); }
	inline Il2CppObject * get_extractNameTransform__14() const { return ___extractNameTransform__14; }
	inline Il2CppObject ** get_address_of_extractNameTransform__14() { return &___extractNameTransform__14; }
	inline void set_extractNameTransform__14(Il2CppObject * value)
	{
		___extractNameTransform__14 = value;
		Il2CppCodeGenWriteBarrier(&___extractNameTransform__14, value);
	}

	inline static int32_t get_offset_of_useZip64__15() { return static_cast<int32_t>(offsetof(FastZip_t2142290519, ___useZip64__15)); }
	inline int32_t get_useZip64__15() const { return ___useZip64__15; }
	inline int32_t* get_address_of_useZip64__15() { return &___useZip64__15; }
	inline void set_useZip64__15(int32_t value)
	{
		___useZip64__15 = value;
	}

	inline static int32_t get_offset_of_password__16() { return static_cast<int32_t>(offsetof(FastZip_t2142290519, ___password__16)); }
	inline String_t* get_password__16() const { return ___password__16; }
	inline String_t** get_address_of_password__16() { return &___password__16; }
	inline void set_password__16(String_t* value)
	{
		___password__16 = value;
		Il2CppCodeGenWriteBarrier(&___password__16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
