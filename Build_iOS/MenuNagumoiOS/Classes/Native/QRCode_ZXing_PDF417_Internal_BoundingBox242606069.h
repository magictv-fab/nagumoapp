﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZXing.Common.BitMatrix
struct BitMatrix_t1058711404;
// ZXing.ResultPoint
struct ResultPoint_t1538592853;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.BoundingBox
struct  BoundingBox_t242606069  : public Il2CppObject
{
public:
	// ZXing.Common.BitMatrix ZXing.PDF417.Internal.BoundingBox::image
	BitMatrix_t1058711404 * ___image_0;
	// ZXing.ResultPoint ZXing.PDF417.Internal.BoundingBox::<TopLeft>k__BackingField
	ResultPoint_t1538592853 * ___U3CTopLeftU3Ek__BackingField_1;
	// ZXing.ResultPoint ZXing.PDF417.Internal.BoundingBox::<TopRight>k__BackingField
	ResultPoint_t1538592853 * ___U3CTopRightU3Ek__BackingField_2;
	// ZXing.ResultPoint ZXing.PDF417.Internal.BoundingBox::<BottomLeft>k__BackingField
	ResultPoint_t1538592853 * ___U3CBottomLeftU3Ek__BackingField_3;
	// ZXing.ResultPoint ZXing.PDF417.Internal.BoundingBox::<BottomRight>k__BackingField
	ResultPoint_t1538592853 * ___U3CBottomRightU3Ek__BackingField_4;
	// System.Int32 ZXing.PDF417.Internal.BoundingBox::<MinX>k__BackingField
	int32_t ___U3CMinXU3Ek__BackingField_5;
	// System.Int32 ZXing.PDF417.Internal.BoundingBox::<MaxX>k__BackingField
	int32_t ___U3CMaxXU3Ek__BackingField_6;
	// System.Int32 ZXing.PDF417.Internal.BoundingBox::<MinY>k__BackingField
	int32_t ___U3CMinYU3Ek__BackingField_7;
	// System.Int32 ZXing.PDF417.Internal.BoundingBox::<MaxY>k__BackingField
	int32_t ___U3CMaxYU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_image_0() { return static_cast<int32_t>(offsetof(BoundingBox_t242606069, ___image_0)); }
	inline BitMatrix_t1058711404 * get_image_0() const { return ___image_0; }
	inline BitMatrix_t1058711404 ** get_address_of_image_0() { return &___image_0; }
	inline void set_image_0(BitMatrix_t1058711404 * value)
	{
		___image_0 = value;
		Il2CppCodeGenWriteBarrier(&___image_0, value);
	}

	inline static int32_t get_offset_of_U3CTopLeftU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BoundingBox_t242606069, ___U3CTopLeftU3Ek__BackingField_1)); }
	inline ResultPoint_t1538592853 * get_U3CTopLeftU3Ek__BackingField_1() const { return ___U3CTopLeftU3Ek__BackingField_1; }
	inline ResultPoint_t1538592853 ** get_address_of_U3CTopLeftU3Ek__BackingField_1() { return &___U3CTopLeftU3Ek__BackingField_1; }
	inline void set_U3CTopLeftU3Ek__BackingField_1(ResultPoint_t1538592853 * value)
	{
		___U3CTopLeftU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTopLeftU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CTopRightU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BoundingBox_t242606069, ___U3CTopRightU3Ek__BackingField_2)); }
	inline ResultPoint_t1538592853 * get_U3CTopRightU3Ek__BackingField_2() const { return ___U3CTopRightU3Ek__BackingField_2; }
	inline ResultPoint_t1538592853 ** get_address_of_U3CTopRightU3Ek__BackingField_2() { return &___U3CTopRightU3Ek__BackingField_2; }
	inline void set_U3CTopRightU3Ek__BackingField_2(ResultPoint_t1538592853 * value)
	{
		___U3CTopRightU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTopRightU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CBottomLeftU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(BoundingBox_t242606069, ___U3CBottomLeftU3Ek__BackingField_3)); }
	inline ResultPoint_t1538592853 * get_U3CBottomLeftU3Ek__BackingField_3() const { return ___U3CBottomLeftU3Ek__BackingField_3; }
	inline ResultPoint_t1538592853 ** get_address_of_U3CBottomLeftU3Ek__BackingField_3() { return &___U3CBottomLeftU3Ek__BackingField_3; }
	inline void set_U3CBottomLeftU3Ek__BackingField_3(ResultPoint_t1538592853 * value)
	{
		___U3CBottomLeftU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CBottomLeftU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CBottomRightU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(BoundingBox_t242606069, ___U3CBottomRightU3Ek__BackingField_4)); }
	inline ResultPoint_t1538592853 * get_U3CBottomRightU3Ek__BackingField_4() const { return ___U3CBottomRightU3Ek__BackingField_4; }
	inline ResultPoint_t1538592853 ** get_address_of_U3CBottomRightU3Ek__BackingField_4() { return &___U3CBottomRightU3Ek__BackingField_4; }
	inline void set_U3CBottomRightU3Ek__BackingField_4(ResultPoint_t1538592853 * value)
	{
		___U3CBottomRightU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CBottomRightU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CMinXU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(BoundingBox_t242606069, ___U3CMinXU3Ek__BackingField_5)); }
	inline int32_t get_U3CMinXU3Ek__BackingField_5() const { return ___U3CMinXU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CMinXU3Ek__BackingField_5() { return &___U3CMinXU3Ek__BackingField_5; }
	inline void set_U3CMinXU3Ek__BackingField_5(int32_t value)
	{
		___U3CMinXU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CMaxXU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(BoundingBox_t242606069, ___U3CMaxXU3Ek__BackingField_6)); }
	inline int32_t get_U3CMaxXU3Ek__BackingField_6() const { return ___U3CMaxXU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CMaxXU3Ek__BackingField_6() { return &___U3CMaxXU3Ek__BackingField_6; }
	inline void set_U3CMaxXU3Ek__BackingField_6(int32_t value)
	{
		___U3CMaxXU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CMinYU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(BoundingBox_t242606069, ___U3CMinYU3Ek__BackingField_7)); }
	inline int32_t get_U3CMinYU3Ek__BackingField_7() const { return ___U3CMinYU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CMinYU3Ek__BackingField_7() { return &___U3CMinYU3Ek__BackingField_7; }
	inline void set_U3CMinYU3Ek__BackingField_7(int32_t value)
	{
		___U3CMinYU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CMaxYU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(BoundingBox_t242606069, ___U3CMaxYU3Ek__BackingField_8)); }
	inline int32_t get_U3CMaxYU3Ek__BackingField_8() const { return ___U3CMaxYU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CMaxYU3Ek__BackingField_8() { return &___U3CMaxYU3Ek__BackingField_8; }
	inline void set_U3CMaxYU3Ek__BackingField_8(int32_t value)
	{
		___U3CMaxYU3Ek__BackingField_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
