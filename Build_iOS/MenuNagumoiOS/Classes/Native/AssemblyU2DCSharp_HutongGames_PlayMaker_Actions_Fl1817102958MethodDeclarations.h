﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FloatMultiply
struct FloatMultiply_t1817102958;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FloatMultiply::.ctor()
extern "C"  void FloatMultiply__ctor_m3361890952 (FloatMultiply_t1817102958 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatMultiply::Reset()
extern "C"  void FloatMultiply_Reset_m1008323893 (FloatMultiply_t1817102958 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatMultiply::OnEnter()
extern "C"  void FloatMultiply_OnEnter_m353526879 (FloatMultiply_t1817102958 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatMultiply::OnUpdate()
extern "C"  void FloatMultiply_OnUpdate_m1502958020 (FloatMultiply_t1817102958 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
