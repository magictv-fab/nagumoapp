﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DeviceFileInfo/<getUncachedRevision>c__AnonStorey9A
struct U3CgetUncachedRevisionU3Ec__AnonStorey9A_t2060657264;
// MagicTV.vo.MetadataVO
struct MetadataVO_t2511256998;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_vo_MetadataVO2511256998.h"

// System.Void DeviceFileInfo/<getUncachedRevision>c__AnonStorey9A::.ctor()
extern "C"  void U3CgetUncachedRevisionU3Ec__AnonStorey9A__ctor_m279777019 (U3CgetUncachedRevisionU3Ec__AnonStorey9A_t2060657264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.vo.MetadataVO DeviceFileInfo/<getUncachedRevision>c__AnonStorey9A::<>m__31(MagicTV.vo.MetadataVO)
extern "C"  MetadataVO_t2511256998 * U3CgetUncachedRevisionU3Ec__AnonStorey9A_U3CU3Em__31_m1257115677 (U3CgetUncachedRevisionU3Ec__AnonStorey9A_t2060657264 * __this, MetadataVO_t2511256998 * ___mx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
