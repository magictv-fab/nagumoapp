﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.MouseLook2
struct MouseLook2_t3768519990;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmFloat2134102846.h"

// System.Void HutongGames.PlayMaker.Actions.MouseLook2::.ctor()
extern "C"  void MouseLook2__ctor_m647578608 (MouseLook2_t3768519990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MouseLook2::Reset()
extern "C"  void MouseLook2_Reset_m2588978845 (MouseLook2_t3768519990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MouseLook2::OnEnter()
extern "C"  void MouseLook2_OnEnter_m3239480263 (MouseLook2_t3768519990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MouseLook2::OnUpdate()
extern "C"  void MouseLook2_OnUpdate_m773199708 (MouseLook2_t3768519990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MouseLook2::DoMouseLook()
extern "C"  void MouseLook2_DoMouseLook_m799901095 (MouseLook2_t3768519990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.MouseLook2::GetXRotation()
extern "C"  float MouseLook2_GetXRotation_m1962641024 (MouseLook2_t3768519990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.MouseLook2::GetYRotation()
extern "C"  float MouseLook2_GetYRotation_m165689665 (MouseLook2_t3768519990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.MouseLook2::ClampAngle(System.Single,HutongGames.PlayMaker.FsmFloat,HutongGames.PlayMaker.FsmFloat)
extern "C"  float MouseLook2_ClampAngle_m3742153203 (Il2CppObject * __this /* static, unused */, float ___angle0, FsmFloat_t2134102846 * ___min1, FsmFloat_t2134102846 * ___max2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
