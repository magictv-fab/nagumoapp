﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MagicTV.vo.BundleVO[]
struct BundleVOU5BU5D_t2162892100;
// MagicTVAbstractWindowConfirmControllerScript
struct MagicTVAbstractWindowConfirmControllerScript_t227123142;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.in_apps.InAppOnDemandDownloadManager
struct  InAppOnDemandDownloadManager_t507396607  : public Il2CppObject
{
public:
	// MagicTV.vo.BundleVO[] MagicTV.in_apps.InAppOnDemandDownloadManager::_bundles
	BundleVOU5BU5D_t2162892100* ____bundles_0;
	// System.Boolean MagicTV.in_apps.InAppOnDemandDownloadManager::_started
	bool ____started_2;

public:
	inline static int32_t get_offset_of__bundles_0() { return static_cast<int32_t>(offsetof(InAppOnDemandDownloadManager_t507396607, ____bundles_0)); }
	inline BundleVOU5BU5D_t2162892100* get__bundles_0() const { return ____bundles_0; }
	inline BundleVOU5BU5D_t2162892100** get_address_of__bundles_0() { return &____bundles_0; }
	inline void set__bundles_0(BundleVOU5BU5D_t2162892100* value)
	{
		____bundles_0 = value;
		Il2CppCodeGenWriteBarrier(&____bundles_0, value);
	}

	inline static int32_t get_offset_of__started_2() { return static_cast<int32_t>(offsetof(InAppOnDemandDownloadManager_t507396607, ____started_2)); }
	inline bool get__started_2() const { return ____started_2; }
	inline bool* get_address_of__started_2() { return &____started_2; }
	inline void set__started_2(bool value)
	{
		____started_2 = value;
	}
};

struct InAppOnDemandDownloadManager_t507396607_StaticFields
{
public:
	// MagicTVAbstractWindowConfirmControllerScript MagicTV.in_apps.InAppOnDemandDownloadManager::confirm
	MagicTVAbstractWindowConfirmControllerScript_t227123142 * ___confirm_1;

public:
	inline static int32_t get_offset_of_confirm_1() { return static_cast<int32_t>(offsetof(InAppOnDemandDownloadManager_t507396607_StaticFields, ___confirm_1)); }
	inline MagicTVAbstractWindowConfirmControllerScript_t227123142 * get_confirm_1() const { return ___confirm_1; }
	inline MagicTVAbstractWindowConfirmControllerScript_t227123142 ** get_address_of_confirm_1() { return &___confirm_1; }
	inline void set_confirm_1(MagicTVAbstractWindowConfirmControllerScript_t227123142 * value)
	{
		___confirm_1 = value;
		Il2CppCodeGenWriteBarrier(&___confirm_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
