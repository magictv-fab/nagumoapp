﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.Code93Reader
struct Code93Reader_t2183409032;
// ZXing.Result
struct Result_t2610723219;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>
struct IDictionary_2_t759989846;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"

// System.Void ZXing.OneD.Code93Reader::.ctor()
extern "C"  void Code93Reader__ctor_m521870747 (Code93Reader_t2183409032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Result ZXing.OneD.Code93Reader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern "C"  Result_t2610723219 * Code93Reader_decodeRow_m223003130 (Code93Reader_t2183409032 * __this, int32_t ___rowNumber0, BitArray_t4163851164 * ___row1, Il2CppObject* ___hints2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZXing.OneD.Code93Reader::findAsteriskPattern(ZXing.Common.BitArray)
extern "C"  Int32U5BU5D_t3230847821* Code93Reader_findAsteriskPattern_m2681982371 (Code93Reader_t2183409032 * __this, BitArray_t4163851164 * ___row0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.Code93Reader::toPattern(System.Int32[])
extern "C"  int32_t Code93Reader_toPattern_m529499563 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* ___counters0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.Code93Reader::patternToChar(System.Int32,System.Char&)
extern "C"  bool Code93Reader_patternToChar_m2044444534 (Il2CppObject * __this /* static, unused */, int32_t ___pattern0, Il2CppChar* ___c1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.OneD.Code93Reader::decodeExtended(System.Text.StringBuilder)
extern "C"  String_t* Code93Reader_decodeExtended_m2377500061 (Il2CppObject * __this /* static, unused */, StringBuilder_t243639308 * ___encoded0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.Code93Reader::checkChecksums(System.Text.StringBuilder)
extern "C"  bool Code93Reader_checkChecksums_m1296164717 (Il2CppObject * __this /* static, unused */, StringBuilder_t243639308 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.Code93Reader::checkOneChecksum(System.Text.StringBuilder,System.Int32,System.Int32)
extern "C"  bool Code93Reader_checkOneChecksum_m3820709620 (Il2CppObject * __this /* static, unused */, StringBuilder_t243639308 * ___result0, int32_t ___checkPosition1, int32_t ___weightMax2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.Code93Reader::.cctor()
extern "C"  void Code93Reader__cctor_m2810995058 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
