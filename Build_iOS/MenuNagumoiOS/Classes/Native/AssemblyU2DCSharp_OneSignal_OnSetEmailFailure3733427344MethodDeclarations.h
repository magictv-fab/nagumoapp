﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OneSignal/OnSetEmailFailure
struct OnSetEmailFailure_t3733427344;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t696267445;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void OneSignal/OnSetEmailFailure::.ctor(System.Object,System.IntPtr)
extern "C"  void OnSetEmailFailure__ctor_m4050137703 (OnSetEmailFailure_t3733427344 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal/OnSetEmailFailure::Invoke(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void OnSetEmailFailure_Invoke_m642143200 (OnSetEmailFailure_t3733427344 * __this, Dictionary_2_t696267445 * ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult OneSignal/OnSetEmailFailure::BeginInvoke(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnSetEmailFailure_BeginInvoke_m4057149685 (OnSetEmailFailure_t3733427344 * __this, Dictionary_2_t696267445 * ___error0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal/OnSetEmailFailure::EndInvoke(System.IAsyncResult)
extern "C"  void OnSetEmailFailure_EndInvoke_m520810487 (OnSetEmailFailure_t3733427344 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
