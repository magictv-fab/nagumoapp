﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen3781873254MethodDeclarations.h"

// System.Void System.Predicate`1<HutongGames.PlayMaker.FsmProperty>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m408190134(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t3538215890 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m982040097_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<HutongGames.PlayMaker.FsmProperty>::Invoke(T)
#define Predicate_1_Invoke_m4066559440(__this, ___obj0, method) ((  bool (*) (Predicate_1_t3538215890 *, FsmProperty_t3927159007 *, const MethodInfo*))Predicate_1_Invoke_m4106178309_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<HutongGames.PlayMaker.FsmProperty>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m1085391395(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t3538215890 *, FsmProperty_t3927159007 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m2038073176_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<HutongGames.PlayMaker.FsmProperty>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m854999044(__this, ___result0, method) ((  bool (*) (Predicate_1_t3538215890 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m3970497007_gshared)(__this, ___result0, method)
