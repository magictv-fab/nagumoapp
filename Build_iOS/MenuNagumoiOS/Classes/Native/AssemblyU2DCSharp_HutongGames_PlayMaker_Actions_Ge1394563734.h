﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// UnityEngine.Animator
struct Animator_t2776330603;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve
struct  GetAnimatorIsParameterControlledByCurve_t1394563734  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve::parameterName
	FsmString_t952858651 * ___parameterName_10;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve::isControlledByCurve
	FsmBool_t1075959796 * ___isControlledByCurve_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve::isControlledByCurveEvent
	FsmEvent_t2133468028 * ___isControlledByCurveEvent_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve::isNotControlledByCurveEvent
	FsmEvent_t2133468028 * ___isNotControlledByCurveEvent_13;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve::_animator
	Animator_t2776330603 * ____animator_14;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(GetAnimatorIsParameterControlledByCurve_t1394563734, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_parameterName_10() { return static_cast<int32_t>(offsetof(GetAnimatorIsParameterControlledByCurve_t1394563734, ___parameterName_10)); }
	inline FsmString_t952858651 * get_parameterName_10() const { return ___parameterName_10; }
	inline FsmString_t952858651 ** get_address_of_parameterName_10() { return &___parameterName_10; }
	inline void set_parameterName_10(FsmString_t952858651 * value)
	{
		___parameterName_10 = value;
		Il2CppCodeGenWriteBarrier(&___parameterName_10, value);
	}

	inline static int32_t get_offset_of_isControlledByCurve_11() { return static_cast<int32_t>(offsetof(GetAnimatorIsParameterControlledByCurve_t1394563734, ___isControlledByCurve_11)); }
	inline FsmBool_t1075959796 * get_isControlledByCurve_11() const { return ___isControlledByCurve_11; }
	inline FsmBool_t1075959796 ** get_address_of_isControlledByCurve_11() { return &___isControlledByCurve_11; }
	inline void set_isControlledByCurve_11(FsmBool_t1075959796 * value)
	{
		___isControlledByCurve_11 = value;
		Il2CppCodeGenWriteBarrier(&___isControlledByCurve_11, value);
	}

	inline static int32_t get_offset_of_isControlledByCurveEvent_12() { return static_cast<int32_t>(offsetof(GetAnimatorIsParameterControlledByCurve_t1394563734, ___isControlledByCurveEvent_12)); }
	inline FsmEvent_t2133468028 * get_isControlledByCurveEvent_12() const { return ___isControlledByCurveEvent_12; }
	inline FsmEvent_t2133468028 ** get_address_of_isControlledByCurveEvent_12() { return &___isControlledByCurveEvent_12; }
	inline void set_isControlledByCurveEvent_12(FsmEvent_t2133468028 * value)
	{
		___isControlledByCurveEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___isControlledByCurveEvent_12, value);
	}

	inline static int32_t get_offset_of_isNotControlledByCurveEvent_13() { return static_cast<int32_t>(offsetof(GetAnimatorIsParameterControlledByCurve_t1394563734, ___isNotControlledByCurveEvent_13)); }
	inline FsmEvent_t2133468028 * get_isNotControlledByCurveEvent_13() const { return ___isNotControlledByCurveEvent_13; }
	inline FsmEvent_t2133468028 ** get_address_of_isNotControlledByCurveEvent_13() { return &___isNotControlledByCurveEvent_13; }
	inline void set_isNotControlledByCurveEvent_13(FsmEvent_t2133468028 * value)
	{
		___isNotControlledByCurveEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___isNotControlledByCurveEvent_13, value);
	}

	inline static int32_t get_offset_of__animator_14() { return static_cast<int32_t>(offsetof(GetAnimatorIsParameterControlledByCurve_t1394563734, ____animator_14)); }
	inline Animator_t2776330603 * get__animator_14() const { return ____animator_14; }
	inline Animator_t2776330603 ** get_address_of__animator_14() { return &____animator_14; }
	inline void set__animator_14(Animator_t2776330603 * value)
	{
		____animator_14 = value;
		Il2CppCodeGenWriteBarrier(&____animator_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
