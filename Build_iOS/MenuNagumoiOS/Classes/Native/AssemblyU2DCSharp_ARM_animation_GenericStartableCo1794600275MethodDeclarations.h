﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.animation.GenericStartableComponentControllAbstract
struct GenericStartableComponentControllAbstract_t1794600275;
// OnEvent
struct OnEvent_t314892251;
// ARM.abstracts.components.GeneralComponentsAbstract
struct GeneralComponentsAbstract_t3900398046;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OnEvent314892251.h"
#include "AssemblyU2DCSharp_ARM_abstracts_components_General3900398046.h"
#include "AssemblyU2DCSharp_ARM_animation_GenericStartableCo1794600275.h"

// System.Void ARM.animation.GenericStartableComponentControllAbstract::.ctor()
extern "C"  void GenericStartableComponentControllAbstract__ctor_m397452510 (GenericStartableComponentControllAbstract_t1794600275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARM.animation.GenericStartableComponentControllAbstract::isFinished()
extern "C"  bool GenericStartableComponentControllAbstract_isFinished_m1325382478 (GenericStartableComponentControllAbstract_t1794600275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.animation.GenericStartableComponentControllAbstract::AddOnFinishedEventHandler(OnEvent)
extern "C"  void GenericStartableComponentControllAbstract_AddOnFinishedEventHandler_m2100035801 (GenericStartableComponentControllAbstract_t1794600275 * __this, OnEvent_t314892251 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.animation.GenericStartableComponentControllAbstract::RemoveOnFinishedEventHandler(OnEvent)
extern "C"  void GenericStartableComponentControllAbstract_RemoveOnFinishedEventHandler_m3088116268 (GenericStartableComponentControllAbstract_t1794600275 * __this, OnEvent_t314892251 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.animation.GenericStartableComponentControllAbstract::RaiseOnFinishedEventHandler()
extern "C"  void GenericStartableComponentControllAbstract_RaiseOnFinishedEventHandler_m2116391145 (GenericStartableComponentControllAbstract_t1794600275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARM.animation.GenericStartableComponentControllAbstract::isPlaying()
extern "C"  bool GenericStartableComponentControllAbstract_isPlaying_m630980596 (GenericStartableComponentControllAbstract_t1794600275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.animation.GenericStartableComponentControllAbstract::Play()
extern "C"  void GenericStartableComponentControllAbstract_Play_m578323162 (GenericStartableComponentControllAbstract_t1794600275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.animation.GenericStartableComponentControllAbstract::Stop()
extern "C"  void GenericStartableComponentControllAbstract_Stop_m672007208 (GenericStartableComponentControllAbstract_t1794600275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.animation.GenericStartableComponentControllAbstract::Pause()
extern "C"  void GenericStartableComponentControllAbstract_Pause_m451578482 (GenericStartableComponentControllAbstract_t1794600275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.animation.GenericStartableComponentControllAbstract::RaiseOnStartEventHandler()
extern "C"  void GenericStartableComponentControllAbstract_RaiseOnStartEventHandler_m103579533 (GenericStartableComponentControllAbstract_t1794600275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.animation.GenericStartableComponentControllAbstract::addProcessInitStep(ARM.abstracts.components.GeneralComponentsAbstract,OnEvent)
extern "C"  void GenericStartableComponentControllAbstract_addProcessInitStep_m148806698 (GenericStartableComponentControllAbstract_t1794600275 * __this, GeneralComponentsAbstract_t3900398046 * ___component0, OnEvent_t314892251 * ___nextStepMethod1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.animation.GenericStartableComponentControllAbstract::addProcessStartStep(ARM.animation.GenericStartableComponentControllAbstract,OnEvent)
extern "C"  void GenericStartableComponentControllAbstract_addProcessStartStep_m1047505114 (GenericStartableComponentControllAbstract_t1794600275 * __this, GenericStartableComponentControllAbstract_t1794600275 * ___component0, OnEvent_t314892251 * ___nextStepMethod1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
