﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NativeImagePicker/NativeImagePickerBehaviour
struct NativeImagePickerBehaviour_t3828351720;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void NativeImagePicker/NativeImagePickerBehaviour::.ctor()
extern "C"  void NativeImagePickerBehaviour__ctor_m3139065011 (NativeImagePickerBehaviour_t3828351720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeImagePicker/NativeImagePickerBehaviour::CallbackSelectedImage(System.String)
extern "C"  void NativeImagePickerBehaviour_CallbackSelectedImage_m3902827574 (NativeImagePickerBehaviour_t3828351720 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
