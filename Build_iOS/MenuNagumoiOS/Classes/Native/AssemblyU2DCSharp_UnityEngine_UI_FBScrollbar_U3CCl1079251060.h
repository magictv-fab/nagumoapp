﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1848751023;
// System.Object
struct Il2CppObject;
// UnityEngine.UI.FBScrollbar
struct FBScrollbar_t2094246736;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.FBScrollbar/<ClickRepeat>c__Iterator1C
struct  U3CClickRepeatU3Ec__Iterator1C_t1079251060  : public Il2CppObject
{
public:
	// UnityEngine.EventSystems.PointerEventData UnityEngine.UI.FBScrollbar/<ClickRepeat>c__Iterator1C::eventData
	PointerEventData_t1848751023 * ___eventData_0;
	// UnityEngine.Vector2 UnityEngine.UI.FBScrollbar/<ClickRepeat>c__Iterator1C::<localMousePos>__0
	Vector2_t4282066565  ___U3ClocalMousePosU3E__0_1;
	// System.Single UnityEngine.UI.FBScrollbar/<ClickRepeat>c__Iterator1C::<axisCoordinate>__1
	float ___U3CaxisCoordinateU3E__1_2;
	// System.Int32 UnityEngine.UI.FBScrollbar/<ClickRepeat>c__Iterator1C::$PC
	int32_t ___U24PC_3;
	// System.Object UnityEngine.UI.FBScrollbar/<ClickRepeat>c__Iterator1C::$current
	Il2CppObject * ___U24current_4;
	// UnityEngine.EventSystems.PointerEventData UnityEngine.UI.FBScrollbar/<ClickRepeat>c__Iterator1C::<$>eventData
	PointerEventData_t1848751023 * ___U3CU24U3EeventData_5;
	// UnityEngine.UI.FBScrollbar UnityEngine.UI.FBScrollbar/<ClickRepeat>c__Iterator1C::<>f__this
	FBScrollbar_t2094246736 * ___U3CU3Ef__this_6;

public:
	inline static int32_t get_offset_of_eventData_0() { return static_cast<int32_t>(offsetof(U3CClickRepeatU3Ec__Iterator1C_t1079251060, ___eventData_0)); }
	inline PointerEventData_t1848751023 * get_eventData_0() const { return ___eventData_0; }
	inline PointerEventData_t1848751023 ** get_address_of_eventData_0() { return &___eventData_0; }
	inline void set_eventData_0(PointerEventData_t1848751023 * value)
	{
		___eventData_0 = value;
		Il2CppCodeGenWriteBarrier(&___eventData_0, value);
	}

	inline static int32_t get_offset_of_U3ClocalMousePosU3E__0_1() { return static_cast<int32_t>(offsetof(U3CClickRepeatU3Ec__Iterator1C_t1079251060, ___U3ClocalMousePosU3E__0_1)); }
	inline Vector2_t4282066565  get_U3ClocalMousePosU3E__0_1() const { return ___U3ClocalMousePosU3E__0_1; }
	inline Vector2_t4282066565 * get_address_of_U3ClocalMousePosU3E__0_1() { return &___U3ClocalMousePosU3E__0_1; }
	inline void set_U3ClocalMousePosU3E__0_1(Vector2_t4282066565  value)
	{
		___U3ClocalMousePosU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CaxisCoordinateU3E__1_2() { return static_cast<int32_t>(offsetof(U3CClickRepeatU3Ec__Iterator1C_t1079251060, ___U3CaxisCoordinateU3E__1_2)); }
	inline float get_U3CaxisCoordinateU3E__1_2() const { return ___U3CaxisCoordinateU3E__1_2; }
	inline float* get_address_of_U3CaxisCoordinateU3E__1_2() { return &___U3CaxisCoordinateU3E__1_2; }
	inline void set_U3CaxisCoordinateU3E__1_2(float value)
	{
		___U3CaxisCoordinateU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CClickRepeatU3Ec__Iterator1C_t1079251060, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CClickRepeatU3Ec__Iterator1C_t1079251060, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EeventData_5() { return static_cast<int32_t>(offsetof(U3CClickRepeatU3Ec__Iterator1C_t1079251060, ___U3CU24U3EeventData_5)); }
	inline PointerEventData_t1848751023 * get_U3CU24U3EeventData_5() const { return ___U3CU24U3EeventData_5; }
	inline PointerEventData_t1848751023 ** get_address_of_U3CU24U3EeventData_5() { return &___U3CU24U3EeventData_5; }
	inline void set_U3CU24U3EeventData_5(PointerEventData_t1848751023 * value)
	{
		___U3CU24U3EeventData_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EeventData_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_6() { return static_cast<int32_t>(offsetof(U3CClickRepeatU3Ec__Iterator1C_t1079251060, ___U3CU3Ef__this_6)); }
	inline FBScrollbar_t2094246736 * get_U3CU3Ef__this_6() const { return ___U3CU3Ef__this_6; }
	inline FBScrollbar_t2094246736 ** get_address_of_U3CU3Ef__this_6() { return &___U3CU3Ef__this_6; }
	inline void set_U3CU3Ef__this_6(FBScrollbar_t2094246736 * value)
	{
		___U3CU3Ef__this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
