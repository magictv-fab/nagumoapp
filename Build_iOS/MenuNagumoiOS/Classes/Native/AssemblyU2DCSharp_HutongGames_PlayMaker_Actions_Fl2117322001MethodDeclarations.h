﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FloatCompare
struct FloatCompare_t2117322001;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FloatCompare::.ctor()
extern "C"  void FloatCompare__ctor_m2878934133 (FloatCompare_t2117322001 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatCompare::Reset()
extern "C"  void FloatCompare_Reset_m525367074 (FloatCompare_t2117322001 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatCompare::OnEnter()
extern "C"  void FloatCompare_OnEnter_m88491788 (FloatCompare_t2117322001 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatCompare::OnUpdate()
extern "C"  void FloatCompare_OnUpdate_m1876804791 (FloatCompare_t2117322001 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatCompare::DoCompare()
extern "C"  void FloatCompare_DoCompare_m978022381 (FloatCompare_t2117322001 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Actions.FloatCompare::ErrorCheck()
extern "C"  String_t* FloatCompare_ErrorCheck_m3586345138 (FloatCompare_t2117322001 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
