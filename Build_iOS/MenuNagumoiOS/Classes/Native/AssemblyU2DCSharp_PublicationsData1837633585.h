﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PublicationData[]
struct PublicationDataU5BU5D_t1155489811;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PublicationsData
struct  PublicationsData_t1837633585  : public Il2CppObject
{
public:
	// PublicationData[] PublicationsData::publicacoes
	PublicationDataU5BU5D_t1155489811* ___publicacoes_0;

public:
	inline static int32_t get_offset_of_publicacoes_0() { return static_cast<int32_t>(offsetof(PublicationsData_t1837633585, ___publicacoes_0)); }
	inline PublicationDataU5BU5D_t1155489811* get_publicacoes_0() const { return ___publicacoes_0; }
	inline PublicationDataU5BU5D_t1155489811** get_address_of_publicacoes_0() { return &___publicacoes_0; }
	inline void set_publicacoes_0(PublicationDataU5BU5D_t1155489811* value)
	{
		___publicacoes_0 = value;
		Il2CppCodeGenWriteBarrier(&___publicacoes_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
