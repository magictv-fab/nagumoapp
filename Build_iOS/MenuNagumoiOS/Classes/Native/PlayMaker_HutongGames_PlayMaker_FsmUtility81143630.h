﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Text.UTF8Encoding
struct UTF8Encoding_t2817869802;
// HutongGames.PlayMaker.Fsm
struct Fsm_t1527112426;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmUtility
struct  FsmUtility_t81143630  : public Il2CppObject
{
public:

public:
};

struct FsmUtility_t81143630_StaticFields
{
public:
	// System.Text.UTF8Encoding HutongGames.PlayMaker.FsmUtility::encoding
	UTF8Encoding_t2817869802 * ___encoding_0;
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.FsmUtility::CurrentFsm
	Fsm_t1527112426 * ___CurrentFsm_1;

public:
	inline static int32_t get_offset_of_encoding_0() { return static_cast<int32_t>(offsetof(FsmUtility_t81143630_StaticFields, ___encoding_0)); }
	inline UTF8Encoding_t2817869802 * get_encoding_0() const { return ___encoding_0; }
	inline UTF8Encoding_t2817869802 ** get_address_of_encoding_0() { return &___encoding_0; }
	inline void set_encoding_0(UTF8Encoding_t2817869802 * value)
	{
		___encoding_0 = value;
		Il2CppCodeGenWriteBarrier(&___encoding_0, value);
	}

	inline static int32_t get_offset_of_CurrentFsm_1() { return static_cast<int32_t>(offsetof(FsmUtility_t81143630_StaticFields, ___CurrentFsm_1)); }
	inline Fsm_t1527112426 * get_CurrentFsm_1() const { return ___CurrentFsm_1; }
	inline Fsm_t1527112426 ** get_address_of_CurrentFsm_1() { return &___CurrentFsm_1; }
	inline void set_CurrentFsm_1(Fsm_t1527112426 * value)
	{
		___CurrentFsm_1 = value;
		Il2CppCodeGenWriteBarrier(&___CurrentFsm_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
