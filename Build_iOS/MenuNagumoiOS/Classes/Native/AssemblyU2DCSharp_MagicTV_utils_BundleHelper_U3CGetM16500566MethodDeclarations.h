﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.utils.BundleHelper/<GetMetadataValuesByKey>c__AnonStoreyA4
struct U3CGetMetadataValuesByKeyU3Ec__AnonStoreyA4_t16500566;
// MagicTV.vo.MetadataVO
struct MetadataVO_t2511256998;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_vo_MetadataVO2511256998.h"

// System.Void MagicTV.utils.BundleHelper/<GetMetadataValuesByKey>c__AnonStoreyA4::.ctor()
extern "C"  void U3CGetMetadataValuesByKeyU3Ec__AnonStoreyA4__ctor_m3694955013 (U3CGetMetadataValuesByKeyU3Ec__AnonStoreyA4_t16500566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicTV.utils.BundleHelper/<GetMetadataValuesByKey>c__AnonStoreyA4::<>m__74(MagicTV.vo.MetadataVO)
extern "C"  bool U3CGetMetadataValuesByKeyU3Ec__AnonStoreyA4_U3CU3Em__74_m150799901 (U3CGetMetadataValuesByKeyU3Ec__AnonStoreyA4_t16500566 * __this, MetadataVO_t2511256998 * ___mt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
