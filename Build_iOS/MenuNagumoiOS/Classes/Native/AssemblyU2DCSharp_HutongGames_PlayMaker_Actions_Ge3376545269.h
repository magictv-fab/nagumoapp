﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// PlayMakerAnimatorMoveProxy
struct PlayMakerAnimatorMoveProxy_t4175490694;
// UnityEngine.Animator
struct Animator_t2776330603;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorBool
struct  GetAnimatorBool_t3376545269  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorBool::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetAnimatorBool::parameter
	FsmString_t952858651 * ___parameter_10;
	// System.Boolean HutongGames.PlayMaker.Actions.GetAnimatorBool::everyFrame
	bool ___everyFrame_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetAnimatorBool::result
	FsmBool_t1075959796 * ___result_12;
	// PlayMakerAnimatorMoveProxy HutongGames.PlayMaker.Actions.GetAnimatorBool::_animatorProxy
	PlayMakerAnimatorMoveProxy_t4175490694 * ____animatorProxy_13;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorBool::_animator
	Animator_t2776330603 * ____animator_14;
	// System.Int32 HutongGames.PlayMaker.Actions.GetAnimatorBool::_paramID
	int32_t ____paramID_15;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(GetAnimatorBool_t3376545269, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_parameter_10() { return static_cast<int32_t>(offsetof(GetAnimatorBool_t3376545269, ___parameter_10)); }
	inline FsmString_t952858651 * get_parameter_10() const { return ___parameter_10; }
	inline FsmString_t952858651 ** get_address_of_parameter_10() { return &___parameter_10; }
	inline void set_parameter_10(FsmString_t952858651 * value)
	{
		___parameter_10 = value;
		Il2CppCodeGenWriteBarrier(&___parameter_10, value);
	}

	inline static int32_t get_offset_of_everyFrame_11() { return static_cast<int32_t>(offsetof(GetAnimatorBool_t3376545269, ___everyFrame_11)); }
	inline bool get_everyFrame_11() const { return ___everyFrame_11; }
	inline bool* get_address_of_everyFrame_11() { return &___everyFrame_11; }
	inline void set_everyFrame_11(bool value)
	{
		___everyFrame_11 = value;
	}

	inline static int32_t get_offset_of_result_12() { return static_cast<int32_t>(offsetof(GetAnimatorBool_t3376545269, ___result_12)); }
	inline FsmBool_t1075959796 * get_result_12() const { return ___result_12; }
	inline FsmBool_t1075959796 ** get_address_of_result_12() { return &___result_12; }
	inline void set_result_12(FsmBool_t1075959796 * value)
	{
		___result_12 = value;
		Il2CppCodeGenWriteBarrier(&___result_12, value);
	}

	inline static int32_t get_offset_of__animatorProxy_13() { return static_cast<int32_t>(offsetof(GetAnimatorBool_t3376545269, ____animatorProxy_13)); }
	inline PlayMakerAnimatorMoveProxy_t4175490694 * get__animatorProxy_13() const { return ____animatorProxy_13; }
	inline PlayMakerAnimatorMoveProxy_t4175490694 ** get_address_of__animatorProxy_13() { return &____animatorProxy_13; }
	inline void set__animatorProxy_13(PlayMakerAnimatorMoveProxy_t4175490694 * value)
	{
		____animatorProxy_13 = value;
		Il2CppCodeGenWriteBarrier(&____animatorProxy_13, value);
	}

	inline static int32_t get_offset_of__animator_14() { return static_cast<int32_t>(offsetof(GetAnimatorBool_t3376545269, ____animator_14)); }
	inline Animator_t2776330603 * get__animator_14() const { return ____animator_14; }
	inline Animator_t2776330603 ** get_address_of__animator_14() { return &____animator_14; }
	inline void set__animator_14(Animator_t2776330603 * value)
	{
		____animator_14 = value;
		Il2CppCodeGenWriteBarrier(&____animator_14, value);
	}

	inline static int32_t get_offset_of__paramID_15() { return static_cast<int32_t>(offsetof(GetAnimatorBool_t3376545269, ____paramID_15)); }
	inline int32_t get__paramID_15() const { return ____paramID_15; }
	inline int32_t* get_address_of__paramID_15() { return &____paramID_15; }
	inline void set__paramID_15(int32_t value)
	{
		____paramID_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
