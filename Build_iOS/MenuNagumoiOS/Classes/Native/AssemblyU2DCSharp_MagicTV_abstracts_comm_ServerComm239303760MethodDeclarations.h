﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.abstracts.comm.ServerCommunicationAbstract/OnUpdateRequestCompleteEventHandler
struct OnUpdateRequestCompleteEventHandler_t239303760;
// System.Object
struct Il2CppObject;
// MagicTV.vo.RevisionInfoVO
struct RevisionInfoVO_t1983749152;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_MagicTV_vo_RevisionInfoVO1983749152.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void MagicTV.abstracts.comm.ServerCommunicationAbstract/OnUpdateRequestCompleteEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnUpdateRequestCompleteEventHandler__ctor_m1627402999 (OnUpdateRequestCompleteEventHandler_t239303760 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.comm.ServerCommunicationAbstract/OnUpdateRequestCompleteEventHandler::Invoke(MagicTV.vo.RevisionInfoVO)
extern "C"  void OnUpdateRequestCompleteEventHandler_Invoke_m2659097097 (OnUpdateRequestCompleteEventHandler_t239303760 * __this, RevisionInfoVO_t1983749152 * ___revisionInfoVO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult MagicTV.abstracts.comm.ServerCommunicationAbstract/OnUpdateRequestCompleteEventHandler::BeginInvoke(MagicTV.vo.RevisionInfoVO,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnUpdateRequestCompleteEventHandler_BeginInvoke_m759346974 (OnUpdateRequestCompleteEventHandler_t239303760 * __this, RevisionInfoVO_t1983749152 * ___revisionInfoVO0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.abstracts.comm.ServerCommunicationAbstract/OnUpdateRequestCompleteEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnUpdateRequestCompleteEventHandler_EndInvoke_m1532606599 (OnUpdateRequestCompleteEventHandler_t239303760 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
