﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.globals.events.ContentEvents
struct ContentEvents_t2493264844;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.globals.events.ContentEvents::.ctor()
extern "C"  void ContentEvents__ctor_m39926369 (ContentEvents_t2493264844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ContentEvents::RaiseUpdate()
extern "C"  void ContentEvents_RaiseUpdate_m667767700 (ContentEvents_t2493264844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ContentEvents::RaiseUpdateComplete()
extern "C"  void ContentEvents_RaiseUpdateComplete_m1585099693 (ContentEvents_t2493264844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MagicTV.globals.events.ContentEvents MagicTV.globals.events.ContentEvents::GetInstance()
extern "C"  ContentEvents_t2493264844 * ContentEvents_GetInstance_m183818855 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
