﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericComparer`1<System.Int64>
struct GenericComparer_1_t3619153819;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.Int64>::.ctor()
extern "C"  void GenericComparer_1__ctor_m3905899928_gshared (GenericComparer_1_t3619153819 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m3905899928(__this, method) ((  void (*) (GenericComparer_1_t3619153819 *, const MethodInfo*))GenericComparer_1__ctor_m3905899928_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Int64>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m46862487_gshared (GenericComparer_1_t3619153819 * __this, int64_t ___x0, int64_t ___y1, const MethodInfo* method);
#define GenericComparer_1_Compare_m46862487(__this, ___x0, ___y1, method) ((  int32_t (*) (GenericComparer_1_t3619153819 *, int64_t, int64_t, const MethodInfo*))GenericComparer_1_Compare_m46862487_gshared)(__this, ___x0, ___y1, method)
