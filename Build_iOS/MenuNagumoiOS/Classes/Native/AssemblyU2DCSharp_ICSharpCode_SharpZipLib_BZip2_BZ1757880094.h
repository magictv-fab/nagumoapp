﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.BZip2.BZip2Constants
struct  BZip2Constants_t1757880094  : public Il2CppObject
{
public:

public:
};

struct BZip2Constants_t1757880094_StaticFields
{
public:
	// System.Int32[] ICSharpCode.SharpZipLib.BZip2.BZip2Constants::RandomNumbers
	Int32U5BU5D_t3230847821* ___RandomNumbers_10;

public:
	inline static int32_t get_offset_of_RandomNumbers_10() { return static_cast<int32_t>(offsetof(BZip2Constants_t1757880094_StaticFields, ___RandomNumbers_10)); }
	inline Int32U5BU5D_t3230847821* get_RandomNumbers_10() const { return ___RandomNumbers_10; }
	inline Int32U5BU5D_t3230847821** get_address_of_RandomNumbers_10() { return &___RandomNumbers_10; }
	inline void set_RandomNumbers_10(Int32U5BU5D_t3230847821* value)
	{
		___RandomNumbers_10 = value;
		Il2CppCodeGenWriteBarrier(&___RandomNumbers_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
