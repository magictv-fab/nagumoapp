﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PastePositionAngleFilter
struct  PastePositionAngleFilter_t1444485071  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject PastePositionAngleFilter::objectToPaste
	GameObject_t3674682005 * ___objectToPaste_2;
	// System.Boolean PastePositionAngleFilter::readGlobalPosition
	bool ___readGlobalPosition_3;
	// System.Boolean PastePositionAngleFilter::writeGlobalPosition
	bool ___writeGlobalPosition_4;
	// System.Boolean PastePositionAngleFilter::copyPosition
	bool ___copyPosition_5;
	// System.Boolean PastePositionAngleFilter::copyAngle
	bool ___copyAngle_6;
	// System.Boolean PastePositionAngleFilter::_changeOnLateUpdate
	bool ____changeOnLateUpdate_7;
	// System.Boolean PastePositionAngleFilter::_active
	bool ____active_8;

public:
	inline static int32_t get_offset_of_objectToPaste_2() { return static_cast<int32_t>(offsetof(PastePositionAngleFilter_t1444485071, ___objectToPaste_2)); }
	inline GameObject_t3674682005 * get_objectToPaste_2() const { return ___objectToPaste_2; }
	inline GameObject_t3674682005 ** get_address_of_objectToPaste_2() { return &___objectToPaste_2; }
	inline void set_objectToPaste_2(GameObject_t3674682005 * value)
	{
		___objectToPaste_2 = value;
		Il2CppCodeGenWriteBarrier(&___objectToPaste_2, value);
	}

	inline static int32_t get_offset_of_readGlobalPosition_3() { return static_cast<int32_t>(offsetof(PastePositionAngleFilter_t1444485071, ___readGlobalPosition_3)); }
	inline bool get_readGlobalPosition_3() const { return ___readGlobalPosition_3; }
	inline bool* get_address_of_readGlobalPosition_3() { return &___readGlobalPosition_3; }
	inline void set_readGlobalPosition_3(bool value)
	{
		___readGlobalPosition_3 = value;
	}

	inline static int32_t get_offset_of_writeGlobalPosition_4() { return static_cast<int32_t>(offsetof(PastePositionAngleFilter_t1444485071, ___writeGlobalPosition_4)); }
	inline bool get_writeGlobalPosition_4() const { return ___writeGlobalPosition_4; }
	inline bool* get_address_of_writeGlobalPosition_4() { return &___writeGlobalPosition_4; }
	inline void set_writeGlobalPosition_4(bool value)
	{
		___writeGlobalPosition_4 = value;
	}

	inline static int32_t get_offset_of_copyPosition_5() { return static_cast<int32_t>(offsetof(PastePositionAngleFilter_t1444485071, ___copyPosition_5)); }
	inline bool get_copyPosition_5() const { return ___copyPosition_5; }
	inline bool* get_address_of_copyPosition_5() { return &___copyPosition_5; }
	inline void set_copyPosition_5(bool value)
	{
		___copyPosition_5 = value;
	}

	inline static int32_t get_offset_of_copyAngle_6() { return static_cast<int32_t>(offsetof(PastePositionAngleFilter_t1444485071, ___copyAngle_6)); }
	inline bool get_copyAngle_6() const { return ___copyAngle_6; }
	inline bool* get_address_of_copyAngle_6() { return &___copyAngle_6; }
	inline void set_copyAngle_6(bool value)
	{
		___copyAngle_6 = value;
	}

	inline static int32_t get_offset_of__changeOnLateUpdate_7() { return static_cast<int32_t>(offsetof(PastePositionAngleFilter_t1444485071, ____changeOnLateUpdate_7)); }
	inline bool get__changeOnLateUpdate_7() const { return ____changeOnLateUpdate_7; }
	inline bool* get_address_of__changeOnLateUpdate_7() { return &____changeOnLateUpdate_7; }
	inline void set__changeOnLateUpdate_7(bool value)
	{
		____changeOnLateUpdate_7 = value;
	}

	inline static int32_t get_offset_of__active_8() { return static_cast<int32_t>(offsetof(PastePositionAngleFilter_t1444485071, ____active_8)); }
	inline bool get__active_8() const { return ____active_8; }
	inline bool* get_address_of__active_8() { return &____active_8; }
	inline void set__active_8(bool value)
	{
		____active_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
