﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkIsClient
struct NetworkIsClient_t2739991281;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkIsClient::.ctor()
extern "C"  void NetworkIsClient__ctor_m2102985573 (NetworkIsClient_t2739991281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkIsClient::Reset()
extern "C"  void NetworkIsClient_Reset_m4044385810 (NetworkIsClient_t2739991281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkIsClient::OnEnter()
extern "C"  void NetworkIsClient_OnEnter_m1726235132 (NetworkIsClient_t2739991281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkIsClient::DoCheckIsClient()
extern "C"  void NetworkIsClient_DoCheckIsClient_m2042232181 (NetworkIsClient_t2739991281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
