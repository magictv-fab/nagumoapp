﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Tar.TarEntry
struct TarEntry_t762185187;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// ICSharpCode.SharpZipLib.Tar.TarHeader
struct TarHeader_t2980539316;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// ICSharpCode.SharpZipLib.Tar.TarEntry[]
struct TarEntryU5BU5D_t277262130;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Tar_TarH2980539316.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Tar_TarEn762185187.h"
#include "mscorlib_System_DateTime4283661327.h"

// System.Void ICSharpCode.SharpZipLib.Tar.TarEntry::.ctor()
extern "C"  void TarEntry__ctor_m1188430996 (TarEntry_t762185187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarEntry::.ctor(System.Byte[])
extern "C"  void TarEntry__ctor_m1812138933 (TarEntry_t762185187 * __this, ByteU5BU5D_t4260760469* ___headerBuffer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarEntry::.ctor(ICSharpCode.SharpZipLib.Tar.TarHeader)
extern "C"  void TarEntry__ctor_m2565745126 (TarEntry_t762185187 * __this, TarHeader_t2980539316 * ___header0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ICSharpCode.SharpZipLib.Tar.TarEntry::Clone()
extern "C"  Il2CppObject * TarEntry_Clone_m2721524026 (TarEntry_t762185187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Tar.TarEntry ICSharpCode.SharpZipLib.Tar.TarEntry::CreateTarEntry(System.String)
extern "C"  TarEntry_t762185187 * TarEntry_CreateTarEntry_m3003376531 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Tar.TarEntry ICSharpCode.SharpZipLib.Tar.TarEntry::CreateEntryFromFile(System.String)
extern "C"  TarEntry_t762185187 * TarEntry_CreateEntryFromFile_m905632202 (Il2CppObject * __this /* static, unused */, String_t* ___fileName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Tar.TarEntry::Equals(System.Object)
extern "C"  bool TarEntry_Equals_m2929065041 (TarEntry_t762185187 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarEntry::GetHashCode()
extern "C"  int32_t TarEntry_GetHashCode_m1753692533 (TarEntry_t762185187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Tar.TarEntry::IsDescendent(ICSharpCode.SharpZipLib.Tar.TarEntry)
extern "C"  bool TarEntry_IsDescendent_m701918696 (TarEntry_t762185187 * __this, TarEntry_t762185187 * ___toTest0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Tar.TarHeader ICSharpCode.SharpZipLib.Tar.TarEntry::get_TarHeader()
extern "C"  TarHeader_t2980539316 * TarEntry_get_TarHeader_m1195240224 (TarEntry_t762185187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Tar.TarEntry::get_Name()
extern "C"  String_t* TarEntry_get_Name_m2237469607 (TarEntry_t762185187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarEntry::set_Name(System.String)
extern "C"  void TarEntry_set_Name_m137320714 (TarEntry_t762185187 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarEntry::get_UserId()
extern "C"  int32_t TarEntry_get_UserId_m125399309 (TarEntry_t762185187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarEntry::set_UserId(System.Int32)
extern "C"  void TarEntry_set_UserId_m619892676 (TarEntry_t762185187 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarEntry::get_GroupId()
extern "C"  int32_t TarEntry_get_GroupId_m2085387253 (TarEntry_t762185187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarEntry::set_GroupId(System.Int32)
extern "C"  void TarEntry_set_GroupId_m1932767648 (TarEntry_t762185187 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Tar.TarEntry::get_UserName()
extern "C"  String_t* TarEntry_get_UserName_m1936114130 (TarEntry_t762185187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarEntry::set_UserName(System.String)
extern "C"  void TarEntry_set_UserName_m2269612351 (TarEntry_t762185187 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Tar.TarEntry::get_GroupName()
extern "C"  String_t* TarEntry_get_GroupName_m3363395120 (TarEntry_t762185187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarEntry::set_GroupName(System.String)
extern "C"  void TarEntry_set_GroupName_m4155705827 (TarEntry_t762185187 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarEntry::SetIds(System.Int32,System.Int32)
extern "C"  void TarEntry_SetIds_m2670731776 (TarEntry_t762185187 * __this, int32_t ___userId0, int32_t ___groupId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarEntry::SetNames(System.String,System.String)
extern "C"  void TarEntry_SetNames_m4232414408 (TarEntry_t762185187 * __this, String_t* ___userName0, String_t* ___groupName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime ICSharpCode.SharpZipLib.Tar.TarEntry::get_ModTime()
extern "C"  DateTime_t4283661327  TarEntry_get_ModTime_m534743807 (TarEntry_t762185187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarEntry::set_ModTime(System.DateTime)
extern "C"  void TarEntry_set_ModTime_m1431227188 (TarEntry_t762185187 * __this, DateTime_t4283661327  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ICSharpCode.SharpZipLib.Tar.TarEntry::get_File()
extern "C"  String_t* TarEntry_get_File_m2015794776 (TarEntry_t762185187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Tar.TarEntry::get_Size()
extern "C"  int64_t TarEntry_get_Size_m1824808713 (TarEntry_t762185187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarEntry::set_Size(System.Int64)
extern "C"  void TarEntry_set_Size_m3073197248 (TarEntry_t762185187 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Tar.TarEntry::get_IsDirectory()
extern "C"  bool TarEntry_get_IsDirectory_m1774792120 (TarEntry_t762185187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarEntry::GetFileTarHeader(ICSharpCode.SharpZipLib.Tar.TarHeader,System.String)
extern "C"  void TarEntry_GetFileTarHeader_m3479027270 (TarEntry_t762185187 * __this, TarHeader_t2980539316 * ___header0, String_t* ___file1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICSharpCode.SharpZipLib.Tar.TarEntry[] ICSharpCode.SharpZipLib.Tar.TarEntry::GetDirectoryEntries()
extern "C"  TarEntryU5BU5D_t277262130* TarEntry_GetDirectoryEntries_m3944030839 (TarEntry_t762185187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarEntry::WriteEntryHeader(System.Byte[])
extern "C"  void TarEntry_WriteEntryHeader_m1567868377 (TarEntry_t762185187 * __this, ByteU5BU5D_t4260760469* ___outBuffer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarEntry::AdjustEntryName(System.Byte[],System.String)
extern "C"  void TarEntry_AdjustEntryName_m4282048517 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___buffer0, String_t* ___newName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarEntry::NameTarHeader(ICSharpCode.SharpZipLib.Tar.TarHeader,System.String)
extern "C"  void TarEntry_NameTarHeader_m3561605341 (Il2CppObject * __this /* static, unused */, TarHeader_t2980539316 * ___header0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
