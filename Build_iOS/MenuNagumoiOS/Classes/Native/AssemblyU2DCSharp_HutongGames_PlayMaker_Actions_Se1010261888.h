﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Com715545838.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAudioLoop
struct  SetAudioLoop_t1010261888  : public ComponentAction_1_t715545838
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAudioLoop::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetAudioLoop::loop
	FsmBool_t1075959796 * ___loop_12;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(SetAudioLoop_t1010261888, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_loop_12() { return static_cast<int32_t>(offsetof(SetAudioLoop_t1010261888, ___loop_12)); }
	inline FsmBool_t1075959796 * get_loop_12() const { return ___loop_12; }
	inline FsmBool_t1075959796 ** get_address_of_loop_12() { return &___loop_12; }
	inline void set_loop_12(FsmBool_t1075959796 * value)
	{
		___loop_12 = value;
		Il2CppCodeGenWriteBarrier(&___loop_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
