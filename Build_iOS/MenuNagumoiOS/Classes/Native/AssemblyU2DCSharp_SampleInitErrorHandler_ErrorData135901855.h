﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SampleInitErrorHandler/ErrorData
struct  ErrorData_t135901855 
{
public:
	// System.String SampleInitErrorHandler/ErrorData::Title
	String_t* ___Title_0;
	// System.String SampleInitErrorHandler/ErrorData::Text
	String_t* ___Text_1;

public:
	inline static int32_t get_offset_of_Title_0() { return static_cast<int32_t>(offsetof(ErrorData_t135901855, ___Title_0)); }
	inline String_t* get_Title_0() const { return ___Title_0; }
	inline String_t** get_address_of_Title_0() { return &___Title_0; }
	inline void set_Title_0(String_t* value)
	{
		___Title_0 = value;
		Il2CppCodeGenWriteBarrier(&___Title_0, value);
	}

	inline static int32_t get_offset_of_Text_1() { return static_cast<int32_t>(offsetof(ErrorData_t135901855, ___Text_1)); }
	inline String_t* get_Text_1() const { return ___Text_1; }
	inline String_t** get_address_of_Text_1() { return &___Text_1; }
	inline void set_Text_1(String_t* value)
	{
		___Text_1 = value;
		Il2CppCodeGenWriteBarrier(&___Text_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: SampleInitErrorHandler/ErrorData
struct ErrorData_t135901855_marshaled_pinvoke
{
	char* ___Title_0;
	char* ___Text_1;
};
// Native definition for marshalling of: SampleInitErrorHandler/ErrorData
struct ErrorData_t135901855_marshaled_com
{
	Il2CppChar* ___Title_0;
	Il2CppChar* ___Text_1;
};
