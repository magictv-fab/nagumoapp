﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Tar.TarOutputStream
struct TarOutputStream_t1551862984;
// System.IO.Stream
struct Stream_t1561764144;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// ICSharpCode.SharpZipLib.Tar.TarEntry
struct TarEntry_t762185187;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "mscorlib_System_IO_SeekOrigin4120335598.h"
#include "AssemblyU2DCSharp_ICSharpCode_SharpZipLib_Tar_TarEn762185187.h"

// System.Void ICSharpCode.SharpZipLib.Tar.TarOutputStream::.ctor(System.IO.Stream)
extern "C"  void TarOutputStream__ctor_m1345603766 (TarOutputStream_t1551862984 * __this, Stream_t1561764144 * ___outputStream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarOutputStream::.ctor(System.IO.Stream,System.Int32)
extern "C"  void TarOutputStream__ctor_m2278632417 (TarOutputStream_t1551862984 * __this, Stream_t1561764144 * ___outputStream0, int32_t ___blockFactor1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Tar.TarOutputStream::get_IsStreamOwner()
extern "C"  bool TarOutputStream_get_IsStreamOwner_m3075867729 (TarOutputStream_t1551862984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarOutputStream::set_IsStreamOwner(System.Boolean)
extern "C"  void TarOutputStream_set_IsStreamOwner_m677815392 (TarOutputStream_t1551862984 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Tar.TarOutputStream::get_CanRead()
extern "C"  bool TarOutputStream_get_CanRead_m61812814 (TarOutputStream_t1551862984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Tar.TarOutputStream::get_CanSeek()
extern "C"  bool TarOutputStream_get_CanSeek_m90567856 (TarOutputStream_t1551862984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Tar.TarOutputStream::get_CanWrite()
extern "C"  bool TarOutputStream_get_CanWrite_m2438850761 (TarOutputStream_t1551862984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Tar.TarOutputStream::get_Length()
extern "C"  int64_t TarOutputStream_get_Length_m406148037 (TarOutputStream_t1551862984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Tar.TarOutputStream::get_Position()
extern "C"  int64_t TarOutputStream_get_Position_m3982708168 (TarOutputStream_t1551862984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarOutputStream::set_Position(System.Int64)
extern "C"  void TarOutputStream_set_Position_m886000765 (TarOutputStream_t1551862984 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Tar.TarOutputStream::Seek(System.Int64,System.IO.SeekOrigin)
extern "C"  int64_t TarOutputStream_Seek_m2010170623 (TarOutputStream_t1551862984 * __this, int64_t ___offset0, int32_t ___origin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarOutputStream::SetLength(System.Int64)
extern "C"  void TarOutputStream_SetLength_m3577213047 (TarOutputStream_t1551862984 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarOutputStream::ReadByte()
extern "C"  int32_t TarOutputStream_ReadByte_m1457614261 (TarOutputStream_t1551862984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarOutputStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t TarOutputStream_Read_m1077083836 (TarOutputStream_t1551862984 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarOutputStream::Flush()
extern "C"  void TarOutputStream_Flush_m3758296897 (TarOutputStream_t1551862984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarOutputStream::Finish()
extern "C"  void TarOutputStream_Finish_m1966293016 (TarOutputStream_t1551862984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarOutputStream::Close()
extern "C"  void TarOutputStream_Close_m1090241845 (TarOutputStream_t1551862984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarOutputStream::get_RecordSize()
extern "C"  int32_t TarOutputStream_get_RecordSize_m718488242 (TarOutputStream_t1551862984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Tar.TarOutputStream::GetRecordSize()
extern "C"  int32_t TarOutputStream_GetRecordSize_m3243638835 (TarOutputStream_t1551862984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Tar.TarOutputStream::get_IsEntryOpen()
extern "C"  bool TarOutputStream_get_IsEntryOpen_m1901611034 (TarOutputStream_t1551862984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarOutputStream::PutNextEntry(ICSharpCode.SharpZipLib.Tar.TarEntry)
extern "C"  void TarOutputStream_PutNextEntry_m384480558 (TarOutputStream_t1551862984 * __this, TarEntry_t762185187 * ___entry0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarOutputStream::CloseEntry()
extern "C"  void TarOutputStream_CloseEntry_m594776351 (TarOutputStream_t1551862984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarOutputStream::WriteByte(System.Byte)
extern "C"  void TarOutputStream_WriteByte_m3500193255 (TarOutputStream_t1551862984 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarOutputStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C"  void TarOutputStream_Write_m3389475597 (TarOutputStream_t1551862984 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Tar.TarOutputStream::WriteEofBlock()
extern "C"  void TarOutputStream_WriteEofBlock_m792314669 (TarOutputStream_t1551862984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
