﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t2131419205;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_De1460219571.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DebugDrawShape
struct  DebugDrawShape_t3550345874  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.DebugDrawShape::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_9;
	// HutongGames.PlayMaker.Actions.DebugDrawShape/ShapeType HutongGames.PlayMaker.Actions.DebugDrawShape::shape
	int32_t ___shape_10;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.DebugDrawShape::color
	FsmColor_t2131419205 * ___color_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DebugDrawShape::radius
	FsmFloat_t2134102846 * ___radius_12;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.DebugDrawShape::size
	FsmVector3_t533912882 * ___size_13;

public:
	inline static int32_t get_offset_of_gameObject_9() { return static_cast<int32_t>(offsetof(DebugDrawShape_t3550345874, ___gameObject_9)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_9() const { return ___gameObject_9; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_9() { return &___gameObject_9; }
	inline void set_gameObject_9(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_9, value);
	}

	inline static int32_t get_offset_of_shape_10() { return static_cast<int32_t>(offsetof(DebugDrawShape_t3550345874, ___shape_10)); }
	inline int32_t get_shape_10() const { return ___shape_10; }
	inline int32_t* get_address_of_shape_10() { return &___shape_10; }
	inline void set_shape_10(int32_t value)
	{
		___shape_10 = value;
	}

	inline static int32_t get_offset_of_color_11() { return static_cast<int32_t>(offsetof(DebugDrawShape_t3550345874, ___color_11)); }
	inline FsmColor_t2131419205 * get_color_11() const { return ___color_11; }
	inline FsmColor_t2131419205 ** get_address_of_color_11() { return &___color_11; }
	inline void set_color_11(FsmColor_t2131419205 * value)
	{
		___color_11 = value;
		Il2CppCodeGenWriteBarrier(&___color_11, value);
	}

	inline static int32_t get_offset_of_radius_12() { return static_cast<int32_t>(offsetof(DebugDrawShape_t3550345874, ___radius_12)); }
	inline FsmFloat_t2134102846 * get_radius_12() const { return ___radius_12; }
	inline FsmFloat_t2134102846 ** get_address_of_radius_12() { return &___radius_12; }
	inline void set_radius_12(FsmFloat_t2134102846 * value)
	{
		___radius_12 = value;
		Il2CppCodeGenWriteBarrier(&___radius_12, value);
	}

	inline static int32_t get_offset_of_size_13() { return static_cast<int32_t>(offsetof(DebugDrawShape_t3550345874, ___size_13)); }
	inline FsmVector3_t533912882 * get_size_13() const { return ___size_13; }
	inline FsmVector3_t533912882 ** get_address_of_size_13() { return &___size_13; }
	inline void set_size_13(FsmVector3_t533912882 * value)
	{
		___size_13 = value;
		Il2CppCodeGenWriteBarrier(&___size_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
