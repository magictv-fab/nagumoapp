﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FakeUpdateProcessController
struct FakeUpdateProcessController_t4246497901;

#include "codegen/il2cpp-codegen.h"

// System.Void FakeUpdateProcessController::.ctor()
extern "C"  void FakeUpdateProcessController__ctor_m4017662878 (FakeUpdateProcessController_t4246497901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FakeUpdateProcessController::prepare()
extern "C"  void FakeUpdateProcessController_prepare_m725407075 (FakeUpdateProcessController_t4246497901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FakeUpdateProcessController::Play()
extern "C"  void FakeUpdateProcessController_Play_m1110746138 (FakeUpdateProcessController_t4246497901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FakeUpdateProcessController::Stop()
extern "C"  void FakeUpdateProcessController_Stop_m1204430184 (FakeUpdateProcessController_t4246497901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FakeUpdateProcessController::Pause()
extern "C"  void FakeUpdateProcessController_Pause_m4071788850 (FakeUpdateProcessController_t4246497901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
