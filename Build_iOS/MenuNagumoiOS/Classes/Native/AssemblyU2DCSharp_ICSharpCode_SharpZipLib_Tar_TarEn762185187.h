﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// ICSharpCode.SharpZipLib.Tar.TarHeader
struct TarHeader_t2980539316;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Tar.TarEntry
struct  TarEntry_t762185187  : public Il2CppObject
{
public:
	// System.String ICSharpCode.SharpZipLib.Tar.TarEntry::file
	String_t* ___file_0;
	// ICSharpCode.SharpZipLib.Tar.TarHeader ICSharpCode.SharpZipLib.Tar.TarEntry::header
	TarHeader_t2980539316 * ___header_1;

public:
	inline static int32_t get_offset_of_file_0() { return static_cast<int32_t>(offsetof(TarEntry_t762185187, ___file_0)); }
	inline String_t* get_file_0() const { return ___file_0; }
	inline String_t** get_address_of_file_0() { return &___file_0; }
	inline void set_file_0(String_t* value)
	{
		___file_0 = value;
		Il2CppCodeGenWriteBarrier(&___file_0, value);
	}

	inline static int32_t get_offset_of_header_1() { return static_cast<int32_t>(offsetof(TarEntry_t762185187, ___header_1)); }
	inline TarHeader_t2980539316 * get_header_1() const { return ___header_1; }
	inline TarHeader_t2980539316 ** get_address_of_header_1() { return &___header_1; }
	inline void set_header_1(TarHeader_t2980539316 * value)
	{
		___header_1 = value;
		Il2CppCodeGenWriteBarrier(&___header_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
