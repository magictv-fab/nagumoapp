﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FloatChanged
struct  FloatChanged_t1905781472  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatChanged::floatVariable
	FsmFloat_t2134102846 * ___floatVariable_9;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.FloatChanged::changedEvent
	FsmEvent_t2133468028 * ___changedEvent_10;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.FloatChanged::storeResult
	FsmBool_t1075959796 * ___storeResult_11;
	// System.Single HutongGames.PlayMaker.Actions.FloatChanged::previousValue
	float ___previousValue_12;

public:
	inline static int32_t get_offset_of_floatVariable_9() { return static_cast<int32_t>(offsetof(FloatChanged_t1905781472, ___floatVariable_9)); }
	inline FsmFloat_t2134102846 * get_floatVariable_9() const { return ___floatVariable_9; }
	inline FsmFloat_t2134102846 ** get_address_of_floatVariable_9() { return &___floatVariable_9; }
	inline void set_floatVariable_9(FsmFloat_t2134102846 * value)
	{
		___floatVariable_9 = value;
		Il2CppCodeGenWriteBarrier(&___floatVariable_9, value);
	}

	inline static int32_t get_offset_of_changedEvent_10() { return static_cast<int32_t>(offsetof(FloatChanged_t1905781472, ___changedEvent_10)); }
	inline FsmEvent_t2133468028 * get_changedEvent_10() const { return ___changedEvent_10; }
	inline FsmEvent_t2133468028 ** get_address_of_changedEvent_10() { return &___changedEvent_10; }
	inline void set_changedEvent_10(FsmEvent_t2133468028 * value)
	{
		___changedEvent_10 = value;
		Il2CppCodeGenWriteBarrier(&___changedEvent_10, value);
	}

	inline static int32_t get_offset_of_storeResult_11() { return static_cast<int32_t>(offsetof(FloatChanged_t1905781472, ___storeResult_11)); }
	inline FsmBool_t1075959796 * get_storeResult_11() const { return ___storeResult_11; }
	inline FsmBool_t1075959796 ** get_address_of_storeResult_11() { return &___storeResult_11; }
	inline void set_storeResult_11(FsmBool_t1075959796 * value)
	{
		___storeResult_11 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_11, value);
	}

	inline static int32_t get_offset_of_previousValue_12() { return static_cast<int32_t>(offsetof(FloatChanged_t1905781472, ___previousValue_12)); }
	inline float get_previousValue_12() const { return ___previousValue_12; }
	inline float* get_address_of_previousValue_12() { return &___previousValue_12; }
	inline void set_previousValue_12(float value)
	{
		___previousValue_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
