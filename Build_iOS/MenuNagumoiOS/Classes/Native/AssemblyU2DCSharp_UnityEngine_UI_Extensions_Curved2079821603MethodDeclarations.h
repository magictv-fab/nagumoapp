﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Extensions.CurvedLayout
struct CurvedLayout_t2079821603;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.Extensions.CurvedLayout::.ctor()
extern "C"  void CurvedLayout__ctor_m4204440549 (CurvedLayout_t2079821603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.CurvedLayout::OnEnable()
extern "C"  void CurvedLayout_OnEnable_m2428720513 (CurvedLayout_t2079821603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.CurvedLayout::SetLayoutHorizontal()
extern "C"  void CurvedLayout_SetLayoutHorizontal_m2159000819 (CurvedLayout_t2079821603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.CurvedLayout::SetLayoutVertical()
extern "C"  void CurvedLayout_SetLayoutVertical_m1716250437 (CurvedLayout_t2079821603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.CurvedLayout::CalculateLayoutInputVertical()
extern "C"  void CurvedLayout_CalculateLayoutInputVertical_m880198607 (CurvedLayout_t2079821603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.CurvedLayout::CalculateLayoutInputHorizontal()
extern "C"  void CurvedLayout_CalculateLayoutInputHorizontal_m1872076541 (CurvedLayout_t2079821603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Extensions.CurvedLayout::CalculateRadial()
extern "C"  void CurvedLayout_CalculateRadial_m110982760 (CurvedLayout_t2079821603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
