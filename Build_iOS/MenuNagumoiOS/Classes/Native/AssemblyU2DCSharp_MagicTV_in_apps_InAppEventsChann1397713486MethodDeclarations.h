﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.in_apps.InAppEventsChannel
struct InAppEventsChannel_t1397713486;
// System.String
struct String_t;
// ARM.events.VoidEventDispatcher
struct VoidEventDispatcher_t1760461203;
// ARM.events.GenericParameterEventDispatcher
struct GenericParameterEventDispatcher_t3811317845;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void MagicTV.in_apps.InAppEventsChannel::.ctor()
extern "C"  void InAppEventsChannel__ctor_m3023087788 (InAppEventsChannel_t1397713486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MagicTV.in_apps.InAppEventsChannel::GetFloatValue(System.String)
extern "C"  float InAppEventsChannel_GetFloatValue_m3910871929 (InAppEventsChannel_t1397713486 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppEventsChannel::SetFloatValue(System.String,System.Single)
extern "C"  void InAppEventsChannel_SetFloatValue_m934790214 (InAppEventsChannel_t1397713486 * __this, String_t* ___key0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.in_apps.InAppEventsChannel::GetStringValue(System.String)
extern "C"  String_t* InAppEventsChannel_GetStringValue_m2885889533 (InAppEventsChannel_t1397713486 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppEventsChannel::SetStringValue(System.String,System.String)
extern "C"  void InAppEventsChannel_SetStringValue_m2013058312 (InAppEventsChannel_t1397713486 * __this, String_t* ___key0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MagicTV.in_apps.InAppEventsChannel::GetIntValue(System.String)
extern "C"  int32_t InAppEventsChannel_GetIntValue_m1944178318 (InAppEventsChannel_t1397713486 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppEventsChannel::SetIntValue(System.String,System.Int32)
extern "C"  void InAppEventsChannel_SetIntValue_m3838403299 (InAppEventsChannel_t1397713486 * __this, String_t* ___key0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ARM.events.VoidEventDispatcher MagicTV.in_apps.InAppEventsChannel::GetVoidEventHandler(System.String)
extern "C"  VoidEventDispatcher_t1760461203 * InAppEventsChannel_GetVoidEventHandler_m2255131217 (InAppEventsChannel_t1397713486 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ARM.events.GenericParameterEventDispatcher MagicTV.in_apps.InAppEventsChannel::GetGenericParameterEventHandler(System.String)
extern "C"  GenericParameterEventDispatcher_t3811317845 * InAppEventsChannel_GetGenericParameterEventHandler_m244419089 (InAppEventsChannel_t1397713486 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
