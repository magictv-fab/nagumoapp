﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// MagicTV.vo.UrlInfoVO[]
struct UrlInfoVOU5BU5D_t1681515545;

#include "AssemblyU2DCSharp_MagicTV_vo_VOWithId2461972856.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.vo.FileVO
struct  FileVO_t1935348659  : public VOWithId_t2461972856
{
public:
	// System.String MagicTV.vo.FileVO::bundle_id
	String_t* ___bundle_id_1;
	// System.String MagicTV.vo.FileVO::type
	String_t* ___type_2;
	// MagicTV.vo.UrlInfoVO[] MagicTV.vo.FileVO::urls
	UrlInfoVOU5BU5D_t1681515545* ___urls_3;

public:
	inline static int32_t get_offset_of_bundle_id_1() { return static_cast<int32_t>(offsetof(FileVO_t1935348659, ___bundle_id_1)); }
	inline String_t* get_bundle_id_1() const { return ___bundle_id_1; }
	inline String_t** get_address_of_bundle_id_1() { return &___bundle_id_1; }
	inline void set_bundle_id_1(String_t* value)
	{
		___bundle_id_1 = value;
		Il2CppCodeGenWriteBarrier(&___bundle_id_1, value);
	}

	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(FileVO_t1935348659, ___type_2)); }
	inline String_t* get_type_2() const { return ___type_2; }
	inline String_t** get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(String_t* value)
	{
		___type_2 = value;
		Il2CppCodeGenWriteBarrier(&___type_2, value);
	}

	inline static int32_t get_offset_of_urls_3() { return static_cast<int32_t>(offsetof(FileVO_t1935348659, ___urls_3)); }
	inline UrlInfoVOU5BU5D_t1681515545* get_urls_3() const { return ___urls_3; }
	inline UrlInfoVOU5BU5D_t1681515545** get_address_of_urls_3() { return &___urls_3; }
	inline void set_urls_3(UrlInfoVOU5BU5D_t1681515545* value)
	{
		___urls_3 = value;
		Il2CppCodeGenWriteBarrier(&___urls_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
