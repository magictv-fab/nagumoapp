﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.RSS.Expanded.Decoders.BlockParsedResult
struct BlockParsedResult_t1963745509;
// ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation
struct DecodedInformation_t1859732120;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_OneD_RSS_Expanded_Decoders_DecodedInf1859732120.h"

// System.Void ZXing.OneD.RSS.Expanded.Decoders.BlockParsedResult::.ctor(System.Boolean)
extern "C"  void BlockParsedResult__ctor_m2710457923 (BlockParsedResult_t1963745509 * __this, bool ___finished0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.OneD.RSS.Expanded.Decoders.BlockParsedResult::.ctor(ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation,System.Boolean)
extern "C"  void BlockParsedResult__ctor_m2122459929 (BlockParsedResult_t1963745509 * __this, DecodedInformation_t1859732120 * ___information0, bool ___finished1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation ZXing.OneD.RSS.Expanded.Decoders.BlockParsedResult::getDecodedInformation()
extern "C"  DecodedInformation_t1859732120 * BlockParsedResult_getDecodedInformation_m3457018789 (BlockParsedResult_t1963745509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.RSS.Expanded.Decoders.BlockParsedResult::isFinished()
extern "C"  bool BlockParsedResult_isFinished_m1014782280 (BlockParsedResult_t1963745509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
