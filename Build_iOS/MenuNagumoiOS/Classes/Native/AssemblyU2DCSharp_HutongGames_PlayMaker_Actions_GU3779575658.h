﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2615417833.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutTextLabel
struct  GUILayoutTextLabel_t3779575658  : public GUILayoutAction_t2615417833
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutTextLabel::text
	FsmString_t952858651 * ___text_11;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutTextLabel::style
	FsmString_t952858651 * ___style_12;

public:
	inline static int32_t get_offset_of_text_11() { return static_cast<int32_t>(offsetof(GUILayoutTextLabel_t3779575658, ___text_11)); }
	inline FsmString_t952858651 * get_text_11() const { return ___text_11; }
	inline FsmString_t952858651 ** get_address_of_text_11() { return &___text_11; }
	inline void set_text_11(FsmString_t952858651 * value)
	{
		___text_11 = value;
		Il2CppCodeGenWriteBarrier(&___text_11, value);
	}

	inline static int32_t get_offset_of_style_12() { return static_cast<int32_t>(offsetof(GUILayoutTextLabel_t3779575658, ___style_12)); }
	inline FsmString_t952858651 * get_style_12() const { return ___style_12; }
	inline FsmString_t952858651 ** get_address_of_style_12() { return &___style_12; }
	inline void set_style_12(FsmString_t952858651 * value)
	{
		___style_12 = value;
		Il2CppCodeGenWriteBarrier(&___style_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
