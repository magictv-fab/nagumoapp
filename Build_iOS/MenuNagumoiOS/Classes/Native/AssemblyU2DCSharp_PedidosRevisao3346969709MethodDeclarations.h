﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PedidosRevisao
struct PedidosRevisao_t3346969709;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void PedidosRevisao::.ctor()
extern "C"  void PedidosRevisao__ctor_m3542532814 (PedidosRevisao_t3346969709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PedidosRevisao::.cctor()
extern "C"  void PedidosRevisao__cctor_m1962238623 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PedidosRevisao::Start()
extern "C"  void PedidosRevisao_Start_m2489670606 (PedidosRevisao_t3346969709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PedidosRevisao::BtnAvancar()
extern "C"  void PedidosRevisao_BtnAvancar_m2709268204 (PedidosRevisao_t3346969709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PedidosRevisao::AceitaPassarPeso(System.Boolean)
extern "C"  void PedidosRevisao_AceitaPassarPeso_m3208392691 (PedidosRevisao_t3346969709 * __this, bool ___aceitaPassarPeso0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PedidosRevisao::IEnviarPedido(System.Boolean)
extern "C"  Il2CppObject * PedidosRevisao_IEnviarPedido_m1786025238 (PedidosRevisao_t3346969709 * __this, bool ___aceitaPassarPeso0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PedidosRevisao::PopUp(System.String)
extern "C"  void PedidosRevisao_PopUp_m84777034 (PedidosRevisao_t3346969709 * __this, String_t* ___txt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
