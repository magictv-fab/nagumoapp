﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.in_apps.InAppImagePresentation
struct InAppImagePresentation_t2058331705;
// MagicTV.vo.BundleVO
struct BundleVO_t1984518073;
// ARM.utils.request.ARMRequestVO
struct ARMRequestVO_t2431191322;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MagicTV_vo_BundleVO1984518073.h"
#include "AssemblyU2DCSharp_ARM_utils_request_ARMRequestVO2431191322.h"

// System.Void MagicTV.in_apps.InAppImagePresentation::.ctor()
extern "C"  void InAppImagePresentation__ctor_m3778795809 (InAppImagePresentation_t2058331705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppImagePresentation::DoRun()
extern "C"  void InAppImagePresentation_DoRun_m2141365311 (InAppImagePresentation_t2058331705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppImagePresentation::SetBundleVO(MagicTV.vo.BundleVO)
extern "C"  void InAppImagePresentation_SetBundleVO_m1316109829 (InAppImagePresentation_t2058331705 * __this, BundleVO_t1984518073 * ___bundle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppImagePresentation::applyConfig(MagicTV.vo.BundleVO)
extern "C"  void InAppImagePresentation_applyConfig_m3306556594 (InAppImagePresentation_t2058331705 * __this, BundleVO_t1984518073 * ___bundle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppImagePresentation::prepare()
extern "C"  void InAppImagePresentation_prepare_m3102387750 (InAppImagePresentation_t2058331705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppImagePresentation::prepareFile()
extern "C"  void InAppImagePresentation_prepareFile_m15009346 (InAppImagePresentation_t2058331705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppImagePresentation::Update()
extern "C"  void InAppImagePresentation_Update_m2905415180 (InAppImagePresentation_t2058331705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppImagePresentation::onFileComplete(ARM.utils.request.ARMRequestVO)
extern "C"  void InAppImagePresentation_onFileComplete_m3082356157 (InAppImagePresentation_t2058331705 * __this, ARMRequestVO_t2431191322 * ___vo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppImagePresentation::DoStop()
extern "C"  void InAppImagePresentation_DoStop_m1985619824 (InAppImagePresentation_t2058331705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.in_apps.InAppImagePresentation::Dispose()
extern "C"  void InAppImagePresentation_Dispose_m2073690398 (InAppImagePresentation_t2058331705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.in_apps.InAppImagePresentation::GetInAppName()
extern "C"  String_t* InAppImagePresentation_GetInAppName_m3759754327 (InAppImagePresentation_t2058331705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
