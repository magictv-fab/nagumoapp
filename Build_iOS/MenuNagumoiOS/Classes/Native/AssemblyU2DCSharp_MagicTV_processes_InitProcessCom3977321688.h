﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ARM.components.ComponentsStarter
struct ComponentsStarter_t1779695339;

#include "AssemblyU2DCSharp_ARM_animation_GenericStartableCo1794600275.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.processes.InitProcessComponent
struct  InitProcessComponent_t3977321688  : public GenericStartableComponentControllAbstract_t1794600275
{
public:
	// ARM.components.ComponentsStarter MagicTV.processes.InitProcessComponent::_componentStarter
	ComponentsStarter_t1779695339 * ____componentStarter_8;

public:
	inline static int32_t get_offset_of__componentStarter_8() { return static_cast<int32_t>(offsetof(InitProcessComponent_t3977321688, ____componentStarter_8)); }
	inline ComponentsStarter_t1779695339 * get__componentStarter_8() const { return ____componentStarter_8; }
	inline ComponentsStarter_t1779695339 ** get_address_of__componentStarter_8() { return &____componentStarter_8; }
	inline void set__componentStarter_8(ComponentsStarter_t1779695339 * value)
	{
		____componentStarter_8 = value;
		Il2CppCodeGenWriteBarrier(&____componentStarter_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
