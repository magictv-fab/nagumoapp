﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Common.StringUtils
struct  StringUtils_t3420815774  : public Il2CppObject
{
public:

public:
};

struct StringUtils_t3420815774_StaticFields
{
public:
	// System.String ZXing.Common.StringUtils::PLATFORM_DEFAULT_ENCODING
	String_t* ___PLATFORM_DEFAULT_ENCODING_0;
	// System.String ZXing.Common.StringUtils::SHIFT_JIS
	String_t* ___SHIFT_JIS_1;
	// System.String ZXing.Common.StringUtils::GB2312
	String_t* ___GB2312_2;
	// System.Boolean ZXing.Common.StringUtils::ASSUME_SHIFT_JIS
	bool ___ASSUME_SHIFT_JIS_3;

public:
	inline static int32_t get_offset_of_PLATFORM_DEFAULT_ENCODING_0() { return static_cast<int32_t>(offsetof(StringUtils_t3420815774_StaticFields, ___PLATFORM_DEFAULT_ENCODING_0)); }
	inline String_t* get_PLATFORM_DEFAULT_ENCODING_0() const { return ___PLATFORM_DEFAULT_ENCODING_0; }
	inline String_t** get_address_of_PLATFORM_DEFAULT_ENCODING_0() { return &___PLATFORM_DEFAULT_ENCODING_0; }
	inline void set_PLATFORM_DEFAULT_ENCODING_0(String_t* value)
	{
		___PLATFORM_DEFAULT_ENCODING_0 = value;
		Il2CppCodeGenWriteBarrier(&___PLATFORM_DEFAULT_ENCODING_0, value);
	}

	inline static int32_t get_offset_of_SHIFT_JIS_1() { return static_cast<int32_t>(offsetof(StringUtils_t3420815774_StaticFields, ___SHIFT_JIS_1)); }
	inline String_t* get_SHIFT_JIS_1() const { return ___SHIFT_JIS_1; }
	inline String_t** get_address_of_SHIFT_JIS_1() { return &___SHIFT_JIS_1; }
	inline void set_SHIFT_JIS_1(String_t* value)
	{
		___SHIFT_JIS_1 = value;
		Il2CppCodeGenWriteBarrier(&___SHIFT_JIS_1, value);
	}

	inline static int32_t get_offset_of_GB2312_2() { return static_cast<int32_t>(offsetof(StringUtils_t3420815774_StaticFields, ___GB2312_2)); }
	inline String_t* get_GB2312_2() const { return ___GB2312_2; }
	inline String_t** get_address_of_GB2312_2() { return &___GB2312_2; }
	inline void set_GB2312_2(String_t* value)
	{
		___GB2312_2 = value;
		Il2CppCodeGenWriteBarrier(&___GB2312_2, value);
	}

	inline static int32_t get_offset_of_ASSUME_SHIFT_JIS_3() { return static_cast<int32_t>(offsetof(StringUtils_t3420815774_StaticFields, ___ASSUME_SHIFT_JIS_3)); }
	inline bool get_ASSUME_SHIFT_JIS_3() const { return ___ASSUME_SHIFT_JIS_3; }
	inline bool* get_address_of_ASSUME_SHIFT_JIS_3() { return &___ASSUME_SHIFT_JIS_3; }
	inline void set_ASSUME_SHIFT_JIS_3(bool value)
	{
		___ASSUME_SHIFT_JIS_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
