﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.Datamatrix.Internal.Version
struct Version_t2313761970;
// ZXing.Datamatrix.Internal.Version/ECBlocks
struct ECBlocks_t3710297555;
// System.String
struct String_t;
// ZXing.Datamatrix.Internal.Version[]
struct VersionU5BU5D_t2074916935;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Datamatrix_Internal_Version_ECBlocks3710297555.h"

// System.Void ZXing.Datamatrix.Internal.Version::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,ZXing.Datamatrix.Internal.Version/ECBlocks)
extern "C"  void Version__ctor_m2097384471 (Version_t2313761970 * __this, int32_t ___versionNumber0, int32_t ___symbolSizeRows1, int32_t ___symbolSizeColumns2, int32_t ___dataRegionSizeRows3, int32_t ___dataRegionSizeColumns4, ECBlocks_t3710297555 * ___ecBlocks5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Internal.Version::getVersionNumber()
extern "C"  int32_t Version_getVersionNumber_m744799062 (Version_t2313761970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Internal.Version::getSymbolSizeRows()
extern "C"  int32_t Version_getSymbolSizeRows_m352606687 (Version_t2313761970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Internal.Version::getSymbolSizeColumns()
extern "C"  int32_t Version_getSymbolSizeColumns_m3603329593 (Version_t2313761970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Internal.Version::getDataRegionSizeRows()
extern "C"  int32_t Version_getDataRegionSizeRows_m3187913221 (Version_t2313761970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Internal.Version::getDataRegionSizeColumns()
extern "C"  int32_t Version_getDataRegionSizeColumns_m1098473555 (Version_t2313761970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.Datamatrix.Internal.Version::getTotalCodewords()
extern "C"  int32_t Version_getTotalCodewords_m4192459557 (Version_t2313761970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Datamatrix.Internal.Version/ECBlocks ZXing.Datamatrix.Internal.Version::getECBlocks()
extern "C"  ECBlocks_t3710297555 * Version_getECBlocks_m3126367437 (Version_t2313761970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Datamatrix.Internal.Version ZXing.Datamatrix.Internal.Version::getVersionForDimensions(System.Int32,System.Int32)
extern "C"  Version_t2313761970 * Version_getVersionForDimensions_m4194449814 (Il2CppObject * __this /* static, unused */, int32_t ___numRows0, int32_t ___numColumns1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.Datamatrix.Internal.Version::ToString()
extern "C"  String_t* Version_ToString_m755119202 (Version_t2313761970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.Datamatrix.Internal.Version[] ZXing.Datamatrix.Internal.Version::buildVersions()
extern "C"  VersionU5BU5D_t2074916935* Version_buildVersions_m4057765347 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.Datamatrix.Internal.Version::.cctor()
extern "C"  void Version__cctor_m3309675362 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
