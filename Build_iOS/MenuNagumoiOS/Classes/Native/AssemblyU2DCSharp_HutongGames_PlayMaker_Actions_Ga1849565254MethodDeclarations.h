﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GameObjectIsChildOf
struct GameObjectIsChildOf_t1849565254;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void HutongGames.PlayMaker.Actions.GameObjectIsChildOf::.ctor()
extern "C"  void GameObjectIsChildOf__ctor_m3409596848 (GameObjectIsChildOf_t1849565254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsChildOf::Reset()
extern "C"  void GameObjectIsChildOf_Reset_m1056029789 (GameObjectIsChildOf_t1849565254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsChildOf::OnEnter()
extern "C"  void GameObjectIsChildOf_OnEnter_m3249219975 (GameObjectIsChildOf_t1849565254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsChildOf::DoIsChildOf(UnityEngine.GameObject)
extern "C"  void GameObjectIsChildOf_DoIsChildOf_m970755908 (GameObjectIsChildOf_t1849565254 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
