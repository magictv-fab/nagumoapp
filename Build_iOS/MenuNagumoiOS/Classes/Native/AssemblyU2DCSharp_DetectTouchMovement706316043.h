﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DetectTouchMovement
struct  DetectTouchMovement_t706316043  : public MonoBehaviour_t667441552
{
public:

public:
};

struct DetectTouchMovement_t706316043_StaticFields
{
public:
	// System.Single DetectTouchMovement::turnAngleDelta
	float ___turnAngleDelta_8;
	// System.Single DetectTouchMovement::turnAngle
	float ___turnAngle_9;
	// System.Single DetectTouchMovement::pinchDistanceDelta
	float ___pinchDistanceDelta_10;
	// System.Single DetectTouchMovement::pinchDistance
	float ___pinchDistance_11;

public:
	inline static int32_t get_offset_of_turnAngleDelta_8() { return static_cast<int32_t>(offsetof(DetectTouchMovement_t706316043_StaticFields, ___turnAngleDelta_8)); }
	inline float get_turnAngleDelta_8() const { return ___turnAngleDelta_8; }
	inline float* get_address_of_turnAngleDelta_8() { return &___turnAngleDelta_8; }
	inline void set_turnAngleDelta_8(float value)
	{
		___turnAngleDelta_8 = value;
	}

	inline static int32_t get_offset_of_turnAngle_9() { return static_cast<int32_t>(offsetof(DetectTouchMovement_t706316043_StaticFields, ___turnAngle_9)); }
	inline float get_turnAngle_9() const { return ___turnAngle_9; }
	inline float* get_address_of_turnAngle_9() { return &___turnAngle_9; }
	inline void set_turnAngle_9(float value)
	{
		___turnAngle_9 = value;
	}

	inline static int32_t get_offset_of_pinchDistanceDelta_10() { return static_cast<int32_t>(offsetof(DetectTouchMovement_t706316043_StaticFields, ___pinchDistanceDelta_10)); }
	inline float get_pinchDistanceDelta_10() const { return ___pinchDistanceDelta_10; }
	inline float* get_address_of_pinchDistanceDelta_10() { return &___pinchDistanceDelta_10; }
	inline void set_pinchDistanceDelta_10(float value)
	{
		___pinchDistanceDelta_10 = value;
	}

	inline static int32_t get_offset_of_pinchDistance_11() { return static_cast<int32_t>(offsetof(DetectTouchMovement_t706316043_StaticFields, ___pinchDistance_11)); }
	inline float get_pinchDistance_11() const { return ___pinchDistance_11; }
	inline float* get_address_of_pinchDistance_11() { return &___pinchDistance_11; }
	inline void set_pinchDistance_11(float value)
	{
		___pinchDistance_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
