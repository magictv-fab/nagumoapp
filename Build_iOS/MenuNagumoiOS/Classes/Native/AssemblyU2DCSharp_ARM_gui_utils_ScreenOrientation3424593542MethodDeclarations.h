﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.gui.utils.ScreenOrientation
struct ScreenOrientation_t3424593542;
// ARM.gui.utils.ScreenOrientation/OnScreenOrientationEventHandler
struct OnScreenOrientationEventHandler_t449305768;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ARM_gui_utils_ScreenOrientationT3688806624.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_ARM_gui_utils_ScreenOrientation_O449305768.h"
#include "AssemblyU2DCSharp_ARM_gui_utils_ScreenOrientation3424593542.h"

// System.Void ARM.gui.utils.ScreenOrientation::.ctor()
extern "C"  void ScreenOrientation__ctor_m3527195753 (ScreenOrientation_t3424593542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.gui.utils.ScreenOrientation::.cctor()
extern "C"  void ScreenOrientation__cctor_m1486789732 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ARM.gui.utils.ScreenOrientationType ARM.gui.utils.ScreenOrientation::get_Orientation()
extern "C"  int32_t ScreenOrientation_get_Orientation_m2064662885 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.gui.utils.ScreenOrientation::RaiseScreenOrientationType(UnityEngine.Vector2,ARM.gui.utils.ScreenOrientationType)
extern "C"  void ScreenOrientation_RaiseScreenOrientationType_m3635178589 (ScreenOrientation_t3424593542 * __this, Vector2_t4282066565  ___screenSize0, int32_t ___screenOrientationType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.gui.utils.ScreenOrientation::AddChangeListener(ARM.gui.utils.ScreenOrientation/OnScreenOrientationEventHandler)
extern "C"  void ScreenOrientation_AddChangeListener_m1406734718 (Il2CppObject * __this /* static, unused */, OnScreenOrientationEventHandler_t449305768 * ___listener0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.gui.utils.ScreenOrientation::RemoveChangeListener(ARM.gui.utils.ScreenOrientation/OnScreenOrientationEventHandler)
extern "C"  void ScreenOrientation_RemoveChangeListener_m2231004551 (Il2CppObject * __this /* static, unused */, OnScreenOrientationEventHandler_t449305768 * ___listener0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ARM.gui.utils.ScreenOrientation ARM.gui.utils.ScreenOrientation::get_InstanceScreenOrientation()
extern "C"  ScreenOrientation_t3424593542 * ScreenOrientation_get_InstanceScreenOrientation_m172327710 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.gui.utils.ScreenOrientation::set_InstanceScreenOrientation(ARM.gui.utils.ScreenOrientation)
extern "C"  void ScreenOrientation_set_InstanceScreenOrientation_m3595975223 (Il2CppObject * __this /* static, unused */, ScreenOrientation_t3424593542 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ARM.gui.utils.ScreenOrientationType ARM.gui.utils.ScreenOrientation::GetCurrentOrientationType()
extern "C"  int32_t ScreenOrientation_GetCurrentOrientationType_m3879696421 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARM.gui.utils.ScreenOrientation::CheckOrientationChange()
extern "C"  bool ScreenOrientation_CheckOrientationChange_m661635199 (ScreenOrientation_t3424593542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.gui.utils.ScreenOrientation::Update()
extern "C"  void ScreenOrientation_Update_m3695748036 (ScreenOrientation_t3424593542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.gui.utils.ScreenOrientation::Awake()
extern "C"  void ScreenOrientation_Awake_m3764800972 (ScreenOrientation_t3424593542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.gui.utils.ScreenOrientation::Start()
extern "C"  void ScreenOrientation_Start_m2474333545 (ScreenOrientation_t3424593542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.gui.utils.ScreenOrientation::OnDestroy()
extern "C"  void ScreenOrientation_OnDestroy_m1125659874 (ScreenOrientation_t3424593542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 ARM.gui.utils.ScreenOrientation::get_screenSize()
extern "C"  Vector2_t4282066565  ScreenOrientation_get_screenSize_m924489910 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
