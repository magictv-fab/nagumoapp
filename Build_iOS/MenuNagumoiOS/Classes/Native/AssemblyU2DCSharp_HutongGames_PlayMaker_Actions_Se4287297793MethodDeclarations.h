﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAnimatorBool
struct SetAnimatorBool_t4287297793;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAnimatorBool::.ctor()
extern "C"  void SetAnimatorBool__ctor_m1067241301 (SetAnimatorBool_t4287297793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorBool::Reset()
extern "C"  void SetAnimatorBool_Reset_m3008641538 (SetAnimatorBool_t4287297793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorBool::OnEnter()
extern "C"  void SetAnimatorBool_OnEnter_m2808402412 (SetAnimatorBool_t4287297793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorBool::OnAnimatorMoveEvent()
extern "C"  void SetAnimatorBool_OnAnimatorMoveEvent_m3179013686 (SetAnimatorBool_t4287297793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorBool::OnUpdate()
extern "C"  void SetAnimatorBool_OnUpdate_m294688215 (SetAnimatorBool_t4287297793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorBool::SetParameter()
extern "C"  void SetAnimatorBool_SetParameter_m3647936758 (SetAnimatorBool_t4287297793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorBool::OnExit()
extern "C"  void SetAnimatorBool_OnExit_m238060588 (SetAnimatorBool_t4287297793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
