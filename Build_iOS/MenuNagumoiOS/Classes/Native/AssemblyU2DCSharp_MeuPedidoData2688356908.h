﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PedidoData[]
struct PedidoDataU5BU5D_t3795515318;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MeuPedidoData
struct  MeuPedidoData_t2688356908  : public Il2CppObject
{
public:
	// System.Int32 MeuPedidoData::posicao
	int32_t ___posicao_0;
	// PedidoData[] MeuPedidoData::pedidos
	PedidoDataU5BU5D_t3795515318* ___pedidos_1;

public:
	inline static int32_t get_offset_of_posicao_0() { return static_cast<int32_t>(offsetof(MeuPedidoData_t2688356908, ___posicao_0)); }
	inline int32_t get_posicao_0() const { return ___posicao_0; }
	inline int32_t* get_address_of_posicao_0() { return &___posicao_0; }
	inline void set_posicao_0(int32_t value)
	{
		___posicao_0 = value;
	}

	inline static int32_t get_offset_of_pedidos_1() { return static_cast<int32_t>(offsetof(MeuPedidoData_t2688356908, ___pedidos_1)); }
	inline PedidoDataU5BU5D_t3795515318* get_pedidos_1() const { return ___pedidos_1; }
	inline PedidoDataU5BU5D_t3795515318** get_address_of_pedidos_1() { return &___pedidos_1; }
	inline void set_pedidos_1(PedidoDataU5BU5D_t3795515318* value)
	{
		___pedidos_1 = value;
		Il2CppCodeGenWriteBarrier(&___pedidos_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
