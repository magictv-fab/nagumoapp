﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.utils.chroma.vuforia.VuforiaChromaKeyConfig
struct VuforiaChromaKeyConfig_t3978586099;
// UnityEngine.Material
struct Material_t3870600107;
// MagicTV.vo.MetadataVO[]
struct MetadataVOU5BU5D_t4238035203;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"

// System.Void MagicTV.utils.chroma.vuforia.VuforiaChromaKeyConfig::.ctor()
extern "C"  void VuforiaChromaKeyConfig__ctor_m2646524634 (VuforiaChromaKeyConfig_t3978586099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.utils.chroma.vuforia.VuforiaChromaKeyConfig::configChroma(UnityEngine.Material,MagicTV.vo.MetadataVO[])
extern "C"  void VuforiaChromaKeyConfig_configChroma_m252518352 (Il2CppObject * __this /* static, unused */, Material_t3870600107 * ___material0, MetadataVOU5BU5D_t4238035203* ___metas1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.utils.chroma.vuforia.VuforiaChromaKeyConfig::changeSensibilidadeFina(UnityEngine.Material,System.Single)
extern "C"  void VuforiaChromaKeyConfig_changeSensibilidadeFina_m169626393 (Il2CppObject * __this /* static, unused */, Material_t3870600107 * ___material0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.utils.chroma.vuforia.VuforiaChromaKeyConfig::addSesibilidadeFina(UnityEngine.Material,System.Single)
extern "C"  void VuforiaChromaKeyConfig_addSesibilidadeFina_m2022931232 (Il2CppObject * __this /* static, unused */, Material_t3870600107 * ___material0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MagicTV.utils.chroma.vuforia.VuforiaChromaKeyConfig::getSensibilidadeFina(UnityEngine.Material)
extern "C"  float VuforiaChromaKeyConfig_getSensibilidadeFina_m3432287796 (Il2CppObject * __this /* static, unused */, Material_t3870600107 * ___material0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.utils.chroma.vuforia.VuforiaChromaKeyConfig::GetLabelFloat4()
extern "C"  String_t* VuforiaChromaKeyConfig_GetLabelFloat4_m3896549981 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.utils.chroma.vuforia.VuforiaChromaKeyConfig::changeSmooth(UnityEngine.Material,System.Single)
extern "C"  void VuforiaChromaKeyConfig_changeSmooth_m3092304975 (Il2CppObject * __this /* static, unused */, Material_t3870600107 * ___material0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.utils.chroma.vuforia.VuforiaChromaKeyConfig::addSmooth(UnityEngine.Material,System.Single)
extern "C"  void VuforiaChromaKeyConfig_addSmooth_m890266382 (Il2CppObject * __this /* static, unused */, Material_t3870600107 * ___material0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MagicTV.utils.chroma.vuforia.VuforiaChromaKeyConfig::getSmooth(UnityEngine.Material)
extern "C"  float VuforiaChromaKeyConfig_getSmooth_m2565040106 (Il2CppObject * __this /* static, unused */, Material_t3870600107 * ___material0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.utils.chroma.vuforia.VuforiaChromaKeyConfig::GetLabelFloat3()
extern "C"  String_t* VuforiaChromaKeyConfig_GetLabelFloat3_m3896549020 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.utils.chroma.vuforia.VuforiaChromaKeyConfig::changeRecorte(UnityEngine.Material,System.Single)
extern "C"  void VuforiaChromaKeyConfig_changeRecorte_m4151046579 (Il2CppObject * __this /* static, unused */, Material_t3870600107 * ___material0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.utils.chroma.vuforia.VuforiaChromaKeyConfig::addRecorte(UnityEngine.Material,System.Single)
extern "C"  void VuforiaChromaKeyConfig_addRecorte_m312359636 (Il2CppObject * __this /* static, unused */, Material_t3870600107 * ___material0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MagicTV.utils.chroma.vuforia.VuforiaChromaKeyConfig::getRecorte(UnityEngine.Material)
extern "C"  float VuforiaChromaKeyConfig_getRecorte_m1124870734 (Il2CppObject * __this /* static, unused */, Material_t3870600107 * ___material0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.utils.chroma.vuforia.VuforiaChromaKeyConfig::GetLabelFloat1()
extern "C"  String_t* VuforiaChromaKeyConfig_GetLabelFloat1_m3896547098 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.utils.chroma.vuforia.VuforiaChromaKeyConfig::changeSensibilidade(UnityEngine.Material,System.Single)
extern "C"  void VuforiaChromaKeyConfig_changeSensibilidade_m1200777667 (Il2CppObject * __this /* static, unused */, Material_t3870600107 * ___material0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.utils.chroma.vuforia.VuforiaChromaKeyConfig::addSensibilidade(UnityEngine.Material,System.Single)
extern "C"  void VuforiaChromaKeyConfig_addSensibilidade_m769103396 (Il2CppObject * __this /* static, unused */, Material_t3870600107 * ___material0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MagicTV.utils.chroma.vuforia.VuforiaChromaKeyConfig::getSensibilidade(UnityEngine.Material)
extern "C"  float VuforiaChromaKeyConfig_getSensibilidade_m1720237662 (Il2CppObject * __this /* static, unused */, Material_t3870600107 * ___material0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.utils.chroma.vuforia.VuforiaChromaKeyConfig::GetLabelFloat2()
extern "C"  String_t* VuforiaChromaKeyConfig_GetLabelFloat2_m3896548059 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
