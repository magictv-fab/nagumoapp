﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2615417833.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutFloatLabel
struct  GUILayoutFloatLabel_t2180325291  : public GUILayoutAction_t2615417833
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutFloatLabel::prefix
	FsmString_t952858651 * ___prefix_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutFloatLabel::floatVariable
	FsmFloat_t2134102846 * ___floatVariable_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutFloatLabel::style
	FsmString_t952858651 * ___style_13;

public:
	inline static int32_t get_offset_of_prefix_11() { return static_cast<int32_t>(offsetof(GUILayoutFloatLabel_t2180325291, ___prefix_11)); }
	inline FsmString_t952858651 * get_prefix_11() const { return ___prefix_11; }
	inline FsmString_t952858651 ** get_address_of_prefix_11() { return &___prefix_11; }
	inline void set_prefix_11(FsmString_t952858651 * value)
	{
		___prefix_11 = value;
		Il2CppCodeGenWriteBarrier(&___prefix_11, value);
	}

	inline static int32_t get_offset_of_floatVariable_12() { return static_cast<int32_t>(offsetof(GUILayoutFloatLabel_t2180325291, ___floatVariable_12)); }
	inline FsmFloat_t2134102846 * get_floatVariable_12() const { return ___floatVariable_12; }
	inline FsmFloat_t2134102846 ** get_address_of_floatVariable_12() { return &___floatVariable_12; }
	inline void set_floatVariable_12(FsmFloat_t2134102846 * value)
	{
		___floatVariable_12 = value;
		Il2CppCodeGenWriteBarrier(&___floatVariable_12, value);
	}

	inline static int32_t get_offset_of_style_13() { return static_cast<int32_t>(offsetof(GUILayoutFloatLabel_t2180325291, ___style_13)); }
	inline FsmString_t952858651 * get_style_13() const { return ___style_13; }
	inline FsmString_t952858651 ** get_address_of_style_13() { return &___style_13; }
	inline void set_style_13(FsmString_t952858651 * value)
	{
		___style_13 = value;
		Il2CppCodeGenWriteBarrier(&___style_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
