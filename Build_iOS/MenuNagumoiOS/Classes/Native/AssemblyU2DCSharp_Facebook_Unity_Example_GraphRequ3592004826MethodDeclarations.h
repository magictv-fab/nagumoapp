﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator29
struct U3CTakeScreenshotU3Ec__Iterator29_t3592004826;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator29::.ctor()
extern "C"  void U3CTakeScreenshotU3Ec__Iterator29__ctor_m3107483089 (U3CTakeScreenshotU3Ec__Iterator29_t3592004826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator29::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTakeScreenshotU3Ec__Iterator29_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m34040673 (U3CTakeScreenshotU3Ec__Iterator29_t3592004826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator29::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTakeScreenshotU3Ec__Iterator29_System_Collections_IEnumerator_get_Current_m4279652597 (U3CTakeScreenshotU3Ec__Iterator29_t3592004826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator29::MoveNext()
extern "C"  bool U3CTakeScreenshotU3Ec__Iterator29_MoveNext_m3982647171 (U3CTakeScreenshotU3Ec__Iterator29_t3592004826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator29::Dispose()
extern "C"  void U3CTakeScreenshotU3Ec__Iterator29_Dispose_m1187260878 (U3CTakeScreenshotU3Ec__Iterator29_t3592004826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator29::Reset()
extern "C"  void U3CTakeScreenshotU3Ec__Iterator29_Reset_m753916030 (U3CTakeScreenshotU3Ec__Iterator29_t3592004826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
