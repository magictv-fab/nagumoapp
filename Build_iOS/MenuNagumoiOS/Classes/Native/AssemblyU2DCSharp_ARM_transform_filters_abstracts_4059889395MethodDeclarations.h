﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.transform.filters.abstracts.histories.HistoryAngleFilterAbstract
struct HistoryAngleFilterAbstract_t4059889395;

#include "codegen/il2cpp-codegen.h"

// System.Void ARM.transform.filters.abstracts.histories.HistoryAngleFilterAbstract::.ctor()
extern "C"  void HistoryAngleFilterAbstract__ctor_m267637090 (HistoryAngleFilterAbstract_t4059889395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
