﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UniWebView
struct UniWebView_t424341801;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PublishLink
struct  PublishLink_t1052985737  : public MonoBehaviour_t667441552
{
public:
	// System.String PublishLink::optionalUrl
	String_t* ___optionalUrl_3;
	// System.String PublishLink::htmlTxt
	String_t* ___htmlTxt_4;
	// UniWebView PublishLink::uniWebView
	UniWebView_t424341801 * ___uniWebView_5;

public:
	inline static int32_t get_offset_of_optionalUrl_3() { return static_cast<int32_t>(offsetof(PublishLink_t1052985737, ___optionalUrl_3)); }
	inline String_t* get_optionalUrl_3() const { return ___optionalUrl_3; }
	inline String_t** get_address_of_optionalUrl_3() { return &___optionalUrl_3; }
	inline void set_optionalUrl_3(String_t* value)
	{
		___optionalUrl_3 = value;
		Il2CppCodeGenWriteBarrier(&___optionalUrl_3, value);
	}

	inline static int32_t get_offset_of_htmlTxt_4() { return static_cast<int32_t>(offsetof(PublishLink_t1052985737, ___htmlTxt_4)); }
	inline String_t* get_htmlTxt_4() const { return ___htmlTxt_4; }
	inline String_t** get_address_of_htmlTxt_4() { return &___htmlTxt_4; }
	inline void set_htmlTxt_4(String_t* value)
	{
		___htmlTxt_4 = value;
		Il2CppCodeGenWriteBarrier(&___htmlTxt_4, value);
	}

	inline static int32_t get_offset_of_uniWebView_5() { return static_cast<int32_t>(offsetof(PublishLink_t1052985737, ___uniWebView_5)); }
	inline UniWebView_t424341801 * get_uniWebView_5() const { return ___uniWebView_5; }
	inline UniWebView_t424341801 ** get_address_of_uniWebView_5() { return &___uniWebView_5; }
	inline void set_uniWebView_5(UniWebView_t424341801 * value)
	{
		___uniWebView_5 = value;
		Il2CppCodeGenWriteBarrier(&___uniWebView_5, value);
	}
};

struct PublishLink_t1052985737_StaticFields
{
public:
	// System.String PublishLink::url
	String_t* ___url_2;

public:
	inline static int32_t get_offset_of_url_2() { return static_cast<int32_t>(offsetof(PublishLink_t1052985737_StaticFields, ___url_2)); }
	inline String_t* get_url_2() const { return ___url_2; }
	inline String_t** get_address_of_url_2() { return &___url_2; }
	inline void set_url_2(String_t* value)
	{
		___url_2 = value;
		Il2CppCodeGenWriteBarrier(&___url_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
