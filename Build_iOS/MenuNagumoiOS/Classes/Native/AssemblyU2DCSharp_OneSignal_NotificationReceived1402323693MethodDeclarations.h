﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OneSignal/NotificationReceived
struct NotificationReceived_t1402323693;
// System.Object
struct Il2CppObject;
// OSNotification
struct OSNotification_t892481775;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_OSNotification892481775.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void OneSignal/NotificationReceived::.ctor(System.Object,System.IntPtr)
extern "C"  void NotificationReceived__ctor_m468715284 (NotificationReceived_t1402323693 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal/NotificationReceived::Invoke(OSNotification)
extern "C"  void NotificationReceived_Invoke_m367129759 (NotificationReceived_t1402323693 * __this, OSNotification_t892481775 * ___notification0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult OneSignal/NotificationReceived::BeginInvoke(OSNotification,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * NotificationReceived_BeginInvoke_m2010784024 (NotificationReceived_t1402323693 * __this, OSNotification_t892481775 * ___notification0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OneSignal/NotificationReceived::EndInvoke(System.IAsyncResult)
extern "C"  void NotificationReceived_EndInvoke_m4293534500 (NotificationReceived_t1402323693 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
