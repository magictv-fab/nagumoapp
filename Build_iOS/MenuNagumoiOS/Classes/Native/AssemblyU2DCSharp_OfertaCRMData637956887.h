﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OfertaCRMData
struct  OfertaCRMData_t637956887  : public Il2CppObject
{
public:
	// System.Int32 OfertaCRMData::id_oferta
	int32_t ___id_oferta_0;
	// System.Int32 OfertaCRMData::tipo
	int32_t ___tipo_1;
	// System.Int32 OfertaCRMData::unidade
	int32_t ___unidade_2;
	// System.Int32 OfertaCRMData::desconto
	int32_t ___desconto_3;
	// System.Int32 OfertaCRMData::pague
	int32_t ___pague_4;
	// System.Int32 OfertaCRMData::leve
	int32_t ___leve_5;
	// System.Int64 OfertaCRMData::id_crm
	int64_t ___id_crm_6;
	// System.Int32 OfertaCRMData::peso
	int32_t ___peso_7;
	// System.Single OfertaCRMData::preco
	float ___preco_8;
	// System.Single OfertaCRMData::de
	float ___de_9;
	// System.Single OfertaCRMData::por
	float ___por_10;
	// System.String OfertaCRMData::imagem
	String_t* ___imagem_11;
	// System.String OfertaCRMData::titulo
	String_t* ___titulo_12;
	// System.String OfertaCRMData::texto
	String_t* ___texto_13;
	// System.String OfertaCRMData::datainicial
	String_t* ___datainicial_14;
	// System.String OfertaCRMData::datafinal
	String_t* ___datafinal_15;
	// System.String OfertaCRMData::link
	String_t* ___link_16;
	// System.String OfertaCRMData::ean
	String_t* ___ean_17;
	// System.String OfertaCRMData::dataAquisicao
	String_t* ___dataAquisicao_18;
	// System.Boolean OfertaCRMData::aderiu
	bool ___aderiu_19;

public:
	inline static int32_t get_offset_of_id_oferta_0() { return static_cast<int32_t>(offsetof(OfertaCRMData_t637956887, ___id_oferta_0)); }
	inline int32_t get_id_oferta_0() const { return ___id_oferta_0; }
	inline int32_t* get_address_of_id_oferta_0() { return &___id_oferta_0; }
	inline void set_id_oferta_0(int32_t value)
	{
		___id_oferta_0 = value;
	}

	inline static int32_t get_offset_of_tipo_1() { return static_cast<int32_t>(offsetof(OfertaCRMData_t637956887, ___tipo_1)); }
	inline int32_t get_tipo_1() const { return ___tipo_1; }
	inline int32_t* get_address_of_tipo_1() { return &___tipo_1; }
	inline void set_tipo_1(int32_t value)
	{
		___tipo_1 = value;
	}

	inline static int32_t get_offset_of_unidade_2() { return static_cast<int32_t>(offsetof(OfertaCRMData_t637956887, ___unidade_2)); }
	inline int32_t get_unidade_2() const { return ___unidade_2; }
	inline int32_t* get_address_of_unidade_2() { return &___unidade_2; }
	inline void set_unidade_2(int32_t value)
	{
		___unidade_2 = value;
	}

	inline static int32_t get_offset_of_desconto_3() { return static_cast<int32_t>(offsetof(OfertaCRMData_t637956887, ___desconto_3)); }
	inline int32_t get_desconto_3() const { return ___desconto_3; }
	inline int32_t* get_address_of_desconto_3() { return &___desconto_3; }
	inline void set_desconto_3(int32_t value)
	{
		___desconto_3 = value;
	}

	inline static int32_t get_offset_of_pague_4() { return static_cast<int32_t>(offsetof(OfertaCRMData_t637956887, ___pague_4)); }
	inline int32_t get_pague_4() const { return ___pague_4; }
	inline int32_t* get_address_of_pague_4() { return &___pague_4; }
	inline void set_pague_4(int32_t value)
	{
		___pague_4 = value;
	}

	inline static int32_t get_offset_of_leve_5() { return static_cast<int32_t>(offsetof(OfertaCRMData_t637956887, ___leve_5)); }
	inline int32_t get_leve_5() const { return ___leve_5; }
	inline int32_t* get_address_of_leve_5() { return &___leve_5; }
	inline void set_leve_5(int32_t value)
	{
		___leve_5 = value;
	}

	inline static int32_t get_offset_of_id_crm_6() { return static_cast<int32_t>(offsetof(OfertaCRMData_t637956887, ___id_crm_6)); }
	inline int64_t get_id_crm_6() const { return ___id_crm_6; }
	inline int64_t* get_address_of_id_crm_6() { return &___id_crm_6; }
	inline void set_id_crm_6(int64_t value)
	{
		___id_crm_6 = value;
	}

	inline static int32_t get_offset_of_peso_7() { return static_cast<int32_t>(offsetof(OfertaCRMData_t637956887, ___peso_7)); }
	inline int32_t get_peso_7() const { return ___peso_7; }
	inline int32_t* get_address_of_peso_7() { return &___peso_7; }
	inline void set_peso_7(int32_t value)
	{
		___peso_7 = value;
	}

	inline static int32_t get_offset_of_preco_8() { return static_cast<int32_t>(offsetof(OfertaCRMData_t637956887, ___preco_8)); }
	inline float get_preco_8() const { return ___preco_8; }
	inline float* get_address_of_preco_8() { return &___preco_8; }
	inline void set_preco_8(float value)
	{
		___preco_8 = value;
	}

	inline static int32_t get_offset_of_de_9() { return static_cast<int32_t>(offsetof(OfertaCRMData_t637956887, ___de_9)); }
	inline float get_de_9() const { return ___de_9; }
	inline float* get_address_of_de_9() { return &___de_9; }
	inline void set_de_9(float value)
	{
		___de_9 = value;
	}

	inline static int32_t get_offset_of_por_10() { return static_cast<int32_t>(offsetof(OfertaCRMData_t637956887, ___por_10)); }
	inline float get_por_10() const { return ___por_10; }
	inline float* get_address_of_por_10() { return &___por_10; }
	inline void set_por_10(float value)
	{
		___por_10 = value;
	}

	inline static int32_t get_offset_of_imagem_11() { return static_cast<int32_t>(offsetof(OfertaCRMData_t637956887, ___imagem_11)); }
	inline String_t* get_imagem_11() const { return ___imagem_11; }
	inline String_t** get_address_of_imagem_11() { return &___imagem_11; }
	inline void set_imagem_11(String_t* value)
	{
		___imagem_11 = value;
		Il2CppCodeGenWriteBarrier(&___imagem_11, value);
	}

	inline static int32_t get_offset_of_titulo_12() { return static_cast<int32_t>(offsetof(OfertaCRMData_t637956887, ___titulo_12)); }
	inline String_t* get_titulo_12() const { return ___titulo_12; }
	inline String_t** get_address_of_titulo_12() { return &___titulo_12; }
	inline void set_titulo_12(String_t* value)
	{
		___titulo_12 = value;
		Il2CppCodeGenWriteBarrier(&___titulo_12, value);
	}

	inline static int32_t get_offset_of_texto_13() { return static_cast<int32_t>(offsetof(OfertaCRMData_t637956887, ___texto_13)); }
	inline String_t* get_texto_13() const { return ___texto_13; }
	inline String_t** get_address_of_texto_13() { return &___texto_13; }
	inline void set_texto_13(String_t* value)
	{
		___texto_13 = value;
		Il2CppCodeGenWriteBarrier(&___texto_13, value);
	}

	inline static int32_t get_offset_of_datainicial_14() { return static_cast<int32_t>(offsetof(OfertaCRMData_t637956887, ___datainicial_14)); }
	inline String_t* get_datainicial_14() const { return ___datainicial_14; }
	inline String_t** get_address_of_datainicial_14() { return &___datainicial_14; }
	inline void set_datainicial_14(String_t* value)
	{
		___datainicial_14 = value;
		Il2CppCodeGenWriteBarrier(&___datainicial_14, value);
	}

	inline static int32_t get_offset_of_datafinal_15() { return static_cast<int32_t>(offsetof(OfertaCRMData_t637956887, ___datafinal_15)); }
	inline String_t* get_datafinal_15() const { return ___datafinal_15; }
	inline String_t** get_address_of_datafinal_15() { return &___datafinal_15; }
	inline void set_datafinal_15(String_t* value)
	{
		___datafinal_15 = value;
		Il2CppCodeGenWriteBarrier(&___datafinal_15, value);
	}

	inline static int32_t get_offset_of_link_16() { return static_cast<int32_t>(offsetof(OfertaCRMData_t637956887, ___link_16)); }
	inline String_t* get_link_16() const { return ___link_16; }
	inline String_t** get_address_of_link_16() { return &___link_16; }
	inline void set_link_16(String_t* value)
	{
		___link_16 = value;
		Il2CppCodeGenWriteBarrier(&___link_16, value);
	}

	inline static int32_t get_offset_of_ean_17() { return static_cast<int32_t>(offsetof(OfertaCRMData_t637956887, ___ean_17)); }
	inline String_t* get_ean_17() const { return ___ean_17; }
	inline String_t** get_address_of_ean_17() { return &___ean_17; }
	inline void set_ean_17(String_t* value)
	{
		___ean_17 = value;
		Il2CppCodeGenWriteBarrier(&___ean_17, value);
	}

	inline static int32_t get_offset_of_dataAquisicao_18() { return static_cast<int32_t>(offsetof(OfertaCRMData_t637956887, ___dataAquisicao_18)); }
	inline String_t* get_dataAquisicao_18() const { return ___dataAquisicao_18; }
	inline String_t** get_address_of_dataAquisicao_18() { return &___dataAquisicao_18; }
	inline void set_dataAquisicao_18(String_t* value)
	{
		___dataAquisicao_18 = value;
		Il2CppCodeGenWriteBarrier(&___dataAquisicao_18, value);
	}

	inline static int32_t get_offset_of_aderiu_19() { return static_cast<int32_t>(offsetof(OfertaCRMData_t637956887, ___aderiu_19)); }
	inline bool get_aderiu_19() const { return ___aderiu_19; }
	inline bool* get_address_of_aderiu_19() { return &___aderiu_19; }
	inline void set_aderiu_19(bool value)
	{
		___aderiu_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
