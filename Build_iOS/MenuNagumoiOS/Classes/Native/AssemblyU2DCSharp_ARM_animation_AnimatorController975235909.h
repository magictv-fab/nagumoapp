﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Animator
struct Animator_t2776330603;

#include "AssemblyU2DCSharp_ARM_animation_ViewTimeControllAb2357615493.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.animation.AnimatorController
struct  AnimatorController_t975235909  : public ViewTimeControllAbstract_t2357615493
{
public:
	// UnityEngine.Animator ARM.animation.AnimatorController::anim
	Animator_t2776330603 * ___anim_6;
	// System.Boolean ARM.animation.AnimatorController::RestartOnHide
	bool ___RestartOnHide_7;
	// System.Boolean ARM.animation.AnimatorController::_isReadyToPlay
	bool ____isReadyToPlay_8;
	// System.Boolean ARM.animation.AnimatorController::autoPlay
	bool ___autoPlay_9;
	// System.Boolean ARM.animation.AnimatorController::_playNow
	bool ____playNow_10;

public:
	inline static int32_t get_offset_of_anim_6() { return static_cast<int32_t>(offsetof(AnimatorController_t975235909, ___anim_6)); }
	inline Animator_t2776330603 * get_anim_6() const { return ___anim_6; }
	inline Animator_t2776330603 ** get_address_of_anim_6() { return &___anim_6; }
	inline void set_anim_6(Animator_t2776330603 * value)
	{
		___anim_6 = value;
		Il2CppCodeGenWriteBarrier(&___anim_6, value);
	}

	inline static int32_t get_offset_of_RestartOnHide_7() { return static_cast<int32_t>(offsetof(AnimatorController_t975235909, ___RestartOnHide_7)); }
	inline bool get_RestartOnHide_7() const { return ___RestartOnHide_7; }
	inline bool* get_address_of_RestartOnHide_7() { return &___RestartOnHide_7; }
	inline void set_RestartOnHide_7(bool value)
	{
		___RestartOnHide_7 = value;
	}

	inline static int32_t get_offset_of__isReadyToPlay_8() { return static_cast<int32_t>(offsetof(AnimatorController_t975235909, ____isReadyToPlay_8)); }
	inline bool get__isReadyToPlay_8() const { return ____isReadyToPlay_8; }
	inline bool* get_address_of__isReadyToPlay_8() { return &____isReadyToPlay_8; }
	inline void set__isReadyToPlay_8(bool value)
	{
		____isReadyToPlay_8 = value;
	}

	inline static int32_t get_offset_of_autoPlay_9() { return static_cast<int32_t>(offsetof(AnimatorController_t975235909, ___autoPlay_9)); }
	inline bool get_autoPlay_9() const { return ___autoPlay_9; }
	inline bool* get_address_of_autoPlay_9() { return &___autoPlay_9; }
	inline void set_autoPlay_9(bool value)
	{
		___autoPlay_9 = value;
	}

	inline static int32_t get_offset_of__playNow_10() { return static_cast<int32_t>(offsetof(AnimatorController_t975235909, ____playNow_10)); }
	inline bool get__playNow_10() const { return ____playNow_10; }
	inline bool* get_address_of__playNow_10() { return &____playNow_10; }
	inline void set__playNow_10(bool value)
	{
		____playNow_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
