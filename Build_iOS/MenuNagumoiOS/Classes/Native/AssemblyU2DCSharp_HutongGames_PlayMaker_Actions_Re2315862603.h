﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmRect
struct FsmRect_t1076426478;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectContains
struct  RectContains_t2315862603  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.RectContains::rectangle
	FsmRect_t1076426478 * ___rectangle_9;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.RectContains::point
	FsmVector3_t533912882 * ___point_10;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectContains::x
	FsmFloat_t2134102846 * ___x_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectContains::y
	FsmFloat_t2134102846 * ___y_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.RectContains::trueEvent
	FsmEvent_t2133468028 * ___trueEvent_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.RectContains::falseEvent
	FsmEvent_t2133468028 * ___falseEvent_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.RectContains::storeResult
	FsmBool_t1075959796 * ___storeResult_15;
	// System.Boolean HutongGames.PlayMaker.Actions.RectContains::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_rectangle_9() { return static_cast<int32_t>(offsetof(RectContains_t2315862603, ___rectangle_9)); }
	inline FsmRect_t1076426478 * get_rectangle_9() const { return ___rectangle_9; }
	inline FsmRect_t1076426478 ** get_address_of_rectangle_9() { return &___rectangle_9; }
	inline void set_rectangle_9(FsmRect_t1076426478 * value)
	{
		___rectangle_9 = value;
		Il2CppCodeGenWriteBarrier(&___rectangle_9, value);
	}

	inline static int32_t get_offset_of_point_10() { return static_cast<int32_t>(offsetof(RectContains_t2315862603, ___point_10)); }
	inline FsmVector3_t533912882 * get_point_10() const { return ___point_10; }
	inline FsmVector3_t533912882 ** get_address_of_point_10() { return &___point_10; }
	inline void set_point_10(FsmVector3_t533912882 * value)
	{
		___point_10 = value;
		Il2CppCodeGenWriteBarrier(&___point_10, value);
	}

	inline static int32_t get_offset_of_x_11() { return static_cast<int32_t>(offsetof(RectContains_t2315862603, ___x_11)); }
	inline FsmFloat_t2134102846 * get_x_11() const { return ___x_11; }
	inline FsmFloat_t2134102846 ** get_address_of_x_11() { return &___x_11; }
	inline void set_x_11(FsmFloat_t2134102846 * value)
	{
		___x_11 = value;
		Il2CppCodeGenWriteBarrier(&___x_11, value);
	}

	inline static int32_t get_offset_of_y_12() { return static_cast<int32_t>(offsetof(RectContains_t2315862603, ___y_12)); }
	inline FsmFloat_t2134102846 * get_y_12() const { return ___y_12; }
	inline FsmFloat_t2134102846 ** get_address_of_y_12() { return &___y_12; }
	inline void set_y_12(FsmFloat_t2134102846 * value)
	{
		___y_12 = value;
		Il2CppCodeGenWriteBarrier(&___y_12, value);
	}

	inline static int32_t get_offset_of_trueEvent_13() { return static_cast<int32_t>(offsetof(RectContains_t2315862603, ___trueEvent_13)); }
	inline FsmEvent_t2133468028 * get_trueEvent_13() const { return ___trueEvent_13; }
	inline FsmEvent_t2133468028 ** get_address_of_trueEvent_13() { return &___trueEvent_13; }
	inline void set_trueEvent_13(FsmEvent_t2133468028 * value)
	{
		___trueEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___trueEvent_13, value);
	}

	inline static int32_t get_offset_of_falseEvent_14() { return static_cast<int32_t>(offsetof(RectContains_t2315862603, ___falseEvent_14)); }
	inline FsmEvent_t2133468028 * get_falseEvent_14() const { return ___falseEvent_14; }
	inline FsmEvent_t2133468028 ** get_address_of_falseEvent_14() { return &___falseEvent_14; }
	inline void set_falseEvent_14(FsmEvent_t2133468028 * value)
	{
		___falseEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___falseEvent_14, value);
	}

	inline static int32_t get_offset_of_storeResult_15() { return static_cast<int32_t>(offsetof(RectContains_t2315862603, ___storeResult_15)); }
	inline FsmBool_t1075959796 * get_storeResult_15() const { return ___storeResult_15; }
	inline FsmBool_t1075959796 ** get_address_of_storeResult_15() { return &___storeResult_15; }
	inline void set_storeResult_15(FsmBool_t1075959796 * value)
	{
		___storeResult_15 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_15, value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(RectContains_t2315862603, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
