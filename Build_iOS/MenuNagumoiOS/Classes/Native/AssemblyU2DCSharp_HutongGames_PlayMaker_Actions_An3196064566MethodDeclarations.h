﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AnimatorStopRecording
struct AnimatorStopRecording_t3196064566;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AnimatorStopRecording::.ctor()
extern "C"  void AnimatorStopRecording__ctor_m4246016704 (AnimatorStopRecording_t3196064566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorStopRecording::Reset()
extern "C"  void AnimatorStopRecording_Reset_m1892449645 (AnimatorStopRecording_t3196064566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorStopRecording::OnEnter()
extern "C"  void AnimatorStopRecording_OnEnter_m3889817239 (AnimatorStopRecording_t3196064566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
