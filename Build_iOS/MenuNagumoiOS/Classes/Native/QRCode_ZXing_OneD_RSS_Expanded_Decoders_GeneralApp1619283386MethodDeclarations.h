﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder
struct GeneralAppIdDecoder_t1619283386;
// ZXing.Common.BitArray
struct BitArray_t4163851164;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t243639308;
// ZXing.OneD.RSS.Expanded.Decoders.DecodedNumeric
struct DecodedNumeric_t133612409;
// ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation
struct DecodedInformation_t1859732120;
// ZXing.OneD.RSS.Expanded.Decoders.BlockParsedResult
struct BlockParsedResult_t1963745509;
// ZXing.OneD.RSS.Expanded.Decoders.DecodedChar
struct DecodedChar_t1702476758;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_Common_BitArray4163851164.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "mscorlib_System_String7231557.h"

// System.Void ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::.ctor(ZXing.Common.BitArray)
extern "C"  void GeneralAppIdDecoder__ctor_m2013816630 (GeneralAppIdDecoder_t1619283386 * __this, BitArray_t4163851164 * ___information0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::decodeAllCodes(System.Text.StringBuilder,System.Int32)
extern "C"  String_t* GeneralAppIdDecoder_decodeAllCodes_m4007883216 (GeneralAppIdDecoder_t1619283386 * __this, StringBuilder_t243639308 * ___buff0, int32_t ___initialPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::isStillNumeric(System.Int32)
extern "C"  bool GeneralAppIdDecoder_isStillNumeric_m1910411713 (GeneralAppIdDecoder_t1619283386 * __this, int32_t ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.OneD.RSS.Expanded.Decoders.DecodedNumeric ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::decodeNumeric(System.Int32)
extern "C"  DecodedNumeric_t133612409 * GeneralAppIdDecoder_decodeNumeric_m1971986197 (GeneralAppIdDecoder_t1619283386 * __this, int32_t ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::extractNumericValueFromBitArray(System.Int32,System.Int32)
extern "C"  int32_t GeneralAppIdDecoder_extractNumericValueFromBitArray_m3081741092 (GeneralAppIdDecoder_t1619283386 * __this, int32_t ___pos0, int32_t ___bits1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::extractNumericValueFromBitArray(ZXing.Common.BitArray,System.Int32,System.Int32)
extern "C"  int32_t GeneralAppIdDecoder_extractNumericValueFromBitArray_m945682955 (Il2CppObject * __this /* static, unused */, BitArray_t4163851164 * ___information0, int32_t ___pos1, int32_t ___bits2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::decodeGeneralPurposeField(System.Int32,System.String)
extern "C"  DecodedInformation_t1859732120 * GeneralAppIdDecoder_decodeGeneralPurposeField_m3005213607 (GeneralAppIdDecoder_t1619283386 * __this, int32_t ___pos0, String_t* ___remaining1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::parseBlocks()
extern "C"  DecodedInformation_t1859732120 * GeneralAppIdDecoder_parseBlocks_m722636861 (GeneralAppIdDecoder_t1619283386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.OneD.RSS.Expanded.Decoders.BlockParsedResult ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::parseNumericBlock()
extern "C"  BlockParsedResult_t1963745509 * GeneralAppIdDecoder_parseNumericBlock_m2196424538 (GeneralAppIdDecoder_t1619283386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.OneD.RSS.Expanded.Decoders.BlockParsedResult ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::parseIsoIec646Block()
extern "C"  BlockParsedResult_t1963745509 * GeneralAppIdDecoder_parseIsoIec646Block_m4040976913 (GeneralAppIdDecoder_t1619283386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.OneD.RSS.Expanded.Decoders.BlockParsedResult ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::parseAlphaBlock()
extern "C"  BlockParsedResult_t1963745509 * GeneralAppIdDecoder_parseAlphaBlock_m2174617129 (GeneralAppIdDecoder_t1619283386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::isStillIsoIec646(System.Int32)
extern "C"  bool GeneralAppIdDecoder_isStillIsoIec646_m2319730794 (GeneralAppIdDecoder_t1619283386 * __this, int32_t ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.OneD.RSS.Expanded.Decoders.DecodedChar ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::decodeIsoIec646(System.Int32)
extern "C"  DecodedChar_t1702476758 * GeneralAppIdDecoder_decodeIsoIec646_m2783253233 (GeneralAppIdDecoder_t1619283386 * __this, int32_t ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::isStillAlpha(System.Int32)
extern "C"  bool GeneralAppIdDecoder_isStillAlpha_m3242329810 (GeneralAppIdDecoder_t1619283386 * __this, int32_t ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.OneD.RSS.Expanded.Decoders.DecodedChar ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::decodeAlphanumeric(System.Int32)
extern "C"  DecodedChar_t1702476758 * GeneralAppIdDecoder_decodeAlphanumeric_m1885556536 (GeneralAppIdDecoder_t1619283386 * __this, int32_t ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::isAlphaTo646ToAlphaLatch(System.Int32)
extern "C"  bool GeneralAppIdDecoder_isAlphaTo646ToAlphaLatch_m3850895836 (GeneralAppIdDecoder_t1619283386 * __this, int32_t ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::isAlphaOr646ToNumericLatch(System.Int32)
extern "C"  bool GeneralAppIdDecoder_isAlphaOr646ToNumericLatch_m4254115429 (GeneralAppIdDecoder_t1619283386 * __this, int32_t ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::isNumericToAlphaNumericLatch(System.Int32)
extern "C"  bool GeneralAppIdDecoder_isNumericToAlphaNumericLatch_m2423849 (GeneralAppIdDecoder_t1619283386 * __this, int32_t ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
