﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2615417833.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutIntLabel
struct  GUILayoutIntLabel_t3771756536  : public GUILayoutAction_t2615417833
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutIntLabel::prefix
	FsmString_t952858651 * ___prefix_11;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GUILayoutIntLabel::intVariable
	FsmInt_t1596138449 * ___intVariable_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutIntLabel::style
	FsmString_t952858651 * ___style_13;

public:
	inline static int32_t get_offset_of_prefix_11() { return static_cast<int32_t>(offsetof(GUILayoutIntLabel_t3771756536, ___prefix_11)); }
	inline FsmString_t952858651 * get_prefix_11() const { return ___prefix_11; }
	inline FsmString_t952858651 ** get_address_of_prefix_11() { return &___prefix_11; }
	inline void set_prefix_11(FsmString_t952858651 * value)
	{
		___prefix_11 = value;
		Il2CppCodeGenWriteBarrier(&___prefix_11, value);
	}

	inline static int32_t get_offset_of_intVariable_12() { return static_cast<int32_t>(offsetof(GUILayoutIntLabel_t3771756536, ___intVariable_12)); }
	inline FsmInt_t1596138449 * get_intVariable_12() const { return ___intVariable_12; }
	inline FsmInt_t1596138449 ** get_address_of_intVariable_12() { return &___intVariable_12; }
	inline void set_intVariable_12(FsmInt_t1596138449 * value)
	{
		___intVariable_12 = value;
		Il2CppCodeGenWriteBarrier(&___intVariable_12, value);
	}

	inline static int32_t get_offset_of_style_13() { return static_cast<int32_t>(offsetof(GUILayoutIntLabel_t3771756536, ___style_13)); }
	inline FsmString_t952858651 * get_style_13() const { return ___style_13; }
	inline FsmString_t952858651 ** get_address_of_style_13() { return &___style_13; }
	inline void set_style_13(FsmString_t952858651 * value)
	{
		___style_13 = value;
		Il2CppCodeGenWriteBarrier(&___style_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
