﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Area730.Notifications.NotificationTracker
struct NotificationTracker_t76028059;

#include "codegen/il2cpp-codegen.h"

// System.Void Area730.Notifications.NotificationTracker::.ctor()
extern "C"  void NotificationTracker__ctor_m1372437259 (NotificationTracker_t76028059 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Area730.Notifications.NotificationTracker::TrackId(System.Int32)
extern "C"  void NotificationTracker_TrackId_m3921114944 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Area730.Notifications.NotificationTracker::CancelAll()
extern "C"  void NotificationTracker_CancelAll_m2868363056 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
