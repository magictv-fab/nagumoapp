﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Area730.Notifications.Samples
struct Samples_t1320174935;

#include "codegen/il2cpp-codegen.h"

// System.Void Area730.Notifications.Samples::.ctor()
extern "C"  void Samples__ctor_m211041743 (Samples_t1320174935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Area730.Notifications.Samples::SimpleNotificationSample()
extern "C"  void Samples_SimpleNotificationSample_m3348628892 (Samples_t1320174935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Area730.Notifications.Samples::CustomizedNotificationSample()
extern "C"  void Samples_CustomizedNotificationSample_m1332537579 (Samples_t1320174935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Area730.Notifications.Samples::RepeatingNotificationSample()
extern "C"  void Samples_RepeatingNotificationSample_m2812111401 (Samples_t1320174935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Area730.Notifications.Samples::CustomIconsAndSoundSample()
extern "C"  void Samples_CustomIconsAndSoundSample_m1936828920 (Samples_t1320174935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Area730.Notifications.Samples::ScheduleCreatedInEditorSample()
extern "C"  void Samples_ScheduleCreatedInEditorSample_m1803079066 (Samples_t1320174935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Area730.Notifications.Samples::CancelAllSample()
extern "C"  void Samples_CancelAllSample_m1774785790 (Samples_t1320174935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Area730.Notifications.Samples::ClearAllSample()
extern "C"  void Samples_ClearAllSample_m3285164019 (Samples_t1320174935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
