﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.abstracts.device.BenchmarkInfo
struct  BenchmarkInfo_t940577717  : public Il2CppObject
{
public:
	// System.Int32 MagicTV.abstracts.device.BenchmarkInfo::_quality
	int32_t ____quality_0;

public:
	inline static int32_t get_offset_of__quality_0() { return static_cast<int32_t>(offsetof(BenchmarkInfo_t940577717, ____quality_0)); }
	inline int32_t get__quality_0() const { return ____quality_0; }
	inline int32_t* get_address_of__quality_0() { return &____quality_0; }
	inline void set__quality_0(int32_t value)
	{
		____quality_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
