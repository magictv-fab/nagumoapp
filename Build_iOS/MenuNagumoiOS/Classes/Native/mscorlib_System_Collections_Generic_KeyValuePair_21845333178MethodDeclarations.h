﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21845333178.h"
#include "AssemblyU2DCSharp_MagicTV_globals_StateMachine367995870.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.KeyValuePair`2<MagicTV.globals.StateMachine,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m726640746_gshared (KeyValuePair_2_t1845333178 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m726640746(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1845333178 *, int32_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m726640746_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<MagicTV.globals.StateMachine,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1096840254_gshared (KeyValuePair_2_t1845333178 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m1096840254(__this, method) ((  int32_t (*) (KeyValuePair_2_t1845333178 *, const MethodInfo*))KeyValuePair_2_get_Key_m1096840254_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<MagicTV.globals.StateMachine,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2352912255_gshared (KeyValuePair_2_t1845333178 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m2352912255(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1845333178 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m2352912255_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<MagicTV.globals.StateMachine,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3223841534_gshared (KeyValuePair_2_t1845333178 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m3223841534(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t1845333178 *, const MethodInfo*))KeyValuePair_2_get_Value_m3223841534_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<MagicTV.globals.StateMachine,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2289308671_gshared (KeyValuePair_2_t1845333178 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m2289308671(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1845333178 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m2289308671_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<MagicTV.globals.StateMachine,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m469421891_gshared (KeyValuePair_2_t1845333178 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m469421891(__this, method) ((  String_t* (*) (KeyValuePair_2_t1845333178 *, const MethodInfo*))KeyValuePair_2_ToString_m469421891_gshared)(__this, method)
