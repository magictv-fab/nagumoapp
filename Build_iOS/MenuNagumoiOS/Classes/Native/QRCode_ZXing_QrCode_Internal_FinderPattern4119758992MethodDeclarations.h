﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.QrCode.Internal.FinderPattern
struct FinderPattern_t4119758992;

#include "codegen/il2cpp-codegen.h"

// System.Void ZXing.QrCode.Internal.FinderPattern::.ctor(System.Single,System.Single,System.Single)
extern "C"  void FinderPattern__ctor_m1179110585 (FinderPattern_t4119758992 * __this, float ___posX0, float ___posY1, float ___estimatedModuleSize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.QrCode.Internal.FinderPattern::.ctor(System.Single,System.Single,System.Single,System.Int32)
extern "C"  void FinderPattern__ctor_m1244497086 (FinderPattern_t4119758992 * __this, float ___posX0, float ___posY1, float ___estimatedModuleSize2, int32_t ___count3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ZXing.QrCode.Internal.FinderPattern::get_EstimatedModuleSize()
extern "C"  float FinderPattern_get_EstimatedModuleSize_m3969918758 (FinderPattern_t4119758992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZXing.QrCode.Internal.FinderPattern::get_Count()
extern "C"  int32_t FinderPattern_get_Count_m3246118990 (FinderPattern_t4119758992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZXing.QrCode.Internal.FinderPattern::aboutEquals(System.Single,System.Single,System.Single)
extern "C"  bool FinderPattern_aboutEquals_m1902006875 (FinderPattern_t4119758992 * __this, float ___moduleSize0, float ___i1, float ___j2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.QrCode.Internal.FinderPattern ZXing.QrCode.Internal.FinderPattern::combineEstimate(System.Single,System.Single,System.Single)
extern "C"  FinderPattern_t4119758992 * FinderPattern_combineEstimate_m1620788816 (FinderPattern_t4119758992 * __this, float ___i0, float ___j1, float ___newModuleSize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
