﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CollisionCheck
struct CollisionCheck_t2934647062;
// UnityEngine.Collider2D
struct Collider2D_t1552025098;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098.h"

// System.Void CollisionCheck::.ctor()
extern "C"  void CollisionCheck__ctor_m284720197 (CollisionCheck_t2934647062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CollisionCheck::Start()
extern "C"  void CollisionCheck_Start_m3526825285 (CollisionCheck_t2934647062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CollisionCheck::Update()
extern "C"  void CollisionCheck_Update_m1963253608 (CollisionCheck_t2934647062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CollisionCheck::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void CollisionCheck_OnTriggerEnter2D_m1218183123 (CollisionCheck_t2934647062 * __this, Collider2D_t1552025098 * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
