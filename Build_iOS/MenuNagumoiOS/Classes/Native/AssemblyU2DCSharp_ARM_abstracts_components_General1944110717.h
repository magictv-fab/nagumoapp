﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OnEvent
struct OnEvent_t314892251;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.abstracts.components.GeneralComponentsObjectAbstract
struct  GeneralComponentsObjectAbstract_t1944110717  : public Il2CppObject
{
public:
	// OnEvent ARM.abstracts.components.GeneralComponentsObjectAbstract::onCompoentIsReady
	OnEvent_t314892251 * ___onCompoentIsReady_0;
	// System.Boolean ARM.abstracts.components.GeneralComponentsObjectAbstract::_isComponentReady
	bool ____isComponentReady_1;

public:
	inline static int32_t get_offset_of_onCompoentIsReady_0() { return static_cast<int32_t>(offsetof(GeneralComponentsObjectAbstract_t1944110717, ___onCompoentIsReady_0)); }
	inline OnEvent_t314892251 * get_onCompoentIsReady_0() const { return ___onCompoentIsReady_0; }
	inline OnEvent_t314892251 ** get_address_of_onCompoentIsReady_0() { return &___onCompoentIsReady_0; }
	inline void set_onCompoentIsReady_0(OnEvent_t314892251 * value)
	{
		___onCompoentIsReady_0 = value;
		Il2CppCodeGenWriteBarrier(&___onCompoentIsReady_0, value);
	}

	inline static int32_t get_offset_of__isComponentReady_1() { return static_cast<int32_t>(offsetof(GeneralComponentsObjectAbstract_t1944110717, ____isComponentReady_1)); }
	inline bool get__isComponentReady_1() const { return ____isComponentReady_1; }
	inline bool* get_address_of__isComponentReady_1() { return &____isComponentReady_1; }
	inline void set__isComponentReady_1(bool value)
	{
		____isComponentReady_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
