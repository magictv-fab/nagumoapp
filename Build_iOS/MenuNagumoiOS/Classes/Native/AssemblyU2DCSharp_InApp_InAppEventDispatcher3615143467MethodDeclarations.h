﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InApp.InAppEventDispatcher
struct InAppEventDispatcher_t3615143467;
// System.String
struct String_t;
// InApp.InAppEventDispatcher/InAppEventDispatcherWithString
struct InAppEventDispatcherWithString_t1910714196;
// InApp.InAppEventDispatcher/InAppEventDispatcherOnOff
struct InAppEventDispatcherOnOff_t3474551315;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_InApp_InAppEventDispatcher_Event1614635846.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_InApp_InAppEventDispatcher_InApp1910714196.h"
#include "AssemblyU2DCSharp_InApp_InAppEventDispatcher_InApp3474551315.h"

// System.Void InApp.InAppEventDispatcher::.ctor()
extern "C"  void InAppEventDispatcher__ctor_m3337371588 (InAppEventDispatcher_t3615143467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// InApp.InAppEventDispatcher InApp.InAppEventDispatcher::GetInstance()
extern "C"  InAppEventDispatcher_t3615143467 * InAppEventDispatcher_GetInstance_m2280407175 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// InApp.InAppEventDispatcher/EventNames InApp.InAppEventDispatcher::getEventNameByString(System.String)
extern "C"  int32_t InAppEventDispatcher_getEventNameByString_m3586531934 (InAppEventDispatcher_t3615143467 * __this, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.InAppEventDispatcher::AddEventListener(InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherWithString)
extern "C"  void InAppEventDispatcher_AddEventListener_m2573587351 (InAppEventDispatcher_t3615143467 * __this, int32_t ___eventName0, InAppEventDispatcherWithString_t1910714196 * ___action1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.InAppEventDispatcher::AddEventListener(InApp.InAppEventDispatcher/EventNames,InApp.InAppEventDispatcher/InAppEventDispatcherOnOff)
extern "C"  void InAppEventDispatcher_AddEventListener_m2085623540 (InAppEventDispatcher_t3615143467 * __this, int32_t ___eventName0, InAppEventDispatcherOnOff_t3474551315 * ___action1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.InAppEventDispatcher::raiseInAppEvent(InApp.InAppEventDispatcher/EventNames,System.String)
extern "C"  void InAppEventDispatcher_raiseInAppEvent_m3973567580 (InAppEventDispatcher_t3615143467 * __this, int32_t ___eventName0, String_t* ___metadado1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.InAppEventDispatcher::raiseInAppEvent(InApp.InAppEventDispatcher/EventNames)
extern "C"  void InAppEventDispatcher_raiseInAppEvent_m2943378464 (InAppEventDispatcher_t3615143467 * __this, int32_t ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.InAppEventDispatcher::treatToRaiseEvents(System.Collections.Generic.List`1<System.String>)
extern "C"  void InAppEventDispatcher_treatToRaiseEvents_m2820495236 (InAppEventDispatcher_t3615143467 * __this, List_1_t1375417109 * ___metadatas0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.InAppEventDispatcher::AddToResetList(InApp.InAppEventDispatcher/InAppEventDispatcherOnOff)
extern "C"  void InAppEventDispatcher_AddToResetList_m4218787134 (InAppEventDispatcher_t3615143467 * __this, InAppEventDispatcherOnOff_t3474551315 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InApp.InAppEventDispatcher::raiseReset()
extern "C"  void InAppEventDispatcher_raiseReset_m65456547 (InAppEventDispatcher_t3615143467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
