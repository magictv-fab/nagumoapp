﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CEP
struct CEP_t66606;

#include "codegen/il2cpp-codegen.h"

// System.Void CEP::.ctor()
extern "C"  void CEP__ctor_m2016850685 (CEP_t66606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
