﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co3146284570MethodDeclarations.h"

// System.Void HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUITexture>::.ctor()
#define ComponentAction_1__ctor_m2230533721(__this, method) ((  void (*) (ComponentAction_1_t2995916491 *, const MethodInfo*))ComponentAction_1__ctor_m1981820550_gshared)(__this, method)
// UnityEngine.Rigidbody HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUITexture>::get_rigidbody()
#define ComponentAction_1_get_rigidbody_m2715558872(__this, method) ((  Rigidbody_t3346577219 * (*) (ComponentAction_1_t2995916491 *, const MethodInfo*))ComponentAction_1_get_rigidbody_m3654614315_gshared)(__this, method)
// UnityEngine.Renderer HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUITexture>::get_renderer()
#define ComponentAction_1_get_renderer_m1739810132(__this, method) ((  Renderer_t3076687687 * (*) (ComponentAction_1_t2995916491 *, const MethodInfo*))ComponentAction_1_get_renderer_m2707840429_gshared)(__this, method)
// UnityEngine.Animation HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUITexture>::get_animation()
#define ComponentAction_1_get_animation_m3133761016(__this, method) ((  Animation_t1724966010 * (*) (ComponentAction_1_t2995916491 *, const MethodInfo*))ComponentAction_1_get_animation_m3244187609_gshared)(__this, method)
// UnityEngine.AudioSource HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUITexture>::get_audio()
#define ComponentAction_1_get_audio_m4192031965(__this, method) ((  AudioSource_t1740077639 * (*) (ComponentAction_1_t2995916491 *, const MethodInfo*))ComponentAction_1_get_audio_m1198830264_gshared)(__this, method)
// UnityEngine.Camera HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUITexture>::get_camera()
#define ComponentAction_1_get_camera_m1608918168(__this, method) ((  Camera_t2727095145 * (*) (ComponentAction_1_t2995916491 *, const MethodInfo*))ComponentAction_1_get_camera_m2580411949_gshared)(__this, method)
// UnityEngine.GUIText HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUITexture>::get_guiText()
#define ComponentAction_1_get_guiText_m3663724984(__this, method) ((  GUIText_t3371372606 * (*) (ComponentAction_1_t2995916491 *, const MethodInfo*))ComponentAction_1_get_guiText_m995647329_gshared)(__this, method)
// UnityEngine.GUITexture HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUITexture>::get_guiTexture()
#define ComponentAction_1_get_guiTexture_m2636015758(__this, method) ((  GUITexture_t4020448292 * (*) (ComponentAction_1_t2995916491 *, const MethodInfo*))ComponentAction_1_get_guiTexture_m3296952909_gshared)(__this, method)
// UnityEngine.Light HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUITexture>::get_light()
#define ComponentAction_1_get_light_m2311385912(__this, method) ((  Light_t4202674828 * (*) (ComponentAction_1_t2995916491 *, const MethodInfo*))ComponentAction_1_get_light_m3399183485_gshared)(__this, method)
// UnityEngine.NetworkView HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUITexture>::get_networkView()
#define ComponentAction_1_get_networkView_m3411395480(__this, method) ((  NetworkView_t3656680617 * (*) (ComponentAction_1_t2995916491 *, const MethodInfo*))ComponentAction_1_get_networkView_m1393581751_gshared)(__this, method)
// System.Boolean HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUITexture>::UpdateCache(UnityEngine.GameObject)
#define ComponentAction_1_UpdateCache_m1836126492(__this, ___go0, method) ((  bool (*) (ComponentAction_1_t2995916491 *, GameObject_t3674682005 *, const MethodInfo*))ComponentAction_1_UpdateCache_m1764863969_gshared)(__this, ___go0, method)
