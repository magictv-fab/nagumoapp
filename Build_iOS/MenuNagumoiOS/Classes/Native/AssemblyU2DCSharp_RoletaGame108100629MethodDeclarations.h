﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RoletaGame
struct RoletaGame_t108100629;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void RoletaGame::.ctor()
extern "C"  void RoletaGame__ctor_m3779847206 (RoletaGame_t108100629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RoletaGame::Start()
extern "C"  void RoletaGame_Start_m2726984998 (RoletaGame_t108100629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RoletaGame::UpdateText()
extern "C"  void RoletaGame_UpdateText_m2625194676 (RoletaGame_t108100629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RoletaGame::BtnPlay()
extern "C"  void RoletaGame_BtnPlay_m3333097524 (RoletaGame_t108100629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RoletaGame::TurnRoletaWaiting()
extern "C"  void RoletaGame_TurnRoletaWaiting_m1552578417 (RoletaGame_t108100629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RoletaGame::TurnRoleta()
extern "C"  void RoletaGame_TurnRoleta_m3469051614 (RoletaGame_t108100629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RoletaGame::TurnRoletaComplete()
extern "C"  void RoletaGame_TurnRoletaComplete_m190315767 (RoletaGame_t108100629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator RoletaGame::ShowPopup()
extern "C"  Il2CppObject * RoletaGame_ShowPopup_m921381995 (RoletaGame_t108100629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
