﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VideoPlaybackBehaviour/<ResetToPortraitSmoothly>c__Iterator86
struct U3CResetToPortraitSmoothlyU3Ec__Iterator86_t338551677;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VideoPlaybackBehaviour/<ResetToPortraitSmoothly>c__Iterator86::.ctor()
extern "C"  void U3CResetToPortraitSmoothlyU3Ec__Iterator86__ctor_m3506154126 (U3CResetToPortraitSmoothlyU3Ec__Iterator86_t338551677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VideoPlaybackBehaviour/<ResetToPortraitSmoothly>c__Iterator86::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CResetToPortraitSmoothlyU3Ec__Iterator86_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2541434948 (U3CResetToPortraitSmoothlyU3Ec__Iterator86_t338551677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VideoPlaybackBehaviour/<ResetToPortraitSmoothly>c__Iterator86::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CResetToPortraitSmoothlyU3Ec__Iterator86_System_Collections_IEnumerator_get_Current_m1208894424 (U3CResetToPortraitSmoothlyU3Ec__Iterator86_t338551677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VideoPlaybackBehaviour/<ResetToPortraitSmoothly>c__Iterator86::MoveNext()
extern "C"  bool U3CResetToPortraitSmoothlyU3Ec__Iterator86_MoveNext_m1444303782 (U3CResetToPortraitSmoothlyU3Ec__Iterator86_t338551677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoPlaybackBehaviour/<ResetToPortraitSmoothly>c__Iterator86::Dispose()
extern "C"  void U3CResetToPortraitSmoothlyU3Ec__Iterator86_Dispose_m2058038091 (U3CResetToPortraitSmoothlyU3Ec__Iterator86_t338551677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoPlaybackBehaviour/<ResetToPortraitSmoothly>c__Iterator86::Reset()
extern "C"  void U3CResetToPortraitSmoothlyU3Ec__Iterator86_Reset_m1152587067 (U3CResetToPortraitSmoothlyU3Ec__Iterator86_t338551677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
