﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3211770239;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3211770239.h"
#include "mscorlib_System_Object4170816371.h"

// System.String HutongGames.PlayMaker.NamedVariable::get_Name()
extern "C"  String_t* NamedVariable_get_Name_m3214577877 (NamedVariable_t3211770239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.NamedVariable::set_Name(System.String)
extern "C"  void NamedVariable_set_Name_m926849846 (NamedVariable_t3211770239 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.NamedVariable::get_Tooltip()
extern "C"  String_t* NamedVariable_get_Tooltip_m3321065371 (NamedVariable_t3211770239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.NamedVariable::set_Tooltip(System.String)
extern "C"  void NamedVariable_set_Tooltip_m2642183102 (NamedVariable_t3211770239 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.NamedVariable::get_UseVariable()
extern "C"  bool NamedVariable_get_UseVariable_m692522704 (NamedVariable_t3211770239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.NamedVariable::set_UseVariable(System.Boolean)
extern "C"  void NamedVariable_set_UseVariable_m4266138971 (NamedVariable_t3211770239 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.NamedVariable::get_ShowInInspector()
extern "C"  bool NamedVariable_get_ShowInInspector_m2101101378 (NamedVariable_t3211770239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.NamedVariable::set_ShowInInspector(System.Boolean)
extern "C"  void NamedVariable_set_ShowInInspector_m2820077645 (NamedVariable_t3211770239 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.NamedVariable::get_NetworkSync()
extern "C"  bool NamedVariable_get_NetworkSync_m3720647190 (NamedVariable_t3211770239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.NamedVariable::set_NetworkSync(System.Boolean)
extern "C"  void NamedVariable_set_NetworkSync_m1835232801 (NamedVariable_t3211770239 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.NamedVariable::get_IsNone()
extern "C"  bool NamedVariable_get_IsNone_m281035543 (NamedVariable_t3211770239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.NamedVariable::get_UsesVariable()
extern "C"  bool NamedVariable_get_UsesVariable_m3517877629 (NamedVariable_t3211770239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.NamedVariable::.ctor()
extern "C"  void NamedVariable__ctor_m438771392 (NamedVariable_t3211770239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.NamedVariable::.ctor(System.String)
extern "C"  void NamedVariable__ctor_m395475010 (NamedVariable_t3211770239 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.NamedVariable::.ctor(HutongGames.PlayMaker.NamedVariable)
extern "C"  void NamedVariable__ctor_m3930624905 (NamedVariable_t3211770239 * __this, NamedVariable_t3211770239 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.NamedVariable::CompareTo(System.Object)
extern "C"  int32_t NamedVariable_CompareTo_m3543297572 (NamedVariable_t3211770239 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
