﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.StandardOleMarshalObject
struct StandardOleMarshalObject_t25375932;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.InteropServices.StandardOleMarshalObject::.ctor()
extern "C"  void StandardOleMarshalObject__ctor_m1621286064 (StandardOleMarshalObject_t25375932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
