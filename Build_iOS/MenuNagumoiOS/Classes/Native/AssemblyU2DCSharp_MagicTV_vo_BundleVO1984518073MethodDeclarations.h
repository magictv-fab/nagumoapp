﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.vo.BundleVO
struct BundleVO_t1984518073;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void MagicTV.vo.BundleVO::.ctor()
extern "C"  void BundleVO__ctor_m237557434 (BundleVO_t1984518073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MagicTV.vo.BundleVO::isDateActive()
extern "C"  bool BundleVO_isDateActive_m2532603028 (BundleVO_t1984518073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MagicTV.vo.BundleVO::ToString()
extern "C"  String_t* BundleVO_ToString_m558429107 (BundleVO_t1984518073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
