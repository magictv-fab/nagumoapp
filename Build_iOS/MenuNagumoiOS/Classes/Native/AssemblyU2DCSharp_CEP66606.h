﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CEP
struct  CEP_t66606  : public Il2CppObject
{
public:
	// System.String CEP::cep
	String_t* ___cep_0;
	// System.String CEP::logradouro
	String_t* ___logradouro_1;
	// System.String CEP::complemento
	String_t* ___complemento_2;
	// System.String CEP::bairro
	String_t* ___bairro_3;
	// System.String CEP::localidade
	String_t* ___localidade_4;
	// System.String CEP::uf
	String_t* ___uf_5;

public:
	inline static int32_t get_offset_of_cep_0() { return static_cast<int32_t>(offsetof(CEP_t66606, ___cep_0)); }
	inline String_t* get_cep_0() const { return ___cep_0; }
	inline String_t** get_address_of_cep_0() { return &___cep_0; }
	inline void set_cep_0(String_t* value)
	{
		___cep_0 = value;
		Il2CppCodeGenWriteBarrier(&___cep_0, value);
	}

	inline static int32_t get_offset_of_logradouro_1() { return static_cast<int32_t>(offsetof(CEP_t66606, ___logradouro_1)); }
	inline String_t* get_logradouro_1() const { return ___logradouro_1; }
	inline String_t** get_address_of_logradouro_1() { return &___logradouro_1; }
	inline void set_logradouro_1(String_t* value)
	{
		___logradouro_1 = value;
		Il2CppCodeGenWriteBarrier(&___logradouro_1, value);
	}

	inline static int32_t get_offset_of_complemento_2() { return static_cast<int32_t>(offsetof(CEP_t66606, ___complemento_2)); }
	inline String_t* get_complemento_2() const { return ___complemento_2; }
	inline String_t** get_address_of_complemento_2() { return &___complemento_2; }
	inline void set_complemento_2(String_t* value)
	{
		___complemento_2 = value;
		Il2CppCodeGenWriteBarrier(&___complemento_2, value);
	}

	inline static int32_t get_offset_of_bairro_3() { return static_cast<int32_t>(offsetof(CEP_t66606, ___bairro_3)); }
	inline String_t* get_bairro_3() const { return ___bairro_3; }
	inline String_t** get_address_of_bairro_3() { return &___bairro_3; }
	inline void set_bairro_3(String_t* value)
	{
		___bairro_3 = value;
		Il2CppCodeGenWriteBarrier(&___bairro_3, value);
	}

	inline static int32_t get_offset_of_localidade_4() { return static_cast<int32_t>(offsetof(CEP_t66606, ___localidade_4)); }
	inline String_t* get_localidade_4() const { return ___localidade_4; }
	inline String_t** get_address_of_localidade_4() { return &___localidade_4; }
	inline void set_localidade_4(String_t* value)
	{
		___localidade_4 = value;
		Il2CppCodeGenWriteBarrier(&___localidade_4, value);
	}

	inline static int32_t get_offset_of_uf_5() { return static_cast<int32_t>(offsetof(CEP_t66606, ___uf_5)); }
	inline String_t* get_uf_5() const { return ___uf_5; }
	inline String_t** get_address_of_uf_5() { return &___uf_5; }
	inline void set_uf_5(String_t* value)
	{
		___uf_5 = value;
		Il2CppCodeGenWriteBarrier(&___uf_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
