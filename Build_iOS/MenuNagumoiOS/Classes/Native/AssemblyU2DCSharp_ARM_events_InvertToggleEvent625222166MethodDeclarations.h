﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.events.InvertToggleEvent
struct InvertToggleEvent_t625222166;

#include "codegen/il2cpp-codegen.h"

// System.Void ARM.events.InvertToggleEvent::.ctor()
extern "C"  void InvertToggleEvent__ctor_m2621460878 (InvertToggleEvent_t625222166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.events.InvertToggleEvent::Start()
extern "C"  void InvertToggleEvent_Start_m1568598670 (InvertToggleEvent_t625222166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
