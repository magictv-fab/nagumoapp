﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Screenshot/OnCompleteEventHandler
struct OnCompleteEventHandler_t2690410097;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void Screenshot/OnCompleteEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnCompleteEventHandler__ctor_m844494280 (OnCompleteEventHandler_t2690410097 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Screenshot/OnCompleteEventHandler::Invoke(System.String)
extern "C"  void OnCompleteEventHandler_Invoke_m1589615136 (OnCompleteEventHandler_t2690410097 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Screenshot/OnCompleteEventHandler::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnCompleteEventHandler_BeginInvoke_m150615597 (OnCompleteEventHandler_t2690410097 * __this, String_t* ___path0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Screenshot/OnCompleteEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnCompleteEventHandler_EndInvoke_m1935379416 (OnCompleteEventHandler_t2690410097 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
