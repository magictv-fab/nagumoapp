﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AnimatorMatchTarget
struct AnimatorMatchTarget_t2687678941;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AnimatorMatchTarget::.ctor()
extern "C"  void AnimatorMatchTarget__ctor_m2276692473 (AnimatorMatchTarget_t2687678941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorMatchTarget::Reset()
extern "C"  void AnimatorMatchTarget_Reset_m4218092710 (AnimatorMatchTarget_t2687678941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorMatchTarget::OnEnter()
extern "C"  void AnimatorMatchTarget_OnEnter_m1154841488 (AnimatorMatchTarget_t2687678941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorMatchTarget::OnUpdate()
extern "C"  void AnimatorMatchTarget_OnUpdate_m573907123 (AnimatorMatchTarget_t2687678941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorMatchTarget::DoMatchTarget()
extern "C"  void AnimatorMatchTarget_DoMatchTarget_m2294716546 (AnimatorMatchTarget_t2687678941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
