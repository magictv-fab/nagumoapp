﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Action>
struct Dictionary_2_t3531948414;

#include "AssemblyU2DCSharp_ARM_abstracts_internet_InternetIn823636095.h"
#include "UnityEngine_UnityEngine_NetworkReachability612403035.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARM.device.internet.InternetInfo
struct  InternetInfo_t59940211  : public InternetInfoAbstract_t823636095
{
public:
	// UnityEngine.NetworkReachability ARM.device.internet.InternetInfo::wifiReachability
	int32_t ___wifiReachability_7;
	// System.Collections.Generic.Dictionary`2<UnityEngine.NetworkReachability,System.Action> ARM.device.internet.InternetInfo::_wifiReachabilityDictionary
	Dictionary_2_t3531948414 * ____wifiReachabilityDictionary_8;

public:
	inline static int32_t get_offset_of_wifiReachability_7() { return static_cast<int32_t>(offsetof(InternetInfo_t59940211, ___wifiReachability_7)); }
	inline int32_t get_wifiReachability_7() const { return ___wifiReachability_7; }
	inline int32_t* get_address_of_wifiReachability_7() { return &___wifiReachability_7; }
	inline void set_wifiReachability_7(int32_t value)
	{
		___wifiReachability_7 = value;
	}

	inline static int32_t get_offset_of__wifiReachabilityDictionary_8() { return static_cast<int32_t>(offsetof(InternetInfo_t59940211, ____wifiReachabilityDictionary_8)); }
	inline Dictionary_2_t3531948414 * get__wifiReachabilityDictionary_8() const { return ____wifiReachabilityDictionary_8; }
	inline Dictionary_2_t3531948414 ** get_address_of__wifiReachabilityDictionary_8() { return &____wifiReachabilityDictionary_8; }
	inline void set__wifiReachabilityDictionary_8(Dictionary_2_t3531948414 * value)
	{
		____wifiReachabilityDictionary_8 = value;
		Il2CppCodeGenWriteBarrier(&____wifiReachabilityDictionary_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
