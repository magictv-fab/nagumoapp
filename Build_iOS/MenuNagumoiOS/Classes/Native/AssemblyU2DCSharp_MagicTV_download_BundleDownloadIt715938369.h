﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// MagicTV.vo.UrlInfoVO
struct UrlInfoVO_t1761987528;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MagicTV.download.BundleDownloadItem
struct  BundleDownloadItem_t715938369  : public Il2CppObject
{
public:
	// System.String MagicTV.download.BundleDownloadItem::fileDestination
	String_t* ___fileDestination_0;
	// MagicTV.vo.UrlInfoVO MagicTV.download.BundleDownloadItem::UrlInfoVO
	UrlInfoVO_t1761987528 * ___UrlInfoVO_1;
	// System.Single MagicTV.download.BundleDownloadItem::fileWeight
	float ___fileWeight_2;

public:
	inline static int32_t get_offset_of_fileDestination_0() { return static_cast<int32_t>(offsetof(BundleDownloadItem_t715938369, ___fileDestination_0)); }
	inline String_t* get_fileDestination_0() const { return ___fileDestination_0; }
	inline String_t** get_address_of_fileDestination_0() { return &___fileDestination_0; }
	inline void set_fileDestination_0(String_t* value)
	{
		___fileDestination_0 = value;
		Il2CppCodeGenWriteBarrier(&___fileDestination_0, value);
	}

	inline static int32_t get_offset_of_UrlInfoVO_1() { return static_cast<int32_t>(offsetof(BundleDownloadItem_t715938369, ___UrlInfoVO_1)); }
	inline UrlInfoVO_t1761987528 * get_UrlInfoVO_1() const { return ___UrlInfoVO_1; }
	inline UrlInfoVO_t1761987528 ** get_address_of_UrlInfoVO_1() { return &___UrlInfoVO_1; }
	inline void set_UrlInfoVO_1(UrlInfoVO_t1761987528 * value)
	{
		___UrlInfoVO_1 = value;
		Il2CppCodeGenWriteBarrier(&___UrlInfoVO_1, value);
	}

	inline static int32_t get_offset_of_fileWeight_2() { return static_cast<int32_t>(offsetof(BundleDownloadItem_t715938369, ___fileWeight_2)); }
	inline float get_fileWeight_2() const { return ___fileWeight_2; }
	inline float* get_address_of_fileWeight_2() { return &___fileWeight_2; }
	inline void set_fileWeight_2(float value)
	{
		___fileWeight_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
