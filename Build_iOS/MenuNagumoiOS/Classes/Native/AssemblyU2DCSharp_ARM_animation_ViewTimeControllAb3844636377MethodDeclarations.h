﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARM.animation.ViewTimeControllAbstract/OnFinishedEvent
struct OnFinishedEvent_t3844636377;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ARM.animation.ViewTimeControllAbstract/OnFinishedEvent::.ctor(System.Object,System.IntPtr)
extern "C"  void OnFinishedEvent__ctor_m2196374784 (OnFinishedEvent_t3844636377 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.animation.ViewTimeControllAbstract/OnFinishedEvent::Invoke()
extern "C"  void OnFinishedEvent_Invoke_m3997102426 (OnFinishedEvent_t3844636377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ARM.animation.ViewTimeControllAbstract/OnFinishedEvent::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnFinishedEvent_BeginInvoke_m2413313385 (OnFinishedEvent_t3844636377 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARM.animation.ViewTimeControllAbstract/OnFinishedEvent::EndInvoke(System.IAsyncResult)
extern "C"  void OnFinishedEvent_EndInvoke_m2466772752 (OnFinishedEvent_t3844636377 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
