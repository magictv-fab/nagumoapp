﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "PlayMaker_HutongGames_PlayMaker_LogLevel284580066.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DebugGameObject
struct  DebugGameObject_t2633884050  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.LogLevel HutongGames.PlayMaker.Actions.DebugGameObject::logLevel
	int32_t ___logLevel_9;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.DebugGameObject::gameObject
	FsmGameObject_t1697147867 * ___gameObject_10;

public:
	inline static int32_t get_offset_of_logLevel_9() { return static_cast<int32_t>(offsetof(DebugGameObject_t2633884050, ___logLevel_9)); }
	inline int32_t get_logLevel_9() const { return ___logLevel_9; }
	inline int32_t* get_address_of_logLevel_9() { return &___logLevel_9; }
	inline void set_logLevel_9(int32_t value)
	{
		___logLevel_9 = value;
	}

	inline static int32_t get_offset_of_gameObject_10() { return static_cast<int32_t>(offsetof(DebugGameObject_t2633884050, ___gameObject_10)); }
	inline FsmGameObject_t1697147867 * get_gameObject_10() const { return ___gameObject_10; }
	inline FsmGameObject_t1697147867 ** get_address_of_gameObject_10() { return &___gameObject_10; }
	inline void set_gameObject_10(FsmGameObject_t1697147867 * value)
	{
		___gameObject_10 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
