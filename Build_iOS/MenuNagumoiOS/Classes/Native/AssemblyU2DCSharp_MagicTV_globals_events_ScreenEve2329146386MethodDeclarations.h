﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MagicTV.globals.events.ScreenEvents/OnStringEventHandler
struct OnStringEventHandler_t2329146386;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void MagicTV.globals.events.ScreenEvents/OnStringEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void OnStringEventHandler__ctor_m121075641 (OnStringEventHandler_t2329146386 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ScreenEvents/OnStringEventHandler::Invoke(System.String)
extern "C"  void OnStringEventHandler_Invoke_m1329382543 (OnStringEventHandler_t2329146386 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult MagicTV.globals.events.ScreenEvents/OnStringEventHandler::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnStringEventHandler_BeginInvoke_m2560070164 (OnStringEventHandler_t2329146386 * __this, String_t* ___path0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MagicTV.globals.events.ScreenEvents/OnStringEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void OnStringEventHandler_EndInvoke_m3085282889 (OnStringEventHandler_t2329146386 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
