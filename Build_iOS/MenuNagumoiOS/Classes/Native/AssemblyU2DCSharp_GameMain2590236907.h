﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;
// UnityEngine.AudioClip
struct AudioClip_t794140988;
// UnityEngine.TextMesh
struct TextMesh_t2567681854;
// ServerControl
struct ServerControl_t2725829754;
// LightsControl
struct LightsControl_t2444629088;
// UnityEngine.AudioSource
struct AudioSource_t1740077639;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_PointsAngle2793040272.h"
#include "AssemblyU2DCSharp_iTween_EaseType2734598229.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameMain
struct  GameMain_t2590236907  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject GameMain::buttonPlay
	GameObject_t3674682005 * ___buttonPlay_2;
	// PointsAngle GameMain::premio
	int32_t ___premio_3;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GameMain::roletaLst
	List_1_t747900261 * ___roletaLst_4;
	// iTween/EaseType GameMain::turnEaseType
	int32_t ___turnEaseType_5;
	// UnityEngine.AudioClip GameMain::playSound
	AudioClip_t794140988 * ___playSound_6;
	// UnityEngine.AudioClip GameMain::winSound
	AudioClip_t794140988 * ___winSound_7;
	// UnityEngine.AudioClip GameMain::loseSound
	AudioClip_t794140988 * ___loseSound_8;
	// System.Single GameMain::minTime
	float ___minTime_9;
	// System.Single GameMain::delayTime
	float ___delayTime_10;
	// UnityEngine.TextMesh GameMain::letreiroDigital
	TextMesh_t2567681854 * ___letreiroDigital_11;
	// System.Int32 GameMain::jogadasRestantes
	int32_t ___jogadasRestantes_12;
	// System.Boolean GameMain::onGame
	bool ___onGame_13;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GameMain::alertLst
	List_1_t747900261 * ___alertLst_14;
	// ServerControl GameMain::serverControl
	ServerControl_t2725829754 * ___serverControl_15;
	// LightsControl GameMain::lightControl
	LightsControl_t2444629088 * ___lightControl_16;
	// UnityEngine.AudioSource GameMain::audioSource
	AudioSource_t1740077639 * ___audioSource_17;
	// System.Int32 GameMain::counterStop
	int32_t ___counterStop_18;
	// UnityEngine.Color GameMain::startButtonColor
	Color_t4194546905  ___startButtonColor_19;

public:
	inline static int32_t get_offset_of_buttonPlay_2() { return static_cast<int32_t>(offsetof(GameMain_t2590236907, ___buttonPlay_2)); }
	inline GameObject_t3674682005 * get_buttonPlay_2() const { return ___buttonPlay_2; }
	inline GameObject_t3674682005 ** get_address_of_buttonPlay_2() { return &___buttonPlay_2; }
	inline void set_buttonPlay_2(GameObject_t3674682005 * value)
	{
		___buttonPlay_2 = value;
		Il2CppCodeGenWriteBarrier(&___buttonPlay_2, value);
	}

	inline static int32_t get_offset_of_premio_3() { return static_cast<int32_t>(offsetof(GameMain_t2590236907, ___premio_3)); }
	inline int32_t get_premio_3() const { return ___premio_3; }
	inline int32_t* get_address_of_premio_3() { return &___premio_3; }
	inline void set_premio_3(int32_t value)
	{
		___premio_3 = value;
	}

	inline static int32_t get_offset_of_roletaLst_4() { return static_cast<int32_t>(offsetof(GameMain_t2590236907, ___roletaLst_4)); }
	inline List_1_t747900261 * get_roletaLst_4() const { return ___roletaLst_4; }
	inline List_1_t747900261 ** get_address_of_roletaLst_4() { return &___roletaLst_4; }
	inline void set_roletaLst_4(List_1_t747900261 * value)
	{
		___roletaLst_4 = value;
		Il2CppCodeGenWriteBarrier(&___roletaLst_4, value);
	}

	inline static int32_t get_offset_of_turnEaseType_5() { return static_cast<int32_t>(offsetof(GameMain_t2590236907, ___turnEaseType_5)); }
	inline int32_t get_turnEaseType_5() const { return ___turnEaseType_5; }
	inline int32_t* get_address_of_turnEaseType_5() { return &___turnEaseType_5; }
	inline void set_turnEaseType_5(int32_t value)
	{
		___turnEaseType_5 = value;
	}

	inline static int32_t get_offset_of_playSound_6() { return static_cast<int32_t>(offsetof(GameMain_t2590236907, ___playSound_6)); }
	inline AudioClip_t794140988 * get_playSound_6() const { return ___playSound_6; }
	inline AudioClip_t794140988 ** get_address_of_playSound_6() { return &___playSound_6; }
	inline void set_playSound_6(AudioClip_t794140988 * value)
	{
		___playSound_6 = value;
		Il2CppCodeGenWriteBarrier(&___playSound_6, value);
	}

	inline static int32_t get_offset_of_winSound_7() { return static_cast<int32_t>(offsetof(GameMain_t2590236907, ___winSound_7)); }
	inline AudioClip_t794140988 * get_winSound_7() const { return ___winSound_7; }
	inline AudioClip_t794140988 ** get_address_of_winSound_7() { return &___winSound_7; }
	inline void set_winSound_7(AudioClip_t794140988 * value)
	{
		___winSound_7 = value;
		Il2CppCodeGenWriteBarrier(&___winSound_7, value);
	}

	inline static int32_t get_offset_of_loseSound_8() { return static_cast<int32_t>(offsetof(GameMain_t2590236907, ___loseSound_8)); }
	inline AudioClip_t794140988 * get_loseSound_8() const { return ___loseSound_8; }
	inline AudioClip_t794140988 ** get_address_of_loseSound_8() { return &___loseSound_8; }
	inline void set_loseSound_8(AudioClip_t794140988 * value)
	{
		___loseSound_8 = value;
		Il2CppCodeGenWriteBarrier(&___loseSound_8, value);
	}

	inline static int32_t get_offset_of_minTime_9() { return static_cast<int32_t>(offsetof(GameMain_t2590236907, ___minTime_9)); }
	inline float get_minTime_9() const { return ___minTime_9; }
	inline float* get_address_of_minTime_9() { return &___minTime_9; }
	inline void set_minTime_9(float value)
	{
		___minTime_9 = value;
	}

	inline static int32_t get_offset_of_delayTime_10() { return static_cast<int32_t>(offsetof(GameMain_t2590236907, ___delayTime_10)); }
	inline float get_delayTime_10() const { return ___delayTime_10; }
	inline float* get_address_of_delayTime_10() { return &___delayTime_10; }
	inline void set_delayTime_10(float value)
	{
		___delayTime_10 = value;
	}

	inline static int32_t get_offset_of_letreiroDigital_11() { return static_cast<int32_t>(offsetof(GameMain_t2590236907, ___letreiroDigital_11)); }
	inline TextMesh_t2567681854 * get_letreiroDigital_11() const { return ___letreiroDigital_11; }
	inline TextMesh_t2567681854 ** get_address_of_letreiroDigital_11() { return &___letreiroDigital_11; }
	inline void set_letreiroDigital_11(TextMesh_t2567681854 * value)
	{
		___letreiroDigital_11 = value;
		Il2CppCodeGenWriteBarrier(&___letreiroDigital_11, value);
	}

	inline static int32_t get_offset_of_jogadasRestantes_12() { return static_cast<int32_t>(offsetof(GameMain_t2590236907, ___jogadasRestantes_12)); }
	inline int32_t get_jogadasRestantes_12() const { return ___jogadasRestantes_12; }
	inline int32_t* get_address_of_jogadasRestantes_12() { return &___jogadasRestantes_12; }
	inline void set_jogadasRestantes_12(int32_t value)
	{
		___jogadasRestantes_12 = value;
	}

	inline static int32_t get_offset_of_onGame_13() { return static_cast<int32_t>(offsetof(GameMain_t2590236907, ___onGame_13)); }
	inline bool get_onGame_13() const { return ___onGame_13; }
	inline bool* get_address_of_onGame_13() { return &___onGame_13; }
	inline void set_onGame_13(bool value)
	{
		___onGame_13 = value;
	}

	inline static int32_t get_offset_of_alertLst_14() { return static_cast<int32_t>(offsetof(GameMain_t2590236907, ___alertLst_14)); }
	inline List_1_t747900261 * get_alertLst_14() const { return ___alertLst_14; }
	inline List_1_t747900261 ** get_address_of_alertLst_14() { return &___alertLst_14; }
	inline void set_alertLst_14(List_1_t747900261 * value)
	{
		___alertLst_14 = value;
		Il2CppCodeGenWriteBarrier(&___alertLst_14, value);
	}

	inline static int32_t get_offset_of_serverControl_15() { return static_cast<int32_t>(offsetof(GameMain_t2590236907, ___serverControl_15)); }
	inline ServerControl_t2725829754 * get_serverControl_15() const { return ___serverControl_15; }
	inline ServerControl_t2725829754 ** get_address_of_serverControl_15() { return &___serverControl_15; }
	inline void set_serverControl_15(ServerControl_t2725829754 * value)
	{
		___serverControl_15 = value;
		Il2CppCodeGenWriteBarrier(&___serverControl_15, value);
	}

	inline static int32_t get_offset_of_lightControl_16() { return static_cast<int32_t>(offsetof(GameMain_t2590236907, ___lightControl_16)); }
	inline LightsControl_t2444629088 * get_lightControl_16() const { return ___lightControl_16; }
	inline LightsControl_t2444629088 ** get_address_of_lightControl_16() { return &___lightControl_16; }
	inline void set_lightControl_16(LightsControl_t2444629088 * value)
	{
		___lightControl_16 = value;
		Il2CppCodeGenWriteBarrier(&___lightControl_16, value);
	}

	inline static int32_t get_offset_of_audioSource_17() { return static_cast<int32_t>(offsetof(GameMain_t2590236907, ___audioSource_17)); }
	inline AudioSource_t1740077639 * get_audioSource_17() const { return ___audioSource_17; }
	inline AudioSource_t1740077639 ** get_address_of_audioSource_17() { return &___audioSource_17; }
	inline void set_audioSource_17(AudioSource_t1740077639 * value)
	{
		___audioSource_17 = value;
		Il2CppCodeGenWriteBarrier(&___audioSource_17, value);
	}

	inline static int32_t get_offset_of_counterStop_18() { return static_cast<int32_t>(offsetof(GameMain_t2590236907, ___counterStop_18)); }
	inline int32_t get_counterStop_18() const { return ___counterStop_18; }
	inline int32_t* get_address_of_counterStop_18() { return &___counterStop_18; }
	inline void set_counterStop_18(int32_t value)
	{
		___counterStop_18 = value;
	}

	inline static int32_t get_offset_of_startButtonColor_19() { return static_cast<int32_t>(offsetof(GameMain_t2590236907, ___startButtonColor_19)); }
	inline Color_t4194546905  get_startButtonColor_19() const { return ___startButtonColor_19; }
	inline Color_t4194546905 * get_address_of_startButtonColor_19() { return &___startButtonColor_19; }
	inline void set_startButtonColor_19(Color_t4194546905  value)
	{
		___startButtonColor_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
