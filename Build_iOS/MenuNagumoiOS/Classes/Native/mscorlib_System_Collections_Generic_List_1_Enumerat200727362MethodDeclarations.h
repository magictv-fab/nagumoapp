﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<DataRow>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2005554199(__this, ___l0, method) ((  void (*) (Enumerator_t200727362 *, List_1_t181054592 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<DataRow>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1342721499(__this, method) ((  void (*) (Enumerator_t200727362 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<DataRow>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1269529351(__this, method) ((  Il2CppObject * (*) (Enumerator_t200727362 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<DataRow>::Dispose()
#define Enumerator_Dispose_m442505148(__this, method) ((  void (*) (Enumerator_t200727362 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<DataRow>::VerifyState()
#define Enumerator_VerifyState_m2718537973(__this, method) ((  void (*) (Enumerator_t200727362 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<DataRow>::MoveNext()
#define Enumerator_MoveNext_m1556384455(__this, method) ((  bool (*) (Enumerator_t200727362 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<DataRow>::get_Current()
#define Enumerator_get_Current_m3875134508(__this, method) ((  DataRow_t3107836336 * (*) (Enumerator_t200727362 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
