﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.DecodeHintType,System.Object>
struct KeyCollection_t2808875952;
// System.Collections.Generic.Dictionary`2<ZXing.DecodeHintType,System.Object>
struct Dictionary_2_t1182116501;
// System.Collections.Generic.IEnumerator`1<ZXing.DecodeHintType>
struct IEnumerator_1_t4007646398;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// ZXing.DecodeHintType[]
struct DecodeHintTypeU5BU5D_t244884712;

#include "codegen/il2cpp-codegen.h"
#include "QRCode_ZXing_DecodeHintType2095781349.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1797052555.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.DecodeHintType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m3059621971_gshared (KeyCollection_t2808875952 * __this, Dictionary_2_t1182116501 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m3059621971(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2808875952 *, Dictionary_2_t1182116501 *, const MethodInfo*))KeyCollection__ctor_m3059621971_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.DecodeHintType,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m726864163_gshared (KeyCollection_t2808875952 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m726864163(__this, ___item0, method) ((  void (*) (KeyCollection_t2808875952 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m726864163_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.DecodeHintType,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1046553434_gshared (KeyCollection_t2808875952 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1046553434(__this, method) ((  void (*) (KeyCollection_t2808875952 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1046553434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.DecodeHintType,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m461058187_gshared (KeyCollection_t2808875952 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m461058187(__this, ___item0, method) ((  bool (*) (KeyCollection_t2808875952 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m461058187_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.DecodeHintType,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2472560816_gshared (KeyCollection_t2808875952 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2472560816(__this, ___item0, method) ((  bool (*) (KeyCollection_t2808875952 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2472560816_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.DecodeHintType,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m915550124_gshared (KeyCollection_t2808875952 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m915550124(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2808875952 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m915550124_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.DecodeHintType,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m959893708_gshared (KeyCollection_t2808875952 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m959893708(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2808875952 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m959893708_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.DecodeHintType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1264611419_gshared (KeyCollection_t2808875952 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1264611419(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2808875952 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1264611419_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.DecodeHintType,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3576344876_gshared (KeyCollection_t2808875952 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3576344876(__this, method) ((  bool (*) (KeyCollection_t2808875952 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3576344876_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.DecodeHintType,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m959709406_gshared (KeyCollection_t2808875952 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m959709406(__this, method) ((  bool (*) (KeyCollection_t2808875952 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m959709406_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.DecodeHintType,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m3977197456_gshared (KeyCollection_t2808875952 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3977197456(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2808875952 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3977197456_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.DecodeHintType,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m2869531400_gshared (KeyCollection_t2808875952 * __this, DecodeHintTypeU5BU5D_t244884712* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m2869531400(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2808875952 *, DecodeHintTypeU5BU5D_t244884712*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2869531400_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.DecodeHintType,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1797052555  KeyCollection_GetEnumerator_m686606421_gshared (KeyCollection_t2808875952 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m686606421(__this, method) ((  Enumerator_t1797052555  (*) (KeyCollection_t2808875952 *, const MethodInfo*))KeyCollection_GetEnumerator_m686606421_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<ZXing.DecodeHintType,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m199071384_gshared (KeyCollection_t2808875952 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m199071384(__this, method) ((  int32_t (*) (KeyCollection_t2808875952 *, const MethodInfo*))KeyCollection_get_Count_m199071384_gshared)(__this, method)
