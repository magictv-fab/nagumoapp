﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SimpleJSON
struct SimpleJSON_t561564090;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void SimpleJSON::.ctor()
extern "C"  void SimpleJSON__ctor_m3363905569 (SimpleJSON_t561564090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SimpleJSON::NeedEscape(System.String,System.Int32)
extern "C"  bool SimpleJSON_NeedEscape_m1424661699 (Il2CppObject * __this /* static, unused */, String_t* ___src0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SimpleJSON::EscapeString(System.String)
extern "C"  String_t* SimpleJSON_EscapeString_m3776619306 (Il2CppObject * __this /* static, unused */, String_t* ___src0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
