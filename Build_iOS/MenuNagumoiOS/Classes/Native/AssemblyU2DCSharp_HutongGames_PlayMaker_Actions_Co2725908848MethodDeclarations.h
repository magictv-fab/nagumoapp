﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.CollisionEvent
struct CollisionEvent_t2725908848;
// UnityEngine.Collision
struct Collision_t2494107688;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t2416790841;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision2494107688.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit2416790841.h"

// System.Void HutongGames.PlayMaker.Actions.CollisionEvent::.ctor()
extern "C"  void CollisionEvent__ctor_m1056806134 (CollisionEvent_t2725908848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CollisionEvent::Reset()
extern "C"  void CollisionEvent_Reset_m2998206371 (CollisionEvent_t2725908848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CollisionEvent::Awake()
extern "C"  void CollisionEvent_Awake_m1294411353 (CollisionEvent_t2725908848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CollisionEvent::StoreCollisionInfo(UnityEngine.Collision)
extern "C"  void CollisionEvent_StoreCollisionInfo_m2435541930 (CollisionEvent_t2725908848 * __this, Collision_t2494107688 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CollisionEvent::DoCollisionEnter(UnityEngine.Collision)
extern "C"  void CollisionEvent_DoCollisionEnter_m858586008 (CollisionEvent_t2725908848 * __this, Collision_t2494107688 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CollisionEvent::DoCollisionStay(UnityEngine.Collision)
extern "C"  void CollisionEvent_DoCollisionStay_m1075927171 (CollisionEvent_t2725908848 * __this, Collision_t2494107688 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CollisionEvent::DoCollisionExit(UnityEngine.Collision)
extern "C"  void CollisionEvent_DoCollisionExit_m1519540254 (CollisionEvent_t2725908848 * __this, Collision_t2494107688 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CollisionEvent::DoControllerColliderHit(UnityEngine.ControllerColliderHit)
extern "C"  void CollisionEvent_DoControllerColliderHit_m1076704762 (CollisionEvent_t2725908848 * __this, ControllerColliderHit_t2416790841 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Actions.CollisionEvent::ErrorCheck()
extern "C"  String_t* CollisionEvent_ErrorCheck_m4239141905 (CollisionEvent_t2725908848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
