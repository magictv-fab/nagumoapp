﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>
struct Dictionary_2_t4287577744;
// System.Collections.Generic.IEqualityComparer`1<InApp.InAppEventDispatcher/EventNames>
struct IEqualityComparer_1_t2405670250;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<InApp.InAppEventDispatcher/EventNames>
struct ICollection_1_t2509225833;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<InApp.InAppEventDispatcher/EventNames,System.Object>[]
struct KeyValuePair_2U5BU5D_t1464939079;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<InApp.InAppEventDispatcher/EventNames,System.Object>>
struct IEnumerator_1_t1803256203;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<InApp.InAppEventDispatcher/EventNames,System.Object>
struct KeyCollection_t1619369899;
// System.Collections.Generic.Dictionary`2/ValueCollection<InApp.InAppEventDispatcher/EventNames,System.Object>
struct ValueCollection_t2988183457;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24186358450.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_InApp_InAppEventDispatcher_Event1614635846.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1309933840.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m3193402279_gshared (Dictionary_2_t4287577744 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m3193402279(__this, method) ((  void (*) (Dictionary_2_t4287577744 *, const MethodInfo*))Dictionary_2__ctor_m3193402279_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1695601822_gshared (Dictionary_2_t4287577744 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m1695601822(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t4287577744 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1695601822_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m2110383736_gshared (Dictionary_2_t4287577744 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m2110383736(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t4287577744 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m2110383736_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m3778415720_gshared (Dictionary_2_t4287577744 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m3778415720(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t4287577744 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m3778415720_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2166782663_gshared (Dictionary_2_t4287577744 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2166782663(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t4287577744 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2166782663_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m746560941_gshared (Dictionary_2_t4287577744 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m746560941(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t4287577744 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m746560941_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m2553080667_gshared (Dictionary_2_t4287577744 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m2553080667(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t4287577744 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m2553080667_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3108295322_gshared (Dictionary_2_t4287577744 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3108295322(__this, method) ((  bool (*) (Dictionary_2_t4287577744 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3108295322_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3463770399_gshared (Dictionary_2_t4287577744 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3463770399(__this, method) ((  bool (*) (Dictionary_2_t4287577744 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3463770399_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m1283860737_gshared (Dictionary_2_t4287577744 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m1283860737(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t4287577744 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m1283860737_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m3803715302_gshared (Dictionary_2_t4287577744 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m3803715302(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t4287577744 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m3803715302_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m2343104427_gshared (Dictionary_2_t4287577744 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m2343104427(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t4287577744 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m2343104427_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m1984091883_gshared (Dictionary_2_t4287577744 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m1984091883(__this, ___key0, method) ((  bool (*) (Dictionary_2_t4287577744 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1984091883_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m1683037476_gshared (Dictionary_2_t4287577744 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m1683037476(__this, ___key0, method) ((  void (*) (Dictionary_2_t4287577744 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m1683037476_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3537975369_gshared (Dictionary_2_t4287577744 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3537975369(__this, method) ((  bool (*) (Dictionary_2_t4287577744 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3537975369_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m4191226229_gshared (Dictionary_2_t4287577744 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m4191226229(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t4287577744 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m4191226229_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2535887693_gshared (Dictionary_2_t4287577744 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2535887693(__this, method) ((  bool (*) (Dictionary_2_t4287577744 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2535887693_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1224796090_gshared (Dictionary_2_t4287577744 * __this, KeyValuePair_2_t4186358450  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1224796090(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t4287577744 *, KeyValuePair_2_t4186358450 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1224796090_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m378725832_gshared (Dictionary_2_t4287577744 * __this, KeyValuePair_2_t4186358450  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m378725832(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t4287577744 *, KeyValuePair_2_t4186358450 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m378725832_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m35462430_gshared (Dictionary_2_t4287577744 * __this, KeyValuePair_2U5BU5D_t1464939079* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m35462430(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t4287577744 *, KeyValuePair_2U5BU5D_t1464939079*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m35462430_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2343558893_gshared (Dictionary_2_t4287577744 * __this, KeyValuePair_2_t4186358450  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2343558893(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t4287577744 *, KeyValuePair_2_t4186358450 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2343558893_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m417537917_gshared (Dictionary_2_t4287577744 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m417537917(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t4287577744 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m417537917_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m4188815544_gshared (Dictionary_2_t4287577744 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m4188815544(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t4287577744 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m4188815544_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3171402421_gshared (Dictionary_2_t4287577744 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3171402421(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t4287577744 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3171402421_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m4137903696_gshared (Dictionary_2_t4287577744 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m4137903696(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t4287577744 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m4137903696_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m271623055_gshared (Dictionary_2_t4287577744 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m271623055(__this, method) ((  int32_t (*) (Dictionary_2_t4287577744 *, const MethodInfo*))Dictionary_2_get_Count_m271623055_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m2463014012_gshared (Dictionary_2_t4287577744 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m2463014012(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t4287577744 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m2463014012_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m1760082215_gshared (Dictionary_2_t4287577744 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m1760082215(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t4287577744 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m1760082215_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m2774242079_gshared (Dictionary_2_t4287577744 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m2774242079(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t4287577744 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m2774242079_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m616663672_gshared (Dictionary_2_t4287577744 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m616663672(__this, ___size0, method) ((  void (*) (Dictionary_2_t4287577744 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m616663672_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m1889842292_gshared (Dictionary_2_t4287577744 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m1889842292(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t4287577744 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m1889842292_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t4186358450  Dictionary_2_make_pair_m3071858624_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m3071858624(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t4186358450  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m3071858624_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::pick_key(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_key_m2547716150_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m2547716150(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_key_m2547716150_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m3779696502_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m3779696502(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m3779696502_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m1604568283_gshared (Dictionary_2_t4287577744 * __this, KeyValuePair_2U5BU5D_t1464939079* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m1604568283(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t4287577744 *, KeyValuePair_2U5BU5D_t1464939079*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1604568283_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m269019505_gshared (Dictionary_2_t4287577744 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m269019505(__this, method) ((  void (*) (Dictionary_2_t4287577744 *, const MethodInfo*))Dictionary_2_Resize_m269019505_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m961146094_gshared (Dictionary_2_t4287577744 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m961146094(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t4287577744 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m961146094_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m599535570_gshared (Dictionary_2_t4287577744 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m599535570(__this, method) ((  void (*) (Dictionary_2_t4287577744 *, const MethodInfo*))Dictionary_2_Clear_m599535570_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m3570166840_gshared (Dictionary_2_t4287577744 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m3570166840(__this, ___key0, method) ((  bool (*) (Dictionary_2_t4287577744 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m3570166840_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m2015814712_gshared (Dictionary_2_t4287577744 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m2015814712(__this, ___value0, method) ((  bool (*) (Dictionary_2_t4287577744 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m2015814712_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m1014524869_gshared (Dictionary_2_t4287577744 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m1014524869(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t4287577744 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m1014524869_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m2997422015_gshared (Dictionary_2_t4287577744 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m2997422015(__this, ___sender0, method) ((  void (*) (Dictionary_2_t4287577744 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m2997422015_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m2711554488_gshared (Dictionary_2_t4287577744 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m2711554488(__this, ___key0, method) ((  bool (*) (Dictionary_2_t4287577744 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m2711554488_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m2787445905_gshared (Dictionary_2_t4287577744 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m2787445905(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t4287577744 *, int32_t, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m2787445905_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::get_Keys()
extern "C"  KeyCollection_t1619369899 * Dictionary_2_get_Keys_m3564432398_gshared (Dictionary_2_t4287577744 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m3564432398(__this, method) ((  KeyCollection_t1619369899 * (*) (Dictionary_2_t4287577744 *, const MethodInfo*))Dictionary_2_get_Keys_m3564432398_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::get_Values()
extern "C"  ValueCollection_t2988183457 * Dictionary_2_get_Values_m2117423886_gshared (Dictionary_2_t4287577744 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m2117423886(__this, method) ((  ValueCollection_t2988183457 * (*) (Dictionary_2_t4287577744 *, const MethodInfo*))Dictionary_2_get_Values_m2117423886_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m1997575057_gshared (Dictionary_2_t4287577744 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m1997575057(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t4287577744 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m1997575057_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m2092010641_gshared (Dictionary_2_t4287577744 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m2092010641(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t4287577744 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m2092010641_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m144078747_gshared (Dictionary_2_t4287577744 * __this, KeyValuePair_2_t4186358450  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m144078747(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t4287577744 *, KeyValuePair_2_t4186358450 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m144078747_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1309933840  Dictionary_2_GetEnumerator_m286662060_gshared (Dictionary_2_t4287577744 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m286662060(__this, method) ((  Enumerator_t1309933840  (*) (Dictionary_2_t4287577744 *, const MethodInfo*))Dictionary_2_GetEnumerator_m286662060_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<InApp.InAppEventDispatcher/EventNames,System.Object>::<CopyTo>m__2(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__2_m3395001913_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__2_m3395001913(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__2_m3395001913_gshared)(__this /* static, unused */, ___key0, ___value1, method)
