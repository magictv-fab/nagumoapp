﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZXing.BarcodeReader
struct BarcodeReader_t240638149;
// ZXing.Reader
struct Reader_t2610170425;
// System.Func`4<UnityEngine.Color32[],System.Int32,System.Int32,ZXing.LuminanceSource>
struct Func_4_t4289141939;
// System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer>
struct Func_2_t2392815316;
// ZXing.LuminanceSource
struct LuminanceSource_t1231523093;
// UnityEngine.Color32[]
struct Color32U5BU5D_t2960766953;

#include "codegen/il2cpp-codegen.h"

// System.Void ZXing.BarcodeReader::.ctor()
extern "C"  void BarcodeReader__ctor_m891835954 (BarcodeReader_t240638149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.BarcodeReader::.ctor(ZXing.Reader,System.Func`4<UnityEngine.Color32[],System.Int32,System.Int32,ZXing.LuminanceSource>,System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer>)
extern "C"  void BarcodeReader__ctor_m2918615106 (BarcodeReader_t240638149 * __this, Il2CppObject * ___reader0, Func_4_t4289141939 * ___createLuminanceSource1, Func_2_t2392815316 * ___createBinarizer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZXing.BarcodeReader::.cctor()
extern "C"  void BarcodeReader__cctor_m1395014587 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ZXing.LuminanceSource ZXing.BarcodeReader::<.cctor>b__4(UnityEngine.Color32[],System.Int32,System.Int32)
extern "C"  LuminanceSource_t1231523093 * BarcodeReader_U3C_cctorU3Eb__4_m2628328306 (Il2CppObject * __this /* static, unused */, Color32U5BU5D_t2960766953* ___rawColor320, int32_t ___width1, int32_t ___height2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
