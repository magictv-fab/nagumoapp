﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_IO_Compression_DeflateStream2030147241MethodDeclarations.h"
#include "AssemblyU2DCSharp_UniWebViewInterface2459884048MethodDeclarations.h"

extern const Il2CppMethodPointer g_ReversePInvokeWrapperPointers[3] = 
{
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_DeflateStream_UnmanagedRead_m404803083),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_DeflateStream_UnmanagedWrite_m223973422),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_UniWebViewInterface_SendMessage_m1406601688),
};
